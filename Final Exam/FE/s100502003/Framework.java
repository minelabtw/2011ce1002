package fe.s100502003;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.Timer;

public class Framework extends JFrame {
	private Locale[] locales = { Locale.CHINESE, Locale.FRANCE, Locale.JAPAN, Locale.ENGLISH};

	private ResourceBundle res = ResourceBundle.getBundle("Confidential");
	private String Title = res.getString("Title");
	private String Language = res.getString("Language");
	private String Position = res.getString("Position");
	private String Result = res.getString("Result");
	
	private JLabel title = new JLabel(Title);
	private JLabel language = new JLabel(Language + ": ");
	private JLabel position = new JLabel(Position + ": ");
	private JLabel result = new JLabel(Result + ": ");

	private JComboBox jcbLanguage = new JComboBox(locales);

	private int xPoint, yPoint;
	private String getx_y = xPoint + " , " + yPoint;
	private String Solution;
	private JLabel position_x_y = new JLabel(getx_y);
	private JLabel solution = new JLabel(Solution);
	
	DrawBuildings draw = new DrawBuildings();
	
	public Framework() {
		
		
		jcbLanguage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setLocale(locales[jcbLanguage.getSelectedIndex()]);
				res = ResourceBundle.getBundle("Confidential", getLocale());
				title.setText(res.getString("Title"));
				language.setText(res.getString("Language"));
				position.setText(res.getString("Position"));
				result.setText(res.getString("Result"));
			}
		});
		
		addMouseListener(new MouseListener() {
			public void mousePressed(MouseEvent e) {
				draw.setCheck(true);
				xPoint = getX();
				yPoint = getY();
				
				draw.setMousePositionX(xPoint);
				draw.setMousePositionY(yPoint);
				repaint();
			}

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		JPanel panel1 = new JPanel();
		panel1.setLayout(new GridLayout(1,2));
		panel1.add(language);
		panel1.add(jcbLanguage);
		
		JPanel panel_top = new JPanel();
		panel_top.setLayout(new GridLayout(1,2));
		panel_top.add(title);
		panel_top.add(panel1);
		
		JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayout(1,2));
		panel2.add(position);
		panel2.add(position_x_y);
		
		JPanel panel3 = new JPanel();
		panel3.setLayout(new GridLayout(1,2));
		panel3.add(result);
		panel3.add(solution);
		
		JPanel panel_bottom = new JPanel();
		panel_bottom.setLayout(new GridLayout(1,2));
		panel_bottom.add(panel2);
		panel_bottom.add(panel3);
		
		setLayout(new BorderLayout(5,5));
		add(panel_top, BorderLayout.NORTH);
		add(draw, BorderLayout.CENTER);
		add(panel_bottom, BorderLayout.SOUTH);
		
	}
	

}
