package fe.s100502003;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FE11 extends JApplet {
	private JLabel icons = new JLabel();
	private int now_pointer = 0;
	
	// 圖片input進去時會有紅字，可能是哪裡打錯了，但不知道是哪裡
	
//	private ImageIcon[] pictures = {
//		new ImageIcon(getClass.getResource("image/cat_icon.jpg")), 
//		new ImageIcon(getClass.getResource("image/cat1.jpg")),
//		new ImageIcon(getClass.getResource("image/cat2.jpg")),
//		new ImageIcon(getClass.getResource("image/cat3.jpg")),
//		new ImageIcon(getClass.getResource("image/dog_icon.jpg")),
//		new ImageIcon(getClass.getResource("image/dog1.jpg")),
//		new ImageIcon(getClass.getResource("image/dog2.jpg")),
//		new ImageIcon(getClass.getResource("image/dog3.jpg")),
//		new ImageIcon(getClass.getResource("image/mouse_icon.jpg")),
//		new ImageIcon(getClass.getResource("image/mouse1.jpg")),
//		new ImageIcon(getClass.getResource("image/mouse2.jpg")),
//		new ImageIcon(getClass.getResource("image/mouse3.jpg"))};
	
	private ImageIcon[] pictures = new ImageIcon[12];
//	pictures[0] = new ImageIcon(getClass.getResource("image/cat_icon.jpg"));
//	pictures[1] = new ImageIcon(getClass.getResource("image/cat1.jpg"));
//	pictures[2] = new ImageIcon(getClass.getResource("image/cat2.jpg"));
//	pictures[3] = new ImageIcon(getClass.getResource("image/cat3.jpg"));
//	pictures[4] = new ImageIcon(getClass.getResource("image/dog_icon.jpg"));
//	pictures[5] = new ImageIcon(getClass.getResource("image/dog1.jpg"));
//	pictures[6] = new ImageIcon(getClass.getResource("image/dog2.jpg"));
//	pictures[7] = new ImageIcon(getClass.getResource("image/dog3.jpg"));
//	pictures[8] = new ImageIcon(getClass.getResource("image/mouse_icon.jpg"));
//	pictures[9] = new ImageIcon(getClass.getResource("image/mouse1.jpg"));
//	pictures[10] = new ImageIcon(getClass.getResource("image/mouse2.jpg"));
//	pictures[11] = new ImageIcon(getClass.getResource("image/mouse3.jpg"));
	
	public FE11() {
		Action catAction = new animalAction("cat", pictures[0]);
		Action dogAction = new animalAction("dog", pictures[4]);
		Action mouseAction = new animalAction("mouse", pictures[8]);
		
		JMenuBar jMenuBar = new JMenuBar();
		JMenu menu = new JMenu("Animals Gallery");
		setJMenuBar(jMenuBar);
		jMenuBar.add(menu);
		
		menu.add(catAction);
		menu.add(dogAction);
		menu.add(mouseAction);
		
		JToolBar jToolBar = new JToolBar(JToolBar.VERTICAL);
		jToolBar.setBorder(BorderFactory.createLineBorder(Color.blue));
		jToolBar.add(catAction);
		jToolBar.add(dogAction);
		jToolBar.add(mouseAction);
		
		JButton Previous = new JButton("←");
		JButton Next = new JButton("→");
		
		ListenerClass1 listener1 = new ListenerClass1();
		Previous.addActionListener(listener1);
		ListenerClass1 listener2 = new ListenerClass1();
		Next.addActionListener(listener2);
		
		JPanel buttons = new JPanel();
		buttons.setLayout(new GridLayout(1,2,5,5));
		buttons.add(Previous);
		buttons.add(Next);
		
		setLayout(new BorderLayout(5,5));
		add(icons, BorderLayout.CENTER);
		add(buttons, BorderLayout.SOUTH);
		add(jToolBar, BorderLayout.EAST);
	}
	
	public class ListenerClass1 implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(now_pointer == 1) {
				JOptionPane.showMessageDialog(null, "It's the first one!");
			}
			else {
				now_pointer--;
			}	
		}
	}
	
	public class ListenerClass2 implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(now_pointer == 3) {
				JOptionPane.showMessageDialog(null, "It's the last one!");
			}
			else {
				now_pointer++;
			}
		}
	}
	
	public class animalAction extends AbstractAction {
		String name;
		animalAction(String name, Icon icon) {
			super(name, icon);
			putValue(Action.SHORT_DESCRIPTION, name);
			this.name = name;
		}
		public void actionPerformed(ActionEvent e) {
			if(name.equals("cat")) {
				if(now_pointer == 1)
					icons.setIcon(pictures[1]);
				else if(now_pointer == 2)
					icons.setIcon(pictures[2]);
				else {
					icons.setIcon(pictures[3]);
				}
			}
			else if(name.equals("dog")) {
				if(now_pointer == 1)
					icons.setIcon(pictures[5]);
				else if(now_pointer == 2)
					icons.setIcon(pictures[6]);
				else {
					icons.setIcon(pictures[7]);
				}
			}
			else if(name.equals("mouse")) {
				if(now_pointer == 1)
					icons.setIcon(pictures[9]);
				else if(now_pointer == 2)
					icons.setIcon(pictures[10]);
				else {
					icons.setIcon(pictures[11]);
				}
			}
		}
	}
}
