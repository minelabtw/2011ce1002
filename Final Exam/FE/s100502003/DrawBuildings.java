package fe.s100502003;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class DrawBuildings extends JPanel {
	private int xBase = 0;
	private int yBase = getHeight()/2;
	private int xMouse, yMouse;
	private boolean check = false;
	private Timer timer = new Timer(200, new TimerListener());
	
	public DrawBuildings() {
		timer.start();
		
	}
	
	public void setMousePositionX(int x_temp) {
		this.xMouse = x_temp;
	}
	
	public void setMousePositionY(int y_temp) {
		this.yMouse = y_temp;
	}
	
	public int getMousePositionX() {
		return xMouse;
	}
	
	public int getMousePositionY() {
		return yMouse;
	}
	
	public void setCheck(boolean CHECK) {
		this.check = CHECK;
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		int x1[]={xBase+10, xBase+200, xBase+200, xBase+10};
		int y1[]={yBase+50, yBase+50, yBase+200+50, yBase+200+50};
		g.setColor(Color.BLUE);
		g.drawString("A", xBase+2, yBase+200+50);
		g.drawPolygon(x1,y1,x1.length);
		
		int x2[]={xBase+60, xBase+250, xBase+250, xBase+60};
		int y2[]={yBase-50+50, yBase-50+50, yBase+100+50, yBase+100+50};
		g.setColor(Color.ORANGE);
		g.drawString("B", xBase+52, yBase+100+50);
		g.drawPolygon(x2,y2,x2.length);
		
		if(check) {
			g.setColor(Color.red);
			g.fillOval(xMouse, yMouse, 10, 10);
		}
	}
	
	private class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(xBase == getWidth()) {
				xBase = 0;
				repaint();
			}
			else {
				xBase++;
				repaint();
			}
		}
	}
}
