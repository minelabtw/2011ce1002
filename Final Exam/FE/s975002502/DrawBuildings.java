package fe.s975002502;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class DrawBuildings extends JLabel implements ActionListener {
	private int xBase = 0;
	private int yBase = getHeight()/2;
	private int ALength = 190;
	private int AWidth = 150;
	private int BLength = 190;
	private int BWidth = 200;
	
	private Timer timer = new Timer(200,this);
	
	public DrawBuildings() {
		move();
	}
	
	private void move() {
		timer.start();
		if(xBase > getWidth()) {
			xBase = 0;
		}
		else {
			xBase += 2;
		}
		repaint();
		invalidate();
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		g.drawRect(xBase + 60, yBase - 50 , AWidth, AWidth);
		g.drawRect(xBase + 10, yBase, BWidth, BWidth);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

}
