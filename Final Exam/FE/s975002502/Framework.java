package fe.s975002502;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.text.*;

public class Framework extends JFrame implements ActionListener, MouseListener, MouseMotionListener {
	private JLabel title = new JLabel("The Bomber Infrared Screen");
	private JLabel language = new JLabel("Language: ");
	private JComboBox comboLanguage = new JComboBox();
	private JLabel buildings = new DrawBuildings();
	private JLabel position = new JLabel("Position: ");
	private JLabel isPosition = new JLabel();
	private JLabel result = new JLabel("Result: ");
	private JLabel isResult = new JLabel();
	
	private Locale[] avaliableLocales = Locale.getAvailableLocales();
	
	public Framework() {
		// Create northPanel
		JPanel northPanel = new JPanel(new GridLayout(1,2,5,5));
		JPanel languePanel = new JPanel();
		languePanel.add(language);
		languePanel.add(comboLanguage);
		northPanel.add(title);
		northPanel.add(languePanel);
		
		// Create southPanel
		JPanel southPanel = new JPanel(new GridLayout(1,4,5,5));
		southPanel.add(position);
		southPanel.add(isPosition);
		southPanel.add(result);
		southPanel.add(isResult);
		
		add(northPanel, BorderLayout.NORTH);
		add(buildings, BorderLayout.CENTER);
		add(southPanel, BorderLayout.SOUTH);
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
