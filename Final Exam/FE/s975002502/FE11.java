package fe.s975002502;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FE11 extends JApplet implements ActionListener {
	// Create Image Icon
	private ImageIcon Cat_icon = new ImageIcon(getClass().getResource("image/cat_icon.jpg"));
	private ImageIcon Dog_icon = new ImageIcon(getClass().getResource("image/dog_icon.jpg"));
	private ImageIcon Mouse_icon = new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));
	
	// Create one-dimension Image array
	private ImageIcon[] CDM_img = new ImageIcon[9];
	
	private JLabel Label_icon = new JLabel();
	private JLabel Label_img = new JLabel();
	
	private JButton previous = new JButton("<-");
	private JButton next = new JButton("->");
	
	private int count = 0;
	private int gallery = 0;
	
	public FE11() {
		CDM_img[0] = new ImageIcon(getClass().getResource("image/cat1.jpg"));
		// Create image(Cat)
		for(int i=0; i<3; i++) {
			CDM_img[i] = new ImageIcon(getClass().getResource("image/cat" + (i+1) + ".jpg"));
		}
		// Create image(Dog)
		for(int i=3; i<6; i++) {
			CDM_img[i] = new ImageIcon(getClass().getResource("image/dog" + (i-2) + ".jpg"));
		}
		// Create image(Mouse)
		for(int i=6; i<9; i++) {
			CDM_img[i] = new ImageIcon(getClass().getResource("image/mouse" + (i-5) + ".jpg"));
		}
		
		// Create Actions 
		Action Cat_action = new MyAction("Cat", Cat_icon); 
		Action Dog_action = new MyAction("Dog", Dog_icon); 
		Action Mouse_action = new MyAction("Mouse", Mouse_icon); 
		
		// Create Menu
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("Animal Gallery");
		setJMenuBar(menuBar);
		menuBar.add(menu);
		
		// Add actions to menu
		menu.add(Cat_action);
		menu.add(Dog_action);
		menu.add(Mouse_action);
		
		// Create toolbar
		JToolBar toolBar = new JToolBar(JToolBar.VERTICAL);
		toolBar.setBorder(BorderFactory.createLineBorder(Color.red));
		
		// Add actions to toolbar
		toolBar.add(Cat_action);
		toolBar.add(Dog_action);
		toolBar.add(Mouse_action);
		
		// Create button Panel
		JPanel btnPanel = new JPanel();
		btnPanel.add(previous);
		btnPanel.add(next);
		
		previous.addActionListener(this);
		next.addActionListener(this);
		
		add(toolBar, BorderLayout.EAST);
		add(Label_img, BorderLayout.CENTER);
		add(btnPanel, BorderLayout.SOUTH);
	}
	
	//Implement AbstractionAction
	private class MyAction extends AbstractAction{
		String name;
		MyAction(String name, Icon icon) {
			super(name, icon);
			putValue(Action.SHORT_DESCRIPTION, "Select the " + name);
			this.name = name;
		}
		
		public void actionPerformed(ActionEvent e) {
			if(name.equals("Cat")) {
				gallery = 0; 
				count = 0;
				Label_img.setIcon(CDM_img[0]);
			}
			else if(name.equals("Dog")) {
				gallery = 1;
				count = 0;
				Label_img.setIcon(CDM_img[3]);
			}
			else if(name.equals("Mouse")) {
				gallery = 2;
				count = 0;
				Label_img.setIcon(CDM_img[6]);
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		JButton btn = (JButton)arg0.getSource();
		
		if (btn == previous) {
			if(count == 0) {
				JOptionPane.showMessageDialog(null, "This is the first image!!");
			}
			else if(count>0){
				count--;
				Label_img.setIcon(CDM_img[(9%3)+(3*gallery)+count]);
				repaint();
				invalidate();
			}
		}
		else if (btn == next) {
			if(count == 2) {
				JOptionPane.showMessageDialog(null, "This is the last image!!");
			}
			else if(count<2) {
				count++;
				Label_img.setIcon(CDM_img[(9%3)+(3*gallery)+count]);
				repaint();
				invalidate();
			}
		}
	}


}
