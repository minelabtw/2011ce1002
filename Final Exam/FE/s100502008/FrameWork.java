package fe.s100502008;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Calendar;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.event.MouseInputListener;

public class FrameWork extends JFrame implements MouseListener{
	DrawBuildings b = new DrawBuildings();
	ResourceBundle res=ResourceBundle.getBundle("Confidential_en");
	JLabel JLabel1=new JLabel(res.getString("Title"));
	JLabel JLabel2=new JLabel(res.getString("Language"));
	String[] str ={"英文","中文","法文","日文"};
	JComboBox JLocale =new JComboBox(str);
	//Locale locale= Locale.getDefault();
	//Locale[] locales=Locale.getAvailableLocales();
	JLabel JLabel4=new JLabel(res.getString("Position"));
	JLabel JLabel5=new JLabel("None");
	JLabel JLabel6=new JLabel(res.getString("Result"));
	JLabel JLabel7=new JLabel("Nothing");
	int chos=0;;
	ResourceBundle newres;
	public FrameWork()
	{
		/*for(int i =0;i<locales.length;i++)
		{
			JLocale.addItem(locales[i].getDisplayName());
		}*/
		JPanel topp1 = new JPanel(new GridLayout(1,2));
		
		topp1.add(JLabel2);
		topp1.add(JLocale);
		JPanel topp = new JPanel(new GridLayout(1,2));
		topp.add(JLabel1);
		topp.add(topp1);
		JPanel botp1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		botp1.add(JLabel4);
		botp1.add(JLabel5);
		JPanel botp2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		botp2.add(JLabel6);
		botp2.add(JLabel7);
		JPanel botp = new JPanel(new GridLayout(1,2));
		botp.add(botp1);
		botp.add(botp2);
		botp.setBorder(new LineBorder(Color.black,2));
		JPanel p = new JPanel(new BorderLayout());
		p.add(topp,BorderLayout.NORTH);
		p.add(b,BorderLayout.CENTER);
		p.add(botp,BorderLayout.SOUTH);
		add(p);
		b.timer.start();
		addMouseListener(this);
		JLocale.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				chos=JLocale.getSelectedIndex();
				update();
			}
		});
	}
	public void update()
	{
		if(chos==0)
		{
			newres=ResourceBundle.getBundle("Confidential_en");
		}
		else if(chos==1)
		{
			newres=ResourceBundle.getBundle("Confidential_ch");
		}
		else if(chos==2)
		{
			newres=ResourceBundle.getBundle("Confidential_ch");
		}
		else if(chos==3)
		{
			newres=ResourceBundle.getBundle("Confidential_ch");
		}
		 JLabel1.setText(newres.getString("Title"));
		 JLabel2.setText(newres.getString("Language"));
		 JLabel4.setText(newres.getString("Position"));
		 JLabel6.setText(newres.getString("Result"));
	}
	public void mouseClicked(MouseEvent e) {
	}
	public void mouseEntered(MouseEvent e) {
	}
	public void mouseExited(MouseEvent e) {
	}
	public void mouseReleased(MouseEvent e) {
	}
	public void mousePressed(MouseEvent e) {

		b.setMousePositionX(e.getX()-14);
		b.setMousePositionY(e.getY()-63);
		b.isClicked();
		int mx=e.getX()-14+5,my=e.getY()-63+5;
		JLabel5.setText(mx+","+my);
		JLabel7.setText(b.determine());
	}
}
