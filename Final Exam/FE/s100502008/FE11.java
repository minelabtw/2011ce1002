package fe.s100502008;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class FE11 extends JApplet implements ActionListener{
	private ImageIcon caticon=new ImageIcon(getClass().getResource("image/cat_icon.jpg"));
	private ImageIcon dogicon=new ImageIcon(getClass().getResource("image/dog_icon.jpg"));
	private ImageIcon mouseicon=new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));
	private ImageIcon[] nowimg=new ImageIcon[3];
	private ImageIcon[] catimg=new ImageIcon[3];
	private ImageIcon[] dogimg=new ImageIcon[3];
	private ImageIcon[] mouseimg=new ImageIcon[3];
	private JMenuBar menubar = new JMenuBar();
	private JMenu menu=new JMenu("Animal");
	private JToolBar toolbar=new JToolBar(JToolBar.VERTICAL);
	private JLabel img=new JLabel();
	private int count =0;
	JButton right=new JButton("��");
	JButton left=new JButton("��");
	public FE11()
	{
		catimg[0]=new ImageIcon(getClass().getResource("image/cat1.jpg"));
		catimg[1]=new ImageIcon(getClass().getResource("image/cat2.jpg"));
		catimg[2]=new ImageIcon(getClass().getResource("image/cat3.jpg"));
		dogimg[0]=new ImageIcon(getClass().getResource("image/dog1.jpg"));
		dogimg[1]=new ImageIcon(getClass().getResource("image/dog2.jpg"));
		dogimg[2]=new ImageIcon(getClass().getResource("image/dog3.jpg"));
		mouseimg[0]=new ImageIcon(getClass().getResource("image/mouse1.jpg"));
		mouseimg[1]=new ImageIcon(getClass().getResource("image/mouse2.jpg"));
		mouseimg[2]=new ImageIcon(getClass().getResource("image/mouse3.jpg"));
		
		Action cataction=new MyAction("cat",caticon);
		Action dogaction=new MyAction("dog",dogicon);
		Action mouseaction=new MyAction("mouse",mouseicon);
		
		setJMenuBar(menubar);
		menubar.add(menu);
		menu.add(cataction);
		menu.add(dogaction);
		menu.add(mouseaction);
		
		toolbar.add(cataction);
		toolbar.add(dogaction);
		toolbar.add(mouseaction);
		
		JPanel btp=new JPanel(new FlowLayout(FlowLayout.CENTER));
		btp.add(left);
		btp.add(right);
		JPanel p = new JPanel(new BorderLayout());
		p.add(toolbar,BorderLayout.EAST);
		p.add(btp,BorderLayout.SOUTH);
		p.add(img,BorderLayout.CENTER);
		add(p);
		right.addActionListener(this);
		left.addActionListener(this);
	}
	class MyAction extends AbstractAction{
			String name;
			MyAction(String aniname,Icon icon){
				super(aniname,icon);
				putValue(Action.SHORT_DESCRIPTION,aniname);
				name=aniname;
			}
			public void actionPerformed(ActionEvent e) {
				if(name.equals("cat"))
				{
					nowimg=catimg;
					img.setIcon(catimg[0]);
					count =0;
				}
				else if(name.equals("dog"))
				{
					nowimg=dogimg;
					img.setIcon(dogimg[0]);
					count =0;
				}
				else if(name.equals("mouse"))
				{
					nowimg=mouseimg;
					img.setIcon(mouseimg[0]);
					count =0;
				}
			}
		}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==right)
		{
			if(count==2)
			{
				JOptionPane.showMessageDialog(null, "This is the last one!");
			}
			else
			{
				count++;
				img.setIcon(nowimg[count]);
			}
		}
		else if(e.getSource()==left)
		{
			if(count==0)
			{
				JOptionPane.showMessageDialog(null, "This is the first one!");
			}
			else
			{
				count--;
				img.setIcon(nowimg[count]);
			}
		}
	}
	
}
