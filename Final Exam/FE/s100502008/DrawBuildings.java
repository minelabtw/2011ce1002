package fe.s100502008;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class DrawBuildings extends JPanel {
	Timer timer = new Timer(200,new TimerListener());
	int x = 0;
	int y;
	int mouseX;
	int mouseY;
	boolean click=false;
	
	public void setMousePositionX(int temp)
	{
		 mouseX=temp;
	}
	public void setMousePositionY(int temp)
	{
		 mouseY=temp;
	}
	public void isClicked()
	{
		click=true;
	}
	public String determine()
	{
		if(mouseX+5<=(x+200)&&mouseX+5>=(x+60)&&mouseY+5>=(y)&&mouseY+5<=(y+100))
		{
			return "Hit Both buildings A and B";
		}
		else if(mouseX+5<=(x+250)&&mouseX+5>=(x+60)&&mouseY+5>=(y-50)&&mouseY+5<=(y+100))
		{
			return "Hit building B";
		}
		else if(mouseX+5<=(x+200)&&mouseX+5>=(x+10)&&mouseY+5>=(y)&&mouseY+5<=(y+200))
		{
			return "Hit building A";
		}
		else 
		{
			return "Miss";
		}
	}
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		y = getHeight()/2;
		g.drawRect(x+60, y-50, 190, 150);
		g.drawRect(x+10, y, 190, 200);
		g.drawString("A",x , y+200);
		g.drawString("B",x+50 , y+100);
		if(click)
		{
			g.setColor(Color.RED);
			g.fillOval(mouseX,mouseY,10,10);
		}
		x+=10;
		if(x+10>=getWidth())
		{
			x=0;
		}
	}
	class TimerListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e) {
			repaint();
		}
	}
}
