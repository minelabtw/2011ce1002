package fe.s100502505;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class FE11 extends JApplet implements ActionListener{

	JButton jbt1 = new JButton(new ImageIcon((getClass().getResource("cat_icon.jpg"))));//按鈕上有圖片
	JButton jbt2 = new JButton(new ImageIcon((getClass().getResource("dog_icon.jpg"))));
	JButton jbt3 = new JButton(new ImageIcon((getClass().getResource("mouse_icon.jpg"))));
	JButton previous = new JButton("Previous");//按鈕
	JButton next = new JButton("Next");
	
	ImageIcon pic1 = new ImageIcon(getClass().getResource("cat1.jpg"));//圖片
	ImageIcon pic2 = new ImageIcon(getClass().getResource("cat2.jpg"));
	ImageIcon pic3 = new ImageIcon(getClass().getResource("cat3.jpg"));
	ImageIcon pic4 = new ImageIcon(getClass().getResource("dog1.jpg"));
	ImageIcon pic5 = new ImageIcon(getClass().getResource("dog2.jpg"));
	ImageIcon pic6 = new ImageIcon(getClass().getResource("dog3.jpg"));
	ImageIcon pic7 = new ImageIcon(getClass().getResource("mouse1.jpg"));
	ImageIcon pic8 = new ImageIcon(getClass().getResource("mouse2.jpg"));
	ImageIcon pic9 = new ImageIcon(getClass().getResource("mouse3.jpg"));
	
	JToolBar jToolBar = new JToolBar("box");
	JMenuBar menuBar = new JMenuBar();
	JMenu menu = new JMenu("Animals");
	JMenuItem cat = new JMenuItem("cat");
	JMenuItem dog = new JMenuItem("dog");
	JMenuItem mouse = new JMenuItem("mouse");
	
	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();
	JPanel p3 = new JPanel();
	JLabel label = new JLabel();
	
	public FE11()
	{
		jToolBar.add(jbt1);//加進去
		jToolBar.add(jbt2);
		jToolBar.add(jbt3);
		menu.add(cat);
		menu.add(dog);
		menu.add(mouse);
		menuBar.add(menu);
		setJMenuBar(menuBar);
		
		p1.setLayout(new GridLayout(3,1));//排版
		p2.setLayout(new GridLayout(1,5));
		p2.add(jToolBar);
		p3.add(previous);
		p3.add(next);
		
		
		add(p1, BorderLayout.NORTH);//排版
		add(p2, BorderLayout.EAST);
		add(p3, BorderLayout.SOUTH);
		add(label, BorderLayout.CENTER);
		
		jbt1.addActionListener(this);
		jbt2.addActionListener(this);
		jbt3.addActionListener(this);
		cat.addActionListener(this);
		dog.addActionListener(this);
		mouse.addActionListener(this);
		previous.addActionListener(this);
		next.addActionListener(this);
		
	}
	
	
	
	public void actionPerformed(ActionEvent arg0) {
		
		if(arg0.getSource() == jbt1)//點了按鈕會發生事情
		{
			label.setIcon(pic1);
		}else if(arg0.getSource() == jbt2)
		{
			label.setIcon(pic4);
		}else if(arg0.getSource() == jbt3)
		{
			label.setIcon(pic7);
		}else if(arg0.getSource() == cat)
		{
			label.setIcon(pic1);
		}else if(arg0.getSource() == dog)
		{
			label.setIcon(pic4);
		}else if(arg0.getSource() == mouse)
		{
			label.setIcon(pic7);
		}else if(arg0.getSource() == previous)
		{
			if(label.getIcon() == pic1 || label.getIcon() == pic4 || label.getIcon() == pic7)
			{
				JOptionPane.showMessageDialog(null,"沒有照片了");
			}else if(label.getIcon() == pic2)
			{
				label.setIcon(pic1);
			}else if(label.getIcon() == pic3)
			{
				label.setIcon(pic2);
			}else if(label.getIcon() == pic5)
			{
				label.setIcon(pic4);
			}else if(label.getIcon() == pic6)
			{
				label.setIcon(pic5);
			}else if(label.getIcon() == pic8)
			{
				label.setIcon(pic7);
			}else if(label.getIcon() == pic9)
			{
				label.setIcon(pic8);
			}
		}else if(arg0.getSource() == next)
		{
			if(label.getIcon() == pic3 || label.getIcon() == pic6 || label.getIcon() == pic9)
			{
				JOptionPane.showMessageDialog(null,"沒有照片了");
			}else if(label.getIcon() == pic1)
			{
				label.setIcon(pic2);
			}else if(label.getIcon() == pic2)
			{
				label.setIcon(pic3);
			}else if(label.getIcon() == pic4)
			{
				label.setIcon(pic5);
			}else if(label.getIcon() == pic5)
			{
				label.setIcon(pic6);
			}else if(label.getIcon() == pic7)
			{
				label.setIcon(pic8);
			}else if(label.getIcon() == pic8)
			{
				label.setIcon(pic9);
			}
		}
		
	}
	
}//完成
