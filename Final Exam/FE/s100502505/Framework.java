package fe.s100502505;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.Timer;

import javax.swing.*;

public class Framework extends JFrame implements ActionListener{

	DrawBuildings db = new DrawBuildings();
	
	Locale localelan = Locale.ENGLISH;//語言
	Locale[] localelans = { Locale.ENGLISH, Locale.JAPAN,Locale.CHINESE, Locale.FRANCE };
	ResourceBundle rb = ResourceBundle.getBundle("Confidential",localelan);//Resource
	
	JLabel l1 = new JLabel(rb.getString("Title"));//Label
	JLabel l2 = new JLabel(rb.getString("Language"));
	JLabel l3 = new JLabel("");
	JLabel l4 = new JLabel(rb.getString("Position"));
	JLabel l5 = new JLabel("");
	JLabel l6 = new JLabel(rb.getString("Result"));
	JLabel l7 = new JLabel("");
	
	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();
	
	JComboBox lan = new JComboBox();
	
	public Framework()
	{
		initializeComboBox();//讓ComboBox有選項
		
		lan.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0)
			{
				localelan = localelans[lan.getSelectedIndex()];
				update();
			}
		});
		
		p1.add(l1);//排版
		p1.add(l2);
		p1.add(lan);
		p2.add(l4);
		p2.add(l5);
		p2.add(l6);
		p2.add(l7);
		
		add(p1, BorderLayout.NORTH);
		add(l3, BorderLayout.CENTER);
		add(p2, BorderLayout.SOUTH);
	}
	
	public void initializeComboBox()
	{
		for(int i=0;i<localelans.length;i++)
		{
			lan.addItem(localelans[i].getDisplayName());
		}
	}
	
	public void update()
	{
		rb = ResourceBundle.getBundle("Confidential",localelan);
		l1.setText(rb.getString("Title"));
		l2.setText(rb.getString("Language"));
		l4.setText(rb.getString("Position"));
		l6.setText(rb.getString("Result"));
	}
	
	public Timer timer = new Timer();
	
	
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
