package fe.s100502030;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Framework extends JFrame {
	private Locale locale = Locale.US;
	private Locale[] locales = { Locale.ENGLISH, Locale.FRENCH, Locale.JAPAN,
			Locale.CHINA };

	private JComboBox jcboLocale = new JComboBox();

	private ResourceBundle res = ResourceBundle.getBundle("Confidential",
			locale);

	private JLabel jlabel1 = new JLabel(res.getString("Title"));
	private JLabel jlabel2 = new JLabel(res.getString("Language"));
	private JLabel jlabel3 = new DrawBuildings();
	private JLabel jlabel4 = new JLabel(res.getString("Position"));
	private JLabel jlabel5 = new JLabel();
	private JLabel jlabel6 = new JLabel(res.getString("Result"));
	private JLabel jlabel7 = new JLabel();

	public Framework() {
		for (int i = 0; i < 4; i++) {

			jcboLocale.addItem(locales[i].getDisplayName());
		}

		JPanel p1 = new JPanel(new GridLayout(1, 2));
		JPanel p1_2 = new JPanel();
		p1_2.add(jlabel2);
		p1_2.add(jcboLocale);

		p1.add(jlabel1);
		p1.add(p1_2);

		JPanel p2 = new JPanel(new GridLayout(1, 2));
		JPanel p2_1 = new JPanel(new GridLayout(1, 2));
		JPanel p2_2 = new JPanel(new GridLayout(1, 2));
		p2_1.add(jlabel4);
		p2_1.add(jlabel5);
		p2_2.add(jlabel6);
		p2_2.add(jlabel6);
		p2_2.add(jlabel7);

		p2.add(p2_1);
		p2.add(p2_2);

		setLayout(new BorderLayout());
		add(p1, BorderLayout.NORTH);
		add(jlabel3, BorderLayout.CENTER);
		add(p2, BorderLayout.SOUTH);
		
		jcboLocale.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				locale = locales[jcboLocale.getSelectedIndex()];
				updateString();
			}
		});

	}

	private void updateString() {
		res = ResourceBundle.getBundle("Confidential", locale);
		jlabel1.setText(res.getString("Title"));
		jlabel2.setText(res.getString("Language"));
		jlabel4.setText(res.getString("Position"));
		jlabel6.setText(res.getString("Result"));
	}
}
