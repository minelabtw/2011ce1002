package fe.s100502030;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.Timer;

public class DrawBuildings extends JLabel {
	private Timer t = new Timer(200, new TimerListener());
	private int dx = 1;
	private boolean start = true;
	private int xBase;
	private int yBase;

	public DrawBuildings() {
		t.start();
	}

	protected void paintComponent(Graphics g) {

		super.paintComponent(g);

		//if (start) {
			xBase = 0;
			yBase = getHeight() / 2;
			//start = false;
		//}

		g.setColor(Color.BLUE);
		g.drawRect(xBase + 10, yBase, 200, 200);
		g.setColor(Color.GREEN);
		g.drawRect(xBase + 60, yBase - 50, 190, 190);

		xBase += dx;

	}

	public class TimerListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			repaint();

		}

	}
}
