package fe.s100502034;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JToolBar;

public class FE11 extends JApplet{
	private JMenuBar mB = new JMenuBar();
	private JMenu anmo = new JMenu("Animals");
	private Action catA = new action("cat", new ImageIcon(getClass().getResource("image/cat_icon.jpg")));//三張icon圖
	private Action dogA = new action("dog" , new ImageIcon(getClass().getResource("image/dog_icon.jpg")));
	private Action mouseA = new action("mouse", new ImageIcon(getClass().getResource("image/mouse_icon.jpg")));
	private JLabel pictureL = new JLabel();//用來放圖片的Label
	private ImageIcon[] anIcon = new ImageIcon[9];//一維陣列九張圖
	private JToolBar toolB= new JToolBar(JToolBar.VERTICAL);
	private JButton previous = new JButton("Previous");
	private JButton next = new JButton("Next");
	private JLabel l = new JLabel();//提示使用者已經沒有下一張或者上一張了
	private int no = 0;
	public FE11(){
		JPanel p = new JPanel();
		stonIcon();//初始化加入所有圖片
		anmo.add(catA);//menu
		anmo.add(dogA);
		anmo.add(mouseA);
		mB.add(anmo);//menuBar
		setJMenuBar(mB);

		toolB.add(catA);//toolBar
		toolB.add(dogA);
		toolB.add(mouseA);
		Listener listener = new Listener();//button listener
		previous.addActionListener(listener);
		next.addActionListener(listener);
		p.add(previous);
		p.add(next);
		p.add(l);
		
		add(toolB, BorderLayout.EAST);
		add(pictureL,BorderLayout.CENTER);
		add(p,BorderLayout.SOUTH);
	}
	private class Listener implements ActionListener{
		public void actionPerformed(ActionEvent e) {	
			if(e.getSource() == previous){
				if(no == 0 || no == 3 ||no==6){//判斷是否有上一張
				}
				else{
					no-=1;
					pictureL.setIcon(anIcon[no]);
				}
			}
			else if (e.getSource() == next){//判斷是否有下一張
				if(no==2||no==5||no==8){
				}
				else{
					no+=1;
					pictureL.setIcon(anIcon[no]);
				}
			}
			check();
		}
	}

	private class action extends AbstractAction{//三個action的method
		String n = "";
		public action(String name, Icon m){
			super(name,m);
			n = name;
		}
		public void actionPerformed(ActionEvent e) {
			
			if( n == "cat"){
				no = 0;
				pictureL.setIcon(anIcon[0]);
			}
			else if (n == "dog"){
				no = 3;
				pictureL.setIcon(anIcon[3]);
			}
			else if (n == "mouse"){
				no = 6;
				pictureL.setIcon(anIcon[6]);
			}
			check();
			repaint();
		}
	}
	public void main(String arg[]){
		FE11 fe = new FE11();
	}
	public void check(){//檢查是否已到臨界
		if(no == 0 || no == 3 ||no==6){
			l.setText("no previous");
		}
		else if(no==2||no==5||no==8){
			l.setText("no next");
		}
		else{
			l.setText("");
		}
	}
	public void stonIcon(){//圖片帶入
		anIcon[0] = new ImageIcon(getClass().getResource("image/cat1.jpg"));
		anIcon[1] = new ImageIcon(getClass().getResource("image/cat2.jpg"));
		anIcon[2] = new ImageIcon(getClass().getResource("image/cat3.jpg"));
		anIcon[3] = new ImageIcon(getClass().getResource("image/dog1.jpg"));
		anIcon[4] = new ImageIcon(getClass().getResource("image/dog2.jpg"));
		anIcon[5] = new ImageIcon(getClass().getResource("image/dog3.jpg"));
		anIcon[6] = new ImageIcon(getClass().getResource("image/mouse1.jpg"));
		anIcon[7] = new ImageIcon(getClass().getResource("image/mouse2.jpg"));
		anIcon[8] = new ImageIcon(getClass().getResource("image/mouse3.jpg"));
	}
}
