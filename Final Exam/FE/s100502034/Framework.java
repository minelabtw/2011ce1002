package fe.s100502034;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
public class Framework extends JFrame implements ActionListener,MouseListener , MouseMotionListener{
	private JLabel[] label = new JLabel[7];
	private Locale[] locales = {Locale.ENGLISH,Locale.FRENCH,Locale.JAPANESE,Locale.CHINESE};
	private JComboBox combox = new JComboBox(locales);
	private Locale locale = Locale.US;
	private ResourceBundle res = ResourceBundle.getBundle("Confidential",locale);
	private DrawBuildings d = new DrawBuildings(this.getHeight());
	private int x;
	private int y;
	public Framework(){
		for(int i =0;i<label.length;i++){
			label[i] = new JLabel(i+"");
		}
		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();
		p1.setLayout(new GridLayout(1,3));
		p1.add(label[0]);
		p1.add(label[1]);
		combox.addActionListener(this);
		p1.add(combox);
		add(p1, BorderLayout.NORTH);
		p2.setLayout(new GridLayout(1,4));
		label[2].addMouseListener(this);
		add(label[2],BorderLayout.CENTER);
		add(d);
		p2.add(label[3]);
		p2.add(label[4]);
		p2.add(label[5]);
		p2.add(label[6]);
		add(p2, BorderLayout.SOUTH);
	}
	public void updateString(){
		label[0].setText(res.getString("Title"));
		label[1].setText(res.getString("Language"));
		label[3].setText(res.getString("Position"));
		label[5].setText(res.getString("Result"));
	}
	public void actionPerformed(ActionEvent e) {	
		if(e.getSource() == combox){
			locale = locales[combox.getSelectedIndex()];
			res = ResourceBundle.getBundle("Confidential",locale);
			updateString();
		}
	}
	public void mouseDragged(MouseEvent arg0) {
		
	}
	public void mouseMoved(MouseEvent arg0) {
	}
	public void mouseClicked(MouseEvent arg0){ 
	
	}
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		d.setMousePositionX(e.getX());
		d.setMousePositionY(e.getY());
		System.out.println("1");
	}
	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
