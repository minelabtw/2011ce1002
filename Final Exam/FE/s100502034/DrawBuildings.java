package fe.s100502034;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class DrawBuildings extends JPanel implements ActionListener {
	private Timer timer;
	private int xBase = 0;
	private int yBase;
	private int[] x1 = new int[4];
	private int[] x2 = new int[4];
	private int[] y1 = new int[4];
	private int[] y2 = new int[4];
	private int mPX = -10;
	private int mPY = -10;
	public DrawBuildings(int a){
		timer = new Timer(200, new TimerListener());
		timer.start();
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		yBase = getHeight()/2;
		y1[0] = yBase -50;
		y1[1] = yBase -50;
		y1[2] = yBase +100;
		y1[3] = yBase +100;
		y2[0] = yBase;
		y2[1] = yBase;
		y2[2] = yBase + 200;
		y2[3] = yBase +200;
		x1[0] = xBase +60;
		x1[1] = xBase +250;
		x1[2] = xBase +250;
		x1[3] = xBase +60;
		x2[0] = xBase +10;
		x2[1] = xBase +200;
		x2[2] = xBase +200;
		x2[3] = xBase +10;
		g.setColor(Color.red);
		g.drawPolygon(x1, y1, 4);
		g.drawString("B", x1[3]-20, y1[3]);
		g.setColor(Color.blue);
		g.drawPolygon(x2, y2, 4);
		g.drawString("A", x2[3]-20, y2[3]);
		xBase += 10;
		if(xBase >= getWidth()){
			xBase = -250;
		}
		g.setColor(Color.red);
		g.fillOval(mPX, mPY, 10, 10);
	}
	class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			repaint();
		}
	}
	public void setMousePositionX(int x_temp){
		mPX =x_temp; 
	}
	public void setMousePositionY(int y_temp){
		mPY =y_temp; 
	}
	/*public int getX(){
		return xBase;
	}
	public int getY(){
		return yBase;
	}*/
	public void actionPerformed(ActionEvent arg0) {
		
	}

}
