package fe.s100502517;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

import javax.swing.*;

public class DrawBuildings extends JPanel{
	int xBase = 0;
	int Bomb_X = 0;
	int Bomb_Y = 0;
	boolean click = false;
	
	public DrawBuildings(){
		Timer timer = new Timer(200, new TimerListener());
		timer.start();
	}
	
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		int yBase = getHeight()/2;
		int aX[] = {xBase+10, xBase+200, xBase+200, xBase+10};
		int aY[] = {yBase, yBase, yBase+200, yBase+200};
		int bX[] = {xBase+50, xBase+250, xBase+250, xBase+50};
		int bY[] = {yBase-50, yBase-50, yBase+100, yBase+100};
		g.drawPolygon(aX, aY, aX.length);
		g.drawPolygon(bX, bY, bX.length);	
		if(click == true){
			g.fillOval(Bomb_X,Bomb_Y,5,5);
			g.setColor(Color.RED);
		}
	}	
	public void setClick(){
		click = true;
	}
	
	public void setMousePositionX(int x_temp){
		Bomb_X = x_temp;
	}
	
	public void setMousePositionY(int y_temp){
		Bomb_Y = y_temp;
	}	
	
	class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			xBase++;
			repaint();		
			System.out.println(Bomb_X+" "+Bomb_Y);
		}
	}
		
	

}
