package fe.s100502517;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class FE11 extends JApplet{
	
	private JButton cat = new JButton(new ImageIcon("../image/cat_icon.jpg"));
	private JButton dog = new JButton(new ImageIcon("../image/dog_icon.jpg"));
	private JButton mouse = new JButton(new ImageIcon("../image/mouse_icon.jpg"));
	
	//read the picture into an array
	private ImageIcon[] a ={ 
			new ImageIcon("../image/cat1.jpg"),
			new ImageIcon("../image/cat2.jpg"),
			new ImageIcon("../image/cat3.jpg"),
			new ImageIcon("../image/dog1.jpg"),
			new ImageIcon("../image/dog2.jpg"),
			new ImageIcon("../image/dog3.jpg"),
			new ImageIcon("../image/mouse1.jpg"),
			new ImageIcon("../image/mouse2.jpg"),
			new ImageIcon("../image/mouse3.jpg")};
	private ImageIcon picture = null;
	private JLabel pic = new JLabel(picture);
	
	private JButton Previous = new JButton(" <-- ");
	private JButton Next = new JButton(" --> ");
	
	private JMenuItem cats, dogs, mouses;
	private int order = 0;
	
	public FE11(){
		
		//menu
		JMenuBar jmb = new JMenuBar();
		setJMenuBar(jmb);
		
		JMenu menu = new JMenu("Animal");
		menu.add(cats = new JMenuItem("Cats"));
		menu.add(dogs = new JMenuItem("Dogs"));
		menu.add(mouses = new JMenuItem("Mouses"));
		jmb.add(menu);
		
		JPanel p0 = new JPanel();
		p0.add(jmb);
		
		//picture
		JPanel p1 = new JPanel();
		p1.setLayout(new BorderLayout());
		p1.add(pic, BorderLayout.CENTER);
		
		//button
		JPanel p2 = new JPanel();
		setLayout(new BorderLayout());
		p2.add(Previous);
		p2.add(Next);
		
		//toolbar
		JToolBar jtb = new JToolBar("Animals");
		jtb.setFloatable(true);
		jtb.add(cat);
		jtb.add(dog);
		jtb.add(mouse);
		
		setLayout(new BorderLayout());
		add(p0, BorderLayout.NORTH);
		add(p1, BorderLayout.CENTER);
		add(jtb, BorderLayout.EAST);
		add(p2, BorderLayout.SOUTH);
		
		Previous.addActionListener(new ButtonListener());
		Next.addActionListener(new ButtonListener());
		cat.addActionListener(new ButtonListener());
		dog.addActionListener(new ButtonListener());
		mouse.addActionListener(new ButtonListener());
		
		
		repaint();
		
	}
	
	class ButtonListener implements ActionListener{		
		public void actionPerformed(ActionEvent e) {
			
			//click it will jump to the first picture of cats
			if(e.getSource() == cats){
				order = 0;
				picture = a[order];
				
			}
			
			if(e.getSource() == cat){
				order = 0;
				picture = a[order];
				
			}
			
			//click it will jump to the first picture of dogs
			if(e.getSource() == dogs){
				order = 3;
				picture = a[order];
			}
			
			if(e.getSource() == dog){
				order = 3;
				picture = a[order];
			}
			
			//click it will jump to the first picture of mouses
			if(e.getSource() == mouses){
				order = 6;
				picture = a[order];
			}
			
			if(e.getSource() == mouse){
				order = 6;
				picture = a[order];
			}
			
			//jump to the previous picture
			if(e.getSource() == Previous){
				if(order == 0){
					order = 0;
				}
				else{
					order--;					
				}
				picture = a[order];
			}
			
			//jump to the next picture
			if(e.getSource() == Next){
				if(order == 8){
					order = 8;
				}
				else{
					order++;					
				}
				picture = a[order];
			}
			pic.setIcon(picture);
			//System.out.println(order);
			
			
		}
	}

}
