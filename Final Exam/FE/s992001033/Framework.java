package fe.s992001033;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

public class Framework extends JFrame implements ActionListener,MouseListener,MouseMotionListener
{
	int x,y;
	ResourceBundle res = ResourceBundle.getBundle("Confidential");
	JPanel top = new JPanel();
	JPanel buttom = new JPanel();
	JLabel Title = new JLabel(res.getString("Title"));
	JLabel language = new JLabel(res.getString("Language"));
	JLabel position = new JLabel(res.getString("Position"));
	JLabel displayPosition = new JLabel();
	JLabel result = new JLabel(res.getString("Result"));
	JLabel showresult = new JLabel();
	JComboBox locale = new JComboBox();
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	Framework()
	{
		top.add(Title);
		top.add(language);
		top.add(locale);
		buttom.add(position);
		buttom.add(displayPosition);
		buttom.add(result);
		buttom.add(showresult);
		setLayout(new BorderLayout());
		add(top,BorderLayout.PAGE_START);
		add(buttom,BorderLayout.PAGE_END);
		addMouseListener(this);
	}
	public void setJComboBox()
	{
		locale.addItem(Locale.CHINESE.getDisplayName());
	}
	
	
	
	
	
	
	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mousePressed(MouseEvent e)
	{
		x=e.getX();
		y=e.getY();
		displayPosition.setText(x+","+y);
	}
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


}
