package fe.s92001033;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class FE11 extends JApplet implements ActionListener
{
	JPanel all = new JPanel();
	JPanel control = new JPanel();//panel of button at the bottom
	
	ImageIcon[] photo = new ImageIcon[12];//to store image
	JToolBar toolbar = new JToolBar();
	JMenu menu = new JMenu();
	JMenuBar jmb = new JMenuBar();
	JLabel ImageViewer = new JLabel();//image presentation
	JButton previous = new JButton("��");
	JButton next = new JButton("��");
	
	JButton cat = new JButton();//toolbar button
	JButton dog = new JButton();
	JButton mouse = new JButton();
	
	int index =0;//change gallery
	int time =0;//change page
	public FE11()
	{
		photo[0]=new ImageIcon(getClass().getResource("image/cat_icon.jpg"));//set image
		photo[1]=new ImageIcon(getClass().getResource("image/cat1.jpg"));
		photo[2]=new ImageIcon(getClass().getResource("image/cat2.jpg"));
		photo[3]=new ImageIcon(getClass().getResource("image/cat3.jpg"));
		photo[4]=new ImageIcon(getClass().getResource("image/dog_icon.jpg"));
		photo[5]=new ImageIcon(getClass().getResource("image/dog1.jpg"));
		photo[6]=new ImageIcon(getClass().getResource("image/dog2.jpg"));
		photo[7]=new ImageIcon(getClass().getResource("image/dog3.jpg"));
		photo[8]=new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));
		photo[9]=new ImageIcon(getClass().getResource("image/mouse1.jpg"));
		photo[10]=new ImageIcon(getClass().getResource("image/mouse2.jpg"));
		photo[11]=new ImageIcon(getClass().getResource("image/mouse3.jpg"));
		
		cat.setIcon(photo[0]);//toolbar
		dog.setIcon(photo[4]);
		mouse.setIcon(photo[8]);
		toolbar.setLayout(new GridLayout(3,1));
		toolbar.add(cat);
		toolbar.add(dog);
		toolbar.add(mouse);
		
		cat.addActionListener(this);//actionlistener
		dog.addActionListener(this);
		mouse.addActionListener(this);
		previous.addActionListener(this);
		next.addActionListener(this);
		
		control.setLayout(new GridLayout(1,2));
		control.add(previous);
		control.add(next);
		all.setLayout(new BorderLayout());
		all.add(ImageViewer,BorderLayout.CENTER);
		all.add(control,BorderLayout.SOUTH);
		all.add(toolbar,BorderLayout.LINE_END);
		setLayout(new BorderLayout());
		add(all,BorderLayout.CENTER);
		setJMenuBar(jmb);
	}
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()==cat)//change gallery
		{
			ImageViewer.setIcon(photo[1]);
			index=1;
			time=0;
		}
		else if(e.getSource()==dog)
		{
			ImageViewer.setIcon(photo[5]);
			index=5;
			time=0;
		}
		else if(e.getSource()==mouse)
		{
			ImageViewer.setIcon(photo[9]);
			index=9;
			time=0;
		}
		if(e.getSource()==next&&time<2)
		{
			time++;
			ImageViewer.setIcon(photo[index+time]);//change page
		}
		else if(e.getSource()==next)
			System.out.println("This is last page");//forgot the way using GUI
		if(e.getSource()==previous&&time>0)
		{
			time--;
			ImageViewer.setIcon(photo[index+time]);
		}
		else if(e.getSource()==previous)
			System.out.println("This is first page");		
	}
}
