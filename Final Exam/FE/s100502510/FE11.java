package fe.s100502510;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class FE11 extends JApplet implements ActionListener {
	ImageIcon caticon = new ImageIcon(getClass().getResource(
			"image/cat_icon.jpg"));
	ImageIcon dogicon = new ImageIcon(getClass().getResource(
			"image/dog_icon.jpg"));
	ImageIcon mouseicon = new ImageIcon(getClass().getResource(
			"image/mouse_icon.jpg"));
	ImageIcon cats[] = new ImageIcon[3];
	ImageIcon dogs[] = new ImageIcon[3];
	ImageIcon mouses[] = new ImageIcon[3];
	JMenuBar menubar = new JMenuBar();
	JMenu menu = new JMenu("Menu");
	JLabel display = new JLabel();
	JMenuItem cat = new JMenuItem("Cat");
	JMenuItem dog = new JMenuItem("Dog");
	JMenuItem mouse = new JMenuItem("Mouse");
	JToolBar toolbar = new JToolBar();
	JButton catbutton = new JButton();
	JButton dogbutton = new JButton();
	JButton mousebutton = new JButton();
	JButton previous = new JButton("Previous");
	JButton next = new JButton("Next");
	JPanel buttons = new JPanel();
	int index1 = 0;
	int index2 = 0;
	int index3 = 0;
	int flag;

	public FE11() {

		cats[0] = new ImageIcon(getClass().getResource("image/cat1.jpg"));
		cats[1] = new ImageIcon(getClass().getResource("image/cat2.jpg"));
		cats[2] = new ImageIcon(getClass().getResource("image/cat3.jpg"));
		dogs[0] = new ImageIcon(getClass().getResource("image/dog1.jpg"));
		dogs[1] = new ImageIcon(getClass().getResource("image/dog2.jpg"));
		dogs[2] = new ImageIcon(getClass().getResource("image/dog3.jpg"));
		mouses[0] = new ImageIcon(getClass().getResource("image/mouse1.jpg"));
		mouses[1] = new ImageIcon(getClass().getResource("image/mouse2.jpg"));
		mouses[2] = new ImageIcon(getClass().getResource("image/mouse3.jpg"));
		setJMenuBar(menubar);
		menubar.add(menu);
		menu.add(cat);
		menu.add(dog);
		menu.add(mouse);
		this.setLayout(new BorderLayout());
		toolbar.setLayout(new GridLayout(3, 1));
		catbutton.setIcon(caticon);
		dogbutton.setIcon(dogicon);
		mousebutton.setIcon(mouseicon);
		toolbar.add(catbutton);
		toolbar.add(dogbutton);
		toolbar.add(mousebutton);
		this.add(toolbar, BorderLayout.EAST);
		this.add(display, BorderLayout.CENTER);
		buttons.setLayout(new GridLayout(1, 2));
		buttons.add(previous);
		buttons.add(next);
		this.add(buttons, BorderLayout.SOUTH);
		cat.addActionListener(this);
		dog.addActionListener(this);
		mouse.addActionListener(this);
		catbutton.addActionListener(this);
		dogbutton.addActionListener(this);
		mousebutton.addActionListener(this);
		previous.addActionListener(this);
		next.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == cat) {
			index1 = 0;
			display.setIcon(cats[index1]);
			flag = 1;

		} else if (e.getSource() == dog) {
			index2 = 0;
			display.setIcon(dogs[index2]);
			flag = 2;
		} else if (e.getSource() == mouse) {
			index3 = 0;
			display.setIcon(mouses[index3]);
			flag = 3;
		} else if (e.getSource() == catbutton) {
			index1 = 0;
			display.setIcon(cats[index1]);
			flag = 1;

		} else if (e.getSource() == dogbutton) {
			index2 = 0;
			display.setIcon(dogs[index2]);
			flag = 2;
		} else if (e.getSource() == mousebutton) {
			index3 = 0;
			display.setIcon(mouses[index3]);
			flag = 3;
		}
		if (e.getSource() == previous && flag == 1) {
			if (index1 == 0) {
				JOptionPane.showMessageDialog(null,
						"This is the first picture!");
			} else if (index1 != 0) {
				index1--;
				display.setIcon(cats[index1]);
			}
		}
		if (e.getSource() == previous && flag == 2) {
			if (index2 == 0) {
				JOptionPane.showMessageDialog(null,
						"This is the first picture!");
			} else if (index2 != 0) {
				index2--;
				display.setIcon(dogs[index2]);
			}
		}
		if (e.getSource() == previous && flag == 3) {
			if (index3 == 0) {
				JOptionPane.showMessageDialog(null,
						"This is the first picture!");
			} else if (index3 != 0) {
				index3--;
				display.setIcon(mouses[index3]);
			}
		}
		if (e.getSource() == next && flag == 1) {
			if (index1 == 2) {
				JOptionPane
						.showMessageDialog(null, "This is the last picture!");
			} else if (index1 != 2) {
				index1++;
				display.setIcon(cats[index1]);
			}
		}
		if (e.getSource() == next && flag == 2) {
			if (index2 == 2) {
				JOptionPane
						.showMessageDialog(null, "This is the last picture!");
			} else if (index2 != 2) {
				index2++;
				display.setIcon(dogs[index2]);
			}
		}
		if (e.getSource() == next && flag == 3) {
			if (index3 == 2) {
				JOptionPane
						.showMessageDialog(null, "This is the last picture!");
			} else if (index3 != 2) {
				index3++;
				display.setIcon(mouses[index3]);
			}
		}
	}
}

// 60