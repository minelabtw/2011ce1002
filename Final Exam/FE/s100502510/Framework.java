package fe.s100502510;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Framework extends JFrame implements ActionListener {
	JLabel title = new JLabel("The Bomber Infrared Screen");
	JLabel languagetitle = new JLabel("Language:");
	JComboBox languages = new JComboBox();
	JLabel positiontitle = new JLabel("Position:");
	JLabel currentplace = new JLabel();
	JLabel resulttitle = new JLabel("Result:");
	JLabel finalresult = new JLabel();
	DrawBuildings draw = new DrawBuildings();
	JPanel up = new JPanel();
	JPanel left = new JPanel();
	JPanel right = new JPanel();
	JPanel down = new JPanel();
	JPanel downleft = new JPanel();
	JPanel downright = new JPanel();
	int position = 0;
	String position2 = "";

	public Framework() {
		position2 = position2 + position;
		currentplace.setText(position2);
		this.setLayout(new BorderLayout());
		left.setLayout(new BorderLayout());
		right.setLayout(new BorderLayout());
		up.setLayout(new BorderLayout());
		left.add(title, BorderLayout.WEST);
		right.add(languagetitle, BorderLayout.WEST);
		right.add(languages, BorderLayout.EAST);
		up.add(left, BorderLayout.WEST);
		up.add(right, BorderLayout.EAST);
		downleft.setLayout(new BorderLayout());
		downleft.add(positiontitle, BorderLayout.WEST);
		downleft.add(currentplace, BorderLayout.CENTER);
		downright.setLayout(new BorderLayout());
		downright.add(resulttitle, BorderLayout.CENTER);
		downright.add(finalresult, BorderLayout.EAST);
		down.setLayout(new GridLayout(1, 2));
		down.add(downleft);
		down.add(downright);
		this.add(down, BorderLayout.SOUTH);
		this.add(up, BorderLayout.NORTH);
		this.add(draw, BorderLayout.CENTER);

	}

	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub

	}

}
