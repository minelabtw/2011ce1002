package fe.s100502510;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class DrawBuildings extends JPanel implements ActionListener,
		MouseListener, MouseMotionListener {
	Timer timer = new Timer(200, this);
	int xBase = 0;

	public DrawBuildings() {
		timer.start();

	}

	public void paintComponent(Graphics g) {
		int yBase = getHeight() / 2;
		super.paintComponent(g);
		g.drawRect(xBase + 10, yBase, 200, 200);
		g.drawRect(xBase + 60, yBase - 50, 200, 200);
		xBase += 10;
		if (xBase >= getWidth() - 250) {
			xBase = 0;
		}

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		repaint();

	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

}
