package fe.s100502516;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class DrawBuildings extends JPanel implements ActionListener {
	private int xBase = 0;
	private int yBase = -1;
	private Timer timer = new Timer(200, this);
	private boolean pointLive = false;	//is point click
	private MouseEvent e;
	
	public DrawBuildings()
	{				
		timer.start();
	}
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		if(yBase == -1)
			yBase = getHeight() / 2;	
		
		g.drawRect(xBase + 60, yBase - 50, 190, 150);
		g.drawRect(xBase + 10, yBase, 190, 200);
		
		if(pointLive)
		{
			g.fillOval(e.getX() - 10, e.getY() - 70, 10, 10);			
		}
	}
	public void setMouseEvent(MouseEvent e)
	{
		this.e = e;
	}
	public void setPointLive(boolean bool)
	{
		pointLive = bool;
	}
	public boolean isGetInA()
	{
		if(e.getX() >= xBase + 60 && e.getX() <= xBase + 250)
			return true;
		return false;
	}
	public boolean isGetInB()
	{
		return false;
	}
	
	public void actionPerformed(ActionEvent e) 
	{		
		yBase = getHeight() / 2;	
		
		xBase++;
		if(xBase >= getWidth())
		{
			xBase = 0;
		}
		
		repaint();
	}
}
