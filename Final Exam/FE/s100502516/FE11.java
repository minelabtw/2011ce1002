package fe.s100502516;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class FE11 extends JApplet {
	private ImageIcon[] titleIcon = new ImageIcon[3];		
	private ImageIcon[] catIcon = new ImageIcon[3];
	private ImageIcon[] dogIcon = new ImageIcon[3];
	private ImageIcon[] mouseIcon = new ImageIcon[3];
	private Gallery gallery;
	private JButton jbtPrevious = new JButton("��");		//control the gallery image
	private JButton jbtNext = new JButton("��");
	
	public void init()
	{
		titleIcon[0] = new ImageIcon(getClass().getResource("image/cat_icon.jpg"));
		titleIcon[1] = new ImageIcon(getClass().getResource("image/dog_icon.jpg"));
		titleIcon[2] = new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));
		
		for(int i = 0; i < 3; i++)
		{
			catIcon[i] = new ImageIcon(getClass().getResource("image/cat" + (i + 1) + ".jpg"));
			dogIcon[i] = new ImageIcon(getClass().getResource("image/dog" + (i + 1) + ".jpg"));
			mouseIcon[i] = new ImageIcon(getClass().getResource("image/mouse" + (i + 1) + ".jpg"));
		}		
		
		Action catAction, dogAction, mouseAction;		
		JMenuBar jmb = new JMenuBar();
		JMenu menu = new JMenu("Animals Gallery");
		menu.add(catAction = new MyAction("cat", titleIcon[0]));
		menu.add(dogAction = new MyAction("dog", titleIcon[1]));
		menu.add(mouseAction = new MyAction("mouse", titleIcon[2]));
		this.setJMenuBar(jmb);
		jmb.add(menu);
		
		JToolBar jtb = new JToolBar("Animals Gallery");
		jtb.setBorder(BorderFactory.createLineBorder(Color.BLACK));				
		jtb.add(catAction);
		jtb.add(dogAction);
		jtb.add(mouseAction);
		this.add(jtb, BorderLayout.EAST);
		
		gallery = new Gallery(catIcon);
		this.add(gallery, BorderLayout.CENTER);
		
		jbtPrevious.addActionListener(new ButtonListener());
		jbtNext.addActionListener(new ButtonListener());
		
		JPanel p = new JPanel(new GridLayout(1, 2));
		p.add(jbtPrevious);
		p.add(jbtNext);
		this.add(p, BorderLayout.SOUTH);
	}
	
	private class MyAction extends AbstractAction	//action for menu and tool bar
	{
		String name;
		
		public MyAction(String name, ImageIcon icon)
		{
			super(name, icon);
			putValue(Action.SHORT_DESCRIPTION, name + " gallery");
			this.name = name;
		}
		
		public void actionPerformed(ActionEvent e) 
		{		
			if(name.equals("cat"))
			{
				gallery.setIconArray(catIcon);
			}
			else if(name.equals("dog"))
			{
				gallery.setIconArray(dogIcon);
			}
			else if(name.equals("mouse"))
			{
				gallery.setIconArray(mouseIcon);
			}		
		}		
	}
	private class Gallery extends JLabel	//the label for display the animal image
	{
		private ImageIcon[] icon;
		private int num = 0;
		
		public Gallery(ImageIcon[] icon)
		{
			setIconArray(icon);
		}
		public void setIconArray(ImageIcon[] icon)
		{
			num = 0;
			this.icon = icon;
			this.setIcon(icon[num]);
		}
		public void previous()	//to previous image
		{
			num--;
			
			if(num < 0)
			{
				num = 0;
				JOptionPane.showMessageDialog(null, "the first");
			}
			
			this.setIcon(icon[num]);
		}
		public void next()	//to next image
		{
			num++;
			
			if(num > 2)
			{
				num = 2;
				JOptionPane.showMessageDialog(null, "the last");
			}
			
			this.setIcon(icon[num]);
		}
	}
	private class ButtonListener implements ActionListener	//action for two button
	{
		public void actionPerformed(ActionEvent e) 
		{
			if(e.getSource() == jbtPrevious)
			{
				gallery.previous();
			}
			else if(e.getSource() == jbtNext)
			{
				gallery.next();
			}
		}		
	}
}
