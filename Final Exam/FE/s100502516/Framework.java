package fe.s100502516;

import java.awt.*;
import java.awt.event.*;
import java.util.ResourceBundle;

import javax.swing.*;

public class Framework extends JFrame implements ActionListener {
	private JLabel jlelTitle = new JLabel();
	private JLabel jlelLanguage = new JLabel();	
	private JLabel jlelPosition = new JLabel();
	private JLabel jlelPositionValue = new JLabel("positionValue");
	private JLabel jlelResult = new JLabel();
	private JLabel jlelResultValue = new JLabel("resultValue");
	private DrawBuildings display = new DrawBuildings();
	private String[] country = {"中國", "日本", "英國" , "法國"};
	private JComboBox jcbo = new JComboBox(country);
	private ResourceBundle resource;
	
	public Framework()
	{
		JPanel p1 = new JPanel(new GridLayout(1, 2));
		JPanel p2 = new JPanel(new FlowLayout());		
		p2.add(jlelLanguage);
		p2.add(jcbo);
		p1.add(jlelTitle);
		p1.add(p2);
		this.add(p1, BorderLayout.NORTH);
		
		this.add(display, BorderLayout.CENTER);
		
		JPanel p3 = new JPanel(new GridLayout(1, 2));
		JPanel p4 = new JPanel(new FlowLayout());
		JPanel p5 = new JPanel(new FlowLayout());
		p4.add(jlelPosition);
		p4.add(jlelPositionValue);
		p5.add(jlelResult);
		p5.add(jlelResultValue);
		p3.add(p4);
		p3.add(p5);
		this.add(p3, BorderLayout.SOUTH);		
		
		jcbo.addItemListener(new JComboBoxListener());
		this.addMouseListener(new MouseActionListener());
				
		resource = ResourceBundle.getBundle("Confidential_zh");
		this.setText();
	}
	public void setLanguage(int index)	//change language
	{
		switch(index)
		{
			case 0:
				resource = ResourceBundle.getBundle("Confidential_zh");
				break;
			case 1:
				resource = ResourceBundle.getBundle("Confidential_ja");
				break;
			case 2:
				resource = ResourceBundle.getBundle("Confidential_en");
				break;
			case 3:
				resource = ResourceBundle.getBundle("Confidential_fr");
				break;
			default:
				resource = ResourceBundle.getBundle("Confidential");
				break;
		}
		
		this.setText();
	}
	public void setText()	//update language
	{
		jlelTitle.setText(resource.getString("Title"));
		jlelLanguage.setText(resource.getString("Language"));
		jlelPosition.setText(resource.getString("Position"));
		jlelResult.setText(resource.getString("Result"));
	}
	
	public void actionPerformed(ActionEvent e) 
	{	
		
	}
	
	private class JComboBoxListener implements ItemListener
	{
		public void itemStateChanged(ItemEvent e)
		{		
			setLanguage(jcbo.getSelectedIndex());
		}		
	}
	private class MouseActionListener implements MouseListener
	{
		public void mouseClicked(MouseEvent e)	//add point
		{		
			display.setPointLive(true);
			display.setMouseEvent(e);
			jlelPositionValue.setText(String.valueOf(e.getX()) + "," + String.valueOf(e.getX()));
		}
		public void mouseEntered(MouseEvent e)
		{
					
		}		
		public void mouseExited(MouseEvent e)
		{
		
		}
		public void mousePressed(MouseEvent e)
		{
			
		}
		public void mouseReleased(MouseEvent e) 
		{
						
		}
		
	}
}
