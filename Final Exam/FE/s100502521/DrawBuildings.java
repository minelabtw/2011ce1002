package fe.s100502521;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class DrawBuildings extends JPanel
{
	private int xBase=0,yBase=0;
	private int bombX=-50,bombY=-50;
	private Timer timer=new Timer(200,new timerListener());
	public DrawBuildings()
	{
		
		
		
		
		
		timer.start();
	}
	public int getResult()
	{
		boolean a=false,b=false;
		if(bombX>xBase+10&&bombX<xBase+200)
		{
			if(bombY>yBase&&bombY<yBase+200)
			{
				a=true;
			}
		}
		if(bombX>xBase+60&&bombX<xBase+250)
		{
			if(bombY>yBase-50&&bombY<yBase+100)
			{
				b=true;
			}
		}
		if(a&&b)
		{
			return 3;
		}
		else if(a)
		{
			return 1;
		}
		else if(b)
		{
			return 2;
		}
		else
		{
			return 0;
		}
	}
	public void setMousePositionX(int x_temp)
	{
		bombX=x_temp;
	}
	public void setMousePositionY(int y_temp)
	{
		bombY=y_temp;
	}
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		yBase=getHeight()/2;
		g.drawRect(xBase+10, yBase, 190, 200);
		g.drawString("A", xBase, yBase+190);
		g.drawRect(xBase+60, yBase-50, 190, 150);
		g.drawString("B", xBase+50, yBase+90);
		g.drawOval(bombX, bombY, 10, 10);
		g.fillOval(bombX, bombY, 10, 10);
	}
	public class timerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(xBase<getWidth())
			{
				xBase++;
			}
			else
			{
				xBase=-200;
			}
			repaint();
		}
		
	}
}
