package fe.s100502521;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Framework extends JFrame implements ActionListener,MouseListener,MouseMotionListener
{
	private JLabel []label=new JLabel[6];
	private ResourceBundle res=ResourceBundle.getBundle("fe/s100502521/Confidential");
	private Locale local=Locale.getDefault();
	private JComboBox chosseLocale=new JComboBox();
	private JPanel North=new JPanel();
	private JPanel South=new JPanel();
	private int x=0,y=0;
	private DrawBuildings paintpanel=new DrawBuildings();
	public Framework()
	{
		setLayout(new BorderLayout());
		North.setLayout(new GridLayout(0,3,0,0));
		South.setLayout(new GridLayout(0,4,0,0));
		label[0]=new JLabel(res.getString("Title"));
		label[1]=new JLabel(res.getString("Language"));
		label[2]=new JLabel(res.getString("Position"));
		label[3]=new JLabel("0,0");		
		label[4]=new JLabel(res.getString("Result"));
		label[5]=new JLabel("");
		chosseLocale.addItem("中文");
		chosseLocale.addItem("日文");
		chosseLocale.addItem("英文");
		chosseLocale.addItem("法文");
		chosseLocale.addItemListener(new combo());
		North.add(label[0]);
		North.add(label[1]);
		North.add(chosseLocale);
		South.add(label[2]);
		South.add(label[3]);
		South.add(label[4]);
		South.add(label[5]);
		add(North,BorderLayout.NORTH);
		add(South,BorderLayout.SOUTH);
		add(paintpanel,BorderLayout.CENTER);
		addMouseListener(this);
		addMouseMotionListener(this);
	}
	public void reFresh()
	{
		label[0].setText(res.getString("Title"));
		label[1].setText(res.getString("Language"));
		label[2].setText(res.getString("Position"));
		label[4].setText(res.getString("Result"));
	}
	public class combo implements ItemListener
	{
		public void itemStateChanged(ItemEvent e)
		{
			if(e.getSource()==chosseLocale)
			{
				switch(chosseLocale.getSelectedIndex())
				{
				case 0:
					res=ResourceBundle.getBundle("fe/s100502521/Confidential",Locale.CHINESE);
					break;
				case 1:
					res=ResourceBundle.getBundle("fe/s100502521/Confidential",Locale.JAPANESE);
					break;
				case 2:
					res=ResourceBundle.getBundle("fe/s100502521/Confidential",Locale.ENGLISH);
					break;
				case 3:
					res=ResourceBundle.getBundle("fe/s100502521/Confidential",Locale.FRENCH);
					break;
				}
				reFresh();
			}
		}
		
	}
	public void mouseDragged(MouseEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseMoved(MouseEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseClicked(MouseEvent arg0) 
	{
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseEntered(MouseEvent arg0) 
	{
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent arg0)
	{
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mousePressed(MouseEvent arg0) 
	{
		x=arg0.getX();
		y=arg0.getY();
		label[3].setText(x+","+y);
		paintpanel.setMousePositionX(x-10);
		paintpanel.setMousePositionY(y-label[0].getHeight()-32);
		switch(paintpanel.getResult())
		{
		case 0:
			label[5].setText("Miss");
			break;
		case 1:
			label[5].setText("Hit Building A");
			break;
		case 2:
			label[5].setText("Hit Building B");
			break;
		case 3:
			label[5].setText("Hit Building A AND B");
			break;
		}
	}
	@Override
	public void mouseReleased(MouseEvent arg0) 
	{
		// TODO Auto-generated method stub
		
	}
	@Override
	public void actionPerformed(ActionEvent arg0) 
	{
		// TODO Auto-generated method stub
		
	}
	
}
