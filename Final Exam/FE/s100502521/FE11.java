package fe.s100502521;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class FE11 extends JApplet
{
	private ImageIcon []pictures=new ImageIcon[9];
	private ImageIcon []choose=new ImageIcon[3];
	private JMenuBar menubar=new JMenuBar();
	private JMenu menu=new JMenu("menu");
	private JMenuItem []m_choose=new JMenuItem[3];
	private JToolBar toolbar=new JToolBar();
	private int gallery=0,p_index=0;
	private JLabel picture=new JLabel();
	private JPanel upDown=new JPanel();
	private JButton up=new JButton("上一張");
	private JButton down=new JButton("下一張");
	private Action action1;
	private Action action2;
	private Action action3;
	public FE11()
	{
		setLayout(new BorderLayout());
		pictures[0]=new ImageIcon(getClass().getResource("image/cat1.jpg"));
		pictures[1]=new ImageIcon(getClass().getResource("image/cat2.jpg"));
		pictures[2]=new ImageIcon(getClass().getResource("image/cat3.jpg"));
		pictures[3]=new ImageIcon(getClass().getResource("image/dog1.jpg"));
		pictures[4]=new ImageIcon(getClass().getResource("image/dog2.jpg"));
		pictures[5]=new ImageIcon(getClass().getResource("image/dog3.jpg"));
		pictures[6]=new ImageIcon(getClass().getResource("image/mouse1.jpg"));
		pictures[7]=new ImageIcon(getClass().getResource("image/mouse2.jpg"));
		pictures[8]=new ImageIcon(getClass().getResource("image/mouse3.jpg"));
		choose[0]=new ImageIcon(getClass().getResource("image/cat_icon.jpg"));
		choose[1]=new ImageIcon(getClass().getResource("image/dog_icon.jpg"));
		choose[2]=new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));
		action1=new Action("Cat",choose[0]);
		action2=new Action("Dog",choose[1]);
		action3=new Action("Mouse",choose[2]);
		m_choose[0]=new JMenuItem("Cat");
		m_choose[1]=new JMenuItem("Dog");
		m_choose[2]=new JMenuItem("Mouse");
		m_choose[0].addActionListener(new actionListener());
		m_choose[1].addActionListener(new actionListener());
		m_choose[2].addActionListener(new actionListener());
		menubar.add(menu);
		menu.setAutoscrolls(false);
		menu.add(m_choose[0]);
		menu.add(m_choose[1]);
		menu.add(m_choose[2]);
		add(menubar,BorderLayout.NORTH);
		add(picture,BorderLayout.CENTER);
		upDown.setLayout(new GridLayout(0,2,0,0));
		up.addActionListener(new actionListener());
		down.addActionListener(new actionListener());
		upDown.add(up);
		upDown.add(down);
		add(upDown,BorderLayout.SOUTH);
		changePicture();
		toolbar.add(action1);
		toolbar.add(action2);
		toolbar.add(action3);
		add(toolbar,BorderLayout.EAST);
	}
	public void changePicture()
	{
		picture.setIcon(pictures[3*gallery+p_index]);
	}
	public class Action extends AbstractAction
	{
		public Action(String name,Icon icon)
		{
			this.getValue(name);
			this.putValue(name, null);
		}
		public void actionPerformed(ActionEvent e)
		{
			if(e.getSource()==action1)
			{
				gallery=0;
				changePicture();
			}
			else if(e.getSource()==action2)
			{
				gallery=1;
				changePicture();
			}
			else if(e.getSource()==action3)
			{
				gallery=2;
				changePicture();
			}
		}
	}
	public class actionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(e.getSource()==m_choose[0])
			{
				gallery=0;
				changePicture();
			}
			else if(e.getSource()==m_choose[1])	
			{
				gallery=1;
				changePicture();
			}
			else if(e.getSource()==m_choose[2])
			{
				gallery=2;
				changePicture();
			}
			else if(e.getSource()==up)
			{
				if(p_index==0)
				{
					JOptionPane.showMessageDialog(null, "這是第一張了喔");
				}
				else
				{
					p_index--;
				}
				changePicture();
			}
			else if(e.getSource()==down)
			{
				if(p_index==2)
				{
					JOptionPane.showMessageDialog(null, "這是最後一張了喔");
				}
				else
				{
					p_index++;
				}
				changePicture();
			}
		}
		
	}
}
