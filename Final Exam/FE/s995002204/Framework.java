package fe.s995002204;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.*;

public class Framework extends JFrame{
	
	//紀錄滑鼠座標
	private int position_x=0;
	private int position_y=0;
	
	//declare variable
	private ResourceBundle res = ResourceBundle.getBundle("Confidential");
	private Locale locale = Locale.getDefault();
	
	private JLabel label_1 = new JLabel(res.getString("Title"));
	private JLabel label_2 = new JLabel(res.getString("Language"));
	private JLabel label_3 = new JLabel();
	private JLabel label_4 = new JLabel(res.getString("Position"));
	private JLabel label_5 = new JLabel();
	private JLabel label_6 = new JLabel(res.getString("Result"));
	private JLabel label_7 = new JLabel("無結果");
	private JPanel panel = new JPanel();
	
	private DrawBuildings build = new DrawBuildings();
	private JPanel up = new JPanel();
	private JPanel down = new JPanel();
	
	private String language[] = {"中文","法文","英文","日文"};
	private JComboBox combobox = new JComboBox(language);
	
	//Constructor
	public Framework()
	{
		setLayout(new BorderLayout());
		
		up.add(label_1);
		up.add(label_2);
		up.add(combobox);
		
		label_3.add(build);
		
		down.add(label_4);
		down.add(label_5);
		down.add(label_6);
		down.add(label_7);
		
		
		add(up,BorderLayout.NORTH);
		add(label_3,BorderLayout.CENTER);
		add(down,BorderLayout.SOUTH);
		
		//顯示座標
		label_3.addMouseListener(new MouseAdapter()
		{
			public void mousePressed(MouseEvent e)
			{
				label_5.setText(e.getPoint().toString());
				position_x = e.getPoint().x;
				position_y = e.getPoint().y;
			}
		});
		
		//combobox listener
		combobox.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if(combobox.getSelectedIndex()==0)
				{
					locale = locale.CHINESE;
					UpdateString();
				}
				else if(combobox.getSelectedIndex()==1)
				{
					locale = locale.FRENCH;
					UpdateString();
				}
				else if(combobox.getSelectedIndex()==2)
				{
					locale = locale.ENGLISH;
					UpdateString();
				}
				else
				{
					locale = locale.JAPANESE;
					UpdateString();
				}
			}
		});
	}
	
	//判斷擊中
	public void check(int x,int y)
	{
		boolean a = false;
		boolean b = false;
		if((x>build.getBaseX()+10)&&(x<build.getBaseX()+200)&&(y<build.getBaseY()+200)&&(y>build.getBaseY()))
		{
			a = true;
		}
		if((x>build.getBaseX()+60)&&(x<build.getBaseX()+250)&&(y<build.getBaseY()+100)&&(y>build.getBaseY()-50))
		{
			b = true;
		}
		
		if(a&&b)
		{
			label_5.setText("A屋和B屋擊中!!!");
		}
		else if(a)
		{
			label_5.setText("A屋擊中!!!");
		}
		else if(b)
		{
			label_5.setText("B屋擊中!!!");
		}
		else
		{
			label_5.setText("沒擊中QQ");
		}
	}
	
	//更新字串
	private void UpdateString()
	{
		res = ResourceBundle.getBundle("Confidential",locale);
		label_1.setText(res.getString("Title"));
		label_2.setText(res.getString("Language"));
		label_4.setText(res.getString("Position"));
		label_6.setText(res.getString("Result"));
		repaint();
	}
}
