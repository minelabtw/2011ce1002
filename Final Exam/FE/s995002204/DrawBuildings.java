package fe.s995002204;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class DrawBuildings extends JPanel{
	
	private int xBase = 85;
	private int yBase = 190;
	
	private Timer timer = new Timer(200,new ActionListener()
	{
		public void actionPerformed(ActionEvent e)
		{
			xBase++;
			repaint();
		}
	});
	
	public DrawBuildings()
	{
		timer.start();
	}
	
	//�e��
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.YELLOW);
		g.drawRect(xBase+10, yBase, 190, 200);
		g.drawRect(xBase+60, yBase-50, 190, 150);
	}
	
	public void setMousePositionX(int x_temp)
	{
		xBase = x_temp;
	}
	
	public void setMousePositionY(int y_temp)
	{
		yBase = y_temp;
	}
	
	public int getBaseX()
	{
		return xBase;
	}
	
	public int getBaseY()
	{
		return yBase;
	}
}
