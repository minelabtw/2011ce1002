package fe.s100502545;

import java.awt.*;
import java.awt.event.*;


import javax.swing.*;



public class FE11 extends JApplet implements ActionListener 
{
	//create Image_Icon
	private ImageIcon cat1=new ImageIcon(getClass().getResource("Image/cat1.jpg"));								
	private ImageIcon cat2=new ImageIcon(getClass().getResource("Image/cat2.jpg"));
    private ImageIcon cat3=new ImageIcon(getClass().getResource("Image/cat3.jpg"));
    private ImageIcon dog1=new ImageIcon(getClass().getResource("Image/dog1.jpg"));
    private ImageIcon dog2=new ImageIcon(getClass().getResource("Image/dog2.jpg"));
    private ImageIcon dog3=new ImageIcon(getClass().getResource("Image/dog3.jpg"));
    private ImageIcon mouse1=new ImageIcon(getClass().getResource("Image/mouse1.jpg"));
    private ImageIcon mouse2=new ImageIcon(getClass().getResource("Image/mouse2.jpg"));
    private ImageIcon mouse3=new ImageIcon(getClass().getResource("Image/mouse3.jpg"));
    
	private ImageIcon Animals[]={cat1,cat2,cat3,dog1,dog2,dog3,mouse1,mouse2,mouse3};
	
	private ImageIcon cat_icon = new ImageIcon(getClass().getResource("Image/cat_icon.jpg"));
	private ImageIcon dog_icon = new ImageIcon(getClass().getResource("Image/dog_icon.jpg"));
	private ImageIcon mouse_icon = new ImageIcon(getClass().getResource("Image/mouse_icon.jpg"));
	
	
	private JLabel Label_img = new JLabel();
	private JPanel p = new JPanel();
	private JButton L = new JButton("��");
	private JButton R = new JButton("��");
	
	int index =0 ;

	public FE11()
	{
		Action cat_Actions = new MyAction("cat",cat_icon);
		Action dog_Actions = new MyAction("dog",dog_icon);
		Action mouse_Actions = new MyAction("mouse",mouse_icon);
		//create Menu
		JMenuBar menubar = new JMenuBar();
		JMenu menu = new JMenu("ablum");
		menubar.add(menu);
		setJMenuBar(menubar);
		
		menu.add(cat_Actions);
		menu.add(dog_Actions);
		menu.add(mouse_Actions);
		
		//create ToolBar
		JToolBar toolbar = new JToolBar(JToolBar.VERTICAL);
		//toolbar.setBorder(BorderFactory.createLineBorder(Color.red));
		
		
		
		toolbar.add(cat_Actions);
		toolbar.add(dog_Actions);
		toolbar.add(mouse_Actions);
		
		p.add(L);
		p.add(R);
		
		
		add(toolbar,BorderLayout.EAST);
		add(Label_img,BorderLayout.CENTER);
		add(p,BorderLayout.SOUTH);
		
		L.addActionListener(this);
		R.addActionListener(this);
	}
	
	
	
	
	public class MyAction extends AbstractAction
	{//menu actions
		String name;
		MyAction(String name,Icon icon)
		{
			super(name,icon);
			putValue(Action.SHORT_DESCRIPTION,name+"ablum");
			this.name=name;
		}
		public void actionPerformed(ActionEvent e) 
		{
			if(name.equals("cat"))
			{
				index =0;
				Label_img.setIcon(Animals[index]);
			}
			else if(name.equals("dog")){
				index = 3;
				Label_img.setIcon(Animals[index]);
			}
			else if(name.equals("mouse")){
				index = 6;
				Label_img.setIcon(Animals[index]);
			}
		}
	}


	//�W�U�i
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()  == L)
		{
			index ++;
			Label_img.setIcon(Animals[index]);
		}
		
		else if(e.getSource()  == R)
		{
			index --;
			Label_img.setIcon(Animals[index]);
		}
	}
}
		


