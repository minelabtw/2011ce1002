package fe.s100502028;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;



public class FE11 extends JApplet implements ActionListener {
	private ImageIcon[] arrayImageIcon = {
			new ImageIcon(getClass().getResource("image/cat1.jpg")),
			new ImageIcon(getClass().getResource("image/cat2.jpg")),
			new ImageIcon(getClass().getResource("image/cat3.jpg")),
			new ImageIcon(getClass().getResource("image/dog1.jpg")),
			new ImageIcon(getClass().getResource("image/dog2.jpg")),
			new ImageIcon(getClass().getResource("image/dog3.jpg")),
			new ImageIcon(getClass().getResource("image/mouse1.jpg")),
			new ImageIcon(getClass().getResource("image/mouse2.jpg")),
			new ImageIcon(getClass().getResource("image/mouse3.jpg"))
			};
	private int index = 0;
	
	private ImageIcon cat_icon = new ImageIcon(getClass().getResource("image/cat_icon.jpg"));
	private ImageIcon dog_icon = new ImageIcon(getClass().getResource("image/dog_icon.jpg"));
	private ImageIcon mouse_icon = new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));
	
	private JLabel Label_icon = new JLabel();
	
	private Action cat_action = new MyAction("Cat", cat_icon);
	private Action dog_action = new MyAction("Dog", dog_icon);
	private Action mouse_action = new MyAction("Mouse", mouse_icon);
	
	private JButton jbtPrevious = new JButton("Previous");
	private JButton jbtNext = new JButton("Next");
	
	public FE11() {
		JMenuBar jMenuBar = new JMenuBar();
		JMenu jmenuAnimal = new JMenu("Animals Gallery");
		setJMenuBar(jMenuBar);
		jMenuBar.add(jmenuAnimal);
		jmenuAnimal.add(cat_action);
		jmenuAnimal.add(dog_action);
		jmenuAnimal.add(mouse_action);
		
		JToolBar jToolBar = new JToolBar(JToolBar.VERTICAL);
		jToolBar.setFloatable(false);
		jToolBar.add(cat_action);
		jToolBar.add(dog_action);
		jToolBar.add(mouse_action);
		
		JPanel panelForButtons = new JPanel(new GridLayout(1, 2, 5, 5));
		panelForButtons.add(jbtPrevious);
		panelForButtons.add(jbtNext);
		
		add(jToolBar, BorderLayout.EAST);
		add(Label_icon, BorderLayout.CENTER);
		add(panelForButtons, BorderLayout.SOUTH);
		
		jbtPrevious.addActionListener(this);
		jbtNext.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == jbtPrevious) {
			if (index == 0 || index == 3 || index == 6) {
				Label_icon.setIcon(arrayImageIcon[index]);
				JOptionPane.showMessageDialog(null, "This is the first picture!");
			}
			else {
				index--;
				Label_icon.setIcon(arrayImageIcon[index]);
			}
		}
		else if (e.getSource() == jbtNext) {
			if (index == 2 || index == 5 || index == 8) {
				Label_icon.setIcon(arrayImageIcon[index]);
				JOptionPane.showMessageDialog(null, "This is the last picture!");
			}
			else {
				index++;
				Label_icon.setIcon(arrayImageIcon[index]);
			}
		}
	}
	
	private class MyAction extends AbstractAction {
		String name;
		MyAction(String name, Icon icon) {
			super(name, icon);
			this.name = name;
		}
		
		public void actionPerformed(ActionEvent e) {
			if (name.equals("Cat")) {
				index = 0;
				Label_icon.setIcon(arrayImageIcon[index]);
			}
			else if (name.equals("Dog")) {
				index = 3;
				Label_icon.setIcon(arrayImageIcon[index]);
			}
			else if (name.equals("Mouse")) {
				index = 6;
				Label_icon.setIcon(arrayImageIcon[index]);
			}
		}
	}
}
