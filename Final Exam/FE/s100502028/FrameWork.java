package fe.s100502028;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener, MouseListener, MouseMotionListener{
	private Locale[] availableLocales = {Locale.US, Locale.CHINA, Locale.FRANCE, Locale.JAPAN};
	private Locale locale = Locale.getDefault();
	
	private ResourceBundle res = ResourceBundle.getBundle("Confidential");
	
	private JLabel JLabel_1 = new JLabel(res.getString("Title"));
	private JLabel JLabel_2 = new JLabel(res.getString("Language"));
	private JLabel JLabel_3 = new JLabel();
	private JLabel JLabel_4 = new JLabel(res.getString("Position"));
	private JLabel JLabel_5 = new JLabel("0,0");
	private JLabel JLabel_6 = new JLabel(res.getString("Result"));
	private JLabel JLabel_7 = new JLabel();
	
	private JComboBox jcboLanguages = new JComboBox();
	
	DrawBuildings drawPanel = new DrawBuildings();
	
	public FrameWork() {
		for (int i = 0; i < availableLocales.length; i++)
			jcboLanguages.addItem(availableLocales[i].getDisplayName());
		updateStrings();
		
		JPanel p11 = new JPanel(new BorderLayout(5, 5));
		p11.add(JLabel_2, BorderLayout.WEST);
		p11.add(jcboLanguages, BorderLayout.CENTER);
		
		JPanel p1 = new JPanel(new GridLayout(1, 2, 5, 5));
		p1.add(JLabel_1);
		p1.add(p11);
		
		JLabel_3.add(drawPanel);
		
		JPanel p21 = new JPanel(new GridLayout(1, 2, 5, 5));
		p21.add(JLabel_4);
		p21.add(JLabel_5);
		
		JPanel p22 = new JPanel(new GridLayout(1, 2, 5, 5));
		p22.add(JLabel_6);
		p22.add(JLabel_7);
		
		JPanel p2 = new JPanel(new GridLayout(1, 2, 5, 5));
		p2.add(p21);
		p2.add(p22);
		
		add(p1, BorderLayout.NORTH);
		add(drawPanel, BorderLayout.CENTER);
		add(p2, BorderLayout.SOUTH);
		
		jcboLanguages.addActionListener(this);
		addMouseListener(this);
		repaint();
		revalidate();
	}
	
	public void updateStrings() {
		res = ResourceBundle.getBundle("Confidential", locale);
		JLabel_1.setText(res.getString("Title"));
		JLabel_2.setText(res.getString("Language"));
		JLabel_4.setText(res.getString("Position"));
		JLabel_6.setText(res.getString("Result"));
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == jcboLanguages) {
			locale = availableLocales[jcboLanguages.getSelectedIndex()];
			setLocale(locale);
			updateStrings();
		}
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (e.getClickCount() == 1) {
			JLabel_5.setText("" + e.getXOnScreen() + "," + e.getYOnScreen());
			drawPanel.setMousePositionX(e.getXOnScreen());
			drawPanel.setMousePositionY(e.getYOnScreen());
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
