package fe.s100502028;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class DrawBuildings extends JPanel implements ActionListener {
	private int xBase = 0, yBase = 0;
	private int x = 0, y = 0;
	private Timer timer = new Timer(200, new TimerListener());
	
	public DrawBuildings() {
		
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		timer.start();
		
		yBase = getHeight() / 2;
		int x1[] = {xBase+10, xBase+200, xBase+200, xBase+10};
		int y1[] = {yBase+200, yBase+200, yBase, yBase};
		int x2[] = {xBase+60, xBase+250, xBase+250, xBase+60};
		int y2[] = {yBase+100, yBase+100, yBase-50, yBase-50};
		
		g.drawPolygon(x1, y1, 4);
		g.drawPolygon(x2, y2, 4);
		
		if(xBase > getWidth())
			xBase = -190;
		else
			xBase += 5;
		
		g.drawOval(x, y, 5, 5);
	
	}
	
	public void setMousePositionX(int x_temp) {
		x = x_temp;
	}
	
	public void setMousePositionY(int y_temp) {
		y = y_temp;
	}

	public void actionPerformed(ActionEvent e) {
		
	}
	
	private class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			repaint();
		}
	}
}
