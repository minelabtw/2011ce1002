package fe.s100502502;
import java.awt.BorderLayout;
import java.awt.event.*;
import javax.swing.*;

public class FE11 extends JApplet{
	//create ImageIcons, JMenu, JToolBar, Action, JPanel, JLabel 
	ImageIcon[] images = {new ImageIcon(getClass().getResource("image/cat1.jpg")), 
			new ImageIcon(getClass().getResource("image/cat2.jpg")), 
			new ImageIcon(getClass().getResource("image/cat3.jpg")),
			new ImageIcon(getClass().getResource("image/dog1.jpg")), 
			new ImageIcon(getClass().getResource("image/dog2.jpg")), 
			new ImageIcon(getClass().getResource("image/dog3.jpg")), 
			new ImageIcon(getClass().getResource("image/mouse1.jpg")), 
			new ImageIcon(getClass().getResource("image/mouse2.jpg")), 
			new ImageIcon(getClass().getResource("image/mouse3.jpg")), 
			new ImageIcon(getClass().getResource("image/cat_icon.jpg")), 
			new ImageIcon(getClass().getResource("image/dog_icon.jpg")), 
			new ImageIcon(getClass().getResource("image/mouse_icon.jpg"))};
	int now = 0;
	JMenuBar menuBar = new JMenuBar();
	JMenu animals = new JMenu("Animals Gallery");
	JToolBar toolBar = new JToolBar();
	JButton previous = new JButton("Previous");
	JButton next = new JButton("Next");
	Action catAction = new Action("Cat", images[9]);
	Action dogAction = new Action("Dog", images[10]);
	Action mouseAction = new Action("Mouse", images[11]);
	JPanel P1 = new JPanel();//up
	JPanel P2 = new JPanel();//down
	JLabel L1 = new JLabel();
	public void init(){//initialize and addActionListener
		setSize(1200, 800);
		setJMenuBar(menuBar);
		menuBar.add(animals);
		animals.add(catAction);
		animals.add(dogAction);
		animals.add(mouseAction);
		toolBar.setOrientation(JToolBar.VERTICAL);
		toolBar.add(catAction);
		toolBar.add(dogAction);
		toolBar.add(mouseAction);
		L1.setIcon(images[0]);
		animals.setSelectedIcon(images[9]);
		P1.add(L1, BorderLayout.CENTER);
		P1.add(toolBar, BorderLayout.EAST);
		P2.add(previous);
		P2.add(next);
		add(P1, BorderLayout.CENTER);
		add(P2, BorderLayout.SOUTH);
		previous.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(animals.getSelectedIcon() == images[9]){//cat
					if(now == 0){
						JOptionPane.showMessageDialog(null, "This is the first picture.");
					}
					else if(now <= 2 && now>0){
						now--;
						L1.setIcon(images[now]);
					}
					else{}
				}
				else if(animals.getSelectedIcon() == images[10]){//dog
					if(now == 3){
						JOptionPane.showMessageDialog(null, "This is the first picture.");
					}
					else if(now <=5 && now > 3){
						now--;
						L1.setIcon(images[now]);
					}
					else{}
				}
				else if(animals.getSelectedIcon() == images[11]){//mouse
					if(now == 6){
						JOptionPane.showMessageDialog(null, "This is the first picture.");
					}
					else if(now <= 8 && now > 6){
						now--;
						L1.setIcon(images[now]);
					}
				}
			}
		});
		next.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(animals.getSelectedIcon() == images[9]){//cat
					System.out.print("1");
					if(now == 2){
						JOptionPane.showMessageDialog(null, "This is the last picture.");
					}
					else if(now <2 && now>=0){
						now++;
						L1.setIcon(images[now]);
					}
					else{}
				}
				else if(animals.getSelectedIcon() == images[10]){//dog
					System.out.print("2");
					if(now == 5){
						JOptionPane.showMessageDialog(null, "This is the last picture.");
					}
					else if(now < 5 && now>=3){
						
						now++;
						L1.setIcon(images[now]);
					}
					else{}
				}
				else if(animals.getSelectedIcon() == images[11]){//mouse
					if(now == 8){
						JOptionPane.showMessageDialog(null, "This is the last picture.");
					}
					else if(now < 8 && now>=6){
						now++;
						L1.setIcon(images[now]);
					}
				}
			}
		});
	}
	class Action extends AbstractAction{
		String name;
		Action(String name, Icon image){
			super(name, image);
			this.name = name;
		}
		public void actionPerformed(ActionEvent e){//when select action buttons
			if(name == "Cat"){
				now = 0;
				animals.setSelectedIcon(images[9]);
				L1.setIcon(images[0]);
			}
			else if(name == "Dog"){
				now = 3;
				animals.setSelectedIcon(images[10]);
				L1.setIcon(images[3]);
			}
			else if(name == "Mouse"){
				now = 6;
				animals.setSelectedIcon(images[11]);
				L1.setIcon(images[6]);
			}
		}
	}
}
