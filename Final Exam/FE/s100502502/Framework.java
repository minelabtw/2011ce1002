package fe.s100502502;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
public class Framework extends JFrame{
	//create Locale , JComboBox, JPanel, JLabel, Resource
	Locale locale = Locale.ENGLISH;
	Locale[] locales = {Locale.ENGLISH, Locale.CHINESE, Locale.JAPAN, Locale.FRENCH}; 
	JComboBox comboBox = new JComboBox(new String[]{locales[0].getDisplayName(), locales[1].getDisplayName(), locales[2].getDisplayName(), locales[3].getDisplayName()});
	ResourceBundle res = ResourceBundle.getBundle("Confidential", locale);
	JPanel P1 = new JPanel();
	JPanel P2 = new JPanel();
	JPanel P3 = new JPanel();
	JLabel L1 = new JLabel(res.getString("Title"));
	JLabel L2 = new JLabel(res.getString("Language"));
	JLabel L3 = new JLabel();
	JLabel L4 = new JLabel(res.getString("Position"));
	JLabel L5 = new JLabel();
	JLabel L6 = new JLabel(res.getString("Result"));
	JLabel L7 = new JLabel();
	DrawBuildings draw = new DrawBuildings();
	Framework(){//initialize
		P1.setLayout(new GridLayout(1, 3, 1, 1));
		P1.add(L1);
		P1.add(L2);
		P1.add(comboBox);
		L3.add(draw);
		P2.add(L3);
		P3.setLayout(new GridLayout(1, 4, 1, 1));
		P3.add(L4);
		P3.add(L5);
		P3.add(L6);
		P3.add(L7);
		add(P1, BorderLayout.NORTH);
		add(P2, BorderLayout.CENTER);
		add(P3, BorderLayout.SOUTH);
		comboBox.addActionListener(new ActionListener(){//when select
			public void actionPerformed(ActionEvent e){
				if(comboBox.getSelectedIndex() == 0){
					locale = locales[0];
					updateString();
				}
				else if(comboBox.getSelectedIndex() == 1){
					locale = locales[1];
					updateString();
				}
				else if(comboBox.getSelectedIndex() == 2){
					locale = locales[2];
					updateString();
				}
				else if(comboBox.getSelectedIndex() == 3){
					locale = locales[3];
					updateString();
				}
			}
		});
	}
	public void updateString(){//update string
		res = ResourceBundle.getBundle("Confidential", locale);
		L1.setText(res.getString("Title"));
		L2.setText(res.getString("Language"));
		L4.setText(res.getString("Position"));
		L6.setText(res.getString("Result"));
		repaint();
	}
}
