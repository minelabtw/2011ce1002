package fe.s100502023;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.*;


public class Framework extends JFrame implements ActionListener
{
	private Locale locale = Locale.ENGLISH;
	private Locale [] availableLocale = new Locale[4];
	
	private ResourceBundle res = ResourceBundle.getBundle("Confidential",locale);
	
	private JLabel jl_1 = new JLabel(res.getString("Title"));
	private JLabel jl_2 = new JLabel(res.getString("Language"));
	
	private JLabel jl_4 = new JLabel(res.getString("Position"));
	private JLabel jl_5 = new JLabel();
	private JLabel jl_6 = new JLabel(res.getString("Result"));
	private JLabel jl_7 = new JLabel();
	
	private JComboBox jcbo_language = new JComboBox(); 
	
	
	private JPanel jp_top = new JPanel();
	private JLabel jl_3 = new JLabel();
	private JPanel jp_buttom = new JPanel();
	
	private int mouse_poi_x;
	private int mouse_poi_y;
	
	private DrawBuildings building =  new DrawBuildings();
	
	public Framework()
	{
		set_Locale();
		set_comboBox();
		
		jp_top.setLayout(new GridLayout(1,3));
		
		jp_top.add(jl_1);
		jp_top.add(jl_2);
		jp_top.add(jcbo_language);
		
		jp_buttom.setLayout(new GridLayout(1,4));
		
		jp_buttom.add(jl_4);
		jp_buttom.add(jl_5);
		jp_buttom.add(jl_6);
		jp_buttom.add(jl_7);
		
		setLayout(new BorderLayout(10,10));
		
		add(jp_top,BorderLayout.NORTH);
		add(jl_3,BorderLayout.CENTER);
		add(jp_buttom,BorderLayout.SOUTH);
		
		//label_5 :show mouse position
		jcbo_language.addActionListener(this);
		
		this.addMouseListener(new Mouse_Listener());
		
		jl_5.setText(mouse_poi_x+","+mouse_poi_y);
		
		//label 3
		
		jl_3.setLayout(new BorderLayout(10,10));
		jl_3.add(building,BorderLayout.CENTER);
		
		
		
	}
	
	public void set_Locale()
	{
		availableLocale[0]=Locale.ENGLISH;
		availableLocale[1]=Locale.FRANCE;
		availableLocale[2]=Locale.JAPAN;
		availableLocale[3]=Locale.CHINESE;
	}
	
	public void set_comboBox()
	{
		jcbo_language.addItem("ENGLISH");
		jcbo_language.addItem("FRANCE");
		jcbo_language.addItem("JAPAN");
		jcbo_language.addItem("CHINESE");
	}
	
	public void actionPerformed(ActionEvent e) 
	{
		if (e.getSource()==jcbo_language)
		{
			locale=availableLocale[jcbo_language.getSelectedIndex()];
			
			res = ResourceBundle.getBundle("Confidential",locale);
			
			jl_1.setText(res.getString("Title"));
			jl_2.setText(res.getString("Language"));
			jl_4.setText(res.getString("Position"));
			jl_6.setText(res.getString("Result"));
			
			
			
		}

		
	}
	
	public class Mouse_Listener implements MouseListener
	{

		@Override
		public void mouseClicked(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent e) 
		{
			mouse_poi_x=e.getX();
			mouse_poi_y=e.getY();
			
			jl_5.setText(mouse_poi_x+","+mouse_poi_y);
			
			building.setMousePositionX(mouse_poi_x);
			building.setMousePositionY(mouse_poi_y);
			
			if (building.result==1)
			{
				jl_7.setText("A & B");
			}
			else if(building.result==2) 
			{
				jl_7.setText("A");
			}
			else if(building.result==3)
			{
				jl_7.setText("B");
			}
			else if (building.result==0)
			{
				jl_7.setText("nothing");
			}
			
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
}
