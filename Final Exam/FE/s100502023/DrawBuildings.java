package fe.s100502023;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class DrawBuildings extends JPanel implements ActionListener
{

	private int xBase =0;
	private int yBase;
	private int x_mouse;
	private int y_mouse;
	
	private Timer timer = new Timer(200,this);
	private boolean is_mouse_press=false;
	
	protected int result;
	
	public DrawBuildings()
	{
		timer.start();
	}
	
	public void setMousePositionX(int x_temp)
	{
		x_mouse=x_temp;
		is_mouse_press=true;
	}
	
	public void setMousePositionY(int y_temp)
	{
		y_mouse=y_temp;
		is_mouse_press=true;
	}
	
	public void paintComponent(Graphics g)
	{
	
		super.paintComponent(g);
		
		
		yBase=getHeight()/2;
		getWidth();
		//rectangle
		g.drawRect(xBase+10, yBase, 190, 200);
		g.drawRect(xBase+60, yBase-50, 190, 150);
		
		if (is_mouse_press)
		{
			g.fillOval(x_mouse,y_mouse, 10,10);
		}
		
	}
	
	
	public void check ()
	{
		//a,b
		if (x_mouse>(xBase+60) && x_mouse<xBase+200 && y_mouse>yBase && y_mouse<yBase+100)
		{
			result=1;
		}
		else if (x_mouse>xBase+10 && x_mouse<xBase+200 && y_mouse>yBase &&  y_mouse<yBase+200)//a
		{
			result=2;
		}
		else if (x_mouse>xBase+60 && x_mouse<xBase+250 && y_mouse>yBase-50 &&  y_mouse<yBase+100)//b
		{
			result=3;
		}
		else  //nothing
		{
			result=0;
		}
		
	}
	public void actionPerformed(ActionEvent e) 
	{
		xBase+=5;
		repaint();
		
		//System.out.println("mouse x:"+x_mouse);
		//System.out.println("mouse y:"+y_mouse);
		
		System.out.println(xBase);
		System.out.println(yBase);
		
		check ();
		
	}
	
	
	
	
}
