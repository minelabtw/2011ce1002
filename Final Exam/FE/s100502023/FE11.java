package fe.s100502023;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class FE11 extends JApplet implements ActionListener
{
	//image
	private ImageIcon [] animal_picture = new ImageIcon[9];
	private ImageIcon cat_icon = new ImageIcon(getClass().getResource("image/cat_icon.jpg"));
	private ImageIcon dog_icon = new ImageIcon(getClass().getResource("image/dog_icon.jpg"));
	private ImageIcon mouse_icon = new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));
	
	
	private JToolBar jToolBar = new JToolBar(JToolBar.VERTICAL);
	
	private JMenuBar jMenuBar = new JMenuBar();
	private JMenu jMenu = new JMenu("��ܬ�ï");
	private JMenuItem jmt_cat;
	private JMenuItem jmt_dog;
	private JMenuItem jmt_mouse;
	
	private Action cat_action = new MyAction("cat",cat_icon);
	private Action dog_action = new MyAction("dog",dog_icon);
	private Action mouse_action = new MyAction("mouse",mouse_icon);
	
	private JButton jbt_previous = new JButton("��");//Previous
	private JButton jbt_next = new JButton("��");//Next
	
	
	private JPanel jp_all = new JPanel();
	private JLabel jl_show = new JLabel();
	private JLabel jl_button = new JLabel();
	
	private int pic_index; 
	private int animal_index;
	
	
	public void init()
	{
		setImage_Animal();
		setItem_ToolBarItem();
		
		add(jMenuBar,BorderLayout.NORTH);
		jMenuBar.add(jMenu);
		jMenu.add(jmt_cat);
		jMenu.add(jmt_dog);
		jMenu.add(jmt_mouse);
		
		add(jToolBar,BorderLayout.EAST);
		
		//jpanel
		jl_button.setLayout(new GridLayout(1,2,10,10));
		jl_button.add(jbt_previous);
		jl_button.add(jbt_next);
		
		jp_all.setLayout(new GridLayout(2,1));
		
		jp_all.add(jl_show);
		jp_all.add(jl_button);
		
		add(jp_all);
		addActionlistener_button();
		
	}
	
	public void setImage_Animal()
	{
		for (int i=0;i<3;i++)
		{//cat
			animal_picture[i]=new ImageIcon(getClass().getResource("image/cat"+(i+1)+".jpg"));
		}
		
		for (int i=3;i<6;i++)
		{//dog
			animal_picture[i]=new ImageIcon(getClass().getResource("image/dog"+(i-2)+".jpg"));
		}
		
		for (int i=6;i<9;i++)
		{//mouse
			animal_picture[i]=new ImageIcon(getClass().getResource("image/mouse"+(i-5)+".jpg"));
		}
		
	}
	
	public void setItem_ToolBarItem()
	{
		// menu item
		jmt_cat = new JMenuItem(cat_action);
		jmt_dog = new JMenuItem(dog_action);
		jmt_mouse = new JMenuItem(mouse_action);
		
		//tool
		jToolBar.add(cat_action);
		jToolBar.add(dog_action);
		jToolBar.add(mouse_action);
		
		
		
		
	}
	
	public void addActionlistener_button()
	{
		jbt_previous.addActionListener(this);
		jbt_next.addActionListener(this);
	}
	
	public static void main(String[] args)
	{
		

	}
	
	public void set_pic()
	{
		jl_show.setIcon(animal_picture[pic_index]);
	}
	
	private class MyAction extends AbstractAction
	{
		String kind;
		
		MyAction(String input_kind,Icon icon)
		{
			super(input_kind,icon);
			kind=input_kind;
		}
		public void actionPerformed(ActionEvent arg0)
		{
			if (kind.equals("cat"))
			{
				jl_show.setIcon(animal_picture[0]);
				animal_index=1;
				pic_index=0;
				
			}
			else if (kind.equals("dog"))
			{
				jl_show.setIcon(animal_picture[3]);
				animal_index=2;
				pic_index=3;
			}
			else if (kind.equals("mouse"))
			{
				jl_show.setIcon(animal_picture[6]);
				animal_index=3;
				pic_index=6;
			}
			
			System.out.println("pic"+pic_index);
			System.out.println("animal:"+animal_index);
		}
			
	}

	public void actionPerformed(ActionEvent e) 
	{
		if (e.getSource()==jbt_previous)
		{
			pic_index-=1;
		}
		else if (e.getSource()==jbt_next)
		{
			pic_index+=1;
		}
		
		System.out.println("pic"+pic_index);
		System.out.println("animal:"+animal_index);
		
		if (animal_index==1)//cat
		{
			if (pic_index<0)
			{
				JOptionPane.showMessageDialog(null, "This is the first picture");
				pic_index=0;
			}
			else if (pic_index>2)
			{
				JOptionPane.showMessageDialog(null, "This is the last picture");
				pic_index=2;
			}
			else
			{
				set_pic();
			}
		}
		else if (animal_index==2)//dog
		{
			if (pic_index<3)
			{
				JOptionPane.showMessageDialog(null, "This is the first picture");
				pic_index=3;
			}
			else if (pic_index>5)
			{
				JOptionPane.showMessageDialog(null, "This is the last picture");
				pic_index=5;
			}
			else
			{
				set_pic();
			}
		}
		else if (animal_index==3)//mouse
		{
			if (pic_index<6)
			{
				JOptionPane.showMessageDialog(null, "This is the first picture");
				pic_index=6;
			}
			else if (pic_index>8)
			{
				JOptionPane.showMessageDialog(null, "This is the last picture");
				pic_index=8;
			}
			else
			{
				set_pic();
			}
		}
	}
	
}
