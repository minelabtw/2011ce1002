package fe.s100502014;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.Timer;
public class Framework extends JFrame implements ActionListener {
	private Timer timer = new Timer(200, this);
	private DrawBuildings drawBuildings = new DrawBuildings();
	
	/*private Locale[] availableLocales = Locale.getAvailableLocales();
	
	private Locale locale = Locale.US;
	private String timeZone = TimeZone.getDefault().getID();
	
	private ResourceBundle res = ResourceBundle.getBundle("Confidential",locale);

	private JLabel jlbTitle = new JLabel(res.getString("Title"));
	private JLabel jlbLanguage = new JLabel(res.getString("language"));
	private JLabel jlbPosition = new JLabel(res.getString("Position"));
	private JLabel jlbResult = new JLabel(res.getString("Result"));

	private JComboBox jcboLocale = new JComboBox();
	
	private void updateStrings() {
		res = ResourceBundle.getBundle("Confidential",locale);
		jlbLanguage.setText(res.getString("language"));
		jlbPosition.setText(res.getString("Position"));
		jlbResult.setText(res.getString("Result"));
	}*/
	
	public Framework() {
		/*for(int i=0;i<availableLocales.length;i++)
			jcboLocale.addItem(availableLocales[i].getDisplayName());
		
		JPanel p1 = new JPanel(new GridLayout(1, 2));
		
		JPanel p1_1 = new JPanel();
		p1_1.add(jlbTitle);

		JPanel p1_2 = new JPanel(new GridLayout(1, 2));
	    JPanel p1_2_1 = new JPanel(new BorderLayout());
	    JPanel p1_2_2 = new JPanel(new BorderLayout());
	    p1_2_1.add(jlbLanguage, BorderLayout.WEST);
	    p1_2_2.add(jcboLocale, BorderLayout.CENTER);
	    p1_2.add(p1_2_1);
	    p1_2.add(p1_2_2);
	    p1.add(p1_1);
	    p1.add(p1_2);
	    add(p1, BorderLayout.NORTH);*/
		add(drawBuildings);
		
		timer.start();
	}
	public void actionPerformed(ActionEvent e) {
		int temp = drawBuildings.getX();
		temp += 10;
		temp %= 600;
		drawBuildings.setX(temp);
		drawBuildings.repaint();
	}
	

}
