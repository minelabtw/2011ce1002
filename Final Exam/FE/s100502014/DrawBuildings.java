package fe.s100502014;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;
public class DrawBuildings extends JPanel {
	private int xBase;
	private int yBase;
	private int MouseX;
	private int MouseY;
	public DrawBuildings() {
		xBase = 0;
		yBase = 100;
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		//A
		g.setColor(Color.BLUE);
		g.drawRect(xBase+10, yBase, 190, 200);
		
		//B
		g.setColor(Color.GREEN);
		g.drawRect(xBase+60, yBase-50, 190, 150);
	}
	public void setX(int x) {
		xBase = x;
	}
	public int getX() {
		return xBase;
	}
	public int getY() {
		return yBase;
	}
	public void setMousePositionX(int x) {
		MouseX = x;
	}
	public void setMousePositionY(int y) {
		MouseY = y;
	}
	
}
