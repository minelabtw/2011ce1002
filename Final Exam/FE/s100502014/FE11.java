package fe.s100502014;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FE11 extends JApplet {
	//image
	private ImageIcon[] catPic = new ImageIcon[3];
	private ImageIcon[] dogPic = new ImageIcon[3];
	private ImageIcon[] mousePic = new ImageIcon[3];
	{
		for(int i=0;i<3;i++) {
			catPic[i] = new ImageIcon(getClass().getResource("image/cat"+(i+1)+".jpg"));
			dogPic[i] = new ImageIcon(getClass().getResource("image/dog"+(i+1)+".jpg"));
			mousePic[i] = new ImageIcon(getClass().getResource("image/mouse"+(i+1)+".jpg"));
		}
	}
	private ImageIcon catIcon = new ImageIcon(getClass().getResource("image/cat_icon.jpg"));
	private ImageIcon dogIcon = new ImageIcon(getClass().getResource("image/dog_icon.jpg"));
	private ImageIcon mouseIcon = new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));
	
	//toolbar
	private JToolBar jtb = new JToolBar();
	private JButton jbtCat = new JButton(catIcon);
	private JButton jbtDog = new JButton(dogIcon);
	private JButton jbtMouse = new JButton(mouseIcon);
	
	//menu
	private JMenu jmn = new JMenu("Animals Gallery");
	private JMenuBar jmb = new JMenuBar();
	private JMenuItem jmiCat = new JMenuItem(catIcon);
	private JMenuItem jmiDog = new JMenuItem(dogIcon);
	private JMenuItem jmiMouse = new JMenuItem(mouseIcon);
	
	//button
	private JButton left = new JButton("��");
	private JButton right = new JButton("��");
	
	//label and panel
	private JLabel jlbGal = new JLabel();
	private JPanel jplBut = new JPanel();
	
	//int
	private int page;
	private int type;
	
	public FE11() {
		
		//action
		Action catAction = new MyAction("Cat", catIcon);
		Action dogAction = new MyAction("Dog", dogIcon);
		Action mouseAction = new MyAction("Mouse", mouseIcon);
		
		//menu
		jmn.add(catAction);
		jmn.add(dogAction);
		jmn.add(mouseAction);
		jmb.add(jmn);
		
		//toolbar
		jtb.setLayout(new GridLayout(3,1));
		jtb.add(catAction);
		jtb.add(dogAction);
		jtb.add(mouseAction);
		
		//button
		jplBut.add(left);
		jplBut.add(right);
		left.addActionListener(new ButtonListener());
		right.addActionListener(new ButtonListener());
		
		add(jmb, BorderLayout.NORTH);
		add(jtb, BorderLayout.EAST);
		add(jlbGal, BorderLayout.CENTER);
		add(jplBut,BorderLayout.SOUTH);
		
	}
	
	private class MyAction extends AbstractAction {
		String name;
		MyAction(String name, Icon icon) {
			super(name, icon);
			this.name = name;
	    }
	    public void actionPerformed(ActionEvent e) {
	    	page = 0;
	    	if (name.equals("Cat")){
	    		//setIcon
	    		jlbGal.setIcon(catPic[page]);
	    		type = 1;
	    	}
	    	else if (name.equals("Dog")){
	    		//setIcon
	    		jlbGal.setIcon(dogPic[page]);
	    		type = 2;
	    	}
	    	else if (name.equals("Mouse")){
	    		//setIcon
	    		jlbGal.setIcon(mousePic[page]);
	    		type = 3;
	    	}
	    }
	}
	
	private class ButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == left) {
	    		if(page!=0) {
	    			page--;
	    			if(type==1) {	//cat
	    				jlbGal.setIcon(catPic[page]);
	    			}
	    			else if(type==2) {	//dog
	    				jlbGal.setIcon(dogPic[page]);
	    			}
	    			else {	//mouse
	    				jlbGal.setIcon(mousePic[page]);
	    			}
	    		}
	    		else {	//the first page
	    			JOptionPane.showMessageDialog(null, "This is the first page!!");
	    		}
	    	}
	    	if(e.getSource() == right) {
	    		if(page!=2) {
	    			page++;
	    			if(type==1) {	//cat
	    				jlbGal.setIcon(catPic[page]);
	    			}
	    			else if(type==2) {	//dog
	    				jlbGal.setIcon(dogPic[page]);
	    			}
	    			else {	//mouse
	    				jlbGal.setIcon(mousePic[page]);
	    			}
	    		}
	    		else {	//the last page
	    			JOptionPane.showMessageDialog(null, "This is the last page!!");
	    		}
	    	}
		}
	}
}
