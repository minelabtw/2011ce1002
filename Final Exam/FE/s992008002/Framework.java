package fe.s992008002;

import java.awt.*;

import javax.swing.*;
import java.awt.event.*;
import java.util.Locale;
import java.util.ResourceBundle;


public class Framework extends JFrame implements ActionListener{

	private Locale[] availableLocales = Locale.getAvailableLocales();

	private Locale locale = Locale.US;
	private ResourceBundle res = ResourceBundle.getBundle("Confidential", locale);

	//語言選擇區
	private JComboBox languageBox = new JComboBox();
	private JLabel title= new JLabel(res.getString("The Bomber Infrared Screen"));
	private JLabel language = new JLabel(res.getString("Language"));	
	private JLabel position = new JLabel(res.getString("Position"));
	private JLabel result = new JLabel(res.getString("Result"));
	
	private JLabel currentPositon = new JLabel();
	private JLabel currentresult= new JLabel();
	
	private JLabel paneLabel = new JLabel();
		//改變語言
	private void updateStrings(){
		res = ResourceBundle.getBundle("", locale);
		title.setText(res.getString("The Bomber Infrared Screen"));
		language.setText(res.getString("Language"));
		position.setText(res.getString("Position"));
		result.setText(res.getString("Result"));
	}
	
    public Framework(){
	
    	for(int i = 0; i<availableLocales.length; i++){
    		languageBox.addItem(availableLocales[i].getDisplayName());
    	}
    	
    	JPanel uptownJPanel = new JPanel(new GridLayout(1,3));
    	JPanel midtownJPanel = new JPanel(new GridLayout(1,1));
    	JPanel downtownJPanel = new JPanel(new GridLayout(1,4));
    	//範例JLabel_1 JLabel2 JComboBox
    	uptownJPanel.add(title);
    	uptownJPanel.add(language);
    	uptownJPanel.add(languageBox);
    	//JLabel_3
    	midtownJPanel.add(paneLabel);
    	//JLabel_4 JLabel_5 JLabel_6 JLabel7
    	downtownJPanel.add(position);
    	downtownJPanel.add(currentPositon);
    	downtownJPanel.add(result);
    	downtownJPanel.add(currentresult);
    	
    	this.setLayout(new GridLayout(3,1));
    	this.add(uptownJPanel);
    	this.add(midtownJPanel);
    	this.add(downtownJPanel);
    	
    	languageBox.addActionListener(this);
	}
	
    public void actionPerformed(ActionEvent e){
    	
    	if(e.getSource()==languageBox){
    		locale = availableLocales[languageBox.getSelectedIndex()];
    		updateStrings();
    		}
    }
    


  }
    

