package fe.s992008002;

import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;
import java.security.MessageDigest;

import javax.swing.*;

public class FE11 extends JApplet {
	
	
	// create icon image
	private ImageIcon cat_icon = new ImageIcon("image/cat_icon.jpg");
	private ImageIcon dog_icon = new ImageIcon("image/dog_icon.jpg");
	private ImageIcon mouse_icon = new ImageIcon("image/mouse_icon.jpg");
	
	private int index = 0;
	private ImageIcon[] imageObjects = {
			new ImageIcon("image/cat1.jpg"),
			new ImageIcon("image/cat2.jpg"),
			new ImageIcon("image/cat3.jpg"),
			new ImageIcon("image/dog1.jpg"),
			new ImageIcon("image/dog2.jpg"),
			new ImageIcon("image/dog3.jpg"),
			new ImageIcon("image/mouse1.jpg"),
			new ImageIcon("image/mouse2.jpg"),
			new ImageIcon("image/mouse3.jpg")
	};
	
	
	private JButton leftButton = new JButton("��");
	private JButton rightButton = new JButton("��");
	private JPanel buttonPanel = new JPanel();
	

	//private JLabel Label_icon = new JLabel();
	private JLabel Label_img = new JLabel();

	public FE11(){
		
		//Actions
		Action cat_action = new MyAction("Cat", cat_icon);
		Action dog_action = new MyAction("Dog", dog_icon);
		Action mouse_action = new MyAction("Mouse", mouse_icon);

		//Menu
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("Galleries");
		setJMenuBar(menuBar);
		menuBar.add(menu);
		
		menu.add(cat_action);
		menu.add(dog_action);
		menu.add(mouse_action);
	
		//toolbar
		JToolBar toolBar = new JToolBar(JToolBar.VERTICAL);
		toolBar.setBorder(BorderFactory.createLineBorder(Color.blue));
		
		toolBar.add(cat_action);
		toolBar.add(dog_action);
		toolBar.add(mouse_action);
		
		//Buttons 
		buttonPanel.add(leftButton);
		buttonPanel.add(rightButton);
		
		leftButton.addActionListener(
				new ActionListener(){
					public void actionPerformed(ActionEvent e){	
						index--;
						if(index<0){
						//	JOptionPane.showMessageDialog(,"This is the last picture!!","Alert");
						System.out.println("This is the last picture!!");
						}
						Label_img.setIcon(imageObjects[index]);
					}
				}
	);
		
		rightButton.addActionListener(
				new ActionListener(){
					public void actionPerformed(ActionEvent e){
						index++;
						if(index>8){
						//JOptionPane.showMessageDialog(0,"This is the last picture!!");
							System.out.println("This is the first picture!!");
						}
						Label_img.setIcon(imageObjects[index]);
					}
				}
	);
		add(toolBar, BorderLayout.EAST);
		add(Label_img, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
	
	} 


	
	// Implement AbstractAction
	private class MyAction extends AbstractAction {
		String name;

		MyAction(String name, Icon icon) {
			super(name, icon);
			putValue(Action.SHORT_DESCRIPTION, " Select the " + name + " galleries");
			this.name = name;
	    } 

		
		//actionListener of MyAction
	    public void actionPerformed(ActionEvent e) {
	    	if (name.equals("Cat")){
	    		//this.icon = imageObjects[index];
	    		Label_img.setIcon(imageObjects[1]);
	    	}
	    	else if(name.equals("Dog")){
	    		Label_img.setIcon(imageObjects[4]);
	    	}
	    	else if(name.equals("Mouse")){	    		
	    		Label_img.setIcon(imageObjects[7]);
	    	}
	    
	    }
	}

}
