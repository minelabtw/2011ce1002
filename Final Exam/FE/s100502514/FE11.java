/** Final Exam 2012.06.21 Question 1 */
//TODO JMenuBar isn't shown.
package fe.s100502514;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class FE11 extends JApplet implements SelectActionTarget {
	/** UID */
	private static final long serialVersionUID = 1L;
	
	/** Two-dimensional array for three pictures for each of three animals */
	private ImageIcon[][] galleries = new ImageIcon
			[SelectAction.imageURLPrefixes.length][SelectAction.imageURLSuffixes.length];
	private int prefix = -1, suffix = -1;
	
	/** GUI Members */
	//JPanel mainPanel = new JPanel();
	
	ImageIcon showingImage = null;
	
	JMenuBar bar = new JMenuBar();
	
	JToolBar myToolBar = new JToolBar(JToolBar.VERTICAL);
	JMenu myMenu = new JMenu();
	JPanel displayPanel = new JPanel(){
		/** UID */
		private static final long serialVersionUID = 1L;

		@Override
		protected void paintComponent(Graphics g){
			super.paintComponent(g);
			try{
				g.drawImage(showingImage.getImage(), 0, 0, this);
			}catch(Exception err){
				/* Do not draw if error occurs. (ex: showingIcon==null) */
			}
		}
	};
	
	@Override
	public void init(){
		//Layout init
		setLayout(new BorderLayout());
		
		bar.add(myMenu);
		setJMenuBar(bar); //TODO to be modified
		add(myToolBar, BorderLayout.EAST);
		
		add(displayPanel, BorderLayout.CENTER);
		add(new SwitchButtonPanel(), BorderLayout.SOUTH);
		
		for(int i=0; i<SelectAction.imageURLPrefixes.length; i++){
			SelectAction action = new SelectAction(i, this);
			myMenu.add(action);
			myToolBar.add(action);
		}
		
		//Icon init
		for(int i=0; i<galleries.length; i++){
			for(int j=0; j<galleries[i].length; j++){
				galleries[i][j] = new ImageIcon(
						getClass().getResource(SelectAction.getNewImageURLString(i, j))
				);
			}
		}
		
	}

	@Override
	public void runSelectAction(SelectAction action) {
		showingImage = galleries[prefix = action.getImagePrefix()][suffix = 0];
		repaint();
	}
	
	//inner class for prev/next buttons
	class SwitchButtonPanel extends JPanel implements ActionListener{

		/** UID */
		private static final long serialVersionUID = 1L;
		
		JButton prev = new JButton("PREV");
		JButton next = new JButton("NEXT");
		
		public SwitchButtonPanel(){
			setLayout(new FlowLayout(FlowLayout.CENTER));
			add(prev);
			add(next);
			prev.addActionListener(this);
			next.addActionListener(this);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			int incr = 0;
			if(e.getSource() == prev){
				incr = -1;
			}else if(e.getSource() == next){
				incr = 1;
			}
			try{
				showingImage = galleries[prefix][suffix+incr];
				suffix += incr;
			}catch(ArrayIndexOutOfBoundsException err){
				if(suffix<0){
					JOptionPane.showMessageDialog(null, "This is no image chosen.");
				}else if(suffix==0){
					JOptionPane.showMessageDialog(null, "This is the first image.");
				}else{
					JOptionPane.showMessageDialog(null, "This is the last image.");
				}
			}
			displayPanel.repaint();
		}
		
	}
	
}
