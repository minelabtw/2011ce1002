/** This class inherits a subclass from interface Action. */
package fe.s100502514;

import java.awt.event.*;

import javax.swing.*;

class SelectAction extends AbstractAction {

	/** UID */
	private static final long serialVersionUID = 1L;
	
	//Icon names, URLs and so on
	static String[] imageURLPrefixes = {
		"cat", "dog", "mouse"
	};
	static String iconSuffix = "_icon.jpg";
	static String[] imageURLSuffixes = {
		"1.jpg", "2.jpg", "3.jpg"
	};
	
	private int imagePrefix;
	public int getImagePrefix(){
		return imagePrefix;
	}
	
	public static String getNewImageURLString(int prefix, int suffix){
		return "image/"+imageURLPrefixes[prefix]+imageURLSuffixes[suffix];
	}
	
	//Listener Declaration
	private SelectActionTarget target;
	
	/** Constructors of the Action class
	 *  prefix: 0(cat) 1(dog) 2(mouse)
	 *  suffix for icons
	 */
	public SelectAction(int imagePrefix, SelectActionTarget target){
		super(imageURLPrefixes[imagePrefix],
				new ImageIcon(SelectAction.class.getResource(
						"image/"+imageURLPrefixes[imagePrefix]+iconSuffix)));
		this.imagePrefix = imagePrefix;
		this.target = target;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(target!=null){
			target.runSelectAction(this);
		}
	}
}
