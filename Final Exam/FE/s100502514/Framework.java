/** Final Exam Question 2 Framework */
package fe.s100502514;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;

public class Framework extends JFrame {

	/** UID */
	private static final long serialVersionUID = 1L;
	
	ResourceBundle bundle = ResourceBundle.getBundle("Confidential");
	
	FELabel titleLabel = new FELabel("Title"); //Label 1
	FELabel languageLabel = new FELabel("Language"); //Label 2
	LanguageComboBox languageChoice = new LanguageComboBox(); //ComboBox
	DrawBuildings displayBuildings = new DrawBuildings(); //Label 3 (Actually a Panel)
	FELabel positionLabel = new FELabel("Position"); //Label 4, 5 combination
	FELabel resultLabel = new FELabel("Result"); //Label 6, 7 combination
	
	Locale currentLocale = getLocale();
	
	public Framework(){
		/* Layout settings */
		setLayout(new BorderLayout());
		add(new UpperPanel(), BorderLayout.NORTH);
		add(displayBuildings, BorderLayout.CENTER);
		add(new LowerPanel(), BorderLayout.SOUTH);
		
	}
	
	/** Inner class for upper part: title/language selecting */
	class UpperPanel extends JPanel{

		/** UID */
		private static final long serialVersionUID = 1L;
		
		public UpperPanel(){
			setLayout(new GridLayout(1, 3));
			add(titleLabel);
			add(languageLabel);
			add(languageChoice);
		}
		
	}
	
	/** Inner class for lower part: position/result displaying */
	class LowerPanel extends JPanel{

		/** UID */
		private static final long serialVersionUID = 1L;
		
		public LowerPanel(){
			setLayout(new GridLayout(1, 2));
			add(positionLabel);
			add(resultLabel);
		}
		
	}
	
	/** Inner class for language selecting combo box */
	class LanguageComboBox extends JComboBox<String>{

		/** UID */
		private static final long serialVersionUID = 1L;
		
		private Locale[] locales = {Locale.CHINESE, Locale.FRENCH, Locale.JAPANESE, Locale.ENGLISH};
		
		public LanguageComboBox(){
			for(int i=0; i<locales.length; i++){
				addItem(locales[i].getDisplayName());
			}
			addItemListener(new ItemListener(){

				@Override
				public void itemStateChanged(ItemEvent e) {
					// TODO Auto-generated method stub
					currentLocale = locales[getSelectedIndex()];
				}
				
			});
		}
		
	}
	
	/** Inner class for all language-changeable labels */
	class FELabel extends JLabel{

		/** UID */
		private static final long serialVersionUID = 1L;
		
		String varname;
		String addition;
		
		public FELabel(String varname, String addition){
			this.varname = varname;
			this.addition = addition;
		}
		public FELabel(String varname){
			this(varname, "");
		}
		
		protected void paintComponent(Graphics g){
			setText(bundle.getString(varname)+addition);
			super.paintComponent(g);
		}
		
	}
	
	//Offer the Framework reference for inner classes.
	private Framework getThisFrame(){
		return this;
	}
	
}
