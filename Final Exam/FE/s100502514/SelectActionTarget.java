package fe.s100502514;

public interface SelectActionTarget {
	public void runSelectAction(SelectAction action);
}
