/** Final Exam Question 2 class DrawBuildings */
package fe.s100502514;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class DrawBuildings extends JPanel implements ActionListener {

	/** UID */
	private static final long serialVersionUID = 1L;
	
	int xBase = 0;
	int yBase = getHeight()/2;
	
	final int a_left=10, a_right=200, a_top=0, a_bottom=200;
	final int b_left=60, b_right=250, b_top=-50, b_bottom=100;
	
	Timer timer;
	
	public DrawBuildings(){
		timer = new Timer(200, this);
		timer.start();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// Move rightward
		xBase = (xBase+20)%getWidth();
		repaint();
	}
	
	public boolean isHitA(){
		Point p = getMousePosition();
		return p.x >= a_left && p.x <= a_right && p.y >= a_top && p.y <= a_bottom;
	}
	public boolean isHitB(){
		Point p = getMousePosition();
		return p.x >= b_left && p.x <= b_right && p.y >= b_top && p.y <= b_bottom;
	}
	
	@Override
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		//Draw A
		g.setColor(Color.red);
		g.drawRect(xBase+a_left, yBase+a_top, a_right-a_left, a_bottom-a_top);
		g.drawString("A", xBase+a_left, a_bottom);
		//Draw B
		g.setColor(Color.blue);
		g.drawRect(xBase+b_left, yBase+b_top, b_right-b_left, b_bottom-b_top);
		g.drawString("B", xBase+b_left, b_bottom);
	}

}
