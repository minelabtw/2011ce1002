package fe.s995002201;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;

public class FE11 extends Applet implements ActionListener
{
	private int[] catpic = new int[2];
	ImageIcon caticon1 = new ImageIcon(getClass().getResource("image/cat1.jpg"));//宣告圖片
	ImageIcon caticon2 = new ImageIcon(getClass().getResource("image/cat2.jpg"));
	ImageIcon caticon3 = new ImageIcon(getClass().getResource("image/cat3.jpg"));
	
	ImageIcon dogicon4 = new ImageIcon(getClass().getResource("image/dog1.jpg"));
	ImageIcon dogicon5 = new ImageIcon(getClass().getResource("image/dog2.jpg"));
	ImageIcon dogicon6 = new ImageIcon(getClass().getResource("image/dog3.jpg"));
	
	ImageIcon mouseicon7 = new ImageIcon(getClass().getResource("image/mouse1.jpg"));
	ImageIcon mouseicon8 = new ImageIcon(getClass().getResource("image/mouse2.jpg"));
	ImageIcon mouseicon9 = new ImageIcon(getClass().getResource("image/mouse3.jpg"));
	
	ImageIcon icon10 = new ImageIcon(getClass().getResource("image/cat_icon.jpg"));
	ImageIcon icon11 = new ImageIcon(getClass().getResource("image/dog_icon.jpg"));
	ImageIcon icon12 = new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));
	
	private JMenuItem cat,dog,mouse;//宣告Jmenu
	private JButton jbtcat = new JButton(icon10);//宣告旁邊的Jtoolbar
	private JButton jbtdog = new JButton(icon11);
	private JButton jbtmouse = new JButton(icon12);
	
	private JButton left = new JButton("←");
	private JButton right = new JButton("→");
	
	private JLabel pic = new JLabel("",JLabel.CENTER);
	
	public FE11()
	{
		JMenuBar jmb = new JMenuBar();
		//setJMenuBar(jmb);
		JMenu operationMenu = new JMenu("Animal Gallery");//JMenu的東西
		operationMenu.setMnemonic('0');
		jmb.add(operationMenu);
		operationMenu.add(cat = new JMenuItem("Cat"));
		operationMenu.add(dog = new JMenuItem("Dog"));
		operationMenu.add(mouse = new JMenuItem("Mouse"));
		
		JToolBar jToolBar1 = new JToolBar("my toolbar");//Jtoolbar的東西
		jToolBar1.setFloatable(true);
		jToolBar1.add(jbtcat);
		jToolBar1.add(jbtdog);
		jToolBar1.add(jbtmouse);
		
		jbtcat.setToolTipText("Cat");
		jbtdog.setToolTipText("Dog");
		jbtmouse.setToolTipText("Mouse");
		jbtcat.setBorderPainted(false);
		jbtdog.setBorderPainted(false);
		jbtmouse.setBorderPainted(false);
		
		left.addActionListener(
				//left Actionlistener事件處理
				new ActionListener(){
					public void actionPerformed(ActionEvent e)
					{
						if(pic.getIcon()==caticon1)
						{
							JOptionPane.showMessageDialog(null, "This is the first picture");
						}
						if(pic.getIcon()==caticon2)
						{
							pic.setIcon(caticon1);
						}
						if(pic.getIcon()==caticon3)
						{
							pic.setIcon(caticon2);
						}
						//
						if(pic.getIcon()==dogicon4)
						{
							JOptionPane.showMessageDialog(null, "This is the first picture");
						}
						if(pic.getIcon()==dogicon5)
						{
							pic.setIcon(dogicon4);
						}
						if(pic.getIcon()==dogicon6)
						{
							pic.setIcon(dogicon5);
						}
						//
						if(pic.getIcon()==mouseicon7)
						{
							JOptionPane.showMessageDialog(null, "This is the first picture");
						}
						if(pic.getIcon()==mouseicon8)
						{
							pic.setIcon(mouseicon7);
						}
						if(pic.getIcon()==mouseicon9)
						{
							pic.setIcon(mouseicon8);
						}
					}
				}
		);
		right.addActionListener(
				//right Actionlistener事件處理
				new ActionListener(){
					public void actionPerformed(ActionEvent e)
					{
						if(pic.getIcon()==caticon3)
						{
							JOptionPane.showMessageDialog(null, "This is the last picture");
						}
						if(pic.getIcon()==caticon2)
						{
							pic.setIcon(caticon3);
						}
						if(pic.getIcon()==caticon1)
						{
							pic.setIcon(caticon2);
						}
						//
						if(pic.getIcon()==dogicon6)
						{
							JOptionPane.showMessageDialog(null, "This is the last picture");
						}
						if(pic.getIcon()==dogicon5)
						{
							pic.setIcon(dogicon6);
						}
						if(pic.getIcon()==dogicon4)
						{
							pic.setIcon(dogicon5);
						}
						//
						if(pic.getIcon()==mouseicon9)
						{
							JOptionPane.showMessageDialog(null, "This is the last picture");
						}
						if(pic.getIcon()==mouseicon8)
						{
							pic.setIcon(mouseicon9);
						}
						if(pic.getIcon()==mouseicon7)
						{
							pic.setIcon(mouseicon8);
						}
					}
				}
		);
		cat.addActionListener(
				//cat Actionlistener事件處理
				new ActionListener(){
					public void actionPerformed(ActionEvent e)
					{
						pic.setIcon(caticon1);
					}
				}
		);
		jbtcat.addActionListener(
				new ActionListener(){
					public void actionPerformed(ActionEvent e)
					{
						pic.setIcon(caticon1);
					}
				}
		);
		dog.addActionListener(
				//dog Actionlistener事件處理
				new ActionListener(){
					public void actionPerformed(ActionEvent e)
					{
						pic.setIcon(dogicon4);
					}
				}
		);
		jbtdog.addActionListener(
				new ActionListener(){
					public void actionPerformed(ActionEvent e)
					{
						pic.setIcon(dogicon4);
					}
				}
		);
		mouse.addActionListener(
				//mouse Actionlistener事件處理
				new ActionListener(){
					public void actionPerformed(ActionEvent e)
					{
						pic.setIcon(mouseicon7);
					}
				}
		);
		jbtmouse.addActionListener(
				new ActionListener(){
					public void actionPerformed(ActionEvent e)
					{
						pic.setIcon(mouseicon7);
					}
				}
		);
		//排版
		JPanel p1 = new JPanel(new FlowLayout());
		p1.add(left);
		p1.add(right);
		setLayout(new BorderLayout());
		add(p1,BorderLayout.SOUTH);
		add(jmb,BorderLayout.NORTH);
		add(jToolBar1,BorderLayout.EAST);
		add(pic,java.awt.BorderLayout.CENTER);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		// TODO Auto-generated method stub
		
	}
}