package fe.s100502016;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.Timer;

public class DrawBuildings extends JPanel implements ActionListener {
	int lengthA;
	int widthA;
	int lengthB;
	int widthB;
	int xBase;
	int yBase;
	int x;
	int y;
	String result = "";
	Timer timer = new Timer(200, this);

	public DrawBuildings() {
		timer.start();
		xBase = 0;
		yBase = 300;
	}

	public void setMousePointX(int x_temp) {
		x = x_temp;
	}

	public void setMousePointY(int y_temp) {
		y = y_temp;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		xBase += 1;
		repaint();
		if (xBase >= 600) {
			xBase = 0;
		}
	}

	public String getResult() {
		return result;
	}

	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);

		g.drawRect(xBase + 10, yBase, 190, 200);
		g.drawString("A",xBase,yBase+190);
		g.drawRect(xBase + 60, yBase - 50, 190, 150);
		g.drawString("B",xBase+50,yBase+120);
		g.setColor(Color.red);
		g.fillRect(x-5, y-55, 5, 5);

		if (x <= xBase + 200 && x >= xBase + 10 && y -48>= yBase
				&& y-48<= yBase + 200 && x <= xBase + 250 && x >= xBase + 60
				&& y-48 >= yBase - 50 && y-48 <= yBase + 100) {
			result = "Hit Both buildings A and B";

		} else if (x <= xBase + 200 && x >= xBase + 10 && y-48 >= yBase
				&& y-48 <= yBase + 200) {
			result = "Hit building A ";
	
		} else if (x <= xBase + 250 && x >= xBase + 60 && y-48 >= yBase - 50
				&& y-48 <= yBase + 100) {
			result = "Hit building B";
	
		} else {
			result = "Miss";
		}

	}

}
