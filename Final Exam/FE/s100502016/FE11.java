package fe.s100502016;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;

public class FE11 extends JApplet {
	private ImageIcon imgIconCat = new ImageIcon(getClass().getResource(
			"image/cat_icon.jpg"));
	private ImageIcon imgIconDog = new ImageIcon(getClass().getResource(
			"image/dog_icon.jpg"));
	private ImageIcon imgIconMouse = new ImageIcon(getClass().getResource(
			"image/mouse_icon.jpg"));
	private ImageIcon[] imgAnimal = {
			new ImageIcon(getClass().getResource("image/cat1.jpg")),
			new ImageIcon(getClass().getResource("image/cat2.jpg")),
			new ImageIcon(getClass().getResource("image/cat3.jpg")),
			new ImageIcon(getClass().getResource("image/dog1.jpg")),
			new ImageIcon(getClass().getResource("image/dog2.jpg")),
			new ImageIcon(getClass().getResource("image/dog3.jpg")),
			new ImageIcon(getClass().getResource("image/mouse1.jpg")),
			new ImageIcon(getClass().getResource("image/mouse2.jpg")),
			new ImageIcon(getClass().getResource("image/mouse3.jpg")) };

	private Action catAction = new MyAction("cat", imgIconCat);
	private Action dogAction = new MyAction("dog", imgIconDog);
	private Action mouseAction = new MyAction("mouse", imgIconMouse);
	private Action previousAction = new MyAction("previous", imgIconCat);
	private Action nextAction = new MyAction("next", imgIconDog);

	private JMenuBar jmb = new JMenuBar();
	private JMenu picMenu = new JMenu("Picture");
	private JToolBar jtb = new JToolBar();
	private JLabel lbl = new JLabel();

	private JPanel p = new JPanel();
	private JButton btnPrevious = new JButton(previousAction);
	private JButton btnNext = new JButton(nextAction);

	private JButton btnCat = new JButton(catAction);
	private JButton btnDog = new JButton(dogAction);
	private JButton btnMouse = new JButton(mouseAction);

	private int currentType = 0;
	private int currentPIC = 0;

	public FE11() {

		jmb.add(picMenu);
		picMenu.add(catAction);
		picMenu.add(dogAction);
		picMenu.add(mouseAction);

		jtb.add(btnCat);
		jtb.add(btnDog);
		jtb.add(btnMouse);

		setLayout(new BorderLayout());
		add(jmb, BorderLayout.NORTH);
		add(jtb, BorderLayout.EAST);
		add(lbl, BorderLayout.CENTER);
		p.setLayout(new GridLayout(1, 2));
		p.add(btnPrevious);
		p.add(btnNext);
		add(p, BorderLayout.SOUTH);

	}

	private class MyAction extends AbstractAction {
		String name;

		MyAction(String name, Icon icon) {
			// TODO Auto-generated constructor stub
			super(name, icon);
			this.name = name;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (name.equals("cat")) {
				currentType = 0;
				currentPIC = 0;
				lbl.setIcon(imgAnimal[currentType * 3]);
			} else if (name.equals("dog")) {
				currentType = 1;
				currentPIC = 0;
				lbl.setIcon(imgAnimal[currentType * 3]);
			} else if (name.equals("mouse")) {
				currentType = 2;
				currentPIC = 0;
				lbl.setIcon(imgAnimal[currentType * 3]);
			} else if (name.equals("previous")) {
				if (currentPIC == 0) {
					JOptionPane.showMessageDialog(null, "First pucture !!");
				} else {
					currentPIC--;
					lbl.setIcon(imgAnimal[currentType * 3 + currentPIC]);
				}
			} else if (name.equals("next")) {
				if (currentPIC == 2) {
					JOptionPane.showMessageDialog(null, "Last pucture !!");
				} else {
					currentPIC++;
					lbl.setIcon(imgAnimal[currentType * 3 + currentPIC]);
				}
			}
		}
	}

}
