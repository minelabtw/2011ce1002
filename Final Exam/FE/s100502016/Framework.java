package fe.s100502016;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Framework extends JFrame implements ActionListener, MouseListener,
		MouseMotionListener {
	Locale locale = Locale.getDefault();
	Locale[] locales = { locale.ENGLISH, locale.FRANCE, locale.CHINESE,
			locale.JAPANESE };
	ResourceBundle res = ResourceBundle.getBundle("Confidential", locale);

	DrawBuildings drawBuildings = new DrawBuildings();
	JLabel lbl_1 = new JLabel(res.getString("Title"));
	JLabel lbl_2 = new JLabel(res.getString("Language"));
	JLabel lbl_3 = new JLabel();
	JLabel lbl_4 = new JLabel(res.getString("Position"));
	JLabel lbl_5 = new JLabel();
	JLabel lbl_6 = new JLabel(res.getString("Result"));
	JLabel lbl_7 = new JLabel();

	int bombX;
	int bombY;

	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();

	JComboBox jcbCountry = new JComboBox();

	public Framework() {
		// TODO Auto-generated constructor stub
		addMouseListener(this);
		setLayout(new BorderLayout());
		p1.setLayout(new GridLayout(1, 3));
		p1.add(lbl_1);
		p1.add(lbl_2);
		p1.add(jcbCountry);
		for (int i = 0; i < 4; i++)
			jcbCountry.addItem(locales[i].getDisplayName());
		jcbCountry.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				locale = locales[jcbCountry.getSelectedIndex()];
				updateString();
			}
		});

		add(p1, BorderLayout.NORTH);
		add(drawBuildings, BorderLayout.CENTER);
		p2.setLayout(new GridLayout(1, 4));
		p2.add(lbl_4);
		p2.add(lbl_5);
		p2.add(lbl_6);
		p2.add(lbl_7);
		add(p2, BorderLayout.SOUTH);

	}

	private void updateString() {
		res = ResourceBundle.getBundle("Confidential", locale);
		lbl_1.setText(res.getString("Title"));
		lbl_2.setText(res.getString("Language"));
		lbl_4.setText(res.getString("Position"));
		lbl_6.setText(res.getString("Result"));

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		bombX = e.getX();
		bombY = e.getY();
		lbl_5.setText(bombX + "," + bombY);
		drawBuildings.setMousePointX(bombX);
		drawBuildings.setMousePointY(bombY);
		lbl_7.setText(drawBuildings.getResult());
		repaint();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
