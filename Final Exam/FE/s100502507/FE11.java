package fe.s100502507;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class FE11 extends JApplet {
	public FE11() {
		index = 0;
		
		images = new ImageIcon[9];
		images[0] = new ImageIcon(getClass().getResource("image/cat1.jpg"));
		images[1] = new ImageIcon(getClass().getResource("image/cat2.jpg"));
		images[2] = new ImageIcon(getClass().getResource("image/cat3.jpg"));
		images[3] = new ImageIcon(getClass().getResource("image/dog1.jpg"));
		images[4] = new ImageIcon(getClass().getResource("image/dog2.jpg"));
		images[5] = new ImageIcon(getClass().getResource("image/dog3.jpg"));
		images[6] = new ImageIcon(getClass().getResource("image/mouse1.jpg"));
		images[7] = new ImageIcon(getClass().getResource("image/mouse2.jpg"));
		images[8] = new ImageIcon(getClass().getResource("image/mouse3.jpg"));
		
		PictureAction actCat = new PictureAction("Cat", new ImageIcon(getClass().getResource("image/cat_icon.jpg")));
		PictureAction actDog = new PictureAction("Dog", new ImageIcon(getClass().getResource("image/dog_icon.jpg")));
		PictureAction actMouse = new PictureAction("Mouse", new ImageIcon(getClass().getResource("image/mouse_icon.jpg")));
		ButtonAction buttonAction = new ButtonAction();
		
		JToolBar toolBar = new JToolBar(JToolBar.VERTICAL);
		toolBar.add(actCat);
		toolBar.add(actDog);
		toolBar.add(actMouse);
		
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("File");
		menu.add(actCat);
		menu.add(actDog);
		menu.add(actMouse);
		setJMenuBar(menuBar);
		menuBar.add(menu);
		
		display = new JLabel();
		display.setIcon(images[index]);
		
		previous = new JButton("��");
		next = new JButton("��");
		previous.addActionListener(buttonAction);
		next.addActionListener(buttonAction);
		
		JPanel panel = new JPanel(new GridLayout(1, 2));
		panel.add(previous);
		panel.add(next);
		
		add(menuBar);
		add(toolBar, BorderLayout.EAST);
		add(display, BorderLayout.CENTER);
		add(panel, BorderLayout.SOUTH);
	}
	
	private class PictureAction extends AbstractAction {
		PictureAction(String name, Icon icon) {
			super(name, icon);
			putValue(Action.SHORT_DESCRIPTION, " Select the " + name + " flag to display");
			this.name = name;
	    } // end of MyAction constructor
		
		// actionListener of MyAction
	    public void actionPerformed(ActionEvent e) {
	    	if(name.equals("Cat")) {
	    		index = 0;
	    	}
	    	else if(name.equals("Dog")) {
	    		index = 3;
	    	}
	    	else if(name.equals("Mouse")) {
	    		index = 6;
	    	}
    		display.setIcon(images[index]);
	    }
	    
	    String name;
	}// end of MyAction
	
	private class ButtonAction extends AbstractAction {
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == previous) {
				if(index <= 0) {
					index = 0;
					JOptionPane.showMessageDialog(null, "This is the first page!", "Error message", JOptionPane.ERROR_MESSAGE);
				}
				else {
					index -= 1;
				}
				display.setIcon(images[index]);
			}
			else if(e.getSource() == next) {
				if(index >= 8) {
					index = 8;
					JOptionPane.showMessageDialog(null, "This is the last page!", "Error message", JOptionPane.ERROR_MESSAGE);
				}
				else {
					index += 1;
				}
				display.setIcon(images[index]);
			}
		}
	}
	
	private int index;
	private JLabel display;
	private JButton previous, next;
	private ImageIcon[] images;
}
