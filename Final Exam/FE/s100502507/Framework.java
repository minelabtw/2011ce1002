package fe.s100502507;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.Timer;

public class Framework extends JFrame {
	Framework() {
		xBase = -250;
		yBase = 150;
		mouseX = mouseY = -10;
		
		jcbLanguage = new JComboBox(new String[] {"英文", "法文", "日文", "中文"});
		jcbLanguage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switch(jcbLanguage.getSelectedIndex()) {
					case 0:
						locale = Locale.ENGLISH;
						break;
					case 1:
						locale = Locale.FRENCH;
						break;
					case 2:
						locale = Locale.JAPANESE;
						break;
					case 3:
						locale = Locale.CHINESE;
						break;
				}
				res = ResourceBundle.getBundle("Confidential", locale);
				updateStrings();
			}
		});
		
		jlbTitle = new JLabel(res.getString("Title"));
		jlbLanguage = new JLabel(res.getString("Language"));
		jlbPosition = new JLabel(res.getString("Position"));
		jlbResult = new JLabel(res.getString("Result"));
		jlbPositionValue = new JLabel(String.valueOf(mouseX) + ", " + String.valueOf(mouseY));
		jlbResultValue = new JLabel();
		
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(1, 2));
		JPanel panel2 = new JPanel();
		panel2.add(jlbLanguage);
		panel2.add(jcbLanguage);
		panel.add(jlbTitle);
		panel.add(panel2);
		add(panel, BorderLayout.NORTH);
		
		add(new Display(), BorderLayout.CENTER);
		
		panel = new JPanel();
		panel.setLayout(new GridLayout(1, 2));
		panel2 = new JPanel();
		panel2.add(jlbPosition);
		panel2.add(jlbPositionValue);
		panel.add(panel2);
		panel2 = new JPanel();
		panel2.add(jlbResult);
		panel2.add(jlbResultValue);
		panel.add(panel2);
		add(panel, BorderLayout.SOUTH);
		
		Timer timer = new Timer(100, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(xBase > getWidth()) {
					xBase = 0;
				}
				else {
					xBase += 1;
				}
				jlbPositionValue.setText(String.valueOf(mouseX) + ", " + String.valueOf(mouseY));
				repaint();
			}
		});
		timer.start();
	}
	
	public void updateStrings() {
		jlbTitle.setText(res.getString("Title"));
		jlbLanguage.setText(res.getString("Language"));
		jlbPosition.setText(res.getString("Position"));
		jlbResult.setText(res.getString("Result"));
	}
	
	public void setMousePositionX(int x_temp) {
		mouseX = x_temp;
	}
	
	public void setMousePositionY(int y_temp) {
		mouseY = y_temp;
	}
	
	public void setResult() {
		if(Math.abs(mouseX - (xBase + 105)) <= 95 && Math.abs(mouseY - (yBase + 100)) <= 100) {
			if(mouseX - (xBase + 10) >= 50) {
				jlbResultValue.setText("Building A&B");
			}
			else {
				jlbResultValue.setText("Building A");
			}
		}
		else if(Math.abs(mouseX - (xBase + 155)) <= 95 && Math.abs(mouseY - (yBase + 25)) <= 100) {
			if(mouseX - (xBase + 60) >= 50) {
				jlbResultValue.setText("Building A&B");
			}
			else {
				jlbResultValue.setText("Building B");
			}
		}
		else {
			jlbResultValue.setText("Miss");
		}
	}

	private class Display extends JLabel implements MouseListener {
		Display() {
			addMouseListener(this);
		}

		public void mouseClicked(MouseEvent e) {
			setMousePositionX(e.getX());
			setMousePositionY(e.getY());
			setResult();
		}

		public void mouseEntered(MouseEvent e) {
			
		}

		public void mouseExited(MouseEvent e) {
			
		}
		
		public void mousePressed(MouseEvent e) {
			
		}

		public void mouseReleased(MouseEvent e) {
			
		}
		
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			
			g.drawRect(xBase + 10, yBase, 190, 200);
			g.drawRect(xBase + 60, yBase - 50, 190, 200);
			g.setColor(Color.RED);
			g.fillOval(mouseX, mouseY, 5, 5);
		}
	}
	
	private int xBase, yBase;
	private int mouseX, mouseY;
	private JLabel jlbTitle, jlbLanguage, jlbPosition, jlbPositionValue, jlbResult, jlbResultValue;
	private JComboBox jcbLanguage;
	private Locale locale = Locale.ENGLISH;
	private ResourceBundle res = ResourceBundle.getBundle("Confidential", locale);
}
