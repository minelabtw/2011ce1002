package fe.s100502002;
import java.awt.*;

import javax.swing.*;
import javax.swing.Timer;

import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class Framework extends JFrame
{
	private JLabel JLabel_1= new JLabel("label1");
	private JLabel JLabel_2= new JLabel("label2");
	private JLabel JLabel_3= new JLabel("label3");
	private JLabel JLabel_4= new JLabel("label4");
	private JLabel JLabel_5= new JLabel("label5");
	private JLabel JLabel_6= new JLabel("label6");
	private JLabel JLabel_7= new JLabel("label7");
	private JPanel P1 = new JPanel();
	private JPanel P2 = new JPanel();
	private JPanel P3 = new JPanel();
	
	private JComboBox jcboLocale = new  JComboBox();
	private ActionListener timelistener;
	private Timer li = new Timer(200,timelistener);
	private Locale[] locale= Locale.getAvailableLocales();
	private String [] slocale = {"en","fr","ja","zh"};
	
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()==jcboLocale)
		{
			
			updateString();
		}
	}
	public Framework()
	{
		ResourceBundle res = ResourceBundle.getBundle("FE12_Language/Confidential");
		
		for(int i=0;i<locale.length;i++)
		{
			jcboLocale.addItem(locale[i].getDisplayName());
		}
		
		JLabel_2.setText(res.getString("Language"));
		JLabel_1.setText(res.getString("Title"));
		JLabel_4.setText(res.getString("Position"));
		JLabel_6.setText(res.getString("Result"));
		P1.setLayout(new GridLayout(1,3,0,0));
		P3.setLayout(new GridLayout(1,4,0,0));
		
		P1.add(JLabel_1);
		P1.add(JLabel_2);
		P1.add(jcboLocale);
		
		P2.add(JLabel_3);
		
		P3.add(JLabel_4);
		P3.add(JLabel_5);
		P3.add(JLabel_6);
		P3.add(JLabel_7);
		
		setLayout(new GridLayout(3,1,0,0));
		add(P1);
		add(P2);
		add(P3);

		
	}
	
	public void updateString()
	{
		ResourceBundle res = ResourceBundle.getBundle("Confidential");
		JLabel_2.setText(res.getString("Language"));
		JLabel_1.setText(res.getString("Title"));
		JLabel_4.setText(res.getString("Position"));
		JLabel_6.setText(res.getString("Result"));
		
		
		
	}
	protected void paintComponent(Graphics g)
	{
		super.paintComponents(g);
		int  xbase = 20;
		int  ybase = 500;
		
		g.drawRect(xbase+10, ybase, 190, 200);
		g.drawRect(xbase+250, ybase-50, 190, 200);
		
		xbase++;
		
	}
	public  class timelistener implements ActionListener
	{
		
		public void actionPerformed(ActionEvent e)
		{
			repaint();
		}
		
	}

}
