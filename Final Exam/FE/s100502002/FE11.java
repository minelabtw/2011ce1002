package fe.s100502002;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
public class FE11 extends JApplet
{
	private ImageIcon cat_icon = new ImageIcon("image/cat_icon.jpg");
	private ImageIcon cat1 = new ImageIcon("image/cat1.jpg");
	private ImageIcon cat2 = new ImageIcon("image/cat2.jpg");
	private ImageIcon cat3 = new ImageIcon("image/cat3.jpg");
	private ImageIcon dog_icon = new ImageIcon("image/dog_icon.jpg");
	private ImageIcon dog1 = new ImageIcon("image/dog1.jpg");
	private ImageIcon dog2 = new ImageIcon("image/dog2.jpg");
	private ImageIcon dog3 = new ImageIcon("image/dog3.jpg");
	private ImageIcon mouse_icon = new ImageIcon("image/mouse_icon.jpg");
	private ImageIcon mouse1 = new ImageIcon("image/mouse1.jpg");
	private ImageIcon mouse2 = new ImageIcon("image/mouse2.jpg");
	private ImageIcon mouse3 = new ImageIcon("image/mouse3.jpg");
	private ImageIcon [] picture = {cat1,cat2,cat3,dog1,dog2,dog3,mouse1,mouse2,mouse3};
	private JButton pre = new JButton("pre");
	private JButton next = new JButton("next");
	private JPanel p1 = new JPanel();
	private JPanel p2 = new JPanel();
	private JLabel display = new JLabel();
	private int index;
	private buttonlistener li = new buttonlistener();
	public FE11()
	{
		/*action*/
		Action cat_action = new MyAction("cat",picture);
		Action dog_action = new MyAction("dog",picture);
		Action mouse_action = new MyAction("mouse",picture);
		JMenuBar menuBar = new JMenuBar();
		JMenu menu= new JMenu("Animals Gallery");
		setJMenuBar(menuBar);
		/*create toolbar*/
		JToolBar toolbar = new JToolBar();
		JButton tc = new JButton(cat_action);
		JButton td = new JButton(dog_action);
		JButton tm = new JButton(mouse_action);
		tc.setIcon(cat_icon);
		td.setIcon(dog_icon);
		tm.setIcon(mouse_icon);
		toolbar.add(tc);
		toolbar.add(td);
		toolbar.add(tm);
		pre.addActionListener(li);
		next.addActionListener(li);
		display.setIcon(picture[index]);
		
		/*create menubar*/
		JMenuItem cat =new JMenuItem(cat_action); 
		JMenuItem dog =new JMenuItem(dog_action); 
		JMenuItem mouse =new JMenuItem(mouse_action); 
		
		cat.setIcon(cat_icon);
		dog.setIcon(dog_icon);
		mouse.setIcon(mouse_icon);
		menu.add(cat);
		menu.add(dog);
		menu.add(mouse);
		
		menuBar.add(menu);
		
		
		setLayout(new GridLayout(2,1,1,1));		
		add(toolbar);
		
		p1.add(display);
		add(p1);
		p2.setLayout(new GridLayout(1,2));
		
		p2.add(pre);
		p2.add(next);
		add(p2);
		
	}
	/*buttonlistener*/
	private class buttonlistener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(e.getSource()==pre)
			{
				index--;
				if(index<=0)
				{
					index=0;
				}
				display.setIcon(picture[index]);
			}
			else if(e.getSource()==next)
			{
				index++;
				if(index>=8)
				{
					index=8;
				}
				System.out.println(index);
				display.setIcon(picture[index]);
			}
		}
	}
	/*action*/
	private class MyAction extends AbstractAction
	{
		String name;
		public MyAction(String name, ImageIcon[] picture) 
		{
			putValue(Action.SHORT_DESCRIPTION,"select the"+name+"flag to display");
			this.name=name;
		}
		public void actionPerformed(ActionEvent e)
		{
			
			if(name.equals("cat"))
			{
				index=0;
				display.setIcon(picture[index]);
				
				
			}
			else if(name.equals("dog"))
			{
				index=3;
				display.setIcon(picture[index]);
				
			}
			else if(name.equals("mouse"))
			{
				index=6;
				display.setIcon(picture[index]);
			}
		}
		
	}
		
	
}

