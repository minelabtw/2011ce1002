// Decompiled by DJ v3.7.7.81 Copyright 2004 Atanas Neshkov  Date: 2012/6/25 �U�� 06:01:51
// Home Page : http://members.fortunecity.com/neshkov/dj.html  - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Framework.java

package fe.s100502010;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Framework extends JFrame
    implements ActionListener
{

    public Framework()
    {
        label1 = new JLabel("The Bomber Infared Screen");
        label2 = new JLabel("Language");
        label3 = new JLabel();
        label4 = new JLabel("Position");
        label5 = new JLabel();
        label6 = new JLabel("Result");
        label7 = new JLabel();
        combo = new JComboBox();
        xbase = 0;
        ybase = 200;
        timer = new Timer(300, this);
        JPanel p1 = new JPanel();
        p1.setLayout(new GridLayout(1, 3, 30, 10));
        p1.add(label1);
        p1.add(label2);
        p1.add(combo);
        JPanel p2 = new JPanel();
        p2.add(label3);
        JPanel p3 = new JPanel();
        p3.setLayout(new GridLayout(1, 4, 10, 10));
        p3.add(label4);
        p3.add(label5);
        p3.add(label6);
        p3.add(label7);
        JPanel p4 = new JPanel();
        p4.setLayout(new BorderLayout());
        p4.add(p1, "North");
        p4.add(p2, "Center");
        p4.add(p3, "South");
        add(p4);
        timer.start();
        repaint();
    }

    public void paintComponent(Graphics g)
    {
        if(xbase == 500)
            xbase = 0;
        else
            xbase++;
        Polygon p = new Polygon();
        p.addPoint(xbase + 10, ybase + 200);
        p.addPoint(xbase + 10, ybase);
        p.addPoint(xbase + 200, ybase);
        p.addPoint(xbase + 200, ybase + 200);
        g.drawPolygon(p);
        Polygon p2 = new Polygon();
        p2.addPoint(xbase + 60, ybase - 50);
        p2.addPoint(xbase + 60, ybase + 100);
        p2.addPoint(xbase + 250, ybase - 50);
        p2.addPoint(xbase + 250, ybase + 100);
        g.drawPolygon(p2);
    }

    public void actionPerformed(ActionEvent actionevent)
    {
    }

    public JLabel label1;
    public JLabel label2;
    public JLabel label3;
    public JLabel label4;
    public JLabel label5;
    public JLabel label6;
    public JLabel label7;
    public JComboBox combo;
    public int xbase;
    public int ybase;
    Timer timer;
}