// Decompiled by DJ v3.7.7.81 Copyright 2004 Atanas Neshkov  Date: 2012/6/25 �U�� 06:04:56
// Home Page : http://members.fortunecity.com/neshkov/dj.html  - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   FE11.java

package fe.s100502501;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import javax.swing.*;

public class FE11 extends JApplet
{
    private class MyAction extends AbstractAction
    {

        public void actionPerformed(ActionEvent e)
        {
            if(name.equals("Cat"))
            {
                co = 0;
                Label_img.setIcon(ani[co]);
                if(e.getSource() == pre)
                {
                    co--;
                    if(co < 0)
                        JOptionPane.showMessageDialog(null, " Error! This is the first picture!");
                    else
                        Label_img.setIcon(ani[co]);
                } else
                if(e.getSource() == next)
                {
                    co++;
                    if(co > 3)
                        JOptionPane.showMessageDialog(null, " Error! This is the last picture!");
                    else
                        Label_img.setIcon(ani[co]);
                }
            }
            if(name.equals("Dog"))
            {
                co = 3;
                Label_img.setIcon(ani[co]);
                if(e.getSource() == pre)
                {
                    co--;
                    if(co < 3)
                        JOptionPane.showMessageDialog(null, " Error! This is the first picture!");
                    else
                        Label_img.setIcon(ani[co]);
                } else
                if(e.getSource() == next)
                {
                    co++;
                    if(co > 5)
                        JOptionPane.showMessageDialog(null, " Error! This is the last picture!");
                    else
                        co++;
                    Label_img.setIcon(ani[co]);
                }
            }
            if(name.equals("Mouse"))
            {
                co = 6;
                Label_img.setIcon(ani[co]);
                if(e.getSource() == pre)
                {
                    co--;
                    if(co < 6)
                        JOptionPane.showMessageDialog(null, " Error! This is the first picture!");
                    else
                        Label_img.setIcon(ani[co]);
                } else
                if(e.getSource() == next)
                {
                    co++;
                    if(co > 8)
                        JOptionPane.showMessageDialog(null, " Error! This is the last picture!");
                    else
                        Label_img.setIcon(ani[co]);
                }
            }
        }

        String name;
        final FE11 this$0;

        MyAction(String name, Icon icon)
        {
            this$0 = FE11.this;
            super(name, icon);
            putValue("ShortDescription", (new StringBuilder(" Select the ")).append(name).append(" animal to display").toString());
            this.name = name;
        }
    }


    public FE11()
    {
        cicon = new ImageIcon(getClass().getResource("image/cat_icon.jpg"));
        dicon = new ImageIcon(getClass().getResource("image/dog_icon.jpg"));
        micon = new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));
        ani = new ImageIcon[9];
        pre = new JButton("\u2190");
        next = new JButton("\u2192");
        Label_img = new JLabel();
        p1 = new JPanel(new GridLayout(1, 2));
        ani[0] = new ImageIcon(getClass().getResource("image/cat1.jpg"));
        ani[1] = new ImageIcon(getClass().getResource("image/cat2.jpg"));
        ani[2] = new ImageIcon(getClass().getResource("image/cat3.jpg"));
        ani[3] = new ImageIcon(getClass().getResource("image/dog1.jpg"));
        ani[4] = new ImageIcon(getClass().getResource("image/dog2.jpg"));
        ani[5] = new ImageIcon(getClass().getResource("image/dog3.jpg"));
        ani[6] = new ImageIcon(getClass().getResource("image/mouse1.jpg"));
        ani[7] = new ImageIcon(getClass().getResource("image/mouse2.jpg"));
        ani[8] = new ImageIcon(getClass().getResource("image/mouse3.jpg"));
        javax.swing.Action caction = new MyAction("Cat", cicon);
        javax.swing.Action daction = new MyAction("Dog", dicon);
        javax.swing.Action maction = new MyAction("Mouse", micon);
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Animal");
        setJMenuBar(menuBar);
        menuBar.add(menu);
        menu.add(caction);
        menu.add(daction);
        menu.add(maction);
        JToolBar toolBar = new JToolBar(1);
        toolBar.setBorder(BorderFactory.createLineBorder(Color.red));
        toolBar.add(caction);
        toolBar.add(daction);
        toolBar.add(maction);
        p1.add(pre);
        p1.add(next);
        add(toolBar, "East");
        add(Label_img, "Center");
        add(p1, "South");
        pre.addActionListener(maction);
        pre.addActionListener(daction);
        pre.addActionListener(caction);
        next.addActionListener(caction);
        next.addActionListener(maction);
        next.addActionListener(daction);
    }

    private ImageIcon cicon;
    private ImageIcon dicon;
    private ImageIcon micon;
    private ImageIcon ani[];
    private JButton pre;
    private JButton next;
    private JLabel Label_img;
    private JPanel p1;
    private int co;






}