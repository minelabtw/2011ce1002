// Decompiled by DJ v3.7.7.81 Copyright 2004 Atanas Neshkov  Date: 2012/6/25 �U�� 06:05:16
// Home Page : http://members.fortunecity.com/neshkov/dj.html  - Check often for new version!
// Decompiler options: packimports(3) 
// Source File Name:   Framework.java

package fe.s100502501;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.swing.*;

// Referenced classes of package fe.s100502501:
//            DrawBuildings

public class Framework extends JFrame
    implements ActionListener
{

    public Framework()
    {
        availableLocales = Locale.getAvailableLocales();
        locale = Locale.US;
        res = ResourceBundle.getBundle("Confidential", locale);
        db = new DrawBuildings();
        j1 = new JLabel(res.getString("Title"));
        j2 = new JLabel(res.getString("Language"));
        j3 = new JLabel();
        j4 = new JLabel(res.getString("Position"));
        j5 = new JLabel();
        j6 = new JLabel(res.getString("Result"));
        j7 = new JLabel();
        p1 = new JPanel(new GridLayout(1, 3));
        p2 = new JPanel();
        p3 = new JPanel(new GridLayout(1, 4));
        jcb = new JComboBox();
        for(int i = 0; i < availableLocales.length; i++)
            jcb.addItem(availableLocales[i].getDisplayName());

        setLayout(new BorderLayout());
        res = ResourceBundle.getBundle("Confidential", locale);
        j1.setText(res.getString("Title"));
        j2.setText(res.getString("Language"));
        j4.setText(res.getString("Position"));
        j6.setText(res.getString("Result"));
        j3.add(db);
        p1.add(j1);
        p1.add(j2);
        p1.add(jcb);
        p2.add(j3);
        p3.add(j4);
        p3.add(j5);
        p3.add(j6);
        p3.add(j7);
        add(p1, "North");
        add(p2, "Center");
        add(p3, "South");
        jcb.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource() == jcb)
            locale = availableLocales[jcb.getSelectedIndex()];
    }

    private Locale availableLocales[];
    private Locale locale;
    private ResourceBundle res;
    DrawBuildings db;
    JLabel j1;
    JLabel j2;
    JLabel j3;
    JLabel j4;
    JLabel j5;
    JLabel j6;
    JLabel j7;
    JPanel p1;
    JPanel p2;
    JPanel p3;
    JComboBox jcb;
}