package fe.s985003038;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;

public class FE11 extends JApplet {
	private static final long serialVersionUID = 1L;
	
	private ImageIcon[][] photos = new ImageIcon[][]{						// 2-dimension array to keep the photos
			{
				new ImageIcon(getClass().getResource("image/cat1.jpg")),
				new ImageIcon(getClass().getResource("image/cat2.jpg")),
				new ImageIcon(getClass().getResource("image/cat3.jpg"))
			}, {
				new ImageIcon(getClass().getResource("image/dog1.jpg")),
				new ImageIcon(getClass().getResource("image/dog2.jpg")),
				new ImageIcon(getClass().getResource("image/dog3.jpg"))
			}, {
				new ImageIcon(getClass().getResource("image/mouse1.jpg")),
				new ImageIcon(getClass().getResource("image/mouse2.jpg")),
				new ImageIcon(getClass().getResource("image/mouse3.jpg"))
			}
		};
	
	private int AnimalIndex = 0;											// index for animals, 0 for cat, 1 for dog, 2 for mouse
	private int PhotoIndex = 0;												// index for photos
	private static final int totalPhoto = 3;								// total number of photos
	private JLabel photoLabel = new JLabel(photos[0][0]);					// JLabel default showing the first photo of cat
	
	public FE11(){
		Action cat_action = new MyAction("Cat", new ImageIcon(getClass().getResource("image/cat_icon.jpg")));
		Action dog_action = new MyAction("Dog", new ImageIcon(getClass().getResource("image/dog_icon.jpg")));
		Action mouse_action = new MyAction("Mouse", new ImageIcon(getClass().getResource("image/mouse_icon.jpg")));
		
		JMenuBar menuBar = new JMenuBar();									// design the JMenuBar
		JMenu menu = new JMenu("Animals Gallery");
		this.setJMenuBar(menuBar);
		menuBar.add(menu);
		menu.add(cat_action);
		menu.add(dog_action);
		menu.add(mouse_action);
		
		JToolBar toolBar = new JToolBar(JToolBar.VERTICAL);					// design the JToolBar
		toolBar.add(cat_action);
		toolBar.add(dog_action);
		toolBar.add(mouse_action);
		
		JPanel controlPanel = new JPanel();									// design the control panel which contain two buttons
		JButton previous = new JButton("Previous");
		JButton next = new JButton("Next");
		controlPanel.add(previous);
		controlPanel.add(next);
		
		previous.addActionListener(new ActionListener(){					// event handler for the previous button
			public void actionPerformed(ActionEvent e) {
				if(PhotoIndex - 1 < 0)										// check if it is the first photo
					JOptionPane.showMessageDialog(null, "This is the first photo!", "Final Exam", JOptionPane.WARNING_MESSAGE);
				else {
					PhotoIndex = --PhotoIndex % totalPhoto;
					UpdatePhoto();
				}
			}
		});
		
		next.addActionListener(new ActionListener(){						// event handler for the next button
			public void actionPerformed(ActionEvent e) {
				if(PhotoIndex + 1 >= totalPhoto)							// check if it is the last photo
					JOptionPane.showMessageDialog(null, "This is the last photo!", "Final Exam", JOptionPane.WARNING_MESSAGE);
				else {
					PhotoIndex = ++PhotoIndex % totalPhoto;
					UpdatePhoto();
				}
			}
		});
		
		this.setLayout(new BorderLayout(2,2));								// design the applet
		this.add(photoLabel, BorderLayout.CENTER);
		this.add(toolBar, BorderLayout.EAST);
		this.add(controlPanel, BorderLayout.SOUTH);
	}
	
	private void UpdatePhoto(int AnimalIndex){								// method to update photo which change the animal according to the input animal index
		if(AnimalIndex >= 0 && AnimalIndex < totalPhoto){
			this.AnimalIndex = AnimalIndex;
			UpdatePhoto();
		} else																// check if the input animal index is out of range
			JOptionPane.showMessageDialog(null, "This Animal is undefined!", "Final Exam", JOptionPane.WARNING_MESSAGE);
	}
	
	private void UpdatePhoto(){												// method to update photo which only change the photo according to the photo index
		photoLabel.setIcon(photos[AnimalIndex][PhotoIndex]);				// update the icon of the JLabel
	}
	
	private class MyAction extends AbstractAction {							// Action for the JMenuBar and JToolBar
		private static final long serialVersionUID = 1L;
		String name;
		
		public MyAction(String name, Icon icon){
			super(name, icon);
			this.putValue(Action.SHORT_DESCRIPTION, "Gallery of " + name);
			this.name = name;
		}
		
		public void actionPerformed(ActionEvent e) {
			if(name == "Cat"){												// for selecting each animals, update the animal index and update the photo
				UpdatePhoto(0);
			} else if(name == "Dog"){
				UpdatePhoto(1);
			} else if(name == "Mouse"){
				UpdatePhoto(2);
			}
		}
		
	}
}
