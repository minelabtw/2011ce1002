package fe.s985003038;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Framework extends JFrame {
	private static final long serialVersionUID = 1L;
	private static final int xBase = 630, yBase = 200;
	private static final int BuildAWidth = 190, BuildAHeight = 200, BuildBWidth = 190, BuildBHeight = 150;
	private int[] ClickLocation = {0, 0};
	private int[] BuildALocation = {xBase + 10, yBase};
	private int[] BuildBLocation = {xBase + 60, yBase - 50};
	private String Result = "Nothing";
	private int Score = 0;
	private JLabel JLabel_1 = new JLabel();
	private JLabel JLabel_2 = new JLabel();
	private JLabel JLabel_4 = new JLabel();
	private JLabel JLabel_5 = new JLabel(ClickLocation[0] + "," + ClickLocation[1]);
	private JLabel JLabel_6 = new JLabel();
	private JLabel JLabel_7 = new JLabel(Result);
	private JLabel JLabel_8 = new JLabel("Scores");
	private JLabel JLabel_9 = new JLabel("0");
	private JComboBox JComboBox_1 = new JComboBox();
	private Locale[] locale = new Locale[]{Locale.ENGLISH, Locale.CHINESE, Locale.JAPANESE, Locale.FRENCH};
	private ResourceBundle res = ResourceBundle.getBundle("Confidential", locale[0]);
	
	public Framework(){
		JPanel upperPanel = new JPanel();							// create the upper panel which contain label 1, 2 and the combobox
		upperPanel.setLayout(new GridLayout(1,3));
		upperPanel.add(JLabel_1);
		upperPanel.add(JLabel_2);
		upperPanel.add(JComboBox_1);
		for(int i = 0; i < locale.length; i++)						// add the language selection to the combobox
			JComboBox_1.addItem(locale[i].getDisplayLanguage());
		
		JComboBox_1.addActionListener(new ActionListener(){			// add action listener to the combobox to change the language
			public void actionPerformed(ActionEvent e) {
				res = ResourceBundle.getBundle("Confidential", locale[JComboBox_1.getSelectedIndex()]);
				UpdateLanguage();
			}
		});
		
		JPanel lowerPanel = new JPanel();							// create the lower panel which contain label 4 to 7
		lowerPanel.setLayout(new GridLayout(1,6));
		lowerPanel.add(JLabel_4);
		lowerPanel.add(JLabel_5);
		lowerPanel.add(JLabel_6);
		lowerPanel.add(JLabel_7);
		lowerPanel.add(JLabel_8);
		lowerPanel.add(JLabel_9);
		
		this.setLayout(new BorderLayout(3,1));						// add all the components to the frame
		this.add(upperPanel, BorderLayout.NORTH);
		this.add(lowerPanel, BorderLayout.SOUTH);
		UpdateLanguage();
		
		new Timer(20, new ActionListener(){							// set the action of the timer and start the timer
			public void actionPerformed(ActionEvent e) {
				if(BuildBLocation[0] + 190 < 0){					// if the buildings are out of screen, reset the position of the buildings
					BuildALocation[0] = xBase + 10;
					BuildBLocation[0] = xBase + 60;
				} else {
					BuildALocation[0]--;
					BuildBLocation[0]--;
				}
				repaint();
			}
		}).start();
		
		this.addMouseListener(new MouseListener(){					// add mouse event handler to the frame
			public void mouseClicked(MouseEvent e) {}
			public void mouseEntered(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {}
			public void mouseReleased(MouseEvent e) {}
			public void mousePressed(MouseEvent e) {				// if mouse pressed is detected
				ClickLocation[0] = e.getPoint().x;					// set the click point
				ClickLocation[1] = e.getPoint().y;
				repaint();											// paint the red dot
				CheckResult();										// compute the result
			}			
		});
	}
	
	private void CheckResult(){										// method to compute ther result
		boolean BuildAHit = false, BuildBHit = false;				// flag to indicate whether the buildings are hit
		if(ClickLocation[0] > BuildALocation[0] && ClickLocation[0] < BuildALocation[0] + BuildAWidth &&
			ClickLocation[1] > BuildALocation[1] && ClickLocation[1] < BuildALocation[1] + BuildAHeight)
			BuildAHit = true;										// if the mouse click in building A, set the flag A
		if(ClickLocation[0] > BuildBLocation[0] && ClickLocation[0] < BuildBLocation[0] + BuildBWidth &&
				ClickLocation[1] > BuildBLocation[1] && ClickLocation[1] < BuildBLocation[1] + BuildBHeight)
			BuildBHit = true;										// if the mouse click in building B, set the flag B
		
		if(BuildAHit && BuildBHit){									// update the result and scores according to the flags
			JLabel_7.setText("Both A & B");
			Score += 2;
		} else if(BuildAHit){
			JLabel_7.setText("Building A");
			Score++;
		} else if(BuildBHit){
			JLabel_7.setText("Building B");
			Score++;
		} else
			JLabel_7.setText("Nothing");
		
		JLabel_9.setText(String.valueOf(Score));					// show the scores
	}
	
	private void UpdateLanguage(){									// method to update the language
		JLabel_1.setText(res.getString("Title"));
		JLabel_2.setText(res.getString("Language"));
		JLabel_4.setText(res.getString("Position"));
		JLabel_6.setText(res.getString("Result"));
	}
	
	public void paint(Graphics g){									// method to draw the animation
		super.paint(g);
		g.drawRect(BuildALocation[0], BuildALocation[1], BuildAWidth, BuildAHeight);
		g.drawRect(BuildBLocation[0], BuildBLocation[1], BuildBWidth, BuildBHeight);
		g.setFont(new Font("Arial", Font.BOLD, 20));
		g.drawString("A", BuildALocation[0] - 30, BuildALocation[1] + 190);
		g.drawString("B", BuildBLocation[0] - 30, BuildBLocation[1] + 140);
		g.setColor(Color.RED);
		g.fillOval(ClickLocation[0] - 5, ClickLocation[1] - 5, 10, 10);
	}
}
