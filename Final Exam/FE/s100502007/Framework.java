package fe.s100502007;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

import javax.swing.*;

public class Framework extends JFrame implements ActionListener{
	private Locale[] availableLocales = Locale.getAvailableLocales();
	private JComboBox jcboLocale = new JComboBox();
	private Locale locale = Locale.US;
	private ResourceBundle res = ResourceBundle.getBundle("Confidential",locale);
	private JLabel label = new JLabel(res.getString("Choose_locale"));
	private JLabel label1 = new JLabel("The Bomber Infared Screen     ");
	private JLabel label2 = new JLabel("   Language : ");
	private JLabel label3 = new JLabel();
	private JLabel label4 = new JLabel("      Position : ");
	private JLabel label5 = new JLabel();
	private JLabel label6 = new JLabel("      Result : ");
	private JLabel label7 = new JLabel();
	private void updateString(){
		res = ResourceBundle.getBundle("Confidential",locale);;
		label3.setText(res.getString("Choose_locale"));
	}
	public Framework(){
		for(int i=0;i<availableLocales.length;i++){
			jcboLocale.addItem(availableLocales[i].getDisplayName());
		}
		JPanel ptop = new JPanel(new BorderLayout());
		JPanel pmid = new JPanel(new BorderLayout());
		JPanel pdown = new JPanel(new BorderLayout());
		ptop.add(label1);
		ptop.add(label2);
		ptop.add(jcboLocale);
		pmid.add(label3);
		pdown.add(label4);
		pdown.add(label5);
		pdown.add(label6);
		pdown.add(label7);
		this.setLayout(new GridLayout(3,1));
		this.add(ptop);
		this.add(pmid);
		this.add(pdown);
		jcboLocale.addActionListener(this);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == jcboLocale){
			locale = availableLocales[jcboLocale.getSelectedIndex()];
			updateString();
		}
	}
}
