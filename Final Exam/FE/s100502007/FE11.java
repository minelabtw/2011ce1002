package fe.s100502007;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FE11 extends JApplet{
	private ImageIcon cat_icon = new ImageIcon(getClass().getResource("image/cat_icon.jpg"));
	private ImageIcon dog_icon = new ImageIcon(getClass().getResource("image/dog_icon.jpg"));
	private ImageIcon mouse_icon = new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));
	
	private ImageIcon cat1 = new ImageIcon(getClass().getResource("image/cat1.jpg"));
	private ImageIcon cat2 = new ImageIcon(getClass().getResource("image/cat2.jpg"));
	private ImageIcon cat3 = new ImageIcon(getClass().getResource("image/cat3.jpg"));
	private ImageIcon dog1 = new ImageIcon(getClass().getResource("image/dog1.jpg"));
	private ImageIcon dog2 = new ImageIcon(getClass().getResource("image/dog2.jpg"));
	private ImageIcon dog3 = new ImageIcon(getClass().getResource("image/dog3.jpg"));
	private ImageIcon mouse1 = new ImageIcon(getClass().getResource("image/mouse1.jpg"));
	private ImageIcon mouse2 = new ImageIcon(getClass().getResource("image/mouse2.jpg"));
	private ImageIcon mouse3 = new ImageIcon(getClass().getResource("image/mouse3.jpg"));
	JButton previous = new JButton("Previous");
	JButton next = new JButton("Next");
	
	int number;
	
	private JPanel panel_button = new JPanel(new FlowLayout());
	private JLabel label_img = new JLabel();
	public FE11(){
		Action cat_action = new MyAction("cat",cat_icon);
		Action dog_action = new MyAction("dog",dog_icon);
		Action mouse_action = new MyAction("mouse",mouse_icon);
		
		JMenuBar menubar = new JMenuBar();
		JMenu menu = new JMenu();
		setJMenuBar(menubar);
		menubar.add(menu);
		menu.add(cat_action);
		menu.add(dog_action);
		menu.add(mouse_action);
		
		JToolBar toolbar = new JToolBar(JToolBar.VERTICAL);
		toolbar.setBorder(BorderFactory.createLineBorder(Color.red));
		toolbar.add(cat_action);
		toolbar.add(dog_action);
		toolbar.add(mouse_action);
		add(toolbar,BorderLayout.EAST);

		JButton previous = new JButton("Previous");
		JButton next = new JButton("Next");
		panel_button.add(previous);
		panel_button.add(next);
		add(label_img,BorderLayout.CENTER);
		add(panel_button,BorderLayout.SOUTH);
		number=0;
	}
	public void actionPerformed(ActionEvent e){
		if(e.getSource()==previous){
			number = number-1;
		}
		else if(e.getSource()==next){
			number = number+1;
		}
	}
	private class MyAction extends AbstractAction{
		String name;
		MyAction(String name,Icon icon){
			super(name,icon);
			putValue(Action.SHORT_DESCRIPTION,"Animals Gallery");
			this.name=name;
		}
		public void actionPerformed(ActionEvent e){
			if(number==1){
				label_img.setIcon(cat1);
			}
			else if(number==2){
				label_img.setIcon(cat2);
			}
			else if(number==3){
				label_img.setIcon(cat3);
			}
			else if(number==4){
				label_img.setIcon(dog1);
			}
			else if(number==5){
				label_img.setIcon(dog2);
			}
			else if(number==6){
				label_img.setIcon(dog3);
			}
			else if(number==7){
				label_img.setIcon(mouse1);
			}
			else if(number==8){
				label_img.setIcon(mouse2);
			}
			else if(number==9){
				label_img.setIcon(mouse3);
			}
			if(name.equals("cat")){
				number=1;
			}
			else if(name.equals("dog")){
				number=4;
			}
			else if(name.equals("mouse")){
				number=7;
			}
		}
	}
	
}

