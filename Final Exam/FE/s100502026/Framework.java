package fe.s100502026;

import java.awt.*;
import javax.swing.*;

public class Framework extends JFrame{
	
	private JPanel midPanel = new JPanel();
	private JPanel upperPanel = new JPanel();
	private JPanel lowerPanel = new JPanel();
	private JLabel JLabel_1 = new JLabel( "Title" );
	private JLabel JLabel_2 = new JLabel( "language" );
	private JLabel JLabel_3 = new JLabel();
	private JLabel JLabel_4 = new JLabel( "Position" );
	private JLabel JLabel_5 = new JLabel();
	private JLabel JLabel_6 = new JLabel( "Result" );
	private JLabel JLabel_7 = new JLabel();
	JComboBox jcbo = new JComboBox();
	
	Framework()
	{
		jcbo.add( new JButton( "EN" ) );
		jcbo.add( new JButton( "CH" ) );
		jcbo.add( new JButton( "FR" ) );
		jcbo.add( new JButton( "JP" ) );
		
		upperPanel.add( JLabel_1 );
		upperPanel.add( JLabel_2 );
		upperPanel.add( jcbo );

		midPanel.add( JLabel_3 );
		
		lowerPanel.add( JLabel_4 );
		lowerPanel.add( JLabel_5 );
		lowerPanel.add( JLabel_6 );
		lowerPanel.add( JLabel_7 );
		
		this.add( upperPanel , BorderLayout.NORTH );
		this.add( midPanel , BorderLayout.CENTER );
		this.add( lowerPanel , BorderLayout.SOUTH );
	}

}
