package fe.s100502017;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
public class DrawBuildings extends JPanel{
	int xBase=0;
	int yBase;
	TimerListener tl=new TimerListener();
	Timer time =new Timer(200,tl);
	public DrawBuildings(){
		time.start();
		repaint();
	}
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		yBase=this.getHeight()/2;
		g.drawRect(xBase+10, yBase, 190, 200);
		g.drawRect(xBase+60, yBase-50, 190,150);
		
	}
	class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			xBase+=3;
			repaint();
		}
		
	}
	
}
