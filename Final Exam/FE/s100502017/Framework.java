package fe.s100502017;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import java.text.*;
import java.util.Locale;
import java.util.ResourceBundle;
public class Framework extends JFrame implements ActionListener {
	DrawBuildings building=new DrawBuildings();
	Locale[] AvilLoc=Locale.getAvailableLocales();
	Locale locale=Locale.CHINESE;
	ResourceBundle res=ResourceBundle.getBundle("Confidential",locale);
	JLabel[] label=new JLabel[8];
	JComboBox cb=new JComboBox();
	JPanel pan1=new JPanel();
	JPanel pan3=new JPanel();
	public Framework(){
		for(int j=0;j<8;j++){
			label[j]=new JLabel();
		}
		label[1].setText(res.getString("Title"));
		label[2].setText(res.getString("Language"));
		label[4].setText(res.getString("Position"));
		label[6].setText(res.getString("Result"));
		pan1.setLayout(new GridLayout(1,3));
		pan1.add(label[1]);
		pan1.add(label[2]);
		pan1.add(cb);
		pan1.setLayout(new GridLayout(1,4));
		pan3.add(label[4]);
		pan3.add(label[5]);
		pan3.add(label[6]);
		pan3.add(label[7]);
		add(pan1,BorderLayout.NORTH);
		add(building,BorderLayout.CENTER);
		add(pan3,BorderLayout.SOUTH);
		
	}

	public void actionPerformed(ActionEvent e) {
		
	}

}
