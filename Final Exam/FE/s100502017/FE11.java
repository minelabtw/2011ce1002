package fe.s100502017;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class FE11 extends JApplet implements ActionListener{
	JToolBar tool=new JToolBar();
	JButton[] toolButton=new JButton[3];
	JMenuBar mb=new JMenuBar();
	JMenu mu=new JMenu("animals picture");
	ImageIcon[] picture=new ImageIcon[9];
	ImageIcon smallcat=new ImageIcon(getClass().getResource("image/cat_icon.jpg"));
	ImageIcon smalldog=new ImageIcon(getClass().getResource("image/dog_icon.jpg"));
	ImageIcon smallmouse=new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));
	JMenuItem mucat=new JMenuItem("cat");
	JMenuItem mudog=new JMenuItem("dog");
	JMenuItem mumouse=new JMenuItem("mouse");
	JLabel glab =new JLabel();
	JPanel picturespace =new JPanel();
	JPanel bottonspace =new JPanel();
	JButton previous=new JButton("previous");
	JButton next=new JButton("next");
	private int nowIndex=0;
	public FE11(){//constructor for this applet
		for(int i=0;i<9;i++){//declare pictures
			if(i<3)
				picture[i]=new ImageIcon(getClass().getResource("image/cat"+(i+1)+".jpg"));
			else if(i<6)
				picture[i]=new ImageIcon(getClass().getResource("image/dog"+(i-2)+".jpg"));
			else if(i<9)
				picture[i]=new ImageIcon(getClass().getResource("image/mouse"+(i-5)+".jpg"));
		}
		tool.setLayout((new GridLayout(3,1)));
		toolButton[0]=new JButton(smallcat);
		toolButton[1]=new JButton(smalldog);
		toolButton[2]=new JButton(smallmouse);
		tool.add(toolButton[0]);
		tool.add(toolButton[1]);
		tool.add(toolButton[2]);
		add(tool,BorderLayout.EAST);
		add(mb,BorderLayout.NORTH);
		mb.add(mu);
		mu.add(mucat);
		mu.add(mudog);
		mu.add(mumouse);
		mucat.setIcon(smallcat);
		mudog.setIcon(smalldog);
		mumouse.setIcon(smallmouse);
		bottonspace.setLayout(new GridLayout(1,2));
		bottonspace.add(previous);
		bottonspace.add(next);
		picturespace.add(glab,BorderLayout.CENTER);
		add(picturespace,BorderLayout.CENTER);
		add(bottonspace,BorderLayout.SOUTH);
		toolButton[0].addActionListener(this);//to add actionListener
		toolButton[1].addActionListener(this);
		toolButton[2].addActionListener(this);
		mucat.addActionListener(this);
		mudog.addActionListener(this);
		mumouse.addActionListener(this);
		previous.addActionListener(this);
		next.addActionListener(this);
		
	}
	public static void main(String[] args){
		JFrame frame=new JFrame();
		JApplet applet=new FE11();
		frame.add(applet,BorderLayout.CENTER);
		frame.setSize(600, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	public void actionPerformed(ActionEvent e) {//event is here
		if(e.getSource()==toolButton[0]||e.getSource()==mucat){//the cat dog mouse button
			nowIndex=0;
			glab.setIcon(picture[nowIndex]);
		}
		else if(e.getSource()==toolButton[1]||e.getSource()==mudog){
			nowIndex=3;
			glab.setIcon(picture[nowIndex]);
		}
		else if(e.getSource()==toolButton[2]||e.getSource()==mumouse){
			nowIndex=6;
			glab.setIcon(picture[nowIndex]);
		}
		else if(e.getSource()==previous){//let user look previous picture
			if((nowIndex-1)==-1||(nowIndex-1)==2||(nowIndex-1)==5){
				JOptionPane.showMessageDialog(null,"this is first picture!");
			}
			else{
				nowIndex--;
				glab.setIcon(picture[nowIndex]);
			}
		}
		else if(e.getSource()==next){//let user look next picture
			if((nowIndex+1)==3||(nowIndex+1)==6||(nowIndex+1)==9){
				JOptionPane.showMessageDialog(null,"this is last picture!");
			}
			else{
				nowIndex++;
				glab.setIcon(picture[nowIndex]);
			}
		}
	}
	
}
