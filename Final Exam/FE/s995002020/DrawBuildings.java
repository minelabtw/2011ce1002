package fe.s995002020;
import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;
public class DrawBuildings extends JPanel implements ActionListener  {
	  private int xBase = 60;
	  private int yBase = HEIGHT/2;
		DrawBuildings(Graphics g){
			g.drawLine(xBase+10,yBase+200,xBase+200,yBase+200);
			g.drawLine(xBase+10,yBase+200,xBase+10,yBase+200);
			g.drawLine(xBase+10, yBase, xBase+200, yBase);
			g.drawLine(xBase+200,yBase,xBase+200,yBase+200);
		    
			g.drawOval(xBase, yBase, 10, 10);
			g.drawRect(xBase+225, yBase-25,190,150);
		
	Timer timer = new Timer(200,this);
	
	if (xBase >= WIDTH){
		xBase = 0;
		repaint();
	}
	
		}
	public int setMousePositionX(int x_temp){
		x_temp = xBase;
		return x_temp;
		}
	public int setMousePositionY(int y_temp){
		y_temp = yBase;
		return y_temp;
	}
	public String BombPosition(int xbomb,int ybomb){
		if(xBase+60 < xbomb < xBase+200 && yBase < ybomb< yBase+100){
			return "Both";
		}
		else if(xBase+60<=xbomb<=xBase+250 && yBase-50<=ybomb<=yBase+100){
			return "A";
		}
	}
}
