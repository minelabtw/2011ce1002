package fe.s995002020;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

	public class FE11 extends JApplet{
		private ImageIcon Cat_icon = new ImageIcon(getClass().getResource("image/cat_icon.jpg"));//pictures
		private ImageIcon Mouse_icon = new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));
		private ImageIcon Dog_icon = new ImageIcon(getClass().getResource("image/dog_icon.jpg"));
		
		private ImageIcon Cat1_img = new ImageIcon(getClass().getResource("image/cat1.jpg"));
		private ImageIcon Cat2_img = new ImageIcon(getClass().getResource("image/cat2.jpg"));
		private ImageIcon Cat3_img = new ImageIcon(getClass().getResource("image/cat3.jpg"));
		private ImageIcon Dog1_img = new ImageIcon(getClass().getResource("image/dog1.jpg"));
		private ImageIcon Dog2_img = new ImageIcon(getClass().getResource("image/dog2.jpg"));
		private ImageIcon Dog3_img = new ImageIcon(getClass().getResource("image/dog3.jpg"));
		private ImageIcon Mouse1_img = new ImageIcon(getClass().getResource("image/mouse1.jpg"));
		private ImageIcon Mouse2_img = new ImageIcon(getClass().getResource("image/mouse2.jpg"));
		private ImageIcon Mouse3_img = new ImageIcon(getClass().getResource("image/mouse3.jpg"));
		
		private JLabel label_icon = new JLabel();//JLabel
		private JLabel label_img = new JLabel();
		public JButton previous = new JButton("<-");
		public JButton next = new JButton("->");

	public FE11(){
		Action Cat_action = new MyAction("Cat",Cat_icon);//icon Action
		Action Mouse_action = new MyAction("Mouse",Mouse_icon);
		Action Dog_action = new MyAction ("Dog",Dog_icon);
		JMenuBar menuBar = new JMenuBar();        //Menu Settings
		JMenu menu = new JMenu("Country");
		setJMenuBar(menuBar);
		menuBar.add(menu);
		menu.add(Dog_action);
		menu.add(Mouse_action);
		menu.add(Cat_action);
		JToolBar toolBar = new JToolBar(JToolBar.VERTICAL);
		toolBar.setBorder(BorderFactory.createLineBorder(Color.red));
		toolBar.add(Dog_action);
		toolBar.add(Mouse_action);
		toolBar.add(Cat_action);
		add(toolBar,BorderLayout.EAST);
		add(label_img,BorderLayout.CENTER);
	}
	private class MyAction extends AbstractAction {
		String name;
		MyAction(String name,Icon icon){
			super(name,icon);
			putValue(Action.SHORT_DESCRIPTION,"Select the" + name + "flag to display.");
		this.name = name;}
	public void actionPerformed(ActionEvent e){
		if (name.equals("Mouse")){
			label_img.setIcon(Mouse1_img);}
		else if(name.equals("Cat")){
			label_img.setIcon(Cat1_img);}
		else if(name.equals("Dog")){
			label_img.setIcon(Dog1_img);}
		}
	}
	}


