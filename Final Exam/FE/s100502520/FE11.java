package fe.s100502520;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JToolBar;

public class FE11 extends JApplet implements ActionListener{
	private ImageIcon[] picture = new ImageIcon[10];
	private ImageIcon[] icon = new ImageIcon[3];
	private JLabel img = new JLabel();
	private JButton next = new JButton("next");
	private JButton previous = new JButton("previous");
	public int index = 0;
	public int type = 0;
	private JPanel p1 = new JPanel();
	
	public FE11(){
		//讀取picture所需的圖片
		for(int i = 1; i<4; i++){
			picture[i] = new ImageIcon(getClass().getResource("image/cat" + i + ".jpg"));
			picture[i+3] = new ImageIcon(getClass().getResource("image/dog" + i + ".jpg"));
			picture[i+6] = new ImageIcon(getClass().getResource("image/mouse" + i + ".jpg"));
		}
		//讀取icon所需的圖片
		icon[0] = new ImageIcon(getClass().getResource("image/cat_icon.jpg"));
		icon[1] = new ImageIcon(getClass().getResource("image/dog_icon.jpg"));
		icon[2] = new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));
		
		//設定各action
		Action cat_action = new Myaction("cat",icon[0]);
		Action dog_action = new Myaction("dog",icon[1]);
		Action mouse_action = new Myaction("mouse",icon[2]);
		
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("animals");
		setJMenuBar(menuBar);
		menuBar.add(menu);
		//新增icon
		JToolBar toolBar = new JToolBar(JToolBar.VERTICAL);
		toolBar.setBorder(BorderFactory.createLineBorder(Color.RED));
		toolBar.add(cat_action);
		toolBar.add(dog_action);
		toolBar.add(mouse_action);
		
		add(toolBar, BorderLayout.EAST);
		add(img, BorderLayout.CENTER);
		p1.add(previous, BorderLayout.WEST);
		p1.add(next, BorderLayout.EAST);
		add(p1, BorderLayout.SOUTH);
		previous.addActionListener(this);
		next.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e){
		//按下previous按鍵時判斷使否超出相簿並更換圖片
		if(e.getSource() == previous){
			if(type == 0){
				if(index == 1){
					
				}
				else{
					index--;
					img.setIcon(picture[index]);
				}
			}
			else if(type == 1){
				if(index == 4){
					
				}
				else{
					index--;
					img.setIcon(picture[index]);
				}
			}
			if(type == 2){
				if(index == 7){
					
				}
				else{
					index--;
					img.setIcon(picture[index]);
				}
			}
		}
		//按下next按鍵時判斷使否超出相簿並更換圖片
		else if(e.getSource() == next){
			if(type == 0){
				if(index == 3){
					
				}
				else{
					index++;
					img.setIcon(picture[index]);
				}
			}
			else if(type == 1){
				if(index == 6){
					
				}
				else{
					index++;
					img.setIcon(picture[index]);
				}
			}
			if(type == 2){
				if(index == 9){
					
				}
				else{
					index++;
					img.setIcon(picture[index]);
				}
			}
		}
	}
	
	private class Myaction extends AbstractAction implements ActionListener{
		String name;
		Myaction(String name, Icon icon){
			super(name, icon);
			putValue(Action.SHORT_DESCRIPTION, "select the galleries");
			this.name = name;
		}
		//判斷點下各icon時所顯示的圖片
		public void actionPerformed(ActionEvent e){
			if(name.endsWith("cat")){
				type = 0;
				index = 1;
				img.setIcon(picture[1]);
			}
			else if(name.endsWith("dog")){
				type = 1;
				index = 4;
				img.setIcon(picture[4]);
			}
			else if(name.endsWith("mouse")){
				type = 2;
				index =7;
				img.setIcon(picture[7]);
			}
		}
	}
}
