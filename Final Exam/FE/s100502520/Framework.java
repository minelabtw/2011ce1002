package fe.s100502520;

import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Framework extends JFrame implements ActionListener, MouseListener, MouseMotionListener{
	private JLabel label_1 = new JLabel("The Bomber Infrared Screen");
	private JLabel label_2 = new JLabel("Language: ");
	private JLabel label_3 = new JLabel();
	private JLabel label_4 = new JLabel("Position: ");
	private JLabel label_5 = new JLabel();
	private JLabel label_6 = new JLabel("Result: ");
	private JLabel label_7 = new JLabel();
	private JComboBox combo = new JComboBox();
	public Framework() {
		label_1.setBounds(0, 0, 200, 100);
		add(label_1);
		label_2.setBounds(400, 0, 200, 100);
		add(label_2);
		label_4.setBounds(0, 400, 200, 100);
		add(label_4);
		label_6.setBounds(400, 400, 200, 100);
		add(label_6);
		setVisible(true);
	}
}
