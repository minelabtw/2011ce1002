package fe.s100502013;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class FE11 extends JApplet implements ActionListener{
	private ImageIcon[] pictures = new ImageIcon[9];
	private String[] picturename = {"image/cat1.jpg","image/cat2.jpg",
			"image/cat3.jpg","image/dog1.jpg","image/dog2.jpg",
			"image/dog3.jpg","image/mouse1.jpg","image/mouse2.jpg",
			"image/mouse3.jpg"};
	private JMenu menu = new JMenu("Menu");
	private JToolBar tool = new JToolBar();
	private JLabel picturel = new JLabel();
	private JPanel buttonpanel = new JPanel();
	private JButton previousb = new JButton("Previous");
	private JButton nextb = new JButton("Next");
	private MyAction cataction = new MyAction("Cat",new ImageIcon(getClass().getResource("image/cat_icon.jpg")));
	private MyAction dogaction  = new MyAction("Dog",new ImageIcon(getClass().getResource("image/dog_icon.jpg")));
	private MyAction mouseaction = new MyAction("Mouse",new ImageIcon(getClass().getResource("image/mouse_icon.jpg")));
	private int imagec = 0;
	
	public FE11(){ //constructor
		for(int i=0;i<9;i++){
			pictures[i] = new ImageIcon(getClass().getResource(picturename[i]));
		}
		menu.add(cataction);
		menu.add(dogaction);
		menu.add(mouseaction);
		add(menu);
		tool.add(cataction);
		tool.add(dogaction);
		tool.add(mouseaction);
		buttonpanel.setLayout(new GridLayout(1,2));
		buttonpanel.add(previousb);
		buttonpanel.add(nextb);
		add(tool,BorderLayout.NORTH);
		add(picturel,BorderLayout.CENTER);
		add(buttonpanel,BorderLayout.SOUTH);
		picturel.setIcon(pictures[0]);
		previousb.addActionListener(this);
		nextb.addActionListener(this);
	}
	
	public class MyAction extends AbstractAction{ //toolbar action
		String name;
		
		MyAction(String name,Icon icon){
			super(name,icon);
			putValue(Action.SHORT_DESCRIPTION,"");
			this.name = name;
		}
		
		public void actionPerformed(ActionEvent e) {
			if(this.name=="Cat"){
				imagec = 0;
				picturel.setIcon(pictures[0]);
			}
			else if(this.name=="Dog"){
				imagec = 3;
				picturel.setIcon(pictures[3]);
			}
			else if(this.name=="Mouse"){
				imagec = 6;
				picturel.setIcon(pictures[6]);
			}
		}
	}

	public void actionPerformed(ActionEvent e) { //button event
		if(e.getSource()==previousb){ //previous button
			if(imagec==0 || imagec==3 || imagec==6){
				JOptionPane warn = new JOptionPane();
				warn.showMessageDialog(warn, "This is the first photo!");
			}
			else{
				imagec--;
				picturel.setIcon(pictures[imagec]);
			}
		}
		if(e.getSource()==nextb){ //next button
			if(imagec==2 || imagec==5 || imagec==8){
				JOptionPane warn = new JOptionPane();
				warn.showMessageDialog(warn, "This is the last photo!");
			}
			else{
				imagec++;
				picturel.setIcon(pictures[imagec]);
			}
		}
	}
}
