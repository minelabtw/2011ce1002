package fe.s100502013;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class DrawBuildings extends JPanel implements ActionListener{
	private int xBase = 0;
	private int yBase = getHeight()/2;
	private int mouseX = -20;
	private int mouseY = -20;
	private Timer timer = new Timer(200,new TimerListener());
	
	public DrawBuildings(){
		timer.start();
	}
	
	public void setMousePositionX(int x_temp){
		mouseX = x_temp;
	}
	
	public void setMousePositionY(int y_temp){
		mouseY = y_temp;
	}
	
	public int getmousex(){
		return mouseX;
	}
	
	public int getmousey(){
		return mouseY;
	}

	public String getHittingTarget(){
		boolean inA = false;
		boolean inB = false;
		if(mouseX<=xBase+200 && mouseX>=xBase+10 && mouseY<=yBase+200 && mouseY>=yBase){
			inA = true;
		}
		if(mouseX<=xBase+250 && mouseX>=xBase+60 && mouseY<=yBase-50 && mouseY>=yBase+100){
			inB = true;
		}
		if(inA && inB){
			return "Hit both A and B";
		}
		else if(inA && !inB){
			return "Hit A";
		}
		else if(!inA && inB){
			return "Hit B";
		}
		else{
			return "Miss";
		}
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		
		g.setColor(Color.BLACK);
		g.drawString("B",xBase+57, yBase+97);
		g.drawString("A",xBase+7, yBase+197);
		g.drawRect(xBase+10, yBase, 190, 200);
		g.drawRect(xBase+60, yBase-50, 150, 190);
		g.setColor(Color.RED);
		g.fillOval(mouseX, mouseY, 10, 10);
	}
	
	public void actionPerformed(ActionEvent arg0) {
		
	}
	
	public class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			xBase++;
			repaint();
		}
	}
}
