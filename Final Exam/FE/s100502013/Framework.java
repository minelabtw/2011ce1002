package fe.s100502013;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

public class Framework extends JFrame implements ActionListener{
	private JPanel total = new JPanel();
	private JPanel upperp = new JPanel();
	private JLabel JLabel_1 = new JLabel("The Bomber Infrared Screen");
	private JPanel langua = new JPanel();
	private JLabel JLabel_2 = new JLabel("Language:");
	private JComboBox comboc = new JComboBox();
	private JLabel JLabel_3 = new JLabel();
	private DrawBuildings drawg = new DrawBuildings();
	private JPanel lowerp = new JPanel();
	private JLabel JLabel_4 = new JLabel("Position:");
	private JLabel JLabel_5 = new JLabel();
	private JLabel JLabel_6 = new JLabel("Result:");
	private JLabel JLabel_7 = new JLabel();
	
	public Framework(){
		total.setLayout(new BorderLayout(1,1));
		langua.setLayout(new GridLayout(1,2));
		langua.add(JLabel_2);
		langua.add(comboc);
		upperp.setLayout(new GridLayout(1,2));
		upperp.add(JLabel_1);
		upperp.add(langua);
		lowerp.setLayout(new GridLayout(1,4));
		lowerp.add(JLabel_4);
		lowerp.add(JLabel_5);
		lowerp.add(JLabel_6);
		lowerp.add(JLabel_7);
		JLabel_3.add(drawg);
		JLabel_3.setBorder(new LineBorder(Color.BLACK, 1));
		total.add(upperp,BorderLayout.NORTH);
		total.add(JLabel_3,BorderLayout.CENTER);
		total.add(lowerp,BorderLayout.SOUTH);
		add(total);
	}

	public void actionPerformed(ActionEvent e) {
		
	}
}
