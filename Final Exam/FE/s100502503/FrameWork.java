package fe.s100502503;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.*;

public class FrameWork extends JFrame 
{
	//private ResourceBundle rb = ResourceBundle.getBundle("MyResources", Locale.ENGLISH);
	
	private JLabel l1 = new JLabel("Title");
	private JLabel l2 = new JLabel("Language");
	private JComboBox jcb = new JComboBox();
	private Panel  l3 = new Panel();
	private JLabel l4 = new JLabel("Position");
	private JLabel l5 = new JLabel();
	private JLabel l6 = new JLabel("Result");
	private JLabel l7 = new JLabel();
	
	private String[] contry = {"En", "Zh", "Jp", "Fr"};
	
	private JPanel p1 = new JPanel(new GridLayout(1, 3, 5, 5));
	private JPanel p2 = new JPanel(new GridLayout(1, 4, 5, 5));
	
	protected int userX, userY;
	
	private Locale[] locale = {Locale.ENGLISH, Locale.JAPANESE, Locale.CHINESE, Locale.FRANCE};
	
	public FrameWork()
	{
		l1.setBackground(Color.WHITE);
		
		p1.add(l1);
		p1.add(l2);
		p1.add(jcb);
		
		p2.add(l4);
		p2.add(l5);
		p2.add(l6);
		p2.add(l7);
		
		add(p1, BorderLayout.NORTH);
		add(l3, BorderLayout.CENTER);
		add(p2, BorderLayout.SOUTH);
	}
	
	public void setjcb()
	{
		for(int i = 0; i < contry.length; i++)
		{
			jcb.addItem(contry[i]);
		}
	}
	public void setMousePositionX(int x_temp)
	{
		userX = l3.getX();
	}
	
	public void setMousePositionY(int y_temp)
	{
		userY = l3.getY();
	}
	
	public void decideMP()
	{
	}
	
	private void updateString()
	{
		
	}
	
}
