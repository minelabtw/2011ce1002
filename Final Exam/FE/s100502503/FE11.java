package fe.s100502503;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class FE11 extends JApplet
{
	private ImageIcon[] picjtb = new ImageIcon[3];
	private ImageIcon[] pic = new ImageIcon[9];
	private JMenuBar m = new JMenuBar();
	private JMenu menu = new JMenu("Animals");
	private JToolBar jtb = new JToolBar(JToolBar.VERTICAL);

	private JLabel l1;
	private JButton previous = new JButton("<-");
	private JButton next = new JButton("->");
	private JPanel p2 = new JPanel(new GridLayout(1, 2, 5, 5));
	
	
	public FE11()
	{
		pic[0] = new ImageIcon(getClass().getResource("image/cat1.jpg"));
		pic[1] = new ImageIcon(getClass().getResource("image/cat2.jpg"));
		pic[2] = new ImageIcon(getClass().getResource("image/cat3.jpg"));
		pic[3] = new ImageIcon(getClass().getResource("image/dog1.jpg"));
		pic[4] = new ImageIcon(getClass().getResource("image/dog2.jpg"));
		pic[5] = new ImageIcon(getClass().getResource("image/dog3.jpg"));
		pic[6] = new ImageIcon(getClass().getResource("image/mouse1.jpg"));
		pic[7] = new ImageIcon(getClass().getResource("image/mouse2.jpg"));
		pic[8] = new ImageIcon(getClass().getResource("image/mouse3.jpg"));
		
		picjtb[0] = new ImageIcon(getClass().getResource("image/cat_icon.jpg"));
		picjtb[1] = new ImageIcon(getClass().getResource("image/dog_icon.jpg"));
		picjtb[2] = new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));
		Action cat = new MyAction("cat", picjtb[0]);
		Action dog = new MyAction("dog", picjtb[1]);
		Action mouse = new MyAction("mouse", picjtb[2]);
		
		this.setJMenuBar(m);
		m.add(menu);
		
		menu.add(cat);
		menu.add(dog);
		menu.add(mouse);
		
		jtb.add(cat);
		jtb.add(dog);
		jtb.add(mouse);
		
		//l1.setIcon(pic[0]);
		//add(l1, BorderLayout.NORTH);
		
		add(jtb, BorderLayout.EAST);
		p2.add(previous);
		p2.add(next);
		add(p2, BorderLayout.SOUTH);
		
		previous.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0)
			{
				
			}
		});
		next.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{

				
			}
		});
	}

	private class MyAction extends AbstractAction
	{
		String name;
		MyAction(String name, Icon icon) 
		{
			super(name);
			putValue(Action.SHORT_DESCRIPTION, "Time wait for no ones");
			this.name = name;
		}
		@Override
		public void actionPerformed(ActionEvent arg0) 
		{
			if(name.equals("cat"))
				;
			else if(name.equals("dog"))
				;
			else if(name.equals("mouse"))
				;
		}
	}
}


//<applet = code></applet>