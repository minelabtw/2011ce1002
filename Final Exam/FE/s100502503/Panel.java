package fe.s100502503;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.*;

public class Panel extends JLabel implements ActionListener, MouseListener, MouseMotionListener
{
	private Timer timer = new Timer(200, new TimerListener());
	private int mx, my;
	private int xb, yb;
	
	public Panel()
	{
		timer.start();
	}
	public void paintComponent(Graphics g)
	{
		super.paintComponents(g);
		xb = getWidth()/2;
		yb = getHeight()/2;
		
		int[] x = {xb+60, xb+250, xb+60, xb+250, xb+10, xb+200, xb+10, xb+200};
		int[] y = {yb-50, yb-50, yb+100, yb+100, yb, yb, yb+200, yb+200};
		
		g.drawLine(x[0], y[0], x[1], y[1]);
		g.drawLine(x[0], y[0], x[2], y[2]);
		g.drawLine(x[1], y[1], x[3], y[3]);
		g.drawLine(x[2], y[2], x[3], y[3]);
		g.drawLine(x[4], y[4], x[5], y[5]);
		g.drawLine(x[4], y[4], x[6], y[6]);
		g.drawLine(x[5], y[5], x[7], y[7]);
		g.drawLine(x[6], y[6], x[7], y[7]);		
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseClicked(MouseEvent e)
	{
		mx = e.getX();
		my = e.getY();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public int getX()
	{
		return mx;
	}
	
	public int getY()
	{
		return my;
	}

	class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) 
		{
			xb += 20;
			
			repaint();
		}
	}
}
