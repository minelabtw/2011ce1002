package fe.s100502029;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Locale;
import java.util.ResourceBundle;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Framework extends JFrame implements ActionListener, MouseListener, MouseMotionListener {
	private Locale usa = Locale.US;
	private Locale china = Locale.CHINESE;
	private Locale japan = Locale.JAPANESE;
	private Locale france = Locale.FRENCH;
	private JComboBox jcbLocale = new JComboBox();
	
	private Locale locale = usa;
	private ResourceBundle res = ResourceBundle.getBundle("Confidential", locale);
	private JLabel jlblTitle = new JLabel(res.getString("Title"), JLabel.CENTER);
	private JLabel jlblLanguage = new JLabel(res.getString("Language"), JLabel.RIGHT);
	private JLabel jlblPosition = new JLabel(res.getString("Position"), JLabel.RIGHT);
	private JLabel jlblResult = new JLabel(res.getString("Result"), JLabel.RIGHT);
	private JLabel jlbl5 = new JLabel("0,0", JLabel.LEFT);
	private JLabel jlbl7 = new JLabel("Nothing", JLabel.LEFT);
	
	private DrawBuildings build = new DrawBuildings();
	private int mousePositionX;
	private int mousePositionY;
	
	// change string by locale
	private void updateStrings() {
		res = ResourceBundle.getBundle("Confidential", locale);
		jlblTitle.setText(res.getString("Title"));
		jlblLanguage.setText(res.getString("Language"));
		jlblPosition.setText(res.getString("Position"));
		jlblResult.setText(res.getString("Result"));
	}
	
	public Framework() {
		// add item in combo box
		jcbLocale.addItem(usa.getDisplayName());
		jcbLocale.addItem(china.getDisplayName());
		jcbLocale.addItem(japan.getDisplayName());
		jcbLocale.addItem(france.getDisplayName());
		
		JPanel p1 = new JPanel();
		p1.add(jlblLanguage);
		p1.add(jcbLocale);
		
		JPanel p2 = new JPanel(new GridLayout(1, 2));
		p2.add(jlblTitle);
		p2.add(p1);
		
		JPanel p3 = new JPanel(new GridLayout(1, 4));
		p3.add(jlblPosition);
		p3.add(jlbl5);
		p3.add(jlblResult);
		p3.add(jlbl7);
		
		add(p2, BorderLayout.NORTH);
		add(build, BorderLayout.CENTER);
		add(p3, BorderLayout.SOUTH);
		
		// add actionlistener to combo box
		jcbLocale.addActionListener(this);
		
		// add mouselistener and move motion listener
		addMouseListener(this);
		addMouseMotionListener(this);
	}
	
	public void actionPerformed(ActionEvent e) {
		// set locale
		if (e.getSource() == jcbLocale) {
			if (jcbLocale.getSelectedIndex() == 0)
				locale = usa;
			else if (jcbLocale.getSelectedIndex() == 1)
				locale = china;
			else if (jcbLocale.getSelectedIndex() == 2)
				locale = japan;
			else if (jcbLocale.getSelectedIndex() == 3)
				locale = france;
			
			updateStrings(); // call update String method
		}
	}

	public void mouseDragged(MouseEvent e) {
		
	}

	// show �ƹ��y�� on screen
	public void mouseMoved(MouseEvent e) {
		build.mouseX = e.getX() - 8;
		build.mouseY = e.getY() - 67;
		jlbl5.setText((e.getX() - 8) + "," + (e.getY() - 67));
		build.determined();
		if (build.inA && build.inB)
			jlbl7.setText("Both A & B");
		else if (build.inA && !build.inB)
			jlbl7.setText("Building A");
		else if (!build.inA && build.inB)
			jlbl7.setText("Building B");
		else if (!build.inA && !build.inB)
			jlbl7.setText("Nothing");
	}

	public void mouseClicked(MouseEvent e) {
	
	}

	public void mouseEntered(MouseEvent e) {

	}

	public void mouseExited(MouseEvent e) {

	}

	// pass the position of mouse to the build
	public void mousePressed(MouseEvent e) {
		build.setBombPositionX(e.getX() - 8);
		build.setBombPositionY(e.getY() - 67);
		build.repaint();
	}

	public void mouseReleased(MouseEvent e) {

	}
}
