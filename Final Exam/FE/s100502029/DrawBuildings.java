package fe.s100502029;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.Timer;

public class DrawBuildings extends JPanel {
	private Timer timer = new Timer(200, new TimerListener());
	private int xBase;
	private int yBase;
	protected int mouseX;
	protected int mouseY;
	private int bombX;
	private int bombY;
	protected boolean inA = false;
	protected boolean inB = false;
	
	// initialize the variables
	public DrawBuildings() {
		xBase = 0;
		bombX = -5;
		bombY = -5;
		timer.start();
	}
	
	// set x-coordinate bomb position
	public void setBombPositionX(int x_temp) {
		bombX = x_temp;
	}
	
	// set y-coordinate bomb position
	public void setBombPositionY(int y_temp) {
		bombY = y_temp;
	}
	
	// 判斷滑鼠是否在建築物A B裡
	public void determined() {
		if (mouseX >= xBase + 60 && mouseX <= xBase + 250 && mouseY >= yBase - 50 && mouseY <= yBase + 100)
			inB = true;
		else
			inB = false;
		
		if (mouseX >= xBase + 10 && mouseX <= xBase + 200 && mouseY >= yBase && mouseY <= yBase + 200)
			inA = true;
		else
			inA = false;
	}
	
	// paint building
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (xBase > getWidth())
			xBase = -190;
		
		yBase = getHeight() / 2;
		
		g.setColor(Color.red);
		g.drawLine(xBase + 60, yBase - 50, xBase + 250, yBase - 50);
		g.drawLine(xBase + 250, yBase - 50, xBase + 250, yBase + 100);
		g.drawLine(xBase + 250, yBase + 100, xBase + 60, yBase + 100);
		g.drawLine(xBase + 60, yBase + 100, xBase + 60, yBase - 50);
		g.drawString("B", xBase + 50, yBase + 100);
		
		g.setColor(Color.blue);
		g.drawLine(xBase + 10, yBase, xBase + 200, yBase);
		g.drawLine(xBase + 200, yBase, xBase + 200, yBase + 200);
		g.drawLine(xBase + 200, yBase + 200, xBase + 10, yBase + 200);
		g.drawLine(xBase + 10, yBase + 200, xBase + 10, yBase);
		g.drawString("A", xBase, yBase + 200);
		
		g.setColor(Color.BLACK);
		g.fillOval(bombX - 5, bombY - 5, 10, 10);
	}
	
	public class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			xBase += 1;
			repaint();
		}
	}
}
