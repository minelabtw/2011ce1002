package fe.s100502029;

import java.awt.BorderLayout;
import java.awt.event.*;
import javax.swing.*;

public class FE11 extends JApplet {
	// store images in array
	private ImageIcon[] image = {
			new ImageIcon(getClass().getResource("image/cat1.jpg")),
			new ImageIcon(getClass().getResource("image/cat2.jpg")),
			new ImageIcon(getClass().getResource("image/cat3.jpg")),
			new ImageIcon(getClass().getResource("image/dog1.jpg")),
			new ImageIcon(getClass().getResource("image/dog2.jpg")),
			new ImageIcon(getClass().getResource("image/dog3.jpg")),
			new ImageIcon(getClass().getResource("image/mouse1.jpg")),
			new ImageIcon(getClass().getResource("image/mouse2.jpg")),
			new ImageIcon(getClass().getResource("image/mouse3.jpg"))};
	private ImageIcon cat_icon = new ImageIcon(getClass().getResource("image/cat_icon.jpg"));
	private ImageIcon dog_icon = new ImageIcon(getClass().getResource("image/dog_icon.jpg"));
	private ImageIcon mouse_icon = new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));
	
	private JLabel jlblPicture = new JLabel();
	private JButton jbtPrevious = new JButton("Previous");
	private JButton jbtNext = new JButton("Next");
	private int flag = 0;
	
	public FE11() {
		// create three action of MyAction
		Action catAction = new MyAction("cat", cat_icon);
		Action dogAction = new MyAction("dog", dog_icon);
		Action mouseAction = new MyAction("mouse", mouse_icon);
		
		// create Menu bar
		JMenuBar jmb = new JMenuBar();
		setJMenuBar(jmb);
		
		// add menu in menu bar
		JMenu animalMenu = new JMenu("Animals Gallery");
		jmb.add(animalMenu);
		
		animalMenu.add(catAction);
		animalMenu.add(dogAction);
		animalMenu.add(mouseAction);
		
		JToolBar jtb = new JToolBar(JToolBar.VERTICAL);
		jtb.add(catAction);
		jtb.add(dogAction);
		jtb.add(mouseAction);
		
		JPanel p1 = new JPanel();
		p1.add(jbtPrevious);
		p1.add(jbtNext);
		add(p1, BorderLayout.SOUTH);
		add(jtb, BorderLayout.EAST);
		jlblPicture.setText(null);
		add(jlblPicture, BorderLayout.CENTER);
		
		// add actionlistener to jbutton previous
		jbtPrevious.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (flag % 3 == 0) {
					System.err.println("This is the first picture");
				}
				else {
					flag--;
					jlblPicture.setIcon(image[flag]);
				}
			}
		});
		
		// add actionlistener to jbutton next
		jbtNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (flag % 3 == 2) {
					System.err.println("This is the last picture");
				}
				else {
					flag++;
					jlblPicture.setIcon(image[flag]);
				}
			}
		});
	}
	
	private class MyAction extends AbstractAction {
		String gallery;
		MyAction(String gallery, Icon icon) {
			super(gallery, icon);
			this.gallery = gallery;
		}
		public void actionPerformed(ActionEvent e) {
			if (gallery.equals("cat"))
				flag = 0;
			else if (gallery.equals("dog"))
				flag = 3;
			else if (gallery.equals("mouse"))
				flag = 6;
			
			jlblPicture.setIcon(image[flag]);
		}
	}
}
