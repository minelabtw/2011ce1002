package fe.s100502011;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Locale;
import java.util.ResourceBundle;
public class Framework extends JFrame implements ActionListener ,MouseListener,MouseMotionListener{
	DrawBuildings build = new DrawBuildings();
	public JLabel title = new JLabel("The Boomber Infrared Screen");
	public Locale[] availLocale = Locale.getAvailableLocales();
	public JLabel language = new JLabel("Language");
	public JPanel building = new JPanel();
	public JLabel position = new JLabel("Position");
	public JLabel current = new JLabel("");
	public JLabel result = new JLabel("result");
	public JLabel finalRe = new JLabel();
	public JPanel upper = new JPanel();
	public JPanel down = new JPanel();
	public JComboBox jcbLocale = new JComboBox(availLocale);
	ResourceBundle res = ResourceBundle.getBundle("Confidential");
	public Framework(){
		//set arrangement
		upper.setLayout(new GridLayout(1,3));
		down.setLayout(new GridLayout(1,4));
		setLayout(new BorderLayout(5,5));
		upper.add(title);
		upper.add(language);
		upper.add(jcbLocale);
		down.add(position);
		down.add(current);
		down.add(result);
		down.add(finalRe);
		building.setLayout(new GridLayout(1,1));
		building.add(build);
		//add to frame
		add(upper,BorderLayout.NORTH);
		add(building,BorderLayout.CENTER);
		add(down,BorderLayout.SOUTH);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==jcbLocale){
			
		}
	}


	@Override
	public void mousePressed(MouseEvent e) {
		build.setMousePositionX(e.getX()-13);
		build.setMousePositionY(e.getY()-67);
		build.press=1;
		current.setText(""+build.getMouseX()+","+build.getMouseY());
		// inside both
		if(build.getMouseX()>=(build.xBase+60)&&build.getMouseX()<=(build.xBase+200)&&build.getMouseY()>=build.yBase&&build.getMouseY()<=(build.yBase+100))
			finalRe.setText("A&B");
		//inside a
		else if(build.getMouseX()<=(build.xBase+200)&&build.getMouseX()>=(build.xBase+10)&&build.getMouseY()<=(build.yBase+200)&&build.getMouseY()>=(build.yBase)){
			finalRe.setText("A");
		}
		//inside b
		else if(build.getMouseX()<=(build.xBase+250)&&build.getMouseX()>=(build.xBase+60)&&build.getMouseY()<=(build.yBase+100)&&build.getMouseY()>=(build.yBase-50)){
			finalRe.setText("B");
		}
		else
			finalRe.setText("Nothing");
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}



}
