package fe.s100502011;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class FE11 extends JApplet implements ActionListener{
	int count=0;
	int choose=1;
	JOptionPane message =new JOptionPane();
	public ImageIcon catIcon = new ImageIcon(getClass().getResource("image/cat_icon.JPG"));
	public ImageIcon dogIcon = new ImageIcon(getClass().getResource("image/dog_icon.JPG"));
	public ImageIcon mouseIcon = new ImageIcon(getClass().getResource("image/mouse_icon.JPG"));
	public ImageIcon cat1 = new ImageIcon(getClass().getResource("image/cat1.JPG"));
	public ImageIcon cat2 = new ImageIcon(getClass().getResource("image/cat2.JPG"));
	public ImageIcon cat3 = new ImageIcon(getClass().getResource("image/cat3.JPG"));
	public ImageIcon dog1 = new ImageIcon(getClass().getResource("image/dog1.JPG"));
	public ImageIcon dog2 = new ImageIcon(getClass().getResource("image/dog2.JPG"));
	public ImageIcon dog3 = new ImageIcon(getClass().getResource("image/dog3.JPG"));
	public ImageIcon mouse1 = new ImageIcon(getClass().getResource("image/mouse1.JPG"));
	public ImageIcon mouse2 = new ImageIcon(getClass().getResource("image/mouse2.JPG"));
	public ImageIcon mouse3 = new ImageIcon(getClass().getResource("image/mouse3.JPG"));
	JMenuBar animal = new JMenuBar();
	JMenu jmAni= new JMenu("Animal");
	JToolBar jtbAni = new JToolBar(JToolBar.VERTICAL);
	public ImageIcon[] Animal ={cat1,cat2,cat3,dog1,dog2,dog3,mouse1,mouse2,mouse3};
	public JButton next = new JButton(">");
	public JButton previous = new JButton("<");
	public JLabel pic = new JLabel();
	public FE11(){
		// give proporty to action
		Action catAction = new MyAction("Cat",catIcon);
		Action dogAction = new MyAction("Dog",dogIcon);
		Action mouseAction = new MyAction("Mouse",mouseIcon);
		//set menu bar
		setJMenuBar(animal);
		animal.add(jmAni);
		jmAni.add(catAction);
		jmAni.add(dogAction);
		jmAni.add(mouseAction);
		// set tool bar
		jtbAni.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		jtbAni.add(catAction);
		jtbAni.add(dogAction);
		jtbAni.add(mouseAction);
		
		// to arrange the frame
		this.setLayout(null);
		previous.setBounds(200, 500, 50, 50);
		next.setBounds(350, 500, 50, 50);
		pic.setBounds(0, 100, 550, 400);
		jtbAni.setBounds(550,0,50,600);
		add(pic);
		add(previous);
		add(next);
		add(jtbAni,BorderLayout.EAST);
		//add action
		previous.addActionListener(this);
		next.addActionListener(this);
		
	}
	
	private class MyAction extends AbstractAction{
		String name;
		MyAction(String name,Icon icon){
			super(name,icon);
			this.name=name;
		}
		// action performed
		public void actionPerformed(ActionEvent e) {
			
			if(name.equals("Cat")){//choose cat
				choose=1;
				count=0;
				pic.setIcon(Animal[count]);
			}
			else if(name.equals("Dog")){ // choose dog
				choose=2;
				count=3;
				pic.setIcon(Animal[count]);
			}
			else if(name.equals("Mouse")){ // choose mouse
				choose=3;
				count=6;
				pic.setIcon(Animal[count]);
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==previous){
			if(choose==1&&count==0) // first picture
				message.showMessageDialog(null, "This is the first picture");
			if(choose==2&&count==3)// first picture
				message.showMessageDialog(null, "This is the first picture");
			if(choose==3&&count==6)// first picture
				message.showMessageDialog(null, "This is the first picture");
			if(choose==1&&count>=1){// to previous
				count--;
				pic.setIcon(Animal[count]);
			}
			if(choose==2&&count>=4){// to previous
				count--;
				pic.setIcon(Animal[count]);
			}
			if(choose==3&&count>=7){ // to previous
				count--;
				pic.setIcon(Animal[count]);
			}
		}
		if(e.getSource()==next){
			if(choose==1&&count==2) // last picture
				message.showMessageDialog(null, "This is the last picture");
			if(choose==2&&count==5)  // last picture
				message.showMessageDialog(null, "This is the last picture");
			if(choose==3&&count==8)  // last picture
				message.showMessageDialog(null, "This is the last picture");
			if(choose==1&&count<=1){ // to next
				count++;
				pic.setIcon(Animal[count]);
			}
			if(choose==2&&count<=4){ // to next
				count++;
				pic.setIcon(Animal[count]);
			}
			if(choose==3&&count<=7){ // to next
				count++;
				pic.setIcon(Animal[count]);
			}
			
		}
	}
}
