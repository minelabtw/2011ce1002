package fe.s100502011;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

public class DrawBuildings extends JPanel{
	Timer timer = new Timer(200, new timerListener());
	public int[] x1= new int[4];
	public int[] y1 = new int[4];
	public int[] x2= new int[4];
	public int[] y2 = new int[4];
	public int mousex=0;
	public int mousey=0;
	int dir =1;
	int xBase;
	int yBase;
	int count=0;
	int press=0;
	public DrawBuildings(){
		timer.start();
	}
	public void paintComponent(Graphics g){
		//initial value
		if(count==0){
			xBase=0;
			yBase=getHeight()/2;
			x1=new int[]{xBase+10,xBase+200,xBase+200,xBase+10};
			x2=new int[]{xBase+60,xBase+250,xBase+250,xBase+60};
			y1=new int[]{yBase+200,yBase+200,yBase,yBase};
			y2=new int[]{yBase-50,yBase-50,yBase+100,yBase+100};
		}
		super.paintComponent(g);
		x1=new int[]{xBase+10,xBase+200,xBase+200,xBase+10};
		x2=new int[]{xBase+60,xBase+250,xBase+250,xBase+60};
		y1=new int[]{yBase+200,yBase+200,yBase,yBase};
		y2=new int[]{yBase-50,yBase-50,yBase+100,yBase+100};
		//draw buildings
		g.drawString("A",xBase, yBase+200);
		g.drawString("B",xBase+50, yBase+100);
		g.drawPolygon(x1, y1, x1.length);
		g.drawPolygon(x2, y2, x2.length);
		count++;
		if(dir==1){
			xBase+=10;
		}
		if(dir==0)
			xBase-=10;
		if(x2[1]>570){
			dir=0;
		}
		if(x1[0]<=0){
			dir=1;
		}
		// mouse pressed
		if(press==1){
			g.setColor(Color.RED);
			g.drawOval(mousex,mousey,10,10);
			g.fillOval(mousex,mousey,10,10);
		}
	}
	//set mouse x
	public void setMousePositionX(int x_temp){
		mousex=x_temp;
	}
	//set mouse y
	public void setMousePositionY(int y_temp){
		mousey=y_temp;
	}
	//get mouse x
	public int getMouseX(){
		return mousex;
	}
	//get mouse y
	public int getMouseY(){
		return mousey;
	}
	//action
	class timerListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			System.out.println(mousex+"   "+mousey);
			repaint();
		}
	}
}
