package fe1.s995002056;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToolBar;

public class FE11 extends JApplet implements ActionListener {
	ImageIcon i_cat1 = new ImageIcon("image/cat_icon.jpg");
	ImageIcon i_cat2 = new ImageIcon("image/cat1.jpg");
	ImageIcon i_cat3 = new ImageIcon("image/cat2.jpg");
	ImageIcon i_cat4 = new ImageIcon("image/cat3.jpg");
	ImageIcon i_dog1 = new ImageIcon("image/dog_icon.jpg");
	ImageIcon i_dog2 = new ImageIcon("image/dog1.jpg");
	ImageIcon i_dog3 = new ImageIcon("image/dog2.jpg");
	ImageIcon i_dog4 = new ImageIcon("image/dog3.jpg");
	ImageIcon i_mou1 = new ImageIcon("image/mouse_icon.jpg");
	ImageIcon i_mou2 = new ImageIcon("image/mouse1.jpg");
	ImageIcon i_mou3 = new ImageIcon("image/mouse2.jpg");
	ImageIcon i_mou4 = new ImageIcon("image/mouse3.jpg");

	JButton b_cat = new JButton(i_cat1);
	JButton b_dog = new JButton(i_dog1);
	JButton b_mou = new JButton(i_mou1);
	JButton b_left = new JButton("��");
	JButton b_right = new JButton("��");

	JLabel l_pic1 = new JLabel(i_cat2);
	JLabel l_pic2 = new JLabel(i_cat3);
	JLabel l_pic3 = new JLabel(i_cat4);
	JLabel l_pic4 = new JLabel(i_dog2);
	JLabel l_pic5 = new JLabel(i_dog3);
	JLabel l_pic6 = new JLabel(i_dog4);
	JLabel l_pic7 = new JLabel(i_mou2);
	JLabel l_pic8 = new JLabel(i_mou3);
	JLabel l_pic9 = new JLabel(i_mou4);

	JToolBar p1 = new JToolBar();
	JToolBar p2 = new JToolBar();
	JPanel p3 = new JPanel();

	int count = 1;

	public FE11() {

		b_cat.addActionListener(this);
		b_dog.addActionListener(this);
		b_mou.addActionListener(this);
		b_left.addActionListener(this);
		b_right.addActionListener(this);

		p1.setLayout(new GridLayout(4, 1, 10, 10));
		setLayout(null);

		p1.setBounds(400, 10, 50, 200);
		p1.add(b_cat);
		p1.add(b_dog);
		p1.add(b_mou);
		add(p1);

		p2.setLayout(new GridLayout(1, 2, 10, 10));
		p2.setBounds(100, 300, 150, 20);
		p2.add(b_left);
		p2.add(b_right);
		add(p2);


		p3.setBounds(0, 0, 400, 300);
		p3.add(l_pic1);
		add(p3);

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == b_cat) {
			count = 1;
			p3.removeAll();
			p3.add(l_pic1);
			p3.repaint();
		}
		if (e.getSource() == b_dog) {
			count = 4;
			p3.removeAll();
			p3.add(l_pic4);
			p3.repaint();

		}
		if (e.getSource() == b_mou) {
			count = 7;
			p3.removeAll();
			p3.add(l_pic7);
			p3.repaint();
		}
		if (e.getSource() == b_left) {
			count--;
		}
		if (e.getSource() == b_right) {
			count++;
		}
	}

}
