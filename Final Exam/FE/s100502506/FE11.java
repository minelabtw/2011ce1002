package fe.s100502506;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;

public class FE11 extends JApplet
{
	private ImageIcon[] images=new ImageIcon[12];
	private JToolBar tb=new JToolBar(JToolBar.VERTICAL);
	private JMenu mu=new JMenu("Option");
	private JMenuBar mub=new JMenuBar();
	private JMenuItem mi[]=new JMenuItem[3];
	private JButton iconButton[]=new JButton[3];
	private JLabel disJLabel=new JLabel();
	private JButton nextbButton=new JButton();
	private JButton prebButton =new JButton();
	private int nowIndex;
	private int nowType;
	private JPanel conPanel=new JPanel();
	public FE11()//inti JApplet
	{
		//load pictures
		for(int i=0;i<12;i++)
			images[i]=new ImageIcon();
		images[0]=new ImageIcon(getClass().getResource("image/cat_icon.jpg"));
		images[1]=new ImageIcon(getClass().getResource("image/dog_icon.jpg"));
		images[2]=new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));
		
		images[3]=new ImageIcon(getClass().getResource("image/cat1.jpg"));
		images[4]=new ImageIcon(getClass().getResource("image/cat2.jpg"));
		images[5]=new ImageIcon(getClass().getResource("image/cat3.jpg"));
		
		images[6]=new ImageIcon(getClass().getResource("image/dog1.jpg"));
		images[7]=new ImageIcon(getClass().getResource("image/dog2.jpg"));
		images[8]=new ImageIcon(getClass().getResource("image/dog3.jpg"));
		
		images[9]=new ImageIcon(getClass().getResource("image/mouse1.jpg"));
		images[10]=new ImageIcon(getClass().getResource("image/mouse2.jpg"));
		images[11]=new ImageIcon(getClass().getResource("image/mouse3.jpg"));
		//add to toolbar
		for(int i=0;i<3;i++)
		{
			iconButton[i]=new JButton();
			iconButton[i].setIcon(images[i]);
			tb.add(iconButton[i]);
		}
		nowIndex=0;
		nowType=0;
		//add to menu
		for(int i=0;i<3;i++)
		{
			mi[i]=new JMenuItem();
			mi[i].setIcon(images[i]);
			mu.add(mi[i]);
		}
		//button next
		nextbButton.setText("��");
		nextbButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if(nowIndex==2)
				{
					String message=String.format("This is the final page");
					JOptionPane.showMessageDialog(null, message);
				}
				else 
				{
					nowIndex++;
					if(nowType==0)
						disJLabel.setIcon(images[nowIndex+3]);
					else if(nowType==1)
						disJLabel.setIcon(images[nowIndex+3*2]);
					else if(nowType==2)
						disJLabel.setIcon(images[nowIndex+3*3]);
				}
				
			}
		});
		//button pre
		prebButton.setText("��");
		prebButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				if(nowIndex==0)
				{
					String message=String.format("This is the first page");
					JOptionPane.showMessageDialog(null, message);
				}
				else
				{
					nowIndex--;
					if(nowType==0)
						disJLabel.setIcon(images[nowIndex+3]);
					else if(nowType==1)
						disJLabel.setIcon(images[nowIndex+3*2]);
					else if(nowType==2)
						disJLabel.setIcon(images[nowIndex+3*3]);
				}
			}
		});
		//buttons and action listener
		iconButton[0].addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				nowIndex=0;
				nowType=0;
				disJLabel.setIcon(images[3]);
			}
		});
		iconButton[1].addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				nowIndex=0;
				nowType=1;
				disJLabel.setIcon(images[6]);
			}
		});
		iconButton[2].addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				nowIndex=0;
				nowType=2;
				disJLabel.setIcon(images[9]);
			}
		});
		mi[0].addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				nowIndex=0;
				nowType=0;
				disJLabel.setIcon(images[3]);
			}
		});
		mi[1].addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				nowIndex=0;
				nowType=1;
				disJLabel.setIcon(images[6]);
			}
		});
		mi[2].addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				nowIndex=0;
				nowType=2;
				disJLabel.setIcon(images[9]);
			}
		});
		mub.add(mu);
		conPanel.add(prebButton);
		conPanel.add(nextbButton);
		conPanel.setLayout(new GridLayout(1,2));
		add(mub,BorderLayout.NORTH);
		add(tb,BorderLayout.EAST);
		add(disJLabel,BorderLayout.CENTER);
		add(conPanel,BorderLayout.SOUTH);
		
	}
	public static void main(String argc[])
	{
		
	}
}
