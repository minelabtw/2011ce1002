package fe.s100502506;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Calendar;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Framework extends JFrame
{
	private DrawBuildings draw=new DrawBuildings();
	
	private ResourceBundle myres = ResourceBundle.getBundle("Confidential");//load Resource
	private Locale locales[] =Calendar.getAvailableLocales();
	private Locale locale;
	private JLabel jLabel_1=new JLabel("The Bomber Infrared Screen");
	private JLabel jLabel_2=new JLabel("Language:");
	private JLabel jLabel_4=new JLabel("Position");
	private JLabel jLabel_5=new JLabel();
	private JLabel jLabel_6=new JLabel("Result");
	private JLabel jLabel_7=new JLabel();
	
	private JComboBox selectBox=new JComboBox();
	
	private JPanel panel1=new JPanel();
	private JPanel panel2=new JPanel();
	public Framework()//framework
	{
		
		jLabel_1.setText(myres.getString("Title"));
		jLabel_2.setText(myres.getString("Language"));
		jLabel_4.setText(myres.getString("Position"));
		jLabel_6.setText(myres.getString("Result"));
		
		for (int i = 0; i < locales.length; i++)
			selectBox.addItem(locales[i].getDisplayName());
		selectBox.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				 locale = locales[selectBox.getSelectedIndex()];
				 updateStrings();
			}
		});
		panel1.setLayout(new GridLayout(1,3));
		panel1.add(jLabel_1);
		panel1.add(jLabel_2);
		panel1.add(selectBox);
		panel2.setLayout(new GridLayout(1,4));
		panel2.add(jLabel_4);
		panel2.add(jLabel_5);
		panel2.add(jLabel_6);
		panel2.add(jLabel_7);
		add(panel1,BorderLayout.NORTH);
		add(draw);
		add(panel2,BorderLayout.SOUTH);
		
		addMouseListener(new MouseListener() //add mouselistener
		{
			
			@Override
			public void mouseReleased(MouseEvent arg0) 
			{
			}
			@Override
			public void mousePressed(MouseEvent e) 
			{
				int type=0;
				int mouse_X=e.getX()-10;
				int mouse_Y=e.getY()-60;
				jLabel_5.setText(mouse_X+","+mouse_Y);
				draw.setMouse_X(mouse_X);
				draw.setMouse_Y(mouse_Y);
				//check
				if(checkInRect(draw.getfirst_rec_X(),draw.getfirst_rec_Y(),draw.getfirst_Xrange(),draw.getfirst_Yrange(), mouse_X,mouse_Y))
				{
					type=1;
				}
					
				if(checkInRect(draw.getsec_rec_X(),draw.getsec_rec_Y(),draw.getsec_Xrange(),draw.getsec_Yrange(), mouse_X,mouse_Y))
				{
					type=2;
				}
				if(checkInRect(draw.getfirst_rec_X(),draw.getfirst_rec_Y(),draw.getfirst_Xrange(),draw.getfirst_Yrange(), mouse_X,mouse_Y)&&checkInRect(draw.getsec_rec_X(),draw.getsec_rec_Y(),draw.getsec_Xrange(),draw.getsec_Yrange(), mouse_X,mouse_Y))
				{
					type=3;
				}	
				//check type
				if(type==0)
				{
					jLabel_7.setText("Nothing");
				}
				else if(type==1)
				{
					jLabel_7.setText("Hit Building A");
				}
				else if(type==2)
				{
					jLabel_7.setText("Hit Building B");
				}
				else if(type==3)
				{
					jLabel_7.setText("Hit both");
				}
				repaint();
			}
			
			@Override
			public void mouseExited(MouseEvent arg0) 
			{
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent arg0) 
			{
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent arg0) 
			{
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	public void updateStrings()//update String
	{
		myres = ResourceBundle.getBundle("Confidential",locale);
		jLabel_1.setText(myres.getString("Title"));
		jLabel_2.setText(myres.getString("Language"));
		jLabel_4.setText(myres.getString("Position"));
		jLabel_6.setText(myres.getString("Result"));
	}
	public Boolean checkInRect(int xbase,int ybase,int Xrange,int Yrange,int mouse_X,int mouse_Y)//check rect
	{
		Boolean flag=false;
		
		if(mouse_X>=xbase&&mouse_X<=xbase+Xrange)
		{
			if(mouse_Y>=ybase&&mouse_Y<=ybase+Yrange)
			{
				flag=true;
			}
		}
		return flag;
	}

}
