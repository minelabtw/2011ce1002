package fe.s100502506;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

import javax.swing.JPanel;



public class DrawBuildings extends JPanel
{
	private int xBase=0;
	private int yBase=300;
	private int firXrange;
	private int firYrange;
	private int SecXrange;
	private int SecYrange;
	private int mouse_X;
	private int mouse_Y;
	private Timer moveTimer = new Timer(200, new TimeAction());
	public DrawBuildings()
	{
		
	}
	//draw
	protected void paintComponent(Graphics g)
	{
		firXrange=190;
		firYrange=200;
		SecXrange=190;
		SecYrange=150;
		super.paintComponent(g);
		g.setColor(Color.RED);
		g.drawRect(xBase+10, yBase, firXrange,firYrange);
		g.setColor(Color.BLUE);
		g.drawRect(xBase+60, yBase-50, SecXrange,SecYrange);
		moveTimer.start();
		g.setColor(Color.RED);
		g.fillOval(mouse_X, mouse_Y,10, 10);
		

	}
	//Time action
	class TimeAction implements ActionListener
	{
		public void actionPerformed(ActionEvent e) 
		{
			xBase=xBase+10;
			if(xBase==600)
			{
				xBase=-SecXrange;
			}
			
			updateUI();
			repaint();
		}
	}
	//set
	public void setMouse_X(int X)
	{
		mouse_X=X;
	}
	public void setMouse_Y(int Y)
	{
		mouse_Y=Y;
	}
	//get
	public int getfirst_rec_X()
	{
		return xBase+10;
	}
	public int getfirst_rec_Y()
	{
		return yBase;
	}
	public int getsec_rec_X()
	{
		return xBase+60;
	}
	public int getsec_rec_Y()
	{
		return yBase-50;
	}
	
	public int getfirst_Xrange()
	{
		return firXrange;
	}
	public int getfirst_Yrange()
	{
		return firYrange;
	}
	public int getsec_Xrange()
	{
		return SecXrange;
	}
	public int getsec_Yrange()
	{
		return SecYrange;
	}
}
