package fe.s100502018;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class FE11 extends JApplet {
	private ImageIcon[] cats = {
			new ImageIcon(getClass().getResource("image/cat1.jpg")),
			new ImageIcon(getClass().getResource("image/cat2.jpg")),
			new ImageIcon(getClass().getResource("image/cat3.jpg")), };
	private ImageIcon[] dogs = {
			new ImageIcon(getClass().getResource("image/dog1.jpg")),
			new ImageIcon(getClass().getResource("image/dog2.jpg")),
			new ImageIcon(getClass().getResource("image/dog3.jpg")) };
	private ImageIcon[] mouses = {
			new ImageIcon(getClass().getResource("image/mouse1.jpg")),
			new ImageIcon(getClass().getResource("image/mouse2.jpg")),
			new ImageIcon(getClass().getResource("image/mouse3.jpg")) };
	private JMenuBar jmb = new JMenuBar();
	private JMenu jmuAnimals = new JMenu("Animal's Gallery");
	private JMenuItem jmiCat = new JMenuItem("Cats");
	private JMenuItem jmiDog = new JMenuItem("Dogs");
	private JMenuItem jmiMouse = new JMenuItem("Mouses");
	private JToolBar myToolBar = new JToolBar(JToolBar.VERTICAL);
	private JButton jbtCat = new JButton(new ImageIcon(getClass().getResource(
			"image/cat_icon.jpg")));
	private JButton jbtDog = new JButton(new ImageIcon(getClass().getResource(
			"image/dog_icon.jpg")));
	private JButton jbtMouse = new JButton(new ImageIcon(getClass()
			.getResource("image/mouse_icon.jpg")));
	private JButton jbtPrevious = new JButton("��");
	private JButton jbtNext = new JButton("��");
	private JPanel panel = new JPanel();
	private JLabel display = new JLabel();
	private int index = -1;
	private int curAlbum = -1;

	public FE11() {
		Action catAction = new Action("Cats");
		Action dogAction = new Action("Dogs");
		Action mouseAction = new Action("Mouses");

		setLayout(new BorderLayout());
		setJMenuBar(jmb);
		jmb.add(jmuAnimals);
		jmuAnimals.add(jmiCat);
		jmuAnimals.add(jmiDog);
		jmuAnimals.add(jmiMouse);
		jmiCat.addActionListener(catAction);
		jmiDog.addActionListener(dogAction);
		jmiMouse.addActionListener(mouseAction);
		myToolBar.add(jbtCat);
		myToolBar.add(jbtDog);
		myToolBar.add(jbtMouse);
		jbtCat.addActionListener(catAction);
		jbtDog.addActionListener(dogAction);
		jbtMouse.addActionListener(mouseAction);
		panel.add(jbtPrevious);
		panel.add(jbtNext);
		jbtPrevious.addActionListener(new ActionListener() { // change to the
																// previous
																// picture

					@Override
					public void actionPerformed(ActionEvent arg0) {
						// TODO Auto-generated method stub
						if (index >= 0) {
							if (index == 0) {
								JOptionPane.showMessageDialog(null,
										"There are no previous picture!");
							} else {
								switch (curAlbum) {
								case 0:
									display.setIcon(cats[--index]);
									break;
								case 1:
									display.setIcon(dogs[--index]);
									break;
								case 2:
									display.setIcon(mouses[--index]);
									break;
								}
							}
						}
					}
				});
		jbtNext.addActionListener(new ActionListener() { // change to the next
															// picture

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if (index >= 0) {
					if (index == 2) {
						JOptionPane.showMessageDialog(null,
								"There are no next picture!");
					} else {
						switch (curAlbum) {
						case 0:
							display.setIcon(cats[++index]);
							break;
						case 1:
							display.setIcon(dogs[++index]);
							break;
						case 2:
							display.setIcon(mouses[++index]);
							break;
						}
					}
				}
			}
		});
		add(myToolBar, BorderLayout.EAST);
		add(display, BorderLayout.CENTER);
		add(panel, BorderLayout.SOUTH);
	}

	class Action extends AbstractAction { // the Action to show the album
		private String name;

		public Action(String name) {
			this.name = name;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if (name.equals("Cats") && curAlbum != 0) {
				index = 0;
				curAlbum = 0;
				display.setIcon(cats[index]);
			} else if (name.equals("Dogs") && curAlbum != 1) {
				index = 0;
				curAlbum = 1;
				display.setIcon(dogs[index]);
			} else if (name.equals("Mouses") && curAlbum != 2) {
				index = 0;
				curAlbum = 2;
				display.setIcon(mouses[index]);
			}
		}
	}
}
