package fe.s100502018;

import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.*;

import javax.swing.*;

public class Framework extends JFrame implements ActionListener, MouseListener,
		MouseMotionListener {
	JLabel jlblTitle = new JLabel("The Bomber Infrared Screen");
	JLabel jlblLanguage = new JLabel("Language:");
	JLabel jlblBuildings = new JLabel();
	JLabel jlblPosition = new JLabel("Positions:");
	JLabel jlblCurrentPositon = new JLabel();
	JLabel jlblResult = new JLabel("Result");
	JLabel jlblFinalResult = new JLabel();
	String[] locales = { "English", "Chinese", "Japanese", "France" };
	JComboBox jcboLocales = new JComboBox(locales);
	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();
	JPanel p3 = new JPanel();
	JPanel total = new JPanel();

	public Framework() {
		p1.setLayout(new GridLayout(1, 3));
		p1.add(jlblTitle);
		p1.add(jlblLanguage);
		p1.add(jcboLocales);
		p2.add(jlblBuildings);
		p3.setLayout(new GridLayout(1, 4));
		p3.add(jlblPosition);
		p3.add(jlblCurrentPositon);
		p3.add(jlblResult);
		p3.add(jlblFinalResult);
		total.setLayout(new BorderLayout());
		total.add(p1, BorderLayout.NORTH);
		total.add(p2, BorderLayout.CENTER);
		total.add(p3, BorderLayout.SOUTH);
		add(total);
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub

	}
}
