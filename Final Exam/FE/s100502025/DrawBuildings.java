package fe.s100502025;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

public class DrawBuildings extends JPanel {
	private int xBase;
	private int yBase;
	private int mouseX;
	private int mouseY;
	
	public DrawBuildings() {
		setSize(500,500);
		xBase = getWidth() / 2;
		yBase = getHeight() / 2;	
	}
	
	public void setMousePositionX(int x_temp) {
		mouseX = x_temp;
	}
	
	public void setMousePositionY(int y_temp) {
		mouseY = y_temp;
	}
	
	protected void PaintComponent(Graphics g) {
		super.paintComponent(g);
		System.out.println("1");
		if(xBase + 10 >=  xBase * 2) {
			xBase = -10;
		}
		else {
			xBase = xBase + 5;
		}
		
		g.drawRect(xBase + 60, yBase - 50, 190, 150);
		g.drawRect(xBase + 10, yBase, 190, 200);
		
		g.setColor(Color.RED);
		g.drawOval(mouseX, mouseY, 10, 10);
	}
}
