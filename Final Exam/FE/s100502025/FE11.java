package fe.s100502025;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FE11 extends JApplet {
	private ImageIcon[] picture = new ImageIcon[12];  //12張圖片都存在這個陣列中
	private JLabel jlb_img = new JLabel();  //顯示圖片的地方
	private JPanel panel_button = new JPanel();  //放置兩個按鈕的板子
	private JButton jbtLeft = new JButton("←");  //前一張
	private JButton jbtRight = new JButton("→");  //後一張
	private int index = 0;
	
	public FE11() {
		picture[0] = new ImageIcon(getClass().getResource("image/cat_icon.jpg"));
		picture[1] = new ImageIcon(getClass().getResource("image/dog_icon.jpg"));
		picture[2] = new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));
		picture[3] = new ImageIcon(getClass().getResource("image/cat1.jpg"));
		picture[4] = new ImageIcon(getClass().getResource("image/cat2.jpg"));
		picture[5] = new ImageIcon(getClass().getResource("image/cat3.jpg"));
		picture[6] = new ImageIcon(getClass().getResource("image/dog1.jpg"));
		picture[7] = new ImageIcon(getClass().getResource("image/dog2.jpg"));
		picture[8] = new ImageIcon(getClass().getResource("image/dog3.jpg"));
		picture[9] = new ImageIcon(getClass().getResource("image/mouse1.jpg"));
		picture[10] = new ImageIcon(getClass().getResource("image/mouse2.jpg"));
		picture[11] = new ImageIcon(getClass().getResource("image/mouse3.jpg"));
		
		panel_button.setLayout(new GridLayout(1,2));
		panel_button.add(jbtLeft);
		panel_button.add(jbtRight);
				
		Action cat_action = new MyAction("Cat" , picture[0]);
		Action dog_action = new MyAction("Dog" , picture[1]);
		Action mouse_action = new MyAction("Mouse" , picture[2]);
		
		JMenuBar menubar = new JMenuBar();
		JMenu menu = new JMenu("Animals Gallery");
		setJMenuBar(menubar);
		menubar.add(menu);
		
		menu.add(cat_action);
		menu.add(dog_action);
		menu.add(mouse_action);
		
		JToolBar toolbar = new JToolBar(JToolBar.VERTICAL);
		toolbar.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		toolbar.add(cat_action);
		toolbar.add(dog_action);
		toolbar.add(mouse_action);
		
		add(toolbar , BorderLayout.EAST);
		add(jlb_img , BorderLayout.CENTER);
		add(panel_button , BorderLayout.SOUTH);
		
		jbtLeft.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(index == 0 ) {
					index = 0;
				}
				else {
					if(index == 3 || index == 6 || index == 9) {
						index = index;
					}
					else {
						index = index - 1 ;
					}
					jlb_img.setIcon(picture[index]);
				}
			}
		});
		 
		jbtRight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(index == 0 ) {
					index = 0;
				}
				else {
					if(index == 5 || index == 8 || index == 11) {
						index = index;						
					}
					else {
						index = index + 1 ;
					}
					jlb_img.setIcon(picture[index]);
				}			
			}
		});
	}
	
	private class MyAction extends AbstractAction {
		String name;
		
		MyAction(String name , Icon icon) {
			super(name , icon);
			putValue(Action.SHORT_DESCRIPTION , "Select the " + name + " to display");
			this.name = name;
		}
		
		public void actionPerformed(ActionEvent e) {
			if(name.equals("Cat")) {
				jlb_img.setIcon(picture[3]);
				index = 3;
			}
			else if(name.equals("Dog")) {
				jlb_img.setIcon(picture[6]);
				index = 6;
			}
			else if(name.equals("Mouse")) {
				jlb_img.setIcon(picture[9]);
				index = 9;
			}
		}
	}
	
}
