package fe.s100502025;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.Timer;

public class FrameWork extends JFrame {//implements ActionListener {
	private ResourceBundle res = ResourceBundle.getBundle("Confidential");
	private JLabel JLabel_1 = new JLabel("The Bomber Infrared Screen");
	private JLabel JLabel_2 = new JLabel("Language");
	private JLabel JLabel_3 = new JLabel();
	private JLabel JLabel_4 = new JLabel("Position:");
	private JLabel JLabel_5 = new JLabel();
	private JLabel JLabel_6 = new JLabel("Result:");
	private JLabel JLabel_7 = new JLabel();
	private JComboBox jcb_language = new JComboBox(new String[] {"英文","日文","法文","中文"});
	private JPanel p1 = new JPanel();
	private JPanel p2 = new JPanel();
	private JPanel p3 = new JPanel();
	private JPanel p4 = new JPanel();
	private JPanel p5 = new JPanel();
	private int mouseX;
	private int mouseY;
	
	private Timer timer = new Timer(200, new TimerListener());
	
	private DrawBuildings buildings = new DrawBuildings();
	
	public FrameWork() {
		Locale locale = Locale.US;
		
		p1.setLayout(new GridLayout(1,2));
		p1.add(JLabel_2);
		p1.add(jcb_language);
		
		p2.setLayout(new GridLayout(1,2));
		p2.add(JLabel_1);
		p2.add(p1);
		
		p3.setLayout(new GridLayout(1,2));
		p3.add(JLabel_4);
		p3.add(JLabel_5);
		
		p4.setLayout(new GridLayout(1,2));
		p4.add(JLabel_6);
		p4.add(JLabel_7);
		
		p5.setLayout(new GridLayout(1,2));
		p5.add(p3);
		p5.add(p4);
		
		buildings.setMousePositionX(mouseX);
		buildings.setMousePositionY(mouseY);
		
		//JLabel_3.addMouseListener();
		
		JLabel_3.add(buildings);
		add(p2 , BorderLayout.NORTH);
		add(JLabel_3 , BorderLayout.CENTER);
		add(p5 , BorderLayout.SOUTH);
		
		jcb_language.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		
		timer.start();
	}
	

	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			repaint();
		}
	}
	
	public void mousePressed(MouseEvent e) {
		mouseX = e.getX();
		mouseY = e.getY();
		JLabel_5.setText(mouseX + "," + mouseY);
	}
}
