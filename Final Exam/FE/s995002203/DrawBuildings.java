package fe.s995002203;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class DrawBuildings extends JPanel{
	Timer timer = new Timer(200, new TimerListener());
	int xbase=0;;
	int ybase=getHeight()+150;
	
	int x=0,y=0;
	public DrawBuildings(){
		this.setBorder(BorderFactory.createLineBorder(Color.black,2));
		timer.start();
		//repaint();
	}

	public void paintComponent(Graphics g) {
	    super.paintComponent(g);
	    
	    int [] ax={xbase+10,xbase+10,xbase+200,xbase+200};
	    int [] bx={xbase+60,xbase+60,xbase+250,xbase+250};
	    
	    int [] ay={ybase+200,ybase,ybase,ybase+200};
	    int [] by={ybase+100,ybase-50,ybase-50,ybase+100};
	    
	    g.setColor(Color.blue);
	    g.drawString("A", xbase, ybase+200);
	    g.drawPolygon(ax, ay, ax.length);//a
	    g.setColor(Color.green);
	    g.drawString("B", xbase+50, ybase+100);
	    g.drawPolygon(bx, by, bx.length);//b
	}
	  private class TimerListener implements ActionListener {
		    public void actionPerformed(ActionEvent e) {
		    	xbase++;
		    	if(xbase==getWidth())
		    		xbase=0;
		    	repaint();
		    }
		  }
}
