package fe.s995002203;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.*;

public class Framework extends JFrame implements ActionListener{
	private Locale[] availableLocales = Locale.getAvailableLocales();
	private Locale locale = Locale.US;
	private ResourceBundle res = ResourceBundle.getBundle("Confidential",locale);
	private JLabel jlbTitle=new JLabel(res.getString("Title"));
	private JLabel jlbLang=new JLabel(res.getString("Language"));
	private JLabel jlbPosition=new JLabel(res.getString("Position"));
	private JLabel jlbResult=new JLabel(res.getString("Result"));
	
	private JLabel jlbCurPos, jlbfinalResult;
	private JComboBox jcboLang = new JComboBox();
	
	private DrawBuildings draw1=new DrawBuildings();
	
	private void updateStrings(){
		 res = ResourceBundle.getBundle("Confidential",locale);
		 jlbTitle.setText(res.getString("Title"));
		 jlbLang=new JLabel(res.getString("Language"));
		 jlbPosition.setText(res.getString("Position"));
		 jlbResult.setText(res.getString("Result"));
	}
	public Framework() {
		for(int i=0;i<availableLocales.length;i++)
			jcboLang.addItem(availableLocales[i].getDisplayName());
		JPanel p1=new JPanel(new GridLayout(1,3,5,5));
		JPanel p2=new JPanel();
		JPanel p3=new JPanel(new GridLayout(1,2,5,5));
		p1.add(jlbTitle);
		p1.add(jlbLang);
		p1.add(jcboLang);
		
		p2=draw1;
		
		p3.add(jlbPosition);
		//p3.add(jlbCurPos);
		p3.add(jlbResult);
		//p3.add(jlbfinalResult);
		jcboLang.addActionListener(this);
		this.setLayout(new BorderLayout());
		this.add(p1, BorderLayout.NORTH);
		this.add(p2, BorderLayout.CENTER);
		this.add(p3, BorderLayout.SOUTH);
	 }
	public void actionPerformed(ActionEvent e) {
			locale = availableLocales[jcboLang.getSelectedIndex()];
		    updateStrings();
	}
	public void mousePressed(MouseEvent e){
		draw1.x=e.getX();
		draw1.y=e.getY();
	}
}
