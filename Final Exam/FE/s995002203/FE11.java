package fe.s995002203;

import java.awt.*;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class FE11 extends JApplet {
	private ImageIcon catIcon = new ImageIcon(getClass().getResource("image/cat_icon.jpg"));
	private ImageIcon dogIcon = new ImageIcon(getClass().getResource("image/dog_icon.jpg"));
	private ImageIcon mouseIcon = new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));
	
	private ImageIcon [] cat=new ImageIcon[3];
	private ImageIcon [] dog=new ImageIcon[3];
	private ImageIcon [] mouse=new ImageIcon[3];

	private JLabel Label_img = new JLabel();
	private JPanel buttonPanel=new JPanel(new GridLayout(1,2,5,5));
	private JButton left=new JButton("←");
	private JButton right=new JButton("→");
	
	private int curIndex=0;//紀錄現在是第幾張照片
	private int curAnimal=-1;//紀錄現在是哪一種動物
	
	private buttonListener buttonListener=new buttonListener();
	
	public FE11(){
		for(int i=0;i<3;i++){
			cat[i]=new ImageIcon(getClass().getResource("image/cat"+(i+1)+".jpg"));//把三種動物的圖片存到個別的相簿中
			dog[i]=new ImageIcon(getClass().getResource("image/dog"+(i+1)+".jpg"));
			mouse[i]=new ImageIcon(getClass().getResource("image/mouse"+(i+1)+".jpg"));
		}
		
		Action catIconAction = new MyAction("Cat", catIcon);
		Action dogIconAction = new MyAction("Dog", dogIcon);
		Action mouseIconAction = new MyAction("Mouse", mouseIcon);
		
		JMenuBar menubar=new JMenuBar();//設定menubar
		JMenu menu=new JMenu("animal");
		setJMenuBar(menubar);
		menubar.add(menu);
		menu.add(catIconAction);
		menu.add(dogIconAction);
		menu.add(mouseIconAction);

		JToolBar toolBar = new JToolBar(JToolBar.VERTICAL);//設定toolbar
		toolBar.setBorder(BorderFactory.createLineBorder(Color.black));
		
		toolBar.add(catIconAction);
		toolBar.add(dogIconAction);
		toolBar.add(mouseIconAction);
		
		add(toolBar, BorderLayout.EAST);
		add(Label_img, BorderLayout.CENTER);
		
		left.addActionListener(buttonListener);
		right.addActionListener(buttonListener);
		
		buttonPanel.add(left);
		buttonPanel.add(right);
		add(buttonPanel, BorderLayout.SOUTH);
	}
	
	private class MyAction extends AbstractAction{
		String name;
		MyAction(String name,Icon icon){
			super(name,icon);
			putValue(Action.SHORT_DESCRIPTION,"select"+name);
			this.name = name;
		}
		public void actionPerformed(ActionEvent e) {
	    	if (name.equals("Cat")){
	    		Label_img.setIcon(cat[0]);
	    		curAnimal=0;
	    	}
	    	else if (name.equals("Dog")){
	    		Label_img.setIcon(dog[0]);
	    		curAnimal=1;
	    	}
	    	if (name.equals("Mouse")){
	    		Label_img.setIcon(mouse[0]);
	    		curAnimal=2;
	    	}
	    	curIndex=0;//每次切換動物 都會把index歸零 從每本相簿的第一張圖開始看
		}
	}
	private class buttonListener implements ActionListener {
	    /** Handle the action event */
	    public void actionPerformed(ActionEvent e) {
	    	if(e.getSource()==left){
	    		if(curAnimal<0)//預設是負數，選擇動物後會變為正數，所以沒選動物就按左右鍵會跳出對話窗
	    			JOptionPane.showMessageDialog(null, "you have to choose an animal");
	    		else if(curIndex!=0)//index下限為0
	    			curIndex--;    		
	    		else
	    			JOptionPane.showMessageDialog(null, "this is the first photo");
	    	}
	    	else{
	    		if(curAnimal<0)//預設是負數，選擇動物後會變為正數，所以沒選動物就按左右鍵會跳出對話窗
	    			JOptionPane.showMessageDialog(null, "you have to choose an animal");
	    		else if(curIndex<2)//index上限為2
	    			curIndex++;	
	    		else
	    			JOptionPane.showMessageDialog(null, "this is the last photo");
	    	}
	    	switch(curAnimal){
	    		case 0://貓
	    			Label_img.setIcon(cat[curIndex]);
	    			break;
	    		case 1://狗
	    			Label_img.setIcon(dog[curIndex]);
	    			break;
	    		case 2://老鼠
	    			Label_img.setIcon(mouse[curIndex]);
	    			break;
	    		default:
	    			break;
	    	}
	    }
	}
}
