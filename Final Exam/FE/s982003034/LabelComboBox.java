package a12.s982003034;

import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class LabelComboBox extends JPanel {

	protected JLabel label;
	protected JComboBox comboBox;
	protected ActionListener actionListener;
	
	public LabelComboBox (String Name, String[] comboBoxElements, ActionListener AL) {
		
		actionListener = AL;

		label = new JLabel (Name);
		
		comboBox = new JComboBox (comboBoxElements);
		comboBox.addActionListener(actionListener);	
		comboBox.setEditable(false);
		
	}
	
	public void setName (String Name) {
		label.setText(Name);
	}
	
	public void setComboBoxElements (String [] comboBoxElements) {
		comboBox = new JComboBox (comboBoxElements);
	}
	
}
