package a12.s982003034;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

public class A121 extends Applet {
	
	protected ArrayList <String> nationName;
	protected ArrayList <Action> showFlag;
	protected ArrayList <JButton> flagButton;
	protected JPanel buttonPanel;
	
	protected ArrayList <JMenuItem> flagMenu;
	protected JMenu menu;
	protected JMenuBar menuBar;

	protected JLabel display;
	
	public void init () {
		
		nationName = new ArrayList <String> (); 
		nationName.add("Canada");
		nationName.add("China");
		nationName.add("Germany");
		nationName.add("Canada");
		nationName.add("USA");
		
		showFlag = new ArrayList <Action> ();
		for (int i = 0; i < nationName.size(); i++) {
			showFlag.add(new flagButtonAction (nationName.get(i)));
		} // initialize custom action
		
		flagButton = new ArrayList <JButton> ();
		for (int i = 0; i < showFlag.size(); i++) {
			flagButton.add(new JButton (showFlag.get(i)));
		} // initialize button
		
		buttonPanel = new JPanel ();
		buttonPanel.setLayout(new GridLayout (flagButton.size(),1));
		for (int i = 0; i < flagButton.size(); i++) {
			buttonPanel.add(flagButton.get(i));
		} // initialize button panel
		
		flagMenu = new ArrayList <JMenuItem> ();
		for (int i = 0; i < showFlag.size(); i++) {
			flagMenu.add(new JMenuItem (showFlag.get(i)));
		} // initialize menu item
		
		menu = new JMenu();
		for (int i = 0; i < flagMenu.size(); i++) {
			menu.add(flagMenu.get(i));
		} // initialize menu
		
		menuBar = new JMenuBar ();		
		menuBar.add(menu);
		// initialize menu bar
		
		display = new JLabel ();
		
		this.setLayout(new BorderLayout ());
		this.add (menuBar, BorderLayout.NORTH);
		this.add (display, BorderLayout.CENTER);
		this.add (buttonPanel, BorderLayout.EAST);
		this.setVisible(true);
		
	}
	
	public class flagButtonAction extends AbstractAction {

		protected ImageIcon bigFlag;
		
		protected final static String imgDir = "image/";
		protected final static String flagIcon = "_Icon";
		
		public flagButtonAction (String nation) {
			super ();
			ImageIcon icon = loadIcon (imgDir + nation + flagIcon);
			bigFlag = loadIcon (imgDir + nation);
			//this.putValue(NAME, nation);
			this.putValue(SMALL_ICON, icon);
		}
		
		private ImageIcon loadIcon (String icon_path) {
			URL imgURL = A121.class.getResource(icon_path+".gif");
			if (imgURL == null) {
	            System.err.println("Resource not found");
	            return null;
	        } else {
	            return new ImageIcon(imgURL);
	        } // else

		} // load icon
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			displayFlag (bigFlag);
			
		} // actionPerformed
		
	} // class flagButtonAction

	public void displayFlag(ImageIcon bigFlag) {
		display.setIcon(bigFlag);
	}

}
