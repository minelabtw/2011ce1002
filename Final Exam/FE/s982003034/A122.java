package a12.s982003034;

import java.applet.Applet;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;
import java.util.TimeZone;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class A122 extends Applet implements ActionListener {
	
	protected final String localeDir = "Language/";
	protected ArrayList <String> localeFileDir;
	protected ArrayList <Properties> localeFile;
	protected ArrayList <Locale> locale;
	
	
	protected Date date;

	protected JLabel dateLabel;
	protected JTextField dateField;
	
	protected LabelComboBox dateFormat;
	
	protected JLabel timeLabel;
	protected JTextField timeField;
	
	protected LabelComboBox timeFormat;
	
	protected LabelComboBox language;
	protected LabelComboBox timezone;
	
	/**
	 * @param args
	 */
	
	public void init () {
		
		localeFileDir = new ArrayList <String> ();
		
		localeFileDir.add("MyResource_en.properties");
		localeFileDir.add("MyResource_fr.properties");
		localeFileDir.add("MyResource_ja.properties");
		localeFileDir.add("MyResource_zh.properties");
		
		localeFile = loadLocale (localeFileDir);
		
		locale = new ArrayList <Locale> ();
		locale.add (Locale.ENGLISH);
		locale.add (Locale.FRENCH);
		locale.add (Locale.JAPANESE);
		locale.add (Locale.CHINESE);
				
		date = new Date ();

		dateLabel = new JLabel (localeFile.get(0).getProperty("Date"));
		dateField = new JTextField (DateFormat.getDateInstance(DateFormat.SHORT, locale.get(0)).format(date));
		
		String [] dateFormatTemp = new String [] {"Short", "Medium", "Long", "Full"};
		dateFormat = new LabelComboBox (localeFile.get(0).getProperty("Date_Style"), dateFormatTemp, this);
		
		timeLabel = new JLabel (localeFile.get(0).getProperty("Time"));
		timeField =  new JTextField (DateFormat.getDateInstance(DateFormat.SHORT, locale.get(0)).format(date));
		
		timeFormat = new LabelComboBox (localeFile.get(0).getProperty("Time_Style"), dateFormatTemp, this);
		
		String [] languageTemp = new String [locale.size()];
		for (int i = 0; i < locale.size(); i++) {
			languageTemp [i] = locale.get(i).getDisplayLanguage(locale.get(0));
		}
		language =  new LabelComboBox (localeFile.get(0).getProperty("Choose_Locale"), languageTemp, this);
		
		timezone = new LabelComboBox (localeFile.get(0).getProperty("Time_Zone"), TimeZone.getAvailableIDs(), this);

	}
	
	private ArrayList<Properties> loadLocale(ArrayList<String> localeFileDir) {
		ArrayList <Properties> temp = new ArrayList <Properties> ();
		try {
			for (int i = 0; i < localeFileDir.size(); i++) {
				temp.add (new Properties());
				temp.get(i).load (new FileReader (new File (localeDir+localeFileDir.get(i))));	
			} // for
		} // try
		
		
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return temp;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		LabelComboBox temp = (LabelComboBox)(e.getSource());
		Object selection = temp.comboBox.getSelectedItem();
		
		if (temp == dateFormat) {
			String format = (String)selection;
			int selectFormat;
			if (format.compareTo("Short") == 0) selectFormat = DateFormat.SHORT; 
		}
		if (temp == timeFormat) {
			;
		}
		if (temp == language){
			;
		}
		if (temp == timezone){
			;
		}
	}

}