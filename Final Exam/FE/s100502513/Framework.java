package fe.s100502513;

import java.awt.BorderLayout;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

public class Framework extends JFrame implements ActionListener {
	private DrawBuildings draw = new DrawBuildings();
	private ResourceBundle res = ResourceBundle
			.getBundle("FE12_Language/Confidential");
	private ResourceBundle[] ress = new ResourceBundle[4];
				private Locale[] locales = Locale.getAvailableLocales();
	private JLabel l_1 = new JLabel(res.getString("Title"));
	private JLabel l_2 = new JLabel(res.getString("Language"));
	private JPanel l_3 = draw;
	private JLabel l_4 = new JLabel(res.getString("Position"));
	private JLabel l_5 = new JLabel();
	private JLabel l_6 = new JLabel(res.getString("Result"));
	private JLabel l_7 = new JLabel();
	private String[] country = { "En", "Zh", "Jp", "Fr" };
	private JComboBox cb = new JComboBox(country);

	public Framework() {
		ress[0]= ResourceBundle.getBundle("FE12_Language/Confidential_en");  //初始語系
		ress[1]= ResourceBundle.getBundle("FE12_Language/Confidential_zh");
		ress[2]= ResourceBundle.getBundle("FE12_Language/Confidential_ja");
		ress[3]= ResourceBundle.getBundle("FE12_Language/Confidential_fr");
		JPanel p1 = new JPanel();
		p1.add(l_1);
		p1.add(l_2);
		p1.add(cb);
		JPanel p2 = new JPanel();
		p2.add(l_4);
		p2.add(l_5);
		p2.add(l_6);
		p2.add(l_7);
		add(p1, BorderLayout.NORTH);
		add(l_3, BorderLayout.CENTER);
		add(p2, BorderLayout.SOUTH);
		cb.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				res=ress[cb.getSelectedIndex()];
				updatestring();
			}
		});
		addMouseListener(new MouseListener() {
			public void mousePressed(MouseEvent e) {
				l_5.setText(draw.getmouse());
				repaint();
			}
			public void mouseClicked(MouseEvent e) {

			}
			public void mouseEntered(MouseEvent e) {
			}
			public void mouseExited(MouseEvent e) {

			}
			public void mouseReleased(MouseEvent e) {

			}
		});
	}

	public void updatestring() {
		l_1.setText(res.getString("Title"));  //改變語系
		l_2.setText(res.getString("Language"));
		l_4.setText(res.getString("Position"));
		l_6.setText(res.getString("Result"));
		repaint();
	}

//	public String setmousex() {
//		
//	}
//
//	public void getmousey() {
//		return getmousex();
//	}

	public void actionPerformed(ActionEvent e) {
	}

}
