package fe.s100502513;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FE11 extends JApplet implements ActionListener {
	private JToolBar tBar = new JToolBar();
	private JMenuBar mBar = new JMenuBar();
	private JMenu menu = new JMenu("Animals Gallery");
	private JMenuItem cat, dog, mouse;
	private ImageIcon[] pic = new ImageIcon[9];
	private int icat = 0;  //判斷圖片的張數
	private int idog = 3;
	private int imouse = 6;
	private int anilmal = 1;  //判斷動物是哪類
	private JButton dogButton = new JButton(new ImageIcon(getClass()
			.getResource("image/dog_icon.jpg")));
	private JButton catButton = new JButton(new ImageIcon(getClass()
			.getResource("image/cat_icon.jpg")));
	private JButton mouseButton = new JButton(new ImageIcon(getClass()
			.getResource("image/mouse_icon.jpg")));
	private JButton next = new JButton("→");
	private JButton pre = new JButton("←");
	private JLabel picJLabel = new JLabel();

	public FE11() {
		for (int i = 0; i < 3; i++) {  //初始圖片
			pic[0 + i] = new ImageIcon(getClass().getResource(  //貓
					"image/cat" + (i + 1) + ".jpg"));
			pic[3 + i] = new ImageIcon(getClass().getResource(  //狗
					"image/dog" + (i + 1) + ".jpg"));
			pic[6 + i] = new ImageIcon(getClass().getResource(  //鼠
					"image/mouse" + (i + 1) + ".jpg"));
		}

		setJMenuBar(mBar);  //上方menu部分
		menu.add(cat = new JMenuItem("Cats"));
		menu.add(dog = new JMenuItem("Dogs"));
		menu.add(mouse = new JMenuItem("Mouses"));
		mBar.add(menu);

		tBar.setOrientation(JToolBar.VERTICAL);  //使排列為垂直
		tBar.add(catButton);
		tBar.add(dogButton);
		tBar.add(mouseButton);
		add(tBar, BorderLayout.EAST);

		picJLabel.setIcon(pic[icat]);  //圖片的LABEL
		add(picJLabel, BorderLayout.CENTER);

		JPanel p = new JPanel();  //下方2個前進後退按鈕
		p.add(pre);
		p.add(next);
		add(p, BorderLayout.SOUTH);
		
		catButton.addActionListener(this);
		dogButton.addActionListener(this);
		mouseButton.addActionListener(this);
		cat.addActionListener(this);
		dog.addActionListener(this);
		mouse.addActionListener(this);
		next.addActionListener(this);
		pre.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == catButton) {
			picJLabel.setIcon(pic[icat]);  //換成貓的圖片
			anilmal = 1;  //動物類型改變
		} 
		else if (e.getSource() == dogButton) {
			picJLabel.setIcon(pic[idog]);  //換成狗的圖片
			anilmal = 2;
		} 
		else if (e.getSource() == mouseButton) {
			picJLabel.setIcon(pic[imouse]);  //換成鼠的圖片
			anilmal = 3;
		} 
		else if (e.getSource() == cat) {
			picJLabel.setIcon(pic[icat]);
			anilmal = 1;
		} 
		else if (e.getSource() == dog) {
			picJLabel.setIcon(pic[idog]);
			anilmal = 2;
		} 
		else if (e.getSource() == mouse) {
			picJLabel.setIcon(pic[imouse]);
			anilmal = 3;
		} 
		else if (e.getSource() == next) {
			if (anilmal == 1) {  //如果類型是貓
				if (icat < 2)  //沒超過最後一張顯示圖片
					picJLabel.setIcon(pic[++icat]);
				else  //超過顯示訊息
					System.err.println("This is the last picture");
			} 
			else if (anilmal == 2) {
				if (idog < 5)
					picJLabel.setIcon(pic[++idog]);
				else
					System.err.println("This is the last picture");
			} 
			else if (anilmal == 3) {
				if (imouse < 8)
					picJLabel.setIcon(pic[++imouse]);
				else
					System.err.println("This is the last picture");	
			}
		} else if (e.getSource() == pre) {
			if (anilmal == 1) {
				if (icat > 0) //沒超過第一張顯示圖片
					picJLabel.setIcon(pic[--icat]);
				else
					System.err.println("This is the first picture");
			} else if (anilmal == 2) {
				if (idog > 3)
					picJLabel.setIcon(pic[--idog]);
				else
					System.err.println("This is the first picture");
			} else if (anilmal == 3) {
				if (imouse > 6)
					picJLabel.setIcon(pic[--imouse]);
				else
					System.err.println("This is the first picture");
			}
		}
	}
}
