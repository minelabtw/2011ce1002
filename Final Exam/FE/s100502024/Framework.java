package fe.s100502024;
import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.event.*;
import java.util.*;

public class Framework extends JFrame implements ActionListener
{
	JLabel jlb_1 = new JLabel("The Bomber Infrared Screen                 ");
	JLabel jlb_2 = new JLabel("Language   ");
	JLabel jlb_3 = new JLabel();
	JLabel jlb_4 = new JLabel("Position: ");
	JLabel jlb_5 = new JLabel();
	JLabel jlb_6 = new JLabel("         Result:");
	JLabel jlb_7 = new JLabel();
	
	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();
	
	JComboBox select_language = new JComboBox();
	Locale English = new Locale("en","US");
	Locale Chinese = new Locale("zh","TW");
	Locale Japan = new Locale("ja","JP");
	Locale French = new Locale("fr","CA");
	Locale[] locales = {English,Chinese,Japan,French};
	
	ResourceBundle res = ResourceBundle.getBundle("Confidential");
	
	int X;
	int Y;
	DrawBuildings d = new DrawBuildings();
	public Framework()
	{
		setlanguage();
		p1.add(jlb_1);
		p1.add(jlb_2);
		p1.add(select_language);
		p2.add(jlb_4);
		p2.add(jlb_5);
		p2.add(jlb_6);
		p2.add(jlb_7);
		add(p1,BorderLayout.NORTH);
		add(d);
		add(p2,BorderLayout.SOUTH);
		
		select_language.addActionListener(this);
		d.addMouseListener(new MouseListener(){  // 按鍵事件
			public void mouseClicked(MouseEvent e) 
			{
				jlb_5.setText(e.getX()+","+e.getY());
				d.setMousePositionX(e.getX());
				d.setMousePositionY(e.getY());
				d.repaint();
				d.determine();
				jlb_7.setText(d.determine);
			}
			public void mouseEntered(MouseEvent e) 
			{
				
			}
			public void mouseExited(MouseEvent e) 
			{
				
			}
			public void mousePressed(MouseEvent e) 
			{
		
			}
			public void mouseReleased(MouseEvent e) 
			{
				
			}
		});

	}
	public void setlanguage()  //  存語言
	{
		for(int i=0;i<locales.length;i++)
		{
			select_language.addItem(locales[i].getDisplayName());
		}
	}
	public void update()  // 更新語言
	{
		res = ResourceBundle.getBundle("Confidential",locales[select_language.getSelectedIndex()]);
		jlb_1.setText(res.getString("Title")+"          ");
		jlb_2.setText(res.getString("Language"));
		jlb_4.setText(res.getString("Position")+"        ");
		jlb_6.setText(res.getString("Result"));
	};
	public void actionPerformed(ActionEvent e) 
	{
		if(select_language.getSelectedIndex() == 0)
		{
			update();
		}
		if(select_language.getSelectedIndex() == 1)
		{
			update();
		}
		if(select_language.getSelectedIndex() == 2)
		{
			update();
		}
		if(select_language.getSelectedIndex() == 3)
		{
			update();
		}
	}
	
}
