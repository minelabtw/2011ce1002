package fe.s100502024;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class DrawBuildings extends JPanel implements ActionListener
{
	int xBase = 20;
	int yBase = 200;
	int mouse_x;
	int mouse_y;
	Timer timer = new Timer(200,this);
	protected String determine;
	public DrawBuildings()
	{
		timer.start();
	}
	
	public void actionPerformed(ActionEvent e)  // 移動
	{
		xBase+=2;
		repaint();
	}
	
	protected void paintComponent(Graphics g)  // 畫圖
	{
		super.paintComponent(g);
		g.drawRect(xBase+60,yBase-50,200,200);
		g.drawRect(xBase+10,yBase,200,200);
		g.fillOval(mouse_x, mouse_y,10,10);
	}
	public void setMousePositionX(int x_temp)  // 傳X
	{
		mouse_x = x_temp;
	}
	public void setMousePositionY(int y_temp)  // 傳Y
	{
		mouse_y = y_temp;
	}
	public void determine() // 判斷
	{
		if(mouse_x < xBase+260 && mouse_x > xBase+60 && mouse_y < yBase+150 && mouse_y > yBase-50)
		{
			if(mouse_x < xBase+210 && mouse_y > yBase)
			{
				determine = "Building A & B";
			}
			else
			{
				determine = "Building A";
			}
		}
		if(mouse_x < xBase+210 && mouse_x > xBase+10 && mouse_y < yBase+200 && mouse_y > yBase)
		{
			if(mouse_x > xBase+60 && mouse_y < yBase+150)
			{
				determine = "Building A & B";
			}
			else
			{
				determine = "Building B";
			}
		}
		else
		{
			determine = "Nothing";
		}
	}
}
