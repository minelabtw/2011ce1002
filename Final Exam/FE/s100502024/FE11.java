package fe.s100502024;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.*;
import javax.swing.*;

public class FE11 extends JApplet
{
	ImageIcon cat_icon = new ImageIcon(getClass().getResource("image/cat_icon.jpg")); // 讀圖片
	ImageIcon dog_icon = new ImageIcon(getClass().getResource("image/dog_icon.jpg"));
	ImageIcon mouse_icon = new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));
	ImageIcon image_1 = new ImageIcon(getClass().getResource("image/cat1.jpg"));
	ImageIcon image_2 = new ImageIcon(getClass().getResource("image/cat2.jpg"));
	ImageIcon image_3 = new ImageIcon(getClass().getResource("image/cat3.jpg"));
	ImageIcon image_4 = new ImageIcon(getClass().getResource("image/dog1.jpg"));
	ImageIcon image_5 = new ImageIcon(getClass().getResource("image/dog2.jpg"));
	ImageIcon image_6 = new ImageIcon(getClass().getResource("image/dog3.jpg"));
	ImageIcon image_7 = new ImageIcon(getClass().getResource("image/mouse1.jpg"));
	ImageIcon image_8 = new ImageIcon(getClass().getResource("image/mouse2.jpg"));
	ImageIcon image_9 = new ImageIcon(getClass().getResource("image/mouse3.jpg"));
	ImageIcon[] images = {image_1,image_2,image_3,image_4,image_5,image_6,image_7,image_8,image_9}; // 把圖片存入array來控制
	
	JPanel total = new JPanel();
	JPanel button_panel = new JPanel();
	JButton last = new JButton("<"); // 控制前後的按鈕
	JButton next = new JButton(">");
	JLabel show_image = new JLabel();
	int image_index = 0;
	public FE11()
	{
		button_panel.add(last);
		button_panel.add(next);
		total.add(show_image,BorderLayout.CENTER);
		add(button_panel,BorderLayout.SOUTH);
		add(total);
		
		Action cat = new MyAction("Cat",cat_icon);
		Action dog = new MyAction("Dog",dog_icon);
		Action mouse = new MyAction("Mouse",mouse_icon);
		
		JMenuBar jmb = new JMenuBar(); // 選單
		JMenu animals = new JMenu("Animals Gallery"); 
		jmb.add(animals); 
		animals.add(cat); // 增加選單項目
		animals.add(dog);
		animals.add(mouse);
		setJMenuBar(jmb);
		
		JToolBar jToolBar = new JToolBar(JToolBar.VERTICAL);
		jToolBar.add(cat);
		jToolBar.add(dog);
		jToolBar.add(mouse);
		add(jToolBar,BorderLayout.EAST);
		
		last.addActionListener(new ActionListener(){ // 按下last按鈕後要做的事
			public void actionPerformed(ActionEvent e)
			{
				if(image_index == 1 || image_index == 4 || image_index == 7) // 判斷圖片是否為相簿中的第一張
				{
					JOptionPane.showMessageDialog(null,"This is the first page!!"); // 顯示訊息
				}
				else
				{
					image_index--;
					showimage();
				}
				
			}
		});
		next.addActionListener(new ActionListener(){ // 按下next按鈕後要做的事
			public void actionPerformed(ActionEvent e) 
			{
				if(image_index == 3 || image_index == 6 || image_index == 9) // 判斷圖片是否為相簿中的最後一張
				{
					JOptionPane.showMessageDialog(null,"This is the last page!!");
				}
				else
				{
					image_index++;
					showimage();
				}
			}
		});
	}
	public void showimage() // 設定顯示圖片
	{
		show_image.setIcon(images[image_index-1]);
	}
	private class MyAction extends AbstractAction
	{
		String animal_name;
		public MyAction(String name,Icon icon)
		{
			super(name,icon);
			this.animal_name = name;
		}
		public void actionPerformed(ActionEvent e)
		{
			if(animal_name.equals("Cat"))
			{
				show_image.setIcon(images[0]);
				image_index = 1;
			}
			if(animal_name.equals("Dog"))
			{
				show_image.setIcon(images[3]);
				image_index = 4;
			}
			if(animal_name.equals("Mouse"))
			{
				show_image.setIcon(images[6]);
				image_index = 7;
			}
		}
	}
}
