package fe.s100502012;

import java.awt.BorderLayout;
import java.util.Timer;

import javax.swing.ComboBoxEditor;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Framework extends JFrame{
    private JLabel title=new JLabel(/*res.getString("Title")*/);
    private JLabel language=new JLabel(/*res.getString("Language")*/);
    private JLabel result=new JLabel(/*res.getString("Result")*/);
    private JLabel position=new JLabel(/*res.getString("Position")*/);
    private JLabel positionResult=new JLabel();
    private JLabel resultResult=new JLabel();
    private JComboBox languageBox=new JComboBox();
    private JPanel panel1=new JPanel();
    private JPanel panel2=new JPanel();
    private target t=new target();
    private Timer timer=new Timer(/*200,new TimerListener()*/);
    
    public Framework(){
    	setLayout(null);
    	
    	panel1.setSize(600,50);
    	panel1.setLocation(0,20);
    	panel1.setLayout(null);
    	title.setLocation(0,0);
    	language.setLocation(500,0);
    	languageBox.setLocation(550,0);
        panel1.add(title);
        panel1.add(language);
        panel1.add(languageBox);
        add(panel1);
        
    	panel2.setSize(600,50);
    	panel2.setLocation(0,550);
    	panel2.setLayout(null);
    	position.setLocation(0,550);
    	positionResult.setLocation(100,550);
    	result.setLocation(400,550);
    	resultResult.setLocation(500,550);
        panel2.add(position);
        panel2.add(positionResult);
        panel2.add(result);
        panel2.add(resultResult);
        add(panel2);
        
        t.setSize(500,500);
        t.setLocation(0,50);
        add(t);
    }
}