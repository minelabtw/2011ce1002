package fe.s100502012;

import java.awt.Graphics;
import java.awt.Polygon;

import javax.swing.JPanel;

public class target extends JPanel{
	private int xBase=0;
	private int yBase=100;
	
	protected void paintComponent(Graphics g){
	    super.paintComponent(g);
	    
	   
	    Polygon rec1=new Polygon();
	    
	    rec1.addPoint(xBase+60,yBase-50);
	    rec1.addPoint(xBase+250,yBase-50);
	    rec1.addPoint(xBase+250,yBase+100);
	    rec1.addPoint(xBase+60,yBase+100);
	    
	    
	    Polygon rec2=new Polygon();
	    
	    rec2.addPoint(xBase+10,yBase);
	    rec2.addPoint(xBase+200,yBase);
	    rec2.addPoint(xBase+200,yBase+200);
	    rec2.addPoint(xBase+10,yBase+200);

	    
	    g.drawPolygon(rec1);
	    g.drawPolygon(rec2);
		}
}
