package fe.s100502012;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;



public class FE11 extends JApplet{
    public FE11(){
    	add(new FrameWork());
    }
}


class FrameWork extends JPanel implements ActionListener{
	private JLabel label=new JLabel();
	private JButton leftButton=new JButton("<--");
	private JButton rightButton=new JButton("-->");
	private JToolBar albumBar=new JToolBar();
	private JMenu m=new JMenu("MENU");
	private JMenuBar mb=new JMenuBar();
	private ImageIcon[] pic= new ImageIcon[12];
	private int type=1;
	private int number1=1;
	private int number2=5;
	private int number3=9;
	
	public FrameWork(){
		setLayout(null);
		
		pic[0]=new ImageIcon(getClass().getResource("image/cat_icon.jpg"));
		pic[1]=new ImageIcon(getClass().getResource("image/cat1.jpg"));
		pic[2]=new ImageIcon(getClass().getResource("image/cat2.jpg"));
		pic[3]=new ImageIcon(getClass().getResource("image/cat3.jpg"));
		pic[4]=new ImageIcon(getClass().getResource("image/dog_icon.jpg"));
		pic[5]=new ImageIcon(getClass().getResource("image/dog1.jpg"));
		pic[6]=new ImageIcon(getClass().getResource("image/dog2.jpg"));
		pic[7]=new ImageIcon(getClass().getResource("image/dog3.jpg"));
		pic[8]=new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));
		pic[9]=new ImageIcon(getClass().getResource("image/mouse1.jpg"));
		pic[10]=new ImageIcon(getClass().getResource("image/mouse2.jpg"));
		pic[11]=new ImageIcon(getClass().getResource("image/mouse3.jpg"));
		
		Action catAction= new MyAction("cat",pic[0]);
		m.add(catAction);
		Action dogAction= new MyAction("dog",pic[4]);
		m.add(dogAction);
		Action mosAction= new MyAction("mos",pic[8]);
		m.add(mosAction);
		
		mb.add(m);
		albumBar.add(mb);
		albumBar.add(catAction);
		albumBar.add(dogAction);
		albumBar.add(mosAction);
		albumBar.setSize(800,50);
		albumBar.setLocation(0,0);
		add(albumBar);
		
		rightButton.setSize(200,100);
		rightButton.setLocation(500,550);
		add(rightButton);
		rightButton.addActionListener(this);
		
		leftButton.setSize(200,100);
		leftButton.setLocation(300,550);
		add(leftButton);
		leftButton.addActionListener(this);
		
		label.setLocation(300,50);
		label.setSize(1000,400);
		label.setIcon(pic[1]);
		add(label);
		
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==leftButton){
			if (type==1){
                number1-=1;
                if (number1<1){
                	number1=1;
                	JOptionPane.showMessageDialog(null,"No more picture!!!");
                }
				label.setIcon(pic[number1]);
			}
			
			if (type==2){
                number2-=1;
                if (number2<5){
                	number2=5;
                	JOptionPane.showMessageDialog(null,"No more picture!!!");
                }
				label.setIcon(pic[number2]);
			}
			
			if (type==3){
                number3-=1;
                if (number3<9){
                	number3=9;
                	JOptionPane.showMessageDialog(null,"No more picture!!!");
                }
				label.setIcon(pic[number3]);
			}
		}
		
        if (e.getSource()==rightButton){
        	if (type==1){
                number1+=1;
                if (number1>3){
                	number1=3;
                	JOptionPane.showMessageDialog(null,"No more picture!!!");
                }
				label.setIcon(pic[number1]);
			}
			
			if (type==2){
                number2+=1;
                if (number2>7){
                	number2=7;
                	JOptionPane.showMessageDialog(null,"No more picture!!!");
                }
				label.setIcon(pic[number2]);
			}
			
			if (type==3){
                number3+=1;
                if (number3>11){
                	number3=11;
                	JOptionPane.showMessageDialog(null,"No more picture!!!");
                }
				label.setIcon(pic[number3]);
			}
		}
	}
	
	private class MyAction extends AbstractAction{
		String animal;
		public MyAction(String animal,Icon icon) {
			super(animal,icon);
			this.animal=animal;
		}

		public void actionPerformed(ActionEvent e){
			if (animal.equals("cat")){
				label.setIcon(pic[1]);
				type=1;
			}
			
			if (animal.equals("dog")){
				label.setIcon(pic[5]);
				type=2;
			}
			
			if (animal.equals("mos")){
				label.setIcon(pic[9]);
				type=3;
			}
		}
	}
}
