package fe.s100502508;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Framework extends JFrame
{
	private Locale[] availablelocales=Locale.getAvailableLocales();
	private ResourceBundle res=ResourceBundle.getBundle("Confidential");
	private int xcoordinate;
	private int ycoordinate;
	
	JLabel titlelabel=new JLabel(res.getString("Title"),JLabel.CENTER);
	JLabel languagelabel=new JLabel(res.getString("Language"),JLabel.CENTER);
	JLabel positionlabel=new JLabel(res.getString("Position"),JLabel.CENTER);
	JLabel coordinate=new JLabel("( , )",JLabel.CENTER);
	JLabel resultlabel=new JLabel(res.getString("Result"),JLabel.CENTER);
	JLabel resultlabel2=new JLabel("Nothing",JLabel.CENTER);
	
	JPanel panel1=new JPanel(new GridLayout(1, 3, 0, 0));
	JPanel panel2=new JPanel(new GridLayout(1, 4, 0, 0));
	JPanel panel3=new JPanel(new GridLayout(3, 1, 0, 0));
	
	JComboBox comboBox=new JComboBox();
	
	DrawBuilings drawBuilings=new DrawBuilings();
	
	public Framework()
	{
		for(int a=0;a<availablelocales.length;a++)
		{
			comboBox.addItem(availablelocales[a].getDisplayName()+" "+availablelocales[a].toString());
		}
		
		panel1.add(titlelabel);
		panel1.add(languagelabel);
		panel1.add(comboBox);
		
		panel2.add(positionlabel);
		panel2.add(coordinate);
		panel2.add(resultlabel);
		panel2.add(resultlabel2);
		
		add(panel1,BorderLayout.NORTH);
		add(drawBuilings,BorderLayout.CENTER);
		add(panel2,BorderLayout.SOUTH);
		
		if((xcoordinate>=drawBuilings.getxbase())&&(xcoordinate<=drawBuilings.getxbase()+190)&&(ycoordinate>=drawBuilings.getybase())&&(ycoordinate<=drawBuilings.getybase()+150))
		{
			if((xcoordinate<=drawBuilings.getxbase()+140)&&(ycoordinate>=drawBuilings.getybase()+50))
			{
				resultlabel2.setText("Hit both builing A and B");
			}
			resultlabel2.setText("Hit building A");
		}
		comboBox.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				setLocale(availablelocales[(comboBox.getSelectedIndex())]);
				res=ResourceBundle.getBundle("Confidential",getLocale());
				titlelabel.setText(res.getString("Title"));
				languagelabel.setText(res.getString("Language"));
				positionlabel.setText(res.getString("Position"));
				resultlabel.setText(res.getString("Result"));
				panel1.updateUI();
				panel2.updateUI();
			}
		});
		
		addMouseListener(new MouseListener() 
		{
			public void mouseReleased(MouseEvent arg0) 
			{
			}
			public void mousePressed(MouseEvent arg0) 
			{
			}
			public void mouseExited(MouseEvent arg0) 
			{
			}
			public void mouseEntered(MouseEvent arg0) 
			{
			}
			public void mouseClicked(MouseEvent e) 
			{
				xcoordinate=e.getX();
				ycoordinate=e.getY();
				coordinate.setText("("+xcoordinate+","+ycoordinate+")");
				drawBuilings.setMousePositionX(xcoordinate);
				drawBuilings.setMousePositionY(ycoordinate);
			}
		});
	}
}
