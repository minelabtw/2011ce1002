package fe.s100502508;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;


public class FE11 extends JApplet
{
	private int gallerychoose=0;
	private int imagechoose=0;
	
	private JButton PreviousButton=new JButton("<-");
	private JButton NextButton=new JButton("->");
	private ImageIcon[] imageIcons=new ImageIcon[9];
	private ImageIcon catIcon=new ImageIcon(getClass().getResource("image/cat_icon.jpg"));//讀圖片
	private ImageIcon dogIcon=new ImageIcon(getClass().getResource("image/dog_icon.jpg"));//讀圖片
	private ImageIcon mouseIcon=new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));//讀圖片
	
	JPanel panel=new JPanel();
	JPanel buttonpanel=new JPanel();
	JLabel label=new JLabel();
	JToolBar toolBar=new JToolBar("My ToolBar",JToolBar.VERTICAL);//create toolbar
	JMenuBar menuBar=new JMenuBar();//create menu
	JMenu menu=new JMenu("Animals Gallery");
	
	public FE11()
	{
		for(int a=1;a<=9;a++)//把九張圖片讀進來
		{
			if(a>=1&&a<=3)
			{
				imageIcons[a-1]=new ImageIcon(getClass().getResource("image/cat"+a+".jpg"));
			}
			else if(a>=4&&a<=6)
			{
				imageIcons[a-1]=new ImageIcon(getClass().getResource("image/dog"+(a-3)+".jpg"));
			}
			else 
			{
				imageIcons[a-1]=new ImageIcon(getClass().getResource("image/mouse"+(a-6)+".jpg"));
			}
		}
		
		//create actions
		Action cataction=new MyAction("Cat", catIcon);
		Action dogaction=new MyAction("Dog", dogIcon);
		Action mouseaction=new MyAction("Mouse", mouseIcon);
		
		//ass actions to the menu
		menu.add(cataction);
		menu.add(dogaction);
		menu.add(mouseaction);
		menuBar.add(menu);
		
		//add actions to the toolbar
		toolBar.add(cataction);
		toolBar.add(dogaction);
		toolBar.add(mouseaction);
		
		panel.add(label);
		add(menuBar,BorderLayout.NORTH);
		add(panel,BorderLayout.CENTER);
		add(toolBar,BorderLayout.EAST);
		buttonpanel.add(PreviousButton,BorderLayout.WEST);
		buttonpanel.add(NextButton,BorderLayout.EAST);
		add(buttonpanel,BorderLayout.SOUTH);
		
		PreviousButton.addActionListener(new ActionListener()//讓Previous按鈕起作用
		{
			public void actionPerformed(ActionEvent e) 
			{
				if(gallerychoose!=0)//選了相簿後
				{
					imagechoose--;
					if(gallerychoose==1)//貓的相簿
					{
						if(imagechoose<0)//已經是第一張
						{
							JOptionPane.showMessageDialog(null, "已經是第一張了");//show a message
							imagechoose++;
						}
						else 
						{
							label.setIcon(imageIcons[imagechoose]);//set icon to label
						}
					}
					else if(gallerychoose==2)//狗相簿
					{
						if(imagechoose<0)//已經是第一張
						{
							JOptionPane.showMessageDialog(null, "已經是第一張了");//show a message
							imagechoose++;
						}
						else 
						{
							label.setIcon(imageIcons[imagechoose+3]);//set icon to label
						}
					}
					else//老鼠相簿 
					{
						if(imagechoose<0)//已經是第一張
						{
							JOptionPane.showMessageDialog(null, "已經是第一張了");//show a message
							imagechoose++;
						}
						else 
						{
							label.setIcon(imageIcons[imagechoose+6]);//set icon to label
						}
					}
				}
				else//未選相簿的處理  
				{
					JOptionPane.showMessageDialog(null, "Please choose a gallary first");
				}
			}
		});
		NextButton.addActionListener(new ActionListener()//讓Next按鈕起作用
		{
			public void actionPerformed(ActionEvent e) 
			{
				if(gallerychoose!=0)//選了相簿後
				{
					imagechoose++;
					if(gallerychoose==1)//貓的相簿
					{
						if(imagechoose>2)//已經是最後一張
						{
							JOptionPane.showMessageDialog(null, "已經是最後一張了");//show a message
							imagechoose--;
						}
						else 
						{
							label.setIcon(imageIcons[imagechoose]);//set icon to label
						}
					}
					else if(gallerychoose==2)//狗相簿
					{
						if(imagechoose>2)//已經是最後一張
						{
							JOptionPane.showMessageDialog(null, "已經是最後一張了");//show a message
							imagechoose--;
						}
						else 
						{
							label.setIcon(imageIcons[imagechoose+3]);//set icon to label
						}
					}
					else//老鼠相簿  
					{
						if(imagechoose>2)//已經是最後一張
						{
							JOptionPane.showMessageDialog(null, "已經是最後一張了");//show a message
							imagechoose--;
						}
						else 
						{
							label.setIcon(imageIcons[imagechoose+6]);//set icon to label
						}
					}
				}
				else//未選相簿的處理 
				{
					JOptionPane.showMessageDialog(null, "Please choose a gallary first");//show a message
				}
			}
		});
	}
	
	private class MyAction extends AbstractAction
	{
		String name;

		public MyAction(String name,Icon icon)//constructor 
		{
			super(name,icon);
			this.name=name;
		}
		
		public void actionPerformed(ActionEvent e)//handle
		{
			imagechoose=0;
			if(name.equals("Cat"))//選貓相簿
			{
				gallerychoose=1;
				label.setIcon(imageIcons[0]);//set icon to label
			}
			else if(name.equals("Dog"))//選狗相簿
			{
				gallerychoose=2;
				label.setIcon(imageIcons[3]);//set icon to label
			}
			else if(name.equals("Mouse"))//選老鼠相簿
			{
				gallerychoose=3;
				label.setIcon(imageIcons[6]);//set icon to label
			}
		}
	}
	
	public static void main(String[] args)
	{
		JFrame frame=new JFrame();//create a frame
		FE11 applet=new FE11();//create an instance of the applet
		frame.add(applet);//add the applet to the frame
		
		frame.setSize(800, 600);//set the frame size
		frame.setLocationRelativeTo(null);//center a frame
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);//display the frame
	}
}


