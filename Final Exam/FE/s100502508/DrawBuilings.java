package fe.s100502508;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

import javax.swing.JPanel;

public class DrawBuilings extends JPanel
{
	private int xBase=0;
	private int yBase;
	private int newxbase;
	private int newybase;
	private int Xcoordinate;
	private int Ycoordinate;
	
	Timer timer=new Timer(200,new TimerListener());
	
	public DrawBuilings()
	{
		
	}
	
	protected void paintComponent(Graphics g)//override paintComponent
	{
		super.paintComponent(g);//draw things in the superclass
		yBase=getHeight()/2;
		g.setColor(Color.BLACK);
		g.drawRect(xBase+60, yBase-50, 190, 150);
		newxbase=xBase+60;
		newybase=yBase+60;
		g.setColor(Color.BLUE);
		g.drawRect(xBase+10, yBase, 190, 200);
		timer.start();
		if(drawredpoint())
		{
			g.setColor(Color.RED);
			g.fillOval(Xcoordinate, Ycoordinate, 10, 10);
			repaint();
		}
	}
	
	class TimerListener implements ActionListener//Handle ActionEvent
	{
		public void actionPerformed(ActionEvent e) 
		{
			// TODO Auto-generated method stub
			xBase++;
			repaint();
		}
		
	}
	public void setMousePositionX(int xcoordinate)
	{
		Xcoordinate=xcoordinate-13;
	}
	public void setMousePositionY(int ycoordinate)
	{
		Ycoordinate=ycoordinate-62;
	}
	public Boolean drawredpoint()
	{
		if(Xcoordinate==0&&Ycoordinate==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	public int getxbase()
	{
		return newxbase;
	}
	public int getybase()
	{
		return newybase;
	}
}
