package fe.s100502509;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;



public class FE11 extends JApplet{
	private JLabel display_Label = new JLabel("");
	int cat_number=0;
	int dog_number=3;
	int mouse_number=6;
	int Category=0;
	JButton next_Button = new JButton("Next");
	JButton previous_Button = new JButton("Previous");

	private ImageIcon [] Animal_ImageIcon = {
			new ImageIcon(getClass().getResource("image/cat1.jpg")),
			new ImageIcon(getClass().getResource("image/cat2.jpg")),
			new ImageIcon(getClass().getResource("image/cat3.jpg")),
			new ImageIcon(getClass().getResource("image/dog1.jpg")),
			new ImageIcon(getClass().getResource("image/dog2.jpg")),
			new ImageIcon(getClass().getResource("image/dog3.jpg")),
			new ImageIcon(getClass().getResource("image/mouse1.jpg")),
			new ImageIcon(getClass().getResource("image/mouse2.jpg")),
			new ImageIcon(getClass().getResource("image/mouse3.jpg")),
		
	};// read the picture
	
	private ImageIcon [] animal_Action_Icons = {
			new ImageIcon(getClass().getResource("image/cat_icon.jpg")),
			new ImageIcon(getClass().getResource("image/dog_icon.jpg")),
			new ImageIcon(getClass().getResource("image/mouse_icon.jpg")),
	};
	
	
	public FE11(){
		
		JPanel panel1 = new JPanel();
		
		JButton next_Button = new JButton("Next");
		JButton previous_Button = new JButton("Previous");
		next_Button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(Category==0){
					if(cat_number==2){
						JOptionPane.showMessageDialog(null,"The point is the last Picture ");
					}
					
					else{
						cat_number++;
						display_Label.setIcon(Animal_ImageIcon[cat_number]);
					}
					
				}
				
				else if(Category==1){
						if(dog_number==5){
							JOptionPane.showMessageDialog(null,"The point is the last Picture ");
						}
						else{
							dog_number++;
							display_Label.setIcon(Animal_ImageIcon[dog_number]);
						}
					}
				
				else{
					if(mouse_number==8){
						JOptionPane.showMessageDialog(null,"The point is the last Picture ");
					}
					
					else{
					mouse_number++;
						display_Label.setIcon(Animal_ImageIcon[mouse_number]);
					}
				// TODO Auto-generated method stub
				
			}
			}
		});
		
		previous_Button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if(Category==0){

					if(cat_number==0){
						JOptionPane.showMessageDialog(null,"The point is the first Picture ");
					}
					
					else{
						cat_number--;
						display_Label.setIcon(Animal_ImageIcon[cat_number]);
					}
					
				}
				
				else if(Category==1){
					if(dog_number==3){
						JOptionPane.showMessageDialog(null,"The point is the first Picture ");
					}
					else{
						dog_number--;
						display_Label.setIcon(Animal_ImageIcon[dog_number]);
					}
				}
				
				else{
					if(mouse_number==6){
						JOptionPane.showMessageDialog(null,"The point is the first Picture ");
					}
					
					else{
					mouse_number--;
						display_Label.setIcon(Animal_ImageIcon[mouse_number]);
					}
				
			}
			}
		});

		
		panel1.setLayout(new GridLayout(1, 2));
		panel1.add(previous_Button);
		panel1.add(next_Button);
		
		Action Cat_Action = new MyAction("Cat", animal_Action_Icons[0],
				"Display the image of Cat", 0);// use Action
		
		Action Dog_Action = new MyAction("Dog", animal_Action_Icons[1],
				"Display the image of Cat", 1);// use Action
		
		Action Mouse_Action = new MyAction("Mouse", animal_Action_Icons[2],
				"Display the image of Cat", 2);// use Action
	

		JMenuBar menuBar = new JMenuBar();//Declare JMenuBar
		JMenu menu = new JMenu("Animal Category");

		menu.add(Cat_Action);//add Action to menu
		menu.add(Dog_Action);
		menu.add(Mouse_Action);
		menuBar.add(menu);

		JToolBar jToolBar = new JToolBar(JToolBar.VERTICAL);//Declare JToolBar
		jToolBar.add(Cat_Action);//add Action to JToolBar
		jToolBar.add(Dog_Action);
		jToolBar.add(Mouse_Action);


		add(menuBar, BorderLayout.NORTH);//add to JApplet
		add(jToolBar, BorderLayout.EAST);
		add(panel1,BorderLayout.SOUTH);
		add(display_Label);

	}
	

	private class MyAction extends AbstractAction {
		;
		private int Decide_Animal;

		MyAction(String Name, ImageIcon Animal_Icon,
				String Animal_Description, int Decide_Animal) {
			super(Name, Animal_Icon);
			this.Decide_Animal = Decide_Animal;
			putValue(Action.SHORT_DESCRIPTION, Animal_Description);
		}

		public void actionPerformed(ActionEvent e) {
			if (Decide_Animal == 0) {
				Category=0;
				display_Label.setIcon(Animal_ImageIcon[cat_number]);//set Picture Canada
			}

			else if (Decide_Animal == 1) {//use the Flag of China
				Category=1;
				display_Label.setIcon(Animal_ImageIcon[dog_number]);
			}

			else {//use the Flag of German
				Category=2;
				display_Label.setIcon(Animal_ImageIcon[mouse_number]);
			}



		}

	}

}
