package fe.s100502509;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Framework extends JFrame implements ActionListener{
	JComboBox ComboBox_Locale;
	JLabel jLabel1 = new JLabel("Title");
	JLabel jLabe2 = new JLabel("Language");
	JLabel jLabe3= new JLabel("");
	JLabel jLabe4 = new JLabel("Position");
	JLabel jLabel5 = new JLabel("Current Position");
	JLabel jLabe6 = new JLabel("Result");
	JLabel jLabe7 = new JLabel("Final Result");
	Locale[] locale = Locale.getAvailableLocales();
	ResourceBundle resourceBundle = ResourceBundle.getBundle("Confidential");
	
	String [] languageString = {
		"En","Zh","Jp","Fr"	
	};
	
	JComboBox language_ComboBox = new JComboBox();
	public Framework(){
		
		ComboBox_Locale = new JComboBox(); // Choose locales
		for (int i = 0; i < locale.length; i++) { // Choose items of Combobox
			ComboBox_Locale.addItem(locale[i].getDisplayName() + " "
					+ locale[i].toString());
		}
		
		ComboBox_Locale.addActionListener(new ActionListener() { // Action happens when items changed
			public void actionPerformed(ActionEvent e) {
				setLocale(locale[ComboBox_Locale.getSelectedIndex()]);
				updateString();
			}
		});
		
		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(1,3));
		for(int i=0;i<4;i++){
			language_ComboBox.addItem(languageString[i]);
		}
		
		
		p1.add(jLabel1);
		p1.add(jLabe2);
		p1.add(language_ComboBox);
		
		JPanel p2 = new JPanel();
		p2.setLayout(new GridLayout(1,4));
		p2.add(jLabe4);
		p2.add(jLabel5);
		p2.add(jLabe6);
		p2.add(jLabe7);
		
		add(p1,BorderLayout.NORTH);
		add(new DrawBuildings(),BorderLayout.CENTER);
		add(p2,BorderLayout.SOUTH);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public void updateString(){
		resourceBundle = ResourceBundle.getBundle("getResource", getLocale());
		jLabel1.setText(resourceBundle.getString("Title"));
		jLabe2.setText(resourceBundle.getString("language"));
		jLabe4.setText(resourceBundle.getString("Position"));
		jLabe6.setText(resourceBundle.getString("Result"));
	}

}
