package fe.s100502509;

import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class DrawBuildings extends JPanel{
	int x_Base;
	int y_Base= getHeight();
	int velocity=5;
	int times=0;
	public DrawBuildings(){
		Timer t = new Timer(200, new TimerListener());
		t.start();
	}
	
	protected void paintComponent(Graphics g){//graphic the car
		super.paintComponent(g);
		Polygon polygon = new Polygon();
		if(times==0){
			x_Base = 0;
			y_Base = getHeight()/2;
		}
		
		else{
			x_Base+=velocity;
		}
		
		if(x_Base+10>getWidth()){
			x_Base=0;
		}
		
		int[] p1x = { x_Base + 60, x_Base + 250, x_Base + 250, x_Base + 60 };
		int[] p1y = { y_Base-50, y_Base-50, y_Base + 100, y_Base +100 };
		
		int[] p2x = { x_Base + 10, x_Base + 200, x_Base + 200, x_Base + 10 };
		int[] p2y = { y_Base, y_Base, y_Base + 200, y_Base+200 };
		
		for (int i = 0; i < 4; i++)
			polygon.addPoint(p1x[i], p1y[i]);
		
		for (int i = 0; i < 3; i++)
			g.drawLine(p2x[i], p2y[i], p2x[i+1], p2y[i+1]);
			
		g.drawLine(p2x[3], p2y[3], p2x[0], p2y[0]);
		g.drawPolygon(polygon);
	}
		
		
		
	
	public void setMousePositionX(int x_temp){
		
	}
	
	public void setMousePositionY(int y_temp){
		
	}
	
	class TimerListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			times++;
			repaint();
		}

		
	}
	
	
}

