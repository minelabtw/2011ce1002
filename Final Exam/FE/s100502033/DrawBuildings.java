package fe.s100502033;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class DrawBuildings extends JPanel
{
	private Timer timer;
	private int xBase = 0;
	private int yBase = 300;
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.drawRect(xBase + 60, yBase - 50, 200, 150);
		g.drawRect(xBase + 10, yBase, 190, 200);
		g.drawString("A", xBase + 50, yBase + 100);
		g.drawString("B", xBase, yBase + 200);
	}
	public DrawBuildings()
	{
		timer = new Timer(200, new TimerListener());
		timer.start();
	}
	class TimerListener implements ActionListener 
	{
		public void actionPerformed(ActionEvent e) 
	    {
			xBase++;
			repaint();
	    }
	}
}
