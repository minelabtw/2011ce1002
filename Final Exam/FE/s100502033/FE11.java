package fe.s100502033;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class FE11 extends JApplet
{
	private ImageIcon cat[]= new ImageIcon[3]; 
	private ImageIcon dog[]= new ImageIcon[3]; 
	private ImageIcon mouse[]= new ImageIcon[3];
	private ImageIcon cat0= new ImageIcon(getClass().getResource("image/cat_icon.jpg")); 
	private ImageIcon dog0= new ImageIcon(getClass().getResource("image/dog_icon.jpg")); 
	private ImageIcon mouse0= new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));
	private JLabel jlblImage = new JLabel("");
	private JButton Previous = new JButton("Previous");
	private JButton Next = new JButton("Next");
	private JPanel p1 = new JPanel();
	private int number = 0;
	private int number1 = 0;
    class MyAction extends AbstractAction //運行ACTION
	{
	    String name;
	    MyAction(String name, Icon icon) 
	    {
	      super(name, icon);
	      this.name = name;
	    }
	    public void actionPerformed(ActionEvent event)
	    {
	    	if (name.equals("cat"))//點擊CAT時執行
	    	{
	    		number = 0;
	    		number1 = 0;
	    		jlblImage.setIcon(cat[0]);
	    	}
	    	else if(name.equals("dog"))//點擊DOG時執行
	    	{
	    		number = 0;
	    		number1 = 1;
	    		jlblImage.setIcon(dog[0]);
	    	}
	    	else if(name.equals("mouse"))//點擊MOUSE時執行
	    	{
	    		number = 0;
	    		number1 = 2;
	    		jlblImage.setIcon(mouse[0]);
	    	}
	    }
	}
	public FE11()
	{
		p1.setLayout(new GridLayout(1,2));
		cat[0] = new ImageIcon(getClass().getResource("image/cat1.jpg"));
		cat[1] = new ImageIcon(getClass().getResource("image/cat2.jpg"));
		cat[2] = new ImageIcon(getClass().getResource("image/cat3.jpg"));
		dog[0] = new ImageIcon(getClass().getResource("image/dog1.jpg"));
		dog[1] = new ImageIcon(getClass().getResource("image/dog2.jpg"));
		dog[2] = new ImageIcon(getClass().getResource("image/dog3.jpg"));
		mouse[0] = new ImageIcon(getClass().getResource("image/mouse1.jpg"));
		mouse[1] = new ImageIcon(getClass().getResource("image/mouse2.jpg"));
		mouse[2] = new ImageIcon(getClass().getResource("image/mouse3.jpg"));
		Action cat1 = new MyAction("cat" , cat0);//初始化ACTION
		Action dog1 = new MyAction("dog" , dog0);//初始化ACTION
		Action mouse1 = new MyAction("mouse" , mouse0);//初始化ACTION
		JMenuBar JB = new JMenuBar();    
		this.setJMenuBar(JB);
		JMenu fileMenu = new JMenu("Animals Gallery");
		JB.add(fileMenu);
	    JToolBar jToolBar1 = new JToolBar(JToolBar.VERTICAL);
	    jToolBar1.setFloatable(false);
	    jToolBar1.setBorder(BorderFactory.createLineBorder(Color.BLACK));
	    jToolBar1.add(cat1);
	    jToolBar1.add(dog1);
	    jToolBar1.add(mouse1);
		fileMenu.add(cat1);
		fileMenu.add(dog1);
		fileMenu.add(mouse1);
		Previous.setBounds(100, 100, 100, 100);
		p1.add(Previous);
		p1.add(Next);
		jlblImage.setIcon(cat[0]);
		this.add(p1 , BorderLayout.SOUTH);
		this.add(jlblImage);
		add(jToolBar1, BorderLayout.EAST);
		Listener list = new Listener();
		Previous.addActionListener(list);
		Next.addActionListener(list);
	}
	public class Listener implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			if(event.getSource() == Previous)
			{
				switch(number)//判定是否到盡頭
				{
					case 0://等於0時到盡頭
						JOptionPane.showMessageDialog(null , "第一張圖");
						break;
					case 1://等於1時就上一張
						number--;
						if(number1 == 0)
						{
							jlblImage.setIcon(cat[number]);
						}
						else if(number1 == 1)
						{
							jlblImage.setIcon(dog[number]);
						}
						else if(number1 == 2)
						{
							jlblImage.setIcon(mouse[number]);
						}
						break;
					case 2://等於2時就上一張
						number--;
						if(number1 == 0)
						{
							jlblImage.setIcon(cat[number]);
						}
						else if(number1 == 1)
						{
							jlblImage.setIcon(dog[number]);
						}
						else if(number1 == 2)
						{
							jlblImage.setIcon(mouse[number]);
						}
						break;
					default:
						break;
				}
			}
			if(event.getSource() == Next)
			{
				switch(number)//判定是否到盡頭
				{
					case 0://等於0就下一張
						number++;
						if(number1 == 0)
						{
							jlblImage.setIcon(cat[number]);
						}
						else if(number1 == 1)
						{
							jlblImage.setIcon(dog[number]);
						}
						else if(number1 == 2)
						{
							jlblImage.setIcon(mouse[number]);
						}
						break;
					case 1://等於1就下一張
						number++;
						if(number1 == 0)
						{
							jlblImage.setIcon(cat[number]);
						}
						else if(number1 == 1)
						{
							jlblImage.setIcon(dog[number]);
						}
						else if(number1 == 2)
						{
							jlblImage.setIcon(mouse[number]);
						}
						break;
					case 2://等於2就到盡頭
						JOptionPane.showMessageDialog(null , "最後一張");
						break;
					default:
						break;
				}
			}
		}
	}
}
