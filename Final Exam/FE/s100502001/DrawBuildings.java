package fe.s100502001;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.JPanel;
import javax.swing.Timer;

public class DrawBuildings extends JPanel{
	private int X=20,Y=300;
	private Timer TimerListener=new Timer(200,new TimerListener());
	private boolean forward=true;
	public DrawBuildings(){
		TimerListener.start();
	}
	public void paintComponent(Graphics g){
		super.paintComponents(g);
	
		g.drawRect(X+60, Y-50,190, 150);
		g.drawRect(X+10,Y,190, 200);
		if(!forward){
			if(X+250>getWidth()){
				X=getWidth()-250;
				forward=false;
			}
			else
				X--;
		}
		else {
			if(X<0){
				X=0;
				forward=true;
			}
			else
				X++;
		}
	}
	class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			repaint();
			
		}
		
	}
}
