package fe.s100502001;


import javax.swing.*;


import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ResourceBundle;
public class Framework extends JFrame{
	//Ū��
	private ResourceBundle res=ResourceBundle.getBundle("Confidential");
	private ResourceBundle res_en=ResourceBundle.getBundle("Confidential_en");
	private ResourceBundle res_zh=ResourceBundle.getBundle("Confidential_zh");
	private ResourceBundle res_ja=ResourceBundle.getBundle("Confidential_ja");
	private ResourceBundle res_fr=ResourceBundle.getBundle("Confidential_fr");
	private JLabel jl1=new JLabel("The Bomber Infrared Screen");
	private JLabel jl2=new JLabel();
	
	private JLabel jl4=new JLabel();
	private JLabel jl5=new JLabel();
	private JLabel jl6=new JLabel();
	private JLabel jl7=new JLabel();
	private String[] str={"En","Zh","Jp","Fr"};
	private JComboBox jcblan=new JComboBox(str);
	private DrawBuildings building=new DrawBuildings();
	public Framework(){
		JPanel jp1=new JPanel();
		JPanel jp2=new JPanel();
		jp1.setLayout(new GridLayout(1,3));
		jp1.add(jl1);
		jp1.add(jl2);
		jp1.add(jcblan);
		jp2.setLayout(new GridLayout(1,4));
		jp2.add(jl4);
		jp2.add(jl5);
		jp2.add(jl6);
		jp2.add(jl7);
		this.add(jp1,BorderLayout.NORTH);
		this.add(jp2,BorderLayout.SOUTH);
		this.add(building,BorderLayout.CENTER);
		jcblan.addItemListener(new ItemListener(){  //��ܻy�t
			public void itemStateChanged(ItemEvent e) {
				if(jcblan.getSelectedIndex()==0)
					setStyle(res_en);
				else if(jcblan.getSelectedIndex()==1)
					setStyle(res_zh);
				else if(jcblan.getSelectedIndex()==2)
					setStyle(res_ja);
				else if(jcblan.getSelectedIndex()==3)
					setStyle(res_fr);
			}
			
		});
		
	}
	public void setStyle(ResourceBundle temp){
		jl1.setText(temp.getString("Title"));
		jl2.setText(temp.getString("Language"));
		jl4.setText(temp.getString("Position"));
		jl6.setText(temp.getString("Result"));
	}
	
}
