package fe.s100502001;
import java.applet.Applet;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import javax.swing.*;
public class FE11 extends JApplet/*extends JFrame*/{
	private ImageIcon[] image={
			new ImageIcon(getClass().getResource("image/cat_icon.jpg")),//圖檔
			new ImageIcon(getClass().getResource("image/dog_icon.jpg")),
			new ImageIcon(getClass().getResource("image/mouse_icon.jpg")),
			new ImageIcon(getClass().getResource("image/cat1.jpg")),//3
			new ImageIcon(getClass().getResource("image/cat2.jpg")),
			new ImageIcon(getClass().getResource("image/cat3.jpg")),
			new ImageIcon(getClass().getResource("image/dog1.jpg")),//6
			new ImageIcon(getClass().getResource("image/dog2.jpg")),
			new ImageIcon(getClass().getResource("image/dog3.jpg")),
			new ImageIcon(getClass().getResource("image/mouse1.jpg")),//9
			new ImageIcon(getClass().getResource("image/mouse2.jpg")),
			new ImageIcon(getClass().getResource("image/mouse3.jpg")),};
	private JLabel jlimg=new JLabel(); //show image in the center
	private JButton jppre=new JButton("Previous");
	private JButton jpnext=new JButton("Next");
	private int index=0;//哪張圖
	private int index_curr=0;
	 
	public FE11(){
		Action cat=new ImgAction("Cat",image[0],"cat");
		Action dog=new ImgAction("Dog",image[1],"dog");
		Action mouse=new ImgAction("Cat",image[2],"mouse");
		JMenuBar jmb=new JMenuBar();
		JMenu jmicon=new JMenu("Animal");
		jmicon.add(cat);
		jmicon.add(dog);
		jmicon.add(mouse);
		setJMenuBar(jmb);
		jmb.add(jmicon);
		JToolBar jtoolicon=new JToolBar(JToolBar.VERTICAL);
		jtoolicon.add(cat);
		jtoolicon.add(dog);
		jtoolicon.add(mouse);
		JPanel jpchoose=new JPanel();
		jpchoose.add(jppre,BorderLayout.WEST);
		jpchoose.add(jpnext,BorderLayout.EAST);
		this.add(jmb,BorderLayout.NORTH);
		this.add(jlimg,BorderLayout.CENTER);
		this.add(jtoolicon,BorderLayout.EAST);
		this.add(jpchoose,BorderLayout.SOUTH);
	}

	class ImgAction extends AbstractAction{
		private String name;
		private Icon icon;
		private select s=new select();
		public ImgAction(String name,Icon icon,String desc){
			super(name,icon);
			putValue(Action.SHORT_DESCRIPTION,desc);
			this.name=name;
			this.icon=icon;
			
		}
		
		public void actionPerformed(ActionEvent e) {
			//index=0;
			if(icon.equals(image[0])){ //選擇"cat"項補
				//index=0;
				jppre.addActionListener(s);
				jpnext.addActionListener(s);
				index_curr=3;
			}
			else if(icon.equals(image[1])){ //選擇"dog"項補
				//index=0;
				jppre.addActionListener(s);
				jpnext.addActionListener(s);
				jlimg.setIcon(image[index+6]);
				index_curr=6;
			}
			else if(icon.equals(image[2])){ //選擇"mouse"項補
				//index=0;
				jppre.addActionListener(s);
				jpnext.addActionListener(s);
				index_curr=9;
			}
			
			
		}
	}
	class select implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if(e.getSource()==jppre){
				index--;
				index_curr--;
				if(index<0){
					JOptionPane.showMessageDialog(null, "沒有上一張咯");
					index=0;
					jlimg.setIcon(null);
				}
				else if(index>2){
					JOptionPane.showMessageDialog(null, "沒有下一張咯");
					index=2;
					jlimg.setIcon(null);
				}
				else 
					jlimg.setIcon(image[index_curr]);
			}
			else if(e.getSource()==jpnext){
				index++;
				index_curr++;
				if(index<0){
					JOptionPane.showMessageDialog(null, "沒有上一張咯");
					index=0;
					jlimg.setIcon(null);
				}
				else if(index>2){
					JOptionPane.showMessageDialog(null, "沒有下一張咯");
					index=2;
					jlimg.setIcon(null);
				}
				else 
					jlimg.setIcon(image[index_curr]);
			}
			revalidate();
			System.out.println("index"+index);
			System.out.println("index_curr"+index_curr);
		}
		
	}
	
}
