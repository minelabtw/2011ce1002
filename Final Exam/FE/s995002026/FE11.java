package fe.s995002026;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import java.applet.*;

public class FE11 extends JApplet implements ActionListener {
	int i=0;		//換圖
	int j=1;		//判斷有沒有到最後一張或第一張
	ImageIcon mouse_icon = new ImageIcon(getClass().getResource(
			"image/mouse_icon.jpg"));
	ImageIcon cat_icon = new ImageIcon(getClass().getResource(
			"image/cat_icon.jpg"));
	ImageIcon dog_icon = new ImageIcon(getClass().getResource(
			"image/dog_icon.jpg"));
	ImageIcon[] galleries = new ImageIcon[9];
	JMenuBar menubar = new JMenuBar();
	JMenu menu = new JMenu("Galleries");
	JToolBar toolbar = new JToolBar(JToolBar.VERTICAL);
	JLabel l1 = new JLabel();
	JPanel p1 = new JPanel();			//整合
	JPanel p2 = new JPanel();			//圖片
	JPanel p3 = new JPanel(new GridLayout(1, 2));	//按鈕平面
	JButton next = new JButton("next");
	JButton previous = new JButton("previous");

	public FE11() {
		next.addActionListener(this);
		previous.addActionListener(this);
		setJMenuBar(menubar);
		menubar.add(menu);
		galleries[0] = new ImageIcon(getClass().getResource("image/cat1.jpg"));
		galleries[1] = new ImageIcon(getClass().getResource("image/cat2.jpg"));
		galleries[2] = new ImageIcon(getClass().getResource("image/cat3.jpg"));
		galleries[3] = new ImageIcon(getClass().getResource("image/dog1.jpg"));
		galleries[4] = new ImageIcon(getClass().getResource("image/dog2.jpg"));
		galleries[5] = new ImageIcon(getClass().getResource("image/dog3.jpg"));
		galleries[6] = new ImageIcon(getClass()
				.getResource("image/mouse1.jpg"));
		galleries[7] = new ImageIcon(getClass()
				.getResource("image/mouse2.jpg"));
		galleries[8] = new ImageIcon(getClass()
				.getResource("image/mouse3.jpg"));
		Action mouse = new MyAction("mouse", mouse_icon);
		Action cat = new MyAction("cat", cat_icon);
		Action dog = new MyAction("dog", dog_icon);
		menu.add(mouse);
		menu.add(dog);
		menu.add(cat);
		toolbar.add(mouse);
		toolbar.add(dog);
		toolbar.add(cat);
		p2.add(l1);
		p3.add(previous);
		p3.add(next);
		p1.add(p3, BorderLayout.NORTH);
		p1.add(p2,BorderLayout.CENTER);
		
		add(p1);
		add(menubar, BorderLayout.NORTH);
		add(toolbar, BorderLayout.EAST);
	}

	public void actionPerformed(ActionEvent e) {	//換圖
		if(e.getSource()==next){
			j=j+1;						//判斷第幾張
			if(j==4){
				j=j-1;					//到底,返回
				JOptionPane.showMessageDialog(null, "沒有下一張了");
			}
			else{
				i=i+1;
				l1.setIcon(galleries[i]);
			}
		}
		if(e.getSource()==previous){
			j=j-1;
			if(j==0){
				j=j+1;
				JOptionPane.showMessageDialog(null, "沒有上一張了");
			}
			else{
				i=i-1;
				l1.setIcon(galleries[i]);
			}
		}
	}

	public static void main(String[] argv) {
		FE11 fe11 = new FE11();
	}

	public class MyAction extends AbstractAction {
		String name;

		MyAction(String name, Icon icon) {
			super(name, icon);
			putValue(Action.SHORT_DESCRIPTION, "Selet the" + name
					+ "flag to display");
			this.name = name;
		}

		public void actionPerformed(ActionEvent e) {
			if (name.equals("dog")) {
				j=1;
				i=3;
				l1.setIcon(galleries[i]);
			}
			if (name.equals("cat")) {
				j=1;
				i=0;
				l1.setIcon(galleries[i]);
				
			}
			if (name.equals("mouse")) {
				j=1;
				i=6;
				l1.setIcon(galleries[i]);
			}
		}
	}

}
