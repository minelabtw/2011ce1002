package fe.s100502512;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class FE11 extends JApplet {
	private JMenuBar M = new JMenuBar();
	private JMenu m = new JMenu();
	private JMenuItem cat = new JMenuItem("cat");
	private JMenuItem dog = new JMenuItem("dog");
	private JMenuItem mouse = new JMenuItem("mouse");
	private JToolBar t = new JToolBar(JToolBar.VERTICAL);
	private JButton Previous = new JButton("Previous");
	private JButton Next = new JButton("Next");
	private JPanel P1 = new JPanel();
	private JPanel P2 = new JPanel();
	private JPanel P3 = new JPanel();
	private JPanel P4 = new JPanel();
	private JLabel L = new JLabel();
	private ImageIcon cat1 = new ImageIcon("image/image/cat_icon");
	private ImageIcon dog1 = new ImageIcon("image/dog_icon");
	private ImageIcon mouse1 = new ImageIcon("image/mouse_icon");
	
	public FE11() {
		
		setJMenuBar(M);
		M.add(m);
		m.add(cat);
		m.add(dog);
		m.add(mouse);
		P1.setLayout(new BorderLayout());
		P1.add(L, BorderLayout.CENTER);
		P1.add(M, BorderLayout.EAST);
		P4.setLayout(new BorderLayout());
		P4.add(P1, BorderLayout.CENTER);
		P4.add(t, BorderLayout.EAST);
		P2.add(Previous);
		P2.add(Next);
		P3.setLayout(new BorderLayout());
		P3.add(P4, BorderLayout.CENTER);
		P3.add(P2, BorderLayout.SOUTH);
		add(P3);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == Previous) {

		}
		if (e.getSource() == Next) {

		}
		if (e.getSource() == cat) {
			
		}
		if (e.getSource() == dog) {

		}
		if (e.getSource() == mouse) {

		}
	}
}