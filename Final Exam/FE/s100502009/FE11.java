package fe.s100502009;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class FE11 extends JApplet {
	private JButton left=new JButton("<-");
	private JButton right=new JButton("->");
	private JPanel panel=new JPanel();
	private JPanel panel2=new JPanel(new GridLayout(1,2));
	private JLabel image;
	int animal;
	int number;

	ImageIcon[] pic=//store the pictures
	{
		new ImageIcon(getClass().getResource("image/cat_icon.jpg")),
		new ImageIcon(getClass().getResource("image/cat1.jpg")),
		new ImageIcon(getClass().getResource("image/cat2.jpg")),
		new ImageIcon(getClass().getResource("image/cat3.jpg")),
		new ImageIcon(getClass().getResource("image/dog_icon.jpg")),
		new ImageIcon(getClass().getResource("image/dog1.jpg")),
		new ImageIcon(getClass().getResource("image/dog2.jpg")),
		new ImageIcon(getClass().getResource("image/dog3.jpg")),
		new ImageIcon(getClass().getResource("image/mouse_icon.jpg")),
		new ImageIcon(getClass().getResource("image/mouse1.jpg")),
		new ImageIcon(getClass().getResource("image/mouse2.jpg")),
		new ImageIcon(getClass().getResource("image/mouse3.jpg")),
	};
	
	public FE11()
	{
		image=new JLabel(new ImageIcon(getClass().getResource("image/cat1.jpg")));
		Action cat=new MyAction(pic[0],"CAT");
		Action dog=new MyAction(pic[4],"DOG");
		Action mouse=new MyAction(pic[8],"MOUSE");
		
		JMenuBar bar=new JMenuBar();//create menu
		JMenu menu=new JMenu("Animals Gallery");
		setJMenuBar(bar);
		bar.add(menu);
		menu.add(cat);
		menu.add(dog);
		menu.add(mouse);
		
		JToolBar tbar=new JToolBar(JToolBar.VERTICAL);//create tool bar
		tbar.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		tbar.add(cat);
		tbar.add(dog);
		tbar.add(mouse);
		panel2.add(left);
		panel2.add(right);
		panel.setLayout(new BorderLayout());
		panel.add(image,BorderLayout.CENTER);
		add(panel,BorderLayout.CENTER);
		add(tbar,BorderLayout.EAST);
		add(panel2,BorderLayout.SOUTH);
		
		left.addActionListener(new ActionListener()//button left
		{
			public void actionPerformed(ActionEvent e)
			{				
				if(animal==1||animal==5||animal==9)		
				{
					JOptionPane.showMessageDialog(null, "This is the first picture");	
				}
				else
				{
					animal--;
					image.setIcon(pic[animal]);
				}
			}
		});
		right.addActionListener(new ActionListener()//button right
		{
			public void actionPerformed(ActionEvent e)
			{
				if(animal==3||animal==7||animal==11)
				{
					JOptionPane.showMessageDialog(null, "This is the last picture");				
				}
				else
				{
					animal++;
					image.setIcon(pic[animal]);	
				}
			}
		});
		
	}
	
	private class MyAction extends AbstractAction
	{
		String name;
		MyAction(ImageIcon image,String n)
		{
			super(n,image);
			this.name=n;
		}
		
		public void actionPerformed(ActionEvent e)
		{
			if(name.equals("CAT"))
			{
				image.setIcon(pic[1]);
				number=1;
				setname(number);
			}
			else if(name.equals("DOG"))
			{
				image.setIcon(pic[5]);
				number=5;
				setname(number);
			}
			else if(name.equals("MOUSE"))
			{
				image.setIcon(pic[9]);
				number=8;
				setname(number);
			}
			panel.revalidate();
		}
		
	}
	
	public void setname(int na)
	{
		animal=na;
	}
	
	

}
