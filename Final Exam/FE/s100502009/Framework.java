package fe.s100502009;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.text.*;

public class Framework extends JFrame implements ActionListener{
	private ResourceBundle res=ResourceBundle.getBundle("Confidential");
	private int x;
	private int y;
	DrawBuildings draw=new DrawBuildings();
	private JLabel l1=new JLabel(res.getString("Title"));
	private JLabel l2=new JLabel(res.getString("Language"));
	private JPanel l3=draw;
	private JLabel l4=new JLabel(res.getString("Position"));
	private JLabel l5=new JLabel(x+","+y);
	private JLabel l6=new JLabel(res.getString("Result"));
	private JLabel l7=new JLabel();
	private String[] country={"英國(英文)","法國(法文)","日本(日文)","中國(中文)"};
	private JComboBox jcblocale=new JComboBox(country);	
	private Locale[] locales={Locale.US,Locale.FRENCH,Locale.JAPAN,Locale.CHINA};
	private Locale locale=Locale.US;	
	private JPanel p1=new JPanel(new GridLayout(1,3));
	private JPanel p2=new JPanel(new GridLayout(1,4));

	public Framework()
	{		
		p1.add(l1);
		p1.add(l2);
		p1.add(jcblocale);
		
		p2.add(l4);
		p2.add(l5);
		p2.add(l6);
		p2.add(l7);
		
		add(p1,BorderLayout.NORTH);
		add(p2,BorderLayout.SOUTH);
		add(l3,BorderLayout.CENTER);
		
		jcblocale.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()==jcblocale)
		{
			this.locale=locales[jcblocale.getSelectedIndex()];
			updateString();
		}
	}
	
	
	
	public void updateString()//renew the string
	{
		res=ResourceBundle.getBundle("Confidential",locale);
		l1.setText(res.getString("Title"));
		l2.setText(res.getString("Language"));
		l4.setText(res.getString("Position"));
		l6.setText(res.getString("Result"));
	}
	

}
