package fp.s100502004;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import org.w3c.dom.stylesheets.StyleSheetList;

public class FE11 extends JApplet implements ActionListener{
	
	ImageIcon[] catimageIcon = new ImageIcon[3];
	ImageIcon[] dogimageIcon = new ImageIcon[3];
	ImageIcon[] mouseimageIcon = new ImageIcon[3];
	ImageIcon[] littlepicture = new ImageIcon[3];
	JButton previousButton = new JButton("previous");
	JButton nextButton = new JButton("next");
	JLabel show = new JLabel();
	JPanel buttonpPanel = new JPanel();
	int index = 0;
	int animaltype = 0;
	
	public FE11(){	
			littlepicture[0] = new ImageIcon(getClass().getResource("image/cat_icon.jpg"));
			littlepicture[1] = new ImageIcon(getClass().getResource("image/dog_icon.jpg"));
			littlepicture[2] = new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));
			catimageIcon[0] = new ImageIcon(getClass().getResource("image/cat1.jpg"));
			catimageIcon[1] = new ImageIcon(getClass().getResource("image/cat2.jpg"));
			catimageIcon[2] = new ImageIcon(getClass().getResource("image/cat3.jpg"));
			dogimageIcon[0] = new ImageIcon(getClass().getResource("image/dog1.jpg"));
			dogimageIcon[1] = new ImageIcon(getClass().getResource("image/dog2.jpg"));
			dogimageIcon[2] = new ImageIcon(getClass().getResource("image/dog3.jpg"));
			mouseimageIcon[0] = new ImageIcon(getClass().getResource("image/mouse1.jpg"));
			mouseimageIcon[1] = new ImageIcon(getClass().getResource("image/mouse2.jpg"));
			mouseimageIcon[2] = new ImageIcon(getClass().getResource("image/mouse3.jpg"));
			buttonpPanel.setLayout(new GridLayout(1,2));
			Action cat = new MyAction("Cat", littlepicture[0]);			
			Action dog = new MyAction("Dog", littlepicture[1]);			
			Action mouse = new MyAction("Mouse", littlepicture[2]);			
			buttonpPanel.add(previousButton);
			buttonpPanel.add(nextButton);
			previousButton.addActionListener(this);
			nextButton.addActionListener(this);
			
			
			JMenu kind = new JMenu("Animal");
			JMenuBar menuBar = new JMenuBar();
			kind.add(cat);
			kind.add(dog);
			kind.add(mouse);		
			menuBar.add(kind);
			
			JToolBar toolBar = new JToolBar(JToolBar.VERTICAL);
			toolBar.add(cat);
			toolBar.add(dog);
			toolBar.add(mouse);
			
			add(menuBar,BorderLayout.NORTH);
			add(toolBar,BorderLayout.EAST);
			add(show,BorderLayout.CENTER);
			add(buttonpPanel,BorderLayout.SOUTH);
	}
	
	

	
	
	
	
	public class MyAction extends AbstractAction{
			String name; 			
			MyAction(String animal, ImageIcon imageIcon) {
				// TODO Auto-generated constructor stub			
				super(animal,imageIcon);
				name = animal;				
			}						
			public void actionPerformed(ActionEvent e){
				if(name.equals("Cat")){
					show.setIcon(catimageIcon[index]);
					animaltype = 1;	
				}
				
				if(name.equals("Dog")){
					show.setIcon(dogimageIcon[index]);		
					animaltype = 2;				
				}		
				
				if(name.equals("Mouse")){
					show.setIcon(mouseimageIcon[index]);
					animaltype = 3;						
				}									
			}					
		}
	
		
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == nextButton){
								
				if(index<2){
					index ++;
					setImage(animaltype, index)	;
					
				}
				if(index>=2){
					index = 2;
					setImage(animaltype, index)	;
					JOptionPane.showMessageDialog(null,"沒有下一張囉");
				}
			}
			
			
			else if(e.getSource() == previousButton){
				if(index>0){
					index --;
					setImage(animaltype, index)	;	
				}
				if(index<=0){
					index = 0;
					setImage(animaltype, index)	;
					JOptionPane.showMessageDialog(null,"沒有前一張囉");
				}
			}
			
		}
		
		
		public void setImage(int animal , int number){
			if(animal == 1){
				show.setIcon(catimageIcon[number]);				
			}
			if(animal == 2){
				show.setIcon(dogimageIcon[number]);	
				
			}
			if(animal == 3){
				show.setIcon(mouseimageIcon[number]);
			}
			
		}
		
	
}
