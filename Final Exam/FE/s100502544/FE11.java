package fe.s100502544;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JToolBar;

public class FE11 extends JApplet implements ActionListener{
	
	ImageIcon[] imageIcons={new ImageIcon("image/cat_icon.jpg"),//0
	new ImageIcon("image/dog_icon.jpg"),//1
	new ImageIcon("image/mouse_icon.jpg"),//2
	new ImageIcon("image/cat1.jpg"),//3
	new ImageIcon("image/cat2.jpg"),//4
	new ImageIcon("image/cat3.jpg"),//5
	new ImageIcon("image/dog1.jpg"),//6
	new ImageIcon("image/dog2.jpg"),//7
	new ImageIcon("image/dog3.jpg"),//8
	new ImageIcon("image/mouse1.jpg"),//9
	new ImageIcon("image/mouse2.jpg"),//10
	new ImageIcon("image/mouse3.jpg")};//11
	
	Action CAT=new MyAction("CAT", imageIcons[0]);
	Action DOG=new MyAction("DOG", imageIcons[1]);
	Action MOUSE=new MyAction("MOUSE", imageIcons[2]);
	
	JLabel L=new JLabel();
	
	JButton B1=new JButton("上一張");
	JButton B2=new JButton("下一張");
	
	JPanel P1=new JPanel();
	
	public FE11(){	
		
		//排板
		P1.setLayout(new GridLayout(1,2));
		P1.add(B1);
		P1.add(B2);
		B1.addActionListener(this);
		B2.addActionListener(this);
		add(P1,BorderLayout.SOUTH);
		//JMenuBar宣告
		JMenuBar MB=new JMenuBar();
		JMenu menu=new JMenu("galleries");
		setJMenuBar(MB);
		MB.add(menu);
		menu.add(CAT);
		menu.add(DOG);
		menu.add(MOUSE);
		add(L,BorderLayout.CENTER);
		
		//JToolBar宣告
		JToolBar TB=new JToolBar(JToolBar.VERTICAL);
		TB.add(CAT);
		TB.add(DOG);
		TB.add(MOUSE);
		add(TB,BorderLayout.EAST);
		
	}
	//MyAction class
	class MyAction extends AbstractAction{
		String name;
		
		public MyAction(String name,Icon icon) {
			super(name,icon);//繼承上一個class的東西
			this.name=name;
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(name.equals("CAT")){
				
				L.setIcon(imageIcons[3]);
				
			}
			else if(name.equals("DOG")){
				L.setIcon(imageIcons[6]);
			}
			else if(name.equals("MOUSE")){
				L.setIcon(imageIcons[9]);
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==B2){
			L.setIcon(imageIcons[4]);
			
		}
	}
	
}





