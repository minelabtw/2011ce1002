package fe.s100502544;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Framework extends JFrame implements ActionListener{
	
	JComboBox languageBox=new JComboBox();
	ResourceBundle resourceBundle=ResourceBundle.getBundle("Confidential");
	JLabel L1=new JLabel(resourceBundle.getString("Title"));
	JLabel L2=new JLabel(resourceBundle.getString("Language"));
	JLabel L3=new JLabel(resourceBundle.getString("Position"));
	JLabel L4=new JLabel(resourceBundle.getString("Result"));
	JPanel P1=new JPanel();
	JPanel P2=new JPanel();
	
	
	private Locale locale=Locale.getDefault();
	Locale locales[]={Locale.CHINESE,Locale.ENGLISH,Locale.FRENCH,Locale.JAPANESE};
	public void languagelocales(){
		for(int i=0;i<locales.length;i++){
			languageBox.addItem(locales[i].getDisplayName());
		}
	}
	
	
	public Framework(){
		languagelocales();
		P1.setLayout(new GridLayout(1,3));
		P1.add(L1);
		P1.add(L2);
		P1.add(languageBox);
		P2.setLayout(new GridLayout(1,2));
		P2.add(L3);
		P2.add(L4);
		add(P1,BorderLayout.NORTH);
		add(P2,BorderLayout.SOUTH);
		languageBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				locale=locales[languageBox.getSelectedIndex()];
				updatestring();
			}
		});
		
	}
	private void updatestring() {
		// TODO Auto-generated method stub
		resourceBundle =ResourceBundle.getBundle("Confidential",locale);
		L1.setText(resourceBundle.getString("Title"));
		L2.setText(resourceBundle.getString("Language"));
		L3.setText(resourceBundle.getString("Position"));
		L4.setText(resourceBundle.getString("Result"));
		repaint();
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
}
