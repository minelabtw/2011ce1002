package EF1.s100502015;

import java.awt.*;

import javax.swing.*;

import java.awt.event.*;

public class FE11 extends JApplet implements ActionListener {
	int select = 0;
	int part = 0;
	private ImageIcon[] iconarray = { new ImageIcon("image/cat1.jpg"),
			new ImageIcon("image/cat2.jpg"), new ImageIcon("image/cat3.jpg"),
			new ImageIcon("image/dog1.jpg"), new ImageIcon("image/dog2.jpg"),
			new ImageIcon("image/dog3.jpg"), new ImageIcon("image/mouse1.jpg"),
			new ImageIcon("image/mouse2.jpg"),
			new ImageIcon("image/mouse3.jpg") };
	private JLabel picture = new JLabel();
	private JButton jCat = new JButton(new ImageIcon("image/cat_icon.jpg"));
	private JButton jDog = new JButton(new ImageIcon("image/dog_icon.jpg"));
	private JButton jMouse = new JButton(new ImageIcon("image/mouse_icon.jpg"));
	private JButton jNext = new JButton("->");
	private JButton jBack = new JButton("<-");
	private JLabel topic = new JLabel("Animals");
	private JPanel nbJPanel = new JPanel();
	private JToolBar t1 = new JToolBar();
	private JLabel error = new JLabel();

	public FE11() {
		setLayout(new BorderLayout());
		t1.setLayout(new GridLayout(3, 1));
		t1.add(jCat);
		t1.add(jDog);
		t1.add(jMouse);
		add(topic, BorderLayout.NORTH);
		add(t1, BorderLayout.EAST);
		add(picture);
		nbJPanel.setLayout(new BorderLayout());
		nbJPanel.add(jNext, BorderLayout.EAST);
		nbJPanel.add(error, BorderLayout.CENTER);
		nbJPanel.add(jBack, BorderLayout.WEST);
		add(nbJPanel, BorderLayout.SOUTH);
		jCat.addActionListener(this);
		jDog.addActionListener(this);
		jMouse.addActionListener(this);
		jNext.addActionListener(this);
		jBack.addActionListener(this);

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == jCat) {
			part = 0;
			select = 0;
			picture.setIcon(iconarray[select]);
		} else if (e.getSource() == jDog) {
			part = 1;
			select = 3;
			picture.setIcon(iconarray[select]);
		} else if (e.getSource() == jMouse) {
			part = 2;
			select = 6;
			picture.setIcon(iconarray[select]);
		} else if (e.getSource() == jNext) {
			select++;
			error.setText("");
			if (part == 0 && select >= 0 && select < 3) {
				picture.setIcon(iconarray[select]);

			} else if (part == 1 && select >= 3 && select < 6) {
				picture.setIcon(iconarray[select]);

			} else if (part == 2 && select >= 6 && select < 9) {

				picture.setIcon(iconarray[select]);

			} else {
				error.setText("ERROR");
				select--;
			}
		} else if (e.getSource() == jBack) {
			error.setText("");
			select--;
			if (part == 0 && select >= 0 && select < 3) {
				picture.setIcon(iconarray[select]);

			} else if (part == 1 && select >= 3 && select < 6) {

				picture.setIcon(iconarray[select]);

			} else if (part == 2 && select >= 6 && select < 9) {

				picture.setIcon(iconarray[select]);

			} else {
				error.setText("ERROR");
				select++;
			}
		}
	}

}
