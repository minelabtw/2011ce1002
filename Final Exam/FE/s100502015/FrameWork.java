package EF1.s100502015;

import java.awt.*;
import java.util.Locale;
import javax.swing.*;
import java.util.ResourceBundle;
import java.awt.event.*;
public class FrameWork extends JFrame {
	private JLabel title =new JLabel("BOMBER");
	private JLabel lan = new JLabel("�y��");
	private paintPanel pp1 = new paintPanel(); 
	private JLabel position = new JLabel("position");
	private JLabel currentposition = new JLabel();
	private JLabel Result = new JLabel("Result");
	private JLabel finalResult = new JLabel();
	private JComboBox jcbCountry = new JComboBox();
	private JPanel j1 = new JPanel();
	private JPanel j2 = new JPanel();
	private int delay = 1000;
	private Locale localelan = Locale.getDefault();
	private Locale[] localelans = { Locale.ENGLISH, Locale.JAPAN,
			Locale.CHINESE, Locale.FRANCE };		
	public FrameWork(){		
		setLayout(new BorderLayout());
		j1.setLayout(new GridLayout(1,3));
		j2.setLayout(new GridLayout(1,4));
		j1.add(title);
		j1.add(lan);
		j1.add(jcbCountry);
		j2.add(position);
		j2.add(currentposition);
		j2.add(Result);
		j2.add(finalResult);
		
		add(j1,BorderLayout.NORTH);
		add(pp1);
		add(j2,BorderLayout.SOUTH);
		Timer timer = new Timer(delay,new TimerListener());
		timer.start();
		for (int i = 0; i < localelans.length; i++) {
			jcbCountry.addItem(localelans[i].getDisplayName());
		}
		jcbCountry.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				localelan = localelans[jcbCountry.getSelectedIndex()];
				
			}
		});		
	}	
	class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			repaint();
		}
	}	
	
}