package fe.s100502027;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class DrawBuildings extends JPanel implements ActionListener{
	Timer timer = new Timer(200,this);
	int timex = 0 ;
	int clickX = 0;
	int clickY = 0;
	int xBase = timex;
	int yBase = getHeight()/2;
	public DrawBuildings(){
		
		timer.start();
	}
	
	public void setMousePositionX(int x_temp){  //get the X 
		clickX = x_temp ;
	}
	
	public void setMousePositionY(int y_temp){ // get the Y
		clickY = y_temp;
	}
	
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		xBase = timex;
		yBase = getHeight()/2;
		int AX[] = {xBase+10,xBase+10,xBase+200,xBase+200};
		int AY[] = {yBase+200,yBase,yBase,yBase+200};
		int BX[] = {xBase+60,xBase+60,xBase+250,xBase+250};
		int BY[] = {yBase+100,yBase-50,yBase-50,yBase+100};
		g.setColor(Color.RED);
		g.drawPolygon(AX, AY, 4);  // draw first rectangle
		g.drawString("A", xBase, yBase+200);  // set 'A' 
		g.setColor(Color.BLUE);
		g.drawPolygon(BX, BY, 4);// draw second rectangle
		g.drawString("B", xBase+50, yBase+100);// set 'B' 
		g.setColor(Color.GREEN);
		g.fillOval(clickX-5,clickY-5,10,10); // use the get clickX.Y to show the seat
		
	}
	public String bombposition(){  //  count the result of click 
		if(clickX>xBase+60&&clickX<xBase+200&&clickY>yBase&&clickY<yBase+100){
			return "Hit Both buildings A and B";
		}
		else if(clickX>xBase+10&&clickX<xBase+200&&clickY>yBase&&clickY<yBase+200){
			return"Hit Building A";
		}
		else if(clickX>xBase+60&&clickX<xBase+250&&clickY>yBase-50&&clickY<yBase+100){
			return"Hit Building B";
		}
		else{
			return "Miss";
		}
		
	}
	public void actionPerformed(ActionEvent e) {   // let the graphic move   and renew the paint
		if(timex<=getWidth()){
			timex++;
		}
		else{
			timex=0;
		}
		repaint();
	}
}
