package fe.s100502027;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Framework extends JFrame{
	private ResourceBundle res = ResourceBundle.getBundle("Confidential");
	private JLabel JLTitle = new JLabel(res.getString("Title"));
	private JLabel JLLanguage = new JLabel(res.getString("Language"));
	private JLabel JLPosition = new JLabel(res.getString("Position"));
	private JLabel JLcurrent = new JLabel("");
	private JLabel JLResult = new JLabel(res.getString("Result"));
	private JLabel JLshow = new JLabel("");
	private String[] LanChoose = {"英文","法文","日文","中文"};
	private Locale[] LanLocale = {Locale.ENGLISH,Locale.FRANCE,Locale.JAPANESE,Locale.CHINESE};
	private JComboBox JCBLan = new JComboBox(LanChoose);
	private Locale nowLocale = Locale.ENGLISH;
	public Framework(){
		setLayout(new BorderLayout());
		
		JPanel JPlangu = new JPanel();
		JPlangu.setLayout(new GridLayout(1,2));
		JPlangu.add(JLLanguage);
		JPlangu.add(JCBLan);
		
		JPanel JPtop = new JPanel();
		JPtop.setLayout(new GridLayout(1,2));
		JPtop.add(JLTitle);
		JPtop.add(JPlangu);
		
		JPanel JPunder = new JPanel();
		JPunder.setLayout(new GridLayout(1,4));
		JPunder.add(JLPosition);
		JPunder.add(JLcurrent);
		JPunder.add(JLResult);
		JPunder.add(JLshow);
		
		final DrawBuildings DrawPanel = new DrawBuildings();
		DrawPanel.setBackground(Color.WHITE);
		DrawPanel.addMouseListener(new MouseListener(){  //  when click the point 

			@Override
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				int nowX = e.getX();
				int nowY = e.getY();
				JLcurrent.setText(nowX+" , "+nowY);  // show the current X and Y
				DrawPanel.setMousePositionX(nowX);   // and send the x and y to count
				DrawPanel.setMousePositionY(nowY);
				JLshow.setText(DrawPanel.bombposition());  // get the count result to show 
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		JCBLan.addActionListener(new ActionListener(){   // when change the language,set the new locale and renew label
			public void actionPerformed(ActionEvent e) {
				nowLocale = LanLocale[JCBLan.getSelectedIndex()] ;
				upString();
			}
		});
		
		add(JPtop,BorderLayout.NORTH);
		add(DrawPanel,BorderLayout.CENTER);
		add(JPunder,BorderLayout.SOUTH);
		
	}
	public void upString(){  // the method to renew the lable message with new locale
		res = ResourceBundle.getBundle("Confidential",nowLocale);
		JLTitle.setText(res.getString("Title"));
		JLLanguage.setText(res.getString("Language"));
		JLPosition.setText(res.getString("Position"));
		JLResult.setText(res.getString("Result"));
	}
}
