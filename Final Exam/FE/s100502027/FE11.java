package fe.s100502027;

import java.awt.BorderLayout;
import java.awt.Desktop.Action;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToolBar;

public class FE11 extends JApplet implements ActionListener{
	private ImageIcon[] catgalleries = new ImageIcon[3];
	private ImageIcon[] doggalleries = new ImageIcon[3];
	private ImageIcon[] mousegalleries = new ImageIcon[3];
	private ImageIcon catpic = new ImageIcon(getClass().getResource("image/cat_icon.jpg"));
	private ImageIcon dogpic = new ImageIcon(getClass().getResource("image/dog_icon.jpg"));
	private ImageIcon mousepic = new ImageIcon(getClass().getResource("image/mouse_icon.jpg"));
	private JButton JBprevious = new JButton("Previous");
	private JButton JBnext = new JButton("Next");
	private JToolBar JTBPIC = new JToolBar(JToolBar.VERTICAL);
	private JMenuBar JMBPIC = new JMenuBar();
	private JMenu JMPIC = new JMenu("Gallery");
	private JLabel JLSHOWPIC = new JLabel(" ");
	private int pickind = 1;
	private int piccount = 1;
	public FE11(){
		
		setLayout(new BorderLayout());
		MyAction cat = new MyAction("cat",catpic);
		MyAction dog = new MyAction("dog",dogpic);
		MyAction mouse = new MyAction("mouse",mousepic);
		//To set pic to the imageicon
		catgalleries[0] = new ImageIcon(getClass().getResource("image/cat1.jpg"));
		catgalleries[1] = new ImageIcon(getClass().getResource("image/cat2.jpg"));
		catgalleries[2] = new ImageIcon(getClass().getResource("image/cat3.jpg"));
		doggalleries[0] = new ImageIcon(getClass().getResource("image/dog1.jpg"));
		doggalleries[1] = new ImageIcon(getClass().getResource("image/dog2.jpg"));
		doggalleries[2] = new ImageIcon(getClass().getResource("image/dog3.jpg"));
		mousegalleries[0] = new ImageIcon(getClass().getResource("image/mouse1.jpg"));
		mousegalleries[1] = new ImageIcon(getClass().getResource("image/mouse2.jpg"));
		mousegalleries[2] = new ImageIcon(getClass().getResource("image/mouse3.jpg"));
		
		setJMenuBar(JMBPIC);
		JMBPIC.add(JMPIC);
		JMPIC.add(cat);
		JMPIC.add(dog);
		JMPIC.add(mouse);
		
		JTBPIC.add(cat);
		JTBPIC.add(dog);
		JTBPIC.add(mouse);
		
		JPanel JPJButton = new JPanel();
		JPJButton.setLayout(new GridLayout(1,6));
		JPJButton.add(new JLabel(""));
		JPJButton.add(new JLabel(""));
		JPJButton.add(JBprevious);
		JPJButton.add(JBnext);
		JPJButton.add(new JLabel(""));
		JPJButton.add(new JLabel(""));
		
		JBprevious.addActionListener(this);
		JBnext.addActionListener(this);
		
		JLSHOWPIC.setIcon(catgalleries[0]);
		
		add(JTBPIC,BorderLayout.EAST);
		add(JLSHOWPIC,BorderLayout.CENTER);
		add(JPJButton,BorderLayout.SOUTH);
		
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==JBprevious){
			piccount -= 1;
			if(piccount<1){//if this is 1 ,stop at 1 
				piccount=1;
				JOptionPane.showMessageDialog(null, "This is the first!");
			}
			else{
				switch(pickind){ //change the gallerypic with pickind to choose cat or dog or mouse
					case 1:
						JLSHOWPIC.setIcon(catgalleries[piccount-1]);
						break;
					
					case 2:
						JLSHOWPIC.setIcon(doggalleries[piccount-1]);
						break;
					
					case 3:
						JLSHOWPIC.setIcon(mousegalleries[piccount-1]);
						break;
			}
			}
		}
		else if(e.getSource()==JBnext){
			piccount += 1;
			if(piccount>3){//if this is 3 ,stop at 3 
				piccount=3;
				JOptionPane.showMessageDialog(null, "This is the last!");
			}
			else{
				switch(pickind){//change the gallerypic with pickind to choose cat or dog or mouse
					case 1:
						JLSHOWPIC.setIcon(catgalleries[piccount-1]);
						break;
						
					case 2:
						JLSHOWPIC.setIcon(doggalleries[piccount-1]);
						break;
						
					case 3:
						JLSHOWPIC.setIcon(mousegalleries[piccount-1]);
						break;
				}
			}
			
		}
	}
	
	private class MyAction extends AbstractAction{
		String name;   //set the name to use and icon to show
		public MyAction(String name ,Icon icon){
			super(name,icon);
			this.name=name;
		}
		public void actionPerformed(ActionEvent e) {
			if(name.equals("cat")){ // cat: set kind=1 and change to the catGallery's 1
				pickind=1;
				piccount = 1;
				JLSHOWPIC.setIcon(catgalleries[0]);
			}
			else if(name.equals("dog")){// dog: set kind=2 and change to the dogGallery's 1
				pickind=2;
				piccount = 1;
				JLSHOWPIC.setIcon(doggalleries[0]);
			}
			else if(name.equals("mouse")){// mouse: set kind=3 and change to the mouseGallery's 1
				pickind=3;
				piccount = 1;
				JLSHOWPIC.setIcon(mousegalleries[0]);
			}
		}
	}
}
