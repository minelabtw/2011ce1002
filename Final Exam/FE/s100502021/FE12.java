package fe.s100502021;

import javax.swing.*;


public class FE12 extends JFrame{
	public static void main(String [] args){
		Framework f = new Framework();
		f.setTitle("Final Exam");
		f.setSize(600, 600);//set the size of the frame
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
