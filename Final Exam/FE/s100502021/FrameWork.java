package fe.s100502021;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Time;

import javax.management.timer.Timer;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener{
	Timer time=new Timer(1000,new TimerListener());
	DrawBuildings d=new DrawBuildings();
	public FrameWork() {
		add(d);
	}
	private class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			d.move();
			repaint();
		}
	}
}
