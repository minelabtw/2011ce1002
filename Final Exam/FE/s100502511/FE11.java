package fe.s100502511;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FE11 extends JApplet {
	private JMenuBar menu = new JMenuBar(); // 創立MenuBar
	private JMenu file = new JMenu("File");
	private JMenuItem exit = new JMenuItem("Exit", 'E');
	private JToolBar toolbar = new JToolBar(); // 創立ToolBar
	private JLabel view = new JLabel();
	private JPanel BUTTON = new JPanel(new GridLayout(1, 2));
	private JButton button1 = new JButton("<<<");
	private JButton button2 = new JButton(">>>");

	private int judge = 0; // 判斷用

	private int i = 0;
	private int j = 3;
	private int k = 6;

	private ImageIcon[] image = {
			new ImageIcon(getClass().getResource("image/cat1.jpg")),
			new ImageIcon(getClass().getResource("image/cat2.jpg")),
			new ImageIcon(getClass().getResource("image/cat3.jpg")),
			new ImageIcon(getClass().getResource("image/dog1.jpg")),
			new ImageIcon(getClass().getResource("image/dog2.jpg")),
			new ImageIcon(getClass().getResource("image/dog3.jpg")),
			new ImageIcon(getClass().getResource("image/mouse1.jpg")),
			new ImageIcon(getClass().getResource("image/mouse2.jpg")),
			new ImageIcon(getClass().getResource("image/mouse3.jpg")) };

	private ImageIcon[] icon = {
			new ImageIcon(getClass().getResource("image/cat_icon.jpg")),
			new ImageIcon(getClass().getResource("image/dog_icon.jpg")),
			new ImageIcon(getClass().getResource("image/mouse_icon.jpg")) };

	public FE11() {

		BUTTON.add(button1);
		BUTTON.add(button2);

		JMenuItem cat, dog, mouse;
		JButton cat1 = new JButton();
		JButton dog1 = new JButton();
		JButton mouse1 = new JButton();

		cat1.setIcon(icon[0]);
		dog1.setIcon(icon[1]);
		mouse1.setIcon(icon[2]);

		view.setIcon(image[0]);
		toolbar.add(cat1);
		toolbar.add(dog1);
		toolbar.add(mouse1);

		file.add(cat = new JMenuItem(icon[0]));
		file.add(dog = new JMenuItem(icon[1]));
		file.add(mouse = new JMenuItem(icon[2]));
		file.add(exit);
		menu.add(file);

		add(view);
		add(toolbar, BorderLayout.NORTH);
		setJMenuBar(menu);
		toolbar.setFloatable(true);
		add(BUTTON, BorderLayout.SOUTH);

		cat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				view.setIcon(image[0]);
				judge = 0;
				i = 0;
			}
		});
		dog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				view.setIcon(image[3]);
				judge = 1;
				k = 3;
			}
		});
		mouse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				view.setIcon(image[6]);
				judge = 2;
				j = 6;
			}
		});
		cat1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				view.setIcon(image[0]);
				judge = 0;
				i = 0;
			}
		});
		dog1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				view.setIcon(image[3]);
				judge = 1;
				j = 3;
			}
		});
		mouse1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				view.setIcon(image[6]);
				judge = 2;
				k = 6;
			}
		});
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (judge == 0) {
					if (i >= 0 && i <= 2) {
						i--;
						if (i < 0) {
							JOptionPane.showMessageDialog(null,
									"This is the first page!!");
							i = 0;
							view.setIcon(image[0]);
						}
						view.setIcon(image[i]);
					}
				}
				if (judge == 1) {
					if (j >= 3 && j <= 5) {
						j--;
						if (j < 3) {
							JOptionPane.showMessageDialog(null,
									"This is the first page!!");
							j = 3;
							view.setIcon(image[3]);
						}
						view.setIcon(image[j]);
					}
				}

				if (judge == 2) {
					if (k >= 6 && k <= 8) {
						k--;
						if (k < 6) {
							JOptionPane.showMessageDialog(null,
									"This is the first page!!");
							k = 6;
							view.setIcon(image[6]);
						}
						view.setIcon(image[k]);
					}
				}
			}
		});
		button2.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				if (judge == 0) {
					i++;
					view.setIcon(image[i]);
					if (i > 2) {
						view.setIcon(image[2]);
						i = 2;
						JOptionPane.showMessageDialog(null,
								"This is the last page!!");
					}
				}
				if (judge == 1) {
					j++;
					view.setIcon(image[j]);
					if (j > 5) {
						view.setIcon(image[5]);
						j = 5;
						JOptionPane.showMessageDialog(null,
								"This is the last page!!");
					}
				}

				if (judge == 2) {

					k++;
					if (k <= 8) {
						view.setIcon(image[k]);
					}
					if (k > 8) {
						view.setIcon(image[8]);
						k = 8;
						JOptionPane.showMessageDialog(null,
								"This is the last page!!");
					}
				}
			}
		});
	}
}
