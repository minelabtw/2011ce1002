package fe.s100502511;

import java.awt.*;
import java.awt.event.*;
import java.util.ResourceBundle;
import javax.annotation.Resource;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class Framework extends JFrame implements ActionListener {
	private JPanel UP = new JPanel(new GridLayout(1, 3));
	private JPanel CENTER = new JPanel(new GridLayout(1, 3));
	private JPanel DOWN = new JPanel(new GridLayout(1, 3));
	private JPanel screen = new JPanel(new GridLayout(3, 1));
	private Border lineBorder = new LineBorder(Color.BLACK, 2);
	private JComboBox combobox = new JComboBox(new Object[] { "France",
			"Japan", "Taiwan", "English" });
	private JLabel L1 = new JLabel("The Bomber Infrared Screen");
	private JLabel L2 = new JLabel("Language:");
	private JLabel L3 = new JLabel();
	private JLabel L4 = new JLabel("Position:");
	private JLabel L5 = new JLabel();
	private JLabel L6 = new JLabel("Result:");
	private JLabel L7 = new JLabel("");

	public Framework() {
		UP.add(L1);
		UP.add(L2);
		UP.add(combobox);
		CENTER.add(L3);
		DOWN.add(L4);
		DOWN.add(L5);
		DOWN.add(L6);
		DOWN.add(L7);
		UP.setBorder(lineBorder);
		CENTER.setBorder(lineBorder);
		DOWN.setBorder(lineBorder);
		screen.add(UP);
		screen.add(CENTER);
		screen.add(DOWN);

		add(screen);
	}

	public void actionPerformed(ActionEvent e) {
		/*
		 * if(e.getSource()=){
		 * 
		 * } }
		 */

	}
}
