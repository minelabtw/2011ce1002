package fe.s100502020;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class FE11 extends JApplet implements ActionListener{
	private JPanel p1 = new JPanel();
	private JPanel p2 = new JPanel();
	private JPanel p3 = new JPanel();
	private JLabel title = new JLabel("Animals Gallery");
	private JButton Previous = new JButton("Previous");
	private JButton Next = new JButton("Next");
	private ImageIcon [] picture = new ImageIcon[12];
	private JToolBar toolbar = new JToolBar();
	private JMenu menu = new JMenu();
	
	public FE11()
	{
		this.setLayout(new BorderLayout());		
		p1.add(title);
		p3.setLayout(new GridLayout(1,2,1,1));
		p3.add(Previous);
		p3.add(Next);
		picture[0] = new ImageIcon(getClass().getResource("image/cat_icon.jpg"));
		picture[1] = new ImageIcon(getClass().getResource("image/cat1.jpg")); 
		picture[2] = new ImageIcon(getClass().getResource("image/cat2.jpg")); 
		picture[3] = new ImageIcon(getClass().getResource("image/cat3.jpg")); 
		picture[4] = new ImageIcon(getClass().getResource("image/dog_icon.jpg")); 
		picture[5] = new ImageIcon(getClass().getResource("image/dog1.jpg")); 
		picture[6] = new ImageIcon(getClass().getResource("image/dog2.jpg")); 
		picture[7] = new ImageIcon(getClass().getResource("image/dog3.jpg")); 
		picture[8] = new ImageIcon(getClass().getResource("image/mouse_icon.jpg")); 
		picture[9] = new ImageIcon(getClass().getResource("image/mouse1.jpg")); 
		picture[10] = new ImageIcon(getClass().getResource("image/mouse2.jpg"));
		picture[11] = new ImageIcon(getClass().getResource("image/mouse3.jpg"));
		add(p1, BorderLayout.NORTH);
		add(p2, BorderLayout.CENTER);
		add(p3, BorderLayout.SOUTH);
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	

}
