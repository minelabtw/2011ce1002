package fe.s100502020;

import java.awt.*;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener, MouseListener,
		MouseMotionListener {
	private JPanel p1 = new JPanel();
	private JPanel p2 = new JPanel();
	private JPanel p3 = new JPanel();
	private JLabel JLabel_1 = new JLabel("The Bomber Infrared Screen");
	private JLabel JLabel_2 = new JLabel("Language:");
	private JLabel JLabel_3 = new JLabel();
	private JLabel JLabel_4 = new JLabel("Position:");
	private JLabel JLabel_5 = new JLabel();
	private JLabel JLabel_6 = new JLabel("Result");
	private JLabel JLabel_7 = new JLabel();
	private JComboBox combobox = new JComboBox();

	public FrameWork() {
		this.setLayout(new BorderLayout());
		p1.setLayout(new GridLayout(1, 3, 1, 1));
		p1.add(JLabel_1);
		p1.add(JLabel_2);
		p1.add(combobox);

		p2.add(JLabel_3);
		p3.setLayout(new GridLayout(1, 4, 1, 1));
		p3.add(JLabel_4);
		p3.add(JLabel_5);
		p3.add(JLabel_6);
		p3.add(JLabel_7);
		this.add(p1, BorderLayout.NORTH);
		this.add(p2, BorderLayout.CENTER);
		this.add(p3, BorderLayout.SOUTH);
		addMouseListener(this);
		addMouseMotionListener(this);
	}
	public void paintComponent(Graphics g)
	{
		super.paintComponents(g);
		
	}
	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}

}
