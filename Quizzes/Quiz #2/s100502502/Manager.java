//Manager.java
package q2.s100502502;
public class Manager extends Employee{
	protected String title;
	Manager(String Name, int Age, String Gender, int Salary, String Title) throws ageException, salaryException{
		setInfo(Name, Age, Gender, Salary, Title);
	}
	public String toStrong(){//show message
		return  super.toString() + "This guy is a Manager!";
	}
	public void setInfo(String N, int A, String G, int S, String T) throws ageException, salaryException{
		//save user input and throw message
		name = N;
		age = A;
		gender = G;
		salary = S;
		title = T;
		if(A <= 0)
			throw new ageException();
		if(S<0)
			throw new salaryException();
	}
	public void getInfo(){//show message
		System.out.println("This guy is"+name+", "+age+" years old, "+gender);
		toString();
		System.out.println("Title is "+title+", Salary is "+salary);
	}
}
