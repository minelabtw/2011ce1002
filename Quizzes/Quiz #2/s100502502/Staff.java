package q2.s100502502;
public class Staff extends Employee{
	protected String phoneNumber;
	Staff(String Name, int Age, String Gender, int Salary, String Phone) throws ageException, salaryException, phoneException{
		setInfo(Name, Age, Gender, Salary, Phone);
	}
	public String toString(){//show message
		return super.toString()+"This guy is a Staff!";
	}
	public void setInfo(String N, int A, String G, int S, String P) throws ageException, salaryException, phoneException {
		//save user input and throw message
		name = N;
		age = A;
		gender = G;
		salary = S;
		phoneNumber = P;
		if(age<=0)
			throw new ageException();
		if(salary<0)
			throw new salaryException();
		if(phoneNumber.length()<10)
			throw new phoneException();
	}
	public void getInfo(){
		//show message
		System.out.println("This guy is"+name+", "+age+" years old, "+gender);
		toString();
		System.out.println("Phone number is "+phoneNumber+", Salary is "+salary);
	}
}
