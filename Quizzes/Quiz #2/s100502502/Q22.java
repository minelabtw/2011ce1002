//Q22.java
package q2.s100502502;
import java.util.Scanner;
public class Q22 {
	public static void main(String[] args){
		boolean ON = true;//control while loop
		Scanner input = new Scanner(System.in);
		while(ON){
			System.out.println("Please choose the employee tou want to create: 1.Manage 2.Staff 3.Exit");
			int choose = input.nextInt();//save user choice
			switch(choose){
				case 1:
					System.out.println("Please input name, age, gender, salary, title");
					//save user input
					String Name = input.next();
					int Age = input.nextInt();
					String Gender = input.next();
					int Salary = input.nextInt();
					String Title = input.next();
					try{
						Manager User = new Manager(Name, Age, Gender, Salary, Title);//create object
						User.getInfo();
					}
					catch(ageException  e){
						System.out.println("Initial Failed!! Please try again!!\n");
					}
					catch(salaryException e){
						System.out.println("Initial Failed!! Please try again!!\n");
					}
					break;
				case 2:
					System.out.println("Please input name, age, gender, salary, phone number");
					//save user input
					String Name2 = input.next();
					int Age2 = input.nextInt();
					String Gender2 = input.next();
					int Salary2 = input.nextInt();
					String Phone = input.next();
					try{
						Staff User2 = new Staff(Name2, Age2, Gender2, Salary2, Phone);//create object
						User2.getInfo();
					}
					catch(ageException  e){
						System.out.println("Initial Failed!! Please try again!!\n");
					}
					catch(salaryException e){
						System.out.println("Initial Failed!! Please try again!!\n");
					}
					catch(phoneException e){
						System.out.println("Initial Failed!! Please try again!!\n");
					}
					break;
				case 3://Exit
					ON = false;
					System.out.println("BYE~");
					break;
			}
		}
	}
}
