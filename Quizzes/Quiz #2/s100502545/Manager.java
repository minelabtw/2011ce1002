package q2.s100502545;

public class Manager extends Employee 
{
	public String title;
	
	
	public Manager(String A,int B,String C,int D,String E)
	{
		setInfo(A,B,C,D,E);
		toString();
		getInfo();
	}
	
	public void setInfo(String A,int B,String C,int D,String E )
	{
		name = A;
		age = B;
		gender = C;
		salary = D;
		title = E;
	}
	

	
	public String toString()
	{
		return " This guy is a Manager!!";
		
	} 
	
	public String toString(boolean check)
	{
		if (check == true)
		{
			return this.toString();
		}
		else
			return "Though this guy is not a Manager,"+super.toString();
	}
	
	public void getInfo()
	{
		System.out.println(name);
		System.out.println(age);
		System.out.println(gender);
		System.out.println(salary);
		System.out.println(title);
		
	}
	
}


