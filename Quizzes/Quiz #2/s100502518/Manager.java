package q2.s100502518;

public class Manager extends Employee{
	private String title;
	
	public Manager(String n,int a,String g,int s,String t)
	{
		setInfo(n, a, g, s, t);
	}
	public String toString()
	{
		return "This guy is a Manager!!"+super.toString();
	}
	
	public void setInfo(String n,int a,String g,int s,String t)
	{
		name=n;
		gender=g;
		title=t;
		age=a;
		salary=s;
	}
	
	public void getInfo()
	{
		try
		{
			checkage(age);
			checksalary(salary);
			System.out.println("This guy is "+name+","+age+" years old, "+gender);
			System.out.println(toString());
			System.out.println("Title is "+title+", Salary is "+salary);
		}
		
		catch (ageException ex) 
		{
			System.out.println(ex.getMessage());
			System.out.println("Initial Faild!! Please try again!!");
		}
		catch (salaryException ex) 
		{
			System.out.println(ex.getMessage());
			System.out.println("Initial Faild!! Please try again!!");
		}
	}
}
