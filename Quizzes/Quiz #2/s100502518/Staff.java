package q2.s100502518;

public class Staff extends Employee{
	private String phonenumber;
	
	public Staff(String n,int a,String g,int s,String p)
	{
		setInfo(n, a, g, s, p);
	}
	public String toString()
	{
		return "This guy is a Staff!!"+super.toString();
	}
	
	public void setInfo(String n,int a,String g,int s,String p)
	{
		name=n;
		gender=g;
		age=a;
		phonenumber=p;
		salary=s;
	
	}
	
	public void getInfo()
	{
		try
		{
			checkage(age);
			checksalary(salary);
			checkphone(phonenumber);
			System.out.println("This guy is "+name+","+age+" years old, "+gender);
			System.out.println(toString());
			System.out.println("Phonenumber is "+phonenumber+", Salary is "+salary);
		}
		
		catch (ageException ex) 
		{
			System.out.println(ex.getMessage());
			System.out.println("Initial Faild!! Please try again!!");
		}
		catch (salaryException ex) 
		{
			System.out.println(ex.getMessage());
			System.out.println("Initial Faild!! Please try again!!");
		}
		catch (phoneException ex) 
		{
			System.out.println(ex.getMessage());
			System.out.println("Initial Faild!! Please try again!!");
		}
		
	}
}
