package q2.s100502518;

public class Person {
	protected String name;
	protected int age;
	protected String gender;
	
	public String toString() 
	{
		return "This guy is a person!!";
	}
	
	public void checkage(int a) throws ageException
	{
		if(a<=0)
		{
			throw new ageException("Age can't be less than or equal zero!!");
		}
	}
	
	public void checksalary(int a) throws salaryException
	{
		if(a<0)
		{
			throw new salaryException("Salary can't be less than zero!!");
		}
	}
	
	public void checkphone(String a) throws phoneException
	{
		
		if(a.length()!=10)
		{
			throw new phoneException("Phone number can't be less than 10 digits!!");
		}
	}

}
