package q2.s100502028;

public class Manager extends Employee{
	private String title;
	
	public Manager(String nameOfInput,int ageOfInput, String genderOfInput, int salaryOfInput, String titleOfInput){
		try{
			setInfo(nameOfInput, ageOfInput, genderOfInput, salaryOfInput, titleOfInput);
			System.out.println(getInfo());
		}
		catch(ageException ex){
			System.out.println(ex.getResult());
		}
		catch(salaryException ex){
			System.out.println(ex.getResult());
		}
	}
	
	public void setInfo(String n, int a, String g, int s, String t) throws ageException, salaryException{
		name = n;
		age = a;
		gender = g;
		salary = s;
		title = t;
		String wrongResult1 = new String("The age " + age + " is less than or equal 0!!");
		String wrongResult2 = new String("The salary " + salary + " is less than or equal 0!!");
		if(age <= 0){
			throw new ageException(wrongResult1);
		}
		else if(salary <= 0){
			throw new salaryException(wrongResult2);
		}
	}
	
	public String getInfo(){
		return "This guy is " + getName() + ", " + getAge() + " years old, " + getGender() + "\n" + toString() +
		"Title is " + getTitle() + ", Salary is " + getSalary();
	}
	
	// Get method to return the title
	public String getTitle(){
		return title;
	}
	
	// A override method to show a message to say object with this class represents a employee
	public String toString(){ 
		return super.toString() + "This guy is a Manager!!\n";
	}
}
