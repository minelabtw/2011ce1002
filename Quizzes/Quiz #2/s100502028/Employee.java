package q2.s100502028;

public class Employee extends Person {
	protected int salary;
	
	// Get method to return the salary
	public int getSalary(){
		return salary;
	}
	
	// A override method to show a message to say object with this class represents a employee
	public String toString(){ 
		return "This guy is an Employee!!";
	}
}
