package q2.s100502028;

public class Staff extends Employee{
	private String phoneNumber;
	
	public Staff(String nameOfInput,int ageOfInput, String genderOfInput, int salaryOfInput, String phoneNumberOfInput){
		try{
			setInfo(nameOfInput, ageOfInput, genderOfInput, salaryOfInput, phoneNumberOfInput);
			System.out.println(getInfo());
		}
		catch(ageException ex){
			System.out.println(ex.getResult());
			System.out.println("Initial Faild!! Please try again!!");
		}
		catch(salaryException ex){
			System.out.println(ex.getResult());
			System.out.println("Initial Faild!! Please try again!!");
		}
		catch(phoneException ex){
			System.out.println(ex.getResult());
			System.out.println("Initial Faild!! Please try again!!");
		}
	} 
	
	public void setInfo(String n, int a, String g, int s, String p) throws ageException, salaryException, phoneException{
		name = n;
		age = a;
		gender = g;
		salary = s;
		phoneNumber = p;
		String wrongResult1 = new String("The age " + age + " is less than or equal 0!!");
		String wrongResult2 = new String("The salary " + salary + " is less than or equal 0!!");
		String wrongResult3 = new String("Phone number can't be less than 10 digits!!");
		if(age <= 0){
			throw new ageException(wrongResult1);
		}
		else if(salary <= 0){
			throw new salaryException(wrongResult2);
		}
		else if(phoneNumber.length() < 10){
			throw new phoneException(wrongResult3);
		}
	}
	
	public String getInfo(){
		return "This guy is " + getName() + ", " + getAge() + " years old, " + getGender() + "\n" + toString() +
		"Phone number is " + getPhoneNumber() + ", Salary is " + getSalary();
	}
	
	public String getPhoneNumber(){
		return phoneNumber;
	}
	
	public String toString(){ 
		return super.toString() + "This guy is a Staff!!";
	}
}
