package q2.s100502028;

public class ageException extends Exception{
	private String wrongAge;
	
	// Construct an exception
	public ageException(String error) {
		wrongAge = error;
	}
	
	// Return the exception
	public String getResult() {
		return wrongAge;
	}
}
