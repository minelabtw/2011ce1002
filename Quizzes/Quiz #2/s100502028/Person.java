package q2.s100502028;

public class Person {
	protected String name, gender;
	protected int age;
	
	// Constructor
	public Person(){
		
	}
	
	// Get method to return the name
	public String getName(){
		return name;
	}
	
	// Get method to return the gender
	public String getGender(){
		return gender;
	}
	
	// Get method to return the age
	public int getAge(){
		return age;
	}
	
	// A method to show message to say object with this class represents a person
	public String toString(){
		return "This guy is a Person!!";
	}
}
