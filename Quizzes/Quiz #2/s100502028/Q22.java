package q2.s100502028;
import java.util.*;

public class Q22 {
	public static void main(String[] agrs){
		Scanner input = new Scanner(System.in);
	
		boolean exitFlag = false;
		
		while(exitFlag == false){
			System.out.println("Please choose the employee you want to create 1.Manager 2.Staff 3.Exit ");
			int userChoice = input.nextInt();
			System.out.println("");
			
			// user choice for creating object
			switch(userChoice){
				case 1:
					// new a Employee object, and initial it
					System.out.println("Please input name, age, gender, salary, title");
					Manager teacher = new Manager(input.next(), input.nextInt(), input.next(), input.nextInt(), input.next());		
					System.out.println("");
					break;
								
				case 2:
					// new a Student object, and initial it
					System.out.println("Please input name, age, gender, salary, phone number");
					Staff staff = new Staff(input.next(), input.nextInt(), input.next(), input.nextInt(), input.next());
					System.out.println("");
					break;
								
				case 3:
					exitFlag = true;
					System.out.println("See you next time.");
					break;
								
				default:
					break;
			} // End switch statement
		}
	}
}
