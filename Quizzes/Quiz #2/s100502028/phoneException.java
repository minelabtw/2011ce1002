package q2.s100502028;

public class phoneException extends Exception{
	private String wrongPhone;
	
	// Construct an exception
	public phoneException(String error) {
		wrongPhone = error;
	}
	
	// Return the exception
	public String getResult() {
		return wrongPhone;
	}
}
