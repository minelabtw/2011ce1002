package q2.s100502028;

public class salaryException extends Exception{
	private String wrongSalary;
	
	// Construct an exception
	public salaryException(String error) {
		wrongSalary = error;
	}
	
	// Return the exception
	public String getResult() {
		return wrongSalary;
	}
}
