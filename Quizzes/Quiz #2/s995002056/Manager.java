package q2.s995002056;

public class Manager extends Employee{
	String	title;
	
	Manager(String n,int a,String g,int s,String t){
		name = n;
		age = a;
		gender = g;
		salary = s;
		title = t;
		try{
			set();
		}
		catch(Exceptions ex){
			System.err.println(ex);
		}
	}
	
	public String toString(){
		String str = super.toString();
		return str + "!!This guy is a Manager";
	}
	
	public String getN(){
		return name;
	}
	
	public int getA(){
		return age;
	}
	
	public String getG(){
		return gender;
	}
	
	public int getS(){
		return salary;
	}
	
	public String getT(){
		return title;
	}
	
	public void set() throws Exceptions{
		if(age <=0 )
			throw new Exceptions("age is error!");
		if(salary < 0)
			throw new Exceptions("salary is error!");
	}
}
