package q2.s995002056;
import java.util.Scanner;
import javax.swing.*;

public class Q21 {
	public static void main(String[] argv){
		Scanner input = new Scanner(System.in);
		
		System.out.print("please input a lenth:");
		int len = input.nextInt();
				
		FrameWork f = new FrameWork(len);
		f.setTitle("Q21");
		f.setSize(500,500);
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
