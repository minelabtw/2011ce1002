package q2.s995002056;
import java.util.Scanner;

public class Q22 {
	public static void main(String[] argv){
		String	name;
		int	age;
		String gender;
		int salary;
		String title;
		String phoneNumber;
		boolean exit = true;
		
		Scanner input = new Scanner(System.in);

		while(exit){
			
			System.out.println("Please choose the employee you want to create:1.Manager 2.Staff 3.Exit");
			int ch = input.nextInt();
			
			switch(ch){
				case 1:
					System.out.println("Please input name,age,gender,salary,title");
					name = input.next();
					age	= input.nextInt();
					gender = input.next();
					salary = input.nextInt();
					title = input.next();
					Manager ma = new Manager(name,age,gender,salary,title);
					
					System.out.println("this guy is"+ma.getN()+","+ma.getA()+","+ma.getG());
					System.out.println(ma.toString());
					System.out.println("Title is"+ma.getT()+",salary is"+ma.getS());
					break;
					
				case 2:
					System.out.println("Please input name,age,gender,salary,phone number");
					name = input.next();
					age	= input.nextInt();
					gender = input.next();
					salary = input.nextInt();
					phoneNumber = input.next();
					Staff st = new Staff(name,age,gender,salary,phoneNumber);
					
					System.out.println("this guy is"+st.getN()+","+st.getA()+","+st.getG());
					System.out.println(st.toString());
					System.out.println("Title is"+st.getP()+",salary is"+st.getS());
					break;
					
				case 3:
					System.out.println("Bye!");
					exit = false;
					break;
			}
		}
	}

}
