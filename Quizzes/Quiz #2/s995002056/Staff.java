package q2.s995002056;

public class Staff extends Employee{
	String phoneNumber;
	
	Staff(String n,int a,String g,int s,String pn){
		name = n;
		age = a;
		gender = g;
		salary = s;
		phoneNumber = pn;
		
		try{
			set();
		}
		catch(Exceptions ex){
			System.err.println(ex);
		}
	}
	
	public String toString(){
		String str = super.toString();
		return str + "!!This guy is a Staff";
	}
	
	public String getN(){
		return name;
	}
	
	public int getA(){
		return age;
	}
	
	public String getG(){
		return gender;
	}
	
	public int getS(){
		return salary;
	}
	
	public String getP(){
		return phoneNumber;
	}
	public void set() throws Exceptions{
		if(age <=0 )
			throw new Exceptions("age is error!");
		if(salary < 0)
			throw new Exceptions("salary is error!");
		if(phoneNumber.length() < 10)
			throw new Exceptions("phoneNumber is error!");
			
	}
}
