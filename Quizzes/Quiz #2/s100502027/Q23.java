package q2.s100502027;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class Q23 {
	public static void main(String [] args) throws IOException {
		
		File sourcefile = new File("Input.txt");   //set the file 
		Scanner input = new Scanner(sourcefile);   
		while(true){
			String dataString = input.next() ;  //get the string 
			char[] letters = dataString.toCharArray();  //to catch every in the string
			System.out.println("The input string :" + dataString);
			
			Stack q2 = new Stack();
			for (int t = 0 ; t<dataString.length();t++){
				int number = letters[t]-'0' ;
				int temp1 =0,temp2=0 ;
				q2.showALL();
				
				if(number >= 0){  //if it is number   push the stack 
					q2.push(number);
				}
				else if(letters[t]=='+'){  // to catch the letter to count  
					temp1 = q2.pop();  //get the last one
					temp2 = q2.pop() ;  //get the last two 
					q2.push(temp1+temp2);  // push the result to stack
				}
				else if(letters[t]=='-'){
					temp1 = q2.pop();
					temp2 = q2.pop() ;
					q2.push(temp1-temp2);
				}
				else if(letters[t]=='*'){
					temp1 = q2.pop();
					temp2 = q2.pop() ;
					q2.push(temp1*temp2);
				}
				else if(letters[t]=='/'){
					temp1 = q2.pop();
					temp2 = q2.pop() ;
					q2.push(temp1/temp2);
				}
				else{
				}
			}
			q2.showALL();
			System.out.println("Here is the result : "+q2.pop());
			System.out.println("");
		}
	}
}
