package q2.s100502027;

public class Stack {
	private int[] element=new int[5];
	private int index_stack=0;
	
	void push(int value){    //push the now number
		element[index_stack] = value;
		index_stack++;
	}
	
	int pop(){   // pop the last number
		int stackcount=index_stack-1;
		index_stack--;
		if(isEmpty()){
			System.out.println("Stack is Empty!");
		}
		else if(isFull()){
			System.out.println("Stack is Full!");
		}
		return element[stackcount];
		
	}
	
	Boolean isFull(){   /// make sure the stack is full?
		if(index_stack==5){
			return true;
		}
		else{
			return false;
		}
	}
	
	Boolean isEmpty(){   /// make sure the stack is empty?
		if(index_stack==0){
			return true;
		}
		else{
			return false;
		}
	}
	
	void showALL(){   //output the every number in the stack
		System.out.print("[ ");
		for (int t=0;t<index_stack;t++){
			System.out.print(element[t]+" ");
		}
		System.out.println(" ]");
	}
}
