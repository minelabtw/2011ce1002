package q2.s100502033;

public class Employee extends Person
{
	protected static int salary;
	public Employee(String name1 , int age1 , String gender1 , int salary1)
	{
		super(name1 , age1 , gender1);
		salary = salary1;
	}
	public String toString()
	{
		return "this guy is an Employee";
	}
}
