package s100502034;

public class Employee extends Person{
	int salary;
	public Employee(String n, int ag, String gen, int s) {
		super(n, ag, gen);
		salary = s;
	}
	
	public String toString(){
		return "This guy is an Employee";
	}
}