package s100502034;

public class Manager extends Employee{
	String title;
	public Manager(String n, int ag, String gen, int s, String t) {
		super(n, ag, gen, s);
		title = t;
	}
	
	public String toString(){
		return super.toString() + "This guy is a Manager";
	}
	public void setInfo(int m,int age,String t){
		title = t;
		ageException.check(age);
		salaryException.check(m);
	}
	public void getInfo(){
		System.out.println("This guy is " + name);
		System.out.println(", " + age +"years old, " + gender);
		super.toString();
	}
}
