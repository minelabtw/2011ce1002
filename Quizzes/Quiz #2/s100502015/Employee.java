package q2.s100502015;

public class Employee extends Person { // inherit person
	private int salary;

	public Employee(String emname, String emgender, int emage, int salary) {// constructor
		super(emname, emgender, emage);
		this.salary = salary;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public String toString() {
		return "this guy is a employee!!";
	}

	public String toString(boolean check) {// result
		if (check) {
			return "this guy is a employee!!";
		} else {
			return "this guy is not a staff " + super.toString();
		}

	}
}
