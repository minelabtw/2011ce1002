package q2.s100502015;
import java.util.Scanner;
public class Q22 {
public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);

		boolean exitFlag = false;

		while (exitFlag == false) {
			System.out
					.println("Please the person you want to create 1.teacher 2.student 3.Exit: ");
			int userChoice = input.nextInt();

			// user choice for creating object
			switch (userChoice) {
			case 1:
				// new a Employee object, and initial it
				System.out
						.println("Please input the Manager's name, gender, age,salary");				
				Employee teacher = new Employee(input.next(), input.next(),
				input.nextInt(), input.nextInt());

				// use methods to answer the questions
				System.out
						.println("The Manager's name is " + teacher.getName());
				System.out.println("The Manager's gender is "
						+ teacher.getGender());
				System.out.println("The Manager's salary is "
						+ teacher.getSalary());				
				System.out.println("The category of this guy is: "
						+ teacher.toString());
				System.out.println("Is this guy a Employee? "
						+ teacher.toString(true));
				System.out.println("Is this guy a staff? "
						+ teacher.toString(false));

				break;

			case 2:
				// new a Student object, and initial it
				System.out
						.println("Please input the student's name, gender, and classroom number");
				Staff student = new Staff(input.next(), input.next(),
						input.next());

				// use methods to answer the questions
				System.out.println("The student name is " + student.getName());
				System.out.println("The student's gender is "
						+ student.getGender());				
				System.out.println("The category of this guy is: "
						+ student.toString());
				System.out.println("Is this guy a staff? "
						+ student.toString(true));
				System.out.println("Is this guy a Employee? "
						+ student.toString(false));

				break;

			case 3:
				exitFlag = true;
				System.out.println("See you next week~~");
				break;

			default:
				break;
			}
		}
	}
}
