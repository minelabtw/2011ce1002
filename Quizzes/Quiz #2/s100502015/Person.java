package q2.s100502015;

public class Person {
	private String name = "";
	private String gender = "";
	private int age = 0;
	public Person() {
		this("tony", "male",1000);
	}

	public Person(String name, String gender,int age) {
		this.name = name;
		this.gender = gender;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public String getGender() {
		return gender;
	}
	public int age()
	{
		return age;
	}

	public void setName(String input) {
		name = input;
	}

	public void setGender(String input) {
		gender = input;
	}
	public void setage(int input)
	{
		age = input;
	}
	public String toString(){//result
		return "this guy is a person";
	}

}
