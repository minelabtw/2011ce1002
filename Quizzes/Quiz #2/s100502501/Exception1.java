package q2.s100502501;

public class Exception1 extends Exception{
	public Exception1(int cases){
		switch(cases){
		case 1: 
			System.err.println("Age can't be less than or equal 0!!!");
			break;
		case -2:			
			System.err.println("Salary can't be less than 0!!!");
			break;
		case -3: 
			System.err.println("PhoneNumber can't be less than 10 digits!!!");
			break;
		default:
			break;
	}
	}
}
