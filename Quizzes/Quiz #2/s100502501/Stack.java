package q2.s100502501;

public class Stack {
	private int[] element;
	private int size;
	private int index_stack;
	public Stack(){
		element=new int[5];
	}
	public void push(int value){
		if(!isFull()){
			int tp[]=new int[5];
			System.arraycopy(element, 0, tp, 0, 5);
			element=tp;
			element[size++]=value;
		}
		else
			System.out.print("Full!");
	}
	public void pop(int a){
		if(!isEmpty()){
			if(size>=a)
				size-=a;
			else{
				size=0;
				System.out.print("Empty!");
			}
		}
		else
			System.out.print("Empty!");
	}
	public boolean isFull(){
		if(size==5)
			return false;
		else
			return true;
	}
	public boolean isEmpty(){
		if(size==0)
			return false;
		else
			return true;
	}
	public void showAll(){
		for(int i=0;i<size;i++){
			System.out.print(element[i]+"\t");
		}
	}
}
