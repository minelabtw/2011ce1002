package q2.s100502501;
public class Staff extends Employee {
	protected String phoneNumber;
	
	public Staff(String name1,int age1,String gender1,int money,String fone) throws Exception1{
		setInfo(name1,age1,gender1,money,fone);
		
	}
	public String toString(){
		return super.toString()+"This guy is a Staff!!";
	}
	
	public void setInfo(String name1,int age1,String gender1,int money,String fone) throws Exception1{
		name=name1;
		if(age1<=0)
			throw new Exception1(1);
		else
			age=age1;
		gender=gender1;
		
		if(money<0)
			throw new Exception1(2);
		else
			salary=money;
		if(fone.length()<10)
			throw new Exception1(3);
		else
			phoneNumber=fone;
	}
	
	public void getInfo(){
		System.out.println("This guy is "+name+","+age+" yrs old,"+gender+"\n"+this.toString()+"Salary is "+salary+"\nPhoneNumber is "+phoneNumber);
	}
}
