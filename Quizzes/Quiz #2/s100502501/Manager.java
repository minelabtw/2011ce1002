package q2.s100502501;
import java.util.Scanner;
public class Manager extends Employee{
	protected String title;
	
	public Manager(String name1,int age1,String gender1,int money,String title1) throws Exception1{
		setInfo(name1,age1,gender1,money,title1);	
	}
	
	public String toString(){
		return super.toString()+" This guy is a Manager!!";
	}
	
	public void setInfo(String name1,int age1,String gender1,int money,String title1) throws Exception1{
		name=name1;
		if(age1<=0)
			throw new Exception1(1);
		else
			age=age1;
		gender=gender1;
		
		if(money<0)
			throw new Exception1(2);
		else
			salary=money;
		
		title=title1;
	}
	
	public void getInfo(){
		System.out.println("This guy is "+name+","+age+" yrs old,"+gender+"\n"+this.toString()+"\nTitle is "+title+"Salary is "+salary);
	}
}
