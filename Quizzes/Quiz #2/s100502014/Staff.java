package q2.s100502014;

public class Staff extends Employee {
	private String phoneNumber;
	public Staff(String name1, int age1, String gender1, int salary1, String phoneNumber1) {
		super(name1, age1, gender1, salary1);
		phoneNumber = phoneNumber1;
	}
	public String toString() {
		return "This guy is a staff, and " + super.toString();
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
}
