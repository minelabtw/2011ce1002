package q2.s100502014;

public class Manager extends Employee {
	private String title;
	public Manager(String name1, int age1, String gender1, int salary1, String title1) {
		super(name1, age1, gender1, salary1);
		title = title1;
	}
	public String toString() {
		return "This guy is a manager, and " + super.toString();
	}
	public String getTitle() {
		return title;
	}
}
