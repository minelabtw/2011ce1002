package q2.s100502014;

public class Employee extends Person {
	private int salary;
	public Employee(String name1, int age1, String gender1, int salary1) {
		super(name1, age1, gender1);
		salary = salary1;
	}
	public String toString() {
		return "This guy is an employee";
	}
	public int getSalary() {
		return salary;
	}
}
