package q2.s100502014;
import java.awt.*;
import javax.swing.*;
public class FrameWork extends JFrame {
	private Color black_button = Color.BLACK, white_button = Color.WHITE;
	public FrameWork(int len) {
		setLayout(new GridLayout(len,len));
		for(int i=0;i<(len-1)/2;i++) {
			for(int j=0;j<(len-2*i+1)/2;j++) {
				drawButton(white_button);
			}
		}
		for(int i=0;i<len;i++) {
			drawButton(black_button);
		}
		for(int i=0;i<(len-1)/2;i++) {
			for(int j=0;j<(len-2*i+1)/2;j++) {
				drawButton(white_button);
			}
		}
	}
	public void drawButton(Color C) {
		JButton jbtn = new JButton();
		jbtn.setBackground(C);
		add(jbtn);
	}
}
