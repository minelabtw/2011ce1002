package q2.s100502014;

import java.util.Scanner;

import javax.swing.JFrame;

public class Q21 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		FrameWork frameWork = new FrameWork(input.nextInt());
		frameWork.setSize(500,500);
		frameWork.setVisible(true);
		frameWork.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
