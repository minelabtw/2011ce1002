package q2.s100502014;
import java.util.Scanner;

import javax.management.monitor.StringMonitor;
public class Q22 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		boolean flag = true;
		while(flag) {
			System.out.println("Please choose the employee you want to create:1.Manager 2.Staff 3.Exit");
			switch(input.nextInt()) {
				//Manager
				case 1:
					System.out.println("Please input name, age, gender, salary, title");
					int age;
					int salary;
					try {
						Manager m = new Manager(input.next(), age=input.nextInt(), input.next(), salary=input.nextInt(), input.next());
						if(age<=0) {
							throw new AgeException();
						}
						if(salary<0) {
							throw new SalaryException();
						}
						System.out.println("This guy is "+m.getName()+", "+m.getAge()+", "+m.getGender());
						System.out.println(m.toString());
						System.out.println("Title is "+m.getTitle()+", Salary is "+m.getSalary());
					}
					catch(AgeException e) {
						System.err.println("Initial Failed!! Please try again!!!");
					}
					catch(SalaryException e) {
						System.err.println("Initial Failed!! Please try again!!!");
					}
					break;
				//Staff
				case 2:
					System.out.println("Please input name, age, gender, salary, phone number");
					String phoneNumber;
					try {
						Staff s = new Staff(input.next(), age=input.nextInt(), input.next(), salary=input.nextInt(), phoneNumber=input.next());
						if(age<=0) {
							throw new AgeException();
						}
						if(salary<0) {
							throw new SalaryException();
						}
						if(phoneNumber.length()<10) {
							throw new PhoneException();
						}
						System.out.println("This guy is "+s.getName()+", "+s.getAge()+", "+s.getGender());
						System.out.println(s.toString());
						System.out.println("Phone number is "+s.getPhoneNumber()+", Salary is "+s.getSalary());
					}
					catch(AgeException e) {
						System.err.println("Initial Failed!! Please try again!!!");
					}
					catch(SalaryException e) {
						System.err.println("Initial Failed!! Please try again!!!");
					}
					catch(PhoneException e) {
						System.err.println("Initial Failed!! Please try again!!!");
					}
					break;
				//Exit
				case 3:
					flag = false;
					break;
				default:
					System.err.println("Option error!!");
					break;
					
			}
		}
	}
}
