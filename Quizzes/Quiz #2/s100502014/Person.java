package q2.s100502014;

public class Person {
	private String name;
	private int age;
	private String gender;
	public Person(String name1, int age1, String gender1) {
		name = name1;
		age = age1;
		gender = gender1;
	}
	public String toString() {
		return "This guy is a person";
	}
	public String getName() {
		return name;
	}
	public int getAge() {
		return age;
	}
	public String getGender() {
		return gender;
	}
}
