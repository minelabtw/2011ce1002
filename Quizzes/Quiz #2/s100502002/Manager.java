package q2.s100502002;

public class Manager extends Employee
{
	protected String title;
	public Manager()
	{
		
	}
	public Manager(String name,int age,String gender,int salary,String title)
	{
		super();
		setInfo(name,age,gender,salary,title);
	}
	public void setInfo (String name,int age,String gender,int salary,String title) throws salaryException 
	{
		if(salary<0)
		{
			throw new salaryException("something wrong!!");
		}
		super.setage(age);
		super.setgender(gender);
		super.setname(name);
		super.setsalary(salary);
		settitle(title);
		
	}
	public void settitle(String t)
	{
		title = t;
	}
	public String gettitle()
	{
		return title;
	}
	public String toString()
	{
		return "this guy is a Manager" + super.toString();
	}
	public void getInfo()
	{
		System.out.print("this guy is"+super.name);
		System.out.print(","+super.age+"years old,");
		System.out.print(super.gender);
		super.toString();
		toString();
		System.out.print("title is"+title);
		System.out.print("salary is"+salary);
		
	}
	

}
