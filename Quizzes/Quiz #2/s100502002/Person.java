package q2.s100502002;

public class Person {
	protected String name;
	protected String gender;
	protected int age;
	public Person()
	{
		
	}
	public void setname(String Name)
	{
		name=Name;
	}
	public void setgender(String Gender)
	{
		gender = Gender;
	}
	public void setage(int Age)
	{
		age = Age;
	}
	public String getname()
	{
		return name;
	}
	public String getgender()
	{
		return gender;
	}
	public int getage()
	{
		return age;
	}
	public String toString()
	{
		return "this guy is a person!!";
	}

}
