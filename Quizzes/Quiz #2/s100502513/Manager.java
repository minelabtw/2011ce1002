package q2.s100502513;

public class Manager extends Employee {
	private String title;

	public Manager() {

	}

	public Manager(String name, int age, String gender, int salary, String title)
			throws ageException, salaryException {
		setInfo(name, age, gender, salary, title);
	}

	public String toString() {
		return super.toString() + "This guy is a Manager.";
	}

	public void setInfo(String name, int age, String gender, int salary,
			String title) throws ageException, salaryException {
		this.name = name;
		this.gender = gender;
		this.title = title;
		if (age <= 0)
			throw new ageException("Age could not less than or equal 0");
		else
			this.age = age;
		if (salary <= 0)
			throw new salaryException("Salary could not less than 0");
		else
			this.salary = salary;
	}

	public void getInfo() {
		System.out.println("This guy is " + name + ", " + age + " years old, "
				+ gender);
		System.out.println(toString());
		System.out.println("Title is " + title + ", Salary is " + salary);
	}

}
