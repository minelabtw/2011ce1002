package q2.s100502513;

public class Staff extends Employee{
	private String phoneNumber;

	public Staff() {

	}

	public Staff(String name, int age, String gender, int salary, String phoneNumber)throws ageException, phoneException ,salaryException{
		setInfo(name, age, gender, salary, phoneNumber);
	}

	public String toString() {
		return "This guy is a Staff." + super.toString();
	}
	public void setInfo(String name, int age, String gender, int salary,
			String phoneNumber) throws ageException, phoneException, salaryException {
		this.name = name;
		this.gender = gender;
		this.phoneNumber = phoneNumber;
		if (age <= 0)
			throw new ageException("Age could not less than or equal 0");
		else
			this.age = age;
		if (salary <= 0)
			throw new salaryException("Salary could not less than 0");
		else
			this.salary = salary;
		if (phoneNumber.length() < 10)
			throw new phoneException("The digit could not less than 10");
		else
			this.phoneNumber = phoneNumber;
	}

	public void getInfo() {
		System.out.println("This guy is " + name + ", " + age + " years old, "
				+ gender);
		System.out.println(toString());
		System.out.println("PhoneNumber is " + phoneNumber + ", Salary is " + salary);
	}
}
