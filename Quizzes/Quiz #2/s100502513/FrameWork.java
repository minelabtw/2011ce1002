package q2.s100502513;

import java.awt.*;
import javax.swing.*;

public class FrameWork extends JFrame {
	private Color black_button = new Color(0, 0, 0);
	private Color white_button = new Color(255, 255, 255);

	FrameWork(int l) {
		setLayout(new GridLayout(l, l));
		for (int i = 0; i < l * l; i++) {
			add(drawButton(white_button));
		}
	}

	public JButton drawButton(Color c) {
		JButton bu = new JButton();
		bu.setBackground(c);
		return bu;
	}
}
