package q2.s100502009;

public class Person {
	protected String name;
	protected int age;
	protected String gender;
	
	public Person(String n,int a,String g)
	{
		setinf(n,a,g);
	}
	
	public Person()
	{
	}

	public void setinf(String n,int a,String g)
	{
		name=n;
		age=a;
		gender=g;
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getAge()
	{
		return age;
	}
	
	public String getGender()
	{
		return gender;
	}
	
	public String toString()
	{
		return "this guy is a person";
	}
}
