package q2.s100502009;
import javax.swing.*;
import java.awt.*;

public class FrameWork extends JFrame{
	Color black_button=Color.BLACK,white_button=Color.WHITE;	
	JButton jbt=new JButton();
	public FrameWork(int number)
	{
		setLayout(new GridLayout(number,number));
		for(int i=0;i<=(number+1)/2;i++)
		{
			for(int j=(number+1)/2-(i+1);j>0;j--)
			{				
				drawButton(white_button);
				add(jbt);				
			}
			for(int k=((i+1)*2)-1;k>0;k--)
			{
				drawButton(black_button);
				add(jbt);
			}
			for(int m=(number+1)/2-i;m>0;m--)
			{
				drawButton(white_button);
				add(jbt);
			}
		}
		for(int i=1;i<=(number-1)/2;i++)
		{
			for(int j=i;j>0;j--)
			{
				drawButton(white_button);
				add(jbt);	
			}
			for(int k=number-2*i;k>0;k--)
			{
				drawButton(black_button);
				add(jbt);
			}
			for(int m=i;m>0;m--)
			{
				drawButton(white_button);
				add(jbt);
			}
		}
	}		
	public void drawButton(Color c)
	{
		jbt.setBackground(c);
	}
}
