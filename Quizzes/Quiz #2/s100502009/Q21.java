package q2.s100502009;
import javax.swing.JOptionPane;


public class Q21{	
	public static void main(String[] args)
	{
		String length=JOptionPane.showInputDialog("Please input the lenth: ");
		int number=Integer.parseInt(length);
		FrameWork frame = new FrameWork(number);
		frame.setTitle("Quiz 2-1");
		frame.setSize(200,200);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(FrameWork.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

}
