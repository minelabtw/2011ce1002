package q2.s100502009;

public class Manager extends Employee{
	protected String title;
	
	public Manager(String t)
	{
		setInfo(t);
	}
	
	public void setInfo(String t)
	{
		title=t;
	}
	
	public String getInfo()
	{
		return title;
	}
	
	public String toString()
	{
		return "this guy is a manager,"+super.toString();
	}

}
