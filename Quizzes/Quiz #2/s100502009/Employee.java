package q2.s100502009;

public class Employee extends Person{
	protected int salary;
	
	public Employee()
	{		
	}
	
	public Employee(int s)
	{
		setSalary(s);
	}
	
	public void setSalary(int s)
	{
		salary=s;
	}
	
	public int getSalary()
	{
		return salary;
	}
	
	public String toString()
	{
		return "this guy is an employee";
	}
	
	

}
