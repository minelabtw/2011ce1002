package q2.s100502515;

@SuppressWarnings("serial")
public class PhoneException extends Exception{
	PhoneException(String phoneNumber) {
		System.out.println("The digits of " + phoneNumber + " is less than ten!");
	}
}
