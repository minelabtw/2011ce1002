package q2.s100502515;

public class Manager extends Employee {
	Manager(String inputName, int inputAge, String inputGender, int inputSalary) {
		super(inputName, inputAge, inputGender, inputSalary);
		
	}

	private String title;
	private static String name;
	private static int age;
	private static String gender;
	{
		try {
			setInfo(gender, age, name, age, title);
		} catch (AgeException | SalaryException e) {
			
			e.printStackTrace();
		}

	}

	@Override
	public String toString() {
		return "This guy is a manager, not a dog!" + Employee.class.toString();

	}

	public void setInfo(String inputName, int inputAge, String inputGender,
			int inputSalary, String inputTitle) throws AgeException,
			SalaryException {
		name = inputName;
		gender = inputGender;
		title = inputTitle;

		if (inputAge <= 0)
			throw new AgeException();
		else
			age = inputAge;

		if (inputSalary < 0)
			throw new SalaryException();
		else {
		}

	}

}
