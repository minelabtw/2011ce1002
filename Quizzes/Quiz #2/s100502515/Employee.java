package q2.s100502515;

public class Employee extends Person{
	protected int salary;
	
	Employee(String inputName, int inputAge, String inputGender, int inputSalary) {
		super(inputName, inputAge, inputGender);
		salary = inputSalary;
		
	}
	
	@Override
	public String toString() {
		return "This guy is an employee, not a dog!";
	}

	
	
	
}
