package q2.s100502515;

@SuppressWarnings("serial")
public class AgeException extends Exception {
	AgeException() {
		System.out.println("A person should be at least 1 year old!");
	}
}
