package q2.s100502515;

public class Person {
	protected String name;
	protected int age;
	protected static String gender;
	
	Person(String inputName, int inputAge, String inputGender) {
		name = inputName;
		age = inputAge;
		gender = inputGender;
	}

	public String toString() {
		return "This guy is a person, not a dog!";
	}
}
