package q2.s100502515;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Q21 {
	public static void main (String[] args) {
		String inputLength = JOptionPane.showInputDialog("Enter the length:");
		int length = Integer.parseInt(inputLength);
		
		
		Framework framework = new Framework(length);
		framework.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		framework.setSize(800, 800);
		framework.setVisible(true);
	}
	
} 