package q2.s100502020;

public class Manager extends Employee{
	private String title;
	
	public Manager(String N,int A,String G,int S,String T) throws Exception
	{
		setInfo(N,A,G,S,T);
	}
	public String toString()
	{
		return "This guy is a Manager and " + super.toString();
	}
	public void setInfo(String N,int A,String G,int S,String T) throws ageException
	{
		name = N;
		age = A;
		gender = G;
		salary = S;
		title = T;
	}
	public void getInfo()
	{
		System.out.print("This guy is "+ name + ", "+age+" years old, "+gender+"\n");
		toString();
		System.out.print("Salary is "+salary+", title is "+ title);
	}
	
}
