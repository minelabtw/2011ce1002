package q2.s100502020;

public class Staff extends Employee{
	private String phonenumber;
	
	public Staff(String N,int A,String G,int S,String P) throws Exception
	{
		setInfo(N,A,G,S,P);
	}
	public String toString()
	{
		return "This guy is a staff and " + super.toString();
	}
	public void setInfo(String N,int A,String G,int S,String P) throws ageException,phoneException
	{
		
		name = N;
		age = A;
		gender = G;
		salary = S;
		phonenumber = P;
		
	}
	public void getInfo()
	{
		System.out.print("This guy is "+ name + ", "+age+" years old, "+gender+"\n");
		toString();
		System.out.print("Salary is "+salary+", phone number is "+phonenumber);
	}
}
