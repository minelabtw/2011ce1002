package q2.s100502007;

public class Manager extends Employee{
	String title;
	Manager(String Name, int Age, String Gender, int Salary, String Title) {
		super(Name, Age, Gender, Salary, Title);
		title=Title;
	}
	public String getTitle(){
		return title;
	}
	public String toString(){
		return "This guy is a Manager";
	}
	public String toString(boolean check){
		if(check==true){
			return toString();
		}
		else{
			return " Though this guy is not a Employee,"+super.toString();
		}
	}
}

