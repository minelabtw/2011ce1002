package q2.s100502007;

public class Staff extends Employee{
	String phonenumber;
	Staff(String Name, int Age, String Gender, int Salary, String Title) {
		super(Name, Age, Gender, Salary, Title);
		phonenumber=Title;
	}
	public String getPhonenumber(){
		return  phonenumber;
	}
	public String toString(boolean check){
		if(check==true){
			return toString();
		}
		else{
			return " Though this guy is not a Employee,"+super.toString();
		}
	}

}
