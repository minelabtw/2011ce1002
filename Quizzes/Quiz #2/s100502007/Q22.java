package q2.s100502007;
import java.util.Scanner;

public class Q22 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);

		boolean exitFlag = false;

		while(exitFlag == false){
			System.out.println("Please choose the employee you want to create 1.Manager 2.Staff 3.Exit ");
			int userChoice = input.nextInt();
			// user choice for creating object
			switch(userChoice){
				case 1:
					System.out.println("Please input name, age, gender, salary, title");
					Manager manager = new Manager( input.next(), input.nextInt(), input.next(), input.nextInt(), input.next() );
					// new a Manager object, and initial it
					if(manager.getAge()<=0){//age exception
						System.out.println("age can not less than or equal 0");
					}
					if(manager.getSalary()<0){//salary exception
						System.out.println("salary can not less than 0");
					}
					else{
						System.out.println("The guy is " + manager.getName()+", "+manager.getAge()+"years old, "+manager.getGender());
						System.out.println(manager.toString(true)+manager.toString());
						System.out.println("Title is "+ manager.getTitle()+", Salary is "+manager.getSalary());
					}
					break;

				case 2:
					// new a Student object, and initial it
					System.out.println("Please input name, age, gender, salary, phone number");
					Staff staff= new Staff( input.next(), input.nextInt(), input.next(), input.nextInt(), input.next() );
					// use methods to answer the questions
					int[]array=new int [10];
					int count=0;
					for(int i=0;i<10;i++){
						array[i]=staff.getPhonenumber().charAt(i);
						count++;
					}
					if(staff.getAge()<=0){//age exception
						System.out.println("age can not less than or equal 0");
					}
					if(staff.getSalary()<0){//salary exception
						System.out.println("salary can not less than 0");
					}
					if(count<10){//phone exception
						System.out.println("Phone number can't be less than 10 digits!!");
					}
					else{
						System.out.println("The guy is " + staff.getName()+", "+staff.getAge()+"years old, "+staff.getGender());
						System.out.println(staff.toString(true)+staff.toString());
						System.out.println("Phone number is "+ staff.getPhonenumber()+", Salary is "+staff.getSalary());
						}
					break;

				case 3://exit
					exitFlag = true;
					System.out.println("See you next week~~");
					break;

				default:
					break;
			}
		}
	}
}
