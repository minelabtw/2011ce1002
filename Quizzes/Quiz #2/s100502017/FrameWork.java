package q2.s100502017;
import java.awt.Color;
import java.awt.*;
import java.lang.Math;
import javax.swing.*;
public class FrameWork extends JFrame {
	public JButton black_button =new JButton("");
	public JButton white_button =new JButton("");
	public FrameWork(){//constructor
		String s=JOptionPane.showInputDialog("please input length");//let user inpput length
		int length=Integer.valueOf(s);//string to int
		setLayout(new GridLayout(length,length));
		for(int i=0;i<length;i++){//use for loop to determined white or black
			int num1=Math.abs((length-1)/2-i);
			int num2=length-2*(num1);
			for(int j=0;j<num1;j++){
				drawButton(new Color(255,255,255));
			}
			for(int j=0;j<num2;j++){
				drawButton(new Color(0,0,0));
			}
			for(int j=0;j<num1;j++){
				drawButton(new Color(255,255,255));
			}
		}
	}
	
	public void drawButton(Color c){//new the button by color
		if(c.equals(new Color(255,255,255))){
			black_button =new JButton("");
			black_button.setBackground(c);
			add(black_button);
		}
		else{
			white_button =new JButton("");
			white_button.setBackground(c);
			add(white_button);
		}
	}
}
