package q2.s100502511;

public class Person {
	private String name;
	private String gender;
	private int age;

	Person() {

	}

	public Person(String newname, int newage, String newgender) {
		name = newname;
		age = newage;
		gender = newgender;

	}
	

	public String getName() { 
		return name;
	}
	
	public String getAge() {
		if (age==1)
		return "male";
		else 
		return "female";
	}

	public String getGender() {
		return gender;
	}
	

	public String toString() {
		return super.toString();
	}
}
