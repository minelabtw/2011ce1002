package q2.s100502511;

public class Manager extends Employee {
	private String title;

	Manager() {

	}

	public Manager(String name, int age, String gender, double newsalary,
			String title) {
		super(name, age, gender, newsalary);
		this.title = title;

	}

	public String getTitle(){
		return title;
	}
	
	
	public String toString() {
		return "This guy is a Manager,this guy ia an Employee!!";
	}

}
