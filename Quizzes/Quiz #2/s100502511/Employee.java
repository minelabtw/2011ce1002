package q2.s100502511;

public class Employee extends Person {
	private double salary;

	Employee() {

	}

	public Employee(String name, int age, String gender, double newsalary) {
		super(name, age, gender);
		this.salary = newsalary;

	}

	public double getSalary(){
		return salary;
	}
	
	public String toString() {
		return "This guy is a Employee!!";
	}

}
