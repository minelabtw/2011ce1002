package q2.s100502511;

public class Staff extends Employee {
	private String phoneNumber;

	Staff() {

	}

	Staff(String name, int age, String gender, double newsalary,
			String phoneNumber) {
		super(name, age, gender, newsalary);
		this.phoneNumber = phoneNumber;
	}
	
	public String getPhoneNumber(){
		return phoneNumber;
	}

	public String toString() {
		return "This guy is a Staff,this guy ia an Employee!!";
	}
}
