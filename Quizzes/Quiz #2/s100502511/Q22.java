package q2.s100502511;

import java.util.Scanner;

public class Q22 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		boolean exitFlag = false;

		while (exitFlag == false) {
			System.out
					.println("Please choose the employee you want to create 1.Manager 2.Staff 3.Exit: ");
			int userChoice = input.nextInt();

			switch (userChoice) {
			case 1:
				System.out
						.println("Please input the name,gender,age, salary,title");

				Manager manager = new Manager(input.next(), input.nextInt(),
						input.next(), input.nextDouble(), input.next());

				System.out.println("This guy is " + manager.getName() + ","
						+ manager.getAge() + "," + manager.getGender());
				System.out.println(manager.toString());
				System.out.println("Title is " + manager.getTitle() + ","
						+ "Salary is " + manager.getSalary());
				break;

			case 2:
				System.out
						.println("Please input the name,gender,age, salary,phonenumber");
				Staff staff = new Staff(input.next(), input.nextInt(),
						input.next(), input.nextDouble(), input.next());
				System.out.println("This guy is " + staff.getName() + ","
						+ staff.getAge() + "," + staff.getGender());
				System.out.println(staff.toString());
				System.out.println("Phonenumber is " + staff.getPhoneNumber()
						+ "," + "Salary is " + staff.getSalary());
				break;

			default:
				exitFlag = true;
				break;
			}
		}
	}
}
