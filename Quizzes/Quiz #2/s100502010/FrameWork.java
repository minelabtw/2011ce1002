package q2.s100502010;
import java.awt.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;


public class FrameWork extends JFrame
{
	public FrameWork()
	{
		
		JButton black=new JButton();
		JButton white=new JButton();
		black.setBackground(Color.BLACK);
		white.setBackground(Color.WHITE);
		
		String temp=JOptionPane.showInputDialog("Please input the length: ");
		int length=Integer.parseInt(temp);
		
		setLayout(new GridLayout(length,length,0,0));
		for(int counter=1;counter<=(length-1)/2;counter++)//print the upper picture
		{
			for(int counter2=1;counter2<=(length-(counter*2-1))/2;counter2++)
			{
				add(new JButton("")).setBackground(Color.WHITE);
			}
			for(int counter1=1;counter1<=2*counter-1;counter1++)
			{
				add(new JButton("")).setBackground(Color.BLACK);
			}
			for(int counter2=1;counter2<=(length-(counter*2-1))/2;counter2++)
			{
				add(new JButton("")).setBackground(Color.WHITE);
			}
		}
		for(int counter=1;counter<=length;counter++)//print the middle place
		{
			add(new JButton("")).setBackground(Color.BLACK);
		}
		for(int counter=1;counter<=(length-1)/2;counter++)//print the lower picture
		{
			for(int counter1=1;counter1<=counter;counter1++)
			{
				add(new JButton("")).setBackground(Color.WHITE);
			}
			for(int counter1=1;counter1<=length-2*counter;counter1++)
			{
				add(new JButton("")).setBackground(Color.BLACK);
			}
			for(int counter1=1;counter1<=counter;counter1++)
			{
				add(new JButton("")).setBackground(Color.WHITE);
			}
			
			
		}
		
		
	}
	

}
