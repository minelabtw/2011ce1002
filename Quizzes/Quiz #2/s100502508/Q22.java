package q2.s100502508;

import java.util.Scanner;
public class Q22 
{
	public static void main(String[] args)
	{
		Scanner input=new Scanner(System.in);
		boolean flag=true;
		while(flag)
		{
			System.out.println("Please choose the wmpoyee you want to create:1.Manager 2.Staff 3.Exit ");
			int choose=input.nextInt();
			switch(choose)//switch case
			{
				case 1:
					System.out.println("Please input name, age, gender, salary, title");
					Manager manager=new Manager(input.next(), input.nextInt(), input.next(), input.nextInt(), input.next());
					try
					{
						if(manager.getage()<0)
						{
							System.out.println("Age can't be less than 0 years old!!");
						}
						else if(manager.getsalary()<0)
						{
							System.out.println("Salary can't be less than 0 dollars!!");
						}
						else 
						{
							System.out.print(manager.getInfo());
						}
					}
					catch (Exception ex) //handle error
					{
						System.out.println("Initial Failed!! Please try again!!\n");
					}
					flag=true;
					break;
				case 2:
					System.out.println("Please input name, age, gender, salary, phone number");
					Staff staff=new Staff(input.next(), input.nextInt(), input.next(), input.nextInt(), input.next());
					try
					{
						if(staff.getphonelength()<10)
						{
							System.out.println("Phone number can't be less than 10 digits!!");
						}
						else 
						{
							System.out.print(staff.getInfo());
						}
						
					}
					catch (Exception e) //handle error
					{
						System.out.println("Initial Failed!! Please try again!!\n");
					}
					flag=true;
					break;
				case 3:
					System.out.println("Good bye!!");
					flag=false;
					break;
				default:
					System.out.println("Please enter numbers 1 to 3");
					flag=true;
					break;
			}
		}
	}
}
