package q2.s100502508;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.util.Scanner;

public class Q21 
{
	public static void main(String[] args)
	{
		Scanner input=new Scanner(System.in); 
		int length;
		length=input.nextInt();
		FrameWork f = new FrameWork(length);//create a frame
		f.setTitle("Quiz#2");//set the frame title
		f.setSize(500, 500);//set the frame size
		f.setLocationRelativeTo(null);//center a frame
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);//display the frame
	}
}
