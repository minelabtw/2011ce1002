package q2.s100502508;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class FrameWork  extends JFrame
{
	private Color black_button;
	private Color white_button;
	
	public  FrameWork(int length)
	{
		drawButton();
		JPanel panel=new JPanel(new GridLayout(length,length,0,0));
		JButton button1=new JButton();
		JButton button2=new JButton();
		button1.setBackground(black_button);
		button2.setBackground(white_button);	
		for(int a=0;a<length;a++)
		{
			for(int b=0;b<length;b++)
			{
				panel.add(button1);
			}
		}
		add(panel);
	}
	public void drawButton()
	{
		black_button=new Color(0,0,0);
		white_button=new Color(255,255,255);
	}
}
