package q2.s100502508;

public class Employee extends Person 
{
	protected int salary;
	
	public Employee(String Name,int Age,String Gender,int Salary)//initial
	{
		super(Name,Age,Gender);
		salary=Salary;
	}
	public String toString()//get String
	{
		return "This guy is an Employee";
	}
	
}
