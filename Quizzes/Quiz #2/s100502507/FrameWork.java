package q2.s100502507;
import javax.swing.*;
import java.awt.*;
public class FrameWork extends JFrame{
	private Color black_button;
	private Color white_button;
	private JButton button;
	private JPanel p;
	private int length;
	
	FrameWork() {
		p = new JPanel();
		black_button = new Color(0, 0, 0);
		white_button = new Color(255, 255, 255);
		boolean exit = false;
		while(!exit) {
			String input = JOptionPane.showInputDialog(null, "Input length please");
			length = Integer.parseInt(input);
			if(length<3) {
				JOptionPane.showMessageDialog(null, "Length cannot be negative");
			}
			else if(length%2==0) {
				JOptionPane.showMessageDialog(null, "Length should be odd");
			}
			else {
				p.setLayout(new GridLayout(length, length));
				exit = true;
			}
		}
		
		for(int i=0; i<length/2; i++) {
			for(int j=0; j<(int)(length/2)-i; j++) {//Up Left White
				drawButton(white_button);
			}
			for(int j=0; j<=i+1; j++) {//Up Left Black
				drawButton(black_button);
			}
			for(int j=0; j<i; j++) {//Up Right Black
				drawButton(black_button);
			}
			for(int j=0; j<(int)(length/2)-i; j++) {//Up Right White
				drawButton(white_button);
			}
		}
		for(int j=0; j<length; j++) {//Mid-Black Buttons
			drawButton(black_button);
		}
		for(int i=0; i<length/2; i++) {
			for(int j=0; j<=i+1; j++) {//Down Left White
				drawButton(white_button);
			}
			for(int j=0; j<(int)(length/2)-i; j++) {//Down Left Black
				drawButton(black_button);
			}
			for(int j=0; j<(int)(length/2)-i; j++) {//Down Right Black
				drawButton(black_button);
			}
			for(int j=0; j<i; j++) {//Down Right White
				drawButton(white_button);
			}
		}
		add(p);
	}
	
	public void drawButton(Color c) {
		button = new JButton();
		button.setBackground(c);
		p.add(button);
	}
}
