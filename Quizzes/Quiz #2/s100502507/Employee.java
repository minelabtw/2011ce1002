package q2.s100502507;

public class Employee extends Person {
	Employee(String Name, int Age, String Gender, int Salary) {
		super(Name, Age, Gender);
		salary = Salary;
	}
	public String toString() {
		return "This guy is an Employee!!";
	}
	protected int salary;
}