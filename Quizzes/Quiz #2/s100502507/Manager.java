package q2.s100502507;

public class Manager extends Employee {
	Manager(String Name, int Age, String Gender, int Salary, String Title) {
		super(Name, Age, Gender, Salary);
		title = Title;
	}
	public void setInfo() {
		
	}
	public void getInfo() {
		System.out.println("This guy is " + name +", " + age + "years old, " + gender);
		System.out.println(toString());
		System.out.println("Title is " + title + ", salary is " + salary);
	}
	public String toString() {
		return "This guy is an Manager!!\n" + super.toString();
	}
	protected String title;
}