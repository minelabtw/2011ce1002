package q2.s100502544;

public class Person {
	String name;
	int age;
	String gender;
	public Person(String n,int a,String g){
		setname(n);
		setage(a);
		setgender(g);
	}
	public void setname(String nn){
		name=nn;
	}
	public void setage(int aa){
		age=aa;
	}
	public void setgender(String gg){
		gender=gg;
	}
	public String getname(){
		return name;
	}
	public int getage(){
		return age;
	}
	public String getgender(){
		return gender;
	}
	public String toString(){
		return "this guy is an person";
	}
}
