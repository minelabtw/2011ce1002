package q2.s100502544;

public class Employee extends Person{
	int salary;
	String aTA;
	public Employee(String n,int a,String g,int s,String TA){
		super(n,a,g);
		setsalary(s);	
		setTA(TA);
	}
	public void setsalary(int ss){
		 salary=ss;
	}
	public int getsalary(){
		return salary;
	}
	public void setTA(String TA){
		 aTA=TA;
	}
	public String getTA(){
		return aTA;
	}
	public String usetoString(){
		
		return "this guy is an Employee!!";
	}
	public String toString(){
		return "this guy is a Manager";
	}
}
