package q2.s100502544;

public class Staff extends Employee{
	String number;
	public Staff(String n,int a,String g,int s,String TA,String num){
		super(n,a,g,s,TA);
		setnumber(num);
	}
	
	public void setnumber(String numnum){
		number=numnum;
	}
	public String getnumber(){
		return number;
	}
	public String toString(){
		return "this guy is a Staff!!"+super.usetoString();
	}

}
