package q2.s100502519;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.*;
import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener{
	
	JButton black_button = new JButton();
	JButton white_button = new JButton();
	JButton start = new JButton("Start");
	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();
	JPanel p3 = new JPanel();
	JPanel p4 = new JPanel();
	JPanel p5 = new JPanel();
	JPanel p6 = new JPanel();
	JPanel p7 = new JPanel();
	JPanel all = new JPanel();
	
	JTextField qs = new JTextField(5); 
	
	public FrameWork(){

		p5.add(qs);
		p5.add(start);
		all.add(p5);
		add(all);
	}

	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == start){
			
			all.removeAll();
			setVisible(true);
			all.setVisible(true);
			
			setLayout(new GridLayout(3,2,0,0));
			
			white_button.setBackground(Color.WHITE);
			black_button.setBackground(Color.BLACK);
			
			int num = Integer.parseInt(qs.getText());
			
			p1.setLayout(new GridLayout((num-1)/2,(num-1)/2,0,0));
			p2.setLayout(new GridLayout((num-1)/2,(num-1)/2,0,0));
			p3.setLayout(new GridLayout((num-1)/2,(num-1)/2,0,0));
			p4.setLayout(new GridLayout((num-1)/2,(num-1)/2,0,0));
			
			for(int i=0; i<(num-1)/2 ; i++){
				int j=i;
				int k=i;
				
				while(j+9>0){
					p1.add(white_button);
					j--;
					
				}
				while(k>0){
					p1.add(black_button);
					k--;
				}
				p1.add(black_button);
			}
		
			all.add(p1);
			
			for(int i=0; i<(num-1)/2 ; i++){
				
				int j=i;
				int k=i;
				
				while(k>0){
					p2.add(black_button);
					k--;
				}
				
				while(j+9>0){
					p2.add(white_button);
					j--;
				}
			}
			
			all.add(p2);
			
			for(int i=0 ; i<(num-1)/2 ; i++){
				p6.add(black_button);
			}
			
			add(p6);
			
			for(int i=0 ; i<=(num-1)/2 ; i++){
				p7.add(black_button);
			}
			
			add(p7);
			
			for(int i=0; i<(num-1)/2 ; i++){
				
				int j=i;
				int k=i;
				
				while(k>=0){
					p3.add(black_button);
					k--;
				}
				
				while(j+8>0){
					p3.add(white_button);
					j--;
				}
			}
			
			all.add(p3);
			
			for(int i=0; i<(num-1)/2 ; i++){
				
				int j=i;
				int k=i;
				
				while(j+8>0){
					p4.add(white_button);
					j--;
				}
				
				while(k>=0){
					p4.add(black_button);
					k--;
				}
			}
			
			all.add(p4);
			
			add(all);
		}
	}
}
