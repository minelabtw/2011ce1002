package q2.s995002203;

public class Manager extends Employee{
	private String title;
	Manager(){}
	Manager(String name, int age, String gender, int salary, String title)
	throws salaryException, ageException{
		setInfo(name, age, gender, salary, title);
	}
	public String toString(){
		return super.toString()+"This guy is a Manager";
	}
	public void setInfo(String name, int age, String gender, int salary, String title) 
	throws salaryException, ageException{
		if(salary<0)
			throw new salaryException();
		else if(age<0)
			throw new ageException();
		else{
			this.age=age;
			this.salary=salary;
			this.name=name;
			this.gender=gender;
			this.title=title;
		}
	}	
	public void getInfo(){
		System.out.println("This guy is "+this.name+", "+this.age+" years old, "+this.gender);
		System.out.println(this.toString());
		System.out.println("This is "+this.title+", Salary is "+this.salary);
	}
}
