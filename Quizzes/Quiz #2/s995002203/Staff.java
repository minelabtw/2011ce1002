package q2.s995002203;

public class Staff extends Employee{
	String phoneNumber;
	Staff(){}
	Staff(String name, int age, String gender, int salary, String phoneNUmber)
	throws salaryException, ageException, phoneException{
		setInfo(name, age, gender, salary, phoneNUmber);
	}
	public String toString(){
		return super.toString()+"This guy is a Staff";
	}
	public void setInfo(String name, int age, String gender, int salary, String phoneNumber)
			throws salaryException, ageException, phoneException{
		if(salary<0)
			throw new salaryException();
		else if(age<0)
			throw new ageException();
		else if(phoneNumber.length()<10)
			throw new phoneException();
		else{
			this.age=age;
			this.salary=salary;
			this.name=name;
			this.gender=gender;
			this.phoneNumber=phoneNumber;
		}
	}
	public void getInfo(){
		System.out.println("This guy is "+this.name+", "+this.age+", "+this.gender);
		System.out.println(this.toString());
	}
}
