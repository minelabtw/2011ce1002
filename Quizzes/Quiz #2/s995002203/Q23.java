package q2.s995002203;
import java.io.*;
import java.util.*;

public class Q23 {
	public static void calculate(String str){
		Stack s1=new Stack();
		int tmp1, tmp2;
		for(int i=0;i<str.length();i++){
			s1.showAll();
			if(str.charAt(i)>='0' &&str.charAt(i)<='9')
				s1.push(str.charAt(i)-'0');
			else{
				if(str.charAt(i)=='+'){
					tmp1=s1.pop();
					tmp2=s1.pop();
					tmp1+=tmp2;
					s1.push(tmp1);
				}
				else if(str.charAt(i)=='-')
				{
					tmp1=s1.pop();
					tmp2=s1.pop();
					tmp1-=tmp2;
					s1.push(tmp1);
				}
				else if(str.charAt(i)=='*')
				{
					tmp1=s1.pop();
					tmp2=s1.pop();
					tmp1*=tmp2;
					s1.push(tmp1);
				}
			}
		}
		s1.showAll();
	}
	public static void main(String [] args)throws Exception{
		File file=new File("input.txt");
		Scanner input=new Scanner(file);
		String inputString;
		while(input.hasNext()){
			inputString=input.next();
			System.out.println("The input string is: "+inputString);
			calculate(inputString);
		}
	}
	
}
