package q2.s995002203;
import java.util.*;

public class Q22 {
	public static void main(String [] args) throws Exception{
		Scanner input=new Scanner(System.in);
		String name, gender, title, tel;
		int choice, age, salary;
		while(true){
			System.out.println("Please choose the employee you want to creat:1.Manageer 2.Staff 3.Exit");
			choice=input.nextInt();
			if(choice==1){
				Manager m1;
				System.out.println("Please input name, age, gender, salary, title");
				name=input.next();
				age=input.nextInt();
				gender=input.next();
				salary=input.nextInt();
				title=input.next();
				try{
					m1=new Manager(name, age, gender, salary, title);
				}
				catch(Exception ex){
					System.err.println("Initial Faild!!");
					continue;
				}
				m1.getInfo();
				continue;
			}
			else if(choice==2){
				Staff s1;
				System.out.println("Please input name, age, gender, salary, phone number");
				name=input.next();
				age=input.nextInt();
				gender=input.next();
				salary=input.nextInt();
				tel=input.next();
				try{
					s1=new Staff(name, age, gender, salary, tel);
				}
				catch(Exception ex){
					System.err.println("Initial Faild!!");
					continue;
				}
				s1.getInfo();
				continue;
			}
			else{
				System.out.println("bye~");
				System.exit(0);
			}
		}
	}
}
