package q2.s995002203;

public class Stack {
	private int[] element=new int[5];
	private int index_stack;
	
	Stack(){
		index_stack=0;
	}
	public void push(int value){
		element[index_stack]=value;
		index_stack++;
	}
	
	public int pop(){
		int tmp=element[index_stack];
		element[index_stack]=0;
		index_stack--;
		return tmp;
	}
	
	public boolean isFull(){
		if(index_stack==4)
			return true;
		return false;
	}
	public boolean isEmpty(){
		if(index_stack==0)
			return true;
		return false;
	}
	
	public void showAll(){
		System.out.print("[ ");
		for(int i=0;i<5;i++)
			System.out.print(element[i]+" ");
		System.out.print("]\n");
	}
}
