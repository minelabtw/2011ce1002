package q2.s975002502;
import javax.swing.*;

import a7.s972002502.InvalidPasswordException;
import a7.s972002502.Password;

import java.awt.*;
import java.util.*;

public class Q22 {
	public static void main(String[] agrs) {
		boolean cont = true;
		int func;
		String name;							// the name user inputed
		int age;
		String gender;
		int salary;
		String title;
		String phonenum;

		Scanner input = new Scanner(System.in);		// new a scanner
		
		// let user input
		while(cont){
			System.out.println("Please choose the empoyee you want to create: 1.Manager 2.Staff 3.Exit ");
			func = input.nextInt();
			switch(func){
			case 1:
				Manager manager = new Manager();				// new a Manager
				System.out.println("Please input name, age, gender, salary, title ");
				name = input.next();
				age = input.nextInt();
				gender = input.next();
				salary = input.nextInt();
				title = input.next();
				
				// catch exception
				try {
					manager.setInfo(name, age, gender, salary, title);
					manager.getInfo();
				}
				catch (AgeException ex){
					System.err.println(ex.getMessage());
					System.err.println("Age can't less than 1!! \nInitial Faild!! Please try again!!");
					System.err.println("");
				}
				catch (SalaryException ex){
					System.err.println(ex.getMessage());
					System.err.println("Salary can't less than 0!! \nInitial Faild!! Please try again!!");
					System.err.println("");
				}
				break;
				
			case 2:
				Staff staff = new Staff();				// new a Staff
				System.out.println("Please input name, age, gender, salary, phonenumber ");
				name = input.next();
				age = input.nextInt();
				gender = input.next();
				salary = input.nextInt();
				phonenum = input.next();
				
				// catch exception
				try {
					staff.setInfo(name, age, gender, salary, phonenum);
					staff.getInfo();
				}
				catch (AgeException ex){
					System.err.println(ex.getMessage());
					System.err.println("Age can't less than 1!! \nInitial Faild!! Please try again!!");
					System.err.println("");
				}
				catch (PhoneException ex){
					System.err.println(ex.getMessage());
					System.err.println("Phone number can't be less than 10 digits!! \nInitial Faild!! Please try again!!");
					System.err.println("");
				} 
				catch (SalaryException ex) {
					System.err.println(ex.getMessage());
					System.err.println("Salary can't less than 0!! \nInitial Faild!! Please try again!!");
					System.err.println("");
				}
				break;
			case 3:
				System.out.println("bye~");
				cont = false;
				break;
			default:
				break;
			}
		}
	}
}
