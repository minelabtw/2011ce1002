package q2.s975002502;

public class PhoneException extends Exception {
	/** Construct an exception with no message */
	public PhoneException() {
		super("Illegal phone number format");
	}
	
	/** Construct an exception with the err */
	public PhoneException(String err) {
		System.err.println(err);
	}

}
