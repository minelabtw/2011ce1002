package q2.s975002502;

public class AgeException extends Exception {
	/** Construct an exception with no message */
	public AgeException() {
		super("Illegal age format");
	}
	
	/** Construct an exception with the err */
	public AgeException(String err) {
		System.err.println(err);
	}

}
