package q2.s975002502;

public class Manager extends Employee {
	protected String title;
	
	public Manager() {
	}
	
	/** Construct a Manager for initialization */
	public Manager(String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String toString() {
		super.toString();
		return "This guy " + name + " is a Manager. \n";
	}
	
	// set the Information of Manager
	public void setInfo(String name, int age, String gender, int salary, String title) throws AgeException, SalaryException {
		this.name = name;
		
		if(age < 1){
			throw new AgeException("Age can't less than 1!! \nInitial Faild!! Please try again!!");
		}
		else{
			this.age = age;
		}
		
		this.gender = gender;
		
		if(salary < 0){
			throw new AgeException("Salary can't less than 0!! \nInitial Faild!! Please try again!!");
		}
		else{
			this.salary = salary;
		}
		
		this.title = title;
		
	}
	
	public void getInfo() {
		System.out.println("This guy is " + name + ", " + age + " years old, " + gender);
		toString();
		System.out.println("Title is " + title + ", Salary is " + salary);
	}
}
