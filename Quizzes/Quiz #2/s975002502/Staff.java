package q2.s975002502;

public class Staff extends Employee{
	protected String phoneNumber;
	
	public Staff() {
	}
	
	/** Construct a Staff for initialization */
	public Staff(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public String toString() {
		super.toString();
		return "This guy " + name + " is a Staff. \n";
	}

	// set the Information of Staff
	public void setInfo(String name, int age, String gender, int salary, String phoneNumber) throws AgeException, SalaryException, PhoneException{
		this.name = name;
		
		if(age < 1){
			throw new AgeException("Age can't less than 1!! \nInitial Faild!! Please try again!!");
		}
		else{
			this.age = age;
		}
		
		this.gender = gender;
		
		if(salary < 0){
			throw new SalaryException("Salary can't less than 0!! \nInitial Faild!! Please try again!!");
		}
		else{
			this.salary = salary;
		}
		
		if(phoneNumber.length() < 10){
			throw new PhoneException("Phone number can't be less than 10 digits!! \nInitial Faild!! Please try again!!");
		}
		else{
			this.phoneNumber = phoneNumber;
		}
		
		
	}
	
	public void getInfo() {
		System.out.println("This guy is " + name + ", " + age + " years old, " + gender);
		super.toString();
		toString();
		System.out.println("Phone number is " + phoneNumber + ", Salary is " + salary);
	}

}
