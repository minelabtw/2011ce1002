package q2.s975002502;

public class SalaryException extends Exception {
	/** Construct an exception with no message */
	public SalaryException() {
		super("Illegal Salary format");
	}
	
	/** Construct an exception with the err */
	public SalaryException(String err) {
		System.err.println(err);
	}

}
