package q2.s975002502;

public class Person {
	protected String name;
	protected int age;
	protected String gender;
	
	public Person() {
	}
	
	/** Construct a Person for initialization */
	public Person(String name, int age, String gender) {
		this.name = name;
		this.age = age;
		this.gender = gender;		
	}
	
	/** Get the stored Name */
	public String getName() {
		return name;
	}
	
	/** Get the stored Age */
	public int getAge() {
		return age;
	}
	
	/** Get the stored Gender */
	public String getGender() {
		return gender;
	}

	/** set the Name */
	public void setName(String name) {
		this.name = name;
	}
	
	/** set the Age */
	public void setAge(int age) {
		this.age = age;
	}
	
	/** set the Gender */
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String toString() {
		return "This guy " + name + " is a Person.\n";
	}

}
