package q2.s975002502;

public class Employee extends Person {
	protected int salary;
	
	public Employee() {
	}
	public Employee(int salary) {
		this.salary = salary;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	public String toString() {
		return "This guy " + name + " is a Employee. \n";
	}
}
