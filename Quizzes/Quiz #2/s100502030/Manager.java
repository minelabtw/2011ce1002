package q2.s100502030;

public class Manager extends Employee{
	private String title;
	
	public Manager(String name,int age,String gender,int salary,String title) {
		setInfo(name,age,gender,salary,title);
	}
	
	public String toString() {
		return "This guy is a Manage! And"+super.toString();
	}
	
	public void setInfo(String name,int age,String gender,int salary,String title){
		super(name,age,gender,salary);
		this.title = title;
		
	}

}
