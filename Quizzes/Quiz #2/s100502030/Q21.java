package q2.s100502030;

import javax.swing.*;


public class Q21 {
	public static void main(String[] args) {
		
		String lengthStr = JOptionPane.showInputDialog("Please input the length of the paintain");
		int length = Integer.parseInt(lengthStr);
		
		JFrame frame = new FrameWork(length);
		frame.setSize(500,500);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		
	}
}
