package q2.s100502030;

public class Person {
	protected String name;
	protected int age;
	protected String gender;
	
	public Person(String name,int age,String gender) {
		this.name = name;
		this.age = age;
		this.gender = gender;
	}
	
	public String toString() {
		return "This guy is a person !";
	}
}
