package q2.s100502030;

public class Employee extends Person{
	protected int salary;
	
	public Employee(String name,int age,String gender,int salary){
		super(name,age,gender);
		this.salary = salary;
	}
	
	public String toString() {
		return "This guy is a Employee !";
	}
}
