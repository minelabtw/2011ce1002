package q2.s100502030;

import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

public class FrameWork extends JFrame {
	private Color black_button = Color.BLACK;
	private Color white_button = Color.WHITE;


	public FrameWork(int input) {

		JPanel p = new JPanel();
		p.setLayout(new GridLayout(input, input));

		for (int i = 1; i <= (input + 1) / 2; i++) {
			for (int j = 0; j < (input - (2 * i - 1)) / 2; j++) {
				JButton white = new JButton("");
				white.setBackground(white_button);
				p.add(white);
			}
			for (int j = 0; j < 2 * i - 1; j++) {
				JButton black = new JButton("");
				black.setBackground(black_button);
				p.add(black);
			}
			for (int j = 0; j < (input - (2 * i - 1)) / 2; j++) {
				JButton white = new JButton("");
				white.setBackground(white_button);
				p.add(white);
			}
		}
		for (int i = 1; i <= (input - 1) / 2; i++) {
			for (int j = 0; j < i; j++) {
				JButton white = new JButton("");
				white.setBackground(white_button);
				p.add(white);
			}
			for (int j = 0; j < input - 2*i; j++) {
				JButton black = new JButton("");
				black.setBackground(black_button);
				p.add(black);
			}
			for (int j = 0; j < i; j++) {
				JButton white = new JButton("");
				white.setBackground(white_button);
				p.add(white);
			}

		}
		setLayout(new GridLayout(1, 1));
		add(p);

	}

}
