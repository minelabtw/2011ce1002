package q2.s100502001;

public class Person {
	protected String name;
	protected int age;
	protected String gender;
	public Person(String n ,int a ,String g){
		name=n;
		age=a;
		gender=g;
	}
	public String getname(){
		return name;
	}
	public int getage(){
		return age;
	}
	public String getgender(){
		return gender;
	}
	public void tostring(){
		System.out.println("This guy is a person");
	}
	

}
