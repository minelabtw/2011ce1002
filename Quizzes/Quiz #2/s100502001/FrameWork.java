package q2.s100502001;
import java.awt.*;
import java.util.*;

import javax.swing.*;
public class FrameWork {
	private Color black_button;
	private Color white_button;
	private int height;
	public FrameWork(){
		black_button=new Color(255,255,255);
		white_button=new Color(0,0,0);
		Scanner input=new Scanner(System.in);
		String height=JOptionPane.showInputDialog("Height");
		int num=Integer.parseInt(height);
		setHeight(num);
	}
	public void setHeight(int n){
		height=n;
	}
	
	public void draw(){
		//setLayout(new GridLayout(height,height));
		for(int i=0;i<height;i++){
			for(int j=0;j<height;j++){
				if(i%2==0&&j%2==0){
					JLabel b=new JLabel("");
					b.setBackground(black_button);
					add(b);
				}
				else{
					JLabel w=new JLabel("");
					w.setBackground(white_button);
					add(w);
				}
					
					
			}
		}
	}

}
