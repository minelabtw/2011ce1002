package q2.s100502001;

public class Employee extends Person{
	private int salary;
	public Employee(int s,String name,int age,String gen){
		super(name, age, gen);
		salary=s;
	}
	public int getsalary(){
		return salary;
	}
	public void tostring(){
		System.out.println("This guy is an Employee");
	}
	
}
