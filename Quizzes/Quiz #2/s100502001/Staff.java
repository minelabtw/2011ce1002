package q2.s100502001;

public class Staff extends Employee{
	private String phone;
	public  Staff(String name,int age,String gen,int salary,String num){
		super(salary,name,age,gen);
		phone=num;
	}
	public String getphone(){
		return phone;
	}
	public void tostring(){
		System.out.println("This guy is an Staff");
	}
	
	public void setInfo() throws Exception{
		try{
			if(getsalary()<0)
				throw new SalaryException("Wrong numbers.");
			else if(getage()<0)
				throw new AgeException("Wrong age");
			else if(phone.length()<10)
				throw new PhoneException("Wrong phone length");
		}
		catch(SalaryException s){
			s.show();
			
		}
		catch(AgeException a){
			a.show();
			
		}
		catch(PhoneException p){
			p.show();
			
		}
			
	}
	public void getInfo(){
		System.out.print("This guy is"+getname()+",");
		System.out.print(getage()+"years old,");
		System.out.println(getgender());
		tostring();
		System.out.println("Title is "+getphone()+"Salary is "+getsalary());
	}
}
