package q2.s100502001;

public class Manager extends Employee{
	private String title;
	public Manager(String name,int age,String gen ,String t,int salary){
		super(salary,name,age,gen);
		title=t;
		
		
	}
	public String gettitle(){
		return title;
	}
	public void tostring(){
		System.out.println("This guy is an Manager");
	}
	
	public void setInfo() throws Exception{
		try{
			if(getsalary()<0)
				throw new SalaryException("Wrong numbers.");
			else if(getage()<0)
				throw new AgeException("Wrong age");
		}
		catch(SalaryException s){
			s.show();
			
		}
		catch(AgeException a){
			a.show();
			
		}
	
	}
	public void getInfo(){
		System.out.print("This guy is"+getname()+",");
		System.out.print(getage()+"years old,");
		System.out.println(getgender());
		tostring();
		System.out.println("Title is "+gettitle()+"Salary is "+getsalary());
	}
}
