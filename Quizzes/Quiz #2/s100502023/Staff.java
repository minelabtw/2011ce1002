package q2.s100502023;

public class Staff extends Employee 
{
	protected String phoneNumber;
	private boolean is_valid;
	
	public Staff(String name,int age,String gender,int salary,String phoneNumber) throws Exception
	{
		setInfo(name,age,gender,salary,phoneNumber);
	}
	
	public String toString()
	{
		String message=super.toString()+"This guy is a Staff";
		
		return message;
	}
	public void setInfo(String name,int age,String gender,int salary,String phoneNumber) throws Exception
	{
		if (age<=0)
		{
			is_valid=false;
			throw new ageException(is_valid);
		}
		else if (salary<0)
		{
			is_valid=false;
			throw new salaryException(is_valid);
		}
		else if (phoneNumber.length()<10)
		{
			is_valid=false;
			throw new phoneException(is_valid);
		}
		else
		{
			is_valid=true;
			this.name=name;
			this.age=age;
			this.gender=gender;
			this.salary=salary;
			this.phoneNumber=phoneNumber;
			
		}
	}
	
	public String getInfo()
	{
		String info="this guy is "+this.name+","+this.age+" years old,"+this.gender+"\n"+toString()+"\n"+"phoneNumber is "+this.phoneNumber+",salay is "+this.salary+"\n";
		return info;
	}
	
	
}
