package q2.s100502023;

import java.util.*;

public class Q22 
{

	public static void main(String[] args) throws Exception 
	{
	
		Scanner input = new Scanner(System.in);
		
		boolean is_finished=false;
		
		while(!is_finished)
		{
			System.out.println("Please choose the employee you want to create: 1.Manager 2.Staff 3.exit");
			int employee=input.nextInt();
			
			switch(employee)
			{
				case 1:
					System.out.println("Please input name,age,gender,salary,title");
					String employee_Manager_name = input.next();
					int employee_Manager_age = input.nextInt();
					String employee_Manager_gender =input.next();
					int employee_Manager_salary = input.nextInt();
					String employee_Manager_title = input.next();
					
					try
					{
						Manager employee_Manager = new Manager(employee_Manager_name, employee_Manager_age, employee_Manager_gender, employee_Manager_salary,employee_Manager_title);
						System.out.println(employee_Manager.getInfo());
					}
					catch(ageException ex)
					{
						System.out.println("Initial Faild pealse try again");
					}
					catch(salaryException ex)
					{
						System.out.println("Initial Faild pealse try again");
					}
					catch(phoneException ex)
					{
						System.out.println("Initial Faild pealse try again");
					}
					
					break;
					
				case 2:
					System.out.println("Please input name,age,gender,salary,phoneNumber");
					String employee_Staff_name = input.next();
					int employee_Staff_age = input.nextInt();
					String employee_Staff_gender =input.next();
					int employee_Staff_salary = input.nextInt();
					String employee_Staff_phoneNumber = input.next();
					
					try
					{
						Staff employee_Staff = new Staff(employee_Staff_name, employee_Staff_age, employee_Staff_gender, employee_Staff_salary,employee_Staff_phoneNumber);
						System.out.println(employee_Staff.getInfo());
					}
					catch(ageException ex)
					{
						System.out.println("Initial Faild pealse try again");
					}
					catch(salaryException ex)
					{
						System.out.println("Initial Faild pealse try again");
					}
					catch(phoneException ex)
					{
						System.out.println("Initial Faild pealse try again");
					}
					
					break;
					
				case 0:
					is_finished=true;
					break;
					
				default:
					System.out.println("error try again");
		}
		
			
		}
	}

}
