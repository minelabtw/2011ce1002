package q2.s100502023;

public class Manager extends Employee
{
	protected String title;
	private boolean is_valid;
	
	public Manager(String name,int age,String gender,int salary,String input_title) throws Exception
	{
		setInfo(name,age,gender,salary,input_title);
	}
	
	public String toString()
	{
		String message=super.toString()+"This guy is a Manager";
		
		return message;
	}
	
	public void setInfo(String name,int age,String gender,int salary,String input_title) throws Exception
	{
		//title=input_title;
		
		if (age<=0)
		{
			is_valid=false;
			throw new ageException(is_valid);
		}
		else if (salary<0)
		{
			is_valid=false;
			throw new salaryException(is_valid);
		}
		else
		{
			is_valid=true;
			this.name=name;
			this.age=age;
			this.gender=gender;
			this.salary=salary;
			this.title=input_title;
		}
	}
	
	public String getInfo()
	{
		String info="this guy is "+this.name+","+this.age+" years old,"+this.gender+"\n"+toString()+"\n"+"title is "+this.title+",salay is "+this.salary+"\n";
		return info;
	}
}
