package q2.s100502024;

public class Manager extends Employee
{
	protected String title;
	public Manager(String NAME,int AGE,String GENDER,int SALARY,String TITLE) throws Exception
	{
		super(NAME,AGE,GENDER);
		try
		{
			if(agecheck(AGE) == false)
			{
				throw new ageException();
			}
			if(salarycheck(SALARY) == false)
			{
				throw new salaryException();
			}
			setInfo(NAME,AGE,GENDER,SALARY,TITLE);
		}
		catch(ageException a)
		{
			System.out.println("Initial Faild!! Please try again!!!");
		}
		catch(salaryException b)
		{
			System.out.println("Initial Faild!! Please try again!!!");
		}
		
	}
	public String toString()
	{
		return "this guy is a Manager."+super.toString();
	}
	public void settitle(String title)
	{
		this.title = title;
	}
	public void setInfo(String Name,int Age,String Gender,int Salary,String Title)
	{
		setname(Name);
		setage(Age);
		setgender(Gender);
		setsalary(Salary);
		settitle(Title);
	}
	public void getInfo()
	{
		System.out.println("This guy is"+name+","+age+" years old,"+gender);
		System.out.println(toString());
		System.out.println("Title is"+title+",Salary is"+salary);
	}
	public boolean agecheck(int AGE)
	{
		boolean answer = true;
		if(age <= 0)
		{
			answer = false;
		}
		return answer;
	}
	public boolean salarycheck(int SALARY)
	{
		boolean answer = true;
		if(salary < 0)
		{
			answer = false;
		}
		return answer;
	}
}
