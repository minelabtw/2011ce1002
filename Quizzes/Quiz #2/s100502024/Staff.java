package q2.s100502024;

public class Staff extends Employee
{
	protected String phoneNumber;
	public Staff(String NAME,int AGE,String GENDER,int SALARY,String PHONENUMBER) throws Exception
	{
		super(NAME,AGE,GENDER);
		try
		{
			if(agecheck(AGE) == false)
			{
				throw new ageException();
			}
			if(salarycheck(SALARY) == false)
			{
				throw new salaryException();
			}
			if(phonecheck(PHONENUMBER) == false)
			{
				throw new phoneException();
			}
			setInfo(NAME,AGE,GENDER,SALARY,PHONENUMBER);
		}
		catch(ageException a)
		{
			System.out.println("Initial Faild!! Please try again!!!");
		}
		catch(salaryException b)
		{
			System.out.println("Initial Faild!! Please try again!!!");
		}
		catch(phoneException c)
		{
			System.out.println("Initial Faild!! Please try again!!!");
		}
		
	}
	public void setInfo(String Name,int Age,String Gender,int Salary,String PhoneNumber)
	{
		setname(Name);
		setage(Age);
		setgender(Gender);
		setsalary(Salary);
		setphoneNumber(PhoneNumber);
	}
	public void setphoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}
	public void getInfo()
	{
		System.out.println("This guy is"+name+","+age+" years old,"+gender);
		System.out.println(toString());
		System.out.println("PhoneNUmber is"+phoneNumber);
	}
	public String toString()
	{
		return "This guy is a Staff."+super.toString();
	}
	public boolean agecheck(int AGE)
	{
		boolean answer = true;
		if(age <= 0)
		{
			answer = false;
		}
		return answer;
	}
	public boolean salarycheck(int SALARY)
	{
		boolean answer = true;
		if(salary < 0)
		{
			answer = false;
		}
		return answer;
	}
	public boolean phonecheck(String PHONENUMBER)
	{
		boolean answer = true;
		int length = PHONENUMBER.length();
		if(length < 10)
		{
			answer = false;
		}
		return answer;
	}
}
