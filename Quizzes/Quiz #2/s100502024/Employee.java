package q2.s100502024;

public class Employee extends Person
{
	protected int salary;
	public Employee(String NAME, int AGE, String GENDER) 
	{
		super(NAME,AGE,GENDER);
		setname(NAME);
		setage(AGE);
		setgender(GENDER);
	}
	public void setsalary(int salary)
	{
		this.salary = salary;
	}
	public String getname(String name)
	{
		return name;
	}
	public int getage(int age)
	{
		return age;
	}
	public String getgender(String gender)
	{
		return gender;
	}
	public String toString()
	{
		return "this guy is an Employee.";
	}
}
