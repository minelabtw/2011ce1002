package q2.s100502011;

public class Person {
	
	protected String name;
	protected int age;
	protected String gender;
	Person(String username,int userage,String usergender){
		name = username;
		age = userage;
		gender =usergender;
		
	}
	
	public String toString(){
		return "Person";
	}
	public String getGender(){
		return gender;
	}
	public int getAge(){
		return age;
	}
	public String getName(){
		return name;
	}
}
