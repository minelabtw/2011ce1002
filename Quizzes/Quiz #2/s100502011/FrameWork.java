package q2.s100502011;

import java.awt.*;
import javax.swing.*;
public class FrameWork extends JFrame{
	private int leng; // length
	public JPanel panel = new JPanel(); // panel
	public FrameWork(){ //constructor
		
		String lengt = JOptionPane.showInputDialog("Enter an odd number : "); // length
		int length = Integer.parseInt(lengt);
		panel.setLayout(new GridLayout(length,length,0,0)); // grid
		
		for(int a=0;a<(1+length)/2-1;a++){ // upper picture
			for(int b=0;b<((1+length)/2-1-a);b++){
				panel.add((new JButton())).setBackground(Color.WHITE);
			}
			for(int c=0;c<1+2*a;c++){
				panel.add((new JButton())).setBackground(Color.BLACK);
			}
			for(int d=0;d<((1+length)/2-1-a);d++){
				panel.add((new JButton())).setBackground(Color.WHITE);
			}
		}
		for(int e=1;e<=length;e++){  // middle
			panel.add((new JButton())).setBackground(Color.BLACK);
		}
		for(int a=(1+length)/2-1;a>0;a--){ // lower picture
			for(int b=0;b<=((1+length)/2-1-a);b++){
				panel.add((new JButton())).setBackground(Color.WHITE);
			}
			for(int c=0;c<1+2*(a-1);c++){
				panel.add((new JButton())).setBackground(Color.BLACK);
			}
			for(int d=0;d<=((1+length)/2-1-a);d++){
				panel.add((new JButton())).setBackground(Color.WHITE);
			}
		}
		this.add(panel); // add to frame
	}

}
