package q2.s100502011;

public class Employee extends Person{
	protected int salary;
	Employee(String uname,int uage,String ugender,int usalary){
		super(uname,uage,ugender);
		salary = usalary;
	}
	
	public String toString(){
		return "Employee";
		}
	public int getSalary(){
		return salary;
	}

}
