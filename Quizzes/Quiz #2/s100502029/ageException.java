package q2.s100502029;

public class ageException extends Exception {
	private int age;
	
	public ageException(int age) {
		super("age could not less than or equal 0");
		this.age = age;
	}
	
	public int getAge() {
		return age;
	}
}
