package q2.s100502029;

public class Employee extends Person {
	private int salary;
	
	public Employee() {
		
	}
	
	public Employee(String name, int age, String gender, int salary) {
		setName(name);
		setAge(age);
		setGender(gender);
		this.salary = salary;
	}
	
	public void setSalary(int salary) {
		this.salary = salary;
	}
	
	public int getSalary() {
		return salary;
	}
	
	public String toString() {
		return "this guy is an Employee";
	}
}
