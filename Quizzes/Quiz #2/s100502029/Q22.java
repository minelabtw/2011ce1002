package q2.s100502029;
import java.util.Scanner;

public class Q22 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String name;
		int age;
		String gender;
		int salary;
		String title;
		String phoneNumber;
		Manager manager;
		Staff staff;
		while (true) {
			System.out.println("Please choose the employee you want to create:1.Manager 2.Staff 3.Exit");
			int choose = input.nextInt();
			switch (choose) {
				case 1:
					System.out.println("Please input name,age,gender,salary,title");
					name = input.next();
					age = input.nextInt();
					gender = input.next();
					salary = input.nextInt();
					title = input.next();
					try {
						manager = new Manager(name, age, gender, salary, title);
						manager.getInfo();
						System.out.println();
					}
					catch (ageException ex) {
						System.err.println(ex.getMessage());
						System.out.println("Initial Failed!!Please try again!!\n");
					}
					catch (salaryException ex) {
						System.err.println(ex.getMessage());
						System.out.println("Initial Failed!!Please try again!!\n");
					}
					break;
				case 2:
					System.out.println("Please input name,age,gender,salary,phone number");
					name = input.next();
					age = input.nextInt();
					gender = input.next();
					salary = input.nextInt();
					phoneNumber = input.next();
					try {
						staff = new Staff(name, age, gender, salary, phoneNumber);
						staff.getInfo();
						System.out.println();
					}
					catch (ageException exAge) {
						System.err.println(exAge.getMessage());
						System.out.println("Initial Failed!!Please try again!!\n");
					}
					catch (salaryException ex) {
						System.err.println(ex.getMessage());
						System.out.println("Initial Failed!!Please try again!!\n");
					}
					catch (phoneException ex) {
						System.err.println(ex.getMessage());
						System.out.println("Initial Failed!!Please try again!!\n");
					}
					break;
				case 3:
					System.exit(0);
				default:
					System.out.println("wrong input,please try again!");
					break;
			}
		}
	}
}
