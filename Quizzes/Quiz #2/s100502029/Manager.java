package q2.s100502029;

public class Manager extends Employee {
	private String title;
	
	public Manager() {
		
	}
	
	public Manager(String name, int age, String gender, int salary, String title) throws ageException, salaryException {
		setInfo(name, age, gender, salary, title);
	}
	
	public void setInfo(String name, int age, String gender, int salary, String title) throws ageException, salaryException {
		setName(name);
		if (checkAge(age))
			setAge(age);
		else
			throw new ageException(age);
		setGender(gender);		
		if (checkSalary(salary))
			setSalary(salary);
		else
			throw new salaryException(salary);		
		this.title = title;
	}
	
	public void getInfo() {
		System.out.println("this guy is " + getName() + "," + getAge() + "years old" + "," + getGender());
		System.out.println(toString());
		System.out.println("Title is " + title + ",salary is " + getSalary());
	}
	
	public String toString() {
		return super.toString() + "!!this guy is a Manager";
	}
	
	public boolean checkAge(int age) {
		if (age > 0)
			return true;
		else
			return false;
	}
	
	public boolean checkSalary(int salary) {
		if (salary >= 0)
			return true;
		else
			return false;
	}
}
