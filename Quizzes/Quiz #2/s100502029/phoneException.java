package q2.s100502029;

public class phoneException extends Exception {
	private String phoneNumber;
	
	public phoneException(String phoneNumber) {
		super("phone number can't be less than 10 digits!!");
		this.phoneNumber = phoneNumber;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
}
