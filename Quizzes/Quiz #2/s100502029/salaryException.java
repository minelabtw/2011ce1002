package q2.s100502029;

public class salaryException extends Exception {
	private int salary;
	
	public salaryException(int salary) {
		super("salary could not less than 0");
		this.salary = salary;
	}
	
	public int getSalary() {
		return salary;
	}
}
