package q2.s100502029;

public class Staff extends Employee{
	private String phoneNumber;
	
	public Staff() {
		
	}
	
	public Staff(String name, int age, String gender, int salary, String phoneNumber) throws ageException, salaryException, phoneException {
		setInfo(name, age, gender, salary, phoneNumber);
	}
	
	public void setInfo(String name, int age, String gender, int salary, String phoneNumber) throws ageException, salaryException, phoneException {
		setName(name);
		if (checkAge(age))
			setAge(age);
		else
			throw new ageException(age);
		setGender(gender);
		if (checkSalary(salary))
			setSalary(salary);
		else
			throw new salaryException(salary);
		if (checkPhoneNumber(phoneNumber))
			this.phoneNumber = phoneNumber;
		else
			throw new phoneException(phoneNumber);
	}
	
	public void getInfo() {
		System.out.println("this guy is " + getName() + "," + getAge() + "years old" + "," + getGender());
		System.out.println(toString());
		System.out.println("phonenumber is " + phoneNumber + ",salary is " + getSalary());
	}
	
	public String toString() {
		return super.toString() + "!!this guy is a Staff";
	}
	
	public boolean checkAge(int age) {
		if (age > 0)
			return true;
		else
			return false;
	}
	
	public boolean checkSalary(int salary) {
		if (salary >= 0)
			return true;
		else
			return false;
	}
	
	public boolean checkPhoneNumber(String phoneNumber) {
		if (phoneNumber.length() >= 10)
			return true;
		else
			return false;
	}
}
