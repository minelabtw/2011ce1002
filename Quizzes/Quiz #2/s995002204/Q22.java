package q2.s995002204;

import java.util.Scanner;

public class Q22 {
	public static void main(String args[]) throws Exceptions
	{
		//declare variable
		int choice=0;
		String name;
		int age;
		String gender;
		int salary;
		String title;
		String phonenumber;
		
		Scanner input = new Scanner(System.in);
		System.out.println("Please choose the employee you want to create:1.Manager 2.Staff 3.Exit");
		choice = input.nextInt();
		
		//loop
		do
		{
			// do choice
			switch(choice)
			{
			case 1:
				System.out.println("Please input name,age,gender,salary,title");
				name = input.next();
				age = input.nextInt();
				gender = input.next();
				salary = input.nextInt();
				title = input.next();
				Manager manager1 = new Manager(name,age,gender,salary,title);
				manager1.getInfo();
				break;
			case 2:	
				System.out.println("Please input name,age,gender,salary,phone number");
				name = input.next();
				age = input.nextInt();
				gender = input.next();
				salary = input.nextInt();
				phonenumber = input.next();
				Staff staff1 = new Staff(name,age,gender,salary,phonenumber);
				staff1.getInfo();
				break;
			default:
				break;
			}
			
		}while(choice!=3);
	}
}
