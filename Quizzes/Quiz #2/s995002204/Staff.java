package q2.s995002204;

public class Staff extends Employee{
	
	protected String phonenumber;
	
	Staff(String a,int b,String c,int d,String e) throws Exceptions	//constructor
	{
		setInfo(a,b,c,d,e);
	}
	
	public String toString()
	{
		return super.toString()+"This guy is a staff.";
	}
	
	public String getInfo()	//get information
	{
		return "This guy is "+name+","+age+" years old,"+gender+"\n"+this.toString()+"\n"+"Phonenumber is "+phonenumber+"Salary is "+salary;
	}
	
	public void setInfo(String inputName,int inputAge,String inputGender,int inputSalary,String inputPhonenumber) throws Exceptions 
	{
		try
		{
			name = inputName;
			if(inputAge<=0)
				throw new Exceptions(1);
			age = inputAge;
			gender = inputGender;
			if(inputSalary<0)
				throw new Exceptions(2);
			salary = inputSalary;
			if(inputPhonenumber.length()<10)
				throw new Exceptions(3);
			phonenumber = inputPhonenumber;
		}
		
		catch (Exceptions ex)
		{
			System.err.println("Initial Failed\n");
		}
	}
}
