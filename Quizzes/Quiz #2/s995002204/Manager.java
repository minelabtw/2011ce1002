package q2.s995002204;

public class Manager extends Employee{
	
	protected String title;	//title
	
	Manager(String a,int b,String c,int d,String e) throws Exceptions	//constructor
	{
		setInfo(a,b,c,d,e) ;
	}
	
	
	public String toString()
	{
		return super.toString()+"This guy is a Manager.";
	}
	
	public String getInfo()	//get information
	{
		return "This guy is "+name+","+age+" years old,"+gender+"\n"+this.toString()+"\n"+"Title is "+title+",Salary is "+salary;
	}
	
	public void setInfo(String inputName,int inputAge,String inputGender,int inputSalary,String inputTitle) throws Exceptions
	{
		try
		{
			name = inputName;
			if(inputAge<=0)
				throw new Exceptions(1);
			age = inputAge;
			gender = inputGender;
			if(inputSalary<0)
				throw new Exceptions(2);
			salary = inputSalary;
			title = inputTitle;
		}
		
		catch (Exceptions ex)
		{
			System.err.println("Initial Failed\n");
		}
	}
}
