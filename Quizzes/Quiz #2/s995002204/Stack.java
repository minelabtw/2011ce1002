package q2.s995002204;

public class Stack {
	private int element[] = new int[5];
	private int index_stack = -1;
	
	public int pop()
	{
		return element[index_stack];
	}
	
	public void push(int a)
	{
		index_stack++;
		element[index_stack] = a;
	}
	
	public boolean isFull()
	{
		if(index_stack>4)
			return true;
		else
			return false;
	}
	
	public boolean isEmpty()
	{
		if(index_stack<0)
			return true;
		else
			return false;
	}
}
