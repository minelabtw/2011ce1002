package q2.s100502510;

public class Person {
	protected String name;
	protected int age;
	protected String gender;
	protected String message;

	public Person(String name2, int age2, String gender2) {
		name = name2;
		age = age2;
		gender = gender2;
	}

	public String toString() {

		message = "This guy is a person!!";
		return message;
	}
}
