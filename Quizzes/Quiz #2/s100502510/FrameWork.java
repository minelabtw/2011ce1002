package q2.s100502510;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import java.util.Scanner;

public class FrameWork extends JFrame {
	Color black_button=new Color(255,255,255);
	Color white_button=new Color(0,0,0);
	JButton button=new JButton();
	int length;
	int times = 1;

	public FrameWork() {
		Scanner input = new Scanner(System.in);
		System.out.print("PLease input the length:");
		length = input.nextInt();
		if (length % 2 == 0) {
			System.err.print("The inputed should be odd integer!!!");
			System.exit(0);
		}
		JPanel panel = new JPanel(new GridLayout(length, length));
		for (int i = 0; i < length; i++) {
			
			if(i/((length-1)/2)==times&&i>=(length-1)/2){
				
				this.drawButton(black_button);
				panel.add(button);
				
			}
			for(int j=i;j<(j+times);j++){
				
				this.drawButton(black_button);
				panel.add(button);
			}
			times=times+2;
		}
		add(panel);
	}

	public void drawButton(Color c) {

		button.setBackground(c);
		
	}
}
