package q2.s100502022;

public class Employee extends Person{
	protected int salary;
	
	public Employee(String name, int age, String gender,int salary) {
		super(name, age, gender);
		this.salary=salary;
		// TODO Auto-generated constructor stub
	}
	
	public String toString(){
		return "This guy is "+name+","+age+" years old, "+gender+"\nThis guy is an Employee!!";
	}
	
}
