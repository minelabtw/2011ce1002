package q2.s100502022;

public class Manager extends Employee{
	private String title;
	
	public Manager(String name, int age, String gender, int salary,String title) throws ageException, salaryException {
		super(name, age, gender, salary);
		this.title=title;
		// TODO Auto-generated constructor stub
		setInfo(name,age,gender,salary,title);
	}
	
	public void setInfo(String n,int a,String g,int s,String t)throws ageException,salaryException{
		if(s<=0){
			throw new salaryException();
		}
		else if (a<=0){
			throw new ageException();
		}
		else if(a>0&&s>0){
		toString();
		getInfo();
		}
		
	}
	public String getInfo(){
		return "Title is "+title+",Salary is "+salary;
	}

	public String toString(){
		return super.toString()+"This guy is a Manager\n";
	}


}
