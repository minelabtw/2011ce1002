package q2.s100502022;

public class Staff extends Employee{
	private String phone;
	public Staff(String name, int age, String gender, int salary,String phone) throws ageException, salaryException, phoneException {
		super(name, age, gender, salary);
		this.phone=phone;
		// TODO Auto-generated constructor stub
		setInfo(name,age,gender,salary,phone);
	}
	
	public void setInfo(String n,int a,String g,int s,String p)throws ageException,salaryException,phoneException{
		if(s<=0){
			throw new salaryException();
		}
		else if(a<=0)
		{
			throw new ageException();
		}
		else if (p.length()!=10){
			throw new phoneException();
		}
		else if(a>0&&s>0&&p.length()==10){
		toString();
		getInfo();
		}
	}
	public String getInfo(){
		return "Phonenumber is "+phone+",Salary is "+salary;
	}

	public String toString(){
		return super.toString()+"This guy is a Staff";
	}

}
