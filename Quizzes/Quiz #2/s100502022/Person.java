package q2.s100502022;

public class Person {
	protected String name;
	protected int age;
	protected String gender;
	
	public Person(String name,int age,String gender){
		this.age=age;
		this.name=name;
		this.gender=gender;
	}
	public String toString(){
		return "This guy is "+name+","+age+" years old, "+gender;
	}
}
