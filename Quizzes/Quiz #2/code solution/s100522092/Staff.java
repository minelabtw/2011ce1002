package q2.s100522092;

public class Staff extends Employee {
	private String phoneNumber;
	
	public Staff(String nameInput, int ageInput, String genderInput, int salaryInput, String phoneInput)
		throws ageException, salaryException, phoneException
	{
		setInfo(nameInput, ageInput, genderInput, salaryInput, phoneInput);
	}
	
	// show a message
	public String toString(){
		return super.toString() + "This guy is a Satff!! ";
	}
	
	//set information with exception handling
	public void setInfo(String nameInput, int ageInput, String genderInput, int salaryInput, String phoneInput)
			throws ageException, salaryException, phoneException
		{
			name = nameInput;
			
			if(ageInput <= 0) // if age is less than 0
				throw new ageException();
			else 
				age = ageInput;
			
			gender = genderInput;
			
			if(salaryInput < 0) // if salary is less than 0
				throw new salaryException();
			else
				salary = salaryInput;
			
			if(phoneInput.length() < 10) // if the title is null
				throw new phoneException();
			else
				phoneNumber = phoneInput;
		}
	
	// get information
	public void getInfo(){
		System.out.println("This guy is " + name + ", " + age + " years old, " + gender);
		System.out.println(toString());
		System.out.println("Salary is " + salary + ", phone number is " + phoneNumber);
	}
}
