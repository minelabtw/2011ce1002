package q2.s100522092;

public class Person {
	protected String name;
	protected int age;
	protected String gender;
	
	// implicit constructor
	public Person(){
		
	}
	
	// show a message
	public String toString(){
		return "This guy is a person";
	}
}
