package q2.s100522092;
import java.util.*;

public class Q22 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		
		System.out.println("Please choose the employee you want to create: 1.Manager 2.Staff 3.Exit");
		int userChoice = input.nextInt();
			
		switch(userChoice){
			case 1:
				System.out.println("Please input name, age, gender, salary, title");
				try{
					Manager m1 = new Manager(input.next(), input.nextInt(), input.next(), input.nextInt(), input.next());
					m1.toString();
					m1.getInfo();
				}
				catch(ageException e){	
					System.err.println("Initial Failed!! Please try again!!");
				}
				catch(salaryException e){	
					System.err.println("Initial Failed!! Please try again!!");
				}
				break;
			case 2:
				System.out.println("Please input name, age, gender, salary, phone number");
				try{
					Staff s1 = new Staff(input.next(), input.nextInt(), input.next(), input.nextInt(), input.next());
					s1.toString();
					s1.getInfo();
				}
				catch(ageException e){
					System.err.println("Initial Failed!! Please try again!!");
				}
				catch(salaryException e){	
					System.err.println("Initial Failed!! Please try again!!");
				}
				catch(phoneException e){
					System.err.println("Initial Failed!! Please try again!!");
				}
				break;
			default:
				break;
		}
	}
}
