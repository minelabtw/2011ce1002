package q2.s100522092;

public class Manager extends Employee {
	private String title;
	
	public Manager(String nameInput, int ageInput, String genderInput, int salaryInput, String titleInput)
		throws ageException, salaryException
	{
		setInfo(nameInput, ageInput, genderInput, salaryInput, titleInput);
	}
		
	// show a message
	public String toString(){
		return super.toString() + "This guy is a Manager!!";
	}
	
	
	//set information with exception handling
	public void setInfo(String nameInput, int ageInput, String genderInput, int salaryInput, String titleInput)
		throws ageException, salaryException
	{
		name = nameInput;
		
		if(ageInput <= 0) // if age is less than 0
			throw new ageException();
		else 
			age = ageInput;
		
		gender = genderInput;
		
		if(salaryInput < 0) // if salary is less than 0
			throw new salaryException();
		else
			salary = salaryInput;
	}
	
	// get information
	public void getInfo(){
		System.out.println("This guy is " + name + ", " + age + " years old, " + gender);
		System.out.println(toString());
		System.out.println("Title is " + title + ", Salary is " + salary);
	}
}
