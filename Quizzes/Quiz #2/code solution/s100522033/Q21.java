package q2.s100522033;

import javax.swing.JFrame;

  
public class Q21 {
	public static void main(String [] args){
		FrameWork f = new FrameWork();
		f.setTitle("Quiz #2"); //Frame title
		f.setSize(500, 500);   //Frame size
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
