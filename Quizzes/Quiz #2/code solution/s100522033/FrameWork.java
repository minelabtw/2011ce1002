package q2.s100522033;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.*;

public class FrameWork extends JFrame{
	
	//Initial two colors
	Color black_b = new Color(0,0,0);
	Color white_b = new Color(255,255,255);
	
	//new and assign the color
	public void drawButton(Color c){
		JButton button = new JButton();
		button.setBackground(c);
		this.add(button);	
	}
	
	public FrameWork(){
		
		String input_s = JOptionPane.showInputDialog("Enter the integer:");
		int input_n = Integer.parseInt(input_s);

		int center = (input_n+1)/2-1;//the center of length
		this.setLayout(new GridLayout(input_n,input_n,1,1));//for example: 5*5

		//draw from top row to center row
		for(int i=0;i<=center;i++){
			for(int j=0;j<center-i;j++)
				drawButton(white_b);
			for(int j=0;j<2*i+1;j++)
				drawButton(black_b);
			for(int j=0;j<center-i;j++)
				drawButton(white_b);
		}
		
		//draw from the row below center row to the last row
		for(int i=1;i<=center;i++){
			for(int j=0;j<i;j++)
				drawButton(white_b);
			for(int j=0;j<2*(center-i)+1;j++)
				drawButton(black_b);
			for(int j=0;j<i;j++)
				drawButton(white_b);
		}
		
	}
}
