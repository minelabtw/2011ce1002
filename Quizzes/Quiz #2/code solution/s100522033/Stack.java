package q2.s100522033;

public class Stack {
	
	private int[] elements;
	private int index = 0;
	private static final int capacity = 10;
	
	public Stack(){
		elements = new int[capacity];
	}
	
	
	public void push(int value){
		if(!isFull()){
			elements[index] = value;
			index++;
		}
		else{
			index = capacity;
			//System.out.println("The Stack is FULL");
		}
	}
	
	public int pop(){
		if(!isEmpty()){
			index--;
			//System.out.println("The element is popped:"+elements[index]);
			return elements[index];
		}
		else{
			//System.out.println("The Stack is EMPTY");
			index = 0;
			return 0;
		}
	}
	
	public boolean isFull(){
		if(index == capacity)
			return true;
		else
			return false;
	}
	
	public boolean isEmpty(){
		if(index == 0)
			return true;
		else 
			return false;
	}
	
	public void showAll(){
		System.out.printf("[");
		for(int i=0;i<index;i++){
			System.out.printf(elements[i]+" ");
		}
		System.out.println("]");
	}

}
