package q2.s100522033;

import java.io.*;
import java.util.Scanner;

public class Q23 extends Exception{
	public static void main(String[] args) throws Exception{
	Stack s = new Stack();

	FileReader file = new FileReader("input.txt"); //read the input file

	Scanner input = new Scanner(file);//read the string line by line in the input file

	while(input.hasNext()){
		
		String input_s = input.next();//store the string in it
		System.out.println("The input string: "+input_s);

		for(int i=0;i<input_s.length();i++){
			s.showAll();//show the stack in each step
			
			//if the character is operator
			if(input_s.charAt(i)=='+' || input_s.charAt(i)=='-' || input_s.charAt(i)=='*' || input_s.charAt(i)=='/'){
				int b = s.pop();//pop the two value from the top of stack
				int a = s.pop();
				
				//do the operation and push the value back
				switch(input_s.charAt(i)){
					case '+':
						s.push(a+b);//plus
						break;
					case '-':
						s.push(a-b);//sub
						break;
					case '*':
						s.push(a*b);//mul
						break;
					case '/':
						s.push(a/b);//div
						break;
				}
			}
			//if the character is operand
			else{
				s.push(input_s.charAt(i)-'0');//convert to integer 
			}
		}
		
		//if there's no character left in the string, 
		//the last one,which is in the stack should be the anwser
		s.showAll();
		System.out.println("Here is the result: "+s.pop()+"\n\n");
		
		}
	}
}
