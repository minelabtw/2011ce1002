package q2.s100502521;

public class Stack
{
	private int[] element=new int[5];//存放用
	private int index_stack;//多少元素
	public Stack()//無傳入值設初始值100
	{
		index_stack=0;
	}
	public int[] showAll()//回傳全部內容  不直接print 可以傳到視窗上做顯示
	{
		return element;
	}
	public int Pop()//pop 取出  如果中途結束 會回傳false
	{
		if(isEmpty())
		{
			System.err.println("Stack empty");
		}
		index_stack--;
		return element[index_stack];
	}
	public void Push(int num)//push 將數字加入 如果滿了則傳回false
	{
		if(isFull())
		{
			System.err.println("Stack Full");
		}
		else
		{
			element[index_stack]=num;
			index_stack++;
		}
	}
	public boolean isEmpty()//如果是空的則傳回true
	{
		if(index_stack==0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public boolean isFull()//如果是滿的則傳回true
	{
		if(index_stack==element.length)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public int getSize()//回傳目前元素量
	{
		return index_stack;
	}
}
