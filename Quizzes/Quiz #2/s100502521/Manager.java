package q2.s100502521;

public class Manager extends Employee 
{
	protected String title;
	public Manager(String input_name,int input_age,String input_gender,int input_salary,String input_title)
	{
		super(input_name,input_age,input_gender,input_salary);
		setInfo(input_name,input_age,input_gender,input_salary,input_title);
	}
	public void setInfo(String input_name,int input_age,String input_gender,int input_salary,String input_title)
	{
		name=input_name;
		age=input_age;
		gender=input_gender;
		salary=input_salary;
		title=input_title;
		try
		{
			if(age<=0)
			{
				throw new Exceptions(1);
			}
			if(salary<0)
			{
				throw new Exceptions(2);
			}			
			getInfo();
		}
		catch(Exception e)
		{
			System.err.println("Initial Faild!!Please try again");
		}
		
	}
	public void getInfo()
	{
		System.out.println("This guy is "+name+","+age+" years old, "+gender);
		System.out.println(toString());
		System.out.println("Title is "+title+",Salary is "+salary);
	}
	public String toString()
	{
		return super.toString()+"This guy is a Manager.";
	}
}
