package q2.s100502521;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Q21 
{
	public static void main(String[] args)
	{
		int number;
		while(true)
		{
			String input=JOptionPane.showInputDialog("請輸入行數(奇數):");//讓使用者輸入姓名
			number=Integer.parseInt(input);
			if(number%2==1)
			{
				break;
			}
			else
			{
				System.out.println("輸入錯誤!!");
			}
		}
		FrameWork f = new FrameWork(number);
		f.setTitle("Asignment #7");
		f.setSize(800, 600);
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
