package q2.s100502521;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;

public class FrameWork extends JFrame
{
	public FrameWork(int line)
	{
		setLayout(new GridLayout(line,line));
		line=line/2;
		for(int i=0;i<line+1;i++)
		{
			for(int j=line;j>i;j--)
			{
				drawButton(Color.WHITE);
			}
			for(int j=0;j<i*2+1;j++)
			{
				drawButton(Color.BLACK);
			}
			for(int j=line;j>i;j--)
			{
				drawButton(Color.WHITE);
			}
		}
		for(int i=0;i<line;i++)
		{
			for(int j=0;j<i+1;j++)
			{
				drawButton(Color.WHITE);
			}
			for(int j=line;j>i*2;j--)
			{
				drawButton(Color.BLACK);
			}
			for(int j=0;j<i+1;j++)
			{
				drawButton(Color.WHITE);
			}
		}
	}
	void drawButton(Color c)
	{
		JButton button=new JButton("");
		button.setBackground(c);
		add(button);
	}
}
