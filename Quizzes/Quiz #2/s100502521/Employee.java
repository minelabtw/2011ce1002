package q2.s100502521;

public class Employee extends Person
{
	protected int salary;
	public Employee(String input_name,int input_age,String input_gender,int input_salary)
	{
		super(input_name,input_age,input_gender);
		salary=input_salary;
	}
	public String toString()
	{
		return "This guy is an Employee!!";
	}
}
