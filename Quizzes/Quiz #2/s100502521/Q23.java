package q2.s100502521;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Q23 
{
	public static void main(String[] args) throws FileNotFoundException
	{
		Stack stack=new Stack();
		File inputfile=new File("Input.txt");
		Scanner input=new Scanner(inputfile);
		while(input.hasNextLine())
		{
			String line=input.nextLine();
			for(int i=0;i<line.length();i++)
			{
				if( (int)line.charAt(i)>=48 && (int)line.charAt(i)<=57 )
				{
					switch((int)line.charAt(i))
					{
					case 48:
						stack.Push(0);
						break;
					case 49:
						stack.Push(1);
						break;
					case 50:
						stack.Push(2);
						break;
					case 51:
						stack.Push(3);
						break;
					case 52:
						stack.Push(4);
						break;
					case 53:
						stack.Push(5);
						break;
					case 54:
						stack.Push(6);
						break;
					case 55:
						stack.Push(7);
						break;
					case 56:
						stack.Push(8);
						break;
					case 57:
						stack.Push(9);
						break;
					}
				}
				else
				{
					int temp=stack.Pop();
					int temp2=stack.Pop();
					if(line.charAt(i)=='+')
					{
						stack.Push(temp+temp2);
					}
					else if(line.charAt(i)=='-')
					{
						stack.Push(temp-temp2);
					}
					else if(line.charAt(i)=='*')
					{
						stack.Push(temp*temp2);
					}
					else if(line.charAt(i)=='/')
					{
						stack.Push(temp/temp2);
					}
				}
				int[] array=stack.showAll();
				System.out.print("[");
				for(int j=0;j<stack.getSize();j++)
				{
					System.out.print(array[j]+" ");
				}
				System.out.println("]");
			}
			System.out.println("Here is the result: "+stack.Pop());
		}
	}
}
