package q2.s100502521;

public class Staff extends Employee
{
	private String phoneNumber;
	public Staff(String input_name,int input_age,String input_gender,int input_salary,String input_number)
	{
		super(input_name,input_age,input_gender,input_salary);
		setInfo(input_name,input_age,input_gender,input_salary,input_number);
	}
	public void setInfo(String input_name,int input_age,String input_gender,int input_salary,String input_number)
	{
		name=input_name;
		age=input_age;
		gender=input_gender;
		salary=input_salary;
		phoneNumber=input_number;
		try
		{
			if(age<=0)
			{
				throw new Exceptions(1);
			}
			if(salary<0)
			{
				throw new Exceptions(2);
			}
			if(phoneNumber.length()!=10)
			{
				throw new Exceptions(3);
			}	
			getInfo();
		}
		catch(Exception e)
		{
			System.err.println("Initial Faild!!Please try again");
		}
	}
	public void getInfo()
	{
		System.out.println("This guy is "+name+","+age+" years old, "+gender);
		System.out.println(toString());
		System.out.println("phoneNumber is "+phoneNumber+",Salary is "+salary);
	}
	public String toString()
	{
		return super.toString()+"This guy is a staff.";
	}
	
}
