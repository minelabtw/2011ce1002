package q2.s9820903034;

public class Staff extends Employee{
	
	String phoneNumber;
	
	public Staff
	(String name, int age, String gender, int salary, String phoneNumber)
	throws ageException, salaryException, phoneException {
				
		super ();
		this.phoneNumber = "";
		setInfo (name, age, gender, salary, phoneNumber);
		String [] temp = getInfo();
		System.out.println(toString());
		for (int i = 0; i < 5; i++) {
			System.out.println(temp[i]);
		}
		
	}
	
	public Staff () throws ageException, salaryException, phoneException {
		this ("", 0, "", 0, "");
	}
	
	public String toString () {
		return (super.toString() + "This guy is a staff, phone number is" + phoneNumber + ", salary is " + salary + ".\n");
	}
	
	public void setInfo
	(String name, int age, String gender, int salary, String phoneNumber)
	throws ageException, salaryException, phoneException {
		
		this.setName(name);
		this.setAge (age);
		this.setGender(gender);
		this.setSalary(salary);
		this.setphoneNumber(phoneNumber);
		
	}
	
	public String[] getInfo () {
		String[] info = new String [5];
		info[0] = this.name + " ";
		info[1] = ""+this.age + " ";
		info[2] = this.gender + " ";
		info[3] = ""+this.salary + " ";
		info[4] = this.phoneNumber + " ";
		return info;
	}
	public void setName (String name) {
		this.name = name;
	}
	
	public void setAge (int age) throws ageException {
		if (age < 0) throw new ageException ();
		else this.age = age;
	}
	
	public void setGender (String gender) {
		this.gender = gender;
	}
	
	public void setSalary (int salary) throws salaryException {
		if (salary < 0) throw new salaryException ();
		else this.salary = salary;
	}
	
	public void setphoneNumber (String phoneNumber) throws phoneException {
		if (phoneNumber.length() < 10) throw new phoneException ();
		else this.phoneNumber = phoneNumber;
	}
	
	
}
