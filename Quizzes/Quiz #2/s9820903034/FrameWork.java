package q2.s9820903034;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;

public class FrameWork extends JFrame {
	
	static final Color black = new Color (0,0,0);
	static final Color white = new Color (255,255,255);
	
	public FrameWork (int size) {
		
		if (size%2==0) size--;
		if (size < 0) size*=(-1);
		this.setLayout(new GridLayout (size, size));
		
		for (int i = 0; i < size; i++) {
			
			// draw the first white part
			for (int l = 0; l < (size-i)/2; l++) {
				this.add(drawButton (white));
			}
			// draw the second black part
			for (int c = 0; c < 1+2*i; c++) {
				this.add(drawButton (black));
			}
			// draw the third white part
			for (int r = 0; r < (size-i)/2; r++) {
				this.add(drawButton (white));
			}
			
		}
			
	}

	private Component drawButton(Color color) {
		JButton button = new JButton();
		button.setBackground(color);
		return button;
	}
}
