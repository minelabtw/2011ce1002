package q2.s9820903034;

public class Person {
	String name;
	int age;
	String gender;
	
	public Person (String name, int age, String gender) {
		this.name = name;
		this.age = age;
		this.gender = gender;
	}
	
	public Person () {
		this("", 0, "");
	}
	
	public String toString () {
		return "This guy is" + name + ", " + age + " years old, " + gender + ".\n";
	}
	
}
