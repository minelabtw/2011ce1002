package q2.s9820903034;

public class Manager extends Employee {

	String title;
	
	public Manager
	(String name, int age, String gender, int salary, String title)
	throws ageException, salaryException {
		
		super ();
		this.title = "";
		setInfo (name, age, gender, salary, title);
		String [] temp = getInfo();
		System.out.println(toString());
		for (int i = 0; i < 5; i++) {
			System.out.println(temp[i]);
		}
		
	}
	
	public Manager () throws ageException, salaryException {
		this ("", 0, "", 0, "");
	}
	
	public String toString () {
		return (super.toString() + "This guy is a manager, title is" + title + ", salary is " + salary + ".\n");
	}
	
	public void setInfo
	(String name, int age, String gender, int salary, String title)
	throws ageException, salaryException {
		
		this.setName(name);
		this.setAge (age);
		this.setGender(gender);
		this.setSalary(salary);
		this.setTitle(title);
		
	}
	
	public String[] getInfo () {
		String[] info = new String [5];
		info[0] = this.name + " ";
		info[1] = ""+this.age + " ";
		info[2] = this.gender + " ";
		info[3] = ""+this.salary + " ";
		info[4] = this.title + " ";
		return info;
	}
	public void setName (String name) {
		this.name = name;
	}
	
	public void setAge (int age) throws ageException {
		if (age < 0) throw new ageException ();
		else this.age = age;
	}
	
	public void setGender (String gender) {
		this.gender = gender;
	}
	
	public void setSalary (int salary) throws salaryException {
		if (salary < 0) throw new salaryException ();
		else this.salary = salary;
	}
	
	public void setTitle (String title) {
		this.title = title;
	}
	
}
