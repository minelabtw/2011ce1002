package q2.s9820903034;

import javax.swing.JOptionPane;

public class Q22 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String input = "";
		int select = 0;
		while (true) {
			
			input = JOptionPane.showInputDialog(null, "Please choose the employee you want to create:\n1. Manager\n2. Staff\n 3. Exit");
			select = toInt (input);
			
			String name;
			int age;
			int salary;
			String title;
			
			switch (select) {
				
			
			case 1:
				input = JOptionPane.showInputDialog(null, "Please input name");
				name = input;
				input = JOptionPane.showInputDialog(null, "Please input age");
				age = toInt (input);
				input = JOptionPane.showInputDialog(null, "Please input gender");
				String gender = input;
				input = JOptionPane.showInputDialog(null, "Please input salary");
				salary = toInt (input);
				input = JOptionPane.showInputDialog(null, "Please input title");
				title = input;
				
				try {
					Manager manager = new Manager (name, age, gender, salary, title);
					String[] info = manager.getInfo();
					String combine = "";
					for (int i =0; i < 5; i++) combine += info[i];
					JOptionPane.showMessageDialog (null, manager.toString() + "\n" + combine);
				} catch (ageException e) {
					JOptionPane.showMessageDialog (null, "Age cannot be negative");
				} catch (salaryException e) {
					JOptionPane.showMessageDialog (null, "Salary cannot be negative");
				}
				break;
				
			case 2:
				input = JOptionPane.showInputDialog(null, "Please input name");
				name = input;
				input = JOptionPane.showInputDialog(null, "Please input age");
				age = toInt (input);
				input = JOptionPane.showInputDialog(null, "Please input gender");
				gender = input;
				input = JOptionPane.showInputDialog(null, "Please input salary");
				salary = toInt (input);
				input = JOptionPane.showInputDialog(null, "Please input phone number");
				title = input;
				
				try {
					Staff staff = new Staff (name, age, gender, salary, title);
					String[] info = staff.getInfo();
					String combine = "";
					for (int i =0; i < 5; i++) combine += info[i];
					JOptionPane.showMessageDialog (null, staff.toString() + "\n" + combine);
				} catch (ageException e) {
					JOptionPane.showMessageDialog (null, "Age cannot be negative");
				} catch (salaryException e) {
					JOptionPane.showMessageDialog (null, "Salary cannot be negative");
				} catch (phoneException e) {
					JOptionPane.showMessageDialog (null, "Phone number cannot be shroter than 10 digits");
				}
				break;
				
			case 3:
				JOptionPane.showMessageDialog (null, "Bye bye!");
				System.exit(0);
				break;
				
				default: break;
			}
			
		}

	}
	
	public static int toInt (String input) {
		
		if (input == null) return 0;
		
		int res;
		try	{
			res = Integer.parseInt(input);
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Input is not an integer!");
			res = -1;
		}
		return res;
	}

}
