package q2.s9820903034;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Q23 {
	
	public static void main(String[] args) throws IOException {
		
		String input;
		BufferedReader br = new BufferedReader (new FileReader ("input.txt"));
		input = br.readLine();
		while (input != null) {
			int res = postorder (input);
			System.out.println ("Result is " + res);
			input = br.readLine();
		}
		

	}

	private static int postorder(String input) {
		
		Stack stack = new Stack ();
		
		for (int i =0; i < input.length(); i++) {
			
			stack.showAll();
			
			if (Character.isDigit(input.charAt(i))) {
				int n = (int)input.charAt(i) - 48;
				stack.push(n);
			}
			
			else {
				char operator = input.charAt(i);
				int op1 = stack.pop();
				int op2 = stack.pop();
				switch (operator) {
				case '+': op1+=op2;
				break;
				case '-': op1-=op2;
				break;
				case '*': op1*=op2;
				break;
				case '/':
					if (op2 == 0) {
						System.err.println("Division by zero!");
						op1 = 0;
					}
					else op1/=2;
					break;
				}
							
				stack.push(op1);
				
			} // else
						
		} // for
		
		stack.showAll();
		return stack.pop();
		
	} // postorder
	
}
