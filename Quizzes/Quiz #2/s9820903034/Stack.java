package q2.s9820903034;

public class Stack {
	
	static final int size = 5;
	int [] element;
	int index_stack;
	
	public Stack () {
		element = new int[size];
		for (int i = 0; i < size; i++) element[i]=0;
		index_stack = 0;
	}
	public void push (int value) {
		if (isFull());
		else {
			element[index_stack] = value;
			index_stack++;
		}
	}
	
	public int pop () {
		if (isEmpty()) return (Integer) null;
		else {
			index_stack --;
			return element [index_stack]; 
		}
	}

	boolean isFull() {
		if (index_stack == size) return true;
		else return false;
	}
	
	boolean isEmpty() {
		if (index_stack == 0) return true;
		else return false;
	}
	
	public void showAll () {
		String res = "";
		if (isEmpty()) res =  "[ ]";
		else {
			for (int i = 0; i < index_stack-1; i++) {
				res += "[";
				res += element[i];
				res += "],";
			}
			res += "[";
			res += element[index_stack-1];
			res += "]";
		}
		System.out.println(res);
	}

}
