package q2.s9820903034;

public class Employee extends Person {

	int salary;
	Employee (String name, int age, String gender, int salary) {
		super (name, age, gender);
		this.salary = salary;
	}
	
	Employee () {
		this("", 0, "", 0);
	}
	
	public String toString () {
		return (super.toString() + "This guy is an employee.\n");
	}
	
	
}
