package q2.s100502506;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class FrameWork extends JFrame
{
	public FrameWork()
	{
		int max;
		String questionString =JOptionPane.showInputDialog("Enter number");
		max=Integer.parseInt(questionString);
		setLayout(new GridLayout(max,max,0,0));
		
		for(int i=0;i<=max;i++)
		{
			for(int z=max;z>i;z--)
				drawButton(Color.WHITE);
			for(int j=-1;j<i;j++)
				drawButton(Color.BLACK);
			for(int j=0;j<i;j++)
				drawButton(Color.BLACK);
			for(int z=max/2;z>i;z--)
				drawButton(Color.WHITE);
		}
		for(int i=0;i<=max;i++)
		{
			for(int j=0;j<i;j++)
				drawButton(Color.WHITE);
			for(int j=max;j>i-1;j--)
				drawButton(Color.BLACK);
			for(int j=max/2;j>i;j--)
				drawButton(Color.BLACK);
			for(int j=0;j<i;j++)
				drawButton(Color.WHITE);
		}
	}
	public void drawButton(Color c)
	{
		JButton button =new JButton();
		button.setBackground(c);
		add(button);
	}
}
