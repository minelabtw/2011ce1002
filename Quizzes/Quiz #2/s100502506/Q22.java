package q2.s100502506;
import javax.swing.*;
import java.util.Scanner;

public class Q22 
{
	public static void main(String argc[])//main
	{
		int question=0;
		int age;
		String genderString;
		int salary;
		String titleString;
		String phoneString;
		String namesString;
		Scanner inputScanner = new Scanner(System.in);
		while(true)
		{
			
			System.out.println("Please choose the employee you want to create:1.Manager 2.Staff 3.Exit");
			question=inputScanner.nextInt();
			if(question==1)// 1 manager
			{
				
					System.out.print("Please input name,age,gender,salary,title");
					namesString=inputScanner.next();
					age=inputScanner.nextInt();
					genderString=inputScanner.next();
					salary=inputScanner.nextInt();
					titleString=inputScanner.next();
					
				try
				{
					Manager manager1=new Manager(namesString,age,genderString,salary,titleString);
					
					System.out.println("This guy is "+manager1.getName()+", "+manager1.getAge()+" years old, "+manager1.getGender());
					System.out.println(manager1.toString());
					System.out.println("Title is "+manager1.getInfo()+", Salary is "+manager1.getSalary());
				}
				catch (Exception e) 
				{
					System.err.println("Initial Failed!! Please try again!!");
				}
			}
			else if(question==2) //2 staff
			{
				System.out.print("Please input name,age,gender,salary,number");
				namesString=inputScanner.next();
				age=inputScanner.nextInt();
				genderString=inputScanner.next();
				salary=inputScanner.nextInt();
				phoneString=inputScanner.next();
				try
				{
					Staff staff1 =new Staff(namesString, age, genderString, salary, phoneString);
					System.out.println("This guy is "+staff1.getName()+", "+staff1.getAge()+" years old, "+staff1.getGender());
					System.out.println(staff1.toString());
					System.out.println("Phone number is "+staff1.getInfo()+", Salary is"+staff1.getSalary());
				}
				catch (Exception e) 
				{
					System.err.println("Initial Failed!! Please try again!!");
				}
			}
			else if(question==3)// 3exit
			{
				System.exit(0);
			}
			else
			{
				System.out.println("���s��J");
			}
		}
			
	
	}
}
