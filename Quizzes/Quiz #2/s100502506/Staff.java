package q2.s100502506;


public class Staff extends Employee// staff inherit employee
{
	private String phoneNumber;
	public Staff(String inputname, int inputage, String inputgender,int inputsalary,String input) throws phoneException, salaryException, ageException
	{
		super(inputname, inputage, inputgender, inputsalary);
		if(input.length()<=10)
			throw new phoneException();
		setInfo(input);
	}
	public String toString()
	{
		return super.toString()+"This guy is a Staff";
	}
	public void setInfo(String input)
	{
		phoneNumber=input;
	}
	public String getInfo()
	{
		return phoneNumber;
	}
}
