package q2.s100502506;

public class Manager extends Employee // manager inherit employee
{
	private String title;
	public Manager(String inputname, int inputage, String inputgender,int inputsalary,String inputtitle) throws salaryException, ageException
	{
		super(inputname, inputage, inputgender, inputsalary);
		setInfo(inputtitle);
	}
	public String toString() // is 
	{
		return super.toString()+"This guy is an Manager!";
	}
	public void setInfo(String arguments)
	{
		title=arguments;
	}
	public String getInfo()
	{
		return title;
	}
}
