package q2.s100502506;

public class Employee extends Person //employee inheit person
{
	private int salary;
	public Employee(String inputname, int inputage, String inputgender,int inputsalary) throws salaryException, ageException 
	{
		super(inputname, inputage, inputgender);
		if(inputsalary<0)
			throw new salaryException();
		setSalary(inputsalary);
	}
	
	public String toString()
	{
		return "This guy is an Employee!";
	}
	public void setSalary(int input)
	{		
		salary=input;
	}
	public int getSalary()
	{
		return salary;
	}
}
