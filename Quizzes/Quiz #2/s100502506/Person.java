package q2.s100502506;



public class Person  //person
{
	private String name;
	private int age;
	private String gender;
	public Person(String inputname,int inputage,String inputgender) throws ageException
	{
		if(inputage<=0)
			throw new ageException();
		setName(inputname);
		setAge(inputage);
		setGender(inputgender);
	}
	public String toString() // is person
	{
		return "This guy is Person!";
	}
	public void setName(String input)
	{
		name=input;
	}
	public void setAge(int input)
	{
		age=input;
	}
	public void setGender(String input)
	{
		gender=input;
	}
	public String getName()
	{
		return name;
	}
	public int getAge()
	{
		return age;
	}
	public String getGender()
	{
		return gender;
	}
	
}
