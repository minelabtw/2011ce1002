package q2.s100502505;

import java.util.Scanner;

public class Q22 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out
				.println("Please choose the employee you want to create:1.Manager 2.Staff 3.Exit");
		int userChoice = input.nextInt();

		switch (userChoice) {
		case 1:
			System.out.println("Please input name,age,gender,salary,title");
			Manager manager = new Manager(input.next(), input.nextInt(),
					input.next(), input.nextInt(), input.next());
			System.out.println("The guy is " + manager.getName() + " " + manager.getAge() + " years old " + manager.getGender() );
			System.out.println(manager.toString());
			System.out.println("Title is " + manager.getTitle() + " Salary is "
					+ manager.getSalary());
		case 2:
			System.out.println("Please input name,age,gender,salary,phonenumber");
			Staff staff = new Staff(input.next(), input.nextInt(),
					input.next(), input.nextInt(), input.next());
			System.out.println("The guy is " + staff.getName() + " " + staff.getAge() + " years old " + staff.getGender());
			System.out.println(staff.toString());
			System.out.println("Salary is " + staff.getSalary());
			System.out.println("The phone number is " + staff.getphoneNumber());
		}
	}
}
