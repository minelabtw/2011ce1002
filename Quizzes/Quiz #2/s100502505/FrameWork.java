package q2.s100502505;
import java.awt.*;

import javax.swing.*;
import javax.swing.border.*;

public class FrameWork {
	public static void main(String args[]) {
		String input = JOptionPane.showInputDialog(null,"請輸入一個奇數:");
		FrameWork(Integer.parseInt(input));
	}
	
	public static void FrameWork(int number)
	{
		JFrame frame = new JFrame();//frame
		JPanel p1 = new JPanel(new FlowLayout(FlowLayout.LEFT, 2, 2));//板子
		p1.setLayout(new GridLayout(number,number));//棋盤
		for (int i = 0; i < number; i++) 
		{
			for (int j = 0; j < number; j++) 
			{
				if(j == number/2 || i == number/2)
				{
					JButton Black = new JButton("");
					Black.setBackground(Color.BLACK);
					p1.add(Black);
				}else{
					JButton White = new JButton("");
					White.setBackground(Color.WHITE);
					p1.add(White);
				}
			}
		}
		frame.add(p1);//加到frame裡面
		frame.setSize(400, 300);//設定FRAME的SIZE
		frame.setTitle("Q2-1");
		frame.setVisible(true);//讓FRAME能看見
	}
}

