package q2.s100502505;

public class Person {
	Person(String name,int age ,String gender)
	{
		Name = name;
		Age = age;
		Gender = gender;
	}
	
	public String getName()
	{
		return Name;
	}
	
	public int getAge()
	{
		return Age;
	}
	
	public String getGender()
	{
		return Gender;
	}
	
	public String toString()
	{
		return "This guy is a person!";
	}
	
	protected String Name;
	protected int Age;
	protected String Gender;
}
