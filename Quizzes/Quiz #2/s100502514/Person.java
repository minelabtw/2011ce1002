package q2.s100502514;

public class Person {
	private String name;
	private int age;
	private boolean isMale;
	
	public Person(String name, int age, String gender) throws GenderException, AgeException{
		setName(name);
		setAge(age);
		setGender(gender);
	}
	public Person() throws GenderException, AgeException{
		this("Guest", 18, "male");
	}
	
	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		/** Create a new String to prevent usage of getName() = "A new string"; */
		return new String(name);
	}
	
	public void setAge(int age) throws AgeException{
		if(age<=0)throw new AgeException();
		this.age = age;
	}
	public int getAge(){
		return age;
	}
	
	public void setGender(String gender) throws GenderException{
		String lowered_gender = gender.toLowerCase();
		
		//Analyze and determine the gender
		if(lowered_gender.equals("male") || lowered_gender.equals("guy")
				 || lowered_gender.equals("man") || lowered_gender.equals("boy")){
			isMale = true;
		}else if(lowered_gender.equals("female") || lowered_gender.equals("lady")
				 || lowered_gender.equals("woman") || lowered_gender.equals("girl")){
			isMale = false;
		}else{
			throw new GenderException();
		}
	}
	public String getGender(){
		return isMale ? "male" : "female";
	}
	
	public String toString(){
		return "This "+howToCall()+" is "+getName()+", "+getAge()+"years old, "+getGender()+".";
	}
	
	//Determine either "This guy..." or "This lady..." will be shown.
	protected String howToCall(){
		return isMale ? "guy" : "lady";
	}
}
