package q2.s100502514;

import java.util.Scanner;

public class Q22 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		
		boolean continuing = true;
		
		/** Input fields */
		String name="";
		int age=0;
		String gender="";
		int salary=0;
		String title_or_phone="";
		
		do{
			try{
				System.out.println("Please choose the employee:\n" +
						"1. Manager\t2. Staff  \t3.Exit");
				int choose = input.nextInt();
				Employee somebody = new Employee();
				switch(choose){
				case 1:
					System.out.println("Please input name, age, gender, salary, title:");
					input_values(input, name, age, gender, salary, title_or_phone);
					somebody = new Manager();
					break;
				case 2:
					System.out.println("Please input name, age, gender, salary, phone:");
					input_values(input, name, age, gender, salary, title_or_phone);
					somebody = new Staff();
					break;
				case 3:
					continuing = false; //exit
					break;
				default:
					throw new WrongChoiceException(); //Input an invalid choose
				}
				
				System.out.println(somebody.toString());
				
			}catch(AgeException err){
				System.out.println("Invalid Age!!");
			}catch(GenderException err){
				System.out.println("Invalid Gender!!");
			}catch(PhoneException err){
				System.out.println("Invalid Phone!!");
			}catch(SalaryException err){
				System.out.println("Invalid Salary!!");
			}catch(WrongChoiceException input_err){
				System.out.println("Invalid Chosen Number!!");
			}
			
			
			if(!continuing){
				break;
			}
		}while(input.hasNext());
		System.out.println("See you next time!!");
	}
	
	public static void input_values(Scanner input,
			String name, int age, String gender, int salary, String title_or_phone){
		name = input.next();
		age = input.nextInt();
		gender = input.next();
		salary = input.nextInt();
		title_or_phone = input.next();
	}
}
