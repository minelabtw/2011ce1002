package q2.s100502514;

public class Employee extends Person {
	
	private int salary;
	
	public Employee(int salary) throws GenderException, AgeException, SalaryException {
		setSalary(salary);
	}
	public Employee(String name, int age, String gender, int salary)
			throws GenderException, AgeException, SalaryException {
		super(name, age, gender);
		setSalary(salary);
	}
	public Employee() throws GenderException, AgeException, SalaryException{
		this(0);
	}
	
	public void setSalary(int salary) throws SalaryException{
		if(salary<0)throw new SalaryException();
		this.salary = salary;
	}
	public int getSalary(){
		return salary;
	}
	
	public String toString(){
		return super.toString()+"\nThis "+howToCall()+" is an Employee.";
	}
}
