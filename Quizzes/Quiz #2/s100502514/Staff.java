package q2.s100502514;

public class Staff extends Employee {
	
	private String phoneNumber;
	
	public Staff(String name, int age, String gender, int salary, String phone)
			throws GenderException, AgeException, SalaryException, PhoneException {
		super(name, age, gender, salary);
		setPhoneNumber(phone);
	}
	public Staff(String phone)
			throws GenderException, AgeException, SalaryException, PhoneException {
		setPhoneNumber(phone);
	}
	public Staff() throws GenderException, AgeException, SalaryException, PhoneException{
		this("Manager");
	}
	
	public void setPhoneNumber(String num) throws PhoneException{
		if(num.length()<10)throw new PhoneException();
		phoneNumber = num;
	}
	public String getPhoneNumber(){
		return phoneNumber;
	}
	
	public void setInfo(String name, int age, String gender, int salary, String phone)
			throws GenderException, AgeException, SalaryException, PhoneException {
		setName(name);
		setAge(age);
		setGender(gender);
		setSalary(salary);
		setPhoneNumber(phone);
	}
	
	public String toString(){
		return super.toString()+"\nThis "+howToCall()+" is a Staff.\nPhone Number is "+getPhoneNumber()+
				", Salary is "+getSalary();
	}
	
}
