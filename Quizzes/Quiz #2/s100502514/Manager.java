package q2.s100502514;

public class Manager extends Employee {
	
	private String title;
	
	public Manager(String name, int age, String gender, int salary, String title)
			throws GenderException, AgeException, SalaryException {
		setInfo(name, age, gender, salary, title);
	}
	public Manager(String title)
			throws GenderException, AgeException, SalaryException {
		setTitle(title);
	}
	public Manager() throws GenderException, AgeException, SalaryException{
		this("Manager");
	}
	
	public void setTitle(String title){
		this.title = title;
	}
	public String getTitle(){
		return new String(title);
	}
	
	public void setInfo(String name, int age, String gender, int salary, String title)
			throws GenderException, AgeException, SalaryException {
		setName(name);
		setAge(age);
		setGender(gender);
		setSalary(salary);
		setTitle(title);
	}
	
	public String toString(){
		return super.toString()+"\nThis "+howToCall()+" is a Manager.\nTitle is "+getTitle()+
				", Salary is "+getSalary();
	}
	
}
