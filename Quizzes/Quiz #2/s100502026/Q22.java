package q2.s100502026;

import java.util.Scanner;

public class Q22 {
	
	public static void main(String[] args)
	{
		boolean exit=true;
		int choose;
		String line1;
		String line2;
		String line3;
		int number1;
		int number2;
		Scanner input=new Scanner(System.in);
		
		while(exit){
			System.out.println("Please choose the employee you want to create: 1.Manager 2.Staff 3.Exit");
			choose=input.nextInt();
			switch(choose)
			{
				case 1:
					System.out.println("Please input title,salary,name,age,gender one by one.");
					line1=input.nextLine();
					number1=input.nextInt();
					line2=input.nextLine();
					number2=input.nextInt();
					line3=input.nextLine();
					Manager manager=new Manager(line1,number1,line2,number2,line3);
					break;
					
				case 2:
					System.out.println("Please input title,salary,name,age,gender one by one.");
					line1=input.nextLine();
					number1=input.nextInt();
					line2=input.nextLine();
					number2=input.nextInt();
					line3=input.nextLine();
					Staff staff=new Staff(line1,number1,line2,number2,line3);
					break;

				case 3:
					exit=false;
					break;
			}
		}
	}
	
}
