package q2.s100502026;

import java.util.ArrayList;

public class Stack {
	private ArrayList list=new ArrayList();
	private int indexStack;
	
	Stack()
	{
	}
	
	public void push(int value)
	{
		list.add(value);
	}
	
	public int pop()
	{
		int temp= (int) list.get(list.size()-1);
		list.remove(list.size()-1);
		return temp;
	}
	
	public boolean isFull()
	{
		if(list.size()<5)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public boolean isEmpty()
	{
		return list.isEmpty();
	}
	
	public void showAll()
	{
		for(int i=0;i<list.size();i++)
		{
			System.out.print(list.get(i)+" ");
		}
	}
	
}
