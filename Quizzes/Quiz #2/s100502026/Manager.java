package q2.s100502026;

public class Manager extends Employee {
	
	Manager(String line1,int money,String line2,int year,String line3)
	{
		setInfo(line1,money,line2,year,line3);
		getInfo();
	}
	
	private String title;
	private int salary;
	private String name;
	private int age;
	private String gender;
	
	public void setInfo(String line1,int money,String line2,int year,String line3)
	{
		setTitle(line1);
		setSalary(money);
		setName(line2);
		setAge(year);
		setGender(line3);
	}
	
	public void getInfo()
	{
		System.out.println("Here's "+name+","+age+"years old"+","+gender);
		this.toString();
		System.out.println("Title is "+title+"with salary,"+salary);
	}
	
	public void setName(String line)
	{
		name=line;
	}
	
	public void setAge(int year)
	{
		age=year;
	}
	
	public void setGender(String line)
	{
		gender=line;
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getAge()
	{
		return age;
	}
	
	public String getGender()
	{
		return gender;
	}
	
	public void setSalary(int money)
	{
		salary=money;
	}
	
	public int getSalary()
	{
		return salary;
	}

	public void setTitle(String line)
	{
		title=line;
	}
	
	private String getTitle()
	{
		return title;
	}
	
	public String toString()
	{
		return super.toString()+"\n"+name+" is a manager.";
	}
}
