package q2.s100502026;

public class Person {
	
	private String name;
	private int age;
	private String gender;
	
	public void setName(String line)
	{
		name=line;
	}
	
	public void setAge(int year)
	{
		age=year;
	}
	
	public void setGender(String line)
	{
		gender=line;
	}
	
	public String getName()
	{
		return name;
	}
	
	public int getAge()
	{
		return age;
	}
	
	public String getGender()
	{
		return gender;
	}
	
	public String toString()
	{
		return name+" is a person.";
	}

}
