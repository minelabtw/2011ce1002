package q2.s100502008;

public class Staff extends Employee {
	protected String phonenumber;

	public Staff(String inname, int inage, String ingender, int insalary,//constructor
			String inphonenumber) throws ageException, phoneException {
		setInfo(inname, inage, ingender, insalary, inphonenumber);//initialize
	}

	public String toString() {//override
		return "this guy is a Staff, " + super.toString();
	}

	public void setInfo(String inname, int inage, String ingender,
			int insalary, String inphonenumber) throws ageException, phoneException {
		name = inname;
		if (inage <= 0) {//check
			throw new ageException();
		} else {
			age = inage;
		}
		gender = ingender;
		salary = insalary;
		if (inphonenumber.length() < 10) {//check
			throw new phoneException();
		} else {
			phonenumber = inphonenumber;

		}
	}

	public String getInfo() {//show
		return "this guy is " + name + ", " + age + " years old, " + gender
				+ "\n" + "Phonenumber is " + phonenumber
				+ ", Salary is " + salary;
	}
}
