package q2.s100502008;

public class Manager extends Employee {
	protected String title;

	public Manager(String inname, int inage, String ingender, int insalary,//constructor
			String intitle) throws ageException, salaryException {
		setInfo(inname, inage, ingender, insalary, intitle);//initialize
	}

	public String toString() {//override
		return  super.toString()+", this guy is an Manager";
	}

	public void setInfo(String inname, int inage, String ingender,
			int insalary, String intitle) throws ageException, salaryException {
		name = inname;
		if (inage <= 0) {//check
			throw new ageException();
		} else {
			age = inage;
		}
		gender = ingender;
		if (insalary < 0) {//check
			throw new salaryException();
		} else {
			salary = insalary;
		}
		title = intitle;
	}

	public String getInfo() {//show
		return "this guy is " + name + ", " + age + " years old, " + gender
				+ "\n" + "Title is " + title
				+ ", Salary is " + salary;
	}
}
