package q2.s992008002;

public class Manager extends Employee{

	private String title;
	
	
	public void Manager(){
		
	}
	
	public Manager(String Name,int Age,String Gender,int Salary,String Title){
		super(Name, Age, Gender, Salary);
		title = Title;
	}
	public String toString(){
		return String.format("The guy is an Employee!!This guy is a Manager!!");
	}
	
	public String getTitle(){
		return title;
	}
	
	
	public String getInfo(){
		return("The guy name is " + getName()+", "+getAge()+"years, "+getGender()+"\n"+
		toString()+"\nTitle is: " + getTitle()+"Salary is "+getSalary());
	}
	
	
	public void setInfo(int age,int salary)throws ageException,salaryException{
		
		if(age<=0)throw new ageException();
		
		else if(salary <=0)throw new salaryException();
		
	}
	
}
