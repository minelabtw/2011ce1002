package q2.s992008002;

public class Staff extends Employee{

	private String phoneNumber;
	
	
	public void Staff(){
		
	}
	
	public Staff(String Name,int Age,String Gender,int Salary, String PhoneNumber){
		
		super(Name, Age, Gender, Salary);
		phoneNumber = PhoneNumber;
	}
	
	public String getPhoneNumber(){
		return phoneNumber;
	}

	public String toString()
	{
		return String.format("This guy is an Employee!!This guy is a Staff!!");
	}
	
	public String getInfo(){
		return("The guy name is " + getName()+", "+getAge()+"years, "+getGender()+"\n"+
		toString()+"\nPhoneNumber is: " + getPhoneNumber()+"Salary is "+getSalary());
	}
}
