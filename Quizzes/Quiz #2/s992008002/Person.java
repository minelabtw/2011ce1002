package q2.s992008002;

public class Person {

	int age;
	String name;
	String gender;
	
	public Person(String Name, int Age, String Gender){
		name = Name;
		gender = Gender;		
		age = Age;
	}
	


	//回傳名字
	public String getName(){
		return name;
	}
	
	//回傳性別
	public String getGender(){
		return gender;
	}
	
	//回傳年齡
	public int getAge(){
		return age;
	}
	
	
	
	
	public String toString(){
		return String.format("This guy is a Person!!");
	}
	
}
