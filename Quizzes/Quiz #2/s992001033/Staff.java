package q2.s992001033;

public class Staff extends Employee
{
	String phoneNumber = "";
	public Staff(String name, int age,String gender,int salary,String phoneNumber)
	{
		 setInfo(name,age,gender,salary,phoneNumber);
		 getInfo();
	}
	public String toString()
	{
		super.toString();
		return "this guy is a Staff";
	}
	public void setInfo(String name, int age,String gender,int salary,String phoneNumber)
	{
		super.name = name;
		super.age = age;
		super.gender = gender;
		super.salary = salary;
		this.phoneNumber = phoneNumber;
	}
	public void getInfo()
	{
		System.out.println("This guy is "+super.name+", "+super.age+" years old"+", "+super.gender);
		toString();
		System.out.println("The phoneNumber is "+phoneNumber+", Salary is"+super.salary);
	}
}
