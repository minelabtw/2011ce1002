package q2.s992001033;

import javax.swing.*;
import java.awt.*;
public class FrameWork extends JFrame
{
	Color black_button = new Color(255, 255, 255);
	Color white_button = new Color(0, 0, 0);
	public FrameWork(int length)
	{
		setLayout(new GridLayout(length,length));
		for(int i=0;i<length;i++)
		{
			for(int j=0;j<length;j++)
			{
				if(j<(length+1-i)/2||j>(length+1-i)/2)
					drawButton(white_button);
				else
					drawButton(black_button);
			}
		}
		JButton black = new JButton("");
		black.setBackground(Color.black);
		add(black);
		
	}
	public void drawButton(Color c)
	{
		JButton button = new JButton("");
		button.setBackground(c);
		add(button);
	}
}
