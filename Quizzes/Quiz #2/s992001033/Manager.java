package q2.s992001033;

public class Manager extends Employee
{
	String title = "";
	public Manager(String name, int age,String gender,int salary,String title)
	{
		 setInfo(name,age,gender,salary,title);
		 getInfo();
	}
	public String toString()
	{
		super.toString();
		return "this guy is an Manager";
	}
	public void setInfo(String name, int age,String gender,int salary,String title)
	{
		super.name = name;
		super.age = age;
		super.gender = gender;
		super.salary = salary;
		this.title = title;
	}
	public void getInfo()
	{
		System.out.println("This guy is "+super.name+", "+super.age+" years old"+", "+super.gender);
		this.toString();
		System.out.println("The title is "+title+", Salary is "+super.salary);
	}
}
