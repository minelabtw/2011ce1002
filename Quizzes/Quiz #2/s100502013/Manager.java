package q2.s100502013;

public class Manager extends Employee{
	private String title;
	
	Manager(String a,int b,String c,int d,String e){
		setInfo(a,b,c,d,e);
	}
	
	public String toString(){ //output identity
		return super.toString() + "This guy is a manager!!";
	}
	
	public void setInfo(String a,int b,String c,int d,String e){ //set details
		name = a;
		age = b;
		gender = c;
		salary = d;
		title = e;
	}
	
	public void getInfo() throws ageException,salaryException,phoneException{ //throws exceptions and output result
		int exflag=0; //use to count error number
		try{
			if(age<=0){
				exflag++;
				throw new ageException();
			}
		}
		catch(ageException ex){
			System.out.println(ex.checking());
		}
		try{
			if(salary<0){
				exflag++;
				throw new salaryException();
			}
		}
		catch(salaryException ex){
			System.out.println(ex.checking());
		}
		if(exflag==0){ //no error. output result
			System.out.println("This guy is " + name + "," + age + " years old, " + gender);
			System.out.println(toString());
			System.out.println("Title is " + title + ", Salary is " + salary);
		}
		else //some error. output error message
			System.out.println("Initial failed!!Please try again!!");
	}
}
