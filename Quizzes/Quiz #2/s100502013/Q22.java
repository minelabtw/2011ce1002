package q2.s100502013;
import java.util.Scanner;

public class Q22 {
	public static void main(String[] args) throws ageException, salaryException, phoneException{
		Scanner input = new Scanner(System.in);
		int choose = 1; //use to choose function
		String myname;
		int myage;
		String mygender;
		int mysalary;
		String mytitle;
		String myphone;
		boolean exit=true; //use to decide exiting
		while(exit){
			System.out.println("Please choose the employee you want to create:1.Manager 2.Staff 3.Exit");
			choose = input.nextInt();
			switch(choose){
				case 1: //function: Manager
					System.out.println("Please input name, age, gender, salary, title");
					myname = input.next();
					myage = input.nextInt();
					mygender = input.next();
					mysalary = input.nextInt();
					mytitle = input.next();
					Manager mymanager = new Manager(myname, myage, mygender, mysalary, mytitle);
					mymanager.getInfo();
					break;
					
				case 2: //function: Staff
					System.out.println("Please input name, age, gender, salary, phonenumber");
					myname = input.next();
					myage = input.nextInt();
					mygender = input.next();
					mysalary = input.nextInt();
					myphone = input.next();
					Staff mystaff = new Staff(myname, myage, mygender, mysalary, myphone);
					mystaff.getInfo();
					break;
					
				case 3: //function: Exit
					System.out.println("Goodbye!");
					exit = false;
					break;
					
				default: //invalid choose
					System.out.println("Invalid set!!Please try again!!");
					break;
			}
		}
	}
}
