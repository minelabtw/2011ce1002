package q2.s985003038;

import java.io.*;

public class Q23 {
	private static final String inputPath = "input.txt";
	private static final int size = 5;
	
	public static void main(String[] args) throws IOException{
		Stack stack = new Stack(size);							// create a stack with size 5
		String data = read(inputPath);							// call method to read file and return the string result
		boolean startflag = true;								// consider to start calculating a new equation
		for(int i = 0; i < data.length(); i++){					// loop whole the buffer
			if(Character.isDigit(data.charAt(i))){				// if the char is a digit
				startflag = false;
				stack.push(data.charAt(i) - 48);				// push it into the stack
				stack.showAll();								// end of round, show temporary result
			} else if(data.charAt(i) == '\r'){					// if there is a next line character, the current equation is finished
				System.out.println("Here is the result: " + stack.pop() + "\n");
				startflag = true;								// show the result and reset the starting flag
				i++;											// since the next line character is \r\n, therefore, we need to increase the counter i by 1 so we will not read the \n
			} else if(!startflag){								// if we read an operator, and also starting flag is false, that is, operator cannot be the first character of your equation
				int operant1 = stack.pop();						// pop the first two numbers
				int operant2 = stack.pop();
				if(data.charAt(i) == '+')						// and do the operation according to the operator character
					stack.push(operant1 + operant2);
				else if(data.charAt(i) == '-')
					stack.push(operant1 - operant2);
				else if(data.charAt(i) == '*')
					stack.push(operant1 * operant2);
				else if(data.charAt(i) == '/')
					stack.push(operant1 / operant2);
				stack.showAll();								// show the round result
			} else {											// otherwise, exit the program
				System.out.println("End of calculation");
				System.exit(0);
			}
		}		
	}
	
	public static String read(String inputPathName) throws IOException{
		FileReader reader = new FileReader(inputPath);			// read the file with the input file path
		StringBuffer buff = new StringBuffer();
		char[] data = new char[256];
		while(reader.read(data) > 0){							// read the file and append the data to the buffer variable
			buff.append(data);
		}
		reader.close();
		return buff.toString();									// return the data in the file
	}
}
