package q2.s985003038;

import javax.swing.*;

public class Q21 {
	public static void main(String[] args){
		String input = JOptionPane.showInputDialog(null, "Plese define the size", "Quiz 2", JOptionPane.QUESTION_MESSAGE);
		FrameWork frame = new FrameWork(Integer.valueOf(input));	// create frame with defined size
		frame.setSize(400, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);										// set its parameter and show it
	}
}
