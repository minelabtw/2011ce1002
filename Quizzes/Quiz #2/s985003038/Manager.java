package q2.s985003038;

public class Manager extends Employee {
	private String title;

	public void setInfo(String newName, int newAge, String newGender, int newSalary, String newTitle) throws ageException, salaryException{
		super.setInfo(newName, newAge, newGender, newSalary);	// call its parent setInfo method
		title = newTitle;
	}
	
	public String getInfo(){									// get info method
		return "This guy is " + name + ", " + age + " years old, " + gender + "\r\n" + toString() + "\r\n" + "Title is " + title + ", Salary is " + salary;
	}
	
	public String toString(){									// override toString method with calling its parent toString method
		return "This guy is a manager. " + super.toString();
	}
}
