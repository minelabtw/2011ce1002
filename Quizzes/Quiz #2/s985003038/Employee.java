package q2.s985003038;

public class Employee extends Person {
	protected int salary;
	
	public void setInfo(String newName, int newAge, String newGender, int newSalary) throws ageException, salaryException{
		super.setInfo(newName, newAge, newGender);		// call its parent setInfo method
		if(newSalary > 0)								// check for exception
			salary = newSalary;
		else
			throw new salaryException();
	}
	
	public String toString(){							// toString method
		return "This guy is an employee.";
	}
}
