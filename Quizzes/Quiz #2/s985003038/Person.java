package q2.s985003038;

public class Person {
	protected String name;
	protected int age;
	protected String gender;
	
	public void setInfo(String newName, int newAge, String newGender) throws ageException{
		name = newName;
		if(newAge > 0)				// check exception
			age = newAge;
		else
			throw new ageException();
		gender = newGender;
	}
	
	public String toString(){		// toString method
		return "This guy is a person.";
	}
}
