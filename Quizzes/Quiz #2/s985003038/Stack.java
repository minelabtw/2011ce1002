package q2.s985003038;

public class Stack {
	private int size;
	private int[] element;
	private int index_stack;
	
	public Stack(int newSize){				// constructor to create a stack with defined size
		size = newSize;
		element = new int[size];
		index_stack = 0;
	}
	
	public boolean isFull(){				// check if the top pointer is pointing out of the stack, that is, the stack is full
		if(index_stack >= size)
			return true;
		else
			return false;
	}
	
	public boolean isEmpty(){				// check if the top pointer is pointing to the bottom of the stack, that is, the stack is empty
		if(index_stack == 0)
			return true;
		else
			return false;
	}
	
	public void push(int value){			// push the input value into the stack, and increase the top pointer
		if(!isFull())
			element[index_stack++] = value;
		else
			System.err.println("Stack is full.");
	}
	
	public int pop(){						// pop the top element out of the stack and return it to the caller, and decrease the top pointer
		if(!isEmpty())
			return element[--index_stack];
		else {
			System.err.println("Stack is empty");
			return -1;
		}
	}
	
	public void showAll(){					// show all the elements in the stack
		System.out.print("[");
		for(int i = 0; i < index_stack; i++)
			System.out.print(element[i] + " ");
		System.out.println("]");
	}
}
