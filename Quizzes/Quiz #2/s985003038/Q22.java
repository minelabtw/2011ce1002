package q2.s985003038;

import java.util.Scanner;

public class Q22 {										// test program
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		
		while(true){
			System.out.println("Please choose the employee you want to create: 1.Manager 2.Staff 3.Exit");
			int operation = input.nextInt();			// get your choose
			if(operation == 1){
				System.out.println("Please input name, age, gender, salary, title");
				Manager employee = new Manager();		// if your choose is 1, create a manager
				try {									// and try to set its info
					employee.setInfo(input.next(), input.nextInt(), input.next(), input.nextInt(), input.next());
					System.out.println(employee.getInfo());	// if there is no exception, print its info
				} catch(Exception e){					// if there is exception, show error
					System.err.println("Initial Failed!! Please try again!!");
				}
			} else if(operation == 2){					// if your choose is 2, create a staff
				System.out.println("Please input name, age, gender, salary, phone number");
				Staff employee = new Staff();			// similarly create a staff
				try {									// try to set its info
					employee.setInfo(input.next(), input.nextInt(), input.next(), input.nextInt(), input.next());
					System.out.println(employee.getInfo());	// if there is no exception, print its info
				} catch(Exception e){					// otherwise, print error message
					System.err.println("Initial Failed!! Please try again!!");
				}
			} else if(operation == 3){					// otherwise, exit the program
				System.exit(0);
			}
		}
	}
}
