package q2.s985003038;

import java.awt.*;
import javax.swing.*;

public class FrameWork extends JFrame {
	private static final long serialVersionUID = 1L;
	
	public FrameWork(int number){
		if(number % 2 == 1){							// check for odd number
			setLayout(new GridLayout(number, number));	// set grid layout
			
			int counter = 0;
			for(int i = 0; i < number; i++){
				for(int j = 0; j < number; j++){		// if it is at the middle
					if(j < number / 2 - counter || j > number / 2 + counter)
						drawButton(Color.white);		// call drawButton with color white
					else
						drawButton(Color.black);		// call drawButton with color black
				}
				if(i < number / 2)
					counter += 1;						// increase counter at the first half
				else
					counter -= 1;						// decrease counter at the second half
			}
		} else {
			JOptionPane.showMessageDialog(null, "Input is not an odd number.", "Quiz 2", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void drawButton(Color c){
		JButton button = new JButton();					// create button
		button.setBackground(c);						// set its color
		add(button);									// add it to the frame
	}
}
