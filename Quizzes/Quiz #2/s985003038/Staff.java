package q2.s985003038;

public class Staff extends Employee {
	private String phoneNumber;
	
	public void setInfo(String newName, int newAge, String newGender, int newSalary, String newPhoneNumber) throws ageException, salaryException, phoneException{
		super.setInfo(newName, newAge, newGender, newSalary);	// call its parent setInfo method
		if(newPhoneNumber.length() >= 10)						// check for exception
			phoneNumber = newPhoneNumber;
		else
			throw new phoneException();
	}
	
	public String getInfo(){									// get info method
		return "This guy is " + name + ", " + age + " years old, " + gender + "\r\n" + toString() + "\r\n" + "Phone number is " + phoneNumber + ", Salary is " + salary;
	}
	
	public String toString(){									// override toString method with calling its parent toString method
		return "This guy is a staff. " + super.toString();
	}
}
