package q2.s100502509;

public class Manager extends Employee{
	String Etitle;
	
	public Manager(String name, int age, String gender, int salary, String title) {
		super(name, age, gender, salary);
		Etitle=title;
		// TODO Auto-generated constructor stub
	}

	public String toString(){//Function to show Message
		return super.toString()+"This guy is a Manager!!";
	}

	public String getTitle()
	{
		return Etitle; 
	}
}
