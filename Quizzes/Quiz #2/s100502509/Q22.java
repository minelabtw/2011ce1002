package q2.s100502509;
import java.util.Scanner;

public class Q22 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		boolean exitFlag = false;
		while(exitFlag == false){
			System.out.println("Please the person you want to create 1.Manager 2.Staff 3.Exit: ");			
			int userChoice = input.nextInt();
			switch(userChoice){
			
			case 1:
				// new a Employee object, and initial it
				System.out.println("Please input the name, gender, salary, title");
				//System.out.println(", and rank(1 for Professor, 2 for Associated Professor, 3 for Assitant Professor, 4 for Lecturer");
				Manager manager = new Manager( input.next(), input.nextInt(), input.next(), input.nextInt(),input.next() );
				// use methods to answer the questions
				System.out.println("This guy is"+manager.getName()+","+manager.getGender()+" Years old, "+manager.getGender());
				System.out.println(manager.toString());
				System.out.println("Title is "+manager.getTitle()+", Salary is"+manager.getSalary());
				//System.out.println("Is this guy a Employee? " + manager.toString(true));
				//System.out.println("Is this guy a Student? " + manager.toString(false));
				
				break;

			case 2:
				// new a Student object, and initial it
				System.out.println("Please input the name, age,gender,salary,phone number");
				Staff staff = new Staff( input.next(), input.nextInt(),input.next() ,input.nextInt(),input.next() );
				System.out.println("This guy is"+staff.getName()+","+staff.getGender()+" Years old, "+staff.getGender());
				// use methods to answer the questions
				System.out.println(staff.toString());
				System.out.println("Phone number is"+staff.getPhoneNumber());
			
				break;

			case 3:
				exitFlag = true;
				System.out.println("See you next week~~");
				break;

			default:
				break;



		}
		}
		
	}

}
