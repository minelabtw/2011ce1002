package q2.s100502509;

public class Staff extends Employee{
	String Phonum;
	public Staff(String name, int age, String gender, int salary, String phoneNumber) {
		super(name,age,gender,salary);
		Phonum=phoneNumber;
		
	}

	public String toString(){//Function to show Message
		return super.toString()+"This guy is a Staff!!";
	}
	
	public String getPhoneNumber(){
		return Phonum;
	}
}
