package q2.s100502520;

import java.util.Scanner;

public class Q22 {
	public void main(String[] args){
		Scanner input = new Scanner(System.in);
		boolean jud = true;
		String name;
		int age;
		String gender;
		int salary;
		String title;
		String phonenumber;
		int choose;
		Manager newManager;
		Staff newStaff;
		while(jud){
			System.out.println("Please choose the employee you want to creater: 1. Manager 2. Staff 3. Exit");
			choose = input.nextInt();
			switch(choose){
			
			//製造一個manager
			case 1:
				System.out.println("Please input name, age, gender, salary, title");
				try{
					name = input.next();
					age = input.nextInt();
					gender = input.next();
					salary = input.nextInt();
					title = input.next();
					if(age <= 0){  //檢查年齡
						throw new Exception("age cannot be 0");
					}
					
					if(salary < 0){  //檢查薪水
						throw new Exception("salary cannot be 0");
					}
					newManager = new Manager(name, age, gender, salary, title);
				}
				catch (Exception e) {  //意外處理及錯誤提示
					e.getMessage();
				}
				break;
				
			//製造一個staff
			case 2:
				System.out.println("Please input name, age, gender, salary, phonenumber");
				try{
					name = input.next();
					age = input.nextInt();
					gender = input.next();
					salary = input.nextInt();
					phonenumber = input.next();
					if(age <= 0){  //檢查年齡
						throw new Exception("age cannot be 0");
					}
					
					if(salary < 0){  //檢查薪水
						throw new Exception("salary cannot be 0");
					}
					
					if(phonenumber.length() < 0){  //檢查電話
						throw new Exception("phonenumber must over 10 number");
					}
					newStaff = new Staff(name, age, gender, salary, phonenumber);
				}
				catch (Exception e) {  //意外處理及錯誤提示
					e.getMessage();
				}
				break;
				
			//離開
			case 3:
				jud = false;
				break;
			}
		}
	}
}
