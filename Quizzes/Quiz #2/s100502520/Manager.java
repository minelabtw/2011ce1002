package q2.s100502520;

import java.util.jar.Attributes.Name;

public class Manager extends Employee{
	private String title;
	
	public Manager(String n, int a, String g, int s, String t){
		super();
		setinfo(n, a, g, s, t);
		tostring();
		getinfo();
	}
	
	public void setinfo(String inputname, int inputage, String inputgender, int inputsalary, String inputtitle){
		name = inputname;
		age = inputage;
		gender = inputgender;
		salary = inputsalary;
		title = inputtitle;
	}
	
	public void tostring(){
		System.out.println("this guy is a Manager");
		super.toString();
	}
	
	public void getinfo(){
		System.out.println("This guy is " + name + "," + age + " years oil, " + gender);
		System.out.println("Title is " + title + ",salary is " + salary);
	}
}
