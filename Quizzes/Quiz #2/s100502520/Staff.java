package q2.s100502520;

public class Staff extends Employee{
	private String phoneNumberString;
  
	public Staff(String n, int a, String g, int s, String p){
		super();
		setinfo(n, a, g, s, p);
		getinfo();
		tostring();
	}
	
	public void setinfo(String username, int userage, String usergender, int usersalary, String userphone){
		name = username;
		age = userage;
		gender = usergender;
		salary = usersalary;
		phoneNumberString = userphone;
	}
	
	@Override
	public void tostring() {
		System.out.println("this guy is a staff");
		super.toString();
	}
	
	public void getinfo(){
		System.out.println("This guy is " + name + "," + age + " years oil, " + gender);
		System.out.println("Phonenumber is " + phoneNumberString + ",salary is " + salary);
	}
	
}
