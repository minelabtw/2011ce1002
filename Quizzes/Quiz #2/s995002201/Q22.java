package q2.s995002201;

import java.util.Scanner;

public class Q22 
{
	public static void main ( String[] args)
	{
		int exitflag=1;//end
		int choose;
		String name;
		int age;
		String gender;
		int salary;
		String title;
		String phonenumber;
		Scanner input = new Scanner(System.in);
		Manager manager = new Manager();
		Staff staff = new Staff();
		while(exitflag==1)
		{
			System.out.println("Please choose the employee you want to create: 1.Manager 2.Staff 3.Exit");
			choose = input.nextInt();
			if(choose==1)
			{
				System.out.println("Please input name,age,gender,salary,title");
				name = input.next();
				age = input.nextInt();
				gender = input.next();
				salary = input.nextInt();
				title = input.next();
				
				if(age>0 && salary>=0)
				{
					System.out.println("This guy is "+name+", "+age+" years old, "+gender);
					manager.toString();
					System.out.println("This guy is an Employee!!This guy is a Manager");
					System.out.println("Title is "+title+",Salary is "+salary);
				}
				else
				{
					try
					{
						throw new Exception();
					}catch(Exception e)
					{
						if(age<=0)
						{
							System.err.println("Age can't be less than 10 digits!!");
							System.err.println("Initial Faild!!Please try again!!");
						}
						if(salary<0)
						{
							System.err.println("Salary can't be less than 10 digits!!");
							System.err.println("Initial Faild!!Please try again!!");
						}
					}
				}
			}
			if(choose==2)
			{
				System.out.println("Please input name,age,gender,salary,title");
				name = input.next();
				age = input.nextInt();
				gender = input.next();
				salary = input.nextInt();
				phonenumber = input.next();
				
				if(age>0 && salary>=0 && phonenumber.length()==10)
				{
					System.out.println("This guy is "+name+", "+age+"years old, "+gender);
					staff.toString();
					System.out.println("This guy is an Staff!!This guy is a Manager");
					System.out.println("The phone number is "+phonenumber);
				}
				else
				{
					try
					{
						throw new Exception();
					}catch(Exception e)
					{
						if(age==0)
						{
							System.err.println("Age can't be less than 10 digits!!");
							System.err.println("Initial Faild!!Please try again!!");
						}
						if(salary<0)
						{
							System.err.println("Salary can't be less than 10 digits!!");
							System.err.println("Initial Faild!!Please try again!!");
						}
						if(phonenumber.length()<10)
						{
							System.err.println("Phone number can't be less than 10 digits!!");
							System.err.println("Initial Faild!!Please try again!!");
						}
					}
				}
			}
			if(choose==3)//end
			{
				exitflag=0;
				System.out.println("goodbye");
			}
		}
	}
}
