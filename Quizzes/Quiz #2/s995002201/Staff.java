package q2.s995002201;

public class Staff extends Employee
{
	protected String phonenumber;
	protected String name;
	protected int age;
	protected String gender;
	Employee employee = new Employee();
	public String toString()
	{
		return String.format("This guy is a Staff!!" + employee.toString());
	}
	public void setInfo(String sphonenumber,String sname,String sgender,int sage,int ssalary) throws Exception
	{
		phonenumber = sphonenumber;
		name = sname;
		age = sage;
		gender = sgender;
		salary = ssalary;
		if(age==0)
		{
			System.err.println("Age can't be less than 10 digits!!");
			System.err.println("Initial Faild!!Please try again!!");
			throw new Exception();
		}
		if(salary<0)
		{
			System.err.println("Salary can't be less than 10 digits!!");
			System.err.println("Initial Faild!!Please try again!!");
			throw new Exception();
		}
		if(phonenumber.length()<10)
		{
			System.err.println("Phone number can't be less than 10 digits!!");
			System.err.println("Initial Faild!!Please try again!!");
			throw new Exception();
		}
	}
	public String getInfo()
	{
		return phonenumber+name+age+gender+salary;
	}
}
