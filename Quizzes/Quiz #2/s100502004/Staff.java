package q2.s100502004;

public class Staff extends Employee{//set Staff
	String phoneNumber;
	public Staff(String newname,int newage,String newgender,int newsalary ,String newphoneNumber) throws salaryException, ageException, phoneException{
		super(newname, newage, newgender, newsalary);		
		setInfo(newage, newsalary, newphoneNumber);
		phoneNumber = newphoneNumber;
	}
	
	
	public String toString(){// create the method toString
		return  "This guy is a Staff , " + super.toString();
	}
	
	public void setInfo(int age,int salary, String phone) throws salaryException, ageException, phoneException{
		boolean check = true;
		try{
			if(age<0){
				check = false;
				throw new ageException(age);			
			}
			else{
				super.age = age;
			}			
		}
		catch(ageException e){
			System.out.println("Please input again!");

		}
		try{
			if(salary<0){
				check = false;
				throw new salaryException(salary);			
			}
			else{
				super.salary = salary;
			}
			
		}
		catch (salaryException s) {
			System.out.println("Please input again!");
		}
		try{
			if(phone.length()!=10){
				throw new phoneException(phone);	
			}
			else{
				phoneNumber = phone;
			}
		}
		catch(phoneException p){
			System.out.println("Please input again!");
		}
	}
	
	public void getInfo(){
		System.out.println("This guy is "+ name + ", " +age+" years old, "+gender);
		System.out.println(toString());
		System.out.println("Phonenumber is "+ phoneNumber );
	}
	
	
	
	
}
