package q2.s100502004;


public class Manager extends Employee{
	String title;
	public Manager(String newname,int newage,String newgender,int newsalary ,String newtitle) throws salaryException, ageException{
		super(newname, newage,newgender,newsalary);
		setInfo(newage,newsalary);
		title = newtitle;
	}	
	public String toString(){// create the method toString
		return super.toString()  + "This guy is a Manager";
	}
	
	public void setInfo(int age,int salary) throws salaryException, ageException{
		boolean check = true;
		try{
			if(age<0){
				check = false;
				throw new ageException(age);			
			}
			else{
				super.age = age;
			}			
		}
		catch(ageException e){
			System.out.println("Please input again!");

		}
		try{
			if(salary<0){
				check = false;
				throw new salaryException(salary);			
			}
			else{
				super.salary = salary;
			}
			
		}
		catch (salaryException s) {
			System.out.println("Please input again!");
		}
		if(check){
			getInfo();
		}
	}
	
	public void getInfo(){
		System.out.println("This guy is "+ name + ", "+age+" years old, "+gender);
		System.out.println(toString());
		System.out.println("Title is "+ title + ", "+"Salary is "+salary);
	}
	
}
