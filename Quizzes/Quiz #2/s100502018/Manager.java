package q2.s100502018;

public class Manager extends Employee {
	private String title;

	public Manager(String a, int b, String c, int d, String e) throws AgeException{
		super(a, b, c, d);
		title = e;
	}

	public String toString() {
		return super.toString() + "This guy is a Manager!!!";
	}

	public String getInfo() {
		return super.toString() + "\nTitle is " + title + ", Salary is "
				+ super.getSalary();
	}
}
