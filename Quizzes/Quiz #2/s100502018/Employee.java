package q2.s100502018;

public class Employee extends Person {
	private int salary;
	
	public Employee(String a,int b,String c,int d) throws AgeException{
		super(a, b, c);
		salary = d;
	}
	
	public int getSalary() {
		return salary;
	}
	
	public String toString() {
		return super.toString() + "\nThis guy is an Employee!!!";
	}
}
