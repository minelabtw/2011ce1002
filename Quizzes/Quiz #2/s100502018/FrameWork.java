package q2.s100502018;

import javax.swing.*;
import java.awt.*;

public class FrameWork extends JFrame {
	Color black_button = new Color(0, 0, 0);
	Color white_button = new Color(255, 255, 255);
	JPanel buttonsea = new JPanel();

	public FrameWork(int x) {
		buttonsea.setLayout(new GridLayout(x,x)); //show the graph
		for (int i = 0; i < ((x - 1) / 2); i++) {
			for (int j = 0; j < ((x - 1) / 2) - i; j++) {
				drawButton(white_button);
			}
			for (int k = 0; k < i; k++) {
				drawButton(black_button);
			}
			drawButton(black_button);
			for (int k = 0; k < i; k++) {
				drawButton(black_button);
			}
			for (int j = 0; j < ((x - 1) / 2) - i; j++) {
				drawButton(white_button);
			}
		}
		for (int i = 0; i < x; i++) {
			drawButton(black_button);
		}
		for (int i = 0; i < ((x - 1) / 2); i++) {
			for (int j = 0; j < i+1; j++) {
				drawButton(white_button);
			}
			for (int k = 0; k < ((x - 1) / 2) - i-1; k++) {
				drawButton(black_button);
			}
			drawButton(black_button);
			for (int k = 0; k < ((x - 1) / 2) - i-1; k++) {
				drawButton(black_button);
			}
			for (int j = 0; j < i+1; j++) {
				drawButton(white_button);
			}
		}
		add(buttonsea);
	}

	public void drawButton(Color c) { //set the button
		JButton button = new JButton("");
		button.setBackground(c);
		buttonsea.add(button);
	}
}
