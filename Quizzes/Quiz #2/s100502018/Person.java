package q2.s100502018;

public class Person {
	private String name;
	private int age;
	private String gender;

	public Person(String a, int b, String c) throws AgeException {
		name = a;
		age = b;
		gender = c;
	}

	public String toString() {
		return "This guy is " + name + ", " + age + "years old, " + gender;
	}
}
