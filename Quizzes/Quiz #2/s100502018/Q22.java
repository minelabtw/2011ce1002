package q2.s100502018;

import java.util.Scanner;

public class Q22 {
	public static void main(String[] args) throws AgeException, SalaryException, PhoneException{
		Scanner input = new Scanner(System.in);
		int choice = 0;
		int age,salary;
		String name,gender,title,phonenumber;
		while (choice != 3) {
			System.out
					.println("Please choose the employee you want to create: 1.Manager 2. Staff 3. Exit");
			choice = input.nextInt();
			switch (choice) {			
			case 1:
				System.out
						.println("Please input name, age, gender, salary, title: ");
					name = input.nextLine();
					age = input.nextInt();
					gender = input.nextLine();
					salary = input.nextInt();
					title = input.nextLine();
				try {
					if(age<=0) {
						throw new AgeException();
					}else if(salary<0) {
						throw new SalaryException();
					}else {
						Manager myManager = new Manager(name,
								age, gender, salary,
								title);
						System.out.print(myManager.toString());
					}
				}catch (AgeException ex){
					System.err.println("The age cannot less than or equal to 0");
				}catch (SalaryException ex) {
					System.err.println("The salary cannot less than 0");
				}
				break;
			case 2:
				System.out
						.println("Please input name, age, gender, salary, phone number: ");
					name = input.nextLine();
					age = input.nextInt();
					gender = input.nextLine();
					salary = input.nextInt();
					phonenumber = input.nextLine();
				try {
					if(age<=0) {
						throw new AgeException();
					}else if(salary<0) {
						throw new SalaryException();
					}else if(phonenumber.length()<10) {
						throw new PhoneException();
					}else {
						Staff myStaff = new Staff(name, age,
								gender, salary, phonenumber);
						System.out.print(myStaff.toString());
					}
				}catch (AgeException ex){
					System.err.println("The age cannot less than or equal to 0");
				}catch (SalaryException ex) {
					System.err.println("The salary cannot less than 0");
				}catch (PhoneException ex) {
					System.err.println("The digit cannot less than 10");
				}				
				break;
			case 3:
				System.out.println("Thanks for your using.");
				break;
			}
		}
	}
}
