package q2.s100502018;

public class Staff extends Employee {
	private String phoneNumber;

	public Staff(String a, int b, String c, int d, String e) throws AgeException, SalaryException, PhoneException{
		super(a, b, c, d);
		phoneNumber = e;
	}

	public String toString() {
		return "";
	}

	public String getInfo() {
		return super.toString() + "\nPhone number is " + phoneNumber + ", Salary is "
				+ super.getSalary();
	}

}
