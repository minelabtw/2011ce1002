package q2.s100502018;

import javax.swing.*;

public class Q21 {
	public static void main(String[] args) { //show the Frame
		String oddnumber = JOptionPane.showInputDialog(null,
				"Please input a odd number: ", "Size",
				JOptionPane.INFORMATION_MESSAGE);
		FrameWork myFrameWork = new FrameWork((Integer.parseInt(oddnumber)));
		myFrameWork.setTitle("Q21");
		myFrameWork.setSize(500, 500);
		myFrameWork.setLocationRelativeTo(null);
		myFrameWork.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		myFrameWork.setVisible(true);
	}
}
