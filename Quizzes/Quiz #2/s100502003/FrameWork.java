package q2.s100502003;

import java.awt.*;
import javax.swing.*;

public class FrameWork extends Q21{
	JButton black_button, white_button;
	public FrameWork(int length) {
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(length, length));
		for(int i=0; i<(length-1)/2; i++) {
			for(int j=(length-1)/2; j>i; j--) {
				p.add(white_button);
			}
			for(int k=i+1;k<=2*i-1;k++) {
				p.add(black_button);
			}
			for(int l=(length-1)/2; l>i; l--) {
				p.add(white_button);
			}
		}
		for(int m=0; m<length; m++) {
			p.add(black_button);
		}
		for(int a=0; a<(length-1)/2; a++) {
			for(int b=0; b<=a; b++) {
				p.add(white_button);
			}
			for(int c=(length-1)/2; a<=2*c&&c>0; c--) {
				p.add(black_button);
			}
			for(int d=0; d<=a; d++) {
				p.add(white_button);
			}
		}
		this.add(p);
	}
	public void drawButton(Color c) {
		black_button.setBackground(c.BLACK);
		white_button.setBackground(c.WHITE);
	}
}
