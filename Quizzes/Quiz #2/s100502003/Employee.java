package q2.s100502003;

public class Employee extends Person {
	protected static int salary;
	public Employee() {
		super(name, age, gender);
	}
	public Employee(int Salary) {
		super(name, age, gender);
		salary = Salary;
	}
	
	public int getSalary() {
		return salary;
	}
	public String toString() {
		return "This guy is an Employee!";
	}
}
