package q2.s100502003;

public class Person {
	protected static String name, gender;
	protected static int age;
	public Person(String Name, int Age, String Gender) {
		name = Name;
		age = Age;
		gender = Gender;
	}
	
	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}
	
	public String getGender() {
		return gender;
	}
	
	public String toString() {
		return "This guy is a person!";
	}
}
