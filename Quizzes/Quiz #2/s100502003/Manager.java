package q2.s100502003;

public class Manager extends Employee{
	private String title;
	Employee em = new Employee();
	
	public Manager(String Name, int Age, String Gender, int Salary, String Title) {
		super(salary);
		setInfo(Name,Age,Gender,Salary,Title);
	}
	public String getTitle() {
		return title;
	}
	
	public String toString() {
		return "This guy is a Manager!"+em.toString();
	}
	
	public void setInfo(String Name, int Age, String Gender, int Salary, String Title) {
		name = Name;
		age = Age;
		gender = Gender;
		salary = Salary;
		title = Title;
	}
	
	public String getInfo() {
		return name+"\n"+age+"\n"+gender+"\n"+salary+"\n"+title+"\n";
	}
}
