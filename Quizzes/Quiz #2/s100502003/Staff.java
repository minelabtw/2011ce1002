package q2.s100502003;

public class Staff extends Employee{
	private String phoneNumber;
	Employee Em = new Employee();
	
	public Staff(String PhoneNumber) {
		setInfo(PhoneNumber);
	}
	public String toString() {
		return "This guy is a Staff!"+Em.toString();
	}
	public void setInfo(String PhoneNumber) {
		phoneNumber = PhoneNumber;
	}
	public String getInfo() {
		return "Phone number: "+phoneNumber;
	}
	
}
