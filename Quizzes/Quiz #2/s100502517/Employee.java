package q2.s100502517;

public class Employee extends Person{
	protected int salary;
	
	public String toString(){
		return "This guy is an Employee~!!!";
	}
	
	// to set the Information
	public void setInFo(String name, int age, String gender, int salary){
		super.setInFo(name, age, gender);
		this.salary = salary;		
	}
	
	// to check if the salary is less than 0
	public void check() throws ageException, salaryException, phoneException{
		super.check();
		if(this.salary <= 0){
			throw new salaryException();
		}
	}
	
	
}
