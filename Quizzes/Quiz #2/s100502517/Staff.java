package q2.s100502517;

public class Staff extends Employee{
	private String phoneNumber;
	
	public Staff(String name, int age, String gender, int salary,String phoneNumber){
		this.setInFo(name, age,gender,salary,phoneNumber);
	}
	
	// to set the Information
	public void setInFo(String name, int age, String gender, int salary,String phoneNumber){
		super.setInFo(name, age, gender,salary);
		this.phoneNumber = phoneNumber;
	}
	
	//get the Information
	public String getInFo(){
		return "The guy's name is "+name+", "+age+" years old, "+gender+"\n"+this.toString()+"\nphone number is "+phoneNumber+", Salary is "+salary;
	}
	
	//to check if phoneNumber is less than 10 digits
	public void check()throws ageException,salaryException,phoneException{
		super.check();
		int length = phoneNumber.length();
		if(length != 10){
			throw new phoneException();
		}
	}
	
	
	
}
