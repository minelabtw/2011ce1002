package q2.s100502517;

public class Manager extends Employee{
	private String title;
	
	public Manager(String name, int age, String gender, int salary, String title){
		this.setInFo(name, age, gender, salary,title);
	}
	
	public String toString(){
		return super.toString()+"This guy is a Manager";
	}
	
	// to set the Information
	public void setInFo(String name, int age, String gender, int salary, String title){
		super.setInFo(name, age, gender, salary);
		this.title = title;
	}
	
	public String getInFo(){
		return "The guy's name is "+name+", "+age+" years old, "+gender+"\n"+this.toString()+"\nTitle is "+title+", Salary is "+salary;
	}	
	
}
