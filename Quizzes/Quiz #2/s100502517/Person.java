package q2.s100502517;

public class Person {
	
	protected String name;
	protected int age;
	protected String gender;
	
	
	public String toString(){
		return "The guy is a person~!!!";
	}
	
	// to set the Information
	public void setInFo(String name, int age, String gender) {
		this.name = name;
		this.age = age;
		this.gender = gender;		
	}
	
	//check if age is less than 0
	public void check() throws ageException, salaryException, phoneException{
		if(this.age <= 0){
			throw new ageException();
		}
	}
}
