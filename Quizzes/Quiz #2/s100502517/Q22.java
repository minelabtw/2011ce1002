package q2.s100502517;
import java.util.Scanner;
public class Q22 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		
		//control the while
		boolean stop = true;
		while(stop){
			
			System.out.println("Please choose the employee you want to create: 1.Manager 2.Staff 3.Exit");
			int choose = input.nextInt();
			try{
				switch(choose){
					case 1:					
						System.out.println("Please input name, age, gender, salary, title");
						Manager manager = new Manager(input.next(),input.nextInt(),input.next(),input.nextInt(),input.next());
						manager.check();
						System.out.println(manager.getInFo());
						break;
						
					case 2:				
						System.out.println("Please input name, age, gender,salary,phone number");
						Staff staff = new Staff(input.next(),input.nextInt(),input.next(),input.nextInt(),input.next());
						staff.check();
						System.out.println(staff.getInFo());
						break;
					case 3:
						System.out.println("Bye~ Bye~!!!");
						stop = false;
						break;
					default:
						stop = false;	
						break;
				}
			}
			//if age <= 0
			catch(ageException age){
				System.out.println(age.ageerror());
			}
			
			//if salary <= 0
			catch(salaryException age){
				System.out.println(age.salaryerror());
			}
			
			//if phonenumber is less than 10 digits
			catch(phoneException age){
				System.out.println(age.phoneerror());
			}
		}
	}

}
