package q2.s100502025;

public class Staff extends Employee {
	
	private String phoneNumber;
	
	public Staff() {
		super();
	}
	
	public String toString() {
		return "This guy is a staff ," + super.toString();
	}
}
