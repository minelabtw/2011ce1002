package q2.s100502025;

import java.util.Scanner;
import javax.swing.JFrame;
import java.awt.*;

public class Q21 {
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.print("Input your length with odd number :");
		int length = input.nextInt();
		
		FrameWork frame = new FrameWork(length);
		frame.setTitle("Quiz #2");
		frame.setSize(500,500);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
