package q2.s100502025;

import javax.swing.*;
import java.awt.*;

public class FrameWork extends JFrame{
	
	private JButton black_button = new JButton("");
	private JButton white_button = new JButton("");
	
	public FrameWork(int length) {
		setLayout( new GridLayout( length , length ) );
		
		black_button.setBackground(Color.BLACK);
		white_button.setBackground(Color.WHITE);
		
		
		for(int i = 1 ; i < ( length / 2 ) ; i++) {
			for(int j = 1 ; j <= ( length + 1 - i * 2 ) / 2 ; j++ ) {
				add(white_button);
			}
			for(int j = 1 ; j <= ( i * 2 - 1 ) ; j++ ) {
				add(black_button);
			}
			for(int j = 1 ; j <= ( length + 1 - i * 2 ) / 2 ; j++ ) {
				add(white_button);
			}
		}
		for(int i = 1 ; i <= length ; i++ ) {
			add(black_button);
		}
		for(int i = 1 ; i < ( length / 2 ) ; i++) {
			for(int j = 1 ; j <= i ; j++ ) {
				add(white_button);
			}
			for(int j = 1 ; j < ( length - i * 2 ) ; i++) {
				add(black_button);
			}
			for(int j = 1 ; j <= i ; j++ ) {
				add(white_button);
			}
		}
	}
}
