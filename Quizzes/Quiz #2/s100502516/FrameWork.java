package q2.s100502516;

import javax.swing.*;
import java.awt.*;

public class FrameWork extends JFrame{
	private Color black_button = new Color(0,0,0);
	private Color white_button = new Color(255,255,255);
	
	public FrameWork()
	{			
		String input = JOptionPane.showInputDialog("odd length");
		int length = Integer.parseInt(input);
		
		while(length % 2 == 0)
		{
			JOptionPane.showMessageDialog(null, "Invalid enter, try again.");
			input = JOptionPane.showInputDialog("odd length");
			length = Integer.parseInt(input);
		}
		
		setLayout(new GridLayout(length, length));				
		
		for(int i = 0; i < (length - 1) / 2; i++)
		{
			for(int j = 0; j < ((length - 1) / 2 - 2 * i) / 2; j++)			
				drawButton(white_button);			
			
			for(int j = 0; j < 1 + 2 * i; j++)
				drawButton(black_button);
			
			for(int j = 0; j < ((length - 1) / 2 - 2 * i) / 2; j++)			
				drawButton(white_button);			
		}
		
		for(int i = 0; i < length ; i++)
			drawButton(black_button);
		
		for(int i = 0; i < (length - 1) / 2; i++)
		{
			for(int j = i; j > ((length - 1) / 2 - 2 * i) / 2; j--)			
				drawButton(white_button);			
			
			for(int j = i; j > 1 + 2 * i; j--)
				drawButton(black_button);
			
			for(int j = i; j > ((length - 1) / 2 - 2 * i) / 2; j--)			
				drawButton(white_button);			
		}
	}
	
	void drawButton(Color c)
	{
		JButton b = new JButton();
		b.setBackground(c);
		add(b);
	}
}
