package q2.s100502516;

public class Staff extends Employee {
	private String phoneNumber;
	
	public Staff(String name, int age, String gender, int salary, String phoneNumber) throws AgeException, SalaryException, PhoneException
	{
		setInfo(name, age, gender, salary, phoneNumber);
		
		if(age <= 0)	//check eroor
			throw new AgeException();
		
		if(salary < 0)
			throw new SalaryException();		
		
		if(phoneNumber.length() < 10)
			throw new PhoneException();
	}
	public String toString()
	{
		return "This guy is a Staff!! " + super.toString();
	}
	public void setInfo(String name, int age, String gender, int salary, String phoneNumber)
	{
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.salary = salary;
		this.phoneNumber = phoneNumber;
	}	
	public void getInfo()	//output all information
	{
		System.out.println("This gut is " + name + ", " + age + " years old, " + gender + 
				"\n" + toString() + "\nPhone number is " + phoneNumber + ", Salary is " + salary);
	}
}
