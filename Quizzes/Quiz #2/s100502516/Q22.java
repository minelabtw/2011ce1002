package q2.s100502516;

import java.util.*;

public class Q22 {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int selection;
		boolean exit = false;	//exit flag	
		
		while(!exit)
		{
			System.out.println("Please choose the employee you want to create:1.Manager 2.Staff 3.Exit");
			selection = input.nextInt();
			
			switch(selection)
			{
				case 1:	//manager
					System.out.println("Please input name, age, gender, salary, title");
					try
					{
						Manager manager = new Manager(input.next(), input.nextInt(), input.next(), input.nextInt(), input.next());
						manager.getInfo();
					}
					catch(AgeException e)
					{
						System.err.println("Age invalid!!");
					}
					catch(SalaryException e)
					{
						System.err.println("Salary invalid!!");
					}
					catch(PhoneException e)
					{
						System.err.println("Phone number invalid!!");
					}
					break;
					
				case 2:	//staff
					System.out.println("Please input name, age, gender, salary, phone number");
					try
					{
						Staff staff = new Staff(input.next(), input.nextInt(), input.next(), input.nextInt(), input.next());
						staff.getInfo();
					}
					catch(AgeException e)
					{
						System.err.println("Age invalid!!");
					}
					catch(SalaryException e)
					{
						System.err.println("Salary invalid!!");
					}
					catch(PhoneException e)
					{
						System.err.println("Phone number invalid!!");
					}
					break;
					
				case 3:	//exit
					exit = true;
					break;
			}
		}		
	}
}
