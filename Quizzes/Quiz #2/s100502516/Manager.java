package q2.s100502516;

public class Manager extends Employee {
	private String title;
	
	public Manager(String name, int age, String gender, int salary, String title) throws AgeException, SalaryException, PhoneException
	{
		setInfo(name, age, gender, salary, title);
		
		if(age <= 0)	//check error
			throw new AgeException();
		
		if(salary < 0)
			throw new SalaryException();		
	}	
	public String toString()
	{
		return "This guy is a Manager!! " + super.toString();
	}	
	public void setInfo(String name, int age, String gender, int salary, String title)  
	{
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.salary = salary;
		this.title = title;		
	}	
	public void getInfo()	// output all information
	{
		System.out.println("This gut is " + name + ", " + age + " years old, " + gender + 
				"\n" + toString() + "\nTitle is " + title + ", Salary is " + salary);
	}
}
