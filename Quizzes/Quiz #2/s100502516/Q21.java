package q2.s100502516;

import javax.swing.JFrame;

public class Q21 {
	public static void main(String[] args)
	{
		FrameWork frame = new FrameWork();
		frame.setTitle("FrameWork");
		frame.setSize(500, 500);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
