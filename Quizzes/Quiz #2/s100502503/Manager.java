package q2.s100502503;

public class Manager extends Employee
{
	protected String title;
	
	public Manager(String e_name, int e_age, String e_gender, int e_salary, String e_title)
	{
		super();
		setInfo(e_name,  e_age, e_gender, e_salary, e_title);
	}
	
	public String toString()
	{
		return "The guy is a Manager";
	}
	
	public void setInfo(String e_name, int e_age, String e_gender, int e_salary, String e_title)
	{
		name = e_name;
		age = e_age;
		salary = e_salary;
		title = e_title;
		
		try
		{
			if(age <= 0)
				throw new ageException(age);
		}
		catch(ageException e)
		{
			System.err.println("Initial faild! Try again");
		}
		try
		{
			if(salary <= 0)
				throw new salaryException(salary);
		}
		catch(salaryException e)
		{
			System.err.println("Initial faild! Try again");
		}				

	}
	
	public void getInfo()
	{
		System.out.println("This guy is " + name + ", " + age + " years old, " + gender +
							"\n" + super.toString() + toString() + 
							"\nTitle is " + title + ", Salary is " + salary);
	}
}
