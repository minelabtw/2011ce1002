package q2.s100502503;

public class Staff extends Employee
{
	protected String phoneNumber;
	
	public Staff(String e_name, int e_age, String e_gender, int e_salary, String e_phoneNumber)
	{
		super();
		setInfo(e_name, e_age, e_gender, e_salary, e_phoneNumber);
	}
	
	public void setInfo(String e_name, int e_age, String e_gender, int e_salary, String e_phoneNumber)
	{
		name = e_name;
		age = e_age;
		salary = e_salary;
		phoneNumber = e_phoneNumber;
		
		try
		{
			if(age <= 0)
				throw new ageException(age);
		}
		catch(ageException e)
		{
			System.err.println("Initial faild! Try again");
		}
		try
		{
			if(salary <= 0)
				throw new salaryException(salary);
		}
		catch(salaryException e)
		{
			System.err.println("Initial faild! Try again");
		}				
		try
		{
			if(phoneNumber.length() < 10)
			throw new phoneException(phoneNumber);
		}
		catch(phoneException e)
		{
			System.err.println("Initial faild! Try again");
		}
	}
	
	public void getInfo()
	{
		System.out.println("This guy is " + name + ", " + age + " years old, " + gender +
							"\n" + super.toString() + toString() + 
							"\nphone Number is " + phoneNumber + ", Salary is " + salary);
	}
	
}
