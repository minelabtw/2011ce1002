package q2.s100502503;

import java.util.Scanner;

public class Q22 
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		System.out.println("Please the employee you want to create: 1.Manager 2.Staff 3.Exit");
		
		int choose = input.nextInt();
		
		boolean flag = true;
		
		while(flag)
		{
			switch(choose)
			{
				case 1:
					System.out.println("Please input name, age, gender, salary, title");
					String name = input.next();
					int age =input.nextInt();
					String gender = input.next();
					int salary = input.nextInt();
					String title = input.next();
					
					Manager manager = new Manager(name, age, gender, salary, title);
					
					manager.getInfo();
					break;
					
				case 2:
					System.out.println("Please input name, age, gender, salary, phone number");
					String name2 = input.next();
					int age2 =input.nextInt();
					String gender2 = input.next();
					int salary2 = input.nextInt();
					String phoneNumber = input.next();
					
					Staff staff = new Staff(name2, age2, gender2, salary2, phoneNumber);
					
					staff.getInfo();
					break;
					
				case 3:
					flag = false;
					break;
			}
		}
	}

}
