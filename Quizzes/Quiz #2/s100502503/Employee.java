package q2.s100502503;

public class Employee extends Person
{
	protected int salary;
	
	public Employee()
	{
		super();
	}
	
	public String toString()
	{
		return "The guy is an Employee";
	}

}
