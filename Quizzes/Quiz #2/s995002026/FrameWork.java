package q2.s995002026;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public class FrameWork extends JFrame{
	
	FrameWork(int input){
		JPanel p=new JPanel(new GridLayout(input,input));
		
		for(int i=1;i<=input/2+1;i++){		//上半
			int count=0;
			for(int j=1;j<=input;j++){
				if(j<input/2+2-i)
					p.add(white());
				else if(count!=i*2-1){
					p.add(black());
					count++;
				}
				else 
					p.add(white());
			}
		}
		
		for(int i=1;i<=input/2;i++){		//下半
			int count=0;
			for(int j=1;j<=input;j++){
				if(j<input/2+2-i)
					p.add(white());
				else if(count!=i){
					p.add(black());
					count++;
				}
				else 
					p.add(white());
			}
		}
		
		add(p,BorderLayout.CENTER);
	}
	
	public JButton black(){				//黑色
		
		JButton b= new JButton();
		b.setBackground(Color.black);
		
		return b;
	}
	public JButton white(){				//白色
		JButton w= new JButton();
		w.setBackground(Color.white);
		
		return w;
	}
	
}
