package q1.s100502509;
import java.io.*;
import java.util.*;
public class Q12 {
	public static void main (String[] args)throws Exception{
		
		File inputFile = new File("input.txt"); 
		Scanner input = new Scanner(inputFile);
		String readData = input.next();
		String[] Store = new String[readData.length()];
		for(int i=0; i<readData.length(); i++){
			if(Character.isLetter(readData.charAt(i))){
				Store[i]= String.valueOf(transform(readData.charAt(i)));
			}
			
			else
				Store[i]= String.valueOf(readData.charAt(i));
		}
		
		 
		
		PrintWriter outputString = new PrintWriter("Q1-100502509.txt");
		
		for(int k=0; k<readData.length(); k++)
		{
			outputString.print(Store[k]);
		}
		
	}
	
	public static char transform(char input){ // if the input is char, and return the corresponding char of the number
		if(input == 'A' || input == 'B' || input == 'C')
			return '2';
		else if(input == 'D' || input == 'E' || input == 'F')
			return '3';
		else if(input == 'G' || input == 'H' || input == 'I')
			return '4';
		else if(input == 'J' || input == 'K' || input == 'L')
			return '5';
		else if(input == 'M' || input == 'N' || input == 'O')
			return '6';
		else if(input == 'P' || input == 'Q' || input == 'R' || input == 'S')
			return '7';
		else if(input == 'T' || input == 'U' || input == 'V')
			return '8';
		else
			return '9';
	}



}
