package q1.s985003038;

public class ISBN {
	private int[] ISBNcode;
	
	public ISBN(String s){
		ISBNcode = new int[10];
		for(int i = 0; i < 9; i++)
			ISBNcode[i] = Character.getNumericValue(s.charAt(i));
	}
	
	public String getISBN(){
		StringBuffer result = new StringBuffer();
		for(int i = 0; i < 9; i ++)
			result.append(ISBNcode[i]);
		int checksum = checksum();
		if(checksum == 10)
			result.append("-X");
		else
			result.append("-" + checksum);
		return result.toString();
	}
	
	public void setISBN(String s){
		for(int i = 0; i < 9; i++)
			ISBNcode[i] = Character.getNumericValue(s.charAt(i));
	}

	private int checksum(){
		int result = 0;
		for(int i = 0; i < 9; i++)
			result += ISBNcode[i] * (i + 1);
		result = result % 11;
		return result;
	}
}
