package q1.s985003038;

import java.io.*;

public class Q12 {
	public static void main(String[] args) throws IOException{
		File file = new File("input.txt");
		if(file.exists()){
			FileReader reader = new FileReader("input.txt");
			StringBuffer input = new StringBuffer();
			char[] buffer = new char[256];
			while(reader.read(buffer) > 0){
				input.append(buffer);
			}
			reader.close();
			
			System.out.println("The input is: " + input.toString());
			
			for(int i = 0; i < input.length(); i++)
				if(Character.isLetter(input.charAt(i)))
					input.replace(i, i+1, String.valueOf(getNumber(input.charAt(i))));
			
			System.out.println("The output is: " + input.toString());
			FileWriter writer = new FileWriter("Q1-985003038.txt");
			writer.write(input.toString());
			writer.close();
			System.out.println("Output file created.");
		} else {
			System.out.println("File does not exist.");
		}
	}
	
	private static int getNumber(char input){
		if((input >= 'A' && input <= 'C') || (input >= 'a' && input <= 'c'))
			return 2;
		else if((input >= 'D' && input <= 'F') || (input >= 'd' && input <= 'f'))
			return 3;
		else if((input >= 'G' && input <= 'I') || (input >= 'g' && input <= 'i'))
			return 4;
		else if((input >= 'J' && input <= 'L') || (input >= 'j' && input <= 'l'))
			return 5;
		else if((input >= 'M' && input <= 'O') || (input >= 'm' && input <= 'o'))
			return 6;
		else if((input >= 'P' && input <= 'S') || (input >= 'p' && input <= 's'))
			return 7;
		else if((input >= 'T' && input <= 'V') || (input >= 't' && input <= 'v'))
			return 8;
		else if((input >= 'W' && input <= 'Z') || (input >= 'w' && input <= 'z'))
			return 9;
		else
			return 1;
	}
}
