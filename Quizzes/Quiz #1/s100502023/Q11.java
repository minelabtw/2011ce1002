package q1.s100502023;

import javax.swing.JOptionPane;

public class Q11
{

	public static void main(String[] args)
	{
		String input=JOptionPane.showInputDialog(null,"Sample input:");
		
		ISBN booknumber= new ISBN();
		booknumber.set(input);
		
		JOptionPane.showMessageDialog(null,"Sample output:" + booknumber.get() +"-" +calculateChecksum(booknumber));
		
	}
	
	public static int calculateChecksum(ISBN booknumber)
	{
		String booknumber_String =booknumber.get();
		int tmp=0;
		int d10=0;
		
		int number= Integer.parseInt(booknumber_String);
		
		System.out.println(number);
		for(int i=0;i<booknumber_String.length();i++)
		{
			
			tmp+=(number%10)*(9-i);
			number=number/10;
		}
		
		d10=tmp%11;
		
		if (d10==10)
		{
			return 'X';
		}
		else
		{
			return d10;
		}
		
	}

}


