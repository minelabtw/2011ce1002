package q1.s100502023;

import java.io.FileNotFoundException;
import java.util.*;

public class Q12 
{

	public static void main(String[] args) throws Exception 
	{
		java.io.File inputfile = new java.io.File("input.txt");
		java.io.File outputfile = new java.io.File("Q1-school number.txt");
		
		Scanner input = new Scanner(inputfile);
				
		String origin=input.nextLine();
		
		String result="";
		
		for (int i=0;i<origin.length();i++)
		{
			if(Character.isLetter(origin.charAt(i))==true)
			{
				result+=getNumber(origin.charAt(i));
			}
			else
			{
				result+=origin.charAt(i);
			}
		}
		
		java.io.PrintWriter output = new java.io.PrintWriter(outputfile);
	
		output.println("Sample input:"+ origin);
		output.print("Sample output:" + result);
		output.close();
		
	}
	
	public static int getNumber(char letter)
	{
		if(letter=='A' ||letter=='B'||letter=='C')
		{
			return 2;
		}
		else if(letter=='D' ||letter=='E'||letter=='F')
		{
			return 3;
		}
		else if(letter=='G' ||letter=='H'||letter=='I')
		{
			return 4;
		}
		else if(letter=='J' ||letter=='K'||letter=='L')
		{
			return 5;
		}
		else if(letter=='M' ||letter=='N'||letter=='O')
		{
			return 6;
		}
		else if(letter=='P' ||letter=='Q'||letter=='R' ||letter=='S')
		{
			return 7;
		}
		else if(letter=='T' ||letter=='U'||letter=='V')
		{
			return 8;
		}
		else if(letter=='W' ||letter=='X'||letter=='Y' ||letter=='Z')
		{
			return 7;
		}
		else
		{
			return letter;
		}
		
		
	}

}
