package q1.s100502506;

import java.io.File;
import javax.swing.JOptionPane;
import java.util.Scanner;

public class Q12 
{
	public static void main(String argc[]) throws Exception
	{
		java.io.File outputFile =new java.io.File("Q1-100502506.txt");
		java.io.PrintWriter output =new java.io.PrintWriter(outputFile);
		if(!outputFile.exists())
		{
			System.out.print("Don't have output!");
			System.exit(0);
		}
		java.io.File inputFile =new java.io.File("input.txt");
		if(!inputFile.exists())
		{
			System.out.print("Don't have input!");
			System.exit(0);
		}
		Scanner inputScanner=new Scanner(inputFile);
		String messageString =new String();
		messageString=inputScanner.nextLine();
		
		char[] a=new char[messageString.length()];
		for(int i=0;i<messageString.length();i++)
		{
			int j = 0;
			if(check(messageString.charAt(i))==true)
			{
				output.print(getNumber(messageString.charAt(i)));
			}
			output.print(messageString.charAt(i));
		}
		output.close();
	}
	public static int getNumber(char letter)
	{
		int number = 0;
		if(checklower(letter)==true)
		{
			number=(int)letter-96;
		}
		else if(checklower(letter)==false)
		{
			number=(int)letter-64;
		}
		return number;
		
	}
	public static boolean check(char x)
	{
		if(((int)x>(int)'a')&&(int)x<(int)'z')
		{
			return true;
		}
		else if((int)x>(int)'A'&&(int)x<(int)'Z')
		{
			return true;
		}
		else 
		{
			return false;
		}
		
	}
	public static boolean checklower(char x)
	{
		if(((int)x>(int)'a')&&(int)x<(int)'z')
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}

}
