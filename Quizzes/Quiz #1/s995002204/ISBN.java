package q1.s995002204;

public class ISBN {
	private static int digit[]= new int[10];
	
	
	public ISBN(int a,int b,int c,int d,int e,int f, int g,int h,int i,int j)
	{
		digit[0] = a;
		digit[1] = b;
		digit[2] = c;
		digit[3] = d;
		digit[4] = e;
		digit[5] = f;
		digit[6] = g;
		digit[7] = h;
		digit[8] = i;
		digit[9] = j;
	}
	
	public static int[] getISBN()
	{
		return digit;
	}
}
