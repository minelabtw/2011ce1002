package q1.s100502501;
import java.io.*;
import java.util.*;
public class Q12 {
	public static void main(String[] args) throws Exception{
		String str="";
		File infile=new File("input.txt");
		File outfile=new File("output.txt");
		Scanner fin=new Scanner(infile);
		PrintWriter fout=new PrintWriter(outfile); 
		String pin=fin.next();
		char[] ppin=new char[pin.length()];
		while(fin.hasNext()){
			 str=fin.next();
		}
		for(int i=0;i<pin.length();i++){
			if(Character.isLetter(pin.charAt(i))&&ppin[i]!='-')
				ppin[i]=getNumber(ppin[i]);
		}
	}
	public static int getNumber(char letter){
		if(letter=='A'||letter=='B'||letter=='C')
			return (int)2;
		else if(letter=='D'||letter=='E'||letter=='F')
			return (int)3;
		else if(letter=='G'||letter=='H'||letter=='I')
			return (int)4;
		else if(letter=='J'||letter=='K'||letter=='L')
			return (int)5;
		else if(letter=='M'||letter=='N'||letter=='O')
			return (int)6;
		else if(letter=='P'||letter=='Q'||letter=='R'||letter=='S')
			return (int)7;
		else if(letter=='T'||letter=='U'||letter=='V')
			return (int)8;
		else if(letter=='W'||letter=='X'||letter=='Y'||letter=='Z')
			return (int)9;
	}
}
