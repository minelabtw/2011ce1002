package q1.s100502503;

import java.util.Scanner;
import java.io.*;

public class Q12 
{
	Scanner input = new Scanner(System.in);
	public void main(String[] args) throws Exception
	{
		String data = "";
		char[] numberChar = new char[100];
		File fileRD = new File("input.txt");
		Scanner input = new Scanner(fileRD);
		
		File fileWD = new File("Q1-school number.txt");
		
		while(input.hasNext())
		{
			data = input.next();
		}
		PrintWriter output = new PrintWriter(fileWD);
		for(int i = 0; i <= data.length(); i++)
		{
			numberChar[i] = (char) getNumber(data.charAt(i));
			output.print(numberChar[i]);
		}
		
		input.close();
		output.close();
	}
	
	//determine what is the letter stand for
	public int getNumber(char letter)
	{
		while(Character.isLetter(letter))
		{
			if(letter >= 'A' && letter <= 'C')
				letter = 2;
			else if(letter >= 'D' && letter <= 'F')
				letter = 3;
			else if(letter >= 'G' && letter <= 'I')
				letter = 4;
			else if(letter >= 'J' && letter <= 'K')
				letter = 5;
			else if(letter >= 'M' && letter <= 'O')
				letter = 6;
			else if(letter >= 'P' && letter <= 'S')
				letter = 7;
			else if(letter >= 'T' && letter <= 'V' )
				letter = 8;
			else if(letter >= 'W' && letter <= 'Z')
				letter = 9;
			else
				break;		
		}
		return letter;
	
				
	}
}
