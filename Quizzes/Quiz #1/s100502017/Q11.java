package q1.s100502017;
import javax.swing.JOptionPane;
public class Q11 {
	public static void main(String[] args){
		String ini=JOptionPane.showInputDialog("Please enter night digits");
		ISBN object=new ISBN(checksum(ini));
		JOptionPane.showMessageDialog(null,object.get_isbn());
	}
	public static String checksum(String s){
		int d10=0;
		for(int i=1;i<=9;i++){
			d10+=(Integer.parseInt(String.valueOf(s.charAt(i-1)))*i)%11;
		}
		char[] arr=new char[11];
		for(int j=0;j<=9;j++){
			if(j<=8)
				arr[j]=s.charAt(j);
			else if(d10==10){
				arr[j]='-';
				arr[j+1]='X';
			}
			else{
				arr[j]='-';
				arr[j+1]=inttochar(d10);
			}
			
		}
		String ans=new String(arr);
		return ans;
	}
	public static char inttochar(int i){
		if(i==0)
			return'0';
		if(i==1)
			return'1';
		if(i==2)
			return'2';
		if(i==3)
			return'3';
		if(i==4)
			return'4';
		if(i==5)
			return'5';
		if(i==6)
			return'6';
		if(i==7)
			return'7';
		if(i==8)
			return'8';
		else
			return'9';
	}
}
