package q1.s100502017;
import java.util.Scanner;
public class Q12 {
	public static void main(String[] args)throws Exception{
		java.io.File in=new java.io.File("input.txt");
		Scanner input=new Scanner(in);
		String str=input.next();
		java.io.File out=new java.io.File("Q1-100502017.txt");
		java.io.PrintWriter output=new java.io.PrintWriter(out);
		char[] arr=new char[str.length()];
		for(int j=0;j<str.length();j++){
			arr[j]=str.charAt(j);
		}
		for(int i=0;i<str.length();i++){
			if(Character.isLetter(str.charAt(i)))
				arr[i]=inttochar(getNumber(str.charAt(i)));
		}
		String ans=new String(arr);
		output.print(ans);
		input.close();
		output.close();
		System.out.print("change over");
	}
	public static int getNumber(char letter){
		if(letter=='A'||letter=='B'||letter=='C')
			return 2;
		if(letter=='D'||letter=='E'||letter=='F')
			return 3;
		if(letter=='G'||letter=='H'||letter=='I')
			return 4;
		if(letter=='J'||letter=='K'||letter=='L')
			return 5;
		if(letter=='M'||letter=='N'||letter=='O')
			return 6;
		if(letter=='P'||letter=='Q'||letter=='R'||letter=='S')
			return 7;
		if(letter=='T'||letter=='U'||letter=='V')
			return 8;
		else
			return 9;
	}
	public static char inttochar(int i){
		if(i==0)
			return'0';
		if(i==1)
			return'1';
		if(i==2)
			return'2';
		if(i==3)
			return'3';
		if(i==4)
			return'4';
		if(i==5)
			return'5';
		if(i==6)
			return'6';
		if(i==7)
			return'7';
		if(i==8)
			return'8';
		else
			return'9';
	}
}
