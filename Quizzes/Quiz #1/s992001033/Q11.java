package q1.s992001033;
import javax.swing.JOptionPane;

public class Q11
{
	public static void main(String[] args)
	{
		String firstNineISBN = JOptionPane.showInputDialog("Enter the first nine digits : ");
		ISBN book = new ISBN(firstNineISBN);
		book.setISBN(transform(firstNineISBN));
		JOptionPane.showMessageDialog(null, book.getISBN());
	}
	public static String transform(String nineISBN)//算出第10位的method
	{
		String result = "";
		char[] digits = new char[11];
		int d10 = 0;
		for(int i=0;i<9;i++)
		{
			digits[i] = nineISBN.charAt(i);//分別拆開
			d10 += (digits[i]-'0')*(i+1);//計算
			result += digits[i];
		}
		result += '-';
		if((d10%11)==10)
			result +='X';
		else
			result += (d10%11);
		return result;
	}
	
}
