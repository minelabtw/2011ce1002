package q1.s995002026;

public class ISBN {
	private String d10;
	
	private int[] dx=new int[10];
	
	private String ds;
	
	ISBN(String input){
		ds=input;
		
		for(int i=0;i<input.length();i++){
			dx[i]=Integer.parseInt(input.substring(i, i+1));
			
		}
		d10=Integer.toString((dx[0]*1+dx[1]*2+dx[2]*3+dx[3]*4+dx[4]*5+dx[5]*6+dx[6]*7+dx[7]*8+dx[8]*9)%11);
	}
	
	public void setd(){
		d10="X";
	}
	
	public  String getd(){
		
		if((dx[0]*1+dx[1]*2+dx[2]*3+dx[3]*4+dx[4]*5+dx[5]*6+dx[6]*7+dx[7]*8+dx[8]*9)%11==10)
			setd();
		
		
		return ds+"-"+d10;
	}
	
}
