package q1.s995002026;

import java.nio.file.*;

import java.util.Scanner;

public class Q12 {
	public static void main(String[] argv){
		
		Scanner input=new Scanner();
		
		String x=input.next();
		for(int i=0;i<x.length();i++){
			if(Character.isLetter(x.charAt(i)))
				getNumber(x.charAt(i));
		
	}
	
	public static int getNumber(char letter){
		if(letter-'0'<=19 && letter-'0'>=17)
			return Integer.parseInt("2");
			
		else if(letter-'0'<=22 && letter-'0'>=20)
			return Integer.parseInt("3");
	
		else if(letter-'0'<=25 && letter-'0'>=23)
			return Integer.parseInt("4");
		
		else if(letter-'0'<=28 && letter-'0'>=26)
			return Integer.parseInt("5");
		
		else if(letter-'0'<=31 && letter-'0'>=29)
			return Integer.parseInt("6");
		
		else if(letter-'0'<=35 && letter-'0'>=32)
			return Integer.parseInt("7");
		
		else if(letter-'0'<=38 && letter-'0'>=36)
			return Integer.parseInt("8");
		
		else if(letter-'0'<=42 && letter-'0'>=39)
			return Integer.parseInt("9");
	}
}
