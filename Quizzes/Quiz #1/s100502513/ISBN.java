package q1.s100502513;

import java.util.*;

public class ISBN {

	private String num;
	private int sum;
	private String n10 = "-X";
	
	ISBN() {
	}

	ISBN(String number) {
		num = number;
	}

	public String getisbn() {
		return num;
	}

	public void setisbn(String number) {
		num = number;
	}

	public void caculate(String number) {
		for (int i = 0; i < number.length(); i++)
			sum += number.charAt(i) * (i + 1) / (49 * (i+1));
		for (int j = 0; j < 10; j++) {
			if (sum % 11 == 10) {
				setisbn(number+"-X");
			} 
			else if (sum % 11 == j) {
				
				setisbn(number+"-"+j);
			}
			else
				continue;
		}
	}
}
