package q1.s100502001;

public class ISBN {
	private int[] isbn;
	private int isbn_num;
	public ISBN(int[] num){
		isbn=num;
	}
	public void set_isbn(int num1){
		isbn_num=num1;
	}
	public int get_isbn(){
		return isbn_num;
	}
	public void operation(){
		int sum=0;
		for(int i=0;i<isbn.length;i++){
			for(int j=9-i;j>=1;j--)
				sum+=isbn[i]*j;
		}
		set_isbn(sum%11);
	}
}
