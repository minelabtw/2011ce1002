package q1.s100502001;
import java.util.Scanner;
import java.io.*;
public class Q12 {
	public static void main(String[] args) throws Exception{
		File file = new File("input.txt");
		Scanner input= new Scanner(file);
		String str=input.next();
		String str_number="";
		char[] num=new char[11];
		for(int i=0;i<num.length;i++)
			num[i]=str.charAt(i);
		for(int i=0;i<num.length;i++){
			if(Character.isLetter(num[i]))
				str_number+=Integer.toString(getNumber(num[i]));
			else
				str_number+=String.valueOf(Integer.toString(num[i]));
		}
		PrintWriter output=new PrintWriter("Q1-100502001-txt");
		output.print(str_number);
		input.close();
		output.close();
	}
	public static int getNumber(char letter){
		char[][] test ={{},{'A','B','C'},{'D','E','F'},{'G','H','I'},{'J','K','L'},{'M','N','O'},{'P','Q','R','S'},{'T','U','V'},{'W','X','Y','Z'}};
		for(int i=0;i<test.length;i++){
			for(int j=0;j<test[i].length;j++)
				if(letter==test[i][j])
					return i;
			
		}
		return 0;
				
	}

}
