package q1.s975002502;
import java.io.*;
import java.util.*;

public class Q12{
	public static int getNumber(char letter){
		
		if(letter=='A' || letter=='B' || letter=='C'){
			return 2;
		}
		else if(letter=='D' || letter=='E' || letter=='F'){
			return 3;
		}
		else if(letter=='G' || letter=='H' || letter=='I'){
			return 4;
		}
		else if(letter=='J' || letter=='K' || letter=='L'){
			return 5;
		}
		else if(letter=='M' || letter=='N' || letter=='O'){
			return 6;
		}
		else if(letter=='P' || letter=='Q' || letter=='R' || letter=='S'){
			return 7;
		}
		else if(letter=='T' || letter=='U' || letter=='V'){
			return 8;
		}
		else{
			return 9;
		}

	}
	public static void main(String[] args)throws Exception{
		
		File file = new File("input.txt");
		File ofile = new File("Q1-975002502.txt");
		Scanner input = new Scanner(file);
		PrintWriter printfile = new PrintWriter(ofile);
		String output = "";
		
		while(input.hasNext()){
			String str = input.next();
			output = str;
			System.out.print(str+"\n");
			for(int i=0; i<str.length(); i++){
				if((str.charAt(i)!='-') && (Character.isLetter(str.charAt(i)))){
					int num = getNumber(str.charAt(i));
					output = output.substring(0,i)+Integer.toString(num);
					System.out.println(output);
				}
			}
			printfile.print(output);
		}
		input.close();
		printfile.close();
		
	}
}
