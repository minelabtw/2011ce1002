package q1.s975002502;

public class ISBN {
	
	public static String number = "";
	
	ISBN(){}
	
	ISBN(String str){
		number = str;
	}
	
	public static String getISBN(){
		return number;
	}
	
	public static void setISBN(String isbn){
		number = isbn;
	}
	
	public static void countISBN(String input){
		String output= "";		
		int count=0;
		
		for(int i=0; i<input.length(); i++){
			count += Integer.parseInt(input.substring(i,i+1))*(i+1);
		}
		
		count = count%11;
		
		if(count!=10){
			output = input + "-" + Integer.toString(count);
		}
		else
			output = input + "-X"; 
		
		setISBN(output);
	}
}
