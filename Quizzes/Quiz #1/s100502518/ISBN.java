package q1.s100502518;

public class ISBN {
	private String number;
	private char[] store;
	private int d10;
	
	ISBN(String a) 
	{
		number=a;
	}
	
	public String getnumber() 
	{
		return number;
	}
	
	public void calculate() 
	{
		store=new char[9];
		for(int a=0;a<9;a++)
		{
			store[a]=number.charAt(a);
		}
		
		for(int a=0;a<9;a++)
		{
			d10=(store[a]-48)*(a+1)+d10;
		}
		
		d10=d10%11;
		
		if(d10==10)
		{
			number=number+'-'+'X';
		}
		else
		{
			number=number+'-'+d10;
		}
	}

}
