package q1.s100502518;

import java.util.*;
import java.io.*;
public class Q12 {
	public static void main(String[] args) throws Exception
	{
		File file = new File("input.txt");
		Scanner input=new Scanner(file);
		
		String number=input.next();
		char[] store=new char[13];
		
		for(int a=0;a<13;a++)
		{
			store[a]=number.charAt(a);
		}
		for(int a=0;a<13;a++)
		{
			if(Character.isLetter(store[a]))
			{
				store[a]=getNumber(store[a]);
			}
		}
		PrintWriter output = new PrintWriter("Q1-100502518.txt"); 
		
		for(int a=0;a<13;a++)
		{
			output.print(store[a]); 
		}
		
		input.close();
		output.close();
	}
	
	public static char getNumber(char letter) 
	{
		if(letter=='A'||letter=='B'||letter=='C')
		{
			return '2';
		}
		
		else if(letter=='D'||letter=='E'||letter=='F')
		{
			return '3';
		}
		
		else if(letter=='G'||letter=='H'||letter=='I')
		{
			return '4';
		}
		
		else if(letter=='J'||letter=='K'||letter=='L')
		{
			return '5';
		}
		
		else if(letter=='M'||letter=='N'||letter=='O')
		{
			return '6';
		}
		
		else if(letter=='P'||letter=='Q'||letter=='R'||letter=='S')
		{
			return '7';
		}
		
		else if(letter=='T'||letter=='U'||letter=='V')
		{
			return '8';
		}
		
		else 
		{
			return '9';
		}
	}

}
