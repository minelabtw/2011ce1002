package q1.s100502014;
import java.io.*;
import java.util.*;
public class Q12 {
	public static void main(String[] args) throws Exception {
		 File f = new File("input.txt");	//open input.txt
		 Scanner input = new Scanner(f);	//input
		 String num = input.next();			//store into sting
		 String newNum = "";				//create new string and initialize
		 for(int i=0;i<num.length();i++) {	//transform the number
			 if(Character.isLetter(num.charAt(i)))	//is letter
				 newNum += getNumber(num.charAt(i));
			 else if (num.charAt(i) == '-')			//is -
				 newNum += '-';
			 else									//is number
				 newNum += num.charAt(i);
		 }
		 File f2 = new File("Q1-100502014.txt");	//create Q1-100502014.txt
		 PrintWriter output = new PrintWriter(f2);
		 output.print(newNum);				//write into output
		 output.close();					//close Q1-100502014.txt
	}
	
	public static int getNumber(char letter) {
		int temp = letter - 'A';
		if(temp<3 && temp>=0)		//A B C
			return 2;
		else if(temp<6 && temp>=3)	//D E F
			return 3;
		else if(temp<9 && temp>=6)	//G H I
			return 4;
		else if(temp<12 && temp>=9)	//J K L
			return 5;
		else if(temp<15 && temp>=12)//M N O
			return 6;
		else if(temp<19 && temp>=15)//P Q R S
			return 7;
		else if(temp<22 && temp>=19)//T U V
			return 8;
		else						//W X Y Z
			return 9;
	}
}
