package q1.s100502014;
import javax.swing.*;
public class Q11 {
	public static void main(String[] args) {
		String num = JOptionPane.showInputDialog(null,
					 "Please input 9 digits:",
					 "Input!!!",
					 JOptionPane.QUESTION_MESSAGE);
		ISBN isbn = new ISBN(num);
		String output = "Your ISBN is " + num + test(isbn.getString());
		JOptionPane.showMessageDialog(null,
									  output,
									  "ISBN!!!",
									  JOptionPane.INFORMATION_MESSAGE);
	}
	
	//test the last number
	public static String test(String num) {
		int x=0;
		for(int i=0;i<num.length();i++) {
			x += ( num.charAt(i) - '0' );
		}
		if(x%11 != 10)	//if the last number is not 10
			return "-" + x%11;
		else	//the last number is 10
			return "-X";
	}
}
