package q1.s100502016;

import java.util.Scanner;
import java.io.*;

public class Q12 {
	public static void main(String[] args) throws FileNotFoundException {
		File file = new File("input.txt");
		Scanner input = new Scanner(file);
		String string;
		while (input.hasNext()) {
			string = input.next();
			for (int i = 0; i < string.length(); i++){
				if (Character.isLetter(string.charAt(i)))
				getNumber(string.charAt(i));
			}
		}
		PrintWriter output =new PrintWriter("Q1-100502016.txt");
		output.print(string);


	}

	public static int getNumber(char letter) {

		if (letter >= 65 && letter <= 67 && letter >= 97 && letter <= 99) {
			return 2;
		} else if (letter >= 68 && letter <= 67 && letter >= 100
				&& letter <= 102) {
			return 3;
		} else if ((letter >= 71 && letter <= 73)
				|| (letter >= 103 && letter <= 105)) {
			return 4;
		} else if ((letter >= 74 && letter <= 76)
				|| (letter >= 106 && letter <= 108)) {
			return 5;
		} else if ((letter >= 77 && letter <= 79)
				|| (letter >= 109 && letter <= 111)) {
			return 6;
		} else if ((letter >= 80 && letter <= 82)
				|| (letter >= 112 && letter <= 115)) {
			return 7;
		} else if ((letter >= 84 && letter <= 86)
				|| (letter >= 116 && letter <= 118)) {
			return 8;
		} else if ((letter >= 87 && letter <= 90)
				|| (letter >= 119 && letter <= 122)) {
			return 9;
		}
		return letter;

	}
}
