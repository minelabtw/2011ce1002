package q1.s100502512;

import java.util.Scanner;
import javax.swing.*;

public class Q11 {
	public static void main(String[] args) {
		int result;
		String A;
		Scanner input = new Scanner(System.in);
		System.out.println("Please input a book number:");
		String ISBN = input.next();
		ISBN Test = new ISBN(ISBN);
		A = Test.getISBN(ISBN);
		result = checksum(A);
		String B=(ISBN+"-"+result);
		JOptionPane.showMessageDialog(null,B);
	}

	public static int checksum(String A) {
		
		int result = 0;
		for (int i = 0; i < 8; i++) {
			result = (result + A.charAt(i) * (i + 1)) % 11;
		}
		if(result ==10){
			System.out.print("X");
		}

		return result;
	}

}
