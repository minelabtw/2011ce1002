package q1.s995002203;
import java.io.*;
import java.util.*;

public class Q12 {
	public static void main( String[] args )throws Exception
	{
		File input=new File("src/q1/s995002203/input.txt");
		File output=new File("src/q1/s995002203/Q1-995002203.txt");
		PrintWriter cout=new PrintWriter(output);
		Scanner cin = new Scanner(input);
		String tel=cin.next();//讀取input.txt中的資料
		char ary[]=tel.toCharArray();//將string轉成char陣列
		for(int i=0;i<tel.length();i++){
			if(Character.isLetter(ary[i])&&ary[i]!='-')//若資料是字母且不是'-'
				ary[i]=getNumber(ary[i]);//把字母轉成數字字元
		}
		String newTel=new String(ary);//把char陣列轉成string
		cout.println(newTel);//輸出
		cin.close();
		cout.close();
	}
	public static char getNumber(char letter)
	{
		char tmp='0';
		switch(letter)
		{
			case 'A':case 'B':case 'C':
				tmp='2';
				break;
			case 'D':case 'E':case 'F':
				tmp='3';
				break;
			case 'G':case 'H':case 'I':
				tmp='4';
				break;
			case 'J':case 'K':case 'L':
				tmp='5';
				break;
			case 'M':case 'N':case 'O':
				tmp='6';
				break;
			case 'P':case 'Q':case 'R':case 'S':
				tmp='7';
				break;
			case 'T':case 'U':case 'V':
				tmp='8';
				break;
			case 'W':case 'X':case 'Y':case 'Z':
				tmp='9';
				break;
		}
		return tmp;
	}
}
