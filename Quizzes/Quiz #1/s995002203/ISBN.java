package q1.s995002203;

public class ISBN {
	ISBN(String input)
	{
		int tmp;
		for(int i=0;i<input.length();i++)
		{
			tmp=input.charAt(i)-'0';//字元轉數字
			d[i]=tmp;
		}
	}
	
	public String getIsbn(){
		d[9]=(d[0]*1+d[1]*2+d[2]*3+d[3]*4+d[4]*5+d[5]*6+d[6]*7+d[7]*8+d[8]*9)%11;//求出checksum
		char [] charAry=new char[11];
		int tmp;
		for(int i=0;i<9;i++)//將前九個isbn碼從
		{
			tmp=d[i]+'0';//數字轉成ascii
			charAry[i]=(char)tmp;//ascii轉型成char並存入char陣列
		}
		charAry[9]='-';
		if(d[9]==10)//若checksum等於10就存入'X'
			charAry[10]='X';
		else
		{
			tmp=d[9]+'0';
			charAry[10]=(char)tmp;
		}
		String result=new String(charAry);//char陣列轉乘string
		return result;
	}

	private int [] d=new int [10];//isbn前九碼
}
