package q1.s100502515;


public class ISBN {
	private String iSBN;
	
	public ISBN(String string) {
		iSBN = string;
	}
	
	private int d1 = Character.getNumericValue(iSBN.charAt(0));
	private int d2 = Character.getNumericValue(iSBN.charAt(1));
	private int d3 = Character.getNumericValue(iSBN.charAt(2));
	private int d4 = Character.getNumericValue(iSBN.charAt(3));
	private int d5 = Character.getNumericValue(iSBN.charAt(4));
	private int d6 = Character.getNumericValue(iSBN.charAt(5));
	private int d7 = Character.getNumericValue(iSBN.charAt(6));
	private int d8 = Character.getNumericValue(iSBN.charAt(7));
	private int d9 = Character.getNumericValue(iSBN.charAt(8));

	public String getISBN() {
		return iSBN;
	}

	public int calculateISBN() {
		return (d1 * 1 + d2 * 2 + d3 * 3 + d4 * 4 + d5 * 5 + d6 * 6 + d7 * 7
				+ d8 * 8 + d9 * 9) % 11;
	}
}
