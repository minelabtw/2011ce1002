package q1.s100502515;

import java.util.Scanner;

public class Q11 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter the inital number: ");
		String inputISBN = input.next();

		ISBN iISBN = new ISBN(inputISBN);
		
		System.out.print(iISBN.getISBN());

		if (iISBN.calculateISBN() == 10)
			System.out.println("The ISBN is " + iISBN.getISBN() + "-X");
		else
			System.out.println("The ISBN is " + iISBN.getISBN() + "-"
					+ iISBN.calculateISBN());
	}
}
