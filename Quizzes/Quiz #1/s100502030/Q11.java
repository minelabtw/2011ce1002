package q1.s100502030;

import javax.swing.*;

public class Q11 {

	public static int checksum(String p_isbn) {
		char[] dChar = new char[p_isbn.length()];
		int[] d = new int[p_isbn.length()];
		int sum = 0;
		for (int i = 0; i < 9; i++) {
			dChar[i] = p_isbn.charAt(i);
			d[i] = dChar[i] - '0';
			sum = sum + d[i] * (i+1);
		}
		return sum%11;
	}

	public static void main(String[] args) {

		String isbn = JOptionPane
				.showInputDialog("Please input a ISBN(d1~d9):");
		String d10;
		if(checksum(isbn)==10)
			d10="x";
		else d10 =""+ checksum(isbn);
		ISBN isbnx = new ISBN(isbn);
		JOptionPane.showMessageDialog(null, "The new ISBN is " + isbnx.getISBN()+'-'+d10);
	}
}
