package q1.s100502516;

import java.util.Scanner;
import java.io.*;

public class Q12 {
	public static void main(String[] args) throws Exception
	{
		File file = new File("input.txt");
		Scanner input = new Scanner(file);
		PrintWriter output = new PrintWriter("Q1-100502516.txt");
		String number = "";//store the output
		
		String program = input.next();		
		
		for(int i = 0; i < program.length(); i++)
		{
			if(Character.isLetter(program.charAt(i)) && program.charAt(i) != '-')//check is it a letter
				number += getNumber(program.charAt(i));
			else
				number += program.charAt(i);
		}
		
		output.print(number);
		
		input.close();
		output.close();
	}
	
	public static int getNumber(char letter)
	{
		int num = 0;
		
		switch(letter)//transform by the letter/number mapping
		{
			case 'A':
			case 'B':
			case 'C':
				num = 2;
				break;
			case 'D':
			case 'E':
			case 'F':
				num = 3;
				break;
			case 'G':
			case 'H':
			case 'I':
				num = 4;
				break;
			case 'J':
			case 'K':
			case 'L':
				num = 5;
				break;
			case 'M':
			case 'N':
			case 'O':
				num = 6;
				break;
			case 'P':
			case 'Q':
			case 'R':
			case 'S':
				num = 7;
				break;
			case 'T':
			case 'U':
			case 'V':
				num = 8;
				break;
			case 'W':
			case 'X':
			case 'Y':
			case 'Z':
				num = 9;
				break;
			case 'a':
			case 'b':
			case 'c':
				num = 2;
				break;
			case 'd':
			case 'e':
			case 'f':
				num = 3;
				break;
			case 'g':
			case 'h':
			case 'i':
				num = 4;
				break;
			case 'j':
			case 'k':
			case 'l':
				num = 5;
				break;
			case 'm':
			case 'n':
			case 'o':
				num = 6;
				break;
			case 'p':
			case 'q':
			case 'r':
			case 's':
				num = 7;
				break;
			case 't':
			case 'u':
			case 'v':
				num = 8;
				break;
			case 'w':
			case 'x':
			case 'y':
			case 'z':
				num = 9;
				break;				
		}
		
		return num;
	}
}
