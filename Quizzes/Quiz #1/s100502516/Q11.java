package q1.s100502516;

import javax.swing.JOptionPane;

public class Q11 {
	public static void main(String[] args)
	{
		String ISBNbefore = JOptionPane.showInputDialog("Enter first night digits: ");
		
		ISBN isbn = new ISBN(ISBNafter(ISBNbefore));
		
		JOptionPane.showMessageDialog(null, isbn.getISBNnum());
	}
	
	public static String ISBNafter(String ISBN)
	{
		char[] nums = new char[ISBN.length()];
		int checkSum = 0;
		String str = "";//store the answer
		
		for(int i = 0; i < ISBN.length(); i++)
			nums[i] = ISBN.charAt(i);
		
		for(int i = 0; i < ISBN.length(); i++)
		{			
			switch(nums[i])//calculate the checksum
			{
				case '0':
					checkSum += 0 * i+1;
					break;
				case '1':
					checkSum += 1 * i+1;
					break;
				case '2':
					checkSum += 2 * i+1;
					break;
				case '3':
					checkSum += 3 * i+1;
					break;
				case '4':
					checkSum += 4 * i+1;
					break;
				case '5':
					checkSum += 5 * i+1;
					break;
				case '6':
					checkSum += 6 * i+1;
					break;
				case '7':
					checkSum += 7 * i+1;
					break;
				case '8':
					checkSum += 8 * i+1;
					break;
				case '9':
					checkSum += 9 * i+1;
					break;
			}
		}
		
		checkSum %= 11;	
		
		if(checkSum == 10)
			str += ISBN + "-X";
		else
			str += ISBN + "-" + checkSum;
		
		return str;
	}
}
