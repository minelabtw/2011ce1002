package q1.s100502034;
import javax.swing.JOptionPane;
import q1.s100502034.ISBN;
public class Q11 {
	public static void main(String[] args){
		String input = ""; //給使用者輸入的String
		double number = 0; //用來把String變成double的變數
		input = JOptionPane.showInputDialog("please enter the number");//show一個訊息給使用 要他輸入首9個ISBN
		number = Double.parseDouble(input);//變成double
		int[] allNumber = calculate(number);//丟給calculate method去計算
		String all = "";//用來放計算好的東西
		for ( int i = 0 ; i< allNumber.length;i++){//把每一個字連起來
			if (i == 9){//最後一個要用 "-"
				all = all + "-";
			}
			if (allNumber[9] == 10 && i == 9){ //判斷是否10 就是給他變成X
				all = all + "X";
			}
			else{
				all = all + allNumber[i];
			}
		}
		ISBN isbn = new ISBN(all);//丟給class ISBN
		JOptionPane.showMessageDialog(null,isbn.getISBN());//輸出結果
	}
	public static int[] calculate(double no){
		int [] save = new int [10];//因為第十位的關係 所以要宣告[10]的陣列
		for ( int i = 0 ; i < 9 ;i++){
			save[i] = (int) (no /Math.pow(10, 8-i)); //我的方法是比較基礎啦 可能你不想我這樣做 就是用數學的方法得到不同的位數
		}
		for ( int i = 8; i>0; i--){
			save[i] = save[i] - save[i-1]*10;//就是位數之間互減得到一個個位數;
		}
		int d10 = 0;//d10
		for ( int i = 0;i<9;i++){//d10的計算公式
			d10 = d10 + save[i]*(i+1);
		}
		d10 = d10%11;//d10的計算公式的最後一步
		save[9] = d10;
		return save;
	}
}