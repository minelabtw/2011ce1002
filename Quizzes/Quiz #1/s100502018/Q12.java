package q1.s100502018;

import java.util.Scanner;
import java.io.*;

public class Q12 {
	public static void main(String[] args) throws Exception {
		File file1 = new File("src/q1/s100502018/input.txt");
		Scanner input = new Scanner(file1);
		String data = input.nextLine();
		char[] ch = new char[data.length()];
		for (int i = 0; i < data.length(); i++) {
			ch[i] = data.charAt(i);
		}
		File file2 = new File("src/q1/s100502018/Q1-100502018.txt");
		PrintWriter output = new PrintWriter(file2);
		for (int i = 0; i < data.length(); i++) {
			if (Character.isLetter(ch[i])) {
				output.print(getNumber(ch[i]));
			} else {
				output.print(ch[i]);
			}
		}
		input.close();
		output.close();
	}

	public static int getNumber(char letter) {
		if (letter == 'A' || letter == 'B' || letter == 'C') {
			return 2;
		} else if (letter == 'D' || letter == 'E' || letter == 'F') {
			return 3;
		} else if (letter == 'G' || letter == 'H' || letter == 'I') {
			return 4;
		} else if (letter == 'J' || letter == 'K' || letter == 'L') {
			return 5;
		} else if (letter == 'M' || letter == 'N' || letter == 'O') {
			return 6;
		} else if (letter == 'P' || letter == 'Q' || letter == 'R'
				|| letter == 'S') {
			return 7;
		} else if (letter == 'T' || letter == 'U' || letter == 'V') {
			return 8;
		} else if (letter == 'W' || letter == 'X' || letter == 'Y'
				|| letter == 'Z') {
			return 9;
		}else {
			return 0;
		}

	}
}
