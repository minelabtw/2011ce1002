package q1.s100502018;

import javax.swing.JOptionPane;

public class Q11 {
	public static void main(String[] args) {
		String input = JOptionPane.showInputDialog(null,
				"Please input the GUI", "INPUT",
				JOptionPane.INFORMATION_MESSAGE);
		ISBN myISBN = new ISBN(input);
		JOptionPane.showMessageDialog(null, "The complete ISBN is " + input
				+ "-" + myISBN.getResult());
	}
}
