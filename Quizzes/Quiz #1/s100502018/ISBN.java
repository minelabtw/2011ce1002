package q1.s100502018;

public class ISBN {
	private String initial;
	private String result = "";

	ISBN() {
		initial = "000000000";
	}

	ISBN(String s) {
		setInitial(s);
	}

	public void setInitial(String s) {
		initial = s;
	}

	public String getResult() {
		int sum = 0;
		char[] ch = new char[initial.length()];
		for (int i = 0; i < initial.length(); i++) {
			ch[i] = initial.charAt(i);
		}
		for (int i = 0; i < ch.length; i++) {
			sum += (ch[i] - 48) * (i + 1);
		}
		result += sum % 11;
		if (sum % 11 == 10) {
			return "X";
		} else {
			return result;
		}
	}
}
