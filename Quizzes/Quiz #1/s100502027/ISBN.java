package q1.s100502027;

public class ISBN {
	private String oldnumbers;  //the object to store the digits
	private String newnumbers;
	ISBN(String numbers){   //construct to set the numbers to object
		oldnumbers = numbers;
	}
	public String getoldnumbers(){
		return oldnumbers;
	}
	public String getnewnumbers(){
		return newnumbers;
	}
	public void calculate(){
		char[] numbers=new char[9];
		numbers = getoldnumbers().toCharArray();   //let the string turn to the char array
		int sum = 0;
		String result="";
		for (int t=0;t<9;t++){            //[for]to count the d1*1....d9*9 
			sum += (numbers[t]-'0')*(t+1) ;
			result +=numbers[t];
		}
		result += "-" + sum%11;
		newnumbers=result;
	}
}
