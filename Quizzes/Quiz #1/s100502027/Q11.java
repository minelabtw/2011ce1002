package q1.s100502027;

import javax.swing.JOptionPane;

public class Q11 {
	public static void main(String [] args){  //use the simple GUI to let user control
		String inputnumbers = JOptionPane.showInputDialog("Please enter nine digits to calculate the ISBN");//GUI-get the nine digits
		ISBN test = new ISBN(inputnumbers); //declared the class to use
		test.calculate(); // let nine digits to calculate the checksum
		JOptionPane.showMessageDialog(null,test.getnewnumbers()); // GUI-show the ISBN 
	}
}
