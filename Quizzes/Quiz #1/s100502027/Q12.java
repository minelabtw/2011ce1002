package q1.s100502027;

import java.io.*;
import java.util.*;

public class Q12 {
	public static void main(String[] args) throws IOException{
		File sourcefile = new File("input.txt");   //set the file which is used 
		File targetfile = new File("Q1-100502027.txt");
		Scanner input = new Scanner(sourcefile);
		PrintWriter output = new PrintWriter(targetfile);
		String sampleinput = input.next();
		char[] letters = sampleinput.toCharArray();   //let the string turn to the char array
		for(int t=0 ; t < letters.length ; t++){
			
			if(Character.isLetter(letters[t])==true){   //let the char turn to needs 
				output.print(getNumber(letters[t]));
			}
			else{
				output.print(letters[t]);
			}
		}
		input.close();
		output.close();
	}
	
	//this method use to convert the letter to number 
	public static int getNumber(char letter){        
		if(letter>='A'){
			if(letter > 'C'){
				if(letter > 'F'){
					if(letter > 'I'){
						if(letter > 'L'){
							if(letter>'O'){
								if(letter > 'S'){
									if(letter >'V'){
										return 9;
									}
									else{
										return 8 ;
									}
								}
								else{
									return 7;
								}
							}
							else{
								return 6;
							}
						}
						else{
							return 5;
						}
					}
					else {
						return 4;
					}
				}
				else{
					return 3;
				}
			}
			else
			{
				return 2 ;
			}
		}
		else{
			return letter -'0';
		}
	}
}
