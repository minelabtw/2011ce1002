package q1.s100502010;

import q1.s100502010.ISBN;

public class ISBN 
{
	private static int array[]=new int[9];
	private static int answer1;
	public ISBN(int array1[])
	{
		array=array1;
		int last=ISBN.calculate();
		ISBN.setISBNlast(last);
	}
	public static int calculate()
	{
		int temp=0;
		for(int counter=0;counter<=8;counter++)
		{
			temp=temp+array[counter]*(counter+1);
		}
		int answer=temp%11;
		return answer;
	}
	public static void setISBNlast(int last)
	{
		answer1=last;
		
	}
	public static int returnISBN()
	{
		return answer1;
	}

}