//Q11.java
package q1.s100502502;
import javax.swing.JOptionPane;
public class Q11 {
	public static void main(String[] args){
		String E = JOptionPane.showInputDialog("Enter night digits of ISBN :");
		ISBN s = new ISBN(); 
		s.setISBN(E);
		String d10 = ISBNnum(s.getISBN());
		String b = s.getISBN() +"-"+ d10;
		s.setISBN(b);
		for(int i = 0; i < 9; i++){
			System.out.print(s.getISBN().charAt(i));
		}
		System.out.print("-" + s.getISBN().charAt(9));
	}
	public static String ISBNnum(String a){
		char[] array = new char[9];
		String checksum = "";
		int d = 0;
		for(int i= 0; i <9; i++){
			array[i] = a.charAt(i);
		}
		d = (array[0]*1+array[1]*2+array[2]*3+array[3]*4+array[4]*5+array[5]*6+array[6]*7+array[7]*8+array[8]*9)%11;
		if(d == 10)
			checksum = "X";
		else
			checksum = "" + d;
		return checksum;
	}
}
