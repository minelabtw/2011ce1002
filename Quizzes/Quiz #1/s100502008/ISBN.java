package q1.s100502008;

public class ISBN {
	private int[] array = new int[9];
	private String d10 = "", inISBN;
	String temp = "";

	public ISBN(String innum) {//constructor
		inISBN = innum;
	}

	public String getISBN() {//output result
		return inISBN + "-" + d10;
	}

	public void Cald10() {//compute d10
		for (int i = 0; i < 9; i++) {
			temp = "";
			temp += inISBN.charAt(i);//char to str
			array[i] = Integer.parseInt(temp);//str to int
		}
		int sum = 0;
		for (int i = 0; i < 9; i++) {
			sum += array[i] * (i + 1);
		}
		if (sum % 11 == 10) {
			d10 = "X";
		} else {
			d10 = d10 + sum % 11;
		}
	}
}
