package q1.s100502008;

import q1.s100502008.ISBN;
import javax.swing.JOptionPane;

public class Q11 {
	public static void main(String[] args) {
		ISBN myISBN;
		String input;
		input = JOptionPane.showInputDialog(null, "Fill in ISBN: ");//input
		myISBN = new ISBN(input);//pass to constructor
		myISBN.Cald10();//compute
		JOptionPane.showMessageDialog(null,
				"The complete ISBN is : " + myISBN.getISBN());//output
	}
}
