package q1.s100502008;
import java.util.Scanner;

public class Q12 {
	public static void main(String[] args)
	throws Exception{
		java.io.File file=new java.io.File("src/q1/s100502008/input.txt");//input file
		java.io.PrintWriter output=new java.io.PrintWriter("src/q1/s100502008/output.txt");//output file
		Scanner input=new Scanner(file);
		String instr,outstr="";
		instr=input.nextLine();
		for(int i=0;i<instr.length();i++)
		{
			if(Character.isLetter(instr.charAt(i)))//check
			{
				if(instr.charAt(i)=='A'||instr.charAt(i)=='B'||instr.charAt(i)=='C')//case 2
				{
					outstr+="2";
				}
				else if(instr.charAt(i)=='D'||instr.charAt(i)=='E'||instr.charAt(i)=='F')//case 3
				{
					outstr+="3";
				}
				else if(instr.charAt(i)=='G'||instr.charAt(i)=='H'||instr.charAt(i)=='I')//case 4
				{
					outstr+="4";
				}
				else if(instr.charAt(i)=='J'||instr.charAt(i)=='K'||instr.charAt(i)=='L')//case 5
				{
					outstr+="5";
				}
				else if(instr.charAt(i)=='M'||instr.charAt(i)=='N'||instr.charAt(i)=='O')//case 6
				{
					outstr+="6";
				}
				else if(instr.charAt(i)=='P'||instr.charAt(i)=='Q'||instr.charAt(i)=='R'||instr.charAt(i)=='S')//case 7
				{
					outstr+="7";
				}
				else if(instr.charAt(i)=='T'||instr.charAt(i)=='U'||instr.charAt(i)=='V')//case 8
				{
					outstr+="8";
				}
				else//case 9
				{
					outstr+="9";
				}
			}
			else//other
			{
				outstr+=instr.charAt(i);
			}
		}
		output.print(outstr);//output result
		output.close();
	}
}
