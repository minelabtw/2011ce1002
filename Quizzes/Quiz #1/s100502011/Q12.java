package q1.s100502011;

import java.util.Scanner;
import java.io.*;

public class Q12 {
	public static void main(String[] args) throws Exception{
		String number = ""; //the result phone number
		java.io.File file = new java.io.File("input.txt"); // read file
		Scanner input = new Scanner(file); // store to input
		String phone = input.nextLine(); // set to phone
		
		for(int i=0;i<phone.length();i++){ // determine it is char or not and store to string 
			if(Character.isLetter(phone.charAt(i))){ //the element at phone is char
				number +=getNumber(phone.charAt(i)); 
			}
			else //the element at phone is number or other
				number +=phone.charAt(i);
		}
		
		PrintWriter output =new PrintWriter("Q1-100502011.txt"); // output file
		output.println(number); // store to output
		input.close(); // close file
		output.close(); // close file
	}
    public static int getNumber(char letter){ // change char to number
    	if(letter <='C'){ // A~C
    		return 2;
    	}
    	else if(letter <='F'){ //D~F
    		return 3;
    	}
    	else if(letter <='I'){ //G~I
    		return 4;
    	}
    	else if(letter <='L'){ //J~L
    		return 5;
    	}
    	else if(letter <='O'){ //M~O
    		return 6;
    	}
    	else if(letter <='S'){ //P~S
    		return 7;
    	}
    	else if(letter <='V'){ //T~V
    		return 8;
    	}
    	else //W~Z
    		return 9;
    }
	

}
