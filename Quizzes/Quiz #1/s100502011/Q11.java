package q1.s100502011;

import javax.swing.JOptionPane;
public class Q11 {
	public static void main(String[] args){
		int d10=0; //the last number of ISBN
		String input = JOptionPane.showInputDialog(null,"please enter first nine digits of the ISBN"); //input
		d10=calculateCheck(input); // calculate d10
		if(d10==10){ //10 change to X
			ISBN isbn = new ISBN(input +"-X");
			JOptionPane.showMessageDialog(null,"The entire ISBN is\n "+isbn.getISBN());
		}
		else{ // 0~9
			ISBN isbn = new ISBN(input +"-" + d10);
			JOptionPane.showMessageDialog(null,"The entire ISBN is\n "+isbn.getISBN());
		}
	}
	
	public static int calculateCheck(String s){ // calculate d10
		int sum=0;
		int result=0;
		for(int i=0;i<s.length();i++){ // sum
			char digitsChar = s.charAt(i);
			sum +=(digitsChar-48)*(i+1);
		}
		result = sum%11; //d10
		return result;
	}
}
