package q1.s100502520;

import java.util.Scanner;

public class Q11 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		System.out.println("please input a ISBN: ");
		String isbnString = input.next();
		ISBN use = new ISBN(isbnString);
		use.calculate();
		System.out.println("the result is: ");
		System.out.println(isbnString + "-" + use.getISBN());
	}
}
