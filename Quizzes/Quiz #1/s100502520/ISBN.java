package q1.s100502520;

public class ISBN {
	private char[] isbn = new char[9];
	private int[] isbn2 = new int[9];
	private int d10;
	
	public ISBN(String input){
		isbn = input.toCharArray();
		for(int i = 0; i<9; i++){
			isbn2[i]=Character.getNumericValue(isbn[i]);
		} 
	}

	public int getISBN(){
		return d10;
	}

	public void calculate(){
		int result = 0;
		for(int i =0; i<9; i++){
			result = result +isbn2[i]*(i+1);
		}
		d10 = result % 11;
	}

}