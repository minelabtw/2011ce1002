package q1.s100502007;
import java.util.Scanner;
public class Q12 {
	public static void main(String[] args)throws Exception{
		
		java.io.File file = new java.io.File("input.txt");
		Scanner word = new Scanner(file);
		String number=word.next();
		int [] array=new int[number.length()];
		String newword=" ";
		for(int i=0;i<number.length();i++){
			
			if(Character.isLetter(number.charAt(i))==true){
				array[i]=getNumber(number.charAt(i));
				newword+=array[i];
			}
			else{
				newword+=number.charAt(i);
			}
		}

		java.io.PrintWriter output = new java.io.PrintWriter("Q1-100502007.txt");
		output.print(newword);
		word.close();
		output.close();

	}
	public static int getNumber(char letter){
		int number=0;
		if(letter=='A'||letter=='a'||letter=='B'||letter=='b'||letter=='C'||letter=='c'){
			number=2;
		}
		else if(letter=='D'||letter=='d'||letter=='E'||letter=='e'||letter=='F'||letter=='f'){
			number=3;
		}
		else if(letter=='G'||letter=='g'||letter=='H'||letter=='h'||letter=='I'||letter=='i'){
			number=4;
		}
		else if(letter=='J'||letter=='j'||letter=='K'||letter=='k'||letter=='L'||letter=='l'){
			number=5;
		}
		else if(letter=='M'||letter=='m'||letter=='N'||letter=='n'||letter=='O'||letter=='o'){
			number=6;
		}
		else if(letter=='P'||letter=='p'||letter=='Q'||letter=='q'||letter=='R'||letter=='r'||letter=='S'||letter=='s'){
			number=7;
		}
		else if(letter=='T'||letter=='t'||letter=='U'||letter=='u'||letter=='V'||letter=='v'){
			number=8;
		}
		else if(letter=='W'||letter=='w'||letter=='X'||letter=='x'||letter=='Y'||letter=='y'||letter=='Z'||letter=='z'){
			number=9;
		}
		return number;
	}
}
