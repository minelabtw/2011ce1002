package q1.s100502007;
import javax.swing.JOptionPane;

public class Q11 {
	public static void main(String[] args){
		String input = JOptionPane.showInputDialog("please input the number : ");
		ISBN isbn=new ISBN(input);
		JOptionPane.showMessageDialog(null,input+"-"+checksum(isbn.get()));
	}
	public static int checksum(String input){

		int total=0;
		for(int i=0;i<input.length();i++){
			int temp = input.charAt(i)-'0';
			total+=temp;
		}
		if(total%11==10){
			return 'x';
		}
		else{
		return total%11;
		}
	}
}
