package q1.s982003034;

public class ISBN {
	
	private int[] isbn;	
	private int checksum;
	
	public ISBN () {
		this (0, 0, 0, 0, 0, 0, 0, 0, 0);
	}
	
	public ISBN (
			int d1, int d2, int d3,
			int d4, int d5, int d6,
			int d7, int d8, int d9
									) { // ISBN constructor begin
		
		isbn = new int [] {
			d1, d2, d3,
			d4, d5, d6,
			d7, d8, d9
			};
		
		calcChecksum ();
		
	} // ISBN constructor end
	
	public void calcChecksum () {
		
		checksum = 0;
		
		for (int i = 0; i <9; i++) {
			checksum += (isbn[i])*(i+1);
		}
		
		checksum %= 11;
	}
		
	public void setISBN (int num, int pos) {
		
		isbn[pos] = num;
		
	}
	
	public boolean setISBN (String input) {
		
		if (input.length() == 9) {
			
			for (int i = 0; i < 9; i++) {
				
				if (Character.isDigit(input.charAt(i)))
				isbn[i] = (int)input.charAt(i) - 48;
				else return false; // input content check
			
			}
			
			calcChecksum();
			return true;
			
		}
		
		else return false; // input length check
		
	}
	
	public String getFullISBN () {
		String temp = "";
		for (int i = 0; i < 9; i ++) {
			temp = temp + isbn[i];
		}
		if (checksum == 10) temp = temp + "-X";
		else temp = temp + "-" + checksum;
		return temp;
	}
}
