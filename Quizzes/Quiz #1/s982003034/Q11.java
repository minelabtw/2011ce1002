package q1.s982003034;

import javax.swing.JOptionPane;

public class Q11 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String temp = JOptionPane.showInputDialog("Please insert the ISBN");
		ISBN isbn = new ISBN();
		if (isbn.setISBN(temp)) JOptionPane.showMessageDialog(null, "ISBN:" + isbn.getFullISBN());

		else JOptionPane.showMessageDialog (null, "Wrong ISBN format");
		
	}

}
