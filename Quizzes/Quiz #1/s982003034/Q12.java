package q1.s982003034;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Q12 {

	/**
	 * @param args
	 * @throws IOException 
	 */
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader br = new BufferedReader (new FileReader ("input.txt"));
		BufferedWriter bw = new BufferedWriter (new FileWriter ("Q1-982003034.txt"));
		
		String temp = br.readLine();
		String result = convert (temp);
		bw.write(result);
		bw.close();
		
		
	}
	
	public static String convert (String input) {
		
		String result = "";
		int num = -1;
		
		for (int i = 0; i < input.length(); i++) {
			
			num = getNumber (input.charAt(i));
			// actually I prefer getNumber to return char
			// because in such way, there is no need to check num
			// but still the test required getNumber to return int :((
			if (num == -1) {
				result += input.charAt(i);
			}
			else result += num;
		}
		return result;
		
	}
	
	public static int getNumber (char letter) {
		
		switch (letter) {
		
		case 'A': case 'B': case 'C': return 2;
		case 'D': case 'E': case 'F': return 3;
		case 'G': case 'H': case 'I': return 4;
		case 'J': case 'K': case 'L': return 5;
		case 'M': case 'N': case 'O': return 6;
		case 'P': case 'Q': case 'R': case 'S': return 7;
		case 'T': case 'U': case 'V': return 8;
		case 'W': case 'X': case 'Y': case 'Z': return 9;
		
		// I decided that the program will still accept lowercase letters
		case 'a': case 'b': case 'c': return 2;
		case 'd': case 'e': case 'f': return 3;
		case 'g': case 'h': case 'i': return 4;
		case 'j': case 'k': case 'l': return 5;
		case 'm': case 'n': case 'o': return 6;
		case 'p': case 'q': case 'r': case 's': return 7;
		case 't': case 'u': case 'v': return 8;
		case 'w': case 'x': case 'y': case 'z': return 9;
		
		default: return -1; // for every other mistake
		}
	}
	
	/*
	 // if getNumber were to return char, it will look like this
	 // my main concern is that the getNumber function already done what Character.isLetter done
	 
	 	public static int getNumber (char letter) {
		
		switch (letter) {
		
		case 'A': case 'B': case 'C': return '2';
		case 'D': case 'E': case 'F': return '3';
		case 'G': case 'H': case 'I': return '4';
		case 'J': case 'K': case 'L': return '5';
		case 'M': case 'N': case 'O': return '6';
		case 'P': case 'Q': case 'R': case 'S': return '7';
		case 'T': case 'U': case 'V': return '8';
		case 'W': case 'X': case 'Y': case 'Z': return '9';
		
		// I decided that the program will still accept lowercase letters
		case 'a': case 'b': case 'c': return '2';
		case 'd': case 'e': case 'f': return '3';
		case 'g': case 'h': case 'i': return '4';
		case 'j': case 'k': case 'l': return '5';
		case 'm': case 'n': case 'o': return '6';
		case 'p': case 'q': case 'r': case 's': return '7';
		case 't': case 'u': case 'v': return '8';
		case 'w': case 'x': case 'y': case 'z': return '9';
		
		default: return letter; // return the original char if it is not letter
		}
		
		
		// *******************************************************
		// this way the function convert can "brute force" input.charAt (i) without further need of checking
		
		public static String convert (String input) {
		
			String result = "";
			
			for (int i = 0; i < input.length(); i++) {	
				result += getNumber (input.charAt(i));
			}
			return result;	
		}
		
	 */

}
