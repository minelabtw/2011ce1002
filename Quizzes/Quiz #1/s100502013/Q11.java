package q1.s100502013;
import javax.swing.JOptionPane;
import java.util.Scanner;

public class Q11 {
	public static void main(String args[]){
		Scanner input = new Scanner(System.in);
		JOptionPane.showInputDialog(null,"Please enter the first nine ISBN number.");
		String ISBN9 = input.next();
		ISBN9 += checksum(ISBN9);
		ISBN userISBN = new ISBN(ISBN9);
		JOptionPane.showMessageDialog(null, userISBN.getISBN());
	}
	
	int checksum(String a){
		int d10=0;
		for(int j=0;j<9;j++){
			d10 += a.charAt(j) * (j+1);
		}
		return d10;
	}
}
