package q1.s100502013;
import java.io.*;
import java.util.Scanner;

public class Q12 {
	public static void main(String args[]) throws Exception{
		File myfile = new File("input.txt");
		Scanner input = new Scanner(myfile);
		String mynumber = input.next();
		char[] phonenum = new char[13];
		String result = "";
		for(int i=0;i<13;i++){
			phonenum[i] = mynumber.charAt(i);
		}
		for(int j=0;j<13;j++){
			if(Character.isLetter(phonenum[j]))
				phonenum[j] = getNumber(phonenum[j]);
			else
				result += phonenum[j];
		}
		PrintWriter outfile = new PrintWriter("Q1-100502013.txt");
		outfile.print(phonenum);
		input.close();
		outfile.close();
	}
	
	char getNumber(char letter){
		if(letter == 'A' || letter == 'B' || letter == 'C')
			return '2';
		else if(letter == 'D' || letter == 'E' || letter == 'F')
			return '3';
		else if(letter == 'G' || letter == 'H' || letter == 'I')
			return '4';
		else if(letter == 'J' || letter == 'K' || letter == 'L')
			return '5';
		else if(letter == 'M' || letter == 'N' || letter == 'O')
			return '6';
		else if(letter == 'P' || letter == 'Q' || letter == 'R' || letter == 'S')
			return '7';
		else if(letter == 'T' || letter == 'U' || letter == 'V')
			return '8';
		else 
			return '9';
	}
}
