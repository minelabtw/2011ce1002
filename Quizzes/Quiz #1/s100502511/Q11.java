package q1.s100502511;

import java.util.Scanner;
import javax.swing.*;

public class Q11 {
	public static int checksum(String ISBN) {
		int checksum = 0;
		int left = 0;
		for (int i = 0; i <= 8; i++) {
			checksum += (ISBN.charAt(i) * (i + 1) - (48 * (i + 1))) / (i + 1);
		}
		left = checksum % 11;
		if (left <= 9)
			return left;
		else
			return 10;
	}

	public static void main(String[] args) {
		String output = "";
		Scanner input = new Scanner(System.in);
		String ISBN = input.next();

		ISBN isbn = new ISBN(ISBN);
		output = output + isbn.getISBN();
		System.out.print("-");
		output = output + "-";
		if (checksum(ISBN) != 10) {
			System.out.print(checksum(ISBN));
			output = output + checksum(ISBN);
		} else if (checksum(ISBN) == 10) {
			System.out.print("X");
			output = output + "X";
		}
		JOptionPane.showMessageDialog(null, output);
	}
}
