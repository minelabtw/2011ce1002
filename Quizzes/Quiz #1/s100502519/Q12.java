package q1.s100502519;
import java.util.Scanner;
public class Q12 {
	public static void main(String[]args)throws Exception{

		
		java.io.File file1 = new java.io.File("input.txt");
		Scanner input = new Scanner(file1);
		String words = input.next();
		input.close();
		String nums = null;
		for(int i=0;i<words.length();i++){
			words=words+getNumber(words.charAt(i));
		}
		java.io.File file2 = new java.io.File("Q1-100502519.txt");
		java.io.PrintWriter output = new java.io.PrintWriter(file2);
		output.print(words);
		output.close();
	}
	
	static int getNumber(char letter){
		
		if(letter == 'A' || letter == 'B' || letter == 'C'){
			return 2;
		}
		else if(letter == 'D' || letter == 'E' || letter == 'F' ){
			return 3;
		}
		else if(letter == 'G' || letter == 'H' || letter == 'I'){
			return 4;
		}
		else if(letter == 'J' || letter == 'K' || letter == 'L'){
			return 5;
		}
		else if(letter == 'M' || letter == 'N' || letter == 'O'){
			return 6;
		}
		else if(letter == 'P' || letter == 'Q' || letter == 'R' || letter == 'S'){
			return 7;
		}
		else if(letter == 'T' || letter == 'U' || letter == 'V'){
			return 8;
		}
		else{
			return 9;
		}
	}
	
	
}
