package q1.s100502519;

public class ISBN {

	private int a;
	private int b;
	private int c;
	private int d;
	private int e;
	private int f;
	private int g;
	private int h;
	private int i;
	
	
	public ISBN(String nine){
		a = nine.charAt(0)-'0';
		b = nine.charAt(1)-'0';
		c = nine.charAt(2)-'0';
		d = nine.charAt(3)-'0';
		e = nine.charAt(4)-'0';
		f = nine.charAt(5)-'0';
		g = nine.charAt(6)-'0';
		h = nine.charAt(7)-'0';
		i = nine.charAt(8)-'0';
	}
	
	
	int calculate(){
		int ten;
		ten = (a*1+b*2+c*3+d*4+e*5+f*6+g*7+h*8+i*9)%11;
		if(ten!=10){
			return ten;
		}
		else
			return 'x';
	}
}
