package q1.s100502505;

import java.util.Scanner;

import javax.swing.JOptionPane;

public class Q11 {
	public static void main(String arg[])
	{
		String d1;
		int[] array= new int[10];
		System.out.print("Please input the nine digits:");
		//String in = JOptionPane.showInputDialog(null,"Please input the nine digits:");
		Scanner input = new Scanner(System.in);
		d1 = input.nextLine();
		for(int i=0;i<9;i++)
		{
			array[i] = d1.charAt(i);
		}
		ISBN isbn = new ISBN(array[0],array[1],array[2],array[3],array[4],array[5],array[6],array[7],array[8],array[9]);
		isbn.get();
		Q11 a = new Q11();
		System.out.println(d1 + "-" + a.checksum(array[0],array[1],array[2],array[3],array[4],array[5],array[6],array[7],array[8]));
		//String out = JOptionPane.showMessageDialog(null,d1 + "-" + a.checksum(array[0],array[1],array[2],array[3],array[4],array[5],array[6],array[7],array[8]));
	}
	
	public int checksum(int a,int b,int c,int d,int e,int f,int g,int h,int i)
	{
		int j = (a*1+b*2+c*3+d*4+e*5+f*6+g*7+h*8+i*9)%11;
		return j;
	}
}
