package q1.s100502510;

import java.util.Scanner;
import javax.swing.*;

public class Q11 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Sample input:");

		String isbn = input.nextLine();

		ISBN iSBN = new ISBN(isbn);
		iSBN.setisbn(isbn);
		JOptionPane.showMessageDialog(null,
				"Sample output:" + calculate(iSBN.getisbn()));

	}

	public static String calculate(String inputed) {
		int checksum = 0;
		String result;
		for (int times = 0; times < 9; times++) {
			checksum += (inputed.charAt(times) - 48) * (times + 1);
		}
		if (checksum % 11 == 10) {
			result = (inputed + "-x");
			return result;
		} else {
			result = (inputed + "-" + (checksum % 11));
			return result;
		}
	}
}
