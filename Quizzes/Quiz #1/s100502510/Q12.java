package q1.s100502510;

import java.util.Scanner;

public class Q12 {
	public static void main(String[] args) throws Exception {
		java.io.File file = new java.io.File("input.txt");
		java.io.File filetwo = new java.io.File("Q1-school number.txt");
		java.io.PrintWriter output = new java.io.PrintWriter(filetwo);
		Scanner input = new Scanner(file);
		String inputed="";
		System.out.print("Sample input:");
		while (input.hasNext()) {

			inputed = input.nextLine();
		}
		output.print("sample output:");
		for (int b = 0; b <= 5; b++) {
			output.print(inputed.charAt(b));
		}
		for (int a = 6; a <= 12; a++) {
			if (Character.isLetter(inputed.charAt(a))) {

				output.print(getNumber(inputed.charAt(a)));

			} else {
				output.print("It's not a letter!");
			}
		}
	}

	public static int getNumber(char letter) {
		if (letter == 'A' || letter == 'B' || letter == 'C') {
			return 2;
		} else if (letter == 'D' || letter == 'E' || letter == 'F') {
			return 3;
		} else if (letter == 'G' || letter == 'H' || letter == 'I') {
			return 4;
		} else if (letter == 'J' || letter == 'K' || letter == 'L') {
			return 5;
		} else if (letter == 'M' || letter == 'N' || letter == 'O') {
			return 6;
		} else if (letter == 'P' || letter == 'Q' || letter == 'R'
				|| letter == 'S') {
			return 7;
		} else if (letter == 'T' || letter == 'U' || letter == 'V') {
			return 8;
		} else if (letter == 'W' || letter == 'X' || letter == 'Y'
				|| letter == 'Z') {
			return 9;
		} else {
			return letter - 48;
		}

	}
}
