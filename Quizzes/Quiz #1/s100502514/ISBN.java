/** class ISBN, Quiz 1-1 100502514 ce1002 20120322 */
package q1.s100502514;

public class ISBN {
	/** Private Members */
	private String isbn;
	
	/** Default and Specified Constructors */
	public ISBN(String isbn){
		this.isbn = isbn;
	}
	public ISBN(){
		this("");
	}
	
	/** Set and Get Methods */
	public void setISBN(String isbn){
		this.isbn = isbn;
	}
	public String getISBN(){
		return isbn;
	}
	
}
