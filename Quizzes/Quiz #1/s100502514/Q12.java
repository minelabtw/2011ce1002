/** Main class, Quiz 1-2 100502514 ce1002 20120322 */
package q1.s100502514;

import java.io.*;
import java.util.*;

public class Q12 {
	public static void main(String[] args){
		/** Declare a file object */
		File inputFile = new File("input.txt");
		
		/** Load and access a file in a try-catch statement */
		try{
			Scanner inputScanner = new Scanner(inputFile);
			PrintWriter outputBuffer = new PrintWriter("Q1-100502514.txt");
			while(inputScanner.hasNext()){
				outputBuffer.println(getTranslatedNumber(inputScanner.next()));
			}			
			inputScanner.close();
			outputBuffer.close();
			System.out.println("Succeed!!");
		}catch(FileNotFoundException err){
			System.err.println("File Not Found!!");
		}
	}
	
	public static String getTranslatedNumber(String orig){
		String newstr = "";
		for(int i=0;i<orig.length();i++){
			int n=getNumber(orig.charAt(i));
			if(n>=0)newstr+=n;
			else newstr+=orig.charAt(i);
		}
		return newstr;
	}
	
	public static int getNumber(char letter){
		if(Character.isLetter(letter)){
			String str_begin_indexes = "ADGJMPTW"; //from 2 to 9
			for(int i=0;i<str_begin_indexes.length();i++){
				if(letter>='a')letter-=('a'-'A'); //to upper case
				if(letter < str_begin_indexes.charAt(i))return i+1;
			}
			return 9; //if WXYZ, not true in all conditions in for-loop
		}else{
			return -1; //non-letter, not to be transferred.
		}
	}
}
