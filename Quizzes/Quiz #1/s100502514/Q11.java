/** Main class, Quiz 1-1 100502514 ce1002 20120322 */
package q1.s100502514;

import javax.swing.*;

public class Q11 {
	public static void main(String[] args){
		/** Each phase in the loop is for each input&output */
		while(true){
			String input="";
			/** The inner loop ends if correct input */
			while(true){
				try{
					input = JOptionPane.showInputDialog("Please input the first 9 digits of ISBN:");
					/** Break if correct input */
					break;
				}catch(Exception err){
					JOptionPane.showMessageDialog(null,"Invalid Value!! Please Try Again!!");
				}
			}
			JOptionPane.showMessageDialog(null,"The whole ISBN code with checksum:\n" +
					getISBNWithChecksum(new ISBN(input)).getISBN());
			if(JOptionPane.showConfirmDialog(null, "Would you like to input again?")
					!=JOptionPane.OK_OPTION){
				JOptionPane.showMessageDialog(null,"Goodbye!!");
				/** Quit the loop and program */
				return;
			}
		}
	}
	
	/** Calculating the checksum and Return a new ISBN object with whole codes */
	public static ISBN getISBNWithChecksum(ISBN input){
		String code = input.getISBN();
		/** Return an error message if string length is not correct */
		if(code.length()!=9) return new ISBN("Invalid Length Input");
		
		int sum = 0;
		for(int i=0;i<9;i++){
			int digit = (int)(code.charAt(i)-'0');
			/** Return an error message if found a non-digit character */
			if(digit>9 || digit<0) return new ISBN("Invalid Character Input");
			sum+=digit*(i+1);
		}
		/** Let values to be in 0~10 */
		sum%=11;
		
		/** Return the calculated result */
		return new ISBN( code + "-" + ((sum==10)?"X":sum) );
	}
}
