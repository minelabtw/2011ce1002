package q1.s100502009;
import javax.swing.JOptionPane;
import java.lang.Integer;

public class Q11 {
	public static void main(String[] args)
	{
		String input=JOptionPane.showInputDialog("Please enter the first nine digits: ");
		ISBN isbn=new ISBN(input);
		String output="The ISBN is: "+isbn.get()+"-"+calculate(isbn.get());
		JOptionPane.showMessageDialog(null,output);		
	}
	
	public static int calculate(String number)
	{
		int total=0;
		for(int i=0;i<number.length();i++)
		{
			int num=number.charAt(i)-'0';
			total+=num*(i+1);
		}
		if(total%11>=10)
			return 'X';
		else
			return total%11;
	}

}
