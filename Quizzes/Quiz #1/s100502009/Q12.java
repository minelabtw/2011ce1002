package q1.s100502009;
import java.util.Scanner;

public class Q12 {
	public static void main(String[] args)throws Exception
	{
		java.io.File file=new java.io.File("input.txt"); 
		Scanner input=new Scanner(file);
		String number=input.next();		
		int[] array=new int[number.length()];
		String op="";
		for(int i=0;i<number.length();i++)
		{	
			if(Character.isLetter(number.charAt(i))==true)
			{
				array[i]=getNumber(number.charAt(i));
				op+=array[i];
			}
			else				
				op+=number.charAt(i);
		}	
		java.io.PrintWriter output=new java.io.PrintWriter("Q1-100502009.txt"); 
		output.print(op);
		input.close();
		output.close();
	}
	public static int getNumber(char letter)
	{
		int number=0;		
		if(letter=='A'||letter=='B'||letter=='C')
		{	number=2;
		}
		else if(letter=='D'||letter=='E'||letter=='F')
		{
			number=3;
		}
		else if(letter=='G'||letter=='H'||letter=='I')
		{	
			number=4;	
		}
		else if(letter=='J'||letter=='K'||letter=='L')
		{	
			number=5;
		}
		else if(letter=='M'||letter=='N'||letter=='O')
		{	
			number=6;
		}
		else if(letter=='P'||letter=='Q'||letter=='R'||letter=='S')
		{	
			number=7;
		}
		else if(letter=='T'||letter=='U'||letter=='V')
		{	
			number=8;
		}
		else if(letter=='W'||letter=='X'||letter=='Y'||letter=='Z')
		{	
			number=9;		
		}
		return number;
	}
}
