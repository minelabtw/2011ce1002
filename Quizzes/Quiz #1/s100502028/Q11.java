package q1.s100502028;
import javax.swing.*;
public class Q11 {
	public static void main(String[] args){

		String numberOfString = JOptionPane.showInputDialog("Enter 9 digits: ");
		ISBN object = new ISBN(numberOfString);
		checksum(object.getISBN());
	}
	
	public static void checksum(String sum){
		int output = 0; 
		for(int i=0;i<sum.length();i++){
			int[] intArray = new int[sum.length()];
			intArray[i] = sum.charAt(i);
			output += intArray[i] * (i+1);
		}
		int result = output % 11;
		if(result == 10)
			JOptionPane.showMessageDialog(null, sum + "-X");
		else
			JOptionPane.showMessageDialog(null, sum + "-" + result);
	}
}
