package q1.s100502003;

import java.util.Scanner;
import javax.swing.*;

public class Q11 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		ISBN Isbn = new ISBN();
		String isbn_in;
		JOptionPane.showInputDialog("Please input a string with nine numbers: ");
		isbn_in = input.next();
		Isbn.set_ISBN(checksum(isbn_in));
		String result;
		result = isbn_in+"-"+Isbn.get_ISBN();
		JOptionPane.showMessageDialog(null, result);
		
		
	}
	public static int checksum(String d1to9) {
		int[] result = new int[9];
		int sum = 0;
		for(int j=0; j<d1to9.length(); j++) {
			result[j] = d1to9.charAt(j);
		}
		for(int k=0; k<9; k++) {
			sum = sum + result[k]*k;
		}
		return (sum%11);
	}
}
