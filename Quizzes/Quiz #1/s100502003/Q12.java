package q1.s100502003;

import java.util.Scanner;
import java.io.*;

public class Q12 {
	public static void main(String[] args) throws Exception{
		File file = new File("input.txt");
		Scanner input = new Scanner(file);
		String Input = input.next();
		String letters = Input.toUpperCase();
		int[] eachletter = new int[Input.length()];
		for(int i=0; i<Input.length(); i++) {
			eachletter[i] = getNumber(letters.charAt(i));
			if(eachletter[i] == 10) {
				eachletter[i] = letters.charAt(i);
			}
		}
		String result = "";
		for(int j=0; j<Input.length(); j++) {
			result = result + eachletter[j];
		}
		
		PrintWriter output = new PrintWriter("Q1-100502003.txt");
		output.println(result);
		
		input.close();
		output.close();
	}
	public static int getNumber(char letter) {
		boolean check = Character.isLetter(letter);
		int number=26;
		if(check == false) {
			return 10;
		}
		else
			number = (int)letter-65;
		
		if(number<=2)
			return 2;
		else if(number<=5)
			return 3;
		else if(number<=8)
			return 4;
		else if(number<=11)
			return 5;
		else if(number<=14)
			return 6;
		else if(number<=18)
			return 7;
		else if(number<=21)
			return 8;
		else if(number<=25)
			return 9;
		else 
			return 10;
					
	}
}
