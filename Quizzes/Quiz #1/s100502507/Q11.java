package q1.s100502507;
import javax.swing.JOptionPane;
public class Q11 {
	public static void main(String[] args) {//Function main begins execution
		String input = JOptionPane.showInputDialog(null, "Please input 9 digits of the ISBN", "Q11", JOptionPane.INFORMATION_MESSAGE);//Let user input
		ISBN isbn = new ISBN();//Declare ISBN
		int d10 = checksum(input);//Checking sum
		if(d10==10) {//Decide value of d10
			input += "-X";
		}
		else {
			input = input + "-" + String.valueOf(d10);
		}
		isbn.setContent(input);//Set final result to object isbn
		JOptionPane.showMessageDialog(null, "Output: " + isbn.getContent(), "Q11", JOptionPane.INFORMATION_MESSAGE);//Output result
	}//End function main
	public static int checksum(String input) {//Calculating checksum
		int d10 = 0;
		for(int i=0; i<input.length(); i++) {
			d10 += (input.charAt(i) - '0')*(i+1);
		}
		return d10%11;
	}
}
