package q1.s100502507;

public class ISBN {
	ISBN() {//Initializing variable
		content = " ";
	}
	
	public void setContent(String input) {//Revise content of variable
		content = input;
	}
	
	public String getContent() {//Return content of variable
		return content;
	}
	
	private String content;//The data member

}
