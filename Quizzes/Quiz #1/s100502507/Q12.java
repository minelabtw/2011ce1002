package q1.s100502507;
import java.io.*;
import java.util.Scanner;
public class Q12 {
	public static void main(String[] args) throws Exception {//Function main begins execution
		File input = new File("input.txt");//Open file
		Scanner inputString = new Scanner(input);
		String content = inputString.next();//Store content of input.txt
		inputString.close();
		String newContent = "";//New string stores revised content
		for(int i=0; i<content.length(); i++) {//Convert char to number
			if(Character.isLetter(content.charAt(i))) {
				newContent += getNumber(content.charAt(i));
			}
			else {
				newContent += content.charAt(i);
			}
		}
		PrintWriter output = new PrintWriter("Q1-100502507.txt");//Output file
		output.println(newContent);
		output.close();
	}//End function main
	public static int getNumber(char letter) {//A method to convert char to number
		int result = letter - 'A' + 1;
		switch(result) {
			case 1:case 2:case 3:
				return 2;
			case 4:case 5:case 6:
				return 3;
			case 7:case 8:case 9:
				return 4;
			case 10:case 11:case 12:
				return 5;
			case 13:case 14:case 15:
				return 6;
			case 16:case 17:case 18:case 19:
				return 7;
			case 20:case 21:case 22:
				return 8;
			case 23:case 24:case 25:case 26:
				return 9;
			default:
				return 0;
		}
	}
}
