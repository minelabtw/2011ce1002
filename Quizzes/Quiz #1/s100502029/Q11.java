package q1.s100502029;
import javax.swing.JOptionPane;

public class Q11 {
	public static void main(String[] args) {
		String ninenumber = JOptionPane.showInputDialog("Input nine digits of the ISBN:");
		ISBN isbn = new ISBN(ninenumber);
		if (checksum(isbn) != 10)
			isbn.setISBN(isbn.getISBN() + "-" + checksum(isbn));
		else
			isbn.setISBN(isbn.getISBN() + "-X");
		JOptionPane.showMessageDialog(null, isbn.getISBN());
	}
	
	public static int checksum(ISBN a) {
		char[] digits = a.getISBN().toCharArray();
		int result = 0;
		for (int i = 0; i < digits.length; i++) {
			result += (digits[ i ] - '0') * (i + 1);
		}
		int digit10 = result % 11;
		return digit10;
	}
}
