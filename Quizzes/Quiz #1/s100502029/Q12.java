package q1.s100502029;
import java.io.*;
import java.util.*;

public class Q12 {
	public static void main(String[] args) throws Exception {
		File sourceFile = new File("input.txt");
		File targetFile = new File("Q1-100502029.txt");
		Scanner input = new Scanner(sourceFile);
		PrintWriter output = new PrintWriter(targetFile);
		
		while (input.hasNext()) {
			String number = input.next();
			char[] digit = number.toCharArray();
			for (int i = 0; i < digit.length; i++) {
				if (Character.isLetter(digit[ i ]))
					digit[ i ] = getNumber(digit[ i ]);
			}
			number = new String(digit);
			output.println(number);
		}
		
		input.close();
		output.close();
	}
	
	public static char getNumber(char letter) {
		if (letter == 'A' || letter == 'B' || letter == 'C')
			return '2';
		else if (letter == 'D' || letter == 'E' || letter == 'F')
			return '3';
		else if (letter == 'G' || letter == 'H' || letter == 'I')
			return '4';
		else if (letter == 'J' || letter == 'K' || letter == 'L')
			return '5';
		else if (letter == 'M' || letter == 'N' || letter == 'O')
			return '6';
		else if (letter == 'P' || letter == 'Q' || letter == 'R' || letter == 'S')
			return '7';
		else if (letter == 'T' || letter == 'U' || letter == 'V')
			return '8';
		else
			return '9';
	}
}
