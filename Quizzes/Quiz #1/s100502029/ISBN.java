package q1.s100502029;

public class ISBN {
	private String booknumber;
	public ISBN() {
		
	}
	
	public ISBN(String newISBN) {
		booknumber = newISBN;
	}
	
	public void setISBN(String newISBN) {
		booknumber = newISBN;
	}
	
	public String getISBN() {
		return booknumber;
	}
}
