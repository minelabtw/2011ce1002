package q3.s100502512;

import java.awt.*;


import javax.swing.JFrame;

public class DrawPolygon {

	public static void main(String args[]) {
		FrameWork f = new FrameWork();
		f.setSize(500, 500);
		f.setTitle("q31");
		f.setFont(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}

	private int xTemp;
	private int yTemp;
	private int a;
	private int angle;

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int Radius = 150;
		int xCenter = Radius / 2;
		int yCenter = Radius / 2;
		int xTemp = xCenter--;
		int yTemp = yCenter--;
		int temp = xTemp;
		xTemp = (int) (temp * Math.cos(angle) - yTemp * Math.sin(angle));
		yTemp = (int) (temp * Math.sin(angle) + yTemp * Math.cos(angle));
		g.drawPolygon(xCenter, yCenter,a);
	}

	public void drawPolygon(int x, int y, int a) {
		this.xTemp = x;
		this.yTemp = y;
		this.a = a;

	}

	public void setNumberVertex(int a) {
		a = 4;
		angle = (int) ((2 * Math.PI) / a);
	}

	public int getAngle() {
		return angle;
	}

}
