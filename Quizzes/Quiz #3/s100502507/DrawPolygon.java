package q3.s100502507;

import java.awt.*;
import javax.swing.*;
import java.lang.Math;

public class DrawPolygon extends JLabel {
	DrawPolygon() {//Initializing variables
		xCenter = 200;
		yCenter = 200;
		vertices = 4;
		angle = 2*Math.PI/4.0;
		radius = 80;
	}
	
	public void setNumVervex(int a) {//Changes the quantity of vertices
		vertices = a;
		angle = 2*Math.PI/a;
	}
	
	public int getNumVertex() {//Return how many vertices in total
		return vertices;
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponents(g);
		
		Polygon polygon = new Polygon();
		for(int i=0; i<vertices; i++) {//Add vertices to polygon
			polygon.addPoint((int)(xCenter + radius*Math.sin(angle*(i + 1))), (int)(yCenter - radius*Math.cos(angle*(i + 1))));
		}
		/*The concept is:
		The x position is Rsin(angle) because the first angle is created between first vertex and the y coordinate.
		The y position is Rcos(angle) because the first angle is created between first vertex and the y coordinate.
		If there're k vertices, I multiply the angle by 1, 2, 3 to k, so the angle will match my requirement by using for loop.
		*/
		g.setColor(Color.WHITE);
		g.fillPolygon(polygon);
		g.setColor(Color.BLACK);
		g.drawPolygon(polygon);
	}
	
	private int xCenter, yCenter, vertices, radius;
	private double angle;
}
