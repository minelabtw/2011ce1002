package q3.s100502507;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener {
	FrameWork() {//Initializing variables
		polygon = new DrawPolygon();
		
		btAdd = new JButton("+");
		btDec = new JButton("-");
		
		JPanel p = new JPanel(new GridLayout(1, 2));
		
		setLayout(new BorderLayout());
		
		btAdd.addActionListener(this);
		btDec.addActionListener(this);
		p.add(btAdd);
		p.add(btDec);
		add(polygon, BorderLayout.CENTER);
		add(p, BorderLayout.SOUTH);
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==btAdd) {//If button "+" is clicked, add 1 vertex to the polygon
			int vertex = polygon.getNumVertex();
			polygon.setNumVervex(vertex + 1);
			polygon.repaint();
		}
		if(e.getSource()==btDec) {//If button "-" is clicked, minus 1 vertex to the polygon
			int vertex = polygon.getNumVertex();
			if(vertex>3) {//Avoid error(line)
				polygon.setNumVervex(vertex - 1);
				polygon.repaint();
			}
			else {
				JOptionPane.showMessageDialog(null, "What are you doing=.=\nThe graphic will be a line if you do so!!", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	private JButton btAdd, btDec;
	private DrawPolygon polygon;
}
