package q3.s100502521;

import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
public class DrawPolygon extends JPanel
{
	private Polygon polygon=new Polygon();//Polygons物件
	private int number=4,xCenter,yCenter,radius;
	private double angel;
	public DrawPolygon(int input1,int input2)
	{
		setNumVertex(4);
		setCenter(input1,input2);
		radius=(int) (Math.min(xCenter, yCenter)*0.6);
	}
	public void setNumVertex(int i)//設定N邊形 角度
	{
		number=i;
		angel=(2*Math.PI)/number;
	}
	public int getNumber()//回傳目前是幾N邊形
	{
		return number;
	}
	public void setCenter(int input1,int input2)
	{
		xCenter=input1;
		yCenter=input2;
	}
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		polygon.reset();
		double degree=Math.PI/2;
		int xTemp,yTemp;
		for(int i=0;i<number;i++)
		{
			xTemp=(int) (xCenter+radius*Math.cos(degree));
			yTemp=(int) (yCenter-radius*Math.sin(degree));
			polygon.addPoint(xTemp, yTemp);
			degree+=angel;
		}
		g.setColor(Color.BLUE);
		g.drawPolygon(polygon.xpoints,polygon.ypoints,number);		
	}
}
