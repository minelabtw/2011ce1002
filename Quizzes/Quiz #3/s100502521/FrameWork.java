package q3.s100502521;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class FrameWork extends JFrame implements ActionListener
{
	private DrawPolygon panel;
	private JPanel panel2;
	private JButton button1,button2;
	public FrameWork()
	{
		setTitle("多邊形");
		setSize(800,600);
		setLayout(new BorderLayout());
		setResizable(false);
		panel=new DrawPolygon(this.getWidth()/2,this.getHeight()/2);
		panel2=new JPanel();
		button1=new JButton(" + ");
		button2=new JButton(" - ");
		button1.addActionListener(this);
		button2.addActionListener(this);
		panel2.add(button1);
		panel2.add(button2);
		add(panel,BorderLayout.CENTER);
		add(panel2,BorderLayout.SOUTH);
		setVisible(true);
	}
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource()==button1)//button1增加1
		{
			panel.setNumVertex(panel.getNumber()+1);
			repaint();
		}
		else if(e.getSource()==button2)//button1減少1
		{
			if(panel.getNumber()>3)//3的時候就不減了
			{
				panel.setNumVertex(panel.getNumber()-1);
				repaint();
			}
		}
	}
}
