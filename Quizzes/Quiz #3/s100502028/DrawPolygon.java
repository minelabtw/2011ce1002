package q3.s100502028;

import java.util.*;
import java.awt.*;

import javax.swing.*;

public class DrawPolygon extends JPanel {
	protected int xCenter, yCenter, numberOfVertices, angle, radius;
	
	public DrawPolygon() {
		
	}
	
	public DrawPolygon(int w, int h) {
		xCenter = w / 2;
		yCenter = h / 2;
		numberOfVertices = 4;
		angle = (int)((Math.PI) / 2);
		radius = 100;
	}
	
	public void setNumVertex(int a) {
		angle = (int)((2 * Math.PI) / a);
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		//new DrawPolygon();
		int xTemp = 0, yTemp = 0, temp;
		int[] pointX = new int[numberOfVertices+1];
		int[] pointY = new int[numberOfVertices+1];
		
		pointX[0] = xCenter;
		pointY[0] = yCenter - radius;
		
		for(int i=1; i<numberOfVertices; i++) {
			xTemp -= xCenter;
			yTemp -= yCenter;
		
			temp = xTemp;
			xTemp = (int)(temp * Math.cos(angle) - yTemp * Math.sin(angle));
			yTemp = (int)(temp * Math.sin(angle) + yTemp * Math.cos(angle));
			
			xTemp += xCenter;
			yTemp += yCenter;
			
			pointX[i-1] = xTemp;
			pointY[i-1] = yTemp;
			
			
		}
		
		g.drawPolygon(pointX, pointY, pointX.length);
		//g.drawOval(xCenter, yCenter,100, 100);
		//repaint();
		//repaint();
	}
}
