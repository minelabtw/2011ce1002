package q3.s100502028;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener{
	private JButton jbtAdd = new JButton("+");
	private JButton jbtMinus = new JButton("-");
	int x = getWidth();
	int y = getHeight();
	DrawPolygon polygon = new DrawPolygon(getWidth(),getHeight());
	
	public FrameWork() {
		JPanel p = new JPanel(new GridLayout(1, 2));
		p.add(jbtAdd);
		p.add(jbtMinus);
		
		add(polygon, BorderLayout.CENTER);
		add(p, BorderLayout.SOUTH);
		
		jbtAdd.addActionListener(this);
		jbtMinus.addActionListener(this);
		
	}

	public void actionPerformed(ActionEvent e) {
		DrawPolygon polygon = new DrawPolygon(x,y);
		polygon.repaint();
		if(e.getSource() == jbtAdd) {
			
			//polygon.setNumVertex();
			polygon.repaint();
		}
		if(e.getSource() == jbtMinus) {
			
		}
	}
}
