package q3.s100502014;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class FrameWork extends JFrame {
	private JButton add = new JButton("+");
	private JButton minus = new JButton("-");
	private int vertices;
	private DrawPolygon drawPolygon = new DrawPolygon();
	
	public FrameWork() {
		//initial the vertices
		vertices = 4;
		
		setTitle("Q31");
		add(drawPolygon);
		
		//button panel
		JPanel j = new JPanel();
		j.setLayout(new GridLayout(1,2));
		j.add(add);
		j.add(minus);
		
		//add button panel
		add(j, BorderLayout.SOUTH);
		
		//add button listener
		add.addActionListener(new ButtonListener());
		minus.addActionListener(new ButtonListener());
	}
	
	class ButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == add) {
				vertices++;
				drawPolygon.setNumVertex(vertices);
			}
			
			//let the vertices be always over 3
			if((e.getSource() == minus) && (vertices > 3)) {
				vertices--;
				drawPolygon.setNumVertex(vertices);
			}
				
		}
	}
}
