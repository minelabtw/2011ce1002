package q3.s100502014;
import java.awt.*;
import javax.swing.*;
public class DrawPolygon extends JPanel {
	private int xCenter, yCenter, radius, vertices;
	private double angle;
	
	public DrawPolygon() {
		xCenter = 250;
		yCenter = 250;
		radius = 100;
		vertices = 4;
		angle = 2*Math.PI/vertices;
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Polygon polygon = new Polygon();
		
		//all points
		int[] xTemp = new int[vertices];
		int[] yTemp = new int[vertices];
		
		//first point about origin
		xTemp[0] = 0;
		yTemp[0] = (-1) * radius;
		
		//other point about origin
		for(int i=1;i<vertices;i++) {
			xTemp[i] = (int)(xTemp[0]*Math.cos(i*angle)-yTemp[0]*Math.sin(i*angle));
			yTemp[i] = (int)(xTemp[0]*Math.sin(i*angle)+yTemp[0]*Math.cos(i*angle));
		}
		
		//translate to center
		for(int i=0;i<vertices;i++) {
			xTemp[i] += xCenter;
			yTemp[i] += yCenter;
			polygon.addPoint(xTemp[i], yTemp[i]);
		}
		
		//how many vertices
		g.setFont(new Font("Arial", Font.BOLD, 50));
		g.drawString(vertices+" Vertices", 150, 100);
		
		//draw polygon
		g.drawPolygon(polygon);
	}
	
	//set the number of vertices
	public void setNumVertex(int a) {
		vertices = a;
		angle = 2*Math.PI/vertices;
		repaint();
	}
}
