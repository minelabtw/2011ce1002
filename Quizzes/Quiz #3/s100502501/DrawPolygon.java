package q3.s100502501;
import javax.swing.*;
import java.awt.*;
public class DrawPolygon extends JPanel{
	private int xCenter;
	private int yCenter;
	protected int vertice=4;
	private double angle=2*Math.PI/vertice;
	private int radius=100;
	
	public DrawPolygon(){
	}
	
	public void setNumVertex(int a){
		vertice=a;
		angle=2*Math.PI/a;
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		xCenter=getWidth()/2;
		yCenter=getHeight()/2;
		int xTemp=xCenter;
		int yTemp=yCenter-radius;
		int temp;
		Polygon plg=new Polygon();
		plg.addPoint(getWidth()/2,yCenter-radius);
		
		for(int i=0;i<vertice-1;i++){
			xTemp-=xCenter;
			yTemp-=yCenter;
			temp=xTemp;
			xTemp=(int)(temp*Math.cos(angle)-yTemp*Math.sin(angle));
			yTemp=(int)(temp*Math.sin(angle)+yTemp*Math.cos(angle));
			xTemp+=xCenter;
			yTemp+=yCenter;
			plg.addPoint(xTemp,yTemp);
		}
		g.drawPolygon(plg);
	}
}
