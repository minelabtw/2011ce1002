package q3.s100502501;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class FrameWork extends JFrame implements ActionListener{
	private DrawPolygon dplg=new DrawPolygon();
	JButton b1=new JButton("+");
	JButton b2=new JButton("-");
	
	public FrameWork(){
		JPanel p1=new JPanel(new GridLayout(1,2));
		JPanel p2=new JPanel(new BorderLayout());
		p1.add(b1);
		p1.add(b2);
		p2.add(dplg,BorderLayout.CENTER);
		p2.add(p1,BorderLayout.SOUTH);
		add(p2);
		b1.addActionListener(this);
		b2.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==b1){
			dplg.vertice++;
			dplg.setNumVertex(dplg.vertice);
			dplg.repaint();
		}
		else if(e.getSource()==b2){
			dplg.vertice--;
			dplg.setNumVertex(dplg.vertice);
			repaint();
		}
	}
}
