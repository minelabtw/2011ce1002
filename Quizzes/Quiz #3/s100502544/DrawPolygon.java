package q3.s100502544;

import java.awt.Graphics;

import javax.swing.JLabel;
import java.awt.Polygon;
public class DrawPolygon extends JLabel{
	Polygon p=new Polygon();
	int num=4;
	double angel=Math.PI/2;
	int radius=150;
	int xcenter=getWidth()/2;
	
	int ycenter=getHeight()/2;
	public void setNumVertex(int a){
		 angel=(2*Math.PI)/a;
	}
	
	public double getangel(){
		return angel;
	}
	public void paintComponent(Graphics g){
		super.printComponent(g);
		int xtemp=0;
		int ytemp=0;
		xtemp-=xcenter;
		ytemp-=ycenter;
		xtemp+=xcenter;
		ytemp+=ycenter;
		int temp=0;
		temp=xtemp;
		xtemp=(int)(temp*Math.cos(angel)- ytemp*Math.sin(angel));
		ytemp=(int)(temp*Math.sin(angel)+ ytemp*Math.cos(angel));
		g.drawLine(xcenter, ycenter, xtemp, ytemp);
		g.drawLine(xcenter, ycenter, xtemp+radius, ytemp);
		g.drawLine(xcenter, ycenter, xtemp, ytemp+radius);
	}
	public void drawPolygon(int x,int y){
		
	}
	
}
