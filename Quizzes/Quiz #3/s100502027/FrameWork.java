package q3.s100502027;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class FrameWork extends JFrame implements ActionListener{
	private JButton JBadd = new JButton("+");
	private JButton JBminus = new JButton("-");
	private int vertex = 4 ;
	private DrawPolygon QQ = new DrawPolygon();
	public FrameWork(){
		setLayout(new BorderLayout(5,5));
		
		JPanel  buttons = new JPanel();
		buttons.setLayout(new GridLayout(1,2,5,5));
		buttons.add(JBadd);
		buttons.add(JBminus);
		JBadd.addActionListener(this);
		JBminus.addActionListener(this);
		
		add(QQ,BorderLayout.CENTER);
		add(buttons,BorderLayout.SOUTH);
	}
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == JBadd){
			vertex++;
			QQ.setNumVertex(vertex);
			QQ.repaint();
		}
		if(e.getSource() ==JBminus){
			vertex--;
			QQ.setNumVertex(vertex);
			QQ.repaint();
		}
	}
}
