package q3.s100502027;

import java.awt.Graphics;
import java.awt.Polygon;

import javax.swing.JPanel;

public class DrawPolygon extends JPanel {
	private int vertice = 4 ;
	public DrawPolygon(){
	}
	public void setNumVertex(int a){
		vertice = a ;
	}
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		int XC =getWidth()/2;
		int YC =getHeight()/2;
		int radius=Math.min(XC,YC) /2;
		Polygon shapeuse = new Polygon();
		for(int t=0;t<vertice;t++){
			shapeuse.addPoint(XC+(int)(Math.sin(Math.PI*2*t/vertice)*radius), YC-(int)(Math.cos(Math.PI*2*t/vertice)*radius));
		}
		g.drawPolygon(shapeuse);
	}
}
