package q3.s995002026;

import javax.swing.*;

import java.awt.*;

public class DrawPolygon extends JPanel{
	private int vertex=4;
	
	private int angle;
	
	private int x[]=new int[vertex];
	
	private int y[]=new int[vertex];
	
	private int xcenter,ycenter;
	
	private int radius=100;
	
	DrawPolygon(){
		xcenter=getWidth()/2;
		
		ycenter=getHeight()/2;
		
		setXY();
		
		
	}
	
	public void paintComponent(Graphics g){
		
		super.paintComponent(g);
	
		g.setColor(Color.BLACK);

		g.drawPolygon(x, y, vertex);
		
	}
	
	public void setNumVertex(int a){	
		vertex+=a;
		
		angle=(int) ((2*Math.PI)/vertex);
	}
	
	public void setXY(){			
		x[0]=xcenter;
		
		y[0]=ycenter-radius;
		
		int xTemp;
		int yTemp;
		
		for(int i=0;i<=vertex;i++){
			xTemp=x[i];
			yTemp=y[i];
			
			xTemp-=xcenter;
			yTemp+=ycenter;
			
			int temp=xTemp;
			
			xTemp=(int)(temp*Math.cos(angle)-yTemp*Math.sin(angle));
			yTemp=(int)(temp*Math.sin(angle)+yTemp*Math.cos(angle));
			
			x[i]=xTemp;
			y[i]=yTemp;

		}
	}
}
