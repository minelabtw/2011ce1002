package q3.s995002026;

import javax.swing.*;

import java.awt.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrameWork extends JFrame implements ActionListener{
	DrawPolygon pg=new DrawPolygon();
	
	public JPanel p1=new JPanel(new GridLayout(1,2));
	
	public JPanel p2=new JPanel();
	
	public JButton add=new JButton("+");
	
	public JButton sub=new JButton("-");
	
	
	FrameWork(){
		p1.add(add);
		
		p1.add(sub);
		
		add(pg,BorderLayout.CENTER);
		
		add(p1,BorderLayout.SOUTH);
		
		add.addActionListener(this);
		
		sub.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e){
		
		if(e.getSource()==add){
			pg.setNumVertex(1);
		}
		
		if(e.getSource()==sub){
			pg.setNumVertex(-1);
		}
		pg.repaint();
	}
	
	
}
