package q3.s100502505;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class FrameWork extends JFrame implements ActionListener
{
	DrawPolygon d = new DrawPolygon();
	JButton add = new JButton("Add");
	JButton minus = new JButton("Minus");
		
	public FrameWork()
	{
		JPanel p = new JPanel();
		p.add(add);
		p.add(minus);
		add(d,BorderLayout.CENTER);
		add(p,BorderLayout.SOUTH);
		add.addActionListener(this);
		minus.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent arg0) 
	{
		if(arg0.getSource() == add)
		{
			d.setNumVertex(d.getv()+1);
			repaint();			
		}
		if(arg0.getSource() == minus)
		{
			d.setNumVertex(d.getv()-1);
			repaint();
		}
	}
	
}
