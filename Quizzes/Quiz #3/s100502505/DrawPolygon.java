package q3.s100502505;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;

public class DrawPolygon extends JLabel{
	int xCenter = getWidth()/2;
	int yCenter = getHeight()/2;
	int vertices = 4;
	double angle = 2*Math.PI/vertices;
	int radius = 20;
	
	public DrawPolygon()
	{
		setNumVertex(vertices);
	}
	
	public int getv()
	{
		return vertices;
	}
	
	public void setNumVertex(int a)
	{
		vertices = a;
		angle = 2*Math.PI/vertices;
	}
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		int[] x = new int[30];
		int[] y = new int[30]; 
		x[0] = xCenter;
		y[0] = yCenter;
		double temp=0;
		for(int i=1;i<vertices;i++)
		{
			x[i] -= x[i-1];
			y[i] -= y[i-1];
			temp = x[i];
			x[i] = (int)(temp*Math.cos(angle)+y[i]*Math.sin(angle));
			y[i] = (int)(temp*Math.sin(angle)+y[i]*Math.cos(angle));
			x[i] += x[i-1];
			y[i] += y[i-1];
			g.drawLine(x[i-1],y[i-1], x[i], y[i]);
		}
	}
}


