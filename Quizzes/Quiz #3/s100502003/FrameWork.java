package q3.s100502003;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class FrameWork extends JFrame {
	public JButton plus = new JButton("+");
	public JButton minus = new JButton("-");
	public int Number;
	public FrameWork() {
		JPanel p = new JPanel();
		DrawPolygon draw = new DrawPolygon();
		AddListener addListener = new AddListener();
		MinusListener minusListener = new MinusListener();
		plus.addActionListener(addListener);
		minus.addActionListener(minusListener);
		draw.setNumVertex(Number);
		p.add(plus);
		p.add(minus);
		add(draw, BorderLayout.CENTER);
		add(p, BorderLayout.SOUTH);
	}
	class AddListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Number = Number+1;
		}
	}
	class MinusListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Number = Number-1;
		}
	}
}
