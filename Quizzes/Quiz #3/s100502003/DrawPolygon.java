package q3.s100502003;

import java.awt.*;
import javax.swing.*;

public class DrawPolygon extends JLabel{
	private int xCenter, yCenter, number, angle, radius;
	public DrawPolygon() {
		xCenter = getWidth()/2;
		yCenter = getHeight()/2;
		radius = getWidth()*3/5/2;
		number = 4;
		angle = (int)Math.PI/2;
	}
	public void setNumVertex(int a) {
		this.number = a;
		this.angle = (int)((2*Math.PI)/number);
		repaint();
	}
	protected void paintComponent(Graphics g) {	
		super.paintComponent(g);
		int xTemp=0, yTemp=0, temp=0;
		Polygon polygon = new Polygon();
		polygon.addPoint(xCenter, yCenter-radius);
		for(int i=1; i<number; i++) {
			xTemp -= xCenter;
			yTemp -= yCenter;
			temp = xTemp;
			xTemp = (int)(temp*Math.cos(angle)-yTemp*Math.sin(angle));
			yTemp = (int)(temp*Math.sin(angle)+yTemp*Math.cos(angle));
			xTemp += xCenter;
			yTemp += yCenter;
			polygon.addPoint(xTemp, yTemp);
		}
		g.drawPolygon(polygon);
	}
}

