package q3.s100502511;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener {
	JButton Add = new JButton("Add");
	JButton Minus = new JButton("Minus");
	JPanel Polygon = new JPanel();
	JPanel Button = new JPanel(new GridLayout(1, 2));
	DrawPolygon polygon = new DrawPolygon();
	int initial = 5;

	public FrameWork() {
		Polygon.add(polygon);
		Button.add(Add);
		Button.add(Minus);

		add(Polygon, BorderLayout.CENTER);
		add(Button, BorderLayout.SOUTH);

		Add.addActionListener(this);
		Minus.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == Add) {
			initial++;
			polygon.setNumVertex(initial);
		}
		if (e.getSource() == Minus) {
			initial--;
			polygon.setNumVertex(initial);
		}
	}

}
