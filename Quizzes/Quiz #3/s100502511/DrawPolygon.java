package q3.s100502511;

import java.awt.Polygon;
import javax.swing.*;
import java.awt.*;

public class DrawPolygon extends JPanel {
	private int xCenter;
	private int yCenter;
	private int the_number_of_vertices;
	private double angle;
	private int radius_of_polygon;
	private int Radius;
	private int xTemp;
	private int yTemp;
	private int temp;

	public DrawPolygon() {
		xCenter = getWidth();
		yCenter = getHeight();
		angle = Math.PI / 2;
		the_number_of_vertices = 4;
		Radius = 150;
		add(new PolygonPanel());
	}

	void setNumVertex(int a) {
		the_number_of_vertices = a;
		angle = Math.PI * 2 / the_number_of_vertices;
		repaint();
	}

	class PolygonPanel extends JPanel {
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			Polygon a = new Polygon();
			a.addPoint(xCenter, yCenter - Radius);

			xTemp -= xCenter;
			yTemp -= yCenter;
			temp = xTemp;

			xTemp = (int) (temp * Math.cos(angle) - yTemp * Math.sin(angle));
			yTemp = (int) (temp * Math.sin(angle) + yTemp * Math.cos(angle));

			a.addPoint(xTemp, yTemp);

			xTemp += xCenter;
			yTemp += yCenter;

			g.drawPolygon(a);
		}
	}
}
