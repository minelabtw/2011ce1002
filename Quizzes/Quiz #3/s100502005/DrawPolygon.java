package q3.s100502005;

import javax.swing.*;
import java.awt.*;
import java.awt.Polygon;
import java.math.*;

public class DrawPolygon extends JFrame 
{
	public static int NumberOfPoints = 4;
	public static double angle = (2*Math.PI)/NumberOfPoints;
	public static double radius = 0;
	
	public static void main(String[] args)
	{
		DrawPolygon frame = new DrawPolygon();
		frame.setSize(1000,800);
		frame.setLocationRelativeTo(null);
		frame.setTitle("Q3");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);		
	}
	
	public void setNumVertex(int a)
	{
		NumberOfPoints = a;
	}
	
	
		protected void paintComponent(Graphics g)
		{
			double temp;
			double xTemp = 0;
			double yTemp = 0;
			double xCenter = 500;
			double yCenter = 400;			
			xTemp -= xCenter;
			yTemp -= yCenter;
			
			temp = xTemp;
			xTemp = (int)(temp*Math.cos(angle)-yTemp*Math.sin(angle));
			yTemp = (int)(temp*Math.sin(angle)+yTemp*Math.cos(angle));
			
			super.paintComponents(g);
			g.drawLine(50, 20, 0, 0);
		}
		
		public void drawPolygon()
		{
			
		}
}
