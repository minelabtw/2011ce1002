package q3.s100502017;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
public class FrameWork extends JFrame implements ActionListener{
	JButton ADD=new JButton("+");//Declare two button
	JButton SUB=new JButton("-");
	JPanel p=new JPanel();
	DrawPolygon draw=new DrawPolygon();
	public FrameWork(){
		p.setLayout(new GridLayout(1,2));
		p.add(ADD);
		p.add(SUB);
		add(draw,BorderLayout.CENTER);
		add(p,BorderLayout.SOUTH);
		ADD.addActionListener(this);
		SUB.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e){//button event is here
		int v=draw.vertices;
		if(e.getSource()==ADD){
			v++;
			draw.setNumVertex(v);//to add vertices
		}
		if(e.getSource()==SUB){
			v--;
			draw.setNumVertex(v);//to decrease vertices
		}
		repaint();//renew drawing
	}
}
