package q3.s100502017;
import javax.swing.*;
import java.awt.*;
public class DrawPolygon extends JPanel{
	protected int vertices,rad;
	protected double xCenter,yCenter,angle;
	public DrawPolygon(){//constructor to initial variable
		setNumVertex(4);
		setRadius(200);
	}
	public void setRadius(int r){//to set rad
		rad=r;
	}
	public void setNumVertex(int v){//to set number of point
		vertices=v;
		angle=2*Math.PI/v;
	}
	public void paintComponent(Graphics g){//draw graphic
		super.paintComponent(g);
		xCenter=getWidth()*0.5;
		yCenter=getHeight()*0.5;
		Polygon pol=new Polygon();
		for(int i=0;i<vertices;i++){//use for loop to decide point
			pol.addPoint((int)(xCenter-rad*Math.sin(angle*i)),(int)(yCenter-rad*Math.cos(angle*i)));
		}
		g.drawPolygon(pol);
	}
	
}
