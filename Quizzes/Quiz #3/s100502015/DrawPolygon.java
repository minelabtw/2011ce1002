package q3.s100502015;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
public class DrawPolygon extends JPanel{
	protected int corx;
	protected int cory;
	protected int tempx[] = new int[100];
	protected int tempy[] = new int[100];
	protected int polygon = 4;
	public DrawPolygon()
	{		
		corx = getWidth()/2;
		cory = getHeight()/2;		
	}	
	public void drawpoint()
	{		
		int radius = 100;
		for(int i=0;i<polygon;i++)
		{
			tempx[i] = (int)(radius*Math.sin(2*Math.PI/(polygon)*i)+corx+250);
			tempy[i] = (int)(radius*Math.cos(2*Math.PI/(polygon)*i)+cory+250);
		}
		
	}
	public void paintComponent(Graphics g) 
	{
		g.drawPolygon(tempx, tempy, polygon);
		super.paintComponents(g);
	}

}
