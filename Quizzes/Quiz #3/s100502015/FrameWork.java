package q3.s100502015;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
public class FrameWork extends JFrame implements ActionListener{
	private JPanel p1 = new JPanel();
	private JPanel p2 = new JPanel();
	private DrawPolygon draw1 = new DrawPolygon();  
	private JButton b1 = new JButton("+");
	private JButton b2 = new JButton("-");
	public FrameWork()
	{
		p1.setLayout(new GridLayout(2,1));
		p2.setLayout(new GridLayout(1,2));
		p1.add(draw1);
		p1.add(p2);
		p2.add(b1);
		p2.add(b2);
		b1.addActionListener(this);
		b2.addActionListener(this);
		add(p1);		
	}
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()==b1)
		{
			draw1.polygon +=1;
			draw1.drawpoint();
			p1.repaint();
		}
		else if(e.getSource()==b2)
		{
			draw1.polygon -=1;
			draw1.drawpoint();
			p1.repaint();
		}
	}
	
}
