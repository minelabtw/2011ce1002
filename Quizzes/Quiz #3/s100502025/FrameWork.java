package q3.s100502025;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener {
	private JButton jbtPlus = new JButton("+");
	private JButton jbtMinus = new JButton("-");
	private JPanel p1 = new JPanel();
	private DrawPolygon polygon = new DrawPolygon();
	private int vertice = 4;
	
	public FrameWork() {
		p1.setLayout( new GridLayout( 1 , 2 ) );
		p1.add( jbtPlus );
		p1.add( jbtMinus );
		setLayout(new BorderLayout());
		
		add(polygon , BorderLayout.CENTER);
		add(p1 , BorderLayout.SOUTH);
		
		jbtPlus.addActionListener(this);
		jbtMinus.addActionListener(this);
		
		
	}
		
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == jbtPlus) {
			vertice = vertice + 1;
			polygon.setNumVertex(vertice);
			repaint();
		}
		if(e.getSource() == jbtMinus) {
			vertice = vertice - 1;
			if(vertice < 3) {
				vertice = 3;
			}
			polygon.setNumVertex(vertice);
			repaint();
		}
	}
}
