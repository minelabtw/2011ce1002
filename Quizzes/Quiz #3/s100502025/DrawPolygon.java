package q3.s100502025;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.Graphics;


public class DrawPolygon extends JPanel {
	private int vertice = 4;
	
			
	public void setNumVertex(int a) {
		vertice = a;
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		int[] x = new int[vertice];
		int[] y = new int[vertice];
		int xCenter = getWidth() / 2;
		int yCenter = getHeight() / 2;
		int radius = (int)(Math.min(getWidth() * 0.5 * .45 , getHeight() * 0.5 * 0.45));
		
		
		for(int i = 0 ; i < vertice ; i++ ) {
			if(i == 0 ) {
				x[i] = xCenter;
				y[i] = yCenter - radius;
			}
			else {
				int xTemp = x[ i -1 ] , yTemp = y[ i -1 ];
				xTemp -= xCenter;
				yTemp -= yCenter;
				
				int temp = xTemp;
				xTemp = (int)(temp*Math.cos(2*Math.PI / vertice) - yTemp*Math.sin(2*Math.PI / vertice));
				yTemp = (int)(temp*Math.sin(2*Math.PI / vertice) + yTemp*Math.cos(2*Math.PI / vertice));
				
				x[i] = xTemp + xCenter;
				y[i] = yTemp + yCenter;
			}
		}
				
		g.drawPolygon(x, y, vertice);
	}
}
