package q3.s100502510;

import java.awt.BorderLayout;

import javax.swing.*;

public class FrameWork extends JFrame {
	JButton plus = new JButton("+");
	JButton minus = new JButton("-");
	DrawPolygon draw = new DrawPolygon();
	JPanel buttons = new JPanel();

	public FrameWork() {
		buttons.setLayout(new BorderLayout());
		this.setLayout(new BorderLayout());
		buttons.add(plus, BorderLayout.WEST);
		buttons.add(minus, BorderLayout.EAST);
		this.add(draw, BorderLayout.CENTER);
		this.add(buttons, BorderLayout.SOUTH);
	}

}
