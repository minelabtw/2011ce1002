package q3.s100502510;

import java.awt.Graphics;

import javax.swing.*;

public class DrawPolygon extends JLabel {

	int vertices = 4;
	double angle;
	double radius = 100;

	public DrawPolygon() {

	}

	public void setNumVertex(int a) {
		vertices = a;
		angle = (2 * Math.PI) / vertices;
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		double xCenter = getWidth() / 2;
		double yCenter = getHeight() / 2;
		double firsty = yCenter - radius;
		double[] xtimes = new double[20];
		double[] ytimes = new double[20];
		xtimes[0] = xCenter;
		ytimes[0] = firsty;
		for (int i = 0; i < vertices; i++) {
			double temp = radius;
			xtimes[i + 1] = xtimes[i] + (double) (temp * Math.cos(90 - angle));
			ytimes[i + 1] = ytimes[i] + (double) (temp * Math.sin(90 - angle));
			angle += angle;
		}
		for (int j = 0; j < vertices; j++) {
			g.drawLine((int) xtimes[j], (int) ytimes[j], (int) xtimes[j + 1],
					(int) ytimes[j + 1]);
			if (j + 1 == vertices) {
				break;
			}
		}
		// g.drawLine((int)xtimes[0],(int)ytimes[0],(int)xtimes[0]+100,(int)ytimes[0]);

	}
}
