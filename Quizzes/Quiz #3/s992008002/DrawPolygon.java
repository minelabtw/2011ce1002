package q3.s992008002;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.awt.Polygon;


public class DrawPolygon extends JLabel {

	private int xCenter,yCenter,vertices=4,xTemp,yTemp,temp;
	private double Angle= Math.PI/2,Radius;
	
	
	
	
	public DrawPolygon(){
		
	}
	
	public void setNumVertex(int a){
		Angle = (2*Math.PI)/a;
	}
	
	public double getAngle(){
		return Angle;
	}
	
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		xCenter = getWidth()/2;
		yCenter = getHeight()/2;
		Radius = (int)(Math.min(getHeight(), getWidth())*0.8*0.5);
		
		xTemp -= xCenter;
		yTemp -= yCenter;
		
		temp = xTemp;
		
		xCenter += xTemp;
		yCenter += yTemp;
		
		xTemp = (int)(temp*Math.cos(getAngle())-yTemp*Math.sin(getAngle()));
		yTemp = (int)(temp*Math.sin(getAngle())-yTemp*Math.cos(getAngle()));		
		
		
		//g.drawPolygon(xCenter,yCenter-Radius,1);
		
	}
	
	
	
}
