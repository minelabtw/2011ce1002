package q3.s100502518;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener{
	JLabel screen=new JLabel();
	JButton add=new JButton("+");
	JButton minus=new JButton("-");
	JPanel panel=new JPanel();
	
	public FrameWork()
	{
		panel.setLayout(new GridLayout(1,2));
		panel.add(add);
		panel.add(minus);
		
		setLayout(new BorderLayout());
		add(screen,BorderLayout.NORTH);
		add(panel,BorderLayout.SOUTH);
		add.addActionListener(this);
		minus.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()==add)
		{
			
		}
		
		else if(e.getSource()==minus)
		{
			 
		}
	}
}
