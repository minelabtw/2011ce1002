package q3.s100502029;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class FrameWork extends JFrame implements ActionListener {
	JButton jbtAdd = new JButton("+"); // create a button to add
	JButton jbtMinus = new JButton("-"); // create a button to minus
	DrawPolygon picture = new DrawPolygon(); // create a DrawPolygon object
	
	public FrameWork() {
		JPanel jplButtons = new JPanel(); // create a panel to put add and minus button
		Font font = new Font("TimesNewRoman", Font.BOLD, 20); // create a font
		
		jplButtons.setLayout(new GridLayout(1, 2));
		jplButtons.add(jbtMinus); // add "add button" to panel
		jplButtons.add(jbtAdd); // add "minus button" to panel
		
		// decorate the add and the minus button
		jbtAdd.setBackground(Color.GREEN);
		jbtMinus.setBackground(Color.GREEN);
		jbtAdd.setForeground(Color.BLACK);
		jbtMinus.setForeground(Color.BLACK);
		jbtAdd.setFont(font);
		jbtMinus.setFont(font);
		
		add(picture, BorderLayout.CENTER);
		add(jplButtons, BorderLayout.SOUTH);
		
		// add actionlistener to buttons
		jbtMinus.addActionListener(this);
		jbtAdd.addActionListener(this);
	}
	
	// the action performed when button pressed
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == jbtMinus) {
			picture.vertices--; // minus 1 to the number of vertices
			picture.repaint(); // repaint the polygon
		}
		if (e.getSource() == jbtAdd) {
			picture.vertices++; // add 1 to the number of vertices
			picture.repaint(); // repaint the polygon
		}
	}
}
