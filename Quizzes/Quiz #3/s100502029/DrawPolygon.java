package q3.s100502029;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import javax.swing.JLabel;

public class DrawPolygon extends JLabel {
	protected int vertices = 4; // the number of vertices
	protected double angle;
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		int xCenter = getWidth() / 2; // set the x-coordinate of the center
		int yCenter = getHeight() /2; // set the y-coordinate of the center
		int radius = (int)(Math.min(getWidth(), getHeight()) * 0.8 * 0.5); // set the radius of the polygon
		Polygon p = new Polygon(); // create a polygon object
		
		angle = 2 * Math.PI / vertices; // set the angle
		
		for (int i = 0; i < vertices; i++) {
			// add each point into polygon
			p.addPoint((int)(xCenter + radius * Math.sin(angle * i)), (int)(yCenter - radius * Math.cos(angle * i)));
		}
		
		g.setColor(Color.RED); // set the color of the polygon
		g.drawPolygon(p); // graph the polygon
	}
	
}
