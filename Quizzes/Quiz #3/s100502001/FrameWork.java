package q3.s100502001;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
public class FrameWork extends JFrame {
	
	private JButton jbtplus=new JButton("+");
	private JButton jbtsub=new JButton("-");
	private int num=4;
	DrawPolygon polygon =new DrawPolygon(getWidth()/2,getHeight()/2);
	public FrameWork() {
		
		JPanel jp=new JPanel(); //�ƪ�
		setLayout(new BorderLayout());
		jp.setLayout(new GridLayout(1,2));
		jp.add(jbtplus);
		jp.add(jbtsub);
		add(jp,BorderLayout.SOUTH);
		add(polygon,BorderLayout.CENTER);
		polygon.setNumVertex(num);
		jbtplus.addActionListener(new JbtPlus());
		jbtsub.addActionListener(new JbtSub());

		
	}
	class JbtPlus implements ActionListener{ //�Y���U+
		public void actionPerformed(ActionEvent e){
			
			polygon.setNumVertex(num++);
			repaint();
		}

	}
	class JbtSub implements ActionListener{ //�Y���U-
		public void actionPerformed(ActionEvent e){
			
			polygon.setNumVertex(num--);
			repaint();
		}
	}
}
