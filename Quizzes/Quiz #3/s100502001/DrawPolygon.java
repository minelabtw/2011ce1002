package q3.s100502001;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
public class DrawPolygon extends JPanel{
	private int xCenter;
	private int yCenter;
	private int numOfvertices;
	private double angle;
	private int radius;
	private int[] X; //幾組X
	private int[] Y; //幾組Y
	public DrawPolygon(int X, int Y){
		xCenter=X;
		yCenter=Y;
	}
	public void setNumVertex(int a){
		numOfvertices=a;
		angle=(2*Math.PI)/numOfvertices; 
		X=new int[numOfvertices];
		Y=new int[numOfvertices];
	}
	protected void paintComponent(Graphics g){
		super.paintComponents(g);
		
		X[0]=xCenter; //center上面的點
		Y[0]=yCenter-radius;
		int xTemp=X[0];
		int yTemp=Y[0];
		int temp;
		for(int i=1;i<numOfvertices;i++){
			xTemp-=xCenter; //平移回原點
			yTemp-=yCenter;
			 
			temp=xTemp;
			xTemp=(int)(temp*Math.cos(angle)-yTemp*Math.sin(angle)); //旋轉
			yTemp=(int)(temp*Math.sin(angle)+yTemp*Math.cos(angle));
			
			xTemp+=xCenter; //平移回來
			yTemp+=yCenter;
			
			X[i]=xTemp;
			Y[i]=yTemp;
		}
		
		Polygon polygon=new Polygon(); //畫多邊形
		for(int i=0;i<numOfvertices;i++){
			polygon.addPoint(X[i], Y[i]);
		}
		//g.drawPolyline(X, Y, numOfvertices);
		g.drawPolygon(polygon);
		
	}
	
}
