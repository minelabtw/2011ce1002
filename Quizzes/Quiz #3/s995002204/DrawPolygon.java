package q3.s995002204;

import java.awt.*;
import javax.swing.*;
import java.math.*;

public class DrawPolygon extends JLabel{
	
	private int xCenter = 200;
	private int yCenter = 200;
	private int numberVertice;
	private double angle;
	private int radius;
	
	public DrawPolygon()
	{
		numberVertice = 4;
		angle = Math.PI/2;
		radius = 100;
	}
	
	public int getnumberVertice()
	{
		return numberVertice;
	}
	
	public void setNumVertex(int input)
	{
		numberVertice = input;
		angle = (2*Math.PI)/numberVertice;
	}
	
	protected void paintComponent(Graphics g)
	{
		int xTemp;
		int yTemp;
		int temp;
		
		super.paintComponent(g);
		Polygon polygon = new Polygon();
		
		xTemp = xCenter;
		yTemp = yCenter-radius;
		
		polygon.addPoint(xTemp, yTemp);
	
		for(int i=0;i<numberVertice-1;i++)
		{
			xTemp -= xCenter;
			yTemp -= yCenter;
			
			temp = xTemp;
			xTemp = (int)(temp*Math.cos(angle)-yTemp*Math.sin(angle));
			yTemp = (int)(temp*Math.sin(angle)+yTemp*Math.cos(angle));
			
			
			xTemp += xCenter;
			yTemp += yCenter;
			
			polygon.addPoint(xTemp, yTemp);
		}
		
		g.setColor(Color.BLACK);
		g.drawPolygon(polygon.xpoints, polygon.ypoints, numberVertice);
	}
	
}
