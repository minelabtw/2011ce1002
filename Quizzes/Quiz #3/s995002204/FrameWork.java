package q3.s995002204;

import java.util.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener{
	
	private JPanel buttonControl = new JPanel();
	private JButton add = new JButton("Add");
	private JButton minus =new JButton("Minus");
	private DrawPolygon draw = new DrawPolygon();
	
	public FrameWork()
	{
		setLayout(new BorderLayout());
		
		buttonControl.setLayout(new GridLayout(1,2));
		buttonControl.add(add);
		buttonControl.add(minus);
		
		add(draw,BorderLayout.CENTER);
		add(buttonControl,BorderLayout.SOUTH);
		
		add.addActionListener(this);
		minus.addActionListener(this);
	}
	
	
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()==add)
		{
			int temp = 0;
			temp = draw.getnumberVertice() + 1;
			draw.setNumVertex(temp);
			repaint();
		}
		
		if(e.getSource()==minus)
		{
			int temp = 0;
			temp = draw.getnumberVertice() - 1;
			draw.setNumVertex(temp);
			repaint();
		}
	}
}
