package q3.s100502002;
import java.util.*;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.BorderLayout;
public class FrameWork extends JFrame
{
	private JButton add = new JButton("+");
	private JButton minus = new JButton("-");
	private JPanel p1 = new JPanel();
	int v = 4;
	DrawPolygon d = new DrawPolygon();
	public FrameWork()
	{
		
		
		buttonlistener li = new buttonlistener();
		add.addActionListener(li);
		minus.addActionListener(li);
		
		
		p1.add(add);
		p1.add(minus);
		
		add(d,BorderLayout.CENTER);
		add(p1,BorderLayout.SOUTH);
		
	}
	
	
	
	public class buttonlistener implements ActionListener
	{

		public void actionPerformed(ActionEvent e) 
		{
			
			if(e.getSource() == add)
			{
				v++;
				d.setNumberVertex(v);
				d.repaint();
			}
			if(e.getSource() == minus)
			{
				v--;
				d.setNumberVertex(v);
				d.repaint();
			}
			
		}
	}
	
	

}
