package q3.s100502002;
import java.util.*;
import java.awt.*;
import javax.swing.*;
public class DrawPolygon extends JPanel
{
	private int xcenter = getWidth()/2;
	private int ycenter = getHeight()/2;
	private int vertice;
	private int angle;
	private int radius = 100;
	
	
	public DrawPolygon ()
	{
		vertice = 4;
	}
	public void setNumberVertex(int a)
	{
		vertice = a;
		angle = (int)(Math.PI*2 / vertice);
	}
	
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		Polygon p = new Polygon();
		
		
		int x[] = new int [vertice];
		int y[] = new int [vertice];
		
		
		for(int i=0;i<vertice;i++)
		{
			x[i] = (int) (xcenter + radius * Math.cos(i*angle));
			y[i] = (int) (ycenter - radius * Math.sin(i*angle));
			p.addPoint(x[i], y[i]);
		}
		
		g.drawPolygon(p);
	
		
		
	}
	
}

