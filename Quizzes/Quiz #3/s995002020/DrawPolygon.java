package q3.s995002020;
import java.awt.*;
import javax.swing.JPanel;
public class DrawPolygon extends JPanel{
	private int xCenter=WIDTH/2;
	private int yCenter=HEIGHT/2;
	private int radius = 30;
	public static int numbers=4;
	public double angle = (2 * Math.PI) / numbers ;//angle
	void setNumVertex(int a){
		numbers = a;// numbers of Vertex
	}
	public void paintComponent(Graphics g){
		 Polygon p = new Polygon();
		 p.addPoint(xCenter, yCenter-radius);//first point
		 for(int i=1;i<numbers;i++){//draw n-1 new points
			 
		 
		 int xTemp = xCenter;
		 int yTemp = yCenter-radius;
		 xTemp -=xCenter;
		 yTemp -=yCenter;
		 
		 int temp1 = xTemp;
		 int temp2 = yTemp;
		 xTemp = (int)(temp1 * Math.cos(angle) - temp2*Math.sin(angle));
		 yTemp = (int)(temp1 * Math.cos(angle) + temp2*Math.cos(angle));
		 xTemp += xCenter;// the X coordinate of new point 
		 yTemp += yCenter;// the Y coordinate of new point
		 p.addPoint(xTemp,yTemp);//add new point 
		
	}
		 g.drawPolygon(p); 
	}
	 

}
