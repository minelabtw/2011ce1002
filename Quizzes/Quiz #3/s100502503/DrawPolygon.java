package q3.s100502503;

import java.awt.*;

import javax.swing.*;

public class DrawPolygon extends JLabel
{
	protected int vertices = 4;
	protected double angle = 2 * Math.PI / vertices;
	protected int Radius = (int)(Math.min(getWidth(), getHeight()) * 0.2);
	
	public void setNumVertex(int a)
	{
		vertices = a;
		angle = Math.PI / vertices;
	}
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		Polygon polygon = new Polygon();
		
		int xCenter = getWidth() / 2;
		int yCenter = getHeight() / 2; 
		
		int[] xTemp = new int[vertices];
		int[] yTemp = new int[vertices];
		
		xTemp[0] = xCenter;
		yTemp[0] = yCenter - Radius;
		
		polygon.addPoint(xTemp[0], yTemp[0]);

		xTemp[1] = xCenter;
		yTemp[1] = yCenter - Radius;
		
		for(int i = 1; i < vertices; i++)
		{
			xTemp[i] -= xCenter;
			yTemp[i] = yCenter;
			
			int temp = xTemp[i];
			xTemp[i] = (int)(temp * Math.cos(angle) - yTemp[i] * Math.sin(angle));
			yTemp[i] = (int)(temp * Math.sin(angle) + yTemp[i] * Math.cos(angle));
			
			xTemp[i] += xCenter;
			yTemp[i] += yCenter;
			polygon.addPoint(xTemp[i], yTemp[i]);
		}

		g.drawPolygon(xTemp, yTemp, vertices);		
	}
}
