package q3.s100502503;

import java.awt.BorderLayout;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.Border;

public class FrameWork extends JFrame implements ActionListener
{
	public DrawPolygon d = new DrawPolygon();
	public JButton plus = new JButton("+");
	public JButton minus = new JButton("-");
	public JPanel p1 = new JPanel();
	
	public FrameWork()
	{
		p1.add(plus, BorderLayout.WEST);
		p1.add(minus, BorderLayout.EAST);
		
		add(d, BorderLayout.CENTER);
		add(p1, BorderLayout.SOUTH);
		
		plus.addActionListener(this);
		minus.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == plus)
		{
			d.setNumVertex(d.vertices++);
			d.repaint();
		}
		else if(e.getSource() == minus)
		{
			if(d.vertices > 3)
				d.setNumVertex(d.vertices--);
			d.repaint();
		}
		else;
	}
}
