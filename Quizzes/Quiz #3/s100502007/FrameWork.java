package q3.s100502007;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener{
	private JPanel buttonPanel = new JPanel();
	private JButton addButton = new JButton("+");
	private JButton minusButton = new JButton("-");
	DrawPolygon draw = new DrawPolygon();
	
	public FrameWork(){
		setLayout( new BorderLayout());
		buttonPanel.setLayout( new GridLayout(1, 2, 1 , 1));
		buttonPanel.setSize(500, 500);
		buttonPanel.add(addButton);
		buttonPanel.add(minusButton);

		addButton.addActionListener(this);
		minusButton.addActionListener(this);

		add(draw, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
	}
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == addButton){
			draw.numberplus();
			draw.repaint();
		}

		else if(e.getSource() == minusButton){
			draw.numberminus();
			draw.repaint();
		}
	}
}
