package q3.s100502007;

import java.awt.*;
import javax.swing.JLabel;

public class DrawPolygon extends JLabel{
	private int number = 4;
	public void numberplus(){
		number=number+1;
	}
	public void numberminus(){
		number=number-1;
	}
	public int getnumber(){
		return number;
	}
	public DrawPolygon(){
		
	}
	Polygon p = new Polygon();
	protected void paintComponent(Graphics g){
		super.paintComponent(g);

		int PolygonRadius = (int)(Math.min(getWidth(), getHeight())*0.8*0.5);
		int xCenter = (getWidth()/2);
		int yCenter = (getHeight()/2);

		g.setColor(Color.BLACK);
		/*for(int i=0;i<=getnumber()-1;i++){  這是debug de不出來的,等下問助教~~
			p.addPoint(xCenter + (int)(PolygonRadius*(Math.sin(i*(2 * Math.PI/getnumber())))),
					yCenter - (int)(PolygonRadius*(Math.cos(i*(2 * Math.PI/getnumber())))));
			if(i==getnumber()-1){
				g.drawPolygon(p);
			}
		}*/
		for(int i=0;i<=getnumber()-1;i++){
			g.drawLine(xCenter + (int)(PolygonRadius*(Math.sin(i*(2 * Math.PI/getnumber())))),
					yCenter - (int)(PolygonRadius*(Math.cos(i*(2 * Math.PI/getnumber())))),
					xCenter + (int)(PolygonRadius*(Math.sin((i+1)*(2 * Math.PI/getnumber())))),
					yCenter - (int)(PolygonRadius*(Math.cos((i+1)*(2 * Math.PI/getnumber())))));
		}
	}
}
