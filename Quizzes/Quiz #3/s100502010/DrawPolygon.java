package q3.s100502010;

import java.awt.Graphics;
import java.awt.Polygon;
import javax.swing.JLabel;

public class DrawPolygon extends JLabel
{
	public int vertex=4;
	private double xcenter=400;
	private double ycenter=400;
	private int radius=15;
	private double angle=0;
	public double xtemp;
	public double ytemp;
	
	public void setNumVertex(int a)
	{
		angle=(2*Math.PI)/a;
	}

	protected void paintComponent(Graphics g)
	{
		Polygon polygon=new Polygon();
		
		xtemp=xcenter;
		ytemp=ycenter-radius;
		polygon.addPoint((int)xtemp,(int)ytemp);
		for(int counter=1;counter<=vertex-1;counter++);
		{
			double temp=xtemp;
			xtemp-=xcenter;
			ytemp=ycenter;
			xtemp=(int)(temp*Math.cos(angle)-ytemp*Math.sin(angle));
			ytemp=(int)(temp*Math.sin(angle)+ytemp*Math.cos(angle));
			xtemp=xtemp+xcenter;
			ytemp=ytemp+ycenter;
			polygon.addPoint((int)(xtemp),(int)(ytemp));
		}
		g.drawPolygon(polygon);
		
		
		
	}
}
