package q3.s100502010;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class FrameWork extends JFrame implements ActionListener
{
	private JButton plus= new JButton("+");
	private JButton minus= new JButton("-");
	DrawPolygon draw=new DrawPolygon();
	
	public FrameWork()
	{
		JPanel p1=new JPanel();
		p1.setLayout(new GridLayout(1,2,5,5));
		p1.add(plus);
		p1.add(minus);
		
		setLayout(new BorderLayout(5,5));
		add(draw,BorderLayout.CENTER);
		add(p1,BorderLayout.SOUTH);
		
		plus.addActionListener(this);
		minus.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource()==plus)
			{
				draw.vertex=draw.vertex+1;
				draw.setNumVertex(draw.vertex);
				repaint();
			}
			else if(e.getSource()==minus)
			{
				draw.vertex=draw.vertex-1;
				draw.setNumVertex(draw.vertex);
				repaint();
			}
	}
}
