package q3.s100502517;

import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrameWork extends JFrame{
	JButton plus = new JButton(" + ");
	JButton min = new JButton(" - ");
	DrawPolygon polygon = new DrawPolygon();
	action a = new action();
	
	//版面設計
	public FrameWork(){
		JPanel p1 = new JPanel();
		p1.add(plus);
		p1.add(min);
		
		setLayout(new BorderLayout());
		add(polygon, BorderLayout.CENTER);//圖形
		add(p1, BorderLayout.SOUTH);//button
		
		plus.addActionListener(a);
		min.addActionListener(a);
		
	}
	
	
	class action implements ActionListener{		
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == plus){
				polygon.setNumVertex(polygon.getVertices()+1);//邊長+1
			}
			if(e.getSource() == min){
				polygon.setNumVertex(polygon.getVertices()-1);//邊長-1
			}
			
			
		}
	}
}
