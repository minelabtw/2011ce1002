package q3.s100502517;
import java.awt.*;
import javax.swing.*;

public class DrawPolygon extends JPanel{
	private int vertices = 4;
	private int radius;
	
	
	public DrawPolygon(){
		setNumVertex(vertices);		
	}
	
	//得到邊長數
	public int getVertices(){
		return vertices;
	}
	
	//算出角度
	public int Countangle(int b) {
		int angle = (int)(2*b*Math.PI/vertices);
		return angle;
	}	

	//設置邊長數
	public void setNumVertex(int a){
		this.vertices = a;
	}
	
	//劃出圖形
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		Polygon p = new Polygon();
		
		int xCenter = getWidth()/2;
		int yCenter = getHeight()/2;
		
		radius = (int)(Math.min(xCenter*0.5, yCenter*0.5));//圖形會隨視窗變大變小
		
		
		int[] x = new int[vertices];
		int[] y = new int[vertices];
		
		for(int i = 0; i < vertices; i++){
			x[i] = (int) (xCenter+radius*Math.sin(2*(i+1)*Math.PI/vertices));
			y[i] = (int) (yCenter+radius*Math.cos(2*(i+1)*Math.PI/vertices));
			p.addPoint(x[i],y[i]);
		}
		
		g.drawPolygon(p);		
		repaint();
	}

}
