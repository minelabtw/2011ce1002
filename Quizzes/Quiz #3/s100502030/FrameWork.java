package q3.s100502030;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;


public class FrameWork extends JFrame implements ActionListener {
	private JButton add = new JButton("+");
	private JButton m = new JButton("-");
	private DrawPolygon dp = new DrawPolygon();
	private int number = 4;

	public FrameWork() {

		JPanel p = new JPanel();
		p.setLayout(new GridLayout(1, 2));
		p.add(add);
		p.add(m);
		// add button

		add(dp, BorderLayout.CENTER);// add polygon
		add(p, BorderLayout.SOUTH);

		add.addActionListener(this);
		m.addActionListener(this);// 
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == add) {
			number += 1;
			dp.setNumVertex(number);
			dp.repaint();// add event
		} else if (e.getSource() == m) {
			number -= 1;
			dp.setNumVertex(number);
			dp.repaint();
			// m event
		}
	}
}
