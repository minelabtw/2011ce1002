package q3.s100502030;

import java.awt.*;
import javax.swing.*;

public class DrawPolygon extends JPanel {
	private int number = 4;

	

	public void setNumVertex(int a) {
		number = a;
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int xCenter = getWidth() / 2;
		int yCenter = getHeight() / 2;
		double r = Math.min(getHeight(), getWidth()) * 0.8 * 0.5;

		Polygon p = new Polygon();

		for (int i = 0; i < number; i++) {
			p.addPoint(
					xCenter + (int) (r * Math.sin(2 * Math.PI / number * i)),
					yCenter - (int) (r * Math.cos(2 * Math.PI / number * i)));//set point
		}// add point

		g.drawPolygon(p);
	}
}
