package q3.s100502545;

import java.awt.Graphics;
import java.awt.Polygon;

import javax.swing.JButton;
import javax.swing.JPanel;

public class DrawPolygon extends JPanel 
{
	int vertice = 4;
	int angle = 0 ;
	
	public DrawPolygon()
	{

	}
	
	public void setNumVertex(int a)
	{
		int vertice  = a;
		angle = (int)(2*Math.PI/a);
	}
	
	
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		int xCenter = getWidth()/2;
		int yCenter = getHeight()/2;
		int radius = (int)(Math.min(getWidth(),getHeight())*0.4);
			
		Polygon polygon = new Polygon();
			
		
		
		int x[] = new int [vertice];
		int y[] = new int [vertice];
		for(int i=0;i<vertice;i++)
		{
			x[0] = xCenter+radius;
			y[0] = yCenter;
			x[i] -= xCenter;
			y[i] -= yCenter;
			
			int temp = x[i];
			
			x[i]=(int)(temp*Math.cos(angle)-y[i]*Math.sin(angle));
			y[i]=(int)(temp*Math.sin(angle)-y[i]*Math.cos(angle));
			x[i]+=xCenter;
			y[i]+=yCenter;
			
			g.drawLine(xCenter,yCenter,x[i],y[i]);
		}
		
	}

}
