package q3.s100502545;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class FrameWork extends JFrame implements ActionListener
{
	private JButton add = new JButton("+");
	private JButton dec = new JButton("-");
	
	public FrameWork()
	{
		
		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(1,2,1,1));
		p1.add(add);
		p1.add(dec);
		add(p1,BorderLayout.SOUTH);
		add(new DrawPolygon(),BorderLayout.NORTH);
		add.addActionListener(this);
		dec.addActionListener(this);
		
	}


	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource()==add)
		{
			
		}
		
		if(e.getSource()==dec)
		{
			
		}
	}
	
	
}
