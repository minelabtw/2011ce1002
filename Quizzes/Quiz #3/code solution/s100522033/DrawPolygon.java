package q3.s100522033;
import java.awt.*;
import javax.swing.*;
import java.util.*;

public class DrawPolygon extends JLabel{
	private int numVertex = 4; 
	private double angle = Math.PI/2;
	int xCenter;
	int yCenter;
	int radius = 150;

	int xTemp,yTemp;
	
	public DrawPolygon(int Width,int Height){ // initial the polygon to Quadrilateral
		xCenter = Width/2;
		yCenter = Height/2;
		xTemp = xCenter; // set the next point(1st point) to the upper one
		yTemp = yCenter-radius;
	}
	public void setNumVertex(int a){ // set the number of vertices to argument
		numVertex = a;
		angle = 2*Math.PI / numVertex; 
		System.out.println("numVertex "+numVertex+"angle "+angle);
	}
	public double getAngle(){
		return angle;
	}
	
	protected void paintComponent(Graphics g){ // paint component
		super.paintComponent(g);	
		Polygon polygon = new Polygon();
		System.out.println("Point 1 "+xTemp+" "+yTemp);
		
		polygon.addPoint(xTemp, yTemp); // add the 1st point to the polygon object
		for(int i=1;i<numVertex;i++){
			System.out.println(i);
			
			// translate the n-1th point from actual position to the corresponding position of original point
			xTemp -= xCenter; 
			yTemp -= yCenter;
			
			// calculate the nth point
			int temp;
			temp = xTemp;	
			xTemp = (int)(temp*Math.cos(angle)-yTemp*Math.sin(angle));
			yTemp = (int)(temp*Math.sin(angle)+yTemp*Math.cos(angle));
			
			// translate the nth point from original point to the actual position
			xTemp += xCenter;
			yTemp += yCenter;
			
			System.out.println("Point "+(i+1)+" "+xTemp+" "+yTemp);
			polygon.addPoint(xTemp, yTemp); // add the nth point to the polygon		
		}
		g.drawPolygon(polygon); // draw polygon
		
		// set the center of polygon for the next point calculation
		xTemp = xCenter;
		yTemp = yCenter-radius;
		
		
	}
	
}
