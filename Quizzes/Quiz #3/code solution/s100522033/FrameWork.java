package q3.s100522033;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener{
	
	private JPanel topPanel = new JPanel(); // panel to show the polygon
	private JPanel bottomPanel = new JPanel(); // panel for buttons
	private JButton plusButton = new JButton("+");
	private JButton minusButton = new JButton("-");
	private int numVertex=4; // number of vertices
	
	DrawPolygon drawploygon = new DrawPolygon(600,600);
	
	public FrameWork(){
		
		setLayout(new BorderLayout());
		topPanel.setLayout(new GridLayout(1,1,1,1));
		topPanel.setSize(500, 300);
		topPanel.add(drawploygon);
		bottomPanel.setLayout(new GridLayout(1,2,1,1));
		bottomPanel.add(plusButton);
		bottomPanel.add(minusButton);
		
		add(topPanel,BorderLayout.CENTER);
		add(bottomPanel,BorderLayout.SOUTH);
		
		
		plusButton.addActionListener(this);
		minusButton.addActionListener(this);
		
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) { // implement actionListner
		if(e.getSource()==plusButton){
			System.out.println("plus");
			drawploygon.setNumVertex(++numVertex); // increase number of vertices
			drawploygon.repaint();
		}
		else if(e.getSource()==minusButton){
			System.out.println("minus");
			drawploygon.setNumVertex(--numVertex); // decrease number of vertices
			drawploygon.repaint();
		}
		
	}

}
