package q3.s100502519;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener{

	JButton plus = new JButton("+");
	JButton minus = new JButton("-");
	JPanel control = new JPanel();
	DrawPolygon polygon = new DrawPolygon();
	
	int vt = 4;
	
	public FrameWork(){
		setLayout(new BorderLayout(0,0));
		control.setLayout(new GridLayout(1,2,0,0));
		
		control.add(minus);
		control.add(plus);
		add(polygon,BorderLayout.CENTER);
		add(control,BorderLayout.SOUTH);
		
		plus.addActionListener(this);
		minus.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == minus){
			vt--;
			DrawPolygon.setNumVertex(vt);
			repaint();
		}
		
		if(e.getSource() == plus){
			vt++;
			DrawPolygon.setNumVertex(vt);
			repaint();
		}
	}
}
