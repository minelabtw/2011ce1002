package q3.s100502519;
import javax.swing.*;
import java.awt.*;

public class DrawPolygon extends JPanel{
	int xCenter = getWidth()/2;
	int yCenter = getHeight()/2;
	double radius = getWidth()/2*0.8;
	static int vertices = 4;
	double angle = Math.PI/2;
	int b=0;
	
	public static void setNumVertex(int a){
		vertices = a;
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		
		Polygon p = new Polygon();
		/*p.addPoint(xCenter, (int)(yCenter-radius));
		p.addPoint((int)(xCenter-radius), yCenter);
		p.addPoint((int)(xCenter+radius), yCenter);
		p.addPoint(xCenter, (int)(yCenter+radius));
		g.drawPolygon(p);
		g.drawString("123", xCenter, yCenter);*/
		
		for(int i=vertices;i>0;i--){
			int nextX = (int)(200 + radius*Math.sin(b*(2*Math.PI/vertices)));
			int nextY = (int)(200 - radius*Math.cos(b*(2*Math.PI/vertices)));
			p.addPoint(nextX,nextY);
			b++;
		}
		b=0;
		g.drawPolygon(p);
		
		/*for(int i=vertices;i>0;i--){
			int xTemp = ;
			int yTemp = ;
			
		}*/
	}
}


