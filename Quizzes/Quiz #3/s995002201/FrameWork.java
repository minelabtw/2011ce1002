package q3.s995002201;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class FrameWork extends JFrame implements ActionListener
{
	private DrawPolygon d  = new DrawPolygon();
	JButton add = new JButton("add");
	JButton minus = new JButton("minus");
	JPanel jj = new JPanel();
	protected int verticle = 4;//四邊形
	public FrameWork()
	{
		setLayout(new BorderLayout());
		jj.setSize(200, 200);
		setLayout(new GridLayout(5,5,6,0));
		add(add,BorderLayout.CENTER);
		add(minus,BorderLayout.SOUTH);
		
		add.addActionListener(this);
		minus.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource()==add)
		{
			System.out.println("add");//test
			verticle++;//邊形++
			repaint();
		}
		if(e.getSource()==minus)
		{
			System.out.println("minus");//test
			verticle--;//邊形--
			repaint();
		}
	}
}
