package q3.s995002201;
import java.awt.*;
import javax.swing.JLabel;

public class DrawPolygon extends JLabel
{
	protected int seta;
	protected int verticle = 4;//�|���
	protected double angle = (Math.PI)/2;//90��
	public int setNumVertex(int a)
	{
		angle = (2*Math.PI)/verticle;
		return (int) angle;
	}
	protected void paintComponent(Graphics g)
	{	
		super.paintComponent(g);
		int clockRadius = (int)(Math.min(getWidth(), getHeight())*0.8*0.5);
		int xCenter = getWidth()/2;
		int yCenter = getHeight()/2;
		int temp,xtemp = 0,ytemp = 0;
		xtemp -= xCenter;
		ytemp -= yCenter;
		temp = xtemp;
		
		xtemp = (int)(temp*Math.cos(angle) - ytemp*Math.sin(angle));
		ytemp = (int)(temp*Math.sin(angle) + ytemp*Math.cos(angle));
		
		xtemp += xCenter;
		ytemp += yCenter;
		g.drawOval(xCenter-clockRadius,yCenter-clockRadius,2*clockRadius,2*clockRadius);
		//g.drawPolygon(xtemp,ytemp,verticle);
		verticle++;
	}
}
