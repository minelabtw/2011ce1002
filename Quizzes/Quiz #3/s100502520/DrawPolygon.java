package q3.s100502520;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class DrawPolygon extends JPanel {
	private int xCenter;
	private int yCenter;
	private int num_angle;
	private int radius;
	private double angle;
	
	public void setNumVertex(int a){
		num_angle = a;
		angle =Math.PI*2/a;			
	}
	
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		xCenter = (getWidth()/2);
		yCenter = (getHeight()/2);
		double temp;
		int xTemp = xCenter;
		int yTemp = yCenter - radius;
		Polygon polygon = new Polygon();
		g.setColor(Color.BLACK);
		polygon.addPoint(xTemp, yTemp);
		for(int i = 1; i<num_angle;i++){
			xTemp -= xCenter;
			yTemp -= yCenter;
			
			temp = xTemp;
			xTemp = (int)(temp*Math.cos(angle) - yTemp*Math.sin(angle));
			yTemp = (int)(temp*Math.sin(angle) + yTemp*Math.cos(angle));
			polygon.addPoint(xTemp, yTemp);
			xTemp += xCenter;
			yTemp += yCenter;
		}
		g.drawPolygon(polygon.xpoints, polygon.ypoints, num_angle);
	
	}
	
}
