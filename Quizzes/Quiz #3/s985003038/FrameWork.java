package q3.s985003038;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class FrameWork extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	private DrawPolygon polygon;
	private JButton add, minus;
	
	public FrameWork(){										// constructor
		add = new JButton("Add");							// create the buttons
		minus = new JButton("Remove");
		add.setFont(new Font("Arial", Font.BOLD, 15));
		minus.setFont(new Font("Arial", Font.BOLD, 15));
		add.addActionListener(this);						// adding buttons into event handler
		minus.addActionListener(this);
		
		this.setLayout(new BorderLayout(2,1));
		polygon = new DrawPolygon();						// create a polygon object
		this.add(polygon);
		
		JPanel controlPane = new JPanel();					// create a panel to hold the two buttons
		controlPane.setLayout(new GridLayout(1,2));
		controlPane.add(add);
		controlPane.add(minus);
		this.add(controlPane, BorderLayout.SOUTH);
		
		this.setSize(400, 400);								// set the frame properties
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Quiz #3");
		this.setVisible(true);
		JOptionPane.showMessageDialog(null, "You can try to add over 30 vertices and see the fixed result of floating point error", "Quiz #3", JOptionPane.INFORMATION_MESSAGE);
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == add)							// if the add button is clicked
			polygon.setNumVertex(polygon.getNumVertex()+1);	// call the method to add one vertex
		else if(e.getSource() == minus)						// otherwise, the minus button is clicked
			polygon.setNumVertex(polygon.getNumVertex()-1);	// call the method to remove one vertex
	}
}
