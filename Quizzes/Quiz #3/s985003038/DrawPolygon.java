package q3.s985003038;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class DrawPolygon extends JLabel {
	private static final long serialVersionUID = 1L;
	private int xCenter = 200;
	private int yCenter = 200;
	private int radius = 100;
	private int numOfVertex;
	private int[] VertexX;
	private int[] VertexY;
	private double angle;
	private Color[] colorSet = {Color.blue, Color.red, Color.gray, Color.green, Color.orange, Color.yellow};
	
	public DrawPolygon(){							// constructor using default number of vertex
		setNumVertex(4);
	}
	
	public DrawPolygon(int numOfVertex){			// constructor with specified number of vertex 
		setNumVertex(numOfVertex);
	}
	
	public int getNumVertex(){						// get the number of vertex method
		return numOfVertex;
	}
	
	public void setNumVertex(int a){				// set the number of vertex method
		if(a < 3)									// check if the number of vertex can form a polygon
			JOptionPane.showMessageDialog(null, "This is not a polygon", "Quiz #3", JOptionPane.ERROR_MESSAGE);
		else {
			numOfVertex = a;						// setting the basic information
			angle = 2 * Math.PI / a;
			VertexX = new int[a];
			VertexY = new int[a];
			
			VertexX[0] = xCenter;					// define the first vertex at top of the center
			VertexY[0] = yCenter - radius;
													// using the previous vertex to calculate the next vertex
			for(int i = 1; i <= a / 2; i++){
				VertexX[i] = (int)((VertexX[i-1]-xCenter)*Math.cos(angle) - (VertexY[i-1]-yCenter)*Math.sin(angle)) + xCenter;
				VertexY[i] = (int)((VertexX[i-1]-xCenter)*Math.sin(angle) + (VertexY[i-1]-yCenter)*Math.cos(angle)) + yCenter;
			}
													// using the next vertex to calculate the previous vertex
			for(int i = a - 1; i >= a / 2 + 1; i--){
				VertexX[i] = (int)((VertexX[(i+1)%a]-xCenter)*Math.cos(-angle) - (VertexY[(i+1)%a]-yCenter)*Math.sin(-angle)) + xCenter;
				VertexY[i] = (int)((VertexX[(i+1)%a]-xCenter)*Math.sin(-angle) + (VertexY[(i+1)%a]-yCenter)*Math.cos(-angle)) + yCenter;
			}
			repaint();
		}
	}

	protected void paintComponent(Graphics g){		// painting method to paint out the polygon
		super.paintComponent(g);
		g.drawString("Number of Vertices: " + numOfVertex, 20, 30);
		g.setColor(colorSet[numOfVertex%colorSet.length]);
		g.fillPolygon(VertexX, VertexY, numOfVertex);
		g.setColor(Color.black);
		g.drawLine(xCenter - 5, yCenter - 5, xCenter + 5, yCenter + 5);
		g.drawLine(xCenter - 5, yCenter + 5, xCenter + 5, yCenter - 5);
													// point out the center
	}
}
