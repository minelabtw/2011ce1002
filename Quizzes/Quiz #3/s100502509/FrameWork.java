package q3.s100502509;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class FrameWork extends JFrame implements ActionListener{
	int initialVertice=4;
	JButton jbtadd = new JButton("+");
	JButton jbtdec = new JButton("-");
	DrawPolygon poly = new DrawPolygon();
	public FrameWork(){
		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(1,2));
		p1.add(jbtdec);
		p1.add(jbtadd);
		jbtadd.addActionListener(this);
		jbtdec.addActionListener(this);
		poly.setNumVertex(initialVertice);
		add(p1,BorderLayout.SOUTH);
		add(new DrawPolygon() ,BorderLayout.NORTH);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==jbtadd){
			poly.setNumVertex(initialVertice+1);
			poly.repaint();
		}
		
		if(e.getSource()==jbtdec){
			poly.setNumVertex(initialVertice-1);
			poly.repaint();
		}
	}

}
