package q3.s100502509;

import java.awt.Graphics;

import java.awt.Polygon;

import javax.swing.JPanel;

public class DrawPolygon extends JPanel {
	int angle=0;
	int Vertice=4;
	public DrawPolygon(){

		
	}
	
	public void setNumVertex(int a){
		 Vertice=a;
		 angle = (int) (2*Math.PI/a);
		
	}
	
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		Polygon p = new Polygon();

		int radius = (int) (Math.min(getWidth(), getHeight())*4);
		int xCenter = getWidth()/2;
		int yCenter = getHeight()/2;
		int x [] = new int [Vertice];
		int y [] = new int [Vertice];
		x[0]=xCenter;
		y[0]=yCenter+radius;
		for(int i=1 ; i <4 ; i++)
		{
			
			x[i-1] -= xCenter;
			y[i-1] -= yCenter;
			int temp = x[i-1];
			x[i] = (int)(temp*Math.cos(angle)-y[i]*Math.sin(angle));
			y[i] = (int)(temp*Math.sin(angle)+y[i]*Math.cos(angle));
			x[i]+=xCenter;
			y[i]+=yCenter;
		}
		
		for(int i=0;i<Vertice-1;i++){
			g.drawLine(x[i], y[i], x[i+1], y[i+1]);
		}
		
		g.drawLine(x[0], y[0], x[Vertice-1], y[Vertice-1]);
		
		
	}



}
