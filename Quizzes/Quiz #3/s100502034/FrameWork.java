package q3.s100502034;
import java.awt.BorderLayout;
import java.awt.event.*;
import javax.swing.*;
public class FrameWork extends JFrame  {
	private JButton addButton = new JButton("+");//+Button
	private JButton minButton = new JButton("-");//-Button
	private JButton chanceD = new JButton("fill Draw Change");
	private JLabel display = new JLabel("the number of angle is 4");//陪ボ瞷Τぶ唉à
	DrawPolygon drawP = new DrawPolygon();//DrawPolygon
	public FrameWork() {
		JPanel p = new JPanel();
		Listener listener = new Listener();//ミListenerノㄓ糶Button暗或
		p.add(addButton);
		p.add(minButton);//ノpㄓㄢButton
		p.add(chanceD);
		p.add(display);//陪ボ瞷Τぶ唉à
		addButton.addActionListener(listener);
		minButton.addActionListener(listener);
		chanceD.addActionListener(listener);
		add(drawP, BorderLayout.CENTER);
		add(p,BorderLayout.SOUTH);
	}
	class Listener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == addButton){
				drawP.setNumVertex(-1);//DrawPolygon砞砞把计1 à++
				display.setText("the number of angle is " + drawP.getAngle());
				drawP.repaint();
			}
			if(e.getSource() == minButton){//把计ぃ1 à--
				drawP.setNumVertex(-2);
				display.setText("the number of angle is " + drawP.getAngle());
				drawP.repaint();
			}
			if(e.getSource() == chanceD){
				drawP.setChanceD();
				drawP.repaint();
			}
		}
	}
}
