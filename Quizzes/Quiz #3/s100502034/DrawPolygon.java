package q3.s100502034;
import java.awt.Graphics;
import javax.swing.*;
public class DrawPolygon extends JPanel{
	private int numOfAngle = 4;//起始的角數是4
	private int[] x = new int[100];//宣告一個足夠大的x助標
	private int[] y = new int[100];//宣告一個足夠大的Y助標
	private int contorlD = 1;
	public DrawPolygon(){//只是用來被人宣告
	}
	public int getAngle(){
		return numOfAngle;
	}
	public void setChanceD(){
		contorlD = contorlD * (-1);
	}
	public void setNumVertex(int a){
		if (a == -1){//用這個methods來控制圖形的邊數
			if (numOfAngle != 100){
				numOfAngle++;
			}
			else{
				System.err.println("there have much angle, don' t ++ it again");//因為陣列大小有上限
			}
			
		}
		else if (a ==-2){
			if (numOfAngle !=0){
				numOfAngle--;
			}
			else{
				System.err.println("the angle is 0, don' t -- it again");
			}
			
		}
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
	    int xC = getWidth() / 2;//Frame中心點的X坐標
	    int yC = getHeight() / 2;//Frame中心點的Y坐標
	    int l = (int)(Math.min(getWidth(), getHeight())* 0.45);
	    for (int i = 0 ; i<numOfAngle;i++){
		    x[i] = (int)(xC + l * Math.sin(i*(2 * Math.PI /numOfAngle)));//以i循環  把每一個點的坐標加入陣列裡
		    y[i] = (int)(yC - l * Math.cos(i*(2 * Math.PI /numOfAngle)));
	    }
	    if (contorlD == 1){
	    	g.drawPolygon(x, y, numOfAngle);
	    }
	    else{
	    	g.fillPolygon(x, y, numOfAngle);
	    }
	}
}
