package q3.s100502026;

import java.awt.*;
import javax.swing.*;

public class DrawPolygon extends JPanel{
	
	int number = 4;
	
	public void setNumber( int number)
	{
		this.number=number;
	}
	
	public int getNumber()
	{
		return number;
	}
	
	DrawPolygon()
	{
		
	}

	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		Polygon polygon = new Polygon();
		int xCenter = getWidth();
		int yCenter = getHeight();
		int radius = 200;
		
		for(int i = 1 ; i < number ; i++ )
		{
			int xTemp = xCenter; 
			int yTemp = yCenter;
			int temp = xTemp;
			int angle = (int) (Math.PI/number);
			
			polygon.addPoint(xCenter, yCenter - radius );
			
			xTemp -= xCenter;
			yTemp -= yCenter;
			
			temp = xTemp;
			xTemp = (int)( temp * Math.cos(angle) - yTemp * Math.sin(angle) );
			yTemp = (int)( temp * Math.sin(angle) + yTemp * Math.cos(angle) );
			polygon.addPoint(xTemp, yTemp);
			
			xTemp += xCenter;
			yTemp += yCenter;
		}

		g.drawPolygon(polygon);
	}
}
