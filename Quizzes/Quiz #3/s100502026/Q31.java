package q3.s100502026;

public class Q31 {
	
	public static void main(String[] args)
	{
		FrameWork frame = new FrameWork();
		
		frame.setTitle( "Draw Polygons." );
		frame.setDefaultCloseOperation( frame.EXIT_ON_CLOSE );
		frame.setSize( 500 , 500 );
		frame.setLocationRelativeTo( null );
		frame.setVisible( true );
	}

}
