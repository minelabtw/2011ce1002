package q3.s100502026;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class FrameWork extends JFrame{
	
	private JPanel controlPanel = new JPanel();
	private JPanel graphicPanel  = new JPanel();
	private JButton jbtPlus = new JButton( "+" );
	private JButton jbtMinus = new JButton( "-" );
	private DrawPolygon drawPolygon = new DrawPolygon();
	private ButtonListener listener = new ButtonListener();

	FrameWork()
	{
		jbtPlus.addActionListener((ActionListener) this);
		
		graphicPanel.add( drawPolygon );
		controlPanel.add( jbtPlus );
		controlPanel.add( jbtMinus );
		
		add( controlPanel , BorderLayout.SOUTH );
		add( graphicPanel , BorderLayout.NORTH );
	}
	
	class ButtonListener implements ActionListener()
	{
		public void actionPerformed( ActionEvent e)
		{
			if( e.getSource() == jbtPlus )
			{
				drawPolygon.setNumber( drawPolygon.getNumber() + 1 );
				repaint();
			}
			else
			{
				drawPolygon.setNumber( drawPolygon.getNumber() - 1 );
				repaint();
			}
		}
	}
}
