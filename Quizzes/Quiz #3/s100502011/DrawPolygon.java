package q3.s100502011;
import javax.swing.*;
import java.awt.*;

public class DrawPolygon extends JPanel{
	public int vertics=4; // number of points
	public int radius = 60;
	public double angle;
	public int xcenter; // center of graphics
	public int ycenter; // center of graphics
	public int xtemp; // first point
	public int ytemp; // first point
	public DrawPolygon(){ // constructor
		
	}
	public void setNumVertex(int a){ // set number of points
		vertics = a;
	}
	protected void paintComponent(Graphics g){ // paint graphics
		super.paintComponent(g);
		Polygon p = new Polygon();
		xcenter=getWidth()/2;
		ycenter=getHeight()/2;
		xtemp=xcenter;
		ytemp=ycenter-radius;
		setNumVertex(vertics);
		angle = (2*Math.PI)/vertics;
		for(int i=1;i<vertics;i++){	 // count point
			if(i==1){
				p.addPoint(xtemp, ytemp);
			}
			xtemp-=xcenter;
			ytemp-=ycenter;
			int temp = xtemp;
			xtemp=(int)(temp*Math.cos(angle)-ytemp*Math.sin(angle));
			ytemp=(int)(temp*Math.sin(angle)+ytemp*Math.cos(angle));
			xtemp+=xcenter;
			ytemp+=ycenter;
			p.addPoint(xtemp, ytemp);
		} // end loop
		g.drawPolygon(p); // draw
	}
}
