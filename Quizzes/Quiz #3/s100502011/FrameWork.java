package q3.s100502011;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener{
	private JButton add = new JButton("+");
	private JButton minus = new JButton("-");
	private DrawPolygon polygon = new DrawPolygon();
	private JPanel button =new JPanel();
	private JPanel total = new JPanel();
	public FrameWork(){ // constructor
		button.setLayout(new GridLayout(1,2));
		button.add(add);
		button.add(minus);
		add.addActionListener(this);
		minus.addActionListener(this);
		
		//add to total panel
		total.setLayout(new GridLayout(2,1));
		total.add(polygon);
		total.add(button);
		
		add(total); // add to frame
	}
	
	
	public void actionPerformed(ActionEvent e){ // button action
		if(e.getSource()== add){ // press add button
			polygon.vertics++;
			repaint();
		}
		if(e.getSource()== minus){ // press minus button
			if(polygon.vertics>3){
				polygon.vertics--;
			}
			repaint();
		}
	}
}
