package q3.s100502033;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;;
public class FrameWork extends JFrame
{
	private static JButton add = new JButton("add");
	private static JButton minus = new JButton("minus");
	private static JPanel p1 = new JPanel();
	private static DrawPolygon draw = new DrawPolygon();
	public FrameWork() 
	{
		
		p1.add(add , BorderLayout.WEST);
		p1.add(minus , BorderLayout.EAST);
		add(draw , BorderLayout.CENTER);
		add(p1 , BorderLayout.SOUTH);
		Frame frame = new Frame();
		add.addActionListener(frame);
		minus.addActionListener(frame);
	}
	class Frame implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			if(event.getSource() == add)
			{
				draw.setNumVertex(true);
				repaint();//重新整玾
			}
			else if(event.getSource() == minus)
			{
				draw.setNumVertex(false);
				repaint();//重新整玾
			}
		}
	}
}
