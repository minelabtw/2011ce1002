package q3.s100502033;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
public class DrawPolygon extends JPanel
{
	private static int numberofpoints = 4;
	private static int[] xtemp = new int[10];
	private static int[] ytemp = new int[10];
	public void setNumVertex(boolean a)
	{
		if(a == true)
		{
			numberofpoints++;//按下ADD按鈕加一邊
		}
		else if(a == false)
		{
			numberofpoints--;//按下minus按鈕減一邊
		}
	}
	public void paintComponent(Graphics g)
	{
		int xCenter = getWidth() / 2; //取得中心點
        int yCenter = getHeight() / 2; //取得中心點
        int radius = (int) (Math.min(this.getWidth(), this.getHeight()) * 0.8 * 0.5);//取得半徑
        for( int i = 0 ; i < numberofpoints ; i++)//計算每一個點坐標
        {
        	xtemp[i] = (int)(xCenter + radius * Math.sin(i * (2 * Math.PI/numberofpoints)));//計算出X的坐標，利用2 PI 除以邊數得出的每一邊的角度就可以求出每一邊的坐票
        	ytemp[i] = (int)(yCenter - radius * Math.cos(i * (2 * Math.PI/numberofpoints)));//計算出Y的坐標，利用2 PI 除以邊數得出的每一邊的角度就可以求出每一邊的坐票
        }
        g.setColor(Color.red);  //用紅色
        g.drawPolygon( xtemp , ytemp , numberofpoints);//畫圖
	}
}
