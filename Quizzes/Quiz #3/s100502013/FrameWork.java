package q3.s100502013;
import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener{
	private JPanel totalpanel = new JPanel(new GridLayout(2,1));
	private JLabel outputlabel = new JLabel();
	private JPanel buttonpanel = new JPanel(new GridLayout(1,2));
	private JButton plusbutton = new JButton("+");
	private JButton minusbutton = new JButton("-");
	private Font myfont = new Font("TimesRoman", Font.BOLD , 20);
	private DrawPolygon quizdraw = new DrawPolygon();
	
	FrameWork(){ //Framework set
		plusbutton.setFont(myfont);
		minusbutton.setFont(myfont);
		plusbutton.addActionListener(this);
		minusbutton.addActionListener(this);
		buttonpanel.add(plusbutton);
		buttonpanel.add(minusbutton);
		totalpanel.add(quizdraw);
		totalpanel.add(buttonpanel);
		add(totalpanel);
	}
	
	public void actionPerformed(ActionEvent e) { //button event
		if(e.getSource()==plusbutton){
			quizdraw.setNumVertex(quizdraw.getVertice()+1);
			quizdraw.repaint();
		}
		else if(e.getSource()==minusbutton){
			quizdraw.setNumVertex(quizdraw.getVertice()-1);
			quizdraw.repaint();
		}
	}
}


