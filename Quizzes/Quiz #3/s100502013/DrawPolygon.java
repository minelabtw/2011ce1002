package q3.s100502013;
import java.awt.*;
import javax.swing.*;

public class DrawPolygon extends JLabel{
	private int[] xcoor = new int[100];
	private int[] ycoor = new int[100];
	private int xCenter = 300;
	private int yCenter = 140;
	private int vertice;
	private double angle;
	private int radius = 60;
	private int xTemp;
	private int yTemp;
	private int temp;
	
	DrawPolygon(){ //Constructor:initialized
		vertice = 4;
		angle = Math.PI / 2;
	}
	
	public void setNumVertex(int a){ //set vertice and angle
		vertice = a;
		angle = (2 * Math.PI) / vertice;
	}
	
	public int getVertice(){
		return vertice;
	}
	
	public void paintComponent(Graphics g){ //draw the graphics
		super.paintComponent(g);
		
		xTemp = 300;
		yTemp = 80;
		for(int i=0;i<vertice;i++){
			xcoor[i] = xTemp;
			ycoor[i] = yTemp;
			xTemp -= xCenter;
			yTemp -= yCenter;
			temp = xTemp;
			xTemp = (int)(temp*Math.cos(angle) - yTemp*Math.sin(angle));
			yTemp = (int)(temp*Math.sin(angle) + yTemp*Math.cos(angle));
			xTemp += xCenter;
			yTemp += yCenter;
		}
		Polygon mypolygon = new Polygon(xcoor,ycoor,vertice);
		g.drawPolygon(mypolygon);
	}
}
