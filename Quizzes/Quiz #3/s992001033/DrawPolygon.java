package q3.s992001033;

import java.awt.*;
import javax.swing.*;
public class DrawPolygon extends JPanel
{
	int xCenter=0,yCenter =0,vertices =0,angle =0,radius=0;
	DrawPolygon()
	{
		vertices = 4;
		angle = (int) (2*Math.PI/4);
		xCenter = this.getWidth()/2;
		yCenter = this.getHeight()/2;
	}
	public void setNumVertex(int a)
	{
		vertices = a;
		angle = (int) (2*Math.PI/a);
	}
	protected void paintComponent(Graphics g)
	{
		super.paintComponents(g);
		Polygon poly = new Polygon();
		int xTemp=xCenter,yTemp=yCenter-radius,temp=0;
		poly.addPoint(xCenter, yCenter-radius);
		for(int i =1;i<vertices;i++)
		{
			xTemp -= xCenter;
			yTemp -= yCenter;
			
			temp = xTemp;
			xTemp = (int)(temp*Math.cos(angle)-yTemp*Math.sin(angle));
			yTemp = (int)(temp*Math.sin(angle)+yTemp*Math.cos(angle));
			xTemp += xCenter;
			yTemp += yCenter;
			poly.addPoint(xTemp, yTemp);
		}
		g.drawPolygon(poly);
	}
	public int getVertice()
	{
		return vertices;
	}
}
