package q3.s992001033;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class FrameWork extends JFrame implements ActionListener
{
	
	JButton plus = new JButton("+");
	JButton minus = new JButton("-");
	DrawPolygon p1 = new DrawPolygon();
	FrameWork()
	{
		setLayout(new BorderLayout());
		add(p1,BorderLayout.CENTER);
		JPanel button = new JPanel(new GridLayout(1,2));
		plus.addActionListener(this);
		minus.addActionListener(this);
		button.add(plus);
		button.add(minus);
		add(button,BorderLayout.SOUTH);
	}
	public int getXCenter()
	{
		return getWidth()/2;
	}
	public int getYCenter()
	{
		return getHeight()/2;
	}
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()==plus)
			p1.setNumVertex(p1.getVertice()+1);
		else
			p1.setNumVertex(p1.getVertice()-1);
	}
}
