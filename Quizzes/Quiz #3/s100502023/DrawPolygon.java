package q3.s100502023;

import java.awt.Graphics;
import java.awt.Polygon;
import javax.swing.JLabel;

public class DrawPolygon extends JLabel 
{
	protected int theNumberOfVertices;
	protected int xCenter,yCenter;
	protected int radius=80;
	protected double angle;
	public DrawPolygon()
	{
		xCenter=getWidth()/2;
		yCenter=getHeight()/2;
		setNumVertex(4);
	}
	
	public void setNumVertex(int a)  //SET NumVertex
	{
		theNumberOfVertices=a;
		angle=(2*Math.PI/theNumberOfVertices);
	}
	
	protected void paintComponent(Graphics g)  //DRAW
	{
		Polygon polygon = new Polygon();
		xCenter=getWidth()/2;
		yCenter=getHeight()/2;
		
		int xTemp=xCenter,yTemp=yCenter-radius;
		int temp;

		polygon.addPoint(xCenter,yCenter-radius);

		g.drawPolygon(polygon);
		
		for (int i=0;i<theNumberOfVertices-1;i++)
		{
			
			//translation and rotation
			xTemp-=xCenter;
			yTemp-=yCenter;
			
			temp=xTemp;
			xTemp = (int)(temp*Math.cos(angle)-yTemp*Math.sin(angle));
			yTemp = (int)(temp*Math.sin(angle)+yTemp*Math.cos(angle));
			
			xTemp+=xCenter;
			yTemp+=yCenter;
			
			polygon.addPoint(xTemp, yTemp);
		
		}
		
		g.drawPolygon(polygon);
		
	}
}
