package q3.s100502023;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public  class FrameWork extends JFrame implements ActionListener
{
	protected JButton jbtforPlus = new JButton("+");
	protected JButton jbtforMinus = new JButton("-");
	protected JPanel p2 =new JPanel();
	DrawPolygon polygon = new DrawPolygon();
	protected int theNumberOfVertices_f=polygon.theNumberOfVertices;
	
	public FrameWork()
	{  //FRAME INITIAL
		setLayout(new BorderLayout(10,10));
		add(polygon,BorderLayout.CENTER);
		p2.setLayout(new GridLayout(1,2,10,10));
		
		jbtforPlus.addActionListener(this);
		jbtforMinus.addActionListener(this);
		
		p2.add(jbtforPlus);
		p2.add(jbtforMinus);
		
		add(p2,BorderLayout.SOUTH);
		
	}
	
	public void actionPerformed(ActionEvent e)  //ACTIONLISTENER
	{
		if (e.getSource()==jbtforPlus)
		{
			theNumberOfVertices_f+=1;

		}
		else if (e.getSource()==jbtforMinus)
		{
			theNumberOfVertices_f-=1;
		}
		
		polygon.setNumVertex(theNumberOfVertices_f);
		polygon.repaint();
	}
}

