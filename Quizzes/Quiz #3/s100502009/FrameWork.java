package q3.s100502009;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class FrameWork extends JFrame {
	private JButton addButton=new JButton("+");//set two buttons
	private JButton minusButton=new JButton("-");
	JPanel buttonPanel=new JPanel(new GridLayout(1,2,1,1));	
	DrawPolygon draw=new DrawPolygon();
	FrameWork()//draw the picture
	{
		buttonPanel.add(addButton);
		buttonPanel.add(minusButton);
		add(draw ,BorderLayout.CENTER);
		add(buttonPanel,BorderLayout.SOUTH);
		addButtonListener listener=new addButtonListener();
		addButton.addActionListener(listener);
		minusButtonListener listener2=new minusButtonListener();
		minusButton.addActionListener(listener2);
	}
	class addButtonListener implements ActionListener //set an add button
	{
		public void actionPerformed(ActionEvent e)
		{
			if(e.getSource()==addButton)
			{
				int vertice=draw.getVertice();
				vertice++;
				draw.setNumVertex(vertice);
				draw.repaint();
			}
		}
	}
	
	class minusButtonListener implements ActionListener//set an minus button
	{
		public void actionPerformed(ActionEvent e)
		{
			if(e.getSource()==minusButton)
			{
				int vertice=draw.getVertice();
				vertice--;
				draw.setNumVertex(vertice);
				draw.repaint();
			}
		}
	}
	
}
