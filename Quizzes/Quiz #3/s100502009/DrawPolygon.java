package q3.s100502009;
import javax.swing.*;
import java.awt.*;

public class DrawPolygon extends JPanel{
	private double angle=(Math.PI)/2;
	private int vertice=4;
	private int radius;
	private int xCenter;
	private int yCenter;
	
	public void paintComponent(Graphics g)//draw the polygon
	{
		super.paintComponent(g);
		Polygon polygon=new Polygon();
		xCenter=getWidth()/2;
		yCenter=getHeight()/2;
		radius=(int)Math.min(getWidth(), getHeight()*0.8*0.5);		
		int xTemp=xCenter;
		int yTemp=yCenter-radius;
		polygon.addPoint(xTemp,yTemp);
		for(int i=1;i<this.vertice;i++)
		{
			xTemp-=xCenter;
			yTemp-=yCenter;
			
			int temp=xTemp;
			xTemp=(int)(temp*Math.cos(angle)-yTemp*Math.sin(angle));
			yTemp=(int)(temp*Math.sin(angle)+yTemp*Math.cos(angle));
			xTemp+=xCenter;
			yTemp+=yCenter;
			polygon.addPoint(xTemp, yTemp);
		}
		g.drawPolygon(polygon);
		
	}
	
	public void setNumVertex(int a)//reset the properties
	{
		this.vertice=a;
		angle=(2*Math.PI)/a;
	}
	
	public int getVertice()
	{
		return vertice;
	}
	


}
