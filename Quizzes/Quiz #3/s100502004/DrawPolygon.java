package q3.s100502004;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Polygon;

import javax.swing.JPanel;
import javax.xml.transform.Templates;

public class DrawPolygon extends JPanel{
	int xCenter = getHeight(); 
	int yCenter = getWidth();
	int number = 4;
	double angle = Math.PI/2;
	int radius = 10;
	int xTemp = xCenter ;
	int yTemp = yCenter ;
	int xPoint[] = new int[number+1];
	int yPoint[] = new int[number+1];
	
	public DrawPolygon(){
		xPoint[0] =  xTemp;
		yPoint[0] =  yTemp;
	}
	
	public int getNumber(){
		return number;
	}
	
	public void setNumber(int n){
		number = n;
	}
	
	public void setNumVertex(int a){
		number = a;
		angle = Math.PI*2/a;
	}
	
	public void setPoints(){
		
		for(int i = 0 ; i < number; i ++){
			int temp;
			xTemp -= xCenter;
			yTemp -= yCenter;
			
			
			temp = xTemp;
			xTemp = (int)(temp*Math.cos(angle)-yTemp*Math.sin(angle));
			yTemp = (int)(temp*Math.sin(angle)+yTemp*Math.cos(angle));
			xPoint[i] = xTemp;
			yPoint[i] = yTemp;
			
			
			xTemp += xCenter;
			yTemp += yCenter;
			
		}
	}
	
	
	protected void paintComponent(Graphics g) {		
		super.paintComponent(g);
		g.drawPolyline(xPoint, yPoint, number);		
				
		
	}
	
	public Dimension getPreferredSize(){
		return new Dimension(200,200);
	}	
	
}
