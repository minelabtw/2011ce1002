package q3.s100502004;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class FrameWork extends JFrame implements ActionListener{
	JButton plus = new JButton("+");
	JButton minus = new JButton("-");
	DrawPolygon d = new DrawPolygon();
	public FrameWork(){		
		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(1,2));
		plus.addActionListener(this);
		minus.addActionListener(this);
		p1.add(plus);		
		p1.add(minus);
		add(d,BorderLayout.CENTER);
		add(p1,BorderLayout.SOUTH);
		
	}
	
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == plus){			
			d.setNumVertex(d.getNumber()+1);
			d.setNumber(d.getNumber()+1);
			d.setPoints();			
		}
		
		if(e.getSource() == minus){
			d.setNumVertex(d.getNumber()-1);
			d.setNumber(d.getNumber()-1);
			d.setPoints();			
		}
		
		d.repaint();
	}
	
	
}
