package q3.s100502008;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener {
	DrawPolygon polygon = new DrawPolygon();
	int initVertice = 4;
	JButton btadd = new JButton("+");
	JButton btmin = new JButton("-");

	public FrameWork() {// draw button
		JPanel p1 = new JPanel(new GridLayout(1, 2));
		p1.add(btadd);
		p1.add(btmin);
		JPanel p = new JPanel(new BorderLayout());
		p.add(polygon, BorderLayout.CENTER);
		p.add(p1, BorderLayout.SOUTH);
		add(p);
		btadd.addActionListener(this);
		btmin.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {// event
		if (e.getSource() == btadd) {// add
			initVertice++;
			polygon.setNumVertex(initVertice);
			repaint();
		}
		if (e.getSource() == btmin) {// minus
			if (initVertice == 1)// case that can't draw
			{
				initVertice = 1;
			} else {
				initVertice--;
			}
			polygon.setNumVertex(initVertice);
			repaint();
		}
	}
}
