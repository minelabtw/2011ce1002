package q3.s100502008;

import javax.swing.JFrame;

public class Q31 {
	public static void main(String[] args) {
		FrameWork f = new FrameWork();// declare class
		f.setTitle("Quiz 3");// set frame
		f.setSize(500, 500);
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
