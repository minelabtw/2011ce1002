package q3.s100502008;

import java.awt.*;
import javax.swing.*;

public class DrawPolygon extends JPanel {
	private int Vertice;
	private double angle;
	private int xCenter;
	private int yCenter;
	private int radius = 150;

	public DrawPolygon() {// constructor
		setNumVertex(4);
	}

	void setNumVertex(int a) {// set Vertex
		Vertice = a;
		angle = 2 * Math.PI / a;
	}

	protected void paintComponent(Graphics g) {// paint graphics
		xCenter = getWidth() / 2;
		yCenter = getHeight() / 2;
		double nowangle = 0;
		super.paintComponent(g);
		Polygon p = new Polygon();
		for (int i = 0; i < Vertice; i++) {// loop to add point
			p.addPoint((int) (xCenter + radius * Math.sin(nowangle)),
					(int) (yCenter - radius * Math.cos(nowangle)));
			nowangle += angle;
		}
		g.drawPolygon(p);// draw polygon
	}
}
