package q3.s100502516;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class FrameWork extends JFrame{
	private JButton jbtPlus = new JButton("+");
	private JButton jbtMinus = new JButton("-");
	private DrawPolygon polygon = new DrawPolygon();
	
	public FrameWork()
	{	
		jbtPlus.addActionListener(new ButtonListener());
		jbtMinus.addActionListener(new ButtonListener());
		
		JPanel p = new JPanel(new GridLayout(1, 2));
		p.add(jbtPlus);
		p.add(jbtMinus);
		
		setLayout(new BorderLayout());
		add(polygon, BorderLayout.CENTER);		
		add(p, BorderLayout.SOUTH);
	}
	
	class ButtonListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(e.getSource() == jbtPlus)			
				polygon.setNumVertex(polygon.getNumVerticles()+1);//plus plot
			else if(e.getSource() == jbtMinus)
				polygon.setNumVertex(polygon.getNumVerticles()-1);
			
			polygon.repaint();
		}
	}
}
