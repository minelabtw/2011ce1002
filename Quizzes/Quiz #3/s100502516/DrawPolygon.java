package q3.s100502516;

import javax.swing.*;
import java.awt.*;

public class DrawPolygon extends JPanel{
	private int xCenter;
	private int yCenter;
	private int numVerticles;
	private double angle;//rotate angle
	private int radius = 150;
	
	public DrawPolygon()
	{
		numVerticles = 4;
		angle = (double)(2 * Math.PI / numVerticles);		
	}
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		xCenter = getWidth() / 2;
		yCenter = getHeight() / 2;		
		
		g.fillOval(xCenter, yCenter, 10, 10);
		
		Polygon polygon = new Polygon();
		polygon.addPoint(xCenter, yCenter - radius);
		
		int xTemp = xCenter;
		int yTemp = yCenter - radius;	
		
		for(int i = 0; i < numVerticles - 1; i++)
		{
			xTemp -= xCenter;
			yTemp -= yCenter;
			
			int temp = xTemp;
			xTemp = (int)(temp * Math.cos(angle) - yTemp * Math.sin(angle));//rotate
			yTemp = (int)(temp * Math.sin(angle) + yTemp * Math.cos(angle));
			
			xTemp += xCenter;
			yTemp += yCenter;
			
			polygon.addPoint(xTemp, yTemp);
		}	
		
		g.drawPolygon(polygon);		
	}
	public void setNumVertex(int a)
	{		
		numVerticles = a;
		
		if(a <= 3)
			numVerticles = 3;
		
		angle = (double)(2 * Math.PI / numVerticles);
	}	
	public int getNumVerticles()
	{
		return numVerticles;
	}
}
