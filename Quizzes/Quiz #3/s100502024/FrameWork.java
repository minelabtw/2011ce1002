package q3.s100502024;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class FrameWork extends JFrame implements ActionListener
{
	private JButton Add = new JButton("+");
	private JButton Minus = new JButton("-");
	private JPanel panel = new JPanel();
	DrawPolygon draw = new DrawPolygon();
	public FrameWork() 
	{
		setLayout(new BorderLayout());
		panel.setLayout(new GridLayout());
		panel.add(Add);
		panel.add(Minus);
		add(draw,BorderLayout.CENTER);
		add(panel,BorderLayout.SOUTH);
		
		Add.addActionListener(this);
		Minus.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == Add)
		{
			draw.vertice++;
			draw.setNumVertex(draw.vertice);
			repaint();
		}
		if(e.getSource() == Minus)
		{
			draw.vertice--;
			draw.setNumVertex(draw.vertice);
			repaint();
		}
	}
	
}
