package q3.s100502024;
import javax.swing.*;
import java.awt.*;
public class DrawPolygon extends JPanel
{
	private double angle;
	private int xCenter;
	private int yCenter;
	private int xTemp,yTemp,temp;
	protected int vertice;
	public DrawPolygon()
	{
		setNumVertex(4);
		xCenter = getWidth()/2;
		yCenter = getHeight()/2;
	}
	public void setNumVertex(int a)
	{
		vertice = a;
		angle = ((2*Math.PI)/a);
	}
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		Polygon p = new Polygon();
		xCenter = getWidth()/2;
		yCenter = getHeight()/2;
		int radius = 100;
		p.addPoint(xCenter, yCenter-radius);
		xTemp = xCenter;
		yTemp = yCenter-radius;
		for(int i=1;i<vertice;i++)
		{
			
			xTemp -= xCenter;
			yTemp -= yCenter;
			
			temp = xTemp;
			xTemp = (int)(temp*Math.cos(angle) - yTemp*Math.sin(angle));
			yTemp = (int)(temp*Math.sin(angle) + yTemp*Math.cos(angle));
			xTemp += xCenter;
			yTemp += yCenter;
			p.addPoint(xTemp, yTemp);
			
		}
		g.drawPolygon(p);
		
	}
}
