package q3.s995002203;
import java.awt.*;

import javax.swing.*;

public class DrawPolygon extends JLabel {
	private int xCenter;
	private int yCenter;
	private int vertice=0;
	private int radius=50;
	private double angle;
	
	public DrawPolygon(){
		vertice=4;
		angle=2*Math.PI/vertice;
		
		///xCenter=140;
		//yCenter=100;
	}
	
	public void setNumVertex(int a){
		vertice=a;
		angle=2*Math.PI/vertice;
	}
	
	public int getVertice(){
		return vertice;
	}
	
	protected void paintComponent(Graphics g){
		xCenter=(getWidth()/2);
		yCenter=(getHeight()/2);
		super.paintComponent(g);
		g.setColor(Color.BLACK);
		Polygon p=new Polygon();
		int xtemp, ytemp, temp;
		
		p.addPoint(xCenter, yCenter-radius);//先加第一點
		xtemp=xCenter;
		ytemp=yCenter-radius;
		
		for(int i=1;i<vertice;i++){
			xtemp-=xCenter;
			ytemp-=yCenter;
			
			temp=xtemp;
			
			xtemp=(int)(temp*Math.cos(angle)-ytemp*Math.sin(angle));
			ytemp=(int)(temp*Math.sin(angle)+ytemp*Math.cos(angle));
			
			xtemp+=xCenter;
			ytemp+=yCenter;
			
			p.addPoint(xtemp, ytemp);
		}
		//p.addPoint(50, 50);
		//p.addPoint(getWidth()/2, getHeight()/2);
		g.drawPolygon(p);	
	}
}
