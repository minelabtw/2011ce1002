package q3.s995002203;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class FrameWork extends JFrame implements ActionListener{
	private DrawPolygon d=new DrawPolygon();
	private JPanel buttonPanel=new JPanel();
	private JButton plus=new JButton("+");
	private JButton minus=new JButton("-");
	public FrameWork(){
		setLayout( new BorderLayout());
		buttonPanel.setLayout(new GridLayout(1,2));
		add(d, BorderLayout.CENTER);
		plus.addActionListener(this);
		minus.addActionListener(this);
		buttonPanel.add(plus);
		buttonPanel.add(minus);
		
		add(buttonPanel,BorderLayout.SOUTH);
	}
	public void actionPerformed(ActionEvent e){
		// if click start button, start the timer
		if(e.getSource() == plus){
			d.setNumVertex(d.getVertice()+1);
			d.repaint();
		}

		// if click stop button, stop the timer
		else{
			d.setNumVertex(d.getVertice()-1);
			d.repaint();
		}
	}
}
