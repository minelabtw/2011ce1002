package q3.s100502012;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener {
	private int line;
	DrawPolygon past=new DrawPolygon();
	
	JPanel p=new JPanel();
    JButton b1=new JButton("-");    
    JButton b2=new JButton("+");
    
    public FrameWork(){
        p.setLayout(new GridLayout(1,1));
        p.add(b1);
        b1.addActionListener(this);
        p.add(b2);
        b2.addActionListener(this);
        
        add(past,BorderLayout.CENTER);
        add(p,BorderLayout.SOUTH);
    }
    
    public void actionPerformed(ActionEvent e){
    	if (e.getSource()==b1){
    		int box=line;
    		line--;
     	    if (line<3){
     		    line=box;
     	    }
     	    
    		past.setNumVertex(line);
    		repaint();
    	}
    	
    	if (e.getSource()==b2){
    		line++;
    		past.setNumVertex(line);
    		repaint();
    	}
    }
}
