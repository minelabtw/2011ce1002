package q3.s100502012;

import java.awt.Graphics;
import java.awt.Polygon;
import javax.swing.JPanel;

public class DrawPolygon extends JPanel {
	private int line=4;
	private int radius=100;
	
	
	
    public DrawPolygon(){
    	
    }
    
    public void setNumVertex(int l){
        line=l;
    }
    
    
    
    protected void paintComponent(Graphics g){
    	int Xcore=getWidth()/2;
        int Ycore=getHeight()/2;
        int Xtip=250;
        int Ytip=250-radius;
    	super.paintComponents(g);
    	
    	Polygon picture=new Polygon();
    	
    	int xtemp=Xtip;
    	int ytemp=Ytip;
    	
    	for(int a=1 ; a<line ; a++){
    		int angle=(int)Math.PI/line;
    		xtemp-=Xcore;
    		ytemp-=Ycore;
    		int temp=xtemp;
            
    		xtemp=(int)(temp*Math.cos(angle)-ytemp*Math.sin(angle));
    		ytemp=(int)(temp*Math.sin(angle)+ytemp*Math.cos(angle));
    		
    		picture.addPoint(xtemp, ytemp);
    	}
    	
    	g.drawPolygon(picture);
    }
}
