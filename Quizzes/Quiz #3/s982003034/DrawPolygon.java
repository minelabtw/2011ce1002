package q3.s982003034;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class DrawPolygon extends JPanel {
	private int x; // x center
	private int y; // y center
	private int nvtx; // number of vertices
	private double angle; // angle
	private int rad; // radius
	
	public DrawPolygon (int nv, int r) {
		
		this.setSize(500, 500);
		this.x = this.getWidth()/2;
		this.y = this.getHeight()/2;
		nvtx = nv;
		angle = 2.0*Math.PI/nvtx;
		rad = r;
	}
	
	public void paintComponent (Graphics g) {
		super.paintComponent(g);
		
		int [] newx = new int [nvtx];
		int [] newy = new int [nvtx];
		
		for (int i = 0; i < nvtx; i++) {
			// x= r cos(t) +d
			// y= r sin(t) +d
			// the -90 deg (Math.PI/2) is necessary for the vertices to be drawn from top
			// because Java's degree is clockwise (as opposed to the normal counter-clockwise)
			newx[i] = (int) (rad * Math.cos((i+1)*angle-Math.PI/2)) + x;
			newy[i] = (int) (rad * Math.sin((i+1)*angle-Math.PI/2)) + y;
		}
		
		g.setColor(new Color ((int) (Math.random()*255), (int) (Math.random()*255), (int) (Math.random()*255)));
		g.fillPolygon(newx, newy, nvtx);
		
	}
	
	// function to update vertices count
	public void setVertices (int vx) {
		nvtx = vx;
		angle = 2.0*Math.PI/nvtx;
		this.repaint();
	}
	
}
