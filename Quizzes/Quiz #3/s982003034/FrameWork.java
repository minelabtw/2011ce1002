package q3.s982003034;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class FrameWork extends JFrame implements ActionListener {

	int vertices; // number of vertices
	DrawPolygon mypoly;
	JPanel button_panel;
	JButton plus;
	JButton minus;
	JLabel info;
	
	public FrameWork () {
		
		this.setSize(600, 600);
		this.vertices = 4;
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(new BorderLayout());
		
		mypoly = new DrawPolygon (vertices, 150);
				
		button_panel = new JPanel();
		button_panel.setLayout(new FlowLayout());
		
		plus = new JButton ("  +  ");
		plus.addActionListener(this);
		
		minus = new JButton ("  -  ");
		minus.addActionListener(this);
		
		info = new JLabel ("vertices= " + vertices);
		
		button_panel.add(minus);
		button_panel.add(plus);
		button_panel.add(info);
		
		this.add(button_panel, BorderLayout.SOUTH);
		this.add(mypoly, BorderLayout.CENTER);
		this.setVisible(true);
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == plus) {
			vertices ++;
		}
		
		if (arg0.getSource() == minus) {
			// this is to avoid the vertices to be smaller than 2
			// because if vertices is 0 or negative, an error will occur on DrawPolygon
			// (cannot initialize array with negative size)
			if (vertices >= 4) { 
				vertices --;
			}
			
		}
		
		info.setText("vertices= " + vertices);
		mypoly.setVertices(vertices);
		
	}
	
}
