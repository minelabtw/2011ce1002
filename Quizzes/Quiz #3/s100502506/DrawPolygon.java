package q3.s100502506;

import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class DrawPolygon extends JPanel		//JPanel
{
	private int radius=150;
	private int xCenter;
	private int yCenter;;
	private int vertices=4;
	private double angle=(2*Math.PI)/vertices;
	
	
	public void paintComponent(Graphics g) //paintComponent
	{
		super.paintComponent(g);
		xCenter=getWidth()/2;
		yCenter=getHeight()/2;
		int temp;
		int xpoints[] = new int[vertices];
		int ypoints[] =	new int[vertices];
		xpoints[0]=xCenter;					//set first point
		ypoints[0]=yCenter-radius;			//set second point
		int xTemp = xCenter;
		int yTemp = yCenter-radius;
		
		for(int i=1;i<vertices;i++)			//set points
		{
			
			xTemp=xTemp-xCenter;			//return to zero
			yTemp=yTemp-yCenter;			//return to zero
			
			temp=xTemp;
			xTemp=(int)(temp*Math.cos(angle)-yTemp*Math.sin(angle));	//Rotate
			yTemp=(int)(temp*Math.sin(angle)+yTemp*Math.cos(angle));
			xTemp=xTemp+xCenter;
			yTemp=yTemp+yCenter;
			
			
			
			xpoints[i]=xTemp;
			ypoints[i]=yTemp;
		}
		g.drawPolygon(xpoints,ypoints ,xpoints.length);
		
		
	}
	public void setNumVertex(int a)					//set vertex
	{
		vertices=a;
		angle=(2*Math.PI)/vertices;
	}
	public int getNumVertex()						//return vertex
	{
		return vertices;
	}
	
}
