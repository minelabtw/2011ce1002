package q3.s100502506;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class FrameWork extends JFrame
{

	private JButton plusButton;
	private JButton minusButton;
	private JPanel panel1;
	private DrawPolygon testDrawPolygon=new DrawPolygon();
	public FrameWork() //JFrame
	{
		
		plusButton=new JButton("+");
		minusButton=new JButton("-");
		panel1=new JPanel();
		panel1.setLayout(new GridLayout(1,2,5,5));
		panel1.add(plusButton);
		panel1.add(minusButton);
		
		
		add(testDrawPolygon,BorderLayout.CENTER);
		add(panel1,BorderLayout.SOUTH);
		plusButton.addActionListener(new ButtonAction());//add button listener
		minusButton.addActionListener(new ButtonAction());//add button listener
		
	}
	class ButtonAction implements ActionListener//button event
	{

		public void actionPerformed(ActionEvent e) 
		{
			if(e.getSource()==plusButton)//plus
			{
				testDrawPolygon.setNumVertex(testDrawPolygon.getNumVertex()+1);
				testDrawPolygon.repaint();
			}
			if(e.getSource()==minusButton)//minus
			{
				testDrawPolygon.setNumVertex(testDrawPolygon.getNumVertex()-1);
				testDrawPolygon.repaint();
			}
			
		}
		
	}
	
	
}
