package q3.s100502513;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.Polygon;

public class FrameWork extends JFrame implements ActionListener{
	private JButton addp = new JButton("+");
	private JButton minp = new JButton("-");
	private JPanel p1;
	private JPanel p2 = new JPanel();
	private DrawPolygon dp = new DrawPolygon();
	private int a = 4;

	public FrameWork() {
		p1 = dp;
		p2.setLayout(new GridLayout(1, 2));
		p2.add(addp);
		p2.add(minp);
		add(p1, BorderLayout.CENTER);
		add(p2, BorderLayout.SOUTH);
		addp.addActionListener(this);
		minp.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == addp) {
			a++;
			dp.setnumberVertex(a);
		} 
		else if (e.getSource() == minp) {
			a--;
			if (a <= 2)
				a++;
			else 
				dp.setnumberVertex(a);
		}
	}
}
