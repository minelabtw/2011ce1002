package q3.s100502513;

import java.awt.*;
import javax.swing.*;

public class DrawPolygon extends JPanel {
	private int xCenter, yCenter;
	private int number_of_vertics;
	private double angle, radius;
	private int xTemp=0, yTemp=0, temp;

	public DrawPolygon() {
		number_of_vertics = 4;
		angle = Math.PI / 2;
	}

	public void setnumberVertex(int a) {
		number_of_vertics = a;
		angle = Math.PI*2 / number_of_vertics;
		repaint();
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		xCenter = getWidth() / 2;
		yCenter = getHeight() / 2;
		radius = getWidth() * 0.4;
		Polygon pol = new Polygon();

		for (int i = 1; i < number_of_vertics+1; i++) {
			xTemp =  xCenter+(int) (radius * Math.cos(angle*i));
			yTemp = yCenter+(int) (radius * Math.sin(angle*i));
			pol.addPoint(xTemp, yTemp);
		}
		g.drawPolygon(pol);
	}

}
