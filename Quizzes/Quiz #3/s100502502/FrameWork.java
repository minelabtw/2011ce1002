package q3.s100502502;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class FrameWork extends JFrame implements ActionListener{
	JButton Add = new JButton("+");
	JButton Minus = new JButton("-");
	DrawPolygon panel = new DrawPolygon();
	int number = 1;
	FrameWork(){

		JPanel p1 = new JPanel(new GridLayout(1, 2, 1, 1));
		setLayout(new BorderLayout());
		
		
		p1.add(Add);
		p1.add(Minus);
		
		add(panel,BorderLayout.CENTER);
		add(p1, BorderLayout.SOUTH);
		Add.addActionListener(this);
		Minus.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == Add){
			panel.setNumVertex(number++);
			panel.repaint();
		}
		else if(e.getSource() == Minus){
			if(number < -1){
				
			}
			else{
				panel.setNumVertex(number--);
				panel.repaint();
			}
		}
		else{}
	}
}
