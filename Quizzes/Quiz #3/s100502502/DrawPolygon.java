package q3.s100502502;
import javax.swing.*;
import java.awt.*;
public class DrawPolygon extends JPanel{
	protected int xCenter, yCenter, vertices, radius;
	private double angle;
	DrawPolygon(){
		vertices = 4;
		angle = Math.PI/2;
		radius = 50;
		xCenter = 150;
		yCenter = 150;
	}
	public void setNumVertex(int a){
		vertices = vertices + a;
		angle = 2*Math.PI/vertices;
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		Polygon polygon = new Polygon();
		int xTemp, yTemp, temp;
		xTemp = xCenter;
		yTemp = yCenter - radius;
		polygon.addPoint(xTemp, yTemp);
		for(int i = 0; i < vertices-1; i++){
			xTemp -= xCenter;
			yTemp -= yCenter;
			temp = xTemp;
			xTemp = (int)(temp*Math.cos(angle) - yTemp*Math.sin(angle));
			yTemp = (int)(temp*Math.sin(angle) - yTemp*Math.cos(angle));
			xTemp += xCenter;
			yTemp += yCenter;
			polygon.addPoint(xTemp, yTemp);
		}
		g.drawPolygon(polygon);
	}
}
