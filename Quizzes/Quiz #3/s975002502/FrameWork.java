package q3.s975002502;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class FrameWork extends JFrame {
	private DrawPolygon polygonPanel = new DrawPolygon();
	private JButton plus = new JButton("Plus");
	private JButton minus = new JButton("Minus");
	
	/** Construct a default FrameWork */
	public FrameWork() {
		polygonPanel.setPreferredSize(new Dimension(150, 150));
		JPanel panel = new JPanel();
		
		panel.add(plus);
		panel.add(minus);
		
		BorderLayout border = new BorderLayout();
		add(polygonPanel, border.CENTER);
		add(panel, border.WEST);
		
		plus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				plusVertices();
			}
		});
		
		minus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				minusVertices();
			}
		});
	}
	
	// increase the number of vertices
	public void plusVertices() {
		int a = polygonPanel.getNumVertices() + 1;
		polygonPanel.setNumVertices(a);
	}
	
	// decrease the number of vertices
	public void minusVertices() {
		int a = polygonPanel.getNumVertices() - 1;
		polygonPanel.setNumVertices(a);
	}
}
