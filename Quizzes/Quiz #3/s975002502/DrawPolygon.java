package q3.s975002502;
import javax.swing.*;
import java.awt.*;

public class DrawPolygon extends JPanel {
	private int NumVertices = 4;
	private double angle = Math.PI/2;
	private int radius = 10;
		
	/** Construct a default GraphicPanel */
	public DrawPolygon() {
	}
	
	/** Construct a GraphicPanel with the specified angle */
	public DrawPolygon(int NumVertices) {
		this.NumVertices = NumVertices;
	}
	
	/** Return NumVertices */
	public int getNumVertices() {
		return NumVertices;
	}
	
	/** Set the NumVertices */
	public void setNumVertices(int a) {
		this.NumVertices = a;
		this.angle = (2 * Math.PI)/NumVertices;
		repaint();
	}
	
	/** Draw the Polygon */
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		int xCenter = getWidth()/2;
		int yCenter = getHeight()/2;
		int x[] = new int[NumVertices];
		int y[] = new int[NumVertices];
		
		Polygon polygon = new Polygon();
		
		// Set the first point
		x[0] = xCenter;
		y[0] = yCenter - radius;
		System.out.println("0 = " + x[0]);
		System.out.println("0 = " + y[0]);
		
		for (int i = 1; i < NumVertices; i++) {
			System.out.println("i = " + i);
			// xTemp and yTemp are the coordinates of the front point
			int xTemp = x[i-1];
			int yTemp = y[i-1];
			
			xTemp -= xCenter;
			yTemp -= yCenter;
			
			x[i] = (int)(xTemp*Math.cos(angle) - yTemp*Math.sin(angle)) + xCenter;
			y[i] = (int)(xTemp*Math.sin(angle) + yTemp*Math.cos(angle)) + xCenter;
			
			System.out.println(i+" = " + x[i]);
			System.out.println(i+" = " + y[i]);
		}
		
		g.drawPolygon(x, y, NumVertices);
	}
	
	public Dimension setPreferredSize(){
		return new Dimension(80, 80); 
	}
}
