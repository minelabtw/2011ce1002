package q3.s100502018;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener {
	JButton plus = new JButton("+");
	JButton minus = new JButton("-");
	JPanel graphic = new JPanel();
	JPanel buttons = new JPanel();
	JPanel total = new JPanel();
	DrawPolygon myDrawPolygon = new DrawPolygon();

	public FrameWork() {
		graphic.add(myDrawPolygon);
		buttons.setLayout(new GridLayout(1, 2));
		buttons.add(plus);
		buttons.add(minus);
		plus.addActionListener(this);
		minus.addActionListener(this);
		total.setLayout(new GridLayout(2,1));
		total.add(graphic);
		total.add(buttons);
		add(total);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == plus) {
			myDrawPolygon.setNumVertex(myDrawPolygon.getNumVertex() + 1);
			repaint();
		} else if (e.getSource() == minus) {
			myDrawPolygon.setNumVertex(myDrawPolygon.getNumVertex() - 1);
			repaint();
		}
	}
}
