package q3.s100502018;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class DrawPolygon extends JPanel {
	private int xCenter;
	private int yCenter;
	private int numvertex = 4;
	private double angle;
	private int radius = 50;

	public void setNumVertex(int a) {
		numvertex = a;
		angle = (2 * Math.PI) / a;
	}

	public int getNumVertex() {
		return numvertex;
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int[] xPolygon = new int[numvertex];
		int[] yPolygon = new int[numvertex];
		xCenter = getWidth() / 2;
		yCenter = getHeight() / 2;
		xPolygon[0] = xCenter;
		yPolygon[0] = yCenter - radius;
		for (int i = 1; i < numvertex; i++) {
			int xTemp = xPolygon[i - 1] - xCenter;
			int yTemp = yPolygon[i - 1] - yCenter;

			int temp = xTemp;
			xTemp = (int) (temp * Math.cos(angle) - temp * Math.sin(angle));
			yTemp = (int) (temp * Math.sin(angle) + temp * Math.cos(angle));
			xPolygon[i] = xPolygon[i - 1] + xCenter;
			yPolygon[i] = yPolygon[i - 1] + yCenter;
		}
		g.drawPolygon(xPolygon, yPolygon, numvertex);
	}
}
