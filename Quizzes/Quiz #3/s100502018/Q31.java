package q3.s100502018;

import javax.swing.*;

public class Q31 extends JFrame{
	public static void main(String[] args) {
		FrameWork myFrameWork = new FrameWork();
		myFrameWork.setTitle("Quiz #3");
		myFrameWork.setSize(500,500);
		myFrameWork.setLocationRelativeTo(null);
		myFrameWork.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		myFrameWork.setVisible(true);
	}
}
