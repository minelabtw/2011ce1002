package q3.s100502514;

import java.awt.*;

import javax.swing.*;

public class DrawPolygon extends JPanel {
	/** UID */
	private static final long serialVersionUID = 1L;
	
	/** Private Variables */
	private int numVertex; //Number of Vertices
	
	public DrawPolygon(){
		this(4); //4 vertices as default
	}
	public DrawPolygon(int numVertex){
		setNumVertex(numVertex);
		//Set the background into black
		setBackground(Color.black);
	}
	
	// Setter & Getter of numVertex
	public void setNumVertex(int vertices){
		if(vertices < 3) numVertex = 3; //Number of vertices of a polygon cannot be less than 3.
		else numVertex = vertices;
	}
	public int getNumVertex(){
		return numVertex;
	}
	
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		//Determine center coordinates
		int centerX = getWidth()/2, centerY = getHeight()/2;
		int radius = (int)(Math.min(centerX, centerY)*0.8);
		
		//Array of Points
		MyPoint[] points = new MyPoint[numVertex];
		
		//Calculate the positions of points
		for(int vi=0;vi<numVertex;vi++){
			double rad = ( ((double)(vi)) / numVertex )* (Math.PI*2);
			MyPoint P = new MyPoint();
			//Rotate the coordinate by 90 degrees by ( new_slope = -1/old_slope )
			P.x = (int)(centerX + Math.sin(rad)*radius);
			P.y = (int)(centerY - Math.cos(rad)*radius);
			
			//Record the point into the array
			points[vi] = P;
		}
		
		//Draw the fillings and lines with purple (dark filling and bright lines)
		drawPolygon(g, points); //Call a private method next to this method
		
	}
	
	//drawPolygon(Graphics, MyPoint[] P) equals g.drawPolygon(P.x, P.y)
	//Also implements purple skin here.
	//Syntax is a bit different from the requirements.
	private void drawPolygon(Graphics g, MyPoint[] P){
		int[] px = new int[P.length];
		int[] py = new int[P.length];
		for(int i=0;i<P.length;i++){
			px[i] = P[i].x;
			py[i] = P[i].y;
		}
		g.setColor(new Color(153,0,204));
		g.fillPolygon(px, py, P.length);//Filling
		g.setColor(new Color(204,153,255));
		g.drawPolygon(px, py, P.length);//Lines
	}
	
	
	//Private Inner Class MyPoint, to make the convenience on recording coordinates
	private class MyPoint{
		int x, y; //Public members for this file
		MyPoint(){
			this(0, 0);
		}
		MyPoint(int x, int y){
			this.x = x;
			this.y = y;
		}
	}
	
}
