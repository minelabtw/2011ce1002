package q3.s100502514;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class FrameWork extends JFrame {
	/** UID */
	private static final long serialVersionUID = 1L;
	
	//GUI objects
	DrawPolygon poly = new DrawPolygon();
	JPanel all_of_btns = new JPanel();
	
	String title_prefix = "Quiz 3 Polygon Test";
	
	public FrameWork(){
		setLocalTitleText();//setTitle(title_prefix);
		setSize(300, 250);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		add(poly, BorderLayout.CENTER);
		add(all_of_btns, BorderLayout.SOUTH);
		all_of_btns.setLayout(new GridLayout(1, 2));
		all_of_btns.add(new PlusMinusButton(1)); //+ button
		all_of_btns.add(new PlusMinusButton(-1)); //- button
	}
	
	void setLocalTitleText(){
		setTitle(title_prefix+" ("+poly.getNumVertex()+" vertices)");
	}
	
	//Inner class for +/- buttons
	class PlusMinusButton extends JButton{

		/** UID */
		private static final long serialVersionUID = 1L;
		
		int addition;//Determine how many vertices will be increased/decreased in a click
		
		public PlusMinusButton(int num){
			if(num>0){
				setText("+"+num);
			}else if(num<0){
				setText("-"+num);
			}else{
				setText("(0)");
			}
			addition = num;
			addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e) {
					//Add or Minus the sides of polygon
					poly.setNumVertex(poly.getNumVertex()+addition);
					//Update Title of Window
					setLocalTitleText();
					//Repaint all the window after every events.
					getThisFrameWork().repaint();
				}
			});
		}
		
	}
	
	//To make Inner class readable for this class FrameWork
	private FrameWork getThisFrameWork(){
		return this;
	}
}
