package q3.s100502508;

import java.awt.Graphics;
import javax.swing.JPanel;

public class DrawPolygon extends JPanel 
{
	private int xCenter;
	private int yCenter;
	private int vertices;
	private double angle;
	private int radius;
	
	public DrawPolygon()
	{
		vertices=4;
		angle=2*Math.PI/vertices;
		radius=50;
	}
	
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		xCenter=getWidth();
		yCenter=getHeight();
		
		int[] x=new int[vertices];
		int[] y=new int[vertices];
		
		x[0]=xCenter;
		y[0]=yCenter-radius;
		
		for(int a=1;a<4;a++)
		{
			int xTemp=x[a-1];
			int yTemp=y[a-1];
			xTemp-=xCenter;
			yTemp-=yCenter;
			int temp=xTemp;
			x[a]=translateX(xTemp,yTemp,temp);
			y[a]=translateY(xTemp,yTemp,temp);
		}
		g.drawPolygon(x, y, x.length);
		
	}
	
	public int translateX(int xTemp,int yTemp,int temp)
	{
		xTemp=(int)(temp*Math.cos(angle)-yTemp*Math.sin(angle));
		xTemp+=xCenter;
		return xTemp;
	}
	
	public int translateY(int xTemp,int yTemp,int temp)
	{
		yTemp=(int)(temp*Math.sin(angle)+yTemp*Math.cos(angle));
		yTemp+=yCenter;
		return yTemp;
	}
	
	public void setNumVertex(int a)
	{
		vertices=a;
		angle=2*Math.PI/a;
	}
	
	public int getvertices()
	{
		return vertices;
	}
}
