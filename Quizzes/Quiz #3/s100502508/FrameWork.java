package q3.s100502508;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class FrameWork extends JFrame implements ActionListener
{
	private JButton button1=new JButton("+");
	private JButton button2=new JButton("-");
	JPanel panel1=new JPanel(new GridLayout(1, 2, 0, 0));
	
	DrawPolygon drawPolygon=new DrawPolygon();
	
	public FrameWork()
	{
		panel1.add(button1);
		panel1.add(button2);
		
		add(drawPolygon,BorderLayout.CENTER);
		add(panel1,BorderLayout.SOUTH);
		
		button1.addActionListener(this);
		button2.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource()==button1)
		{
			int vertice=1;
			vertice=vertice+drawPolygon.getvertices();
			drawPolygon.setNumVertex(vertice);
			System.out.println(vertice);
		}
		else 
		{
			int vertice=1;
			vertice=drawPolygon.getvertices()-vertice;
			drawPolygon.setNumVertex(vertice);
			System.out.println(vertice);
		}
	}
}
