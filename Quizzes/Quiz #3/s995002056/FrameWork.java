package q3.s995002056;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener {
	JButton add = new JButton("+");
	JButton sub = new JButton("-");
	JPanel p = new JPanel();
	DrawPolygon dp = new DrawPolygon();

	public FrameWork() {
		add.addActionListener(this);
		sub.addActionListener(this);

		add(this.dp);

		p.setLayout(new GridLayout(1, 2));
		p.add(add);
		p.add(sub);
		add(p, BorderLayout.SOUTH);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == add) {
			dp.add();
			repaint();
		}
		if (e.getSource() == sub) {
			dp.sub();
			repaint();
		}

	}

}
