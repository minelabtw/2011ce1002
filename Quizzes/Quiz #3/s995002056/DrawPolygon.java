package q3.s995002056;

import javax.swing.*;
import java.awt.*;

public class DrawPolygon extends JPanel {
	int point = 3;
	int xc, yc, xt, yt, temp, x, y;
	double angle;

	public DrawPolygon() {

	}

	public void add() {
		point++;
	}

	public void sub() {
		point--;
	}

	public void setAngle(int p) {
		angle = (2 * Math.PI) / p;
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		xc = (int) (getWidth() / 2);
		yc = (int) (getHeight() / 2);
		x = xc;
		y = yc - 150;
		xt = xc;
		yt = yc - 150;

		setAngle(point);

		for (int i = 0; i < point; i++) {
			paint(point);

			g.drawLine(x, y, xt, yt);

			x = xt;
			y = yt;
		}
	}

	public void paint(int p) {
		xt -= xc;
		yt -= yc;

		temp = xt;
		xt = (int) ((temp * Math.cos(angle)) - yt * Math.sin(angle));
		yt = (int) ((temp * Math.sin(angle)) + yt * Math.cos(angle));

		xt += xc;
		yt += yc;

	}
}
