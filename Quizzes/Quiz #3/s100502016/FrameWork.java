package q3.s100502016;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener {
	private JButton btnAdd = new JButton("+");
	private JButton btnMinus = new JButton("-");
	private JPanel panel = new JPanel();
	DrawPolygon drawPolygon  = new DrawPolygon();

	FrameWork() {
		panel.add(btnAdd);
		panel.add(btnMinus);
		
		btnAdd.addActionListener(this);
		btnMinus.addActionListener(this);
		add(drawPolygon,BorderLayout.CENTER);
		add(panel, BorderLayout.SOUTH);
		
		
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnAdd) {
			drawPolygon.setVertice(drawPolygon.getVertice()+1);
		} else if (e.getSource() == btnMinus) {
			drawPolygon.setVertice(drawPolygon.getVertice()-1);
		}

	}

	public static void main(String arg[]) {

		FrameWork frame = new FrameWork();

		frame.setTitle("Draw");
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(500, 500);
		frame.setVisible(true);
	}
}
