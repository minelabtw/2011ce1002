package q3.s100502016;

import java.awt.Graphics;
import java.awt.image.SinglePixelPackedSampleModel;

import javax.swing.JLabel;

public class DrawPolygon extends JLabel {
	private int xCenter;
	private int yCenter;
	private int vertice;
	private int angle;
	private int radius;

	public DrawPolygon() {
		xCenter = getWidth() / 2 + 200;
		yCenter = getHeight() / 2 + 200;
		vertice = 4;
		angle = (int) (2 * Math.PI / vertice);
		radius = 100;
	}

	public void setVertice(int vertice) {
		this.vertice = vertice;
	}

	public int getVertice() {
		return vertice;
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int firstPointX = xCenter;
		int firstPointY = yCenter - radius;
		int[] PolygonX = new int[vertice];
		int[] PolygonY = new int[vertice];
		int tempX = 0;
		int tempY = 0;
		int temp;

		for (int i = 0; i < vertice; i++) {
			if (i == 0) {
				PolygonX[i] = xCenter;
				PolygonY[i] = yCenter - radius;
			}
			if (i > 0) {

				PolygonX[i] -= xCenter;
				PolygonX[i] -= yCenter;
				temp = tempX;
				PolygonX[i] = (int) (temp * Math.cos(angle) - tempY
						* Math.sin(angle));
				PolygonY[i] = (int) (temp * Math.sin(angle) + tempY
						* Math.cos(angle));
				PolygonX[i] += xCenter;
				PolygonX[i] += yCenter;
			}
			PolygonX[i] = (int) (xCenter + radius * Math.cos(angle * i));
			PolygonY[i] = (int) (yCenter + radius * Math.sin(angle * i));

		}
		g.drawPolygon(PolygonX, PolygonY, vertice);
		repaint();
	}
}
