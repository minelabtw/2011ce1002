package fp.s100502014;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.Scanner;
import javax.swing.*;

public class Frame extends JFrame {
	private MainPanel mainPanel = new MainPanel();
	private static Game game = new Game();
	private HighScore highScore = new HighScore();
	private Timer timer = new Timer(1000, new TimeListener());
	private final int width = 11;
	private boolean isEnter = false;
	private Music music = new Music();

	public Frame() {
		setLayout(null);
		
		mainPanel.setBounds(0, 0, 800, 540);
		game.setBounds(0, 0, 800, 540);
		highScore.setBounds(0, 0, 800, 540);
		
		add(mainPanel);
		add(game);
		add(highScore);
		
		mainPanel.setVisible(true);
		game.setVisible(false);
		highScore.setVisible(false);
		
		setResizable(false);
		
		addKeyListener(new KeyboardListener());
		
		music.runloop("sky.wav");
	}
	
	//clear everything in the game
	private void esc() {
		game.getGridPanel().setVisible(true);
		game.getPauseLabel().setVisible(false);
		game.getGameOverLabel().setVisible(false);
		
		timer.stop();
		
		for(int i=0;i<width;i++) {
			for(int j=0;j<width;j++) {
				game.getGrid(j,i).clearBackGround();
			}
		}
		
		game.setIsBomb(true);
		game.setIsStart(false);
		game.setIsPause(false);
		game.setIsOver(false);
		game.setIsUseSkill(false);
		game.getGrid(game.getCharX(),game.getCharY()).clearIcon();
		game.getGrid(game.getX(),game.getY()).clearIcon();
		game.getGrid(game.getSwordX(),game.getSwordY()).clearIcon();
		game.getGrid(game.getSkillX(),game.getSkillY()).clearBorder();
		game.clearCharX();
		game.clearCharY();
		for(int i=0;i<5;i++) {
			game.clearColorNum(i);
		}
		game.clearSwordNum();
		game.clearLife();
		game.setBonus(0);
		game.setScore(0);
		game.setProperty();
	}
	
	class TimeListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Color[] c = new Color[8];
			c[0] = Color.RED;
			c[1] = Color.BLUE;
			c[2] = Color.YELLOW;
			c[3] = Color.GREEN;
			c[4] = Color.GRAY;
			c[5] = Color.GRAY;
			c[6] = Color.GRAY;
			c[7] = Color.GRAY;
			
			//clear background
			for(int i=0;i<width;i++) {
				for(int j=0;j<width;j++) {
					game.getGrid(i,j).clearBackGround();
				}
			}
			
			//clear icon
			if(game.getCharX()!=game.getX() || game.getCharY()!=game.getY()) {
				game.getGrid(game.getX(),game.getY()).clearIcon();
			}
			
			//appear a bomb
			if(game.getIsBomb()) {
				game.setRan((int)(Math.random()*8));
				game.setX((int)(Math.random()*width));
				game.setY((int)(Math.random()*width));
				game.getGrid(game.getX(),game.getY()).setBackGround(c[game.getRan()]);
				if(game.getCharX()!=game.getX() || game.getCharY()!=game.getY() || (game.getSwordX()==game.getX() && game.getSwordY()==game.getY())) {
					game.getGrid(game.getX(),game.getY()).setBnbIcon(game.getRan()%4);
				}
				game.setIsBomb(false);
			}
			//it bombs
			else {
				if(game.getSwordX()!=game.getCharX() || game.getSwordY()!=game.getCharY()) {
					game.getGrid(game.getSwordX(),game.getSwordY()).setSwordIcon();
				}
				// +
				if(game.getRan()==0 || game.getRan()==2 || game.getRan()==4 || game.getRan()==6) {
					for(int i=0;i<width;i++) {
						for(int j=0;j<width;j++) {
							game.getGrid(game.getX(),i).setBackGround(c[game.getRan()]);
							game.getGrid(j,game.getY()).setBackGround(c[game.getRan()]);
						}
					}
				}
				// X
				if(game.getRan()==1 || game.getRan()==2 || game.getRan()==5 || game.getRan()==6) {
					for(int i=0;((game.getX()+i)<width)&&((game.getY()+i)<width);i++) {
						game.getGrid(game.getX()+i,game.getY()+i).setBackGround(c[game.getRan()]);
					}
					for(int i=0;((game.getX()+i)<width)&&((game.getY()-i)>=0);i++) {
						game.getGrid(game.getX()+i,game.getY()-i).setBackGround(c[game.getRan()]);
					}
					for(int i=0;((game.getX()-i)>=0)&&((game.getY()+i)<width);i++) {
						game.getGrid(game.getX()-i,game.getY()+i).setBackGround(c[game.getRan()]);
					}
					for(int i=0;((game.getX()-i)>=0)&&((game.getY()-i)>=0);i++) {
						game.getGrid(game.getX()-i,game.getY()-i).setBackGround(c[game.getRan()]);
					}
				}
				// square
				if(game.getRan()==3 || game.getRan()==7) {
					for(int i=0;((game.getX()+2*i)<width);i++) {
						for(int j=0;(j<=2*i)&&((game.getY()+j)<width);j++) {
							game.getGrid(game.getX()+2*i,game.getY()+j).setBackGround(c[game.getRan()]);
						}
						for(int j=0;(j<=2*i)&&((game.getY()-j)>=0);j++) {
							game.getGrid(game.getX()+2*i,game.getY()-j).setBackGround(c[game.getRan()]);
						}
					}
					for(int i=0;((game.getY()+2*i)<width);i++) {
						for(int j=0;(j<=2*i)&&((game.getX()+j)<width);j++) {
							game.getGrid(game.getX()+j,game.getY()+2*i).setBackGround(c[game.getRan()]);
						}
						for(int j=0;(j<=2*i)&&((game.getX()-j)>=0);j++) {
							game.getGrid(game.getX()-j,game.getY()+2*i).setBackGround(c[game.getRan()]);
						}
					}
					for(int i=0;((game.getX()-2*i)>=0);i++) {
						for(int j=0;(j<=2*i)&&((game.getY()+j)<width);j++) {
							game.getGrid(game.getX()-2*i,game.getY()+j).setBackGround(c[game.getRan()]);
						}
						for(int j=0;(j<=2*i)&&((game.getY()-j)>=0);j++) {
							game.getGrid(game.getX()-2*i,game.getY()-j).setBackGround(c[game.getRan()]);
						}
					}
					for(int i=0;((game.getY()-2*i)>=0);i++) {
						for(int j=0;(j<=2*i)&&((game.getX()+j)<width);j++) {
							game.getGrid(game.getX()+j,game.getY()-2*i).setBackGround(c[game.getRan()]);
						}
						for(int j=0;(j<=2*i)&&((game.getX()-j)>=0);j++) {
							game.getGrid(game.getX()-j,game.getY()-2*i).setBackGround(c[game.getRan()]);
						}
					}
				}
				//test whether die
				if(game.getRan()==0 || game.getRan()==1 || game.getRan()==2 || game.getRan()==3) {
					//die
					if(game.getGrid(game.getCharX(),game.getCharY()).getIsSetBG()) {
						game.setLife(-1);
						game.setProperty();
					}
					//alive
					else {
						game.setColorNum(game.getRan()<4?game.getRan():4,1);
						game.setProperty();
					}
				}
				else {
					//die
					if(!game.getGrid(game.getCharX(),game.getCharY()).getIsSetBG()) {
						game.setLife(-1);
						game.setProperty();
					}
					//alive
					else {
						game.setColorNum(game.getRan()<4?game.getRan():4,1);
						game.setProperty();
					}
				}
				//bonus
				if(game.getScore()/1000 != game.getBonus()) {
					game.setLife(game.getScore()/1000 - game.getBonus());
					game.setProperty();
					game.setBonus(game.getScore()/1000);
				}
				//set game over
				if(game.getLife()==0) {
					game.setIsOver(true);
				}
				//game over
				if(game.getIsOver()) {
					game.getGameOverLabel().setVisible(true);
					timer.stop();
					game.setIsPause(true);
					game.setProperty();
					
					//open high score file
					File file = new File("highScore.txt");
					String[] names = new String[5];
					int[] reds = new int[5];
					int[] blues = new int[5];
					int[] yellows = new int[5];
					int[] greens = new int[5];
					int[] grays = new int[5];
					int[] swords = new int[5];
					int[] scores = new int[5];
					
					try {
						
						//store into array
						Scanner input = new Scanner(file);
						for(int i=0;input.hasNext();i++) {
							names[i] = input.next();
							reds[i] = input.nextInt();
							blues[i] = input.nextInt();
							yellows[i] = input.nextInt();
							greens[i] = input.nextInt();
							grays[i] = input.nextInt();
							swords[i] = input.nextInt();
							scores[i] = input.nextInt();
						}
						
						//break record
						if(game.getScore()>scores[4]) {
							names[4] =	JOptionPane.showInputDialog(null,
										"Please enter your name :",
										"Congratulations!!!",
										JOptionPane.QUESTION_MESSAGE);
							reds[4] = game.getColorNum(0);
							blues[4] = game.getColorNum(1);
							yellows[4] = game.getColorNum(2);
							greens[4] = game.getColorNum(3);
							grays[4] = game.getColorNum(4);
							swords[4] = game.getSwordNum();
							scores[4] = game.getScore();
							
							//bubble sort
							int flag = 1;
							while(flag!=0) {
								
								flag=0; //initial the flag
								for(int i=0;i<4;i++) {
									
									//if left is smaller than right, they swap
									if(scores[i]<scores[i+1]) {
										game.swap(i,i+1,names);
										game.swap(i,i+1,reds);
										game.swap(i,i+1,blues);
										game.swap(i,i+1,yellows);
										game.swap(i,i+1,greens);
										game.swap(i,i+1,grays);
										game.swap(i,i+1,swords);
										game.swap(i,i+1,scores);
										flag=-1; //has swapped
									}
								}
							}
							
							//write into file
							PrintWriter output = new PrintWriter(file);
							for(int i=0;i<5;i++) {
								output.println(names[i]+" "+reds[i]+" "+blues[i]+" "+yellows[i]+" "+greens[i]+" "+grays[i]+" "+swords[i]+" "+scores[i]+" ");
							}
							output.close();
							
						}
						input.close();
					}
					catch(IOException ex) {
						System.err.println("Error");
						System.exit(0);
					}
					
				}
				//doesn't game over
				else {
					game.setProperty();
					game.setIsBomb(true);
				}
			}
		}
	}
	
	class KeyboardListener implements KeyListener {
		public void keyPressed(KeyEvent e) { 
			
			//main panel's
			if(e.getKeyCode()==KeyEvent.VK_UP && isEnter==false) {
				mainPanel.setSwitches();
			}
			if(e.getKeyCode()==KeyEvent.VK_DOWN && isEnter==false) {
				mainPanel.setSwitches();
			}
			
			if(e.getKeyCode()==KeyEvent.VK_ENTER && isEnter==false) {
				if(mainPanel.getSwitches()==1) {
					mainPanel.setVisible(false);
					game.setVisible(true);
					highScore.setVisible(false);
				}
				if(mainPanel.getSwitches()==-1) {
					mainPanel.setVisible(false);
					game.setVisible(false);
					highScore.setVisible(true);
				}
				isEnter = true;
			}
			
			//high score's or game's
			if(isEnter) {
				
				if(e.getKeyCode()==KeyEvent.VK_ESCAPE) {
					esc();
					isEnter = false;
					mainPanel.setVisible(true);
					game.setVisible(false);
					highScore.setVisible(false);
				}
				
				//game start
				if(e.getKeyCode()==KeyEvent.VK_S && game.getIsStart()==false) {
					do {
						game.setSwordX((int)(Math.random()*width));
						game.setSwordY((int)(Math.random()*width));
					}while(game.getSwordX()==width/2 && game.getSwordY()==width/2);
					game.getGrid(game.getSwordX(), game.getSwordY()).setSwordIcon();
					game.getGrid(game.getCharX(), game.getCharY()).setCharIcon();
					game.setIsStart(true);
					timer.start();
				}
				
				//game has started
				if(game.getIsStart()) {
					
					//hasn't pause
					if(!game.getIsPause()) {
						
						//move up
						if(e.getKeyCode()==KeyEvent.VK_UP && game.getCharY()>0) {
							game.getGrid(game.getCharX(), game.getCharY()).clearIcon();
							game.setOneCharY(-1);
							if(game.getSwordX()==game.getCharX() && game.getSwordY()==game.getCharY()) {
								game.setSwordNum();
								game.setProperty();
								game.setSwordX((int)(Math.random()*width));
								game.setSwordY((int)(Math.random()*width));
								game.getGrid(game.getSwordX(), game.getSwordY()).setSwordIcon();
							}
							if(!game.getIsBomb()) {
								game.getGrid(game.getX(), game.getY()).setBnbIcon(game.getRan()%4);
							}
							game.getGrid(game.getCharX(), game.getCharY()).setCharIcon();
							//bonus
							if(game.getScore()/1000 != game.getBonus()) {
								game.setLife(game.getScore()/1000 - game.getBonus());
								game.setProperty();
								game.setBonus(game.getScore()/1000);
							}
						}
						
						//move down
						if(e.getKeyCode()==KeyEvent.VK_DOWN && game.getCharY()<(width-1)) {
							game.getGrid(game.getCharX(), game.getCharY()).clearIcon();
							game.setOneCharY(1);
							if(game.getSwordX()==game.getCharX() && game.getSwordY()==game.getCharY()) {
								game.setSwordNum();
								game.setProperty();
								game.setSwordX((int)(Math.random()*width));
								game.setSwordY((int)(Math.random()*width));
								game.getGrid(game.getSwordX(), game.getSwordY()).setSwordIcon();
							}
							if(!game.getIsBomb()) {
								game.getGrid(game.getX(), game.getY()).setBnbIcon(game.getRan()%4);
							}
							game.getGrid(game.getCharX(), game.getCharY()).setCharIcon();
							//bonus
							if(game.getScore()/1000 != game.getBonus()) {
								game.setLife(game.getScore()/1000 - game.getBonus());
								game.setProperty();
								game.setBonus(game.getScore()/1000);
							}
						}
						
						//move left
						if(e.getKeyCode()==KeyEvent.VK_LEFT && game.getCharX()>0) {
							game.getGrid(game.getCharX(), game.getCharY()).clearIcon();
							game.setOneCharX(-1);
							if(game.getSwordX()==game.getCharX() && game.getSwordY()==game.getCharY()) {
								game.setSwordNum();
								game.setProperty();
								game.setSwordX((int)(Math.random()*width));
								game.setSwordY((int)(Math.random()*width));
								game.getGrid(game.getSwordX(), game.getSwordY()).setSwordIcon();
							}
							if(!game.getIsBomb()) {
								game.getGrid(game.getX(), game.getY()).setBnbIcon(game.getRan()%4);
							}
							game.getGrid(game.getCharX(), game.getCharY()).setCharIcon();
							//bonus
							if(game.getScore()/1000 != game.getBonus()) {
								game.setLife(game.getScore()/1000 - game.getBonus());
								game.setProperty();
								game.setBonus(game.getScore()/1000);
							}
						}
						
						//move right
						if(e.getKeyCode()==KeyEvent.VK_RIGHT && game.getCharX()<(width-1)) {
							//bonus
							if(game.getScore()/1000 != game.getBonus()) {
								game.setLife(game.getScore()/1000 - game.getBonus());
								game.setProperty();
								game.setBonus(game.getScore()/1000);
							}
							game.getGrid(game.getCharX(), game.getCharY()).clearIcon();
							game.setOneCharX(1);
							if(game.getSwordX()==game.getCharX() && game.getSwordY()==game.getCharY()) {
								game.setSwordNum();
								game.setProperty();
								game.setSwordX((int)(Math.random()*width));
								game.setSwordY((int)(Math.random()*width));
								game.getGrid(game.getSwordX(), game.getSwordY()).setSwordIcon();
							}
							if(!game.getIsBomb()) {
								game.getGrid(game.getX(), game.getY()).setBnbIcon(game.getRan()%4);
							}
							game.getGrid(game.getCharX(), game.getCharY()).setCharIcon();
							//bonus
							if(game.getScore()/1000 != game.getBonus()) {
								game.setLife(game.getScore()/1000 - game.getBonus());
								game.setProperty();
								game.setBonus(game.getScore()/1000);
							}
						}
						
						//use skill
						if(e.getKeyCode()==KeyEvent.VK_X) {
							game.setIsUseSkill(true);
							game.getGrid(game.getSkillX(), game.getSkillY()).clearBorder();
							game.setSkillX(game.getCharX());
							game.setSkillY(game.getCharY());
							game.getGrid(game.getSkillX(), game.getSkillY()).setBorder();
						}
						
						//move to the place you use skill
						if(e.getKeyCode()==KeyEvent.VK_Z && game.getIsUseSkill()==true) {
							game.getGrid(game.getCharX(), game.getCharY()).clearIcon();
							if(!game.getIsBomb()) {
								game.getGrid(game.getX(), game.getY()).setBnbIcon(game.getRan()%4);
							}
							game.setCharX(game.getSkillX());
							game.setCharY(game.getSkillY());
							game.getGrid(game.getCharX(), game.getCharY()).setCharIcon();
						}
						
						//pause
						if(e.getKeyCode()==KeyEvent.VK_P && game.getIsOver()==false) {
							game.getPauseLabel().setVisible(true);
							game.getGridPanel().setVisible(false);
							timer.stop();
							game.setIsPause(true);
						}
					}
					//has paused
					else {
						
						//resume
						if(e.getKeyCode()==KeyEvent.VK_R && game.getIsOver()==false) {
							game.getPauseLabel().setVisible(false);
							game.getGridPanel().setVisible(true);
							timer.start();
							game.setIsPause(false);
						}
					}
				}
			}
		}
		
		public void keyReleased(KeyEvent e) {	
		}
		
		public void keyTyped(KeyEvent e) {
		}
	}
	
	//music
	class Music extends JApplet {

		AudioClip clip;

	    public Music() {

	    }
	    public void runloop(String s){
			clip = Applet.newAudioClip(getClass().getResource(s));
			clip.loop();
		}
	}
}