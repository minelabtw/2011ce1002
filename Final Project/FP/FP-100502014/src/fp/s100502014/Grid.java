package fp.s100502014;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
public class Grid extends JPanel {
	private JLabel label = new JLabel();
	private boolean isSetBG = false, isUseSkill=false;

	public Grid() {
		setLayout(null);
		label.setBounds(3, 0, 60, 45);
		add(label);
		setBackground(Color.WHITE);
		setBorder(new LineBorder(Color.BLACK,3));
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(new Color(200,100,200));
		if(isUseSkill) {
			g.fillOval(17, 17, 10, 10);
		}
	}
	
	public void setBackGround(Color c) {
		setBackground(c);
		isSetBG = true;
	}
	
	public void clearBackGround() {
		setBackground(Color.WHITE);
		isSetBG = false;
	}
	
	public void setCharIcon() {
		label.setIcon(new ImageIcon("char.png"));
	}
	
	public void setBnbIcon(int x) {
		if(x==0) {
			label.setIcon(new ImageIcon("plus.png"));
		}
		else if(x==1) {
			label.setIcon(new ImageIcon("cross.png"));
		}
		else if(x==2) {
			label.setIcon(new ImageIcon("star.png"));
		}
		else if(x==3) {
			label.setIcon(new ImageIcon("square.png"));
		}
	}
	
	public void setSwordIcon() {
		label.setIcon(new ImageIcon("sword.png"));
	}
	
	public void clearIcon() {
		label.setIcon(null);
	}
	
	public void setBorder() {
		isUseSkill = true;
		setBorder(new LineBorder(new Color(200,100,200),3));
	}
	
	public void clearBorder() {
		isUseSkill = false;
		setBorder(new LineBorder(Color.BLACK,3));
	}
	
	public boolean getIsSetBG() {
		return isSetBG;
	}
}
