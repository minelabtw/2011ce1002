package fp.s100502014;
import java.awt.*;
import javax.swing.*;
public class ScorePanel extends JPanel {
	private int[] colorNum = {0,0,0,0,0};
	private int swordNum = 0;
	private int score = 0;
	private int life = 3;
	
	public ScorePanel() {
		setLayout(null);
		setOpaque(false);
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.PINK);
		g.setFont(new Font("SansSerif", Font.BOLD, 30));
		g.drawString("Scores : "+score, 20, 50);
		
		g.setColor(Color.RED);
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("RED(*10) : "+colorNum[0], 64, 100);
		
		g.setColor(Color.BLUE);
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("BLUE(*10) : "+colorNum[1], 52, 130);
		
		g.setColor(Color.YELLOW);
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("YELLOW(*20) : "+colorNum[2], 20, 160);
		
		g.setColor(Color.GREEN);
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("GREEN(*40) : "+colorNum[3], 35, 190);
		
		g.setColor(Color.GRAY);
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("GRAY(*20) : "+colorNum[4], 49, 220);
		
		g.setColor(new Color(200,100,200));
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("X : Mark", 50, 330);
		
		g.setColor(new Color(200,100,200));
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("Z : Move", 150, 330);
		
		g.setColor(Color.RED);
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("(Press S) Start", 100, 360);
		
		g.setColor(Color.RED);
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("(Press P) Pause", 100, 400);
		
		g.setColor(Color.RED);
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("(Press R) Resume", 100, 440);
		
		g.setColor(Color.RED);
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("(Press ESC) Exit", 75, 480);
		
		JLabel label = new JLabel();
		label.setIcon(new ImageIcon("sword.png"));
		label.setBounds(50,230,40,40);
		add(label);
		
		g.setColor(Color.BLACK);
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("(*50) : "+swordNum, 105, 250);
		
		JLabel label2 = new JLabel();
		label2.setIcon(new ImageIcon("char.png"));
		label2.setBounds(65,260,40,40);
		add(label2);
		
		g.setFont(new Font("SansSerif", Font.BOLD, 40));
		g.drawString("�� "+life, 120, 295);
	}
	
	public void setColorNum(int[] x) {
		colorNum = x;
	}
	
	public void setSwordNum(int x) {
		swordNum = x;
	}
	
	public void setScore(int x) {
		score = x;
	}
	
	public void setLife(int x) {
		life = x;
	}
	
	public void setRepaint() {
		repaint();
	}
	
	public int getScore() {
		return score;
	}
}