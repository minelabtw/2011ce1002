package fp.s100502014;
import java.awt.*;
import java.util.Scanner;
import java.io.*;
import javax.swing.*;
public class HighScore extends JPanel {
	public HighScore() {
		setLayout(null);
	}
	
	public void paintComponent(Graphics g) {
        super.paintComponent(g);
        ImageIcon icon = new ImageIcon("highScore.jpg");
        int x = 0;
        int y = 0;
        while(true) {
            g.drawImage(icon.getImage(), x, y, this);  
            if (x > getSize().width && y > getSize().height)
                break;
            if (x > getSize().width) {
                x = 0;  
                y += icon.getIconHeight();  
            }
            else  
                x += icon.getIconWidth();  
        }
        File file = new File("highScore.txt");
        String[] names = new String[5];
		int[] reds = new int[5];
		int[] blues = new int[5];
		int[] yellows = new int[5];
		int[] greens = new int[5];
		int[] grays = new int[5];
		int[] swords = new int[5];
		int[] scores = new int[5];
        try {
			Scanner input = new Scanner(file);
			for(int i=0;input.hasNext();i++) {
				names[i] = input.next();
				reds[i] = input.nextInt();
				blues[i] = input.nextInt();
				yellows[i] = input.nextInt();
				greens[i] = input.nextInt();
				grays[i] = input.nextInt();
				swords[i] = input.nextInt();
				scores[i] = input.nextInt();
			}
			input.close();
		}
		catch(IOException ex) {
			System.err.println("Error");
			System.exit(0);
		}
        g.setColor(new Color(200,100,200));
        g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("HIGH SCORE", 350, 100);
		
		g.setColor(Color.BLACK);
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("Rank", 160, 150);
		
		g.setColor(Color.BLACK);
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("1", 250, 150);
		
		g.setColor(Color.BLACK);
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("2", 335, 150);
		
		g.setColor(Color.BLACK);
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("3", 420, 150);
		
		g.setColor(Color.BLACK);
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("4", 505, 150);
		
		g.setColor(Color.BLACK);
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("5", 590, 150);
		
		g.setColor(Color.BLACK);
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("Name", 160, 180);
		
		g.setColor(Color.RED);
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("R(*10)", 160, 210);
		
		g.setColor(Color.BLUE);
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("B(*10)", 160, 240);
		
		g.setColor(Color.YELLOW);
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("Y(*20)", 160, 270);
		
		g.setColor(Color.GREEN);
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("G(*40)", 160, 300);
		
		g.setColor(Color.GRAY);
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("G(*20)", 160, 330);
		
		JLabel label = new JLabel();
		label.setIcon(new ImageIcon("sword.png"));
		label.setBounds(140,340,40,40);
		add(label);
		
		g.setColor(Color.BLACK);
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("(*50)", 178, 360);
		
		g.setColor(Color.BLACK);
		g.drawLine(150, 385, 650, 385);
		
		g.setColor(Color.BLACK);
		g.setFont(new Font("SansSerif", Font.BOLD, 20));
		g.drawString("Score", 160, 415);
		
		for(int i=0;i<5;i++) {
			g.setColor(Color.BLACK);
			g.setFont(new Font("SansSerif", Font.PLAIN, 15));
			g.drawString(names[i]+"", 250+85*i, 180);
			
			g.setColor(Color.BLACK);
			g.setFont(new Font("SansSerif", Font.PLAIN, 15));
			g.drawString(reds[i]+"", 250+85*i, 210);
			
			g.setColor(Color.BLACK);
			g.setFont(new Font("SansSerif", Font.PLAIN, 15));
			g.drawString(blues[i]+"", 250+85*i, 240);
			
			g.setColor(Color.BLACK);
			g.setFont(new Font("SansSerif", Font.PLAIN, 15));
			g.drawString(yellows[i]+"", 250+85*i, 330);
			
			g.setColor(Color.BLACK);
			g.setFont(new Font("SansSerif", Font.PLAIN, 15));
			g.drawString(greens[i]+"", 250+85*i, 270);
			
			g.setColor(Color.BLACK);
			g.setFont(new Font("SansSerif", Font.PLAIN, 15));
			g.drawString(grays[i]+"", 250+85*i, 300);
			
			g.setColor(Color.BLACK);
			g.setFont(new Font("SansSerif", Font.PLAIN, 15));
			g.drawString(swords[i]+"", 250+85*i, 360);
			
			g.setColor(Color.BLACK);
			g.setFont(new Font("SansSerif", Font.PLAIN, 15));
			g.drawString(scores[i]+"", 250+85*i, 415);
		}
    }
}
