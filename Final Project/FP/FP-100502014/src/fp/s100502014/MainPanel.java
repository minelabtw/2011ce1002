package fp.s100502014;
import java.awt.*;
import javax.swing.*;
public class MainPanel extends JPanel {
	private int switches;
	
	public MainPanel() {
		clearSwitches();
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		ImageIcon icon;
		int x;
        int y;
		switch(switches) {
			case 1:
				icon = new ImageIcon("enterGame.jpg");
		        x = 0;
		        y = 0;
		        while(true) {
		            g.drawImage(icon.getImage(), x, y, this);  
		            if (x > getSize().width && y > getSize().height)
		                break;
		            if (x > getSize().width) {
		                x = 0;  
		                y += icon.getIconHeight();  
		            }
		            else  
		                x += icon.getIconWidth();  
		        }
		        break;
			case -1:
				icon = new ImageIcon("enterHighScore.jpg");
		        x = 0;
		        y = 0;
		        while(true) {
		            g.drawImage(icon.getImage(), x, y, this);  
		            if (x > getSize().width && y > getSize().height)
		                break;
		            if (x > getSize().width) {
		                x = 0;  
		                y += icon.getIconHeight();  
		            }
		            else  
		                x += icon.getIconWidth();  
		        }
		        break;
			default:
				System.err.println("Error!!!");
				System.exit(0);
				break;
		}
	}
	
	private void clearSwitches() {
		switches = 1;
		repaint();
	}
	
	public void setSwitches() {
		switches *= (-1);
		repaint();
	}
	
	public int getSwitches() {
		return switches;
	}
}
