package fp.s100502014;
import java.awt.*;
import javax.swing.*;
public class Game extends JPanel {
	public final int width = 11;
	private JPanel gridPanel = new JPanel();
	private JLabel pauseLabel = new JLabel(new ImageIcon("pause.png"));
	private JLabel gameOverLabel = new JLabel(new ImageIcon("gameOver.png"));
	private ScorePanel scorePanel = new ScorePanel();
	private Grid[][] grid = new Grid[width][width];
	private boolean isBomb = true;
	private boolean isStart = false;
	private boolean isPause = false;
	private boolean isOver = false;
	private boolean isUseSkill = false;
	private int ran = 0;
	private int x = 0;
	private int y = 0;
	private int charx = width/2;
	private int chary = width/2;
	private int swordx = 0;
	private int swordy = 0;
	private int skillx=0;
	private int skilly=0;
	private int[] colorNum = new int[5];
	private int swordNum = 0;
	private int score = 0;
	private int life = 3;
	private int bonus=0;
	
	public Game() {
		setLayout(null);
		
		gridPanel.setLayout(new GridLayout(width,width));
		for(int i=0;i<width;i++) {
			for(int j=0;j<width;j++) {
				grid[i][j] = new Grid();
				gridPanel.add(grid[i][j]);
			}
		}
		
		pauseLabel.setBounds(8, 8, 500, 500);
		gameOverLabel.setBounds(8, 8, 500, 500);
		gridPanel.setBounds(8, 8, 500, 500);
		scorePanel.setBounds(510, 8, 280, 500);
		
		gridPanel.setOpaque(false);
		
		gridPanel.setVisible(true);
		scorePanel.setVisible(true);
		pauseLabel.setVisible(false);
		gameOverLabel.setVisible(false);
		
		
		add(pauseLabel);
		add(gameOverLabel);
		add(gridPanel);
		add(scorePanel);
	}
	
	//set and get
	public Grid getGrid(int x, int y) {
		return grid[y][x];
	}
	public void setX(int a) {
		x = a;
	}
	public int getX() {
		return x;
	}
	public void setY(int a) {
		y = a;
	}
	public int getY() {
		return y;
	}
	public void setOneCharX(int a) {
		charx += a;
	}
	public void setCharX(int a) {
		charx = a;
	}
	public int getCharX() {
		return charx;
	}
	public void clearCharX() {
		charx = 5;
	}
	public void setOneCharY(int a) {
		chary += a;
	}
	public void setCharY(int a) {
		chary = a;
	}
	public int getCharY() {
		return chary;
	}
	public void clearCharY() {
		chary = 5;
	}
	public void setSwordX(int a) {
		swordx = a;
	}
	public int getSwordX() {
		return swordx;
	}
	public void setSwordY(int a) {
		swordy = a;
	}
	public int getSwordY() {
		return swordy;
	}
	public void setSkillX(int a) {
		skillx = a;
	}
	public int getSkillX() {
		return skillx;
	}
	public void setSkillY(int a) {
		skilly = a;
	}
	public int getSkillY() {
		return skilly;
	}
	public void setIsBomb(boolean a) {
		isBomb = a;
	}
	public boolean getIsBomb() {
		return isBomb;
	}
	public void setRan(int a) {
		ran = a;
	}
	public int getRan() {
		return ran;
	}
	public void setLife(int a) {
		life += a;
	}
	public int getLife() {
		return life;
	}
	public void clearLife() {
		life = 3;
	}
	public void setColorNum(int a, int b) {
		colorNum[a] += b;
	}
	public void clearColorNum(int a) {
		colorNum[a] = 0;
	}
	public int getColorNum(int a) {
		return colorNum[a];
	}
	public void setSwordNum() {
		swordNum++;
	}
	public int getSwordNum() {
		return swordNum;
	}
	public void clearSwordNum() {
		swordNum = 0;
	}
	public void setScore(int a) {
		score = a;
	}
	public int getScore() {
		return score;
	}
	public void setBonus(int a) {
		bonus = a;
	}
	public int getBonus() {
		return bonus;
	}
	public void setIsOver(boolean a) {
		isOver = a;
	}
	public boolean getIsOver() {
		return isOver;
	}
	public void setIsStart(boolean a) {
		isStart = a;
	}
	public boolean getIsStart() {
		return isStart;
	}
	public void setIsUseSkill(boolean a) {
		isUseSkill = a;
	}
	public boolean getIsUseSkill() {
		return isUseSkill;
	}
	public void setIsPause(boolean a) {
		isPause = a;
	}
	public boolean getIsPause() {
		return isPause;
	}
	public JPanel getGridPanel() {
		return gridPanel;
	}
	public JLabel getPauseLabel() {
		return pauseLabel;
	}
	public JLabel getGameOverLabel() {
		return gameOverLabel;
	}
	public void setProperty() {
		score = colorNum[0]*10+colorNum[1]*10+colorNum[2]*20+colorNum[3]*40+colorNum[4]*20+swordNum*50;
		scorePanel.setColorNum(colorNum);
		scorePanel.setSwordNum(swordNum);
		scorePanel.setScore(score);
		scorePanel.setLife(life);
		scorePanel.setRepaint();
	}
	
	//swap
	public void swap(int a, int b, int[] array) {
		int temp = array[a];
		array[a] = array[b];
		array[b] = temp;
	}
	public void swap(int a, int b, String[] array) {
		String temp = array[a];
		array[a] = array[b];
		array[b] = temp;
	}
	
	//background
	public void paintComponent(Graphics g) {
        super.paintComponent(g);
        ImageIcon icon = new ImageIcon("gameBG.jpg");
        int x = 0;
        int y = 0;
        
        while (true) {  
            g.drawImage(icon.getImage(), x, y, this);  
            if (x > getSize().width && y > getSize().height)
                break;
            if (x > getSize().width) {  
                x = 0;  
                y += icon.getIconHeight();  
            }
            else  
                x += icon.getIconWidth();  
        }
    }
}
