package fp.s100502030;

import java.awt.*;
import javax.swing.*;

public class Food {
	private int foodAmount = 3;// 一般模式的食物
	private ImageIcon[] food = new ImageIcon[4];
	private Image foodImage;
	private int x, y;
	private boolean isSpecial = false;

	public Food() {
		for (int i = 0; i < 4; i++) {
			food[i] = new ImageIcon("image/food/food" + i + ".png");
		}
		// setImage();
		randamFood();
	}
	
	//隨機出現一種食物
	public void randamFood() {
		setImage(isSpecial);
		x = (int) (Math.random() * 20);
		y = (int) (Math.random() * 20);
	}

	public void setImage() {
		setImage(false);
	}

	public void setImage(boolean isSpecial) {
		this.isSpecial = isSpecial;
		if (isSpecial)
			foodImage = food[3].getImage();
		else
			foodImage = food[(int) (Math.random() * foodAmount)].getImage();
	}

	public Image getImage() {
		return foodImage;
	}

	public int getX() {
		return x;
	}// 回傳食物X位置

	public int getY() {
		return y;
	}// 回傳食物X位置
}
