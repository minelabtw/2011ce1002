package fp.s100502030;

import java.util.*;
import java.io.*;

public class Rank {
	private String[] name = new String[10];
	private int[] score = new int[10];
	private String[] rankString;
	private String[] number = new String[10];
	String[] string = new String[10];
	File rankFile;
	Scanner input1;// creat a Scanner for input
	Scanner input2;// creat a Scanner for input

	public Rank() {

		rankFile = new File("rank.txt");

		try {
			input1 = new Scanner(rankFile);
			input2 = new Scanner(rankFile);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.err.println("The file can not be found");
		}

		for (int i = 0; i < 10; i++) {
			number[i] = input1.next();
			name[i] = input1.next();
			score[i] = input1.nextInt();
		}

		for (int i = 0; i < 10; i++) {
			string[i] = input2.nextLine();
		}
		input1.close();
		input2.close();

	}

	public int getHighestsScore() {
		return score[0];
	}

	public int getLowestScore() {
		return score[9];
	}

	public String[] getRank() {
		try {
			input2 = new Scanner(rankFile);

			for (int i = 0; i < 10; i++) {
				string[i] = input2.nextLine();
			}
			input2.close();

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.err.println("The file can not be found");

		}
		return string;
	}

	public void printRank() {
		try {
			PrintWriter output = new PrintWriter(rankFile);
			for (int i = 0; i < 10; i++) {
				output.printf("%-6s%6s%6d", number[i], name[i], score[i]);
				output.println();
			}
			output.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	// 存入要排序的分數
	public void reRank(String playName, int playScore) {

		for (int i = 0; i < 10; i++) {
			if (playScore > score[i]) {
				for (int j = 9; j > i; j--) {
					score[j] = score[j - 1];
					name[j] = name[j - 1];
				}
				score[i] = playScore;
				String nameString = "";
				for (int j = 0; j < playName.length(); j++) {
					if (playName.charAt(j) != ' ') {
						nameString += playName.charAt(j);
					}
				}
				name[i] = nameString;
				break;
			}
		}
	}

}
