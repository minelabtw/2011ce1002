package fp.s100502030;

import java.io.FileNotFoundException;

import javax.swing.JFrame;

public class FinalProject {
	public static void main(String[] args) {
		FrameWork f = new FrameWork();
		f.setSize(800, 600);
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
