package fp.s100502030;


//此Class是別人打的，用來限制可以打的格數

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;

/**
 * 此TextField可限制輸入的字數。
 *
 * @author Mirror Chiu
 * @last-revise 2008-04-19
 */
public class CharacterLimitedTextField extends JTextField {

    /**
     * 以指定的欄位寬度與可輸入字元數量，建立含有指定字串的TextField
     * @param text 字串。
     * @param columns 欄位寬度與可輸入字元數量。
     * @param limit 欄位寬度與可輸入字元數量。
     */
    public CharacterLimitedTextField(String text, int columns, int limit) {
        super(new CharacterLimitedDocument(limit), text, columns);
    }
   
    /**
     * 以指定的欄位寬度與可輸入字元數量，建立TextField物件。
     *
     * @param columns 欄位寬度。
     * @param limit 可輸入字元數量。
     */
    public CharacterLimitedTextField(int columns, int limit) {
        super(new CharacterLimitedDocument(limit),null, columns);
    }
   
    /**
     * 建立欄位寬度與可輸入字元數量相同的TextField物件。
     *
     * @param limit 欄位寬度與可輸入字元數量。
     */
    public CharacterLimitedTextField(int limit) {
        super(new CharacterLimitedDocument(limit),"", limit);
    }

    /**
     * Demo用主程式。
     */
 

    /**
     *    限制輸入字數的Document類別。
     *    @see DefaultStyledDocument
     */
    static public class CharacterLimitedDocument extends DefaultStyledDocument{

        /**
         * 限制的字元數目。
         */
        private final int limit;

        /**
         * 設定限制的輸入字元數量。
         * @param limit
         */
        public CharacterLimitedDocument(int limit) {
            this.limit = limit;
        }
       
        /**
         * 插入字串時，會檢驗當前的字串與要插入的字串總和，是否會超過限制數量的字元。
         * 若超過，就不允許插入字串；否則，就插入字串。
         * @see javax.swing.text.AbstractDocument#insertString(int, java.lang.String, javax.swing.text.AttributeSet)
         */
        @Override
        public synchronized void insertString(int offset,
                             String str,
                             AttributeSet a)
                      throws BadLocationException
        {
            if (this.getLength()+str.length() <= limit) {
                super.insertString(offset, str, a);
            }
        }
    }
}
