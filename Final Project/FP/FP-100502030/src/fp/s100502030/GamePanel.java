package fp.s100502030;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;

import javax.swing.*;

public class GamePanel extends JPanel {
	protected Snake s[] = new Snake[2];
	protected Map map = new Map(0);
	protected Food food = new Food();
	protected Fire fire = new Fire();
	protected Timer[] t = new Timer[2];
	protected Timer fireTimer = new Timer(1000, new TimerListener4());
	protected int initDelay[] = new int[2];
	protected int gameovercounter = 0;
	protected int modechoose = 1;
	protected int score = 0;
	protected boolean[] winner = { false, false };
	protected int mapChoose = 0;
	protected boolean playing = false;
	protected boolean drawPause = false;
	protected JButton[] jbt = new JButton[11];
	private int snakeNumber = 1;
	private Timer gameoverTimer = new Timer(100, new TimerListener3());
	private int interval;
	private int finalDelay = 50;
	private int mapSize = 20;
	private int panelChoose = 0;
	private int firecounter = 0;
	protected int[] hp = { 10, 10 };
	private String highestScore = "1234";
	private String numberString;
	private ImageIcon[] bim1 = new ImageIcon[11];
	private ImageIcon[] bim2 = new ImageIcon[11];
	private Rank rank = new Rank();
	private int[] addspeed = { 100, 100 };
	private boolean addKey = false;
	private boolean exitgame = true;
	private boolean showFrame = true;
	private boolean isGameover = false;
	private JLabel messageLabel = new JLabel("恭喜上榜了！請輸入名字：");
	private CharacterLimitedTextField textField = new CharacterLimitedTextField(
			5, 5);
	private JPanel inputPanel = new JPanel(new GridLayout(2, 1, 5, 5));
	private JFrame inputFrame = new JFrame();

	public GamePanel() {

		for (int i = 0; i < 11; i++) {
			jbt[i] = new JButton();
			bim1[i] = new ImageIcon();
			bim2[i] = new ImageIcon();
			jbt[i].setText(null);
			jbt[i].setBorderPainted(false);
			jbt[i].setContentAreaFilled(false);
			// 去除button內容
		}

		for (int i = 0; i < 6; i++) {
			add(jbt[i]);
		}

		for (int i = 0; i < 2; i++) {

			s[i] = new Snake(i);
			initDelay[i] = 800;
			checkFood(s[i]);

		}

		inputPanel.add(messageLabel);
		inputPanel.add(textField);
		inputFrame.add(inputPanel);

		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nameString = textField.getText();
				for (int i = 0; i < 5 - nameString.length(); i++) {
					nameString = "-" + nameString;
				}
				rank.reRank("-" + nameString, score);
				rank.printRank();
				exitgame = true;
				inputFrame.setVisible(false);
			}
		});

		t[0] = new Timer(500, new TimerListener1());
		t[1] = new Timer(500, new TimerListener2());

	}

	public void checkFood(Snake s) {
		boolean refind = true;
		while (refind) {
			refind = false;
			for (int j = 0; j < s.getSize(); j++) {
				if (food.getX() == s.getX()[j] && food.getY() == s.getY()[j]) {
					refind = true;
					food.randamFood();

					break;
				}

			}
			for (int j = 0; j < map.obstacleNumber; j++) {
				if (food.getX() == map.getX()[j]
						&& food.getY() == map.getY()[j]) {
					refind = true;
					food.randamFood();
					break;
				}
			}
		}
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		int bw = getWidth() / 3;
		int bh = getHeight() * 8 / 55;

		// 設置Button位置
		for (int i = 0; i < 6; i++) {
			jbt[i].setBounds(getWidth() / 3, getHeight() / 55 + i * getHeight()
					* 9 / 55, bw, bh);
		}
		jbt[6].setBounds(getWidth() / 3, getHeight() / 55 + 5 * getHeight() * 9
				/ 55, bw, bh);
		jbt[7].setBounds(getWidth() / 3, getHeight() / 55 + getHeight() * 9
				/ 55, bw, bh);
		jbt[8].setBounds(getWidth() / 3, getHeight() / 55 + getHeight() * 9
				/ 55, bw, bh);
		jbt[7].setBounds(getWidth() / 3 - bh, getHeight() / 55 + getHeight()
				* 9 / 55, bh, bh);
		jbt[8].setBounds(getWidth() / 3 + bw, getHeight() / 55 + getHeight()
				* 9 / 55, bh, bh);
		jbt[9].setBounds(getWidth() / 3 - bh, getHeight() / 55 + 3
				* getHeight() * 9 / 55, bh, bh);
		jbt[10].setBounds(getWidth() / 3 + bw, getHeight() / 55 + 3
				* getHeight() * 9 / 55, bh, bh);

		// 設置Button圖片
		for (int i = 0; i < 11; i++) {
			bim1[i].setImage(new ImageIcon("image/button/button" + i + "0.png")
					.getImage().getScaledInstance(jbt[i].getWidth(),
							jbt[i].getHeight(), Image.SCALE_DEFAULT));
			bim2[i].setImage(new ImageIcon("image/button/button" + i + "1.png")
					.getImage().getScaledInstance(jbt[i].getWidth(),
							jbt[i].getHeight(), Image.SCALE_DEFAULT));
			jbt[i].setIcon(bim1[i]);
			jbt[i].setPressedIcon(bim2[i]);
		}

		interval = Math.min(getWidth() * 3 / 4, getHeight()) / mapSize;
		int Xi = getWidth() * 3 / 4 / 2 - mapSize / 2 * interval;
		int Yi = getHeight() / 2 - mapSize / 2 * interval;

		g.drawImage(new ImageIcon("image/else/back1.png").getImage(), 0, 0,
				getWidth(), getHeight(), this);
		if (panelChoose == 0) {// 遊戲選單畫面
			g.drawImage(new ImageIcon("image/else/back0.png").getImage(), 0, 0,
					getWidth(), getHeight(), this);
		} else if (panelChoose == 1) {// 遊戲進行畫面

			if (addKey) {
				addKeyListener(k1);
				addKey = false;
			}

			highestScore = rank.getHighestsScore() + "";
			if (score > rank.getHighestsScore()) {
				highestScore = score + "";
			}

			// 畫mode1面板
			if (snakeNumber == 1) {
				g.drawImage(new ImageIcon("image/score/record.png").getImage(),
						getWidth() * 3 / 4, Yi, getWidth() / 4,
						getWidth() / 16, this);
				// 最高分
				for (int i = 0; i < highestScore.length(); i++) {
					g.drawImage(
							new ImageIcon("image/score/"
									+ highestScore.charAt(highestScore.length()
											- 1 - i) + ".png").getImage(),
							getWidth() - getWidth() / 24 * (i + 1), Yi
									+ getWidth() / 16, getWidth() / 24,
							getWidth() / 16, this);
				}
				g.drawImage(new ImageIcon("image/score/score.png").getImage(),
						getWidth() * 3 / 4, Yi + getWidth() / 8,
						getWidth() / 4, getWidth() / 16, this);
				numberString = score + "";
				// 現在分數
				for (int i = 0; i < numberString.length(); i++) {
					g.drawImage(
							new ImageIcon("image/score/"
									+ numberString.charAt(numberString.length()
											- 1 - i) + ".png").getImage(),
							getWidth() - getWidth() / 24 * (i + 1), Yi
									+ getWidth() / 16 * 3, getWidth() / 24,
							getWidth() / 16, this);
				}
			}

			// 畫mode2面板
			if (snakeNumber == 2) {
				g.drawImage(
						new ImageIcon("image/score/player1.png").getImage(),
						getWidth() * 3 / 4, Yi, getWidth() / 4,
						getWidth() / 16, this);

				numberString = hp[0] + "";
				g.drawImage(new ImageIcon("image/score/hp.png").getImage(),
						getWidth() * 3 / 4, Yi + getWidth() / 16,
						getWidth() / 4, getWidth() / 16, this);
				for (int i = 0; i < numberString.length(); i++) {
					g.drawImage(
							new ImageIcon("image/score/"
									+ numberString.charAt(numberString.length()
											- 1 - i) + ".png").getImage(),
							getWidth() - getWidth() / 24 * (i + 1), Yi
									+ getWidth() / 16, getWidth() / 24,
							getWidth() / 16, this);
				}
				g.drawImage(
						new ImageIcon("image/score/player2.png").getImage(),
						getWidth() * 3 / 4, Yi + getWidth() / 8,
						getWidth() / 4, getWidth() / 16, this);
				numberString = hp[1] + "";
				g.drawImage(new ImageIcon("image/score/hp.png").getImage(),
						getWidth() * 3 / 4, Yi + getWidth() / 16 * 3,
						getWidth() / 4, getWidth() / 16, this);
				for (int i = 0; i < numberString.length(); i++) {
					g.drawImage(
							new ImageIcon("image/score/"
									+ numberString.charAt(numberString.length()
											- 1 - i) + ".png").getImage(),
							getWidth() - getWidth() / 24 * (i + 1), Yi
									+ getWidth() / 16 * 3, getWidth() / 24,
							getWidth() / 16, this);
				}
			}

			// 畫地圖
			for (int i = 0; i < 20; i++) {
				for (int j = 0; j < 20; j++) {
					g.drawImage(map.getBasicImage(), Xi + i * interval, Yi + j
							* interval, interval, interval, this);
				}
			}

			// 畫障礙物
			for (int i = 0; i < map.obstacleNumber; i++) {

				g.drawImage(map.getObstacleImage(), Xi + map.getX()[i]
						* interval, Yi + map.getY()[i] * interval, interval,
						interval, this);
			}

			// 畫食物

			g.drawImage(food.getImage(), Xi + food.getX() * interval,
					Yi + food.getY() * interval, interval, interval, this);

			// 畫火
			if (snakeNumber == 2) {
				for (int i = 0; i < fire.getFireNumber(); i++) {
					g.drawImage(fire.getFireImage(), Xi + fire.getX()[i]
							* interval, Yi + fire.getY()[i] * interval,
							interval, interval, this);
				}
			}

			// 是否撞到自己
			for (int i = 0; i < snakeNumber; i++) {
				if (!s[i].isDead) {
					for (int j = 1; j < s[i].getSize(); j++) {
						if (s[i].getX()[0] == s[i].getX()[j]
								&& s[i].getY()[0] == s[i].getY()[j]) {
							s[i].isDead = true;
							if (snakeNumber == 1)
								isGameover = true;
							else {
								if (i == 0)
									winner[1] = true;
								else {
									winner[0] = true;
								}
							}
							break;
						}
					}
				}
			}

			// 判斷是否與其他蛇相撞
			if (snakeNumber == 2) {
				for (int i = 0; i < s[1].getSize(); i++) {
					if (s[0].getX()[0] == s[1].getX()[i]
							&& s[0].getY()[0] == s[1].getY()[i]) {
						s[0].isDead = true;
						winner[0] = true;
						break;
					}

				}
				for (int i = 0; i < s[1].getSize(); i++) {
					{
						if (s[1].getX()[0] == s[0].getX()[i]
								&& s[1].getY()[0] == s[0].getY()[i]) {
							s[1].isDead = true;
							winner[1] = true;
							break;
						}
					}
				}
			}

			// 畫蛇
			for (int i = 0; i < snakeNumber; i++) {
				if (!s[i].isDead)
					for (int j = 0; j < s[i].getSize(); j++) {
						g.drawImage(s[i].getImage()[j], Xi + s[i].getX()[j]
								* interval, Yi + s[i].getY()[j] * interval,
								interval, interval, this);

					}
			}

			// 判斷是否吃到食物
			for (int i = 0; i < snakeNumber; i++) {
				if (!s[i].isDead) {
					if (s[i].getX()[0] == food.getX()
							&& s[i].getY()[0] == food.getY()) {
						if (snakeNumber == 1)
							score += 5;
						else {
							hp[i] += 1;
						}

						s[i].lengthenSnake();
						s[i].setImage();
						food.randamFood();
						boolean refind = true;
						while (refind) {
							refind = false;
							for (int j = 0; j < s[i].getSize(); j++) {
								if (food.getX() == s[i].getX()[j]
										&& food.getY() == s[i].getY()[j]) {
									refind = true;
									food.randamFood();
									break;
								}

							}
							for (int j = 0; j < map.obstacleNumber; j++) {
								if (food.getX() == map.getX()[j]
										&& food.getY() == map.getY()[j]) {
									refind = true;
									food.randamFood();
									break;
								}
							}
						}
					}
				}
				repaint();
			}

			// 撞到障礙物
			for (int i = 0; i < snakeNumber; i++) {
				if (!s[i].isDead)
					for (int j = 0; j < map.obstacleNumber; j++) {
						if (s[i].getX()[0] == map.getX()[j]
								&& s[i].getY()[0] == map.getY()[j]) {
							s[i].isDead = true;
							if (snakeNumber == 1)
								isGameover = true;
							else if (snakeNumber == 2) {
								if (i == 0)
									winner[1] = true;
								else {
									winner[0] = true;
								}
							}
						}
					}
			}

			for (int i = 0; i < snakeNumber; i++) {
				if (hp[i] == 0)
					winner[i] = true;
			}

			// 速度變快
			if (snakeNumber == 1) {
				if (score >= addspeed[0] && score != 0 && initDelay[0] > 50) {

					addspeed[0] += 100;
					initDelay[0] -= 50;
					if (initDelay[0] <= 50)
						initDelay[0] = 50;
					t[0].setDelay(initDelay[0]);
				}
			}

			// pause
			if (drawPause) {
				g.drawImage(new ImageIcon("image/else/pause.png").getImage(),
						Xi + 5 * interval, Yi + 9 * interval, interval * 10,
						interval * 2, this);
			}

			// gameover
			if (isGameover) {

				gameoverTimer.start();

				if (gameovercounter / 5 % 2 == 0 || gameovercounter >= 20) {
					g.drawImage(
							new ImageIcon("image/else/gameover.png").getImage(),
							Xi + 5 * interval, Yi + 9 * interval,
							interval * 10, interval * 2, this);
				}// game over文字閃爍

				playing = false;
				for (int i = 0; i < snakeNumber; i++) {
					t[i].stop();
				}
				if (gameovercounter > 25) {
					removeKeyListener(k1);
					gameoverTimer.stop();
					for (int i = 0; i < 2; i++) {
						t[i].stop();
					}
					if (score > rank.getLowestScore() && showFrame) {
						// count = 2;

						inputFrame.setBounds(getWidth() / 2, getHeight() / 2,
								200, 100);
						inputFrame.setVisible(true);
						// add(messageLabel);
						// add(textField);
						exitgame = false;
						showFrame = false;

					}
					if (exitgame) {
						panelChoose = 0;
						for (int i = 0; i < 6; i++) {
							add(jbt[i]);
						}
						isGameover = false;
					}
				}

			}
			// gameover2
			if (winner[0] || winner[1]) {
				System.out.println("11111111111");
				int winnerNumber = 0;
				if (winner[0] && winner[1])
					winnerNumber = 3;
				else if (winner[0])
					winnerNumber = 1;
				else {
					winnerNumber = 2;
				}

				gameoverTimer.start();
				fireTimer.stop();
				if (gameovercounter / 5 % 2 == 0 || gameovercounter >= 20) {
					g.drawImage(new ImageIcon("image/else/player"
							+ winnerNumber + "win.png").getImage(), Xi + 5
							* interval, Yi + 9 * interval, interval * 10,
							interval * 2, this);
				}// resule over文字閃爍

				playing = false;

				for (int i = 0; i < snakeNumber; i++) {
					t[i].stop();
				}
				if (gameovercounter > 25) {
					removeKeyListener(k1);
					gameoverTimer.stop();

					winner[0] = false;
					winner[1] = false;
					for (int i = 0; i < 2; i++) {
						hp[i] = 10;
						t[i].stop();
					}
					if (exitgame) {
						panelChoose = 0;
						for (int i = 0; i < 6; i++) {
							add(jbt[i]);
						}

					}
				}

			}

		} else if (panelChoose == 2) {
			g.drawImage(new ImageIcon("image/else/mode" + modechoose + ".png")
					.getImage(), getWidth() / 2 - bh, getHeight() / 55
					+ getHeight() * 9 / 55, bh * 2, bh, this);

			g.drawImage(new ImageIcon("image/else/map" + mapChoose + ".png")
					.getImage(), getWidth() / 2 - bh, getHeight() / 55 + 3
					* getHeight() * 9 / 55, bh * 2, bh * 2, this);

		} else if (panelChoose == 3) {
			g.drawImage(new ImageIcon("image/else/help.png").getImage(),
					getWidth() / 7, 0, getWidth() * 5 / 7, getHeight() - bh,
					this);

		} else if (panelChoose == 4) {
			g.setColor(Color.GRAY);
			rank.printRank();
			setFont(new Font("SansSerif", Font.BOLD, (getHeight() - bh) / 10));
			FontMetrics fm = g.getFontMetrics();
			for (int i = 0; i < 10; i++) {
				for (int j = 0; j < 18; j++) {
					g.drawString(rank.getRank()[i].charAt(j) + "", j
							* getWidth() / 18, (i + 1) * (getHeight() - bh)
							/ 10);
				}
			}
		}

	}

	// Time Event
	class TimerListener1 implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			s[0].moveSnake();
			s[0].setImage();

			// 碰到火
			if (snakeNumber == 2) {
				for (int j = 0; j < fire.getFireNumber(); j++) {
					if (s[0].getX()[0] == fire.getX()[j]
							&& s[0].getY()[0] == fire.getY()[j]) {
						hp[0]--;

					}
				}
			}
			repaint();
		}
	}

	class TimerListener2 implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			s[1].moveSnake();
			s[1].setImage();
			if (snakeNumber == 2) {
				// 碰到火
				for (int j = 0; j < fire.getFireNumber(); j++) {
					if (s[1].getX()[0] == fire.getX()[j]
							&& s[1].getY()[0] == fire.getY()[j]) {
						hp[1]--;

					}
				}
			}
			repaint();
		}
	}

	class TimerListener3 implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			gameovercounter++;

		}
	}

	class TimerListener4 implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			firecounter++;
			if (firecounter % 2 == 0) {
				fire.randamFireNumber();
				for (int i = 0; i < fire.getFireNumber(); i++)
					fire.randamOneFire(i, s[0], s[1], food, map);
			}
			// System.out.println(fire.getFireNumber()+" "+firecounter.x[0]);
			// repaint();
		}

	}

	// Key Event
	protected KeyAdapter k1 = new KeyAdapter() {
		private boolean p1[] = { false, false };
		private boolean p2[] = { false, false };
		private boolean p3[] = { false, false };
		private boolean p4[] = { false, false };

		public void keyPressed(KeyEvent e) {

			if (e.getKeyCode() == KeyEvent.VK_ENTER) {
				for (int i = 0; i < snakeNumber; i++) {

					t[i].start();
				}
			}

			// User1 Key
			if (e.getKeyCode() == KeyEvent.VK_RIGHT
					&& s[0].direction != s[0].LEFT) {
				for (int i = 0; i < snakeNumber; i++) {
					s[0].setDirection(s[0].RIGHT);
					t[0].setDelay(finalDelay);
					p1[0] = true;
				}
			} else if (e.getKeyCode() == KeyEvent.VK_LEFT
					&& s[0].direction != s[0].RIGHT) {
				s[0].setDirection(s[0].LEFT);
				t[0].setDelay(finalDelay);
				p2[0] = true;
			} else if (e.getKeyCode() == KeyEvent.VK_UP
					&& s[0].direction != s[0].DOWN) {

				s[0].setDirection(s[0].UP);
				t[0].setDelay(finalDelay);
				p3[0] = true;
			} else if (e.getKeyCode() == KeyEvent.VK_DOWN
					&& s[0].direction != s[0].UP) {

				s[0].setDirection(s[0].DOWN);
				t[0].setDelay(finalDelay);
				p4[0] = true;
			}

			// User2 Key
			if (e.getKeyCode() == KeyEvent.VK_D && s[1].direction != s[1].LEFT) {
				for (int i = 0; i < snakeNumber; i++) {
					s[1].setDirection(s[1].RIGHT);
					t[1].setDelay(finalDelay);
					p1[1] = true;
				}
			} else if (e.getKeyCode() == KeyEvent.VK_A
					&& s[1].direction != s[1].RIGHT) {
				s[1].setDirection(s[1].LEFT);
				t[1].setDelay(finalDelay);
				p2[1] = true;
			} else if (e.getKeyCode() == KeyEvent.VK_W
					&& s[1].direction != s[1].DOWN) {

				s[1].setDirection(s[1].UP);
				t[1].setDelay(finalDelay);
				p3[1] = true;
			} else if (e.getKeyCode() == KeyEvent.VK_S
					&& s[1].direction != s[1].UP) {

				s[1].setDirection(s[1].DOWN);
				t[1].setDelay(finalDelay);
				p4[1] = true;
			}

			// stop game
			if (e.getKeyCode() == KeyEvent.VK_SPACE) {
				for (int i = 0; i < snakeNumber; i++) {
					if (!s[i].isDead) {

						if (t[i].isRunning()) {
							t[i].stop();
							drawPause = true;
							fireTimer.stop();
						} else {

							t[i].start();
							fireTimer.start();
							drawPause = false;
						}
					}
				}

			}

			// exit game
			if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {

				panelChoose = 0;
				for (int i = 0; i < 6; i++) {
					add(jbt[i]);
				}
				for (int i = 0; i < snakeNumber; i++) {
					t[i].stop();
				}
				fireTimer.stop();
				drawPause = true;
				removeKeyListener(k1);

			}

		}

		public void keyReleased(KeyEvent e) {

			// User1 Key
			if (e.getKeyCode() == KeyEvent.VK_RIGHT
			/* && s[0].direction != s[0].LEFT */) {
				p1[0] = false;
			} else if (e.getKeyCode() == KeyEvent.VK_LEFT
			/* && s[0].direction != s[0].RIGHT */) {
				p2[0] = false;
			} else if (e.getKeyCode() == KeyEvent.VK_UP
			/* && s[0].direction != s[0].DOWN */) {
				p3[0] = false;
			} else if (e.getKeyCode() == KeyEvent.VK_DOWN
			/* && s[0].direction != s[0].UP */) {
				p4[0] = false;
			}

			// User2 Key
			if (e.getKeyCode() == KeyEvent.VK_D
			/* && s[0].direction != s[0].LEFT */) {
				p1[1] = false;
			} else if (e.getKeyCode() == KeyEvent.VK_A
			/* && s[0].direction != s[0].RIGHT */) {
				p2[1] = false;
			} else if (e.getKeyCode() == KeyEvent.VK_W
			/* && s[0].direction != s[0].DOWN */) {
				p3[1] = false;
			} else if (e.getKeyCode() == KeyEvent.VK_S
			/* && s[0].direction != s[0].UP */) {
				p4[1] = false;
			}

			for (int i = 0; i < snakeNumber; i++) {
				if (p1[i] == false && p2[i] == false && p3[i] == false
						&& p4[i] == false) {
					t[i].setDelay(initDelay[i]);

				}
			}

		}
	};

	public void addKey(boolean addKey) {
		this.addKey = addKey;
	}

	public void PanelChoose(int number) {
		panelChoose = number;
	}

	public int getPanel() {
		return panelChoose;
	}

	public void setSnakeAmount(int number) {
		snakeNumber = number;

	}
}
