package fp.s100502030;

import java.awt.*;

import javax.swing.*;

public class Snake {
	private final int arraySize = 100;
	protected final int RIGHT = 0;
	protected final int DOWN = 1;
	protected final int LEFT = 2;
	protected final int UP = 3;
	protected int direction;
	private int snakeSize = 3;
	private int[] x = new int[arraySize];
	private int[] y = new int[arraySize];
	private int dx;
	private int dy;
	private ImageIcon[] snakeHead = new ImageIcon[4];
	private ImageIcon[] snakeBody = new ImageIcon[6];
	private ImageIcon[] snakeTail = new ImageIcon[4];
	private ImageIcon[] snakeImageIcon = new ImageIcon[100];
	private Image[] snakeImage = new Image[100];
	private int number;
	private ImageIcon tbody;
	protected boolean isDead = false;

	// private Timer t = new

	public Snake(int number) {
		this.number = number;
		// for (int i = 0; i < 100; i++) {
		// snakeImageIcon[i] = new ImageIcon("mapqq.png");
		// }
		setSnakeCase(number);
		initSnake();

	}
	
	// 初始化位置
	public void initSnake() {
		isDead = false;
		if (number == 0) {
			x[0] = 3;
			x[1] = 2;
			x[2] = 1;
			y[0] = 1;
			y[1] = 1;
			y[2] = 1;
			setDirection(RIGHT);

		} else if (number == 1) {
			x[0] = 16;
			x[1] = 17;
			x[2] = 18;
			y[0] = 18;
			y[1] = 18;
			y[2] = 18;
			setDirection(LEFT);
		}
		snakeImageIcon[1] = snakeBody[4];
		snakeImageIcon[2] = snakeTail[0];
		snakeSize = 3;
		setImage();
	}

	public void setSnakeCase(int number) {
		this.number = number;

		for (int i = 0; i < 4; i++) {
			snakeHead[i] = new ImageIcon("image/snake/snakehead" + number + i
					+ ".png");
			snakeBody[i] = new ImageIcon("image/snake/snakebody" + number + i
					+ ".png");
			snakeTail[i] = new ImageIcon("image/snake/snaketail" + number + i
					+ ".png");
		}
		snakeBody[4] = new ImageIcon("image/snake/snakebody" + number + 4
				+ ".png");
		snakeBody[5] = new ImageIcon("image/snake/snakebody" + number + 5
				+ ".png");

	}

	public void moveSnake() {
		tbody = snakeImageIcon[snakeSize - 2];
		for (int i = snakeSize; i > 0; i--) {
			x[i] = x[i - 1];
			y[i] = y[i - 1];
			if (i != snakeSize - 1 || i != 1) {
				snakeImageIcon[i] = snakeImageIcon[i - 1];
			}
		}
		x[0] += dx;
		y[0] += dy;
		
		// 穿牆部分
		if (x[0] == 20) {
			x[0] = 0;
		}
		if (x[0] < 0) {
			x[0] = 19;
		}
		if (y[0] == 20) {
			y[0] = 0;
		}
		if (y[0] < 0) {
			y[0] = 19;
		}
	}
	
	// 方向設置
	public void setDirection(int direction) {
		snakeImageIcon[0] = snakeHead[direction];
		this.direction = direction;
		switch (direction) {
		case RIGHT:
			dx = 1;
			dy = 0;
			break;
		case DOWN:
			dx = 0;
			dy = 1;
			break;
		case LEFT:
			dx = -1;
			dy = 0;
			break;
		case UP:
			dx = 0;
			dy = -1;
			break;
		}
	}
	
	// 設置尾巴圖片
	public void setTail() {
		if (snakeImageIcon[snakeSize - 2] == snakeBody[4]) {
			if (x[snakeSize - 3] - x[snakeSize - 2] == 1
					|| x[snakeSize - 3] - x[snakeSize - 2] == -19) {
				snakeImageIcon[snakeSize - 1] = snakeTail[0];
			} else {
				snakeImageIcon[snakeSize - 1] = snakeTail[2];
			}
		} else if (snakeImageIcon[snakeSize - 2] == snakeBody[5]) {
			if (y[snakeSize - 3] - y[snakeSize - 2] == 1
					|| y[snakeSize - 3] - y[snakeSize - 2] == -19) {
				snakeImageIcon[snakeSize - 1] = snakeTail[1];
			} else {
				snakeImageIcon[snakeSize - 1] = snakeTail[3];
			}
		} else if (snakeImageIcon[snakeSize - 2] == snakeBody[0]) {
			if (x[snakeSize - 3] - x[snakeSize - 2] == 0) {
				snakeImageIcon[snakeSize - 1] = snakeTail[0];
			} else {
				snakeImageIcon[snakeSize - 1] = snakeTail[1];
			}
		} else if (snakeImageIcon[snakeSize - 2] == snakeBody[1]) {
			if (x[snakeSize - 3] - x[snakeSize - 2] == 0) {
				snakeImageIcon[snakeSize - 1] = snakeTail[2];
			} else {
				snakeImageIcon[snakeSize - 1] = snakeTail[1];
			}
		} else if (snakeImageIcon[snakeSize - 2] == snakeBody[2]) {
			if (x[snakeSize - 3] - x[snakeSize - 2] == 0) {
				snakeImageIcon[snakeSize - 1] = snakeTail[2];
			} else {
				snakeImageIcon[snakeSize - 1] = snakeTail[3];
			}
		} else if (snakeImageIcon[snakeSize - 2] == snakeBody[3]) {
			if (x[snakeSize - 3] - x[snakeSize - 2] == 0) {
				snakeImageIcon[snakeSize - 1] = snakeTail[0];
			} else {
				snakeImageIcon[snakeSize - 1] = snakeTail[3];
			}
		}

	}
	
	// 設置第一個身體圖片
	public void setFirstBody() {
		if (direction == RIGHT) {
			if (y[2] - y[0] == 0) {
				snakeImageIcon[1] = snakeBody[4];
			} else if (y[2] - y[0] == 1 || y[2] - y[0] == -19) {
				snakeImageIcon[1] = snakeBody[2];
			} else {
				snakeImageIcon[1] = snakeBody[1];
			}
		} else if (direction == LEFT) {
			if (y[2] - y[0] == 0) {
				snakeImageIcon[1] = snakeBody[4];
			} else if (y[2] - y[0] == 1 || y[2] - y[0] == -19) {
				snakeImageIcon[1] = snakeBody[3];
			} else {
				snakeImageIcon[1] = snakeBody[0];
			}
		} else if (direction == UP) {
			if (x[2] - x[0] == 0) {
				snakeImageIcon[1] = snakeBody[5];
			} else if (x[2] - x[0] == 1 || x[2] - x[0] == -19) {
				snakeImageIcon[1] = snakeBody[1];
			} else {
				snakeImageIcon[1] = snakeBody[0];
			}
		} else if (direction == DOWN) {
			if (x[2] - x[0] == 0) {
				snakeImageIcon[1] = snakeBody[5];
			} else if (x[2] - x[0] == 1 || x[2] - x[0] == -19) {
				snakeImageIcon[1] = snakeBody[2];
			} else {
				snakeImageIcon[1] = snakeBody[3];
			}
		}
	}
	
	//變長
	public void lengthenSnake() {
		snakeSize++;
		snakeImageIcon[snakeSize - 1] = snakeImageIcon[snakeSize - 2];
		snakeImageIcon[snakeSize - 2] = tbody;

	}
	
	public void setImage() {
		setFirstBody();
		setTail();
		for (int i = 0; i < snakeSize; i++) {
			snakeImage[i] = snakeImageIcon[i].getImage();
		}
	}

	public Image[] getImage() {
		return snakeImage;
	}
	
	//回傳蛇每個位置
	public int[] getX() {
		return x;
	}
	public int[] getY() {
		return y;
	}
	
	//回傳蛇長度
	public int getSize() {
		return snakeSize;
	}
}
