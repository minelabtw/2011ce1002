package fp.s100502030;

import java.awt.*;
import javax.swing.*;

public class Map {
	private ImageIcon basicImageIcon = new ImageIcon("image/map/map0.png");
	private ImageIcon obstacleImageIcon = new ImageIcon(
			"image/map/obstacle.png");
	protected int obstacleNumber;
	private int[] x = new int[100];
	private int[] y = new int[100];

	public Map() {
		this(0);
	}

	public Map(int number) {
		setMap(number);
	}

	public void setMap(int number) {
		if (number == 0) {
			obstacleNumber = 0;
		} else if (number == 1) {
			obstacleNumber = 76;
			for (int i = 0; i < 19; i++) {
				x[i] = i;
				y[i] = 0;
			}
			for (int i = 19; i < 38; i++) {
				x[i] = 19;
				y[i] = i - 19;
			}
			for (int i = 38; i < 57; i++) {
				x[i] = 57 - i;
				y[i] = 19;
			}
			for (int i = 57; i < 76; i++) {
				x[i] = 0;
				y[i] = 76 - i;
			}
		}
		else if(number == 2) {
			obstacleNumber = 40;
			int k = 0;
			for(int i=0;i<10;i++){
				x[k]=i;
				y[k] = 4;
				x[k+10] = 14;
				y[k+10] = i;
				x[k+20] = 4;
				y[k+20] = 10+i;
				x[k+30] = 10+i;
				y[k+30] = 14;
				k++;
			}
		}
	}

	public Image getBasicImage() {
		return basicImageIcon.getImage();
	}

	public Image getObstacleImage() {
		return obstacleImageIcon.getImage();
	}

	public int[] getX() {
		return x;
	}

	public int[] getY() {
		return y;
	}
}
