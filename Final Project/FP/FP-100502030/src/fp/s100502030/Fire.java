package fp.s100502030;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Fire {
	private int fireNumber = 1;
	private int[] x = new int[4];
	private int[] y = new int[4];
	private Image fireImage;

	public Fire() {
		fireImage = new ImageIcon("image/else/fire.png").getImage();
		randamFire();
	}

	public void randamFireNumber() {
		int r = (int) (Math.random() * 10);
		if (r < 4)
			fireNumber = 1;
		else if (r < 7)
			fireNumber = 2;
		else if (r < 9)
			fireNumber = 3;
		else
			fireNumber = 4;
	}// 隨機出現的火焰量
	
	public Image getFireImage(){
		return fireImage;
	}
	
	public int getFireNumber(){
		return fireNumber;
	}// 回傳火焰量
	
	public void randamFire(){
		for(int i=0;i<fireNumber;i++){
			randamOneFire(i);
		}
	}// 在不考慮其他東西情況出現火焰
	
	public void randamOneFire(int index) {
		
		boolean refind = true;
		while (refind) {
			refind = false;
			for (int i = 0; i < index; i++) {
				if (x[index] == x[i] && y[index] == y[i]) {
					refind = true;
					x[index] = (int) (Math.random() * 20);
					y[index] = (int) (Math.random() * 20);
					break;
				}
			}
		}
	}
	

	public void randamOneFire(int index, Snake snake1,Snake snake2, Food food, Map map) {
		x[index] = (int) (Math.random() * 20);
		y[index] = (int) (Math.random() * 20);
		boolean refind = true;
		while (refind) {
			refind = false;
			for (int i = 0; i < index; i++) {
				if (x[index] == x[i] && y[index] == y[i]) {
					refind = true;
					x[index] = (int) (Math.random() * 20);
					y[index] = (int) (Math.random() * 20);
					break;
				}
			}
			
			for (int i = 0; i < snake1.getSize(); i++) {
				if (x[index] == snake1.getX()[i] && y[index] == snake1.getY()[i]) {
					refind = true;
					x[index] = (int) (Math.random() * 20);
					y[index] = (int) (Math.random() * 20);
					break;
				}
			}
			
			for (int i = 0; i < snake2.getSize(); i++) {
				if (x[index] == snake2.getX()[i] && y[index] == snake2.getY()[i]) {
					refind = true;
					x[index] = (int) (Math.random() * 20);
					y[index] = (int) (Math.random() * 20);
					break;
				}
			}
			for (int i = 0; i < map.obstacleNumber; i++) {
				if (x[index] == map.getX()[i]
						&& y[index] == map.getY()[i]) {
					refind = true;
					x[index] = (int) (Math.random() * 20);
					y[index] = (int) (Math.random() * 20);
					break;
				}
			}
		}
	}// 考慮其他東西情況出現火焰
	
	public int[] getX(){
		return x;
	}
	
	public int[] getY(){
		return y;
	}
}
