package fp.s100502030;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;

import javax.swing.JFrame;

public class FrameWork extends JFrame {
	private GamePanel gp = new GamePanel();

	public FrameWork() {

		setLayout(new GridLayout(1, 1));
		add(gp);
		gp.setFocusable(true);

		for (int i = 0; i < 6; i++) {
			gp.add(gp.jbt[i]);
			gp.jbt[i].addActionListener(new ButtonListener());

		}
		for (int i = 0; i < 11; i++) {
			gp.jbt[i].addActionListener(new ButtonListener());

		}

	}

	// Button Event
	class ButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			// 因為當初遇到按下Buron後，keylistener會沒有效果
			//無意中發現把panel remove後再重新add keylistener才重新有效
			// 所以下面才在這打

			if (e.getSource() == gp.jbt[0]) {
				// 開始新遊戲，並初始化數值
				gp.drawPause = false;
				gp.food.randamFood();
				for (int i = 0; i < gp.modechoose; i++) {
					gp.s[i].initSnake();
					gp.s[i].isDead = false;
					
					gp.t[i].start();
					gp.initDelay[i] = 500;
					gp.checkFood(gp.s[i]);
					gp.hp[i]=10;

				}
				gp.fireTimer.start();
				gp.map.setMap(gp.mapChoose);
				gp.score = 0;
				if(gp.modechoose==2){
					gp.fire.randamFireNumber();
					for(int i=0;i<gp.fire.getFireNumber();i++)
					gp.fire.randamOneFire(i,gp.s[0],gp.s[1],gp.food,gp.map);
					gp.winner[0]=false;
					gp.winner[1]=false;
					gp.food.setImage(true);
				}
				else {
					gp.food.setImage(false);
				}
				gp.gameovercounter = 0;
				gp.setSnakeAmount(gp.modechoose);
				remove(gp);
				for (int i = 0; i < 6; i++) {
					gp.remove(gp.jbt[i]);
				}
				gp.PanelChoose(1);
				gp.addKey(true);// 開始游戲key才有效
				gp.repaint();
				add(gp);
				gp.playing = true;

			} else if (e.getSource() == gp.jbt[1] && gp.playing) {
				// 繼續遊戲，假如之前沒有開遊戲，此button不會有效
				remove(gp);
				for (int i = 0; i < 6; i++) {
					gp.remove(gp.jbt[i]);
				}
				// gp.addKeyListener(gp.k1);
				// gp.setFocusable(true);
				gp.PanelChoose(1);
				gp.addKey(true);
				gp.playing = true;
				add(gp);
			} else if (e.getSource() == gp.jbt[2]) {
				for (int i = 0; i < 6; i++) {
					gp.remove(gp.jbt[i]);
				}

				for (int i = 6; i < 11; i++) {
					gp.add(gp.jbt[i]);
					;
				}

				gp.PanelChoose(2);

			} else if (e.getSource() == gp.jbt[3]) {
				for (int i = 0; i < 6; i++) {
					gp.remove(gp.jbt[i]);
				}
				gp.add(gp.jbt[6]);
				gp.PanelChoose(3);
				gp.repaint();

			} // 進入help畫面
			else if (e.getSource() == gp.jbt[4]) {
				for (int i = 0; i < 6; i++) {
					gp.remove(gp.jbt[i]);
				}
				gp.add(gp.jbt[6]);
				gp.PanelChoose(4);
				gp.repaint();

			}
			else if (e.getSource() == gp.jbt[5]) {
				System.exit(0);
			}// 離開遊戲
			else if (e.getSource() == gp.jbt[6]) {

				gp.remove(gp.jbt[6]);
				if (gp.getPanel() == 1) {
					gp.remove(gp.jbt[6]);
					gp.gameovercounter = 0;
				} else if (gp.getPanel() == 2) {
					for (int i = 7; i < 11; i++) {
						gp.remove(gp.jbt[i]);
					}
				}
				gp.PanelChoose(0);
				for (int i = 0; i < 6; i++) {
					gp.add(gp.jbt[i]);
				}
				
				//選擇模式
			} else if (e.getSource() == gp.jbt[7]) {
				gp.modechoose--;
				if (gp.modechoose == 0) {
					gp.modechoose = 2;
				}

			} else if (e.getSource() == gp.jbt[8]) {
				gp.modechoose++;
				if (gp.modechoose == 3) {
					gp.modechoose = 1;
				}
				// 選擇地圖
			} else if (e.getSource() == gp.jbt[9]) {
				gp.mapChoose--;
				if (gp.mapChoose == -1) {
					gp.mapChoose = 2;
				}
				gp.repaint();
			} else if (e.getSource() == gp.jbt[10]) {
				gp.mapChoose++;
				if (gp.mapChoose == 3) {
					gp.mapChoose = 0;
				}
				gp.repaint();
			}
			repaint();
		}
	};

}
