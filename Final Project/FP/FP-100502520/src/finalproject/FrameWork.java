package finalproject;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;

public class FrameWork extends JFrame implements ActionListener{
	public Font F1 = new Font("w",Font.BOLD,40);
	public Font F2 = new Font("x",Font.BOLD,24);
	public Font F3 = new Font("y",Font.BOLD,30);
	public Font F4 = new Font("y",Font.BOLD,70);
	public Font F5 = new Font("y",Font.BOLD,50);
	public Font F6 = new Font("y",Font.BOLD,20);
	public LineBorder Line1 = new LineBorder(Color.BLACK, 5);
	public JFrame frame = new JFrame();
	
	public gamedata data = new gamedata();
	public Battleproceduce battle = new Battleproceduce();
	
	private JButton[] B1 = new JButton[2];
	private JButton B2 = new JButton("繼續");
	private JButton[] B3 = new JButton[3];
	private JButton[] B4 = new JButton[3];
	private JButton[] B5 = new JButton[5];
	private JButton[] B6 = new JButton[3];
	private JButton[] B7 = new JButton[3];
	private JButton[] B8 = new JButton[2];
	private JButton end = new JButton();
	public JButton nextTurn = new JButton("回合結束"); 
	public JLabel img = new JLabel();
	public JScrollPane scroll = new JScrollPane();
	public JLabel attack = new JLabel();
	
	private JLabel[] L1 = new JLabel[4];
	private JLabel L_VS = new JLabel("V.S.");
	public JLabel back1;
	public JLabel back2;
	public JLabel back3;
	
	public JPanel P1 = new JPanel();
	public JPanel P2 = new JPanel();
	public JPanel P3 = new JPanel();
	
	public Container cp1;
	
	public ImageIcon[] usercard = new ImageIcon[5];
	public int UserCardNumber[] = new int[5];
	public int UserCardIndex;
	public int EnemyButtonType = 0;
	public int UserTableButtonType = 1;
	public int UserCardButtonType = 0;
	public int musicswitch;
	public int nextturntype = 0;
	
	//設定主選單
	public void menu(){
		//此七行在設定背景
		musicswitch = 0;
		back1 = new JLabel(data.Backgrounds[0]);
		this.getLayeredPane().add(back1, new Integer(Integer.MIN_VALUE));
		back1.setBounds(0, 0, data.Backgrounds[0].getIconWidth(), data.Backgrounds[0].getIconHeight());
		cp1 = this.getContentPane();
		cp1.setLayout(new BorderLayout());
		((JPanel)cp1).setOpaque(false);
		getContentPane().setLayout(null);
		
		for(int i = 0; i<2; i++){
			this.add(B1[i]);
			B1[i].addActionListener(this);
		}
		
		data.Music[0].loop();
	}
	
	//設定並更新生命值
	public void setLife(){
		//敵人生命值歸零
		if(battle.getEnemyLife() == 0){
			this.remove(end);
			end = new JButton("獲勝!!");
			end.setFont(F5);
			end.setBounds( 100, 200, 200, 100);
			add(end);
			end.addActionListener(this);
			nextturntype = 1;
			
		}
		
		//玩家生命值歸零
		if(battle.getUserLife() == 0){
			this.remove(end);
			end = new JButton("戰敗!!");
			end.setFont(F5);
			end.setBounds( 100, 200, 200, 100);
			add(end);
			end.addActionListener(this);
			nextturntype = 1;
		}
		
		L1[2] = new JLabel(Integer.toString(battle.getEnemyLife()));
		L1[3] = new JLabel(Integer.toString(battle.getUserLife()));
		L1[2].setFont(F3);
		L1[3].setFont(F3);
		L1[2].setForeground(Color.BLACK);
		L1[3].setForeground(Color.BLACK);
		
		if(battle.getEnemyLife() <5000){
			L1[2].setForeground(Color.RED);
		}
		if(battle.getUserLife() <5000){
			L1[3].setForeground(Color.RED);
		}
		
		L1[2].setBounds(30, 80, 200, 40);
		L1[3].setBounds(30, 480, 200, 40);
		this.add(L1[2]);
		this.add(L1[3]);
		this.repaint();
		this.setVisible(true);
	}
	
	//遊戲規則說明
	public void illustration(){
		back2 = new JLabel(data.Backgrounds[1]);
		this.getLayeredPane().add(back2, new Integer(Integer.MIN_VALUE));
		back2.setBounds(0, 0, data.Backgrounds[1].getIconWidth(), data.Backgrounds[1].getIconHeight());
		cp1 = this.getContentPane();
		cp1.setLayout(new BorderLayout());
		((JPanel)cp1).setOpaque(false);
		getContentPane().setLayout(null);
		
		//設定繼續建
		B2.setBounds(620, 500, 180, 70);
		B2.setFont(F1);
		B2.setOpaque(false);
		B2.setBorder(null);
		B2.setContentAreaFilled(false);
		this.add(B2);
		B2.addActionListener(this);
		repaint();
	}
	
	//進入戰鬥畫面
	public void Battle(){
		//設定背景
		back3 = new JLabel(data.Backgrounds[2]);
		this.getLayeredPane().add(back3, new Integer(Integer.MIN_VALUE));
		back3.setBounds(0, 0, data.Backgrounds[2].getIconWidth(), data.Backgrounds[2].getIconHeight());
		cp1 = this.getContentPane();
		cp1.setLayout(new BorderLayout());
		((JPanel)cp1).setOpaque(false);
		getContentPane().setLayout(null);
		
		L1[0] = new JLabel("敵人生命值");
		L1[1] = new JLabel("我方生命值");

		L1[0].setFont(F2);
		L1[1].setFont(F2);
		nextTurn.setFont(F2);
		
		L1[0].setBounds(0, 40, 200, 40);
		L1[1].setBounds(0, 440, 200, 40);
		nextTurn.setBounds(635,448,140,100);
		
		L_VS.setBounds(475, 175, 200, 70);
		L_VS.setFont(F4);
		this.add(L_VS);
		this.add(nextTurn);
		nextTurn.addActionListener(this);
		
		this.add(L1[0]);
		this.add(L1[1]);
		setLife();
		for(int i = 0; i<3; i++){
			setUsertable(i);
		}

		for(int i = 0; i<3; i++){
			setAnemytable(i);
		}
		
		for(int i = 0; i<5; i++){
			setUsercards(i);
		}
		
		repaint();
	}
	
	//設定主選單按鈕
	public void setB1(){
		B1[0] = new JButton("戰鬥開始");
		B1[1] = new JButton("離開遊戲");
		
		for(int i = 0; i<2; i++){
			B1[i].setForeground(Color.WHITE);
			B1[i].setFont(F5);
			B1[i].setOpaque(false);
			B1[i].setBorder(null);
			B1[i].setContentAreaFilled(false);
		}
		
		B1[0].setBounds(280, 225, 250, 70);
		B1[1].setBounds(280, 335, 250, 70);
	}
	
	//設定敵方牌面
	public void setAnemytable(int i){
		B3[i] = new JButton("");
		if(i == 0){
			B3[i].setBounds(350, 5, 130, 170);
		}
		else if(i == 1){
			B3[i].setBounds(475, 5, 130, 170);
		}
		else if(i == 2){
			B3[i].setBounds(600, 5, 130, 170);
		}
		
		B3[i].setBorder(Line1);
		B3[i].addActionListener(this);
		B3[i].setFont(F1);
		B3[i].setOpaque(true);
		B3[i].setContentAreaFilled(false);
		this.add(B3[i]);
		this.repaint();
	}
	
	//設定玩家牌面
	public void setUsertable(int i){
		B4[i] = new JButton("");
		if(i == 0){
			B4[i].setBounds(350, 240, 130, 170);
		}
		else if(i == 1){
			B4[i].setBounds(475, 240, 130, 170);
		}
		else if(i == 2){
			B4[i].setBounds(600, 240, 130, 170);
		}
		
		B4[i].setBorder(Line1);
		B4[i].setFont(F1);
		B4[i].setOpaque(true);
		B4[i].setContentAreaFilled(false);
		this.add(B4[i]);
		this.repaint();
	}
	
	//設定玩家手牌
	public void setUsercards(int i){
		UserCardNumber[i] = (int)(Math.random()*35);
		usercard[i] = new ImageIcon(data.Roles[UserCardNumber[i]].getImage());
		usercard[i].setImage(usercard[i].getImage().getScaledInstance(100,130,Image.SCALE_DEFAULT));
		B5[i] = new JButton(usercard[i]);
		
		if(i==0){
			B5[i].setBounds(150, 430, 100, 130);
		}
		else if(i==1){
			B5[i].setBounds(245, 430, 100, 130);
		}
		else if(i==2){
			B5[i].setBounds(340, 430, 100, 130);
		}
		else if(i==3){
			B5[i].setBounds(435, 430, 100, 130);
		}
		else if(i==4){
			B5[i].setBounds(530, 430, 100, 130);
		}
		
		B5[i].setBorder(Line1);
		B5[i].setOpaque(true);
		B5[i].setContentAreaFilled(false);
		this.add(B5[i]);
		B5[i].addActionListener(this);
		this.repaint();
		this.setVisible(true);
	}
	
	//設定選取手牌後的功能
	public void cardfunction(int i){
		P1.removeAll();
		
		B6[0] = new JButton("召換");
		B6[1] = new JButton("資訊");
		B6[2] = new JButton("返回");
		
		if(UserCardButtonType == 0){
			P1.setLayout(new GridLayout(3,1));
			for(int j = 0; j<3; j++){
				B6[j].addActionListener(this);
				P1.add(B6[j]);
			}
		}
		else if(UserCardButtonType == 1){
			P1.setLayout(new GridLayout(2,1));
			for(int j = 1; j<3; j++){
				B6[j].addActionListener(this);
				P1.add(B6[j]);
			}
		}
		
		P1.setBorder(Line1);
		P1.setOpaque(true);
		
		if(i == 0){
			P1.setBounds(150, 430, 100, 130);
		}
		else if(i == 1){
			P1.setBounds(245, 430, 100, 130);
		}
		else if(i == 2){
			P1.setBounds(340, 430, 100, 130);
		}
		else if(i == 3){
			P1.setBounds(435, 430, 100, 130);
		}
		else if(i == 4){
			P1.setBounds(530, 430, 100, 130);
		}
		this.add(P1);
		this.repaint();
		this.setVisible(true);
		UserCardIndex = i;
	}
	
	//玩家從手牌召喚武將到牌面上
	public void changeUsertable(){
		for(int i = 0; i<3;i++){
			if(!battle.getUserRoleExist(i)){
				battle.setUserRole(i, UserCardNumber[UserCardIndex]);
				this.remove(B4[i]);
				B4[i] = new JButton(usercard[UserCardIndex]);
				B4[i].setBorder(Line1);
				B4[i].setOpaque(true);
				B4[i].setContentAreaFilled(false);
				
				if(i == 0){
					B4[i].setBounds(350, 240, 130, 170);
					B4[i].addActionListener(this);
					this.add(B4[i]);
				}
				else if(i == 1){
					B4[i].setBounds(475, 240, 130, 170);
					B4[i].addActionListener(this);
					this.add(B4[i]);
				}
				else if(i == 2){
					B4[i].setBounds(600, 240, 130, 170);
					B4[i].addActionListener(this);
					this.add(B4[i]);
				}
				this.repaint();
				this.setVisible(true);
				break;
			}
		}
	}
	
	//玩家選取牌面上武將所執行的功能
	public void Usertablefunction(int i){
		P2.removeAll();
		B7[0] = new JButton("攻擊");
		B7[1] = new JButton("資訊");
		B7[2] = new JButton("待命");
		
		if(UserTableButtonType == 0){
			P2.setLayout(new GridLayout(3,1));
			for(int j = 0; j<3; j++){
				B7[j].setFont(F1);
				B7[j].addActionListener(this);
				P2.add(B7[j]);
				battle.setUserUsingIndex(i);
			}
		}
		else if(UserTableButtonType == 1){
			P2.setLayout(new GridLayout(2,1));
			for(int j = 1; j<3; j++){
				B7[j].setFont(F1);
				B7[j].addActionListener(this);
				P2.add(B7[j]);
			}
		}
		
		P2.setBorder(Line1);
		P2.setOpaque(true);
		if(i == 0){
			P2.setBounds(350, 240, 130, 170);
		}
		else if(i == 1){
			P2.setBounds(475, 240, 130, 170);
		}
		else if(i == 2){
			P2.setBounds(600, 240, 130, 170);
		}
		this.add(P2);
		this.repaint();
		this.setVisible(true);
		
		battle.setUserUsingIndex(i);
	}
	
	//設定敵方所召喚的武將
	public void setEnemyCard(){
		for(int i = 0; i<3; i++){
			if(!battle.getEnemyRoleExist(i)){
				battle.setEnemyRole(i);
				this.remove(B3[i]);
				ImageIcon change = data.Roles[battle.getEnemyRole(i)];
				change.setImage(change.getImage().getScaledInstance(100,130,Image.SCALE_DEFAULT));
				B3[i] = new JButton(change);
				
				if(i == 0){
					B3[i].setBounds(350, 5, 130, 170);
				}
				else if(i == 1){
					B3[i].setBounds(475, 5, 130, 170);
				}
				else if(i == 2){
					B3[i].setBounds(600, 5, 130, 170);
				}
				EnemyButtonType = 1;
				B3[i].setBorder(Line1);
				B3[i].addActionListener(this);
				B3[i].setFont(F1);
				B3[i].setOpaque(true);
				B3[i].setContentAreaFilled(false);
				this.add(B3[i]);
				this.repaint();
				break;
			}
		}	
	}
	
	//確認牌面上是否有卡片
	public void CheckCard(){
		for(int i = 0; i<3; i++){
			if(!battle.getEnemyRoleExist(i)){
				this.remove(B3[i]);
				setAnemytable(i);
			}
			if(!battle.getUserRoleExist(i)){
				this.remove(B4[i]);
				setUsertable(i);
			}
		}
		
		this.repaint();
		this.setVisible(true);
	}
	
	//設定血量低於一定值時，音樂會改變
	public void changeMusic(){
		if(battle.getUserLife() < 5000 && musicswitch == 0){
			data.Music[1].stop();
			data.Music[2].stop();
			data.Music[3].loop();
			musicswitch = 1;
		}
		
		if(battle.getEnemyLife() < 5000 && musicswitch == 0){
			data.Music[1].stop();
			data.Music[3].stop();
			data.Music[2].loop();
			musicswitch = 1;
		}
	}
	
	//設定按下"資訊"鈕後，所顯示的武將資訊
	public void Introduction(int i){
		frame.setLocationRelativeTo(null);
		frame.setSize(580,500);
		frame.setVisible(true);
		frame.remove(img);
		frame.remove(attack);
		frame.remove(scroll);
		
		
		attack = new JLabel("攻擊力：" + battle.getPow(i));
		
		ImageIcon transfer = new ImageIcon(data.Roles[i].getImage());
		transfer.setImage(transfer.getImage().getScaledInstance(250,400,Image.SCALE_DEFAULT));
		img = new JLabel(transfer);
		
		JTextArea introductArea = new JTextArea(data.Introdution[i]);
		attack.setFont(F2);
		attack.setBackground(Color.GRAY);
		introductArea.setFont(F6);
		introductArea.setBackground(Color.GRAY);
		introductArea.setLineWrap(true);
		introductArea.setWrapStyleWord(true);
		introductArea.setEditable(false);
		scroll = new JScrollPane(introductArea);
		frame.add(attack, BorderLayout.SOUTH);
		frame.add(img, BorderLayout.WEST);
		frame.add(scroll, BorderLayout.CENTER);
		frame.repaint();
		frame.setVisible(true);
	}
	
	public void actionPerformed(ActionEvent e){
		
		//按下主選單的按鈕
		if(e.getSource() == B1[0]){
			for(int i = 0; i<2; i++){
				remove(B1[i]);
			}
			this.getLayeredPane().remove(back1);
			illustration();
		}
		
		if(e.getSource() == B1[1]){
			System.exit(0);
		}
		
		//按下說明畫面的按鈕
		if(e.getSource() == B2){
			remove(B2);
			this.getLayeredPane().remove(back2);
			Battle();
			data.Music[0].stop();
			data.Music[1].loop();
		}
		
		//按下敵方牌面的按鈕
		for(int i = 0; i<3; i++){
			if(e.getSource() == B3[i]){
				if(EnemyButtonType == 2){
					battle.UserBattle(i);
					this.remove(L1[2]);
					this.remove(L1[3]);
					
					for(int j = 0; j<3; j++){
						this.remove(P2);
						B4[j].setVisible(true);
					}
					
					setLife();
					EnemyButtonType = 0;
					CheckCard();
					changeMusic();
				}
			}
		}
		
		//按下玩家手牌的按鈕
		for(int i = 0; i<5; i++){
			if(e.getSource() == B5[i]){
				for(int j = 0; j<5; j++){
					if(j != i){
						B5[j].setVisible(true);
						this.remove(P1);
					}
				}
		
				B5[i].setVisible(false);
				cardfunction(i);
			}
		}
		
		//按下玩家手牌的功能鍵
		if(e.getSource() == B6[0]){
			if(battle.getUserIndex()<3){
				changeUsertable();
				this.remove(B5[UserCardIndex]);
				setUsercards(UserCardIndex);
				for(int i = 0; i<5; i++){
					B5[i].setVisible(true);
					this.remove(P1);
				}
				UserCardButtonType = 1;
				this.remove(P1);
				cardfunction(UserCardIndex);
			}
		}
		else if(e.getSource() == B6[1]){
			Introduction(UserCardNumber[UserCardIndex]);
			for(int i = 0; i<5; i++){
				B5[i].setVisible(true);
				this.remove(P1);
			}
		}
		else if(e.getSource() == B6[2]){
			for(int i = 0; i<5; i++){
				B5[i].setVisible(true);
				this.remove(P1);
			}
		}
		
		//按下玩家牌面的按鈕
		for (int i = 0; i<3; i++){
			if(e.getSource() == B4[i]){
				for(int j = 0; j<3; j++){
					if(j != i){
						B4[j].setVisible(true);
						this.remove(P2);
					}
					B4[i].setVisible(false);
				}
				Usertablefunction(i);
			}
		}
		
		//按下玩家牌面的功能鍵
		if(e.getSource() == B7[0]){
			EnemyButtonType = 2;
			UserTableButtonType = 1;
			this.remove(P2);
			for(int i = 0; i<3; i++){
				B4[i].repaint();
				B4[i].setVisible(true);
			}
			CheckCard();
		}
		else if(e.getSource() == B7[1]){
			Introduction(battle.getUserRole());
		}
		else if(e.getSource() == B7[2]){
			for(int i = 0; i<3; i++){
				B4[i].setVisible(true);
				this.remove(P2);
			}
		}
		
		//按下"回合結束紐"
		if(e.getSource() == nextTurn){
			if(nextturntype ==0){
				EnemyButtonType = 0;
				UserCardButtonType = 0;
				UserTableButtonType = 0;
				setEnemyCard();
				battle.AnemyBattle();
				CheckCard();
				this.remove(L1[2]);
				this.remove(L1[3]);
				setLife();
				changeMusic();
				
				for(int i = 0; i<3; i++){
					B4[i].setVisible(true);
					this.remove(P2);
				}
				
				for(int i = 0; i<5; i++){
					B5[i].setVisible(true);
					this.remove(P1);
				}
			}
			}
		
		//按下"獲勝"或"戰敗"紐
		if(e.getSource() == end){
			System.exit(0);
		}
	}
}
