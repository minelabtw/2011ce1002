package fp.s100502017;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class GameFrame extends JFrame implements ActionListener{

	Game La;
	JPanel p=new JPanel();
	JMenuBar mb=new JMenuBar();
	JMenu file=new JMenu("�ɮ�");
	JMenuItem NewGame=new JMenuItem("�s�C��");
	JMenuItem Exit=new JMenuItem("���}");
	public GameFrame(int level,int userIndex){
		La=new Game(level,userIndex);
		p.add(La);
		add(p,BorderLayout.CENTER);
		p.setFocusable(true);
		this.setJMenuBar(mb);
		mb.add(file);
		file.add(NewGame);
		file.add(Exit);
		this.pack();
		NewGame.addActionListener(this);
		Exit.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e) {//JMemu event is here
		if(e.getSource()==NewGame){
			La.Replay();
			repaint();
		}
		if(e.getSource()==Exit){
			this.setVisible(false);
		}
	}
	
}
