package fp.s100502017;
import java.util.Scanner;
import java.io.*;
import javax.swing.*;
import java.awt.*;
public class IOmanager {
	File save;
	Scanner input;
	public int userNum=0;
	public boolean empty=true;
	protected String[] UserName=new String[100];
	protected int[] EasyTime=new int[100];
	protected int[] EasyWin=new int[100];
	protected int[] EasyAvg=new int[100];
	protected int[] EasyBest=new int[100];
	protected int[] NormalTime=new int[100];
	protected int[] NormalWin=new int[100];
	protected int[] NormalAvg=new int[100];
	protected int[] NormalBest=new int[100];
	protected int[] HardTime=new int[100];
	protected int[] HardWin=new int[100];
	protected int[] HardAvg=new int[100];
	protected int[] HardBest=new int[100];
	public IOmanager(){//read a file
		for(int i=0;i<100;i++){
			UserName[i]="";
		}
		try{
			save=new File("save.txt");
			input=new Scanner(save);
		}
		catch(FileNotFoundException fileNotFoundException){
			System.err.println("Can't find "+save.getAbsolutePath());
			System.exit(0);
		}
		for(int i=0;i<UserName.length&&input.hasNext();i++){
			empty=false;
			UserName[i]=input.next();
			EasyTime[i]=input.nextInt();
			EasyWin[i]=input.nextInt();
			EasyAvg[i]=input.nextInt();
			EasyBest[i]=input.nextInt();
			NormalTime[i]=input.nextInt();
			NormalWin[i]=input.nextInt();
			NormalAvg[i]=input.nextInt();
			NormalBest[i]=input.nextInt();
			HardTime[i]=input.nextInt();
			HardWin[i]=input.nextInt();
			HardAvg[i]=input.nextInt();
			HardBest[i]=input.nextInt();
			userNum++;
		}
		input.close();
	}
	public void setNewUser(String name){//to set a new user
		if(UserName[99]==""){
			int index=-1;
			for(int i=0;i<100;i++){
				if(UserName[i]==""){
					UserName[i]=name;
					EasyTime[i]=0;
					EasyWin[i]=0;
					EasyAvg[i]=0;
					EasyBest[i]=999;
					NormalTime[i]=0;
					NormalWin[i]=0;
					NormalAvg[i]=0;
					NormalBest[i]=999;
					HardTime[i]=0;
					HardWin[i]=0;
					HardAvg[i]=0;
					HardBest[i]=999;
					break;
				}
			}
		}
	}
	public void OutputFile() throws Exception{//write file
		PrintWriter output=new PrintWriter(save);
		for(int i=0;i<100;i++){
			if(UserName[i]!=""){
				output.println(UserName[i]+" "+EasyTime[i]+" "+EasyWin[i]+" "+EasyAvg[i]+" "+EasyBest[i]+" "+NormalTime[i]+" "+NormalWin[i]+" "+NormalAvg[i]+" "+NormalBest[i]+" "+HardTime[i]+" "+HardWin[i]+" "+HardAvg[i]+" "+HardBest[i]);
			}
			else
				break;
		}
		output.close();

	}
	
}
