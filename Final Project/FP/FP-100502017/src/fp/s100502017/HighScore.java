package fp.s100502017;

import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.*;

public class HighScore extends JFrame{
	IOmanager sav=new IOmanager();
	JPanel screen=new JPanel();
	JLabel[] lab=new JLabel[21];
	public int ind=0;
	public HighScore(){
		
		String[] list=new String[sav.userNum];
		for(int i=0;i<sav.userNum;i++){
			list[i]=sav.UserName[i];
		}
		final JComboBox cb=new JComboBox(list);
		screen.setLayout(new GridLayout(3,7,5,5));
		for(int j=0;j<21;j++){
			lab[j]=new JLabel();
			screen.add(lab[j]);
		}
		add(cb,BorderLayout.NORTH);
		add(screen,BorderLayout.CENTER);
		setDisplay(ind);
		cb.addItemListener(new ItemListener(){//item event is here
			public void itemStateChanged(ItemEvent e){
				setDisplay(cb.getSelectedIndex());
			}
		});
	}
	public void setDisplay(int i){//let user see record
		lab[0].setText("");
		lab[7].setText("い");
		lab[14].setText("蔼");
		lab[1].setText("Θ瞯");
		lab[8].setText("Θ瞯");
		lab[15].setText("Θ瞯");
		lab[3].setText("キА计");
		lab[4].setText(""+sav.EasyAvg[i]);
		lab[5].setText("程ㄎ计");
		lab[6].setText(""+sav.EasyBest[i]);
		lab[10].setText("キА计");
		lab[11].setText(""+sav.NormalAvg[i]);
		lab[12].setText("程ㄎ计");
		lab[13].setText(""+sav.NormalBest[i]);
		lab[17].setText("キА计");
		lab[18].setText(""+sav.HardAvg[i]);
		lab[19].setText("程ㄎ计");
		lab[20].setText(""+sav.HardBest[i]);
		if(sav.EasyTime[i]!=0){
			lab[2].setText(""+(int)(100*sav.EasyWin[i]/(double)(sav.EasyTime[i]))+"%");
		}
		else{
			lab[2].setText("0%");
		}
		if(sav.NormalTime[i]!=0){
			lab[9].setText(""+(int)(100*sav.NormalWin[i]/(double)(sav.NormalTime[i]))+"%");
		}
		else{
			lab[9].setText("0%");
		}
		if(sav.HardTime[i]!=0){
			lab[16].setText(""+(int)(100*sav.HardWin[i]/(double)(sav.HardTime[i]))+"%");
		}
		else{
			lab[16].setText("0%");
		}
	}
}
