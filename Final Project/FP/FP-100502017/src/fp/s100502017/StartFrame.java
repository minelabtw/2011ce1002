package fp.s100502017;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class StartFrame extends JFrame implements ActionListener{
	JButton CreateUser=new JButton();
	JButton StartButton=new JButton();
	JButton High=new JButton();
	JLabel title=new JLabel();
	JLabel picture=new JLabel();
	JPanel pan=new JPanel();

	public StartFrame(){
		pan.setLayout(null);
		pan.add(CreateUser);
		pan.add(StartButton);
		pan.add(High);
		pan.add(title);
		pan.add(picture);
		pan.setBackground(Color.BLACK);
		CreateUser.setBounds(380,200,140,58);
		StartButton.setBounds(380,350,140,58);
		High.setBounds(380,500,140,58);
		title.setBounds(75,8,464,125);
		picture.setBounds(0, 100, 313, 495);
		CreateUser.setIcon(new ImageIcon("material/create.jpg"));
		StartButton.setIcon(new ImageIcon("material/game.jpg"));
		High.setIcon(new ImageIcon("material/high.jpg"));
		picture.setIcon(new ImageIcon("material/Main.jpg"));
		title.setIcon(new ImageIcon("material/title.jpg"));
		add(pan,BorderLayout.CENTER);
		CreateUser.addActionListener(this);
		StartButton.addActionListener(this);
		High.addActionListener(this);
	}


	public void actionPerformed(ActionEvent e) {//three button event
		IOmanager sav=new IOmanager();
		if(e.getSource()==CreateUser){
			String s="";
			boolean CheckSpace=false;
			while(!CheckSpace){
				s=JOptionPane.showInputDialog("建立新使用者，請輸入你的名字");
				if(s!=""){
					for(int i=0;i<s.length();i++){
						if(s.charAt(i)==' '){
							CheckSpace=true;
						}
					}
					if(CheckSpace){
						JOptionPane.showMessageDialog(null, "名字不能有空格，請重新輸入", "error",JOptionPane.ERROR_MESSAGE);
						CheckSpace=false;
					}
					else
						CheckSpace=true;
				}
			}
			sav.setNewUser(s);
			try {
				sav.OutputFile();
			} catch (Exception e1) {
				System.err.print("error");
				System.exit(0);
			}
		}
		else if(e.getSource()==StartButton){
			IOmanager creat=new IOmanager();
			if(creat.empty){
				String s="";
				boolean CheckSpace=false;
				while(!CheckSpace){
					s=JOptionPane.showInputDialog("建立新使用者，請輸入你的名字");
					if(s!=""&&s!=null){
						for(int i=0;i<s.length();i++){
							if(s.charAt(i)==' '){
								CheckSpace=true;
							}
						}
						if(CheckSpace){
							JOptionPane.showMessageDialog(null, "名字不能有空格，請重新輸入", "error",JOptionPane.ERROR_MESSAGE);
							CheckSpace=false;
						}
						else
							CheckSpace=true;
					}
				}
				creat.setNewUser(s);
				try {
					creat.OutputFile();
				} catch (Exception e1) {
					System.err.print("error");
					System.exit(0);
				}
			}
			Object nameOb=JOptionPane.showInputDialog(null,"請選擇您的身分","使用者",JOptionPane.QUESTION_MESSAGE,null,sav.UserName,0);
			String name=(String)(nameOb);
			int nameIndex=0;
			for(int i=0;i<100;i++){
				if(name==sav.UserName[i]){
					nameIndex=i;
					break;
				}
			}
			Object levelOb=JOptionPane.showInputDialog(null,"請選擇遊戲難度","難度選擇",JOptionPane.QUESTION_MESSAGE,null,new Object[]{"初級","中級","高級"},0);
			String level=(String)(levelOb);
			if(level=="初級"){
				GameFrame f = new GameFrame(1,nameIndex);
				f.setTitle("六方雷神"); 
				f.setSize(700,750);   
				f.setResizable(false); 
				f.setLocationRelativeTo(f);
				f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				f.setVisible(true);
			}
			else if(level=="中級"){
				GameFrame f = new GameFrame(2,nameIndex);
				f.setTitle("六方雷神"); 
				f.setSize(700,750);   
				f.setResizable(false); 
				f.setLocationRelativeTo(f);
				f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				f.setVisible(true);
			}
			else{
				GameFrame f = new GameFrame(3,nameIndex);
				f.setTitle("六方雷神"); 
				f.setSize(700,750);   
				f.setResizable(false); 
				f.setLocationRelativeTo(f);
				f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				f.setVisible(true);
			}
		}
		else if(e.getSource()==High){
			IOmanager creat=new IOmanager();
			if(creat.empty){
				String s="";
				boolean CheckSpace=false;
				while(!CheckSpace){
					s=JOptionPane.showInputDialog("建立新使用者，請輸入你的名字");
					if(s!=""&&s!=null){
						for(int i=0;i<s.length();i++){
							if(s.charAt(i)==' '){
								CheckSpace=true;
							}
						}
						if(CheckSpace){
							JOptionPane.showMessageDialog(null, "名字不能有空格，請重新輸入", "error",JOptionPane.ERROR_MESSAGE);
							CheckSpace=false;
						}
						else
							CheckSpace=true;
					}
				}
				creat.setNewUser(s);
				try {
					creat.OutputFile();
				} catch (Exception e1) {
					System.err.print("error");
					System.exit(0);
				}
			}
			HighScore h = new HighScore();
			h.setTitle("高分榜"); 
			h.setSize(450,200);   
			h.setResizable(false); 
			h.setLocationRelativeTo(null);
			h.setVisible(true);
		}
	}
}
