package fp.s100502017;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class Game extends JLabel implements ActionListener{
	IOmanager sav=new IOmanager();
	JButton help=new JButton();
	private int user,levelNum;
	Border b=new LineBorder(Color.GRAY);
	final double sqrt3 =Math.sqrt(3);
	protected int size,total,remain,now_X,now_Y;
	protected double unit;
	Point ini;
	Point [][] poi;
	GameChecker[][] Block;
	protected boolean original=true;
	protected boolean start=false;
	private boolean Win=false;
	private boolean Lose=false;
	private boolean LoseLook=false;
	private boolean helpPower=true;
	Font bigfont;
	private ImageIcon backIcon=new ImageIcon("material/back.JPG");
	private ImageIcon flagIcon=new ImageIcon("material/flag.JPG");
	private ImageIcon loseIcon=new ImageIcon("material/lose.JPG");
	private ImageIcon winIcon=new ImageIcon("material/win.JPG");
	private ImageIcon boomIcon=new ImageIcon("material/boom.JPG");
	private ImageIcon Time0Icon=new ImageIcon("material/00.jpg");
	private ImageIcon Time1Icon=new ImageIcon("material/01.jpg");
	private ImageIcon Time2Icon=new ImageIcon("material/02.jpg");
	private ImageIcon Time3Icon=new ImageIcon("material/03.jpg");
	private ImageIcon Time4Icon=new ImageIcon("material/04.jpg");
	private ImageIcon Time5Icon=new ImageIcon("material/05.jpg");
	private ImageIcon Time6Icon=new ImageIcon("material/06.jpg");
	private ImageIcon Time7Icon=new ImageIcon("material/07.jpg");
	private ImageIcon Time8Icon=new ImageIcon("material/08.jpg");
	private ImageIcon Time9Icon=new ImageIcon("material/09.jpg");
	private Image back=backIcon.getImage();
	private Image flag=flagIcon.getImage();
	private Image lose=loseIcon.getImage();
	private Image win=winIcon.getImage();
	private Image boom=boomIcon.getImage();
	private Image[] time=new Image[10];
	
	public Timer t=new Timer(1000,new TimerListener());
	private int second=0;
	
	
	public Game(int level,int userIndex){
		if(level==1){//according to game level decide size(block number) and unit length
			size=7;
			unit=13;
		}
		else if(level==2){
			size=9;
			unit=11;
		}
		else{
			size=12;
			unit=8;
		}
		user=userIndex;
		levelNum=level;
		total=(int)((((size*2-1)*(size*2-1)-Sn(size-1)))/7);//decide boom number
		this.add(help);
		help.setBounds(0, 610, 98, 98);
		help.setIcon(new ImageIcon("material/help.jpg"));
		bigfont=new Font("Bookman Old Style",Font.BOLD,16);
		this.setFont(bigfont);
		time[0]=Time0Icon.getImage();
		time[1]=Time1Icon.getImage();
		time[2]=Time2Icon.getImage();
		time[3]=Time3Icon.getImage();
		time[4]=Time4Icon.getImage();
		time[5]=Time5Icon.getImage();
		time[6]=Time6Icon.getImage();
		time[7]=Time7Icon.getImage();
		time[8]=Time8Icon.getImage();
		time[9]=Time9Icon.getImage();
		help.addActionListener(this);
		addMouseMotionListener(M);
		addMouseListener(m);
	}
	public Point[][] PointProduce(int s,double u){//decide block center coordinate
		double X_Ini=this.getWidth()/2;
		double Y_Ini=this.getHeight()/2-s*2*u*sqrt3-10;
		Point[][] p=new Point[s*2-1][s*2-1];
		for(int i=1;i<s*2;i++){
			for(int j=1;j<s*2;j++){
				if(Math.abs(i-j)<s)//rule
					p[i-1][j-1]=new Point(X_Ini+3*u*(i-1)-3*u*(j-1),Y_Ini+u*sqrt3*i+u*sqrt3*j);
				else
					p[i-1][j-1]=new Point();
			}
		}
		return p;
	}
	
	public int Sn(int n){//1+2+...+n
		int ans=0;
		for(int i=1;i<=n;i++){
			ans+=i;
		}
		return ans;
	}
	
	public Polygon six(int x,int y){//paint six lines
		Polygon t1=new Polygon();
		t1.addPoint((int)(x+unit),(int)(y-unit*sqrt3));
		t1.addPoint((int)(x+2*unit),y);
		t1.addPoint((int)(x+unit),(int)(y+unit*sqrt3));
		t1.addPoint((int)(x-unit),(int)(y+unit*sqrt3));
		t1.addPoint((int)(x-2*unit),y);
		t1.addPoint((int)(x-unit),(int)(y-unit*sqrt3));
		return t1;
	}
	
	public Polygon fillsix(int x,int y,int u){//paint fill sex polygon
		Polygon t2=new Polygon();
		t2.addPoint((int)(x+u),(int)(y-u*sqrt3));
		t2.addPoint((int)(x+2*u),y);
		t2.addPoint((int)(x+u),(int)(y+u*sqrt3));
		t2.addPoint((int)(x-u),(int)(y+u*sqrt3));
		t2.addPoint((int)(x-2*u),y);
		t2.addPoint((int)(x-u),(int)(y-u*sqrt3));
		return t2;
	}
	
	public boolean ValidCheck(int x,int y){//check block whether valid
		if(x<(size*2-1)&&x>=0&&y<(size*2-1)&&y>=0&&Math.abs(x-y)<size)
			return true;
		else
			return false;
	}
	
	public void RandomBoom(){//random boom site
		int x,y;
		for(int i=0;i<total;i++){
			while(true){
				x=(int)(Math.random()*(size*2-1));
				y=(int)(Math.random()*(size*2-1));
				if(ValidCheck(x,y)&&!Block[x][y].getBoom()){
					Block[x][y].setBoom(true);
					
					break;
				}
				
			}
		}
	}
	public void CalculateBoomNum(){//block surround boom number
		int count;
		for(int i=0;i<size*2-1;i++){
			for(int j=0;j<size*2-1;j++){
				count=0;
				if(Block[i][j].getValid()&&!Block[i][j].getBoom()){
					if(ValidCheck(i-1,j-1))
						if(Block[i-1][j-1].getBoom())
							count++;
					if(ValidCheck(i+1,j+1))
						if(Block[i+1][j+1].getBoom())
							count++;
					if(ValidCheck(i,j+1))
						if(Block[i][j+1].getBoom())
							count++;
					if(ValidCheck(i,j-1))
						if(Block[i][j-1].getBoom())
							count++;
					if(ValidCheck(i+1,j))
						if(Block[i+1][j].getBoom())
							count++;
					if(ValidCheck(i-1,j))
						if(Block[i-1][j].getBoom())
							count++;
					Block[i][j].setNum(count);
				}
			}
		}
	}
	public void ZeroOpen(int i,int j){//if surround zero make surround open
		Block[i][j].LetVisible();
		if(ValidCheck(i-1,j-1))
			if(!Block[i-1][j-1].getVisible())
				if(Block[i-1][j-1].getNum()>0)
					Block[i-1][j-1].LetVisible();
				else 
					ZeroOpen(i-1,j-1);
		if(ValidCheck(i+1,j+1))
			if(!Block[i+1][j+1].getVisible())
				if(Block[i+1][j+1].getNum()>0)
					Block[i+1][j+1].LetVisible();
				else 
					ZeroOpen(i+1,j+1);
		if(ValidCheck(i,j-1))
			if(!Block[i][j-1].getVisible())
				if(Block[i][j-1].getNum()>0)
					Block[i][j-1].LetVisible();
				else 
					ZeroOpen(i,j-1);
		if(ValidCheck(i-1,j))
			if(!Block[i-1][j].getVisible())
				if(Block[i-1][j].getNum()>0)
					Block[i-1][j].LetVisible();
				else 
					ZeroOpen(i-1,j);
		if(ValidCheck(i+1,j))
			if(!Block[i+1][j].getVisible())
				if(Block[i+1][j].getNum()>0)
					Block[i+1][j].LetVisible();
				else 
					ZeroOpen(i+1,j);
		if(ValidCheck(i,j+1))
			if(!Block[i][j+1].getVisible())
				if(Block[i][j+1].getNum()>0)
					Block[i][j+1].LetVisible();
				else 
					ZeroOpen(i,j+1);
	}
	public int CalculateRemainNum(){//Calculate remain boom number
		int remain=0;
		for(int i=0;i<size*2-1;i++){
			for(int j=0;j<size*2-1;j++){
				if(ValidCheck(i,j)){
					if(!Block[i][j].getVisible()){
						remain++;
					}
				}
			}
		}
		return remain;
	}
	public void Replay(){//new game
		original=true;
	}

	public void initialization(){//new game setting
		t.stop();
		start=false;
		second=0;
		Win=false;
		Lose=false;
		LoseLook=false;
		helpPower=true;
		remain=total;
		Point [][] poi=new Point [size*2-1][size*2-1];
		poi=PointProduce(size,unit);
		Block=new GameChecker[size*2-1][size*2-1];
		ini=new Point(poi[size-1][size-1].x,poi[size-1][size-1].y);
		
		for(int i=0;i<size*2-1;i++){
			for(int j=0;j<size*2-1;j++){
				if(poi[i][j].getValid())
					Block[i][j]=new GameChecker(i,j,poi[i][j]);
				else
					Block[i][j]=new GameChecker();
			}
		}
		RandomBoom();
		CalculateBoomNum();
	}
	public void RenewSave() throws Exception{//let file renew
		sav.OutputFile();
	}
	public MouseListener m=new MouseListener(){//mouse click event is here
		public void mouseClicked(MouseEvent e) {
		}
		public void mouseEntered(MouseEvent e) {			
		}
		public void mouseExited(MouseEvent e) {			
		}
		public void mousePressed(MouseEvent e) {			
		}
		public void mouseReleased(MouseEvent e) {
			if(!Lose&&!Win){
				if(e.getButton()==1){
					if(!start){//that timer start and record that user has played
						if(levelNum==1){
							sav.EasyTime[user]++;
						}
						else if(levelNum==2){
							sav.NormalTime[user]++;
						}
						else{
							sav.HardTime[user]++;
						}
						try {
							sav.OutputFile();
						} catch (Exception e1) {
							e1.printStackTrace();
						}
						start=true;
						t.start();
					}
					for(int i=0;i<size*2-1;i++){//open block is here
						for(int j=0;j<size*2-1;j++){
							if(Block[i][j].getValid()&&Block[i][j].now&&!Block[i][j].getMark()){
								if(Block[i][j].getBoom()){//Lose
									Lose=true;
									t.stop();
								}
								else if(Block[i][j].getNum()>0){
									Block[i][j].LetVisible();
								}
								else{
									ZeroOpen(i,j);
								}
							}
						}
					}
				}
				if(e.getButton()==3){//make flag on block
					for(int i=0;i<size*2-1;i++){
						for(int j=0;j<size*2-1;j++){
							if(Block[i][j].getValid()&&Block[i][j].now&&!Block[i][j].getVisible()&&!Block[i][j].getMark()){
								Block[i][j].setMark(true);
								remain--;
								}
							else if(Block[i][j].getValid()&&Block[i][j].now&&!Block[i][j].getVisible()&&Block[i][j].getMark()){
								Block[i][j].setMark(false);
								remain++;
							}
						}
					}
				}
				if(!Lose&&CalculateRemainNum()==total){//Win
					Win=true;
					t.stop();
					if(levelNum==1){
						sav.EasyAvg[user]=(int)((sav.EasyWin[user]*sav.EasyAvg[user]+second)/(double)(sav.EasyWin[user]+1));
						sav.EasyWin[user]++;
						if(second<sav.EasyBest[user])
							sav.EasyBest[user]=second;
					}
					else if(levelNum==2){
						sav.NormalAvg[user]=(int)((sav.NormalWin[user]*sav.NormalAvg[user]+second)/(double)(sav.NormalWin[user]+1));
						sav.NormalWin[user]++;
						if(second<sav.NormalBest[user])
							sav.NormalBest[user]=second;
					}
					else{
						sav.HardWin[user]++;
						sav.HardAvg[user]=(int)((sav.HardWin[user]*sav.HardAvg[user]+second)/(double)(sav.HardWin[user]+1));
						sav.HardWin[user]++;
						if(second<sav.HardBest[user])
							sav.HardBest[user]=second;
					}
					try {
						sav.OutputFile();
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}
			else if(Lose==true){//let user can look fail boom(let lose picture disappear)
				LoseLook=true;
			}
		}
		
	};
	
	public MouseMotionListener M=new MouseMotionListener(){//motion event is here
		public void mouseDragged(MouseEvent e) {
			now_X=e.getX();
			now_Y=e.getY();
			for(int i=0;i<size*2-1;i++){
				for(int j=0;j<size*2-1;j++){
					if(Block[i][j].getValid()){
						if(Block[i][j].P.distance(now_X,now_Y)<unit*sqrt3-1)
							Block[i][j].now=true;
						else
							Block[i][j].now=false;
					}
				}
			}
			repaint();
		}
		public void mouseMoved(MouseEvent e) {
			now_X=e.getX();
			now_Y=e.getY();
			for(int i=0;i<size*2-1;i++){
				for(int j=0;j<size*2-1;j++){
					if(Block[i][j].getValid()){
						if(Block[i][j].P.distance(now_X,now_Y)<unit*sqrt3-1&&!Block[i][j].getVisible())
							Block[i][j].now=true;
						else
							Block[i][j].now=false;
					}
				}
			}
			repaint();
		}	
	};
	public void paintComponent(Graphics g){//paint block and picture
		super.paintComponent(g);
		if(original){
			initialization();
			original=false;
		}
		g.drawImage(back, 0, 0, this);
		g.setColor(new Color(200,220,200));
		for(int i=0;i<size*2-1;i++){//draw line
			for(int j=0;j<size*2-1;j++){
				if(Block[i][j].getValid()){
					if(!Block[i][j].now){
						g.drawPolygon(six(Block[i][j].P.x,Block[i][j].P.y));
					}
					else{
						g.setColor(new Color(251,201,0));
						g.drawPolygon(six(Block[i][j].P.x,Block[i][j].P.y));
						g.setColor(new Color(200,220,200));
						
					}
				}
			}
		}
		for(int i=0;i<size*2-1;i++){//draw block
			for(int j=0;j<size*2-1;j++){
				if(Block[i][j].getValid()){
					if(Block[i][j].getVisible()){
						if(Block[i][j].getNum()>0){
							g.setColor(new Color(255,255,255));
							g.fillPolygon(fillsix(Block[i][j].P.x,Block[i][j].P.y,(int)(unit-1)));
							if(Block[i][j].getNum()==1){
								g.setColor(new Color(0,0,255));
								g.drawString(""+1,Block[i][j].P.x-4 ,Block[i][j].P.y+4);
								g.setColor(new Color(200,220,200));
							}
							else if(Block[i][j].getNum()==2){
								g.setColor(new Color(0,128,0));
								g.drawString(""+2,Block[i][j].P.x-4 ,Block[i][j].P.y+4);
								g.setColor(new Color(200,220,200));
							}
							else if(Block[i][j].getNum()==3){
								g.setColor(new Color(255,0,0));
								g.drawString(""+3,Block[i][j].P.x-4 ,Block[i][j].P.y+4);
								g.setColor(new Color(200,220,200));
							}
							else if(Block[i][j].getNum()==4){
								g.setColor(new Color(0,0,128));
								g.drawString(""+4,Block[i][j].P.x-4 ,Block[i][j].P.y+4);
								g.setColor(new Color(200,220,200));
							}
							else if(Block[i][j].getNum()==5){
								g.setColor(new Color(128,0,0));
								g.drawString(""+5,Block[i][j].P.x-4 ,Block[i][j].P.y+4);
								g.setColor(new Color(200,220,200));
							}
							else{
								g.setColor(new Color(0,128,128));
								g.drawString(""+6,Block[i][j].P.x-4 ,Block[i][j].P.y+4);
								g.setColor(new Color(200,220,200));
							}
						}
						else{
							g.setColor(new Color(255,255,255));
							g.fillPolygon(fillsix(Block[i][j].P.x,Block[i][j].P.y,(int)(unit-1)));
							g.setColor(new Color(200,220,200));
						}
					}
					else if(Block[i][j].getBoom()&&Lose){
						g.setColor(new Color(255,255,255));
						g.fillPolygon(fillsix(Block[i][j].P.x,Block[i][j].P.y,(int)(unit-1)));
						g.setColor(new Color(200,220,200));
						g.drawImage(boom,Block[i][j].P.x-9,Block[i][j].P.y-9,this);
					}
					else if(Block[i][j].getMark()){
						g.setColor(new Color(255,255,255));
						g.fillPolygon(fillsix(Block[i][j].P.x,Block[i][j].P.y,(int)(unit-1)));
						g.setColor(new Color(200,220,200));
						g.drawImage(flag,Block[i][j].P.x-9,Block[i][j].P.y-9,this);
					}
					else if(Block[i][j].now){
						g.setColor(new Color(251,201,0));
						g.fillPolygon(fillsix(Block[i][j].P.x,Block[i][j].P.y,(int)(unit-1)));
						g.setColor(new Color(200,220,200));
					}
					else {
						g.fillPolygon(fillsix(Block[i][j].P.x,Block[i][j].P.y,(int)(unit-1)));
					}
				}
			}
		}
		for(int i=0;i<10;i++){//output now time
			if(second%10==i){
				g.drawImage(time[i],this.getWidth()-49,0,this);
			}
			if((second/10)%10==i){
				g.drawImage(time[i],this.getWidth()-49*2,0,this);
			}
			if(second/100==i){
				g.drawImage(time[i],this.getWidth()-49*3,0,this);
			}
			if(remain%10==i){
				g.drawImage(time[i],49,0,this);
			}
			if((remain/10)%10==i){
				g.drawImage(time[i],0,0,this);
			}
		}
		
		if(Lose==true){//lose picture
			if(!LoseLook){
				g.drawImage(lose,0,this.getHeight()/2-90,this);
			}
			else{
			}
		}
		else if(Win==true){//win picture
			
			g.drawImage(win,0,this.getHeight()/2-90,this);
		}
	}
	public Dimension getPreferredSize(){//set label size
		return new Dimension(700,710);
	}
	public void actionPerformed(ActionEvent e) {//the help button is here
		if(e.getSource()==help&&!Win&&!Lose&&helpPower){
			if(!start){
				if(levelNum==1){
					sav.EasyTime[user]++;
				}
				else if(levelNum==2){
					sav.NormalTime[user]++;
				}
				else{
					sav.HardTime[user]++;
				}
				try {
					sav.OutputFile();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				start=true;
				t.start();
			}
			for(int i=0;i<size*2-1&&helpPower;i++){
				for(int j=0;j<size*2-1&&helpPower;j++){
					if(ValidCheck(i,j)){
						if(!Block[i][j].getVisible()&&Block[i][j].getNum()==0){
							ZeroOpen(i,j);
							helpPower=false;
						}
					}
				}
			}
			if(helpPower){
				for(int i=0;i<size*2-1&&helpPower;i++){
					for(int j=0;j<size*2-1&&helpPower;j++){
						if(ValidCheck(i,j)){
							if(!Block[i][j].getVisible()&&Block[i][j].getNum()>0){
								Block[i][j].LetVisible();
								helpPower=false;
							}
						}
					}
				}
			}
		}
		if(!Lose&&CalculateRemainNum()==total&&!helpPower){
			Win=true;
			t.stop();
			if(levelNum==1){
				sav.EasyAvg[user]=(int)((sav.EasyWin[user]*sav.EasyAvg[user]+second)/(double)(sav.EasyWin[user]+1));
				sav.EasyWin[user]++;
				if(second<sav.EasyBest[user])
					sav.EasyBest[user]=second;
			}
			else if(levelNum==2){
				sav.NormalAvg[user]=(int)((sav.NormalWin[user]*sav.NormalAvg[user]+second)/(double)(sav.NormalWin[user]+1));
				sav.NormalWin[user]++;
				if(second<sav.NormalBest[user])
					sav.NormalBest[user]=second;
			}
			else{
				sav.HardWin[user]++;
				sav.HardAvg[user]=(int)((sav.HardWin[user]*sav.HardAvg[user]+second)/(double)(sav.HardWin[user]+1));
				sav.HardWin[user]++;
				if(second<sav.HardBest[user])
					sav.HardBest[user]=second;
			}
			try {
				sav.OutputFile();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			helpPower=true;
		}
	}
	class GameChecker{//every block's setting
		private int X,Y;
		private int num=-1;
		protected Point P;
		private boolean valid=true;
		private boolean boom,visible,marker;
		protected boolean now=false;
		
		GameChecker(){
			valid=false;
			boom=false;
		}
		
		GameChecker(int i,int j,Point p){
			X=i;
			Y=j;
			P=p;
			visible=false;
			marker=false;
			boom=false;
			
		}
		public boolean getValid(){
			return valid;
		}
		public void setBoom(boolean b){
			boom=b;
		}
		public boolean getBoom(){
			return boom;
		}
		public void setNum(int n){
			num=n;
		}
		public int getNum(){
			return num;
		}
		public void LetVisible(){
			visible=true;
		}
		public boolean getVisible(){
			return visible;
		}
		public void setMark(boolean m){
			marker=m;
		}
		public boolean getMark(){
			return marker;
		}
	}
	class TimerListener implements ActionListener{//time event is here
		public void actionPerformed(ActionEvent e){
			if(second<999)
				second++;
			else
				second=999;
			repaint();
		}
	}
}
