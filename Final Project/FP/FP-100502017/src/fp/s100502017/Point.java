package fp.s100502017;

public class Point {
	protected int x,y;
	private boolean valid=true;
	public Point(){
		valid=false;
		x=0;
		y=0;
	}
	public Point(double a,double b){
		x=(int)(a);
		y=(int)(b);
	}
	public double distance(int x1,int y1){
		return Math.sqrt((x-x1)*(x-x1)+(y-y1)*(y-y1));
	}

	public boolean getValid(){
		return valid;
	}
}
