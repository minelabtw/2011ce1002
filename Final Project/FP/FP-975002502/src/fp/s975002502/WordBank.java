package fp.s975002502;

/** Store all the words */
public class WordBank {
	public static final int CarBrands = 1;
	public static final int Animal = 2;
	
	// 1. car brand word sheet
	public String[] carbrand = {"Acura", "Audi", "BMW", "Bentley", "Buick", "Bugatti", "Cadillac", "Caterham",
			"Carver", "Chery", "Chevrolet", "Chrysler", "Citroen", "Daewoo", "Daihatsu",
			"Daimler", "Dodge",	"Eagle", "Fiat", "Ferrari", "Ford", "Formosa", "GM", "GMC",
			"Ginetta", "Holden", "Honda", "Hummer", "Hyundai", "Infiniti", "Isuzu", "Jaguar",
			"Jeep", "Kia", "Koenigsegg", "Lamborghini", "Lancia", "LDV", "Lexus", "Lincoln", 
			"Lotus ", "Luxgen", "Marcos", "Mangusta", "MG", "Maserati", "Mazda", "McLaren",
			"Micro", "Mini", "Mercury", "Mitsubishi", "Morgan", "Navistar",
			"Nissan", "Oldsmobile", "Opel", "Packard", "Perodua", "Peugeot", "Pontiac",
			"Porsche", "Proton", "Renault", "Saab", "Saturn", "Scion", "Seat", "Shelby",
			"Skoda", "Smart", "Subaru",	"Suzuki", "Toyota", "TVR", "Vauxhall", "Volkswagen", "Volvo", "Ultima"
			};
	// 2. animal word sheet
	public String[] animal = {"antelope", "beaver", "budgie", "buzzard", "cat", "crocodile", "dog", "donkey", "duck", "eagle", "elephant", "ferret", "flamingo", "giraffe", "goat", "gorilla", "kangaroo", "lion", "owl", "pig", "zibra"};
	
	// the random word 
	public String str = "TestWord";
	
	/** Construct a default WordSheet with upper case string */
	public WordBank() {
		str = str.toUpperCase();
	}
	
	/** Construct a WordSheet with upper case secret string */
	public WordBank(int type) {
		int tmp;	// random number
		
		// choose the word sheet class
		switch(type) {
			case CarBrands:
				tmp=(int)(Math.random()*(carbrand.length));
				this.str = carbrand[tmp];
				break;
				
			case Animal:
				tmp=(int)(Math.random()*(animal.length));
				this.str = animal[tmp]; 
				break;
				
		}
		
		str = str.toUpperCase();
	}
	
	/** Return Word */
	public String getWord() {
		return str;
	}
	
	/** Return word sheet length */
	public int getSheetLengh(int type) {
		int length = 0;
		switch(type){
			case CarBrands:
				length = carbrand.length;
				break;
				
			case Animal:
				length = animal.length;
				break;
		}
		return length;
	}
}
