package fp.s975002502;
import java.awt.*;
import java.net.URL;

import javax.swing.*;

public class ImagePanel extends JPanel {
	
	// Define constants
	public static final int GALLOWS = 0;
	public static final int HEAD = 1;
	public static final int BODY = 2;
	public static final int LEFTHAND = 3;
	public static final int RIGHTHAND = 4;
	public static final int LEFTFOOT = 5;
	public static final int RIGHTFOOT = 6;
	
	private int step = 0;
	
	URL urlBackground = this.getClass().getResource("image/bg.jpg");
	public ImageIcon bg_icon = new ImageIcon(urlBackground);
	public Image background = bg_icon.getImage();
	
	/** Construct a default imagePanel */
	public ImagePanel() {}
	
	/** Construct a imagePanel with the specified step */
	public ImagePanel(int step) {
		this.step = step;
		repaint();
	}
	
	/** Draw the image on the panel */
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		if(background != null) {
			g.drawImage(background, 0, 0, getWidth(), getHeight(), this);
		}
		
		// Get the appropriate size for the figure
		int width = getSize().width;
		int height = getSize().height;
		
		Graphics2D g2d = (Graphics2D)g;		// Construct Graphics2D Object to draw thick lines
	    int linewidth;						// Set line's width
	    
	    linewidth = (int)(0.04*width);
		g2d.setStroke(new BasicStroke(linewidth));
	    
		// step by step draw image
		switch(step) {
			// draw right foot
			case RIGHTFOOT:
				g.setColor(Color.WHITE);
				g2d.drawLine((int)(0.55*width), (int)(0.55*height), (int)(0.73*width), (int)(0.65*height));
				g2d.drawLine((int)(0.73*width), (int)(0.65*height), (int)(0.57*width), (int)(0.75*height));
				
			// draw left foot
			case LEFTFOOT:
				g.setColor(Color.WHITE);
				g2d.drawLine((int)(0.52*width), (int)(0.55*height), (int)(0.34*width), (int)(0.65*height));
				g2d.drawLine((int)(0.34*width), (int)(0.65*height), (int)(0.50*width), (int)(0.75*height));
				
			// draw right hand
			case RIGHTHAND:
				g.setColor(Color.WHITE);
				g2d.drawLine((int)(0.55*width), (int)(0.26*height)+(int)(0.3*width), (int)(0.73*width), (int)(0.53*height));
				
			// draw left hand
			case LEFTHAND:
				g.setColor(Color.WHITE);
				g2d.drawLine((int)(0.52*width), (int)(0.26*height)+(int)(0.3*width), (int)(0.34*width), (int)(0.53*height));
				
			// draw body
			case BODY:
				linewidth = (int)(0.1*width);
			    g2d.setStroke(new BasicStroke(linewidth));
			   
				g.setColor(Color.WHITE);
				g2d.drawLine((int)(0.535*width), (int)(0.25*height)+(int)(0.3*width), (int)(0.535*width), (int)(0.55*height));
				
			// draw head
			case HEAD:
				g.setColor(Color.WHITE);
				g.fillOval((int)(0.385*width), (int)(0.23*height), (int)(0.3*width), (int)(0.3*width));
				
				if(step == RIGHTFOOT) {
					g.setColor(Color.BLACK);
					linewidth = (int)(0.01*width);
					g2d.setStroke(new BasicStroke(linewidth));
					
					g2d.drawLine((int)(0.485*width), (int)(0.31*height), (int)(0.44*width), (int)(0.27*height));
					g2d.drawLine((int)(0.485*width), (int)(0.27*height), (int)(0.44*width), (int)(0.31*height));
					g2d.drawLine((int)(0.585*width), (int)(0.31*height), (int)(0.63*width), (int)(0.27*height));
					g2d.drawLine((int)(0.585*width), (int)(0.27*height), (int)(0.63*width), (int)(0.31*height));
				}
			
			// draw gallows
			case GALLOWS:
				g.setColor(Color.YELLOW);
				// Pole (|)
				g.fillRect((int)(0.15*width), (int)(0.05*height), (int)(0.09*width), (int)(0.85*height));
				// Rope (|)
				g.fillRect((int)(0.5*width), (int)(0.05*height), (int)(0.07*width), (int)(0.18*height));
				// Top (-)
				g.fillRect((int)(0.15*width), (int)(0.05*height), (int)(0.4*width), (int)(0.09*width));
				// Ground (-)
				g.fillRect((int)(0.1*width), (int)(0.85*height), (int)(0.8*width), (int)(0.1*width));
				break;
		}
	}
	
	/** Return step */
	public int getStep() {
		return step;
	}
	
	/** Set a new step */
	public void setStep(int step) {
		this.step = step;
		repaint();
	}
	
	/** Specify preferred size */
	public Dimension getPreferredSize() {
		return new Dimension(100, 100);
	}	
}
