package fp.s975002502;
import java.awt.*;
import java.awt.event.*;
import javax.sound.sampled.*;
import javax.swing.*;
import java.io.IOException;
import java.net.URL;

public class HangMan extends JFrame {
	public ButtonPanel buttons = new ButtonPanel();			// A-Z letter button panel
	public static JButton[] letters = new JButton[26];		// A-Z letter buttons
	
	public WordBank words = new WordBank();					// Secret word bank
	public LabelPanel labels = new LabelPanel();			// Display the secret word 
	
	public int incorrect = 0;								// The false count
	public ImagePanel image = new ImagePanel(incorrect);	// Display the Gallows picture
	
	public JRadioButton jrbAnimal, jrbCarBrand;
	public int wordCategory = words.CarBrands;
	
	/** Hang man game with initial objects */
	public HangMan() {
		// Random a secret word
		words = new WordBank(wordCategory);
		String str = words.getWord();
		
		// Construct labels
		labels = new LabelPanel(str);
		
		// Construct ratio button
		JPanel ratioPanel = new JPanel();
		ratioPanel.add(jrbAnimal = new JRadioButton("Animal"));
		ratioPanel.add(jrbCarBrand = new JRadioButton("Car Brand"));
		
		JPanel rPanel = new JPanel();
		rPanel.add(buttons);
		rPanel.add(ratioPanel);
		
		// Create panel and add Image panel and buttons
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(1, 2, 5, 5));
		panel.add(image);
		panel.add(rPanel);
		
		// Add panel and labels
		add(panel, BorderLayout.CENTER);
		add(labels, BorderLayout.SOUTH);
		
		// display the background music
		try {
			// Open an audio input stream.
			URL url = this.getClass().getResource("music/bgMusic.WAV");
			AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
			// Get a sound clip resource.
			Clip clip = AudioSystem.getClip();
			// Open audio clip and load samples from the audio input stream.
			clip.open(audioIn);
			clip.start();
		}
		catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		catch (LineUnavailableException e) {
			e.printStackTrace();
		}
		
		// Create a radio button group to group two buttons
		ButtonGroup group = new ButtonGroup();
		group.add(jrbAnimal);
		group.add(jrbCarBrand);
		
		// Register listeners for check boxes
		jrbAnimal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				wordCategory = words.Animal;
				words = new WordBank(wordCategory);
				String str = words.getWord();
				labels.changeStr(str);
				buttons.resetButton();
				letters = buttons.getButtons();
				incorrect = 0;
				image.setStep(incorrect);
			}
		});
		jrbCarBrand.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				wordCategory = words.CarBrands;
				words = new WordBank(wordCategory);
				String str = words.getWord();
				labels.changeStr(str);
				buttons.resetButton();
				letters = buttons.getButtons();
				incorrect = 0;
				image.setStep(incorrect);
			}
		});
		
		// Set initial word category to car brand
		jrbCarBrand.setSelected(true);
	      
		// Wait for user's action
		ButtonListener listener = new ButtonListener();
		letters = buttons.getButtons();
		for(int i=0; i< 26; i++) {
			letters[i].addActionListener(listener);
		}
	}

	/** Handle user's action */
	private class ButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			// Handle user guessed letter
			int i;
			for (i = 0; i < 26; i++) {
				if (e.getSource() == letters[i]) {
					
					letters[i].setEnabled(false);				// letter had been chosen
					labels.setLabels(i);						// reset the display label if necessary
					
					// If it is the RIGHT choice
					if (labels.getCorrect() == true) {
						
						// If user already guessed the secret word, show WIN message 
						// and ask whether user want to continue or not 
						if (labels.getCount() == 0) {
							// WIN message 
							JOptionPane.showMessageDialog(null, "Congratulations, you WIN!!!");
							
							// Choose to continue or not
							int chosen;
							chosen = JOptionPane.showConfirmDialog(null, "Continue?", "GAME", JOptionPane.YES_NO_OPTION);
							// Not to continue, show Goodbye message
							if(chosen == JOptionPane.NO_OPTION){
								JOptionPane.showMessageDialog(null, "Bye-bye~~~");
								System.exit(0);
							}
							// Continue, random a new word and restart the game 
							else{
								// Random a secret word
								words = new WordBank(wordCategory);
								String str = words.getWord();
								labels.changeStr(str);
								buttons.resetButton();
								letters = buttons.getButtons();
								incorrect = 0;
								image.setStep(incorrect);
							}
						}
					}
					
					// If it is the WRONG chose
					if (labels.getCorrect() == false) {
						incorrect++;
						if(incorrect < 7){
							image.setStep(incorrect);
						}
						else{
							JOptionPane.showMessageDialog(null, "Too bad, you LOST!!!");
							int chosen;
							chosen = JOptionPane.showConfirmDialog(null, "Continue?", "GAME", JOptionPane.YES_NO_OPTION);
							if(chosen == JOptionPane.NO_OPTION){
								JOptionPane.showMessageDialog(null, "Bye-bye~~~");
								System.exit(0);
							}
							else{
								// Random a secret word
								words = new WordBank(wordCategory);
								String str = words.getWord();
								labels.changeStr(str);
								buttons.resetButton();
								letters = buttons.getButtons();
								incorrect = 0;
								image.setStep(incorrect);
							}
						}
					}
					labels.setCorrect(false);
				}
			}
		}
	}
}
