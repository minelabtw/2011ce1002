package fp.s975002502;
import javax.swing.JFrame;

public class FinalProject {
	/** Main method */
	public static void main(String[] args) {
		HangMan frame = new HangMan();
		frame.setSize(500, 650);
		frame.setTitle("FrameWork");
		frame.setLocationRelativeTo(null);	// Center the frame
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setResizable(false);
	}
}
