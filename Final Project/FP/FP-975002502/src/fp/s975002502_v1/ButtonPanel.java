package fp.s975002502_v1;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ButtonPanel  extends JPanel {
	public JButton button[] = new JButton[26];	// Create JButton array button
	public ImageIcon icons[] = new ImageIcon[26];	// Create ImageIcon array icon
	
	/** Construct a ButtonPanel with buttons */
	public ButtonPanel() {
		setPreferredSize(new Dimension(200, 500));
		
		// Set the GridLayout of buttons
		setLayout(new GridLayout(7, 4, 5, 5));
				
		// Create the letter buttons
		for (int i=0; i<26; i++) {
			icons[i] = new ImageIcon("image/"+i+".jpg");
			button[i] = new JButton(icons[i]);
			button[i].setPreferredSize(new Dimension(40, 49));
			add(button[i]);
		}
	}
	
	// get letter buttons
	public JButton[] getButtons() {
		return button;
	}
	
	public void resetButton(){
		for(int i=0; i<26; i++) {
			button[i].setEnabled(true);
			validate();
		}
	}
}
