package fp.s975002502_v1;
import java.awt.*;
import javax.swing.*;

public class LabelPanel extends JPanel {
	public CardLayout cardLayout = new CardLayout(2, 2);	// Create a CardLayout to show the answer 
	public JPanel[] cardPanel;								// Create JPanel array for answer
	
	public ImageIcon[] icon  = new ImageIcon[26];			// Create ImageIcon array icon
	public JLabel[] answer;									// Create ImageIcon array icon
	
	public ImageIcon blankIcon = new ImageIcon("image/null.png");	// transparent image to show the blank in the answer
	public ImageIcon blockIcon = new ImageIcon("image/blank.jpg");	// white image to block the answer
	public JLabel[] blank;											// JLabel To block the answer
	
	public String ans = "START";		// default string
	public int count = 0;
	public boolean correct = false;
	
	/** Construct a default LabelPanel with labels */
	public LabelPanel() {
		setStr(ans);
	}
	
	/** Construct a LabelPanel to set new string with labels */
	public LabelPanel(String ans) {
		this.ans = ans;
		setStr(ans);
	}
	
	/**  */
	public void changeStr(String newAns) {
		this.correct = true;
		System.out.println(ans.length());
		for(int i=0; i<ans.length(); i++) {
			//System.out.println("remove cardpanel : "+i);
			remove(cardPanel[i]);
		}
		setStr(newAns);
		validate();
	}
	
	/** Return correct */
	public boolean getCorrect() {
		return correct;
	}
	
	/** Return count */
	public int getCount() {
		return count;
	}
	
	/** Set correct */
	public void setCorrect(boolean correct) {
		this.correct = correct;
		//System.out.println("set Correct IN!!! corrcet is " + correct);
	}
	
	/** Set the display of labels */
	public void setLabels(int guessedLetter) {
		// if guessed letter is right, set the letter visible in the right place
		for(int i=0; i<ans.length(); i++){
			//System.out.println("answer["+i+"]"+answer[i].getIcon());
			if(answer[i].getIcon() == icon[guessedLetter]) {
				correct = true;
				//System.out.println("set Labels IN!!");
				cardLayout.next(cardPanel[i]);
				count = count-1;
			}
		}
	}
	
	/** Set string and create labels */
	public void setStr(String ans) {
		System.out.println("ans = "+ans);
		this.ans = ans;
		int length = ans.length();
		count = length;
		//System.out.println("secret length:" + length);
		
		// According to the length of answer dynamic create panels and labels
		cardPanel = new JPanel[length];
		answer = new JLabel[length];
		blank = new JLabel[length];
		
		// Create alphabet icom's image
		for(int i=0; i<26; i++) {
			icon[i] = new ImageIcon("image/"+i+".jpg");
		}
		
		// Add element to the display panel
		for (int i=0; i<length; i++) {
			int letter = ((int)ans.charAt(i)) - 65;
			
			blank[i] = new JLabel(blockIcon);		// block the answer
			answer[i] = new JLabel(icon[letter]);	// the answer
			cardPanel[i] = new JPanel();			// the panel
			
			// set the card layout to the panel
			cardPanel[i].add(blank[i]);	
			cardPanel[i].add(answer[i]);
			cardPanel[i].setLayout(cardLayout);		
			
			add(cardPanel[i]);
		}
	}
}
