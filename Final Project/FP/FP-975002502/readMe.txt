1. hang man 是一個猜英文單字的遊戲

2. 遊戲方式 : 
	利用滑鼠點選字母猜下方的單字
	(猜過的字母就不能在點選)
	如果猜到正確字母，則顯示字母在單字中出現的位置
	如果猜錯，則旁邊的絞刑台上會慢慢顯現一個人
	遊戲終止條件 :
	(1) 當所有字母都被猜到         --> WIN
	(2) 當絞刑台上完整顯現一個人了 --> LOST

3. Demo時的Bug
	當猜完第一個字後，想要繼續遊戲時，字母button的控制會發生問題(已解決)
	--> 因為程式的button listener 忘記做更新

	當 random 的字母字數不同時，Label的產生會發生問題(已部分解決)
	--> 檢查過後發現，只要將視窗縮小再放大，殘留的多餘影像就會消失。

4. 新增功能
	加入背景音樂
	加入可以選擇單字庫類別的radio button
	

