package fp.s100502021;

import java.awt.Graphics;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.omg.CosNaming.NamingContextExtPackage.AddressHelper;

public class Rule extends JPanel{
	private String rule=new String();
	public Rule(){
		rule=("猜出電腦的四位數亂數，每猜一次，都會給一次提示，提示由幾A幾B的方式呈現，(A代表著數字與位置都對，B代表著有此數字但位置錯誤)全中即為4A");
	}
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);		
		g.drawString(rule, 5 , 15);		
	}
}
