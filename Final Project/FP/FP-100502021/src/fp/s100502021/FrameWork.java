package fp.s100502021;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.Normalizer.Form;
import java.util.FormatFlagsConversionMismatchException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.Timer;


public class FrameWork extends JFrame implements ActionListener{
	private ImageIcon[] back=new ImageIcon[4];
	private CharacterLimitedTextField guess1=new CharacterLimitedTextField(1);
	private CharacterLimitedTextField guess2=new CharacterLimitedTextField(1);
	private CharacterLimitedTextField guess3=new CharacterLimitedTextField(1);
	private CharacterLimitedTextField guess4=new CharacterLimitedTextField(1);
	private JPanel numberpanel=new JPanel();
	private JPanel guesspanel=new JPanel();
	private JPanel bloodpanel=new JPanel();
	private JPanel bombpanel=new JPanel();
	private JLabel background=new JLabel();
	protected JTextArea record=new JTextArea();
	protected JLabel precord=new JLabel();
	protected String str="";
	protected String str2="0000";
	protected String num="0000";
	protected int A=4;
	protected int point=0;
	private JMenu menu=new JMenu("menu");
	private JMenu explain=new JMenu("rule explain");
	private JMenuItem startgame=new JMenuItem();
	private JMenuItem exit=new JMenuItem();
	private JMenuItem rule=new JMenuItem();
	private JFrame rframe=new JFrame();
	private ImageIcon bombp=new ImageIcon("bomb.jpg");
	private JLabel bomb=new JLabel(bombp);
	private AudioClip boom;
	protected AudioClip count;
	Rule r=new Rule();
	Timer timercountdown=new Timer(50,new TimerListenercountdown());
	Timer TimerListenerchange=new Timer(3000,new TimerListenerchange());
	blood blo=new blood();
	private Container cou;
	int i=0,s=0;
	public FrameWork() {
		boom=Applet.newAudioClip(getClass().getResource("bomb2.wav"));
		count=Applet.newAudioClip(getClass().getResource("2012.wav"));
		back[0]=new ImageIcon("prototype  0.jpg");
		back[1]=new ImageIcon("prototype  1.jpg");
		back[2]=new ImageIcon("prototype  2.jpg");
		back[3]=new ImageIcon("prototype  3.jpg");
		TimerListenerchange.start();
		background=new JLabel(back[i]);
		this.getLayeredPane().add(background,new Integer(Integer.MIN_VALUE));
		background.setBounds(0,0,back[i].getIconWidth(),back[i].getIconHeight());
		cou=this.getContentPane();
		cou.setLayout(new BorderLayout());
		((JPanel)cou).setOpaque(false);
		getContentPane().setLayout(null);
		repaint();
		JToolBar toolBar=new JToolBar("");
		JMenuBar bar=new JMenuBar();
		setJMenuBar(bar);
		bar.add(menu);		
		menu.add(startgame=new JMenuItem("start a new game",'G'));
		menu.add(rule=new JMenuItem("Rule",'R'));
		menu.add(exit=new JMenuItem("exit"));
		record.setLineWrap(true);
		record.setWrapStyleWord(true);
		record.setEditable(false);
		setLayout(new BorderLayout(1,1));
		numberpanel.add(guess1);
		numberpanel.add(guess2);
		numberpanel.add(guess3);
		numberpanel.add(guess4);
		numberpanel.add(blo);
		bloodpanel.add(blo);
		precord=new JLabel("You have "+point+" point");
		precord.setForeground(Color.WHITE);
		add(numberpanel,BorderLayout.WEST);
		add(guesspanel,BorderLayout.EAST);
		add(blo,BorderLayout.SOUTH);
		add(precord,BorderLayout.CENTER);
		
		guess1.addActionListener(this);
		guess2.addActionListener(this);
		guess3.addActionListener(this);
		guess4.addActionListener(this);
		startgame.addActionListener(this);
		rule.addActionListener(this);
		exit.addActionListener(this);			
		rframe.add(r);
		rframe.pack();
		bombpanel.add(bomb);
	}
	public void setanswer(String number){
		num=number;
	}
	public void setAcount(){
		confirm con=new confirm(num,str2);
		A=con.Acounter;
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==guess1||e.getSource()==guess2||e.getSource()==guess3||e.getSource()==guess4){	
			str2=guess1.getText()+guess2.getText()+guess3.getText()+guess4.getText();
			confirm con=new confirm(num,str2);
			str+=guess1.getText()+guess2.getText()+guess3.getText()+guess4.getText()+"        "+con.Acounter+" A  "+con.Bcounter+" B  \n";
			record.setText(str);
			//System.out.println(num);
			setAcount();
			guess1.setText("");
			guess2.setText("");
			guess3.setText("");
			guess4.setText("");
			if(con.Acounter==4){
				blo.x[1]+=20;
				blo.x[2]+=20;
				point++;
				precord.setText("You have "+point+" point");
				str="";
				
			}
			else {
				blo.x[1]-=20;
				blo.x[2]-=20;
			}			
			guesspanel.add(record);
			add(numberpanel,BorderLayout.WEST);
			add(guesspanel,BorderLayout.EAST);
			
			guesspanel.revalidate();
			repaint();
		}		
		if (e.getSource()==startgame) {
			/*guess1=new CharacterLimitedTextField(1);
			guess2=new CharacterLimitedTextField(1);
			guess3=new CharacterLimitedTextField(1);
			guess4=new CharacterLimitedTextField(1);*/
			point=0;
			str="";
			precord.setText("You have "+point+" point");
			record.setText(str);
			blo.x[1]=400;
			blo.x[2]=400;
			timercountdown.start();
			count.play();
			count.loop();
			boom.stop();
			remove(bombpanel);
		}
		if(e.getSource()==exit){
			this.setVisible(false);	
			count.stop();
			boom.stop();
			Start s = new Start();
			s.setTitle("FP"); //Frame title
			s.setSize(1360,768);   //Frame size
			s.setLocationRelativeTo(null);
			s.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			s.setVisible(true);	
			s.number();
			
		//	System.exit(0);
		}
		if(e.getSource()==rule){
			rframe.setSize(840, 100);
			rframe.setVisible(true);
		}
	}
	private class TimerListenercountdown implements ActionListener{
		public void actionPerformed(ActionEvent e){
			blo.count();			
			if(blo.x[1]<=52){				
				blo.x[1]=50;
				blo.x[2]=50;
				
				timercountdown.stop();
				precord.setText("Game Over and You have "+point+" point");
				add(bombpanel,BorderLayout.NORTH);
				boom=Applet.newAudioClip(getClass().getResource("bomb2.wav"));
				boom.play();
				count.stop();
			}			
			repaint();
		}
	}	
	private class TimerListenerchange extends JFrame implements ActionListener{
		public void actionPerformed(ActionEvent e){
			s++;
			i=s%4;
			background.setIcon(back[i]);
			/*back[0]=new ImageIcon("prototype  0.jpg");
			back[1]=new ImageIcon("prototype  1.jpg");
			back[2]=new ImageIcon("prototype  2.jpg");
			back[3]=new ImageIcon("prototype  3.jpg");*/
			
		}
	}	
		
}
