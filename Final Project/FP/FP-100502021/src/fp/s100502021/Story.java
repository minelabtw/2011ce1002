package fp.s100502021;

import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.omg.CosNaming.NamingContextExtPackage.AddressHelper;

public class Story extends JFrame{
	private JTextArea sto=new JTextArea("主角 Alex Mercer 是一位基因改造人，擁有變形的能力，但是他對自己的過去卻完全沒有記憶。他在紐約市的黑暗面中打一場不為人知的戰爭，致力於挖掘出他的過去，並解開自己的存在之謎。由於整個行動逐漸失控，未知的陰謀、他自己的出身，都將威脅著人類的未來。整個遊戲故事敘述失去記憶、擁有特殊能力的 Alex Mercer 在尋找自身過往之際，不僅面對軍方與菁英組織的威脅，甚至傳出城市爆發病毒感染的問題，玩家必須深入這長達 40 年的陰謀中，尋找事件的真相。");
	
	public Story(){		
		sto.setLineWrap(true);
		sto.setWrapStyleWord(true);
		sto.setEditable(false);
		sto.setFont(new Font("Serif", Font.PLAIN, 20));
		add(sto);
	}
}
