package fp.s100502021;

import javax.swing.JFrame;

public class FinalProject {
	public static void main(String[] args){	
		/*DEMO太緊張沒有講到的功能:
		 * 在輸入完一次數字之後，框框會自動變空白的，
		 * 並且CharacterLimitedTextField是上網找的class
		 * 他可以限制輸入框框內的數字數
		 * 答對之後
		 * 之前的提示會消掉
		 */
		Start s = new Start();
		s.setTitle("FP"); //Frame title
		s.setSize(1360,768);   //Frame size
		s.setLocationRelativeTo(null);
		s.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		s.setVisible(true);	
		s.number();
		
	}
}
