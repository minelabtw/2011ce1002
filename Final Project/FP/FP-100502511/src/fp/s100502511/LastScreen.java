package fp.s100502511;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.*;

public class LastScreen extends JFrame implements ActionListener {
	private JButton yes = new JButton("Yes");
	private JButton no = new JButton("No");
	private JLabel message = new JLabel("Do you want to restart the game??");
	private JPanel Screen1 = new JPanel(new GridLayout(1, 1));
	private JPanel Screen2 = new JPanel(new GridLayout(1, 2));
	private JPanel Screen3 = new JPanel(new GridLayout(2, 1));
	private Font setfont = new Font("TimesRoman", Font.BOLD, 25);

	public LastScreen() {
		message.setFont(setfont);
		message.setHorizontalAlignment(JLabel.CENTER);
		Screen1.add(message);
		Screen2.add(yes);
		Screen2.add(no);
		Screen3.add(Screen1);
		Screen3.add(Screen2);
		add(Screen3);
		setTitle("Final Project");
		setSize(800, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
		yes.addActionListener(this);
		no.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == yes) {  // 選擇yes,重來
			FirstScreen F = new FirstScreen();
			F.setTitle("Final project");
			F.setSize(800, 500);
			F.setLocationRelativeTo(null);
			F.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			F.setVisible(true);
			this.setVisible(false); 
		} else if (e.getSource() == no) {  // 選擇no,離開
			JOptionPane.showMessageDialog(null, "Thanks for your playing!!");
			System.exit(0);
		}
	}
}
