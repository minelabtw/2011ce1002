package fp.s100502511;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

public class ThirdScreen extends JFrame implements ActionListener {

	private Time clock = new Time();
	private Timer timer = new Timer(1000, new TimerListener());
	private boolean START = false;
	static MovePanel mp;
	private JLabel Time = new JLabel();
	private JPanel Screen = new JPanel();
	private int judge = 0;
	private JButton end = new JButton("END");
	private JLabel background = new JLabel();
	private ImageIcon back = new ImageIcon("image/photo/8.jpg");

	public ThirdScreen() {

		Time.setLayout(null);
		Time.setBorder(null);

		background.setLayout(null);
		background.setBorder(null);

		end.setLayout(null);
		end.setBorder(null);

		background.setBounds(0, 0, 1200, 700); // 設置位置
		background.setIcon(back);

		end.setPreferredSize(new Dimension(60, 50));
		end.setBounds(1125, 0, 60, 50); // 設置位置

		Time.add(clock);
		Time.setLayout(new FlowLayout(FlowLayout.LEFT));

		mp = new MovePanel();

		Time.setPreferredSize(new Dimension(100, 100));
		Time.setBounds(-10, 0, 100, 100); // 設置位置

		mp.setPreferredSize(new Dimension(1200, 700));
		mp.setBounds(0, 200, 1200, 700); // 設置位置

		Screen.add(background);

		add(Time);
		add(end);
		add(mp);
		add(Screen);

		end.addActionListener(this);
		mp.addKeyListener(new test());
		mp.addMouseListener(new test1());

		setTitle("Final Project");
		setSize(1200, 700);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);

		mp.requestFocus();  // 接收鍵盤訊息

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == end) {
			setVisible(false);

			LastScreen last = new LastScreen();
		}
	}

	public static void main(String[] args) {
		new ThirdScreen();

	}

	public class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (START = true) {
				clock.setCurrentTime();
			} else if (mp.x >= 1000) {
				START = false;
				timer = new Timer(1000, new TimerListener());
				timer.stop();
			}

			clock.repaint();

		}
	}

	public class test extends KeyAdapter { // 輸入對應的鍵，所做的反應

		public void keyPressed(KeyEvent e) {

			switch (e.getKeyCode()) {
			case KeyEvent.VK_LEFT:
				START = true;
				mp.timer2.start();
				timer.start();
				break;
			case KeyEvent.VK_RIGHT:
				START = true;
				mp.timer2.start();
				timer.start();
				break;
			case KeyEvent.VK_UP:
				START = true;
				mp.timer2.start();
				timer.start();
				break;
			case KeyEvent.VK_DOWN:
				START = true;
				mp.timer2.start();
				timer.start();
				break;
			}

		}

	}

	public class test1 extends MouseAdapter { // 輸入對應的鍵，所做的反應
		public void mousePressed(MouseEvent e) {
			switch (e.getButton()) {
			case MouseEvent.BUTTON1:
				START = true;
				mp.timer2.start();
				timer.start();
				break;
			case MouseEvent.BUTTON3:
				START = true;
				mp.timer2.start();
				timer.start();
				break;
			}

		}

	}
}

class MovePanel extends JLabel {
	private final int MOVE_SPACE = 10;
	private BufferedImage img;
	static int x;
	private int y;
	private boolean START = false;
	Time clock1 = new Time();
	Timer timer2 = new Timer(1000, new TimerListener());

	public MovePanel() {

		try {
			img = ImageIO.read(new File("image/photo/Cat.png"));
		} catch (IOException e) { // 沒有圖檔的話，所執行的部分
			JOptionPane.showMessageDialog(null, "Can't fine the cat!!!!");
			System.exit(0);
		}
		addMouseListener(new MoveListener2());
		addKeyListener(new MoveListener1());
		x = 0;
		y = 0;
	}

	public void paint(Graphics g) {
		super.paint(g);
		if (x <= 1000) {
			System.out.println("GO!  GO!  GO!");
		}
		g.drawImage(img, x, y, null);

	}

	private class MoveListener1 extends KeyAdapter {
		int a = 0;
		int b = 0;
		int c = 0;
		int d = 0;

		public void keyPressed(KeyEvent e) {

			switch (e.getKeyCode()) {
			case KeyEvent.VK_LEFT:
				a += 1;
				break;
			case KeyEvent.VK_RIGHT:
				b += 1;
				break;
			case KeyEvent.VK_UP:
				c += 1;
				break;
			case KeyEvent.VK_DOWN:
				d += 1;
				break;
			}
			if (a == 1 && b == 1) {
				x += MOVE_SPACE;
				a = 0;
				b = 0;
			} else if (a > 1 || b > 1) {
				a = 0;
				b = 0;
			} else if (c == 1 && d == 1) {
				x += MOVE_SPACE;
				c = 0;
				d = 0;
			} else if (c > 1 || d > 1) {
				c = 0;
				d = 0;
			}

			MovePanel.this.repaint();
			System.out.println("X : " + x + " Y : " + y);
			if (x >= 1000) {
				timer2.stop();
				START = false;
				JOptionPane.showMessageDialog(null,
						"You reach the goal!! \n  Time: " + clock1.getHour()
								+ "  :  " + clock1.getMinute() + "  :  "
								+ clock1.getSecond());

			}
		}
	}

	private class MoveListener2 extends MouseAdapter {
		int a = 0;
		int b = 0;

		public void mousePressed(MouseEvent e) {
			switch (e.getButton()) {
			case MouseEvent.BUTTON1:
				a += 1;
				break;
			case MouseEvent.BUTTON3:
				b += 1;
				break;
			}
			if (a == 1 && b == 1) {
				x += MOVE_SPACE;
				a = 0;
				b = 0;
			} else if (a > 1 || b > 1) {
				a = 0;
				b = 0;
			}

			MovePanel.this.repaint();
			System.out.println("X : " + x + " Y : " + y);
			if (x >= 1000) {
				timer2.stop();
				START = false;
				JOptionPane.showMessageDialog(null,
						"You reach the goal!! \n  Time: " + clock1.getHour()
								+ "  :  " + clock1.getMinute() + "  :  "
								+ clock1.getSecond());

			}
		}
	}

	public class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (START = true) {
				clock1.setCurrentTime();
			} else if (START = false) {
				timer2 = new Timer(1000, new TimerListener());
				timer2.stop();

			}
			clock1.repaint();
		}
	}
}