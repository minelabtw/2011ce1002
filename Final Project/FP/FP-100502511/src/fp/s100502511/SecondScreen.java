package fp.s100502511;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.*;

public class SecondScreen extends JFrame implements ActionListener {
	private JButton Start = new JButton("START");
	private JLabel Introduce = new JLabel();
	private Font setfont = new Font("TimesRoman", Font.BOLD, 15);
	private JPanel Screen1 = new JPanel(new GridLayout(2, 1));
	private String introduction = "<html>Story: <br/> The cat is in the universe!!"
			+ "<br/>And the cat want to fly!!<br/><br/><br/>How to play: <br/>click<br/> "
			+ "1. mouse button1 and mouse button3<br/>2. → and ←<br/>3. ↓ and ↑<br/>"
			+ "To move the cat to the end!!<br/>上面3種方法皆可以移動\"貓\"，輸入各組每個按鍵一次，<br/>"
			+ "(兩個鍵各按過1次)" + "即可移動一次，最後在點擊右上角END離開。</html>";

	public SecondScreen() {
		Introduce.setFont(setfont);
		Introduce.setText(introduction);
		Introduce.setHorizontalAlignment(JLabel.CENTER);
		Screen1.add(Introduce);
		Screen1.add(Start);
		add(Screen1);
		Start.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == Start) {
			this.setVisible(false);
			ThirdScreen a = new ThirdScreen();
			a.setTitle("Final Project");
			a.setSize(1200, 700);
			a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			a.setLocationRelativeTo(null);
			a.setVisible(true);
		}
	}
}