package fp.s100502511;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.*;

public class FirstScreen extends JFrame implements ActionListener {
	private JButton start = new JButton();
	private JButton exit = new JButton();
	private JPanel Screen3 = new JPanel();
	private Font setfont = new Font("TimesRoman", Font.BOLD, 25);
	private Font setfont1 = new Font("TimesRoman", Font.BOLD, 60);
	private JLabel Title = new JLabel("Flying Cat!!");
	private boolean START = false;
	private JLabel background = new JLabel();
	private ImageIcon button1 = new ImageIcon("image/photo/yes.jpg");
	private ImageIcon button2 = new ImageIcon("image/photo/no.jpg");
	private ImageIcon back = new ImageIcon("image/photo/8.jpg");

	public FirstScreen() {

		background.setLayout(null);
		start.setLayout(null);
		exit.setLayout(null);
		background.setBorder(null);
		start.setBorder(null);
		exit.setBorder(null);
		background.setBounds(0, 0, 800, 500);
		background.setIcon(back);

		start.setIcon(button1);
		start.setPreferredSize(new Dimension(120, 120));
		start.setBounds(100, 300, 120, 120);
		Title.setFont(setfont1);
		Title.setHorizontalAlignment(JLabel.CENTER);
		Title.setForeground(Color.WHITE);
		Title.setPreferredSize(new Dimension(500, 200));
		Title.setBounds(150, 50, 500, 200);
		exit.setIcon(button2);
		exit.setPreferredSize(new Dimension(120, 120));
		exit.setBounds(550, 300, 120, 120);

		Screen3.add(background);

		add(Title);
		add(start);
		add(exit);
		add(Screen3);

		start.addActionListener(this);
		exit.addActionListener(this);

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == start) { // start,�I�sSecondScreen

			setVisible(false);
			SecondScreen game = new SecondScreen();
			game.setTitle("Final Project");
			game.setSize(800, 700);
			game.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			game.setLocationRelativeTo(null);
			game.setVisible(true);

		} else if (e.getSource() == exit) { // exit,�I�sFourthScreen

			setVisible(false);
			FourthScreen Sure = new FourthScreen();

		}
	}
}
