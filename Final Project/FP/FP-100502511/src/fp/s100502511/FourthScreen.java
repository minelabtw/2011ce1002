package fp.s100502511;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.*;

public class FourthScreen extends JFrame implements ActionListener {
	private JButton YES = new JButton("Exit the game!");
	private JButton NO = new JButton("Back to the game!");
	private JLabel MESSAGE = new JLabel(
			"<html>Are you really sure that you want to exit the game??<br/> Or back to the main screen??</html>");
	private JPanel Button = new JPanel(new GridLayout(1, 2));
	private JPanel Screen = new JPanel(new GridLayout(2, 1));
	private Font setfont = new Font("TimesRoman", Font.BOLD, 25);

	public FourthScreen() {
		MESSAGE.setFont(setfont);
		MESSAGE.setHorizontalAlignment(JLabel.CENTER);
		Button.add(YES);
		Button.add(NO);
		Screen.add(MESSAGE);
		Screen.add(Button);
		add(Screen);
		setSize(800, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
		YES.addActionListener(this);
		NO.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == YES) {  // ���YES,���}
			JOptionPane.showMessageDialog(null, "Thanks for your playing!!");
			System.exit(0);
		} else if (e.getSource() == NO) {  // ���NO,��^
			setVisible(false);
			FirstScreen FP = new FirstScreen();
			FP.setTitle("Final Project");
			FP.setSize(800, 500);
			FP.setLocationRelativeTo(null);
			FP.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			FP.setVisible(true);
		}
	}
}
