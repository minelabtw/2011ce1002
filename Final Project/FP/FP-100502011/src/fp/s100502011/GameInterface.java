package fp.s100502011;

import java.util.Scanner;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;


import java.awt.*;
import java.awt.event.*;
public class GameInterface extends JFrame{
	public JPanel game = new JPanel();
	public int direction=1;
	public BackGround back = new  BackGround();
	public ImageIcon retu = new ImageIcon("button/button.gif");
	public Timer timer = new Timer(100,new TimerListener());
	public int second=0;
	public int specialtime=80;
	public int mapChoice = 2;
	int speed=0;
	public Snake s = new Snake();
	public Food food = new Food();
	boolean special=false;
	public GameInterface(){
		
		// to set game's screen
		game.setLayout(new GridLayout(1,1));
		game.setFocusable(true);
		// press to move the snake
		game.addKeyListener(new KeyAdapter(){
			public void keyPressed(KeyEvent e){
				switch(e.getKeyCode()){
					case KeyEvent.VK_DOWN:
						if(direction!=4)
							direction=3;
						break;
					case KeyEvent.VK_UP:
						if(direction!=3)
							direction=4;
						break;
					case KeyEvent.VK_LEFT:
						if(direction!=1)
							direction=2;
						break;
					case KeyEvent.VK_RIGHT:
						if(direction!=2)
							direction=1;
						break;
				}
			}
		});
		timer.start();
		
		// set frame
		game.add(new DrawSnake());
		add(game);
		this.setSize(800,600);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(false);
	}
	// to get random number
	public int getRandomnumbers() {
		return (int)(Math.random()*1000%20-1);
	}
	
	//timer class
	class TimerListener implements ActionListener{ // timer action
		public void actionPerformed(ActionEvent e){
			repaint();
		}
	}
	 // to draw snake
	public class DrawSnake extends JPanel implements ActionListener{
		public int interval;
		public int speed=0;
		public int quan=0;
		public int count = 0;
		public int OverCount=0;
		public int BarrierCount=0;
		public int score = 0;
		boolean over = true;
		public ImageIcon[] snakes = new ImageIcon[100];
		public ImageIcon[] beforeS = new ImageIcon[100];
		public ImageIcon[] specialFood = new ImageIcon[400];
		public ImageIcon imageScore = new ImageIcon("Snake/Score.gif");
		public boolean life = true;
		public JLabel point = new JLabel();
		public JButton again = new JButton("");
		String Score ="";
		boolean change = false;
		Image foods = food.getFood();
		Image foods2 = food.getFood2();
		Image foods3 = food.getFood3();
		public Font font = new Font("Serif",Font.BOLD,50);
		public Font gameover = new Font("Serif",Font.BOLD,100);
		public int food_x1 = getRandomnumbers()*interval;
		public int food_y1 = getRandomnumbers()*interval;
		public DrawSnake (){
		}
		protected void paintComponent(Graphics g){
			//initial value
			interval=Math.min(getHeight(),getWidth())/20;
			int dx=-1;
			int dy=0;
			int barrier1=76;
			int barrier2=58;
			if(count==0){
				second=0;
				s.initSnake();
				food.setLocation();
				snakes[0]=s.head2;
				snakes[1]=s.body2;
				snakes[2]=s.tail2;
			}
			// store the snake's body before move
			for(int q=0;q<s.size;q++){
				beforeS[q]=snakes[q];
			}
			super.paintComponent(g);
			
			//draw background
			g.drawImage(back.back3.getImage(),0,0,getWidth(),getHeight(),this);
			g.drawImage(back.back1.getImage(),15,0,20*interval,20*interval,this);
			// stage3
			if(mapChoice==3){
				if(count==0){
					s.size=5;
					s.x[0]=9;
					s.x[1]=8;
					s.x[2]=7;
					s.x[3]=6;
					s.x[4]=5;
					s.y[0]=18;
					s.y[1]=18;
					s.y[2]=18;
					s.y[3]=18;
					s.y[4]=18;
				}
				g.drawImage(back.wool2.getImage(), 15,0,interval,interval, this);
				g.drawImage(back.wool3.getImage(), 15+19*interval,0,interval,interval, this);
				// to set barriers
				for(int i=1;i<20;i++){
					g.drawImage(back.wool2.getImage(), 15,i*interval,interval,interval, this);
					back.barrier_x[19+i]=0;
					back.barrier_y[19+i]=i;
				}
				for(int i=1;i<20;i++){
					g.drawImage(back.wool4.getImage(), 15+interval*i,19*interval,interval,interval, this);
					back.barrier_x[i+38]=i;
					back.barrier_y[i+38]=19;
				}
				for(int i=1;i<19;i++){
					g.drawImage(back.wool3.getImage(), 15+19*interval,interval*i,interval,interval, this);
					back.barrier_x[i+57]=19;
					back.barrier_y[i+57]=i;
				}
			}
			//stage2
			if(mapChoice==2){
				
				//initial value
				if(count==0){
				s.x[0]=3;
				s.x[1]=2;
				s.x[2]=1;
				s.y[0]=1;
				s.y[1]=1;
				s.y[2]=1;
				}
				// to set barriers
				for(int i =0;i<20;i++){
					g.drawImage(back.wool1.getImage(), 15+interval*i,0,interval,interval, this);
					back.barrier_x[i]=i;
					back.barrier_y[i]=0;
				}
				for(int i=1;i<20;i++){
					g.drawImage(back.wool2.getImage(), 15,i*interval,interval,interval, this);
					back.barrier_x[19+i]=0;
					back.barrier_y[19+i]=i;
				}
				for(int i=1;i<20;i++){
					g.drawImage(back.wool4.getImage(), 15+interval*i,19*interval,interval,interval, this);
					back.barrier_x[i+38]=i;
					back.barrier_y[i+38]=19;
				}
				for(int i=1;i<19;i++){
					g.drawImage(back.wool3.getImage(), 15+19*interval,interval*i,interval,interval, this);
					back.barrier_x[i+57]=19;
					back.barrier_y[i+57]=i;
				}
			}
			//to determine the snake's head
			if(direction==1){
				snakes[0]=s.head2;
			}
			else if(direction==2){
				snakes[0]=s.head4;
			}
			else if(direction==3){
				snakes[0]=s.head3;
			}
			else
				snakes[0]=s.head1;
			//move direction and give their new location
			if(direction==1){
				dx=1;
				dy=0;
				
			}
			else if(direction==2){
				dx=-1;
				dy=0;
			}
			else if(direction==3){
				dy=1;
				dx=0;
			}
			else{
				dy=-1;
				dx=0;
			}
			for(int k=s.getSize();k>0;k--){
					s.x[k]=s.x[k-1];
					s.y[k]=s.y[k-1];	
			}
			s.x[0]+=dx;
			s.y[0]+=dy;
			
			// to repaint before picture
			for(int i=2;i<=s.size;i++){
				snakes[i]=beforeS[i-1];
			}
			// determine snake's body use what picture
			if((snakes[1]==s.corner1&&direction==3)||(snakes[1]==s.corner2&&direction==4)||(snakes[1]==s.corner3&&direction==4)||(snakes[1]==s.corner4&&direction==3)){
				snakes[1]=s.body1;
			}
			if((snakes[1]==s.corner1&&direction==2)||(snakes[1]==s.corner2&&direction==2)||(snakes[1]==s.corner3&&direction==1)||(snakes[1]==s.corner4&&direction==1)){
				snakes[1]=s.body2;
			}
			//if arrive to egde
			if(s.x[0]<0&&direction==2){
				s.x[0]=19;
				change=true;
			}
			if(s.x[0]>19&&direction==1){
				s.x[0]=0;
				change=true;
			}
			if(s.y[0]>19&&direction==3){
				s.y[0]=0;
				change=true;
			}
			if(s.y[0]<0&&direction==4){
				s.y[0]=19;
				change=true;
			}
			if(change==true){
				if(s.x[0]==19&&s.x[2]==0){
					s.x[0]-=20;
				}
				if(s.x[0]==0&&s.x[2]==19){
					s.x[0]+=20;
				}
				if(s.y[0]==0&&s.y[2]==19){
					s.y[0]+=20;
				}
				if(s.y[0]==19&&s.y[2]==0){
					s.y[0]-=20;
				}
			}
			// to determine the corner use what picture
			if((s.x[0]==s.x[2]+1&&s.y[0]==s.y[2]+1&&direction==3)||(s.x[0]==s.x[2]-1&&s.y[0]==s.y[2]-1&&direction==2)
					||(s.x[2]-20)==s.x[0]-1&&s.y[2]==s.y[0]-1||s.x[2]==s.x[0]+1&&s.y[2]+20==s.y[0]+1){
				snakes[1]=s.corner1;
			}
			if((s.x[0]==s.x[2]+1&&s.y[0]==s.y[2]-1&&direction==4)||(s.x[0]==s.x[2]-1&&s.y[0]==s.y[2]+1&&direction==2)
					||(s.x[2]-20)==s.x[0]-1&&s.y[2]==s.y[0]+1||s.x[2]==s.x[0]+1&&s.y[2]-20==s.y[0]-1){
				snakes[1]=s.corner2;
			}
			if((s.x[0]==s.x[2]-1&&s.y[0]==s.y[2]-1&&direction==4)||(s.x[0]==s.x[2]+1&&s.y[0]==s.y[2]+1&&direction==1)
					||(s.x[2]+20)==s.x[0]+1&&s.y[2]==s.y[0]+1||s.x[2]==s.x[0]-1&&s.y[2]-20==s.y[0]-1){
				snakes[1]=s.corner3;
			}
			if((s.x[0]==s.x[2]-1&&s.y[0]==s.y[2]+1&&direction==3)||(s.x[0]==s.x[2]+1&&s.y[0]==s.y[2]-1&&direction==1)
					||(s.x[2]+20)==s.x[0]-1&&s.y[2]==s.y[0]-1||s.x[2]==s.x[0]-1&&s.y[2]+20==s.y[0]+1){
				snakes[1]=s.corner4;
			}
			
			if(change==true){
				if(s.x[0]==-1){
					s.x[0]+=20;
					change=false;
				}
				if(s.x[0]==20){
					s.x[0]-=20;
					change=false;
				}
				if(s.y[0]==20){
					s.y[0]-=20;
					change=false;
				}
				if(s.y[0]==-1){
					s.y[0]+=20;
					change=false;
				}
			}
			// to count time
			count++;
			
			//to draw food
			if(count==1){
				food_x1 = getRandomnumbers()*interval;
				food_y1 = getRandomnumbers()*interval;
			}
			if(mapChoice!=1){
				while(over==true){ // the food can't over with snake's body and barrier
					OverCount=0;
					BarrierCount=0;
					food_x1 = getRandomnumbers()*interval;
					food_y1 = getRandomnumbers()*interval;
					for(int b=0;b<s.size;b++){	
						if((s.x[b])*interval==food_x1&&(s.y[b])*interval==food_y1){
							over=true;
						}
						else
							OverCount++;
					}
					if(OverCount==s.size){
						if(mapChoice==2){
							for(int a=0;a<barrier1;a++){
								if((back.barrier_x[a])*interval==food_x1&&(back.barrier_y[a])*interval==food_y1){
									over=true;
								}
								else
									BarrierCount++;
							}
						}
						if(mapChoice==3){
							for(int a=0;a<barrier2;a++){
								if((back.barrier_x[a])*interval==food_x1&&(back.barrier_y[a])*interval==food_y1){
									over=true;
								}
								else
									BarrierCount++;
							}
						}
					}
					if(mapChoice==2){
						if(OverCount==s.size&&BarrierCount==barrier1){
							over=false;
						}
						else{
							over=true;
						}
					}
					if(mapChoice==3){
						if(OverCount==s.size&&BarrierCount==barrier2){
							over=false;
						}
						else{
							over=true;
						}
					}
				}
			}
			if(mapChoice==1){
				while(over==true){// the food can't over with snake's body and barrier
					OverCount=0;
					food_x1 = getRandomnumbers()*interval;
					food_y1 = getRandomnumbers()*interval;
					for(int b=0;b<s.size;b++){	
						if((s.x[b])*interval==food_x1&&(s.y[b])*interval==food_y1){
							over=true;
						}
						else
							OverCount++;
					}
					if(OverCount==s.size){
						over=false; // not over
					}
				}
				g.drawImage(foods, 15+food_x1,food_y1,interval,interval, this);
			}
			else if(mapChoice==2){
				g.drawImage(foods2, 15+food_x1,food_y1,interval,interval, this);
			}
			else
				g.drawImage(foods3, 15+food_x1,food_y1,interval,interval, this);

			if((s.x[0])*interval==food_x1&&(s.y[0])*interval==food_y1){
				if(foods==food.food7.getImage()){ // special food
					if(s.size>3)
						s.size--;
				}
				CountScore(); //increase score 
				if(mapChoice==1){
					foods=food.getFood(); // reget food
					s.size++;
				}
				if(mapChoice==2){
					s.size++;
					if(foods2==food.food8.getImage()){
						timer.setDelay(40);
						special=true;
					}
					foods2=food.getFood2();
				}
				if(mapChoice==3){
					foods3=food.getFood3();
					
					if(foods3==food.food9.getImage()){
						s.size++;
					}
				}
				over=true;
			}
			if(mapChoice==1){
				if(speed<=70){ // to speed up
					if(second%8==0){
						speed++;
						timer.setDelay(100-speed);
					}
				}
			}
			if(special){ // bonus time
				specialtime--;	
						for(int b=0;b<81;b++){
							if(s.x[0]==food.bonus_x[b]&&s.y[0]==food.bonus_y[b]){
								score+=5;
								s.size++;
								food.bonus_x[b]=0;
								food.bonus_y[b]=0;		
							}
							if(food.bonus_x[b]!=0&&food.bonus_y[b]!=0){
								System.out.println(food.bonus_x[b]+"  "+food.bonus_y[b]);
								g.drawImage(food.food10.getImage(),15+(food.bonus_x[b])*interval,food.bonus_y[b]*interval,interval,interval,this);
							}
						}
				if(specialtime==0)
				{
					timer.setDelay(100);
					specialtime=80;
					food.setLocation();
					special=false;
				}
			}
			if(mapChoice==2){ // to bump into barrier
				for(int i=0;i<back.barrier_x.length;i++){
					if(s.x[0]==back.barrier_x[i]&&s.y[0]==back.barrier_y[i]){
						//System.out.print(s.x[0]+"  "+s.y[0]);
						life=false;
						timer.stop();
					}
				}
			}
			if(mapChoice==3){ // to bump into barrier
				for(int i=0;i<back.barrier_x.length;i++){
					if(s.x[0]==back.barrier_x[i]&&s.y[0]==back.barrier_y[i]){
						//System.out.print(s.x[0]+"  "+s.y[0]);
						life=false;
						timer.stop();
					}
				}
			}
			for(int a=1;a<=s.size;a++){ // to bump into snake's body
				if(s.x[0]==s.x[a]&&s.y[0]==s.y[a]){
					life = false;
					timer.stop();
				}
			}
			for(int i =0;i<s.size;i++){ // draw snake
				g.drawImage(snakes[i].getImage(),15+(s.x[i])*interval,s.y[i]*interval,interval,interval,this);
			}
			for(int q=0;q<s.size;q++){
				beforeS[q]=snakes[q];
			}
			g.drawImage(imageScore.getImage(),590,50,180,100,this);
			point.setBounds(650, 200, 80, 100);
			String Sscore = score+"";
			g.setFont(font);
			g.drawString(Sscore,650, 200);
			
			
			if(life==false){ // dead screen
				g.setColor(Color.RED);
				g.setFont(gameover);
				g.drawString("Game Over",2*interval , 10*interval);
				g.setColor(Color.BLACK);
				g.setFont(new Font("Serif",Font.BOLD,50));
				g.drawString("Your Score:"+score,6*interval ,13*interval);
				again.setBounds(560, 450, 250, 100);
				again.setBorderPainted(false);
				again.setContentAreaFilled(false);
				again.setIcon(retu);
				add(again);
				again.addActionListener(this);
			}
			second++;
	    }
		// to get score
		public int getScore(){
			return score;
		}
		 // to increase score
		public void CountScore(){
			score+=10;
		}
		
		public void actionPerformed(ActionEvent e) {
			Menu m = new Menu();
			// to change screen
			m.setVisible(false);
			if(e.getSource()==again){
				mapChoice=m.cho;
				m.setVisible(true);
				this.setVisible(false);
				
			}
			
		}
	}
}