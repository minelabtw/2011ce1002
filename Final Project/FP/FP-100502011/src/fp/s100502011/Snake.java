package fp.s100502011;

import javax.swing.*;
import java.awt.*;
public class Snake {
	public int size=3;
	public int[] x = new int[100];
	public int[] y = new int[100];
	public ImageIcon body1= new ImageIcon("Snake/body1.gif");
	public ImageIcon body2= new ImageIcon("Snake/body2.gif");
	public ImageIcon corner1= new ImageIcon("Snake/corner1.gif");
	public ImageIcon corner2= new ImageIcon("Snake/corner2.gif");
	public ImageIcon corner3= new ImageIcon("Snake/corner3.gif");
	public ImageIcon corner4= new ImageIcon("Snake/corner4.gif");
	public ImageIcon head1= new ImageIcon("Snake/head1.gif");
	public ImageIcon head2= new ImageIcon("Snake/head2.gif");
	public ImageIcon head3= new ImageIcon("Snake/head3.gif");
	public ImageIcon head4= new ImageIcon("Snake/head4.gif");
	public ImageIcon tail1= new ImageIcon("Snake/tail1.gif");
	public ImageIcon tail2= new ImageIcon("Snake/tail2.gif");
	public ImageIcon tail3= new ImageIcon("Snake/tail3.gif");
	public ImageIcon tail4= new ImageIcon("Snake/tail4.gif");
	public Snake(){
	}
	// initial snake's body's location
	public void initSnake(){
		x[0]=2;
		x[1]=1;
		x[2]=0;
		y[0]=0;
		y[1]=0;
		y[2]=0;
	}
	// to get size
	public int getSize(){
		return size;
	}
	// to set size
	public void setSize(int number){
		size = number;
	}
	// to prolong snake's body
	public void prolong(){
		size++;
	}
}