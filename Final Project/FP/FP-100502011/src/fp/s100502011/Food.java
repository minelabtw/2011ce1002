package fp.s100502011;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class Food {
	public ImageIcon ball= new ImageIcon("food/ball.gif");
	public ImageIcon food1 = new ImageIcon("food/7.gif");
	public ImageIcon food2 = new ImageIcon("food/2.gif");
	public ImageIcon food3 = new ImageIcon("food/3.gif");
	public ImageIcon food4 = new ImageIcon("food/4.gif");
	public ImageIcon food5 = new ImageIcon("food/5.gif");
	public ImageIcon food6 = new ImageIcon("food/6.gif");
	public ImageIcon food7 = new ImageIcon("food/8.gif");
	public ImageIcon food8 = new ImageIcon("food/star.gif");
	public ImageIcon food9 = new ImageIcon("food/heart.gif");
	public ImageIcon food10 = new ImageIcon("food/brick.gif");
	public ImageIcon food11 = new ImageIcon("food/reverse.gif");
	public ImageIcon[] foods = new ImageIcon[10];
	public ImageIcon[] foods2 = new ImageIcon[10];
	public ImageIcon[] foods3 = new ImageIcon[10];
	public int[] bonus_x = new int[81];
	public int[] bonus_y = new int[81];
	public int ball_x;
	public int ball_y;
	public Food(){
		//to store picture to array
		foods[0]=food2;
		foods[1]=food3;
		foods[2]=food4;
		foods[3]=food5;
		foods[4]=food7;
		foods2[0]=food9;
		foods2[1]=food8;
		foods2[2]=food6;
		foods2[3]=food4;
		foods2[4]=food5;
		foods3[0]=food2;
		foods3[1]=food3;
		foods3[2]=food4;
		foods3[3]=food5;
		foods3[4]=food9;
	}
	// to get random number
	public int getRandom1(){
		return (int)(Math.random()*5);
	}
	// to get food
	public Image getFood(){
		return foods[getRandom1()].getImage();
		
	}
	public Image getFood2(){
		return foods2[getRandom1()].getImage();
	}
	public Image getFood3(){
		return foods3[getRandom1()].getImage();
	}
	
	// to set bonus's location
	public void setLocation(){
		for(int i=0;i<9;i++){
			if(i==0){
				bonus_x[i]=2;
				bonus_y[i]=2;
			}
			else{
				bonus_x[i]=2+i*2;
				bonus_y[i]=2;
			}
				
		}
		for(int i=0;i<9;i++){
			if(i==0){
				bonus_x[i+9]=2;
				bonus_y[i+9]=4;
			}
			else{
				bonus_x[i+9]=2+i*2;
				bonus_y[i+9]=4;
			}
				
		}
		for(int i=0;i<9;i++){
			if(i==0){
				bonus_x[i+18]=2;
				bonus_y[i+18]=6;
			}
			else{
				bonus_x[i+18]=2+i*2;
				bonus_y[i+18]=6;
			}
				
		}
		for(int i=0;i<9;i++){
			if(i==0){
				bonus_x[i+27]=2;
				bonus_y[i+27]=8;
			}
			else{
				bonus_x[i+27]=2+i*2;
				bonus_y[i+27]=8;
			}
				
		}
		for(int i=0;i<9;i++){
			if(i==0){
				bonus_x[i+36]=2;
				bonus_y[i+36]=10;
			}
			else{
				bonus_x[i+36]=2+i*2;
				bonus_y[i+36]=10;
			}
				
		}
		for(int i=0;i<9;i++){
			if(i==0){
				bonus_x[i+45]=2;
				bonus_y[i+45]=12;
			}
			else{
				bonus_x[i+45]=2+i*2;
				bonus_y[i+45]=12;
			}
				
		}
		for(int i=0;i<9;i++){
			if(i==0){
				bonus_x[i+54]=2;
				bonus_y[i+54]=14;
			}
			else{
				bonus_x[i+54]=2+i*2;
				bonus_y[i+54]=14;
			}
				
		}
		for(int i=0;i<9;i++){
			if(i==0){
				bonus_x[i+63]=2;
				bonus_y[i+63]=16;
			}
			else{
				bonus_x[i+63]=2+i*2;
				bonus_y[i+63]=16;
			}
				
		}
		for(int i=0;i<9;i++){
			if(i==0){
				bonus_x[i+72]=2;
				bonus_y[i+72]=18;
			}
			else{
				bonus_x[i+72]=2+i*2;
				bonus_y[i+72]=18;
			}
		}
	}
}
