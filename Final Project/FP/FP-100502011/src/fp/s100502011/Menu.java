package fp.s100502011;

import java.awt.Font;
import java.awt.event.*;
import java.awt.Graphics;

import javax.swing.*;

import java.awt.*;
public class Menu extends JFrame implements ActionListener{
	public int screen=0;
	public JButton Stage3 = new JButton("Stage3");
	public JButton Stage1 = new JButton("Stage1");
	public JButton Stage2 = new JButton("Stage2");
	public JPanel face = new JPanel();
	public JPanel panel = new JPanel();
	public JPanel image = new JPanel();
	public JPanel choose = new JPanel();
	public JPanel imagep = new JPanel();
	public JLabel topic =  new JLabel("");
	public JButton start = new JButton("");
	public JButton score = new JButton("");
	public JButton Option = new JButton("");
	public ImageIcon back4 = new ImageIcon("button/back3.gif");
	Image b=back4.getImage();
	public ImageIcon starti = new ImageIcon("button/button2.gif");
	public ImageIcon seti = new ImageIcon("button/button3.gif");
	public ImageIcon exiti = new ImageIcon("button/button4.gif");
	public ImageIcon highi = new ImageIcon("button/button5.gif");
	public ImageIcon topici = new ImageIcon("button/topic.gif");
	public JButton exit = new JButton("");
	public int cho=2;
	GameInterface g = new GameInterface();
	public Menu(){
		// to set frame
		start.setIcon(starti);
		score.setIcon(highi);
		exit.setIcon(exiti);
		Option.setIcon(seti);
		choose.add(start);
		choose.add(Option);
		choose.add(score);
		choose.add(exit);
		start.setBorderPainted(false);
		start.setContentAreaFilled(false);
		score.setBorderPainted(false);
		score.setContentAreaFilled(false);
		exit.setBorderPainted(false);
		exit.setContentAreaFilled(false);
		Option.setBorderPainted(false);
		Option.setContentAreaFilled(false);
		
		Stage1.addActionListener(this);
		Stage2.addActionListener(this);
		Stage3.addActionListener(this);
		start.addActionListener(this);
		score.addActionListener(this);
		exit.addActionListener(this);
		Option.addActionListener(this);
		topic.setBounds(170,60,500,400);
		face.setLayout(null);
		topic.setIcon(topici);
		choose.setBounds(0, 460,800, 200);
		face.add(topic);
		face.add(choose,BorderLayout.SOUTH);
		face.add(new DrawImage());
		face.setOpaque(false);
		add(face);
		this.setSize(800,600);
		setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		panel.setLayout(null);
		Stage1.setBounds(200, 150, 100, 50);
		panel.add(Stage1);
		Stage2.setBounds(300,150, 100, 50);
		panel.add(Stage2);
		Stage3.setBounds(400, 150, 100, 50);
		panel.add(Stage3);
	}
	public void actionPerformed(ActionEvent e){
		
		JFrame f = new JFrame();
		f.setSize(800,600);
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(false);
		f.add(panel);
		g.setVisible(false);
		 // to change screen
		if(e.getSource()==start){
			g.setVisible(true);
			this.setVisible(false);
			
		}
		if(e.getSource()==Option){
			f.setVisible(true);
			this.setVisible(false);
		}
		if(e.getSource()==Stage1){
			f.setVisible(false);
			this.setVisible(true);
			g.mapChoice=1;
		}
		if(e.getSource()==exit){
			System.exit(0);
		}
		if(e.getSource()==Stage2){
			f.setVisible(false);
			this.setVisible(true);
			cho=2;
		}
		if(e.getSource()==Stage3){
			f.setVisible(false);
			this.setVisible(true);
			cho=3;
		}
	}
	class DrawImage extends JPanel{
		protected void paintComponent(Graphics g){
			super.paintComponent(g);
			g.drawImage(b,0,0,800,600,this);
		}
	}
}
