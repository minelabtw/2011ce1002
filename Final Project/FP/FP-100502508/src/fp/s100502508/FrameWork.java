package fp.s100502508;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener,KeyListener
{	
	private int Daccelaration=7;
	private int acceleration=1;
	private int CoordinateX;
	private int CoordinateY;
	private int direction=-1;
	private int timekeeper=0;
	private static int MyChoice;
	private static int YourChoice;
	private int difficulty;
	private int Times=0;
	private int times=1;
	private int firetimes=1;
	private int soundtimes=1;
	private int player;
	private int burns=30;
	private int velocity=3;
	private int password;
	private int Rseat=656;
	private int Lseat=97;
	private boolean Myclicktimes=true;
	private boolean Yourclicktimes=false;
	private boolean ifMyshift=true;
	private boolean ifYourshift=false;
	private boolean RifFired=true;
	private boolean LifFired=true;
	private boolean RFire=true;
	private boolean LFire=true;
	private String input=new String();
	private double Acceleration;
	private final int NUMWEAPON=4;
	private boolean[] Lused=new boolean[NUMWEAPON];
	private boolean[] Rused=new boolean[NUMWEAPON];
	private boolean[] ReRused=new boolean[NUMWEAPON];
	private boolean[] ReLused=new boolean[NUMWEAPON];
	
	private ImageIcon imageRprotect=new ImageIcon("image/RProtect.png");
	private ImageIcon imageLprotect=new ImageIcon("image/LProtect.png");
	private ImageIcon imageRfire=new ImageIcon("image/RFire.png");
	private ImageIcon imageLfire=new ImageIcon("image/LFire.png");
	private ImageIcon[] imageLStanby=new ImageIcon[3];
	private ImageIcon[] imageRStanby=new ImageIcon[3];
	private ImageIcon[] imageRHappy=new ImageIcon[3];
	private ImageIcon[] imageLHappy=new ImageIcon[3];
	private ImageIcon[] imageLthrow=new ImageIcon[3];
	private ImageIcon[] imageRthrow=new ImageIcon[3];
	private ImageIcon[] imageRObject=new ImageIcon[3];
	private ImageIcon[] imageLObject=new ImageIcon[3];
	private ImageIcon[] imageLBehit=new ImageIcon[3];
	private ImageIcon[] imageRBehit=new ImageIcon[3];
	private ImageIcon[] imageLNothit=new ImageIcon[3];
	private ImageIcon[] imageRNothit=new ImageIcon[3];
	private ImageIcon[] imageLDodge=new ImageIcon[3];
	private ImageIcon[] imageRDodge=new ImageIcon[3];
	private ImageIcon[] imageLBefired=new ImageIcon[3];
	private ImageIcon[] imageRBefired=new ImageIcon[3];
	private ImageIcon[] imageWeapon=new ImageIcon[NUMWEAPON];
	
	private JButton ButtonReplay=new JButton("Replay");
	private JButton ButtonMenu=new JButton("Menu");
	private JButton[] RButtonPowerup=new JButton[NUMWEAPON];
	private JButton[] LButtonPowerup=new JButton[NUMWEAPON];
	
	JLabel Plabel11=new JLabel();
	JLabel Plabel21=new JLabel();
	JLabel Olabel=new JLabel();
	JLabel RProtect=new JLabel(imageRprotect);
	JLabel LProtect=new JLabel(imageLprotect);
	
	Background background=new Background();//create a panel
	Timer timer1=new Timer(20,new TimeListener());//create timers
	Timer timer2=new Timer(20,new TimeListener());
	Timer timer3=new Timer(100,new TimeListener2());
	
	Font font1=new Font("SansSerif",Font.BOLD,18);//create fonts
	Font font2=new Font("SansSerif",Font.BOLD,30);
	
	Sound sound=new Sound();
	
	public FrameWork()
	{
		for(int a=1;a<=3;a++)
		{
			imageLStanby[a-1]=new ImageIcon("image/LStandby"+a+".png");//store images into arrays
			imageLHappy[a-1]=new ImageIcon("image/LHappy"+a+".png");
			imageLthrow[a-1]=new ImageIcon("image/Lthrow"+a+".png");
			imageLObject[a-1]=new ImageIcon("image/LObject"+a+".png");
			imageLBehit[a-1]=new ImageIcon("image/LBehit"+a+".png");
			imageLNothit[a-1]=new ImageIcon("image/LNothit"+a+".png");
			imageLDodge[a-1]=new ImageIcon("image/LDodge"+a+".png");
			imageLBefired[a-1]=new ImageIcon("image/LBefired"+a+".png");
			imageRStanby[a-1]=new ImageIcon("image/RStandby"+a+".png");
			imageRHappy[a-1]=new ImageIcon("image/RHappy"+a+".png");
			imageRthrow[a-1]=new ImageIcon("image/Rthrow"+a+".png");
			imageRObject[a-1]=new ImageIcon("image/RObject"+a+".png");
			imageRBehit[a-1]=new ImageIcon("image/RBehit"+a+".png");
			imageRNothit[a-1]=new ImageIcon("image/RNothit"+a+".png");
			imageRDodge[a-1]=new ImageIcon("image/RDodge"+a+".png");
			imageRBefired[a-1]=new ImageIcon("image/RBefired"+a+".png");
		}
		
		for(int a=1;a<=NUMWEAPON;a++)//設定道具
		{
			imageWeapon[a-1]=new ImageIcon("image/Weapon"+a+".jpg");
			RButtonPowerup[a-1]=new JButton(imageWeapon[a-1]);
			LButtonPowerup[a-1]=new JButton(imageWeapon[a-1]);
			Rused[a-1]=false;
			Lused[a-1]=false;
			ReRused[a-1]=false;
			ReLused[a-1]=false;
			RButtonPowerup[a-1].setBounds(480+45*(a-1), 55, 35, 35);//設定道具的位置和大小
			LButtonPowerup[a-1].setBounds(80+45*(a-1), 55, 35, 35);
			background.add(RButtonPowerup[a-1]);
			background.add(LButtonPowerup[a-1]);
			RButtonPowerup[a-1].addActionListener(this);//register listener with buttons
			LButtonPowerup[a-1].addActionListener(this);
		}
		
		background.setLayout(null);
		background.add(Plabel11);
		background.add(Plabel21);
		background.add(Olabel);
		background.add(RProtect);
		background.add(LProtect);
		player2();
		
		add(background);
		//register listener with buttons
		ButtonReplay.addActionListener(this);
		ButtonMenu.addActionListener(this);
		
		setFocusable(true);//set focus
		addKeyListener(this);//register listener
		addMouseListener(new MouseListener() 
		{
			public void mouseClicked(MouseEvent e) 
			{
			}
			public void mousePressed(MouseEvent e)//按著滑鼠會發生的事 
			{
				if(direction==1)//左方攻擊回合
				{
					if((e.getX()>=Lseat&&e.getX()<=Lseat+50)&&(e.getY()>=350&&e.getY()<=450)&&Yourclicktimes&&player==1)
					{
						timer3.start();//timer start
						background.IfAttack(true);//pass parameter to background
						ifYourshift=false;
					}
				}
				else//右方攻擊回合 
				{
					if((e.getX()>=Rseat&&e.getX()<=Rseat+50)&&(e.getY()>=350&&e.getY()<=450)&&Myclicktimes)
					{
						timer3.start();//timer start
						background.IfAttack(true);//pass parameter to background
						ifMyshift=false;
					}
				}
			}
			public void mouseEntered(MouseEvent e) 
			{	
			}
			public void mouseExited(MouseEvent e) 
			{	
			}
			public void mouseReleased(MouseEvent e)//滑鼠放開時會發生的事 
			{
				timer3.stop();//timer stop
				background.IfAttack(false);//pass parameter to background
				if(direction==1)//左方攻擊回合
				{
					if(velocity!=3&&Yourclicktimes&&player==1)
					{
						sound.getSound(2).setFramePosition(0);
						sound.getSound(2).start();//發出音效
						timer2.start();//timer start
						velocity+=background.getWindspeed();//速度受風影響
						Acceleration=(2*background.getCenterY()*10*10)/(30*30);//計算加速度
						Plabel11.setIcon(imageRthrow[YourChoice]);
						Olabel.setVisible(true);
						Yourclicktimes=false;
					}
				}
				else//右方攻擊回合  
				{
					if(velocity!=3&&Myclicktimes)
					{
						sound.getSound(2).setFramePosition(0);
						sound.getSound(2).start();//發出音效
						timer1.start();//timer start
						velocity-=background.getWindspeed();//速度受風影響
						Acceleration=(2*background.getCenterY()*10*10)/(30*30);//計算加速度
						Plabel21.setIcon(imageRthrow[MyChoice]);
						Olabel.setVisible(true);
						Myclicktimes=false;
					}
				}
			}
		});
	}
	//implement actionPerformed
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()==ButtonReplay)
		{
			FrameWork f=new FrameWork();//create a frame
			f.setTitle("FinalProject");//set the frame title
			f.setSize(800, 500);//set the frame size
			f.setLocationRelativeTo(null);//center a frame
			f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			f.setVisible(true);//display the frame
			f.setResizable(false);//can't zoom the window
			f.setDifficulty(difficulty);
			f.setplayer(player);
			setVisible(false);//vanish the frame
		}
		else if(e.getSource()==ButtonMenu)
		{
			FinalProject p1=new FinalProject();//create a frame
			p1.setTitle("FinalProject");//set the frame title
			p1.setSize(800, 500);//set the frame size
			p1.setLocationRelativeTo(null);//center a frame
			p1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			p1.setVisible(true);//display the frame
			p1.setResizable(false);//can't zoom the window
			setVisible(false);//vanish the frame
		}
		for(int a=0;a<NUMWEAPON;a++)
		{
			if(e.getSource()==RButtonPowerup[a])
			{
				if(a==0)
				{
					background.setdeltablood(150);
				}
				else if(a==1) 
				{
					RFire=false;
					background.setdeltablood(100);
					Olabel.setIcon(imageRfire);
				}
				else if(a==2)
				{
					ReRused[a]=true;
				}
				else if(a==3)
				{
					ReRused[a]=true;
					RProtect.setBounds(Rseat-40, 305, 130, 130);//設定防護罩的位置和大小
					RProtect.setVisible(true);
					background.updateUI();//refresh the panel
				}
				Rused[a]=true;
				RButtonPowerup[a].setVisible(false);
			}
			else if(e.getSource()==LButtonPowerup[a])
			{
				if(a==0)
				{
					background.setdeltablood(150);
				}
				else if(a==1) 
				{
					LFire=false;
					background.setdeltablood(100);
					Olabel.setIcon(imageLfire);
				}
				else if(a==2)
				{
					ReLused[a]=true;
				}
				else if(a==3)
				{
					ReLused[a]=true;
					LProtect.setBounds(Lseat-40, 305, 130, 130);//設定防護罩的位置和大小
					background.updateUI();//refresh the panel
					LProtect.setVisible(true);
				}
				Lused[a]=true;
				LButtonPowerup[a].setVisible(false);
			}
		}
		sound.getSound(0).setFramePosition(0);
		sound.getSound(0).start();//發出音效
		repaint();//repaint panel
	}
	
	public void keyTyped(KeyEvent e) 
	{
	}
	public void keyPressed(KeyEvent e) 
	{
		if(e.getKeyCode()==KeyEvent.VK_ENTER)//press the enter key
		{
			input=JOptionPane.showInputDialog(null, "Enter the password", "Input", JOptionPane.QUESTION_MESSAGE);
			try
			{
				if(judge()>0)//number of letters is large than zero
				{
					throw new InvalidPasswordException(input);//error happen and throw to InvalidPasswordException class
				}
				else //number of letters is equal zero
				{
					password=Integer.parseInt(input);//convert string to integer
					if(password!=1234567890)
					{
						throw new InvalidPasswordException(input);//error happen and throw to InvalidPasswordException class
					}
				}
				if(password==1234567890)
				{
					usesecrete();//call usesecrete methed
					JOptionPane.showMessageDialog(null,"回復所有異常和道具", "Message", JOptionPane.INFORMATION_MESSAGE);
				}
			}
			catch (Exception ex)//handle error 
			{
				JOptionPane.showMessageDialog(null,"回到遊戲中", "Message", JOptionPane.INFORMATION_MESSAGE);
			}
		}
		else if(e.getKeyCode()==KeyEvent.VK_RIGHT)//press the right key
		{
			if(direction==1)//左方攻擊回合
			{
				if(player==1&&ifYourshift)
				{
					Lseat+=10;//角色往右移
					if(Lseat>230)//邊界
					{
						Lseat=230;
					}
					Plabel11.setBounds(Lseat, 325, 60, 100);//設定左邊玩家的位置和大小
				}
			}
			else//右方攻擊回合  
			{
				if(ifMyshift)
				{
					Rseat+=10;//角色往右移
					if(Rseat>740)//邊界
					{
						Rseat=740;
					}
					Plabel21.setBounds(Rseat, 325, 60, 100);//設定右邊玩家的位置和大小
				}
			}
			RProtect.setBounds(Rseat-40, 305, 130, 130);//設定防護罩的位置和大小
			LProtect.setBounds(Lseat-40, 305, 130, 130);
			if(!ReRused[3])
			{
				RProtect.setVisible(false);
			}
			if(!ReLused[3])
			{
				LProtect.setVisible(false);
			}
		}
		else if(e.getKeyCode()==KeyEvent.VK_LEFT)//press the left key
		{
			if(direction==1)//左方攻擊回合
			{
				if(player==1&&ifYourshift)
				{
					Lseat-=10;//角色往左移
					if(Lseat<0)//邊界
					{
						Lseat=0;
					}
				}
				Plabel11.setBounds(Lseat, 325, 100, 100);//設定左邊玩家的位置和大小
			}
			else//右方攻擊回合  
			{
				if(ifMyshift)
				{
					Rseat-=10;//角色往左移
					if(Rseat<510)//邊界
					{
						Rseat=510;
					}
					Plabel21.setBounds(Rseat, 325, 100, 100);//設定右邊玩家的位置和大小
				}
			}
			RProtect.setBounds(Rseat-40, 305, 130, 130);//設定防護罩的位置和大小
			LProtect.setBounds(Lseat-40, 305, 130, 130);
			if(!ReRused[3])
			{
				RProtect.setVisible(false);
			}
			if(!ReLused[3])
			{
				LProtect.setVisible(false);
			}
		}
	}
	public void keyReleased(KeyEvent e) 
	{
	}
	
	public void player1()
	{
		Plabel11.setIcon(imageLStanby[YourChoice]);
		Plabel11.setBounds(Lseat, 325, 60, 100);//設定左邊玩家的位置和大小
		Plabel21.setIcon(imageRStanby[MyChoice]);
		Plabel21.setBounds(Rseat, 325, 60, 100);//設定右邊玩家的位置和大小
		Olabel.setIcon(imageLObject[YourChoice]);
		Olabel.setVisible(false);
		Lused[3]=false;
		Yourclicktimes=true;
		ifYourshift=true;
		setInitial();
		if(player==0)
		{
			setvelocity();
			timer2.start();//timer start
		}
		else
		{
			background.setvelocity(0);
		}
		if(ReLused[3])
		{
			LProtect.setVisible(false);
			background.updateUI();//refresh the panel
		}
	}
	
	public void player2()
	{
		Plabel11.setIcon(imageLStanby[YourChoice]);
		Plabel11.setBounds(Lseat, 325, 60, 100);//設定左邊玩家的位置和大小
		Plabel21.setIcon(imageRStanby[MyChoice]);
		Plabel21.setBounds(Rseat, 325, 60, 100);//設定右邊玩家的位置和大小
		Olabel.setIcon(imageRObject[MyChoice]);
		Olabel.setVisible(false);
		setInitial();
		background.setvelocity(0);
		background.setWind();
		Rused[3]=false;
		Myclicktimes=true;
		ifMyshift=true;
		if(ReRused[3])
		{
			RProtect.setVisible(false);
			background.updateUI();//refresh the panel
		}
	}
	
	public void setInitial()
	{
		Daccelaration=7;
		velocity=3;
		CalX(Daccelaration);
		CalY(Daccelaration);
		timekeeper=0;
		times=1;
		soundtimes=1;
		firetimes=1;
		background.setdeltablood(100);
		displaybutton();
	}
	
	public void CalX(int acceleration)//丟出物品的X座標
	{
		if(direction==1)//左方攻擊回合
		{
			CoordinateX=(Lseat)+velocity*acceleration;
		}
		else//右方攻擊回合
		{
			CoordinateX=(Rseat)-velocity*acceleration;
		}
	}
	
	public void CalY(int acceleration)//丟出物品的Y座標
	{
		CoordinateY=background.getCenterY()-40+(int)((acceleration-30)*(acceleration-30)*Acceleration/(2*10*10));
	}
	
	public void displaybutton()//是否顯示道具的出現
	{
		if(direction==-1)
		{
			for(int a=0;a<NUMWEAPON;a++)
			{
				if(Rused[a]==false)
				{
					RButtonPowerup[a].setVisible(true);
					if(a==1)
					{
						if(RFire)
						{
							RButtonPowerup[a].setVisible(true);
						}
						else 
						{
							RButtonPowerup[a].setVisible(false);
						}
					}
					if(ReRused[a])
					{
						if(a!=1)
						{
							RButtonPowerup[a].setVisible(false);
						}
					}
				}
				LButtonPowerup[a].setVisible(false);
			}
		}
		else 
		{
			for(int a=0;a<NUMWEAPON;a++)
			{
				RButtonPowerup[a].setVisible(false);
			}
			if(player==1)
			{
				for(int a=0;a<NUMWEAPON;a++)
				{
					if(Lused[a]==false)
					{
						LButtonPowerup[a].setVisible(true);
						if(a==1)
						{
							if(LFire)
							{
								LButtonPowerup[a].setVisible(true);
							}
							else 
							{
								LButtonPowerup[a].setVisible(false);
							}
						}
						if(ReLused[a])
						{
							if(a!=1)
							{
								LButtonPowerup[a].setVisible(false);
							}
						}
					}
				}
			}
		}
	}
	
	public void passdirection()//傳現在是誰的回合到background李
	{
		background.setdirection(direction);
	}
	
	public void setvelocity()//電腦隨難度隨機設定速度
	{
		velocity=(int)((Rseat-Lseat)/Acceleration);//必定命中的速度
		velocity+=((int)(Math.random()*10)-5);//隨機加減速度
		switch(difficulty)//根據不同的難度設定速度
		{
			case 1://easy
				velocity+=background.getWindspeed();//速度受風影響
				if(velocity!=10&velocity!=11)
				{
					Times++;
					if(Times%5==0)//五次沒中後 一定命中
					{
						velocity=(int)((Rseat-Lseat)/Acceleration);
					}
				}
				else 
				{
					setvelocity();
				}
				break;
			case 2://normal
				if(Times==0)//兩次射擊中 至少有一次會命中
				{
					Times++;
					velocity=(int) ((Rseat-Lseat)/Acceleration);
				}
				else 
				{
					Times--;
					velocity+=background.getWindspeed();//速度受風影響
				}
				System.out.println(velocity);
				break;
			case 3://hard
				velocity=(int) ((Rseat-Lseat)/Acceleration);//每次必命中
				System.out.println(velocity);
				break;
			default :
				break;
		}
	}
	
	public void setplayer(int number)//set player
	{
		player=number;
	}
	
	public void setMyChoose(int number)//設定右邊玩家的人物選擇
	{
		MyChoice=number;
	}
	
	public void setYourChoose(int number)//設定左邊玩家的人物選擇
	{
		YourChoice=number;
	}
	
	public void setMyimageicon(int number)//設定右邊玩家的人物圖片
	{
		Plabel21.setIcon(imageRStanby[number]);
		Olabel.setIcon(imageRObject[number]);
	}
	
	public void setEnemyimageicon(int number)//設定左邊玩家的人物圖片
	{
		Plabel11.setIcon(imageLStanby[number]);
	}
	
	public void Displayfinalbutton()//遊戲結束後 顯示最後的按鈕
	{
		ButtonReplay.setBounds(200, 250, 150, 50);//設定Replay按鈕的位置和大小
		ButtonMenu.setBounds(450, 250, 150, 50);//設定Menu按鈕位置和大小
		ButtonReplay.setFont(font2);
		ButtonMenu.setFont(font2);
		background.add(ButtonReplay);
		background.add(ButtonMenu);
		background.setresult(true);
		for(int a=0;a<4;a++)//道具全不顯示		{
			RButtonPowerup[a].setVisible(false);
			LButtonPowerup[a].setVisible(false);
		}
	}
	
	public void setDifficulty(int degree)//set difficulty
	{
		difficulty=degree;
	}
	
	public void usesecrete()//金手指
	{
		for(int a=0;a<NUMWEAPON;a++)//道具和異常狀態全部恢復
		{
			if(direction==1)//左方攻擊回合
			{
				LButtonPowerup[a].setVisible(true);
				Lused[a]=false;
				ReLused[a]=false;
				LFire=true;
				if(a==1)
				{
					ReRused[a]=false;
					ReLused[a]=true;
				}
			}
			else//右方攻擊回合
			{
				RButtonPowerup[a].setVisible(true);
				Rused[a]=false;
				ReRused[a]=false;
				RFire=true;
				if(a==1)
				{
					ReLused[a]=false;
					ReRused[a]=true;
				}
			}
		}
	}
	
	public int judge()//count the number of letters
	{
		int count=0;
		for(int a=0;a<input.length();a++)
		{
			if(Character.isLetter(input.charAt(a)))
			{
				count++;
			}
		}
		return count;
	}//end judge method
	
	public void setenemy()//設定左邊人物的攻擊
	{
		Olabel.setVisible(true);
		Plabel11.setIcon(imageLthrow[YourChoice]);
		Daccelaration+=acceleration;
		if((CoordinateX>=Rseat-50&&CoordinateX<=Rseat+30)&&(CoordinateY>=300&&CoordinateY<=350))//擊中右邊的人
		{
			if(times==1)//跑一次
			{
				if(!Rused[3])//沒用防護罩
				{
					Plabel11.setIcon(imageLHappy[YourChoice]);
					Plabel21.setIcon(imageRBehit[MyChoice]);
					sound.getSound(1).setFramePosition(0);
					sound.getSound(1).start();//發出音效
					background.setBloodx2();//扣血
					if(Lused[1])//被燒傷
					{
						Lused[1]=false;
						ReLused[1]=true;
					}
				}
				else 
				{
					sound.getSound(3).setFramePosition(0);
					sound.getSound(3).start();//發出音效
					Plabel11.setIcon(imageLNothit[YourChoice]);
					Plabel21.setIcon(imageRDodge[MyChoice]);
					RProtect.setVisible(false);
					Rused[3]=false;
				}
				times++; 
			}
			Olabel.setBounds(CoordinateX, CoordinateY+20, 50, 50);//設定丟出物品的位置和大小
			RProtect.setVisible(false);
			background.updateUI();//refresh the panel
			if(ReLused[1]&&RifFired)//被燒傷
			{
				if(Daccelaration>=100&&Daccelaration<160)
				{
					Plabel11.setIcon(imageLHappy[YourChoice]);
					Plabel21.setIcon(imageRBefired[MyChoice]);
					if(firetimes==1)//扣一次燒傷的血
					{
						sound.getSound(1).setFramePosition(0);
						sound.getSound(1).start();//發出音效
						background.setdeltablood(burns);//燒傷
						background.setBloodx2();//扣血
						firetimes++;
					}
					background.updateUI();//refresh the panel
				}
				else if(Daccelaration>=160)
				{	
					if(background.getBloodx2()<=450)//血被扣完
					{
						timer2.stop();//timer stop
						Displayfinalbutton();
					}
					else//血沒被扣完 
					{
						direction*=-1;//換對方的回合
						player2();
						timer2.stop();//timer stop
						passdirection();//call passdirection method
					}
				}
			}
			else 
			{
				if(Daccelaration>=100)
				{	
					if(background.getBloodx2()<=450)//血被扣完
					{
						timer2.stop();//timer stop
						Displayfinalbutton();
					}
					else//血沒被扣完 
					{
						direction*=-1;//換對方的回合
						player2();
						timer2.stop();//timer stop
						passdirection();//call passdirection method
					}
				}
			}
		}
		else if((CoordinateX>=330&&CoordinateX<=400)&&(CoordinateY>=250&&CoordinateY<=500))//打到中間的牆
		{
			if(Lused[1])//被燒傷
			{
				Lused[1]=false;
				RifFired=false;
			}
			if(soundtimes==1)//發出一次音效
			{
				sound.getSound(3).setFramePosition(0);
				sound.getSound(3).start();//發出音效
				soundtimes++;
			}
			if(Daccelaration>=50)
			{
				Olabel.setBounds(CoordinateX, 425, 50, 50);//設定丟出物品的位置和大小
				Plabel11.setIcon(imageLNothit[YourChoice]);
				Plabel21.setIcon(imageRDodge[MyChoice]);
				if(background.getBloodx2()<=450)//血被扣完
				{
					Plabel11.setIcon(imageLHappy[YourChoice]);
					Plabel21.setIcon(imageRBefired[MyChoice]);
					timer2.stop();//timer stop
					Displayfinalbutton();
				}
				else//血沒被扣完 
				{
					if(ReLused[1])//被燒傷
					{
						if(Daccelaration>100&&Daccelaration<150)
						{
							Plabel11.setIcon(imageLHappy[YourChoice]);
							Plabel21.setIcon(imageRBefired[MyChoice]);
							if(times==1)
							{
								sound.getSound(1).setFramePosition(0);
								sound.getSound(1).start();//發出音效
								background.setdeltablood(burns);//燒傷
								background.setBloodx2();//扣血
							}
							times++;
						}
						if(Daccelaration>150)
						{
							direction*=-1;//換對方的回合
							player2();
							timer2.stop();//timer stop
							passdirection();//call passdirection method
						}
					}
					else 
					{
						if(Daccelaration>100)
						{
							direction*=-1;//換對方的回合
							player2();
							timer2.stop();//timer stop
							passdirection();//call passdirection method
						}
					}
				}
			}
		}
		else//沒打到敵人 
		{
			CalX(Daccelaration);
			CalY(Daccelaration);
			Olabel.setBounds(CoordinateX, CoordinateY-50, 50, 50);//設定丟出物品的位置和大小
			if(CoordinateY>400)
			{
				if(background.getBloodx2()<=450)//血被扣完
				{
					Plabel11.setIcon(imageLHappy[YourChoice]);
					Plabel21.setIcon(imageRBefired[MyChoice]);
					timer2.stop();//timer stop
					Displayfinalbutton();
				}
				else//血沒被扣完 
				{
					if(CoordinateY<=2000)
					{
						if(soundtimes==1)//發出一次音效
						{
							sound.getSound(3).setFramePosition(0);
							sound.getSound(3).start();//發出音效
							soundtimes++;
						}
						Plabel11.setIcon(imageLNothit[YourChoice]);
						Plabel21.setIcon(imageRDodge[MyChoice]);
					}
					if(ReLused[1])//被燒傷
					{
						if(CoordinateY>2000&&CoordinateY<3500)
						{
							Plabel11.setIcon(imageLHappy[YourChoice]);
							Plabel21.setIcon(imageRBefired[MyChoice]);
							if(times==1)
							{
								sound.getSound(1).setFramePosition(0);
								sound.getSound(1).start();//發出音效
								background.setdeltablood(burns);//燒傷
								background.setBloodx2();//扣血
							}
							times++;
						}
						if(CoordinateY>5000)
						{
							direction*=-1;//換對方的回合
							player2();
							timer2.stop();//timer stop
							passdirection();//call passdirection method
						}
					}
					else 
					{
						if(CoordinateY>2000)
						{
							if(Lused[1])//被燒傷
							{
								Lused[1]=false;
								RifFired=false;
							}
							direction*=-1;//換對方的回合
							player2();
							timer2.stop();//timer stop
							passdirection();//call passdirection method
						}
					}
				}
			}
		}
	}
	
	class TimeListener implements ActionListener//Handle ActionEvent
	{
		public void actionPerformed(ActionEvent e)//event handler
		{
			background.setplayer(player);
			acceleration=1;
			if(direction==1)//左方攻擊回合
			{
				if(player==0)
				{
					if(timekeeper==50)
					{
						if(soundtimes==1)//發出一次音效
						{
							sound.getSound(2).setFramePosition(0);
							sound.getSound(2).start();//發出音效
							soundtimes++;
						}
						setenemy();//call setenemy method
					}
					else 
					{
						timekeeper+=acceleration;
					}
				}
				else 
				{
					if(Lused[2]==true)//道具必定命中啟動
					{
						velocity=(int)((Rseat-Lseat)/Acceleration);
						Lused[2]=false;
					}
					setenemy();//call setenemy method
				}
				if(background.getBloodx2()<=450)//血被扣完
				{
					Displayfinalbutton();
				}
			}
			else//右方攻擊回合
			{
				if(Rused[2]==true)//道具必定命中啟動
				{
					velocity=(int) ((Rseat-Lseat)/Acceleration);
					Rused[2]=false;
				}
				Daccelaration += acceleration;
				if ((CoordinateX >= Lseat && CoordinateX <= Lseat+80)&& (CoordinateY >= 300 && CoordinateY <= 350))//打到左方敵人 
				{
					if(times==1)
					{
						if(!Lused[3])//沒用防護罩
						{
							Plabel11.setIcon(imageLBehit[YourChoice]);
							Plabel21.setIcon(imageRHappy[MyChoice]);
							sound.getSound(1).setFramePosition(0);
							sound.getSound(1).start();//發出音效
							background.setBloodx1();//扣血
							if(Rused[1])//被燒傷
							{
								Rused[1]=false;
								ReRused[1]=true;
							}
						}
						else 
						{
							sound.getSound(3).setFramePosition(0);
							sound.getSound(3).start();//發出音效
							Plabel11.setIcon(imageLDodge[YourChoice]);
							Plabel21.setIcon(imageRNothit[MyChoice]);
							LProtect.setVisible(false);
							Lused[3]=false;
						}
						times++;
					}
					Olabel.setBounds(CoordinateX, CoordinateY + 20, 50, 50);//設定丟出物品的位置和大小
					background.updateUI();//refresh the panel
					if(ReRused[1]&&LifFired)//被燒傷
					{
						if(Daccelaration>=100&&Daccelaration<160)
						{
							Plabel11.setIcon(imageLBefired[YourChoice]);
							Plabel21.setIcon(imageRHappy[MyChoice]);
							if(firetimes==1)//扣一次燒傷的血
							{
								sound.getSound(1).setFramePosition(0);
								sound.getSound(1).start();//發出音效
								background.setdeltablood(burns);//燒傷
								background.setBloodx1();//扣血
								firetimes++;
							}
							background.updateUI();//refresh the panel
						}
						else if(Daccelaration>=160)
						{
							if(background.getBloodx1()<=50)
							{
								timer1.stop();//timer stop
								Displayfinalbutton();
							}
							else 
							{
								direction*=-1;//換對方的回合
								player1();
								timer1.stop();//timer stop
								passdirection();//call passdirection method
							}
						}
					}
					else 
					{
						if(Daccelaration>=100)
						{
							if(background.getBloodx1()<=50)
							{
								timer1.stop();//timer stop
								Displayfinalbutton();
							}
							else 
							{
								direction*=-1;//換對方的回合
								player1();
								timer1.stop();//timer stop
								passdirection();//call passdirection method
							}
						}
					}
				} 
				else if((CoordinateX>=350&&CoordinateX<=420)&&(CoordinateY>=250&&CoordinateY<=500))//打到中間的牆
				{
					if(Rused[1])//被燒傷
					{
						Rused[1]=false;
						LifFired=false;
					}
					if(soundtimes==1)//發出一次音效
					{
						sound.getSound(3).setFramePosition(0);
						sound.getSound(3).start();//發出音效
						soundtimes++;
					}
					if(Daccelaration>=50)
					{
						Olabel.setBounds(CoordinateX, 425, 50, 50);//設定丟出物品的位置和大小
						Plabel11.setIcon(imageLDodge[YourChoice]);
						Plabel21.setIcon(imageRNothit[MyChoice]);
						if(background.getBloodx1()<=50)
						{
							Plabel11.setIcon(imageLBefired[YourChoice]);
							Plabel21.setIcon(imageRHappy[MyChoice]);
							timer1.stop();//timer stop
							Displayfinalbutton();
						}
						else 
						{
							if(ReRused[1])//被燒傷
							{
								if(Daccelaration>100&&Daccelaration<150)
								{
									Plabel11.setIcon(imageLBefired[YourChoice]);
									Plabel21.setIcon(imageRHappy[MyChoice]);
									if(times==1)
									{
										
										sound.getSound(1).setFramePosition(0);
										sound.getSound(1).start();//發出音效
										background.setdeltablood(burns);//燒傷
										background.setBloodx1();//扣血
									}
									times++;
								}
								if(Daccelaration>150)
								{
									direction*=-1;//換對方的回合
									player1();
									timer1.stop();//timer stop
									passdirection();//call passdirection method
								}
							}
							else 
							{
								if(Daccelaration>100)
								{
									direction*=-1;//換對方的回合
									player1();
									timer1.stop();//timer stop
									passdirection();//call passdirection method
								}
							}
						}
					}
				}
				else//沒打到敵人 
				{		
					CalX(Daccelaration);
					CalY(Daccelaration);
					Olabel.setBounds(CoordinateX, CoordinateY-50, 50, 50);//設定丟出物品的位置和大小
					if(CoordinateY>400)
					{
						if(background.getBloodx1()<=50)
						{
							Plabel11.setIcon(imageLBefired[YourChoice]);
							Plabel21.setIcon(imageRHappy[MyChoice]);
							timer1.stop();//timer stop
							Displayfinalbutton();
						}
						else 
						{
							if(CoordinateY<=2000)
							{
								if(soundtimes==1)//發出一次音效
								{
									sound.getSound(3).setFramePosition(0);
									sound.getSound(3).start();//發出音效
									soundtimes++;
								}
								Plabel11.setIcon(imageLDodge[YourChoice]);
								Plabel21.setIcon(imageRNothit[MyChoice]);
							}
							if(ReRused[1])//被燒傷
							{	
								if(CoordinateY>2000&&CoordinateY<3500)
								{
									Plabel11.setIcon(imageLBefired[YourChoice]);
									Plabel21.setIcon(imageRHappy[MyChoice]);
									if(times==1)
									{	
										sound.getSound(1).setFramePosition(0);
										sound.getSound(1).start();//發出音效
										background.setdeltablood(burns);//燒傷
										background.setBloodx1();//扣血
									}
									times++;
								}
								if(CoordinateY>3500)
								{
									direction*=-1;//換對方回合
									player1();
									timer1.stop();//timer stop
									passdirection();//call passdirection method
								}
							}
							else 
							{
								if(CoordinateY>2000)
								{
									if(Rused[1])//被燒傷
									{
										Rused[1]=false;
										LifFired=false;
									}
									direction*=-1;//換對方的回合
									player1();
									timer1.stop();//timer stop
									passdirection();//call passdirection method
								}
							}
						}
					}
				}
			}
			repaint();//repaint panel
		}
	}
	class TimeListener2 implements ActionListener//Handle ActionEvent
	{
		public void actionPerformed(ActionEvent e)//event handler
		{
			velocity++;
			if(velocity>=17)
			{
				timer3.stop();//timer stop
			}
			background.setvelocity(velocity-3);
			background.updateUI();//refresh the panel
			repaint();//repaint panel
		}
	}
}
