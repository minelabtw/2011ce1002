package fp.s100502508;

import java.awt.Color;
import java.awt.Font;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class People extends JFrame 
{
	private ImageIcon people1=new ImageIcon("image/LStandby1.png");
	private ImageIcon people2=new ImageIcon("image/RHappy2.png");
	private ImageIcon people3=new ImageIcon("image/RHappy3.png");
	private int Enemy;
	private int degreeofdifficulty;
	private int player;
	private int times=1;
	private int Mychoice;
	private int Yourchoice;
	
	FrameWork f=new FrameWork();//create a frame
	Sound sound=new Sound();
	JPanel panel1=new JPanel();
	
	JLabel image1=new JLabel(people1);
	JLabel image2=new JLabel(people2);
	JLabel image3=new JLabel(people3);
	JLabel word1=new JLabel("Mud");
	JLabel word2=new JLabel("Ghost");
	JLabel word3=new JLabel("Stickman");
	JLabel title=new JLabel();
	
	LineBorder border=new LineBorder(Color.BLACK,3);//create a lineborder
	
	Font font1=new Font("SansSerif",Font.BOLD,25);//create font
	
	public People() 
	{	
		panel1.setLayout(null);

		title.setFont(font1);
		title.setText("Choose my character");
		title.setBounds(55, 20, 300, 40);//設定標題的位置和大小
		
		image1.setBounds(55, 70, 60, 100);//設定第一個人物的位置和大小
		image2.setBounds(170, 70, 60, 100);//設定第二個人物的位置和大小
		image3.setBounds(285, 70, 60, 100);//設定第三個人物的位置和大小
		
		image1.setBorder(border);
		image2.setBorder(border);
		image3.setBorder(border);
		
		word1.setBounds(55, 180, 70, 50);//設定位置和大小
		word2.setBounds(170, 180, 70, 50);
		word3.setBounds(285, 180, 70, 50);
		
		panel1.add(title);
		panel1.add(image1);
		panel1.add(image2);
		panel1.add(image3);
		panel1.add(word1);
		panel1.add(word2);
		panel1.add(word3);

		add(panel1);
		
		addMouseListener(new MouseListener() 
		{
			public void mouseClicked(MouseEvent e)//滑鼠點到範圍會發生的事   
			{
				if((e.getX()>=55&&e.getX()<=105)&&(e.getY()>=70&&e.getY()<=170))//選擇第一位角色
				{				
					sound.getSound(0).setFramePosition(0);
					sound.getSound(0).start();//發出音效
					Mychoice=0;
					Yourchoice=0;
					setProple();//call setPeople method
				}
				else if((e.getX()>=170&&e.getX()<=230)&&(e.getY()>=70&&e.getY()<=170))//選擇第二位角色
				{
					sound.getSound(0).setFramePosition(0);
					sound.getSound(0).start();//發出音效
					Mychoice=1;
					Yourchoice=1;
					setProple();//call setPeople method
				}
				else if((e.getX()>=285&&e.getX()<=345)&&(e.getY()>=70&&e.getY()<=170))//選擇第三位角色
				{
					sound.getSound(0).setFramePosition(0);
					sound.getSound(0).start();//發出音效
					Mychoice=2;
					Yourchoice=2;
					setProple();//call setPeople method
				}
				f.setplayer(player);//call setplayer method to the object of FrameWork
				f.setDifficulty(degreeofdifficulty);//call setDifficulty method to the object of FrameWork
			}
			public void mousePressed(MouseEvent e) 
			{
			}
			public void mouseReleased(MouseEvent e) 
			{				
			}
			public void mouseEntered(MouseEvent e) 
			{	
			}
			public void mouseExited(MouseEvent e) 
			{	
			}
		});
		
		f.setTitle("FinalProject");//set the frame title
		f.setSize(800, 500);//set the frame size
		f.setLocationRelativeTo(null);//center a frame
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setResizable(false);//can't zoom the window
	}
	
	public void Peoplerandom()//隨機選人物
	{
		Enemy=(int)(Math.random()*10);
		if(Enemy==0)
		{
			Peoplerandom();//重跑一次
		}
		else
		{
			if(Enemy%3==0)
			{
				Enemy=0;
			}
			else if(Enemy%3==1)
			{
				Enemy=1;
			}
			else 
			{
				Enemy=2;
			}
		}
		f.setYourChoose(Enemy);//設電腦的角色
	}
	
	public void setProple()//選人物
	{
		sound.getSound(0).setFramePosition(0);
		sound.getSound(0).start();//發出音效
		if(player==0)
		{
			f.setVisible(true);//display the frame
			f.setMyChoose(Mychoice);
			f.setMyimageicon(Mychoice);
			setenemy();//call enemy method
			setVisible(false);//vanish the frame
		}
		else 
		{
			if(times==1)//選右邊的角色
			{
				f.setMyChoose(Mychoice);
				f.setMyimageicon(Mychoice);
				title.setText("Choose enemy character");
				panel1.updateUI();//refresh the panel
			}
			else if(times==2)//選左邊的角色
			{
				f.setVisible(true);//display the frame
				f.setYourChoose(Yourchoice);
				f.setEnemyimageicon(Yourchoice);
				setVisible(false);//vanish the frame
			}
			times++;
		}
	}
	
	public void setDifficulty(int degree)//receive a difficulty parameter
	{
		degreeofdifficulty=degree;
	}
	
	public void setPlayer(int number)//receive a player parameter
	{
		player=number;
	}
	
	public void setenemy()//set enemy's character
	{
		Peoplerandom();//call peoplerandom method
		f.setEnemyimageicon(Enemy);
		f.setplayer(player);
	}
}
