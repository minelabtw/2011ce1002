package fp.s100502508;

import java.awt.*;

import javax.swing.*;

public class Background extends JPanel
{
	private int centerX;
	private int centerY;
	private ImageIcon imagebackground1=new ImageIcon("image/background1.jpg");
	private ImageIcon imagebackground2=new ImageIcon("image/background2.jpg");
	private Image image1=imagebackground1.getImage();
	private Image image2=imagebackground2.getImage();
	private int surplusBloodx1=350;
	private int surplusBloodx2=750;
	private int[] Bloodx1={50,50,surplusBloodx1,surplusBloodx1};
	private int[] Bloodx2={450,450,surplusBloodx2,surplusBloodx2};
	private int[] Bloody={25,50,50,25};
	private int[] WindBackx1={397,397,397+40,397+40};
	private int[] WindBackx2={397-40,397-40,397,397};
	private int[] Windx1=new int[4];
	private int[] Windx2=new int[4];
	private int[] Windy={55,65,65,55};
	private int direction=-1;
	private int Velocity=0;
	private int windspeed;
	private int player;
	private int Deltablood=100;
	private boolean Attack=false;
	private boolean Result=false;
	private final int RANDOMBACKGROUND=((int)(Math.random()*10))%2;
	
	Font font1=new Font("SansSerif",Font.BOLD,25);//create fonts
	Font font2=new Font("SansSerif",Font.BOLD,58);
	Font font3=new Font("SansSerif",Font.BOLD,17);
	
	public Background()
	{
		
	}
	
	protected void paintComponent(Graphics g)//override paintComponent
	{
		super.paintComponent(g);//draw things in the superclass
		if(RANDOMBACKGROUND==0)//亂書的結果
		{
			g.drawImage(image1,0,0,getWidth(),getHeight(),this);//畫背景
		}
		else 
		{
			g.drawImage(image2,0,0,getWidth(),getHeight(),this);//畫背景
		}
		
		centerX=getWidth()/2;
		centerY=getHeight()/2;

		int[] RBlockx={640,640,640,640};
		int[] LBlockx={15,15,15,15};
		int[] Blocky={270,290,290,270};
		
		g.setColor(Color.RED);//set color red
		g.fillPolygon(Bloodx1, Bloody, Bloodx1.length);//左邊角色的血量
		g.fillPolygon(Bloodx2, Bloody, Bloodx2.length);//右邊角色的血量
		g.setColor(Color.BLACK);//set color black
		g.setFont(font3);
		if(windspeed>0)//鳳朝右邊吹
		{
			g.drawString("Wind", 357, 83);
			g.drawLine(402, 78, 437, 78);
			g.drawLine(425, 73, 437, 78);
			g.drawLine(425, 83, 437, 78);
		}
		else if(windspeed==0)//無風
		{
			g.drawString("No Wind", 357, 83);
		}
		else//風朝左邊吹 
		{
			g.drawString("Wind", 357, 83);
			g.drawLine(402, 78, 437, 78); 
			g.drawLine(402, 78, 414, 73);
			g.drawLine(402, 78, 414, 83);
		}
		g.setFont(font1);
		g.drawString("HP", 55, 45);
		g.drawString("HP", 455, 45);
		g.setFont(font2);
		
		if(surplusBloodx1==50)//左邊血沒了
		{
			g.drawString("RIGHT CHARACTER WIN!!", 50, 220);
		}
		else if(surplusBloodx2==450)//右邊血沒了
		{
			g.drawString("LEFT CHARACTER WIN!!", 50, 220);
		}
		if(Result==false)
		{
			if(direction==-1)//右方的回合
			{
				if(Attack)//壓滑鼠的時候
				{
					g.setColor(Color.GREEN);//set color green
					g.fillRect(640,270,140,20);
					for(int b=0;b<4;b++)
					{
						if(b==0||b==1)
						{
							RBlockx[b]=640;
						}
						else 
						{
							RBlockx[b]=640+10*Velocity;
						}
						g.setColor(Color.magenta);//set color magenta
						g.fillPolygon(RBlockx, Blocky, RBlockx.length);//力量條
					}
				}
			}
			else 
			{
				if(player==1)
				{
					if(Attack)//壓滑鼠的時候
					{
						g.setColor(Color.GREEN);//set color green
						g.fillRect(15,270,140,20);
						for(int b=0;b<4;b++)
						{
							if(b==0||b==1)
							{
								LBlockx[b]=15;
							}
							else 
							{
								LBlockx[b]=15+10*Velocity;
							}
							g.setColor(Color.magenta);//set color magenta
							g.fillPolygon(LBlockx, Blocky, LBlockx.length);//力量條
						}
					}
				}
			}
		}
		g.setColor(Color.YELLOW);//set color yellow
		g.fillPolygon(WindBackx1, Windy, WindBackx1.length);
		g.fillPolygon(WindBackx2, Windy, WindBackx2.length);
		g.setColor(Color.BLUE);//set color blue
		if(windspeed>=0)//風向右邊吹
		{
			g.fillPolygon(Windx1, Windy, Windx1.length);//風速力量條
		}
		else//風向左邊吹
		{
			g.fillPolygon(Windx2, Windy, Windx2.length);//風速力量條
		}
		
	}
	
	public void setBloodx1()//左方人物被扣血
	{
		surplusBloodx1-=Deltablood;
		if(surplusBloodx1<=50)
		{
			surplusBloodx1=50;
		}
		Bloodx1[2]=surplusBloodx1;
		Bloodx1[3]=surplusBloodx1;
	}
	
	public void setBloodx2()//右方人物被扣血
	{
		surplusBloodx2-=Deltablood;
		if(surplusBloodx2<=450)
		{
			surplusBloodx2=450;
		}
		Bloodx2[2]=surplusBloodx2;
		Bloodx2[3]=surplusBloodx2;
	}
	
	public int getBloodx1()//得到左方人物剩餘的血量
	{
		return surplusBloodx1;
	}
	
	public int getBloodx2()//得到右方人物剩餘的血量
	{
		return surplusBloodx2;
	}
	
	public int getCenterX()
	{
		return centerX;
	}
	
	public int getCenterY()
	{
		return centerY;
	}
	
	public void setdirection(int direction)//接收誰的回合
	{
		this.direction=direction;
	}
	
	public void setvelocity(int velocity)//接收速度
	{
		Velocity=velocity;
	}
	
	public void setWind()//設定風速
	{
		windspeed=((int)(Math.random()*10)-5);//隨機風速
		if(windspeed>=0)
		{
			for(int a=0;a<4;a++)
			{
				if(a==0||a==1)
				{
					Windx1[a]=397;
				}
				else 
				{
					Windx1[a]=397+windspeed*10;
				}
			}
		}
		else 
		{
			for(int a=0;a<4;a++)
			{
				if(a==0||a==1)
				{
					Windx2[a]=397+windspeed*10;
				}
				else 
				{
					Windx2[a]=397;
				}
			}
		}
	}
	
	public int getWindspeed()//得到風速
	{
		return windspeed;
	}
	
	public void setplayer(int number)//set player
	{
		player=number;
	}
	
	public void setresult(boolean result)//set result
	{
		Result=result;
	}
	
	public void setdeltablood(int delta)//設定被扣的血量
	{
		Deltablood=delta;
	}
	
	public void IfAttack(boolean attack)//是否在押滑鼠
	{
		Attack=attack;
	}
}
