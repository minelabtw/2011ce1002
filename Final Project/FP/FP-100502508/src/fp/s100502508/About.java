package fp.s100502508;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class About extends JFrame
{
	private ImageIcon[] about=new ImageIcon[2];
	
	JPanel panel=new JPanel();
	JLabel aboutLabel=new JLabel();
	
	Sound sound=new Sound();
	
	public About()
	{
		for(int a=1;a<=2;a++)//store images into a array
		{
			about[a-1]=new ImageIcon("image/About"+a+".png");
		}
		
		aboutLabel.setIcon(about[0]);
		panel.add(aboutLabel);
		add(panel);
		
		addMouseListener(new MouseListener() 
		{
			public void mouseReleased(MouseEvent e) 
			{
			}
			public void mousePressed(MouseEvent e) 
			{
			}
			public void mouseExited(MouseEvent e) 
			{
			}
			public void mouseEntered(MouseEvent e) 
			{
			}
			public void mouseClicked(MouseEvent e)//滑鼠點到範圍會發生的事  
			{
				if((e.getX()>=570&&e.getX()<=740)&&(e.getY()>=470&&e.getY()<=480))
				{
					sound.getSound(0).setFramePosition(0);
					sound.getSound(0).start();//發出音效
					FinalProject p1=new FinalProject();//create a frame
					p1.setTitle("FinalProject");//set the frame title
					p1.setSize(800, 500);//set the frame size
					p1.setLocationRelativeTo(null);//center a frame
					p1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					p1.setVisible(true);//display the frame
					p1.setResizable(false);//can't zoom the window
					setVisible(false);//vanish the frame
				}
			}
		});
		addMouseMotionListener(new MouseMotionListener() 
		{
			public void mouseMoved(MouseEvent e)//滑鼠移動島特定範圍發生的事  
			{
				if((e.getX()>=570&&e.getX()<=740)&&(e.getY()>=470&&e.getY()<=480))
				{
					aboutLabel.setIcon(about[1]);//換背景
				}
				else 
				{
					aboutLabel.setIcon(about[0]);//換背景
				}
			}
			public void mouseDragged(MouseEvent e) 
			{
			}
		});
	}
}
