package fp.s100502508;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Mode extends JFrame
{
	People people=new People();//create a frame
	Sound sound=new Sound();
	
	private String[] versusStrings={"One player","Two player"};
	private String[] degreeStrings={"Easy","Normal","Hard"};
	
	private int degree=1;
	private int player=0;

	JPanel panel=new JPanel();
	JLabel title=new JLabel("Mode");
	JLabel versusLabel=new JLabel("Choose game");
	JLabel degreeLabel=new JLabel("Choose degree");
	
	JButton NextButton=new JButton("Next");
	
	JComboBox versusBox=new JComboBox(versusStrings);//create a combo box
	JComboBox degreeBox=new JComboBox(degreeStrings);//create a combo box
	
	Font font1=new Font("SansSerif",Font.BOLD,40);//create fonts
	Font font2=new Font("SansSerif",Font.BOLD,17);
	
	public Mode() 
	{
		panel.setLayout(null);
		
		title.setFont(font1);
		versusLabel.setFont(font2);
		degreeLabel.setFont(font2);
		NextButton.setFont(font2);
		
		title.setBounds(120, 20, 200, 80);//設定標題的位置和大小
		versusLabel.setBounds(50, 120, 120, 30);//設定位置和大小
		degreeLabel.setBounds(50, 180, 120, 30);
		versusBox.setBounds(200, 120, 120, 30);//設定Combobox位置和大小
		degreeBox.setBounds(200, 180, 120, 30);
		NextButton.setBounds(125, 250, 100, 30);//設定Next按鈕位置和大小
		
		panel.add(title);
		panel.add(versusLabel);
		panel.add(degreeLabel);
		panel.add(versusBox);
		panel.add(degreeBox);
		panel.add(NextButton);
		
		add(panel);
		
		versusBox.addItemListener(new ItemListener()//register listener 
		{
			public void itemStateChanged(ItemEvent e)//handel item selection
			{
				player=versusBox.getSelectedIndex();
				if(player==0)//選一個玩家
				{
					degreeLabel.setVisible(true);
					degreeBox.setVisible(true);//難度Combox顯示出來
				}
				else//選兩個玩家
				{
					degreeLabel.setVisible(false);
					degreeBox.setVisible(false);//難度Combox隱藏
				}
				panel.updateUI();//refresh the panel
			}
		});
		
		degreeBox.addItemListener(new ItemListener()//register listener 
		{
			public void itemStateChanged(ItemEvent e)//handel item selection
			{
				degree=degreeBox.getSelectedIndex()+1;
			}
		});
		//register listener with button
		NextButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e)//press the next button
			{
				sound.getSound(0).setFramePosition(0);
				sound.getSound(0).start();//發出音效
				setVisible(false);//vanish the frame
				people.setTitle("FinalProject");//set the frame title
				people.setSize(400, 250);//set the frame size
				people.setLocationRelativeTo(null);//center a frame
				people.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				people.setResizable(false);//can't zoom the window
				people.setVisible(true);//display the frame
				people.setPlayer(player);//call setplayer method to the object of People
				people.setDifficulty(degree);//call setDifficulty method to the object of People
			}
		});
	}
}
