package fp.s100502508;

import javax.swing.*;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class FinalProject extends JFrame 
{	
	private ImageIcon[] start=new ImageIcon[4];
	
	Mode mode=new Mode();//create a frame
	About about=new About();//create a frame
	Sound sound=new Sound();
	JPanel panel=new JPanel();
	JLabel startLabel=new JLabel();
	
	public FinalProject()
	{
		for(int a=1;a<=4;a++)//store images into a array
		{
			start[a-1]=new ImageIcon("image/Start"+a+".png");
		}
		
		startLabel.setIcon(start[0]);
		panel.add(startLabel);
		add(panel);
		//create a new frame 
		mode.setTitle("FinalProject");//set the frame title
		mode.setSize(350, 350);//set the frame size
		mode.setLocationRelativeTo(null);//center a frame
		mode.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mode.setResizable(false);//can't zoom the window
		//create a new frame
		about.setTitle("FinalProject");//set the frame title
		about.setSize(800, 500);//set the frame size
		about.setLocationRelativeTo(null);//center a frame
		about.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		about.setResizable(false);//can't zoom the window
		
		addMouseListener(new MouseListener() 
		{
			public void mouseReleased(MouseEvent e) 
			{
				
			}
			public void mousePressed(MouseEvent e) 
			{
				
			}
			public void mouseExited(MouseEvent e) 
			{
				
			}
			public void mouseEntered(MouseEvent e) 
			{			
			}
			public void mouseClicked(MouseEvent e)//滑鼠點到範圍會發生的事 
			{
				if((e.getX()>=340&&e.getX()<=520)&&(e.getY()>=250&&e.getY()<=285))
				{
					sound.getSound(0).setFramePosition(0);
					sound.getSound(0).start();//發出音效
					mode.setVisible(true);//display the frame
					setVisible(false);//vanish the frame
				}
				else if((e.getX()>=340&&e.getX()<=520)&&(e.getY()>=340&&e.getY()<=375))
				{
					sound.getSound(0).setFramePosition(0);
					sound.getSound(0).start();//發出音效
					about.setVisible(true);//display the frame
					setVisible(false);//vanish the frame
				}
				else if((e.getX()>=365&&e.getX()<=490)&&(e.getY()>=420&&e.getY()<=460))
				{
					System.exit(0);//結束程式
				}
			}
		});
		addMouseMotionListener(new MouseMotionListener() 
		{
			public void mouseMoved(MouseEvent e)//滑鼠移動島特定範圍發生的事 
			{
				if((e.getX()>=340&&e.getX()<=520)&&(e.getY()>=250&&e.getY()<=285))
				{
					startLabel.setIcon(start[1]);//換背景
				}
				else if((e.getX()>=340&&e.getX()<=520)&&(e.getY()>=340&&e.getY()<=375))
				{
					startLabel.setIcon(start[2]);//換背景
				}
				else if((e.getX()>=365&&e.getX()<=490)&&(e.getY()>=420&&e.getY()<=460))
				{
					startLabel.setIcon(start[3]);//換背景
				}
				else 
				{
					startLabel.setIcon(start[0]);//換背景
				}
				panel.updateUI();//refresh the panel
			}
			public void mouseDragged(MouseEvent e) 
			{
			}
		});
	}
	
	public static void main(String[] args)
	{
		FinalProject p1=new FinalProject();//create a frame
		p1.setTitle(" FinalProject");//set the frame title
		p1.setSize(800, 500);//set the frame size
		p1.setLocationRelativeTo(null);//center a frame
		p1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		p1.setVisible(true);//display the frame
		p1.setResizable(false);//can't zoom the window
	}
}
