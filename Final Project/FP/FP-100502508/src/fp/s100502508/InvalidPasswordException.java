package fp.s100502508;

import javax.swing.JOptionPane;

public class InvalidPasswordException extends Exception
{
	public InvalidPasswordException(String password)
	{
		JOptionPane.showMessageDialog(null, password+" is wrong password!!", "Wrong", JOptionPane.ERROR_MESSAGE);
	}
}
