package fp.s100502508;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class Sound 
{
	private Clip[] sound = new Clip[4];
	private AudioInputStream[] audioIn = new AudioInputStream[4];
	
	public Sound()
	{
		File soundFile = new File("sound/Click.wav");// load the sound into memory (Click)
		try 
		{
			audioIn[0] = AudioSystem.getAudioInputStream(soundFile);
		    DataLine.Info info = new DataLine.Info(Clip.class, audioIn[0].getFormat());
		    sound[0] = (Clip) AudioSystem.getLine(info);
		    sound[0].open(audioIn[0]);

		    soundFile = new File("sound/Scream.wav");// load the sound into memory (Scream)
		    audioIn[1] = AudioSystem.getAudioInputStream(soundFile);
		    info = new DataLine.Info(Clip.class, audioIn[1].getFormat());
		    sound[1] = (Clip) AudioSystem.getLine(info);
		    sound[1].open(audioIn[1]);
		    
		    soundFile = new File("sound/Throw.wav");// load the sound into memory (Throw)
		    audioIn[2] = AudioSystem.getAudioInputStream(soundFile);
		    info = new DataLine.Info(Clip.class, audioIn[2].getFormat());
		    sound[2] = (Clip) AudioSystem.getLine(info);
		    sound[2].open(audioIn[2]);
		    
		    soundFile = new File("sound/Laugh.wav");// load the sound into memory (Laugh)
		    audioIn[3] = AudioSystem.getAudioInputStream(soundFile);
		    info = new DataLine.Info(Clip.class, audioIn[3].getFormat());
		    sound[3] = (Clip) AudioSystem.getLine(info);
		    sound[3].open(audioIn[3]);
	    } 
		catch (UnsupportedAudioFileException e) 
		{
		// Auto-generated catch block
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
		// Auto-generated catch block
			e.printStackTrace();
		} 
		catch (LineUnavailableException e) 
		{
		// Auto-generated catch block
			e.printStackTrace();
		}
	}
	public Clip getSound(int number)//get the sound
	{
		return sound[number];
	}
}

//我從這邊找來的 http://www.java2s.com/Code/Java/Development-Class/AnexampleofloadingandplayingasoundusingaClip.htm
