package fp.s995002204;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;

public class FrameWorkEnter extends JFrame{
	
	//畫面顯示
	Container contentPane;
	private JPanel pCard;
	private Dealer21 deal = new Dealer21();
	
	//顯示遊戲主題圖樣
	public MainPanel main = new MainPanel();
	private JLabel theme = new JLabel();
	private JPanel userInformationControl = new JPanel();
	
	//玩家資訊物件建置
	private JLabel userName = new JLabel("1.玩家名稱:");
	private JTextField inputName = new JTextField();
	private JLabel sex = new JLabel("2.性別:");
	private JRadioButton chooseSexB = new JRadioButton("Boy",true);
	private JRadioButton chooseSexG = new JRadioButton("Girl",false);
	private ButtonGroup radioGroup =new ButtonGroup();
	
	//人物選擇
	private JLabel userFace = new JLabel();
	private String face[] = {"預設","賭神","老皮"};
	private JComboBox faceSelect = new JComboBox(face);
	private ImageIcon god = new ImageIcon("src/picture/God.png");
	private ImageIcon circle = new ImageIcon("src/picture/circle.png");
	private ImageIcon dog = new ImageIcon("src/picture/dog.png");
	
	private JButton enterGame =new JButton("進入遊戲");
	
	//使用者資訊控制
	private String userN = new String();
	private boolean buttonClick = false;
	
	public FrameWorkEnter()
	{	
		//設置人物圖像
		userFace.setBackground(Color.WHITE);
		userFace.setBorder(BorderFactory.createLineBorder (Color.black,1));
		userFace.setSize(150,200);
		userFace.setLocation(600, 200);
		faceSelect.setLocation(620, 410);
		faceSelect.setSize(100, 20);
		
		//設置RadioButton
		radioGroup.add(chooseSexB);
		radioGroup.add(chooseSexG);
		
		//使用者資訊控制
		userName.setFont(new Font("新細明體",Font.PLAIN,15));
		userName.setForeground(Color.CYAN);
		userName.setSize(100, 20);
		userName.setLocation(50, 450);
		inputName.setSize(100, 20);
		inputName.setLocation(150, 450);
		sex.setFont(new Font("新細明體",Font.PLAIN,15));
		sex.setForeground(Color.CYAN);
		sex.setSize(100, 20);
		sex.setLocation(50, 480);
		chooseSexB.setSize(50, 20);
		chooseSexB.setLocation(150, 480);
		chooseSexG.setSize(50, 20);
		chooseSexG.setLocation(200, 480);
		
		//Button設置
		enterGame.setSize(200, 50);
		enterGame.setLocation(300,450);
		
		//加入物件至main
		userFace.setIcon(circle);
		
		main.add(userFace);
		main.add(faceSelect);
		main.add(userName);
		main.add(inputName);
		main.add(sex);
		main.add(chooseSexB);
		main.add(chooseSexG);
		main.add(enterGame);
		main.setLocation(400, 200);
		
		//子視窗加入
		deal.main.setLayout(null);
		main.setLayout(null);
		
		//CardLayout設置
		pCard = new JPanel(new CardLayout());
		pCard.add("GameEnter", main);
	    pCard.add("Game", deal.main);
	    ((CardLayout) pCard.getLayout()).show(pCard, "GameEnter");
	    this.add(pCard);
	   
		//進入遊戲監聽器
		enterGame.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				deal.setName(inputName.getText());
				if(chooseSexB.isSelected())
				{
					deal.setSex("Gentleman");
				}
				if(chooseSexG.isSelected())
				{
					deal.setSex("Lady");
				}
				deal.rusername = inputName.getText();
				deal.username.setText(deal.rusername);
				((CardLayout) pCard.getLayout()).show(pCard, "Game");
			}
		});
		
		faceSelect.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent e)
			{
				if(faceSelect.getSelectedIndex()==0)
				{
					userFace.setIcon(circle);
					deal.userimage.setIcon(circle);
				}
				if(faceSelect.getSelectedIndex()==1)
				{
					userFace.setIcon(god);
					deal.userimage.setIcon(god);
				}
				if(faceSelect.getSelectedIndex()==2)
				{
					userFace.setIcon(dog);
					deal.userimage.setIcon(dog);
				}
			}
		});
		
		chooseSexB.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				deal.rsex = "Gentleman";
				deal.usersex.setText(deal.rsex);
			}
		});
		
		chooseSexG.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				deal.rsex = "Lady";
				deal.usersex.setText(deal.rsex);
			}
		});
	}
}