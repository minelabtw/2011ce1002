package fp.s995002204;

import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JPanel;

public class BackgroundPanel extends JPanel{
	
	private Image img;
	
	public BackgroundPanel()
    {
		setLayout(new FlowLayout());
        img = Toolkit.getDefaultToolkit().createImage("src/picture/table.png");
    }
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.drawImage(img, 0, 0, getSize().width,getSize().height,this);
	}
}
