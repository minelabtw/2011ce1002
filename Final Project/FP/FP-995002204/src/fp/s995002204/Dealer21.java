package fp.s995002204;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.*;

public class Dealer21 extends JFrame{
	
	//主要Panel
	public BackgroundPanel main = new BackgroundPanel();
	private JPanel buttonControl = new JPanel();
	private Image img = Toolkit.getDefaultToolkit().createImage("table.png");
	
	//音樂
	private Sound sound = new Sound();
	
	//Stack
	private Stack stack = new Stack();
	private int number1 = 7;
	private int number2 = 2;
	
	//顯示玩家資訊
	public JLabel username = new JLabel();
	public String rusername = "User";
	public JLabel usersex = new JLabel();
	public String rsex = "Gentleman";
	public JLabel userimage = new JLabel();
	private ImageIcon god = new ImageIcon("src/picture/God1.png");
	private ImageIcon circle = new ImageIcon("src/picture/circle1.png");
	private ImageIcon dog = new ImageIcon("src/picture/dog1.png");
	
	//顯示牌
	private JLabel showcard[];
	private JPanel ComCard = new JPanel();
	private JPanel UserCard = new JPanel();
	
	//顯示勝負
	private JLabel result = new JLabel();
	
	//要牌指令
	private JButton deal = new JButton("Deal");
	private JButton cashout = new JButton("Cash Out");
	private JPanel makesure = new JPanel();
	private JButton insurance = new JButton("Insure");
	private JButton hit = new JButton("Hit");
	private JButton stand = new JButton("Stand");
	private JButton doublea = new JButton("Double");
	private JButton one = new JButton("1");
	private JButton five = new JButton("5");
	private JButton twfive = new JButton("25");
	private JButton hundred = new JButton("100");
	private JPanel cash = new JPanel();
	
	private JButton getNew = new JButton("Get the new game");
	
	//儲存蓋牌值
	private int temp = 0;
	private int btemp = 0;
	
	//暫存變數
	private int bet = 0;
	private int blackjackCom1 = 0;
	private int blackjackCom2 = 0;
	private int blackjackUser1 = 0;
	private int blackjackUser2 = 0;
	private boolean click = false;
	
	//儲存牌值
	private int userValue = 0;
	private int comValue = 0;
	
	//顯示玩家金額
	private int cashuser = 10000;
	private JLabel cashUser= new JLabel("10000");
	
	//紀錄雙方牌值
	private JLabel comCash = new JLabel();
	private JLabel userCash = new JLabel();
	
	//儲存牌
	private int value[] = new int[52];
	
	//牌發放Stack
	public int index=-1;
	
	//牌宣告物件
	private ImageIcon card = new ImageIcon("src/picture/card.png");
	private ImageIcon image[] = {
			
	//黑桃牌
	new ImageIcon("src/Puker/SA.png"),new ImageIcon("src/Puker/S2.png"),new ImageIcon("src/Puker/S3.png"),
	new ImageIcon("src/Puker/S4.png"),new ImageIcon("src/Puker/S5.png"),new ImageIcon("src/Puker/S6.png"),
	new ImageIcon("src/Puker/S7.png"),new ImageIcon("src/Puker/S8.png"),new ImageIcon("src/Puker/S9.png"),
	new ImageIcon("src/Puker/S10.png"),new ImageIcon("src/Puker/SJ.png"),new ImageIcon("src/Puker/SQ.png"),
	new ImageIcon("src/Puker/SK.png"),
			
	//紅心牌
	new ImageIcon("src/Puker/HA.png"),new ImageIcon("src/Puker/H2.png"),new ImageIcon("src/Puker/H3.png"),
	new ImageIcon("src/Puker/H4.png"),new ImageIcon("src/Puker/H5.png"),new ImageIcon("src/Puker/H6.png"),
	new ImageIcon("src/Puker/H7.png"),new ImageIcon("src/Puker/H8.png"),new ImageIcon("src/Puker/H9.png"),
	new ImageIcon("src/Puker/H10.png"),new ImageIcon("src/Puker/HJ.png"),new ImageIcon("src/Puker/HQ.png"),
	new ImageIcon("src/Puker/HK.png"),
			
	//方塊牌
	new ImageIcon("src/Puker/DA.png"),new ImageIcon("src/Puker/D2.png"),new ImageIcon("src/Puker/D3.png"),
	new ImageIcon("src/Puker/D4.png"),new ImageIcon("src/Puker/D5.png"),new ImageIcon("src/Puker/D6.png"),
	new ImageIcon("src/Puker/D7.png"),new ImageIcon("src/Puker/D8.png"),new ImageIcon("src/Puker/D9.png"),
	new ImageIcon("src/Puker/D10.png"),new ImageIcon("src/Puker/DJ.png"),new ImageIcon("src/Puker/DQ.png"),
	new ImageIcon("src/Puker/DK.png"),
			
	//梅花牌
	new ImageIcon("src/Puker/CA.png"),new ImageIcon("src/Puker/C2.png"),new ImageIcon("src/Puker/C3.png"),
	new ImageIcon("src/Puker/C4.png"),new ImageIcon("src/Puker/C5.png"),new ImageIcon("src/Puker/C6.png"),
	new ImageIcon("src/Puker/C7.png"),new ImageIcon("src/Puker/C8.png"),new ImageIcon("src/Puker/C9.png"),
	new ImageIcon("src/Puker/C10.png"),new ImageIcon("src/Puker/CJ.png"),new ImageIcon("src/Puker/CQ.png"),
	new ImageIcon("src/Puker/CK.png")
	};
	
	//隨機程序
	private Random random = new Random();
	
	public Dealer21()
	{	
		sound.play();
		//user資訊
		username.setText("名稱:"+rusername);
		username.setLocation(620, 495);
		username.setSize(250, 20);
		username.setForeground(Color.WHITE);
		username.setFont(new Font("Georgia",Font.PLAIN,20));
		usersex.setText(rsex);
		usersex.setLocation(620, 520);
		usersex.setSize(250, 20);
		usersex.setForeground(Color.WHITE);
		usersex.setFont(new Font("Georgia",Font.PLAIN,20));
		userimage.setLocation(620, 330);
		userimage.setSize(110, 150);
		userimage.setIcon(circle);
		
		//再一局
		getNew.setVisible(false);
		getNew.setSize(150, 40);
		getNew.setLocation(320, 300);
		
		//賭金按鍵
		cash.setLayout(new GridLayout(1,4));
		cash.add(one);
		cash.add(five);
		cash.add(twfive);
		cash.add(hundred);
		cash.setSize(220,30);
		cash.setLocation(280, 350);
		
		//Button Control
		hit.setSize(100, 40);
		stand.setSize(100, 40);
		doublea.setSize(100, 40);
		buttonControl.setLayout(new GridLayout(1,3));
		buttonControl.add(hit);
		buttonControl.add(stand);
		buttonControl.add(doublea);
		buttonControl.setLocation(250, 250);
		buttonControl.setSize(300, 40);
		buttonControl.setVisible(false);
		
		//Make sure you want to play
		makesure.setLayout(new GridLayout(1,2));
		makesure.add(deal);
		makesure.add(cashout);
		makesure.setVisible(false);
		makesure.setSize(200, 20);
		makesure.setLocation(280, 300);
		
		//牌型設定
		showcard = new JLabel[10];
		for(int i=0;i<10;i++)
		{
			showcard[i] = new JLabel();
			showcard[i].setBorder(BorderFactory.createLineBorder (Color.BLACK,1));
			showcard[i].setSize(73,105);
		}
		
		//賭金顯示
		cashUser.setBorder(BorderFactory.createLineBorder (Color.BLACK,1));
		cashUser.setFont(new Font("Candara",Font.PLAIN,40));
		cashUser.setForeground(Color.WHITE);
		cashUser.setLocation(45, 480);
		cashUser.setSize(130, 50);
		
		//勝負顯示
		result.setSize(200, 90);
		result.setLocation(600, 240);
		result.setForeground(Color.CYAN);
		result.setFont(new Font( "新細明體", Font.PLAIN, 30 ));
		
		//牌值顯示
		comCash.setSize(200, 50);
		comCash.setLocation(20, 30);
		comCash.setForeground(Color.WHITE);
		comCash.setFont(new Font("新細明體",Font.PLAIN,20));
		userCash.setSize(200, 50);
		userCash.setLocation(20, 350);
		userCash.setForeground(Color.WHITE);
		userCash.setFont(new Font("新細明體",Font.PLAIN,20));
		
		//Computer Card 設定
		ComCard.setLayout(new GridLayout(1,5,2,2));
		ComCard.add(showcard[0]);
		ComCard.add(showcard[1]);
		ComCard.add(showcard[2]);
		ComCard.add(showcard[3]);
		ComCard.add(showcard[4]);
		ComCard.setSize(365, 107);
		ComCard.setLocation(200, 10);
		
		//User Card 設定
		UserCard.setLayout(new GridLayout(1,5,2,2));
		UserCard.add(showcard[5]);
		UserCard.add(showcard[6]);
		UserCard.add(showcard[7]);
		UserCard.add(showcard[8]);
		UserCard.add(showcard[9]);
		UserCard.setSize(365, 107);
		UserCard.setLocation(200, 420);
		
		//保險設定
		insurance.setSize(80, 20);
		insurance.setLocation(355, 225);
		insurance.setVisible(false);
		
		//加入物件
		main.add(userimage);
		main.add(username);
		main.add(usersex);
		main.add(getNew);
		main.add(result);
		main.add(cashUser);
		main.add(insurance);
		main.add(comCash);
		main.add(userCash);
		main.add(ComCard);
		main.add(UserCard);
		main.add(cash);
		main.add(makesure);
		main.add(buttonControl);
		this.add(main);
		
		setCard();
		
		//按鍵設置
		getNew.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				clearFrame();
			}
		});
		
		deal.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				makesure.setVisible(false);
				buttonControl.setVisible(true);
				int i;
				i = stack.pop();	
				blackjackUser1 = valueDetect(i);
				userValue += valueDetect(i);
				showcard[5].setIcon(image[i]);
				userCash.setText(String.valueOf(userValue));
				
				//隱藏電腦牌 紀錄牌值和圖像
				i = stack.pop();
				blackjackCom1 = valueDetect(i);
				btemp = i;
				temp = valueDetect(i);
				showcard[0].setIcon(card);
				
				i = stack.pop();
				blackjackUser2 = valueDetect(i);
				userValue += valueDetect(i);
				showcard[6].setIcon(image[i]);
				userCash.setText(String.valueOf(userValue));
				
				i = stack.pop();
				blackjackCom2 = valueDetect(i);
				comValue += valueDetect(i);
				showcard[1].setIcon(image[i]);
				comCash.setText(String.valueOf(comValue));
				
				if(blackjack(blackjackUser1,blackjackUser2))
				{
					userCash.setText("21");
					result.setText("User Win!!!");
					buttonControl.setVisible(false);
					getNew.setVisible(true);
				}
				
				//判斷是否要保險
				if(valueDetect(i)==1)
				{
					insurance.setVisible(true);
				}
			}
		});
		
		cashout.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				System.exit(0);
			}
		});
		
		hit.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if(blackjack(blackjackCom1,blackjackCom2))
				{
					showcard[0].setIcon(image[btemp]);
					comCash.setText("21");
					result.setText("Com win!!!");
					buttonControl.setVisible(false);
					getNew.setVisible(true);
				}
				
				if(number1<10)
				{
					int i;
					i = stack.pop();
					userValue += valueDetect(i);
					showcard[number1].setIcon(image[i]);
					number1++;
					userCash.setText(String.valueOf(userValue));
					
					if(burst(userValue))
					{
						//stop();
						result.setText("Com Win!!!");
						buttonControl.setVisible(false);
						getNew.setVisible(true);
					}
					if(userGetFive()&&!burst(userValue))
					{
						int s;
						s = Integer.parseInt(cashUser.getText());
						s += 2*bet;
						cashUser.setText(String.valueOf(s));
						result.setText("User Win!!!");
						stop();
						buttonControl.setVisible(false);
						getNew.setVisible(true);
					}
				}
				if(cardEnough())
				{
					setCard();
				}
			}
		});
		
		stand.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if(!click)
				{
					showcard[0].setIcon(image[btemp]);
					comValue += temp;
					comCash.setText(String.valueOf(comValue));
					buttonControl.setEnabled(false);
				}
				
				if(blackjack(blackjackCom1,blackjackCom2))
				{
					result.setText("Com Win!!!");
					comCash.setText("21");
				}
				else
				{
					while(comValue < 17)
					{
						if(number2<5)
						{
							int i;
							i = stack.pop();
							comValue += valueDetect(i);
							showcard[number2++].setIcon(image[i]);
							comCash.setText(String.valueOf(comValue));
						}
						else
							break;
					}
					
					if(comGetFive()&&!burst(comValue))
					{
						result.setText("Com Win!!!");
					}
					else
					{
						winorlose();
					}
				}
				
				if(cardEnough())
				{
					setCard();
				}
				buttonControl.setVisible(false);
				getNew.setVisible(true);
			}
		});
		
		doublea.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				doublea.setEnabled(false);
				int i;
				i = stack.pop();
				userValue += valueDetect(i);
				showcard[number1].setIcon(image[i]);
				number1++;
				userCash.setText(String.valueOf(userValue));
				
				showcard[0].setIcon(image[btemp]);
				comValue += temp;
				comCash.setText(String.valueOf(comValue));
				
				while(comValue < 17)
				{
					if(number2<5)
					{
						int j;
						j = stack.pop();
						comValue += valueDetect(j);
						showcard[number2++].setIcon(image[j]);
						comCash.setText(String.valueOf(comValue));
					}
					else
						break;
				}
				
				if(comGetFive())
				{
					result.setText("Com Win!!!");
				}
				bet *= 2;
				winorlose();
				
				buttonControl.setVisible(false);
				getNew.setVisible(true);
			}
		});
		
		insurance.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int temp1;
				temp1 = Integer.parseInt(cashUser.getText());
				temp1 = temp1 - bet/2;
				cashUser.setText(String.valueOf(temp1));
				//顯示牌
				showcard[0].setIcon(image[btemp]);
				comValue += temp;
				comCash.setText(String.valueOf(comValue));
				
				if(blackjack(blackjackCom1,blackjackCom2))
				{
					comCash.setText("21");
					result.setText("Com Win!!!");
					temp1 = temp1+bet;
					cashUser.setText(String.valueOf(temp1));
					buttonControl.setVisible(false);
					getNew.setVisible(true);
				}
				insurance.setVisible(false);
				click = true;
			}
		});
		
		one.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int i = Integer.parseInt(cashUser.getText());
				cashuser = i-1;
				cashUser.setText(String.valueOf(cashuser));
				makeSure();
				bet = 1;
			}
		});
		
		five.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int i = Integer.parseInt(cashUser.getText());
				cashuser = i-5;
				cashUser.setText(String.valueOf(cashuser));
				makeSure();
				bet =5;
			}
		});
		
		twfive.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int i = Integer.parseInt(cashUser.getText());
				cashuser = i-25;
				cashUser.setText(String.valueOf(cashuser));
				makeSure();
				bet = 25;
			}
		});
		
		hundred.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int i = Integer.parseInt(cashUser.getText());
				cashuser = i-100;
				cashUser.setText(String.valueOf(cashuser));
				makeSure();
				bet = 100;
			}
		});
	}
	
	//判斷是否為21點
	public boolean blackjack(int x,int y)
	{
		if((x==1&&y==10)||(x==10&&y==1))
			return true;
		else
			return false;
	}
	
	//轉換陣列index為1~13牌值
	public int valueDetect(int x)
	{
		if(0<=x&&x<=12)
		{
			if(x<10)
				return x+1;
			else
				return 10;
		}
		else if(13<=x&&x<=25)
		{
			if(x<23)
				return x%13+1;
			else
				return 10;
		}
		else if(26<=x&&x<=38)
		{
			if(x<36)
				return x%13+1;
			else
				return 10;
		}
		else
		{
			if(x<49)
				return x%13+1;
			else
				return 10;
		}
	}
	
	//確定是否賭牌
	public void makeSure()
	{
		makesure.setVisible(true);
		one.setEnabled(false);
		five.setEnabled(false);
		twfive.setEnabled(false);
		hundred.setEnabled(false);
	}
	
	//判斷是否爆點
	public boolean burst(int x)
	{
		if(x>21)
			return true;
		else
			return false;
	}
	
	//五張牌勝利
	public boolean userGetFive()
	{
		if(number1==10)
			return true;
		else
			return false;
	}
	
	public boolean comGetFive()
	{
		if(number2==5)
			return true;
		else
			return false;
	}
	
	//判斷勝負
	public void winorlose()
	{
		if(userValue>comValue&&userValue<=21)
		{
			int temp;
			temp = Integer.parseInt(cashUser.getText());
			temp += 2*bet;
			cashUser.setText(String.valueOf(temp));
			result.setText("User Win!!!");
			stop();
		}
		
		else if(comValue>userValue&&comValue<=21)
		{
			//Nothing Happened
			result.setText("Com Win!!!");
			stop();
		}
		
		else if(burst(comValue)&&!burst(userValue))
		{
			int temp;
			temp = Integer.parseInt(cashUser.getText());
			temp += 2*bet;
			cashUser.setText(String.valueOf(temp));
			result.setText("User Win!!!");
			stop();
		}
		
		else if(!burst(comValue)&&burst(userValue))
		{
			result.setText("Com Win!!!");
			stop();
		}
		
		else
		{
			int temp;
			temp = Integer.parseInt(cashUser.getText());
			temp += bet;
			cashUser.setText(String.valueOf(temp));
			result.setText("Nobody Win!!!");
			stop();
		}
	}
	
	//畫面暫停
	public void stop()
	{
		try
		{
		    Thread.sleep(500); 
		}
		catch(InterruptedException   ex)
		{
			System.out.println("What are you doing?");
		}
	}
	
	//判斷牌數
	public boolean cardEnough()
	{
		if(index<10)
			return true;
		else
			return false;
	}
	
	//設置牌
	public void setCard()
	{
		index = -1;
		
		for(int i=0;i<52;i++)
		{
			value[i] = i;
		}
		
		//洗牌演算法
		for(int i=0;i<52;i++)
		{
			//產生0∼51的亂數
	        int n1=random.nextInt(52);
	        int n2=random.nextInt(52);
	        //將陣列資料進行交換（打散）
			int temp1 = value[n1];
			value[n1]=value[i];
	        value[i]=temp1;
	        temp1 = value[n2];
	        value[n2]=value[i];
	        value[i]=temp1;
	    }
				
		for(int i=0;i<52;i++)
		{
			stack.push(value[i]);
		}
	}
	
	//清理畫面
	public void clearFrame()
	{
		for(int i=0;i<10;i++)
		{
			showcard[i].setIcon(null);
		}
		click = false;
		//setEnable
		buttonControl.setEnabled(true);		//hit stand double
		one.setEnabled(true);				//bet
		five.setEnabled(true);
		twfive.setEnabled(true);
		hundred.setEnabled(true);
		doublea.setEnabled(true);
		//setVisible
		result.setText("");
		comCash.setText("");				
		userCash.setText("");
		cash.setVisible(true);				//bet button control
		insurance.setVisible(false);
		getNew.setVisible(false);	
		makesure.setVisible(false);
		userValue = 0;
		comValue = 0;
		number1 = 7;
		number2 = 2;
	}
	
	public void setName(String name)
	{
		rusername = name;
	}
	
	public void setSex(String sex)
	{
		rsex = sex;
	}
	
	class Stack
	{
		public int value[] = new int[52];
		
		public void push(int x)
		{
			index++;
			value[index] = x;
		}
		
		public int pop()
		{
			return value[index--];
		}
	}
}
