package fp.s100502028;

import javax.swing.JFrame;

/**原本 Final project 是想作德州撲克的遊戲
 * 但在演算法及邏輯判斷上遇到許多困難，最後只好改
 * 做普通的梭哈遊戲 */

public class FinalProject extends JFrame {
	private FrameWork f = new FrameWork();
	
	// Constructor
	public FinalProject() {
		add(f); // Add the panel to the frame
	}
	
	public static void main(String [] args) {
		FinalProject fp = new FinalProject(); // Create an object of FinalProject
		fp.setTitle("Game");
		fp.setSize(500,500); 
		fp.setLocationRelativeTo(null); 
		fp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fp.setVisible(true);
	} // End main method
}
