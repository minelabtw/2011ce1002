package fp.s100502028;

public class MoneyManipulate {
	protected int money = 100000000; // Set initial money
	
	// Null constructor
	public MoneyManipulate() {
		
	}
	
	// Set method
	public void setMoney(int m) {
		money = m;
	}
	
	// Get method
	public int getMoney() {
		return money;
	}
}
