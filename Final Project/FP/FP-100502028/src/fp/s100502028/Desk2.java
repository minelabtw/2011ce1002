package fp.s100502028;

import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;

import javax.swing.*;


public class Desk2 extends JPanel {
	private ImageIcon[] initialPicture = new ImageIcon[5];
	private ImageIcon[] cardPicture = new ImageIcon[54];
	
	private JLabel[] playerInitial = new JLabel[5];
	private JLabel[] computerInitial = new JLabel[5];
	private JLabel[] role = new JLabel[2];
	
	private JLabel playerType = new JLabel("");
	private JLabel computerType = new JLabel("");
	private JLabel gameResult = new JLabel("< Game result >");
	private JLabel jlblMoneyOfPlayer1 = new JLabel("Player's money");
	private JLabel jlblMoneyOfPlayer2 = new JLabel("0");
	private JLabel jlblMoneyOfComputer1 = new JLabel("Computer's money");
	private JLabel jlblMoneyOfComputer2 = new JLabel("0");
	private JLabel jlblShowMoney1 = new JLabel("下注金額：");
	private JLabel jlblShowMoney2 = new JLabel("");
	private JLabel jlblTabelMoney1 = new JLabel("檯面上的錢");
	private JLabel jlblTabelMoney2 = new JLabel("0");
	
	private JButton jbtStart = new JButton("發牌");
	private JButton jbtContinue = new JButton("繼續");
	private JButton jbtResult = new JButton("結果");
	private JButton jbtEnd = new JButton("離開遊戲");
	private JButton jbtTenThousand = new JButton("1萬");
	private JButton jbtFiftyThousand = new JButton("5萬");
	private JButton jbtOneHundredThousand = new JButton("10萬");
	private JButton jbtFiveHundredThousand = new JButton("50萬");
	private JButton jbtOneMillion = new JButton("100萬");
	private JButton jbtFiveMillion = new JButton("500萬");
	private JButton jbtTenMillion = new JButton("1000萬");
	private JButton jbtFiftyMillion = new JButton("5000萬");
	private JButton jbtShowHand = new JButton("ShowHand");
	private JButton jbtCancel = new JButton("取消");
	private JButton jbtConfirm = new JButton("確認");
	
	private int x = 0, contempMoney = 0, jbtContinueClickTime = 2, nullMoney = 0 , p = 0, q = 0;
	private int[] dx = new int[5];
	
	private Font font1 = new Font("Rockwell Extra Bold", Font.BOLD, 20);
	private Font font2 = new Font("標楷體", Font.BOLD, 20);
	private Color color1 = new Color(219, 53, 99);
	private Color color2 = new Color(60, 213, 79);
	private Color color3 = new Color(47, 109, 225);
	
	private CardManipulate2 play = new CardManipulate2();
	private MoneyManipulate moneyOfPlayer = new MoneyManipulate();
	private MoneyManipulate moneyOfComputer = new MoneyManipulate();
	private JFrame endFaceLoseFrame = new JFrame();
	private JFrame endFaceWinFrame = new JFrame();
	
	private Timer timer1 = new Timer(100, new TimerListener1());
	private Timer timer2 = new Timer(100, new TimerListener2());
	
	// Constructor
	public Desk2() {
		setLayout(null);
		
		role[0] = new JLabel("");
		role[1] = new JLabel("");
		role[0].setIcon(new ImageIcon("image/god.png"));
		role[0].setBounds(430, 2, 135, 165);
		role[1].setBounds(390, 550, 220, 151);
		add(role[0]);
		add(role[1]);
		
		// Save the image into an array
		for(int i = 0; i < 53; i++) {
			cardPicture[i+1] = new ImageIcon("image/card/" + (i+1) + ".png"); 
		}
		
		dx[4] = 24;
		dx[3] = 18;
		dx[2] = 12;
		dx[1] = 6;
		dx[0] = 0;
		
		// Add the JLabel for image to the panel 
		for(int i = 4; i >= 0; i--) {
			playerInitial[i] = new JLabel("");
			add(playerInitial[i]);
			computerInitial[i] = new JLabel("");
			add(computerInitial[i]);
		}
		
		jlblShowMoney2.setText("" + nullMoney);
		jlblTabelMoney2.setText("" + contempMoney);
		
		// Set the visibility of the buttons
		jbtStart.setVisible(true);
		jbtContinue.setVisible(false);
		jbtResult.setVisible(false);
		jbtEnd.setVisible(true);
		jbtContinue.setVisible(false);
		jbtResult.setVisible(false);
		jbtTenThousand.setVisible(false);
		jbtFiftyThousand.setVisible(false);
		jbtOneHundredThousand.setVisible(false);
		jbtFiveHundredThousand.setVisible(false);
		jbtOneMillion.setVisible(false);
		jbtFiveMillion.setVisible(false);
		jbtTenMillion.setVisible(false);
		jbtFiftyMillion.setVisible(false);
		jbtShowHand.setVisible(false);
		jbtCancel.setVisible(false);
		jbtConfirm.setVisible(false);
		
		// Set the size and location of the component
		jbtStart.setBounds(835, 550, 150, 30);
		jbtContinue.setBounds(835, 580, 150, 30);
		jbtResult.setBounds(835, 610, 150, 30);
		jbtEnd.setBounds(835, 640, 150, 60);
		jlblShowMoney1.setBounds(835, 200, 150, 30);
		jlblShowMoney2.setBounds(835, 230, 150, 30);
		jlblTabelMoney1.setBounds(500, 340, 150, 20);
		jlblTabelMoney2.setBounds(650, 340, 150, 20);
		jbtTenThousand.setBounds(835, 260, 75, 30);
		jbtFiftyThousand.setBounds(910, 260, 75, 30);
		jbtOneHundredThousand.setBounds(835, 290, 75, 30);
		jbtFiveHundredThousand.setBounds(910, 290, 75, 30);
		jbtOneMillion.setBounds(835, 320, 75, 30);
		jbtFiveMillion.setBounds(910, 320, 75, 30);
		jbtTenMillion.setBounds(835, 350, 75, 30);
		jbtFiftyMillion.setBounds(910, 350, 75, 30);
		jbtShowHand.setBounds(835, 380, 150, 30);
		jbtCancel.setBounds(835, 410, 150, 30);
		jbtConfirm.setBounds(835, 440, 150, 30);
		playerType.setBounds(100, 400, 250, 95);
		computerType.setBounds(100, 220, 250, 95);
		gameResult.setBounds(120, 340, 150, 20);
		jlblMoneyOfPlayer1.setBounds(50, 600, 250, 20);
		jlblMoneyOfPlayer2.setBounds(50, 630, 250, 20);
		jlblMoneyOfComputer1.setBounds(50, 20, 250, 20);
		jlblMoneyOfComputer2.setBounds(50, 50, 250, 20);
		
		// Set some property of the JButton and JLabels
		jbtStart.setBackground(Color.YELLOW);
		jbtContinue.setBackground(Color.YELLOW);
		jbtResult.setBackground(Color.YELLOW);
		jbtEnd.setBackground(Color.YELLOW);
		jbtShowHand.setBackground(color1);
		jbtCancel.setBackground(color2);
		jbtConfirm.setBackground(color3);
		gameResult.setFont(font1);
		gameResult.setForeground(Color.RED);
		jlblTabelMoney1.setFont(font2);
		jlblTabelMoney1.setForeground(Color.RED);
		jlblTabelMoney2.setFont(font1);
		jlblTabelMoney2.setForeground(Color.RED);
		jlblShowMoney1.setFont(font2);
		jlblShowMoney1.setForeground(Color.RED);
		jlblShowMoney2.setFont(font1);
		jlblShowMoney2.setForeground(Color.RED);
		jlblMoneyOfPlayer1.setFont(font1);
		jlblMoneyOfPlayer2.setFont(font1);
		jlblMoneyOfPlayer1.setForeground(Color.YELLOW);
		jlblMoneyOfPlayer2.setForeground(Color.YELLOW);
		jlblMoneyOfComputer1.setFont(font1);
		jlblMoneyOfComputer2.setFont(font1);
		jlblMoneyOfComputer1.setForeground(Color.YELLOW);
		jlblMoneyOfComputer2.setForeground(Color.YELLOW);
		jlblMoneyOfPlayer2.setText("" + moneyOfPlayer.money);
		jlblMoneyOfComputer2.setText("" + moneyOfComputer.money);
		
		// Add the component to the panel
		add(jbtStart);
		add(jbtContinue);
		add(jbtResult);
		add(jbtEnd);
		add(jlblShowMoney1);
		add(jlblShowMoney2);
		add(jbtTenThousand);
		add(jbtFiftyThousand);
		add(jbtOneHundredThousand);
		add(jbtFiveHundredThousand);
		add(jbtOneMillion);
		add(jbtFiveMillion);
		add(jbtTenMillion);
		add(jbtFiftyMillion);
		add(jbtShowHand);
		add(jbtCancel);
		add(jbtConfirm);
		add(jlblMoneyOfPlayer1);
		add(jlblMoneyOfPlayer2);
		add(jlblMoneyOfComputer1);
		add(jlblMoneyOfComputer2);
		add(jlblTabelMoney1);
		add(jlblTabelMoney2);
		add(playerType);
		add(computerType);
		add(gameResult);
		
		// Register listener with buttons
		jbtStart.addActionListener(new ButtonListener());
		jbtContinue.addActionListener(new ButtonListener());
		jbtResult.addActionListener(new ButtonListener());
		jbtEnd.addActionListener(new ButtonListener());
		jbtTenThousand.addActionListener(new ButtonListener());
		jbtFiftyThousand.addActionListener(new ButtonListener());
		jbtOneHundredThousand.addActionListener(new ButtonListener());
		jbtFiveHundredThousand.addActionListener(new ButtonListener());
		jbtOneMillion.addActionListener(new ButtonListener());
		jbtFiveMillion.addActionListener(new ButtonListener());
		jbtTenMillion.addActionListener(new ButtonListener());
		jbtFiftyMillion.addActionListener(new ButtonListener());
		jbtShowHand.addActionListener(new ButtonListener());
		jbtCancel.addActionListener(new ButtonListener());
		jbtConfirm.addActionListener(new ButtonListener());
	} // End constructor
	
	// Get the imageName from FrameWork and then set the icon
	public void setRole(String imageName) {
		role[1].setIcon(new ImageIcon(imageName)); 
	}
	
	// Paint the component
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(new ImageIcon("image/back.png").getImage(), 0, 0, 1000, 740, this);
		g.drawImage(new ImageIcon("image/desk.png").getImage(), 0, 100, 1000, 450, this);
		
		// Make an animation of the cards
		for(int i = 4; i >= 0; i--) {
			playerInitial[i].setBounds(i * 30 + 375 + dx[i] * p, 400, 72, 96);
			computerInitial[i].setBounds(i * 30 + 375 + dx[i] * p, 220, 72, 96);
		}
		if (p == 10) {
			timer1.stop();
		}
		
		// Make an animation of the game result
		if (q / 5 % 2  == 0) {
			gameResult.setText(play.showGameResult(play.getPriority(0), play.getPriority(1)));
		}
		else {
			gameResult.setText("");
		}
	} // End paintComponent
	
	class ButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) { // Implement actionPerformed to let the event work
			
			// If jbtStart is clicked
			if (e.getSource() == jbtStart) {
				timer2.stop();
				play = new CardManipulate2();
				p = 0;
				playerInitial[0].setIcon(cardPicture[play.getNumber(0)]);
				playerInitial[1].setIcon(cardPicture[play.getNumber(1)]);
				computerInitial[0].setIcon(new ImageIcon("image/card/b1fv.png"));
				computerInitial[1].setIcon(cardPicture[play.getNumber(6)]);
				for (int i = 2; i < 5; i++) {
					playerInitial[i].setIcon(new ImageIcon(""));
					computerInitial[i].setIcon(new ImageIcon(""));
				}
				
				playerType.setIcon(new ImageIcon(""));
				computerType.setIcon(new ImageIcon(""));
				gameResult.setText("< Game result >");
				
				jbtStart.setVisible(false);
				jbtTenThousand.setVisible(true);
				jbtFiftyThousand.setVisible(true);
				jbtOneHundredThousand.setVisible(true);
				jbtFiveHundredThousand.setVisible(true);
				jbtOneMillion.setVisible(true);
				jbtFiveMillion.setVisible(true);
				jbtTenMillion.setVisible(true);
				jbtFiftyMillion.setVisible(true);
				jbtShowHand.setVisible(true);
				jbtCancel.setVisible(true);
				validate();
				repaint();
				
				if (moneyOfPlayer.getMoney() <= 0) { // Lose all money,and change to another face
					EndFaceLose lose = new EndFaceLose();
					endFaceLoseFrame.setTitle("Bankruptcy");
					endFaceLoseFrame.setSize(700, 700); 
					endFaceLoseFrame.setLocationRelativeTo(null); 
					endFaceLoseFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					endFaceLoseFrame.add(lose);
					setVisible(false);
					endFaceLoseFrame.setVisible(true);
				}
				else if (moneyOfComputer.getMoney() <= 0) { // Win all money,and change to another face
					EndFaceWin win = new EndFaceWin();
					endFaceWinFrame.setTitle("Win All");
					endFaceWinFrame.setSize(700, 700);
					endFaceWinFrame.setLocationRelativeTo(null); 
					endFaceWinFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					endFaceWinFrame.add(win);
					setVisible(false);
					endFaceWinFrame.setVisible(true);
				}
			}
			
			// If jbtContinue is clicked
			if (e.getSource() == jbtContinue) {
				for (int i = 2; i <= jbtContinueClickTime; i++) {
					playerInitial[i].setIcon(cardPicture[play.getNumber(i)]);
					computerInitial[i].setIcon(cardPicture[play.getNumber(i+5)]);
				}
				jbtContinueClickTime++;
				
				jbtTenThousand.setVisible(true);
				jbtFiftyThousand.setVisible(true);
				jbtOneHundredThousand.setVisible(true);
				jbtFiveHundredThousand.setVisible(true);
				jbtOneMillion.setVisible(true);
				jbtFiveMillion.setVisible(true);
				jbtTenMillion.setVisible(true);
				jbtFiftyMillion.setVisible(true);
				jbtShowHand.setVisible(true);
				jbtCancel.setVisible(true);
				jbtContinue.setVisible(false);

				if (jbtContinueClickTime == 5) {
					jbtTenThousand.setVisible(false);
					jbtFiftyThousand.setVisible(false);
					jbtOneHundredThousand.setVisible(false);
					jbtFiveHundredThousand.setVisible(false);
					jbtOneMillion.setVisible(false);
					jbtFiveMillion.setVisible(false);
					jbtTenMillion.setVisible(false);
					jbtFiftyMillion.setVisible(false);
					jbtShowHand.setVisible(false);
					jbtCancel.setVisible(false);
					jbtConfirm.setVisible(false);
					jbtContinue.setVisible(false);
					jbtResult.setVisible(true);
					jbtContinueClickTime = 2;
				}
				validate();
				repaint();
			}
			
			// If jbtResult is clicked
			if (e.getSource() == jbtResult) {
				x = 0;
				
				computerInitial[0].setIcon(cardPicture[play.getNumber(5)]);
				
				timer1.start();
				timer2.start();
				
				playerType.setIcon(new ImageIcon(play.getCardType(0)));
				computerType.setIcon(new ImageIcon(play.getCardType(5)));
				gameResult.setText(play.showGameResult(play.getPriority(0), play.getPriority(1)));
				
				// Set the money
				if (play.playerWin == true) {
					moneyOfPlayer.setMoney(moneyOfPlayer.getMoney() + Integer.parseInt(jlblTabelMoney2.getText()));
					moneyOfComputer.setMoney(moneyOfComputer.getMoney() - Integer.parseInt(jlblTabelMoney2.getText()));
				}
				else if (play.playerWin == false) {
					moneyOfPlayer.setMoney(moneyOfPlayer.getMoney() - Integer.parseInt(jlblTabelMoney2.getText()));
					moneyOfComputer.setMoney(moneyOfComputer.getMoney() + Integer.parseInt(jlblTabelMoney2.getText()));
				}
				else {
					moneyOfPlayer.setMoney(moneyOfPlayer.getMoney() + (Integer.parseInt(jlblTabelMoney2.getText()) / 2));
					moneyOfComputer.setMoney(moneyOfComputer.getMoney() + (Integer.parseInt(jlblTabelMoney2.getText()) / 2));
				}
				
				jlblMoneyOfPlayer2.setText("" + moneyOfPlayer.getMoney());
				jlblMoneyOfComputer2.setText("" + moneyOfComputer.getMoney());
				
				nullMoney = 0;
				contempMoney = 0;
				jlblShowMoney2.setText("" + nullMoney);
				jlblTabelMoney2.setText("" + contempMoney);
				jbtContinue.setVisible(false);
				jbtResult.setVisible(false);
				jbtStart.setVisible(true);
				validate();
				repaint();
			}
			
			// If jbtEnd is clicked
			if (e.getSource() == jbtEnd) {
				int option = JOptionPane.YES_NO_CANCEL_OPTION;
				option = JOptionPane.showConfirmDialog(null, "You'll lose all your money!!\nAre you sure? ");
				if(option == JOptionPane.YES_OPTION) {
					System.exit(0);
				}
				else {
				}
			}
			
			/** 獎金處理的部分似乎還有一些bug，可從兩人的獎金相加未等於原本的兩翼看出
			 * 問題可能酒出在以下的判斷，未使某些轉換後的值初始化但是我始終 Debug 
			 * de 不出來!!!*/
			
			if (moneyOfPlayer.getMoney() > 0 || moneyOfComputer.getMoney() > 0) {  
				if (e.getSource() == jbtTenThousand) {
					jbtConfirm.setVisible(true);
					contempMoney += 10000 * 2;
					nullMoney += 10000;
					jlblShowMoney2.setText("" + nullMoney);
					jlblTabelMoney2.setText("" + contempMoney);
				}
				
				if (e.getSource() == jbtFiftyThousand) {
					jbtConfirm.setVisible(true);
					contempMoney += 50000 * 2;
					nullMoney += 50000;
					jlblShowMoney2.setText("" + nullMoney);
					jlblTabelMoney2.setText("" + contempMoney);
				}
				
				if (e.getSource() == jbtOneHundredThousand) {
					jbtConfirm.setVisible(true);
					contempMoney += 100000 * 2;
					nullMoney += 100000;
					jlblShowMoney2.setText("" + nullMoney);
					jlblTabelMoney2.setText("" + contempMoney);
				}
				
				if (e.getSource() == jbtFiveHundredThousand) {
					jbtConfirm.setVisible(true);
					contempMoney = 500000 * 2;
					nullMoney += 500000;
					jlblShowMoney2.setText("" + nullMoney);
					jlblTabelMoney2.setText("" + contempMoney);
				}
				
				if (e.getSource() == jbtOneMillion) {
					jbtConfirm.setVisible(true);
					contempMoney += 1000000 * 2;
					nullMoney += 1000000;
					jlblShowMoney2.setText("" + nullMoney);
					jlblTabelMoney2.setText("" + contempMoney);
				}
				
				if (e.getSource() == jbtFiveMillion) {
					jbtConfirm.setVisible(true);
					contempMoney += 5000000 * 2;
					nullMoney += 5000000;
					jlblShowMoney2.setText("" + nullMoney);
					jlblTabelMoney2.setText("" + contempMoney);
				}
				
				if (e.getSource() == jbtTenMillion) {
					jbtConfirm.setVisible(true);
					contempMoney += 10000000 * 2;
					nullMoney += 10000000;
					jlblShowMoney2.setText("" + nullMoney);
					jlblTabelMoney2.setText("" + contempMoney);
				}
				
				if (e.getSource() == jbtFiftyMillion) {
					jbtConfirm.setVisible(true);
					contempMoney += 50000000 * 2;
					nullMoney += 50000000;
					jlblShowMoney2.setText("" + nullMoney);
					jlblTabelMoney2.setText("" + contempMoney);
				}
				
				if (e.getSource() == jbtShowHand) {
					jbtConfirm.setVisible(true);
					contempMoney += moneyOfPlayer.getMoney() + moneyOfComputer.getMoney();
					nullMoney += moneyOfPlayer.getMoney();
					jlblShowMoney2.setText("" + nullMoney);
					jlblTabelMoney2.setText("" + contempMoney);
				}
				
				if (e.getSource() == jbtCancel) {
					jbtConfirm.setVisible(false);
					contempMoney = 0;
					nullMoney = 0;
					jlblShowMoney2.setText("" + nullMoney);
					jlblTabelMoney2.setText("" + contempMoney);
				}
			}
			else {
				moneyOfPlayer.setMoney(0);
				jbtTenThousand.setVisible(false);
				jbtFiftyThousand.setVisible(false);
				jbtOneHundredThousand.setVisible(false);
				jbtFiveHundredThousand.setVisible(false);
				jbtOneMillion.setVisible(false);
				jbtFiveMillion.setVisible(false);
				jbtTenMillion.setVisible(false);
				jbtFiftyMillion.setVisible(false);
				jbtShowHand.setVisible(false);
				jbtCancel.setVisible(false);
				jbtContinue.setVisible(true);
			}
				
			if (e.getSource() == jbtConfirm) { // If jbtConfirm is clicked
				if (nullMoney > moneyOfPlayer.getMoney()) {
					int option = JOptionPane.YES_OPTION;
					option = JOptionPane.showConfirmDialog(null, "下注金額已超過玩家所擁有的錢，請重新設定!! ");
					if (option == JOptionPane.YES_OPTION) {
						nullMoney = 0;
						jlblShowMoney2.setText("" + nullMoney);
					}
					else {
						
					}
				}
				else if (nullMoney > moneyOfComputer.getMoney()) {
					nullMoney = moneyOfComputer.getMoney();
					contempMoney += 2 * moneyOfComputer.getMoney();
				}
				else {
					//moneyOfPlayer.setMoney(moneyOfPlayer.getMoney() - nullMoney);
					//moneyOfComputer.setMoney(moneyOfComputer.getMoney() - nullMoney);
					//jlblMoneyOfPlayer2.setText("" + moneyOfPlayer.getMoney());
					//jlblMoneyOfComputer2.setText("" + moneyOfComputer.getMoney());
					nullMoney = 0;
					jlblShowMoney2.setText("" + nullMoney);

					jbtTenThousand.setVisible(false);
					jbtFiftyThousand.setVisible(false);
					jbtOneHundredThousand.setVisible(false);
					jbtFiveHundredThousand.setVisible(false);
					jbtOneMillion.setVisible(false);
					jbtFiveMillion.setVisible(false);
					jbtTenMillion.setVisible(false);
					jbtFiftyMillion.setVisible(false);
					jbtShowHand.setVisible(false);
					jbtCancel.setVisible(false);
					jbtConfirm.setVisible(false);
					jbtContinue.setVisible(true);
				}
			}
		}
	} // End class ButtonListener
	
	class TimerListener1 implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			p++;
			repaint();
		}
	}
	
	class TimerListener2 implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			q++;
			repaint();
		}
	}
} // End class Desk2