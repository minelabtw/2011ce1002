package fp.s100502028;

public class CardManipulate2 {
	private int[] cardNumber = new int[52];
	private int[] cardPoint = new int[10];
	private int[] cardColor = new int[10];
	private int[] tempDiff = new int[10];
	private int[] arrayForPair = new int [4];
	private int[] typePriority = new int[2];
	
	private String straightFlush = new String("image/straightFlush.png");
	private String fourOfAKind = new String("image/fourOfAKind.png");
	private String fullHouse = new String("image/fullHouse.png");
	private String flush = new String("image/flush.png");
	private String straight = new String("image/straight.png");
	private String threeOfAKind = new String("image/threeOfAKind.png");
	private String twoPairs = new String("image/twoPairs.png");
	private String onePair = new String("image/onePair.png");
	private String highCard = new String("image/highCard.png");
	private String win = "You Win!!";
	private String lose = "You Lose!!";
	
	private boolean isStraight = false;
	private boolean isFlush = false;
	protected boolean playerWin = false;

	// Constructor to generate random number
	public CardManipulate2() {
		/*cardNumber[0] = 3;
		cardNumber[1] = 16;
		cardNumber[2] = 1;
		cardNumber[3] = 14;
		cardNumber[4] = 13;
		
		cardNumber[5] = 13;
		cardNumber[6] = 26;
		cardNumber[7] = 2;
		cardNumber[8] = 15;
		cardNumber[9] = 4;
		/*
		for (int i = 0; i < 5; i++) {
			cardNumber[i] = (int) (Math.random() * 52 + 1);
			for (int j = 0; j < i; j++) { // If the number is repeated, run again
				if (cardNumber[j] == cardNumber[i])
					i--;
			}
		}
		*/
		
		int s = 0, y = 0;
		
		/** 此處的發牌成是由高鎮泰同學友情贊助 */
		for (int x = 0; x < 10; x++) {
			while (y == 0) {
				s = (int) (Math.random() * 52 + 1);
				for (int i = 0; i < 52; i++) {
					if (cardNumber[i] == s) {
						y = 0;
						break;
					}
					y = 1;
				}
			}
			cardNumber[x] = s;
			y=0;
		}
	} // End the Constructor

	// Return the number
	public int getNumber(int num) {
		return cardNumber[num];
	}

	// Return the priority of the cards' type
	public int getPriority(int num) {
		return typePriority[num];
	}

	// Method to determine the cards' type
	public String getCardType(int c) {
		int[] cardKind = new int[14];
		int twoSameKind = 0, threeSameKind = 0, fourSameKind = 0;
		int d = 0;
		if (c == 5) {
			d = 1;
		}
		// For loop to get the kind of the card
		for (int i = c; i < c + 5; i++) {
			if (cardNumber[i] % 13 == 1) // A
				cardKind[13]++;
			else if (cardNumber[i] % 13 == 2) // 2
				cardKind[1]++;
			else if (cardNumber[i] % 13 == 3) // 3
				cardKind[2]++;
			else if (cardNumber[i] % 13 == 4) // 4
				cardKind[3]++;
			else if (cardNumber[i] % 13 == 5) // 5
				cardKind[4]++;
			else if (cardNumber[i] % 13 == 6) // 6
				cardKind[5]++;
			else if (cardNumber[i] % 13 == 7) // 7
				cardKind[6]++;
			else if (cardNumber[i] % 13 == 8) // 8
				cardKind[7]++;
			else if (cardNumber[i] % 13 == 9) // 9
				cardKind[8]++;
			else if (cardNumber[i] % 13 == 10) // 10
				cardKind[9]++;
			else if (cardNumber[i] % 13 == 11) // J
				cardKind[10]++;
			else if (cardNumber[i] % 13 == 12) // Q
				cardKind[11]++;
			else // K
				cardKind[12]++;
		}
		
		for (int i = c; i < c + 5; i++) {
			cardColor[i] = (cardNumber[i] - 1) / 13; // 花色
			if (cardNumber[i] % 13 == 0) {
				cardPoint[i] = 13;
			}
			else if (cardNumber[i] % 13 == 1) { // Save the point of Ace to be 14 
				cardPoint[i] = 14;
			}
			else
				cardPoint[i] = cardNumber[i] % 13; // 點數
		}  
		
		int j = c + 4;
		int tempi;
		
		// For loop to save the cards in order from small to big
		for (int i = c; i < c + 5; i++) {
			for (int l = j; l > i; l--) {
				if (cardPoint[l] < cardPoint[l - 1]) {
					tempi = cardPoint[l - 1];
					cardPoint[l - 1] = cardPoint[l];
					cardPoint[l] = tempi;
					tempi = cardColor[l-1];
					cardColor[l-1] = cardColor[l];
					cardColor[l] = tempi;
				}
			}
		}
		
		// For loop to determine whether it is flush or not
		for (int i = c + 1; i < c + 5; i++) {
			 if(cardColor[i] != cardColor[i - 1]) { // Card color is not the same
                 isFlush = false;
                 break;
			 }
			 else 
				 isFlush = true;
		}
		
		// For loop to determine whether it is straight or not
		for (int i = c + 1; i < c + 5; i++) {
			tempDiff[i - 1] = cardPoint[i] - cardPoint[i - 1];
			if (tempDiff[i - 1] != 1) { // Not continuous
				isStraight = false;
				break;
			}
			else {
				isStraight = true;
			}
		}
		
		// For loop to calculate the same kind card
		for (int i = 1; i < 14; i++) {
			if (cardKind[i] == 2) {
				twoSameKind++;
				
				// Save the pair of player into the array 
				if(c == 0) {
					for (int a = c; a < c + 2; a++) {
						arrayForPair[a] = i;
					}
					
					// Change the order of the pair
					if (arrayForPair[0] < arrayForPair[1]) {
						int temp = arrayForPair[0];
						arrayForPair[0] = arrayForPair[1];
						arrayForPair[1] = temp;
					}
				}
				
				// Save the pair of computer into the array
				else if (c == 5) {
					for (int a = c - 3; a < c - 1; a++) {
						arrayForPair[a] = i;
					}
					
					// Change the order of the pair
					if (arrayForPair[2] < arrayForPair[3]) {
						int temp = arrayForPair[2];
						arrayForPair[2] = arrayForPair[3];
						arrayForPair[3] = temp;
					}
				}
			}
			else if (cardKind[i] == 3)
				threeSameKind++;
			else if (cardKind[i] == 4)
				fourSameKind++;
		}

		// Several if statement to determine the type of the cards and its priority
		if (isFlush == true && isStraight == true) { // Straight Flush
			typePriority[d] = 9;
			return straightFlush;
		}
		
		else if (isFlush == true && isStraight == false) { // Flush
			typePriority[d] = 6;
			return flush;
		}
		
		else if (isFlush == false && isStraight == true) { // Straight
			typePriority[d] = 5;
			return straight;
		}
		
		else {
			if (fourSameKind == 1) { // Four of a kind
				typePriority[d] = 8;
				return fourOfAKind;
			}

			else if (threeSameKind == 1) {
				if (twoSameKind == 1) { // Full house
					typePriority[d] = 7;
					return fullHouse;
				} else {
					typePriority[d] = 4; // Three of a kind
					return threeOfAKind;
				}
			}

			else if (twoSameKind == 2) { // Two pairs
				typePriority[d] = 3;
				return twoPairs;
			}

			else if (twoSameKind == 1) { // One pair
				typePriority[d] = 2;
				return onePair;
			}

			else { // High Card
				typePriority[d] = 1;
				return highCard;
			}
		}
	} // End method getCardType()
	
	// Method to determine and show the game result
	public String showGameResult(int p, int c) {
		if (c > p) // Player lose
			playerWin = false;
		else if (c < p) // Player win
			playerWin = true;
		else {
			if (c == 1) { // If both computer and player are high card 
				if (cardPoint[4] > cardPoint[9])
					playerWin = true;
				else if (cardPoint[4] == cardPoint[9]) {
					if (cardColor[4] < cardColor[9])
						playerWin = true;
					else
						playerWin = false;
				}
				else
					playerWin = false;
			}
			
			else if (c == 2 || c == 3) { // If both computer and player are one pair or two pairs
				if (arrayForPair[0] > arrayForPair[2])
					playerWin = true;
				else
					playerWin = false;
			}
			
			else
				return "Draw";
		}
		
		if (playerWin == true)  
			return win;
		else if (playerWin == false)
			return lose;
		else 
			return "Draw";
	} // End method showGameResult
} // End CardManipulate2