package fp.s100502028;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class FrameWork extends JPanel implements ActionListener {
	private JLabel jlblShowMessage = new JLabel("請選擇一個角色:");
	private JLabel jlblPicture = new JLabel();
	
	private JRadioButton jrbGambleKing = new JRadioButton("賭王");
	private JRadioButton jrbGambleMan = new JRadioButton("賭俠");
	private JRadioButton jrbGambleSaint = new JRadioButton("賭聖");
	private JRadioButton jrbGambleGhost = new JRadioButton("獨眼龍");
	private JRadioButton jrbGambleJunk = new JRadioButton("用facebook帳號登入");
	
	private JFrame desk2Frame = new JFrame(); // Create a frame to put a panel on it
	private Desk2 desk = new Desk2(); // Create an object of Desk2 type, which is a panel
	
	private Color color = new Color(255, 242, 0);
	private Font font = new Font("標楷體", Font.BOLD, 20);
	protected AudioClip music;
	
	// Constructor
	public FrameWork() {
		setLayout(null);
		
		// Create a radio-button group to group five buttons
		ButtonGroup group1 = new ButtonGroup();
		group1.add(jrbGambleKing);
		group1.add(jrbGambleMan);
		group1.add(jrbGambleSaint);
		group1.add(jrbGambleGhost);
		group1.add(jrbGambleJunk);
		
		// Set some property of the JRadioButton and JLabels
		jrbGambleKing.setContentAreaFilled(false);
		jrbGambleMan.setContentAreaFilled(false);
		jrbGambleSaint.setContentAreaFilled(false);
		jrbGambleGhost.setContentAreaFilled(false);
		jrbGambleJunk.setContentAreaFilled(false);
		jlblShowMessage.setForeground(Color.RED);
		jrbGambleKing.setForeground(color);
		jrbGambleMan.setForeground(color);
		jrbGambleSaint.setForeground(color);
		jrbGambleGhost.setForeground(color);
		jrbGambleJunk.setForeground(color);
		jlblShowMessage.setFont(font);
		jrbGambleKing.setFont(font);
		jrbGambleMan.setFont(font);
		jrbGambleSaint.setFont(font);
		jrbGambleGhost.setFont(font);
		jrbGambleJunk.setFont(font);
		
		// Add JRadioButton and JLabels to this panel  
		add(jlblShowMessage);
		add(jrbGambleKing);
		add(jrbGambleMan);
		add(jrbGambleSaint);
		add(jrbGambleGhost);
		add(jrbGambleJunk);
		add(jlblPicture);
		
		// Register listener with buttons
		jrbGambleKing.addActionListener(this);
		jrbGambleMan.addActionListener(this);
		jrbGambleSaint.addActionListener(this);
		jrbGambleGhost.addActionListener(this);
		jrbGambleJunk.addActionListener(this);
		
		// Set the property of the frame desk2Frame
		desk2Frame.setTitle("Game");
		desk2Frame.setSize(1000, 740); 
		desk2Frame.setLocationRelativeTo(null); 
		desk2Frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		desk2Frame.add(desk);
	} // End constructor
	
	// Paint the component
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		// Draw the background of the panel
		g.drawImage(new ImageIcon("image/background.png").getImage(), 0, 0, getWidth(), getHeight(), this);
		
		// Set the size and location of the component
		jlblShowMessage.setBounds(0, 0, getWidth() / 2, getHeight() / 5);
		jrbGambleKing.setBounds(10, 40, getWidth() / 7, getHeight() / 7);
		jrbGambleMan.setBounds(20, 80, getWidth() / 7, getHeight() / 7);
		jrbGambleSaint.setBounds(30, 120, getWidth() / 7, getHeight() / 7);
		jrbGambleGhost.setBounds(40, 160, getWidth() / 5, getHeight() / 7);
		jrbGambleJunk.setBounds(50, 200, getWidth() / 2, getHeight() / 7);
		jlblPicture.setBounds(getWidth() - 220, getHeight() - 150, (getWidth() * 220) / 500, (getHeight() * 3) / 10);
	}
	
	// Implement actionPerformed to let the event work
	public void actionPerformed(ActionEvent e) {
		int option = JOptionPane.YES_NO_CANCEL_OPTION;
		
		if(e.getSource() == jrbGambleKing) { // If jrbGambleKing is clicked
			jlblPicture.setIcon(new ImageIcon("image/king.png")); // Set the icon to the label
			
			// Prompt the user to answer questions
			option = JOptionPane.showConfirmDialog(null, "Are you sure?");
			if(option == JOptionPane.YES_OPTION) {
				music = Applet.newAudioClip(getClass().getResource("god.wav")); // Create an audio clip
				music.play(); // Play the audio
				desk.setRole("image/king.png"); // Pass the name of the image to Desk2
				setVisible(false);
				desk2Frame.setVisible(true); // Change to another frame and panel
			}
			else {
			}
		}
		
		if(e.getSource() == jrbGambleMan) { // If jrbGambleMan is clicked
			jlblPicture.setIcon(new ImageIcon("image/man.png"));
			option = JOptionPane.showConfirmDialog(null, "Are you sure?");
			if(option == JOptionPane.YES_OPTION) {
				music = Applet.newAudioClip(getClass().getResource("god.wav"));
				music.play();
				desk.setRole("image/man.png");
				setVisible(false);
				desk2Frame.setVisible(true); 
			}
			else {
			}
		}
		
		if(e.getSource() == jrbGambleSaint) { // If jrbGambleSaint is clicked
			jlblPicture.setIcon(new ImageIcon("image/saint.png"));
			option = JOptionPane.showConfirmDialog(null, "Are you sure?");
			if(option == JOptionPane.YES_OPTION) {
				music = Applet.newAudioClip(getClass().getResource("god.wav"));
				music.play();
				desk.setRole("image/saint.png");
				setVisible(false);
				desk2Frame.setVisible(true); 
			}
			else {
			}
		}
		
		if(e.getSource() == jrbGambleGhost) { // If jrbGambleGhost is clicked
			jlblPicture.setIcon(new ImageIcon("image/junk.png"));
			option = JOptionPane.showConfirmDialog(null, "Are you sure?");
			if(option == JOptionPane.YES_OPTION) {
				music = Applet.newAudioClip(getClass().getResource("god.wav"));
				music.play();
				desk.setRole("image/junk.png");
				setVisible(false);
				desk2Frame.setVisible(true); 
			}
			else {
			}
		}
		
		if(e.getSource() == jrbGambleJunk) { // If jrbGambleJunk is clicked
			jlblPicture.setIcon(new ImageIcon("image/facebook.png"));
			option = JOptionPane.showConfirmDialog(null, "Are you sure?");
			if(option == JOptionPane.YES_OPTION) {
				music = Applet.newAudioClip(getClass().getResource("god.wav"));
				music.play();
				desk.setRole("image/facebook.png");
				setVisible(false);
				desk2Frame.setVisible(true); 
			}
			else {
			}
		}
	} // End actionPerformed
} // End class FrameWork
