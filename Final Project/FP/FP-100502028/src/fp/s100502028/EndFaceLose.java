package fp.s100502028;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class EndFaceLose extends JPanel implements ActionListener {
	private JButton jbtRestart = new JButton("重新遊戲");
	private JButton jbtEnd = new JButton("結束遊戲");
	
	// Constructor
	public EndFaceLose() {
		setLayout(null);
		add(jbtRestart); // Add button to panel
		add(jbtEnd);
		
		// Register listener with buttons
		jbtRestart.addActionListener(this);
		jbtEnd.addActionListener(this);
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(new ImageIcon("image/rich45.png").getImage(), 0, 0, getWidth(), getHeight(), this); // Draw the background of the panel
		
		// Draw the buttons, set size and location
		jbtRestart.setBounds(getWidth() / 2 - 100, getHeight() - 50, 100, 50); 
		jbtEnd.setBounds(getWidth() / 2, getHeight() - 50, 100, 50);
	}
	
	// Method to let the button event work
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == jbtRestart) { // Restart the program
			setVisible(false);
			FinalProject fp = new FinalProject();
			fp.setTitle("Game");
			fp.setSize(500,500); 
			fp.setLocationRelativeTo(null);
			fp.setVisible(true);
			this.setVisible(false);
		}
		
		if (e.getSource() == jbtEnd) {
			System.exit(0); // End the program
		}
	}
} // End class
