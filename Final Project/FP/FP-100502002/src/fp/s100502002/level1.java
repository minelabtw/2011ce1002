package fp.s100502002;
import java.awt.GridLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
public class level1 extends JPanel
{
	private JButton [][] button = new JButton[6][4];
	private static boolean key[][] = new boolean[6][4];
	private JPanel all = new JPanel();
	protected ImageIcon ca = new ImageIcon("image/ca.png");
	public JButton[][] getbutton()
	{
		return button;
	}
	public static boolean [][] getkey()
	{
		return key;
	}
	public JPanel getlevel1()
	{
		return all;
	}
	public static int getm()
	{
		return 6;
	}
	public static int getn()
	{
		return 4;
	}
	public static int rabbits()
	{
		return 1;
	}
	public static int block()
	{
		return 9;
	}
	public level1()
	{
		all.setLayout(new GridLayout(6,4,1,1));
		for(int i=0;i<6;i++)
		{
			for(int j=0;j<4;j++)
			{
				button[i][j] = new JButton();
				all.add(button[i][j]);
				if((i==0&&j==0)||(i==0&&j==2) || (i==1&j==3)|| (i==3&&j==3) || (i==4&&j==0) ||(i==4&&j==2) ||(i==5&&j==0)||(i==4&&j==2)||(i==5&&j==2)||(i==5&&j==3))
				{
					all.remove(button[i][j]);
					all.add(new JLabel());
					key[i][j]=false;
				}
				else
				{
					//button[i][j].setText("i="+i +"j="+j);
					button[i][j].setIcon(ca);
					
				}	
			}
				
		}
		
	}

}
