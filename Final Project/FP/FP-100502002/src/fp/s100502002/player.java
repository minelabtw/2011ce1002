package fp.s100502002;

public class player 
{
	private boolean array[][];
	private boolean hasbutton[][];
	private int score;
	private int level;
	private boolean flag1;
	private boolean flag2;
	private boolean flag3;
	private boolean flag4;
	private boolean flag5;
	private boolean flag6;
	//把array[][]全都設成false(三關done)
	public void setboolean()
	{
		if(level==1)
		{
			for(int i=0;i<level1.getm();i++)
			{
				for(int j=0;j<level1.getn();j++)
				{
					array[i][j]=false;
					
				}
			}
		}
		else if(level==2)
		{
			for(int i=0;i<level2.getm();i++)
			{
				for(int j=0;j<level2.getn();j++)
				{
					array[i][j]=false;
				}
			}
		}
		else if(level==3)
		{
			for(int i=0;i<level3.getm();i++)
			{
				for(int j=0;j<level3.getn();j++)
				{
					array[i][j]=false;
				}
			}
		}
	}
	//傳入是第幾關
	public void setlevel(int l)
	{
		level=l;
	}
	//設定各關卡的按鈕陣列
	public void setarray()
	{
		if(level==1)
		{
			array=new boolean[6][4];
			hasbutton = level1.getkey();
		}
		else if(level==2)
		{
			array = new boolean[6][5];
		}
		else if(level==3)
		{
			array = new boolean [8][8];
		}
	}
	//傳回陣列
	public boolean[][] getarray()
	{
		return array;
	}
	//判斷輸贏(三關done)
	public boolean result()
	{
		
		int counter=0;
		if(level==1)
		{

			for(int i=0;i<6;i++)
			{
				for(int j=0;j<4;j++)
				{
	
					if(array[i][j]==true)
						counter++;
				}
			}
			if(counter==(level1.getm()*level1.getn())-level1.block())
				return true;
			else
				return false;
		}
	
		
		else if(level==3)
		{
			for(int i=0;i<8;i++)
			{
				for(int j=0;j<8;j++)
				{
					if(array[i][j]==true)
						counter++;
				}
			}
			if(counter==(level3.getm()*level3.getn())-level3.block())
				return true;
			else
				return false;
		}
		else
		{
			return false;
		}
	}
	//計算成績
	public int setscore(int time)
	{
		score =(int)(250- time*2);
		if(score<=0)
		{
			score = 0;
		}
		return score;
	}
	//改變button boolean 以改變圖片(ok 沒有bug)
	public void change(int i,int j)
	{
		if(level==1)
		{
			//直排
			for(int k=0;k<level1.getm();k++)
			{
	
				array[k][j]=true;
				
			}
			//橫排
			for(int l=0;l<level1.getn();l++)
			{
				array[i][l]=true;
			}
			//斜的
			int k1=i;
			int l1=j;
			int k2=i;
			int l2=j;
			int k3=i;
			int l3=j;
			int k4=i;
			int l4=j;
			//右上(有bug)
			for(;;)
			{
				int o=k1--;
				int p=l1++;
				if(o<0||p>=level1.getn())
					break;
				else
				array[o][p]=true;
			
				
			}
			//右下(ok)
			for(;;)
			{
				int o=k2++;
				int p=l2++;
				if(o>=level1.getm()||p>=level1.getn())
					break;
				else
				array[o][p]=true;
				
			}
			//左上(ok)
			for(;;)
			{
				int o=k3--;
				int p=l3--;
				if(o<0||p<0)
					break;
				else
				array[o][p]=true;
			}
			//左下
			for(;;)
			{
				int o=k4++;
				int p=l4--;
				if(o>=level1.getm()||p<0)
					break;
				else
				array[o][p]=true;
	
			}
		}
		else if(level==2)
		{
			//直排
			for(int k=0;k<level2.getm();k++)
			{
				array[k][j]=true;
			}
			//橫排
			for(int l=0;l<level2.getn();l++)
			{
				array[i][l]=true;
			}
			//斜的
			int k1=i;
			int l1=j;
			int k2=i;
			int l2=j;
			int k3=i;
			int l3=j;
			int k4=i;
			int l4=j;
			//右上(有bug)
			for(;;)
			{
				int o=k1--;
				int p=l1++;
				if(o<0||p>=level2.getn())
					break;
				else
				array[o][p]=true;
			
				
			}
			//右下(ok)
			for(;;)
			{
				int o=k2++;
				int p=l2++;
				if(o>=level2.getm()||p>=level2.getn())
					break;
				else
				array[o][p]=true;
				
			}
			//左上(ok)
			for(;;)
			{
				int o=k3--;
				int p=l3--;
				if(o<0||p<0)
					break;
				else
				array[o][p]=true;
			}
			//左下
			for(;;)
			{
				int o=k4++;
				int p=l4--;
				if(o>=level2.getm()||p<0)
					break;
				else
				array[o][p]=true;
	
			}
		}
		else if(level==3)
		{
			//直排
			for(int k=0;k<level3.getm();k++)
			{
				array[k][j]=true;
			}
			//橫排
			for(int l=0;l<level3.getn();l++)
			{
				array[i][l]=true;
			}
			//斜的
			int k1=i;
			int l1=j;
			int k2=i;
			int l2=j;
			int k3=i;
			int l3=j;
			int k4=i;
			int l4=j;
			//右上(有bug)
			for(;;)
			{
				int o=k1--;
				int p=l1++;
				if(o<0||p>=level3.getn())
					break;
				else
				array[o][p]=true;
			
				
			}
			//右下(ok)
			for(;;)
			{
				int o=k2++;
				int p=l2++;
				if(o>=level3.getm()||p>=level3.getn())
					break;
				else
				array[o][p]=true;
				
			}
			//左上(ok)
			for(;;)
			{
				int o=k3--;
				int p=l3--;
				if(o<0||p<0)
					break;
				else
				array[o][p]=true;
			}
			//左下
			for(;;)
			{
				int o=k4++;
				int p=l4--;
				if(o>=level3.getm()||p<0)
					break;
				else
				array[o][p]=true;
	
			}
		}
		
		
	}

	//constructor
	public player(int l)
	{
		setlevel(l);
		setarray();
		System.out.println(level);
	}
	
}
