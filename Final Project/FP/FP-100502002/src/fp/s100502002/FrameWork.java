package fp.s100502002;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class FrameWork extends JFrame
{
	private JPanel head = new JPanel();
	private JPanel body = new JPanel();
	private JPanel end = new JPanel();
	private JLabel time= new JLabel();
	private JLabel fin = new JLabel();
	//private JLabel origin = new JLabel();
	private int counter = 0;
	private int minute;
	private int second;
	private int level=1;
	private ImageIcon rabbit = new ImageIcon("image/rabbit.png");
	private ImageIcon rabbit_ca = new ImageIcon("image/rabbit_ca.png");
	private ImageIcon fi = new ImageIcon("image/final.png");
	private ImageIcon ca = new ImageIcon("image/ca.png");
	//private ImageIcon back = new ImageIcon("image/back.png");
	private JButton again = new JButton("try again");
	private level1 l1 = new level1();
	private level2 l2 =new level2();
	private level3 l3 =new level3();
	private player user ;
	private ButtonListener b1 = new ButtonListener();
	private Timer timer = new Timer(1000,new TimerListener());

	//constructor
	public FrameWork()
	{
		body.setBackground(new Color(189,105,255));
		head.setBackground(new Color(189,105,255));
		end.setBackground(new Color(189,105,255));
		timer.start();
		TIME();
		head.add(time);
		display(2);
		addlistener();
		fin.setIcon(fi);
		add(head,BorderLayout.NORTH);
		add(body,BorderLayout.CENTER);
		add(end,BorderLayout.SOUTH);
	}
	public void display(int q)
	{	
		level=q;
		if(level==1)
		{
			head.add(time);
			body.add(l1.getlevel1());
			l1.getlevel1().setBackground(new Color(189,105,255));
			for(int k=0;k<l1.rabbits();k++)
			{
				JLabel ra = new JLabel(rabbit);
				head.add(ra,BorderLayout.WEST);
			}
			end.add(again);
		}
		else if(level==2)
		{
			head.add(time);
			body.add(l2.getlevel2());
			l2.getlevel2().setBackground(new Color(189,105,255));
			for(int k=0;k<l2.rabbits();k++)
			{
				JLabel ra = new JLabel(rabbit);
				head.add(ra,BorderLayout.WEST);
			}
			end.add(again);
			
			
		}
		else if(level==3)
		{
			head.add(time);
			body.add(l3.getlevel3());
			l3.getlevel3().setBackground(new Color(189,105,255));
			for(int k=0;k<l3.rabbits();k++)
			{
				JLabel ra = new JLabel(rabbit);
				head.add(ra,BorderLayout.WEST);
			}
			end.add(again);
		}
		else if(level==4)
		{
			fin.setIcon(fi);
			body.add(fin);
			head.setBackground(new Color(0,255,0));
			end.setBackground(new Color(0,255,0));
			body.setBackground(new Color(0,255,0));
		}
	}
	public void resetlevel()
	{
		level=level++;
	}
	public void remove()
	{
		head.removeAll();
		body.removeAll();
		revalidate();
		validate();
		repaint();
	}
	public void addlistener()
	{
		
		if(level==1)
		{
			again.addActionListener(b1);
			JButton Button[][]=l1.getbutton();
			for(int i=0;i<l1.getm();i++)
			{
				for(int j=0;j<l1.getn();j++)
				{
					Button[i][j].addActionListener(b1);
				}
			}
		}
		else if(level==2)
		{
			JButton Button[][]=l2.getbutton();
			for(int i=0;i<l2.getm();i++)
			{
				for(int j=0;j<l2.getn();j++)
				{
					Button[i][j].addActionListener(b1);
				}
			}
		}
		else if(level==3)
		{
			JButton Button[][]=l3.getbutton();
			for(int i=0;i<l3.getm();i++)
			{
				for(int j=0;j<l3.getn();j++)
				{
					Button[i][j].addActionListener(b1);
				}
			}
		}
	}
	
	
	
	
	//時間沒有bug 是複製上一個的
	public void TIME()//set time
	{
		if(second>=60)
		{
			second =second-60;
			minute++;
		}
		else
		{
			
		}
	}
	class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			second++;
			if(second>=60)
			{
				minute++;
				second=second-60;
			}
			time.setText(minute+":"+second);
			repaint();
		}
	}
	class ButtonListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(level==1)
			{
				user = new player(1);
				boolean array[][] = user.getarray();
				JButton Button[][] = l1.getbutton();
				for(int i=0;i<l1.getm();i++)
				{
					for(int j=0;j<l1.getn();j++)
					{
						if(e.getSource()==Button[i][j])
						{	
							counter++;
							if(!array[i][j])
							{
								user.change(i,j);
							}
							else
							{
								
							}
							System.out.println("I push the i="+i+"j="+j);
							System.out.println("the boolean is "+array[i][j]);
							for(int k=0;k<l1.getm();k++)
							{
								for(int l=0;l<l1.getn();l++)
								{
									System.out.println("i="+l+"j="+k+" is "+array[k][l]);
									if(array[k][l])
									{
										Button[k][l].setIcon(rabbit_ca);
										Button[k][l].setBackground(Color.red);
									}
									else
									{
										Button[k][l].setIcon(ca);
									}	
								}						
							}
							Button[i][j].setBackground(Color.blue);
						}	
					}
					
				}
				if(counter>=l1.rabbits())
				{
					for(int i=0;i<l1.getm();i++)
					{
						for(int j=0;j<l1.getn();j++)
						{
							Button[i][j].setEnabled(false);
							
						}
					}
					if(user.result())
					{
						System.out.println("win");
						remove();
						JOptionPane.showMessageDialog(null, "you win!");
						display(2);
						revalidate();
						addlistener();
						counter=0;
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Try Again!");
						System.out.println("lose");
					}
					
				}
				if(e.getSource()==again)
				{
					counter=0;
					for(int k=0;k<l1.getm();k++)
					{
						for(int l=0;l<l1.getn();l++)
						{
							array[k][l]=false;
							Button[k][l].setIcon(ca);
							Button[k][l].setEnabled(true);
							Button[k][l].setBackground(null);
							revalidate();
							validate();
							repaint();
						
						}
					}
					System.out.println("again");
				}
			}
			
			else if(level==2)
			{
				user = new player(2);
				boolean array[][] = user.getarray();
				JButton Button[][] = l2.getbutton();
				for(int i=0;i<l2.getm();i++)
				{
					for(int j=0;j<l2.getn();j++)
					{
						if(e.getSource()==Button[i][j])
						{
							counter++;
							if(!array[i][j])
							{
								user.change(i,j);
							}
							else
							{
								
							}
							System.out.println("I push the i="+i+"j="+j);
							System.out.println("the boolean is "+array[i][j]);
							for(int k=0;k<l2.getm();k++)
							{
								for(int l=0;l<l2.getn();l++)
								{
									System.out.println("i="+l+"j="+k+" is "+array[k][l]);
									if(array[k][l])
									{
										Button[k][l].setIcon(rabbit_ca);
										Button[k][l].setBackground(Color.red);
										Button[k][l].setEnabled(false);
									}
									else
									{
									
									}	
								}						
							}
							Button[i][j].setBackground(Color.blue);
						}	
						
					}
					
				}
				
				

				if(counter>=l2.rabbits())
				{
					for(int i=0;i<l2.getm();i++)
					{
						for(int j=0;j<l2.getn();j++)
						{
							Button[i][j].setEnabled(false);
						}
					}
			
					int result=0;
					for(int i=0;i<l2.getm();i++)
					{
						for(int j=0;j<l2.getn();j++)
						{
							if(Button[i][j].isBackgroundSet())
							{
								
								result++;
								System.out.println("result:"+result);
							}
							else
							{
								System.out.println("i "+i+" j"+j+"has background"); 
							}		
						}
					}
					if(result== 24)
					{
						System.out.println("win");
						remove();
						JOptionPane.showMessageDialog(null, "you win!");
						display(3);
						revalidate();
						addlistener();
						user.setlevel(3); 
						System.out.println(level);
						counter=0;
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Try Again!"); 
						System.out.println("lose");
						 
					}
					
				}
				if(e.getSource()==again)
				{
					counter=0;
					for(int k=0;k<l2.getm();k++)
					{
						for(int l=0;l<l2.getn();l++)
						{
							array[k][l]=false;
							Button[k][l].setIcon(ca);
							Button[k][l].setEnabled(true);
							Button[k][l].setBackground(null);
							revalidate();
							validate();
							repaint();
						
						}
					}
					System.out.println("again");
				}
			}
			else if(level==3)
			{
				user = new player(3);
				boolean array[][] = user.getarray();
				JButton Button[][] = l3.getbutton();
				for(int i=0;i<l3.getm();i++)
				{
					for(int j=0;j<l3.getn();j++)
					{
						if(e.getSource()==Button[i][j])
						{
							counter++;
							if(!array[i][j])
							{
								user.change(i,j);
							}
							else
							{
								
							}
							System.out.println("I push the i="+i+"j="+j);
							System.out.println("the boolean is "+array[i][j]);
							for(int k=0;k<l3.getm();k++)
							{
								for(int l=0;l<l3.getn();l++)
								{
									System.out.println("i="+l+"j="+k+" is "+array[k][l]);
									if(array[k][l])
									{
										Button[k][l].setIcon(rabbit_ca);
										Button[k][l].setBackground(Color.red);
										//Button[k][l].setEnabled(false);
									}
									else
									{
										Button[k][l].setIcon(ca);
									}	
								}						
							}
							Button[i][j].setBackground(Color.blue);
						}	
					}
				}
				System.out.println("counter :"+counter);
				if(counter>=l3.rabbits())
				{
					for(int i=0;i<l3.getm();i++)
					{
						for(int j=0;j<l3.getn();j++)
						{
							Button[i][j].setEnabled(false);
							
						}
					}
					int result=0;
					for(int i=0;i<l3.getm();i++)
					{
						for(int j=0;j<l3.getn();j++)
						{
							
							if(Button[i][j].isBackgroundSet())
							{
								result++;
								System.out.println("result is :"+result);
							}
							else
							{
								
							}
						}
					}
					if(result>=64)
					{
						System.out.println("win");
						remove();
						JOptionPane.showMessageDialog(null, "You Win!");
						JOptionPane.showMessageDialog(null,"your score:"+user.setscore(second));
						display(4);
						remove(end);
						revalidate();
						addlistener();
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Try Again!"); 
						System.out.println("lose");	 
					}
				}
				if(e.getSource()==again)
				{
					counter=0;
					for(int k=0;k<l3.getm();k++)
					{
						for(int l=0;l<l3.getn();l++)
						{
							array[k][l]=false;
							Button[k][l].setIcon(ca);
							Button[k][l].setEnabled(true);
							Button[k][l].setBackground(null);
							revalidate();
							validate();
							repaint();
						
						}
					}
					System.out.println("again");
				}
				
			}
		}
	}

}
