package fp.s100502507;

import javax.swing.*;
import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;
import java.lang.Math;
import java.awt.*;
import java.awt.event.*;

public class Framework extends JPanel implements MouseListener, MouseMotionListener {
	Framework() {
		person = new Person[3]; //Some passer-by who can be attacked
		car = new Object();
		for(int i=0; i<3; i++) {
			person[i] = new Person();
		}
		
		personCounter = new int[3];
		
		attack = strongAttack = false;
		exit = false;
		mouseCounter = mouse_x = mouse_y = manAppearCounter = carAppearCounter = personCounter[0] = personCounter[1] = personCounter[2] = manDelay = stage = 0;
		gasCounter = urineCounter = -1; //Set gasCounter to -1(It represents the gas will not be drawn)
		carAppearTime = 200;
		
		ImgBackground = new Image[5]; //Load image of background
		ImgBackground[0] = new ImageIcon("images/background.png").getImage();
		ImgBackground[1] = new ImageIcon("images/background_streetlamp.png").getImage();
		ImgBackground[2] = new ImageIcon("images/background_about.png").getImage();
		ImgBackground[3] = new ImageIcon("images/background_lose.png").getImage();
		ImgBackground[4] = new ImageIcon("images/background_lose2.png").getImage();
		ImgMouse = new Image[6]; //Load images of mouse
		ImgMouse[0] = new ImageIcon("images/mouse01.png").getImage();
		ImgMouse[1] = new ImageIcon("images/mouse02.png").getImage();
		ImgMouse[2] = new ImageIcon("images/mouse03.png").getImage();
		ImgMouse[3] = new ImageIcon("images/mouse04.png").getImage();
		ImgMouse[4] = new ImageIcon("images/mouse05.png").getImage();
		ImgMouse[5] = new ImageIcon("images/mouse06.png").getImage();
		ImgTitle = new Image[4]; //Load images of title
		ImgTitle[0] = new ImageIcon("images/title_default.png").getImage();
		ImgTitle[1] = new ImageIcon("images/title_start.png").getImage();
		ImgTitle[2] = new ImageIcon("images/title_about.png").getImage();
		ImgTitle[3] = new ImageIcon("images/title_exit.png").getImage();
		ImgPlayer = new Image[3]; //Load images of the person who releases gas
		ImgPlayer[0] = new ImageIcon("images/player_default.png").getImage();
		ImgPlayer[1] = new ImageIcon("images/player_attack.png").getImage();
		ImgPlayer[2] = new ImageIcon("images/player_lose.png").getImage();
		ImgGas = new Image[10]; //Load images of gas
		ImgGas[0] = new ImageIcon("images/gas01.png").getImage();
		ImgGas[1] = new ImageIcon("images/gas02.png").getImage();
		ImgGas[2] = new ImageIcon("images/gas03.png").getImage();
		ImgGas[3] = new ImageIcon("images/gas04.png").getImage();
		ImgGas[4] = new ImageIcon("images/gas05.png").getImage();
		ImgGas[5] = new ImageIcon("images/gas06.png").getImage();
		ImgGas[6] = new ImageIcon("images/gas07.png").getImage();
		ImgGas[7] = new ImageIcon("images/gas08.png").getImage();
		ImgGas[8] = new ImageIcon("images/gas09.png").getImage();
		ImgGas[9] = new ImageIcon("images/gas10.png").getImage();
		ImgUrine = new Image[10]; //Load images of urine
		ImgUrine[0] = new ImageIcon("images/urine01.png").getImage();
		ImgUrine[1] = new ImageIcon("images/urine02.png").getImage();
		ImgUrine[2] = new ImageIcon("images/urine03.png").getImage();
		ImgUrine[3] = new ImageIcon("images/urine04.png").getImage();
		ImgUrine[4] = new ImageIcon("images/urine05.png").getImage();
		ImgUrine[5] = new ImageIcon("images/urine06.png").getImage();
		ImgUrine[6] = new ImageIcon("images/urine07.png").getImage();
		ImgUrine[7] = new ImageIcon("images/urine08.png").getImage();
		ImgUrine[8] = new ImageIcon("images/urine09.png").getImage();
		ImgUrine[9] = new ImageIcon("images/urine10.png").getImage();
		ImgHpMp = new Image[2]; //Load the bar of HP and MP
		ImgHpMp[0] = new ImageIcon("images/hpandmp_front.png").getImage();
		ImgHpMp[1] = new ImageIcon("images/hpandmp_behind.png").getImage();
		ImgMan = new Image[4]; //Load images of passer-by
		ImgMan[0] = new ImageIcon("images/man01.png").getImage();
		ImgMan[1] = new ImageIcon("images/man02.png").getImage();
		ImgMan[2] = new ImageIcon("images/man03.png").getImage();
		ImgMan[3] = new ImageIcon("images/man04.png").getImage();
		ImgCar = new Image[2]; //Load images of car
		ImgCar[0] = new ImageIcon("images/car.png").getImage();
		ImgCar[1] = new ImageIcon("images/car2.png").getImage();
		ImgMenu = new Image[2]; //Load images of menu
		ImgMenu[0] = new ImageIcon("images/menu.png").getImage();
		ImgMenu[1] = new ImageIcon("images/menu2.png").getImage();
		
		addMouseListener(this); //These two listener enable this program to handle some actions of mouse
		addMouseMotionListener(this);
		
		sound = new Clip[5];
		audioIn = new AudioInputStream[5];
		
		File soundFile = new File("sounds/car.wav");
		try {
			audioIn[0] = AudioSystem.getAudioInputStream(soundFile);
			 // load the sound into memory (car)
		    DataLine.Info info = new DataLine.Info(Clip.class, audioIn[0].getFormat());
		    sound[0] = (Clip) AudioSystem.getLine(info);
		    sound[0].open(audioIn[0]);
		    
		    soundFile = new File("sounds/fart.wav");
		    audioIn[1] = AudioSystem.getAudioInputStream(soundFile);
			 // load the sound into memory (fart)
		    info = new DataLine.Info(Clip.class, audioIn[1].getFormat());
		    sound[1] = (Clip) AudioSystem.getLine(info);
		    sound[1].open(audioIn[1]);
		    
		    soundFile = new File("sounds/scream.wav");
		    audioIn[2] = AudioSystem.getAudioInputStream(soundFile);
			 // load the sound into memory (scream)
		    info = new DataLine.Info(Clip.class, audioIn[2].getFormat());
		    sound[2] = (Clip) AudioSystem.getLine(info);
		    sound[2].open(audioIn[2]);
		    
		    soundFile = new File("sounds/groan.wav");
		    audioIn[3] = AudioSystem.getAudioInputStream(soundFile);
			 // load the sound into memory (groan)
		    info = new DataLine.Info(Clip.class, audioIn[3].getFormat());
		    sound[3] = (Clip) AudioSystem.getLine(info);
		    sound[3].open(audioIn[3]);
		    
		    soundFile = new File("sounds/urine.wav");
		    audioIn[4] = AudioSystem.getAudioInputStream(soundFile);
			 // load the sound into memory (groan)
		    info = new DataLine.Info(Clip.class, audioIn[4].getFormat());
		    sound[4] = (Clip) AudioSystem.getLine(info);
		    sound[4].open(audioIn[4]);
		} catch (UnsupportedAudioFileException e) {
			// Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}

		timer = new Timer(30, new setTimer()); //Frame per second
		timer.start();
	}
	
	protected void paintComponent(Graphics g) {// Draw everything which needs to be displayed
		super.paintComponent(g);
		
		if(stage == 0) { //When player first executes this game, lead the player to the menu
			if(attack) { //Attack == true, that is, mouse pressed
				if(mouse_x > getWidth()*300/800 && mouse_x < getWidth()*497/800 && mouse_y > getHeight()*230/600 && mouse_y < getHeight()*316/600) { //Pressed "Start"
					player = new Player(); //Create player which controlled by user
					stage = 1;
					sound[1].setFramePosition(0);
					sound[1].start();
				}
				if(mouse_x > getWidth()*297/800 && mouse_x < getWidth()*500/800 && mouse_y > getHeight()*344/600 && mouse_y < getHeight()*407/600) { //Pressed "About"
					stage = 2;
					sound[1].setFramePosition(0);
					sound[1].start();
				}
				if(mouse_x > getWidth()*329/800 && mouse_x < getWidth()*470/800 && mouse_y > getHeight()*448/600 && mouse_y < getHeight()*517/600) { //Pressed "Exit"
					sound[1].setFramePosition(0);
					sound[1].start();
					close();
				}
			}
			else { //Attack == false, that is, mouse doesn't been pressed
				if(mouse_x > getWidth()*300/800 && mouse_x < getWidth()*497/800 && mouse_y > getHeight()*230/600 && mouse_y < getHeight()*316/600) { //Rolled into "Start"
					g.drawImage(ImgTitle[1], 0, 0, getWidth(), getHeight(), this);
				}
				else if(mouse_x > getWidth()*297/800 && mouse_x < getWidth()*500/800 && mouse_y > getHeight()*344/600 && mouse_y < getHeight()*407/600) { //Rolled into "About"
					g.drawImage(ImgTitle[2], 0, 0, getWidth(), getHeight(), this);
				}
				else if(mouse_x > getWidth()*329/800 && mouse_x < getWidth()*470/800 && mouse_y > getHeight()*448/600 && mouse_y < getHeight()*517/600) { //Rolled into "Exit"
					g.drawImage(ImgTitle[3], 0, 0, getWidth(), getHeight(), this);
				}
				else {
					g.drawImage(ImgTitle[0], 0, 0, getWidth(), getHeight(), this); //Doesn't rolled into any choice
				}
			}
		}
		else if(stage == 1) { //When player pressed "Start", isMenu will be changed to false, then jump to the game appearance
			g.drawImage(ImgBackground[0], 0, 0, getWidth(), getHeight(), this); //Draw background

			if((car.getX() >= 800 || car.getX() <= -600) && carAppearCounter >= carAppearTime) { //Create a car
				car = new Object();
				carAppearCounter = 0;
				double hold = Math.random();
				
				if(hold <= 0.3) {
					carAppearTime = 150;
				}
				else if(hold > 0.3 && hold <= 0.6) {
					carAppearTime = 200;
				}
				else {
					carAppearTime = 250;
				}
				
				sound[0].setFramePosition(0);
				sound[0].start();
			}
			
			if(car.isLeft()) {//Draw car
				g.drawImage(ImgCar[0], car.getX(), car.getY(), 600, 400, this);
			}
			else {
				g.drawImage(ImgCar[1], car.getX(), car.getY(), 600, 400, this);
			}
			car.changePosition();
			
			g.drawImage(ImgBackground[1], 0, 0, getWidth(), getHeight(), this); //Draw background
			
			player.minusHp(); //Keep decrease player's Hp
			
			if(player.getHp() == 0) { //Player died
				sound[2].setFramePosition(0);
				sound[2].loop(0);
				stage = 4;
			}
			
			if(attack) { //Left button of mouse pressed
				if(car.getX() <= 800 && car.getX() >= -600) {
					g.drawImage(ImgPlayer[1], getWidth()*50/800, getHeight()*50/800, 200, 400, this); //Player's head draw back
					for(int i=0; i<3; i++) { //Minus the passer-by's HP if player press on him
						if(Math.abs(mouse_x - person[i].getX()) < 200 && Math.abs(mouse_y - person[i].getY()) < 400) {
							person[i].minusHp(0);
						}
						if(!person[i].getAlive()) {
							player.addMp();
							person[i] = new Person();
						}
					}
					player.addHp(0);
					sound[1].setFramePosition(0);
					sound[1].start();
				}
				else {
					sound[3].setFramePosition(0);
					sound[3].loop(0);
					
					g.drawImage(ImgPlayer[2], getWidth()*50/800, getHeight()*50/800, 200, 400, this); //Player releases gas while there's no car
					player.minusHp(1);
					player.minusMp();
					gasCounter = urineCounter = -1;
					stage = 3;
				}
			}
			else if(strongAttack) { //Right button of mouse pressed
				if(car.getX() <= 800 && car.getX() >= -600) {
					if(player.getMp() >= 50) {
						g.drawImage(ImgPlayer[1], getWidth()*50/800, getHeight()*50/800, 200, 400, this); //Player's head draw back
						for(int i=0; i<3; i++) { //Minus the passer-by's HP if player press on him
							if(Math.abs(mouse_x - person[i].getX()) < 200 && Math.abs(mouse_y - person[i].getY()) < 400) {
								person[i].minusHp(1);
								strongAttack = false;
							}
						}
						if(strongAttack == false) {
							player.minusMp();
						}
						player.addHp(1);
						sound[4].setFramePosition(0);
						sound[4].start();
					}
					else {
						g.drawImage(ImgPlayer[0], getWidth()*50/800, getHeight()*50/800, 200, 400, this); //Draw default player
					}
				}
				else {
					g.drawImage(ImgPlayer[2], getWidth()*50/800, getHeight()*50/800, 200, 400, this); //Player releases gas while there's no car
					player.minusHp(1);
					player.minusMp();
					player.minusMp();
					sound[3].setFramePosition(0);
					sound[3].start();
					gasCounter = urineCounter = -1;
					stage = 3;
				}
			}
			else { //Left button of mouse doesn't be pressed
				g.drawImage(ImgPlayer[0], getWidth()*50/800, getHeight()*50/800, 200, 400, this); //Draw default picture of player
			}
			
			if(manAppearCounter >= 300) { //Create a passer-by
				manAppearCounter = 0;
				for(int i=0; i<3; i++) {
					if(!person[i].getGo()) {
						person[i] = new Person();
						person[i].setGo(true);
						if(person[i].isLeft()) {
							personCounter[i] = 2;
						}
						else {
							personCounter[i] = 0;
						}
						break;
					}
				}
			}
			
			for(int i=0; i<3; i++) { //Draw passer-by
				if(person[i].getGo()) {
					g.drawImage(ImgMan[personCounter[i]], person[i].getX(), person[i].getY(), 200, 400, this);
					if(personCounter[i] == 1 && manDelay == 5) {
						personCounter[i] = 0;
					}
					else if(personCounter[i] == 3 && manDelay == 5) {
						personCounter[i] = 2;
					}
					else {
						if(manDelay == 5) {
							personCounter[i] += 1;
							manDelay = 0;
						}
						else {
							manDelay += 1;
						}
					}
					person[i].changePosition();
				}
			}
			
			if(gasCounter > -1 && gasCounter < 10) { //Check if program should draw gas or not
				g.drawImage(ImgGas[gasCounter], mouse_x - 150, mouse_y - 150, 300, 300, this);
				gasCounter += 1; //Next frame of gas
			}
			
			if(gasCounter >= 9) { //A round of frames of gas has completed
				if(attack) { //Player keep pressing left button of mouse
					gasCounter = 0;
				}
				else { //Player doesn't press left button of mouse anymore
					gasCounter = -1;
				}
			}
			

			if(urineCounter > -1 && urineCounter < 10) { //Check if program should draw urine or not
				g.drawImage(ImgUrine[urineCounter], mouse_x - 150, mouse_y - 150, 300, 300, this);
				urineCounter += 1; //Next frame of urine
			}
			
			if(urineCounter >= 8) { //A round of frames of urine has completed
				if(strongAttack) { //Player keep pressing right button of mouse
					urineCounter = 0;
				}
				else { //Player doesn't press right button of mouse anymore
					urineCounter = -1;
				}
			}
			
			g.drawImage(ImgHpMp[1], 10, 400, 400, 150, this); //Draw the bottom of HP&MP bar
			g.setColor(Color.RED);
			g.fillRect(175, 425, player.getHp()*2, 60); //Draw HP bar
			g.setColor(Color.BLUE);
			g.fillRect(173, 490, player.getMp()*2, 35); //Draw MP bar
			g.drawImage(ImgHpMp[0], 10, 400, 400, 150, this); //Draw the top of HP&MP bar
			
			if(mouse_x >= getWidth() - 90 && mouse_x <= getWidth() - 10 && mouse_y >= getHeight() - 40 && mouse_y <= getHeight() - 10) { //Draw menu
				g.drawImage(ImgMenu[1], getWidth() - 90, getHeight() - 40, 80, 30, this);
				if(attack) {
					stage = 0;
					for(int i=0; i<3; i++) { //Refresh persons
						person[i] = new Person();
					}
					car = new Object(); //Refresh car
					gasCounter = urineCounter = -1;
					sound[1].setFramePosition(0);
					sound[1].start();
				}
			}
			else {
				g.drawImage(ImgMenu[0], getWidth() - 90, getHeight() - 40, 80, 30, this);
			}
			
			g.drawImage(ImgMouse[mouseCounter], mouse_x - 100, mouse_y - 100, 201, 201, this); //Draw mouse
			if(mouseCounter < 5) { //Repeat the frame of mouse
				mouseCounter += 1;
			}
			else {
				mouseCounter = 0;
			}
		}
		else if(stage == 2) { //Show informations about this game
			g.drawImage(ImgBackground[2], 0, 0, 800, 600, this);
			
			if(mouse_x >= getWidth() - 90 && mouse_x <= getWidth() - 10 && mouse_y >= getHeight() - 40 && mouse_y <= getHeight() - 10) { //Draw menu
				g.drawImage(ImgMenu[1], getWidth() - 90, getHeight() - 40, 80, 30, this);
				if(attack) {
					stage = 0;
					for(int i=0; i<3; i++) { //Refresh persons
						person[i] = new Person();
					}
					car = new Object(); //Refresh car
					sound[1].setFramePosition(0);
					sound[1].start();
				}
			}
			else {
				g.drawImage(ImgMenu[0], getWidth() - 90, getHeight() - 40, 80, 30, this);
			}
		}
		else if(stage == 3) { //Lost because of the condition: Releases gas while there's no car			
			g.drawImage(ImgBackground[3], 0, 0, 800, 600, this);
			if(mouse_x >= getWidth() - 90 && mouse_x <= getWidth() - 10 && mouse_y >= getHeight() - 40 && mouse_y <= getHeight() - 10) {
				g.drawImage(ImgMenu[1], getWidth() - 90, getHeight() - 40, 80, 30, this);
				if(attack) {
					for(int i=0; i<3; i++) { //Refresh persons
						person[i] = new Person();
					}
					car = new Object(); //Refresh car
					stage = 0;
					sound[1].setFramePosition(0);
					sound[1].start();
				}
			}
			else {
				g.drawImage(ImgMenu[0], getWidth() - 90, getHeight() - 40, 80, 30, this);
			}
		}
		else { //Lost because of the condition: Belly exploded			
			g.drawImage(ImgBackground[4], 0, 0, 800, 600, this);
			if(mouse_x >= getWidth() - 90 && mouse_x <= getWidth() - 10 && mouse_y >= getHeight() - 40 && mouse_y <= getHeight() - 10) {
				g.drawImage(ImgMenu[1], getWidth() - 90, getHeight() - 40, 80, 30, this);
				if(attack) {
					for(int i=0; i<3; i++) { //Refresh persons
						person[i] = new Person();
					}
					car = new Object(); //Refresh car
					stage = 0;
					sound[1].setFramePosition(0);
					sound[1].start();
				}
			}
			else {
				g.drawImage(ImgMenu[0], getWidth() - 90, getHeight() - 40, 80, 30, this);
			}
		}
	}

	public void mouseClicked(MouseEvent e) {
		mouse_x = e.getX();
		mouse_y = e.getY();
	}

	public void mouseEntered(MouseEvent e) {
		mouse_x = e.getX();
		mouse_y = e.getY();
	}

	public void mouseExited(MouseEvent e) {
		mouse_x = -100;
		mouse_y = -100;
	}

	public void mousePressed(MouseEvent e) { //Actions which will be performed while the buttons of mouse are pressed
		if(e.getButton() == MouseEvent.BUTTON1) { //Left button pressed
			if(gasCounter == -1) { //If there's no gas on the screen, create the gas, else, let the current gas keep performing
				gasCounter = 0;
			}
			attack = true;
			strongAttack = false;
		}
		else if(e.getButton() == MouseEvent.BUTTON3) { //Right button pressed
			if(urineCounter == -1 && player.getMp() >= 50) { //If there's no gas on the screen, create the gas, else, let the current gas keep performing
				urineCounter = 0;
			}
			strongAttack = true;
			attack = false;
		}
		mouse_x = e.getX();
		mouse_y = e.getY();
	}

	public void mouseReleased(MouseEvent e) { //Once buttons are released, set all attack boolean to false
		if(e.getButton() == MouseEvent.BUTTON1) {
			attack = false;
		}
		else if(e.getButton() == MouseEvent.BUTTON3) {
			strongAttack = false;
		}
		mouse_x = e.getX();
		mouse_y = e.getY();
	}
	
	public void mouseMoved(MouseEvent e) {
		mouse_x = e.getX();
		mouse_y = e.getY();
	}
	

	public void mouseDragged(MouseEvent e) {
		mouse_x = e.getX();
		mouse_y = e.getY();
	}
	
	public boolean getExit() { //A boolean to return program should close or not
		return exit;
	}
	
	private void close() { //Close this program
		SwingUtilities.getWindowAncestor(this).dispose();
		System.exit(0);
	}
	
	private class setTimer implements ActionListener { //Set the frame per second
		public void actionPerformed(ActionEvent e) {
			manAppearCounter = manAppearCounter + (int)(Math.random()*20); //Counting the time to create passer-by//Counting the time to create car
			carAppearCounter += 1;
			
			repaint();
		}
	}
	
	private int[] personCounter;
	private int mouseCounter, mouse_x, mouse_y, gasCounter, urineCounter, manAppearCounter, carAppearCounter, carAppearTime, manDelay, stage;
	private Timer timer;
	private boolean attack, strongAttack, exit;
	private Image[] ImgBackground, ImgMouse, ImgTitle, ImgPlayer, ImgGas, ImgUrine, ImgHpMp, ImgMan, ImgCar, ImgMenu;
	private Player player;
	private Person[] person;
	private Object car;
	private Clip[] sound;
	private AudioInputStream[] audioIn;
}
