package fp.s100502507;

public class Person extends Object{
	Person() {
		super(0);
		hp = 100;
		isAlive = true;
		go = false;
	}
	
	public void minusHp(int input) { //Decrease person's HP
		if(input == 0) {
			if(hp - 2 >= 0) {
				hp -= 3;
			}
			else {
				hp = 0;
				isAlive = false;
			}
		}
		else {
			hp = 0;
			isAlive = false;
		}
	}
	
	public void changePosition() { //Move player
		if(x < -200 || x > 800) {
			go = false;
			isAlive = true;
			hp = 100;
		}
		else {
			if(hp == 0) {
				isAlive = false;
				go = false;
			}
		}
		
		if(isLeft()) {
			x += 10;
		}
		else {
			x -= 10;
		}
	}
	
	public void setGo(boolean b) { //Decide person should stay or move
		if(b) {
			go = true;
		}
		else {
			go = false;
		}
	}
	
	public boolean getAlive() { //Return true if person is alive
		return isAlive;
	}
	
	public boolean getGo() { //Return true if person should keep moving
		return go;
	}
	
	public int getHp() { //Return person's HP
		return hp;
	}
	
	protected int hp; //Player's HP
	private boolean isAlive, go;
}