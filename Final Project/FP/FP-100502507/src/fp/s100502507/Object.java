package fp.s100502507;

import java.lang.Math;

public class Object {
	Object() { //Constructor decide this object is created at left or right
		if((int)(Math.random()*3) == 1) {
			x = -600;
			Left = true;
		}
		else {
			x = 800;
			Left = false;
		}
		y = (int)(Math.random()*10);
	}
	
	Object(int a) {
		if((int)(Math.random()*3) == 1) {
			x = -200;
			Left = true;
		}
		else {
			x = 800;
			Left = false;
		}
		y = (int)(Math.random()*201);
	}
	
	public int getX() { //Return the X-axis position
		return x;
	}

	public int getY() { //Return the Y-axis position
		return y;
	}
	
	public void changePosition() { //Change position
		if(isLeft()) {
			x += 10;
		}
		else {
			x -= 10;
		}
	}

	public boolean isLeft() { //Return true if this object is created at the left
		return Left;
	}
	
	private boolean Left;
	protected int x, y;
}
