package fp.s100502507;

public class Player extends Person {
	Player() {
		mp = minus = 0;
	}
	
	public void addMp() { //Increase player's MP
		if(mp + 5 <= 100) {
			mp += 10;
		}
		else {
			mp = 100;
		}
	}
	
	public void addHp(int input) { //Increase player's HP(A lot or a little)
		if(input == 0) {
			if(hp + 1 <= 100) {
				hp += 1;
			}
			else {
				hp = 100;
			}
		}
		else {
			if(hp + 35 <= 100) {
				hp += 40;
			}
			else {
				hp = 100;
			}
		}
	}
	
	public void minusHp() { //Decrease player's HP
		if(minus == 1) {
			if(hp - 1 >= 0) {
				hp -= 1;
			}
			else {
				hp = 0;
			}
			minus = 0;
		}
		else {
			minus += 1;
		}
	}
	
	public void minusMp() { //Decrease player's MP
		if(mp - 50 >= 0) {
			mp -= 50;
		}
		else {
			mp = 0;
		}
	}
	
	public int getMp() { //Return player's MP
		return mp;
	}
	
	public boolean isAlive() { //Return if the player is alive or not
		if(getHp() == 0) {
			return false;
		}
		else {
			return true;
		}
	}
	
	private int minus; //Decide if player's HP should be decreased
	private int mp; //Urine
}
