package fp.s992008002;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;


public class ShrewButton extends JButton implements Runnable,ActionListener {
	

long startTime, endTime;
ImageIcon ic;
boolean flag = false;
int score;
int n;
int level;
int index;
//static int shrewStore[] = new int [6];
HitShrew controller;
ImageIcon goldbutton = new ImageIcon("goldbutton.png");
ImageIcon gold = new ImageIcon("gold.jpg");
ImageIcon cool = new ImageIcon("cool.jpg");

//按鈕設定
public ShrewButton(ImageIcon ic, HitShrew controller) {
	this.ic = ic;
	this.controller = controller;
	this.setIcon(cool);
	addActionListener(this);
}
//記錄場地大小
public void setNumber(int n) {
	this.n = n;
}
//記錄選擇等級
public void setLevel(int Level) {
	this.level = Level;
}
public void setIndex(int index){
	this.index = index;
}
//記錄遊戲開始時間
public void setStartTime(long second) {
	startTime = second;
}
//記錄遊戲結束時間
public void setGameLength(long second) {
	endTime=startTime+second;
}

//執行
public void run() {
	//系統時間<結束時間
while ( System.currentTimeMillis()/1000 < endTime) {
	int flag = (int) (Math.random()*n*n);
	setBackground(Color.LIGHT_GRAY);
	if (flag == 1) {
		setIcon(ic);
		setFlag(true);
	}
	
else {
	setIcon(cool);
	setText("");
	setFlag(false);
}
	//設定時間延遲
try {	
	if(this.level == 1)Thread.sleep(950);  //簡單
	else if(this.level == 2)Thread.sleep(750);//中級
	else if(this.level == 3)Thread.sleep(400);//地獄
	else Thread.sleep(750); //輸入不正確直接改為中級
}

catch(InterruptedException e) {
	System.out.println("Unexpected exception!");
}
	setIcon(gold);
	setText("");
	setFlag(false);
}
	setIcon(null);
	setBackground(Color.ORANGE);
	setText(score + " hit!");
	
	setOpaque(true);
}
public void setFlag(boolean flag) {
	this.flag = flag;
}
public boolean getFlag() {
	return flag;
}


public void actionPerformed(ActionEvent e) {
	if ( this.flag == true) {
		setFlag(false);
		setIcon(goldbutton);
		//最後3/10時間內  分數加倍
		if(System.currentTimeMillis()/1000 > endTime - (endTime -startTime)*0.3){
			score+=2;
			controller.setScore(2);
		}
		else{
		score++;
		controller.setScore(1);
		}
	}
  }
}
	
