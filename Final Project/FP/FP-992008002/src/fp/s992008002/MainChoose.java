package fp.s992008002;


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;


import java.io.*;
import sun.audio.*;

public class MainChoose extends JFrame
{	
 private JButton button1 = new JButton("PUSH ");
 private JButton button2 = new JButton("POP");
 private ImageButton button3;
 private ImageButton button4;
 private ImageButton button5;
 private JRadioButton purpleButton = new JRadioButton("PURPLE",true);
 private JRadioButton blueButton = new JRadioButton("HOTEL ",false);
 private JRadioButton car1Button = new JRadioButton("YELLOW",false);;
 private JRadioButton grayButton = new JRadioButton("CAR  1",false);;
 private JRadioButton car2Button = new JRadioButton("CAR  2",false);;
 private ButtonGroup radioGroup = new ButtonGroup();
 private ImageIcon icon = new ImageIcon("gay.jpg");
 private JLabel label = new JLabel(icon);
 private ImageIcon NOW; //未來會用到的寵物欄
 private ImageIcon pet = new ImageIcon("csie.jpg");
 private JLabel Plabel = new JLabel(pet);
 
 //中英文字體  先預備著
 private Font font1 = new Font("SansSerif", Font.BOLD, 20);  
 private Font font2 = new Font("華康隸書體W7", Font.BOLD, 63); 

 String[] field={"<html><font size=6 color=red>Memorandum</font></html>"};//設定表格欄位
 Object[][] data={{"睡覺"},{"要睡覺"},{"一定要記得睡覺"},{"補充泡麵"}};//設定預設資料
 DefaultTableModel tmodel = new DefaultTableModel(data,field); //建立表格
 JTable table1 = new JTable(tmodel);	//建立JTable
 	 
 
 public MainChoose()
 {
	  //初使背景
	 getLayeredPane().add(label, new Integer(Integer.MIN_VALUE));
	 label.setBounds(0,0,icon.getIconWidth(), icon.getIconHeight());
	  Container cp=getContentPane();
	  ((JPanel)cp).setOpaque(false);  
	 
	ImageIcon purple = new ImageIcon("gay.jpg");
	ImageIcon blue = new ImageIcon("hotel.jpg");
	ImageIcon car1 = new ImageIcon("yellow.jpg");
	ImageIcon gray = new ImageIcon("car1.jpg");
	ImageIcon car2 = new ImageIcon("car2.jpg");
	
	ImageIcon normalstart = new ImageIcon("normalstart.jpg");
	ImageIcon notes = new ImageIcon("notes.jpg");
	ImageIcon exit = new ImageIcon("exit.jpg");
	
    button4 = new ImageButton(normalstart);
	button3 = new ImageButton(notes);
	button5 = new ImageButton(exit);
	
  getContentPane().setLayout(new FlowLayout());

  button1.addActionListener(new ActionLis(1));
  button2.addActionListener(new ActionLis(2));
  button3.addActionListener(new ActionLis(3));
  button4.addActionListener(new ActionLis(4));
  button5.addActionListener(new ActionLis(5));
  //背景按鈕
  add(purpleButton);
  add(blueButton);
  add(car1Button);
  add(grayButton);
  add(car2Button);  
 
  radioGroup.add(purpleButton);
  radioGroup.add(blueButton);
  radioGroup.add(car1Button);
  radioGroup.add(grayButton);
  radioGroup.add(car2Button);
  getContentPane().add(new JScrollPane(table1));
	
  //將按鈕放到視窗介面中
  getContentPane().add(button1);
  getContentPane().add(button2);
  //getContentPane().add(Plabel);  未來的寵物欄
  getContentPane().add(button3);

  getContentPane().add(button4);
  getContentPane().add(button5);
  button5.setFont(font2);
  purpleButton.addItemListener(new RadioButtonHandler(purple));
  blueButton.addItemListener(new RadioButtonHandler(blue));
  car1Button.addItemListener(new RadioButtonHandler(car1));
  grayButton.addItemListener(new RadioButtonHandler(gray));
  car2Button.addItemListener(new RadioButtonHandler(car2));
  

  
 }
 //未來會用到的
 public void setPet(ImageIcon icon){
	  Plabel.setIcon(icon);
 }
 
 class ActionLis implements ActionListener//處理按鈕
 {
  int select;
  public ActionLis(int select)
  {
   this.select=select;
  }
  public void actionPerformed(ActionEvent e)
  {
   if(select==1)//"push"鈕
   {
	   String thing = JOptionPane.showInputDialog("請輸入代辦事項: ");
	   
	    Object newdata[] = {thing};
	    tmodel.addRow(newdata);
	   }
	   else if(select==2)//"pop"鈕
	   {
	    int delete = table1.getSelectedRow();
	    if(delete==-1)//當未選取任何資料時,輸出錯誤訊息
	    {
	     JOptionPane.showMessageDialog(null,"您尚未選取欲刪除的資料。","錯誤",JOptionPane.ERROR_MESSAGE);
	    }
	    else//有選取資料時,執行確認刪除命令
	    {
	     String msg_sure="確定刪除已選取的此列 ? ";
	     Object[] delbutton = {"確認刪除","尚未確定"};
	     JOptionPane del = new JOptionPane(msg_sure,JOptionPane.QUESTION_MESSAGE,JOptionPane.DEFAULT_OPTION,null,delbutton);
	     JDialog delD = del.createDialog(del,"詢問");
	     delD.setVisible(true);
	     Object push = del.getValue();
	     if(push==delbutton[0])tmodel.removeRow(delete);
	    }
	   }
   else if(select==3)//"通訊錄"鈕
   {
	   Memorandum Taroka = new Memorandum();
	 
	   
	   Taroka.setTitle("通訊錄");
	   Taroka.setSize(500,510);
	   Taroka.setVisible(true);
	   
	   //設定通訊錄背景
	   ImageIcon img = new ImageIcon("gold.jpg");
	   JLabel imgLabel = new JLabel(img);
	   Taroka.getLayeredPane().add(imgLabel, new Integer(Integer.MIN_VALUE));
	   imgLabel.setBounds(0,0,img.getIconWidth(), img.getIconHeight());//设置背景标签的位置
	   Container cp=Taroka.getContentPane();
	   cp.setLayout(new BorderLayout());
	   ((JPanel)cp).setOpaque(false); 
	   
	   
   }
   //"HitShrew"鈕
   else if(select==4){
	 	  
	  new HitShrew();
	 //在這理會無法連上 ShrewButton Thread  還在想辦法解決
	 //setPet(NOW);
   }
   //"離開"鈕
   else if(select==5)System.exit(1);
  }
 }
 
 //變更背景控制
private class RadioButtonHandler implements ItemListener
 {
	 private ImageIcon background;
	 public RadioButtonHandler(ImageIcon icon){
		 background = icon; 
	 } 
	 public void itemStateChanged(ItemEvent event){
			 if(event.getStateChange() == ItemEvent.SELECTED){
			 label.setIcon(background);
		 }
     }
 }
 

 
}