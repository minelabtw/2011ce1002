package fp.s992008002;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import java.io.*;

public class Memorandum extends JFrame
{
/* 未完成的IO
private ObjectInputStream input;

public void openFile(){
	try{
		input = new ObjectInputStream(new FileInputStream("people.txt"));
	}
	catch(IOException ioException){
		System.err.println("Error opening file");
	}
}
	
public void readRecords(){
	AccountRecord record;
	try{
		while(true){
			record =(AccountRecord)input.readObject();
			Object[][] data = 
		}
	}
}*/
 //設定表格欄位
 String[] field={"姓名","生日","手機","二進位ID","MSN      "};
 //設定預設資料
 Object[][] data={
		 {"阿卡","1985/12/01","0920581935","ICNEW","kiy535353@hotmail.com"},
		 {"阿卡","1985/12/01","0920581935","ICNEW","kiy535353@hotmail.com"},
		 {"阿卡","1985/12/01","0920581935","ICNEW","kiy535353@hotmail.com"},
		 {"阿卡","1985/12/01","0920581935","ICNEW","kiy535353@hotmail.com"},
		 {"大居","____/__/__","問____","ICNBREAK","bigGGGGGGG@hotmail.com"}
 };
 //建立表格
 DefaultTableModel List = new DefaultTableModel(data,field);
 //建立JTable
 JTable table1 = new JTable(List);
 //新增按鈕
 JButton button1 = new JButton("NEW   ");
 JButton button2 = new JButton("DELETE");
 JButton button3 = new JButton("SEARCH");
 JButton button4 = new JButton("DELETE ALL");
 JButton button5 = new JButton("EXIT  ");

 public Memorandum()
 {
  getContentPane().setLayout(new FlowLayout());
  //將table1加到JscrollPane
  getContentPane().add(new JScrollPane(table1));
  //設定各按鈕的傾聽者
  button1.addActionListener(new ActionLisner(1));
  button2.addActionListener(new ActionLisner(2));
  button3.addActionListener(new ActionLisner(3));
  button4.addActionListener(new ActionLisner(4));
  button5.addActionListener(new ActionLisner(5));
  //將按鈕放到視窗介面中
  getContentPane().add(button1);
  getContentPane().add(button2);
  getContentPane().add(button3);
  getContentPane().add(button4);
  getContentPane().add(button5);
 }
 class ActionLisner implements ActionListener//處理按鈕
 {
  int select;
  public ActionLisner(int select)
  {
   this.select=select;
  }
  public void actionPerformed(ActionEvent e)
  {
   if(select==1)//"新增"鈕
   {
    String name = JOptionPane.showInputDialog("請輸入姓名 : ");
    String birth = JOptionPane.showInputDialog("請輸入生日 : ");
    String cell = JOptionPane.showInputDialog("請輸入手機號碼 : ");
    String binary = JOptionPane.showInputDialog("請輸入二進位ID : ");
    String msg = JOptionPane.showInputDialog("請輸入MSN : ");
    Object newdata[] = {name,birth,cell,binary,msg};
    List.addRow(newdata);
   }
   else if(select==2)//"刪除"鈕
   {
    int delete = table1.getSelectedRow();
    if(delete==-1)//當未選取任何資料時,輸出錯誤訊息
    {
     JOptionPane.showMessageDialog(null,"您尚未選取欲刪除的資料。","錯誤",JOptionPane.ERROR_MESSAGE);
    }
    else//有選取資料時,執行確認刪除命令
    {
     String msg_sure="確定刪除已選取的此列 ? ";
     Object[] delbutton = {"確認刪除","尚未確定"};
     JOptionPane del = new JOptionPane(msg_sure,JOptionPane.QUESTION_MESSAGE,JOptionPane.DEFAULT_OPTION,null,delbutton);
     JDialog delD = del.createDialog(del,"詢問");
     delD.setVisible(true);
     Object push = del.getValue();
     if(push==delbutton[0])List.removeRow(delete);
    }
   }
   else if(select==3)//"查詢"鈕
   {
    int i;
    String temp;
    Object[] selectItem = {"姓名稱謂","手機號碼","二進位ID","MSN"};
    Object selectedValue = JOptionPane.showInputDialog(null,"請選擇要用哪個項目進行查詢 ? ","項目選擇",JOptionPane.QUESTION_MESSAGE,null,selectItem,selectItem[0]);
    String select = (String)selectedValue;
    if(select.equals("姓名稱謂")==true)
    {
     temp = JOptionPane.showInputDialog("請輸入欲查詢之姓名 : ");
     for(i=0;i<List.getRowCount();i++)
     {
      if(temp.equals(List.getValueAt(i,0))==true)
      {
       JOptionPane.showMessageDialog(null,"您查詢的姓名 \" "+temp+" \" 在第 "+(i+1)+" 列。","訊息",JOptionPane.INFORMATION_MESSAGE);
       break;
      }
      else if(i==List.getRowCount()-1)
      {
       JOptionPane.showMessageDialog(null,"查無此人！","錯誤",JOptionPane.ERROR_MESSAGE);
      }
     }
    }
    else if(select.equals("手機號碼"))
    {
     temp = JOptionPane.showInputDialog("請輸入欲查詢之手機號碼 : ");
     for(i=0;i<List.getRowCount();i++)
     {
      if(temp.equals(List.getValueAt(i,2))==true)
      {
       JOptionPane.showMessageDialog(null,"您查詢的手機號碼 \" "+temp+" \" 在第 "+(i+1)+" 列。","訊息",JOptionPane.INFORMATION_MESSAGE);
       break;
      }
      else if(i==List.getRowCount()-1)
      {
       JOptionPane.showMessageDialog(null,"查無此號碼！","錯誤",JOptionPane.ERROR_MESSAGE);
      }
     }
    }
    else if(select.equals("二進位ID")==true)
    {
     temp = JOptionPane.showInputDialog("請輸入欲查詢之二進位ID : ");
     for(i=0;i<List.getRowCount();i++)
     {
      if(temp.equals(List.getValueAt(i,3))==true)
      {
       JOptionPane.showMessageDialog(null,"您查詢的二進位ID \" "+temp+" \" 在第 "+(i+1)+" 列。","訊息",JOptionPane.INFORMATION_MESSAGE);
       break;
      }
      else if(i==List.getRowCount()-1)
      {
       JOptionPane.showMessageDialog(null,"查無此號碼！","錯誤",JOptionPane.ERROR_MESSAGE);
      }
     }
    }
    else if(select.equals("MSN")==true)
    {
     temp = JOptionPane.showInputDialog("請輸入欲查詢之MSN : ");
     for(i=0;i<List.getRowCount();i++)
     {
      if(temp.equals(List.getValueAt(i,4))==true)
      {
       JOptionPane.showMessageDialog(null,"您查詢的MSN \" "+temp+" \" 在第 "+(i+1)+" 列。","訊息",JOptionPane.INFORMATION_MESSAGE);
       break;
      }
      else if(i==List.getRowCount()-1)
      {
       JOptionPane.showMessageDialog(null,"查無此資料！","錯誤",JOptionPane.ERROR_MESSAGE);
      }
     }
    }
   }
   else if(select==4){
	   String msg_sure="確定刪除所有項目 ? ";
	     Object[] delbutton = {"確認刪除","尚未確定"};
	     JOptionPane del = new JOptionPane(msg_sure,JOptionPane.QUESTION_MESSAGE,JOptionPane.DEFAULT_OPTION,null,delbutton);
	     JDialog delD = del.createDialog(del,"詢問");
	     delD.setVisible(true);
	     Object push2 = del.getValue();
	     if(push2==delbutton[0])table1.removeAll();
   }
   //"離開"鈕
   else if(select==5)System.exit(1);
  }
 }
 
 
}