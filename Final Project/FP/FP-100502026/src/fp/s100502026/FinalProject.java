package fp.s100502026;

import java.awt.*;
import java.util.Scanner;
import javax.swing.*;

public class FinalProject {
	
	/*
	 * This project is :
	 *		1.Main Goal:
	 *			Detect word from user's input.
	 *		2.Additional function:
	 *			Develop to a simple calculator.
	 *
	 * The bugs till present:
	 * 		1.Connect SelectFrame and Calculator and Detective
	 * 
	 */
	
	public static void main(String[] args)
	{
		Detective frame =new Detective();
		
		frame.setSize( 500 , 500 );
		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		frame.setLocationRelativeTo( null );
		frame.setTitle( "Detective some words" );
		frame.setVisible( true );
	}
}
