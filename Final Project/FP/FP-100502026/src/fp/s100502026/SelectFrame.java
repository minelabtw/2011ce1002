package fp.s100502026;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class SelectFrame extends JFrame{

	private boolean[] checkNumber = new boolean[ 10 ];
	private JButton[] jbtNumber = new JButton[ 10 ];
	private int ward = 0;
	private ButtonListener listener = new ButtonListener();

	SelectFrame()
	{
		this.setSize( 500 , 500 );
		this.setLayout( new FlowLayout() );
		this.setLocationRelativeTo( null );
		this.setTitle( "Select your ward" );
		this.setVisible( false );
	}

	public void setCheckNumber( boolean[] checkNumber )
	{
		this.checkNumber = checkNumber;
	}

	public int getward()
	{
		return ward;
	}

	public void addNumberButton( int i )
	{	
		this.remove( this );
		for( int j = 0 ; j < i ; j ++ )
		{
			if( checkNumber[ j ] )
			{
				jbtNumber[ j ] = new JButton();
				jbtNumber[ j ].setText( String.valueOf( j ) );
				this.add( jbtNumber[ j ] );
				jbtNumber[ j ].addActionListener( listener );
			}
		}
	}
	
	class ButtonListener implements ActionListener
	{
		public void actionPerformed( ActionEvent e )
		{
			if( e.getSource() == jbtNumber[ 0 ] )
			{
				ward = 0;
			}
			else if( e.getSource() == jbtNumber[ 1 ] )
			{
				ward = 1;
			}
			else if( e.getSource() == jbtNumber[ 2 ] )
			{
				ward = 2;
			}
			else if( e.getSource() == jbtNumber[ 3 ] )
			{
				ward = 3;
			}
			else if( e.getSource() == jbtNumber[ 4 ] )
			{
				ward = 4;
			}
			else if( e.getSource() == jbtNumber[ 5 ] )
			{
				ward = 5;
			}
			else if( e.getSource() == jbtNumber[ 6 ] )
			{
				ward = 6;
			}
			else if( e.getSource() == jbtNumber[ 7 ] )
			{
				ward = 7;
			}
			else if( e.getSource() == jbtNumber[ 8 ] )
			{
				ward = 8;
			}
			else if( e.getSource() == jbtNumber[ 9 ] )
			{
				ward = 9;
			}
		}
	}
}
