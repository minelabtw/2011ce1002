package fp.s100502026;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Pen extends JPanel{
	
	private int[] x = new int[ 10000 ];
	private int[] y = new int[ 10000 ];
	private int xTemp = 0;
	private int yTemp = 0;
	private int counter = 0;
	private Color color;
	private Point point;

	Pen()
	{
		color = Color.BLACK;
		
		this.setSize( 300 , 500 );
		
		this.addMouseMotionListener
		(
			new MouseMotionAdapter()
			{
				public void mouseDragged( MouseEvent e )
				{
					if( ( e.getX() != xTemp ) || ( e.getY() != yTemp ) )
					{
						counter += 1;
						x[ counter ] = xTemp = e.getX();
						y[ counter ] = yTemp = e.getY();

						System.out.println( "Test" + "\t" + counter + "\t" + x[ counter ] + "\t" + y[ counter] );

						repaint();
					}
				}
			}
		);
	}	
	
	public void paintComponent( Graphics g )
	{
		super.paintComponent( g );
		
		for( int i = 1 ; i <= counter ; i ++ )	
		{
			g.drawRect( x[ i ] , y[ i ] , 1 , 1 );
		}
	}
	
	public void setCounter( int i )
	{
		counter = i;
	}
	
	public int getCounter()
	{
		return counter;
	}
	
	public int getX( int counter )
	{
		return x[ counter ];
	}
	
	public int getY( int counter )
	{
		return y[ counter ];
	}
	
	public int[] getXArray()
	{
		return x;
	}
	
	public int[] getYArray()
	{
		return y;
	}
	
	public void setXArray( int[] array )
	{
		this.x = array;
	}
	
	public void setYArray( int[] array )
	{
		this.y = array;
	}
	
	public void setX( int counter , int number )
	{
		x[ counter ] = number;
	}
	
	public void setY( int counter , int number )
	{
		y[ counter ] = number;
	}
	
	public void clearPoints( int[] array )
	{
		for( int i = 0 ; i < array.length ; i ++ )
		{
			array[ i ] = -1;
		}
	}
}
