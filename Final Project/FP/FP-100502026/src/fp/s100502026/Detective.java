package fp.s100502026;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Detective extends JFrame{
	
	private JPanel detectPanel = new JPanel();
	private JPanel controlPanel = new JPanel();
	private Pen pen = new Pen();
	private JButton jbtJudge = new JButton( "Judge" );
	private boolean[] checkNumber = new boolean[ 10 ];
	private SelectFrame selectFrame = new SelectFrame();
	private JTextField pass = new JTextField();
	private Calculator calculator = new Calculator();
	
	Detective()
	{
		jbtJudge.addActionListener
		(
			new ActionListener()
			{
				public void actionPerformed( ActionEvent e )
				{
					pen.setXArray( transform( pen.getXArray() , pen.getXArray().length , 1 ) );
					pen.setYArray( transform( pen.getYArray() , pen.getYArray().length , 2 ) );
					
					check0( pen.getXArray() , pen.getYArray() );
					check1( pen.getXArray() , pen.getYArray() );
					check2( pen.getXArray() , pen.getYArray() );
					check3( pen.getXArray() , pen.getYArray() );
					check4( pen.getXArray() , pen.getYArray() );
					check5( pen.getXArray() , pen.getYArray() );
					check6( pen.getXArray() , pen.getYArray() );
					check7( pen.getXArray() , pen.getYArray() );
					check8( pen.getXArray() , pen.getYArray() );
					check9( pen.getXArray() , pen.getYArray() );
					
					selectFrame.setCheckNumber( checkNumber );
					selectFrame.addNumberButton( checkNumber.length );
					selectFrame.setVisible( true );
					
					pen.clearPoints( pen.getXArray() );
					pen.clearPoints( pen.getYArray() );
					System.out.println( findMax( pen.getXArray() , 500 ) );
				}
			}
		);

		pass.setVisible( false );
		
		detectPanel.setSize( 300 , 500 );
		detectPanel.setVisible( false );
		
		controlPanel.setSize( 200 , 500 );
		controlPanel.add( jbtJudge );
		controlPanel.add( pass );
		
		this.add( pen , BorderLayout.CENTER );
		this.add( controlPanel , BorderLayout.EAST );
	}
	
	//Check the points are inside the border or not
	public void check0( int[] x , int[] y )
	{
		int pointCounter = 0;
		
		for( int i = 0 ; i < pen.getCounter() ; i ++ )
		{
			if( ( ( ( x[ i ] - 150 ) / 150 ) * ( ( x[ i ] - 150 ) / 150 ) + ( (y[ i ] - 250 ) / 250 ) * ( ( y[ i ] - 250 ) / 250 ) <= 1) &&
				( ( ( x[ i ] - 150 ) / 50 ) * ( ( x[ i ] - 150 ) / 50 ) + ( ( y[ i ] - 250 ) / 150 ) * ( ( y[ i ] - 250 ) / 150 ) <=1 ) )
			{
				pointCounter ++;
			}
		}
		
		if( pointsInside( pen.getCounter() , pointCounter ) )
		{
			checkNumber[ 0 ] = true;
		}
		else
		{
			checkNumber[ 0 ] = false;
		}
	}
	
	
	public void check1( int[] x , int[] y )
	{
		int pointCounter = 0;
		
		for( int i = 0 ; i < pen.getCounter() ; i ++)
		{
			if( ( x[ i ] > 100 ) && ( x[ i ] < 200 ) )
			{
				pointCounter ++;
			}
		}
		
		if( pointsInside( pen.getCounter() , pointCounter ) )
		{
			checkNumber[ 1 ] = true;
		}
		else
		{
			checkNumber[ 1 ] = false;
		}
	}
	
	
	public void check2( int[] x , int[] y )
	{
		int pointCounter = 0;
		
		for( int i = 0 ; i < pen.getCounter() ; i ++)
		{
			if
			( 
				( ( ( ( x[ i ] - 150 ) / 150 ) * ( ( x[ i ] - 150 ) / 150 ) + ( (y[ i ] - 250 ) / 250 ) * ( ( y[ i ] - 250 ) / 250 ) <= 1) && ( ( ( x[ i ] - 150 ) / 80 ) * ( ( x[ i ] - 150 ) / 80 ) + ( ( y[ i ] - 250 ) / 150 ) * ( ( y[ i ] - 250 ) / 150 ) <=1) && ( y[ i ] <= 130 ) ) ||	
				( ( 185 * x[ i ] + 99 * y[ i ] >= 49500 ) && ( 185 * x[ i ] + 99 * y[ i ] <= 68370 ) && ( y[ i ] >= 130 ) ) ||
				( ( 185 * x[ i ] + 99 * y[ i ] <= 68370 ) && ( y[ i ] >= 430 ) )
			)
			{
				pointCounter ++;
			}
		}
		
		if( pointsInside( pen.getCounter() , pointCounter ) )
		{
			checkNumber[ 2 ] = true;
		}
		else
		{
			checkNumber[ 2 ] = false;
		}
	}
	
	
	public void check3( int[] x , int[] y )
	{
		int pointCounter = 0;
		
		for( int i = 0 ; i < pen.getCounter() ; i ++)
		{
			if
			(
				( ( ( x[ i ] - 150 ) * ( x[ i ] - 150 ) + ( y[ i ] - 130 ) * ( y[ i ] - 130 ) <= 22500 ) && ( ( x[ i ] - 150 ) * ( x[ i ] - 150 ) + ( y[ i ] - 130 ) * ( y[ i ] - 130 ) >= 3600 ) && ( y[ i ] <= 140 ) ) ||
				( ( ( x[ i ] - 150 ) * ( x[ i ] - 150 ) + ( y[ i ] - 130 ) * ( y[ i ] - 130 ) <= 22500 ) && ( y[ i ] >= 140 ) && ( x[ i ] >= 90 ) && ( ( ( x[ i ] - 50 ) / ( 100+ Math.pow( 3500 , 0.5 ) ) ) * ( ( x[ i ] - 50 ) / ( 100+ Math.pow( 3500 , 0.5 ) ) ) + ( ( y[ i ] - 140 ) / ( 120 ) ) * ( ( y[ i ] - 140 ) / ( 120 ) ) >= 1 ) ) ||
				( ( ( x[ i ] - 150 ) * ( x[ i ] - 150 ) + ( y[ i ] - 130 ) * ( y[ i ] - 130 ) <= 22500 ) && ( y[ i ] <= 360 ) && ( x[ i ] >= 90 ) && ( ( ( x[ i ] - 50 ) / ( 100+ Math.pow( 3500 , 0.5 ) ) ) * ( ( x[ i ] - 50 ) / ( 100+ Math.pow( 3500 , 0.5 ) ) ) + ( ( y[ i ] - 360 ) / ( 120 ) ) * ( ( y[ i ] - 360 ) / ( 120 ) ) >= 1 ) ) ||
				( ( ( x[ i ] - 150 ) * ( x[ i ] - 150 ) + ( y[ i ] - 370 ) * ( y[ i ] - 370 ) <= 22500 ) && ( ( x[ i ] - 150 ) * ( x[ i ] - 150 ) + ( y[ i ] - 370 ) * ( y[ i ] - 370 ) >= 3600 ) && ( y[ i ] >= 360 ) ) 
			 )
			{
				pointCounter ++;
			}
		}
		
		if( pointsInside( pen.getCounter() , pointCounter ) )
		{
			checkNumber[ 3 ] = true;
		}
		else
		{
			checkNumber[ 3 ] = false;
		}
	}
	
	
	public void check4( int[] x , int[] y )
	{
		int pointCounter = 0;
		
		for( int i = 0 ; i < pen.getCounter() ; i ++)
		{
			if
			(
				( ( x[ i ] * 17 + y[ i ] * 10 >= 3400 ) && ( x[ i ] * 17 + y[ i ] * 10 <= 4720 ) && ( x[ i ] <= 200 ) && ( y[ i ] <= 340 ) ) ||
				( ( x[ i ] * 17 + y[ i ] * 10 >= 4720 ) && ( x[ i ] >= 160 ) && ( x[ i ] <= 200 ) ) ||
				( ( x[ i ] * 17 + y[ i ] * 10 >= 4720 ) && ( y[ i ] >= 275 ) && ( y[ i ] <=340 ) )
			)
			{
				pointCounter ++;
			}
		}
		
		if( pointsInside( pen.getCounter() , pointCounter ) )
		{
			checkNumber[ 4 ] = true;
		}
		else
		{
			checkNumber[ 4 ] = false;
		}
	}
	
	
	public void check5( int[] x , int[] y )
	{
		int pointCounter = 0;
		
		for( int i = 0 ; i < pen.getCounter() ; i ++)
		{
			if
			(
				( ( x[ i ] <= 280 ) && ( x[ i ] >= 100 ) && ( y[ i ] <= 80 ) ) ||
				( ( ( ( x[ i ] - 80 ) / 90 ) * ( ( x[ i ] - 80 ) / 90 ) + ( ( y[ i ] - 340 ) / 150 ) * ( ( y[ i ] - 340 ) / 150 ) >= 1 ) && ( x[ i ] >= 30 ) && ( x[ i ] <= 100 ) ) ||
				( ( ( ( x[ i ] - 80 ) / 90 ) * ( ( x[ i ] - 80 ) / 90 ) + ( ( y[ i ] - 340 ) / 150 ) * ( ( y[ i ] - 340 ) / 150 ) >= 1 ) && ( ( ( x[ i ] - 120 ) / 280 ) * ( ( x[ i ] - 120 ) / 280 ) + ( ( y[ i ] - 340 ) / 150 ) * ( ( y[ i ] - 340 ) / 150 ) <= 1 ) && ( x[ i ] >= 100 ) ) ||
				( ( ( ( x[ i ] - 120 ) / 280 ) * ( ( x[ i ] - 120 ) / 280 ) + ( ( y[ i ] - 340 ) / 150 ) * ( ( y[ i ] - 340 ) / 150 ) <= 1 ) && ( x[ i ] >= 10 ) && ( x[ i ] <= 100 ) && ( 223 * x[ i ] - 243 * y[ i ] <= 94970) )
			)
			{
				pointCounter ++;
			}
		}
		
		if( pointsInside( pen.getCounter() , pointCounter ) )
		{
			checkNumber[ 5 ] = true;
		}
		else
		{
			checkNumber[ 5 ] = false;
		}
	}
	
	public void check6( int[] x , int[] y )
	{
		int pointCounter = 0;
		
		for( int i = 0 ; i < pen.getCounter() ; i ++)
		{
			if
			(
				( ( ( x[ i ] - 160 ) * ( x[ i ] - 160 ) + ( y[ i ] - 340 ) * ( y[ i ] - 340 ) >= 11025 ) && ( ( ( x[ i ] - 200 ) / 385 ) * ( ( x[ i ] - 200 ) / 385 ) + ( ( y[ i ] - 270 ) / 235 ) * ( ( y[ i ] - 270 ) / 235 ) <= 1 ) && ( ( ( x[ i ] - 80 ) / 90 ) * ( ( x[ i ] - 80 ) / 90 ) + ( ( y[ i ] - 340 ) / 150 ) * ( ( y[ i ] - 340 ) / 150 ) >= 1 ) && ( x[ i ] <= 265 ) ) ||
				( ( ( x[ i ] - 160 ) * ( x[ i ] - 160 ) + ( y[ i ] - 340 ) * ( y[ i ] - 340 ) <= 11025 ) && ( ( x[ i ] - 160 ) * ( x[ i ] - 160 ) + ( y[ i ] - 340 ) * ( y[ i ] - 340 ) >= 6400 ) )
			)
			{
				pointCounter ++;
			}
		}
		
		if( pointsInside( pen.getCounter() , pointCounter ) )
		{
			checkNumber[ 6 ] = true;
		}
		else
		{
			checkNumber[ 6 ] = false;
		}
	}
	
	
	public void check7( int[] x , int[] y )
	{
		int pointCounter = 0;
		
		for( int i = 0 ; i < pen.getCounter() ; i ++)
		{
			if
			(
				( ( 19 * x[ i ] + 8 * y[ i ] <= 4760 ) && ( y[ i ] >= 20 ) && ( y[ i ] <= 80 ) ) ||
				( ( 19 * x[ i ] + 8 * y[ i ] >= 4760 ) && ( 19 * x[ i ] + 8 * y[ i ] <= 6280 ) && ( y[ i ] >= 20 ) )
			)
			{
				pointCounter ++;
			}
		}
		
		if( pointsInside( pen.getCounter() , pointCounter ) )
		{
			checkNumber[ 7 ] = true;
		}
		else
		{
			checkNumber[ 7 ] = false;
		}
	}
	
	
	public void check8( int[] x , int[] y )
	{
		int pointCounter = 0;
		
		for( int i = 0 ; i < pen.getCounter() ; i ++)
		{
			if
			(
				( ( ( x[ i ] - 150 ) * ( x[ i ] - 150 ) + ( y[ i ] - 130 ) * ( y[ i ] - 130 ) >= 4900 ) && ( ( x[ i ] - 150 ) * ( x[ i ] - 150 ) + ( y[ i ] - 130 ) * ( y[ i ] - 130 ) <= 16900 ) ) ||
				( ( ( x[ i ] - 150 ) * ( x[ i ] - 150 ) + ( y[ i ] - 350 ) * ( y[ i ] - 350 ) >= 6400 ) && ( ( x[ i ] - 150 ) * ( x[ i ] - 150 ) + ( y[ i ] - 350 ) * ( y[ i ] - 350 ) <= 36100 ) )
			)
			{
				pointCounter ++;
			}
		}
		
		if( pointsInside( pen.getCounter() , pointCounter ) )
		{
			checkNumber[ 8 ] = true;
		}
		else
		{
			checkNumber[ 8 ] = false;
		}
	}
	
	
	public void check9( int[] x , int[] y )
	{
		int pointCounter = 0;
		
		for( int i = 0 ; i < pen.getCounter() ; i ++)
		{
			if
			(
				( ( ( x[ i ] - 150 ) * ( x[ i ] - 150 ) + ( y[ i ] - 150 ) * ( y[ i ] - 150 ) >= 8100 ) && ( ( x[ i ] - 150 ) * ( x[ i ] - 150 ) + ( y[ i ] - 150 ) * ( y[ i ] - 150 ) <= 19600 ) ) ||
				( ( ( x[ i ] - 150 ) * ( x[ i ] - 150 ) + ( y[ i ] - 150 ) * ( y[ i ] - 150 ) >= 19600 ) && ( ( ( x[ i ] - 100 ) / 130 ) * ( ( x[ i ] - 100 ) / 130 ) + ( ( y[ i ] - 260 ) / 170 ) * ( ( y[ i ] - 260 ) / 170 ) >= 1 ) && ( ( ( x[ i ] - 100 ) / 180 ) * ( ( x[ i ] - 100 ) / 180 ) + ( ( y[ i ] - 220 ) / 230 ) * ( ( y[ i ] - 220 ) / 230 ) <= 1 ) && ( x[ i ] >= 35 ) )
			)
			{
				pointCounter ++;
			}
		}
		
		if( pointsInside( pen.getCounter() , pointCounter ) )
		{
			checkNumber[ 9 ] = true;
		}
		else
		{
			checkNumber[ 9 ] = false;
		}
	}
	
	
	//Satisfy the written ward to corresponding size
	public int[] transform( int[] array , int counter , int check )
	{
		//Set center of the word written to the center of the panel
		int sum = 0;
		double ave , distance;
		
		for( int i = 0 ; i < counter ; i ++ )
		{
			sum += array[ i ];
		}
		
		ave = sum / counter;
		if( check == 1 )
		{
			distance = ave - ( getWidth() / 2 );
		}
		else
		{
			distance = ave - ( getHeight() / 2 );
		}
		
		for( int i = 0 ; i < counter ; i ++ )
		{
			array[ i ] += distance;
		}
		
		//Shrink or enlarge
		if( check == 1 )
		{
			for( int i = 0 ; i < counter ; i ++ )
			{
				array[ i ] -= 150;
			}
		}
		else
		{
			for( int i = 0 ; i < counter ; i ++ )
			{
				array[ i ] -= 250;
			}
		}
		
		if( check == 1 )
		{
			for( int i = 0 ; i < counter ; i ++ )
			{
				array[ i ] *= ( findMax( array , counter ) / 150 );
			}
		}
		else
		{
			for( int i = 0 ; i < counter ; i ++ )
			{
				array[ i ] *= ( findMax( array , counter ) / 250 );
			}
		}
		
		if( check == 1 )
		{
			for( int i = 0 ; i < counter ; i ++ )
			{
				array[ i ] += 150;
			}
		}
		else
		{
			for( int i = 0 ; i < counter ; i ++ )
			{
				array[ i ] += 250;
			}
		}
		
		return array;
	}
	
	public int findMax( int[] array , int counter )
	{
		int max = 0;
		
		for( int i = 0 ; i < counter ; i ++ )
		{
			if( array[ i ] > max )
			{
				max = array[ i ];
			}
		}
		
		return max;
	}
	
	//Count the number in model
	public boolean pointsInside( int counter , int pointCounter )
	{
		double percent = ( pointCounter / counter );
		
		if( percent >= 0.7 )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
