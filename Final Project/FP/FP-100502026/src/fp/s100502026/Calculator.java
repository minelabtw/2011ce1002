package fp.s100502026;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Calculator extends JFrame{
	
	private JPanel upperPanel = new JPanel();
	private JPanel lowerPanel = new JPanel( new GridLayout( 1 , 6 , 5 , 5 ) );
	private JLabel jlblPresentation = new JLabel();
	private JButton jbtEqual = new JButton( "=" );
	private JButton jbtPlus = new JButton( "+" );
	private JButton jbtMinus = new JButton( "-" );
	private JButton jbtMultiple = new JButton( "*" );
	private JButton jbtDevide = new JButton( "/" );
	private JButton jbtWriting = new JButton( "Writing" );
	private String formula = new String( " " );
	private String number = new String( "" );
	private String operator = new String( "" );
	private int[] numbers = new int[ 3 ];
	
	Calculator()
	{
		numbers[ 0 ] = numbers[ 1 ] = -1 ;
		
		jlblPresentation.setText( formula );
		
		upperPanel.add( jlblPresentation );
		
		if( numbers[ 1 ] == -1 )
		{
			lowerPanel.add( jbtPlus );
			lowerPanel.add( jbtMinus );
			lowerPanel.add( jbtMultiple );
			lowerPanel.add( jbtDevide );
		}
		lowerPanel.add( jbtEqual );
		lowerPanel.add( jbtWriting );
	
		jbtPlus.addActionListener
		(
			new ActionListener()
			{
				public void actionPerformed( ActionEvent e )
				{
					formula += ( " + " );
					saveNumber();
					operator = ( "+" );
					calculate();
				}
			}
		);
		
		jbtMinus.addActionListener
		(
			new ActionListener()
			{
				public void actionPerformed( ActionEvent e )
				{
					formula += ( " - " );
					saveNumber();
					operator = ( "-" );
					calculate();
				}
			}
		);
		
		jbtMultiple.addActionListener
		(
			new ActionListener()
			{
				public void actionPerformed( ActionEvent e )
				{
					formula += ( " * " );
					saveNumber();
					operator = ( "*" );
					calculate();
				}
			}
		);
		
		jbtDevide.addActionListener
		(
			new ActionListener()
			{
				public void actionPerformed( ActionEvent e )
				{
					formula += ( " / " );
					saveNumber();
					operator = ( "/" );
					calculate();
				}
			}
		);
		
		jbtEqual.addActionListener
		(
			new ActionListener()
			{
				public void actionPerformed( ActionEvent e )
				{
					if( ( numbers[ 0 ] != -1 ) && ( numbers[ 1 ] != -1 ) )
					{
						formula += ( " = " );
					}
					else
					{
						//JOptionPane.showConfirmDialog( null , "You have to complete this formula." , "Confirm Dlg" , JOptionPane.YES_OPTION , JOptionPane.INFORMATION_MESSAGE );
					}
				}
			}
		);
		
		jbtWriting.addActionListener
		(
			new ActionListener()
			{
				public void actionPerformed( ActionEvent e )
				{
					formula += ( "Keep writing number." );
				}
			}
		);
		
		this.add( upperPanel , BorderLayout.CENTER );
		this.add( lowerPanel , BorderLayout.SOUTH );
		
		this.setTitle( "Calculator" );
		this.setLocationRelativeTo( null );
		this.setSize( 500 , 300 );
		this.setVisible( false );
	}

	public void setFormula( int number )
	{
		formula += ( String.valueOf( number ) );
	}

	public void setNumber( int number )
	{
		this.number += ( String.valueOf( number ) );
	}

	public void saveNumber()
	{
		if( numbers[ 0 ] != -1 )
		{
			numbers[ 0 ] = Integer.parseInt( number );
		}
		else
		{
			numbers[ 1 ] = Integer.parseInt( number );
		}
		number = ( "" );

	}
	
	public void calculate()
	{
		if( operator == "+")
		{
			numbers[ 2 ] = numbers[ 0 ] + numbers[ 1 ];
		}
				
		else if( operator == "-")
		{
			numbers[ 2 ] = numbers[ 0 ] - numbers[ 1 ];
		}
				
		else if( operator == "*" )
		{
			numbers[ 2 ] = numbers[ 0 ] * numbers[ 1 ];
		}
				
		else if( operator == "/")
		{
			try
			{
				if( numbers[ 1 ] == 0 )
					throw new ArithmeticException( "Divisor cannot be zero" );
				
				numbers[ 2 ] = numbers[ 0 ] / numbers[ 1 ];
			}
			catch(ArithmeticException ex)
			{
				System.err.println( "Divisor is zero" );
				JOptionPane.showMessageDialog( null , "Divisor is zero." );
			}

		}
	}
}
