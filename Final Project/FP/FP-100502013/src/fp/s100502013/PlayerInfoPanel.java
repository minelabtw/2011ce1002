package fp.s100502013;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

public class PlayerInfoPanel extends JPanel{
	private JLabel scoreout = new JLabel();
	private JLabel hpout = new JLabel();
	private JLabel timeout = new JLabel();
	private JLabel tiplabel1 = new JLabel();
	private JLabel tiplabel2 = new JLabel();
	private Font usefont = new Font("TimesRoman",Font.BOLD,45);
	private Font textfont = new Font("TimesRoman",Font.BOLD,16);
	private String scoretoshow = "0";
	private String hptoshow = "100";
	private String timesectoshow = "000.0 sec";
	
	public PlayerInfoPanel(){ //set Infopanel details
		setOpaque(false);
		setLayout(null);
		setSize(600,200);
		scoreout.setBounds(0,150,200,75);
		hpout.setBounds(0,255,200,75);
		timeout.setBounds(0,360,200,75);
		tiplabel1.setBounds(0,450,200,25);
		tiplabel2.setBounds(0,475,200,25);
		scoreout.setOpaque(false);
		hpout.setOpaque(false);
		timeout.setOpaque(false);
		tiplabel1.setOpaque(false);
		tiplabel2.setOpaque(false);
		add(scoreout);
		add(hpout);
		add(timeout);
		add(tiplabel1);
		add(tiplabel2);
		scoreout.setFont(usefont);
		scoreout.setForeground(Color.MAGENTA);
		scoreout.setHorizontalAlignment(JTextField.CENTER);
		hpout.setFont(usefont);
		hpout.setForeground(Color.RED);
		hpout.setHorizontalAlignment(JTextField.CENTER);
		timeout.setFont(usefont);
		timeout.setForeground(Color.YELLOW);
		timeout.setHorizontalAlignment(JTextField.CENTER);
		tiplabel1.setForeground(Color.WHITE);
		tiplabel2.setForeground(Color.WHITE);
	}
	
	public void setscoretext(String x){ //set score output
		scoretoshow = x;
		scoreout.setText(scoretoshow);
	}
	
	public void sethptext(String x){ //set hp output
		hptoshow = x;
		hpout.setText(hptoshow);
	}
	
	public void settimetext(int x){ //set time output
		int thousand = x/1000;
		x -= thousand*1000;
		int hundred = x/100;
		x -= hundred*100;
		int ten = x/10;
		x -= ten*10;
		int one = x;
		timesectoshow = "" + thousand + hundred + ten + "." + one + " s" ;
		timeout.setText(timesectoshow);
	}
}
