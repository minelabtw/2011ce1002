package fp.s100502013;
import java.awt.*;
import java.awt.event.*;
import java.lang.Math;
import javax.swing.*;
import java.util.Random;

public class GamePlay extends JPanel{
	private Player player = new Player();
	private HitPoint playerHP = new HitPoint();
	private int hp = 100;
	private Timer playertime = new Timer(25,new TimerListener());
	private Timer circletime = new Timer(25, new CirclePlay());
	private boolean gameprocessing = true;
	private boolean restartpress = false;
	//normal circle set
	private NormalCircle[] normalc1 = new NormalCircle[9];
	private NormalCircle[] normalc2 = new NormalCircle[9];
	private NormalCircle[] normalc3 = new NormalCircle[9];
	private NormalCircle[] normalc4 = new NormalCircle[9];
	private NormalCircle[] normalc5 = new NormalCircle[9];
	private NormalCircle[] normalc6 = new NormalCircle[9];
	private NormalCircle[] normalc7 = new NormalCircle[9];
	private Timer normalsetlocation1 = new Timer(14200, new normallocation1());
	private Timer normalsetlocation2 = new Timer(14200, new normallocation2());
	private Timer normalsetlocation3 = new Timer(14200, new normallocation3());
	private Timer normalsetlocation4 = new Timer(14200, new normallocation4());
	private Timer normalsetlocation5 = new Timer(14200, new normallocation5());
	private Timer normalsetlocation6 = new Timer(14200, new normallocation6());
	private Timer normalsetlocation7 = new Timer(14200, new normallocation7());
	private Random normalaxis = new Random();
	private Random normallocations = new Random();
	//big circle set
	private BigCircle[] bigc1 = new BigCircle[3];
	private BigCircle[] bigc2 = new BigCircle[3];
	private Timer bigsetlocation1 = new Timer(16000, new biglocation1());
	private Timer bigsetlocation2 = new Timer(16000, new biglocation2());
	private Random bigaxis = new Random();
	private Random biglocations = new Random();
	//fast circle set
	private FastCircle[] fastc1 = new FastCircle[4];
	private FastCircle[] fastc2 = new FastCircle[4];
	private FastCircle[] fastc3 = new FastCircle[4];
	private FastCircle[] fastc4 = new FastCircle[4];
	private FastCircle[] fastc5 = new FastCircle[4];
	private Timer fastsetlocation1 = new Timer(5000, new fastlocation1());
	private Timer fastsetlocation2 = new Timer(5000, new fastlocation2());
	private Timer fastsetlocation3 = new Timer(5000, new fastlocation3());
	private Timer fastsetlocation4 = new Timer(5000, new fastlocation4());
	private Timer fastsetlocation5 = new Timer(5000, new fastlocation4());
	
	public GamePlay(){ //TOTAL GAMEPLAY:player key event and circle-in timer
		setOpaque(false);
		setcircle();
		addKeyListener(new KeyAdapter() { //player up down left right
			public void keyPressed(KeyEvent k){
				switch(k.getKeyCode()){
					case KeyEvent.VK_UP:
						player.setxSpeed(0);
						player.setySpeed(-player.getinitvelocity());
						break;
				
					case KeyEvent.VK_DOWN:
						player.setxSpeed(0);
						player.setySpeed(player.getinitvelocity());
						break;
				
					case KeyEvent.VK_RIGHT:
						player.setxSpeed(player.getinitvelocity());
						player.setySpeed(0);
						break;
				
					case KeyEvent.VK_LEFT:
						player.setxSpeed(-player.getinitvelocity());
						player.setySpeed(0);
						break;
						
					case KeyEvent.VK_ENTER:
						if(!getplayif()){
							playerHP.setinitHP();
							for(int i=0;i<9;i++){
								normalc1[i].setinitstatus();
								normalc2[i].setinitstatus();
								normalc3[i].setinitstatus();
								normalc4[i].setinitstatus();
								normalc5[i].setinitstatus();
								normalc6[i].setinitstatus();
								normalc7[i].setinitstatus();
							}
							for(int i=0;i<3;i++){
								bigc1[i].setinitstatus();
								bigc2[i].setinitstatus();
							}
							for(int i=0;i<4;i++){
								fastc1[i].setinitstatus();
								fastc2[i].setinitstatus();
								fastc3[i].setinitstatus();
								fastc4[i].setinitstatus();
								fastc5[i].setinitstatus();
							}
							setandstart();
							setplayif(true);
							restartpress = true;
							player.setinitstatus();
						}
						
					case KeyEvent.VK_ESCAPE:
						if(!getplayif()){
							System.exit(0);
						}
				}
				repaint();
			}
		});
		setandstart();
	}
	
	public void setandstart(){ //set initial delay and start
		normalsetlocation1.setInitialDelay(100);
		normalsetlocation2.setInitialDelay(500);
		normalsetlocation3.setInitialDelay(900);
		normalsetlocation4.setInitialDelay(23100);
		normalsetlocation5.setInitialDelay(49500);
		normalsetlocation6.setInitialDelay(97800);
		normalsetlocation7.setInitialDelay(118200);
		
		bigsetlocation1.setInitialDelay(9000);
		bigsetlocation2.setInitialDelay(129000);
		
		fastsetlocation1.setInitialDelay(30000);
		fastsetlocation2.setInitialDelay(47000);
		fastsetlocation3.setInitialDelay(74000);
		fastsetlocation4.setInitialDelay(101000);
		fastsetlocation5.setInitialDelay(128000);
		
		playertime.start();
		circletime.start();
		
		normalsetlocation1.start();
		normalsetlocation2.start();
		normalsetlocation3.start();
		normalsetlocation4.start();
		normalsetlocation5.start();
		normalsetlocation6.start();
		normalsetlocation7.start();
		
		bigsetlocation1.start();
		bigsetlocation2.start();
		
		fastsetlocation1.start();
		fastsetlocation2.start();
		fastsetlocation3.start();
		fastsetlocation4.start();
		fastsetlocation5.start();
	}
	
	public void setcircle(){ //new the circle
		for(int i=0;i<9;i++){
			normalc1[i] = new NormalCircle();
			normalc2[i] = new NormalCircle();
			normalc3[i] = new NormalCircle();
			normalc4[i] = new NormalCircle();
			normalc5[i] = new NormalCircle();
			normalc6[i] = new NormalCircle();
			normalc7[i] = new NormalCircle();
		}
		for(int i=0;i<3;i++){
			bigc1[i] = new BigCircle();
			bigc2[i] = new BigCircle();
		}
		for(int i=0;i<4;i++){
			fastc1[i] = new FastCircle();
			fastc2[i] = new FastCircle();
			fastc3[i] = new FastCircle();
			fastc4[i] = new FastCircle();
			fastc5[i] = new FastCircle();
		}
	}
	
	public void paintComponent(Graphics g){ //paint all things
		super.paintComponent(g);
		g.drawImage(player.geticon(),player.getx(), player.gety(),this);
		g.setColor(Color.RED);
		for(int i=0;i<9;i++){
			g.fillOval(normalc1[i].getx(),normalc1[i].gety(),20,20);
			g.fillOval(normalc2[i].getx(),normalc2[i].gety(),20,20);
			g.fillOval(normalc3[i].getx(),normalc3[i].gety(),20,20);
			g.fillOval(normalc4[i].getx(),normalc4[i].gety(),20,20);
			g.fillOval(normalc5[i].getx(),normalc5[i].gety(),20,20);
			g.fillOval(normalc6[i].getx(),normalc6[i].gety(),20,20);
			g.fillOval(normalc7[i].getx(),normalc7[i].gety(),20,20);
		}
		g.setColor(Color.GREEN);
		for(int i=0;i<3;i++){
			g.drawImage(bigc1[i].geticon(),bigc1[i].getx(),bigc1[i].gety(),this);
			g.drawImage(bigc2[i].geticon(),bigc2[i].getx(),bigc2[i].gety(),this);
		}
		g.setColor(Color.YELLOW);
		for(int i=0;i<4;i++){
			g.drawImage(fastc1[i].geticon(),fastc1[i].getx(),fastc1[i].gety(),this);
			g.drawImage(fastc2[i].geticon(),fastc2[i].getx(),fastc2[i].gety(),this);
			g.drawImage(fastc3[i].geticon(),fastc3[i].getx(),fastc3[i].gety(),this);
			g.drawImage(fastc4[i].geticon(),fastc4[i].getx(),fastc4[i].gety(),this);
			g.drawImage(fastc5[i].geticon(),fastc5[i].getx(),fastc5[i].gety(),this);
		}
	}
	
	public boolean checkalive(){ //check if die
		if(playerHP.gethitpoint()<=0){
			return false;
		}
		else{
			return true;
		}
	}
	
	public boolean checkhitting(int x1,int y1,int x2,int y2,int r1,int r2){ //check if get hit
		int cx1 = x1 + r1;
		int cy1 = y1 + r1; 
		int cx2 = x2 + r2; 
		int cy2 = y2 + r2;
		double dissquart = Math.pow((cx1-cx2),2) + Math.pow((cy1-cy2),2);
		double distance = Math.sqrt(dissquart);
		if(distance<=r1+r2){
			return true;
		}
		else{
			return false;
		}
	}
	
	public void setplayif(boolean x){ //set playing status
		gameprocessing = x;
	}
	
	public boolean getplayif(){ //get playing status
		return gameprocessing;
	}
	public void setrestart(boolean x){ //set the restart is pressed or not
		restartpress = x;
	}
	
	public boolean getrestartpress(){ //get the restart button status
		return restartpress;
	}
	
	public int savinghp(){ //save and provide getting hp
		hp = playerHP.gethitpoint();
		return hp;
	}
	
	public void checkdamage(){ //get damaged
		int playerx = player.getx();
		int playery = player.gety();
		int playerr = player.getradius();
		//normalc
		for(int i=0;i<9;i++){
			if(checkhitting(playerx,playery,normalc1[i].getx(),normalc1[i].gety(),playerr,normalc1[i].getradius())){
				playerHP.getdamaged(normalc1[i].getdamage());
			}
		}
		for(int i=0;i<9;i++){
			if(checkhitting(playerx,playery,normalc2[i].getx(),normalc2[i].gety(),playerr,normalc2[i].getradius())){
				playerHP.getdamaged(normalc2[i].getdamage());
			}
		}
		for(int i=0;i<9;i++){
			if(checkhitting(playerx,playery,normalc3[i].getx(),normalc3[i].gety(),playerr,normalc3[i].getradius())){
				playerHP.getdamaged(normalc3[i].getdamage());
			}
		}
		for(int i=0;i<9;i++){
			if(checkhitting(playerx,playery,normalc3[i].getx(),normalc3[i].gety(),playerr,normalc3[i].getradius())){
				playerHP.getdamaged(normalc3[i].getdamage());
			}
		}
		for(int i=0;i<9;i++){
			if(checkhitting(playerx,playery,normalc4[i].getx(),normalc4[i].gety(),playerr,normalc4[i].getradius())){
				playerHP.getdamaged(normalc4[i].getdamage());
			}
		}
		for(int i=0;i<9;i++){
			if(checkhitting(playerx,playery,normalc5[i].getx(),normalc5[i].gety(),playerr,normalc5[i].getradius())){
				playerHP.getdamaged(normalc5[i].getdamage());
			}
		}
		for(int i=0;i<9;i++){
			if(checkhitting(playerx,playery,normalc6[i].getx(),normalc6[i].gety(),playerr,normalc6[i].getradius())){
				playerHP.getdamaged(normalc6[i].getdamage());
			}
		}
		//bigc
		for(int i=0;i<3;i++){
			if(checkhitting(playerx,playery,bigc1[i].getx(),bigc1[i].gety(),playerr,bigc1[i].getradius())){
				playerHP.getdamaged(bigc1[i].getdamage());
			}
		}
		for(int i=0;i<3;i++){
			if(checkhitting(playerx,playery,bigc2[i].getx(),bigc2[i].gety(),playerr,bigc2[i].getradius())){
				playerHP.getdamaged(bigc2[i].getdamage());
			}
		}
		//fastc
		for(int i=0;i<4;i++){
			if(checkhitting(playerx,playery,fastc1[i].getx(),fastc1[i].gety(),playerr,fastc1[i].getradius())){
				playerHP.getdamaged(fastc1[i].getdamage());
			}
		}
		for(int i=0;i<4;i++){
			if(checkhitting(playerx,playery,fastc2[i].getx(),fastc2[i].gety(),playerr,fastc2[i].getradius())){
				playerHP.getdamaged(fastc2[i].getdamage());
			}
		}
		for(int i=0;i<4;i++){
			if(checkhitting(playerx,playery,fastc3[i].getx(),fastc3[i].gety(),playerr,fastc3[i].getradius())){
				playerHP.getdamaged(fastc3[i].getdamage());
			}
		}
		for(int i=0;i<4;i++){
			if(checkhitting(playerx,playery,fastc4[i].getx(),fastc4[i].gety(),playerr,fastc4[i].getradius())){
				playerHP.getdamaged(fastc4[i].getdamage());
			}
		}
		for(int i=0;i<4;i++){
			if(checkhitting(playerx,playery,fastc5[i].getx(),fastc5[i].gety(),playerr,fastc5[i].getradius())){
				playerHP.getdamaged(fastc5[i].getdamage());
			}
		}
	}
	
	class CirclePlay implements ActionListener{ //set the circle run
		public void actionPerformed(ActionEvent e){
			for(int i=0;i<9;i++){
				normalc1[i].setRun();
				normalc2[i].setRun();
				normalc3[i].setRun();
				normalc4[i].setRun();
				normalc5[i].setRun();
				normalc6[i].setRun();
				normalc7[i].setRun();
			}
			for(int i=0;i<3;i++){
				bigc1[i].setRun();
				bigc2[i].setRun();
			}
			for(int i=0;i<4;i++){
				fastc1[i].setRun();
				fastc2[i].setRun();
				fastc3[i].setRun();
				fastc4[i].setRun();
				fastc5[i].setRun();
			}
		}
	}
	
	class TimerListener implements ActionListener{ //player TimeListener
		public void actionPerformed(ActionEvent e){
			player.setplayerlocation();
			checkdamage();
			savinghp();
			if(checkalive()){
				repaint();
			}
			else{
				player.seticon();
				repaint();
				playertime.stop();
				circletime.stop();
				normalsetlocation1.stop();
				normalsetlocation2.stop();
				normalsetlocation3.stop();
				normalsetlocation4.stop();
				normalsetlocation5.stop();
				normalsetlocation6.stop();
				normalsetlocation7.stop();
				
				bigsetlocation1.stop();
				bigsetlocation2.stop();
				
				fastsetlocation1.stop();
				fastsetlocation2.stop();
				fastsetlocation3.stop();
				fastsetlocation4.stop();
				fastsetlocation5.stop();
				setplayif(false);
			}
		}
	}
	
	class normallocation1 implements ActionListener{ //set the first normal circle team location
		public void actionPerformed(ActionEvent e){
			int axis = Math.abs(normalaxis.nextInt()%4);
			int tl = Math.abs(normallocations.nextInt()%540);
			for(int i=0;i<9;i++){
				if(axis==0){
					normalc1[i].setDetail(tl,-110*i,0,normalc1[i].getvelocity());
				}
				else if(axis==1){
					normalc1[i].setDetail(tl,600+110*i,0,normalc1[i].getvelocity()*(-1));
				}
				else if(axis==2){
					normalc1[i].setDetail(-110*i,tl,normalc1[i].getvelocity(),0);
				}
				else{
					normalc1[i].setDetail(600+110*i,tl,normalc1[i].getvelocity()*(-1),0);
				}
			}
		}
	}
	
	class normallocation2 implements ActionListener{ //set the second normal circle team location
		public void actionPerformed(ActionEvent e){
			int axis = Math.abs(normalaxis.nextInt()%4);
			int tl = Math.abs(normallocations.nextInt()%540);
			for(int i=0;i<9;i++){
				if(axis==0){
					normalc2[i].setDetail(tl,-110*i,0,normalc2[i].getvelocity());
				}
				else if(axis==1){
					normalc2[i].setDetail(tl,600+110*i,0,normalc2[i].getvelocity()*(-1));
				}
				else if(axis==2){
					normalc2[i].setDetail(-110*i,tl,normalc2[i].getvelocity(),0);
				}
				else{
					normalc2[i].setDetail(600+110*i,tl,normalc2[i].getvelocity()*(-1),0);
				}
			}
		}
	}
	
	class normallocation3 implements ActionListener{ //set the third normal circle team location
		public void actionPerformed(ActionEvent e){
			int axis = Math.abs(normalaxis.nextInt()%4);
			int tl = Math.abs(normallocations.nextInt()%540);
			for(int i=0;i<9;i++){
				if(axis==0){
					normalc3[i].setDetail(tl,-110*i,0,normalc3[i].getvelocity());
				}
				else if(axis==1){
					normalc3[i].setDetail(tl,600+110*i,0,normalc3[i].getvelocity()*(-1));
				}
				else if(axis==2){
					normalc3[i].setDetail(-110*i,tl,normalc3[i].getvelocity(),0);
				}
				else{
					normalc3[i].setDetail(600+110*i,tl,normalc3[i].getvelocity()*(-1),0);
				}
			}
		}
	}
	
	class normallocation4 implements ActionListener{ //set the forth normal circle team location
		public void actionPerformed(ActionEvent e){
			int axis = Math.abs(normalaxis.nextInt()%4);
			int tl = Math.abs(normallocations.nextInt()%540);
			for(int i=0;i<9;i++){
				if(axis==0){
					normalc4[i].setDetail(tl,-110*i,0,normalc4[i].getvelocity());
				}
				else if(axis==1){
					normalc4[i].setDetail(tl,600+110*i,0,normalc4[i].getvelocity()*(-1));
				}
				else if(axis==2){
					normalc4[i].setDetail(-110*i,tl,normalc4[i].getvelocity(),0);
				}
				else{
					normalc4[i].setDetail(600+110*i,tl,normalc4[i].getvelocity()*(-1),0);
				}
			}
		}
	}
	
	class normallocation5 implements ActionListener{ //set the fifth normal circle team location
		public void actionPerformed(ActionEvent e){
			int axis = Math.abs(normalaxis.nextInt()%4);
			int tl = Math.abs(normallocations.nextInt()%540);
			for(int i=0;i<9;i++){
				if(axis==0){
					normalc5[i].setDetail(tl,-110*i,0,normalc5[i].getvelocity());
				}
				else if(axis==1){
					normalc5[i].setDetail(tl,600+110*i,0,normalc5[i].getvelocity()*(-1));
				}
				else if(axis==2){
					normalc5[i].setDetail(-110*i,tl,normalc5[i].getvelocity(),0);
				}
				else{
					normalc5[i].setDetail(600+110*i,tl,normalc5[i].getvelocity()*(-1),0);
				}
			}
		}
	}
	
	class normallocation6 implements ActionListener{ //set the sixth normal circle team location
		public void actionPerformed(ActionEvent e){
			int axis = Math.abs(normalaxis.nextInt()%4);
			int tl = Math.abs(normallocations.nextInt()%540);
			for(int i=0;i<9;i++){
				if(axis==0){
					normalc6[i].setDetail(tl,-110*i,0,normalc6[i].getvelocity());
				}
				else if(axis==1){
					normalc6[i].setDetail(tl,600+110*i,0,normalc6[i].getvelocity()*(-1));
				}
				else if(axis==2){
					normalc6[i].setDetail(-110*i,tl,normalc6[i].getvelocity(),0);
				}
				else{
					normalc6[i].setDetail(600+110*i,tl,normalc6[i].getvelocity()*(-1),0);
				}
			}
		}
	}
	
	class normallocation7 implements ActionListener{ //set the seventh normal circle team location
		public void actionPerformed(ActionEvent e){
			int axis = Math.abs(normalaxis.nextInt()%4);
			int tl = Math.abs(normallocations.nextInt()%540);
			for(int i=0;i<9;i++){
				if(axis==0){
					normalc7[i].setDetail(tl,-110*i,0,normalc7[i].getvelocity());
				}
				else if(axis==1){
					normalc7[i].setDetail(tl,600+110*i,0,normalc7[i].getvelocity()*(-1));
				}
				else if(axis==2){
					normalc7[i].setDetail(-110*i,tl,normalc7[i].getvelocity(),0);
				}
				else{
					normalc7[i].setDetail(600+110*i,tl,normalc7[i].getvelocity()*(-1),0);
				}
			}
		}
	}
	
	class biglocation1 implements ActionListener{ //set the first big circle team location
		public void actionPerformed(ActionEvent e){
			int axis = Math.abs(bigaxis.nextInt()%4);
			int tl = Math.abs(biglocations.nextInt()%140);
			for(int i=0;i<3;i++){
				if(axis==0){
					bigc1[i].setDetail(tl+200*i,-120,0,bigc1[i].getvelocity());
				}
				else if(axis==1){
					bigc1[i].setDetail(tl+200*i,720,0,bigc1[i].getvelocity()*(-1));
				}
				else if(axis==2){
					bigc1[i].setDetail(-120,tl+200*i,bigc1[i].getvelocity(),0);
				}
				else{
					bigc1[i].setDetail(720,tl+200*i,bigc1[i].getvelocity()*(-1),0);
				}
			}
		}
	}
	
	class biglocation2 implements ActionListener{ //set the second big circle team location
		public void actionPerformed(ActionEvent e){
			int axis = Math.abs(bigaxis.nextInt()%4);
			int tl = Math.abs(biglocations.nextInt()%140);
			for(int i=0;i<3;i++){
				if(axis==0){
					bigc2[i].setDetail(tl+200*i,-120,0,bigc2[i].getvelocity());
				}
				else if(axis==1){
					bigc2[i].setDetail(tl+200*i,720,0,bigc2[i].getvelocity()*(-1));
				}
				else if(axis==2){
					bigc2[i].setDetail(-120,tl+200*i,bigc2[i].getvelocity(),0);
				}
				else{
					bigc2[i].setDetail(720,tl+200*i,bigc2[i].getvelocity()*(-1),0);
				}
			}
		}
	}
	
	class fastlocation1 implements ActionListener{ //set the first fast circle team location
		public void actionPerformed(ActionEvent e){
			fastc1[0].setDetail(player.getx()+3,-25,0,fastc1[0].getvelocity());
			fastc1[1].setDetail(player.getx()+3,603,0,fastc1[1].getvelocity()*(-1));
			fastc1[2].setDetail(-25,player.gety()+3,fastc1[2].getvelocity(),0);
			fastc1[3].setDetail(603,player.gety()+3,fastc1[3].getvelocity()*(-1),0);
		}
	}
	
	class fastlocation2 implements ActionListener{ //set the second fast circle team location
		public void actionPerformed(ActionEvent e){
			fastc2[0].setDetail(player.getx()+3,-25,0,fastc2[0].getvelocity());
			fastc2[1].setDetail(player.getx()+3,603,0,fastc2[1].getvelocity()*(-1));
			fastc2[2].setDetail(-25,player.gety()+3,fastc2[2].getvelocity(),0);
			fastc2[3].setDetail(603,player.gety()+3,fastc2[3].getvelocity()*(-1),0);
		}
	}
	
	class fastlocation3 implements ActionListener{ //set the third fast circle team location
		public void actionPerformed(ActionEvent e){
			fastc3[0].setDetail(player.getx()+3,-25,0,fastc3[0].getvelocity());
			fastc3[1].setDetail(player.getx()+3,603,0,fastc3[1].getvelocity()*(-1));
			fastc3[2].setDetail(-25,player.gety()+3,fastc3[2].getvelocity(),0);
			fastc3[3].setDetail(603,player.gety()+3,fastc3[3].getvelocity()*(-1),0);
		}
	}
	
	class fastlocation4 implements ActionListener{ //set the forth fast circle team location
		public void actionPerformed(ActionEvent e){
			fastc4[0].setDetail(player.getx()+3,-25,0,fastc4[0].getvelocity());
			fastc4[1].setDetail(player.getx()+3,603,0,fastc4[1].getvelocity()*(-1));
			fastc4[2].setDetail(-25,player.gety()+3,fastc4[2].getvelocity(),0);
			fastc4[3].setDetail(603,player.gety()+3,fastc4[3].getvelocity()*(-1),0);
		}
	}
	
	class fastlocation5 implements ActionListener{ //set the fifth fast circle team location
		public void actionPerformed(ActionEvent e){
			fastc5[0].setDetail(player.getx()+3,-25,0,fastc5[0].getvelocity());
			fastc5[1].setDetail(player.getx()+3,603,0,fastc5[1].getvelocity()*(-1));
			fastc5[2].setDetail(-25,player.gety()+3,fastc5[2].getvelocity(),0);
			fastc5[3].setDetail(603,player.gety()+3,fastc5[3].getvelocity()*(-1),0);
		}
	}
}
