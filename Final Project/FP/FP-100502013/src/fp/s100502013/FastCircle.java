package fp.s100502013;
import java.awt.*;
import javax.swing.*;

public class FastCircle extends NormalCircle{ //fast circle 
	private int radius = 7;
	private int dx = 0;
	private int dy = 0;
	private int initvelocity = 10;
	private ImageIcon icon = new ImageIcon("src/fp/s100502013/gFASTBALL.png");
	private Image circle = icon.getImage(); 

	public FastCircle(){
		
	}
	
	public int getvelocity(){
		return initvelocity;
	}
	
	public int getradius(){
		return radius;
	}
	
	public Image geticon(){
		return circle;
	}
}
