package fp.s100502013;
import java.awt.Graphics;
import java.awt.event.*;
import javax.swing.*;

public class MainBackground extends JPanel{ //set menu background
	private Timer mainbgtimer = new Timer(100, new mbgtime());
	private ImageIcon background = new ImageIcon("src/fp/s100502013/TITLE01.png");
	private int mbgtype = 1;
	private String mbgdir = "lighter";
	
	public MainBackground(){
		mainbgtimer.start();
	}
	
	public void paintComponent(Graphics g){
		g.drawImage(background.getImage(), 0, 0, 800, 600, null);
	}
	
	class mbgtime implements ActionListener{ //set background animate
		public void actionPerformed(ActionEvent e) {
			if(mbgtype==0){
				background = new ImageIcon("src/fp/s100502013/TITLE01.png");
				mbgtype++;
				mbgdir = "lighter";
			}
			else if(mbgtype==1){
				background = new ImageIcon("src/fp/s100502013/TITLE01.png");
				if(mbgdir=="lighter"){
					mbgtype++;
				}
				else{
					mbgtype--;
				}
			}
			else if(mbgtype==2){
				background = new ImageIcon("src/fp/s100502013/TITLE02.png");
				if(mbgdir=="lighter"){
					mbgtype++;
				}
				else{
					mbgtype--;
				}
			}
			else if(mbgtype==3){
				background = new ImageIcon("src/fp/s100502013/TITLE03.png");
				if(mbgdir=="lighter"){
					mbgtype++;
				}
				else{
					mbgtype--;
				}
			}
			else if(mbgtype==4){
				background = new ImageIcon("src/fp/s100502013/TITLE04.png");
				if(mbgdir=="lighter"){
					mbgtype++;
				}
				else{
					mbgtype--;
				}
			}
			else if(mbgtype==5){
				background = new ImageIcon("src/fp/s100502013/TITLE05.png");
				if(mbgdir=="lighter"){
					mbgtype++;
				}
				else{
					mbgtype--;
				}
			}
			else if(mbgtype==6){
				background = new ImageIcon("src/fp/s100502013/TITLE06.png");
				if(mbgdir=="lighter"){
					mbgtype++;
				}
				else{
					mbgtype--;
				}
			}
			else if(mbgtype==7){
				background = new ImageIcon("src/fp/s100502013/TITLE07.png");
				if(mbgdir=="lighter"){
					mbgtype++;
				}
				else{
					mbgtype--;
				}
			}
			else if(mbgtype==8){
				background = new ImageIcon("src/fp/s100502013/TITLE08.png");
				if(mbgdir=="lighter"){
					mbgtype++;
				}
				else{
					mbgtype--;
				}
			}
			else if(mbgtype==9){
				background = new ImageIcon("src/fp/s100502013/TITLE09.png");
				if(mbgdir=="lighter"){
					mbgtype++;
				}
				else{
					mbgtype--;
				}
			}
			else if(mbgtype==10){
				background = new ImageIcon("src/fp/s100502013/TITLE10.png");
				if(mbgdir=="lighter"){
					mbgtype++;
				}
				else{
					mbgtype--;
				}
			}
			else if(mbgtype==11){
				background = new ImageIcon("src/fp/s100502013/TITLE11.png");
				if(mbgdir=="lighter"){
					mbgtype++;
				}
				else{
					mbgtype--;
				}
			}
			else if(mbgtype==12){
				background = new ImageIcon("src/fp/s100502013/TITLE12.png");
				if(mbgdir=="lighter"){
					mbgtype++;
				}
				else{
					mbgtype--;
				}
			}
			else if(mbgtype==13){
				background = new ImageIcon("src/fp/s100502013/TITLE13.png");
				if(mbgdir=="lighter"){
					mbgtype++;
				}
				else{
					mbgtype--;
				}
			}
			else if(mbgtype==14){
				background = new ImageIcon("src/fp/s100502013/TITLE14.png");
				if(mbgdir=="lighter"){
					mbgtype++;
				}
				else{
					mbgtype--;
				}
			}
			else if(mbgtype==15){
				background = new ImageIcon("src/fp/s100502013/TITLE15.png");
				if(mbgdir=="lighter"){
					mbgtype++;
				}
				else{
					mbgtype--;
				}
			}
			else{
				background = new ImageIcon("src/fp/s100502013/TITLE015.png");
				mbgtype--;
				mbgdir = "darker";
			}
			repaint();
		}
	}
}
