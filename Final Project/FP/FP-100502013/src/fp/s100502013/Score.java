package fp.s100502013;

public class Score { //save the score
	private int score = 0;
	
	public Score(){
		score = 0;
	}
	
	public void setinitscore(){
		score = 0;
	}
	
	public void setscore(int x){
		score += x;
	}
	
	public int getscore(){
		return score;
	}
}