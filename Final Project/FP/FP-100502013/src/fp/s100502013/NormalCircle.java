package fp.s100502013;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class NormalCircle extends JLabel{ //this is the normal circle details
	private int radius = 10;
	private int x = -300;
	private int y = -300;
	private int dx = 0;
	private int dy = 0;
	private int initvelocity = 4;
	private int damage = 1;
	
	public NormalCircle(){
		x = -300;
		y = -300;
		dx = 0;
		dy = 0;
	}
	
	public void setinitstatus(){
		x = -300;
		y = -300;
		dx = 0;
		dy = 0;
	}
	
	public void setDetail(int a,int b,int c,int d){
		x = a - dx;
		y = b - dy;
		dx = c;
		dy = d;
	}
	
	public void setRun(){
		x += dx;
		y += dy;
	}
	
	public int getx(){
		return x;
	}
	
	public int gety(){
		return y;
	}
	
	public int getdx(){
		return dx;
	}
	
	public int getdy(){
		return dy;
	}
	
	public int getradius(){
		return radius;
	}
	
	public int getvelocity(){
		return initvelocity;
	}
	
	public int getdamage(){
		return damage;
	}
}
