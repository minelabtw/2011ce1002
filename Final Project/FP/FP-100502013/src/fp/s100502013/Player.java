package fp.s100502013;
import java.awt.*;
import java.awt.event.*;
import java.lang.Math;
import javax.swing.*;
import java.util.Random;

public class Player extends JPanel{ //this is the player details
	private int radius = 10;
	private int x = 280;
	private int y = 280;
	private int dx = 0;
	private int dy = 0;
	private int initvelocity = 10;
	private int score = 0;
	private ImageIcon icon = new ImageIcon("src/fp/s100502013/gPLAYER.png");
	private Image circle = icon.getImage(); 

	public Player(){
		x = 280;
		y = 280;
		dx = 0;
		dy = 0;
		initvelocity = 10;
		score = 0;
	}
	
	public void setinitstatus(){
		x = 280;
		y = 280;
		dx = 0;
		dy = 0;
		initvelocity = 10;
		score = 0;
		icon = new ImageIcon("src/fp/s100502013/gPLAYER.png");
		circle = icon.getImage(); 
	}
	
	public void setxSpeed(int a){
		dx = a;
	}
	
	public void setySpeed(int a){
		dy = a;
	}
	
	public void setscore(int s){
		score = s;
	}
	
	public int getx(){
		return x;
	}
	
	public int gety(){
		return y;
	}
	
	public int getradius(){
		return radius;
	}
	
	public int getinitvelocity(){
		return initvelocity;
	}
	
	public void seticon(){
		icon = new ImageIcon("src/fp/s100502013/gBLAST.png");
		circle = icon.getImage(); 
		x -= 50;
		y -= 50;
	}
	
	public Image geticon(){
		return circle;
	}
	
	public void setplayerlocation(){ //if hitting the wall,velocity*(-1)
		if(x==0){
			dx = initvelocity;
		}
		if(y==0){
			dy = initvelocity;
		}
		if(x==580){
			dx = -initvelocity;
		}
		if(y==550){
			dy = -initvelocity;
		}
		x += dx;
		y += dy;
	}
}
