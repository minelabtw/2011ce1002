package fp.s100502013;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class BigCircle extends NormalCircle{ //big circle
	private int radius = 60;
	private int dx = 0;
	private int dy = 0;
	private int initvelocity = 2;
	private ImageIcon icon = new ImageIcon("src/fp/s100502013/gBIGBALL.png");
	private Image circle = icon.getImage();
	
	public BigCircle(){
		
	}
	
	public int getvelocity(){
		return initvelocity;
	}
	
	public int getradius(){
		return radius;
	}
	
	public Image geticon(){
		return circle;
	}
}
