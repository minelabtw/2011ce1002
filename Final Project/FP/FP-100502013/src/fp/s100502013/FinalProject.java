package fp.s100502013;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class FinalProject extends JFrame implements KeyListener{
	private JLabel buttonsheet = new JLabel();
	private JLabel startbutton = new JLabel();
	private JLabel helpbutton = new JLabel();
	private JLabel exitbutton = new JLabel();
	private ImageIcon starticon = new ImageIcon("src/fp/s100502013/BUTTONSTART.png");
	private ImageIcon starticonsec = new ImageIcon("src/fp/s100502013/BUTTONSTARTSEC.png");
	private ImageIcon exiticon = new ImageIcon("src/fp/s100502013/BUTTONEXIT.png");
	private ImageIcon exiticonsec = new ImageIcon("src/fp/s100502013/BUTTONEXITSEC.png");
	private ImageIcon helpicon = new ImageIcon("src/fp/s100502013/BUTTONHELP.png");
	private ImageIcon helpiconsec = new ImageIcon("src/fp/s100502013/BUTTONHELPSEC.png");
	private MainBackground mbg = new MainBackground();
	private HelpPanel help = new HelpPanel();
	private String[] mainbutton = {"start","help","exit"};
	private int buttonstatus = 0;
	private boolean helpstatus = false;
	private static FinalProject mygame = new FinalProject();
	
	public FinalProject(){ //set menu
		startbutton.setIcon(starticonsec);
		helpbutton.setIcon(helpicon);
		exitbutton.setIcon(exiticon);
		buttonsheet.setLayout(null);
		startbutton.setBounds(50,460,200,50);
		helpbutton.setBounds(300,460,200,50);
		exitbutton.setBounds(550,460,200,50);
		buttonsheet.setBounds(0,0,800,600);
		help.setBounds(100,50,600,400);
		help.setVisible(false);
		mbg.setLayout(new BorderLayout(0,0));
		setLayout(new BorderLayout(0,0));
		buttonsheet.setOpaque(false);
		buttonsheet.add(help);
		buttonsheet.add(startbutton);
		buttonsheet.add(helpbutton);
		buttonsheet.add(exitbutton);
		mbg.add(buttonsheet,BorderLayout.CENTER);
		add(mbg,BorderLayout.CENTER);
		addKeyListener(this);
	}
	
	public static void main(String[] args){
		mygame.setTitle("AtomCoreMania");
		mygame.setSize(800,600);
		mygame.setResizable(false);
		mygame.setLocationRelativeTo(null);
		mygame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mygame.setVisible(true);
	}

	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode()==e.VK_ENTER){ //menu control
			if(!helpstatus){
				if(mainbutton[buttonstatus]=="exit"){
					System.exit(0);
				}
				else if(mainbutton[buttonstatus]=="start"){
					Game game = new Game();
					game.setTitle("AtomCoreMania");
					game.setSize(800,600);
					game.setResizable(false);
					game.setLocationRelativeTo(null);
					game.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					game.setVisible(true);
					mygame.removeAll();
				}
				else{
					help.setVisible(true);
					helpstatus = true;
				}
			}
			else{
				help.setVisible(false);
				helpstatus = false;
			}
		}
		else if(e.getKeyCode()==e.VK_RIGHT){ //change button
			if(!helpstatus){
				if(mainbutton[buttonstatus]=="start"){
					startbutton.setIcon(starticon);
					helpbutton.setIcon(helpiconsec);
					exitbutton.setIcon(exiticon);
					buttonstatus++;
				}
				else if(mainbutton[buttonstatus]=="help"){
					startbutton.setIcon(starticon);
					helpbutton.setIcon(helpicon);
					exitbutton.setIcon(exiticonsec);
					buttonstatus++;
				}
			}
		}
		else if(e.getKeyCode()==e.VK_LEFT){
			if(!helpstatus){
				if(mainbutton[buttonstatus]=="exit"){
					startbutton.setIcon(starticon);
					helpbutton.setIcon(helpiconsec);
					exitbutton.setIcon(exiticon);
					buttonstatus--;
				}
				else if(mainbutton[buttonstatus]=="help"){
					startbutton.setIcon(starticonsec);
					helpbutton.setIcon(helpicon);
					exitbutton.setIcon(exiticon);
					buttonstatus--;
				}
			}
		}
	}

	public void keyReleased(KeyEvent e) {
		
	}

	public void keyTyped(KeyEvent e) {
		
	}
}
