package fp.s100502013;

public class HitPoint { //a hitpoint details class
	private int hitpoint = 100;
	
	public HitPoint(){
		
	}
	
	public void getdamaged(int a){
		if(hitpoint-a<0){
			hitpoint = 0;
		}
		else{
			hitpoint -= a;
		}
	}
	
	public int gethitpoint(){
		return hitpoint;
	}
	
	public void setinitHP(){
		hitpoint = 100;
	}
}
