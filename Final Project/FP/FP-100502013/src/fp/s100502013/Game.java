package fp.s100502013;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.*;

public class Game extends JFrame{
	private GamePlay gameplay = new GamePlay();
	private Background bgpanel = new Background();
	private JPanel totalpanel = new JPanel();
	private JLabel GGlabel = new JLabel();
	private ImageIcon GGicon = new ImageIcon("src/fp/s100502013/GG.png");
	private PlayerInfoPanel info = new PlayerInfoPanel();
	private InfoBackground infobg = new InfoBackground();
	private Score playerscore = new Score();
	private int timetenpa = 0;
	private int hardalt = 0;
	private int hp = 100;
	private Timer scorecounting = new Timer(100, new scorecount());
	private Timer hpcounting = new Timer(50, new hpcount());
	private Timer timecounting = new Timer(100, new timecount());
	private Timer checkpress = new Timer(100, new checktime());
	
	public Game(){ //set details
		setLayout(null);
		setSize(800,600);
		gameplay.setLayout(null);
		GGlabel.setBounds(100,250,400,100);
		GGlabel.setIcon(GGicon);
		GGlabel.setVisible(false);
		gameplay.add(GGlabel);
		GGlabel.setBorder(new LineBorder(Color.GRAY,2));
		bgpanel.setLayout(new BorderLayout(0,0));
		bgpanel.add(gameplay, BorderLayout.CENTER);
		totalpanel.setLayout(new BorderLayout(0,0));
		totalpanel.add(bgpanel, BorderLayout.CENTER);
		totalpanel.setSize(600,600);
		infobg.setLayout(new BorderLayout(0,0));
		infobg.setSize(200,600);
		infobg.add(info);
		add(totalpanel);
		totalpanel.setBounds(0,0,600,600);
		infobg.setBounds(600,0,200,600);
		add(infobg);
		gameplay.setFocusable(true);
		scorecounting.start();
		hpcounting.start();
		timecounting.start();
		checkpress.start();
	}
	
	public void hardaltset(){ //set the hard status to set score
		switch(timetenpa*100) {
			case 100:
				hardalt++;
				break;
			case 500:
				hardalt++;
				break;
			case 900:
				hardalt++;
				break;
			case 23100:
				hardalt += 2;
				break;
			case 49500:
				hardalt += 2;
				break;
			case 97800:
				hardalt += 3;
				break;
			case 118200:
				hardalt += 4;
				break;
			
			case 9000:
				hardalt += 4;
				break;
			case 129000:
				hardalt += 17;
				break;
			
			case 30000:
				hardalt += 3;
				break;
			case 47000:
				hardalt += 4;
				break;
			case 74000:
				hardalt += 6;
				break;
			case 101000:
				hardalt += 9;
				break;
			case 128000:
				hardalt += 13;
				break;
				
			default:
				break;
		}
	}
	
	public void setplayerhp(){ //set hp to output
		if(gameplay.checkalive()){
			hp = gameplay.savinghp();
		}
		else{
			hp = 0;
		}
	}
	
	class scorecount implements ActionListener{ //count score and set
		public void actionPerformed(ActionEvent e) {
			if(gameplay.getplayif()){
				timetenpa++;
				hardaltset();
				playerscore.setscore(hardalt);
			}
			else{
				scorecounting.stop();
			}
			String temp = "";
			temp += playerscore.getscore();
			info.setscoretext(temp);
		}
	}
	
	class hpcount implements ActionListener{ //count hp and set
		public void actionPerformed(ActionEvent e) {
			setplayerhp();
			String temphp = "" + hp;
			info.sethptext(temphp);
			if(!gameplay.getplayif()){
				hpcounting.stop();
				GGlabel.setVisible(true);
			}
		}
	}
	
	class timecount implements ActionListener{ //count time and set
		public void actionPerformed(ActionEvent e){
			info.settimetext(timetenpa);
			if(!gameplay.getplayif()){
				timecounting.stop();
			}
		}
	}
	
	class checktime implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if(gameplay.getrestartpress()){
				timetenpa = 0;
				hardalt = 0;
				hp = 100;
				playerscore.setinitscore();
				scorecounting.start();
				hpcounting.start();
				timecounting.start();
				GGlabel.setVisible(false);
				gameplay.setrestart(false);
			}
		}
	}
}
