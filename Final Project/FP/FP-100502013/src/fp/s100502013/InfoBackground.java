package fp.s100502013;
import java.awt.*;
import javax.swing.*;

public class InfoBackground extends JPanel{ //set infoPanel background
	private ImageIcon background = new ImageIcon("src/fp/s100502013/INFO.png");
	
	public void paintComponent(Graphics g){
		g.drawImage(background.getImage(), 0, 0, 200, 600, null);
	}
}
