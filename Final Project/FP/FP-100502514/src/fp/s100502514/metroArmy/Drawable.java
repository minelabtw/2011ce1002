/** The following inner classes are just for the scenery outside the car window. */
package fp.s100502514.metroArmy;

import java.awt.*;

public abstract class Drawable {
	double x, z, height;
	public double getZ(){
		return z;
	}
	public abstract void drawOn(RaceCar car, Graphics g);
	public abstract boolean hitTest(Point p);
	
	/** Is the display-object destroyable? */
	public abstract boolean isDestroyable();
	
	/** Destroy (hide) from the scene and return a value for its danger level. */
	public abstract double destroyStuff();
}
