/** class RangeSelectMenu: the level(range) selecting panel */
package fp.s100502514.metroArmy;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class RangeSelectMenu extends JPanel {

	/** UID */
	private static final long serialVersionUID = 1L;
	
	//The map member in the panel
	StationMap choose_map;
	
	//The background image
	ImageIcon bg = new ImageIcon(this.getClass().getResource(
			"images/title_select.jpg"));
	
	//Get the selected data from StationMap choose_map.
	double[] selectedRange = null;
	
	public RangeSelectMenu(){

		//setPreferredSize(new Dimension(600, 300));
		//setBackground(new Color(0, 0, 0, 153));
		
		setBackground(new Color(0, 0, 153));
		
		/*
		choose_map = new StationMap((int)(getWidth()*0.8),
				0, 5750, Double.NaN, 10);
		choose_map.setX((int)(getWidth()*0.1));
		choose_map.setY((int)(getHeight()*0.1));
		*/
		addMouseListener(new MouseAdapter(){
			private double tmp_first_station_width = 0;
			
			@Override
			public void mousePressed(MouseEvent e) {
				if(e.getButton() == MouseEvent.BUTTON1){
					//When Left Mouse Button is Clicked
					if(choose_map==null){
						//System.out.println("Map is null");
						return;
					}
					//Find the position that mouse clicks.
					double orig_map_pos = choose_map.getOriginalPosition(getMousePosition());
					double chosen_station_width = 0;
					
					//Adjust the meter to the nearest station.
					double min_meter = (choose_map.getRightPos()-choose_map.getLeftPos())*2;
					for(int i=0; i < StationMap.station_info.length; i++){
						double tmp = Math.abs(
								StationMap.station_info[i].x +
								StationMap.station_info[i].width/2
								-orig_map_pos);
						if(tmp > min_meter){
							if(i>0){
								//Always true but still prevent from Exception thrown.
								orig_map_pos = StationMap.station_info[i-1].x +
										StationMap.station_info[i-1].width/2;
								chosen_station_width = StationMap.station_info[i-1].width;
							}
							break;
						}
						min_meter = tmp;
					}
					
					choose_map.selectRange(orig_map_pos,
							tmp_first_station_width, chosen_station_width);
					selectedRange = choose_map.getSelectRange();
					
					if(!choose_map.isSelectCompleted()){
						tmp_first_station_width = chosen_station_width;
					}else{
						tmp_first_station_width = 0;
					}
					
					//System.out.println(orig_map_pos);
				}
				repaint();
			}
		});
		
		//Timer for repainting constantly.
		Timer timer = new Timer(100, new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				repaint();
			}
		});
		timer.start();
		
	}
	
	/** Return the result of user choice in range-select-menu, where:
	 *  [0]begin-bound
	 *  [1]end-bound
	 *  [2]player-start
	 *  [3]goal-location.
	 */
	public double[] getUserChoice(){
		if(choose_map==null)return null;
		if(choose_map.isSelectCompleted()){
			return new double[]{
					choose_map.getSelectRange()[0],
					choose_map.getSelectRange()[1],
					choose_map.getSelectRange()[2],
					choose_map.getSelectRange()[3]};
		}else{
			return null;
		}
	}
	
	//Painting Method
	@Override
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		//g.setColor(Color.black);
		
		//Draw on the background
		g.drawImage(bg.getImage(), 0, 0,
				g.getClipBounds().width, g.getClipBounds().height, this);
		
		//Draw on the map
		choose_map = new StationMap((int)(g.getClipBounds().width*0.8),
				0, 5750, Double.NaN, (int)(g.getClipBounds().width*0.008),
				selectedRange);
		choose_map.setX((int)(getWidth()*0.1));
		choose_map.setY((int)(getHeight()*0.4));
		choose_map.drawOn(g);
	}
	
}
