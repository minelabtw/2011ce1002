/** class StationIcon: the station icon class for StationMap use. */
package fp.s100502514.metroArmy;

import java.awt.*;

public class StationIcon {
	
	Station station;
	StationMap map;
	
	public StationIcon(Station station, StationMap map){
		this.station = station;
		this.map = map;
	}
	
	private static final double STATION_BORDER = 20;
	private Point getBeginPosition(boolean isOuter){
		return map.getPositionCoordinate(isOuter ? station.x-STATION_BORDER : station.x);
	}
	private Point getEndPosition(boolean isOuter){
		return map.getPositionCoordinate(isOuter ? station.x+station.width+STATION_BORDER :
			station.x+station.width);
	}
	
	private static final double MAX_STATION_HEIGHT = 80;
	private static final int MAX_STATION_PIXEL = 40;
	private static final int MIN_STATION_PIXEL = 15;

	//Drawing
	public void drawOn(Graphics g, Color outerColor, Color innerColor){
		if(station.x+station.width > map.getRightPos() ||
				station.x < map.getLeftPos()){
			//Do not draw those which are out-of-bounds.
			return;
		}
		
		//save the original color and font to turn back after the drawing ends.
		Color origColor = g.getColor();
		Font origFont = g.getFont();
		
		//Drawing Outer circle of station icon
		Point p1 = getBeginPosition(true), p2 = getEndPosition(true);
		int size12 = (p2.x - p1.x);
		g.setColor(outerColor);
		
		int MAX = Math.min(map.getPositionCoordinate(MAX_STATION_HEIGHT).x-
				map.getPositionCoordinate(0).x,
				MAX_STATION_PIXEL);
		if(MAX < MIN_STATION_PIXEL){
			MAX = MIN_STATION_PIXEL;
		}
		
		if(Math.abs(size12)>MAX){
			if(size12>0){
				g.fillOval(p1.x, p1.y-MAX/2, MAX, MAX);
				g.fillOval(p1.x+size12-MAX, p1.y-MAX/2, MAX, MAX);
				g.fillRect(p1.x+MAX/2, p1.y-MAX/2, size12-MAX, MAX);
			}else{
				g.fillOval(p2.x, p2.y-MAX/2, MAX, MAX);
				g.fillOval(p2.x-size12-MAX, p2.y-MAX/2, MAX, MAX);
				g.fillRect(p2.x+MAX/2, p2.y-MAX/2, -size12-MAX, MAX);
			}
		}else{
			if(Math.abs(size12) < MIN_STATION_PIXEL){
				if(size12>0)size12 = MIN_STATION_PIXEL;
				else size12 = -MIN_STATION_PIXEL;
			}
			
			if(size12>0) g.fillOval(p1.x, p1.y-size12/2, size12, size12);
			else g.fillOval(p2.x, p2.y+size12/2, -size12, -size12);
		}
		
		//Drawing Inner circle of station icon
		Point p3 = getBeginPosition(false), p4 = getEndPosition(false);
		int size34 = (p4.x - p3.x);
		g.setColor(innerColor);
		int MAX_IN = Math.min(map.getPositionCoordinate(MAX_STATION_HEIGHT).x-
				map.getPositionCoordinate(STATION_BORDER).x,
				MAX_STATION_PIXEL-5);
		final int MIN_IN_PIXEL = MIN_STATION_PIXEL-5;
		if(MAX_IN < MIN_IN_PIXEL){
			MAX_IN = MIN_IN_PIXEL;
			//if(MAX_IN<1)MAX_IN=1;
		}
		if(Math.abs(size34)>MAX_IN){
			if(size34>0){
				g.fillOval(p3.x, p3.y-MAX_IN/2, MAX_IN, MAX_IN);
				g.fillOval(p3.x+size34-MAX_IN, p3.y-MAX_IN/2, MAX_IN, MAX_IN);
				g.fillRect(p3.x+MAX_IN/2, p3.y-MAX_IN/2, size34-MAX_IN, MAX_IN);
			}else{
				g.fillOval(p4.x, p4.y-MAX_IN/2, MAX_IN, MAX_IN);
				g.fillOval(p4.x-size34-MAX_IN, p4.y-MAX_IN/2, MAX_IN, MAX_IN);
				g.fillRect(p4.x+MAX_IN/2, p4.y-MAX_IN/2, -size34-MAX_IN, MAX_IN);
			}
		}else{
			if(Math.abs(size34) < MIN_IN_PIXEL){
				if(size34>0)size34 = MIN_IN_PIXEL;
				else size34 = -MIN_IN_PIXEL;
			}
			
			if(size34>0) g.fillOval(p3.x, p3.y-size34/2, size34, size34);
			else g.fillOval(p4.x, p4.y+size34/2, -size34, -size34);
		}
		//Draw the station name label
		int fontSize = Math.min(Math.abs(size12), MAX*2);
		Font labelFont = new Font("�L�n������", Font.BOLD, fontSize);
		String labelName = station.name;
		g.setColor(outerColor);
		g.setFont(labelFont);
		g.drawString(labelName,
				(p1.x+p2.x)/2 - g.getFontMetrics(labelFont).stringWidth(labelName)/2,
				(int)(p1.y+fontSize*1.5));
		
		//Make the color and font setting back.
		g.setColor(origColor);
		g.setFont(origFont);
	}
	
}
