/**
 * An inheritance of Scenery.
 * A player must win by clicking this stuff.
 */
package fp.s100502514.metroArmy;

import java.awt.Graphics;

public class WinnerBox extends Scenery {
	
	/** Default image index, defined by static variable in GameSceneData.java */
	static final double height = 1.3; //a constant of 1 meter
	static final int img_index = 4; //winner_box.png
	
	/** All constructor implementations */
	public WinnerBox() {
		super(height, img_index);
	}
	public WinnerBox(double x, double z) {
		super(x, z, height, img_index);
	}
	public WinnerBox(double x, double z, double origin) {
		super(x, z, height, img_index, origin);
	}
	
	/** Modified draw-on method */
	@Override
	public void drawOn(RaceCar car, Graphics g) {
		if(!car.isReadyForWinner()){
			//Nothing shown if ready-to-win.
			super.drawOn(car, g);
		}
	}
	
	/** WinnerBox is not destroyable. */
	@Override
	public boolean isDestroyable() {
		return false;
	}
	@Override
	public double destroyStuff() {
		return 0;
	}
	
}
