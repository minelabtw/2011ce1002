/**
 * The ActionListener of Timer used in RaceCar to run the main part of game.
 */
package fp.s100502514.metroArmy;

import java.awt.event.*;
import java.util.GregorianCalendar;

class RaceCarRunningListener implements ActionListener {
	
	private RaceCar target;
	public RaceCarRunningListener(RaceCar target){
		this.target = target;
	}
	
	//Refresh the game status here to get the difference if the status is newly changed.
	private GameStatus prevGameStatus = GameStatus.NOT_STARTED;
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		//System.out.println("RUNNING XD");
		
		//Refresh delay first
		long newUTC = new GregorianCalendar().getTimeInMillis();
		target.delay = (int)(newUTC-target.currentUTC);
		target.currentUTC = new GregorianCalendar().getTimeInMillis();
		
		if(target.isPaused()){
			target.repaint();
			return;//Omits every action below if game is paused.
		}
		
		//Initialize or Change the time when_nextEarthquake.
		if(target.when_nextEarthquake<target.getStationTimer()){
			if(target.when_nextEarthquake>=0){
				//Trigger an earthquake.
				double richter = Math.random()*3+3.2;
				//System.out.println("Quake M"+richter);
				target.setEarthquake(richter);
			}
			target.when_nextEarthquake = (long)(target.getStationTimer()+
					1000*(Math.random()*240+60));
			//System.out.println("Next Earthquake at "+target.when_nextEarthquake);
		}
		
		//boolean crashed = false; //Defined as a private member.
		
		//Shake the ground if earthquake.
		target.earthquake_speedY -= target.earthquake_offsetY*0.4;
		target.earthquake_offsetY += target.earthquake_speedY*(target.delay/100.0);
		target.cameraElavation = target.defaultCameraElavation+target.earthquake_offsetY;
		
		target.earthquake_speedX -= target.earthquake_offsetX*0.4;
		target.earthquake_offsetX += target.earthquake_speedX*(target.delay/100.0);
		
		//Determine if car needs to stop when shaking.
		if(RaceCar.getDistance(target.earthquake_speedX, target.earthquake_speedY)>
				Math.exp(2-8)/*M2*/){
			target.earthquake_needToStopCar = true;
			target.earthquake_alert_stop_timer = 2000;
		}else if(target.earthquake_alert_stop_timer>0){
			target.earthquake_alert_stop_timer -= target.delay;
		}else{
			target.earthquake_needToStopCar = false;
		}
		
		//Decrease the shaking intensity by time.
		if(target.earthquake_timer>0){
			target.earthquake_timer -= target.delay;
			target.earthquake_speedX *= Math.pow(0.99, (double)(target.delay)/100);
			target.earthquake_speedY *= Math.pow(0.99, (double)(target.delay)/100);
		}else{
			target.earthquake_timer = 0;
			target.earthquake_speedX *= Math.pow(0.2, (double)(target.delay)/100);
			target.earthquake_speedY *= Math.pow(0.2, (double)(target.delay)/100);
		}
		
		
		//Determined if game-over or winner.
		if(target.isAlreadyWinner()){
			//Do nothing with car since it stopped.
			//But stop the earthquake timer instead.
			target.earthquake_timer = 0;
			//Also, release the mouse-down action.
			target.mousePressed = false;
		}else if(target.crashed){ /** The car will lose control if crashed. */
			target.currentSpeed *= Math.pow(0.96, (double)(target.delay)/100);
		}else if(/*!stopped ||*/ ((target.currentSpeed>0)==(target.isGoingRight()))){
			target.currentSpeed += target.acceleration*(target.delay/40.0);
			//To make sure braking is fine.
		}else{
			//If the speed meets zero, then stop the car.
			target.currentSpeed = target.acceleration = 0;
			//stopped = true;
		}
		
		//Do action if different state changes
		GameStatus currentGameStatus = target.getGameStatus();
		if(currentGameStatus != prevGameStatus){
			//System.out.println(currentGameStatus);
			if(currentGameStatus == GameStatus.WINNER){
				if(prevGameStatus == GameStatus.DERAIL){
					//checkDerailOrFlipOver();
				}
			}
			prevGameStatus = currentGameStatus;
		}
		
		
		if(!target.isGameOver() && target.checkDerailOrFlipOver(target.delay)){
			/** checking will also refresh the derail-record. */
			//TODO Brake while derail-period.
			double tilt_accel = 0.001;
			if(target.isGoingLeft())tilt_accel*=-1;
			target.currentSpeed = (target.currentSpeed-tilt_accel*(target.delay/40.0)) * 0.96;
		}
		
		target.currentPosition += (target.currentSpeed/
				target.getCarWeight())*(target.delay/40.0);
		
		if(!(target.isGoingRight() && target.currentSpeed>0) &&
				!(target.isGoingLeft() && target.currentSpeed<0)){
			target.currentSpeed = target.acceleration = 0;
		}
		
		
		//If the car goes out of boundaries and it will be crashed.
		if(target.isGoingLeft() && target.currentPosition < target.sceneryLeftBound-20){
			target.currentPosition = target.sceneryLeftBound-20;
			//currentSpeed = acceleration = 0;
			target.currentSpeed *= -0.2;
			target.setGameOver();
			target.goRight();
		}else if(target.isGoingRight() && target.currentPosition > target.sceneryRightBound+20){
			target.currentPosition = target.sceneryRightBound+20;
			//currentSpeed = acceleration = 0;
			target.currentSpeed *= -0.2;
			target.setGameOver();
			target.goLeft();
		}
		
		if(!target.isGameOver() && target.mousePressed){
			if(target.mouseHitTarget instanceof Person){
				if(target.getOn((Person)target.mouseHitTarget)){
					/* TODO sounds (?) */
				}
			}else if(target.mouseHitTarget instanceof WinnerBox){
				if(!target.isReadyForWinner()){
					target.setToBeWinner();
					/* TODO remove box sound (?) */
				}
			}
		}
		
		//Decrease the derail number.
		if(!target.isGameOver()){
			target.decreaseDerailNumberByTimer(target.delay);
		}
		
		//Repaint after every event.
		target.repaint();
		
	}

}
