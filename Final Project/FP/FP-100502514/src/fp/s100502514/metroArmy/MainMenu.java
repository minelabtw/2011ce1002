/** !!!Notice!!! This class is currently NOT-IN-USED but will be probably used finally. */
package fp.s100502514.metroArmy;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class MainMenu extends JApplet {
	
	/** TODO TESTING */
	
	/** UID */
	private static final long serialVersionUID = 1L;
	
	JPanel panel;
	
	private boolean is_dragging = false;
	
	public MainMenu(){
		add(panel = new JPanel(){

			/** Inner UID */
			private static final long serialVersionUID = 1L;
			
			@Override
			protected void paintComponent(Graphics g){
				super.paintComponent(g);
				if(is_dragging)g.setColor(Color.red);
				else g.setColor(Color.pink);
				try{
					g.drawString("("+getMousePosition().x+","+getMousePosition().y+")", 10, 10);
				}catch(NullPointerException err){
					g.drawString("( -- , -- )", 10, 10);
				}
			}
			
		});
		
		panel.setBackground(Color.black);
		
		panel.addMouseMotionListener(new MouseAdapter(){
			@Override
			public void mouseDragged(MouseEvent e) {
				// TODO Auto-generated method stub
				is_dragging = true;
				repaint();
			}
			@Override
			public void mouseMoved(MouseEvent e) {
				// TODO Auto-generated method stub
				is_dragging = false;
				repaint();
			}
		});
	}
	
	public static void main(String[] args){
		/*JFrame win = new JFrame();
		MainMenu app = new MainMenu();
		app.init();
		win.add(app);
		win.setSize(1000,800);
		win.setLocationRelativeTo(null);
		win.setTitle("Metro Army Game (under testing)");
		win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);*/
		/*win.addWindowStateListener(new WindowStateListener(){

			@Override
			public void windowStateChanged(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});*/
		BasicWindow win = new BasicWindow(new MainMenu());
		win.setVisible(true);
	}

}
