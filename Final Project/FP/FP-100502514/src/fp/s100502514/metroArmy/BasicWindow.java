package fp.s100502514.metroArmy;

import javax.swing.*;

public class BasicWindow extends JFrame {
	/** UID */
	private static final long serialVersionUID = 1L;
	
	private JApplet content;
	public final JApplet getInnerApplet(){
		return content;
	}
	
	public BasicWindow(JApplet app, String title){
		content = app;
		app.init();
		add(app);
		setSize(1000, 800);
		setLocationRelativeTo(null);
		setTitle(title);
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public BasicWindow(JApplet app){
		this(app, "Metro Survival Game");
	}
}
