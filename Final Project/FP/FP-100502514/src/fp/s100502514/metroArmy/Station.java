package fp.s100502514.metroArmy;

import java.awt.*;
import java.util.Random;

import fp.s100502514.borrowed.a4.InnerTriangle2D;

/** class Station: this class can also be the "ground" or/and "ceiling". */
public class Station extends Drawable{
	double /*x,*/ width, depth, ceiling, startZ; //height is not-in-use.
	String name; //Station name
	static final double defaultWidth = 150;
	private Color groundColor = null;
	public Color getGroundColor(){
		return groundColor;
	}
	public void setGroundColor(Color groundColor){
		this.groundColor = groundColor;
	}
	public Station setColorAndGetStation(Color groundColor){
		setGroundColor(groundColor);
		return this;
	}
	
	/** Constructor implementations */
	public Station(double x, double width, double depth, double ceiling, double startZ,
			String name){
		this.x = x;
		this.width = width;
		this.depth = depth;
		this.ceiling = ceiling;
		this.startZ = startZ;
		this.name = name;
	}
	public Station(double x, double width, double depth, double ceiling, String name){
		this(x, width, depth, ceiling, 0, name);
	}
	public Station(double x, double width, double depth, String name){
		this(x, width, depth, 2.7, name);
	}
	public Station(double x, double width, String name){
		this(x, width, 5, 2.7, name);
	}
	public Station(double x, String name){
		this(x, defaultWidth, 5, 2.7, name);
	}
	public Station(double x, double width, double depth, double ceiling){
		this(x, width, depth, ceiling, null);
	}
	public Station(double x, double width, double depth){
		this(x, width, depth, null);
	}
	public Station(double x, double width){
		this(x, width, null);
	}
	public Station(double x){
		this(x, null);
	}
	/* Clone of Stations */
	public Station(Station st){
		this(st.x, st.width, st.depth, st.ceiling, st.startZ, st.name);
	}
	@Override
	public Station clone(){
		return new Station(this);
	}
	
	/** private variables for saving the attributes of this while painting,
	 *  and for hitTest() use. */
	private Point[][] tmp_poly = new Point[2][4];
	
	@Override
	public double getZ(){
		return (depth+startZ)*RaceCar.scaleRatio;
	}
	
	@Override
	public void drawOn(RaceCar car, Graphics g) {
		int centerX = car.getWidth()/2, centerY = car.getHeight()/2;
		Station st = this;
		for(int h=0; h<=1; h++){ //h=1 for ceiling
			double startZ = this.startZ;
			if(startZ<0.3)startZ = 0.3;
			double x_3d = (st.x-car.getCurrentPosition())/RaceCar.scaleRatio,
					y_3d = -car.getCameraElavation()+h*st.ceiling,
					z_3d = startZ;
			int[] px_arr = new int[4], py_arr = new int[4];
			
			boolean tmp_max_min_compared = false;
			
			for(double m=0; m*RaceCar.scaleRatio<st.width; m++){
				Point[] tmp_p_arr = new Point[4];
				for(int i=0; i<4; i++){
					tmp_p_arr[i] = new Point(
						px_arr[i] = (int)(((x_3d+m+(i/2)*1)
								/(z_3d+((i+1)/2%2)*st.depth))*car.getCameraSize()+centerX),
						py_arr[i] = (int)((-(y_3d)
								/(z_3d+((i+1)/2%2)*st.depth))*car.getCameraSize()+centerY)
					);
				}
				
				if(!tmp_max_min_compared){
					tmp_poly[h] = tmp_p_arr;
					tmp_max_min_compared = true;
				}else{
					//Far-right edge
					tmp_poly[h][2] = tmp_p_arr[2];
					//Near-right edge
					tmp_poly[h][3] = tmp_p_arr[3];
				}
				
				if(name!=null){
					g.setColor(new Color((int)(153+30.0*(m%3)/3), (int)(153+30.0*(m%4)/4),
							(int)(170+30.0*(m%5)/5)));
				}else if(getGroundColor() != null){
					int cr = getGroundColor().getRed()+
							(int)(new Random((long)m+100000).nextInt(21)-10);
					int cg = getGroundColor().getGreen()+
							(int)(new Random((long)m+200000).nextInt(21)-10);
					int cb = getGroundColor().getBlue()+
							(int)(new Random((long)m+300000).nextInt(21)-10);
					cr = cr>=0 ? (cr<=255 ? cr : 255) : 0;
					cg = cg>=0 ? (cg<=255 ? cg : 255) : 0;
					cb = cb>=0 ? (cb<=255 ? cb : 255) : 0;
					g.setColor(new Color(cr, cg, cb));
					
				}else{
					g.setColor(new Color((int)(102+10.0*(m%3)/3), (int)(102+10.0*(m%4)/4),
							(int)(127+10.0*(m%5)/5)));
				}
				g.fillPolygon(px_arr, py_arr, 4);
			}
		}
	}
	
	/** Determine if the mouse overlaps the rectangle sensor-zone of this object. */
	@Override
	public boolean hitTest(Point mouse) {
		if(mouse==null)return false;
		try{
			//Borrow Assignment 4-2's InnerTraingle2D for hitTest.
			InnerTriangle2D floor1 = new InnerTriangle2D(
					tmp_poly[0][0], tmp_poly[0][1], tmp_poly[0][2]);
			InnerTriangle2D floor2 = new InnerTriangle2D(
					tmp_poly[0][3], tmp_poly[0][1], tmp_poly[0][2]);
			InnerTriangle2D roof1 = new InnerTriangle2D(
					tmp_poly[1][0], tmp_poly[1][1], tmp_poly[1][2]);
			InnerTriangle2D roof2 = new InnerTriangle2D(
					tmp_poly[1][3], tmp_poly[1][1], tmp_poly[1][2]);
			if(roof1.contains(mouse) || roof2.contains(mouse)){
				//System.out.println("Hit Station Roof");
				return true;
			}else if(floor1.contains(mouse) || floor2.contains(mouse)){
				//System.out.println("Hit Station Floor");
				return true;
			}
			return false;
		}catch(Exception anyError){// Probably for NullPointerException.
			return false;
		}
	}

	/** Station is not destroyable. */
	@Override
	public boolean isDestroyable() {
		return false;
	}
	@Override
	public double destroyStuff() {
		return 0;
	}
	
}