/**
 * The MouseListener used in RaceCar of the main part of game.
 * Here implements all the required mouse events in the game.
 */
package fp.s100502514.metroArmy;

import java.awt.event.*;

public class RaceCarMouseListener extends MouseAdapter {
	
	private RaceCar target;
	public RaceCarMouseListener(RaceCar target){
		this.target = target;
	}
	
	@Override
	public void mousePressed(MouseEvent e){
		//TODO Mouse Down
		if(e.getButton()==MouseEvent.BUTTON1){//Left button
			if(target.isPaused()){
				target.getController().resumeGame();
			}else if(!(target.isAlreadyWinner() || target.isGameOver())){
				target.mousePressed = true;
				
				/** If 3rd-fast-click on a scenery then it'll explode. */
				if(e.getClickCount()==3 && target.mouseHitTarget instanceof Scenery){
					double stuff = ((Scenery)target.mouseHitTarget).destroyStuff()*
							(1+Math.abs(target.getCurrentSpeed()));
					target.makeDerailDamage(stuff);
					//System.err.println(stuff);
				}
			}
		}else if(e.getButton()==MouseEvent.BUTTON2){//Middle button
			//Secret Skill: Make Earthquake
			if(!(target.isPaused() || target.isAlreadyWinner() || target.isGameOver())
					&& e.getClickCount()>3){
				target.setEarthquake(e.getClickCount()-1);
			}
		}else if(e.getButton()==MouseEvent.BUTTON3){//Right button
			if(!(target.isPaused() || target.isAlreadyWinner() || target.isGameOver())){
				int amount = (int)(target.getNumPassengers()*0.25+5*(Math.random()*2-1));
				if(amount<1)amount=1;
				target.getOff(amount);
			}
		}
	}
	@Override
	public void mouseReleased(MouseEvent e){//Right button
		//TODO Mouse Up
		if(e.getButton()==MouseEvent.BUTTON1){
			target.mousePressed = false;
		}
	}
	@Override
	public void mouseMoved(MouseEvent e){
		//TODO Mouse Moving (not dragging)
	}
	@Override
	public void mouseDragged(MouseEvent e){
		//TODO Mouse Dragging
	}
}
