package fp.s100502514.metroArmy;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class GameControlPanel extends JPanel {

	/** UID */
	private static final long serialVersionUID = 1L;
	
	/** The speed-panel image to be drawn on RaceCar */
	private ImageIcon speed_icon = new ImageIcon(this.getClass().getResource(
			"images/speed_panel.png"));
	//The following 2 points are pre-measured in Adobe Flash, which was used to draw the image.
	private final Point passenger_measure_center = new Point(175, 160);
	private final Point speed_measure_center = new Point(299, 199);
	public void drawSpeedPanelOn(Graphics g, int x, int y, int station_timer,
			float xalign, float yalign){
		//Adjust position by alignment
		int newx = (int)(x-speed_icon.getIconWidth()*xalign);
		int newy = (int)(y-speed_icon.getIconHeight()*yalign);
		
		//Draw background image
		g.drawImage(speed_icon.getImage(), newx, newy, target);
		
		//Draw the timer on the icon
		Font icon_timer_font = new Font("Courier New", Font.BOLD, 30);
		String icon_timer_str = station_timer/60000+"\'"+
				(100+station_timer/1000%60+"").substring(1)+"\""+
				(1000+station_timer%1000+"").substring(1);
		// The use like (1000+n+"").substring(1) is to make sure that three digits display.
		g.setColor(new Color(0, 255, 204));
		g.setFont(icon_timer_font);
		g.drawString(icon_timer_str, newx+210-g.getFontMetrics(icon_timer_font).
				stringWidth(icon_timer_str), newy+320);
		
		//Draw needles
		Point p_center = new Point(passenger_measure_center);
		p_center.translate(newx, newy);
		drawNeedle(g, new Color(255, 255, 0), p_center, target.getNumPassengers());
		Point s_center = new Point(speed_measure_center);
		s_center.translate(newx, newy);
		drawNeedle(g, new Color(255, 153, 102), s_center, target.getSpeedInKPH());
	}
	public void drawSpeedPanelOn(Graphics g, int x, int y, int station_timer, float xalign){
		drawSpeedPanelOn(g, x, y, station_timer, xalign, JComponent.TOP_ALIGNMENT);
	}
	public void drawSpeedPanelOn(Graphics g, int x, int y, int station_timer){
		drawSpeedPanelOn(g, x, y, station_timer, JComponent.LEFT_ALIGNMENT);
	}
	private void drawNeedle(Graphics g, Color color, Point center, double angleDegree){
		double radian = (-150-angleDegree)*Math.PI/180;
		int cx = center.x, cy = center.y;
		int[] xs = {cx+(int)(Math.cos(radian-0.1)*20), cx+(int)(Math.cos(radian+0.1)*20),
				cx+(int)(Math.cos(radian)*110)};
		int[] ys = {cy-(int)(Math.sin(radian-0.1)*20), cy-(int)(Math.sin(radian+0.1)*20),
				cy-(int)(Math.sin(radian)*110)};
		g.setColor(color);
		g.fillPolygon(xs, ys, 3);
	}
	
	/** Testing components */
	private Color default_bgcolor = new Color(51, 0, 127);
	JButton quake_btn = new JButton("地震測試");
	JSlider intensity_bar = new JSlider();
	
	JLabel num_passengers_label = new JLabel();
	public void setDisplayNumPassenger(int n){
		if(n<=0){
			num_passengers_label.setText("車上沒有乘客");
			num_passengers_label.setForeground(new Color(102, 127, 224));
		}else{
			num_passengers_label.setText(n+"位乘客在車上");
			if(n<20){
				num_passengers_label.setForeground(new Color(102, 153, 255));
			}else if(n<40){
				num_passengers_label.setForeground(new Color(102, 224, 236));
			}else if(n<60){
				num_passengers_label.setForeground(new Color(102, 255, 170));
			}else if(n<80){
				num_passengers_label.setForeground(new Color(153, 255, 102));
			}else if(n<100){
				num_passengers_label.setForeground(new Color(204, 255, 85));
			}else if(n<120){
				num_passengers_label.setForeground(new Color(255, 240, 0));
			}else if(n<140){
				num_passengers_label.setForeground(new Color(255, 204, 0));
			}else if(n<160){
				num_passengers_label.setForeground(new Color(255, 127, 0));
			}else{
				num_passengers_label.setForeground(new Color(255, 0, 0));
			}
		}
	}
	
	JButton pause_btn = new JButton("遊戲暫停");
	
	private RaceCar target;
	public RaceCar setTarget(RaceCar target){
		return this.target = target;
	}
	public RaceCar getTarget(){
		return target;
	}
	private MetroArmy mainController;
	protected MetroArmy setController(MetroArmy mainController){
		return this.mainController = mainController;
	}
	public MetroArmy getController(){
		return mainController;
	}
	
	public GameControlPanel(MetroArmy mainController, RaceCar target){
		super();
		setBackground(default_bgcolor);
		setController(mainController);
		setTarget(target);
		initialize();
	}
	public GameControlPanel(MetroArmy mainController){
		this(mainController, null);
	}
	
	protected void initialize(){
		setLayout(new BorderLayout());
		JPanel board = new JPanel(new GridLayout(3, 1, 5, 5));
		board.setBackground(default_bgcolor);
		
		intensity_bar.setMaximum(7);
		intensity_bar.setMinimum(2);
		intensity_bar.setPaintTicks(true);
		intensity_bar.setPaintLabels(true);
		intensity_bar.setMajorTickSpacing(1);
		intensity_bar.setMinorTickSpacing(1);
		intensity_bar.setValue(4);
		
		board.add(quake_btn);
		board.add(intensity_bar);
		
		quake_btn.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				target.setEarthquake(intensity_bar.getValue());
			}
		});
		
		num_passengers_label.setFont(new Font("微軟正黑體", Font.BOLD, 20));
		num_passengers_label.setHorizontalAlignment(JLabel.RIGHT);
		board.add(num_passengers_label);

		add(board, BorderLayout.NORTH);
		
		pause_btn.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if(getController().isPaused()){
					getController().resumeGame();
					pause_btn.setText("遊戲暫停");
				}else{
					getController().pauseGame();
					pause_btn.setText("繼續遊戲");
				}
			}
		});

		add(pause_btn, BorderLayout.SOUTH);
		
	}
	
}
