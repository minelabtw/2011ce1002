/** FinalProject MetroArmy Game: Most of the position of sceneries are defined here. */
package fp.s100502514.metroArmy;

import java.awt.Color;

class GameSceneData {
	
	/** Each constructor creates a "database" for the sceneries. */
	public GameSceneData(){
		initialize();
	}
	
	//Set the image URL strings before initialization of RaceCar.
	//Also called by class RaceCar in static form.
	static final String[] PersonURLs = {
			"images/woman1.png",
			"images/woman2.png",
			"images/man1.png",
			"images/man2.png"
	};
	static final String[] SceneryURLs = {
			"images/building1.png",
			"images/tower1.png",
			"images/tree.png",
			"images/museum.png",
			"images/winner_box.png"
	};
	static final String[] CursorURLs = {
		"images/cursor_star_1.png",
		"images/cursor_star_2.png",
		"images/starpress.png"
	};
	static final String[] StarPressURLs = {
		"images/starpress.png",
		"images/cursor_star_1.png",
		"images/cursor_star_2.png"
	};
	
	public static Station[] getStationOrderInfo(){
		return new Station[]{
			new Station(0, "都會總站"),
			new Station(580, 40, 3, "南門"),
			new Station(1560, 80, "南方之城"),
			new Station(2380, 40, 3, "摩天鐵塔"),
			new Station(2980, 40, 3, "社區公園"),
			new Station(3560, 80, "博物館"),
			new Station(5250, 500, 8, "高鐵站"),
			new Station(12380, 40, 3, "荒野")/*Under Construction*/,
			new Station(13180, 80, "競技場")/*Under Construction*/,
			new Station(15980, 40, 3, "莊園")/*Under Construction*/,
			new Station(RaceCar.sceneryLoopUnit-Station.defaultWidth,
					"田園機場")/*Under Construction*/
		};
	}
	
	//List of Stations
	Station[] stations;
	
	//List of Walls
	Wall[] walls;
	
	//List of all other sceneries (expect people, which are randomly-generated.)
	Drawable[] scenery;
	
	private void initialize(){
		//List of Stations
		stations = getStationOrderInfo();
		
		//List of Walls
		walls = new Wall[]{
			new Wall(20, 2, 30, stations[0].name),
			new Wall(25, 3, 20),
			new Wall(100, 2, 30, stations[0].name),
			new Wall(105, 3, 20),
			
			//TUNNEL starts
			new Wall(-60, 1, 40),
			new Wall(-55, 1, 35),
			new Wall(-20, 1, 5, "違者嚴懲"),
			new Wall(-15, 1, 5, "嚴禁進入"),
			new Wall(-10, 1, 5, "機房重地"),
			new Wall(-5, 1, 5),
			new Wall(-70, 5, 950),
			new Wall(-65, 5, 950),
			new Wall(520, 3, 360),
			new Wall(525, 3, 360),
			
			new Wall(150, 1, 430),
			new Wall(155, 1, 425),
			
			new Wall(620, 1, 240),
			new Wall(625, 1, 235),
			
			new Wall(885, 3, 60),
			new Wall(890, 5, 100),
			//TUNNEL ends
			
			new Wall(590, 2, 20, stations[1].name),
			
			new Wall(1570, 2, 20, stations[2].name),
			new Wall(1610, 2, 20, stations[2].name),
			
			new Wall(2390, 2, 20, stations[3].name),
			
			new Wall(2990, 2, 20, stations[4].name),
	
			new Wall(3570, 2, 20, stations[5].name),
			new Wall(3610, 2, 20, stations[5].name),
			
			new Wall(5270, 2, 30, stations[6].name),
			new Wall(5275, 3, 20),
			new Wall(5350, 2, 30, stations[6].name),
			new Wall(5355, 3, 20),
			new Wall(5448, 3, 4, stations[6].name),
			new Wall(5497, 2, 2, stations[6].name),
			new Wall(5501, 2, 2, stations[6].name),
			new Wall(5548, 3, 4, stations[6].name),
			new Wall(5620, 2, 30, stations[6].name),
			new Wall(5625, 3, 20),
			new Wall(5700, 2, 30, stations[6].name),
			new Wall(5705, 3, 20),
			
			new Wall(5355, 8, 295),
	
			//The last wall (in current scenery-contained spots)
			new Wall(5750, 1, 5, 1.4),
			new Wall(5755, 1, 5, "施工不便"),
			new Wall(5760, 1, 5, "敬請見諒"),
			new Wall(5765, 1, 80, 1.4),
			new Wall(5770, 1, 75, 1.4),
			
			//TUNNEL starts
			new Wall(6200, 1, 105),
			new Wall(6205, 1.2, 100),
			new Wall(6200, 2.4, 105),
			new Wall(6205, 2.4, 100),
	
			new Wall(6800, 1, 4505),
			new Wall(6805, 1, 4500),
			new Wall(7300, 2.4, 3505),
			new Wall(7305, 2.4, 3500),
			//TUNNEL ends
			
			new Wall(12390, 2, 20, stations[7].name),
	
			new Wall(13190, 2, 20, stations[8].name),
			new Wall(13230, 2, 20, stations[8].name),
			
			new Wall(15990, 2, 20, stations[9].name),
			
			new Wall(17870, 2, 30, stations[10].name),
			new Wall(17875, 3, 20),
			new Wall(17950, 2, 30, stations[10].name),
			new Wall(17955, 3, 20),
			
			//The last wall
			new Wall(18000, 1, 30, 1.4),
			new Wall(18005, 1, 25, 1.4)
		};
		
		//List of all other sceneries (expect people, which are randomly-generated.)
		scenery = new Drawable[]{
			//new Person(3000, 2000, 800, 0, 0),
			//new Person(4000, 2000, 1680, 0, 0),
			new Scenery(3210, 2000, 60, 3, 1.0),
			new Scenery(2880, 2000, 150, 1, 1.0),
			new Station(-80, 1000, 7, null),
			
			new Station(700, 1350/*990*/, 5, 0, 10, null)/*road*/,
			new Station(1000, 555, 3, 0, 4, null)/*road*/,
			new Station(1670, 20, 3, 2.4, 7, ""),
			new Station(1670, 20, 3, 2.4, 15, ""),
			new Wall(1672.5, 8.5, 15, 2.4, "公車總站"),
			new Wall(1672.5, 16.5, 15, 2.4, "公車總站"),
			
			new Station(6200, 105, 7, null),
			new Station(6850, 4405, 7, null),
			
			new Scenery(850, 9, 15, 0, 1.0),
			new Scenery(920, 16, 15, 0, 1.0),
			new Scenery(970, 9, 15, 0, 1.0),
			new Scenery(1100, 9, 15, 0, 1.0),
			new Scenery(1140, 18, 15, 0, 1.0),
			new Scenery(1210, 16, 15, 0, 1.0),
			new Scenery(1290, 9, 15, 0, 1.0),
			new Scenery(1400, 16, 15, 0, 1.0),
			new Scenery(1420, 3, 15, 0, 1.0),
			new Scenery(1520, 16, 15, 0, 1.0),
			new Scenery(1550, 9, 15, 0, 1.0),
			new Scenery(1720, 16, 15, 0, 1.0),
			new Scenery(1780, 9, 15, 0, 1.0),
			new Scenery(1820, 16, 15, 0, 1.0),
			new Scenery(1870, 9, 15, 0, 1.0),
			new Scenery(1920, 16, 15, 0, 1.0),
			new Scenery(1970, 9, 15, 0, 1.0),
			new Scenery(1970, 23, 15, 0, 1.0),
			new Scenery(2020, 9, 15, 0, 1.0),
			
			new Scenery(2280, 35, 150, 1, 1.0)/*tower*/,
			new Station(2050, 600, 60, 0, 10, "")/*plaza*/,
			
			//new Scenery(2030, 16, 6, 2, 1.0),
			new Scenery(2030, 10, 6, 2, 1.0),
			new Scenery(2030, 16, 6, 2, 1.0),
			new Scenery(2030, 22, 6, 2, 1.0),
			new Scenery(2030, 28, 6, 2, 1.0),
			new Scenery(2030, 34, 6, 2, 1.0),
			new Scenery(2050, 35, 8, 2, 1.0),
			new Scenery(2080, 35, 8, 2, 1.0),
			new Scenery(2110, 35, 8, 2, 1.0),
			new Scenery(2140, 36, 8, 2, 1.0),
			new Scenery(2170, 35, 8, 2, 1.0),
			new Scenery(2220, 40, 10, 2, 1.0),
			
			new Station(2460, 200, 4, 0, 2.5, null)
				.setColorAndGetStation(new Color(153, 153, 160)),
			new Station(2660, 295, 5.5, 0, 1, null)
				.setColorAndGetStation(new Color(51, 153, 0)),
			new Station(2960, 80, 25, 0, 4, null)
				.setColorAndGetStation(new Color(102, 204, 0)),
				
			new Station(3042, 6, 60, 0, 0.3, null)
				.setColorAndGetStation(new Color(191, 160, 153)),
			new Station(3042, 130, 6, 0, 60, null)
				.setColorAndGetStation(new Color(191, 160, 153)),
			new Station(3172, 6, 55, 0, 5, null)
				.setColorAndGetStation(new Color(191, 160, 153)),
			new Station(3172, 468, 2, 0, 6, null)
				.setColorAndGetStation(new Color(191, 160, 153)),
			new Station(3172, 400, 4, 0, 12, null)
				.setColorAndGetStation(new Color(191, 160, 153)),
			
			new Station(3050, 100, 23, 0, 6, null)
				.setColorAndGetStation(new Color(191, 127, 0)),
			new Station(3050, 500, 5, 0, 1, null)
				.setColorAndGetStation(new Color(51, 153, 0)),
			new Station(3650, 1590, 5, 0, 1, null)
				.setColorAndGetStation(new Color(51, 153, 0)),
			
			new Scenery(2750, 5, 6, 2, 1.0),
			new Scenery(2770, 6, 6, 2, 1.0),
			new Scenery(2790, 5, 7, 2, 1.0),
			new Scenery(2810, 4, 6, 2, 1.0),
			new Scenery(2830, 5, 4, 2, 1.0),
			new Scenery(2850, 6, 6, 2, 1.0),
			new Scenery(2870, 5, 4, 2, 1.0),
			new Scenery(2890, 4, 5, 2, 1.0),
			new Scenery(2910, 5, 6, 2, 1.0),
			new Scenery(2930, 6, 9, 2, 1.0),
			new Scenery(2950, 5, 6, 2, 1.0),
			new Scenery(2960, 16, 6, 2, 1.0),
			new Scenery(2970, 26, 6, 2, 1.0),
			new Scenery(2990, 26, 6, 2, 1.0),
			new Scenery(3000, 35, 20, 2, 1.0),
			new Scenery(3010, 26, 6, 2, 1.0),
			new Scenery(3030, 26, 6, 2, 1.0),
			new Scenery(3030, 16, 6, 2, 1.0),
			new Scenery(3050, 6, 6, 2, 1.0),
			new Scenery(3070, 7, 8, 2, 1.0),
			new Scenery(3090, 6, 6, 2, 1.0),
			new Scenery(3110, 5, 6, 2, 1.0),
			
			new Station(3270, 1830, 2.5, 3.6, 8.45, null)
				.setColorAndGetStation(new Color(204, 102, 51)),
			new Scenery(3170, 11, 60, 3, 1.0)/*museum*/
			/** TODO to be continued~~ */
		};
	}
}
