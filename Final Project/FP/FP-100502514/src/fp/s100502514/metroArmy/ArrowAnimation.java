package fp.s100502514.metroArmy;

import java.awt.*;

public class ArrowAnimation { // A lightning arrow that shows above the window
	
	int period; // period in ms
	
	private Color color; // Color object declaration
	
	public ArrowAnimation(int period, Color color){
		this.period = period;
		this.color = color;
	}
	public ArrowAnimation(int period){
		this(period, new Color(0, 255, 0, 153)); //lime green with 60% alpha
	}
	public ArrowAnimation(){
		this(2000);
	}
	
	public void setColor(Color _new){ // set a new color by class RaceCar
		color = _new;
	}
	public Color getColor(){ // get the arrow color
		return color;
	}
	
	private int direction; // 1 rightward or -1 leftward, or 0 stopped
	public int getDirection(){
		return direction;
	}
	
	public void refreshDirection(RaceCar ref){
		if(ref.isStopped()){
			direction = 0;
		}else if(ref.isGoingRight()){
			direction = 1;
		}else{
			direction = -1;
		}
	}
	
	//private final Station[] station_info = GameSceneData.getStationOrderInfo();
	
	public void drawOnGraphics(Graphics g, RaceCar ref){
		refreshDirection(ref); //Refresh the display via reading the direction
		g.setColor(getColor());
		
		/* station map drawing scope */{
			/*for(int i=0; i<station_info.length; i++){
				TODO [weekend] draw station map
			}*/
		}
		
		int cycleMovement = (int)(60*(ref.getStationTimer()%period)/period-30);
		int arrow_pos = ref.getWidth()/2;
		switch(getDirection()){
		case 1: //Rightward
			arrow_pos += cycleMovement;
			g.fillRect(arrow_pos, 30, 40, 20);
			g.fillPolygon(
					new int[]{arrow_pos+40, arrow_pos+40, arrow_pos+60},
					new int[]{20, 60, 40}, 3);
			break;
		case -1: //Leftward
			arrow_pos -= cycleMovement-40;
			g.fillRect(arrow_pos, 30, 40, 20);
			g.fillPolygon(
					new int[]{arrow_pos, arrow_pos, arrow_pos-20},
					new int[]{20, 60, 40}, 3);
			break;
		default: //Stopped: a square sign
			g.fillRect(arrow_pos+10, 20, 40, 40);
		}
		
		//Message text on the arrow
		String accel_msg = "";
		if(ref.isGameOver()){
			accel_msg = "故障";
		}else if(getDirection()==0){
			accel_msg = "停車";
		}else if(ref.isBoosting()){
			accel_msg = "加速";
		}else if(ref.isBraking()){
			accel_msg = "煞車";
		}else{
			accel_msg = "空檔";
		}
		Font accel_msg_font = new Font("微軟正黑體", Font.BOLD, 16);
		g.setColor(new Color(255, 255, 255));
		g.setFont(accel_msg_font);
		g.drawString(accel_msg,
				arrow_pos + ((getDirection()==0) ? 30 : getDirection()*10+20)
					-g.getFontMetrics(accel_msg_font).stringWidth(accel_msg)/2,
				45);
	}
}
