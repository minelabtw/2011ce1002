/** This Exception will be thrown if Arguments in RaceCar's constructor is wrong. */
package fp.s100502514.metroArmy;

public class InvalidRaceCarArgumentException extends Exception {

	/** UID */
	private static final long serialVersionUID = 1L;
	
	public InvalidRaceCarArgumentException(){
		this("");
	}
	public InvalidRaceCarArgumentException(String message){
		super(message);
	}
}
