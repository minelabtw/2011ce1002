/**
 * CE1002-100502514 FlashTeens Chiang
 * Final Project: Metro Army Game
 * 
 * Modified from CE1002 A111
 * The class for game-display, inherits from JApplet.
 */
package fp.s100502514.metroArmy;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class MetroArmy extends JApplet {

	/** UID */
	private static final long serialVersionUID = 1L;
	
	/** GUI members */
	RaceCar[] cars = new RaceCar[1];
	JPanel carGroup = new JPanel(new GridLayout(cars.length, 1, 5, 5));
	CarSlider[] controls_left = new CarSlider[cars.length];
	CarSlider[] controls_right = new CarSlider[cars.length];
	GameControlPanel east_control = new GameControlPanel(this);
	boolean[] left_dragging = new boolean[cars.length];
	boolean[] right_dragging = new boolean[cars.length];
	
	//Get the inner RaceCar object.
	public final RaceCar getRaceCar(){
		return cars[0];
	}
	
	Color[] color_options = {
			new Color(255, 0, 0, 153)/*red*/,
			new Color(255, 224, 0, 153)/*gold*/,
			new Color(0, 255, 0, 153)/*green*/,
			new Color(0, 0, 255, 153)/*blue*/,
			new Color(255, 102, 204, 153)/*pink*/
	};
	String[] color_labels = {"Red Devil", "Golden Age", "Freedom Express",
			"Cool Sapphire", "Pinky Gigantica"};
	JRadioButton[][] radioButtons = new JRadioButton[cars.length][color_labels.length];
	ButtonGroup[] radioBtnGroups = new ButtonGroup[cars.length];
	int defaultRadioSelection = 2;
	
	/** Timer Declaration */
	Timer mainTimer;
	
	/** RaceCar parameters */
	double begin_meter, end_meter, car_meter, winnerbox_meter;
	int user_delay_ms;
	
	/** Game-display constructor */
	public MetroArmy(double begin_meter, double end_meter, double car_meter,
			double winnerbox_meter, int user_delay_ms){
		this.begin_meter = begin_meter;
		this.end_meter = end_meter;
		this.car_meter = car_meter;
		this.winnerbox_meter = winnerbox_meter;
		this.user_delay_ms = user_delay_ms;
		/** init() will initialize this applet. */
	}
	public MetroArmy(double begin_meter, double end_meter, double car_meter,
			double winnerbox_meter){
		this(begin_meter, end_meter, car_meter, winnerbox_meter, RaceCar.TIMER_MEDIUM_QUALITY);
	}
	public MetroArmy(int user_delay_ms){
		this(0, 5750, 130, 5700, user_delay_ms);
		/** init() will initialize this applet. */
	}
	public MetroArmy(){
		this(RaceCar.TIMER_MEDIUM_QUALITY);
		/** init() will initialize this applet. */
	}
	public void init(){
		setLayout(new BorderLayout());
		/*for(int i=0; i<cars.length; i++)*/{
			
			/*  Originally in Assignment 11, i is between 0 and (cars.length-1);
				However there is only one element in the list cars[]. */
			final int i = 0;
			
			JPanel panelBoard = new JPanel(new BorderLayout());
			
			//Add car driver, which contains the scenery and acceleration bars.
			panelBoard.add(setAndGetNewCarDriver(user_delay_ms), BorderLayout.CENTER);
			
			//Add Radio buttons
			JPanel radioPanel = new JPanel(new GridLayout(color_labels.length, 1));
			radioBtnGroups[i] = new ButtonGroup();
			for(int j=0; j<color_labels.length; j++){
				//make a radio-button instance
				radioButtons[i][j] = new JRadioButton(color_labels[j]);
				//set the colors
				Color label_color = new Color(
						color_options[j].getRed(), color_options[j].getGreen(),
						color_options[j].getBlue(), 255
				);
				radioButtons[i][j].setForeground(label_color);
				radioButtons[i][j].setBackground(Color.black);
				//Add objects and listeners
				radioBtnGroups[i].add(radioButtons[i][j]);
				radioPanel.add(radioButtons[i][j]);
				radioButtons[i][j].addItemListener(radioListener);
			}
			radioButtons[i][defaultRadioSelection].setSelected(true);
			
			JPanel east_control_outer = new JPanel(new BorderLayout());
			east_control_outer.add(east_control, BorderLayout.CENTER);
			east_control_outer.add(radioPanel, BorderLayout.SOUTH);
			//panelBoard.add(east_control_outer, BorderLayout.EAST);
			east_control.setTarget(cars[0]);
			
			carGroup.add(panelBoard);
			
			cars[i].setAccel(0);
		}
		add(carGroup);
		
		/** Implementing Timer Here */
		mainTimer = new Timer(user_delay_ms, new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(isPaused())return;//Omits everything if game is paused.
				
				detectSlider(); //Refresh the slider button position.
				refreshGameControlPanelInformation();
				
				repaint(); //Repaint after every event.
				
			}
			
			private void detectSlider(){
				for(int i=0; i<cars.length; i++){
					
					if(cars[i].isGameOver()){
						//Do nothing if crashed.
						controls_left[i].setCrashed(true);
						controls_right[i].setCrashed(true);
						continue;
					}else if(cars[i].isAlreadyWinner()){
						//Do nothing if crashed.
						controls_left[i].setWinner(true);
						controls_right[i].setWinner(true);
						continue;
					}
					
					if(cars[i].isStopped()){
						controls_left[i].setEnabled(true);
						controls_right[i].setEnabled(true);
						if(!left_dragging[i])controls_left[i].setValue(0);
						if(!right_dragging[i])controls_right[i].setValue(0);
					}
					
					JSlider control;
					int direction;
					if(!controls_right[i].isEnabled()){
						control = controls_left[i];
						direction = -1;
					}else if(!controls_left[i].isEnabled()){
						control = controls_right[i];
						direction = 1;
					}else{
						continue;
					}
					
					//Let the sliders gradually turning back to zero after dragging.
					if(cars[i].isAlreadyWinner()){
						/* Do NOTHING if already wins. */
					}else if(!left_dragging[i] && !right_dragging[i]){
						double currentVal = control.getValue();
						double middleVal = 0;
						
						control.setValue(
							(int)(currentVal+Math.floor((middleVal-currentVal)/4)));
						
						// Turn the unused slider to zero
						if(cars[i].isGoingRight())controls_left[i].setValue(0); 
						else controls_right[i].setValue(0);
						
					}else{
						
						/** If found the direction of speed changes, then updates the data. */
						if(left_dragging[i] && cars[i].isGoingRight()){
							cars[i].goLeft();
						}else if(right_dragging[i] && cars[i].isGoingLeft()){
							cars[i].goRight();
						}
					}
					
					double halfRange = (control.getMaximum()-control.getMinimum())/2;
					cars[i].setAccel((double)(control.getValue()/halfRange)*0.002*direction);
					
				}
			}
			
			private void refreshGameControlPanelInformation(){
				east_control.setDisplayNumPassenger(cars[0].getNumPassengers());
				if(cars[0].isGameOver())east_control.pause_btn.setEnabled(false);
			}
			
		});
		mainTimer.start();
	}
	
	private JPanel setAndGetNewCarDriver(int delay_ms){
		
		/*  Originally in Assignment 11, i is between 0 and (cars.length-1);
			However there is only one element in the list cars[]. */
		final int i = 0;
		
		JPanel driver = new JPanel(new BorderLayout()); // inside panelBoard
		
		//Add display TODO set parameters
		try {
			/** TODO SET PARAMETERS: FOLLOWING 2 LINES */
			driver.add(cars[i] = new RaceCar(begin_meter, end_meter, car_meter, delay_ms),
					BorderLayout.CENTER);
			//cars[i].setCarTimerSettingDelay_ms(delay_ms);
			
			//cars[i].addWinnerBox(5500);
			int num_boxes = (int)Math.abs(end_meter-winnerbox_meter)/30+1;
			for(int j=0; j<num_boxes; j++){
				cars[i].addWinnerBox(winnerbox_meter+
						Math.random()*(end_meter-winnerbox_meter));
			}
			cars[i].setController(this);
			
		} catch (InvalidRaceCarArgumentException e) {
			try {
				driver.add(cars[i] = new RaceCar(), BorderLayout.CENTER);
				cars[i].setGameOver();
				JOptionPane.showMessageDialog(null, "參數設定錯誤，請關閉本視窗!!",
						"Metro Survival Game", JOptionPane.ERROR_MESSAGE);
			} catch (InvalidRaceCarArgumentException e1) {
				/* Program never runs here. */
			}
			//e.printStackTrace();
		}
		
		//Add sliders
		driver.add(controls_left[i] = new CarSlider(), BorderLayout.WEST);
		driver.add(controls_right[i] = new CarSlider(), BorderLayout.EAST);
		
		controls_left[i].addMouseListener(sliderListener);
		controls_right[i].addMouseListener(sliderListener);
		
		left_dragging[i] = right_dragging[i] = false;
		return driver;
	}
	
	MouseAdapter sliderListener = new MouseAdapter(){
		@Override
		public void mousePressed(MouseEvent e) {
			if(isPaused())return;//Omits everything if game is paused.
			
			for(int i=0; i<cars.length; i++){
				if(cars[i].isAlreadyWinner()){
					controls_left[i].setEnabled(true);
					controls_right[i].setEnabled(true);
					continue;
				}else if(cars[i].isGameOver()){
					//Do nothing if crashed.
					continue;
				}
				if(e.getSource() == controls_left[i] &&
						(cars[i].isGoingLeft() || cars[i].isStopped())){
					cars[i].goLeft();
					controls_left[i].setEnabled(true);
					controls_right[i].setEnabled(false);

					controls_right[i].setValue(0); // Turn the unused slider to zero
					
					left_dragging[i] = true;
					break;
				}else if(e.getSource() == controls_right[i] &&
						(cars[i].isGoingRight() || cars[i].isStopped())){
					cars[i].goRight();
					controls_left[i].setEnabled(false);
					controls_right[i].setEnabled(true);
					
					controls_left[i].setValue(0); // Turn the unused slider to zero
					
					right_dragging[i] = true;
					break;
				}
			}
			
			((JComponent)(e.getSource())).repaint(); //Repaint after every event.
			
		}
		@Override
		public void mouseReleased(MouseEvent e) {
			//Omits everything if game is paused or winner.
			if(isPaused()||cars[0].isAlreadyWinner())return;
			
			for(int i=0; i<cars.length; i++){
				if(e.getSource() == controls_left[i]){
					left_dragging[i] = false;
					if(cars[i].isStopped())controls_left[i].setValue(0);
					break;
				}else if(e.getSource() == controls_right[i]){
					right_dragging[i] = false;
					if(cars[i].isStopped())controls_right[i].setValue(0);
					break;
				}
			}
			
			((JComponent)(e.getSource())).repaint(); //Repaint after every event.
			
		}
	};
	
	ItemListener radioListener = new ItemListener(){
		@Override
		public void itemStateChanged(ItemEvent e) {
			for(int i=0; i<cars.length; i++){
				for(int j=0; j<color_labels.length; j++){
					if(e.getSource() == radioButtons[i][j]){
						cars[i].setColor(color_options[j]);
						return;
					}
				}
			}
			
			((JComponent)(e.getSource())).repaint(); //Repaint after every event.
			
		}
	};
	
	/** The methods from RaceCar for Pause of the game. */
	public boolean isPaused(){
		return cars[0].isPaused();
	}
	public void pauseGame(){
		if(cars[0].isPaused() || cars[0].isGameOver())return;
		cars[0].setPaused(true);
		controls_left[0].setEnabled(false);
		controls_right[0].setEnabled(false);
	}
	public void resumeGame(){
		if(!cars[0].isPaused() || cars[0].isGameOver())return;
		cars[0].setPaused(false);
		if(cars[0].isStopped()){
			controls_left[0].setEnabled(true);
			controls_right[0].setEnabled(true);
		}else{
			controls_left[0].setEnabled(cars[0].isGoingLeft());
			controls_right[0].setEnabled(cars[0].isGoingRight());
		}
	}
	
	public static void main(String[] args){
		BasicWindow win = new BasicWindow(new MetroArmy());
		win.setVisible(true);
	}
	
}
