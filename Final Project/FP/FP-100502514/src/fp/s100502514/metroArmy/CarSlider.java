package fp.s100502514.metroArmy;

import java.awt.*;
import java.util.*;

import javax.swing.*;

public class CarSlider extends JSlider {

	/** UID */
	private static final long serialVersionUID = 1L;
	private boolean crash_state = false;
	private boolean is_winner = false;
	
	public CarSlider(){
		super(JSlider.VERTICAL);
		setMaximum(600);
		setMinimum(-1000);
		setPreferredSize(new Dimension(35, getHeight()));
		setCrashed(false);
	}
	
	/** Setter and Getter to determine if the car is crashed. */
	public void setCrashed(boolean isCrashed){
		setEnabled(!isCrashed);
		crash_state = isCrashed;
	}
	public boolean isCrashed(){
		return crash_state;
	}
	@Deprecated
	public boolean getCrashed(){
		return isCrashed();
	}
	
	/** Setter and Getter to determine if the player won the game. */
	public boolean isWinner(){
		return is_winner;
	}
	public void setWinner(boolean state){
		is_winner = state;
	}
	
	/** Once the slider is winner-state, the value will not be used anymore. */
	/*@Override
	public int getValue(){
		if(isWinner()){
			return 0;
		}else{
			return super.getValue();
		}
	}
	@Override
	public void setValue(int val){
		super.setValue(val);
		if(isWinner()){
			try{
				throw new Exception("line trace test");
			}catch(Exception err){
				err.printStackTrace();
			}
		}
	}*/
	
	protected void paintComponent(Graphics g){
		
		if(isWinner()){
			int msg_timer = (int)(new GregorianCalendar().getTimeInMillis()%1600)/400;
			switch(msg_timer){
			case 0:
				setBackground(new Color(255, 102, 153));
				break;
			case 1:
				setBackground(new Color(224, 255, 102));
				break;
			case 2:
				setBackground(new Color(153, 236, 224));
				break;
			default:
				setBackground(new Color(236, 153, 255));
			}
		}else if(isEnabled()){
			Color bgcolor = new Color(240, 236, 0);
			if(getValue()>=0){
				try{
					setBackground(new Color(
						bgcolor.getRed()-getValue()/4, bgcolor.getGreen(), bgcolor.getBlue()));
				}catch(IllegalArgumentException ex){
					setBackground(new Color(
						bgcolor.getRed(), 255, bgcolor.getBlue()));
				}
				
			}else{
				try{
					setBackground(new Color(
						bgcolor.getRed(), bgcolor.getGreen()+getValue()/6, bgcolor.getBlue()));
				}catch(IllegalArgumentException ex){
					setBackground(new Color(
						255, bgcolor.getGreen(), bgcolor.getBlue()));
				}
			}
		}else if(isCrashed()){
			int period = 600; //ms
			int ms = (int)(new GregorianCalendar().getTimeInMillis()%period)-period/2;
			double color_steps = Math.abs(ms*255.0/period*2);
			if(color_steps>255){
				color_steps = 255;
			}
			setBackground(new Color(
				(int)(color_steps/2+127), (int)(color_steps/4+51), (int)(color_steps/5+64)
			));
		}else{
			setBackground(new Color(191, 191, 204));
		}
		
		super.paintComponent(g);
		
	}
	
}
