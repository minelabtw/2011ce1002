package fp.s100502514.metroArmy;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;

public class Wall extends Drawable{
	double /*x, z,*/ width/*, height*/;
	String label;
	static final double defaultWidth = 10;
	static final double defaultZPosition = 4;
	static final double defaultHeight = 2.7;
	public Wall(double x, double z, double width, double height, String label){
		this.x = x;
		this.z = z;
		this.width = width;
		this.height = height;
		this.label = label;
	}
	public Wall(double x, double z, double width, String label){
		this(x, z, width, defaultHeight, label);
	}
	public Wall(double x, double width, String label){
		this(x, defaultZPosition, width, defaultHeight, label);
	}
	public Wall(double x, String label){
		this(x, defaultZPosition, defaultWidth, defaultHeight, label);
	}
	public Wall(double x, double z, double width, double height){
		this(x, z, width, height, null);
	}
	public Wall(double x, double z, double width){
		this(x, z, width, null);
	}
	public Wall(double x, double width){
		this(x, width, null);
	}
	public Wall(double x){
		this(x, null);
	}
	
	/** private variables for saving the attributes of this while painting,
	 *  and for hitTest() use. */
	private int tmp_img_left = 0, tmp_img_top = 0, tmp_img_right = 0, tmp_img_bottom = 0,
			tmp_img_unit_width = 0;
	
	@Override
	public double getZ(){
		return z*RaceCar.scaleRatio;
	}
	
	@Override
	public void drawOn(RaceCar car, Graphics g) {
		// TODO Auto-generated method stub
		int centerX = car.getWidth()/2, centerY = car.getHeight()/2;
		
		Wall st = this;
		{
			double x_3d = st.x-car.getCurrentPosition(),
					y_3d = -car.getCameraElavation(), z_3d = st.z;
			int[] px_arr = new int[4], py_arr = new int[4];
			
			boolean tmp_max_min_compared = false;

			tmp_img_unit_width = (int)((5/RaceCar.scaleRatio/(z_3d))
					*car.getCameraSize());
			
			for(double m=0; m<st.width; m++){
				if(m%10>=5/* Exactly a half then it'll be accurate for hitTest. */){
					continue;
				}
				for(int i=0; i<2; i++){
					int tmp_x, tmp_y;
					tmp_x = px_arr[i] = px_arr[3-i] =
							(int)(((x_3d+m+i*1)/RaceCar.scaleRatio/(z_3d))
							*car.getCameraSize()+centerX);
					tmp_y = py_arr[i*2] = py_arr[i*2+1] =
							(int)((-(y_3d+i*st.height)/(z_3d))
							*car.getCameraSize()+centerY);
					if(!tmp_max_min_compared){
						tmp_img_left = tmp_img_right = tmp_x;
						tmp_img_top = tmp_img_bottom = tmp_y;
						tmp_max_min_compared = true;
					}else{
						tmp_img_left = Math.min(tmp_img_left, tmp_x);
						tmp_img_right = Math.max(tmp_img_right, tmp_x);
						tmp_img_top = Math.min(tmp_img_top, tmp_y);
						tmp_img_bottom = Math.max(tmp_img_bottom, tmp_y);
					}
					
				}
				g.setColor(new Color((int)(102+20.0*(m%3)/3), (int)(102+20.0*(m%4)/4),
						(int)(114+20.0*(m%5)/5)));
				g.fillPolygon(px_arr, py_arr, 4);
				if(st.label!=null){
					if(((m+8)%10)<0.05){
						g.setColor(new Color(224, 102, 51));
						Font stationFont = new Font("�L�n������", Font.BOLD,
								(int)(car.getCameraSize()/z_3d/3));
						g.setFont(stationFont);
						for(int i=0; i<st.label.length(); i++){
							g.drawString(""+st.label.charAt(i),
							(px_arr[1])-g.getFontMetrics(stationFont).
							stringWidth(""+st.label.charAt(i)),
							(py_arr[0]+py_arr[2]*3)/4+i*g.getFontMetrics(stationFont).getHeight()
							);
						}
					}
				}
			}
		}
		////
		/*
		
		int centerX = getWidth()/2, centerY = getHeight()/2;
		Wall wa = obj;
		double x_3d = wa.x-currentPosition, y_3d = -cameraElavation, z_3d = wa.z;
		
		for(double i=0; i<wa.width; i+=10){
			double px = (x_3d+i)/z_3d*cameraSize,
					py = -((y_3d)*scaleRatio)/z_3d*cameraSize;
			double ph = wa.height*cameraSize
					*scaleRatio/z_3d; //Private member scaleRatio
			double pw = 5*cameraSize
					*scaleRatio/z_3d; //pw for each 5 meters
			g.setColor(new Color((int)(51+(i/wa.width)*40), (int)(64+(i/wa.width)*32), 102));
			g.fillRect((int)(px+centerX), (int)(py+centerY-ph),
					(int)(pw), (int)(ph));
		}*/
	}
	
	/** Determine if the mouse overlaps the rectangle sensor-zone of this object. */
	@Override
	public boolean hitTest(Point mouse) {
		if(mouse==null)return false;
		if(mouse.x<tmp_img_left || mouse.x>tmp_img_right){
			return false;
		}else if(mouse.y<tmp_img_top || mouse.y>tmp_img_bottom){
			return false;
		}else if((mouse.x-tmp_img_left)%(tmp_img_unit_width*2) >= tmp_img_unit_width){
			return false;
		}
		return true;
	}
	
	/** Wall is not destroyable CURRENTLY.
	 *  TODO enable the restriction in the future version.
	 */
	@Override
	public boolean isDestroyable() {
		return false;
	}
	@Override
	public double destroyStuff() {
		return 0;
	}
}
