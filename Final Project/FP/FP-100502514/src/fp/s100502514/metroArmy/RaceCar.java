/**
 * Class RaceCar: the core of playing-game panel
 */
package fp.s100502514.metroArmy;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.*;

import javax.swing.*;
import javax.swing.Timer; //not to import java.util.Timer

public class RaceCar extends JPanel {

	/** UID */
	private static final long serialVersionUID = 1L;
	

	/** Constructors of RaceCar 
	 *  @throws InvalidRaceCarArgumentException */
	public RaceCar() throws InvalidRaceCarArgumentException{
		this(0, 5750, 130);
	}
	public RaceCar(double leftBound, double rightBound, double beginPosition)
			throws InvalidRaceCarArgumentException{
		this(leftBound, rightBound, beginPosition, TIMER_MEDIUM_QUALITY);
	}
	public RaceCar(double leftBound, double rightBound, double beginPosition, int user_delay)
			throws InvalidRaceCarArgumentException{
		
		/** Begin init data */
		//Scenery data.
		sceneryData = new GameSceneData();
		
		//Station List
		stations = sceneryData.stations;
		capacity = new int[stations.length]; //for each station
		
		//Set boundaries and the position.
		if(Math.min(leftBound, rightBound)<-50){
			throw new InvalidRaceCarArgumentException("Unable to reach such a SOUTH boundary.");
		}else if(Math.max(leftBound, rightBound)>sceneryLoopUnit+50){
			throw new InvalidRaceCarArgumentException("Unable to reach such a NORTH boundary.");
		}
		
		//Determine the initial direction by reading boundaries' positions.
		if(leftBound<rightBound){
			sceneryLeftBound = leftBound;
			sceneryRightBound = rightBound;
			isGoingRight = true;
		}else if(leftBound>rightBound){
			sceneryLeftBound = rightBound;
			sceneryRightBound = leftBound;
			isGoingRight = false;
		}else{
			throw new InvalidRaceCarArgumentException("Do NOT select the SAME positions.");
		}
		currentPosition = beginPosition;
		
		
		//Wall List
		walls = sceneryData.walls;
		
		//Scenery List
		Drawable[] scenery = sceneryData.scenery;
		/** End init data */
		
		//Set People and Scenery images
		//persons = new ImageIcon[URLs.length];
		try{
			for(int i=0; i<PersonURLs.length; i++){
				persons[i] = new ImageIcon(this.getClass().getResource(PersonURLs[i]));
			}
			for(int i=0; i<SceneryURLs.length; i++){
				sceneries[i] = new ImageIcon(this.getClass().getResource(SceneryURLs[i]));
			}
			for(int i=0; i<CursorURLs.length; i++){
				cursors[i] = new ImageIcon(this.getClass().getResource(CursorURLs[i]));
			}
			for(int i=0; i<StarPressURLs.length; i++){
				stars[i] = new ImageIcon(this.getClass().getResource(StarPressURLs[i]));
			}
		}catch(NullPointerException err){
			//If run in JFrame and throw an exception, then here will be run.
			for(int i=0; i<PersonURLs.length; i++){
				persons[i] = new ImageIcon(""+PersonURLs[i]);
			}
			for(int i=0; i<SceneryURLs.length; i++){
				sceneries[i] = new ImageIcon(""+SceneryURLs[i]);
			}
			for(int i=0; i<CursorURLs.length; i++){
				cursors[i] = new ImageIcon(""+CursorURLs[i]);
			}
			for(int i=0; i<StarPressURLs.length; i++){
				stars[i] = new ImageIcon(""+StarPressURLs[i]);
			}
			//delay = 40;
		}
		setCarTimerSettingDelay_ms(user_delay);
		
		//Tested and got the proper number below.
		scaleRatio = (double)persons[0].getIconHeight()/(double)persons[0].getIconWidth();
		
		//Set Sky background
		setBackground(new Color(102, 153, 255));
		
		
		//Add People
		for(int i=0;i<1500;i++){
			int n = (int)(Math.random()*stations.length);
			if(capacity[n] < stations[n].depth*stations[n].width && Math.random()<0.3){
				objs.add(new Person(i,
					stations[(int)(Math.random()*stations.length)]));
				capacity[n]++;
			}else{
				objs.add(new Person(i));
			}
		}
		
		//Add Walls
		for(int i=0; i<walls.length; i++){
			objs.add(walls[i]);
		}
		
		//Add Stations
		for(int i=0; i<stations.length; i++){
			objs.add(stations[i]);
		}
		
		//Add Scenery
		for(int i=0; i<scenery.length; i++){
			objs.add(scenery[i]);
		}
		
		//Determine the initial direction by reading current position.
		/*if(currentPosition>=(sceneryLeftBound+sceneryRightBound)/2){
			isGoingRight = false;
		}else{
			isGoingRight = true;
		}*/
		//Revise the current position if out-of-boundaries.
		if(currentPosition>sceneryRightBound){
			currentPosition = sceneryRightBound;
		}else if(currentPosition<sceneryLeftBound){
			currentPosition = sceneryLeftBound;
		}
		
		//Add Timer (using RaceCarRunningListener)
		timer = new Timer(init_delay, new RaceCarRunningListener(this));
		timer.start();
		
		/** Initialize the station timer */
		resetStationTimer();
		
		/*TODO change speed while testing*/
		/*goRight();
		currentSpeed = 0.09;*/
		
		/** Add Mouse Listeners (Declared below) */
		addMouseListener(myMouseListener);
		addMouseMotionListener(myMouseListener);
		
		/** Hide Default Mouse Cursor */
		try{
			setCursor(Toolkit.getDefaultToolkit().createCustomCursor(
				new BufferedImage(16, 16, BufferedImage.TYPE_4BYTE_ABGR)
				, new Point(0, 0), "hidden"));
		}catch(Exception err){
			/* omit all exceptions from the line above. */
		}
		
	}
	
	
	
	/** Should not use the following SETTER unless the initialization in MetroArmy. */
	private MetroArmy mainController;
	protected MetroArmy setController(MetroArmy mainController){
		return this.mainController = mainController;
	}
	public MetroArmy getController(){
		return mainController;
	}
	
	/** Get the game control panel (to drawthe speed-panel icon on the display.) */
	public GameControlPanel getGameControlPanel(){
		return mainController.east_control;
	}
	
	/** Settable Properties */
	private boolean isGoingRight = true;   //Left or Right
	public boolean isGoingRight(){
		return isGoingRight;
	}
	public boolean isGoingLeft(){
		return !isGoingRight;
	}
	public void goLeft(){
		isGoingRight = false;
		
		//Reset the station timer if just moved from rest.
		if(currentSpeed==0)proceedStationTimer();//resetStationTimer();
		
		//Just to make the number non-zero, but very close to it.
		//In order not to be regarded as the "stopped" state.
		currentSpeed -= 1e-10;
		
		//Make sure the game starts.
		begun = true;
		
		//stopped = false;
	}
	public void goRight(){
		isGoingRight = true;
		
		//Reset the station timer if just moved from rest.
		if(currentSpeed==0)proceedStationTimer();//resetStationTimer();
		
		//Just to make the number non-zero, but very close to it.
		//In order not to be regarded as the "stopped" state.
		currentSpeed += 1e-10; 
		
		//Make sure the game starts.
		begun = true;
		
		//stopped = false;
	}
	public boolean isStopped(){
		return /*stopped &&*/ currentSpeed == 0;
	}
	/** Get the speed(corrected with considering the car weight.) */
	public double getSpeedInKPH(){
		return Math.abs(currentSpeed/getCarWeight()*25*3.6);
	}
	/*public boolean isVerySlow(){ //If the speed < 0.5kph then return true.
		return getSpeedInKPH() < 0.5;
	}*/
	double acceleration = 0;
	public double getAccel(){
		return acceleration;
	}
	public void setAccel(double accel){
		acceleration = accel;
	}
	public boolean isBoosting(){
		return (Math.abs(acceleration)>0.0002)&&(isGoingRight == (acceleration>0));
	}
	public boolean isBraking(){
		return (Math.abs(acceleration)>0.0002)&&(isGoingRight != (acceleration>0));
	}
	public void setColor(Color newColor){
		arrow.setColor(newColor);
	}
	
	/** Determine if the game is paused. */
	private boolean isPaused = false;
	public boolean isPaused(){
		return isPaused;
	}
	
	/** Determine if the game's consuming-time is started. */
	private boolean begun = false;
	public boolean hasBegun(){
		return begun && !isGameOver() && !isAlreadyWinner();
	}
	
	/** Do NOT call this method directly EXCEPT such methods like MetroArmy.pauseGame(). */
	void setPaused(boolean isPaused){
		this.isPaused = isPaused;
		if(!isPaused){
			proceedStationTimer();
		}
	}
	
	/** Private Members */
	double currentPosition = 130;//130;   //In meter TODO modify when testing
	double currentSpeed = 0;       //For each 90kph
	public double getCurrentPosition(){
		return currentPosition+earthquake_offsetX;
	}
	public double getCurrentPositionForDisplayDigit(){
		//Currently not in use.
		return currentPosition;
	}
	public double getCurrentSpeed(){
		return currentSpeed;
	}
	
	/** The following 2 constants are currently unchangeable. */
	public static final double sceneryLoopUnit = 18000;//In meter
	public static final double maxDistantFromRoad = 70;//In meter
	/** Something about scenery, camera or earthquakes. */
	//Boundaries
	protected double sceneryLeftBound = 0;//In meter
	protected double sceneryRightBound = 5750;//In meter
	
	double defaultCameraElavation = 1.2;  //In meter
	double earthquake_offsetY = 0;
	double earthquake_speedY = 0;
	double earthquake_offsetX = 0;
	double earthquake_speedX = 0;
	int earthquake_timer = 0;
	int earthquake_alert_stop_timer = 0;
	boolean earthquake_needToStopCar = false;
	double cameraElavation = defaultCameraElavation+earthquake_offsetY;  //In meter
	double earthquake_intensity_to_display = 0;
	public double getCameraElavation(){
		return cameraElavation;
	}
	public void setEarthquake(double richter){
		if(isPaused())return;//Omits everything if game is paused.
		
		/* !!! NOTICE !!!
		 * Created on 2012.05.24
		 * The next day, Accidentally Deleted and then Decompiled from JAR File.
		 */
		double angle = Math.random()*2*Math.PI;
		earthquake_speedX = Math.exp(richter-8)*Math.cos(angle)*2;
		earthquake_speedY = Math.exp(richter-8)*Math.sin(angle);
		earthquake_timer = 20000; //decrease more obviously after 20 seconds.
	}
	public double getEarthquakeIntensity() {
		/* !!! NOTICE !!!
		 * Created on 2012.05.24
		 * The next day, Accidentally Deleted and then Decompiled from JAR File.
		 */
		double position = getDistance(earthquake_offsetX/3, earthquake_offsetY);
	    double velocity = getDistance(earthquake_speedX/3, earthquake_speedY);
	    double accel = 0.4 * 
	    		Math.pow(getDistance(earthquake_offsetX/3, earthquake_offsetY), 2);
	    double mass = 1;
	    double kineticEnergy = 0.5 * velocity * velocity;
	    double potentialEnergy = mass * accel * position;
	    return Math.log(kineticEnergy + potentialEnergy) / 2 + 8;
	}
	
	/** The record for when the "next earthquake" will be. */
	long when_nextEarthquake = -1;
	
	/** methods about game-over */
	//boolean stopped = true;
	boolean crashed = false;
	@Deprecated
	public boolean isCrashed(){
		return isGameOver();
	}
	public boolean isGameOver(){
		return crashed;
	}
	protected void setGameOver(){
		crashed = true;
		isWinner = false;
	}
	
	ArrowAnimation arrow = new ArrowAnimation(); //Run in paintComponent()
	
	private long lastStopTime_UTC; //In ms
	
	//Following long-int in ms, display this only if train stops, while repainting.
	private long lastTimeConsuming_UTC = 0;
	/** Methods can only be used in RaceCarPainter */
	void setLastTimeConsuming_UTC(long new_tc_utc){
		lastTimeConsuming_UTC = new_tc_utc;
	}
	long getLastTimeConsuming_UTC(){
		return lastTimeConsuming_UTC;
	}
	
	/** methods for station timer */
	public void resetStationTimer(){
		lastStopTime_UTC = new GregorianCalendar().getTimeInMillis();
	}
	public void proceedStationTimer(){
		lastStopTime_UTC = new GregorianCalendar().getTimeInMillis()-lastTimeConsuming_UTC;
	}
	public long getStationTimer(){
		return new GregorianCalendar().getTimeInMillis() - lastStopTime_UTC;
	}
	
	private double cameraSize = 300;
	public double getCameraSize(){
		return cameraSize;
	}
	//Tested and got the proper number above.
	
	//All of painting objects are saved here.
	LinkedList<Object> objs = new LinkedList<Object>();
	
	//The method to add WinnerBox
	public void addWinnerBox(double position){
		objs.add(new WinnerBox(position, 1.5));
	}
	
	/** The Derail-number: Derail if >=1, Flip-over if >=10. */
	private double derailNumberByBombard = 0;
	private long derailStationTime = 0;
	public double getDerailNumberByBombard(){
		return derailNumberByBombard;
	}
	public double getDerailNumberByEarthquake(){
		return Math.pow(getCurrentSpeed()/0.6, 2) * Math.exp((getEarthquakeIntensity()-5)/2);
	}
	public double getDerailNumberInTotal(){
		return getDerailNumberByBombard()+getDerailNumberByEarthquake();
	}
	void decreaseDerailNumberByTimer(int delay_ms){
		derailNumberByBombard *= Math.pow(0.3, 0.001*delay_ms);
	}
	
	/** Run for each time destroying a scenery. */
	void makeDerailDamage(double derailLevel){
		derailNumberByBombard += derailLevel;
	}
	
	/** Check if the car is to derail or flip-over, and if so then do it. */
	private boolean derailed = false;
	public boolean checkDerailOrFlipOver(int derailSigmaDelay_ms){
		boolean orig_derailed = derailed;
		if(getDerailNumberInTotal()>=1){
			derailed = true;
		}else if(isStopped()){
			if(derailed){
				lastStopTime_UTC -= convertDerailTimerToExtraAddedTime();
				
				//Reset earthquake timer
				when_nextEarthquake = (long)(getStationTimer()+1000*(Math.random()*240+60));
				
				
				derailed = false;
			}
		}
		
		/*derailSigma is similar to an integral of derailNumber
		  from the begin moment of derail.*/
		if(derailed){
			derailSigma += getDerailNumberInTotal()*derailSigmaDelay_ms*0.001;
			if(getDerailSigma()>=10){
				currentSpeed = acceleration = 0;
				setGameOver();
			}
			//System.err.println("S="+derailSigma);
		}else{
			derailSigma = 0;
		}
		
		// Record extra consuming-time for stopped after derail.
		if(!orig_derailed && derailed){
			derailStationTime = lastTimeConsuming_UTC;
		}
		return derailed;
	}
	private double derailSigma = 0;
	double getDerailSigma(){
		return derailSigma;
	}
	/** Get the time(ms) since this car's latest derail action.
	 *	If there is no derail currently, then -1 will be returned.
	 */
	public int getAndRefreshDerailTimer(){
		if(derailed){
			return (int)(lastTimeConsuming_UTC-derailStationTime);
		}else{
			return -1;
		}
	}
	/** Convert the number from getDerailTimer() into the Extra-added consuming-time. */
	public int convertDerailTimerToExtraAddedTime(){
		return convertDerailTimerToExtraAddedTime(getAndRefreshDerailTimer());
	}
	public int convertDerailTimerToExtraAddedTime(int derail_t){
		if(derail_t<0){
			return 0;
		}else{
			return 300000+derail_t*30;
		}
	}
	/** Get the GameStatus, defined by enum GameStatus. */
	//private GameStatus prevGameStatus = GameStatus.NOT_STARTED;
	//The one-above line has been moved to class RaceCarRunningListener.
	public GameStatus getGameStatus(){
		if(hasBegun()){
			if(derailed)return GameStatus.DERAIL;
			else if(isReadyForWinner())return GameStatus.READY_FOR_WINNER;
			else return GameStatus.PLAYING;
		}else if(isAlreadyWinner()){
			return GameStatus.WINNER;
		}else{
			if(isGameOver()){
				if(derailed)return GameStatus.FLIP_OVER;
				else return GameStatus.CRASH_BOUNDS;
			}else{
				return GameStatus.NOT_STARTED;
			}
		}
	}
	
	/** Determine if player is ready to win. */
	private boolean isWinner = false;
	public boolean isReadyForWinner(){
		return isWinner;
	}
	public boolean isAlreadyWinner(){
		return isWinner && isStopped();
	}
	void setToBeWinner(){
		isWinner = true;
	}
	
	/** Set the image URL strings first. */
	static String[] PersonURLs = GameSceneData.PersonURLs;
	static String[] SceneryURLs = GameSceneData.SceneryURLs;
	static String[] CursorURLs = GameSceneData.CursorURLs;
	static String[] StarPressURLs = GameSceneData.StarPressURLs;
	
	//People images will be set in the Constructor.
	static ImageIcon[] persons = new ImageIcon[PersonURLs.length];
	//Scenery images will be set in the Constructor.
	static ImageIcon[] sceneries = new ImageIcon[SceneryURLs.length];
	//Cursor images will be set in the Constructor.
	static ImageIcon[] cursors = new ImageIcon[CursorURLs.length];
	//Starpress images will be set in the Constructor.
	static ImageIcon[] stars = new ImageIcon[StarPressURLs.length];
	
	//Set the scale ratio for a person.
	static double scaleRatio;
	
	//Timer delay: Depends on the quality settings.
	public static final int TIMER_LOW_QUALITY = 150;
	public static final int TIMER_MEDIUM_QUALITY = 100;
	public static final int TIMER_HIGH_QUALITY = 60;
	public static final int TIMER_BEST_QUALITY = 40;
	
	int init_delay = TIMER_MEDIUM_QUALITY, delay = TIMER_MEDIUM_QUALITY;
	public boolean setCarTimerSettingDelay_ms(int ms){
		if(ms<25)return false;
		init_delay = delay = ms;
		return true;
	}
	public int getCarTimerSettingDelay_ms(){
		return init_delay;
	}
	public int getCarTimerCurrentDelay_ms(){
		return delay;
	}
	
	long currentUTC = new GregorianCalendar().getTimeInMillis();
	
	/** Declare all the data for constructor to create and read. */
	private GameSceneData sceneryData;
	Station[] stations;
	int[] capacity; //for each station
	Wall[] walls;
	Drawable[] scenery;
	
	Station nextArrivalStation = null;
	public final Station getNextArrivalStation(){
		return nextArrivalStation;//The returned value is not changeable.
	}
	boolean isArriving = true;
	public int getStationArrivalAndDirection(){
		//-1 for left; 1 for right; 0 for arrival
		return isArriving ? 0 : (isGoingRight()?1:-1);
	}
	
	//Main Timer in the game
	private Timer timer;
	protected void stopTimerBeforeClosing(){
		if(timer != null){
			timer.stop();
		}
	}
	
	/** Mouse listeners */
	boolean mousePressed = false;
	MouseAdapter myMouseListener = new RaceCarMouseListener(this);
	
	/** Number of passengers in the car */
	private ArrayList<Person> passengerList = new ArrayList<Person>();
	public int getNumPassengers(){
		return passengerList.size();
	}
	public double getCarWeight(){
		return 1+getNumPassengers()*0.01;
	}
	/** Remove a person from the scene and go into the car(invisible).
	 *  Returns true if the people existed in the scenery originally. */
	public boolean getOn(Person p){
		//TODO remove a person from the scene and go into the car(invisible).
		if(p==null){
			return false;
		}else if(!p.isVisibleOnGUI()){
			return false;
		}
		p.disable();
		passengerList.add(p);
		return true;
	}
	/** Remove a person from the car and turn back to scene(visible).
	 *  Returns a number for how many people actually got off. */
	public int getOff(int howMany){
		//TODO remove a person from the scene and go into the car(invisible).
		int i;
		for(i=0; i<howMany; i++){
			Person p;
			try{
				p = passengerList.remove((int)(Math.random()*passengerList.size()));
				p.enable();
				p.x = getCurrentPosition()+(Math.random()*2-1)*5;
				p.z += (Math.random()*2-1)*4;
				if(p.z<5)p.z = 5;
			}catch(IndexOutOfBoundsException err){
				//break the loop if the list is empty.
				break;
			}
			num_kicked_off_passengers++;
		}
		return i;
	}
	
	/** Get game score */
	private int num_kicked_off_passengers = 0;
	public int getScore(){
		int s = (int)((10e6*getNumPassengers()/lastTimeConsuming_UTC-
				num_kicked_off_passengers*10)*
				Math.abs(sceneryRightBound-sceneryLeftBound)/2000.0);
		return (s>=0) ? s : 0;
	}
	
	/** Declare an object to determine which drawable object overlaps the mouse position. */
	Drawable mouseHitTarget = null;
	/** The method for painting the car window. */
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		RaceCarPainter.paintOn(this, g);
	}
	
	static double getDistance(double x, double y){
		//Used in Timer to detect if car needs to stop in earthquake.
		return Math.sqrt(x*x+y*y);
	}
	
	/** The following method is used in paintComponents() */
	void draw(Graphics g, ArrowAnimation a){
		a.drawOnGraphics(g, this);
	}
	
}
