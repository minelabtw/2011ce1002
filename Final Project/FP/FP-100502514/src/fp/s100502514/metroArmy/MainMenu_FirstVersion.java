/**
 * Class MainMenu_FirstVersion: The Current-using Main Menu.
 * As to class MainMenu, it might be for future version or it may be replaced with this soon.
 * 20120616 TODO: Make the Station-selecting Menu. (which is before the game starts)
 */
package fp.s100502514.metroArmy;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

import javax.swing.*;

public class MainMenu_FirstVersion extends JApplet implements ActionListener {
	
	/** TODO TESTING */
	
	/** UID */
	private static final long serialVersionUID = 1L;
	
	JPanel south_panel;
	
	//Main Window (only called by main())
	private static BasicWindow win;
	
	//Changing size for whether it's in range-selecting mode. (2nd: Currently not-in-use)
	static final Dimension MAIN_MENU_SIZE = new Dimension(800, 600);
	//static final Dimension RANGE_MENU_SIZE = new Dimension(1000, 300);
	
	//Main Menu panel
	MetroArmy_MenuPanel menu_panel = new MetroArmy_MenuPanel();//call the inner class below
	
	//Inner class for the above panel
	class MetroArmy_MenuPanel extends JPanel{
		/** UID */
		private static final long serialVersionUID = 1L;
		
		private ImageIcon pic = new ImageIcon(this.getClass().getResource(
				"images/title_train.jpg"));
		
		private boolean isRangeSelectMode = false;
		public boolean isRangeSelectMode(){
			return isRangeSelectMode;
		}
		
		private RangeSelectMenu range_menu_panel;
		public RangeSelectMenu getRangeSelectMenuPanel(){
			return range_menu_panel;
		}
		
		public MetroArmy_MenuPanel(){
			setLayout(new BorderLayout());
			add(range_menu_panel = new RangeSelectMenu(), BorderLayout.CENTER);
			range_menu_panel.setVisible(isRangeSelectMode);
		}
		
		public void toggleRangeSelectMode(){
			range_menu_panel.setVisible(isRangeSelectMode = !isRangeSelectMode);
			if(range_menu_panel.isVisible()){
				//setPreferredSize(RANGE_MENU_SIZE);
				range_menu_panel.revalidate();
			}else{
				//setPreferredSize(MAIN_MENU_SIZE);
			}
			/*if(!isRangeSelectMode){
				isRangeSelectMode = true;
				setLayout(new BorderLayout());
				add(range_menu_panel = new RangeSelectMenu());
			}else{
				isRangeSelectMode = false;
				remove(range_menu_panel);
			}*/
		}
		public boolean isSelectDone(){
			try{
				return isRangeSelectMode && range_menu_panel.choose_map.isSelectCompleted();
			}catch(Exception err){//Especially for NullPointerException
				return false;
			}
		}
		
		/** Painting-overridden function */
		@Override
		protected void paintComponent(Graphics g){
			super.paintComponent(g);
			if(!range_menu_panel.isVisible()){
				g.drawImage(pic.getImage(), 0, 0, getWidth(), getHeight(), this);
				//g.setColor(new Color(0, 0, 0, 102));
				//g.fillRect(0, 0, getWidth(), getHeight());
			}
		}
	}
	
	//Buttons on menu
	JButton start_btn = new JButton("開始遊戲");
	JButton readme_btn = new JButton("遊戲說明");
	JButton info_btn = new JButton("遊戲資訊");
	JButton exit_btn = new JButton("離開遊戲");
	
	//Readme Text Pages ArrayList
	static ArrayList<String> readme_message = new ArrayList<String>();
	static final String README_SEPARATOR = "-----";
	void init_readme(){
		if(readme_message.size()>0)return;
		try {
			InputStream src = 
					this.getClass().getResourceAsStream("help_src/readme.txt");
			Scanner reader = new Scanner(src);
			readme_message.add("");
			while(reader.hasNextLine()){
				String tmp = reader.nextLine();
				if(tmp.equals(README_SEPARATOR)){
					readme_message.add(new String(""));
				}else{
					int last_index = readme_message.size()-1;
					readme_message.set(last_index,
							readme_message.get(last_index)+"\n"+tmp);
				}
			}
		} catch (Exception err) {
			readme_message.add("找不到遊戲說明檔案!!");
			err.printStackTrace();
		}
	}
	
	//private boolean is_dragging = false;
	
	public MainMenu_FirstVersion(){
		/** init() will initialize this applet. */
	}
	
	static int currentGameTimerDelay = RaceCar.TIMER_BEST_QUALITY;
	
	@Override
	public void init(){
		setLayout(new BorderLayout());
		/*
		JLabel title = new JLabel("劫運戰(Metro Survival Game)");
		title.setFont(new Font("微軟正黑體", Font.BOLD, 30));
		title.setForeground(new Color(204, 255, 0));
		title.setHorizontalAlignment(JLabel.CENTER);
		*/
		//set background to transparent.
		//menu_panel.setBackground(new Color(0, 0, 0, 0));
		//menu_panel.setLayout(new BorderLayout());
		//menu_panel.add(title, BorderLayout.CENTER);
		add(menu_panel, BorderLayout.CENTER);
		
		south_panel = new JPanel();
		south_panel.setLayout(new GridLayout(1, 4));
		
		start_btn.addActionListener(this);
		readme_btn.addActionListener(this);
		info_btn.addActionListener(this);
		exit_btn.addActionListener(this);
		
		south_panel.add(start_btn);
		south_panel.add(readme_btn);
		south_panel.add(info_btn);
		south_panel.add(exit_btn);
		add(south_panel, BorderLayout.SOUTH);
		
	}
	
	public static void main(String[] args){
		/*JFrame win = new JFrame();
		MainMenu_FirstVersion app = new MainMenu_FirstVersion();
		app.init();
		win.add(app);
		win.setSize(1000,800);
		win.setLocationRelativeTo(null);
		win.setTitle("Metro Army Game (under testing)");
		win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);*/
		/*win.addWindowStateListener(new WindowStateListener(){

			@Override
			public void windowStateChanged(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});*/
		
		JOptionPane.showMessageDialog(null,
				"歡迎蒞臨【劫運戰】!!\n" +
				"本遊戲目前仍為測試版本，需要玩家們更多建議，\n" +
				"以作為將來本遊戲改進的目標!! :)\n" +
				"本遊戲將於數天後，\n" +
				"於FlashTeens Network網站公開下載，\n" +
				"感謝諸位玩家的指教!!\n" +
				"by FlashTeens Chiang 2012.06.17\n" +
				"\nhttp://flashteens.horizon-host.com/",
				"劫運戰 Metro Survival Game",
				JOptionPane.INFORMATION_MESSAGE);
		
		win = new BasicWindow(new MainMenu_FirstVersion());
		win.setSize(MAIN_MENU_SIZE);
		win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		win.setVisible(true);
	}
	
	
	/** Button actions */
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() == start_btn){
			
			if(menu_panel.isRangeSelectMode() && !menu_panel.isSelectDone()){
				//menu_panel.toggleRangeSelectMode();
				JOptionPane.showMessageDialog(null, "區間尚未選取完畢!!\n",
						"劫運戰 Metro Survival Game",
						JOptionPane.WARNING_MESSAGE);
				repaint();
				return;
			}else if(!menu_panel.isRangeSelectMode()){
				menu_panel.toggleRangeSelectMode();
				repaint();
				return;
			}
			double[] settings = menu_panel.getRangeSelectMenuPanel().getUserChoice();
			menu_panel.toggleRangeSelectMode();
			
			/*System.out.println("["+settings[0]+", "+settings[1]+", "+
					settings[2]+", "+settings[3]+"]");*/
			
			BasicWindow game = new BasicWindow(new MetroArmy(
					settings[0], settings[1], settings[2], settings[3],
					/*1560, 3640, 1620, 3620,*/
					currentGameTimerDelay));
			game.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
			game.setVisible(true);
			game.addWindowListener(new WindowAdapter(){
				
				private MetroArmy game_frame;
				private RaceCar tmp;
				
				@Override
				public void windowClosing(WindowEvent e) {
					game_frame = ((MetroArmy)((BasicWindow)e.getSource()).
							getInnerApplet());
					tmp = game_frame.getRaceCar();
					if( !(tmp.isAlreadyWinner() || tmp.isGameOver()) ){
						if(game_frame.isPaused()){
							if(JOptionPane.showConfirmDialog(null,
									"本遊戲還沒結束，是否中斷此局遊戲?\n(遊戲分數將不計)\n",
									"Metro Survival Game",
									JOptionPane.YES_NO_OPTION,
									JOptionPane.QUESTION_MESSAGE)==
									JOptionPane.YES_OPTION){
								closeGame(e);
							}
						}else{
							game_frame.pauseGame();
						}
					}else{
						closeGame(e);
					}
				}
				
				private void closeGame(WindowEvent e){
					//TODO REMOVE all timers in game_frame before closing.
					tmp.stopTimerBeforeClosing();
					//Display the main menu after game is closed.
					((BasicWindow)e.getSource()).setVisible(false);
					win.setVisible(true);
				}
			});
			win.setVisible(false);
		}else if(e.getSource() == readme_btn){
			//Readme messages
			init_readme();
			int count_item = 0;
			while(JOptionPane.showConfirmDialog(null,
					readme_message.get(count_item) +
					(
						(count_item+1 >= readme_message.size()) ?
						"\n\n[感謝您閱讀本遊戲說明，祝您好運!!]" :
						"\n\n[請按【確定】繼續下一頁，或是【取消】退出本說明]\n"
					),
					"遊戲說明 Page "+(count_item+1)+"/"+readme_message.size(),
					JOptionPane.OK_CANCEL_OPTION,
					JOptionPane.INFORMATION_MESSAGE)==JOptionPane.OK_OPTION
					&& (++count_item)<readme_message.size()){
			}
		}else if(e.getSource() == info_btn){
			JOptionPane.showMessageDialog(null,
					"劫運戰(Metro Survival Game)\n" +
					"Alpha Version 0.1.5\n" +
					"by FlashTeens Chiang, NCU CSIE, Taiwan\n" +
					"最近版本已於2012年6月17日更新\n" +
					"將來本遊戲會不斷更新，敬請期待!!\n" +
					"下載點網址：(預計6/22之前開放)\n" +
					"http://flashteens.horizon-host.com/",
					"遊戲版本資訊",
					JOptionPane.INFORMATION_MESSAGE);
		}else if(e.getSource() == exit_btn){
			if(menu_panel.isRangeSelectMode()){
				menu_panel.toggleRangeSelectMode();
			}else{
				System.exit(0);
			}
		}
	}
	
}
