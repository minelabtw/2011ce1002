/**
 * The static painting class used in RaceCar to paintComponent().
 */
package fp.s100502514.metroArmy;

import java.awt.*;
import java.util.*;

import javax.swing.JComponent;

final class RaceCarPainter {
	
	/** The following variable is just for debugging use.
	 *  Set to false is equivalent to commentting the testing codes off. */
	private static final boolean DEBUG = false;
	
	/** No constructors here. */
	private RaceCarPainter(){
		//No Constructors, but just use in this file for the getClass() reference.
	}
	
	/** The method for painting the car window. */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	static void paintOn(RaceCar target, Graphics g){
		
		/* Detect the mouse position */
		Point mousePos = target.getMousePosition();
		target.mouseHitTarget = null;
		
		//Draw Ground
		g.setColor(new Color(51, 204, 153)); //green
		g.fillRect(0, target.getHeight()/2, target.getWidth(), target.getHeight()/2);
		

		//Draw stations
		/*for(int i=0; i<stations.length; i++){
			draw(g, stations[i]);
		}*/
		
		//Draw People
		Object[] objs = target.objs.toArray();
		Arrays.sort(objs, new Comparator(){
			@Override
			public int compare(Object arg0, Object arg1) {
				double z0 = Math.abs(((Drawable)(arg0)).getZ());
				double z1 = Math.abs(((Drawable)(arg1)).getZ());
				if(z0>z1)return -1;
				else if(z0<z1)return 1;
				else return 0;
			}
		});
		for(int i=0; i<objs.length; i++){
			//Draw the objects
			if(objs[i] instanceof Drawable){
				((Drawable)(objs[i])).drawOn(target, g);
			}
		}
		for(int i=objs.length-1; i>=0; i--){
			//Detect the object if overlapped with mouse (by opposite scanning direction)
			if(((Drawable)(objs[i])).hitTest(mousePos)){
				target.mouseHitTarget = (Drawable)(objs[i]);
				break;
			}
		}
		/** TODO all actions if overlaps (?)
		if(mouseHitTarget==null){
			System.out.println("#Nothing is hit.");
		}else if(mouseHitTarget instanceof Station){
			if(((Station)mouseHitTarget).name==null ||
					((Station)mouseHitTarget).name.isEmpty()){
				System.out.println("Sidewalk is hit.");
			}else{
				System.out.println("Station "+((Station)mouseHitTarget).name+" is hit.");
			}
		}else if(mouseHitTarget instanceof Wall){
			if(((Wall)mouseHitTarget).label==null ||
					((Wall)mouseHitTarget).label.isEmpty()){
				System.out.println("Wall is hit.");
			}else{
				System.out.println("Wall of "+((Wall)mouseHitTarget).label+" is hit.");
			}
		}else if(mouseHitTarget instanceof Person){
			switch(((Person)mouseHitTarget).image){
			case 0:
				System.out.println("Woman-faced is hit.");
				break;
			case 1:
				System.out.println("Woman-backed is hit.");
				break;
			case 2:
				System.out.println("Gentleman is hit.");
				break;
			case 3:
				System.out.println("Pirateman is hit.");
				break;
			default:
				System.out.println("Other Person is hit.");
			}
		}else if(mouseHitTarget instanceof WinnerBox){
			System.out.println("The Winner Box is hit.");
		}else if(mouseHitTarget instanceof Scenery){
			switch(((Scenery)mouseHitTarget).image){
			case 0:
				System.out.println("Apartment is hit.");
				break;
			case 1:
				System.out.println("GreatTower is hit.");
				break;
			case 2:
				System.out.println("Tree is hit.");
				break;
			case 3:
				System.out.println("Museum is hit.");
				break;
			default:
				System.out.println("Other Scenery is hit.");
			}
		}
		*/
		if(DEBUG){/** Debug only */
			//Draw messages for speed and position
			g.setColor(Color.orange);
			Font speed_font = new Font("微軟正黑體", Font.BOLD, 20);
			g.setFont(speed_font);
			g.drawString("目前時速為", target.getWidth()-270, 45);
			String speed_txt = Math.rint(target.getSpeedInKPH()*10)/10+"";
			g.drawString(speed_txt, target.getWidth()-60-
					g.getFontMetrics(speed_font).stringWidth(speed_txt), 45);
			g.drawString("公里", target.getWidth()-50, 45);
			
			
			for(int i=1; i<target.stations.length; i++){
				//i=0 is for the begin station, so not included.
				//scope for distance to next station
				
				int count = i;
				if(target.isGoingLeft())count = target.stations.length-i;
				
				double sta_pos_rightward = target.stations[count].x +
						(target.isGoingRight() ? target.stations[count].width : 0);
				double sta_pos_leftward = target.stations[count-1].x +
						(target.isGoingRight() ? target.stations[count-1].width : 0);
				double display_distance = target.isGoingRight()?
						(sta_pos_rightward-target.currentPosition):
							(target.currentPosition-sta_pos_leftward);
				boolean is_excess = false;
				
				if(target.isGoingRight()&&target.currentPosition>target.sceneryRightBound){
					display_distance = target.currentPosition-target.sceneryRightBound;
					g.setColor(Color.red);
					is_excess = true;
				}else if(target.isGoingLeft()&&target.currentPosition<target.sceneryLeftBound){
					display_distance = target.sceneryLeftBound-target.currentPosition;
					g.setColor(Color.red);
					is_excess = true;
				}else if(display_distance>=0){
					g.setColor(Color.yellow);
				}else if(i+1>=target.stations.length){
					display_distance *= -1;
					g.setColor(Color.red);
					is_excess = true;
				}else{
					continue;
				}
				
				Font posi_font = new Font("微軟正黑體", Font.BOLD, 20);
				g.setFont(posi_font);
				
				//Next station: xxx_name
				String leftStation = target.stations[count-1].name;
				String rightStation = target.stations[count].name;
				
				if(leftStation.indexOf("站")==-1){
					leftStation = new String(leftStation+"站");
				}
				if(rightStation.indexOf("站")==-1){
					rightStation = new String(rightStation+"站");
				}
				
				target.isArriving = display_distance<=
						(target.isGoingRight()?target.stations[count].width:
							target.stations[count-1].width)+50;
				
				target.nextArrivalStation = target.isGoingRight() ?
						target.stations[count] : target.stations[count-1];
				
				if(!is_excess){
					String left_msg =
							display_distance>=0 && target.isArriving ? "" : leftStation;
					String mid_msg =
							display_distance>=0 && target.isArriving ?
							(target.isGoingRight()?rightStation:leftStation)+"到了"
							: target.isGoingRight()?"→":"←";
					String right_msg =
							display_distance>=0 && target.isArriving ? "" : rightStation;
					
					g.drawString(left_msg, target.getWidth()
							-210-g.getFontMetrics(posi_font).stringWidth(left_msg)/2, 70);
					g.drawString(mid_msg, target.getWidth()
							-150-g.getFontMetrics(posi_font).stringWidth(mid_msg)/2, 70);
					g.drawString(right_msg, target.getWidth()
							-90-g.getFontMetrics(posi_font).stringWidth(right_msg)/2, 70);
				}
				
				//Distance to next station: ??? m
				String next_indication = target.isArriving ? "停車點" : "下一站";
				
				g.drawString(is_excess ? "已經超過終點" :
					(target.isGoingRight()?"距離"+next_indication+"→":"←距離"+next_indication),
					target.getWidth()-270, 95);
				String posi_txt = Math.rint(display_distance*10)/10+"";
				g.drawString(posi_txt, target.getWidth()-60-
						g.getFontMetrics(posi_font).stringWidth(posi_txt), 95);
				g.drawString("m", target.getWidth()-50, 95);
				
				//After a draw, then break the loop.
				break;
			}
			/* only for an independent scope */{
				//scope for distance to terminal station
				double display_distance = target.isGoingRight() ?
						(target.sceneryRightBound-target.currentPosition):
							(target.currentPosition-target.sceneryLeftBound);
				boolean is_excess = false;
				if(display_distance>=0){
					g.setColor(Color.yellow);
				}else{
					display_distance *= -1;
					g.setColor(Color.red);
					is_excess = true;
				}
				Font posi_font = new Font("微軟正黑體", Font.BOLD, 20);
				g.setFont(posi_font);
				g.drawString(is_excess ? "已經超過終點" : "距離終點還有", target.getWidth()-270, 120);
				String posi_txt = Math.rint(display_distance*10)/10+"";
				g.drawString(posi_txt, target.getWidth()-60-
						g.getFontMetrics(posi_font).stringWidth(posi_txt), 120);
				g.drawString("m", target.getWidth()-50, 120);
			}
		}
		
		/** Update timer */
		if(target.hasBegun() && !target.isPaused() && !target.isAlreadyWinner()){
			target.setLastTimeConsuming_UTC(target.getStationTimer());
		}
		long station_timer = target.getLastTimeConsuming_UTC();
		
		if(DEBUG){/** Debug only */
			//Draw messages for consuming time
			g.setColor(Color.white);
			Font top_right_font = new Font("微軟正黑體", Font.BOLD, 20);
			g.setFont(top_right_font);
			String time_left_str = "開車時間";
			String time_str = station_timer/60000+"分"+
					(100+station_timer/1000%60+"").substring(1)+"秒"+
					(1000+station_timer%1000+"").substring(1);
			// The use like (1000+n+"").substring(1) is to make sure that three digits display.
			if(target.crashed){
				time_str = "";
				time_left_str = "緊急撤離!!";
				if(target.getStationTimer()%600<300)g.setColor(Color.red);
				else g.setColor(Color.yellow);
			}else if(target.isStopped()){
				//time_left_str = "[已停車]";
				g.setColor(new Color(204, 255, 0));
			}
			g.drawString(time_left_str, target.getWidth()-270, 20);
			g.drawString(time_str,
					target.getWidth()-(10+g.getFontMetrics(top_right_font).
							stringWidth(time_str)), 20);
			
			//Draw Arrow Messages (Just for former tests.)
			//target.draw(g, target.arrow);
		}
		/** Draw Station Map only if playing */
		if(!(target.isAlreadyWinner() || target.isGameOver())){
			
			//Determines the bounds.
			double beginIconBound = target.sceneryLeftBound;
			double endIconBound = target.sceneryRightBound;
			if(target.isGoingLeft()){
				//Exchange the 2 variables if the direction is opposite.
				double tmp_exch = beginIconBound;
				beginIconBound = endIconBound;
				endIconBound = tmp_exch;
			}
			
			//Declares the map and draw on.
			StationMap map = new StationMap((int)(g.getClipBounds().width*0.72),
					beginIconBound, endIconBound,
					target.currentPosition, g.getClipBounds().width/100);
			map.setX((int)(g.getClipBounds().width*0.14));
			map.setY((int)(g.getClipBounds().height*0.04));
			map.drawOn(g);
		}
		
		/** Draw speed-panel display icon */
		target.getGameControlPanel().drawSpeedPanelOn(g,
				g.getClipBounds().width+110, g.getClipBounds().height+50, (int)station_timer,
				JComponent.RIGHT_ALIGNMENT, JComponent.BOTTOM_ALIGNMENT);
		
		
		/** Draw an alert if winner or crashed */
		if(target.isAlreadyWinner()){
			int msg_timer = (int)(new GregorianCalendar().getTimeInMillis()%1600)/400;
			
			switch(msg_timer){
			case 0:
				g.setColor(new Color(255, 102, 153, 102));
				break;
			case 1:
				g.setColor(new Color(224, 255, 102, 102));
				break;
			case 2:
				g.setColor(new Color(153, 236, 224, 102));
				break;
			default:
				g.setColor(new Color(236, 153, 255, 102));
			}
			
			g.fillRect(0, 0, target.getWidth(), target.getHeight());
			
			/** draw title */
			Font crash_msg_font = new Font("微軟正黑體", Font.BOLD, 80);
			g.setFont(crash_msg_font);
			String crash_msg = "任務成功!!";
			
			//Set shadow color
			Color txt_bgcolor = new Color(0, 0, 0, 153);
			//Set foreground color
			Color txt_fgcolor;
			switch(msg_timer){
			case 0:
				txt_fgcolor = new Color(255, 191, 204);
				break;
			case 1:
				txt_fgcolor = new Color(204, 255, 0);
				break;
			case 2:
				txt_fgcolor = new Color(0, 255, 204);
				break;
			default:
				txt_fgcolor = new Color(224, 102, 255);
			}
			
			//Shadow part of title
			g.setColor(txt_bgcolor);
			g.drawString(crash_msg,
					(target.getWidth()-g.getFontMetrics(crash_msg_font).
							stringWidth(crash_msg))/2+2,
							target.getHeight()/2-30+2
			);
			
			//Foreground part of title
			g.setColor(txt_fgcolor);
			
			g.drawString(crash_msg,
					(target.getWidth()-g.getFontMetrics(crash_msg_font).
							stringWidth(crash_msg))/2,
							target.getHeight()/2-30
			);
			
			/** draw score message */
			crash_msg_font = new Font("微軟正黑體", Font.BOLD, 30);
			g.setFont(crash_msg_font);
			
			/*TODO SCORING!!!*/
			String crash_msg2 = "您的分數： "+target.getScore()+"分";
			
			//Shadow part of score message
			g.setColor(txt_bgcolor);
			g.drawString(crash_msg2,
					(target.getWidth()-g.getFontMetrics(crash_msg_font).
							stringWidth(crash_msg2))/2+2,
							target.getHeight()/2+50+2
			);
			
			//Foreground part of score message
			g.setColor(txt_fgcolor);
			g.drawString(crash_msg2,
					(target.getWidth()-g.getFontMetrics(crash_msg_font).
							stringWidth(crash_msg2))/2,
					target.getHeight()/2+50
			);
			
		}else if(target.isGameOver()){
			if(target.getGameStatus()==GameStatus.FLIP_OVER){
				g.setColor(new Color(64, 51, 0, 153));
			}else{
				g.setColor(new Color(255, 0, 0, 102));
			}
			g.fillRect(0, 0, target.getWidth(), target.getHeight());
			g.setColor(Color.yellow);
			Font crash_msg_font = new Font("微軟正黑體", Font.BOLD, 80);
			g.setFont(crash_msg_font);
			String crash_msg = "Game Over";
			g.drawString(crash_msg,
					(target.getWidth()-g.getFontMetrics(crash_msg_font).
							stringWidth(crash_msg))/2,
					target.getHeight()/2-30
			);
			crash_msg_font = new Font("微軟正黑體", Font.BOLD, 30);
			g.setFont(crash_msg_font);
			String crash_msg2;
			if(target.getGameStatus()==GameStatus.FLIP_OVER){
				crash_msg2 = "列車已損壞殆盡!!!";
			}else{
				crash_msg2 = "列車已超出終點站20m!!!";
			}
			g.drawString(crash_msg2,
					(target.getWidth()-g.getFontMetrics(crash_msg_font).
							stringWidth(crash_msg2))/2,
							target.getHeight()/2+50
			);
		}else if(target.isPaused()){
			g.setColor(new Color(0, 102, 255, 102));
			g.fillRect(0, 0, target.getWidth(), target.getHeight());
			if(new GregorianCalendar().getTimeInMillis()%800<400){
				g.setColor(new Color(0, 255, 255));
			}else{
				g.setColor(new Color(204, 204, 255));
			}
			Font crash_msg_font = new Font("微軟正黑體", Font.BOLD, 80);
			g.setFont(crash_msg_font);
			String crash_msg = "遊戲暫停!!";
			g.drawString(crash_msg,
				(target.getWidth()-g.getFontMetrics(crash_msg_font).stringWidth(crash_msg))/2,
				target.getHeight()/2-30
			);
			crash_msg_font = new Font("微軟正黑體", Font.BOLD, 30);
			g.setFont(crash_msg_font);
			String crash_msg2 = "請按一下畫面繼續遊戲...";
			g.drawString(crash_msg2,
					(target.getWidth()-g.getFontMetrics(crash_msg_font).
							stringWidth(crash_msg2))/2,
					target.getHeight()/2+50
			);
		}else if(target.earthquake_needToStopCar){
			/* !!! NOTICE !!!
			 * Created on 2012.05.24
			 * The next day, Accidentally Deleted and then Decompiled from JAR File.
			 */
			if(target.getEarthquakeIntensity() > target.earthquake_intensity_to_display){
				target.earthquake_intensity_to_display = target.getEarthquakeIntensity();
			}else{
				target.earthquake_intensity_to_display +=
		        	(target.getEarthquakeIntensity()-target.earthquake_intensity_to_display)/10;
		    }
			int intensity_num = (int)Math.round(target.earthquake_intensity_to_display);
			if(intensity_num < 0){
				intensity_num = 0;
			}
			g.setColor(new Color(255, 153, 0, 
		    	(int)(80*(1-Math.abs(new GregorianCalendar().getTimeInMillis()%700-350)
		    	/350.0))
			));
			g.fillRect(0, 0, target.getWidth(), target.getHeight());
			g.setColor(Color.yellow);
			Font crash_msg_font = new Font("微軟正黑體", 1, 80);
			g.setFont(crash_msg_font);
			String crash_msg = "發生地震!!";
			g.drawString(crash_msg, 
					(target.getWidth() - g.getFontMetrics(crash_msg_font).
							stringWidth(crash_msg)) / 2, 
					target.getHeight() / 2 - 30);
			
			if (intensity_num >= 3) {
				crash_msg_font = new Font("微軟正黑體", 1, 30);
				g.setFont(crash_msg_font);
				String crash_msg2 = "震度已達" + intensity_num + "級，請盡速停車!!";
				g.drawString(crash_msg2, 
					(target.getWidth() - g.getFontMetrics(crash_msg_font).
							stringWidth(crash_msg2)) / 2, 
					target.getHeight() / 2 + 50);
			}
		}
		
		
		if(!target.isAlreadyWinner() && target.getGameStatus()==GameStatus.READY_FOR_WINNER){
			int period_ms = 1600;
			int msg_timer = (int)(new GregorianCalendar().getTimeInMillis()%period_ms);
			int alpha = (int)(((double)msg_timer)/period_ms*256*2);
			if(alpha>=256) alpha = 256*2-1 - alpha;
			
			//Make sure to be in range of 0 to 255 for alpha.
			if(alpha>255) alpha = 255;
			if(alpha<0) alpha = 0;
			
			g.setColor(new Color(0, 0, 255, alpha/2));
			
			g.fillRect(0, 0, target.getWidth(), 80);
			g.setColor(new Color(224, 204, 255, alpha/2+128));
			
			Font crash_msg_font = new Font("微軟正黑體", Font.BOLD, 40);
			g.setFont(crash_msg_font);
			String crash_msg = "已拿到急救箱，請停車以完成任務!!";
			g.drawString(crash_msg,
				(target.getWidth()-g.getFontMetrics(crash_msg_font).stringWidth(crash_msg))/2, 60
			);
		}else if(target.getGameStatus()==GameStatus.DERAIL){
			int period_ms = 600;
			int msg_timer = (int)(new GregorianCalendar().getTimeInMillis()%period_ms);
			int alpha = (int)(((double)msg_timer)/period_ms*256*2);
			if(alpha>=256) alpha = 256*2-1 - alpha;
			
			//Make sure to be in range of 0 to 255 for alpha.
			if(alpha>255) alpha = 255;
			if(alpha<0) alpha = 0;
			
			g.setColor(new Color(255, 0, 0, alpha/2));
			
			g.fillRect(0, 0, target.getWidth(), 150);
			g.setColor(new Color(255, 240, 102, alpha/2+128));
			
			Font crash_msg_font = new Font("微軟正黑體", Font.BOLD, 40);
			g.setFont(crash_msg_font);
			String crash_msg = "列車已出軌，請盡速停車!!";
			g.drawString(crash_msg,
				(target.getWidth()-g.getFontMetrics(crash_msg_font).stringWidth(crash_msg))/2, 60
			);
			
			if(target.getDerailSigma()>=6){
				g.setColor(new Color(255, 51, 0, 153));
				g.fillRoundRect(target.getWidth()/2-300, target.getHeight()/2-100,
						600, 200 , 20, 20);
				Font crash2_msg_font = new Font("微軟正黑體", Font.BOLD, 40);
				g.setFont(crash2_msg_font);
				g.setColor(Color.orange);
				String crash2_msg = "!！!! 小心列車即將全毀 !!！!";
				g.drawString(crash2_msg,
					(target.getWidth()/2-g.getFontMetrics(crash2_msg_font).
							stringWidth(crash2_msg)/2),
					target.getHeight()/2);
			}
			
			if(target.isReadyForWinner()){
				Font crash2_msg_font = new Font("微軟正黑體", Font.BOLD, 30);
				g.setFont(crash2_msg_font);
				String crash2_msg = "已拿到急救箱，維修時間不計";
				g.drawString(crash2_msg,
					(target.getWidth()/2-g.getFontMetrics(crash2_msg_font).
							stringWidth(crash2_msg)/2), 135);
			}else{
				Font crash2_msg_font = new Font("微軟正黑體", Font.BOLD, 30);
				g.setFont(crash2_msg_font);
				String crash2_msg = "停車後將直接跳過";
				g.drawString(crash2_msg,
					(target.getWidth()/2-g.getFontMetrics(crash2_msg_font).
							stringWidth(crash2_msg)), 135);
				
				String crash3_msg = "";
				int will_be_extra_added = target.convertDerailTimerToExtraAddedTime();
				String ext_sec = will_be_extra_added/1000%60+"";
				String ext_min = will_be_extra_added/60000+"";
				if(ext_sec.length()==1)ext_sec = "0"+ext_sec;
				crash3_msg += ext_min+"分"+ext_sec+"秒維修時間";
				
				g.drawString(crash3_msg,
					(target.getWidth()/2-g.getFontMetrics(crash2_msg_font).stringWidth(
					crash3_msg)+270), 135);
			}
		}
		
		/** Draw Cursor at last */
		if(mousePos == null){
			// Don't draw any cursor if mouse is not on the window.
		}else if(target.isPaused() || target.isAlreadyWinner() || target.isGameOver()){
			g.drawImage(RaceCar.cursors[2].getImage(),
					mousePos.x-16, mousePos.y-16,
					32, 32, target);
		}else if(target.mousePressed){
			g.drawImage(RaceCar.cursors[1].getImage(),
					mousePos.x-16, mousePos.y-16,
					32, 32, target);
		}else{
			g.drawImage(RaceCar.cursors[0].getImage(),
					mousePos.x-16, mousePos.y-16,
					32, 32, target);
		}
	}
}
