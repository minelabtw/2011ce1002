package f1;

import java.util.*;

public class StoreData {
	// use to store your last gamedata
	java.io.File filein = new java.io.File("image1/score.txt");
	java.io.File fileout = new java.io.File("image1/lastscore.txt");

	public int inputData() throws Exception {
		Scanner input = new Scanner(filein);
		int score = input.nextInt();
		input.close();
		return score;
	}

	public int inputoutData() throws Exception {
		Scanner input = new Scanner(fileout);
		int score = input.nextInt();
		input.close();
		return score;
	}

	public void outputinData(int score) throws Exception {
		java.io.PrintWriter output = new java.io.PrintWriter(filein);
		output.print(score);
		output.close();
	}

	public void outputData(int score) throws Exception {
		java.io.PrintWriter output = new java.io.PrintWriter(fileout);
		output.print(score);
		output.close();
	}
}
