package f1;

import java.awt.*;
import javax.swing.*;

import java.awt.event.*;

public class paintPicture extends JPanel {
	// bar spin speed
	private int delay = 1;
	private int newdelay = 0;
	// control gameoverpicture's time
	private int delayGameover = 1000;
	// window size
	private int x = getWidth();
	private int y = getHeight();
	private int count = 0;
	private int select = 0;
	private int countTime = 0;
	protected int stage = 0;
	private rules rul1 = new rules();	
	private Timer timer = new Timer(delay, new TimerListener());
	private boolean gameOver = false;
	private boolean enterPushed = false;
	private ImageIcon shopIcon = new ImageIcon("image1/shop.jpg");
	private randomPicture rp1 = new randomPicture();
	private StoreData sd1 = new StoreData();
	private score scr1 = new score();
	private ImageIcon selectbar[] = { new ImageIcon("image1/selectbar.png"),
			new ImageIcon("image1/arrow.png") };
	private ImageIcon itemPictrueIcon[] = {
			new ImageIcon("image1/itemcherry.png"),
			new ImageIcon("image1/itemquestion.png"),
			new ImageIcon("image1/item777.png"),
			new ImageIcon("image1/soldout.png")
			};
	private ImageIcon barscoreIcon[] = { new ImageIcon("image1/seven.gif"),
			new ImageIcon("image1/watermalon.gif"),
			new ImageIcon("image1/question.gif"),
			new ImageIcon("image1/bell.gif"),
			new ImageIcon("image1/cherry.gif"),
			new ImageIcon("image1/double.gif"),
			new ImageIcon("image1/triple.gif"),

	};
	private ImageIcon gameopIcon[] = { new ImageIcon("image1/gameover.jpg"),
			new ImageIcon("image1/gameover1.jpg"),
			new ImageIcon("image1/gameover2.jpg"),
			new ImageIcon("image1/gameover3.jpg"),
			new ImageIcon("image1/gameover4.jpg"), };

	private Font largeFont = new Font("TimeRoman", Font.BOLD, 20);
	private Font largerFont = new Font("TimeRoman", Font.BOLD, 25);

	protected ImageIcon menuBack = new ImageIcon("image1/menu2.jpg");

	private ImageIcon button[] = { new ImageIcon("image1/button1.gif"),
			new ImageIcon("image1/button2.gif") };

	private ImageIcon background = new ImageIcon("image1/back2.jpg");
	private ImageIcon barPictrue[] = { new ImageIcon("image1/seven.jpg"),
			new ImageIcon("image1/watermalon.jpg"),
			new ImageIcon("image1/question.jpg"),
			new ImageIcon("image1/bell.jpg"),
			new ImageIcon("image1/cherry.jpg"),
			new ImageIcon("image1/double.jpg"),
			new ImageIcon("image1/triple.jpg"),
			new ImageIcon("image1/exseven.jpg"),
			new ImageIcon("image1/exquestion.jpg"),
			new ImageIcon("image1/doublecherry.jpg") };

	public paintPicture() {
		addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				switch (e.getKeyCode()) {
				case KeyEvent.VK_UP:
					if (stage == 0) {
						if (select <= 3 && select > 1) {
							select--;
							repaint();
						}
					} else if (stage == 4) {
						if (select <= 3 && select > 1) {
							select--;
							repaint();
						}
					}
					break;
				case KeyEvent.VK_DOWN:
					if (stage == 0) {
						if (select < 3) {
							select++;
							repaint();
						} else if (select == 3) {
							repaint();
						}
					} else if (stage == 4) {
						if (select < 3) {
							select++;
							repaint();
						} else if (select == 3) {
							repaint();
						}
					}
					break;
				case KeyEvent.VK_ENTER:
					// menu panel
					if (stage == 0) {
						timer.stop();
						// start
						if (select == 1) {
							// init score
							scr1.score1 = 4000;
							scr1.cheatcherry = false;
							scr1.cheatQuestion = false;
							rp1.cheat777 = false;
							stage = select;
							gameOver = false;
						}
						// continues
						else if (select == 2) {
							//
							try {
								sd1.outputinData(sd1.inputoutData());
								scr1.score1 = sd1.inputData();
							} catch (Exception e1) {
								e1.printStackTrace();
							}
							stage = select;
							gameOver = false;
						}
						// exit
						else if (select == 3) {
							System.exit(0);
						}
						repaint();
					}
					// game panel
					else if (stage == 1) {

						if (enterPushed == false) {
							if (scr1.score1 >= 50) {
								scr1.score1 -= 50;
								gameOver = false;
								timer.start();
								enterPushed = true;
							} else {
								timer.start();
								gameOver = true;
							}
						} else if (enterPushed == true) {

						}
					} else if (stage == 4) {
						if (select == 1 && scr1.score1 >= 500) {
							scr1.score1 -= 500;
							scr1.cheatcherry = true;
						} else if (select == 2 && scr1.score1 >= 1000) {
							scr1.score1 -= 1000;
							scr1.cheatQuestion = true;
						} else if (select == 3 && scr1.score1 >= 1000) {
							scr1.score1 -= 1000;
							rp1.cheat777 = true;
						}

					}
					break;
				case KeyEvent.VK_E:
					// use to exit gamepanel
					if (stage == 1) {
						try {
							sd1.outputData(scr1.score1);
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						stage = 0;
						timer.stop();
						repaint();
					} else if (stage == 4) {
						stage = 1;
						repaint();
					}
					break;
				case KeyEvent.VK_S:
					if (stage == 1) {
						stage = 4;
						timer.stop();
						repaint();
					}
					break;
				}
			}
		});
	}

	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			repaint();
		}
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		// menu
		if (stage == 0) {
			menuBack.paintIcon(this, g, 0, 0);
			// menu button
			if (select > 0) {
				g.drawLine(160, 330 + 60 * (select - 1), 310,
						330 + 60 * (select - 1));
				selectbar[0].paintIcon(this, g, 160, 330 + 60 * (select - 1));
			}
		}
		// bar page
		else if (stage == 1) {
			// judge if you can play the game
			if (gameOver == false) {
				// bar machine picture
				background.paintIcon(this, g, 0, 0);
				if (enterPushed == false) {
					button[0].paintIcon(this, g, 300, 420);
				}
				// when push button timer start
				if (enterPushed == true) {
					button[1].paintIcon(this, g, 300, 440);
					count += 1;
					newdelay += 100;

					timer.setDelay(newdelay);
					if (newdelay >= 800 && stage == 1) {
						enterPushed = false;
						timer.stop();
						newdelay = 0;
					}
				}
				// create random picture
				rp1.storeRandom(scr1.cheatQuestion, scr1.cheatcherry);
				// bar rules
				rul1.prizeRule(rp1.bar);
				// if spin 10 times renew score
				if (count < 8) {
					g.setFont(largeFont);
					g.drawString(String.valueOf(scr1.score1), 850, 125);
				}
				// create barpicture
				barPictrue[rp1.bar[0][0]].paintIcon(this, g, x + 50, y + 34);
				barPictrue[rp1.bar[1][0]].paintIcon(this, g, x + 50, y + 150);
				barPictrue[rp1.bar[2][0]].paintIcon(this, g, x + 50, y + 266);
				barPictrue[rp1.bar[0][1]].paintIcon(this, g, x + 300, y + 34);
				barPictrue[rp1.bar[1][1]].paintIcon(this, g, x + 300, y + 150);
				barPictrue[rp1.bar[2][1]].paintIcon(this, g, x + 300, y + 266);
				barPictrue[rp1.bar[0][2]].paintIcon(this, g, x + 550, y + 34);
				barPictrue[rp1.bar[1][2]].paintIcon(this, g, x + 550, y + 150);
				barPictrue[rp1.bar[2][2]].paintIcon(this, g, x + 550, y + 266);

				if (count >= 8) {

					g.setFont(largeFont);
					scr1.storeScore(rul1.prize1);
					g.drawString(String.valueOf(scr1.score1), 850, 125);
					for (int i = 0; i < 8; i++) {
						g.setFont(largerFont);
						switch (rul1.prize1[i]) {
						// score field
						case 0:
							break;
						case 1:

							barscoreIcon[0]
									.paintIcon(this, g, x + 810, y + 250);
							g.drawString("+10000", x + 900, y + 285);
							break;
						case 2:
							barscoreIcon[1]
									.paintIcon(this, g, x + 810, y + 300);
							g.drawString("+1000", x + 900, y + 335);
							break;
						case 3:
							barscoreIcon[2]
									.paintIcon(this, g, x + 810, y + 350);
							g.drawString("???", x + 920, y + 385);
							break;
						case 4:
							barscoreIcon[3]
									.paintIcon(this, g, x + 810, y + 400);
							g.drawString("100", x + 920, y + 435);
							break;
						case 5:
							barscoreIcon[4]
									.paintIcon(this, g, x + 810, y + 450);
							g.drawString("+50", x + 920, y + 485);

							break;
						case 6:
							barscoreIcon[5]
									.paintIcon(this, g, x + 810, y + 500);
							g.drawString("double", x + 900, y + 535);
							break;
						case 7:
							barscoreIcon[6]
									.paintIcon(this, g, x + 810, y + 515);
							g.drawString("triple", x + 900, y + 545);
							break;
						case 8:
							barscoreIcon[0]
									.paintIcon(this, g, x + 810, y + 250);
							g.drawString("+10000", x + 900, y + 285);
							break;
						case 9:
							barscoreIcon[2]
									.paintIcon(this, g, x + 810, y + 350);
							g.drawString("???", x + 920, y + 385);
							break;
						case 10:
							barscoreIcon[4]
									.paintIcon(this, g, x + 810, y + 450);
							g.drawString("+100", x + 920, y + 485);
							break;
						}
					}
					button[0].paintIcon(this, g, 300, 420);
					count = 0;
					rp1.cheat777 = false;
				}
				for (int i = 0; i < 8; i++) {
					rul1.prize1[i] = 0;
				}
				
			} else if (gameOver == true) {
				//gameover situation
				timer.setDelay(750);
				gameopIcon[countTime].paintIcon(this, g, 0, 0);
				countTime += 1;
				if (countTime == 5) {
					countTime = 0;
					stage = 0;
					timer.setDelay(delay);
				}
			}
		} else if (stage == 2) {
			stage = 1;
			repaint();
		} else if (stage == 3) {
			System.exit(0);
		} else if (stage == 4) {
			shopIcon.paintIcon(this, g, 0, 0);
			if (select > 0) {
				//shop
				if (select == 1) {	
					//cherry
					if(scr1.cheatcherry==false)
					{
						itemPictrueIcon[0].paintIcon(this, g, 120, 130);
					}
					else
					{
						itemPictrueIcon[0].paintIcon(this, g, 120, 130);
						itemPictrueIcon[3].paintIcon(this, g, 170, 225);
					}
										
				} else if (select == 2) {
					//question
					if(scr1.cheatQuestion==false)
					{
						itemPictrueIcon[1].paintIcon(this, g, 120, 130);
					}
					else
					{
						itemPictrueIcon[1].paintIcon(this, g, 120, 130);
						itemPictrueIcon[3].paintIcon(this, g, 170, 225);
					}
				} else if (select == 3) {
					//seven
					if(rp1.cheat777==false)
					{
						itemPictrueIcon[2].paintIcon(this, g, 120, 130);
					}
					else
					{
						itemPictrueIcon[2].paintIcon(this, g, 120, 130);
						itemPictrueIcon[3].paintIcon(this, g, 170, 225);
					}
				}
				//arrow
				selectbar[1].paintIcon(this, g, 600, 55 + 45 * (select - 1));
				g.setFont(largeFont);
				g.drawString("����:" + String.valueOf(scr1.score1), 630, 25);
				repaint();
			}
		}
	}
}
