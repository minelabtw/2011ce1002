package fp.s995002201;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

public class FrameWork extends JPanel implements ActionListener , KeyListener , Runnable
{
	 /**碰撞部分有參考 http://my.csdn.net/zy950/code/detail/1048*/
	 private static final long serialVersionUID = 1L;
	 //呼叫function
     gameovermusic gameover = new gameovermusic();
     caldistance calsp = new caldistance();
     downthing down = new downthing();
     calscore cals = new calscore();
     music playmusic = new music();
     caltime calt = new caltime();
     MyPanel mp = new MyPanel();
     Car car1 = new Car();
     Car2 car2 = new Car2();
     Car3 car3 = new Car3();
     //變數設定命名
	 protected int settime = 1000;
	 Timer timer = new Timer(settime, new TimerListener());
	 private JButton startbutton = new JButton("開始遊戲<SPACE>");
	 private JButton stopbutton = new JButton("離開遊戲<ESC>");
	 protected int firstspeed = 100;
	 protected int score = 0;
	 private JLabel nowspeed = new JLabel("目前距離 : " + "0" + " 公尺");
	 private JLabel Score = new JLabel("分數 : " + score);
	 private JLabel time = new JLabel("時間 : " + "0:0:0");
	 String yourname = JOptionPane.showInputDialog("<html><font size=16>請輸入遊戲者名稱 ：");	
	 String player = JOptionPane.showInputDialog("<html><font size=16>請輸入遊戲者人數 １∼２：");	
	 private JLabel Name = new JLabel("遊戲者名稱 : " + yourname);
     Thread nThread;
     protected int man = Integer.parseInt(player);//依照使用者輸入的數字對應
	 public JPanel panel_1 = new JPanel(new GridLayout(3,1));
	 public static final int LINE_LEN=29; //邊長
	 public static final int NUM=20; //各數num*num
	 private static final int EDGE_WIDTH=25; //編距
	 private BufferedImage img;
	 private BufferedImage img1;
	 private BufferedImage img2;
	 
	 public FrameWork()
	 {
		//遊戲開始前的提示訊息
		//<html><font size=16>可以設定字體大小
		//start按鈕如果按一次以上，速度會重疊
		if(man==1)//單人遊戲提示訊息
		{
			JOptionPane.showMessageDialog(null,"<html><font size=16>直接按視窗按鈕開始\n\n" +
										   "<html><font size=16>P暫停,空白鍵繼續\n\n" +
										   "<html><font size=16>鍵盤左右控制\n\n" +
										   "<html><font size=16>遊戲結束可按ESC鍵關閉視窗\n\n" +
										   "<html><font size=16>距離每增加100m速度增加\n\n");
		}
		if(man==2)//雙人遊戲提示訊息
		{
			JOptionPane.showMessageDialog(null,"<html><font size=16>直接按視窗按鈕開始\n\n" +
					   					   "<html><font size=16>P暫停,空白鍵繼續\n\n" +
										   "<html><font size=16>遊戲者1鍵盤左右控制\n\n" +
					   					   "<html><font size=16>遊戲者2鍵盤A,D控制\n\n" +
					   					   "<html><font size=16>其中一方死亡遊戲結束\n\n" +
										   "<html><font size=16>遊戲結束可按ESC鍵關閉視窗\n\n" +
										   "<html><font size=16>距離每增加100m速度增加\n\n");
		}
		try //gameover的圖
	    {
	       img=ImageIO.read(new File("C:/eclipse/workspace/FinalProject/src/gameover1.jpg"));
	    }
	    catch (IOException e)
	    {
	    	e.printStackTrace();//若開圖沒有成功則丟出exception
	    }	
		try //背景的圖1
	    {
	       img1=ImageIO.read(new File("C:/eclipse/workspace/FinalProject/src/back.jpg"));
	    }
	    catch (IOException e)
	    {
	    	e.printStackTrace();//若開圖沒有成功則丟出exception
	    }	 
		try //背景的圖2
	    {
	       img2=ImageIO.read(new File("C:/eclipse/workspace/FinalProject/src/back2.jpg"));
	    }
	    catch (IOException e)
	    {
	    	e.printStackTrace();//若開圖沒有成功則丟出exception
	    }	 
		setSize((LINE_LEN+1)*NUM+EDGE_WIDTH, (LINE_LEN+1)*NUM+EDGE_WIDTH);		
		nowspeed.setFont(new Font("BOLD", Font.BOLD, 20));
		Score.setFont(new Font("BOLD", Font.BOLD, 20));
		Name.setFont(new Font("BOLD", Font.BOLD, 20));
		time.setFont(new Font("BOLD", Font.BOLD, 20));
		startbutton.setFont(new Font("sansserif", Font.BOLD, 20));
		stopbutton.setFont(new Font("sansserif", Font.BOLD, 20));
		panel_1.add(nowspeed);
		panel_1.add(Score);
		panel_1.add(Name);
		panel_1.add(time);
		panel_1.add(startbutton);
		panel_1.add(stopbutton);
		startbutton.addActionListener(this);
		stopbutton.addActionListener(this);
		setFocusable(true);
		this.addKeyListener(this);

		add(panel_1);
	 }
	 public void paint(Graphics g) 
	 {
		 super.paint(g);
		 if(!accident())//如果死掉了
		 {
			 repaint();//把原來的圖蓋掉
			 System.out.println("ohoh");
			 g.clearRect(0, 0, getSize().width, getSize().height);//把背景清成白色
			 g.drawImage(img,110,10,null);//放一張gameover的圖
			 playmusic.stop();
			 gameover.play();
			 return;
		 }
		 if(accident())//還沒死,遊戲正常繼續
		 {
			 //1人遊戲和2人遊戲的圖片背景不同
			 if(man==1)
			 {
				  g.clearRect(0, 0, getSize().width, getSize().height);
				  //mp.drawBkgnd(g);//格子背景
			      g.drawImage(img1,20,30,null);//圖片背景1
			      mp.drawRoad(g);//路
			      car1.paintComponent(g);//車
			      Checkdie();//看看遊戲是否還正常運作,還沒死就繼續落下障礙物
			      down.paintComponent(g);//障礙物
			 }
			 if(man==2)
			 {
				  g.clearRect(0, 0, getSize().width, getSize().height);
				  //mp.drawBkgnd(g);//格子背景
			      g.drawImage(img2,50,30,null);//圖片背景2
			      mp.drawRoad(g);//路
			      car2.paintComponent(g);//車
			      car3.paintComponent(g);
			      Checkdie();//看看遊戲是否還正常運作,還沒死就繼續落下障礙物
			      down.paintComponent(g);//障礙物
			 }
		 }
	 }
	 private class TimerListener implements ActionListener
	 { 
	 	 // while the timer action every time
		 public void actionPerformed(ActionEvent e)
		 {
			 calt.setCurrentClock();
			 time.setText("時間 : " + calt.getHour() +":"+ calt.getMinute() +":"+ calt.getSecond());
			 cals.setCurrentScore();
			 Score.setText("分數 : " + cals.getScore());
			 calsp.setCurrentdistance();
			 nowspeed.setText("目前距離 : " + calsp.getdistance() + " 公尺");
		 }
	 }
	 private void Checkdie()
     {
		 //如果可以正常跑到最下面沒有碰到車子
		 //就把舊的圖片清空,另外呼叫一張新的障礙圖片
         if(down.getY()+58>=591)
         {
             down=null;
             down=new downthing();
         }
     }
	 @SuppressWarnings("deprecation")//nThread.stop()的驚嘆號
	 private boolean accident()
     {
		 if(man==1)//單人遊戲部分
		 {
			 //碰到左上方
			 boolean leftup = down.getX()<car1.getX() && 
         			          down.getX()+117>=car1.getX() &&
        			          down.getY()<=car1.getY() &&
        			          down.getY()>=car1.getY();
	         //碰到右上方
	         boolean rightup = down.getX()>car1.getX() &&
	        			       down.getX()<car1.getX()+58 &&
	        			       down.getY()>=car1.getY() &&
	        			       down.getY()<=car1.getY();
	         //任何一個符合就是撞到牆壁,死掉了
	         if(leftup||rightup)
	         {
	             nThread.stop();
	             timer.stop();
	             down=null;
	             car1=null;
	             mp=null;
	             return false;
	         }
		 }
		 if(man==2)//雙人遊戲部分
		 {
			 //car1碰到左上方
			 boolean leftup = down.getX()<car3.getX() && 
					 		  down.getX()+117>=car3.getX() &&
   			                  down.getY()<=car3.getY() &&
   			                  down.getY()>=car3.getY();
             //car1碰到右上方
             boolean rightup = down.getX()>car3.getX() &&
       			               down.getX()<car3.getX()+58 &&
       			               down.getY()>=car3.getY() &&
       			               down.getY()<=car3.getY();
			 //car2碰到左上方
			 boolean leftup2 = down.getX()<car2.getX() && 
					 		   down.getX()+117>=car2.getX() &&
        			           down.getY()<=car2.getY() &&
        			           down.getY()>=car2.getY();
	         //car2碰到右上方
	         boolean rightup2 = down.getX()>car2.getX() &&
	        			        down.getX()<car2.getX()+58 &&
	        			        down.getY()>=car2.getY() &&
	        			        down.getY()<=car2.getY();
	         //任何一個符合就是撞到牆壁,死掉了
	         if(leftup||rightup||leftup2||rightup2)
	         {
	             nThread.stop();
	             timer.stop();
	             down=null;
	             car3=null;
	             car2=null;
	             mp=null;
	             return false;
	         }
		 }
 		 return true;
     }
	 @SuppressWarnings("deprecation")
	 public void actionPerformed(ActionEvent e) 
	 {
		 if(e.getSource() == startbutton)
		 {
	 		playmusic.run();
	 		timer.start();
			nThread=new Thread(this);
            nThread.start();
            this.requestFocus();
            repaint();
		 }
		 if(e.getSource() == stopbutton)
		 {
			 System.exit(0);
			 timer.stop();
			 nThread.stop();
		}
	}
	@SuppressWarnings("deprecation")
	public void keyPressed(KeyEvent e) 
	{
		// TODO Auto-generated method stub
	    switch (e.getKeyCode()) 
	    {
	        case KeyEvent.VK_LEFT:
	            car1.Moveleft();
	            car3.Moveleft();
	            System.out.println("left");
	            break;
	        case KeyEvent.VK_RIGHT:
	            car1.Moveright();
	            car3.Moveright();
	            System.out.println("right");
	            break;
	        case KeyEvent.VK_P:
	        	timer.stop();
				nThread.stop();
	            System.out.println("stop");
	            break;
	        case KeyEvent.VK_A:
	            car2.Moveleft();
	            System.out.println("left");
	            break;
	        case KeyEvent.VK_D:
	            car2.Moveright();
	            System.out.println("right");
	            break;
	        case KeyEvent.VK_SPACE:
	        	timer.start();
				nThread=new Thread(this);
	            nThread.start();
	            this.requestFocus();
	            repaint();
	            System.out.println("restart");
	            break;
	        case KeyEvent.VK_ESCAPE:
	        	System.exit(0);
	            System.out.println("over");
	            break;
	        default:
	            break;
        }      
	}

	public void keyReleased(KeyEvent e)
	{
		// TODO Auto-generated method stub
		
	}

	public void keyTyped(KeyEvent e)
	{
		// TODO Auto-generated method stub
		
	}
	
	public void run() 
	{
        while(true)
        {
            try 
            {
            	if(calsp.getdistance()>=0 && calsp.getdistance()<100)
            	{
            		Thread.sleep(100);//改這會改變速度
            	}
            	else if(calsp.getdistance()>=100 && calsp.getdistance()<200)
            	{
            		Thread.sleep(90);//改這會改變速度
            	}
            	else if(calsp.getdistance()>=200 && calsp.getdistance()<300)
            	{
            		Thread.sleep(80);//改這會改變速度
            	}
            	else if(calsp.getdistance()>=300 && calsp.getdistance()<400)
            	{
            		Thread.sleep(70);//改這會改變速度
            	}
            	else if(calsp.getdistance()>=400 && calsp.getdistance()<500)
            	{
            		Thread.sleep(60);//改這會改變速度
            	}
            	else if(calsp.getdistance()>=500 && calsp.getdistance()<600)
            	{
            		Thread.sleep(50);//改這會改變速度
            	}
            	else if(calsp.getdistance()>=600 && calsp.getdistance()<700)
            	{
            		Thread.sleep(40);//改這會改變速度
            	}
            	else if(calsp.getdistance()>=700 && calsp.getdistance()<800)
            	{
            		Thread.sleep(30);//改這會改變速度
            	}
            	else if(calsp.getdistance()>=800 && calsp.getdistance()<900)
            	{
            		Thread.sleep(20);//改這會改變速度
            	}
            	else if(calsp.getdistance()>=900 && calsp.getdistance()<1000)
            	{
            		Thread.sleep(10);//改這會改變速度
            	}
            	else if(calsp.getdistance()>=1000 && calsp.getdistance()<1100)
            	{
            		Thread.sleep(0);//改這會改變速度
            	}
            	else if(calsp.getdistance()>=1100 && calsp.getdistance()<1200)
            	{
            		Thread.sleep(-10);//改這會改變速度
            	}
            	else if(calsp.getdistance()>=1500 && calsp.getdistance()<1600)
            	{
            		Thread.sleep(-20);//改這會改變速度,應該不會玩到這裡
            	}
            }
            catch (InterruptedException e) 
            {
                e.printStackTrace();
            }
            repaint();
        }
    }
}