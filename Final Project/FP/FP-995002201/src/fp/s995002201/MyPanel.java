package fp.s995002201;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.Timer;

public class MyPanel extends JPanel implements ActionListener
{
	 /**
	 * 
	 */
	 private static final long serialVersionUID = 1L;
	 private Car car1 = new Car();
	 private int timespeed = 75;
	 private Timer timer = new Timer(timespeed,this);
	 private boolean flag=false; //交替畫出黑白方塊
	 private int num=FrameWork.NUM;
	 private int len=FrameWork.LINE_LEN;
	 
	 public MyPanel()
	 {
		 
	 }
	 public void start()
	 {
	 	 timer.start();
	 }
	 public void stop()
	 {
 	 	 timer.stop();
 	 }
	 public void actionPerformed(ActionEvent e) 
	 {
		// TODO Auto-generated method stub
		repaint();
	 }
	 public void setSpeed(int speed)
	 {
	    timer.setDelay(speed);
	 }
	 public void paint(Graphics g) 
	 {
	     g.clearRect(0, 0, getSize().width, getSize().height);
	     drawBkgnd(g);//背景
	     drawRoad(g);//路
	     car1.paintComponent(g);//車
	 }
	 
	 void drawBkgnd(Graphics g)
	 {
		 for(int i=0;i<=num;i++)
	     {
	    	 g.drawLine(0, i*len, len*num, i*len);
	    	 g.drawLine(i*len, 0, i*len, len*num);
	     }    
	 }
	 /*** 另一種不是用開圖片的障礙物寫法,原來的寫法
	 int temp = (int)(Math.random()*14+1);//從最左邊到最右邊會出現的全部可能
	 int temp1 = (int)(Math.random()*14+1);//從最左邊到最右邊會出現的全部可能
	 int temp2 = (int)(Math.random()*14+1);//從最左邊到最右邊會出現的全部可能
	 
	 void drawRock(Graphics g)//隨機畫畫面會出現的障礙物,速度慢的時候可以,速度快的時候..
	 {
		 if(a<20)
		 {
			 g.fillRect((move*temp),(move*a), move*3,move);//1 往右出來幾格,2 往下幾格,3 橫的格數,4 往下複製幾倍
			 g.fillRect((move*temp1),(move*a), move*3,move);//1 往右出來幾格,2 往下幾格,3 橫的格數,4 往下複製幾倍
			 g.fillRect((move*temp2),(move*a), move*1,move);//1 往右出來幾格,2 往下幾格,3 橫的格數,4 往下複製幾倍
			 a++;
		 }
		 if(a>=20)//這樣障礙可以循環出現
		 {
			 int ran = (int)(Math.random()*14+1);//從最左邊到最右邊會出現的全部可能
			 int ran1 = (int)(Math.random()*14+1);//從最左邊到最右邊會出現的全部可能
			 int ran2 = (int)(Math.random()*14+1);//從最左邊到最右邊會出現的全部可能
			 
			 temp = ran;//每20次出現在新的不同地方
			 temp1 = ran1;//第二道障礙
			 temp2 = ran2;//第三道障礙
			 a=0;//每20次歸零
		 }
	 }
	 ***/
	 void drawRoad(Graphics g)//畫出左右兩邊的圍牆道路
	 {
	     if(flag==true)
	     { 
	    	 flag=false;
	    	 for(int i=0;i<num;i+=2)
	    	 {
	    		 g.fillRect(0, i*len, len, len);
	    		 g.fillRect((num-1)*len, i*len, len, len);
	    	 }
	     }
	     else if(flag==false)
	     {
	    	 flag=true;
	    	 for(int i=1;i<num;i+=2)
	    	 {
	    		 g.fillRect(0, i*len, len, len);
	    		 g.fillRect((num-1)*len, i*len, len, len);
	    	 }
	     }
	 }
}