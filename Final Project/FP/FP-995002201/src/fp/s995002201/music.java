package fp.s995002201;
import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JOptionPane;
/***背景音樂播放部分有參考此網址：http://stackoverflow.com/questions/9701631/play-music-on-the-same-time-with-my-jframe*/

public class music implements Runnable 
{
	private File soundFile;
	private Clip clip;
	private Runnable play;
	
	public music(File soundFile)
	{
		this.soundFile = soundFile;
	}
	
	public music()
	{
	    soundFile = new File("C:/eclipse/workspace/FinalProject/src/mario.mid");
	    new Thread(play).start();
	}
	
	public void prepare()
	{
		try
		{
			AudioInputStream soundIn = AudioSystem.getAudioInputStream(soundFile);
			AudioFormat format = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED,AudioSystem.NOT_SPECIFIED,16,2,4,AudioSystem.NOT_SPECIFIED, true);
			DataLine.Info info = new DataLine.Info(Clip.class, format);
		
			clip = (Clip)AudioSystem.getLine(info);
			clip.open(soundIn);
		
		}
		catch(IOException e)
		{
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		catch(UnsupportedAudioFileException e)
		{
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		catch(LineUnavailableException e)
		{
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}
	
	public void run() 
	{
	    prepare();
	    clip.loop(10);//重複播放,因為不知道會玩到哪裡,所以播多次一點
	}
	
	public void play()
	{
	    new Thread(this).start();
	}
	
	public void stop()
	{
		clip.stop();
	}
}