package fp.s100502016;

import java.awt.Image;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;

public class Menu {

	private int select = 0;
	private boolean isSelected[] = { true, false, false, false };
	private ImageIcon imgIconStart_0 = new ImageIcon("images/start_0.png");
	private ImageIcon imgIconGuide_0 = new ImageIcon("images/guide_0.png");
	private ImageIcon imgIconExit_0 = new ImageIcon("images/exit_0.png");
	private ImageIcon imgIconStart_1 = new ImageIcon("images/start_1.png");
	private ImageIcon imgIconGuide_1 = new ImageIcon("images/guide_1.png");
	private ImageIcon imgIconExit_1 = new ImageIcon("images/exit_1.png");
	private ImageIcon imgIconRank_0 = new ImageIcon("images/rank_0.png");
	private ImageIcon imgIconRank_1 = new ImageIcon("images/rank_1.png");

	ImageIcon imgIconBackground = new ImageIcon("images/menu_background.jpg");

	private Image imgStart;
	private Image imgGuide;
	private Image imgRank;
	private Image imgExit;
	Image imgBackground = imgIconBackground.getImage();

	Menu() {
		imgStart = imgIconStart_1.getImage();
		imgGuide = imgIconGuide_0.getImage();
		imgRank = imgIconRank_0.getImage();
		imgExit = imgIconExit_0.getImage();
		int select = 0;
	}

	public int getSelect() {
		return select;
	}

	public Image getImgStart() {
		if (isSelected[0] == true) {
			imgStart = imgIconStart_1.getImage();
			return imgStart;
		} else {
			imgStart = imgIconStart_0.getImage();
			return imgStart;
		}
	}

	public Image getImgGuide() {
		if (isSelected[1] == true) {
			imgGuide = imgIconGuide_1.getImage();
			return imgGuide;
		} else {
			imgGuide = imgIconGuide_0.getImage();
			return imgGuide;
		}
	}

	public Image getImgRank() {
		if (isSelected[2] == true) {
			imgRank = imgIconRank_1.getImage();
			return imgRank;
		} else {
			imgRank = imgIconRank_0.getImage();
			return imgRank;
		}
	}

	public Image getImgExit() {
		if (isSelected[3] == true) {
			imgExit = imgIconExit_1.getImage();
			return imgExit;
		} else {
			imgExit = imgIconExit_0.getImage();
			return imgExit;
		}
	}

	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_UP) {
			if (select > 0)
				select -= 1;
			switch (select) {
			case 0:
				isSelected[0] = true;
				isSelected[1] = false;
				isSelected[2] = false;
				isSelected[3] = false;
				break;
			case 1:
				isSelected[0] = false;
				isSelected[1] = true;
				isSelected[2] = false;
				isSelected[3] = false;
				break;
			case 2:
				isSelected[0] = false;
				isSelected[1] = false;
				isSelected[2] = true;
				isSelected[3] = false;
				break;

			}

		} else if (key == KeyEvent.VK_DOWN) {
			if (select < 3)
				select += 1;
			switch (select) {
			case 1:
				isSelected[0] = false;
				isSelected[1] = true;
				isSelected[2] = false;
				isSelected[3] = false;
				break;
			case 2:
				isSelected[0] = false;
				isSelected[1] = false;
				isSelected[2] = true;
				isSelected[3] = false;
				break;
			case 3:
				isSelected[0] = false;
				isSelected[1] = false;
				isSelected[2] = false;
				isSelected[3] = true;
				break;
			}

		}

	}
}
