package fp.s100502016;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.Timer;

public class SmallGila {
	protected int x;
	protected int y;
	protected int w;
	protected int h;
	protected int dx;
	protected int dy;
	protected int state; // 0 = normal | 1 = injury | 2 = invoked
	protected int direction; // 0 = Left | 1 = Right
	protected int damage;
	protected Image img;
	protected int currentFrame;
	protected int maxFrame;
	protected int hp;
	protected ImageIcon[] imgIcon = { new ImageIcon("images/smallgilla_0.png"),
			new ImageIcon("images/smallgilla_1.png"),
			new ImageIcon("images/smallgilla_2.png"),
			new ImageIcon("images/smallgilla_3.png") };

	protected ImageIcon[] imgIcon_mirror = {
			new ImageIcon("images/smallgilla_mirror_0.png"),
			new ImageIcon("images/smallgilla_mirror_1.png"),
			new ImageIcon("images/smallgilla_mirror_2.png"),
			new ImageIcon("images/smallgilla_mirror_3.png") };

	protected ImageIcon imgIcon_injured = new ImageIcon(
			"images/smallgilla_injured.png");
	protected ImageIcon imgIcon_mirror_injured = new ImageIcon(
			"images/smallgilla_mirror_injured.png");

	protected boolean isAlive;

	public SmallGila(int x, int y, int direction) {
		this.hp = 40;
		this.x = x;
		this.y = y;
		this.w = imgIcon[0].getIconWidth();
		this.h = imgIcon[0].getIconHeight();
		this.direction = direction;
		currentFrame = 0;
		maxFrame = 4;
		isAlive = true;
		img = imgIcon[currentFrame].getImage();
		currentFrame = 0;
		damage = 5;
	}

	protected int updateDelay = 300;
	protected Timer timer = new Timer(updateDelay, new update());

	protected class update implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			currentFrame++;
			if (currentFrame > maxFrame - 1)
				currentFrame = 0;
			dx = (int) (-Math.random() * 6);
			dy = (int) (-Math.random() * 4);
		}
	}

	protected int restoreDelay = 1000;
	protected Timer restoreTimer = new Timer(restoreDelay, new restore());

	protected class restore implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			state = 0;
			restoreTimer.stop();
		}
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getW() {
		return w;
	}

	public int getH() {
		return h;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public int getDamage() {
		return damage;
	}

	public boolean getisAlive() {
		return isAlive;
	}

	public void Move(int TargetX, int TargetY) {
		timer.start();
		state = 0;

		if (x >= 0 && x + imgIcon[0].getIconWidth() <= 800 && y >= 180
				&& y + imgIcon[0].getIconHeight() <= 600) {
			if (TargetX <= x) {
				x += dx;
				direction = 1;
			} else if (TargetX > x) {
				x -= dx;
				direction = 0;
			}
			if (TargetY <= y) {
				y += dy;
			} else if (TargetY > y) {
				y -= dy;
			}
		} else {
			if (x <= 0)
				x += 1;
			else if (x + imgIcon[0].getIconWidth() >= 800)
				x -= 1;
			else if (y <= 180)
				y += 1;
			else if (y + imgIcon[0].getIconHeight() >= 600)
				y -= 1;
		}
	}

	public Image getImg() {
		if (state == 0) {
			if (direction == 1) {
				img = imgIcon[currentFrame].getImage();
				return img;
			} else {
				img = imgIcon_mirror[currentFrame].getImage();
				return img;
			}
		} else {
			if (direction == 1) {
				img = imgIcon_injured.getImage();
				return img;
			} else {
				img = imgIcon_mirror_injured.getImage();
				return img;
			}
		}
	}

	public int getCurrentFrame() {
		return currentFrame;
	}

	public void Collide0(ArrayList<Snowball> snowballs) {

		for (int i = 0; i < snowballs.size(); i++) {
			int x2 = snowballs.get(i).getX();
			int y2 = snowballs.get(i).getY();
			int w2 = snowballs.get(i).getW();
			int h2 = snowballs.get(i).getH();
			if (Math.abs((x + w / 2) - (x2 + w2 / 2)) < Math.abs((w + w2) / 2)
					&& Math.abs((y + h / 2) - (y2 + h2 / 2)) < Math
							.abs((h + h2) / 2)) {
				restoreTimer.start();
				hp -= snowballs.get(i).getDamage();
				state = 1;
				snowballs.remove(i);
				if (direction == 0)
					this.x = this.x - 15;
				else
					this.x = this.x + 15;
				timer.stop();
				if (hp <= 0) {
					isAlive = false;
				}
			}
		}
	}
	public void Collide1(ArrayList<Rocket> rockets) {

		for (int i = 0; i < rockets.size(); i++) {
			int x2 = rockets.get(i).getX();
			int y2 = rockets.get(i).getY();
			int w2 = rockets.get(i).getW();
			int h2 = rockets.get(i).getH();
			if (Math.abs((x + w / 2) - (x2 + w2 / 2)) < Math.abs((w + w2) / 2)
					&& Math.abs((y + h / 2) - (y2 + h2 / 2)) < Math
							.abs((h + h2) / 2)) {
				restoreTimer.start();
				Rocket.isBoom = true;
				hp -= rockets.get(i).damage;
				state = 1;
				rockets.remove(i);
				if (direction == 0)
					this.x = this.x - 15;
				else
					this.x = this.x + 15;
				timer.stop();
				if (hp <= 0) {
					isAlive = false;
				}
			}
		}
	}
	public void Collide2(ArrayList<Sickle> sickles) {

		for (int i = 0; i < sickles.size(); i++) {
			int x2 = sickles.get(i).getX();
			int y2 = sickles.get(i).getY();
			int w2 = sickles.get(i).getW();
			int h2 = sickles.get(i).getH();
			if (Math.abs((x + w / 2) - (x2 + w2 / 2)) < Math.abs((w + w2) / 2)
					&& Math.abs((y + h / 2) - (y2 + h2 / 2)) < Math
							.abs((h + h2) / 2)) {
				restoreTimer.start();
				hp -= sickles.get(i).damage;
				state = 1;
				sickles.remove(i);
				if (direction == 0)
					this.x = this.x - 15;
				else
					this.x = this.x + 15;
				timer.stop();
				if (hp <= 0) {
					isAlive = false;
				}
			}
		}
	}
	public void Collide3(ArrayList<Shit> shits) {

		for (int i = 0; i < shits.size(); i++) {
			int x2 = shits.get(i).getX();
			int y2 = shits.get(i).getY();
			int w2 = shits.get(i).getW();
			int h2 = shits.get(i).getH();
			if (Math.abs((x + w / 2) - (x2 + w2 / 2)) < Math.abs((w + w2) / 2)
					&& Math.abs((y + h / 2) - (y2 + h2 / 2)) < Math
							.abs((h + h2) / 2)) {
				restoreTimer.start();
				hp -= shits.get(i).damage;
				state = 1;
				shits.remove(i);
				if (direction == 0)
					this.x = this.x - 15;
				else
					this.x = this.x + 15;
				timer.stop();
				if (hp <= 0) {
					isAlive = false;
				}
			}
		}
	}
}
