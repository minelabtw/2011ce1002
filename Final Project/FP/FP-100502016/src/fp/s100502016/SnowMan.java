package fp.s100502016;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import java.util.ArrayList;

import javax.swing.ImageIcon;

import javax.swing.Timer;

public class SnowMan {
	protected int x;
	protected int y;
	protected int w;
	protected int h;
	protected int rx; // rx : realx, is set to fix the collide problem , which
						// makes snomMan's range more narrow
	protected int ry;
	protected int rw;
	protected int rh;
	protected int dx;
	protected int dy;
	protected int timerDelay = 200;
	protected int currentFrame = 0;
	protected int state; // State 0 = normal | 1 = PowerUp | 2 = injured
	private int weapon; // 0 = snowball | 1 = rocket | 2=sickle | 3=shit
	private int weaponCount[] = { 0, 0, 0, 0 };
	private int direction; // 0 = Right | 1 = Left
	private int frameDelay = 500;
	private int hp;
	private boolean isUnlimited;
	private boolean isAlive;
	private boolean isUntouchable;

	PowerBar powerBar = new PowerBar();

	private Image img;
	private ImageIcon[] imgIcon = { new ImageIcon("images/alex_0.png"),
			new ImageIcon("images/alex_1.png"),
			new ImageIcon("images/alex_2.png") };

	private ImageIcon[] imgIcon_mirror = {
			new ImageIcon("images/alex_mirror_0.png"),
			new ImageIcon("images/alex_mirror_1.png"),
			new ImageIcon("images/alex_mirror_2.png") };

	private ImageIcon imgIcon_throw = new ImageIcon("images/alex_throw_0.png");
	private ImageIcon imgIcon_throw_mirrir = new ImageIcon(
			"images/alex_throw_mirror_0.png");

	private ImageIcon imgIcon_injured = new ImageIcon("images/alex_injured.png");
	private ImageIcon imgIcon_mirror_injured = new ImageIcon(
			"images/alex_mirror_injured.png");

	static ArrayList<Snowball> snowballs;
	static ArrayList<Rocket> rockets;
	static ArrayList<Sickle> sickles;
	static ArrayList<Shit> shits;

	Timer timer = new Timer(timerDelay, new update());

	public SnowMan(int x, int y) {
		this.x = x;
		this.y = y;
		w = imgIcon[0].getIconWidth();
		h = imgIcon[0].getIconHeight();
		rx = x + 5;
		ry = y + 10;
		rw = w - 5;
		rh = h - 15;
		isAlive = true;
		hp = 100;
		weapon = 0;
		state = 0;
		direction = 0;
		snowballs = new ArrayList();
		rockets = new ArrayList();
		sickles = new ArrayList();
		shits = new ArrayList();
		isUnlimited = false;
		isUntouchable = false;
	}

	public int getRx() {
		return rx;
	}

	public int getRy() {
		return ry;
	}

	public int getWeaponCount(int i) {
		return weaponCount[i];
	}

	public void setUnlimited(boolean isUnlimited) {
		this.isUnlimited = isUnlimited;
	}

	public int getHp() {
		return hp;
	}

	public void setWeaponCount(int type, int amount) {
		this.weaponCount[type] = amount;
	}

	public boolean getIsAlive() {
		return isAlive;

	}

	public int getDirection() {
		return direction;
	}

	public int getWeapon() {
		return weapon;
	}

	public class update implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			currentFrame++;
			if (currentFrame > 2)
				currentFrame = 0;
		}
	}

	protected int restoreDelay = 1500;
	protected Timer restoreTimer = new Timer(restoreDelay, new restore());

	protected class restore implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			state = 0;
			restoreTimer.stop();
			isUntouchable = false;
		}
	}

	public Image getImage() {
		if (state == 0) {
			if (direction == 0) {
				img = imgIcon[currentFrame].getImage();
				return img;
			} else if (direction == 1) {
				img = imgIcon_mirror[currentFrame].getImage();
				return img;
			}
		} else if (state == 1) {
			if (direction == 0) {
				img = imgIcon_throw.getImage();
				return img;
			} else if (direction == 1) {
				img = imgIcon_throw_mirrir.getImage();
				return img;
			}
		} else {
			if (direction == 0) {
				img = imgIcon_injured.getImage();
				return img;
			} else if (direction == 1) {
				img = imgIcon_mirror_injured.getImage();
				return img;
			}
		}
		return img;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void Move() {
		if (x >= 0 && x + imgIcon[0].getIconWidth() <= 800 && y >= 200
				&& y + imgIcon[0].getIconHeight() <= 600) {
			x += dx;
			y += dy;
			rx = x + 5;
			ry = y + 10;
		} else {
			if (x <= 0)
				x += 1;
			else if (x + imgIcon[0].getIconWidth() >= 800)
				x -= 1;
			else if (y <= 200)
				y += 1;
			else if (y + imgIcon[0].getIconHeight() >= 600)
				y -= 1;

		}
	}

	public static ArrayList getsnowballs() {
		return snowballs;
	}

	public static ArrayList getrockets() {
		return rockets;
	}

	public static ArrayList getsickles() {
		return sickles;
	}

	public static ArrayList getshits() {
		return shits;
	}

	public void throwX() {
		if (weapon == 0) {

			Snowball snowball = new Snowball(getX(), getY(), direction,
					((powerBar.getCurrentPower()) - 1) / 20);
			snowballs.add(snowball);

		} else if (weapon == 1 && (weaponCount[1] > 0 || isUnlimited)) {
			if (direction == 0) {
				Rocket rocket = new Rocket(getX() + 60, getY() + 40, direction,
						((powerBar.getCurrentPower()) - 1) / 20);
				rockets.add(rocket);
				weaponCount[1]--;
			} else {
				Rocket rocket = new Rocket(getX() - 60, getY() + 40, direction,
						((powerBar.getCurrentPower()) - 1) / 20);
				rockets.add(rocket);
				weaponCount[1]--;
			}

		} else if (weapon == 2 && (weaponCount[2] > 0 || isUnlimited)) {

			Sickle sickle = new Sickle(getX(), getY(), direction,
					((powerBar.getCurrentPower()) - 1) / 20);
			sickles.add(sickle);
			weaponCount[2]--;

		} else if (weapon == 3 && (weaponCount[3] > 0 || isUnlimited)) {

			Shit shit = new Shit(getX(), getY(), direction,
					((powerBar.getCurrentPower()) - 1) / 20);
			shits.add(shit);
			weaponCount[3]--;

		}
	}

	public void ChangeFrame(int nextDirection) {
		if (nextDirection == 1) {
			if (direction == 0)
				direction = 1;
			timer.start();

		} else {
			if (direction == 1)
				direction = 0;
			timer.start();
		}
	}

	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_LEFT) {
			dx = -2;
			ChangeFrame(1);
		} else if (key == KeyEvent.VK_RIGHT) {
			dx = 2;
			ChangeFrame(0);
		} else if (key == KeyEvent.VK_UP) {
			dy = -1;
			ChangeFrame(direction);
		} else if (key == KeyEvent.VK_DOWN) {
			dy = 1;
			ChangeFrame(direction);
		} else if (key == KeyEvent.VK_RIGHT && key == KeyEvent.VK_UP) {
			dx = 2;
			dy = -1;
			ChangeFrame(0);
		} else if (key == KeyEvent.VK_RIGHT && key == KeyEvent.VK_DOWN) {
			dx = 2;
			dy = 2;
			ChangeFrame(0);
		} else if (key == KeyEvent.VK_LEFT && key == KeyEvent.VK_UP) {
			dx = -2;
			dy = 1;
			ChangeFrame(1);
		} else if (key == KeyEvent.VK_LEFT && key == KeyEvent.VK_DOWN) {
			dx = -2;
			dy = -1;
			ChangeFrame(1);
		} else if (key == KeyEvent.VK_SPACE) {
			if (weapon == 0) {
				state = 1;
			}

		} else if (key == KeyEvent.VK_Z) {
			if (weapon < 4) {
				weapon++;
			}
			if (weapon == 4) {
				weapon = 0;
			}

		}
	}

	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_LEFT) {
			dx = 0;
			timer.stop();
		} else if (key == KeyEvent.VK_RIGHT) {
			dx = 0;
			timer.stop();
		} else if (key == KeyEvent.VK_UP) {
			dy = 0;
			timer.stop();
		} else if (key == KeyEvent.VK_DOWN) {
			dy = 0;
			timer.stop();
		} else if (key == KeyEvent.VK_SPACE) {
			state = 0;
			throwX();
			timer.stop();
		}
	}

	// When Collide with enemies , snowman will be repeled for a distance,and be
	// in the state "restore" , which can't be touch by enemies for one seconds.

	public void Collide0(ArrayList<SmallGila> smallGilas) {
		if (isUntouchable == false) {
			for (int i = 0; i < smallGilas.size(); i++) {
				int x2 = smallGilas.get(i).getX();
				int y2 = smallGilas.get(i).getY();
				int w2 = smallGilas.get(i).getW();
				int h2 = smallGilas.get(i).getH();
				if (Math.abs((x + w / 2) - (x2 + w2 / 2)) < Math
						.abs((w + w2) / 2)
						&& Math.abs((y + h / 2) - (y2 + h2 / 2)) < Math
								.abs((h + h2) / 2)) {
					restoreTimer.start();
					hp -= smallGilas.get(i).getDamage();
					state = 2;
					if (direction == 0)
						this.x = this.x - 15;
					else
						this.x = this.x + 15;
					timer.stop();
					if (hp <= 0) {
						isAlive = false;
					}
					isUntouchable = true;
				}
			}
		}
	}

	public void Collide1(ArrayList<BigGila> bigGilas) {
		if (isUntouchable == false) {
			for (int i = 0; i < bigGilas.size(); i++) {
				int x2 = bigGilas.get(i).getX();
				int y2 = bigGilas.get(i).getY();
				int w2 = bigGilas.get(i).getW();
				int h2 = bigGilas.get(i).getH();
				if (Math.abs((rx + rw / 2) - (x2 + w2 / 2)) < Math
						.abs((rw + w2) / 2)
						&& Math.abs((ry + rh / 2) - (y2 + h2 / 2)) < Math
								.abs((rh + h2) / 2)) {
					restoreTimer.start();
					hp -= bigGilas.get(i).getDamage();
					state = 2;
					if (direction == 0)
						this.x = this.x - 15;
					else
						this.x = this.x + 15;
					timer.stop();
					if (hp <= 0) {
						isAlive = false;
					}
					isUntouchable = true;
				}
			}
		}
	}

	public void Collide2(ArrayList<Wolf> wolfs) {
		if (isUntouchable == false) {
			for (int i = 0; i < wolfs.size(); i++) {
				int x2 = wolfs.get(i).getX();
				int y2 = wolfs.get(i).getY();
				int w2 = wolfs.get(i).getW();
				int h2 = wolfs.get(i).getH();
				if (Math.abs((rx + rw / 2) - (x2 + w2 / 2)) < Math
						.abs((rw + w2) / 2)
						&& Math.abs((ry + rh / 2) - (y2 + h2 / 2)) < Math
								.abs((rh + h2) / 2)) {
					restoreTimer.start();
					hp -= wolfs.get(i).getDamage();
					state = 2;
					if (direction == 0)
						this.x = this.x - 15;
					else
						this.x = this.x + 15;
					timer.stop();
					if (hp <= 0) {
						isAlive = false;
					}
					isUntouchable = true;
				}
			}
		}
	}

	public void Collide4(ArrayList<Penquin> penquins) {
		if (isUntouchable == false) {
			for (int i = 0; i < penquins.size(); i++) {
				int x2 = penquins.get(i).getX();
				int y2 = penquins.get(i).getY();
				int w2 = penquins.get(i).getW();
				int h2 = penquins.get(i).getH();
				if (Math.abs((rx + rw / 2) - (x2 + w2 / 2)) < Math
						.abs((rw + w2) / 2)
						&& Math.abs((ry + rh / 2) - (y2 + h2 / 2)) < Math
								.abs((rh + h2) / 2)) {
					restoreTimer.start();
					hp -= penquins.get(i).getDamage();
					state = 2;
					timer.stop();
					if (hp <= 0) {
						isAlive = false;
					}
					isUntouchable = true;
				}
			}
		}
	}

	public void Collide3(ArrayList<Supply> supplies) {
		for (int i = 0; i < supplies.size(); i++) {
			int x2 = supplies.get(i).getX();
			int y2 = supplies.get(i).getY();
			int w2 = supplies.get(i).getW();
			int h2 = supplies.get(i).getH();
			if (Math.abs((rx + rw / 2) - (x2 + w2 / 2)) < Math
					.abs((rw + w2) / 2)
					&& Math.abs((ry + rh / 2) - (y2 + h2 / 2)) < Math
							.abs((rh + h2) / 2)) {
				switch (supplies.get(i).getType()) {
				case 0:
					weaponCount[1] += 50;
					break;
				case 1:
					weaponCount[2] += 50;
					break;
				case 2:
					weaponCount[3] += 20;
					break;
				case 3:
					hp += 30;
					if (hp >= 100)
						hp = 100;
					break;
				case 4:
					hp -= 30;
					if (hp <= 0)
						hp = 0;
					break;
				}
				supplies.remove(i);
			}
		}
	}

}