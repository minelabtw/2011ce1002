package fp.s100502016;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.Timer;

public class Wolf extends SmallGila {

	protected ImageIcon[] imgIcon = { new ImageIcon("images/wolf_0.png"),
			new ImageIcon("images/wolf_1.png"),
			new ImageIcon("images/wolf_2.png"),
			new ImageIcon("images/wolf_3.png") };

	protected ImageIcon[] imgIcon_mirror = {
			new ImageIcon("images/wolf_mirror_0.png"),
			new ImageIcon("images/wolf_mirror_1.png"),
			new ImageIcon("images/wolf_mirror_2.png"),
			new ImageIcon("images/wolf_mirror_3.png") };

	protected ImageIcon imgIcon_injured = new ImageIcon(
			"images/wolf_injured.png");
	protected ImageIcon imgIcon_mirror_injured = new ImageIcon(
			"images/wolf_mirror_injured.png");

	public Wolf(int x, int y, int direction) {
		super(x, y, direction);
		this.hp = 100;
		this.x = x;
		this.y = y;
		this.w = imgIcon[0].getIconWidth();
		this.h = imgIcon[0].getIconHeight();
		this.direction = direction;
		currentFrame = 0;
		maxFrame = 4;
		isAlive = true;
		img = imgIcon[currentFrame].getImage();
		currentFrame = 0;
		damage = 15;
	}

	protected int updateDelay = 500;
	protected Timer timer = new Timer(updateDelay, new update());

	protected class update implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			currentFrame++;
			if (currentFrame > maxFrame - 1)
				currentFrame = 0;
			dx = (int) (-(Math.random() * 12));
			dy = (int) (-(Math.random() * 7));
		}
	}

	@Override
	public Image getImg() {
		if (state == 0) {
			if (direction == 1) {
				img = imgIcon[currentFrame].getImage();
				return img;
			} else {
				img = imgIcon_mirror[currentFrame].getImage();
				return img;
			}
		} else {
			if (direction == 1) {
				img = imgIcon_injured.getImage();
				return img;
			} else {
				img = imgIcon_mirror_injured.getImage();
				return img;
			}
		}
	}

}
