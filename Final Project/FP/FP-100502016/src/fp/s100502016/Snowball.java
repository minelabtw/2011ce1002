package fp.s100502016;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Snowball {

	protected int x;
	protected int y;
	protected int w;
	protected int h;
	protected int dx;
	protected int dy;
	protected int size;
	protected int direction;
	protected boolean visible;
	protected int damage;

	protected ImageIcon[] imgIcon = { new ImageIcon("images/snowball_0.png"),
			new ImageIcon("images/snowball_1.png"),
			new ImageIcon("images/snowball_2.png"),
			new ImageIcon("images/snowball_3.png"),
			new ImageIcon("images/snowball_4.png") };
	protected Image img;

	public Snowball(int x, int y, int dirction, int size) {
		this.x = x;
		this.y = y;
		w = imgIcon[0].getIconWidth();
		h = imgIcon[0].getIconHeight();
		this.direction = dirction;
		this.size = size;
		img = imgIcon[size].getImage();
		visible = true;
		dx = 3;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getW() {
		return w;
	}

	public int getH() {
		return h;
	}

	public boolean getVisible() {
		return visible;
	}

	public Image getImage() {
		return img;
	}

	public int getDamage() {
		if (size == 0)
			return 10;
		else if (size == 1)
			return 20;
		else if (size == 2)
			return 35;
		else if (size == 3)
			return 50;
		else
			return 100;
	}

	public void Move() {
		if (direction == 0) {
			if (x < 800)
				x += dx;
			else
				visible = false;
		} else if (direction == 1) {
			if (x > 0)
				x -= dx;
			else
				visible = false;
		}
	}

}
