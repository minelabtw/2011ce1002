package fp.s100502016;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Shit extends Snowball {

	protected ImageIcon imgIcon = new ImageIcon("images/shit.png");
	protected ImageIcon imgIcon_mirror = new ImageIcon("images/shit_mirror.png");
	Image img;

	public Shit(int x, int y, int dirction, int size) {
		super(x, y, dirction, size);
		this.x = x;
		this.y = y;
		this.direction = dirction;
		this.size = size;
		visible = true;
		dx = 4;
		damage = 9999;
	}

	@Override
	public Image getImage() {
		if (direction == 0) {
			img = imgIcon.getImage();
			return img;
		} else {
			img = imgIcon_mirror.getImage();
			return img;
		}
	}
	@Override
	public int getDamage() {	
		return damage;
	}

}
