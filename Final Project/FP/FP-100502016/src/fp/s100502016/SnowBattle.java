package fp.s100502016;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.*;

public class SnowBattle extends JFrame {
	
	StageBoard stageBoard = new StageBoard();
	static int state = 0; // 0= nothing | 1 = startGame | 2 = Guide | 3 = About | 4 =
					// Exit

	public SnowBattle() {
		setTitle("SnowBattle");
		setSize(800, 630);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	
		add(stageBoard);	
		stageBoard.setVisible(true);
		stageBoard.setFocusable(true);
		
	}
	public static void main(String arg[]) {

		SnowBattle f = new SnowBattle();
		
	}	
}
