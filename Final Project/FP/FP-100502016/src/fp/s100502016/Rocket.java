package fp.s100502016;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Rocket extends Snowball {

	static ImageIcon imgIcon_rocket = new ImageIcon("images/rocket.png");
	static Image img_rocket = imgIcon_rocket.getImage();
	static ImageIcon imgIcon_mirror_rocket = new ImageIcon(
			"images/rocket_mirror.png");
	static Image img_mirror_rocket = imgIcon_mirror_rocket.getImage();

	static ImageIcon imgIcon_boom = new ImageIcon("images/boom.png");
	static Image img_boom = imgIcon_boom.getImage();
	public static boolean isBoom;

	protected ImageIcon imgIcon = new ImageIcon("images/bullet.png");
	protected ImageIcon imgIcon_mirror = new ImageIcon(
			"images/bullet_mirror.png");

	public Rocket(int x, int y, int dirction, int size) {
		super(x, y, dirction, size);
		this.x = x;
		this.y = y;
		this.direction = dirction;
		this.size = size;
		visible = true;
		dx = 6;
		damage = 60;
	}

	public static Image getImg_rocket() {
		return img_rocket;
	}

	public Image getImage() {
		if (direction == 0) {
			img = imgIcon.getImage();
			return img;
		} else {
			img = imgIcon_mirror.getImage();
			return img;
		}
	}

	@Override
	public int getDamage() {
		return damage;
	}

}
