package fp.s100502016;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.Timer;


public class BigGila extends SmallGila {

	protected ImageIcon[] imgIcon = { new ImageIcon("images/biggilla_0.png"),
			new ImageIcon("images/biggilla_1.png"),
			new ImageIcon("images/biggilla_2.png"),
			new ImageIcon("images/biggilla_3.png") };

	protected ImageIcon[] imgIcon_mirror = {
			new ImageIcon("images/biggilla_mirror_0.png"),
			new ImageIcon("images/biggilla_mirror_1.png"),
			new ImageIcon("images/biggilla_mirror_2.png"),
			new ImageIcon("images/biggilla_mirror_3.png") };
	protected ImageIcon imgIcon_injured = new ImageIcon(
			"images/biggilla_injured.png");
	protected ImageIcon imgIcon_mirror_injured = new ImageIcon(
			"images/biggilla_mirror_injured.png");

	public BigGila(int x, int y, int direction) {
		super(x, y, direction);
		this.hp = 300;
		this.x = x;
		this.y = y;
		this.w = imgIcon[0].getIconWidth();
		this.h = imgIcon[0].getIconHeight();
		this.direction = direction;
		currentFrame = 0;
		maxFrame = 4;
		isAlive = true;
		img = imgIcon[currentFrame].getImage();
		currentFrame = 0;
		damage = 25;
	}
	
	protected int updateDelay = 500;
	protected Timer timer = new Timer(updateDelay, new update());
	protected class update implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			currentFrame++;
			if (currentFrame > maxFrame - 1)
				currentFrame = 0;
			dx = (int) (-(Math.random() * 15));
			dy = (int) (-(Math.random() * 8));
		}
	}
	@Override
	public Image getImg() {
		if (state == 0) {
			if (direction == 1) {
				img = imgIcon[currentFrame].getImage();
				return img;
			} else {
				img = imgIcon_mirror[currentFrame].getImage();
				return img;
			}
		} else {
			if (direction == 1) {
				img = imgIcon_injured.getImage();
				return img;
			} else {
				img = imgIcon_mirror_injured.getImage();
				return img;
			}
		}
	}

}
