package fp.s100502016;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.Timer;

public class Sickle extends Snowball {

	private int currentFrame;
	private int maxFrame;

	public Sickle(int x, int y, int dirction, int size) {
		super(x, y, dirction, size);
		this.x = x;
		this.y = y;
		this.direction = dirction;
		this.size = size;
		visible = true;
		dx = 4;
		damage = 100;
		timer.start();
		currentFrame = 0;
		maxFrame = 7;
	}

	protected int updateDelay = 25;
	protected Timer timer = new Timer(updateDelay, new update());

	protected class update implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			currentFrame++;
			if (currentFrame > maxFrame - 1)
				currentFrame = 0;
		}
	}

	protected ImageIcon[] imgIcon = { new ImageIcon("images/sickle_0.png"),
			new ImageIcon("images/sickle_1.png"),
			new ImageIcon("images/sickle_2.png"),
			new ImageIcon("images/sickle_3.png"),
			new ImageIcon("images/sickle_4.png"),
			new ImageIcon("images/sickle_5.png"),
			new ImageIcon("images/sickle_6.png"),
			new ImageIcon("images/sickle_7.png"),};
	
	protected ImageIcon[] imgIcon_mirror = {
			new ImageIcon("images/sickle_mirror_0.png"),
			new ImageIcon("images/sickle_mirror_1.png"),
			new ImageIcon("images/sickle_mirror_2.png"),
			new ImageIcon("images/sickle_mirror_3.png"),
			new ImageIcon("images/sickle_mirror_4.png"),
			new ImageIcon("images/sickle_mirror_5.png"),
			new ImageIcon("images/sickle_mirror_6.png"),
			new ImageIcon("images/sickle_mirror_7.png"),};
	Image img;

	@Override
	public Image getImage() {
		if (direction == 0) {
			img = imgIcon[currentFrame].getImage();
			return img;
		} else {
			img = imgIcon_mirror[currentFrame].getImage();
			return img;
		}

	}

	@Override
	public int getDamage() {
		return damage;
	}
	
	public void Move(int TargetX,int TargetY) {
		if (TargetX <= x) {
			x += dx;
			direction = 1;
		} else if (TargetX > x) {
			x -= dx;
			direction = 0;
		}
		if (TargetY <= y) {
			y += dy;
		} else if (TargetY > y) {
			y -= dy;
		}
	}

}
