package fp.s100502016;

import java.awt.Image;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

import javax.swing.ImageIcon;

import fp.s100502016.SnowMan.restore;


public class RankMenu {
	private ImageIcon imgIcon = new ImageIcon("images/rank.png");
	private Image img;
	File file = new File("data/rank.txt");
	
	private String[] name = new String[10]; 
	private int[] score = new int[10];

	RankMenu() {
		img = imgIcon.getImage();
		try {
			readData();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void readData() throws FileNotFoundException {
		Scanner input = new Scanner(file);
		for (int i = 9; i >= 0; i--) {
			name[i] = input.next();
			score[i] = input.nextInt();
		}
		input.close();
	}

	public void newData(String newName, int newRecord) {
		int recorded = 0;
		for (int i = 0; i < 10; i++) {
			if (newRecord >= score[i] && recorded == 0) {
				name[i] = newName;
				score[i] = newRecord;
				recorded = 1;
			}
		}
	}

	public void writeDate() throws FileNotFoundException {
		PrintWriter output = new PrintWriter(file);
		for (int i = 9; i >= 0; i--) {
			output.print(name[i] + " " + score[i] + " ");
		}
		output.close();
	}

	public Image getImg() {
		return img;
	}

	public void setName(String name, int i) {
		this.name[i] = name;
	}

	public int getScore(int i) {
		return score[i];
	}

	public String getName(int i) {
		return name[i];
	}
}
