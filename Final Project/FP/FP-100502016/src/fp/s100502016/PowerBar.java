package fp.s100502016;

import java.awt.Color;
import java.awt.event.KeyEvent;

public class PowerBar {

	private int maximum = 100;
	private int currentPower = 0;
	private int PowerUpDelay = 10;
	private Color color0 = new Color(255, 200, 0);
	private Color color1 = new Color(255, 150, 0);
	private Color color2 = new Color(255, 120, 0);
	private Color color3 = new Color(255, 70, 0);
	private Color color4 = new Color(255, 0, 0);

	public Color getColor() {
		if (currentPower < 20)
			return color0;
		else if (currentPower < 40)
			return color1;
		else if (currentPower < 60)
			return color2;
		else if (currentPower < 80)
			return color3;
		else
			return color4;

	}

	public void setCurrentPower(int newPower) {
		if (currentPower < maximum)
			this.currentPower = newPower;
	}

	public int getCurrentPower() {
		return currentPower;
	}

	public void clearCurrentPower() {
		currentPower = 0;
	}

	public void setPowerUpDelay(int newPowerUpDelay) {
		this.PowerUpDelay = newPowerUpDelay;
	}

	public int getPowerUpDelay() {
		return PowerUpDelay;
	}

	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_SPACE) {
			setCurrentPower(getCurrentPower() + PowerUpDelay);
		}
	}

	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_SPACE) {
			clearCurrentPower();
		}
	}
}
