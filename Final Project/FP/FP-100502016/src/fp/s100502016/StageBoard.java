package fp.s100502016;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

public class StageBoard extends JPanel {

	Menu menu = new Menu();
	RankMenu rankMenu = new RankMenu();
	RetryMenu retryMenu = new RetryMenu();

	SnowMan snowman;

	ImageIcon imgIconCharacterBoard = new ImageIcon("images/CharacterBoard.png");
	ImageIcon imgIconBackground = new ImageIcon("images/background_game_0.png");
	Image imgCharacterBoard = imgIconCharacterBoard.getImage();
	Image imgBackground = imgIconBackground.getImage();
	ImageIcon imgIconWeaponSelect = new ImageIcon("images/weaponSelect.png");
	Image imgwepaonSelect = imgIconWeaponSelect.getImage();

	private int score;
	private int warning;
	private int warning2;
	private String name;
	private int state;
	private boolean isRetry;
	private int crazyRound;
	private int surpriseRound;

	Font font32 = new Font("Algerian", Font.BOLD, 32);
	Font font24 = new Font("Rradley Hand ITC", Font.BOLD, 24);

	ArrayList<SmallGila> smallGilas = new ArrayList();
	ArrayList<Wolf> wolfs = new ArrayList();
	ArrayList<BigGila> bigGilas = new ArrayList();
	ArrayList<Penquin> penquins = new ArrayList();

	ArrayList<Snowball> snowballs = new ArrayList();
	ArrayList<Rocket> rockets = new ArrayList();
	ArrayList<Sickle> sickles = new ArrayList();
	ArrayList<Shit> shits = new ArrayList();
	ArrayList<Supply> supplys = new ArrayList();

	StageBoard() {
		snowman = new SnowMan(200, 300);
		addKeyListener(new AL());
		state = 0;
		score = 0;
		isRetry = false;

	}

	public void InitialGame() {
		score = 0;
		snowman = new SnowMan(200, 300);
		updateTimer.start();
		rebornTimer.start();
		enemyActionTimer.start();
		supplyTimer.start();
		crazyTimer.start();
		WarningTimer.start();
		surpriseTimer.start();
		penquinTimer.start();
		crazyRound = 0;
		warning = 30;
		warning2 = 40;

		for (int i = 0; i < snowballs.size(); i++) {
			snowballs.remove(i);
		}
		for (int i = 0; i < smallGilas.size(); i++) {
			smallGilas.remove(i);
		}
		for (int i = 0; i < bigGilas.size(); i++) {
			bigGilas.remove(i);
		}
		for (int i = 0; i < wolfs.size(); i++) {
			wolfs.remove(i);
		}
		for (int i = 0; i < penquins.size(); i++) {
			penquins.remove(i);

		}

	}

	// Frequency of game update
	private int updateDelay = 5;
	Timer updateTimer = new Timer(updateDelay, new update());

	class update implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {

			snowman.Move();

			if (!snowman.getIsAlive()) {
				state = -1;
				updateTimer.stop();
				rebornTimer.stop();
				enemyActionTimer.stop();
				supplyTimer.stop();
				crazyTimer.stop();
				WarningTimer.stop();
				surpriseTimer.stop();
				penquinTimer.stop();
				name = JOptionPane.showInputDialog("Input Your Name:");
				rankMenu.newData(name, score);
				try {
					rankMenu.writeDate();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				isRetry = true;
			}

			// Move bullet

			snowballs = SnowMan.getsnowballs();
			for (int i = 0; i < snowballs.size(); i++) {
				Snowball snowball = (Snowball) snowballs.get(i);
				if (snowball.getVisible()) {
					snowball.Move();
				} else
					snowballs.remove(i);
			}

			rockets = SnowMan.getrockets();
			for (int i = 0; i < rockets.size(); i++) {
				Rocket rocket = (Rocket) rockets.get(i);
				if (rocket.getVisible()) {
					rocket.Move();
				} else
					rockets.remove(i);
			}

			sickles = SnowMan.getsickles();
			for (int i = 0; i < sickles.size(); i++) {
				Sickle sickle = (Sickle) sickles.get(i);
				if (sickle.getVisible()) {
					sickle.Move();
				} else
					sickles.remove(i);
			}

			shits = SnowMan.getshits();
			for (int i = 0; i < shits.size(); i++) {
				Shit shit = (Shit) shits.get(i);
				if (shit.getVisible()) {
					shit.Move();
				} else
					shits.remove(i);
			}

			// Whether Enemy & bullet is collided

			snowballs = snowman.getsnowballs();
			rockets = snowman.getrockets();
			sickles = snowman.getsickles();
			shits = snowman.getshits();

			for (int i = 0; i < smallGilas.size(); i++) {

				smallGilas.get(i).Collide0(StageBoard.this.snowballs);
				smallGilas.get(i).Collide1(StageBoard.this.rockets);
				smallGilas.get(i).Collide2(StageBoard.this.sickles);
				smallGilas.get(i).Collide3(StageBoard.this.shits);
				if (smallGilas.get(i).getisAlive() == false) {
					score += 50;
					smallGilas.remove(i);
				}
			}
			for (int i = 0; i < wolfs.size(); i++) {
				wolfs.get(i).Collide0(StageBoard.this.snowballs);
				wolfs.get(i).Collide1(StageBoard.this.rockets);
				wolfs.get(i).Collide2(StageBoard.this.sickles);
				wolfs.get(i).Collide3(StageBoard.this.shits);

				if (wolfs.get(i).getisAlive() == false) {
					score += 200;
					wolfs.remove(i);
				}
			}
			for (int i = 0; i < bigGilas.size(); i++) {
				bigGilas.get(i).Collide0(StageBoard.this.snowballs);
				bigGilas.get(i).Collide1(StageBoard.this.rockets);
				bigGilas.get(i).Collide2(StageBoard.this.sickles);
				bigGilas.get(i).Collide3(StageBoard.this.shits);
				if (bigGilas.get(i).getisAlive() == false) {
					score += 500;
					bigGilas.remove(i);
				}
			}

			// Whether player and supply is collide

			snowman.Collide3(supplys);

			repaint();
		}
	}

	// Frequency of enemy's reborn
	private int rebornDelay = 2000;
	Timer rebornTimer = new Timer(rebornDelay, new reborn());

	class reborn implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {

			int xRange = (int) (701 + Math.random() * 100);
			int yRange = (int) (201 + Math.random() * 400);
			int direction = (int) Math.random() * 2;
			if (score < 1500) {
				if (crazyRound > 0) {
					for (int i = 0; i < 5; i++) {
						int dice = (int) (Math.random() * 10);

						if (dice % 2 == 0)
							xRange = (int) (750 + Math.random() * 50);
						else
							xRange = (int) (Math.random() * 50 - 50);

						yRange = (int) (201 + Math.random() * 400);
						SmallGila smallGila = new SmallGila(xRange, yRange,
								direction);
						smallGilas.add(smallGila);
					}
					crazyRound--;
				}
				SmallGila smallGila = new SmallGila(xRange, yRange, direction);
				smallGilas.add(smallGila);

			} else if (score < 5000) {
				if (crazyRound > 0) {
					for (int i = 0; i < 5; i++) {
						int dice = (int) (Math.random() * 10);

						if (dice % 2 == 0)
							xRange = (int) (750 + Math.random() * 50);
						else
							xRange = (int) (Math.random() * 50 - 50);

						yRange = (int) (201 + Math.random() * 400);
						Wolf wolf = new Wolf(xRange, yRange, direction);
						wolfs.add(wolf);
					}
					crazyRound--;
				}
				Wolf wolf = new Wolf(xRange, yRange, direction);
				wolfs.add(wolf);
			} else {
				if (crazyRound > 0) {
					for (int i = 0; i < 5; i++) {
						int dice = (int) (Math.random() * 10);

						if (dice % 2 == 0)
							xRange = (int) (750 + Math.random() * 50);
						else
							xRange = (int) (Math.random() * 50 - 50);

						yRange = (int) (201 + Math.random() * 400);
						BigGila bigGila = new BigGila(xRange, yRange, direction);
						bigGilas.add(bigGila);
					}
					crazyRound--;
				}
				BigGila bigGila = new BigGila(xRange, yRange, direction);
				bigGilas.add(bigGila);
			}
		}
	}

	// Frequency of enemy's action
	private int enemyActionDelay = 50;
	Timer enemyActionTimer = new Timer(enemyActionDelay, new enemyAction());

	class enemyAction implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {

			for (int i = 0; i < smallGilas.size(); i++) {
				smallGilas.get(i).Move(snowman.getRx(), snowman.getRy());
			}
			for (int i = 0; i < bigGilas.size(); i++) {
				bigGilas.get(i).Move(snowman.getRx(), snowman.getRy());
			}
			for (int i = 0; i < wolfs.size(); i++) {
				wolfs.get(i).Move(snowman.getRx(), snowman.getRy());
			}
			for (int i = 0; i < penquins.size(); i++) {
				penquins.get(i).Move(snowman.getRx(), snowman.getRy());
				if(penquins.get(i).getX()>=900||penquins.get(i).getX()<=-100)
					penquins.remove(i);
			}

			// Whether player & enemy is collided

			for (int i = 0; i < smallGilas.size(); i++) {
				snowman.Collide0(smallGilas);
			}
			for (int i = 0; i < bigGilas.size(); i++) {
				snowman.Collide1(bigGilas);
			}
			for (int i = 0; i < wolfs.size(); i++) {
				snowman.Collide2(wolfs);
			}
			for (int i = 0; i < penquins.size(); i++) {
				snowman.Collide4(penquins);
			}

			repaint();
		}
	}

	// Frequency of crazytime

	private int crazydelay = 30000;
	Timer crazyTimer = new Timer(crazydelay, new crazyAction());

	class crazyAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			crazyRound = 2;
		}
	}

	// Frequency of Surprise

	private int surpriseDelay = 40000;
	Timer surpriseTimer = new Timer(surpriseDelay, new surpriseAction());

	class surpriseAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			for (int i = 0; i < 6; i++) {
				int xRange;
				int yRange;
				int direction;
				int dice = (int) (Math.random() * 10);

				if (dice % 2 == 0) {
					xRange = (int) (750 + Math.random() * 50);
					direction = 0;
				} else {
					xRange = (int) (Math.random() * 50);
					direction = 1;
				}

				yRange = (int) (201 + Math.random() * 400);
				Penquin penquin = new Penquin(xRange, yRange, direction);
				penquins.add(penquin);
			}
		}
	}

	// Frequency of Surprise

	private int penquinDelay = 4000;
	Timer penquinTimer = new Timer(penquinDelay, new penquinAction());

	class penquinAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			for (int i = 0; i < 2; i++) {
				int xRange;
				int yRange;
				int direction;
				int dice = (int) (Math.random() * 10);

				if (dice % 2 == 0) {
					xRange = (int) (750 + Math.random() * 50);
					direction = 0;
				} else {
					xRange = (int) (Math.random() * 50 - 50);
					direction = 1;
				}

				yRange = (int) (201 + Math.random() * 400);
				Penquin penquin = new Penquin(xRange, yRange, direction);
				penquins.add(penquin);
			}
		}
	}

	// Frequency of Warning

	private int WarningDelay = 1000;
	Timer WarningTimer = new Timer(WarningDelay, new Warning());

	class Warning implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			warning--;
			if (warning == 0)
				warning = 30;
			warning2--;
			if (warning2 == 0)
				warning2 = 40;
			repaint();
		}
	}

	// Frequency of supply

	private int supplyDelay = 8000;
	Timer supplyTimer = new Timer(supplyDelay, new supplyAction());

	class supplyAction implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Supply.count++;
			int xRange = (int) (Math.random() * 800);
			int yRange = (int) (201 + Math.random() * 400);
			int type = (int) (Math.random() * 5);
			Supply supply = new Supply(xRange, yRange, type);
			supplys.add(supply);

			repaint();
		}
	}

	private class AL extends KeyAdapter {

		public void keyPressed(KeyEvent e) {

			int key = e.getKeyCode();
			if (state == 0) { // Menuing Control
				menu.keyPressed(e);
				if (menu.getSelect() == 0 && key == KeyEvent.VK_ENTER) {
					InitialGame();
					state = 1;
				} else if (menu.getSelect() == 1 && key == KeyEvent.VK_ENTER) {
					state = 2;
				} else if (menu.getSelect() == 2 && key == KeyEvent.VK_ENTER) {
					state = 3;
				} else if (menu.getSelect() == 3 && key == KeyEvent.VK_ENTER) {
					System.exit(0);
				}
			} else if (state == 1) { // Gameing Control
				snowman.keyPressed(e);
				snowman.powerBar.keyPressed(e);
			} else if (state == 2) { // Guiding Control
				if (key == KeyEvent.VK_ESCAPE) {
					state = 0;
				}
			} else if (state == 3) { // Ranking Control
				if (key == KeyEvent.VK_ESCAPE) {
					state = 0;
				}

			} else if (state == -1) { // Retrying Control
				retryMenu.keyPressed(e);
				if (retryMenu.getSelect() == 0 && key == KeyEvent.VK_ENTER) {
					InitialGame();
					state = 1;
				} else if (retryMenu.getSelect() == 1
						&& key == KeyEvent.VK_ENTER) {
					state = 0;
				}
			}
		}

		public void keyReleased(KeyEvent e) {
			snowman.keyReleased(e);
			snowman.powerBar.keyReleased(e);

		}

	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		if (state == -1) { // Paint retry menu
			DrawRetryMenu(g);
		} else if (state == 0) { // Paint main menu
			DrawMenu(g);
		} else if (state == 1) { // Paint game
			DrawGame(g);
		} else if (state == 2) { // Paint Guide page
			g.drawImage(Guide.getImg(), 0, 0, 800, 600, this);
			repaint();
		} else if (state == 3) { // Paint Rank page
			DrawRank(g);
		}
	}

	public void DrawMenu(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		g2.drawImage(menu.imgBackground, 0, 0, this);
		g2.drawImage(menu.getImgStart(), 250, 270, this);
		g2.drawImage(menu.getImgGuide(), 250, 350, this);
		g2.drawImage(menu.getImgRank(), 250, 430, this);
		g2.drawImage(menu.getImgExit(), 250, 510, this);
		repaint();
	}

	public void DrawRetryMenu(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		g2.setFont(font24);
		g2.drawImage(retryMenu.imgBackground, 0, 0, this);
		g2.setColor(Color.orange);
		g2.drawString("Your Total Score: " + score, 250, 300);
		g2.drawString("Try again?", 290, 340);
		g2.drawImage(retryMenu.getImgYes(), 250, 370, this);
		g2.drawImage(retryMenu.getImgNo(), 250, 450, this);
		repaint();
	}

	public void DrawGame(Graphics g) {
		super.paint(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.drawImage(imgBackground, 0, 0, this);
		g2.drawImage(snowman.getImage(), snowman.getX(), snowman.getY(), this);
		if (snowman.getWeapon() == 0) {
			g2.setColor(snowman.powerBar.getColor());
			g2.fillRect(snowman.getX(), snowman.getY(),
					snowman.powerBar.getCurrentPower() / 2, 5);
		}
		if (snowman.getWeapon() == 1) {
			// draw rocket
			if (snowman.getDirection() == 0)
				g2.drawImage(Rocket.img_rocket, snowman.getX() - 20,
						snowman.getY() + 30, this);
			else
				g2.drawImage(Rocket.img_mirror_rocket, snowman.getX() - 20,
						snowman.getY() + 30, this);
		}

		// Draw InfoPanel

		g2.drawImage(imgCharacterBoard, 0, 0, this);
		g2.setColor(Color.RED);
		if (snowman.getHp() >= 0)
			g2.fillRect(74, 16, (int) (snowman.getHp() * 1.8), 30);
		g2.drawImage(imgwepaonSelect, 428 + 84 * snowman.getWeapon(), 4, this);
		g2.setFont(font32);
		g.setColor(Color.orange);
		g.drawString("Score:" + score, 0, 80);

		// Draw WeaponCount

		for (int i = 1; i < 4; i++) {
			g2.drawString("" + snowman.getWeaponCount(i), 470 + i * 85, 50);
		}

		// Draw Weapon

		ArrayList<Snowball> snowballs = SnowMan.getsnowballs();
		ArrayList<Rocket> rockets = SnowMan.getrockets();
		ArrayList<Sickle> sickles = SnowMan.getsickles();
		ArrayList<Shit> shits = SnowMan.getshits();

		for (int i = 0; i < snowballs.size(); i++) {
			Snowball snowball = (Snowball) snowballs.get(i);
			g2.drawImage(snowball.getImage(), snowball.getX(), snowball.getY(),
					this);
		}
		for (int i = 0; i < rockets.size(); i++) {
			Rocket rocket = (Rocket) rockets.get(i);
			g2.drawImage(rocket.getImage(), rocket.getX(), rocket.getY(), this);
		}
		for (int i = 0; i < sickles.size(); i++) {
			Sickle sickle = (Sickle) sickles.get(i);
			g2.drawImage(sickle.getImage(), sickle.getX(), sickle.getY(), this);
		}
		for (int i = 0; i < shits.size(); i++) {
			Shit shit = (Shit) shits.get(i);
			g2.drawImage(shit.getImage(), shit.getX(), shit.getY(), this);
		}
		// Draw supply
		for (int i = 0; i < supplys.size(); i++) {
			g2.drawImage(supplys.get(i).getImg(), supplys.get(i).getX(),
					supplys.get(i).getY(), this);
		}
		// Draw Enemy
		for (int i = 0; i < smallGilas.size(); i++) {
			g2.drawImage(smallGilas.get(i).getImg(), smallGilas.get(i).getX(),
					smallGilas.get(i).getY(), this);

		}
		for (int i = 0; i < wolfs.size(); i++) {
			g2.drawImage(wolfs.get(i).getImg(), wolfs.get(i).getX(),
					wolfs.get(i).getY(), this);

		}
		for (int i = 0; i < bigGilas.size(); i++) {
			g2.drawImage(bigGilas.get(i).getImg(), bigGilas.get(i).getX(),
					bigGilas.get(i).getY(), this);

		}
		for (int i = 0; i < penquins.size(); i++) {
			g2.drawImage(penquins.get(i).getImg(), penquins.get(i).getX(),
					penquins.get(i).getY(), this);

		}

		// Draw Warning

		if (warning <= 5 && warning % 2 == 1) {
			g2.setColor(Color.RED);
			g2.setFont(font32);
			g2.drawString("Let's Rock!", 300, 200);
		}
		if (warning2 <= 5 && warning2 % 2 == 0) {
			g2.setColor(Color.RED);
			g2.setFont(font32);
			g2.drawString("Watchout Penquin!", 250, 200);
		}

	}

	public void DrawRank(Graphics g) {
		DrawMenu(g);

		g.drawImage(rankMenu.getImg(), 150, 20, this);
		for (int i = 9; i >= 0; i--) {
			g.setFont(font32);
			g.setColor(Color.orange);
			g.drawString(rankMenu.getName(i), 280, 100 + i * 47);
			g.drawString(Integer.toString(rankMenu.getScore(i)), 510,
					100 + i * 47);
		}
		repaint();
	}

}
