package fp.s100502016;

import java.awt.Image;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;

public class RetryMenu {
	private int select;
	private boolean isSelected[] = { true, false };
	private ImageIcon imgIconYes_0 = new ImageIcon("images/yes_0.png");
	private ImageIcon imgIconYes_1 = new ImageIcon("images/yes_1.png");
	private ImageIcon imgIconNo_0 = new ImageIcon("images/no_0.png");
	private ImageIcon imgIconNo_1 = new ImageIcon("images/no_1.png");
	ImageIcon imgIconBackground = new ImageIcon("images/menu_background.jpg");

	private Image imgYes;
	private Image imgNo;

	Image imgBackground = imgIconBackground.getImage();

	public RetryMenu() {

		imgYes = imgIconYes_1.getImage();
		imgNo = imgIconNo_0.getImage();
		int select = 0;
	}

	public int getSelect() {
		return select;
	}

	public Image getImgYes() {
		if (isSelected[0] == true) {
			imgYes = imgIconYes_1.getImage();
			return imgYes;
		} else {
			imgYes = imgIconYes_0.getImage();
			return imgYes;
		}
	}

	public Image getImgNo() {
		if (isSelected[1] == true) {
			imgNo = imgIconNo_1.getImage();
			return imgNo;
		} else {
			imgNo = imgIconNo_0.getImage();
			return imgNo;
		}
	}

	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_UP) {
			if (select > 0) {
				select -= 1;
			}
			isSelected[0] = true;
			isSelected[1] = false;

		} else if (key == KeyEvent.VK_DOWN) {
			if (select < 1)
				select += 1;
			isSelected[0] = false;
			isSelected[1] = true;

		}

	}

	
}
