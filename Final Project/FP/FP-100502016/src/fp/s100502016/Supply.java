package fp.s100502016;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Supply {
	ImageIcon imageIcon[] = { new ImageIcon("images/bullet_supply.png"),
			new ImageIcon("images/sickle.png"),
			new ImageIcon("images/shit.png"),
			new ImageIcon("images/heart.png"),
			new ImageIcon("images/black_heart.png") };
	Image img;
	private int x;
	private int y;
	private int w;
	private int h;
	private int type;
	static int count = 0;

	Supply(int x, int y, int type) {
		this.x = x;
		this.y = y;
		this.w = imageIcon[0].getIconWidth();
		this.h = imageIcon[0].getIconHeight();
		this.type = type;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getW() {
		return w;
	}

	public int getH() {
		return h;
	}

	public int getType() {
		return type;
	}

	public Image getImg() {
		img = imageIcon[type].getImage();
		return img;
	}
}
