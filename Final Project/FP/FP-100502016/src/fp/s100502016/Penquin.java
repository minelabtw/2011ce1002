package fp.s100502016;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.Timer;

public class Penquin extends SmallGila {

	public Penquin(int x, int y, int direction) {
		super(x, y, direction);
		this.x = x;
		this.y = y;
		this.w = imgIcon[0].getIconWidth();
		this.h = imgIcon[0].getIconHeight();
		this.direction = direction;
		currentFrame = 0;
		maxFrame = 4;
		isAlive = true;
		img = imgIcon[currentFrame].getImage();
		currentFrame = 0;
		damage = 9999;
		if (direction == 0)
			dx = -6;
		else
			dx = 6;
		timer.start();

	}

	protected ImageIcon[] imgIcon = { new ImageIcon("images/penquin_0.png"),
			new ImageIcon("images/penquin_1.png"),
			new ImageIcon("images/penquin_2.png"),
			new ImageIcon("images/penquin_3.png") };

	protected ImageIcon[] imgIcon_mirror = {
			new ImageIcon("images/penquin_mirror_0.png"),
			new ImageIcon("images/penquin_mirror_1.png"),
			new ImageIcon("images/penquin_mirror_2.png"),
			new ImageIcon("images/penquin_mirror_3.png") };

	@Override
	public void Move(int TargetX, int TargetY) {
		x += dx;
	}

	protected int updateDelay = 100;
	protected Timer timer = new Timer(updateDelay, new update());

	protected class update implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			currentFrame++;
			if (currentFrame > maxFrame - 1)
				currentFrame = 0;
		}
	}

	@Override
	public Image getImg() {
		if (direction == 0) {
			img = imgIcon[currentFrame].getImage();
			return img;
		} else {
			img = imgIcon_mirror[currentFrame].getImage();
			return img;
		}
	}

}
