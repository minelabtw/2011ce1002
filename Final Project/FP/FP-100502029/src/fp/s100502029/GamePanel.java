package fp.s100502029;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class GamePanel extends JPanel {
	private Bar bar = new Bar(); // create a bar
	private Ball ball1 = new Ball(); // create a ball
	private Brick brick = new Brick();
	protected HighScore highScore = new HighScore();
	protected Timer timerOfBar = new Timer(5, new BarTimerListener());
	private Timer timerOfBall = new Timer(25, new BallTimerListener());
	private int x_bar;
	private int x_ball, y_ball;
	private int ballCenter_x_initial, ballCenter_y_initial;
	private int[][] dy_effect = new int[brick.row][brick.column];
	private int[] score;
	private int life = 2;
	private int tempScore = 0; // the score of break brick
	private int extraScore = 0; // the score of eat effect ball
	protected int totalScore = 0; // the total score
	protected int changeScreen = 0; // determine which interface will show
	private int buttonWidth = 150;
	private int buttonHeight = 50;
	private int buttonVerticalGap = 20;
	private String lifeString;
	private String scoreString;
	private String[] name; // store the name that in the file
	protected boolean action = false; // true is allowed the key's action
	private boolean pressR = false;
	private boolean pressL = false;
	private boolean gameStart = false; // true is represent game is starting
	protected boolean gameOver = false; // true is represent game over
	private boolean stageFlag = true;
	private boolean startDrawStage = false;
	private boolean checkDraw = false;
	protected boolean readAndWriteData = false;
	private ImageIcon backgroundImageIcon1 = new ImageIcon("image/background1.png");
	private ImageIcon backgroundImageIcon2 = new ImageIcon("image/background2.jpg");
	private ImageIcon frontPageImageIcon = new ImageIcon("image/frontpage1.png");
	private ImageIcon button1 = new ImageIcon("image/button1.png");
	private ImageIcon button2 = new ImageIcon("image/button2.png");
	private ImageIcon button3 = new ImageIcon("image/button3.png");
	private ImageIcon start1 = new ImageIcon("image/start1.png");
	private ImageIcon start2 = new ImageIcon("image/start2.png");
	private ImageIcon start3 = new ImageIcon("image/start3.png");
	private ImageIcon exit1 = new ImageIcon("image/exit1.png");
	private ImageIcon exit2 = new ImageIcon("image/exit2.png");
	private ImageIcon exit3 = new ImageIcon("image/exit3.png");
	private ImageIcon highscore1 = new ImageIcon("image/highscore1.png");
	private ImageIcon highscore2 = new ImageIcon("image/highscore2.png");
	private ImageIcon highscore3 = new ImageIcon("image/highscore3.png");
	private ImageIcon leaderBoard1 = new ImageIcon("image/leaderBoard1.png");
	private ImageIcon record1 = new ImageIcon("image/record1.png");
	private ImageIcon[] number = new ImageIcon[10];
	private ImageIcon x = new ImageIcon("image/x.png");
	private ImageIcon effect1 = new ImageIcon("image/effect1.png");
	private ImageIcon effect2 = new ImageIcon("image/effect2.png");
	private ImageIcon effect3 = new ImageIcon("image/effect3.png");
	private ImageIcon effect4 = new ImageIcon("image/effect4.png");
	private ImageIcon effect5 = new ImageIcon("image/effect5.png");
	private ImageIcon effect6 = new ImageIcon("image/effect6.png");
	private ImageIcon effect7 = new ImageIcon("image/effect4.png");
	private ImageIcon lifeImageIcon = new ImageIcon("image/life1.png");
	private ImageIcon scoreImageIcon = new ImageIcon("image/score1.png");
	private ImageIcon backgroundColorImageIcon = new ImageIcon("image/backgroundColor1.png");
	private Image backgroundImage1 = backgroundImageIcon1.getImage();
	private Image backgroundImage2 = backgroundImageIcon2.getImage();
	private Image frontPageImage = frontPageImageIcon.getImage();
	private Image lifeImage = lifeImageIcon.getImage();
	private Image scoreImage = scoreImageIcon.getImage();
	private Image backgroundColorImage = backgroundColorImageIcon.getImage();
	
	// create buttons
	protected JButton jbtStartGame = new JButton();
	protected JButton jbtHighScore = new JButton();
	protected JButton jbtExit = new JButton();
	protected JButton jbtOK1 = new JButton();
	protected JButton jbtOK2 = new JButton();
		
	public GamePanel() {
		// set start button's option
		jbtStartGame.setText(null);
		jbtStartGame.setContentAreaFilled(false);
		jbtStartGame.setBorderPainted(false);
		jbtStartGame.setIcon(start1);
		jbtStartGame.setRolloverIcon(start2);
		jbtStartGame.setPressedIcon(start3);
		
		// set high score button's option
		jbtHighScore.setText(null);
		jbtHighScore.setContentAreaFilled(false);
		jbtHighScore.setBorderPainted(false);
		jbtHighScore.setIcon(highscore1);
		jbtHighScore.setRolloverIcon(highscore2);
		jbtHighScore.setPressedIcon(highscore3);
		
		// set exit button's option
		jbtExit.setText(null);
		jbtExit.setContentAreaFilled(false);
		jbtExit.setBorderPainted(false);
		jbtExit.setIcon(exit1);
		jbtExit.setRolloverIcon(exit2);
		jbtExit.setPressedIcon(exit3);
		
		// // set ok button's option
		jbtOK1.setText(null);
		jbtOK1.setContentAreaFilled(false);
		jbtOK1.setBorderPainted(false);
		jbtOK1.setIcon(button1);
		jbtOK1.setRolloverIcon(button2);
		jbtOK1.setPressedIcon(button3);
		jbtOK2.setText(null);
		jbtOK2.setContentAreaFilled(false);
		jbtOK2.setBorderPainted(false);
		jbtOK2.setIcon(button1);
		jbtOK2.setRolloverIcon(button2);
		jbtOK2.setPressedIcon(button3);
		
		// initialize the number ImageIcon
		for (int i = 0; i < 10; i++) {
			number[i] = new ImageIcon("image/" + i + ".png");
		}
		
		// add key listener to VK_RIGHT, VK_LEFT, and VK_ENTER
		addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (action) {
					if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
						pressR = true;
						bar.direction = 1;
					}
					
					else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
						pressL = true;
						bar.direction = -1;
					}
					
					else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						ball1.dead = false;
						gameStart = true;
						timerOfBall.start();
					}
				}
			}
			public void keyReleased(KeyEvent e) {
				switch (e.getKeyCode()) {
					case KeyEvent.VK_RIGHT: 
						pressR = false;
						break;
					case KeyEvent.VK_LEFT: 
						pressL = false;
						break;
					default:
						break;
				}
			}
		});
	}
	
	// get the number of disappear brick
	public int getNumberOfDisappearBrick() {
		return brick.numberOfDisappearBrick;
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		// set buttons bounds
		jbtStartGame.setBounds(getWidth() / 2 - buttonWidth / 2, 30 + getHeight() / 2 - buttonHeight * 3 / 2 - buttonVerticalGap, buttonWidth, buttonHeight);
		jbtHighScore.setBounds(getWidth() / 2 - 110, 30 + getHeight() / 2 - buttonHeight / 2, 220, buttonHeight + 10);
		jbtExit.setBounds(getWidth() / 2 - buttonWidth / 2, 30 + getHeight() / 2 + buttonHeight / 2 + buttonVerticalGap, buttonWidth, buttonHeight);
		jbtOK1.setBounds(getWidth() / 2 - 25, 380, 60, 60);
		jbtOK2.setBounds(getWidth() / 2 - 25, 395, 60, 60);
		
		// show menu
		if (changeScreen == 0)
			g.drawImage(frontPageImage, 0, 0, getWidth(), getHeight(), this);
		
		// display game screen
		else if (changeScreen == 1) {			
			g.drawImage(backgroundImage1, 0, 0, getWidth(), getHeight(), this);
			if (!gameOver) {
				if (ball1.dead) {
					timerOfBall.stop();
					gameStart = false;
					ball1.dead = false;
					x_bar = 0;
					x_ball = 0;
					y_ball = 0;
				}
				
				// draw bar
				int barCenter_x = getWidth() / 2;
				int barCenter_y = getHeight() - 30;				
				if (x_bar > getWidth() / 2 - bar.width / 2)
					x_bar = getWidth() / 2 - bar.width / 2;
				else if (x_bar < bar.width / 2 - getWidth() / 2)
					x_bar = bar.width / 2 - getWidth() / 2;
				else
					x_ball += ball1.adjustBallPosition(bar.moveBar(pressR, pressL), gameStart);
				g.drawImage(bar.barImage, x_bar + barCenter_x - bar.width / 2, barCenter_y - bar.HEIGHT / 2, bar.width, bar.HEIGHT, this);
				
				// draw ball
				ballCenter_x_initial = barCenter_x;
				ballCenter_y_initial = barCenter_y - bar.HEIGHT / 2 - ball1.radius - 2;
				g.setColor(Color.blue);
				g.drawImage(ball1.ballImage, x_ball + ballCenter_x_initial - ball1.radius, y_ball + ballCenter_y_initial - ball1.radius, ball1.radius * 2, ball1.radius * 2, this);
						
				// draw brick
				// draw stage1
				if (brick.currentStage == 1) {
					if (stageFlag) { // initialize or reset the options of ball and brick
						stageFlag = false;
						bar.width = 62;
						life = 2;
						x_bar = 0;
						x_ball = 0;
						y_ball = 0;
						tempScore = 0;
						extraScore = 0;
						brick.numberOfDisappearBrick = 0;
						ball1.numberOfDead = 0;
						ball1.initializeBall();
						brick.resetEffect();
						brick.resetBrickExist();
						brick.randomEffect();
					}
					brick.stage1(g, getWidth(), getHeight());
				}
				
				// draw stage2
				else if (brick.currentStage == 2) {
					if (!stageFlag)
						checkDraw = true;
					if (startDrawStage) {
						if (!stageFlag) { // initialize or reset the options of ball and brick
							stageFlag = true;
							gameStart = false;
							checkDraw = false;
							brick.stageMessage = true;
							bar.width = 62;
							x_bar = 0;
							x_ball = 0;
							y_ball = 0;
							ball1.initializeBall();
							brick.resetBrickExist();
							brick.randomEffect();
							timerOfBall.stop();
							timerOfBar.start();
						}
						brick.stage2(g, getWidth(), getHeight());
					}
				}
				
				// clear the stage and go to record screen
				else if (brick.currentStage == 3) {
					gameStart = false;
					brick.currentStage = 1;
					changeScreen = 3;
				}
				
				// draw effect ball
				for (int i = 0; i < brick.row; i++) {
					for (int j = 0; j < brick.column; j++) {
						if (brick.effect[i][j] > 10) {
							switch(brick.effect[i][j]) {
								case 11:
									g.drawImage(effect1.getImage(), brick.x_brickLeftUpPoint[i][j] + brick.WIDTH / 2 - 13, brick.y_brickLeftUpPoint[i][j] + brick.HEIGHT + dy_effect[i][j], 26, 18, this);
									break;
								case 12:
									g.drawImage(effect2.getImage(), brick.x_brickLeftUpPoint[i][j] + brick.WIDTH / 2 - 13, brick.y_brickLeftUpPoint[i][j] + brick.HEIGHT + dy_effect[i][j], 26, 18, this);
									break;
								case 13:
									g.drawImage(effect3.getImage(), brick.x_brickLeftUpPoint[i][j] + brick.WIDTH / 2 - 13, brick.y_brickLeftUpPoint[i][j] + brick.HEIGHT + dy_effect[i][j], 26, 18, this);
									break;
								case 14:
									g.drawImage(effect4.getImage(), brick.x_brickLeftUpPoint[i][j] + brick.WIDTH / 2 - brick.effectBallRadius, brick.y_brickLeftUpPoint[i][j] + brick.HEIGHT + dy_effect[i][j], brick.effectBallRadius * 2, brick.effectBallRadius * 2, this);
									break;
								case 15:
									g.drawImage(effect5.getImage(), brick.x_brickLeftUpPoint[i][j] + brick.WIDTH / 2 - 13, brick.y_brickLeftUpPoint[i][j] + brick.HEIGHT + dy_effect[i][j], 26, 18, this);
									break;
								case 16:
									g.drawImage(effect6.getImage(), brick.x_brickLeftUpPoint[i][j] + brick.WIDTH / 2 - 13, brick.y_brickLeftUpPoint[i][j] + brick.HEIGHT + dy_effect[i][j], 26, 18, this);
									break;
								case 17:
									g.drawImage(effect7.getImage(), brick.x_brickLeftUpPoint[i][j] + brick.WIDTH / 2 - brick.effectBallRadius, brick.y_brickLeftUpPoint[i][j] + brick.HEIGHT + dy_effect[i][j], brick.effectBallRadius * 2, brick.effectBallRadius * 2, this);
									break;
								default:
									break;
							}
							if (brick.effectBallSpeedControl[i][j] % 12 < 5)
								dy_effect[i][j] += 1;
							brick.effectBallSpeedControl[i][j]++;
						}
						if (dy_effect[i][j] + brick.y_brickLeftUpPoint[i][j] + brick.HEIGHT + 5 > getHeight()) {
							dy_effect[i][j] = 0;
							brick.effect[i][j] = 0;
						}
					}
				}
				
				if (checkDraw) {
					startDrawStage = true;
					for (int i = 0; i < brick.row; i++) {
						for (int j = 0; j < brick.column; j++) {
							if (brick.effect[i][j] != 0)
								startDrawStage = false;
						}
					}
				}
				
				// the game over situation
				if (life - ball1.numberOfDead < 0) {
					ball1.numberOfDead--;
					action = false;
					gameOver = true;
					stageFlag = true;
					readAndWriteData = true;
					brick.currentStage = 1;
					changeScreen = 3; // go to record screen
				}
			}
			
			// show life and score on the game screen
			tempScore = getNumberOfDisappearBrick() * 10;
			scoreString = (tempScore + extraScore) + "";
			lifeString = (life - ball1.numberOfDead) + "";
			g.drawImage(lifeImage, 0, 0, 96, 30, this);
			g.drawImage(backgroundColorImage, 96, 0, 54, 30, this);
			g.drawImage(scoreImage, 150, 0, 144, 30, this);
			g.drawImage(backgroundColorImage, 294, 0, 204, 30, this);
			g.setColor(new Color(255, 85, 85));
			g.drawString(lifeString, 100, 24);
			g.drawString(scoreString, 290, 24);
		}
		
		// show high score screen
		else if (changeScreen == 2) {
			timerOfBar.stop();
			timerOfBall.stop();
			g.drawImage(backgroundImage2, 0, 0, getWidth(), getHeight(), this); // set background picture
			g.drawImage(leaderBoard1.getImage(), 0, 0, getWidth(), getHeight(), this);
			name = highScore.getName(); // read and store the names from file
			score = highScore.getScore(); // read and store the score from file
			g.setColor(new Color(234, 255, 5));
			
			// show high score file's name and score one by one
			g.drawString(name[0], 120, 160);
			g.drawString(name[1], 120, 230);
			g.drawString(name[2], 120, 290);
			g.drawString(name[3], 120, 355);
			
			String ScoreString = score[0] + "";
			for (int i = 0; i < ScoreString.length(); i++) {
				int num = ScoreString.charAt(ScoreString.length() - 1 - i) - '0';
				g.drawImage(number[num].getImage(), 410 - 25 * i, 130, 25, 40, this);
			}
			ScoreString = score[1] + "";
			for (int i = 0; i < ScoreString.length(); i++) {
				int num = ScoreString.charAt(ScoreString.length() - 1 - i) - '0';
				g.drawImage(number[num].getImage(), 410 - 25 * i, 200, 25, 40, this);
			}
			ScoreString = score[2] + "";
			for (int i = 0; i < ScoreString.length(); i++) {
				int num = ScoreString.charAt(ScoreString.length() - 1 - i) - '0';
				g.drawImage(number[num].getImage(), 410 - 25 * i, 260, 25, 40, this);
			}
			ScoreString = score[3] + "";
			for (int i = 0; i < ScoreString.length(); i++) {
				int num = ScoreString.charAt(ScoreString.length() - 1 - i) - '0';
				g.drawImage(number[num].getImage(), 410 - 25 * i, 325, 25, 40, this);
			}		
		}
		
		// show total score screen
		else if (changeScreen == 3) {
			timerOfBar.stop();
			timerOfBall.stop();
			g.drawImage(backgroundImage1, 0, 0, getWidth(), getHeight(), this);
			g.drawImage(record1.getImage(), 0, 0, getWidth(), getHeight(), this);
			int remainLife = life - ball1.numberOfDead;
			int playScore = tempScore + extraScore;
			int lifeScore = remainLife * 200;
			totalScore = playScore + lifeScore;
			
			// if game over, show message to tell user the game is game over
			if (gameOver) {
				FontMetrics fm = g.getFontMetrics();
				int stringWidth = fm.stringWidth("Game Over");
				int stringAscent = fm.getAscent();
				g.setColor(Color.red);
				g.drawString("Game Over", getWidth() / 2 - stringWidth / 2 - 5, getHeight() / 2 - stringAscent / 2 - 110);
				g.drawImage(number[0].getImage(), 360, 225, 25, 40, this);
			}
			
			// if user clear stages, show "clear" message
			else {
				FontMetrics fm = g.getFontMetrics();
				int stringWidth = fm.stringWidth("Clear");
				int stringAscent = fm.getAscent();
				g.setColor(Color.red);
				g.drawString("Clear", getWidth() / 2 - stringWidth / 2, getHeight() / 2 - stringAscent / 2 - 110);
				g.drawImage(number[remainLife].getImage(), 255, 225, 25, 40, this);
				g.drawImage(x.getImage(), 285, 220, 20, 40, this);
				g.drawImage(number[2].getImage(), 310, 225, 25, 40, this);
				g.drawImage(number[0].getImage(), 335, 225, 25, 40, this);
				g.drawImage(number[0].getImage(), 360, 225, 25, 40, this);
			}

			// display the scores that get from game
			String playScoreString = playScore + "";
			for (int i = 0; i < playScoreString.length(); i++) {
				int num = playScoreString.charAt(playScoreString.length() - 1 - i) - '0';
				g.drawImage(number[num].getImage(), 360 - 25 * i, 168, 25, 40, this);
			}
			
			// display the total score
			String totalScoreString = totalScore + "";
			for (int i = 0; i < totalScoreString.length(); i++) {
				int num = totalScoreString.charAt(totalScoreString.length() - 1 - i) - '0';
				g.drawImage(number[num].getImage(), 360 - 25 * i, 312, 25, 40, this);
			}
			
			// set jbtok1 can see and press
			jbtOK1.setFocusable(true);
			jbtOK1.setVisible(true);
		}
	}
	
	class BarTimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			x_bar += bar.moveBar(pressR, pressL); // set the bar's displacement along x-axis
			
			// check the bar whether eat the effect ball or not
			for (int i = 0; i < brick.row; i++) {
				for (int j = 0; j < brick.column; j++) {
					if (brick.effect[i][j] > 10) {
						if (brick.effect[i][j] == 11 || brick.effect[i][j] == 12 || brick.effect[i][j] == 13 || brick.effect[i][j] == 15 || brick.effect[i][j] == 16) {
							if (brick.y_brickLeftUpPoint[i][j] + brick.HEIGHT + dy_effect[i][j] + 18 >= getHeight() - 30 - bar.HEIGHT / 2
								&& brick.y_brickLeftUpPoint[i][j] + brick.HEIGHT + dy_effect[i][j] + 9 <= getHeight() - 30 + bar.HEIGHT / 2
								&& brick.x_brickLeftUpPoint[i][j] + brick.WIDTH / 2 >= x_bar + getWidth() / 2 - bar.width / 2 - 13
								&& brick.x_brickLeftUpPoint[i][j] + brick.WIDTH / 2 <= x_bar + getWidth() / 2 + bar.width / 2 + 13) {
								dy_effect[i][j] = 0;
								switch(brick.effect[i][j]) {
									case 11: // add extra points to score
										extraScore += 10;
										break;
									case 12:
										extraScore += 20;
										break;
									case 13:
										extraScore += 30;
										break;
									case 15: // lengthen the bar
										bar.width += 18;
										break;
									case 16: // shorten the bar
										bar.width -= 18;
										break;
									default:
										break;
								}
								brick.effect[i][j] = 0;
							}
						}
						
						if (brick.effect[i][j] == 14 || brick.effect[i][j] == 17) {
							if (brick.y_brickLeftUpPoint[i][j] + brick.HEIGHT + dy_effect[i][j] + brick.effectBallRadius * 2 >= getHeight() - 30 - bar.HEIGHT / 2
									&& brick.y_brickLeftUpPoint[i][j] + brick.HEIGHT + dy_effect[i][j] + brick.effectBallRadius <= getHeight() - 30 + bar.HEIGHT / 2
									&& brick.x_brickLeftUpPoint[i][j] + brick.WIDTH / 2 >= x_bar + getWidth() / 2 - bar.width / 2 - brick.effectBallRadius
									&& brick.x_brickLeftUpPoint[i][j] + brick.WIDTH / 2 <= x_bar + getWidth() / 2 + bar.width / 2 + brick.effectBallRadius) {
								dy_effect[i][j] = 0;
								switch(brick.effect[i][j]) {
									case 14: // add a life
										life++;
										break;
									case 17:
										life++;
										break;
									default:
										break;
								}
								brick.effect[i][j] = 0;
							}
						}
					}
				}
			}
			repaint();
		}
	}
	
	class BallTimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			int hitSide = brick.hitBrick(x_ball + ballCenter_x_initial, y_ball + ballCenter_y_initial, ball1.radius); // call method to know which side the ball hit the brick
			switch(hitSide) {
				case 0: // didn't hit
					ball1.moveBall(ballCenter_x_initial, ballCenter_y_initial, x_ball, y_ball, x_bar, bar.width, getWidth(), getHeight());
					timerOfBall.setDelay(ball1.delay);
					break;
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
					ball1.moveBall(hitSide);
					break;
				default:
					break;
			}
			x_ball += ball1.dx;
			y_ball += ball1.dy;
			repaint();
		}
	}
}
