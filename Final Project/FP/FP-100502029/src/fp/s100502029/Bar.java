package fp.s100502029;

import java.awt.*;
import javax.swing.*;

public class Bar {
	private ImageIcon barImageIcon = new ImageIcon("image/bar1.png");
	protected Image barImage = barImageIcon.getImage();
	private int dx; // the displacement of bar
	protected int width = 62; // initialize the bar's length
	protected final int HEIGHT = 8;
	protected int direction; // 1 is right, -1 is left
	
	public Bar() {
		
	}
	
	// move the bar
	public int moveBar(boolean pressR, boolean pressL) {
		if (pressR || pressL)
			dx = 2;
		else
			dx = 0;
		
		return dx * direction;
	}
}
