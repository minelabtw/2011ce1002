package fp.s100502029;

import java.awt.*;
import javax.swing.*;

public class Ball {
	private ImageIcon ballImageIcon = new ImageIcon("image/ball1.png");
	protected Image ballImage = ballImageIcon.getImage();
	protected  int radius = 6; // the ball's radius
	protected int delay = 5;
	private int initial_dx = 1, initial_dy = -1;
	protected int dx = initial_dx, dy = initial_dy;
	protected int numberOfDead = 0;
	protected boolean dead = false;
	
	public Ball() {

	}
	
	// adjust ball's position before it was launched
	public int adjustBallPosition(int displacement , boolean gameStart) {
		if (!gameStart)
			return displacement;
		else
			return 0;
	}
	
	// reset the ball speed and direction
	public void initializeBall() {
		dx = initial_dx;
		dy = initial_dy;
		delay = 5;
	}
	
	// check the point that the ball hit the bar, and rebound the ball
	public void moveBall(int ballCenter_x_initial, int ballCenter_y_initial, int x_ball, int y_ball, int x_bar, int barWidth, int panelWidth, int panelHeight) {
		if (y_ball >= 2 && y_ball <= radius + 2 && x_ball >= x_bar - barWidth / 2 - radius && x_ball <= x_bar + barWidth / 2 + radius) {
			int interval = (barWidth + radius * 2) / 18;
			int x = x_bar - barWidth / 2 - radius;
			if (x_ball >= x && x_ball < x + interval) {
				dx = -5;
				dy = -1;
				delay = 18;
			}
			else if (x_ball >= x + interval && x_ball < x + interval * 2) {
				dx = -3;
				dy = -1;
				delay = 11;
			}
			else if (x_ball >= x + interval * 2 && x_ball < x + interval * 3) {
				dx = -4;
				dy = -2;
				delay = 16;
			}
			else if (x_ball >= x + interval * 3 && x_ball < x + interval * 4) {
				dx = -4;
				dy = -3;
				delay = 18;
			}
			else if (x_ball >= x + interval * 4 && x_ball < x + interval * 5) {
				dx = -1;
				dy = -1;
				delay = 5;
			}
			else if (x_ball >= x + interval * 5 && x_ball < x + interval * 6) {
				dx = -3;
				dy = -4;
				delay = 18;
			}
			else if (x_ball >= x + interval * 6 && x_ball < x + interval * 7) {
				dx = -2;
				dy = -4;
				delay = 16;
			}
			else if (x_ball >= x + interval * 7 && x_ball < x + interval * 8) {
				dx = -1;
				dy = -3;
				delay = 11;
			}
			else if (x_ball >= x + interval * 8 && x_ball < x + interval * 9) {
				dx = -1;
				dy = -5;
				delay = 18;
			}
			else if (x_ball >= x + interval * 9 && x_ball < x + interval * 10) {
				dx = 1;
				dy = -5;
				delay = 18;
			}
			else if (x_ball >= x + interval * 10 && x_ball < x + interval * 11) {
				dx = 1;
				dy = -3;
				delay = 11;
			}
			else if (x_ball >= x + interval * 11 && x_ball < x + interval * 12) {
				dx = 2;
				dy = -4;
				delay = 16;
			}
			else if (x_ball >= x + interval * 12 && x_ball < x + interval * 13) {
				dx = 3;
				dy = -4;
				delay = 18;
			}
			else if (x_ball >= x + interval * 13 && x_ball < x + interval * 14) {
				dx = 1;
				dy = -1;
				delay = 5;
			}
			else if (x_ball >= x + interval * 14 && x_ball < x + interval * 15) {
				dx = 4;
				dy = -3;
				delay = 18;
			}
			else if (x_ball >= x + interval * 15 && x_ball < x + interval * 16) {
				dx = 4;
				dy = -2;
				delay = 16;
			}
			else if (x_ball >= x + interval * 16 && x_ball < x + interval * 17) {
				dx = 3;
				dy = -1;
				delay = 11;
			}
			else if (x_ball >= x + interval * 17 && x_ball < x + interval * 18) {
				dx = 5;
				dy = -1;
				delay = 18;
			}
		}
		else if (x_ball + ballCenter_x_initial < radius)
			dx = Math.abs(dx);
		else if (x_ball + ballCenter_x_initial > panelWidth - radius)
			dx = -Math.abs(dx);
		else if (y_ball + ballCenter_y_initial < 30 + radius)
			dy = Math.abs(dy);
		
		// if ball go through the bottom of frame, then the ball is dead
		else if (y_ball + ballCenter_y_initial >= panelHeight + radius) {
			dead = true;
			numberOfDead++;
			initializeBall();
		}
	}
	
	// move the ball by determine what side it hit the brick
	public void moveBall(int hitSide) {
		if (hitSide == 1 || hitSide == 2)
			dy = -dy;
		else if (hitSide == 3 || hitSide == 4)
			dx = -dx;
		else if (hitSide == 5) {
			dx = -dx;
			dy = -dy;
		}
	}
}
