package fp.s100502029;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class FrameWork extends JFrame {
	private GamePanel gamePanel = new GamePanel();
	
	public FrameWork() {
		setLayout(new GridLayout(1, 1));
		add(gamePanel);
		gamePanel.setFont(new Font("Cooper Black", Font.BOLD, 35));
		gamePanel.setFocusable(true);
		gamePanel.add(gamePanel.jbtStartGame);
		gamePanel.add(gamePanel.jbtHighScore);
		gamePanel.add(gamePanel.jbtExit);
		gamePanel.add(gamePanel.jbtOK1);
		gamePanel.jbtOK1.setFocusable(false);
		gamePanel.jbtOK1.setVisible(false);
		gamePanel.add(gamePanel.jbtOK2);
		gamePanel.jbtOK2.setFocusable(false);
		gamePanel.jbtOK2.setVisible(false);
		
		// add all buttons action listener
		gamePanel.jbtStartGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gamePanel.timerOfBar.start();
				
				// set the frontpage's button invisible and unfocusable
				gamePanel.jbtStartGame.setFocusable(false);
				gamePanel.jbtStartGame.setVisible(false);
				gamePanel.jbtHighScore.setFocusable(false);
				gamePanel.jbtHighScore.setVisible(false);
				gamePanel.jbtExit.setFocusable(false);
				gamePanel.jbtExit.setVisible(false);
				
				gamePanel.changeScreen = 1;
				gamePanel.action = true;
			}
		});
		
		// go to the high score interface
		gamePanel.jbtHighScore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gamePanel.changeScreen = 2;
				gamePanel.setFont(new Font("Jokerman", Font.BOLD, 30));
				gamePanel.jbtStartGame.setFocusable(false);
				gamePanel.jbtStartGame.setVisible(false);
				gamePanel.jbtHighScore.setFocusable(false);
				gamePanel.jbtHighScore.setVisible(false);
				gamePanel.jbtExit.setFocusable(false);
				gamePanel.jbtExit.setVisible(false);
				gamePanel.jbtOK2.setFocusable(true);
				gamePanel.jbtOK2.setVisible(true);
				gamePanel.repaint();
			}
		});
		
		// exit the program
		gamePanel.jbtExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		
		// click ok1 button to view the high score information
		gamePanel.jbtOK1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gamePanel.gameOver = false;
				
				if (gamePanel.highScore.getRank(gamePanel.totalScore) < 5) {
					gamePanel.readAndWriteData = true;
					String userName = JOptionPane.showInputDialog(null, "Please enter your name:", null, JOptionPane.QUESTION_MESSAGE);
					if (gamePanel.readAndWriteData) {
						gamePanel.readAndWriteData = false;
						gamePanel.highScore.sortData(userName, gamePanel.totalScore);
						gamePanel.highScore.writeData();
					}
				}
				gamePanel.jbtOK1.setFocusable(false);
				gamePanel.jbtOK1.setVisible(false);
				gamePanel.jbtOK2.setFocusable(true);
				gamePanel.jbtOK2.setVisible(true);
				gamePanel.changeScreen = 2;
				gamePanel.repaint();
			}
		});
		
		// click ok2 button to go back to the menu of the game
		gamePanel.jbtOK2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				gamePanel.changeScreen = 0;
				gamePanel.jbtOK2.setFocusable(false);
				gamePanel.jbtOK2.setVisible(false);
				gamePanel.jbtStartGame.setFocusable(true);
				gamePanel.jbtStartGame.setVisible(true);
				gamePanel.jbtHighScore.setFocusable(true);
				gamePanel.jbtHighScore.setVisible(true);
				gamePanel.jbtExit.setFocusable(true);
				gamePanel.jbtExit.setVisible(true);
				gamePanel.setFont(new Font("Cooper Black", Font.BOLD, 35));
				gamePanel.repaint();
			}
		});
	}
}
