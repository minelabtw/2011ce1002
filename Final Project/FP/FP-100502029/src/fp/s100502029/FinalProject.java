package fp.s100502029;

import javax.swing.JFrame;

public class FinalProject {
	public static void main(String[] args) {
		FrameWork frame = new FrameWork();
		frame.setSize(500, 500);
		frame.setTitle("Brick Breaker");
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
