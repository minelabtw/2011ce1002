package fp.s100502029;

import java.awt.*;

import javax.swing.*;

public class Brick {
	protected int row = 5, column = 8;
	protected final int WIDTH = 40; // the brick's width
	protected final int HEIGHT = 15; // the brick's height
	protected int effectBallRadius = 7; // the effect ball's radius
	private ImageIcon[][] brickImageIcon = new ImageIcon[row][column];
	protected Image[][] brickImage = new Image[row][column];
	protected int[][] x_brickLeftUpPoint = new int[row][column];
	protected int[][] y_brickLeftUpPoint = new int[row][column];
	protected boolean[][] brickExist = new boolean[row][column];
	protected int[][] effect = new int[row][column]; // 1 is +10 points, 2 is +20 points, 3 is +30 points, 4 is +1 life, 5 is lengthen the bar, 6 is shorten the bar, 7 is +1 life, and > 10 is represent the effect ball should drop down
	protected int[][] effectBallSpeedControl = new int[row][column];
	protected int numberOfDisappearBrick = 0;
	protected int currentStage = 1;
	private String message;
	protected boolean stageMessage = true;
	private int numberOfDisplay; // control the frequence of the message's flashing
	
	public Brick() {
		// initialize every brick and effect ball
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				brickImageIcon[i][j] = new ImageIcon("image/brick1.png");
				brickImage[i][j] = brickImageIcon[i][j].getImage();
				brickExist[i][j] = true;
				effect[i][j] = 0;
				effectBallSpeedControl[i][j] = 0;
			}
		}
	}
	
	// check whether the ball hit the brick or not, if hit, hit which side
	public int hitBrick(int x_ballCenter, int y_ballCenter, int radius) {
		int hitSide = 0; // 0 is not hit, 1 is bottom side, 2 is upper side, 3 is right side, 4 is left side, 5 is hit the corner
		boolean hit = false;
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				if (brickExist[i][j] && !hit) {
					hit = true;
					brickExist[i][j] = false;
					numberOfDisappearBrick++;
					
					// bottom
					if (y_ballCenter >= y_brickLeftUpPoint[i][j] + HEIGHT && y_ballCenter <= y_brickLeftUpPoint[i][j] + HEIGHT + radius && x_ballCenter >= x_brickLeftUpPoint[i][j] && x_ballCenter <= x_brickLeftUpPoint[i][j] + WIDTH)
						hitSide = 1;
					
					// upper
					else if (y_ballCenter >= y_brickLeftUpPoint[i][j] - radius && y_ballCenter <= y_brickLeftUpPoint[i][j] && x_ballCenter >= x_brickLeftUpPoint[i][j] && x_ballCenter <= x_brickLeftUpPoint[i][j] + WIDTH)
						hitSide = 2;
					
					// right
					else if (y_ballCenter >= y_brickLeftUpPoint[i][j] && y_ballCenter <= y_brickLeftUpPoint[i][j] + HEIGHT && x_ballCenter >= x_brickLeftUpPoint[i][j] + WIDTH && x_ballCenter <= x_brickLeftUpPoint[i][j] + WIDTH + radius)
						hitSide = 3;
					
					// left
					else if (y_ballCenter >= y_brickLeftUpPoint[i][j] && y_ballCenter <= y_brickLeftUpPoint[i][j] + HEIGHT && x_ballCenter >= x_brickLeftUpPoint[i][j] - radius && x_ballCenter <= x_brickLeftUpPoint[i][j])
						hitSide = 4;
					
					// corner
					else if ((y_ballCenter >= y_brickLeftUpPoint[i][j] + HEIGHT && y_ballCenter <= y_brickLeftUpPoint[i][j] + HEIGHT + radius && x_ballCenter >= x_brickLeftUpPoint[i][j] - radius && x_ballCenter <= x_brickLeftUpPoint[i][j])
							|| (y_ballCenter >= y_brickLeftUpPoint[i][j] - radius && y_ballCenter <= y_brickLeftUpPoint[i][j] && x_ballCenter >= x_brickLeftUpPoint[i][j] - radius && x_ballCenter <= x_brickLeftUpPoint[i][j])
							|| (y_ballCenter >= y_brickLeftUpPoint[i][j] - radius && y_ballCenter <= y_brickLeftUpPoint[i][j] && x_ballCenter >= x_brickLeftUpPoint[i][j] + WIDTH && x_ballCenter <= x_brickLeftUpPoint[i][j] + WIDTH + radius)
							|| (y_ballCenter >= y_brickLeftUpPoint[i][j] + HEIGHT && y_ballCenter <= y_brickLeftUpPoint[i][j] + HEIGHT + radius && x_ballCenter >= x_brickLeftUpPoint[i][j] + WIDTH && x_ballCenter <= x_brickLeftUpPoint[i][j] + WIDTH + radius))
						hitSide = 5;
					
					// didn't hit
					else {
						hit = false;
						brickExist[i][j] = true;
						numberOfDisappearBrick--;
					}
					
					// if hit the brick, then drop the effect ball
					if (hit)
						effect[i][j] += 10;
				}
			}
		}
		return hitSide;
	}
	
	// reset the brick exist array
	public void resetBrickExist() {
		for (int i = 0; i < row; i++)
			for (int j = 0; j < column; j++)
				brickExist[i][j] = true;
	}
	
	// reset the brick effect to 0
	public void resetEffect() {
		for (int i = 0; i < row; i++)
			for (int j = 0; j < column; j++)
				effect[i][j] = 0;
	}
	
	// random the effect that in the effect ball
	public void randomEffect() {
		for (int i = 0; i < 4; i++) {
			int a = (int)(Math.random() * 5);
			int b = (int)(Math.random() * 8);
			if (effect[a][b] == 0)
				effect[a][b] = 4 + i;
			else
				i--;
		}
		
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				if (effect[i][j] == 0)
					effect[i][j] = (int)(Math.random() * 3) + 1;
			}
		}
	}
	
	// draw stage 1
	public void stage1(Graphics g, int panelWidth, int panelHeight) {
		currentStage = 2;
		for (int i = 0; i < row; i++) {
			x_brickLeftUpPoint[i][0] = panelWidth / 2 - column * WIDTH / 2;
			y_brickLeftUpPoint[i][0] = 95 + HEIGHT * i;
			for (int j = 0; j < column; j++) {
				y_brickLeftUpPoint[i][j] = y_brickLeftUpPoint[i][0];
				if (brickExist[i][j]) {
					currentStage = 1;
					g.drawImage(brickImage[i][j], x_brickLeftUpPoint[i][j], y_brickLeftUpPoint[i][j], WIDTH, HEIGHT, null);
				}
				x_brickLeftUpPoint[i][j] = panelWidth / 2 - column * WIDTH / 2 + WIDTH * j;
			}
		}
		if (currentStage == 1) {
			if (numberOfDisplay % 80 < 40)
				message = "Stage 1";
			else
				message = "";
			numberOfDisplay++;
			if (numberOfDisplay > 400) {
				numberOfDisplay = 0;
				stageMessage = false;
			}
		}
		if (stageMessage) {
			g.setColor(new Color(0, 176, 240));
			FontMetrics fm = g.getFontMetrics();
			int stringWidth = fm.stringWidth(message);
			int stringAscent = fm.getAscent();
			g.drawString(message, panelWidth / 2 - stringWidth / 2, panelHeight / 2 - stringAscent / 2);
		}
	}
	
	// draw stage 2
	public void stage2(Graphics g, int panelWidth, int panelHeight) {
		currentStage = 3;
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < column; j++) {
				if (j % 2 == 0) {
					if (j == 0) {
						x_brickLeftUpPoint[i][j] = panelWidth / 2 - WIDTH * (6 - i);
						y_brickLeftUpPoint[i][j] = 50 + HEIGHT * i;
					}
					else if (j == 2) {
						x_brickLeftUpPoint[i][j] = panelWidth / 2 + WIDTH * (4 - i);
						y_brickLeftUpPoint[i][j] = 50 + HEIGHT * i;
					}
					else if (j == 4) {
						x_brickLeftUpPoint[i][j] = panelWidth / 2 - WIDTH * (2 + i);
						y_brickLeftUpPoint[i][j] = 50 + HEIGHT * (5 + i);
					}
					else if (j == 6) {
						x_brickLeftUpPoint[i][j] = panelWidth / 2 + WIDTH * i;
						y_brickLeftUpPoint[i][j] = 50 + HEIGHT * (5 + i);
					}
				}
				else {
					x_brickLeftUpPoint[i][j] = x_brickLeftUpPoint[i][j - 1] + WIDTH;
					y_brickLeftUpPoint[i][j] = y_brickLeftUpPoint[i][j - 1];
				}
				if (brickExist[i][j]) {
					currentStage = 2;
					g.drawImage(brickImage[i][j], x_brickLeftUpPoint[i][j], y_brickLeftUpPoint[i][j], WIDTH, HEIGHT, null);
				}
			}
		}
		if (currentStage == 2) {
			if (numberOfDisplay % 80 < 40)
				message = "Stage 2";
			else
				message = "";
			numberOfDisplay++;
			if (numberOfDisplay > 500) {
				numberOfDisplay = 0;
				stageMessage = false;
			}
		}
		if (stageMessage) {
			g.setColor(new Color(0, 176, 240));
			FontMetrics fm = g.getFontMetrics();
			int stringWidth = fm.stringWidth(message);
			int stringAscent = fm.getAscent();
			g.drawString(message, panelWidth / 2 - stringWidth / 2, panelHeight / 2 - stringAscent / 2);
		}
	}
}
