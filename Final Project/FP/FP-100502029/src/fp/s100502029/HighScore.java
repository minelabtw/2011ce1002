package fp.s100502029;

import java.io.*;
import java.util.*;

public class HighScore {
	private File file;
	private String[] name = new String[4];
	private int[] score = new int[4];
	private int rank = 1;
	
	public HighScore() {
		file = new File("highscores.txt"); // new a file
		try {
			// read and store the name and score in the file
			Scanner input = new Scanner(file);
			for (int i = 0; i < 4; i++) {
				name[i] = input.next();
				score[i] = input.nextInt();
			}
			input.close(); // close the file
		}
		catch(Exception e) {
			
		}
	}
	
	// get the all of people's name
	public String[] getName() {
		return name;
	}
	
	// get all score
	public int[] getScore() {
		return score;
	}
	
	// get the lowest score
	public int getLowestScore() {
		return score[3];
	}
	
	// get the rank
	public int getRank(int userScore) {
		if (userScore > score[0])
			rank = 1;
		else if (userScore > score[1])
			rank = 2;
		else if (userScore > score[2])
			rank = 3;
		else if (userScore > score[3])
			rank = 4;
		else
			rank = 5;
		return rank;
	}
	
	// sort the user's data and the file's data
	public void sortData(String userName, int userScore) {
		try {
			Scanner input = new Scanner(file);
			for (int i = 0; i < 4; i++) {
				name[i] = input.next();
				score[i] = input.nextInt();
			}
			
			for (int i = 0; i < 4; i++) {
				if (userScore > score[i]) {
					rank = i + 1;
					break;
				}
				rank = 5;
			}
			
			for (int i = 3; i > rank - 1; i--) {
				name[i] = name[i - 1];
				score[i] = score[i - 1];
			}
			
			if (rank < 5) {
				name[rank - 1] = userName;
				score[rank - 1] = userScore;
			}			
			input.close();
		}
		catch(Exception e) {
			
		}
	}

	// write the new Data in to the file
	public void writeData() {
		try {
			PrintWriter output = new PrintWriter(file);
			for (int i = 0; i < 4; i++) {
				output.print(name[i] + " ");
				output.print(score[i]);
				output.println();
			}
			output.close();
		}				
		catch (Exception e) {
			
		}
	}
}
