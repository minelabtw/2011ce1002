package Queue;

public interface Queue {
	public int getSize();
	public int getElementCounter();
	public boolean isFull();
	public boolean isEmpty();
	public void queue(Object anObject) throws Exception;
	public void queue(Object[] anObjectArray) throws Exception;
	public Object peek();
	public Object dequeue() throws Exception;
	public Object[] dequeueAll() throws Exception;
}
