package Queue;

public class ObjectQueue implements Queue {

	private int size;
	private int front, rear;
	private Object[] elements;
	
	public ObjectQueue(int size){
		elements = new Object[size + 1];
		this.size = size + 1;
	}
	
	public int getSize(){
		return size - 1;
	}
	
	// get the number of elements in the queue
	public int getElementCounter(){
		return (rear - front) % size;
	}
	
	// check if the queue is full
	public boolean isFull(){
		return front == (rear + 1) % size;
	}

	// check if the queue is empty
	public boolean isEmpty(){
		return front == rear;
	}

	// add an object into the queue
	public void queue(Object anObject) throws Exception {
		if(isFull()) throw new Exception("The queue is full");
		elements[rear] = anObject;
		rear = ++rear % size;
	}

	// add all the object in the input array into the queue
	public void queue(Object[] anObjectArray) throws Exception {
		if(front == (rear + anObjectArray.length) % size) throw new Exception("The queue is full");
		for(int i = 0; i < anObjectArray.length; i++){
			elements[rear] = anObjectArray[i];
			rear = ++rear % size;
		}
	}

	// peek the first element in the queue
	public Object peek(){
		if(isEmpty()) return null;
		else return elements[front];
	}

	// get the first element out of the queue
	public Object dequeue() throws Exception {
		if(isEmpty()) throw new Exception("The queue is empty");
		front = ++front % size;
		return elements[(front - 1) % size];
	}

	// get all the elements out of the queue
	public Object[] dequeueAll() throws Exception {
		if(isEmpty()) throw new Exception("The queue is empty");
		int elementCounter = getElementCounter();
		Object[] elements = new Object[elementCounter];
		for(int i = 0; i < elementCounter; i++)
			elements[i] = dequeue();
		return elements;
	}

}
