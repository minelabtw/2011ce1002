package BinarySearchTree;

public abstract class BinaryTreeNode {

	private BinaryTreeNode Parent, LeftChild, RightChild;
	private int ChildCounter;
	
	public abstract int Compare(BinaryTreeNode node);
	
	public BinaryTreeNode getParent(){
		return Parent;
	}
	
	public BinaryTreeNode getLeftChild(){
		return LeftChild;
	}
	
	public BinaryTreeNode getRightChild(){
		return RightChild;
	}
	
	// method to set the left child node
	protected void setLeftChild(BinaryTreeNode TreeNode) throws Exception {
		if(TreeNode == null) throw new Exception("Setting a null tree node");
		if(LeftChild != null) throw new Exception("Left child is already existed");
		if(TreeNode.Parent != null) throw new Exception("Inserting tree node with parents");
		LeftChild = TreeNode;
		TreeNode.Parent = this;
		incChildCounter(LeftChild.ChildCounter + 1);
	}
	
	// method to set the right child node
	protected void setRightChild(BinaryTreeNode TreeNode) throws Exception {
		if(TreeNode == null) throw new Exception("Setting a null tree node");
		if(RightChild != null) throw new Exception("Right child is already existed");
		if(TreeNode.Parent != null) throw new Exception("Inserting tree node with parents");
		RightChild = TreeNode;
		TreeNode.Parent = this;
		incChildCounter(RightChild.ChildCounter + 1);
	}
	
	// replace the left child node by a new node
	protected void resetLeftChild(BinaryTreeNode TreeNode) throws Exception{
		if(TreeNode == null) throw new Exception("Setting a null tree node");
		if(TreeNode.Parent != null) TreeNode.delParent();
		if(LeftChild != null) delLeftChild();
		setLeftChild(TreeNode);
	}
	
	// replace the right child node by a new node
	protected void resetRightChild(BinaryTreeNode TreeNode) throws Exception {
		if(TreeNode == null) throw new Exception("Setting a null tree node");
		if(TreeNode.Parent != null) TreeNode.delParent();
		if(RightChild != null) delRightChild();
		setRightChild(TreeNode);
	}
	
	// delete the relationship between the child and parent
	protected void delParent() throws Exception {
		if(Parent == null) throw new Exception("Parent doesn't exist");
		if(Parent.LeftChild == this)
			Parent.delLeftChild();
		else if(Parent.RightChild == this)
			Parent.delRightChild();
	}
	
	// delete the relationship between the left child and the parent
	protected void delLeftChild() throws Exception {
		if(LeftChild == null) throw new Exception("Left child doesn't exist");
		decChildCounter(LeftChild.ChildCounter + 1);
		if(LeftChild.Parent == this) LeftChild.Parent = null;
		LeftChild = null;
	}
	
	// delete the relationship between the right child and the parent
	protected void delRightChild() throws Exception {
		if(RightChild == null) throw new Exception("Right child doesn't exist");
		decChildCounter(RightChild.ChildCounter + 1);
		if(RightChild.Parent == this) RightChild.Parent = null;
		RightChild = null;
	}
	
	public int getChildCounter(){
		return ChildCounter;
	}
	
	// increase the child counter of the current node and its ancestor
	private void incChildCounter(int increment){
		ChildCounter += increment;
		if(Parent != null) Parent.incChildCounter(increment);
	}
	
	// decrease the child counter of the current node and its ancestor
	private void decChildCounter(int decrement){
		ChildCounter -= decrement;
		if(Parent != null) Parent.decChildCounter(decrement);
	}
	
	// reset all child counter
	public int resetChildCounter(){
		if(LeftChild == null && RightChild == null) ChildCounter = 0;
		else if(LeftChild == null) ChildCounter = RightChild.resetChildCounter();
		else if(RightChild == null) ChildCounter = LeftChild.resetChildCounter();
		else ChildCounter = LeftChild.getChildCounter() + RightChild.resetChildCounter();
		return ChildCounter;
	}
	
}
