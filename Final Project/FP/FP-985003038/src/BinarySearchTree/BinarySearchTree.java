package BinarySearchTree;

import Queue.*;

// abstract class of a binary search tree which extends a binary tree node
public abstract class BinarySearchTree extends BinaryTreeNode {
	
	// method to insert a node into the binary search tree
	public void insert(BinaryTreeNode node) throws Exception {
		// set the current node to the header of the tree and start traversal
		BinaryTreeNode currentNode = this;
		while(true){
			// compare with the current node, if the new node is greater than the current node, traversal to the right subtree
			if(currentNode.Compare(node) > 0){
				// if there is no right subtree anymore, just add the new node at that position
				if(currentNode.getRightChild() == null){
					currentNode.setRightChild(node);
					return;
				} else
					currentNode = currentNode.getRightChild();
			// otherwise, traversal to the left subtree
			} else if(currentNode.Compare(node) < 0) {
				// if there is no left subtree anymore, add the new node at that position
				if(currentNode.getLeftChild() == null){
					currentNode.setLeftChild(node);
					return;
				} else
					currentNode = currentNode.getLeftChild();
			// if the current node is equal to the new node, duplicate key is found
			} else
				throw new Exception("Duplicate key");
		}
	}

	// method to delete a node from the tree
	public BinaryTreeNode remove() throws Exception {
		BinaryTreeNode Parent = getParent();
		// if the node which is going to remove has two children
		if(getLeftChild() != null && getRightChild() != null){
			// we need to find its successor
			BinaryTreeNode Successor = findSuccessor();
			((BinarySearchTree) Successor).remove();
			// and move the children to the successor
			if(getLeftChild() != null) Successor.resetLeftChild(getLeftChild());
			if(getRightChild() != null) Successor.resetRightChild(getRightChild());
			
			// replace the node by the successor
			if(Parent != null)
				if(Parent.getLeftChild() == this)
					Parent.resetLeftChild(Successor);
				else if(Parent.getRightChild() == this)
					Parent.resetRightChild(Successor);
			return Successor;
		// if there is only one child or even no children
		} else {
			BinaryTreeNode newChild = null;
			if(getLeftChild() != null)
				newChild = getLeftChild();
			else if(getRightChild() != null)
				newChild = getRightChild();
			
			// replace the node by the child directly
			if(Parent != null){
				// check for the left child
				if(Parent.getLeftChild() == this)
					if(newChild != null)
						Parent.resetLeftChild(newChild);
					else
						Parent.delLeftChild();
				// check for the right child
				else if(Parent.getRightChild() == this)
					if(newChild != null)
						Parent.resetRightChild(newChild);
					else
						Parent.delRightChild();
			} else if(newChild != null)
				newChild.delParent();
			return newChild;
		}
	}
	
	// method to search for a node
	public BinaryTreeNode search(BinaryTreeNode node){
		// if the compare result is 0, the searching node is found
		if(Compare(node) == 0)
			return this;
		// if the compare result is less than 0, that is, the searching node is smaller than the current node
		// keep searching in the left subtree
		else if(Compare(node) < 0 && getLeftChild() != null)
			return ((BinarySearchTree) getLeftChild()).search(node);
		// if the compare result is greater than 0, that is, the searching node is larger than the current node
		// keep searching in the right subtree
		else if(Compare(node) > 0 && getRightChild() != null)
			return ((BinarySearchTree) getRightChild()).search(node);
		// after searching all the nodes, we know the searching node doesn't exist
		return null;
	}

	// method to traversal the tree
	public Object traversal() throws Exception {
		// create a queue to keep the result
		Queue queue = new ObjectQueue(getChildCounter() + 1);
		// traversal the left subtree
		if(getLeftChild() != null)
			queue.queue(((Queue) ((BinarySearchTree) getLeftChild()).traversal()).dequeueAll());
		// traversal the current node
		queue.queue(this);
		// traversal the right subtree
		if(getRightChild() != null)
			queue.queue(((Queue) ((BinarySearchTree) getRightChild()).traversal()).dequeueAll());
		// return the traversal result
		return queue;
	}
	
	// method to find the successor
	private BinaryTreeNode findSuccessor(){
		// if there is a left child
		if(getLeftChild() != null){
			BinaryTreeNode currentNode = getLeftChild();
			// keep finding the right most child of the left child
			while(currentNode.getRightChild() != null){
				currentNode = currentNode.getRightChild();
			}
			return currentNode;
		// if there is no left child but a parent
		} else if(getParent() != null)
			// the parent is the successor
			return getParent();
		// otherwise, no successor is existed
		else
			return null;
	}
}
