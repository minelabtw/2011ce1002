package PhoneBook;

import java.io.Serializable;

public class PhoneData implements Serializable {
	private static final long serialVersionUID = 1L;
	private String Name;
	private String PhoneNumber;
	private PhoneList FriendList;
	
	public PhoneData(String Name, String PhoneNumber){
		this.Name = Name;
		this.PhoneNumber = PhoneNumber;
	}
	
	// add friends' data to the linked list
	public boolean addFriend(PhoneData Friend){
		// if the list is empty, create a new node and set it to be the head of the list
		if(FriendList == null)
			FriendList = new PhoneList(Friend.getName(), Friend.getPhoneNumber());
		// if the list is not empty, check for the same data in the list
		else if(searchFriend(Friend) != null)
			return false;
		else
			FriendList.insertNext(new PhoneList(Friend.getName(), Friend.getPhoneNumber()));
		return true;
	}
	
	// delete friend from the list 
	public boolean delFriend(PhoneData Friend){
		// check for an empty list
		if(FriendList == null)
			return false;
		// if the friend node which is going to be deleted is the head of the list
		// reset a new head of the list
		else if(FriendList.compare(new PhoneList(Friend.getName(), Friend.getPhoneNumber())) == 0)
			if(FriendList.next() != null)
				FriendList = (PhoneList) FriendList.next();
			else
				FriendList = null;
		else
			FriendList.remove(new PhoneList(Friend.getName(), Friend.getPhoneNumber()));
		return true;
	}
	
	// search friend node from the list
	public PhoneList searchFriend(PhoneData Friend){
		return FriendList.search(new PhoneList(Friend.getName(), Friend.getPhoneNumber()));
	}
	
	public String getName(){
		return Name;
	}
	
	public String getPhoneNumber(){
		return PhoneNumber;
	}
	
	public PhoneList getFriendList(){
		return FriendList;
	}
		
	public void setName(String Name){
		this.Name = Name;
	}
	
	public void setPhoneNumber(String PhoneNumber){
		this.PhoneNumber = PhoneNumber;
	}
	
	// compare the input name is smaller than or greater than the name of this object
	public int compareName(String Name){
		return this.Name.compareTo(Name);
	}
	
	// compare the input phone number is smaller than or greater than the phone number of this object
	public int comparePhoneNumber(String PhoneNumber){
		return PhoneNumber.compareTo(this.PhoneNumber);
	}
	
	// compare the input name whether it is equal to the name of this object
	public boolean equalName(String Name){
		return Name.equals(this.Name);
	}
	
	// compare the input phone number whether it is equal to the phone number of this object
	public boolean equalPhoneNumber(String PhoneNumber){
		return this.PhoneNumber.equals(PhoneNumber);
	}
	
	// check if an input object is equal to this object
	public boolean equals(Object anObject){
		if(anObject == null || getClass() != anObject.getClass()) return false;
		PhoneData phoneData = (PhoneData) anObject;
		if(equalName(phoneData.Name) && equalPhoneNumber(phoneData.PhoneNumber)) return true;
		else return false;
	}
}
