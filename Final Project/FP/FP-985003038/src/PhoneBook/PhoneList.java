package PhoneBook;

import java.io.Serializable;

import LinkedList.LinkedList;

public class PhoneList extends LinkedList implements Serializable {
	private static final long serialVersionUID = 1L;
	private String Name;
	private String PhoneNumber;
	
	public PhoneList(String Name, String PhoneNumber){
		this.Name = Name;
		this.PhoneNumber = PhoneNumber;
	}
	
	public String getName(){
		return Name;
	}
	
	public String getPhoneNumber(){
		return PhoneNumber;
	}
	
	public void setData(String Name, String PhoneNumber){
		this.Name = Name;
		this.PhoneNumber = PhoneNumber;
	}

	// method to search a node in the list
	public PhoneList search(PhoneList node){
		PhoneList biPreviousNode = null, previousNode = null, currentNode = this;
		do {
			// compare if the current node is the searching node
			if(currentNode.compare(node) == 0){
				// move the searching node to the front, so if you always search for the same friend
				// the friend node will be at the front of the list
				if(previousNode != null && biPreviousNode != null){
					previousNode.removeNext();
					biPreviousNode.insertNext(currentNode);
				}
				return currentNode;
			}
			biPreviousNode = previousNode;
			previousNode = currentNode;
			currentNode = (PhoneList) currentNode.next;
		} while(currentNode != null);
		return null;
	}
	
	public int compare(LinkedList node) {
		return PhoneNumber.compareTo(((PhoneList) node).PhoneNumber);
	}
}
