package PhoneBook;

import BinarySearchTree.BinarySearchTree;
import BinarySearchTree.BinaryTreeNode;

public class PhoneTree extends BinarySearchTree {
	private PhoneData Data;
	
	public PhoneTree(String Name, String PhoneNumber){
		Data = new PhoneData(Name, PhoneNumber);
	}
	
	public PhoneTree(PhoneData Data) {
		this.Data = Data;
	}

	public PhoneData getData(){
		return (PhoneData) Data;
	}
	
	public void setData(PhoneData Data){
		this.Data = Data;
	}
	
	public void setData(String Name, String PhoneNumber){
		Data.setName(Name);
		Data.setPhoneNumber(PhoneNumber);
	}
	
	// check if an input object is equal to this object
	public boolean equals(Object anObject){
		if(Data == null || anObject == null || getClass() != anObject.getClass()) return false;
		return Data.equals(((PhoneTree) anObject).Data);
	}
	
	public int Compare(BinaryTreeNode node){
		return Data.comparePhoneNumber(((PhoneTree) node).getData().getPhoneNumber());
	}
}
