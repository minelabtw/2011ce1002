package PhoneBookClient;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import PhoneBook.PhoneData;

public class LoginPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private static final Image defaultBG = new ImageIcon("images/bg.jpg").getImage();
	private Image bgImage;
	private JLabel NameLabel;
	private JLabel PhoneNumberLabel;
	private JTextField Name;
	private JTextField PhoneNumber;
	private JButton Login;
	private Font font = new Font("Arial", Font.PLAIN, 15);
	
	// constructor to create all components
	public LoginPanel(){
		setBGImage(defaultBG);
		NameLabel = new JLabel("Name");
		NameLabel.setHorizontalAlignment(JLabel.CENTER);
		NameLabel.setFont(font);
		PhoneNumberLabel = new JLabel("Phone Number");
		PhoneNumberLabel.setHorizontalAlignment(JLabel.CENTER);
		PhoneNumberLabel.setFont(font);
		Name = new JTextField();
		Name.setFont(font);
		PhoneNumber = new JTextField();
		PhoneNumber.setFont(font);
		Login = new JButton("Login");
		Login.setFont(font);
		Login.addActionListener(this);
		
		setLayout(new GridBagLayout());
		GridBagConstraints gbConstraints = new GridBagConstraints();
	    gbConstraints.fill = GridBagConstraints.BOTH;
	    gbConstraints.anchor = GridBagConstraints.CENTER;
	    gbConstraints.insets = new Insets(10, 10, 10, 10);

	    addComp(NameLabel, this, gbConstraints, 0, 0, 1, 1, 3, 0);
	    addComp(Name, this, gbConstraints, 0, 4, 1, 1, 7, 0);
	    addComp(PhoneNumberLabel, this, gbConstraints, 1, 0, 1, 1, 3, 0);
	    addComp(PhoneNumber, this, gbConstraints, 1, 4, 1, 1, 7, 0);
	    addComp(Login, this, gbConstraints, 2, 0, 1, 10, 10, 0);

	}
	
	// add component to the container
	private void addComp(Component c, Container container, GridBagConstraints gbConstraints, int row, int column, int numberOfRows, int numberOfColumns, double weightx, double weighty){
		gbConstraints.gridx = column;
		gbConstraints.gridy = row;
		gbConstraints.gridwidth = numberOfColumns;
		gbConstraints.gridheight = numberOfRows;
		gbConstraints.weightx = weightx;
		gbConstraints.weighty = weighty;
		container.add(c, gbConstraints);
	}
	
	// set background image and size of the image
	public void setBGImage(Image bgImage){
		this.bgImage = bgImage;
	    Dimension size = new Dimension(350, 500);
	    setPreferredSize(size);
	    setMinimumSize(size);
	    setMaximumSize(size);
	    setSize(size);
	}
	
	// draw the background image
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g.drawImage(bgImage, 0, 0, null);
	}

	// button event handler to login
	public void actionPerformed(ActionEvent e){
		// if name and phone number are both filled, and the button is clicked, check login
		if(e.getSource() == Login && !Name.getText().isEmpty() && !PhoneNumber.getText().isEmpty()){
			try {
				// send instruction to the server and check for login
				PhoneData Data = (PhoneData) PhoneBookClient.send("search " + Name.getText().trim() + " " + PhoneNumber.getText().trim());
				// if login is failed, try to create a new account, which is not finish yet
				if(Data == null){
					PhoneBookClient.send("insert " + Name.getText().trim() + " " + PhoneNumber.getText().trim());
					Data = (PhoneData) PhoneBookClient.send("search " + Name.getText().trim() + " " + PhoneNumber.getText().trim());
					if(Data != null)
						PhoneBookClient.login(Data.getFriendList());
					else {
						JOptionPane.showMessageDialog(null, "Error: Login failed", "Phone Simulator", JOptionPane.ERROR_MESSAGE);
						System.exit(0);
					}
				} else
					// if login successful, get friend list and goto the friend list page
					PhoneBookClient.login(Data.getFriendList());
			} catch (Exception ex) {
				// if error occurs, show the message and terminate the program
				JOptionPane.showMessageDialog(null, "Error: " + ex.getMessage(), "Phone Simulator", JOptionPane.ERROR_MESSAGE);
				System.exit(0);
			}
		}
	}
}
