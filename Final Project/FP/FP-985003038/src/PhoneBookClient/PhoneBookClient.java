package PhoneBookClient;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.net.Socket;

import javax.swing.JOptionPane;

import PhoneBook.PhoneData;
import PhoneBook.PhoneList;

public class PhoneBookClient {
	// server IP and port number
	private final static String serverIP = "127.0.0.1";
	private final static int serverPort = 8888;
	private static Socket socket;
	private static PrintStream output;
	private static ObjectInputStream input;
	private static FrameWork frame;
	
	public static void main(String[] args){
		try {
			// create socket and data stream
			socket = new Socket(serverIP, serverPort);
			output = new PrintStream(socket.getOutputStream());
			input = new ObjectInputStream(socket.getInputStream());
			// create a user interface framework
			frame = new FrameWork();
			do{
				// try to read a input string from the console
				System.out.print("Command >> ");
				BufferedReader inputBuffer = new BufferedReader(new InputStreamReader(System.in));
				String command = inputBuffer.readLine();
				// call send method to send the command to server 
				send(command);
			} while(true);
		} catch (Exception e) {
			// if there is any error, show the error message and terminate the program
			JOptionPane.showMessageDialog(null, "Error: " + e.getMessage(), "Phone Simulator", JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
	}
	
	public static Object send(String command) throws Exception {
		// send the input command out through the output stream
		output.println(command);
		output.flush();
		// if the command is searching a user node, return the user node, otherwise, return the result return from the server
		if(command.toLowerCase().startsWith("search")){
			return (PhoneData) input.readObject();
		} else
			return null;
	}

	// if login success, update the framework
	public static void login(PhoneList Friend){
		frame.showFriendList(Friend);
	}
}
