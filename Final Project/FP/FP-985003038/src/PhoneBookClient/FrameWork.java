package PhoneBookClient;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.Timer;

import PhoneBook.PhoneList;

public class FrameWork extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	private Timer LoadingTimer = new Timer(2000, this);

	public FrameWork(){
		setLayout(new GridLayout(1,1));
		add(new StartPanel());
		LoadingTimer.start();
		
		setTitle("Phone Simulator");
		setSize(350, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);		
		setVisible(true);
	}

	// timer event handler for the logo page
	public void actionPerformed(ActionEvent e){
		// when the timer event happened, refresh to the login page
		if(e.getSource() == LoadingTimer){
			invalidate();
			LoadingTimer.stop();
			getContentPane().removeAll();
			setLayout(new GridLayout(1,1));
			add(new LoginPanel());
			validate();
		}
	}
	
	// refresh to the friend list page
	public void showFriendList(PhoneList Friend){
		invalidate();
		getContentPane().removeAll();
		setLayout(new GridLayout(1,1));
		add(new FriendListPanel(Friend));
		validate();
	}
	
}
