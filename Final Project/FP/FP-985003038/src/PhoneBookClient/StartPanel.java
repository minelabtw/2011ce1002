package PhoneBookClient;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class StartPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private static final Image defaultBG = new ImageIcon("images/logo.png").getImage();
	private Image bgImage;

	public StartPanel(){
		setBGImage(defaultBG);
	}
	
	// set the background image and size of the image
	public void setBGImage(Image bgImage){
		this.bgImage = bgImage;
	    Dimension size = new Dimension(350, 500);
	    setPreferredSize(size);
	    setMinimumSize(size);
	    setMaximumSize(size);
	    setSize(size);
	}
	
	// draw the background
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g.drawImage(bgImage, 0, 0, null);
	}
}
