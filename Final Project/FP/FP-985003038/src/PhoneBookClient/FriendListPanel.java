package PhoneBookClient;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import PhoneBook.PhoneList;

public class FriendListPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private static final Image defaultBG = new ImageIcon("images/bg.jpg").getImage();
	private Image bgImage;
	private String[] NameArray;
	private String[] PhoneNumberArray;
	private JButton[] ButtonArray;
	private int recordCounter = 0;

	public FriendListPanel(PhoneList Friend){
		setLayout(new GridBagLayout());
		GridBagConstraints gbConstraints = new GridBagConstraints();
	    gbConstraints.fill = GridBagConstraints.BOTH;
	    gbConstraints.anchor = GridBagConstraints.CENTER;
	    gbConstraints.insets = new Insets(10, 10, 10, 10);

		setBGImage(defaultBG);
		if(Friend != null){
			PhoneList currentNode = Friend;
			do {
				recordCounter++;
			} while((currentNode = (PhoneList) currentNode.next()) != null);

			NameArray = new String[recordCounter];
			PhoneNumberArray = new String[recordCounter];
			ButtonArray = new JButton[recordCounter]; 
			
			// for each friends, draw a button
			setLayout(new GridBagLayout());
			for(int i = 0; i < recordCounter; i++){
				NameArray[i] = Friend.getName();
				PhoneNumberArray[i] = Friend.getPhoneNumber();
				ButtonArray[i] = new JButton(Friend.getName());
				ButtonArray[i].setFont(new Font("Arial", Font.PLAIN, 15));
				ButtonArray[i].setHorizontalAlignment(JLabel.CENTER);
				addComp(ButtonArray[i], this, gbConstraints, i, 0, 1, 1, 0, 0);
				// button click event handler, which show the messagebox when you click the button
				ButtonArray[i].addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e){
						for(int i = 0; i < recordCounter; i++)
							if(e.getSource() == ButtonArray[i])
								JOptionPane.showMessageDialog(null, NameArray[i] + "'s phone number is: " + PhoneNumberArray[i], "Phone Simulator", JOptionPane.INFORMATION_MESSAGE);
					}
				});
				Friend = (PhoneList) Friend.next();
			}
		}
	}
	
	// add component to the gridbag layout container
	private void addComp(Component c, Container container, GridBagConstraints gbConstraints, int row, int column, int numberOfRows, int numberOfColumns, double weightx, double weighty){
		gbConstraints.gridx = column;
		gbConstraints.gridy = row;
		gbConstraints.gridwidth = numberOfColumns;
		gbConstraints.gridheight = numberOfRows;
		gbConstraints.weightx = weightx;
		gbConstraints.weighty = weighty;
		container.add(c, gbConstraints);
	}
	
	// set the background image and size of the image
	public void setBGImage(Image bgImage){
		this.bgImage = bgImage;
	    Dimension size = new Dimension(350, 500);
	    setPreferredSize(size);
	    setMinimumSize(size);
	    setMaximumSize(size);
	    setSize(size);
	    setLayout(new GridLayout(1,1));
	}
	
	// draw the background image
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g.drawImage(bgImage, 0, 0, null);
	}
}
