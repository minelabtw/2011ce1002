package PhoneBookServer;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

import PhoneBook.PhoneList;
import PhoneBook.PhoneTree;
import PhoneBook.PhoneData;
import Queue.ObjectQueue;
import Queue.Queue;

public class PhoneBookServer {
	private static PhoneTree phoneBook;
	private static boolean commandFlag = false;
	
	// initialize method to create threads
	private static void initialize(){
		ListeningThread listening = new ListeningThread();
		Thread listeningThread = new Thread(listening);
		listeningThread.start();
		SystemMsg("Initialization finished");
	}
	
	// main method which read command from the console
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		Help();
		initialize();
		while(true){
			commandFlag = true;
			System.out.print("Command >> ");
			try {
				// try to execute the input operation
				SelectOperation(input.nextLine());
			} catch (Exception e){
				// if error occurs, show the error message
				SystemMsg(e.getMessage());
			}
		}
	}
	
	// method to choose sub-method in order to execute such operation
	private static void SelectOperation(String command) throws Exception {
		commandFlag = false;
		if(command.toLowerCase().equals("help"))
			Help();
		else if(command.toLowerCase().startsWith("list"))
			List();
		else if(command.toLowerCase().startsWith("insert"))
			Insert(command.toLowerCase());
		else if(command.toLowerCase().startsWith("delete"))
			Delete(command.toLowerCase());
		else if(command.toLowerCase().startsWith("search"))
			Search(command.toLowerCase());
		else if(command.toLowerCase().startsWith("update"))
			Update(command.toLowerCase());
		else if(command.toLowerCase().startsWith("friend"))
			Friend(command.toLowerCase());
		else if(command.toLowerCase().startsWith("unfriend"))
			Unfriend(command.toLowerCase());
		else if(command.toLowerCase().startsWith("save"))
			Save();
		else if(command.toLowerCase().startsWith("load"))
			Load();
		else if(command.toLowerCase().startsWith("halt"))
			Halt();
		else if(!command.isEmpty())
			SystemMsg("No such operation");
	}

	// method to print out the message from the connected clients
	static void ClientMsg(String str, String addr){
		if(commandFlag) System.out.println();
		System.out.println(addr + " >> " + str);
		if(commandFlag) System.out.print("Command >> ");
	}
	
	// method to print out the system message
	static void SystemMsg(String str){
		if(commandFlag) System.out.println();
		System.out.println("System >> " + str);
		if(commandFlag) System.out.print("Command >> ");
	}
	
	// sub-method to print the help list
	static void Help(){
		SystemMsg("You can do the following operation:\n" +
				"======================================================\n" +
				"[Help] list out all the operations you can do.\n" +
				"[List] list out all the records stored in the database.\n" +
				"[Insert NAME PHONENUMBER] insert a record to the database.\n" +
				"[Delete NAME PHONENUMBER] search for a record, which match the NAME and PHONENUMBER, and delete it from the database.\n" +
				"[Search NAME PHONENUMBER] search for a record which match the NAME and PHONENUMBER from the database.\n" +
				"[Update ORGNAME ORGPHONENUMBER NEWNAME NEWPHONENUMBER] update a record in the database.\n" +
				"[Friend NAME PHONENUMBER FRIENDNAME FRIENDPHONENUMBER] add a friend to NAME's friend list\n" +
				"[Unfriend NAME PHONENUMBER FRIENDNAME FRIENDPHONENUMBER] delete a friend from NAME's friend list\n" +
				"[Save] save all the records in database to a storage file.\n" +
				"[Load] load records from a storage file to database.\n" +
				"[Halt] halt the server.\n" +
				"======================================================");
	}
	
	// sub-method to print the user list
	static void List() throws Exception {
		// if there is no user in the user list yet, show the null result
		if(phoneBook == null)
			SystemMsg("No records are found");
		else {
			// traversal the binary tree and return the traversal process with a queue object
			Queue list = (ObjectQueue) phoneBook.traversal();
			
			String result = "All records in the phonebook:\n======================================================\n";
			// for each elements, compute with the following instructions using a loop
			int elementCounter = list.getElementCounter();
			for(int i = 0; i < elementCounter; i++){
				// get the first element from the queue
				PhoneData Data = ((PhoneTree) list.dequeue()).getData();
				// print the name and phone number data of the element
				result = result + (i + 1) + "\t\t" + Data.getName() + "\t\t" + Data.getPhoneNumber() + "\n";
				// and also get the friend list of that user
				PhoneList Friend = Data.getFriendList();
				if(Friend != null)
					// print out all his friends
					do {
						result = result + "\tFD:\t" + Friend.getName() + "\t\t" + Friend.getPhoneNumber() + "\n";
					} while((Friend = (PhoneList) Friend.next()) != null);
			}
			result = result + "======================================================";
			
			SystemMsg(result);	
		}
	}
	
	// sub-method to analysis the input instruction and insert a new user to the list
	static boolean Insert(String command) throws Exception {
		String Name, PhoneNumber;
		
		try {
			// try to analysis the input instruction
			Scanner scanner = new Scanner(command);
			scanner.next();
			Name = scanner.next();
			PhoneNumber = scanner.next();
		} catch(Exception e){
			// if the instruction is not complete, it will has a null exception, and throw it out
			throw new Exception("Parameters missing");
		}
		
		// if the instruction is correct, call an other method to execute, and also return the result
		if(Insert(new PhoneTree(Name, PhoneNumber))){
			SystemMsg("Insert success");
			return true;
		} else
			return false;
	}
	
	// sub-method to insert a new user to the list
	static boolean Insert(PhoneTree Node) throws Exception {
		// if there is no user yet, set the header of the tree pointing to the input node
		// otherwise, call the inserting node method of binary tree to insert a new node
		if(phoneBook == null)
			phoneBook = Node;
		else
			phoneBook.insert(Node);
		return true;
	}
	
	// sub-method to analysis the input instruction and delete a user in the list
	static boolean Delete(String command) throws Exception {
		String Name, PhoneNumber;
		
		try {
			// try to analysis the instruction
			Scanner scanner = new Scanner(command);
			scanner.next();
			Name = scanner.next();
			PhoneNumber = scanner.next();
		} catch(Exception e){
			throw new Exception("Parameters missing");
		}
		
		// if there is no mistakes, call another method to do so
		if(Delete(Name, PhoneNumber)){
			SystemMsg("Delete success");
			return true;
		} else
			return false;
	}
	
	// sub-method to delete a user in the list
	static boolean Delete(String Name, String PhoneNumber) throws Exception {
		// find the location of the user node
		PhoneTree location = Search(Name, PhoneNumber);
		// if the node is not be found, return the null result
		if(location == null){
			SystemMsg("No records are found");
			return false;
		} else {
			// if the node we are going to delete is the header of the binary tree, delete the null and reset the header
			// otherwise, just delete the node using the deleting method of binary tree
			if(location.getParent() == null)
				phoneBook = (PhoneTree) location.remove();
			else
				location.remove();
			
			// also, reset the relationship between him and his friends
			PhoneList Friend = location.getData().getFriendList();
			if(Friend != null)
				// search for each friends, and delete the relationship node of his
				do {
					Search(Friend.getName(), Friend.getPhoneNumber()).getData().delFriend(location.getData());
				} while((Friend = (PhoneList) Friend.next()) != null);
			return true;
		}
	}
	
	// sub-method to analysis the input instruction and search for a user node
	static PhoneTree Search(String command) throws Exception{
		String Name, PhoneNumber;
		try {
			// try to analysis the input instruction
			Scanner scanner = new Scanner(command);
			scanner.next();
			Name = scanner.next();
			PhoneNumber = scanner.next();
		} catch(Exception e){
			throw new Exception("Parameters missing");
		}
		
		// call the sub-method to find the user node
		PhoneTree result = Search(Name, PhoneNumber);
		// if the node is not be found, return the null result
		// otherwise, return the node and print a succeed message
		if(result == null)
			SystemMsg("No records are found");
		else
			SystemMsg(result.getData().getName() + " ( " + result.getData().getPhoneNumber() + " ) is found");
		return result;
	}
	
	// sub-method to search a user node
	static PhoneTree Search(String Name, String PhoneNumber) throws Exception {
		if(Name == null || PhoneNumber == null)
			throw new Exception("Parameters missing");
		
		// if the list is empty, the user node must not be found
		if(phoneBook == null)
			return null;
		
		// search the node using the search method of the binary tree, and return the result
		PhoneTree result = (PhoneTree) phoneBook.search(new PhoneTree(Name, PhoneNumber));
		if(result != null && result.getData().getName().equals(Name) && result.getData().getPhoneNumber().equals(PhoneNumber))
			return result;
		else
			return null;
	}
	
	// sub-method to analysis the instruction and update the value of a user node
	static boolean Update(String command) throws Exception {
		String OrgName, OrgPhoneNumber, NewName, NewPhoneNumber;
		
		try {
			Scanner scanner = new Scanner(command);
			scanner.next();
			OrgName = scanner.next();
			OrgPhoneNumber = scanner.next();
			NewName = scanner.next();
			NewPhoneNumber = scanner.next();
		} catch(Exception e){
			throw new Exception("Parameters missing");
		}
		
		// call the sub-method to update the value of a user node
		if(Update(OrgName, OrgPhoneNumber, NewName, NewPhoneNumber)){
			SystemMsg("Update success");
			return true;
		} else
			return false;
	}
	
	// sub-method to update the value of a user node
	static boolean Update(String OrgName, String OrgPhoneNumber, String NewName, String NewPhoneNumber) throws Exception {
		// before updating the value, search for the user node
		PhoneTree location = Search(OrgName, OrgPhoneNumber);
		
		if(location == null){
			SystemMsg("No records are found");
			return false;
		} else {
			// first, remove the node from the binary tree
			if(location.getParent() == null)
				phoneBook = (PhoneTree) location.remove();
			else
				location.remove();
			// update the data of the node
			location.setData(NewName, NewPhoneNumber);
			// insert it back to the tree
			Insert(location);
			// also, update the data in the friend's node
			PhoneList Friend = location.getData().getFriendList();
			if(Friend != null)
				// search for each friends and update the nodes
				do {
					Search(Friend.getName(), Friend.getPhoneNumber()).getData().getFriendList().search(new PhoneList(OrgName, OrgPhoneNumber)).setData(NewName, NewPhoneNumber);
				} while((Friend = (PhoneList) Friend.next()) != null);
			return true;
		}
	}
	
	// sub-method to analysis instruction and add relationship between two users
	static boolean Friend(String command) throws Exception{
		String Name, PhoneNumber, FriendName, FriendPhoneNumber;
		
		try {
			Scanner scanner = new Scanner(command);
			scanner.next();
			Name = scanner.next();
			PhoneNumber = scanner.next();
			FriendName = scanner.next();
			FriendPhoneNumber = scanner.next();
		} catch(Exception e){
			throw new Exception("Parameters missing");
		}
		
		// call sub-method to add relationship
		if(Friend(Name, PhoneNumber, FriendName, FriendPhoneNumber)){
			SystemMsg("Add friend success");
			return true;
		} else {
			SystemMsg("Add friend failed");
			return false;
		}
	}
	
	// sub-method to add relationship between two users
	static boolean Friend(String Name, String PhoneNumber, String FriendName, String FriendPhoneNumber) throws Exception {
		// search for the user's node and the friend's node
		PhoneTree location = Search(Name, PhoneNumber);
		PhoneTree friend = Search(FriendName, FriendPhoneNumber);
		if(location == null || friend == null){
			SystemMsg("No records are found");
			return false;
		} else {
			// call the add friend method to add relationship
			return location.getData().addFriend(friend.getData()) && friend.getData().addFriend(location.getData());
		}
	}
	
	// sub-method to analysis instruction and break relationship between two users
	static boolean Unfriend(String command) throws Exception{
		String Name, PhoneNumber, FriendName, FriendPhoneNumber;
		
		try {
			Scanner scanner = new Scanner(command);
			scanner.next();
			Name = scanner.next();
			PhoneNumber = scanner.next();
			FriendName = scanner.next();
			FriendPhoneNumber = scanner.next();
		} catch(Exception e){
			throw new Exception("Parameters missing");
		}
		
		// call sub-method to do so
		if(Unfriend(Name, PhoneNumber, FriendName, FriendPhoneNumber)){
			SystemMsg("Delete friend success");
			return true;
		} else {
			SystemMsg("Delete friend failed");
			return false;
		}
	}
	
	// sub-method to break relationship between two users
	static boolean Unfriend(String Name, String PhoneNumber, String FriendName, String FriendPhoneNumber) throws Exception {
		// find the user's node and friend's node
		PhoneTree location = Search(Name, PhoneNumber);
		PhoneTree friend = Search(FriendName, FriendPhoneNumber);
		if(location == null || friend == null){
			SystemMsg("No records are found");
			return false;
		} else {
			// call the delete friend method to break relationship
			return location.getData().delFriend(friend.getData()) && friend.getData().delFriend(location.getData());
		}
	}
	
	// sub-method to save user list
	static void Save() throws Exception {
		// create a data stream to the file
		ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream("phonebook.dat"));
		
		// traversal the phone book and get all the users' data
		Queue queue = (ObjectQueue) phoneBook.traversal();
		int elementCounter = queue.getElementCounter();
		// record the total number of records in the phone book
		output.write(elementCounter);
		// for each record, write the serializible object to the file
		for(int i = 0; i < elementCounter; i++)
			output.writeObject(((PhoneTree) queue.dequeue()).getData());
		output.flush();
		output.close();
		SystemMsg("Saved");
	}
	
	// sub-method to load user list
	static void Load() throws Exception {
		// create a data stream to the file
		ObjectInputStream input = new ObjectInputStream(new FileInputStream("phonebook.dat"));
		// read the total number of records in the file
		int elementCounter = input.read();
		// create an array with size according to the number of records
		PhoneData[] Data = new PhoneData[elementCounter];
		// read all the serializible object from the file
		for(int i = 0; i < elementCounter; i++)
			Data[i] = (PhoneData) input.readObject();
		input.close();
		// initialize the binary tree header
		phoneBook = null;
		// call the sub-method to built the tree
		Build(Data, 0, Data.length);
		SystemMsg("Loaded");
	}
	
	// sub-method to build the binary tree, which will build a optimize binary search tree
	private static void Build(PhoneData[] Data, int startIndex, int size) throws Exception {
		// cut the tree into half, find the node position at the middle
		// insert the node at the middle into the tree
		int middle = startIndex + (size - 1) / 2;
		Insert(new PhoneTree(Data[middle]));
		// if there are elements at the left of the middle point
		// recusive call this function to add the left sub-tree to the binary tree 
		if(startIndex != middle)
			Build(Data, startIndex, middle - startIndex);
		// also, if there are elements at the right of the middle point
		// recusive call this function to add the right sub-tree to the binary tree
		if(startIndex + size - 1 != middle)
			Build(Data, middle + 1, startIndex + size - 1 - middle);
	}
	
	// sub-method to terminate the server
	static void Halt(){
		if(phoneBook != null){
			// ask if the administrator want to save the list before termination
			SystemMsg("Do you want to save the records before termination? [Y]");
			System.out.print("Command >> ");
			Scanner input = new Scanner(System.in);
			// if the reply is not NO, save the list
			if(!input.nextLine().toLowerCase().startsWith("n"))
				try {
					Save();
				} catch(Exception e){
					SystemMsg(e.getMessage());
				}
		}
		SystemMsg("Terminated");
		System.exit(0);
	}
}
