package PhoneBookServer;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;

import PhoneBook.PhoneTree;

class ConnectionThread implements Runnable {
	private Socket clientSocket;
	private ObjectOutputStream output;
	private BufferedReader input;
	
	public ConnectionThread(Socket socket){
		this.clientSocket = socket;
	}
	
	public void run(){
		try {
			output = new ObjectOutputStream(clientSocket.getOutputStream());
			input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			while(true){
				// keep reading socket buffer
				String command = input.readLine();
				// call the method to execute the instruction
				DoOperation(command, clientSocket.getInetAddress().getHostAddress());
			}
		} catch(Exception e){
			// if any errors occur, print the error message
			PhoneBookServer.ClientMsg(e.getMessage(), clientSocket.getInetAddress().getHostAddress());
			try {
				// try to reply the exception message to the client
				output.writeObject(e.getMessage());
			} catch (Exception ex) {
				PhoneBookServer.SystemMsg(ex.getMessage());
			}	
		}
	}
	
	// method to execute instructions
	private void DoOperation(String command, String addr) throws Exception {
		String[] args = null;
		PhoneBookServer.ClientMsg(command, addr);
		try {
			Scanner scanner = new Scanner(command);
			scanner.next();
			// according to the different instruction, scan for the different numbers of argument
			if(command.toLowerCase().startsWith("insert") || command.toLowerCase().startsWith("delete") || command.toLowerCase().startsWith("search"))
				args = new String[]{scanner.next(), scanner.next()}; 
			else if(command.toLowerCase().startsWith("update") || command.toLowerCase().startsWith("friend") || command.toLowerCase().startsWith("unfriend"))
				args = new String[]{scanner.next(), scanner.next(), scanner.next(), scanner.next()};
			else if(!command.isEmpty())
				output.writeChars("No such operation");
		} catch(Exception e){
			throw new Exception("Parameters missing");
		}
		
		// for the different instructions, call the corresponding method to do so
		if(command.toLowerCase().startsWith("insert"))
			if(PhoneBookServer.Insert(new PhoneTree(args[0], args[1])))
				output.writeChars("Insert success");
			else
				output.writeChars("Insert failed");
		else if(command.toLowerCase().startsWith("delete"))
			if(PhoneBookServer.Delete(args[0], args[1]))
				output.writeChars("Delete success");
			else
				output.writeChars("Delete failed");
		else if(command.toLowerCase().startsWith("search")){
			PhoneTree searchResult = (PhoneTree) PhoneBookServer.Search(args[0], args[1]);
			if(searchResult != null)
				output.writeObject(searchResult.getData());
			else
				throw new Exception("Search failed");
		} else if(command.toLowerCase().startsWith("update"))
			if(PhoneBookServer.Update(args[0], args[1], args[2], args[3]))
				output.writeChars("Update success");
			else
				output.writeChars("Update failed");
		else if(command.toLowerCase().startsWith("friend"))
			if(PhoneBookServer.Friend(args[0], args[1], args[2], args[3]))
				output.writeChars("Add friend success");
			else
				output.writeChars("Add friend failed");
		else if(command.toLowerCase().startsWith("unfriend"))
			if(PhoneBookServer.Unfriend(args[0], args[1], args[2], args[3]))
				output.writeChars("Delete friend success");
			else
				output.writeChars("Delete friend failed");
		output.flush();
	}
}
