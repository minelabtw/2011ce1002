package PhoneBookServer;

import java.net.ServerSocket;
import java.net.Socket;

public class ListeningThread implements Runnable {
	private final static int defaultListenPort = 8888;
	private int listenPort;
	private ServerSocket serverSocket;
	private Socket clientSocket;
	
	public ListeningThread(){
		listenPort = defaultListenPort;
		initialize();
	}
	
	public ListeningThread(int listenPort){
		this.listenPort = listenPort;
		initialize();
	}
	
	// initialize the listening socket
	private void initialize(){
		try {
			serverSocket = new ServerSocket(listenPort);
			PhoneBookServer.SystemMsg("Server is now waiting for connection");
		} catch(Exception e){
			PhoneBookServer.SystemMsg(e.getMessage());
			PhoneBookServer.Halt();
		}
	}
	
	public void run(){
		try {
			while(true){
				// keep accepting connection
				clientSocket = serverSocket.accept();
				String HostAddress = clientSocket.getInetAddress().getHostAddress();
				PhoneBookServer.SystemMsg(HostAddress + " connected");
				// throw it to a new thread to handle the connection
				Thread thread = new Thread(new ConnectionThread(clientSocket));
				thread.start();
			}
		} catch(Exception e){
			PhoneBookServer.SystemMsg(e.getMessage());
			PhoneBookServer.Halt();
		}
	}
}
