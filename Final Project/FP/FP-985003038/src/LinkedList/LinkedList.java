package LinkedList;

import java.io.Serializable;

public abstract class LinkedList implements Serializable {
	private static final long serialVersionUID = 1L;
	protected LinkedList next;
	
	public LinkedList next(){
		return next;
	}
	
	// insert the next node
	public void insertNext(LinkedList node){
		if(node == null)
			next = node;
		else {
			node.next = next;
			next = node;
		}
	}
	
	// remove the following node
	public void removeNext(){
		if(next != null){
			if(next.next != null)
				next = next.next;
			else
				next = null;
		}
	}
	
	// find out the specified node and remove it from the linked list
	public void remove(LinkedList node){
		LinkedList currentNode = this;
		while(currentNode.next != null){
			if(currentNode.next.compare(node) == 0){
				currentNode.removeNext();
				return;
			}
			currentNode = currentNode.next;
		}
	}
	
	// search for a node
	public LinkedList search(LinkedList node){
		LinkedList currentNode = this;
		do {
			if(currentNode.compare(node) == 0)
				return currentNode;
		} while((currentNode = currentNode.next) != null);
		return null;
	}
	
	public abstract int compare(LinkedList node);
}
