package fp.s100502544;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

public class Framework extends JFrame implements ActionListener {
	JPanel p1 = new JPanel();// 選項 上面
	JPanel p21 = new JPanel();// 內容1 easy
	JPanel p22 = new JPanel();// 內容2 different
	JPanel p3 = new JPanel();// 顯示訊息 下面 計時
	JButton[][] jbtp21 = new JButton[9][9];// 宣告 1 的 button
	JButton[][] jbtp22 = new JButton[18][18];// 2 button

	JButton startButton = new JButton("開始");
	JButton stopButton = new JButton("暫停");
	JButton endButton = new JButton("結束");

	Font font1 = new Font("Dialog", Font.BOLD, 20);// 上層JBUTTON字型大小
	Font font2 = new Font("Dialog", Font.BOLD, 35);// 中層JBUTTON字型大小(遊戲中)
													// //下層JLabel 時間 字型大小
	Font font3 = new Font("Dialog", Font.BOLD, 50);// 下層JLabel 炸彈數量 字型大小

	Border lineborder = new BevelBorder(BevelBorder.RAISED);
	Border lineborder1 = new BevelBorder(BevelBorder.LOWERED);

	Color color = Color.WHITE;

	// 儲存炸彈 (陣列)
	boolean boom1[][] = new boolean[9][9];
	boolean boom2[][] = new boolean[18][18];

	// 遊戲中炸彈圖
	public ImageIcon icon = new ImageIcon("image/boom.jpg");
	// 時鐘圖
	public ImageIcon icon2 = new ImageIcon("image/time.jpg");
	// 炸彈數量前置圖
	public ImageIcon icon3 = new ImageIcon("image/boom1.jpg");

	private Timer timer;// 宣告timer constructor
	private JLabel screen = new JLabel("00:00:00");// 宣告一個JLabel 顯示時間

	// 宣告 JLabel 放 時鐘 照片
	private JLabel pictureJLabel = new JLabel(icon2);
	// 宣告 JLabel 放 炸彈 照片
	private JLabel boompictureJLabel = new JLabel(icon3);
	// 宣告 JLabel 顯示 有10個炸彈
	private JLabel boom1numberJLabel = new JLabel("X10");
	// 宣告 JLabel 顯示 有65個炸彈
	private JLabel boom2numberJLabel = new JLabel("X65");

	private clock c = new clock();// 計算時間 class

	int textboom = 0;// 設定變數 算按下 正確BUTTON的數量

	int a;// 把num變成 public變數

	boolean run = true;//如果按暫停  裡面的BUTTON按鈕不可以按

	Audio audio = new Audio();// 音樂class
	

	public Framework(int num) {
		audio.Audio();//開始遊戲   音樂開始
		audio.playBackgroundMusic();
		timer = new Timer(1000, new TimerListener());
		// 設定一秒走一格
		a = num;

		setLayout(new BorderLayout(5, 5));

		p1.setLayout(new GridLayout(1, 3, 5, 5));

		p1.add(startButton);
		startButton.addActionListener(this);
		startButton.setFont(font1);
		startButton.setBorder(lineborder);
		startButton.setBackground(Color.MAGENTA);
		startButton.setForeground(color);

		p1.add(stopButton);
		stopButton.addActionListener(this);
		stopButton.setFont(font1);
		stopButton.setBorder(lineborder);
		stopButton.setBackground(Color.MAGENTA);
		stopButton.setForeground(color);

		p1.add(endButton);
		endButton.addActionListener(this);
		endButton.setFont(font1);
		endButton.setBorder(lineborder);
		endButton.setBackground(Color.MAGENTA);
		endButton.setForeground(color);

		add(p1, BorderLayout.NORTH);

		if(num == 1){
			
			add(p21, BorderLayout.CENTER);
			p21.setLayout(new GridLayout(9, 9));// easy 9*9

			// 顯示Button
			for (int i = 0; i < 9; i++) {
				for (int j = 0; j < 9; j++) {
					jbtp21[i][j] = new JButton();
					jbtp21[i][j].addActionListener(this);
					jbtp21[i][j].setBackground(Color.ORANGE);
					p21.add(jbtp21[i][j]);
				}

			}

			// 簡單版 炸彈數量初始化
			int number = 0;
			while (number < 10) {
				// 隨機取樣x和y 範圍 0到8
				int x = (int) (Math.random() * 8);
				int y = (int) (Math.random() * 8);//0到8

				// 判斷隨機取的[x][y]有沒有炸彈 false/true
				// 那炸彈數量+1 直到10個炸彈   而且把[x][y]設為 true(有炸彈)
				if (boom1[x][y] == false) {// false表示沒有炸彈 判斷這個炸彈有沒有重複
					number++;
					boom1[x][y] = true;// 設為 true 有炸彈
				}
			}

		} 
		else if (num == 2) {
			add(p22, BorderLayout.CENTER);
			p22.setLayout(new GridLayout(18, 18));// different 99*99
			for (int i = 0; i < 18; i++) {
				for (int j = 0; j < 18; j++) {
					jbtp22[i][j] = new JButton();//宣告  BUTTON
					jbtp22[i][j].addActionListener(this);
					jbtp22[i][j].setBackground(Color.GREEN);
					p22.add(jbtp22[i][j]);
				}
			}

			// 難度板 炸彈數量初始化
			int number = 0;
			while (number < 65) {
				// 隨機取樣x和y 範圍 0到8
				int x = (int) (Math.random() * 17);
				int y = (int) (Math.random() * 17);

				
				// 判斷隨機取的[x][y]有沒有炸彈 false/true
				// 那炸彈數量+1 直到10個炸彈 而且把[x][y]設為 true(有炸彈)
				if (boom2[x][y] == false) {// false表示沒有炸彈
					number++;
					boom2[x][y] = true;// true有炸彈
				}
			}
		}

		p3.setLayout(new GridLayout(1, 4, 5, 5));
		p3.add(pictureJLabel);
		screen.setFont(font2);
		screen.setForeground(Color.RED);
		p3.add(screen);
		p3.add(boompictureJLabel);
		if (num == 1) {
			boom1numberJLabel.setFont(font3);
			boom1numberJLabel.setBorder(lineborder1);
			boom1numberJLabel.setBackground(Color.pink);
			p3.add(boom1numberJLabel);
		} 
		else if (num == 2) {
			boom2numberJLabel.setFont(font3);
			boom2numberJLabel.setBorder(lineborder1);
			boom2numberJLabel.setBackground(Color.blue);
			p3.add(boom2numberJLabel);
		}
		add(p3, BorderLayout.SOUTH);

	}

	// ActionEvent 按下BUTTON 發生....
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

		if (e.getSource() == startButton) {
			timer.start();
			audio.playBackgroundMusic();
			run = true;

		} else if (e.getSource() == stopButton) {
			timer.stop();
			audio.stopBackgriundMusic();
			run = false;
		}

		else if (e.getSource() == endButton) {
			JOptionPane.showMessageDialog(null, "離開程式");
			System.exit(0);
			
		}
		if (run == true) {

			if (a == 1) {// 在9*9 p21上

				for (int i = 0; i < 9; i++) {
					for (int j = 0; j < 9; j++) {
						// 判斷有沒有炸彈
						if (e.getSource() == jbtp21[i][j]) {
							// 有炸彈 出現炸彈圖

							// 只要按到遊戲的Button就開始計時
							timer.start();

							if (boom1[i][j] == true) {
								audio.stopBackgriundMusic();
								audio.playExplosionMusic();
								jbtp21[i][j].setIcon(icon);
								// 宣告跳出一個畫面

								gameover Gameover = new gameover(c.gethour(),
										c.getmin(), c.getsecond());
								Gameover.setVisible(true);// 顯示 視窗!

								timer.stop();// 失敗遊戲 時間暫停
								run = false;// 遊戲結束 按鈕失效

							}

							// 沒有炸彈就計算附近的九宮格有多少個炸彈
							else {

								int boomnum = 0;

								int x1;// 判斷 x-1 有沒有在邊邊
								if (i - 1 < 0) {
									// 判斷 x-1 小於0 就設為0
									x1 = 0;
								} else {
									x1 = i - 1;
								}

								int x2;// 判斷 x+1 有沒有在邊邊
								if (i + 1 > 8) {
									// 判斷 x+1 大於8 就設為8
									x2 = 8;
								} else {
									x2 = i + 1;
								}

								int y1; // 判斷 y-1 有沒有在邊邊
								if (j - 1 < 0) {
									// 判斷 y-1 小於0 就設為0
									y1 = 0;
								} else {
									y1 = j - 1;
								}

								int y2;
								if (j + 1 > 8) {// 判斷 y+1 有沒有在邊邊
									// 判斷 y+1 大於8 就設為8
									y2 = 8;
								} else {
									y2 = j + 1;
								}

								for (int x = x1; x <= x2; x++) {
									for (int y = y1; y <= y2; y++) {
										if (boom1[x][y] == true) {
											boomnum++;
										}
									}
								}
								// 算空的 字串被點到幾次
								if (jbtp21[i][j].getText() == "") {
									textboom++;
								}
								// 顯示炸彈字數
								jbtp21[i][j].setFont(font2);
								jbtp21[i][j].setText(String.valueOf(boomnum));
								if (textboom == 71) {
									endJFrame endframe = new endJFrame(
											c.gethour(), c.getmin(),
											c.getsecond());
									endframe.setVisible(true);
									timer.stop();// 成功結束遊戲時間就暫停
									run = false;// 遊戲成功 再點遊戲中的按鈕就無效
								}
							}
						}
					}

				}
			}

			if (a == 2) {// 在18*18 p22上
				for (int i = 0; i < 18; i++) {
					for (int j = 0; j < 18; j++) {
						// 判斷有沒有炸彈
						if (e.getSource() == jbtp22[i][j]) {

							// 有炸彈 出現炸彈圖

							// 只要按到遊戲的Button就開始計時
							timer.start();

							if (boom2[i][j] == true) {
								audio.stopBackgriundMusic();
								audio.playExplosionMusic();// 呼叫音樂method
								jbtp22[i][j].setIcon(icon);

								gameover Gameover = new gameover(c.gethour(),
										c.getmin(), c.getsecond());
								Gameover.setVisible(true);// 顯示 視窗!

								run = false;
								timer.stop();// 失敗遊戲 時間暫停
								run = false;// 遊戲結束 按鈕失效
							}

							// 沒有炸彈就計算附近的九宮格有多少個炸彈
							else {

								int boomnum = 0;
								int x1;// 判斷 x-1 有沒有在邊邊
								if (i - 1 < 0) {
									// 判斷 x-1 小於0 就設為0
									x1 = 0;
								} else {
									x1 = i - 1;
								}

								int x2;// 判斷 x+1 有沒有在邊邊
								if (i + 1 > 17) {
									// 判斷 x+1 大於17 就設為17
									x2 = 17;
								} else {
									x2 = i + 1;
								}

								int y1; // 判斷 y-1 有沒有在邊邊
								if (j - 1 < 0) {
									// 判斷 y-1 小於0 就設為0
									y1 = 0;
								} else {
									y1 = j - 1;
								}

								int y2;
								if (j + 1 > 17) {// 判斷 y+1 有沒有在邊邊
									// 判斷 y+1 大於17 就設為17
									y2 = 17;
								} else {
									y2 = j + 1;
								}

								for (int x = x1; x <= x2; x++) {
									for (int y = y1; y <= y2; y++) {

										if (boom2[x][y] == true) {
											boomnum++;
										}
									}
								}

								if (jbtp22[i][j].getText() == "") {
									textboom++;
								}

								// 顯示炸彈字數
								jbtp22[i][j].setText(String.valueOf(boomnum));
								if (textboom == 259) {
									endJFrame endframe = new endJFrame(
											c.gethour(), c.getmin(),
											c.getsecond());
									endframe.setVisible(true);
									timer.stop();// 成功結束遊戲時間就暫停
									run = false;// 遊戲成功 再點遊戲中的按鈕就無效
								}
							}
						}
					}

				}
			}
		}

	}

	// 時間 class
	class TimerListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub

			screen.repaint();// 每一秒重畫一次
			c.setCurrentClock();//
			screen.setText(String.valueOf(c.gethour()) + ":"
					+ String.valueOf(c.getmin()) + ":"
					+ String.valueOf(c.getsecond()));
			// 把hour min second 轉為字串 再顯示在 screen上1
		}

	}

}
