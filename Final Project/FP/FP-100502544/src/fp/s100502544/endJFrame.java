package fp.s100502544;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class endJFrame extends JFrame{
	Font font1=new Font("Dialog",Font.BOLD,50);
	Font font2=new Font("Dialog",Font.BOLD,30);
	JPanel p1=new JPanel();
	JPanel p0=new JPanel();
	public ImageIcon icon=new ImageIcon("image/boom2.jpg");
	
	
	public endJFrame(int hour,int min,int second){
		
		setTitle("嘎勒給給成功篇");
		setSize(600,600);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		int h=hour;
		int m=min;
		int s=second;
		JLabel L1=new JLabel("             YOU WIN!!!");
		JLabel L4=new JLabel(icon);
		L1.setBackground(Color.BLACK);
		L1.setFont(font1);
		L1.setForeground(Color.RED);
		p0.setLayout(new GridLayout(2,1));
		p0.add(L1);
		p0.setBackground(Color.BLACK);
		p0.add(L4);
		add(p0,BorderLayout.NORTH);
		p1.setLayout(new GridLayout(1,2,5,5));
		p1.setBackground(Color.BLACK);
		JLabel L2=new JLabel("你所消耗的時間:");
		L2.setFont(font2);
		L2.setForeground(Color.YELLOW);
		JLabel L3=new JLabel(h+":"+m+":"+s);
		L3.setFont(font2);
		L3.setForeground(Color.CYAN);
		p1.add(L2);
		p1.add(L3);
		add(p1,BorderLayout.CENTER);
		
		
		
	}
}
