package f1.s100502010;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.Scanner;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.border.LineBorder;
	
public class game extends JFrame implements ActionListener
{
	Font font1=new Font("Segoe Script",Font.BOLD+Font.ITALIC,16);
	Font font2=new Font("Segoe Script",Font.BOLD+Font.ITALIC,30);
	private JLabel time=new JLabel("Time:");//p1
	private int timepass=0;
	Timer timer=new Timer(1000, new TimerListener());
	private JLabel scoresget=new JLabel("Scores: 0");
	private int scores=0;
	
	private JLabel picture=new JLabel();//p2
	private ImageIcon[] pic=new ImageIcon[6];
	private ImageIcon p0=new ImageIcon("image/ori.jpg");
	private ImageIcon p1=new ImageIcon("image/0.jpg");
	private ImageIcon p2=new ImageIcon("image/1.jpg");
	private ImageIcon p3=new ImageIcon("image/2.jpg");
	private ImageIcon p4=new ImageIcon("image/3.jpg");
	private ImageIcon p5=new ImageIcon("image/4.jpg");
	private ImageIcon pstart=new ImageIcon("image/start.jpg");
	private ImageIcon phigh=new ImageIcon("image/highscore.jpg");
	
	private JLabel tips=new JLabel("Words You Have Tried:");//p4
	private JLabel stage1=new JLabel("STAGE 1");
	private char[] wrongword=new char[20];
	private String try1="";
	private JLabel words=new JLabel(try1);
	
	private JLabel guess=new JLabel("Guess What's This Word");//p5
	private JLabel que=new JLabel("");
	private String[] dash=new String[20];
	private char[] sep=new char[20];
	
	private JLabel next=new JLabel("Next Guess:");//p7
	private JTextField nextguess=new JTextField();
	
	private JLabel personal=new JLabel("觀音x勃了");//p8
	private String[] voca=new String[100];
	java.io.File file=new java.io.File("wordbank.txt");
	private int wrongcount=0;
	private int choose;
	private int exit=0;
	private int numberofvoca;
	private int loose=0;
	private int nextstage=1;
	
	public game() throws FileNotFoundException
	{
		setSize(900,700);                                //set frame
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);		
		
		pic[0]=p0;
		pic[1]=p1;
		pic[2]=p2;
		pic[3]=p3;
		pic[4]=p4;
		pic[5]=p5;
		Scanner input=new Scanner(file);//read in vocabulary from txt
		int counter=0;
		while(input.hasNext())
		{
			voca[counter]=input.next();
			counter++;
		}
		numberofvoca=counter;
		choose=(int)(Math.random()*counter);
		int length=voca[choose].length();
		for(int counter1=0;counter1<length;counter1++)
		{
			dash[counter1]="_ ";
		}
		for(int counter1=0;counter1<length;counter1++)
		{
			sep[counter1]=voca[choose].charAt(counter1);
		}
		String use="";
		for(int counter1=0;counter1<voca[choose].length();counter1++)
		{
			use=use+dash[counter1]+" ";
		}
		que.setText(use);
		que.setFont(font2);
		JPanel p1=new JPanel();
		JPanel p2=new JPanel();
		JPanel p3=new JPanel();
		JPanel p4=new JPanel();
		JPanel p5=new JPanel();
		JPanel p6=new JPanel();
		JPanel p7=new JPanel();
		JPanel p8=new JPanel();
		JPanel p9=new JPanel();
		JPanel p10=new JPanel();
		JPanel p11=new JPanel();
		
		timer.start();
	
		p1.setLayout(new GridLayout(1,2,5,5));   //layout
		time.setFont(font1);
		scoresget.setFont(font1);
		p1.add(time);
		p1.add(scoresget);
		p1.setBackground(Color.GRAY);
		p1.setBorder(new LineBorder(Color.BLACK,2));
		
		p2.setBackground(Color.GRAY);
		picture.setIcon(pic[0]);
		p2.add(picture);
		p2.setBorder(new LineBorder(Color.BLACK,2));
		
		p3.setLayout(new BorderLayout());
		p3.add(p1,BorderLayout.NORTH);
		p3.add(p2,BorderLayout.CENTER);
		p3.setBorder(new LineBorder(Color.BLACK,2));
		
		p4.setLayout(new GridLayout(3,1,10,10));
		stage1.setFont(font1);
		tips.setFont(font1);
		words.setFont(font2);
		p4.add(stage1);
		p4.add(tips);
		p4.add(words);
		p4.setBackground(Color.GRAY);
		p4.setBorder(new LineBorder(Color.BLACK,2));
		
		p5.setLayout(new BorderLayout());
		guess.setFont(font2);
		que.setFont(font2);
		p5.add(guess,BorderLayout.NORTH);
		p5.add(que,BorderLayout.CENTER);
		p5.setBackground(Color.GRAY);
		p5.setBorder(new LineBorder(Color.BLACK,2));
		
		p6.setLayout(new GridLayout(2,1,0,0));
		p6.add(p4);
		p6.add(p5);
		
		p7.setLayout(new GridLayout(1,2,0,0));
		next.setFont(font1);
		nextguess.setBackground(Color.WHITE);
		p7.add(next);
		p7.add(nextguess);
		p7.setBackground(Color.GRAY);
		p7.setBorder(new LineBorder(Color.BLACK,2));
		
		p8.setBackground(Color.GRAY);
		p8.add(personal);
		p8.setBorder(new LineBorder(Color.BLACK,2));
		
		p9.setLayout(new GridLayout(1,2,0,0));
		p9.add(p7);
		p9.add(p8);
	
		p10.setLayout(new GridLayout(1,2,0,0));
		p10.add(p3);
		p10.add(p6);
		
		p11.setLayout(new BorderLayout());
		p11.add(p10,BorderLayout.CENTER);
		p11.add(p9,BorderLayout.SOUTH);
		add(p11);
		nextguess.addActionListener(this);
		
	}
	public void overyet()//check if this stage is done
	{
		if(exit==voca[choose].length())
		{
			choose=(int)(Math.random()*numberofvoca);
			timer.stop();
			JOptionPane.showMessageDialog(null,"You finish this stage in "+gettime()+" seconds"+"You got "+getscore()+" scores");
			setScore(getscore());
			if(nextstage>=5)
			{
				JOptionPane.showMessageDialog(null,"STAGE 1 clear!!");
				nextstage();
			}
			settime();
			nextword();
			timer.start();
			exit=0;
			nextstage++;
			repaint();
		}
	}
	public void nextstage()//go to stage2
	{
		setVisible(false);
		try {
			game2 g2=new game2(scores);
		}
		catch (FileNotFoundException e) 
		{		
			e.printStackTrace();
		}
	}
	public void nextword()//if a word is finished change into the next one
	{
		char blank=" ".charAt(0);
		for(int counter1=0;counter1<19;counter1++)
		{
			wrongword[counter1]=blank;
		}
		for(int counter1=0;counter1<20;counter1++)
		{
			dash[counter1]="";
		}
		for(int counter1=0;counter1<voca[choose].length();counter1++)
		{
			System.out.println("voca[choose].length()="+voca[choose].length());
			System.out.println("voca[choose]="+voca[choose]);
			dash[counter1]="_ ";
		}
		String use="";
		for(int counter1=0;counter1<voca[choose].length();counter1++)
		{
			use=use+dash[counter1]+" ";
		}
		for(int counter1=0;counter1<20;counter1++)
		{
			sep[counter1]=blank;
		}
		for(int counter1=0;counter1<voca[choose].length();counter1++)
		{
			sep[counter1]=voca[choose].charAt(counter1);
		}
		que.setText(use);
		try1="";
		words.setText(try1);
		loose=0;
		changepic();
		repaint();
	}
	public int getscore()//return score
	{
		int score=0;
		int timeused=gettime();
		score=Math.abs(timeused*10-1000);
		return score;
	}
	public void setScore(int get)//set score on jlabel
	{
		scores=scores+get;
		scoresget.setText("Scores: "+scores);
		repaint();
	}
	public int gettime()//return time
	{
		return timepass;
	}
	public void settime()
	{
		timepass=0;
	}
	public void changepic()//if your answer is wrong change picture
	{
		if(loose==0)
		{
			picture.setIcon(pic[0]);
			repaint();
		}
		if(loose==1)
		{
			picture.setIcon(pic[1]);
			repaint();
		}
		else if(loose==2)
		{
			picture.setIcon(pic[2]);
			repaint();
		}
		else if(loose==3)
		{
			picture.setIcon(pic[3]);
			repaint();	
		}
		else if(loose==4)
		{
			picture.setIcon(pic[4]);
			repaint();
		}
		else if(loose==5)
		{
			picture.setIcon(pic[5]);
			repaint();
		}
	}
	public void loosejudge()//check if you are lose or not
	{
		if(loose==5)
		{
			loose=0;
			timer.stop();
			JOptionPane.showMessageDialog(null,"You Lose");
			nextword();
			settime();
			scores=0;
			setScore(0);
			timer.start();
		}
	}
	public void changewords(int tempin)//change from dash to character
	{
		//tempin 表示第幾個底線要換乘英文單字
		String use="";
		dash[tempin-1]=nextguess.getText();
		for(int counter1=0;counter1<voca[choose].length();counter1++)
		{
			use=use+dash[counter1]+" ";
		}
		que.setText(use);
		exit++;
		repaint();
	}
	public int guess()
	{
		String temp=nextguess.getText();
		return(judge(temp));
	}
	public int judge(String tempin)//check if your answer is right or not
	{
		String temp=tempin;
		int which=-1;
		System.out.println("單字長度"+voca[choose].length());
		for(int counter1=0;counter1<voca[choose].length();counter1++)
		{
			System.out.println("這個字母是"+ Integer.valueOf(sep[counter1]) +" "+Integer.valueOf(temp.charAt(0)));
			if(Integer.valueOf(temp.charAt(0)) == Integer.valueOf(sep[counter1]))
			{
				which=counter1+1;
				System.out.println("which="+which+" counter1="+counter1);	
			}
		}
		return which;
	}
	class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)//clock
		{
			timepass++;
			time.setText("Time:"+timepass);
			repaint();
		}
	}
	public void actionPerformed(ActionEvent e)//大寫問題
	{
		if(e.getSource()==nextguess)//receive character
		{
			System.out.println("有收到字囉");
			int check=0;
			if(nextguess.getText().length()<=1)
			{
				if(guess()==-1)
				{
					System.out.println("錯字");
					for(int counter1=0;counter1<20;counter1++)
					{
						if(Integer.valueOf(wrongword[counter1])==Integer.valueOf(nextguess.getText().charAt(0)))
						{
							check++;
							counter1=30;
							System.out.println("check="+check);
						}
					}
					if(check==0)
					{
						wrongword[wrongcount]=nextguess.getText().charAt(0);
						System.out.println("錯字已存入array");
						wrongcount++;
						try1=try1+"  "+nextguess.getText();
						words.setText(try1);
						loose++;
						changepic();
						loosejudge();
					}
					else
					{
						JOptionPane.showMessageDialog(null,"You hva already try this character!!");
						System.out.println("you had try this before");
					}
					repaint();
				}
				else
				{
					System.out.println("猜對囉");
					System.out.println("which="+guess());
					changewords(guess());
					overyet();
				}
			}
			nextguess.setText("");
		}
	}
}
