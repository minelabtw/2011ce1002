package f1.s100502010;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.util.Scanner;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.border.LineBorder;

import f1.s100502010.game.TimerListener;

public class game2 extends JFrame 
{
	private JLabel time=new JLabel("Time:");//p1
	private JLabel timepass=new JLabel();
	private int timepassint=0;
	private JLabel stage2=new JLabel("STAGE 2");
	private JLabel scores=new JLabel("Scores:");
	private JLabel scoresget=new JLabel();
	private int scoresgetint=0;
	private ImageIcon back=new ImageIcon("image/back2.jpg");
	private JLabel pic=new JLabel();//p2
	
	private JLabel wordplace=new JLabel();//pru
	
	private JLabel typehere=new JLabel();//prd
	private String guess="";
	
	private JLabel randomword=new JLabel();//p3
	private String[] atoz=new String[26];
	java.io.File file=new java.io.File("wordbank.txt");
	
	private String[] voca=new String[100];
	private String[] dash=new String[100];
	private char[] sep=new char[100];
	private int choose=0;
	Timer timer=new Timer(1000, new TimerListener()); 
	private String randomallwords[]=new String[10];
	private int counter2=0;
	
	public int point=0;
	public int timeall=0;
	private String[] name=new String[6];
	private int[] scoresgetfinal=new int[6];
	private int nuberofvoca=0;
	private int finish=1;
	
	public game2(int score) throws FileNotFoundException
	{
		setSize(900,700);                                            //set frame
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		timer.start();
		scoresgetint=score;
		Scanner input=new Scanner(file);
		int counter=0;
		
		for(int counter1=0;counter1<26;counter1++)
		{
			atoz[counter1]=""+(char)(counter1+97);
		}
		while(input.hasNext())
		{
			voca[counter]=input.next();
			counter++;
		}
		nuberofvoca=counter;
		choose=(int)(Math.random()*nuberofvoca);
		int length=voca[choose].length();
		for(int counter1=0;counter1<length;counter1++)
		{
			dash[counter1]="_ ";
		}
		for(int counter1=0;counter1<length;counter1++)
		{
			sep[counter1]=voca[choose].charAt(counter1);
		}
		String use="";
		for(int counter1=0;counter1<voca[choose].length();counter1++)
		{
			use=use+dash[counter1]+" ";
		}
		intrandomarray();
		
		JPanel p1=new JPanel();
		JPanel p2=new JPanel();
		JPanel p3=new JPanel();
		JPanel p4=new JPanel();
		JPanel pru=new JPanel();
		JPanel prd=new JPanel();
		JPanel pr=new JPanel();
		JPanel pmid=new JPanel();
		Font fontb=new Font("Segoe Script",Font.BOLD+Font.ITALIC,30);
		Font fonts=new Font("Segoe Script",Font.BOLD+Font.ITALIC,16);
		
		scoresget.setText(""+scoresgetint);
		time.setFont(fonts);
		timepass.setFont(fonts);
		stage2.setFont(fontb);
		scores.setFont(fonts);
		scoresget.setFont(fonts);
		
		p1.setLayout(new GridLayout(1,5,5,5));                    //set layout
		p1.add(time);
		p1.add(timepass);
		p1.add(stage2);
		p1.add(scores);
		p1.add(scoresget);
		p1.setBorder(new LineBorder(Color.BLACK,2));
		
		p2.add(pic);
		p2.setBorder(new LineBorder(Color.BLACK,2));
		
		wordplace.setText(""+use);
		wordplace.setFont(fontb);
		pru.setLayout(new BorderLayout());
		pru.add(wordplace,BorderLayout.CENTER);
		pru.setBorder(new LineBorder(Color.BLACK,2));

		typehere.setFont(fontb);
		prd.add(typehere);
		prd.setBorder(new LineBorder(Color.BLACK,2));
		
		pr.setLayout(new GridLayout(2,1,0,0));
		pr.add(pru);
		pr.add(prd);
		
		pmid.setLayout(new GridLayout(1,2,0,0));
		pmid.add(p2);
		pmid.add(pr);
		
		randomword.setFont(fontb);//p3
		randomword.setText("");
		p3.add(randomword);
		p3.setBorder(new LineBorder(Color.BLACK,2));
		
		
		setLayout(new BorderLayout());
		add(p1,BorderLayout.NORTH);
		add(pmid,BorderLayout.CENTER);
		add(p3,BorderLayout.SOUTH);
		randomwords();
		addKeyListener(new KeyAdapter()
		{
			public void keyPressed(KeyEvent e)
			{
				switch(e.getKeyCode())
				{
					case KeyEvent.VK_A:
						guess=guess+"a";
						typehere.setText(guess);
						repaint();
						System.out.println("a pressed");
						break;
					case KeyEvent.VK_B:
						guess=guess+"b";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_C:
						guess=guess+"c";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_D:
						guess=guess+"d";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_E:
						guess=guess+"e";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_F:
						guess=guess+"f";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_G:
						guess=guess+"g";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_H:
						guess=guess+"h";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_I:
						guess=guess+"i";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_J:
						guess=guess+"j";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_K:
						guess=guess+"k";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_L:
						guess=guess+"l";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_M:
						guess=guess+"m";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_N:
						guess=guess+"n";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_O:
						guess=guess+"o";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_P:
						guess=guess+"p";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_Q:
						guess=guess+"q";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_R:
						guess=guess+"r";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_S:
						guess=guess+"s";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_T:
						guess=guess+"t";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_U:
						guess=guess+"u";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_V:
						guess=guess+"v";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_W:
						guess=guess+"w";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_X:
						guess=guess+"x";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_Y:
						guess=guess+"y";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_Z:
						guess=guess+"z";
						typehere.setText(guess);
						repaint();
						break;
					case KeyEvent.VK_ENTER:
					try {
						judge();
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					}
						break;
				}
			}
			
		});
	}
	public void nextword()//change from dash to character
	{
		choose=(int)(Math.random()*nuberofvoca);
		int length=voca[choose].length();
		for(int counter1=0;counter1<length;counter1++)
		{
			dash[counter1]="_ ";
		}
		for(int counter1=0;counter1<length;counter1++)
		{
			sep[counter1]=voca[choose].charAt(counter1);
		}
		String use="";
		for(int counter1=0;counter1<voca[choose].length();counter1++)
		{
			use=use+dash[counter1]+" ";
		}
		wordplace.setText(use);
	}
	public void judge() throws FileNotFoundException//check if your answer is right or not
	{
		System.out.println(guess+" "+voca[choose]);
		if(guess.equals(voca[choose]))
		{
			timer.stop();
			JOptionPane.showMessageDialog(null,"Your Answer is Right");
			JOptionPane.showMessageDialog(null,"You Got "+getscore()+" points");
			setscore(getscore());
			settime();
			guess="";
			typehere.setText(guess);
			timer.start();
			if(finish>=1)
			{
				JOptionPane.showMessageDialog(null,"ALL CLEAR");
				save();
				String namein=JOptionPane.showInputDialog("Enter Your Name:");
				savescores(namein);
			}
			finish++;
			nextword();
		}
		else
		{
			JOptionPane.showMessageDialog(null,"Your Answer is Wrong");
			guess="";
			typehere.setText(guess);
			repaint();
		}
	}
	public void settime()
	{
		timepassint=0;
		timepass.setText(""+timepassint);
		repaint();
	}
	public int getscore()//return score
	{
		int score=0;
		int timeused=timepassint;
		score=Math.abs(timeused*10-1000);
		return score;
	}
	public void setscore(int scorein)
	{
		scoresgetint=scoresgetint+scorein;
		scoresget.setText(""+scoresgetint);
		repaint();
	}
	public void intrandomarray()
	{
		for(int counter1=0;counter1<voca[choose].length();counter1++)
		{
			randomallwords[counter1]=""+sep[counter1];
			System.out.println(sep[counter1]);
		}
	}
	public void randomwords()
	{
		int random=10-voca[choose].length();
		System.out.println("voca[choose].length= "+voca[choose].length());
		System.out.println("random= "+random);
		
		for(int counter1=1;counter1<=random;counter1++)
		{
			int which=(int)(Math.random()*26);
			System.out.println("which= "+which);
			System.out.println("atoz[which]= "+atoz[which]);
			addtogether(atoz[which]);
		}
		daluan();
	}
	public void daluan()//mix up the character
	{
		String temp="";
		String[] tempo=new String[10];
		int count=0;
		/*for(int counter1=0;counter1<10;counter1++)
		{
			System.out.println("randomwords[counter1]= "+randomallwords[counter1]);
			temp=temp+randomallwords[counter1]+" ";
		}*/
		for(int counter1=1;counter1<=10;counter1++)
		{
			int tempnum=(int)(Math.random()*10);
			int check=1;
			for(int counter2=0;counter2<10;counter2++)
			{
				if(tempo[counter2]==randomallwords[tempnum])
				{
					check=0;
				}
			}
			if(check==1)
			{
				System.out.println(randomallwords[tempnum]);
				tempo[count]=randomallwords[tempnum];
				count++;
				temp=temp+randomallwords[tempnum]+" ";
			}
			else
			{
				counter1--;
			}
		}
		System.out.println("temp= "+temp);
		randomword.setText(temp);
		repaint();
	}
	public void addtogether(String atoz2)//add the character together
	{
		System.out.println("counter2= "+counter2);
		randomallwords[voca[choose].length()+counter2]=atoz2;
		System.out.println("randomallwords[voca[choose].length()+counter2]= "+randomallwords[voca[choose].length()+counter2]);
		counter2++;
	}
	class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)//clock
		{
			timepassint++;
			timepass.setText(""+timepassint);
			repaint();
		}
	}
	public void save() throws FileNotFoundException//save the grade
	{
		java.io.File file=new java.io.File("scores.txt");
		Scanner input=new Scanner(file);
		if(file.exists())
		{
			System.out.println("File exists");
		}
		int temp=0;
		while(input.hasNext())
		{
			name[temp]=input.next();
			scoresgetfinal[temp]=input.nextInt();
			System.out.println(name[temp]+"  "+scoresgetfinal[temp]);
			temp++;
		}
	}
	public void savescores(String namein) throws FileNotFoundException
	{
		System.out.println("your name is "+namein);
		for(int counter=0;counter<5;counter++)
		{
			if(scoresgetint>=scoresgetfinal[counter])
			{
				System.out.println("counter= "+counter);
				for(int counter3=4;counter3<counter;counter3--)
				{
					name[counter3]=name[counter3-1];
					scoresgetfinal[counter3]=scoresgetfinal[counter3-1];
					for(int temp=0;temp<5;temp++)
					{
						System.out.println(name[counter3]+"  "+scoresgetfinal[counter3]);
					}
				}
				name[counter]=namein;
				scoresgetfinal[counter]=scoresgetint;
				printtotxt();
				System.out.println("��"+counter+"�W");
				counter=6;
				setVisible(false);
				FrameWork f=new FrameWork();
				f.returnhere();
			}
		}
	}
	public void printtotxt() throws FileNotFoundException 
	{
		java.io.File file=new java.io.File("scores.txt");
		java.io.PrintWriter output=new java.io.PrintWriter(file);
		for(int counter=0;counter<5;counter++)
		{
			output.print(name[counter]+" ");
			output.println(scoresgetfinal[counter]);
		}
		output.close();
		System.out.println("Save Success");
	}
}
