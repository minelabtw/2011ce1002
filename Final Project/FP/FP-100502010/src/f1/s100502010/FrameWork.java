package f1.s100502010;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class FrameWork extends JFrame implements ActionListener
{
	private JButton start=new JButton("Start");
	private JButton score=new JButton("High Score");
	private JButton exit=new JButton("Exit");
	private JLabel pic=new JLabel();
	private ImageIcon picontop=new ImageIcon("image/toppic.jpg");
	private ImageIcon startpic=new ImageIcon("image/start.png");
	private ImageIcon exitpic=new ImageIcon("image/exit.png");
	private ImageIcon scorepic=new ImageIcon("image/score.png");
	private ImageIcon maintopic=new ImageIcon("image/topic.png");
	private JLabel topic=new JLabel();
	private JLabel back=new JLabel();
	
	public FrameWork()
	{
		JPanel p1=new JPanel();
		p1.setLayout(null);
		back.setLocation(-250,0);
		back.setSize(700,700);
		
		topic.setText(null);
		topic.setIcon(maintopic);
		topic.setLocation(40,50);
		topic.setSize(640,480);

		start.setText(null);
		start.setBorderPainted(false);
		start.setContentAreaFilled(false);
		start.setLocation(10,550);
		start.setSize(120,100);
		start.setIcon(startpic);
		
		exit.setIcon(exitpic);
		exit.setText(null);
		exit.setBorderPainted(false);
		exit.setContentAreaFilled(false);
		exit.setLocation(200,550);
		exit.setSize(150,100);
		exit.setIcon(exitpic);
		
		score.setIcon(exitpic);
		score.setText(null);
		score.setBorderPainted(false);
		score.setContentAreaFilled(false);
		score.setLocation(400,550);
		score.setSize(250,100);
		score.setIcon(scorepic);
		
		add(topic);
		add(start);
		add(exit);
		add(score);
		add(back);
		
		back.setIcon(picontop);
		start.addActionListener(this);
		score.addActionListener(this);
		exit.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource()==start)//button start action
		{
			this.setVisible(false);
			try 
			{
				game g=new game();
			} 
			catch (FileNotFoundException e1) 
			{
				e1.printStackTrace();
			}
		}
		else if(e.getSource()==score)//button score action
		{
			setVisible(false);
			try {
				highscore h=new highscore();
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
		}
		else if(e.getSource()==exit)//exit
		{
			System.exit(0);
		}
	}
	public void returnhere()
	{
		this.setVisible(true);
	}
}
