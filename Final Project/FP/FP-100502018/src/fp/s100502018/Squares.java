package fp.s100502018;

import java.awt.*;

abstract class Squares {
	private Graphics g;
	private int rotateFlag = 0;
	protected int[][] positions = new int[4][2]; //four blocks for a square
	protected int mx = 100, my = 0; //main x and main y
	protected Color squareColor;

	public Squares(Graphics g) {
		this.g = g;
		defaultRotate();
	}

	public void setRotateflag(int x) {
		this.rotateFlag = x;
	}

	public void setPlace(int x, int y) {
		mx = x;
		my = y;
		switch (rotateFlag) {
		case 0:
			defaultRotate();
			break;
		case 1:
			firstRotate();
			break;
		case 2:
			secondRotate();
			break;
		case 3:
			thirdRotate();
			break;
		}
	}

	public void drawShape(Graphics g) {
		g.setColor(Color.WHITE);
		for (int i = 0; i < 4; i++) {
			g.fillRect(positions[i][0], positions[i][1], 20, 20);
		}
		g.setColor(squareColor);
		for (int i = 0; i < 4; i++) {
			g.fillRect(positions[i][0], positions[i][1], 19, 19);
		}
	}

	public void cleanNext(Graphics g) { //clean the nextsquare
		g.setColor(Color.WHITE);
		g.fillRect(320, 120, 80, 80);
	}

	public void cleanHold(Graphics g) { //clean the holdsquare
		g.setColor(Color.WHITE);
		g.fillRect(220, 120, 80, 80);
	}

	public void rotate() { 
		rotateFlag++;
		if (rotateFlag == 4) {
			rotateFlag = 0;
		}
	}
	
	public void counterrotate() {
		rotateFlag--;
	}

	public int getLeft() {
		int temp = positions[0][0];
		for (int i = 1; i < 4; i++) {
			if (temp > positions[i][0]) {
				temp = positions[i][0];
			}
		}
		return temp;
	}

	public int getRight() {
		int temp = positions[0][0];
		for (int i = 1; i < 4; i++) {
			if (temp < positions[i][0]) {
				temp = positions[i][0];
			}
		}
		return temp + 20;
	}

	public int getBottom() {
		int temp = positions[0][1];
		for (int i = 1; i < 4; i++) {
			if (temp < positions[i][1]) {
				temp = positions[i][1];
			}
		}
		return temp + 20;
	}

	public int[] getFirstPlace() {
		int[] mini = { positions[0][0] / 20, positions[0][1] / 20 };
		return mini;
	}

	public int[] getSecondPlace() {
		int[] mini = { positions[1][0] / 20, positions[1][1] / 20 };
		return mini;
	}

	public int[] getThirdPlace() {
		int[] mini = { positions[2][0] / 20, positions[2][1] / 20 };
		return mini;
	}

	public int[] getFourthPlace() {
		int[] mini = { positions[3][0] / 20, positions[3][1] / 20 };
		return mini;
	}

	protected abstract void defaultRotate();

	protected abstract void firstRotate();

	protected abstract void secondRotate();

	protected abstract void thirdRotate();
}