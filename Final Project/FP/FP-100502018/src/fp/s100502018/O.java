package fp.s100502018;

import java.awt.*;

public class O extends Squares { // the O square and its basic position
	private Color OColor = new Color(219, 245, 65);

	public O(Graphics g) {
		super(g);
		this.squareColor = OColor;
	}

	protected void defaultRotate() {
		positions[0][0] = mx;
		positions[0][1] = my;
		positions[1][0] = mx + 20;
		positions[1][1] = my;
		positions[2][0] = mx;
		positions[2][1] = my + 20;
		positions[3][0] = mx + 20;
		positions[3][1] = my + 20;
	}

	protected void firstRotate() {
		this.defaultRotate();
	}

	protected void secondRotate() {
		this.defaultRotate();
	}

	protected void thirdRotate() {
		this.defaultRotate();
	}
}