package fp.s100502018;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class FinalProject extends JApplet {
	private int mx = 100, my = 0;
	private Squares curSquares;
	private Squares nextSquares;
	private Squares holdSquares;
	private Graphics bg;
	private Image wall;
	private Timer timer = new Timer(500, new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (next) {
				curSquares = nextSquares;
				mx = 100;
				my = 0;
				curSquares.setPlace(mx, my);

				selectSquares();
				next = false;
			} else if (curSquares.getBottom() == 400) {
				setFill();
				next = true;
			} else if (life == 0) {
				gaming = false;
				timer.stop();
				initial();
			} else {
				my += 20;
				curSquares.setPlace(mx, my);
				if (curSquares.getBottom() > 400 || isFilled()) {
					my -= 20;
					curSquares.setPlace(mx, my);
					if (filled[0][0] || filled[1][0] || filled[2][0]
							|| filled[3][0] || filled[4][0] || filled[5][0]
							|| filled[6][0] || filled[7][0] || filled[8][0]
							|| filled[9][0]
							|| curSquares.getFirstPlace()[1] < 0
							|| curSquares.getSecondPlace()[1] < 0
							|| curSquares.getThirdPlace()[1] < 0
							|| curSquares.getFourthPlace()[1] < 0) {
						timer.stop();
						initial();
						bg = wall.getGraphics();
						bg.fillRect(0, 0, 200, 400);
						mx = 100;
						my = 0;
						curSquares.setPlace(mx, my);
						life--;
						hold = false;
						next = false;
						timer.start();
					} else {
						setFill();
						next = true;
					}
				} else if (curSquares.getBottom() == 400) {
					setFill();
					next = true;
				}
			}
			repaint();
		}
	});
	private boolean gaming = false;
	private boolean[][] filled = new boolean[10][20];
	private boolean next = false;
	private boolean hold = false;
	private int score = 0;
	private int life = 3;
	private JLabel jlbPlayer = new JLabel("Score:");
	private JLabel jlbScore = new JLabel(String.valueOf(score));
	private JLabel jlbNextsquare = new JLabel("Next:");
	private JLabel jlbHold = new JLabel("Hold:");
	private JLabel jlbLiving = new JLabel("Life:");
	private JLabel jlbLife = new JLabel(String.valueOf(life));
	private JLabel jlbOver = new JLabel("GAME\nOVER");
	private Font showFont = new Font("Arial Black", Font.BOLD, 20);
	private Font overFont = new Font("TimesRoman", Font.BOLD, 35);
	private JButton jbtStart = new JButton("Start");
	private JButton jbtPause = new JButton("Pause");
	private JButton jbtExit = new JButton("Exit");

	public FinalProject() {
	}

	public void init() { // new the game
		this.initial();
		this.setLayout(null);
		wall = this.createImage(200, 400); // draw the background
		bg = wall.getGraphics();
		bg.fillRect(0, 0, 200, 400);
		switch ((int) (Math.random() * 1000 % 7)) { // random to new the squares
		case 0:
			curSquares = new I(bg);
			break;
		case 1:
			curSquares = new LL(bg);
			break;
		case 2:
			curSquares = new RL(bg);
			break;
		case 3:
			curSquares = new O(bg);
			break;
		case 4:
			curSquares = new S(bg);
			break;
		case 5:
			curSquares = new Z(bg);
			break;
		case 6:
			curSquares = new T(bg);
			break;
		}
		this.selectSquares();

		jlbPlayer.setBounds(220, 20, 80, 20);
		jlbPlayer.setForeground(Color.BLACK);
		jlbPlayer.setHorizontalAlignment(SwingConstants.CENTER);
		jlbScore.setBounds(220, 40, 80, 20);
		jlbScore.setForeground(Color.RED);
		jlbScore.setFont(showFont);
		jlbScore.setHorizontalAlignment(SwingConstants.CENTER);
		jlbLiving.setBounds(320, 20, 80, 20);
		jlbLiving.setForeground(Color.BLACK);
		jlbLiving.setHorizontalAlignment(SwingConstants.CENTER);
		jlbLife.setBounds(320, 40, 80, 20);
		jlbLife.setForeground(Color.RED);
		jlbLife.setFont(showFont);
		jlbLife.setHorizontalAlignment(SwingConstants.CENTER);
		jlbHold.setBounds(220, 80, 80, 20);
		jlbHold.setForeground(Color.BLACK);
		jlbHold.setHorizontalAlignment(SwingConstants.CENTER);
		jlbNextsquare.setBounds(320, 80, 80, 20);
		jlbNextsquare.setForeground(Color.BLACK);
		jlbNextsquare.setHorizontalAlignment(SwingConstants.CENTER);

		jbtStart.setBounds(240, 280, 140, 40);
		jbtStart.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == jbtStart) {
					gaming = true;
					timer.start();
					requestFocus(true);
				}
			}
		});
		jbtPause.setBounds(240, 320, 140, 40);
		jbtPause.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == jbtPause) {
					gaming = false;
					timer.stop();
				}
			}
		});
		jbtExit.setBounds(240, 360, 140, 40);
		jbtExit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == jbtExit) {
					System.exit(0);
				}

			}
		});
		this.add(jlbPlayer);
		this.add(jlbScore);
		this.add(jlbLiving);
		this.add(jlbLife);
		this.add(jlbHold);
		this.add(jlbNextsquare);
		this.add(jbtStart);
		this.add(jbtPause);
		this.add(jbtExit);
		this.addKeyListener(new KeyListener() {

			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyPressed(KeyEvent arg0) {
				if (gaming) {
					if (arg0.getKeyCode() == KeyEvent.VK_UP) { // rotate the
																// squares
						curSquares.rotate();
						curSquares.setPlace(mx, my);
						while (curSquares.getLeft() < 0) { // if touch the left
															// side
							mx += 20;
							curSquares.setPlace(mx, my);
						}
						while (curSquares.getRight() > 200) { // if touch the
																// right side
							mx -= 20;
							curSquares.setPlace(mx, my);
						}
						while (curSquares.getBottom() > 400) { // if touch the
																// bottom
							my -= 20;
							curSquares.setPlace(mx, my);
						}
						if (isFilled()) { // after rotate, if touch anything,
											// the squares will counterrotate
							curSquares.counterrotate();
							curSquares.setPlace(mx, my);
						}
						repaint();
					} else if (arg0.getKeyCode() == KeyEvent.VK_RIGHT) { // move
																			// the
																			// squares
																			// to
																			// right
						mx += 20;
						curSquares.setPlace(mx, my);
						if (curSquares.getRight() > 200 || isFilled()) {
							mx -= 20;
							curSquares.setPlace(mx, my);
						}
						repaint();
					} else if (arg0.getKeyCode() == KeyEvent.VK_LEFT) { // move
																		// the
																		// squares
																		// to
																		// left
						mx -= 20;
						curSquares.setPlace(mx, my);
						if (curSquares.getLeft() < 0 || isFilled()) {
							mx += 20;
							curSquares.setPlace(mx, my);
						}
						repaint();
					} else if (arg0.getKeyCode() == KeyEvent.VK_DOWN) { // move
																		// the
																		// squares
																		// to
																		// down
						my += 20;
						curSquares.setPlace(mx, my);
						if (curSquares.getBottom() > 400 || isFilled()) {
							my -= 20;
							curSquares.setPlace(mx, my);
							if (filled[0][0] || filled[1][0] || filled[2][0]
									|| filled[3][0] || filled[4][0]
									|| filled[5][0] || filled[6][0]
									|| filled[7][0] || filled[8][0]
									|| filled[9][0]
									|| curSquares.getFirstPlace()[1] < 0
									|| curSquares.getSecondPlace()[1] < 0
									|| curSquares.getThirdPlace()[1] < 0
									|| curSquares.getFourthPlace()[1] < 0) {
								timer.stop();
								initial();
								bg = wall.getGraphics();
								bg.fillRect(0, 0, 200, 400);
								mx = 100;
								my = 0;
								curSquares.setPlace(mx, my);
								life--;
								hold = false;
								next = false;
								timer.start();
							} else {
								setFill();
								next = true;
							}
						} else if (curSquares.getBottom() == 400) {
							setFill();
							next = true;
						}
						repaint();
					} else if (arg0.getKeyCode() == KeyEvent.VK_SPACE) { // move
																			// the
																			// squares
																			// to
																			// the
																			// bottom
																			// of
																			// the
																			// wall
						boolean touch = false;
						while (!touch) {
							if (!isFilled()) {
								my += 20;
								curSquares.setPlace(mx, my);
								if (isFilled()) {
									my -= 20;
									curSquares.setPlace(mx, my);
									if (filled[0][0]
											|| filled[1][0]
											|| filled[2][0]
											|| filled[3][0]
											|| filled[4][0]
											|| filled[5][0]
											|| filled[6][0]
											|| filled[7][0]
											|| filled[8][0]
											|| filled[9][0]
											|| curSquares.getFirstPlace()[1] < 0
											|| curSquares.getSecondPlace()[1] < 0
											|| curSquares.getThirdPlace()[1] < 0
											|| curSquares.getFourthPlace()[1] < 0) {
										timer.stop();
										initial();
										bg = wall.getGraphics();
										bg.fillRect(0, 0, 200, 400);
										mx = 100;
										my = 0;
										curSquares.setPlace(mx, my);
										life--;
										hold = false;
										next = false;
										timer.start();
									} else {
										touch = true;
										setFill();
										next = true;
									}
								} else if (curSquares.getBottom() - 20 == 380) {
									touch = true;
									setFill();
									next = true;
								}
							}
						}
					} else if (arg0.getKeyCode() == KeyEvent.VK_SHIFT) { // hold
																			// the
																			// squares
						if (!hold) {
							holdSquares = curSquares;
							holdSquares.setRotateflag(0);
							holdSquares.setPlace(240, 120);
							curSquares = nextSquares;
							selectSquares();
							hold = true;
						} else {
							Squares temp = holdSquares;
							holdSquares = curSquares;
							holdSquares.setRotateflag(0);
							curSquares = temp;
							holdSquares.setPlace(240, 120);
							curSquares.setPlace(100, 0);
						}
					}

				}
			}
		});
	}

	public void initial() { // make the filled[][] to false
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 20; j++) {
				filled[i][j] = false;
			}
		}
	}

	public void paint(Graphics g) { // draw the squares
		if (life != 0) {

			jlbPlayer.repaint();
			jlbScore.setText(String.valueOf(score));
			jlbScore.repaint();
			jlbLiving.repaint();
			jlbLife.setText(String.valueOf(life));
			jlbLife.repaint();
			jlbHold.repaint();
			jlbNextsquare.repaint();
			jbtStart.repaint();
			jbtPause.repaint();
			jbtExit.repaint();

			g.drawImage(wall, 0, 0, null);
			curSquares.drawShape(g);
			curSquares.cleanNext(g);
			nextSquares.drawShape(g);
			holdSquares.cleanHold(g);
			if (hold) {
				holdSquares.drawShape(g);
			}
		} else {
			jlbLife.setText(String.valueOf(life));
			jlbLife.repaint();
			g.drawImage(wall, 0, 0, null);
			jlbOver.setBounds(0, 0, 200, 400);
			jlbOver.setForeground(Color.RED);
			jlbOver.setFont(overFont);
			jlbOver.setHorizontalAlignment(SwingConstants.CENTER);
			this.add(jlbOver);
			jlbOver.paint(g);
		}
	}

	public int getTopRow() { // get the toprow to know how to eliminate
		int toprow = 19;
		for (int i = 19; i >= 0; i--) {
			int empty = 0;
			for (int j = 0; j < 10; j++) {
				if (!filled[j][i]) {
					empty++;
				}
			}
			if (empty == 10) {
				toprow = i + 1;
			}
		}
		return toprow;
	}

	public void setFill() { // make the square fixed
		gaming = false;
		int[] first = curSquares.getFirstPlace();
		int[] second = curSquares.getSecondPlace();
		int[] third = curSquares.getThirdPlace();
		int[] fourth = curSquares.getFourthPlace();
		filled[first[0]][first[1]] = true;
		filled[second[0]][second[1]] = true;
		filled[third[0]][third[1]] = true;
		filled[fourth[0]][fourth[1]] = true;
		curSquares.drawShape(bg);

		int toprow = getTopRow();
		for (int i = 19; i >= toprow; i--) {
			int blocks = 0;
			for (int j = 0; j < 10; j++) {
				if (filled[j][i])
					blocks++;
			}
			if (blocks == 10) {
				for (int k = i - 1; k >= toprow - 1; k--) {
					bg.copyArea(0, k * 20, 200, 20, 0, 20);
				}
				for (int l = i - 1; l >= toprow - 1; l--) {
					for (int m = 0; m < 10; m++) {
						filled[m][l + 1] = filled[m][l];
					}
				}
				score += 10;
				i++;
				toprow++;
			}
		}
		gaming = true;
	}

	public boolean isFilled() { // check the position whether fixed or not
		int[] first = curSquares.getFirstPlace();
		int[] second = curSquares.getSecondPlace();
		int[] third = curSquares.getThirdPlace();
		int[] fourth = curSquares.getFourthPlace();
		if (filled[first[0]][first[1]] || filled[second[0]][second[1]]
				|| filled[third[0]][third[1]] || filled[fourth[0]][fourth[1]]) {
			return true;
		} else {
			return false;
		}
	}

	public void selectSquares() { // random the nextsquare
		switch ((int) (Math.random() * 1000 % 7)) {
		case 0:
			nextSquares = new I(bg);
			break;
		case 1:
			nextSquares = new LL(bg);
			break;
		case 2:
			nextSquares = new RL(bg);
			break;
		case 3:
			nextSquares = new O(bg);
			break;
		case 4:
			nextSquares = new S(bg);
			break;
		case 5:
			nextSquares = new Z(bg);
			break;
		case 6:
			nextSquares = new T(bg);
			break;
		}
		nextSquares.setPlace(340, 120);
		next = false;
	}
}