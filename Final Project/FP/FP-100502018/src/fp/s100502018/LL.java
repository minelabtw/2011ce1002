package fp.s100502018;

import java.awt.*;

public class LL extends Squares { // the LL square and its basic position
	private Color LLColor = new Color(255, 155, 55);

	public LL(Graphics g) {
		super(g);
		this.squareColor = LLColor;
	}

	protected void defaultRotate() {
		positions[0][0] = mx;
		positions[0][1] = my;
		positions[1][0] = mx;
		positions[1][1] = my + 20;
		positions[2][0] = mx;
		positions[2][1] = my + 40;
		positions[3][0] = mx + 20;
		positions[3][1] = my + 40;
	}

	protected void firstRotate() {
		positions[0][0] = mx - 20;
		positions[0][1] = my;
		positions[1][0] = mx;
		positions[1][1] = my;
		positions[2][0] = mx + 20;
		positions[2][1] = my;
		positions[3][0] = mx - 20;
		positions[3][1] = my + 20;
	}

	protected void secondRotate() {
		positions[0][0] = mx;
		positions[0][1] = my;
		positions[1][0] = mx;
		positions[1][1] = my + 20;
		positions[2][0] = mx;
		positions[2][1] = my + 40;
		positions[3][0] = mx - 20;
		positions[3][1] = my;
	}

	protected void thirdRotate() {
		positions[0][0] = mx + 20;
		positions[0][1] = my;
		positions[1][0] = mx - 20;
		positions[1][1] = my + 20;
		positions[2][0] = mx;
		positions[2][1] = my + 20;
		positions[3][0] = mx + 20;
		positions[3][1] = my + 20;
	}
}