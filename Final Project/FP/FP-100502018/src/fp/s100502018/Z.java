package fp.s100502018;

import java.awt.*;

public class Z extends Squares { // the Z square and its basic position
	private Color ZColor = new Color(67, 243, 71);

	public Z(Graphics g) {
		super(g);
		this.squareColor = ZColor;
	}

	protected void defaultRotate() {
		positions[0][0] = mx;
		positions[0][1] = my;
		positions[1][0] = mx - 20;
		positions[1][1] = my;
		positions[2][0] = mx;
		positions[2][1] = my + 20;
		positions[3][0] = mx + 20;
		positions[3][1] = my + 20;
	}

	protected void firstRotate() {
		positions[0][0] = mx + 20;
		positions[0][1] = my;
		positions[1][0] = mx + 20;
		positions[1][1] = my + 20;
		positions[2][0] = mx;
		positions[2][1] = my + 20;
		positions[3][0] = mx;
		positions[3][1] = my + 40;
	}

	protected void secondRotate() {
		this.defaultRotate();
	}

	protected void thirdRotate() {
		this.firstRotate();
	}
}