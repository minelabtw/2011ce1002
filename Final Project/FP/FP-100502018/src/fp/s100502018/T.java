package fp.s100502018;

import java.awt.*;

public class T extends Squares { // the T square and its basic position
	private Color TColor = new Color(244, 62, 249);

	public T(Graphics g) {
		super(g);
		this.squareColor = TColor;
	}

	protected void defaultRotate() {
		positions[0][0] = mx;
		positions[0][1] = my;
		positions[1][0] = mx - 20;
		positions[1][1] = my + 20;
		positions[2][0] = mx;
		positions[2][1] = my + 20;
		positions[3][0] = mx + 20;
		positions[3][1] = my + 20;
	}

	protected void firstRotate() {
		positions[0][0] = mx;
		positions[0][1] = my;
		positions[1][0] = mx;
		positions[1][1] = my + 20;
		positions[2][0] = mx;
		positions[2][1] = my + 40;
		positions[3][0] = mx + 20;
		positions[3][1] = my + 20;
	}

	protected void secondRotate() {
		positions[0][0] = mx - 20;
		positions[0][1] = my;
		positions[1][0] = mx;
		positions[1][1] = my;
		positions[2][0] = mx + 20;
		positions[2][1] = my;
		positions[3][0] = mx;
		positions[3][1] = my + 20;
	}

	protected void thirdRotate() {
		positions[0][0] = mx;
		positions[0][1] = my;
		positions[1][0] = mx;
		positions[1][1] = my + 20;
		positions[2][0] = mx;
		positions[2][1] = my + 40;
		positions[3][0] = mx - 20;
		positions[3][1] = my + 20;
	}
}