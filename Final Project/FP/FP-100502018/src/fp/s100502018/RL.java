package fp.s100502018;

import java.awt.*;

public class RL extends Squares { // the RL square and its basic position
	private Color RLColor = new Color(96, 70, 240);

	public RL(Graphics g) {
		super(g);
		this.squareColor = RLColor;
	}

	protected void defaultRotate() {
		positions[0][0] = mx;
		positions[0][1] = my;
		positions[1][0] = mx;
		positions[1][1] = my + 20;
		positions[2][0] = mx;
		positions[2][1] = my + 40;
		positions[3][0] = mx - 20;
		positions[3][1] = my + 40;
	}

	protected void firstRotate() {
		positions[0][0] = mx - 20;
		positions[0][1] = my;
		positions[1][0] = mx - 20;
		positions[1][1] = my + 20;
		positions[2][0] = mx;
		positions[2][1] = my + 20;
		positions[3][0] = mx + 20;
		positions[3][1] = my + 20;
	}

	protected void secondRotate() {
		positions[0][0] = mx;
		positions[0][1] = my;
		positions[1][0] = mx;
		positions[1][1] = my + 20;
		positions[2][0] = mx;
		positions[2][1] = my + 40;
		positions[3][0] = mx + 20;
		positions[3][1] = my;
	}

	protected void thirdRotate() {
		positions[0][0] = mx;
		positions[0][1] = my;
		positions[1][0] = mx - 20;
		positions[1][1] = my;
		positions[2][0] = mx + 20;
		positions[2][1] = my;
		positions[3][0] = mx + 20;
		positions[3][1] = my + 20;
	}
}