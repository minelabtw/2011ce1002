package fp.s100502018;

import java.awt.*;

public class I extends Squares { // the I square and its basic position
	private Color IColor = new Color(137, 243, 252);

	public I(Graphics g) {
		super(g);
		this.squareColor = IColor;
	}

	protected void defaultRotate() {
		positions[0][0] = mx;
		positions[0][1] = my;
		positions[1][0] = mx;
		positions[1][1] = my + 20;
		positions[2][0] = mx;
		positions[2][1] = my + 40;
		positions[3][0] = mx;
		positions[3][1] = my + 60;
	}

	protected void firstRotate() {
		positions[0][0] = mx;
		positions[0][1] = my;
		positions[1][0] = mx + 20;
		positions[1][1] = my;
		positions[2][0] = mx + 40;
		positions[2][1] = my;
		positions[3][0] = mx + 60;
		positions[3][1] = my;
	}

	protected void secondRotate() {
		this.defaultRotate();
	}

	protected void thirdRotate() {
		this.firstRotate();
	}
}