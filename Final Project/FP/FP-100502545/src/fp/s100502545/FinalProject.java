package fp.s100502545;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class FinalProject extends JFrame implements ActionListener {// 宣告
	static JFrame i = new FinalProject();
	private JPanel p1 = new JPanel();
	private JLabel GameTitle = new JLabel("超級簡易打地鼠游戲");
	private JLabel pic = new JLabel("");
	private JButton Enter = new JButton("進入遊戲");
	private Font f1 = new Font("Serif", Font.BOLD, 72);
	private Font f2 = new Font("Serif", Font.BOLD, 20);
	private ImageIcon f = new ImageIcon("image/g.jpg");
	JFrame Exp = new JFrame();
	GameExplain gp = new GameExplain();

	public FinalProject() {// 設定字型.面板
		p1.setLayout(new GridLayout(1, 1));
		GameTitle.setFont(f1);
		Enter.setFont(f2);
		pic.setIcon(f);
		add(pic);
		add(Enter, BorderLayout.SOUTH);
		// 呼叫下一個視窗
		Exp.add(gp);
		Exp.setTitle("遊戲說明");
		Exp.pack();
		Exp.setSize(300, 350);
		Exp.setLocationRelativeTo(null);
		Exp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Enter.addActionListener(this);
	}

	public static void main(String[] args) {// 初始進入打地鼠遊戲畫面設定
		i.setTitle("超級簡易打地鼠游戲");
		i.setSize(300, 350);
		i.setLocationRelativeTo(null);
		i.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		i.setVisible(true);

	}

	public void actionPerformed(ActionEvent e) {// 按鈕處理,進入遊戲說明畫面
		if (e.getSource() == Enter) {
			Exp.setVisible(true);
			i.setVisible(false);
		}

	}

}
