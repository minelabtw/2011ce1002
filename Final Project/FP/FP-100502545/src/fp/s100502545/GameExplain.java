package fp.s100502545;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class GameExplain extends JPanel implements ActionListener {
	// 宣告panel,label,button,imageIcon
	// 物件使用
	private JPanel jp1 = new JPanel();
	private JPanel jp2 = new JPanel();
	private JPanel jp3 = new JPanel();
	private JLabel Explain = new JLabel("遊戲說明");
	private JLabel jlb1 = new JLabel("打中");
	private JLabel jlb2 = new JLabel("");
	private JLabel jlb3 = new JLabel(" 10分");
	private JLabel jlb4 = new JLabel("打中");
	private JLabel jlb5 = new JLabel("");
	private JLabel jlb6 = new JLabel(" 5分");
	private JLabel jlb7 = new JLabel("打中");
	private JLabel jlb8 = new JLabel("");
	private JLabel jlb9 = new JLabel(" -5分");
	private JLabel jlb10 = new JLabel("打中");
	private JLabel jlb11 = new JLabel("");
	private JLabel jlb12 = new JLabel(" -100分");

	private ImageIcon mouse = new ImageIcon("image/0.jpg");
	private ImageIcon frog = new ImageIcon("image/1.jpg");
	private ImageIcon monster = new ImageIcon("image/2.jpg");
	private ImageIcon bad = new ImageIcon("image/3.jpg");
	private ImageIcon B = new ImageIcon("image/h.jpg");
	private Font f1 = new Font("Serif", Font.BOLD, 20);
	private Font f2 = new Font("Serif", Font.BOLD, 16);
	private JLabel time = new JLabel("遊戲時間為60秒");
	private JButton play = new JButton("Ready Go~");

	JFrame Act = new JFrame();
	GameAction ga = new GameAction();

	public GameExplain() {// 設置增加面板功能

		B = new ImageIcon(B.getImage().getScaledInstance(300, 350,
				Image.SCALE_SMOOTH));
		jp1.add(Explain, BorderLayout.CENTER);
		jp1.setOpaque(false);
		jp2.setLayout(new GridLayout(4, 3));
		jp2.add(jlb1);
		//jp2.setBackground(Color.GREEN);
		jlb2.setIcon(mouse);
		jp2.add(jlb2);
		jp2.add(jlb3);
		jp2.setOpaque(false);

		jp2.add(jlb4);
		jlb5.setIcon(frog);
		jp2.add(jlb5);
		jp2.add(jlb6);

		jp2.add(jlb7);
		jlb8.setIcon(monster);
		jp2.add(jlb8);
		jp2.add(jlb9);

		jp2.add(jlb10);
		jlb11.setIcon(bad);
		jp2.add(jlb11);
		jp2.add(jlb12);

		jp3.setLayout(new GridLayout(1, 2));
		jp3.add(time, BorderLayout.WEST);
		jp3.add(play, BorderLayout.EAST);
		jp3.setOpaque(false);
		
		Explain.setFont(f1);
		jlb1.setFont(f2);
		jlb3.setFont(f2);
		jlb4.setFont(f2);
		jlb6.setFont(f2);
		jlb7.setFont(f2);
		jlb9.setFont(f2);
		jlb10.setFont(f2);
		jlb12.setFont(f2);
		time.setFont(f2);
		play.setFont(f2);
		add(jp1);
		add(jp2);
		add(jp3);

		// 跳到遊戲視窗
		Act.add(ga);
		Act.setTitle("超級簡易打地鼠游戲");
		Act.pack();
		Act.setSize(300, 350);
		Act.setLocationRelativeTo(null);
		Act.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		play.addActionListener(this);
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(B.getImage(), 0, 0, null);
	}

	public void actionPerformed(ActionEvent e) {// 按鈕處理,開始游戲
		if (e.getSource() == play) {
			Act.setVisible(true);
		}

	}

}
