package fp.s100502545;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class GameAction extends JPanel implements ActionListener {// 宣告Panel,Button,ImageIcon
	private JPanel jp1 = new JPanel();
	private JPanel jp2 = new JPanel();
	private JPanel jp3 = new JPanel();

	private JLabel jlb1 = new JLabel("所剩時間");
	private JLabel jlbTime = new JLabel("1:00");
	private JLabel jlb2 = new JLabel("分數");
	private JLabel jlbScore = new JLabel("0");

	private JButton jb1 = new JButton();
	private JButton jb2 = new JButton();
	private JButton jb3 = new JButton();
	private JButton jb4 = new JButton();
	private JButton jb5 = new JButton();
	private JButton jb6 = new JButton();
	private JButton jb7 = new JButton();
	private JButton jb8 = new JButton();
	private JButton jb9 = new JButton();

	private JButton jbt_Start = new JButton("開始");
	private JButton jbt_Stop = new JButton("暫停");
	private JButton jbt_reset = new JButton("重來");
	private JButton jbt_Exit = new JButton("離開");

	private ImageIcon mouse = new ImageIcon("image/0.jpg");
	private ImageIcon bad = new ImageIcon("image/1.jpg");
	private ImageIcon frog = new ImageIcon("image/2.jpg");
	private ImageIcon monster = new ImageIcon("image/3.jpg");
	private ImageIcon B = new ImageIcon("image/E.jpg");

	private Font f2 = new Font("Serif", Font.BOLD, 16);
	private Border lineborder = new LineBorder(Color.black, 2);

	public SetGame t = new SetGame();
	public int rand1;
	public int rand2;
	public int rand3;
	public int rand4;
	public int rand5;
	public int rand6;
	public int rand7;
	public int rand8;
	public int rand9;

	private Clock p1 = new Clock();
	Timer timer = new Timer(1000, new TimerListener());
	JFrame Show = new JFrame();
	ShowScore s = new ShowScore();

	int total = 0;

	public GameAction() {// 增加遊戲動作面板

		add(jp1, BorderLayout.NORTH);
		jp1.setLayout(new GridLayout(1, 4));
		jp1.add(jlb1);
		jp1.add(jlbTime);
		jp1.add(jlb2);
		jp1.add(jlbScore);
		jp1.setOpaque(false);
		B = new ImageIcon(B.getImage().getScaledInstance(300, 350,
				Image.SCALE_SMOOTH));
		jlb1.setFont(f2);
		jlbTime.setFont(f2);
		jlb2.setFont(f2);
		jlbScore.setFont(f2);

		add(jp2, BorderLayout.CENTER);
		jp2.setLayout(new GridLayout(3, 3));
		jp2.setBorder(lineborder);
		jb1 = new JButton(bad);
		jp2.add(jb1);
		jp2.add(jb2);
		jp2.add(jb3);
		jp2.add(jb4);
		jp2.add(jb5);
		jp2.add(jb6);
		jp2.add(jb7);
		jp2.add(jb8);
		jp2.add(jb9);

		add(jp3, BorderLayout.SOUTH);
		jp3.setLayout(new GridLayout(1, 4));
		jp3.add(jbt_Start);
		jp3.add(jbt_Stop);
		jp3.add(jbt_reset);
		jp3.add(jbt_Exit);

		jbt_reset.addActionListener(this);
		jbt_Start.addActionListener(this);
		jbt_Stop.addActionListener(this);
		jbt_Exit.addActionListener(this);

		jb1.addActionListener(this);
		jb2.addActionListener(this);
		jb3.addActionListener(this);
		jb4.addActionListener(this);
		jb5.addActionListener(this);
		jb6.addActionListener(this);
		jb7.addActionListener(this);
		jb8.addActionListener(this);
		jb9.addActionListener(this);

	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(B.getImage(), 0, 0, null);
	}

	public void actionPerformed(ActionEvent e) {// 按鈕處理,開始,暫停,總分,離開,
												// 點集9個button圖片的得分情形

		if (e.getSource() == jbt_Start) {
			timer.start();
			t.setRadom();
		} else if (e.getSource() == jbt_Stop) {

			timer.stop();
		} else if (e.getSource() == jbt_reset) {
			timer.restart();
			timer.stop();
			p1.clearClock();
			jlbScore.setText("0");
			jlbTime.setText("1:00");

		} else if (e.getSource() == jbt_Exit) {
			System.exit(0);
		}

		else if (e.getSource() == jb1) {

			if (p1.getSecond() > 0)
				if (rand1 == 0) {
					total += 10;

				} else if (rand1 == 1) {
					total += 5;

				} else if (rand1 == 2) {
					total -= 5;

				} else if (rand1 == 3) {
					total -= 100;

				}
			jlbScore.setText(Integer.toString(total));
		}

		else if (e.getSource() == jb2) {
			if (p1.getSecond() > 0)
				if (rand2 == 0) {
					total += 10;
				} else if (rand2 == 1) {
					total += 5;
				} else if (rand2 == 2) {
					total -= 5;
				} else if (rand2 == 3) {
					total -= 100;
				}
			jlbScore.setText(Integer.toString(total));
		}

		else if (e.getSource() == jb3) {
			if (p1.getSecond() > 0)
				if (rand3 == 0) {
					total += 10;
				} else if (rand3 == 1) {
					total += 5;
				} else if (rand3 == 2) {
					total -= 5;
				} else if (rand3 == 3) {
					total -= 100;
				}
			jlbScore.setText(Integer.toString(total));
		} else if (e.getSource() == jb4) {
			if (p1.getSecond() > 0)
				if (rand4 == 0) {
					total += 10;
				} else if (rand4 == 1) {
					total += 5;
				} else if (rand4 == 2) {
					total -= 5;
				} else if (rand4 == 3) {
					total -= 100;
				}

			jlbScore.setText(Integer.toString(total));
		} else if (e.getSource() == jb5) {
			if (p1.getSecond() > 0)
				if (rand5 == 0) {
					total += 10;
				} else if (rand5 == 1) {
					total += 5;
				} else if (rand5 == 2) {
					total -= 5;
				} else if (rand5 == 3) {
					total -= 100;
				}

			jlbScore.setText(Integer.toString(total));
		} else if (e.getSource() == jb6) {
			if (p1.getSecond() > 0)
				if (rand6 == 0) {
					total += 10;
				} else if (rand6 == 1) {
					total += 5;
				} else if (rand6 == 2) {
					total -= 5;
				} else if (rand6 == 3) {
					total -= 100;
				}

			jlbScore.setText(Integer.toString(total));
		} else if (e.getSource() == jb7) {
			if (p1.getSecond() > 0)
				if (rand7 == 0) {
					total += 10;
				} else if (rand7 == 1) {
					total += 5;
				} else if (rand7 == 2) {
					total -= 5;
				} else if (rand7 == 3) {
					total -= 100;
				}
			jlbScore.setText(Integer.toString(total));
		} else if (e.getSource() == jb8) {
			if (p1.getSecond() > 0)
				if (rand8 == 0) {
					total += 10;
				} else if (rand8 == 1) {
					total += 5;
				} else if (rand8 == 2) {
					total -= 5;
				} else if (rand8 == 3) {
					total -= 100;
				}

			jlbScore.setText(Integer.toString(total));
		}

		else if (e.getSource() == jb9) {
			if (p1.getSecond() > 0)
				if (rand9 == 0) {
					total += 10;
				} else if (rand9 == 1) {
					total += 5;
				} else if (rand9 == 2) {
					total -= 5;
				} else if (rand9 == 3) {
					total -= 100;
				}

			jlbScore.setText(Integer.toString(total));
		}
	}

	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {// 倒數計時設定,圖片隨機出現
			p1.setCurrentClock();
			jlbTime.setText(p1.getMinute() + ":" + p1.getSecond());
			if (p1.getMinute() == 0 && p1.getSecond() == 0) {
				timer.stop();

			}

			t.setRadom();
			rand1 = t.Number1[0];
			rand2 = t.Number1[1];
			rand3 = t.Number1[2];
			rand4 = t.Number1[3];
			rand5 = t.Number1[4];
			rand6 = t.Number1[5];
			rand7 = t.Number1[6];
			rand8 = t.Number1[7];
			rand9 = t.Number1[8];

			ImageIcon pic1 = new ImageIcon("image/" + rand1 + ".jpg");
			ImageIcon pic2 = new ImageIcon("image/" + rand2 + ".jpg");
			ImageIcon pic3 = new ImageIcon("image/" + rand3 + ".jpg");
			ImageIcon pic4 = new ImageIcon("image/" + rand4 + ".jpg");
			ImageIcon pic5 = new ImageIcon("image/" + rand5 + ".jpg");
			ImageIcon pic6 = new ImageIcon("image/" + rand6 + ".jpg");
			ImageIcon pic7 = new ImageIcon("image/" + rand7 + ".jpg");
			ImageIcon pic8 = new ImageIcon("image/" + rand8 + ".jpg");
			ImageIcon pic9 = new ImageIcon("image/" + rand9 + ".jpg");

			jb1.setIcon(pic1);
			jb2.setIcon(pic2);
			jb3.setIcon(pic3);
			jb4.setIcon(pic4);
			jb5.setIcon(pic5);
			jb6.setIcon(pic6);
			jb7.setIcon(pic7);
			jb8.setIcon(pic8);
			jb9.setIcon(pic9);
		}
	}
}
