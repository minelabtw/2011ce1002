package fp.s100502545;

import javax.swing.JPanel;

public class Clock extends JPanel {
	// 宣告minute從1開始,second 從0;
	protected int second = 0;
	protected int minute = 1;

	public Clock() {

	}

	public void clearClock() {
		second = 0;
		minute = 1;

	}

	public void setCurrentClock() {// 時間設置倒數60秒

		second--;
		if (second == -1) {
			minute--;
			second = 59;

		}

	}

	public int getSecond() {
		return second;
	}

	public int getMinute() {
		return minute;
	}
}
