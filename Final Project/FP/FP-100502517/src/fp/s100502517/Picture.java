package fp.s100502517;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Picture extends JFrame{
	private ImageIcon p1 = new ImageIcon("image/1.jpg");//太極
	private ImageIcon p2 = new ImageIcon("image/3.jpg");//正妹
	private ImageIcon p3 = new ImageIcon("image/7.jpg");//監獄兔	
	private ImageIcon p4 = new ImageIcon("image/8.jpg");//老鼠
	private ImageIcon p5 = new ImageIcon("image/20.jpg");//老師
	
	private ImageIcon p6 = new ImageIcon("image/4.jpg");//醜女
	private ImageIcon p7 = new ImageIcon("image/5.jpg");//笨
	private ImageIcon p8 = new ImageIcon("image/15.jpg");//波波
	private ImageIcon p9 = new ImageIcon("image/16.jpg");//小胖
	private ImageIcon p10 = new ImageIcon("image/17.jpg");//小安
	
	//private boolean result = true;//true is success..false is fail
	
	public Picture(boolean a){
		
		Font font = new Font("SansSerif", Font.BOLD+Font.ITALIC, 45);
		if(a){
			int k = (int)(1+3*Math.random());
			switch(k){
				case 1:
					JLabel word = new JLabel("~  ~  Good Job  ~  ~");//正妹
					JLabel L1 = new JLabel(p2);
					JPanel p = new JPanel();
					word.setFont(font);
					p.setLayout(new BorderLayout());
					p.add(word, BorderLayout.NORTH);
					p.add(L1, BorderLayout.CENTER);
					
					add(p);
					pack();
					break;
				case 2:
					JLabel word2 = new JLabel("~  ~  Well Done   ~  ~");//老鼠
					JLabel L2 = new JLabel(p4);
					JPanel pp = new JPanel();
					word2.setFont(font);
					pp.setLayout(new BorderLayout());
					pp.add(word2, BorderLayout.NORTH);
					pp.add(L2, BorderLayout.CENTER);
					
					add(pp);
					pack();
					break;
				case 3:
					JLabel word3 = new JLabel("  ~   ~  有 練 過 喔   ~   ~ ");//施老師
					JLabel L3 = new JLabel(p5);
					JPanel ppp = new JPanel();
					word3.setFont(font);
					ppp.setLayout(new BorderLayout());
					ppp.add(word3, BorderLayout.NORTH);
					ppp.add(L3, BorderLayout.CENTER);
					
					add(ppp);
					pack();
			}
		}
		
		else{
			int k = (int)(1+3*Math.random());
			switch(k){
				case 1:
					JLabel word = new JLabel("~  ~  ~ 你  錐  錐 耶~  ~  ~");//羅雨晴
					JLabel L1 = new JLabel(p8);
					JPanel p = new JPanel();
					word.setFont(font);
					p.setLayout(new BorderLayout());
					p.add(word, BorderLayout.NORTH);
					p.add(L1, BorderLayout.CENTER);
					
					add(p);
					pack();
					break;
				case 2:
					JLabel word2 = new JLabel("~  ~  連 這 都 不 會   ~  ~");//小胖
					JLabel L2 = new JLabel(p9);
					JPanel pp = new JPanel();
					word2.setFont(font);
					pp.setLayout(new BorderLayout());
					pp.add(word2, BorderLayout.NORTH);
					pp.add(L2, BorderLayout.CENTER);
					
					add(pp);
					pack();
					break;
				case 3:
					JLabel word3 = new JLabel("~  ~  這 題 只 有 帥 哥 會   ~  ~");//小安
					JLabel L3 = new JLabel(p10);
					JPanel ppp = new JPanel();
					word3.setFont(font);
					ppp.setLayout(new BorderLayout());
					ppp.add(word3, BorderLayout.NORTH);
					ppp.add(L3, BorderLayout.CENTER);
					
					add(ppp);
					pack();
					break;
			}
			
		}
	}
}
	
	
