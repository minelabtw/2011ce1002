package fp.s100502517;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Level extends JFrame{
	
	private JButton basic = new JButton(" Basic ");
	private JButton medium = new JButton(" Medium ");
	private JButton high = new JButton(" High ");
	private Font font = new Font("SansSerif", Font.BOLD+Font.ITALIC, 12);
	private ImageIcon con= new ImageIcon("image/10.jpg");
	private FrameWork frame;
	
	private int LV = 1;	
	
	public Level(){
		setTitle("Final Project");
		setSize(300,100);			
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		basic.setFont(font);
		medium.setFont(font);
		high.setFont(font);
		
		//設置背景圖片
		JPanel Board = new JPanel(){
		
			protected void paintComponent(Graphics g){
				g.drawImage(con.getImage(), 0, 0, getWidth(), getHeight(), this);
				super.paintComponent(g);
			}
		};
		Board.setBackground(new Color(0, 0, 0, 0));//設置原本的背景透明化
		Board.setLayout(new FlowLayout(20,15,15));
		Board.add(basic);
		Board.add(medium);
		Board.add(high);
		
		setLayout(new BorderLayout());
		add(Board, BorderLayout.CENTER);
		
		basic.addActionListener(new ButtonListener());
		medium.addActionListener(new ButtonListener());
		high.addActionListener(new ButtonListener());
	}
	
	class ButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			
			//設置  初級  中級   高級  點下去時的反應
			if(e.getSource() == basic){
				setVisible(false);	
				frame = new FrameWork();
				frame.setCup(1);
				frame.setVisible(true);
				
			}
			
			if(e.getSource() == medium){
				setVisible(false);	
				frame = new FrameWork();
				frame.setCup(2);
				frame.setVisible(true);
				
			}
			
			if(e.getSource() == high){
				setVisible(false);	
				frame = new FrameWork();
				frame.setCup(3);
				frame.setVisible(true);
			}
		}
	}

}
