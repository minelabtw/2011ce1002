package fp.s100502517;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class FrameWork extends JFrame{
	
	private Picture p;
	private Cup cup = new Cup();
	private Menu m;
	private Level LV;
	private boolean click = false;
	
	private Font font = new Font("Monospaced", Font.BOLD, 13);
	private Button cup1 = new Button("  C u p  1  ");
	private Button cup2 = new Button("  C u p  2  ");
	private Button cup3 = new Button("  C u p  3  ");
	
	private ImageIcon confuse = new ImageIcon("image/11.jpg");
	
	private Font font2 = new Font("Serif", Font.BOLD, 20);
	private Button start = new Button("  G o  ");
	private Button set = new Button("  R e s e t  ");
	private Button back = new Button("  M e n u  ");
	private int cupsp = 1;
	
	public FrameWork(){
		
		setCup(cupsp);
		setTitle("Final Project");
		setSize(600,400);				
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		cup1.setFont(font);
		cup2.setFont(font);
		cup3.setFont(font);
		//設置背景
		JPanel cups = new JPanel(){
			protected void paintComponent(Graphics g){
				g.drawImage(confuse.getImage(), 0, 0, getWidth(), getHeight(), this);
				super.paintComponent(g);
			}
		};
		//使原本背景透明化
		cups.setBackground(new Color(0, 0, 0, 0));
		cups.setLayout(new FlowLayout(0,60,0));
		cups.add(cup1);
		cups.add(cup2);
		cups.add(cup3);
		
		//杯子的panel
		JPanel list = new JPanel();
		list.setLayout(new BorderLayout());
		list.add(cup, BorderLayout.CENTER);
		list.add(cups, BorderLayout.SOUTH);
		list.setBackground(new Color(0, 0, 0, 0));
		
		start.setFont(font2);
		set.setFont(font2);
		back.setFont(font2);
		
		JPanel list2 = new JPanel();
		list2.setLayout(new GridLayout(3,1,100,10));
		list2.add(start);
		list2.add(set);
		list2.add(back);
		
		JPanel all = new JPanel();
		
		
		all.setLayout(new BorderLayout());
		all.add(list, BorderLayout.CENTER);
		all.add(list2, BorderLayout.EAST);
		
		add(all);
		
		start.addActionListener(new ButtonListener());
		set.addActionListener(new ButtonListener());
		back.addActionListener(new ButtonListener());
		
		cup1.addActionListener(new ButtonListener());
		cup2.addActionListener(new ButtonListener());
		cup3.addActionListener(new ButtonListener());
		back.addActionListener(new ButtonListener());
	}
	
	public void setCup(int a){
		cup.setDelay(a);
	}
	
	class ButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			
			//開始
			if(e.getSource() == start){
				cup.TimeStart();
				click = true;
			}
			
			//返回menu
			if(e.getSource() == back){
				setVisible(false);
				m = new Menu();
				m.setVisible(true);				
			}
			
			//重設
			if(e.getSource() == set){
				setVisible(false);
				LV = new Level();
				LV.setVisible(true);
			}
			
			//以下  cup1  cup2  cup3 都是判斷是否球在該杯子裡  
			if(e.getSource() == cup1){
				int k = cup.getBp();
				
				try{
					if(k == 1){
						int m = cup.getpB();
						if(m == 1){
							p = new Picture(true);
							p.setVisible(true);
						}
						else{
							throw new CupException();
						}						
					}
					if(k == 2){
						int m = cup.getpA();
						if(m == 1){
							p = new Picture(true);
							p.setVisible(true);
						}
						else{
							throw new CupException();
						}
					}
					if(k == 3){
						int m = cup.getpC();
						if(m == 1){
							p = new Picture(true);
							p.setVisible(true);
						}
						else{
							throw new CupException();
						}
					}
					
					
				}
				catch(CupException cp){
					//cp = new CupException();
				}
			}
			
			if(e.getSource() == cup2){
				int k = cup.getBp();
				
				try{
					if(k == 1){
						int m = cup.getpB();
						if(m == 2){
							p = new Picture(true);
							p.setVisible(true);
						}
						else{
							throw new CupException();
						}						
					}
					if(k == 2){
						int m = cup.getpA();
						if(m == 2){
							p = new Picture(true);
							p.setVisible(true);
						}
						else{
							throw new CupException();
						}
					}
					if(k == 3){
						int m = cup.getpC();
						if(m == 2){
							p = new Picture(true);
							p.setVisible(true);
						}
						else{
							throw new CupException();
						}
					}
					
					
				}
				catch(CupException cp){
					//cp = new CupException();
				}
			}
			
			if(e.getSource() == cup3){
				int k = cup.getBp();
				
				try{
					if(k == 1){
						int m = cup.getpB();
						if(m == 3){
							p = new Picture(true);
							p.setVisible(true);
						}
						else{
							throw new CupException();
						}						
					}
					if(k == 2){
						int m = cup.getpA();
						if(m == 3){
							p = new Picture(true);
							p.setVisible(true);
						}
						else{
							throw new CupException();
						}
					}
					if(k == 3){
						int m = cup.getpC();
						if(m == 3){
							p = new Picture(true);
							p.setVisible(true);
						}
						else{
							throw new CupException();
						}
					}
					
					
				}
				catch(CupException cp){
					//cp = new CupException();
				}
			}
		}
	}
}