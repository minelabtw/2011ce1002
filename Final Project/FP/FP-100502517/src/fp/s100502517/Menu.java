package fp.s100502517;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File; 

import javax.swing.*; 


public class Menu extends JFrame{
	private JButton start = new JButton("Start game");
	
	private JButton exit = new JButton("Exit");
	private ImageIcon fire = new ImageIcon("image/2.jpg");
		
	private Level LV;
	
	public Menu(){
		
		
		setTitle("Final Project");
		setSize(400,300);				
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel p1 = new JPanel();
		p1.setLayout(new BorderLayout());
		p1.add(start, BorderLayout.CENTER);
		
		JPanel p2 = new JPanel();
		p2.setLayout(new BorderLayout());
		p2.add(exit, BorderLayout.CENTER);
		
		//設置背景圖片
		JPanel p0 = new JPanel(){
			protected void paintComponent(Graphics g){
				g.drawImage(fire.getImage(), 0, 0, getWidth(), getHeight(), this);
				super.paintComponent(g);
			}
		};
		//使原本的背景透明化		
		p0.setBackground(new Color(0, 0, 0, 0));
		
		p0.setLayout(new FlowLayout());
		p0.add(p1);	
		p0.add(p2);
		
		setLayout(new BorderLayout());
		add(p0);
		
		
		start.addActionListener(new ButtonListener());
	
		exit.addActionListener(new ButtonListener());		
		
		
	}
	
	class ButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			
			if(e.getSource() == start){
				setVisible(false);
				LV = new Level();
				LV.setVisible(true);
			}
			
			
			if(e.getSource() == exit){
				setVisible(false);
			}
		}
	}

}
