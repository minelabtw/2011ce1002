package fp.s100502517;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Cup extends JPanel{
	Timer timer = new Timer(50, new TimerListener());
	
	private ImageIcon sky = new ImageIcon("image/12.jpg");
	
	private int aX = 0;
	private int aY = -100;
	private int bX = 0;	
	private int bY = -100;
	private int cX = 0;
	private int cY = -100;
	
	private int Bx = 0;
	private int Bp = 3;
	
	private int pA = 1;
	private int pB = 2;
	private int pC = 3;
	
	private int move = 1;
	
	private int times = 0;
	private int turn = 0;
	
	private int mode = 1;
	
	
	private int d = getWidth()/13;
	private int h = getHeight()/3;
	
	private boolean appear = true;
	
	public Cup(){
		timer.stop();
	}
	
	protected void paintComponent(Graphics g){
		
		super.paintComponent(g);		
		
		
		int d = getWidth()/13;
		int h = getHeight()/3;
		
		int CX[] = {2*d+aX, 3*d+aX, 4*d+aX, d+aX};//杯子 1 的 X  
		int CY[] = {h+aY, h+aY, 2*h+aY,2*h+aY};//杯子 1 的 Y
		int CX2[] = {6*d+bX, 7*d+bX, 8*d+bX, 5*d+bX};//杯子 2 的 X
		int CY2[] = {h+bY, h+bY, 2*h+bY,2*h+bY};//杯子 2 的 X
		int CX3[] = {10*d+cX, 11*d+cX, 12*d+cX, 9*d+cX};//杯子 3  的 X
		int CY3[] = {h+cY, h+cY, 2*h+cY,2*h+cY};//杯子 3 的 X
		
		int BX = (int)(6*d);//球 的 X
		int BY = (int)(getHeight()/2);//球 的 Y
		
		if(mode == 1){
			g.drawImage(sky.getImage(), 0, 0, getWidth(), getHeight(), this);//背景
			
			g.setColor(Color.YELLOW);
			g.fillPolygon(CX,CY,CX.length);		
						
			g.setColor(Color.GREEN);
			g.fillPolygon(CX2,CY2,CX2.length);		
				
			g.setColor(Color.BLUE);
			g.fillPolygon(CX3,CY3,CX3.length);
				
			//g.setColor(Color.RED);		
		}
			
		
		
		if(mode == 2){
			g.drawImage(sky.getImage(), 0, 0, getWidth(), getHeight(), this);
			
			g.setColor(Color.WHITE);
			g.fillPolygon(CX,CY,CX.length);		
			
			g.setColor(Color.WHITE);
			g.fillPolygon(CX2,CY2,CX2.length);		
			
			g.setColor(Color.WHITE);		
			g.fillPolygon(CX3,CY3,CX3.length);
			//g.setColor(Color.RED);		
			
			
		}
		
		if(mode == 3){
			int k = (int)(1 + 6*Math.random());
			g.drawImage(sky.getImage(), 0, 0, getWidth(), getHeight(), this);
			//讓他不段的變化顏色  形成閃耀的感覺
			switch(k){
				case 1:
					g.setColor(Color.YELLOW);
					g.fillPolygon(CX,CY,CX.length);		
					g.setColor(Color.GREEN);
					g.fillPolygon(CX2,CY2,CX2.length);		
					g.setColor(Color.BLUE);		
					g.fillPolygon(CX3,CY3,CX3.length);
					
					break;
				case 2:
					g.setColor(Color.GRAY);
					g.fillPolygon(CX,CY,CX.length);		
					g.setColor(Color.PINK);
					g.fillPolygon(CX2,CY2,CX2.length);		
					g.setColor(Color.CYAN);		
					g.fillPolygon(CX3,CY3,CX3.length);
					
					break;
				case 3:
					g.setColor(Color.GREEN);
					g.fillPolygon(CX,CY,CX.length);		
					g.setColor(Color.GRAY);
					g.fillPolygon(CX2,CY2,CX2.length);		
					g.setColor(Color.PINK);		
					g.fillPolygon(CX3,CY3,CX3.length);
					
					break;
				case 4:
					g.setColor(Color.RED);
					g.fillPolygon(CX,CY,CX.length);		
					g.setColor(Color.BLUE);
					g.fillPolygon(CX2,CY2,CX2.length);		
					g.setColor(Color.GRAY);		
					g.fillPolygon(CX3,CY3,CX3.length);
					
					break;
				case 5:
					g.setColor(Color.BLUE);
					g.fillPolygon(CX,CY,CX.length);		
					g.setColor(Color.YELLOW);
					g.fillPolygon(CX2,CY2,CX2.length);		
					g.setColor(Color.GRAY);		
					g.fillPolygon(CX3,CY3,CX3.length);
					
					break;
			}
		}
		
		//一開始杯子會在上面   準備蓋球   蓋完球以後   appear 就會變成  false 執行跟換位置的移動
		if(appear){
			g.setColor(Color.WHITE);
			g.fillPolygon(CX,CY,CX.length);		
			g.setColor(Color.WHITE);
			g.fillPolygon(CX2,CY2,CX2.length);		
			g.setColor(Color.WHITE);		
			g.fillPolygon(CX3,CY3,CX3.length);
			
			//球出現
			if(times == 0){
				g.setColor(Color.RED);
				g.fillOval(BX, BY, 50, 50);
				timer.stop();
				times = 2;
				SetBp();
			}
			//球移動到某一個杯子內
			else if(times >= 1 && times < 12){
				g.setColor(Color.RED);
				g.fillOval(BX+Bx, BY, 50, 50);
				
				if(Bp == 1){
					Bx = -4*d*times/12;
				}
				if(Bp == 2){
					Bx = 0;
				}
				if(Bp == 3){
					Bx = 4*d*times/12;
				}
				
			}
			//杯子蓋下
			else if(times == 12){
				appear = false;
				aY = 0;
				bY = 0;
				cY = 0;
				timer.stop();
			}
			
		}
		//杯子蓋下後就開始移動
		else{
			// case 1:  是 1 2 杯子交換     case 2 : 是1 3杯子交換  case 3: 是2 3杯子交換
			switch(move){
				
				// pA pB pC 代表的是 位置 
				case 1: 
					if(pA == 1){
						if(pB == 2){
							aX = 4*d*times/12;
							aY = -(int)(50*Math.sin(times*Math.PI/12));
							bX = -4*d*times/12;
							bY = (int)(50*Math.sin(times*Math.PI/12));
							if(times == 12){
								pA = 2;
								pB = 1;
								moveNextStep();
							}
						}
						if(pC == 2){
							aX = 4*d*times/12;
							aY = -(int)(50*Math.sin(times*Math.PI/12));
							cX = -4*d-4*d*times/12;
							cY = (int)(50*Math.sin(times*Math.PI/12));
							if(times == 12){
								pA = 2;
								pC = 1;
								moveNextStep();
							}
						}
					}
					if(pB == 1){
						if(pA == 2){
							bX = -4*d+4*d*times/12;
							bY = -(int)(50*Math.sin(times*Math.PI/12));
							aX = 4*d-4*d*times/12;
							aY = (int)(50*Math.sin(times*Math.PI/12));
							if(times == 12){
								pB = 2;
								pA = 1;
								moveNextStep();
							}
						}
						if(pC == 2){
							bX = -4*d+4*d*times/12;
							bY = -(int)(50*Math.sin(times*Math.PI/12));
							cX = -4*d-4*d*times/12;
							cY = (int)(50*Math.sin(times*Math.PI/12));
							if(times == 12){
								pB = 2;
								pC = 1;
								moveNextStep();
							}
						}
					}
					if(pC == 1){
						if(pA == 2){
							cX = -8*d+4*d*times/12;
							cY = -(int)(50*Math.sin(times*Math.PI/12));
							aX = 4*d-4*d*times/12;
							aY = (int)(50*Math.sin(times*Math.PI/12));
							if(times == 12){
								pC = 2;
								pA = 1;
								moveNextStep();
							}
						}
						if(pB == 2){
							cX = -8*d+4*d*times/12;
							cY = -(int)(50*Math.sin(times*Math.PI/12));
							bX = -4*d*times/12;
							bY = (int)(50*Math.sin(times*Math.PI/12)); 
							if(times == 12){
								pC = 2;
								pB = 1;
								moveNextStep();
							}
						}
					}
					break;
				case 2:
					if(pA == 1){
						if(pB == 3){
							aX = 8*d*times/12;
							aY = -(int)(50*Math.sin(times*Math.PI/12));
							bX = 4*d-8*d*times/12;
							bY = (int)(50*Math.sin(times*Math.PI/12));
							if(times == 12){
								pA = 3;
								pB = 1;
								moveNextStep();
							}
						}
						if(pC == 3){
							aX = 8*d*times/12;
							aY = -(int)(50*Math.sin(times*Math.PI/12));
							cX = -8*d*times/12;
							cY = (int)(50*Math.sin(times*Math.PI/12));
							if(times == 12){
								pA = 3;
								pC = 1;
								moveNextStep();
							}
						}
					}
					if(pB == 1){
						if(pA == 3){
							bX = -4*d+8*d*times/12;
							bY = -(int)(50*Math.sin(times*Math.PI/12));
							aX = 8*d-8*d*times/12;
							aY = (int)(50*Math.sin(times*Math.PI/12));
							if(times == 12){
								pB = 3;
								pA = 1;
								moveNextStep();
							}
						}
						if(pC == 3){
							bX = -4*d+8*d*times/12;
							bY = -(int)(50*Math.sin(times*Math.PI/12));
							cX = -8*d*times/12;
							cY = (int)(50*Math.sin(times*Math.PI/12));
							if(times == 12){
								pB = 3;
								pC = 1;
								moveNextStep();
							}
						}
					}
					if(pC == 1){
						if(pA == 3){
							cX = -8*d+8*d*times/12;
							cY = -(int)(50*Math.sin(times*Math.PI/12));
							aX = 8*d-8*d*times/12;
							aY = (int)(50*Math.sin(times*Math.PI/12));
							if(times == 12){
								pC = 3;
								pA = 1;
								moveNextStep();
							}
						}
						if(pB == 3){
							cX = -8*d+8*d*times/12;
							cY = -(int)(50*Math.sin(times*Math.PI/12));
							bX = 4*d-8*d*times/12;
							bY = (int)(50*Math.sin(times*Math.PI/12));
							if(times == 12){
								pC = 3;
								pB = 1;
								moveNextStep();
							}
						}
					}
					break;
				case 3:
					if(pA == 2){
						if(pB == 3){
							aX = 4*d+4*d*times/12;
							aY = -(int)(50*Math.sin(times*Math.PI/12));
							bX = 4*d-4*d*times/12;
							bY = (int)(50*Math.sin(times*Math.PI/12));
							if(times == 12){
								pA = 3;
								pB = 2;
								moveNextStep();
							}
						}
						if(pC == 3){
							aX = 4*d+4*d*times/12;
							aY = -(int)(50*Math.sin(times*Math.PI/12));
							cX = -4*d*times/12;
							cY = (int)(50*Math.sin(times*Math.PI/12));
							if(times == 12){
								pA = 3;
								pC = 2;
								moveNextStep();
							}
						}
					}
					if(pB == 2){
						if(pA == 3){
							bX = 4*d*times/12;
							bY = -(int)(50*Math.sin(times*Math.PI/12));
							aX = 8*d-4*d*times/12;
							aY = (int)(50*Math.sin(times*Math.PI/12));
							if(times == 12){
								pB = 3;
								pA = 2;
								moveNextStep();
							}
						}
						if(pC == 3){
							bX = 4*d*times/12;
							bY = -(int)(50*Math.sin(times*Math.PI/12));
							cX = -4*d*times/12;
							cY = (int)(50*Math.sin(times*Math.PI/12));
							if(times == 12){
								pB = 3;
								pC = 2;
								moveNextStep();
							}
						}
					}
					if(pC == 2){
						if(pA == 3){
							cX = -4*d+4*d*times/12;
							cY = -(int)(50*Math.sin(times*Math.PI/12));
							aX = 8*d-4*d*times/12;
							aY = (int)(50*Math.sin(times*Math.PI/12));
							if(times == 12){
								pC = 3;
								pA = 2;
								moveNextStep();
							}
						}
						if(pB == 3){
							cX = -4*d+4*d*times/12;
							cY = -(int)(50*Math.sin(times*Math.PI/12));
							bX = 4*d-4*d*times/12;
							bY = (int)(50*Math.sin(times*Math.PI/12));
							if(times == 12){
								pC = 3;
								pB = 2;
								moveNextStep();
							}
						}
					}
					break;
			}
		}
		
		repaint();						
	}
	
	//回傳一開始球放在哪裡  回傳給FrameWork
	public int getBp(){
		return Bp;
	}
	
	
	public int getpA(){
		return pA;
	}
	
	
	public int getpB(){
		return pB;
	}
	
	
	public int getpC(){
		return pC;
	}
	
	//用來設置球移動的速度
	public void setDelay(int a){
		this.mode = a;
		timer.setDelay(30);
		if(a == 1){
			timer.setDelay(15);
		}
	}
	
	
	public void TimeStart(){
		timer.start();
	}
	
	//因為每次開始遊戲的球  可以選擇任一杯子進入
	public void SetBp(){
		this.Bp = (int)(1+3*Math.random());
	}	
	
	//檢查  
	private void moveNextStep(){
		if(times == 12){
			times = 0;
			
			turn++;
			
			move = (int)(1 + 3*Math.random());
		}
	}
	
class TimerListener implements ActionListener{
	public void actionPerformed(ActionEvent e){
		
		
		
		if(turn != 20){
			times++;
		}
		//20次以後就會停止
		else if(turn == 20){
			timer.stop();
			times = 0;
		}
		
		
		repaint();
	}
}	
	

}
