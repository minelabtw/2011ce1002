package fp.s100502512;

import java.awt.*;
import java.util.*;
import javax.swing.*;

public class Clock extends JPanel {
	private int hour, minute, second;

	public Clock() { // 預設開始時間為零
		this.hour = 0;
		this.minute = 0;
		this.second = 15;
	}

	public void clearClock() { // 清除時間
		this.hour = 0;
		this.minute = 0;
		this.second = 00;
		repaint();
	}

	public void setCurrentClock() { // 設定目前時間
		this.second--; // 每秒second+1
		this.minute = this.second / 60;
		this.hour = this.second / 3600;
		repaint();
	}

	public int getsecond() { // get秒
		return (this.second % 60);
	}

	public int getminute() { // get分
		return (this.minute % 60);
	}

	public int gethour() { // get時
		return this.hour;
	}

}
