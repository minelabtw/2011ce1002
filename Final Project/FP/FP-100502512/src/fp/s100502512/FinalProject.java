package fp.s100502512;

import javax.swing.JFrame;

public class FinalProject {

	public static void main(String[] args) {
		FirstScreen F = new FirstScreen();
		F.setTitle("Final project");
		F.setSize(775, 900);
		F.setLocationRelativeTo(null);
		F.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		F.setVisible(true);
	}

}