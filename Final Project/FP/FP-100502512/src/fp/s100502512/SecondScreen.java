package fp.s100502512;

import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;
import java.util.Random;

import javax.swing.*;
import javax.swing.border.LineBorder;

import fp.s100502512.CFrameWork.TimeListener;

public class SecondScreen extends JFrame implements ActionListener {// 遊戲畫面
	CFrameWork C = new CFrameWork();// 數位時鐘
	JButton Exit = new JButton("Exit");
	JButton Return = new JButton("Return");
	Timer timer = new Timer(1000, new timerListener());
	private JButton Start1 = new JButton();
	private JButton Start2 = new JButton();
	private JButton Start3 = new JButton();
	private JButton Start4 = new JButton();
	private JButton Start5 = new JButton();
	private JButton Start6 = new JButton();
	private JButton Start7 = new JButton();
	private JButton Start8 = new JButton();
	JLabel tLabel = new JLabel("time");
	JLabel mlabel = new JLabel("money:");
	private JPanel p1 = new JPanel();
	private JPanel p2 = new JPanel();
	private JPanel p3 = new JPanel();
	private JPanel p4 = new JPanel();
	private JPanel p5 = new JPanel();
	private JPanel p6 = new JPanel();
	private JPanel p7 = new JPanel();
	private JPanel p8 = new JPanel();
	private Random n = new Random();
	int count = 0;
	int b1, b2, b3, b4, b5, b6, b7, b8;
	static int money = 0;

	public SecondScreen() {
		Start1.setContentAreaFilled(false);// 排版,設置圖片
		p1.setLayout(new GridLayout(2, 7, 5, 5));
		Start1.setBorder(new LineBorder(Color.black, 15));
		b1 = n.nextInt(3);
		Start1.setIcon(new ImageIcon("F/P" + b1 + ".PNG"));
		p1.add(Start1);
		Start2.setContentAreaFilled(false);
		Start2.setBorder(new LineBorder(Color.black, 15));
		b2 = n.nextInt(3);
		Start2.setIcon(new ImageIcon("F/P" + b2 + ".PNG"));
		p1.add(Start2);
		Start3.setContentAreaFilled(false);
		Start3.setBorder(new LineBorder(Color.black, 15));
		b3 = n.nextInt(3);
		Start3.setIcon(new ImageIcon("F/P" + b3 + ".PNG"));
		p1.add(Start3);
		Start4.setContentAreaFilled(false);
		Start4.setBorder(new LineBorder(Color.black, 15));
		b4 = n.nextInt(3);
		Start4.setIcon(new ImageIcon("F/P" + b4 + ".PNG"));
		p1.add(Start4);
		Start5.setContentAreaFilled(false);
		Start5.setBorder(new LineBorder(Color.black, 15));
		b5 = n.nextInt(3);
		Start5.setIcon(new ImageIcon("F/P" + b5 + ".PNG"));
		p1.add(Start5);
		Start6.setContentAreaFilled(false);
		Start6.setBorder(new LineBorder(Color.black, 15));
		b6 = n.nextInt(3);
		Start6.setIcon(new ImageIcon("F/P" + b6 + ".PNG"));
		p1.add(Start6);
		Start7.setContentAreaFilled(false);
		Start7.setBorder(new LineBorder(Color.black, 15));
		b7 = n.nextInt(3);
		Start7.setIcon(new ImageIcon("F/P" + b7 + ".PNG"));
		p1.add(Start7);
		Start8.setContentAreaFilled(false);
		Start8.setBorder(new LineBorder(Color.black, 15));
		b8 = n.nextInt(3);
		Start8.setIcon(new ImageIcon("F/P" + b8 + ".PNG"));
		p1.add(Start8);
		p2.setLayout(new BorderLayout(5, 5));
		p2.add(p1, BorderLayout.NORTH);
		add(p2);
		p3.setLayout(new BorderLayout());
		p3.add(tLabel, BorderLayout.WEST);
		tLabel.setFont(new Font("Serif", Font.BOLD, 24));
		p3.add(C, BorderLayout.CENTER);
		p6.add(mlabel, BorderLayout.CENTER);
		mlabel.setFont(new Font("Serif", Font.BOLD, 24));
		p7.add(p3, BorderLayout.NORTH);
		p7.add(p6, BorderLayout.CENTER);
		p4.setLayout(new BorderLayout(2, 2));
		p4.add(Exit, BorderLayout.CENTER);
		Exit.setFont(new Font("Serif", Font.BOLD, 24));
		p4.add(Return, BorderLayout.WEST);
		Return.setFont(new Font("Serif", Font.BOLD, 24));
		p5.setLayout(new BorderLayout());
		p5.add(p7, BorderLayout.CENTER);
		p5.add(p4, BorderLayout.EAST);
		p8.setLayout(new BorderLayout());
		p8.add(p5, BorderLayout.SOUTH);
		p8.add(p2, BorderLayout.CENTER);
		add(p8);
		Exit.addActionListener(this);
		Return.addActionListener(this);
		Start1.addActionListener(this);
		Start2.addActionListener(this);
		Start3.addActionListener(this);
		Start4.addActionListener(this);
		Start5.addActionListener(this);
		Start6.addActionListener(this);
		Start7.addActionListener(this);
		Start8.addActionListener(this);
		C.getTimer(timer);
	}

	class timerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {// 使圖片依照時間變化(生->熟->焦->生)
			count++;
			if (b1 == 2) {
				b1 = -1;
			}
			if (b2 == 2) {
				b2 = -1;
			}
			if (b3 == 2) {
				b3 = -1;
			}
			if (b4 == 2) {
				b4 = -1;
			}
			if (b5 == 2) {
				b5 = -1;
			}
			if (b6 == 2) {
				b6 = -1;
			}
			if (b7 == 2) {
				b7 = -1;
			}
			if (b8 == 2) {
				b8 = -1;
			}
			if (count % 1 == 0) {
				Start1.setIcon(new ImageIcon("F/P" + (++b1) + ".png"));
				Start1.setEnabled(true);
				Start2.setIcon(new ImageIcon("F/P" + (++b2) + ".png"));
				Start2.setEnabled(true);
				Start3.setIcon(new ImageIcon("F/P" + (++b3) + ".png"));
				Start3.setEnabled(true);
				Start4.setIcon(new ImageIcon("F/P" + (++b4) + ".png"));
				Start4.setEnabled(true);
				Start5.setIcon(new ImageIcon("F/P" + (++b5) + ".png"));
				Start5.setEnabled(true);
				Start6.setIcon(new ImageIcon("F/P" + (++b6) + ".png"));
				Start6.setEnabled(true);
				Start7.setIcon(new ImageIcon("F/P" + (++b7) + ".png"));
				Start7.setEnabled(true);
				Start8.setIcon(new ImageIcon("F/P" + (++b8) + ".png"));
				Start8.setEnabled(true);
			}

		}
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == Exit) {
			remove(p8);
			FinalScreen o = new FinalScreen();
			add(o);
			validate();
			repaint();

		}
		else if (e.getSource() == Return) {// 回去選單畫面
			FirstScreen F = new FirstScreen();
			F.setTitle("Final project");
			F.setSize(775, 900);
			F.setLocationRelativeTo(null);
			F.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			F.setVisible(true);
			this.setVisible(false);
		} 
		else if (e.getSource() == Start1) {
			if (b1 == 1) {// 設定加減錢
				money += 50;
			} else
				money -= 20;
			mlabel.setText("money:" + money);// 顯示獲得金錢
			Start1.setEnabled(false);// 點過的按鈕不能再按
		} else if (e.getSource() == Start2) {
			if (b2 == 1) {
				money += 50;
			} else
				money -= 20;
			mlabel.setText("money:" + money);
			Start2.setEnabled(false);
		} else if (e.getSource() == Start3) {
			if (b3 == 1) {
				money += 50;
			} else
				money -= 20;
			mlabel.setText("money:" + money);
			Start3.setEnabled(false);
		} else if (e.getSource() == Start4) {
			if (b4 == 1) {
				money += 50;
			} else
				money -= 20;
			mlabel.setText("money:" + money);
			Start4.setEnabled(false);
		} else if (e.getSource() == Start5) {
			if (b5 == 1) {
				money += 50;
			} else
				money -= 20;
			mlabel.setText("money:" + money);
			Start5.setEnabled(false);
		} else if (e.getSource() == Start6) {
			if (b6 == 1) {
				money += 50;
			} else
				money -= 20;
			mlabel.setText("money:" + money);
			Start6.setEnabled(false);
		} else if (e.getSource() == Start7) {
			if (b7 == 1) {
				money += 50;
			} else
				money -= 20;
			mlabel.setText("money:" + money);
			Start7.setEnabled(false);
		} else if (e.getSource() == Start8) {
			if (b8 == 1) {
				money += 50;
			} else
				money -= 20;
			mlabel.setText("money:" + money);
			Start8.setEnabled(false);
		}
	}

}
