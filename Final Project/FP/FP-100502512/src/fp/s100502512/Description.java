package fp.s100502512;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Description extends JFrame implements ActionListener {// 說明介面
	private JLabel DS = new JLabel();
	private JButton S = new JButton("Start");
	private JButton R = new JButton("Return");
	private JButton E = new JButton("Exit");
	private JPanel P1 = new JPanel();
	private JPanel P2 = new JPanel();

	public Description() {
		P1.setLayout(new BorderLayout());
		DS.setText("<html>遊戲規則:" + "<br />在規定秒數內,點取已經熟的包子獲得50塊(熟包子為笑臉)"
				+ "<br />若點取未熟或者是烤焦的包子則扣20塊錢(淡定表情與哭臉的包子) "
				+ "<br />時間限制為15秒</html>");
		DS.setFont(new Font("Serif", Font.BOLD, 35));
		add(DS);
		P2.setLayout(new BorderLayout());
		P2.add(S, BorderLayout.NORTH);
		S.setFont(new Font("Serif", Font.BOLD, 50));
		S.setContentAreaFilled(false);
		P2.add(R, BorderLayout.CENTER);
		R.setFont(new Font("Serif", Font.BOLD, 50));
		R.setContentAreaFilled(false);
		P2.add(E, BorderLayout.SOUTH);
		E.setFont(new Font("Serif", Font.BOLD, 50));
		E.setContentAreaFilled(false);
		P1.add(DS, BorderLayout.NORTH);
		P1.add(P2, BorderLayout.CENTER);
		add(P1);
		S.addActionListener(this);
		E.addActionListener(this);
		R.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == S) {
			SecondScreen f = new SecondScreen();
			f.setTitle("Final project");
			f.setSize(1700, 800);
			f.setLocationRelativeTo(null);
			f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			f.setVisible(true);
			this.setVisible(false);
			Audio audio = new Audio();
			audio.setmusic("Nyan Cat Ring.wav");
		}
		if (e.getSource() == E) {
			remove(P1);
			FinalScreen o = new FinalScreen();
			add(o);
			validate();
			repaint();
		}
		if (e.getSource() == R) {// 回去選單畫面
			FirstScreen F = new FirstScreen();
			F.setTitle("Final project");
			F.setSize(775, 900);
			F.setLocationRelativeTo(null);
			F.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			F.setVisible(true);
			this.setVisible(false);
		}
	}
}
