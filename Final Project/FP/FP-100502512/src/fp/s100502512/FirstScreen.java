package fp.s100502512;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

class FirstScreen extends JFrame implements ActionListener {
	private JButton S = new JButton("Start");
	private JButton E = new JButton("Exit");
	private JButton eButton = new JButton("Explain");
	private JLabel W = new JLabel();
	private JPanel A1 = new JPanel();
	private JPanel A2 = new JPanel();

	public FirstScreen() {// 選單畫面
		A2.setLayout(new BorderLayout());// 排版
		A2.add(S, BorderLayout.NORTH);
		S.setContentAreaFilled(false);
		S.setFont(new Font("Serif", Font.BOLD, 100));
		A2.add(E, BorderLayout.CENTER);
		E.setContentAreaFilled(false);
		E.setFont(new Font("Serif", Font.BOLD, 100));
		A2.add(eButton, BorderLayout.SOUTH);
		eButton.setContentAreaFilled(false);
		eButton.setFont(new Font("Serif", Font.BOLD, 100));
		A1.setLayout(new BorderLayout());
		W.setIcon(new ImageIcon("F/S.PNG"));// 背景圖案
		A1.add(W);
		A1.add(A2, BorderLayout.SOUTH);
		add(A1);
		S.addActionListener(this);
		E.addActionListener(this);
		eButton.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == S) {// 前進至遊戲畫面
			SecondScreen f = new SecondScreen();
			f.setTitle("Final project");
			f.setSize(1700, 800);
			f.setLocationRelativeTo(null);
			f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			f.setVisible(true);
			this.setVisible(false);
			Audio audio = new Audio();// 使用音樂程式(按按鈕則撥放音樂)
			audio.setmusic("Nyan Cat Ring.wav");
		}
		if (e.getSource() == E) {// 前進至結束畫面
			remove(A1);
			FinalScreen o = new FinalScreen();
			add(o);
			validate();
			repaint();

		}
		if (e.getSource() == eButton) {// 前進至說明畫面
			Description D1 = new Description();
			D1.setTitle("Description");
			D1.setSize(775, 800);
			D1.setLocationRelativeTo(null);
			D1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			D1.setVisible(true);
			this.setVisible(true);

		}
	}

}
