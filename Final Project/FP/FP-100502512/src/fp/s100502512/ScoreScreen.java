package fp.s100502512;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ScoreScreen extends JFrame implements ActionListener {// 計分畫面
	private JPanel P1 = new JPanel();
	private JPanel P2 = new JPanel();
	private JButton s = new JButton("Start");
	private JButton E = new JButton("Exit");
	private JLabel l = new JLabel();

	public ScoreScreen() {
		P1.setLayout(new BorderLayout());// 排版
		P1.add(s, BorderLayout.NORTH);
		s.setFont(new Font("Serif", Font.BOLD, 50));
		s.setContentAreaFilled(false);
		P1.add(E, BorderLayout.CENTER);
		E.setFont(new Font("Serif", Font.BOLD, 50));
		E.setContentAreaFilled(false);
		P2.setLayout(new BorderLayout());
		P2.add(P1, BorderLayout.CENTER);
		l.setText("Final money:" + SecondScreen.money);
		l.setFont(new Font("Serif", Font.BOLD, 50));
		P2.add(l, BorderLayout.NORTH);
		add(P2);
		s.addActionListener(this);
		E.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == s) {
			SecondScreen f = new SecondScreen();
			f.setTitle("Final project");
			f.setSize(1700, 800);
			f.setLocationRelativeTo(null);
			f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			f.setVisible(true);
			this.setVisible(false);
		}
		if (e.getSource() == E) {
			remove(P2);
			FinalScreen o = new FinalScreen();
			add(o);
			validate();
			repaint();
		}
	}
}
