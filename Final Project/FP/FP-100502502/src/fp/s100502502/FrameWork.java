package fp.s100502502;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.plaf.basic.BasicButtonUI;

import java.awt.event.*;
import java.applet.*;
import java.net.*;
import java.util.Random;

import javax.script.*;
public class FrameWork extends JApplet{
	//create JPanel, JButton, timer, CardPabel
	private int level = 1;
	private int[] Line = {-1, -1, -1, -1, -1, -1, -1, -1};
	private int two = 0;
	private int[] poly = {-1, -1};
	private Border border = new LineBorder(Color.RED);
	public CardPanel[] cardP = new CardPanel[3];
	Image head;
	Graphics back;
	Time clock = new Time();
	Timer timer;
	Font font = new Font("Serif", Font.BOLD, 25);
	JPanel P1 = new JPanel();
	JPanel P2 = new JPanel();
	JLabel L1 = new JLabel("如果準備好了，就按下開始按鈕吧！");
	JLabel time = new JLabel("0:0:0");
	JLabel sign = new JLabel(new ImageIcon("image/14.gif"));
	JButton Start = new JButton("Start");
	JButton Try = new JButton("Try again");
	JButton Next = new JButton("Next");
	FrameWork(){
		//set Button
		Start.setBackground(Color.GRAY);
		Start.setFont(font);
		Start.setUI(new BasicButtonUI());
		Next.setBackground(Color.GRAY);
		Next.setFont(font);
		Next.setUI(new BasicButtonUI());
		Try.setBackground(Color.GRAY);
		Try.setFont(font);
		Try.setUI(new BasicButtonUI());
		Start.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(e.getSource() == Start){
					P1.remove(Start);
					P1.setLayout(new GridLayout(1, 3, 1, 1));
					L1.setBackground(Color.PINK);
					L1.setForeground(Color.BLACK);
					time.setFont(font);
					time.setHorizontalAlignment(JLabel.CENTER);
					L1.setHorizontalAlignment(JLabel.CENTER);
					L1.setText("");
					P1.add(L1);
					P1.add(sign);
					P1.add(time);
					P1.setBackground(Color.PINK);
					cardP[level-1] = new CardPanel(level);//create new CardPanel
					for(int i = 0; i < 13 ; i++){
						for(int j = 0; j < 21; j++){
							cardP[level-1].card.getCardSeat()[i][j].addMouseListener(new MouseListener(cardP[level-1], i, j));
						}
					}
					setLayout(new BorderLayout()); 
					add(P1, BorderLayout.NORTH);
					add(cardP[level-1], BorderLayout.CENTER);//add new CardPanel
					timer = new Timer(1000, new TimerListener(cardP[level-1]));
					validate();
					timer.start();
				}
				else{}
			}
		});
		L1.setFont(font);
		L1.setForeground(Color.WHITE);
		L1.setHorizontalAlignment(JLabel.CENTER);
		L1.setOpaque(true);
		L1.setBackground(Color.BLACK);
		P1.setLayout(new BorderLayout());
		P1.add(Start, BorderLayout.SOUTH);
		P1.add(L1, BorderLayout.CENTER);
		add(P1);
	}
	public boolean ConnectTwo(CardPanel P, int x1, int y1, int x2, int y2){
		//if two label can connect, clear two label and draw line to connect
		boolean ON = false;
		head = P.createImage(870,655);
		back = head.getGraphics();
		if((x1 == x2 && Math.abs(y1-y2) == 1) || (y1 == y2 && Math.abs(x1-x2) == 1)){
			P.card.getCardSeat()[x1][y1].setIcon(null);
			P.remove(P.card.getCardSeat()[x1][y1]);
			P.add(new JLabel(), 21*x1+y1);
			P.card.getCardSeat()[x2][y2].setIcon(null);
			P.remove(P.card.getCardSeat()[x2][y2]);
			P.add(new JLabel(), 21*x2+y2);
			P.paint2(P.getGraphics(),1, P.card.getCardSeat()[x1][y1].getX()+P.card.getCardSeat()[x1][y1].getWidth()/2, P.card.getCardSeat()[x1][y1].getY()+P.card.getCardSeat()[x1][y1].getHeight()/2, 
					P.card.getCardSeat()[x2][y2].getX()+P.card.getCardSeat()[x2][y2].getWidth()/2, P.card.getCardSeat()[x2][y2].getY()+P.card.getCardSeat()[x2][y2].getHeight()/2);
			P.paint2(back,1, P.card.getCardSeat()[x1][y1].getX()+P.card.getCardSeat()[x1][y1].getWidth()/2, P.card.getCardSeat()[x1][y1].getY()+P.card.getCardSeat()[x1][y1].getHeight()/2, 
					P.card.getCardSeat()[x2][y2].getX()+P.card.getCardSeat()[x2][y2].getWidth()/2, P.card.getCardSeat()[x2][y2].getY()+P.card.getCardSeat()[x2][y2].getHeight()/2);
			try {//delay
		        Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			P.paint2(P.getGraphics(),0, P.card.getCardSeat()[x1][y1].getX()+P.card.getCardSeat()[x1][y1].getWidth()/2, P.card.getCardSeat()[x1][y1].getY()+P.card.getCardSeat()[x1][y1].getHeight()/2, 
					P.card.getCardSeat()[x2][y2].getX()+P.card.getCardSeat()[x2][y2].getWidth()/2, P.card.getCardSeat()[x2][y2].getY()+P.card.getCardSeat()[x2][y2].getHeight()/2);
			ON = true;
		}
		else if((y1<y2 && x1==x2 && obstacle(P, x1, y1+1, x2, y2-1) == true) || (y1>y2 && x1==x2 && obstacle(P, x2, y2+1, x1, y1-1) == true)){//直
			//save two label's x and y
			Line[0] = x1;
			Line[1] = y1;
			Line[2] = x2;
			Line[3] = y2;
			ON = true;
		}
		else if((y1==y2 && x1>x2 && obstacle(P, x2+1, y2, x1-1, y1)== true) || (y1==y2 && x1<x2 && obstacle(P, x1+1, y1, x2-1, y2)== true)){//直
			//save two label's x and y
			Line[0] = x1;
			Line[1] = y1;
			Line[2] = x2;
			Line[3] = y2;
			ON = true;
		}
		else if(Line5(P, 1, x1, y1, x2, y2) == true){//旁直
			ON = true;
		}
		else if(Line6(P, 1, x1, y1, x2, y2) == true){//內外2
			ON = true;
		}
		else if(Line7(P, 1, x1, y1, x2, y2) == true){//內內2
			ON = true;
		}
		else if(Line1(P, 1,x1, y1, x2, y2) == true){//1
			ON = true;
		}
		else if(Line2(P, 1, x1, y1, x2, y2) == true ){//2
			ON = true;
		}
		else if(Line3(P, 1, x1, y1, x2, y2) == true){//3
			ON = true;
		}
		else if(Line4(P, 1, x1, y1, x2, y2) == true){//4
			ON = true;
		}
		else{}
		if(ON == true){
			if(checkLine() == 4){// if have 2 points
				P.card.getCardSeat()[x1][y1].setIcon(null);
				P.remove(P.card.getCardSeat()[x1][y1]);
				P.add(new JLabel(), 21*x1+y1);
				P.card.getCardSeat()[x2][y2].setIcon(null);
				P.remove(P.card.getCardSeat()[x2][y2]);
				P.add(new JLabel(), 21*x2+y2);
				P.paint2(P.getGraphics(),1, P.card.getCardSeat()[Line[0]][Line[1]].getX()+P.card.getCardSeat()[Line[0]][Line[1]].getWidth()/2, P.card.getCardSeat()[Line[0]][Line[1]].getY()+P.card.getCardSeat()[Line[0]][Line[1]].getHeight()/2, 
						P.card.getCardSeat()[Line[2]][Line[3]].getX()+P.card.getCardSeat()[Line[2]][Line[3]].getWidth()/2, P.card.getCardSeat()[Line[2]][Line[3]].getY()+P.card.getCardSeat()[Line[2]][Line[3]].getHeight()/2);
				P.paint2(back,1, P.card.getCardSeat()[Line[0]][Line[1]].getX()+P.card.getCardSeat()[Line[0]][Line[1]].getWidth()/2, P.card.getCardSeat()[Line[0]][Line[1]].getY()+P.card.getCardSeat()[Line[0]][Line[1]].getHeight()/2, 
						P.card.getCardSeat()[Line[2]][Line[3]].getX()+P.card.getCardSeat()[Line[2]][Line[3]].getWidth()/2, P.card.getCardSeat()[Line[2]][Line[3]].getY()+P.card.getCardSeat()[Line[2]][Line[3]].getHeight()/2);
				try {//delay
			        Thread.sleep(500);
				} catch (InterruptedException e) {
			        e.printStackTrace();
				}
				P.paint2(P.getGraphics(),0, P.card.getCardSeat()[Line[0]][Line[1]].getX()+P.card.getCardSeat()[Line[0]][Line[1]].getWidth()/2, P.card.getCardSeat()[Line[0]][Line[1]].getY()+P.card.getCardSeat()[Line[0]][Line[1]].getHeight()/2, 
						P.card.getCardSeat()[Line[2]][Line[3]].getX()+P.card.getCardSeat()[Line[2]][Line[3]].getWidth()/2, P.card.getCardSeat()[Line[2]][Line[3]].getY()+P.card.getCardSeat()[Line[2]][Line[3]].getHeight()/2);
				for(int i = 0; i < 8; i++){//reset value
					Line[i] = -1;
				}
			}
			else if(checkLine() == 6){//if have 3 points 
				P.card.getCardSeat()[x1][y1].setIcon(null);
				P.remove(P.card.getCardSeat()[x1][y1]);
				P.add(new JLabel(), 21*x1+y1);
				P.card.getCardSeat()[x2][y2].setIcon(null);
				P.remove(P.card.getCardSeat()[x2][y2]);
				P.add(new JLabel(), 21*x2+y2);
				P.paint3(P.getGraphics(),1, P.card.getCardSeat()[Line[0]][Line[1]].getX()+P.card.getCardSeat()[Line[0]][Line[1]].getWidth()/2, P.card.getCardSeat()[Line[0]][Line[1]].getY()+P.card.getCardSeat()[Line[0]][Line[1]].getHeight()/2, 
						P.card.getCardSeat()[Line[2]][Line[3]].getX()+P.card.getCardSeat()[Line[2]][Line[3]].getWidth()/2, P.card.getCardSeat()[Line[2]][Line[3]].getY()+P.card.getCardSeat()[Line[2]][Line[3]].getHeight()/2, 
						P.card.getCardSeat()[Line[4]][Line[5]].getX()+P.card.getCardSeat()[Line[4]][Line[5]].getWidth()/2, P.card.getCardSeat()[Line[4]][Line[5]].getY()+P.card.getCardSeat()[Line[4]][Line[5]].getHeight()/2);
				P.paint3(back,1, P.card.getCardSeat()[Line[0]][Line[1]].getX()+P.card.getCardSeat()[Line[0]][Line[1]].getWidth()/2, P.card.getCardSeat()[Line[0]][Line[1]].getY()+P.card.getCardSeat()[Line[0]][Line[1]].getHeight()/2, 
						P.card.getCardSeat()[Line[2]][Line[3]].getX()+P.card.getCardSeat()[Line[2]][Line[3]].getWidth()/2, P.card.getCardSeat()[Line[2]][Line[3]].getY()+P.card.getCardSeat()[Line[2]][Line[3]].getHeight()/2, 
						P.card.getCardSeat()[Line[4]][Line[5]].getX()+P.card.getCardSeat()[Line[4]][Line[5]].getWidth()/2, P.card.getCardSeat()[Line[4]][Line[5]].getY()+P.card.getCardSeat()[Line[4]][Line[5]].getHeight()/2);
				try {//delay
			        Thread.sleep(500);
				} catch (InterruptedException e) {
			        e.printStackTrace();
				}
				P.paint3(P.getGraphics(),0, P.card.getCardSeat()[Line[0]][Line[1]].getX()+P.card.getCardSeat()[Line[0]][Line[1]].getWidth()/2, P.card.getCardSeat()[Line[0]][Line[1]].getY()+P.card.getCardSeat()[Line[0]][Line[1]].getHeight()/2, 
						P.card.getCardSeat()[Line[2]][Line[3]].getX()+P.card.getCardSeat()[Line[2]][Line[3]].getWidth()/2, P.card.getCardSeat()[Line[2]][Line[3]].getY()+P.card.getCardSeat()[Line[2]][Line[3]].getHeight()/2, 
						P.card.getCardSeat()[Line[4]][Line[5]].getX()+P.card.getCardSeat()[Line[4]][Line[5]].getWidth()/2, P.card.getCardSeat()[Line[4]][Line[5]].getY()+P.card.getCardSeat()[Line[4]][Line[5]].getHeight()/2);
				for(int i = 0; i < 8; i++){//reset value
					Line[i] = -1;
				}
			}
			else if(checkLine() == 8){//if have 4 points 
				P.card.getCardSeat()[x1][y1].setIcon(null);
				P.remove(P.card.getCardSeat()[x1][y1]);
				P.add(new JLabel(), 21*x1+y1);
				P.card.getCardSeat()[x2][y2].setIcon(null);
				P.remove(P.card.getCardSeat()[x2][y2]);
				P.add(new JLabel(), 21*x2+y2);
				P.paint4(P.getGraphics(),1, P.card.getCardSeat()[Line[0]][Line[1]].getX()+P.card.getCardSeat()[Line[0]][Line[1]].getWidth()/2, P.card.getCardSeat()[Line[0]][Line[1]].getY()+P.card.getCardSeat()[Line[0]][Line[1]].getHeight()/2, 
						P.card.getCardSeat()[Line[2]][Line[3]].getX()+P.card.getCardSeat()[Line[2]][Line[3]].getWidth()/2, P.card.getCardSeat()[Line[2]][Line[3]].getY()+P.card.getCardSeat()[Line[2]][Line[3]].getHeight()/2, 
						P.card.getCardSeat()[Line[4]][Line[5]].getX()+P.card.getCardSeat()[Line[4]][Line[5]].getWidth()/2, P.card.getCardSeat()[Line[4]][Line[5]].getY()+P.card.getCardSeat()[Line[4]][Line[5]].getHeight()/2, 
						P.card.getCardSeat()[Line[6]][Line[7]].getX()+P.card.getCardSeat()[Line[6]][Line[7]].getWidth()/2, P.card.getCardSeat()[Line[6]][Line[7]].getY()+P.card.getCardSeat()[Line[6]][Line[7]].getHeight()/2);
				P.paint4(back,1, P.card.getCardSeat()[Line[0]][Line[1]].getX()+P.card.getCardSeat()[Line[0]][Line[1]].getWidth()/2, P.card.getCardSeat()[Line[0]][Line[1]].getY()+P.card.getCardSeat()[Line[0]][Line[1]].getHeight()/2, 
						P.card.getCardSeat()[Line[2]][Line[3]].getX()+P.card.getCardSeat()[Line[2]][Line[3]].getWidth()/2, P.card.getCardSeat()[Line[2]][Line[3]].getY()+P.card.getCardSeat()[Line[2]][Line[3]].getHeight()/2, 
						P.card.getCardSeat()[Line[4]][Line[5]].getX()+P.card.getCardSeat()[Line[4]][Line[5]].getWidth()/2, P.card.getCardSeat()[Line[4]][Line[5]].getY()+P.card.getCardSeat()[Line[4]][Line[5]].getHeight()/2,
						P.card.getCardSeat()[Line[6]][Line[7]].getX()+P.card.getCardSeat()[Line[6]][Line[7]].getWidth()/2, P.card.getCardSeat()[Line[6]][Line[7]].getY()+P.card.getCardSeat()[Line[6]][Line[7]].getHeight()/2);
				try {//delay
			        Thread.sleep(500);
				} catch (InterruptedException e) {
			        e.printStackTrace();
				}
				
				P.paint4(P.getGraphics(),0, P.card.getCardSeat()[Line[0]][Line[1]].getX()+P.card.getCardSeat()[Line[0]][Line[1]].getWidth()/2, P.card.getCardSeat()[Line[0]][Line[1]].getY()+P.card.getCardSeat()[Line[0]][Line[1]].getHeight()/2, 
						P.card.getCardSeat()[Line[2]][Line[3]].getX()+P.card.getCardSeat()[Line[2]][Line[3]].getWidth()/2, P.card.getCardSeat()[Line[2]][Line[3]].getY()+P.card.getCardSeat()[Line[2]][Line[3]].getHeight()/2, 
						P.card.getCardSeat()[Line[4]][Line[5]].getX()+P.card.getCardSeat()[Line[4]][Line[5]].getWidth()/2, P.card.getCardSeat()[Line[4]][Line[5]].getY()+P.card.getCardSeat()[Line[4]][Line[5]].getHeight()/2, 
						P.card.getCardSeat()[Line[6]][Line[7]].getX()+P.card.getCardSeat()[Line[6]][Line[7]].getWidth()/2, P.card.getCardSeat()[Line[6]][Line[7]].getY()+P.card.getCardSeat()[Line[6]][Line[7]].getHeight()/2);
				for(int i = 0; i < 8; i++){//reset value
					Line[i] = -1;
				}
			}
			else{}
		}
		return ON;
	}
	public boolean check(CardPanel P){//if still have cards = false 
		boolean ON = true;
		for(int i = 0; i < 13; i++){
			for(int j = 0; j < 21; j++){
				if(P.card.getCardSeat()[i][j].getIcon() != null){
					ON = false;
					i = 13;
					break;	
				}
			}
		}
		return ON;
	}
	public void win(CardPanel P){//if win
		remove(P1);
		remove(P);
		level = level+1;//level up
		L1.setText("你贏了!");
		L1.setFont(font);
		L1.setForeground(Color.WHITE);
		L1.setHorizontalAlignment(JLabel.CENTER);
		L1.setOpaque(true);
		L1.setBackground(Color.BLACK);
		P2.setLayout(new BorderLayout());
		P2.add(L1, BorderLayout.CENTER);
		P2.add(Next, BorderLayout.SOUTH);
		add(P2);
		validate();
		Next.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Next.removeActionListener(this);
				remove(P2);
				clock.clearTime();
				L1.setBackground(Color.PINK);
				L1.setForeground(Color.BLACK);
				L1.setText("" );
				P1.add(L1, 0);
				cardP[level-1] = new CardPanel(level);//create new CardPanel
				add(cardP[level-1], BorderLayout.CENTER);//add an new CardPanel
				add(P1, BorderLayout.NORTH);
				for(int i = 0; i < 13 ; i++){
					for(int j = 0; j < 21; j++){
						cardP[level-1].card.getCardSeat()[i][j].addMouseListener(new MouseListener(cardP[level-1], i, j));
					}
				}
				clock.clearTime();
				timer = new Timer(1000, new TimerListener(cardP[level-1]));
				validate();
				timer.start();//time start
			}
		});
		
	}
	public int checkLine(){
		int Linenum = 0;
		for(int i = 0; i < 8; i++){
			if(Line[i] != -1){
				Linenum++;
			}
		}
		return Linenum;
	}
	public boolean Line3(CardPanel P, int off, int x1, int y1, int x2, int y2){
		boolean on = false;
		if(x1<x2 && y1<y2){
			for(int i = 1; x1-i>=0; i++){
				if(obstacle(P, x1-i, y1, x1-1, y1) == true && obstacle(P, x1-i, y1, x1-i, y2) == true && obstacle(P, x1-i, y2, x2-1, y2)== true){
					if(off == 1){
						Line[0] = x1;
						Line[1] = y1;
						Line[2] = x1-i;
						Line[3] = y1;
						Line[4] = x1-i;
						Line[5] = y2;
						Line[6] = x2;
						Line[7] = y2;
					}
					else{}
					on = true;
					break;
				}
				else{}
			}
		}
		else if(x1<x2 && y1>y2){
			for(int i = 1; x1-i>=0; i++){
				if(obstacle(P, x1-i, y1, x1-1, y1) == true && obstacle(P, x1-i, y2, x1-i, y1) == true && obstacle(P, x1-i, y2, x2-1, y2)){
					if(off == 1){
						Line[0] = x1;
						Line[1] = y1;
						Line[2] = x1-i;
						Line[3] = y1;
						Line[4] = x1-i;
						Line[5] = y2;
						Line[6] = x2;
						Line[7] = y2;
					}
					else{}
					on = true;
					break;
				}
				else{}
			}
		}
		else if(x1>x2 && y1>y2){
			for(int i = 1; x2-i>=0; i++){
				if(obstacle(P, x2-i, y2, x2-1, y1) == true && obstacle(P, x2-i, y2, x2-i, y1) == true && obstacle(P, x2-i, y1, x1-1, y1)){
					if(off == 1){
						Line[0] = x2;
						Line[1] = y2;
						Line[2] = x2-i;
						Line[3] = y2;
						Line[4] = x2-i;
						Line[5] = y1;
						Line[6] = x1;
						Line[7] = y1;
					}
					else{}
					on = true;
					break;
				}
				else{}
			}
		}
		else if(x1>x2 && y1<y2){
			for(int i = 1; x2-i>=0; i++){
				if(obstacle(P, x2-i, y2, x2-1, y2) == true && obstacle(P, x2-i, y1, x2-i, y2) == true && obstacle(P, x2-i, y1, x1-1, y1) == true){
					if(off == 1){
						Line[0] = x2;
						Line[1] = y2;
						Line[2] = x2-i;
						Line[3] = y2;
						Line[4] = x2-i;
						Line[5] = y1;
						Line[6] = x1;
						Line[7] = y1;
					}
					else{}
					on = true;
					break;
				}
				else{}
			}
		}
		else{}
		return on;
	}
	public boolean Line4(CardPanel P, int off, int x1, int y1, int x2, int y2){
		boolean on = false;
		if(x1<x2 && y1<y2){
			for(int i = 1; y2+i<=20; i++){
				if(obstacle(P, x2, y2+1, x2, y2+i) == true && obstacle(P, x1, y2+i, x2, y2+i) == true && obstacle(P, x1, y1+1, x1, y2+i) == true){
					if(off == 1){
						Line[0] = x1;
						Line[1] = y1;
						Line[2] = x1;
						Line[3] = y2+i;
						Line[4] = x2;
						Line[5] = y2+i;
						Line[6] = x2;
						Line[7] = y2;
					}
					else{}
					on = true;
					break;
				}
				else{}
			}
			
		}
		else if(x1<x2 && y1>y2){
			for(int i = 1; y1+i<=20; i++){
				if(obstacle(P, x1, y1+1, x1, y1+i) == true && obstacle(P, x1, y1+i, x2, y1+i) == true && obstacle(P, x2, y2+1, x2, y1+i) == true){
					if(off == 1){
						Line[0] = x1;
						Line[1] = y1;
						Line[2] = x1;
						Line[3] = y1+i;
						Line[4] = x2;
						Line[5] = y1+i;
						Line[6] = x2;
						Line[7] = y2;
					}
					else{}
					on = true;
					break;
				}
				else{}
			}
		}
		else if(x1>x2 && y1>y2){
			for(int i = 1; y2+i<=20; i++){
				if(obstacle(P, x1, y1+1, x1, y1+i) == true && obstacle(P, x2, y1+i, x1, y1+i) == true && obstacle(P, x2, y2+1, x2, y1+i)){
					if(off == 1){
						Line[0] = x2;
						Line[1] = y2;
						Line[2] = x2;
						Line[3] = y1+i;
						Line[4] = x1;
						Line[5] = y1+i;
						Line[6] = x1;
						Line[7] = y1;
					}
					else{}
					on = true;
					break;
				}
				else{}
			}
		}
		else if(x1>x2 && y1<y2){
			for(int i = 1; y2+i<=20; i++){
				if(obstacle(P, x2, y2+1, x2, y2+i) == true && obstacle(P, x2, y2+i, x1, y2+i) == true && obstacle(P, x1, y1+1, x1, y2+i) == true){
					if(off == 1){
						Line[0] = x1;
						Line[1] = y1;
						Line[2] = x1;
						Line[3] = y2+i;
						Line[4] = x2;
						Line[5] = y2+i;
						Line[6] = x2;
						Line[7] = y2;
					}
					else{}
					on = true;
					break;
				}
				else{}
			}
		}
		else{}
		return on;
	}
	public boolean Line1(CardPanel P, int off, int x1, int y1, int x2, int y2){
		boolean on = true;
		if(x1<x2 && y1<y2){
			for(int i = 1; y1-i>=0; i++){
				if(obstacle(P, x1, y1-i, x1, y1-1) == true && obstacle(P, x1, y1-i, x2, y1-i) == true && obstacle(P, x2, y1-i, x2, y2-1)){
					if(off == 1){
						Line[0] = x1;
						Line[1] = y1;
						Line[2] = x1;
						Line[3] = y1-i;
						Line[4] = x2;
						Line[5] = y1-i;
						Line[6] = x2;
						Line[7] = y2;
					}
					else{}
					on = true;
					break;
				}
				else{}
			}
		}
		else if(x1<x2 && y1>y2){
			for(int i = 1; y2-i>=0; i++){
				if(obstacle(P, x2, y2-i, x2, y2-1) == true && obstacle(P, x1, y2-i, x2, y2-i) == true && obstacle(P, x1, y2-i, x1, y1-1) == true){
					if(off == 1){
						Line[0] = x1;
						Line[1] = y1;
						Line[2] = x1;
						Line[3] = y2-i;
						Line[4] = x2;
						Line[5] = y2-i;
						Line[6] = x2;
						Line[7] = y2;
					}
					else{}
					on = true;
					break;
				}
				else{}
			}
		}
		else if(x1>x2 && y1>y2){
			for(int i = 1; y2-i>=0; i++){
				if(obstacle(P, x2, y2-i, x2, y2-1) == true && obstacle(P, x2, y2-i, x1, y2-i) == true && obstacle(P, x1, y2-i, x1, y1-1)){
					if(off == 1){
						Line[0] = x1;
						Line[1] = y1;
						Line[2] = x1;
						Line[3] = y2-i;
						Line[4] = x2;
						Line[5] = y2-i;
						Line[6] = x2;
						Line[7] = y2;
					}
					else{}
					on = true;
					break;
				}
				else{}
			}
		}
		else if(x1>x2 && y1<y2){
			for(int i = 0; y1-i>=0; i++){
				if(obstacle(P, x1, y1-i, x1, y1-1) == true && obstacle(P, x2, y1-i, x1, y1-i) == true && obstacle(P, x2, y1-i, x2, y2-1)){
					if(off == 1){
						Line[0] = x1;
						Line[1] = y1;
						Line[2] = x1;
						Line[3] = y1-i;
						Line[4] = x2;
						Line[5] = y1-i;
						Line[6] = x2;
						Line[7] = y2;
					}
					else{}
					on = true;
					break;
				}
				else{}
			}
		}
		else{}
		return on;
	}
	public boolean Line2(CardPanel P, int off, int x1, int y1, int x2, int y2){
		boolean on = false;
		if(x1<x2 && y1<y2){
			for(int i = 0; x2+i<=12; i++){
				if(obstacle(P, x2+1, y2, x2+i, y1) == true && obstacle(P, x2+i, y1, x2+i, y2) == true && obstacle(P, x1+1, y1, x2+i, y1) == true){
					if(off == 1){
						Line[0] = x1;
						Line[1] = y1;
						Line[2] = x2+i;
						Line[3] = y1;
						Line[4] = x2+i;
						Line[5] = y2;
						Line[6] = x2;
						Line[7] = y2;
					}
					else{}
					on = true;
					break;
				}
				else{}
			}
		}
		else if(x1<x2 && y1>y2){
			for(int i = 1; x2+i<=12; i++){
				if(obstacle(P, x2+1, y2, x2+i, y2) == true && obstacle(P, x2+i, y2, x2+i, y1) == true && obstacle(P, x1+1, y1, x2+i, y1) == true){
					if(off == 1){
						Line[0] = x1;
						Line[1] = y1;
						Line[2] = x2+i;
						Line[3] = y1;
						Line[4] = x2+1;
						Line[5] = y2;
						Line[6] = x2;
						Line[7] = y2;
					}
					else{}
					on = true;
					break;
				}
				else{}
			}
		}
		else if(x1>x2 && y1>y2){
			for(int i = 1; x1+i<=12; i++){
				if(obstacle(P, x1+1, y1, x1+i, y1) == true && obstacle(P, x1+i, y2, x1+i, y1) == true && obstacle(P, x2+1, y2, x1+i, y2) == true){
					if(off == 1){
						Line[0] = x1;
						Line[1] = y1;
						Line[2] = x1+i;
						Line[3] = y1;
						Line[4] = x1+i;
						Line[5] = y2;
						Line[6] = x2;
						Line[7] = y2;
					}
					else{}
					on = true;
					break;
				}
				else{}
			}
		}
		else if(x1>x2 && y1<y2){
			for(int i = 1; x1+i<=12; i++){
				if(obstacle(P, x1+1, y1, x1+i, y1) == true && obstacle(P, x1+i, y1, x1+i, y2) == true && obstacle(P, x2+1, y2, x1+i, y2) == true){
					if(off == 1){
						Line[0] = x1;
						Line[1] = y1;
						Line[2] = x1+i;
						Line[3] = y1;
						Line[4] = x1+i;
						Line[5] = y2;
						Line[6] = x2;
						Line[7] = y2;
					}
					else{}
					on = true;
					break;
				}
				else{}
			}	
		}
		else{}
		return on;
	}
	public boolean Line5(CardPanel P, int off, int x1, int y1, int x2, int y2){//直線
		if((x1 == x2 && Math.abs(y1-y2) == 1) || (y1 == y2 && Math.abs(x1-x2) == 1)){
			return true;
		}
		else if((x1 == x2 && y1<y2 && obstacle(P, x1-1, y1, x2-1, y2) == true) || (x1 == x2 && y1>y2 && obstacle(P, x2-1, y2, x1-1, y1) == true)){
			if(off == 1){
				Line[0] = x1;
				Line[1] = y1;
				Line[2] = x1-1;
				Line[3] = y1;
				Line[4] = x2-1;
				Line[5] = y2;
				Line[6] = x2;
				Line[7] = y2;
			}
			else{}
			return true;
		}
		else if((x1 == x2 && y1<y2 && obstacle(P, x1+1, y1, x2+1, y2) == true) || (x1 == x2 && y1>y2 && obstacle(P, x2+1, y2, x1+1, y1) == true)){
			if(off == 1){
				Line[0] = x1;
				Line[1] = y1;
				Line[2] = x1+1;
				Line[3] = y1;
				Line[4] = x2+1;
				Line[5] = y2;
				Line[6] = x2;
				Line[7] = y2;
			}
			else{}
			return true;
		}
		else if((y1 == y2 && x1<x2 && obstacle(P, x1, y1+1, x2, y2+1) == true) || (y1 == y2 && x1>x2 && obstacle(P, x2, y2+1, x1, y1+1) == true)){
			if(off == 1){
				Line[0] = x1;
				Line[1] = y1;
				Line[2] = x1;
				Line[3] = y1+1;
				Line[4] = x2;
				Line[5] = y2+1;
				Line[6] = x2;
				Line[7] = y2;
			}
			else{}
			return true;
		}
		else if((y1 == y2 && x1<x2 && obstacle(P, x1, y1-1, x2, y2-1) == true) || (y1 == y2 && x1>x2 && obstacle(P, x2, y2-1, x1, y1-1) == true)){
			if(off == 1){
				Line[0] = x1;
				Line[1] = y1;
				Line[2] = x1;
				Line[3] = y1-1;
				Line[4] = x2;
				Line[5] = y2-1;
				Line[6] = x2;
				Line[7] = y2;
			}
			else{}
			return true;
		}
		else 
			return false;
	}
	public boolean Line6(CardPanel P, int off, int x1, int y1, int x2, int y2){//內外2
		if((x1<x2 && y1<y2 && obstacle(P, x1, y1+1, x1, y2) == true && obstacle(P, x1, y2, x2-1, y2) == true) || 
				(x1<x2 && y2<y1 && obstacle(P, x1, y2, x2-1, y2) == true && obstacle(P, x1, y2, x1, y1-1) == true) || 
				(x1>x2 && y1>y2 && obstacle(P, x2+1, y2, x1, y2) == true && obstacle(P, x1, y2, x1, y1-1) == true) || 
				(x1>x2 && y2>y1 && obstacle(P, x2+1, y2, x1, y2) == true && obstacle(P, x1, y1+1, x1, y2) == true)){
			if(off == 1){
				Line[0] = x1;
				Line[1] = y1;
				Line[2] = x1;
				Line[3] = y2;
				Line[4] = x2;
				Line[5] = y2;
			}
			else{}
			return true;
		}
		else if((x1<x2 && y1<y2 && obstacle(P, x1+1, y1, x2, y1) == true) && (obstacle(P, x2, y1, x2, y2-1) == true) || 
				(x1<x2 && y2<y1 && obstacle(P, x1+1, y1, x2, y1) == true && obstacle(P, x2, y2+1, x2, y1) == true) || 
				(x1>x2 && y1>y2 && obstacle(P, x2, y2+1, x2, y1) == true && obstacle(P, x2, y1, x1-1, y1) == true) || 
				(x1>x2 && y2>y1 && obstacle(P, x2, y1, x2, y2-1) == true && obstacle(P, x2, y1, x1-1, y1) == true)){
			if(off == 1){
				Line[0] = x2;
				Line[1] = y2;
				Line[2] = x2;
				Line[3] = y1;
				Line[4] = x1;
				Line[5] = y1;
			}
			else{}
			return true;
		}
		else 
			return false;
	}
	public boolean Line7(CardPanel P, int off, int x1, int y1, int x2, int y2){//內內2
		boolean on = false;
		if(x2-x1>1){
			for(int i = 1; i< x2-x1; i++){
				if((y1<y2 && obstacle(P, x1+1, y1, x1+i, y1) == true && obstacle(P, x1+i, y1, x1+i, y2) == true && obstacle(P, x1+i, y2, x2-1, y2) == true) || 
						(y2<y1 && obstacle(P, x1+1, y1, x1+i, y1) == true && obstacle(P, x1+i, y1, x1+i, y2) == true && obstacle(P, x1+i, y2, x2-1, y2) == true)){					if(off == 1){
						Line[0] = x1;
						Line[1] = y1;
						Line[2] = x1+i;
						Line[3] = y1;
						Line[4] = x1+i;
						Line[5] = y2;
						Line[6] = x2;
						Line[7] = y2;
					}
					else{}
					on = true;
					break;
				}
				else{}
			}
		}
		else if(x1-x2>1){
			for(int i = 1; i < x1-x2; i++){
				if((y1>y2 && obstacle(P, x2+1, y2, x2+i, y2) == true && obstacle(P, x2+i, y2, x2+i, y1) == true && obstacle(P, x2+i, y1, x1-1, y1) == true) || 
						(y2>y1 && obstacle(P, x1-i, y1, x1-1, y1) == true && obstacle(P, x1-i, y1, x1-i, y2) == true && obstacle(P, x1-i, y2, x2+1, y2) == true)){
					if(off == 1){
						Line[0] = x2;
						Line[1] = y2;
						Line[2] = x2+i;
						Line[3] = y2;
						Line[4] = x2+i;
						Line[5] = y1;
						Line[6] = x1;
						Line[7] = y1;
					}
					else{}
					on = true;
					break;
				}
				else{}
			}
		}
		else if(y1-y2>1){
		for(int i = 1; i<y1-y2; i++){
			if(x1<x2 && obstacle(P, x1, y1-i, x1, y1-1) == true && obstacle(P, x1, y1-i, x2, y1-i) == true && obstacle(P, x2, y2+1, x2, y1-i) == true){
				if(off == 1){
					Line[0] = x1;
					Line[1] = y1;
					Line[2] = x1;
					Line[3] = y1-i;
					Line[4] = x2;
					Line[5] = y1-i;
					Line[6] = x2;
					Line[7] = y2;
				}
				else{}
				on = true;
				break;
			}
			else if(x1>x2 && obstacle(P, x2, y2+1, x2, y2+i) == true && obstacle(P, x2, y2+i, x1, y2+i) == true && obstacle(P, x1, y2+i, x1, y1-1) == true){
				if(off == 1){
					Line[0] = x2;
					Line[1] = y2;
					Line[2] = x2;
					Line[3] = y2+i;
					Line[4] = x1;
					Line[5] = y2+i;
					Line[6] = x1;
					Line[7] = y1;
				}
				else{}
				on = true;
				break;
			}
			else{}
		}
	}
	else if(y2-y1>1){
		for(int i = 1; i < y2-y1; i++){
			if(x1>x2 && obstacle(P, x2, y2-i, x2, y2-1) == true && obstacle(P, x2, y2-i, x1, y2-i) == true && obstacle(P, x1, y1+1, x1, y2-i) == true){
				if(off == 1){
					Line[0] = x2;
					Line[1] = y2;
					Line[2] = x2;
					Line[3] = y2-i;
					Line[4] = x1;
					Line[5] = y2-i;
					Line[6] = x1;
					Line[7] = y1;
				}
				else{}
				on = true;
				break;
			}
			else if(x1<x2 && obstacle(P,x1, y1+1, x1, y1+i) == true && obstacle(P, x1, y1+i, x2, y1+i) == true && obstacle(P, x2, y1+i, x2, y2-1) == true){
				if(off == 1){
					Line[0] = x1;
					Line[1] = y1;
					Line[2] = x1;
					Line[3] = y1+i;
					Line[4] = x2;
					Line[5] = y1+i;
					Line[6] = x1;
					Line[7] = y1;
				}
				else{}
				on = true;
				break;
			}
		}
	}
		return on;
	}
	public boolean obstacle(CardPanel P, int x1, int y1, int x2, int y2){//if the line have obstacle = false 
		boolean ON = true;
		if(x1 == x2 && y1 == y2){
			if(P.card.getCardSeat()[x1][y1].getIcon() != null){
				ON = false;
			}
			else{}
		}
		else if(x1 == x2 && y1<y2){
			for(int i = 0; i < y2-y1+1; i++){
				if(P.card.getCardSeat()[x1][y1+i].getIcon() != null){
					ON = false;
					break;
				}
				else{}
			}
		}
		else if(x1 == x2 && y1>y2){
			for(int i = 0; i < y1-y2+1; i++){
				if(P.card.getCardSeat()[x2][y2+i].getIcon() != null){
					ON = false;
					break;
				}
				else{}
			}
		}
		else if(y1 == y2 && x1<x2){
			for(int i = 0; i < x2-x1+1; i++){
				if(P.card.getCardSeat()[x1+i][y1].getIcon() != null){
					ON = false;
					break;
				}
			}
		}
		else if(y1 == y2 && x1>x2){
			for(int i = 0; i < x1-x2+1; i++){
				if(P.card.getCardSeat()[x2+i][y1].getIcon() != null){
					ON = false;
					break;
				}
			}
		}
		
		else{}
		return ON;
	}
	public boolean CCTwo(CardPanel P){//if there is still have answer = true
		boolean OFF = false;
		for(int i = 0; i < 13; i++){
			for(int j = 0; j < 21; j++){
				if(P.card.getCardSeat()[i][j] != null){
					for(int k = i; k<13; k++){
						for(int l = j+1; l<21; l++){
							if(P.card.getCardSeat()[i][j].getIcon() == P.card.getCardSeat()[k][l].getIcon()){
								if(Line5(P,0,i,j,k,l) || Line1(P,0,i,j,k,l) || Line2(P,0,i,j,k,l) || Line3(P,0,i,j,k,l) || Line4(P,0,i,j,k,l) || Line6(P,0,i,j,k,l) || Line7(P,0,i,j,k,l)){
									i = 13;
									j = 21;
									l = 21;
									k = 13;
									OFF = true;
								}
								else{}
							}
							else{}
						}
					}
				}
			}
		}
		return OFF;
	}
	public void resetCard(CardPanel P){//reset card
		Random random = new Random();
		int counter = 0;
		for(int i = 0; i<13; i++){
			for(int j = 0; j < 21; j++){
				if(P.card.getCardSeat()[i][j] != null){
					counter++;
				}
				else{}
			}
		}
		int[] array = new int[counter];
		for(int i = 0, k = 0; i<13; i++){
			for(int j = 0; j < 21; j++){
				if(P.card.getCardSeat()[i][j] != null){
					if(P.card.getCardSeat()[i][j].getIcon() == P.card.image[0]){
						array[k] = 0;
						k++;
					}
					else if(P.card.getCardSeat()[i][j].getIcon() == P.card.image[1]){
						array[k] = 1;
						k++;
					}
					else if(P.card.getCardSeat()[i][j].getIcon() == P.card.image[2]){
						array[k] = 2;
						k++;
					}
					else if(P.card.getCardSeat()[i][j].getIcon() == P.card.image[3]){
						array[k] = 3;
						k++;
					}
					else if(P.card.getCardSeat()[i][j].getIcon() == P.card.image[4]){
						array[k] = 4;
						k++;
					}
					else if(P.card.getCardSeat()[i][j].getIcon() == P.card.image[5]){
						array[k] = 5;
						k++;
					}
					else if(P.card.getCardSeat()[i][j].getIcon() == P.card.image[6]){
						array[k] = 6;
						k++;
					}
					else if(P.card.getCardSeat()[i][j].getIcon() == P.card.image[7]){
						array[k] = 7;
						k++;
					}
					else if(P.card.getCardSeat()[i][j].getIcon() == P.card.image[8]){
						array[k] = 8;
						k++;
					}
					else if(P.card.getCardSeat()[i][j].getIcon() == P.card.image[9]){
						array[k] = 9;
						k++;
					}
					else if(P.card.getCardSeat()[i][j].getIcon() == P.card.image[10]){
						array[k] = 10;
						k++;
					}
					else if(P.card.getCardSeat()[i][j].getIcon() == P.card.image[11]){
						array[k] = 11;
						k++;
					}
					else if(P.card.getCardSeat()[i][j].getIcon() == P.card.image[12]){
						array[k] = 12;
						k++;
					}
					else if(P.card.getCardSeat()[i][j].getIcon() == P.card.image[13]){
						array[k] = 13;
						k++;
					}
					else if(P.card.getCardSeat()[i][j].getIcon() == P.card.image[14]){
						array[k] = 14;
						k++;
					}
				}
				else{}
			}
		}
		for(int i = 0; i < 13; i++){//reset imageIcon
			for(int j = 0; j < 21; j++){
				if(P.card.getCardSeat()[i][j].getIcon() != null){
					int temp = random.nextInt(counter);
					if(array[temp] != 0){
						P.card.getCardSeat()[i][j].setIcon(P.card.image[array[temp]]);
						array[temp] = 0;
					}
				}
				else{}
			}
		}
		P.validate();
	}
	public void paint(Graphics g){
		super.paint(g);
		copyFromback(g);
	}
	public void copyFromback(Graphics g){
		if(g != null)
			g.drawImage(head, 0, 0, null);
	}
	class TimerListener implements ActionListener{//when timer run
		private CardPanel P;
		TimerListener(CardPanel panel){
			P = panel;
		}
		public void actionPerformed(ActionEvent e){
			clock.setCurrentTime();
			time.setText("" + clock.getHour() + ":" + clock.getMinute() + ":" + clock.getSecond());
			if(clock.getSecond() == 30){//if minute = 3, loss
				timer.stop();
				P1.removeAll();
				remove(P);
				L1.setForeground(Color.WHITE);
				L1.setBackground(Color.BLACK);
				P1.setLayout(new BorderLayout());
				L1.setText("你輸了！");
				Try.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent e){
				
					}
				});
				P1.add(L1, BorderLayout.CENTER);
				//P1.add(Try, BorderLayout.SOUTH);
				add(P1);
				validate();
			}
		}
	}
	class MouseListener extends MouseAdapter{//when click mouse
		private int row, col;
		private CardPanel P;
		MouseListener(CardPanel panel, int x, int y){
			row = x;
			col = y;
			P = panel;
		}
		public void mouseClicked(MouseEvent e){
			if(P.card.getCardSeat()[row][col].getIcon() == null){
				
			}
			else if(two == 0){
				P.card.getCardSeat()[row][col].setBorder(border);
				poly[0] = row;
				poly[1] = col;
				two = 1;
			}
			else if((two == 1 && (poly[0] != row ))|| (two == 1 && (poly[1] != col))){
				if(P.card.getCardSeat()[row][col].getIcon() != P.card.getCardSeat()[poly[0]][poly[1]].getIcon()){//if different
					P.card.getCardSeat()[poly[0]][poly[1]].setBorder(null);
					P.card.getCardSeat()[row][col].setBorder(border);
					poly[0] = row;
					poly[1] = col;
				}
				else if(P.card.getCardSeat()[row][col].getIcon() == P.card.getCardSeat()[poly[0]][poly[1]].getIcon()){//if same
					if(ConnectTwo(P, poly[0], poly[1], row, col) == true){
						two = 0;
						if(check(P) == true){
							timer.stop();
							clock.clearTime();
							time.setText("" + clock.getHour() + ":" + clock.getMinute() + ":" + clock.getSecond());
							win(P);
						}
						else if(CCTwo(P) == false){
							System.out.print("123");
							resetCard(P);
						}
						else{}
					}
					else if(ConnectTwo(P, poly[0], poly[1], row, col) == false){
						P.card.getCardSeat()[poly[0]][poly[1]].setBorder(null);
							P.card.getCardSeat()[row][col].setBorder(border);
							poly[0] = row;
							poly[1] = col;
					}
					else{}
				}
			}
			else{}
		}
	}
}

