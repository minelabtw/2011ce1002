package fp.s100502502;
public class Time {
	private int second, minute, hour;
	Time(){//initialize  
		second = 0;
		minute = 0;
		hour = 0;
	}
	public void clearTime(){//clear time
		second = 0;
		minute = 0;
		hour = 0;
	}
	public void setCurrentTime(){//set time
		second++;
		if(second == 60){
			second = 0;
			minute = minute + 1;
		}
		else if(minute == 60){
			minute = 0;
			hour = hour + 1;
		}
	}
	public int getSecond(){//get second
		return second;
	}
	public int getMinute(){//get minute
		return minute;
	}
	public int getHour(){//get hour
		return hour;
	}
}
