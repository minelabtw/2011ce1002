//�o�P??connect
package fp.s100502502;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Random;
public class CardPanel extends JPanel{
	Card card;
	CardPanel(int level){//initialize
		card = new Card(level);
		setBackground(Color.BLACK);
		card.setCardSeat();
		setLayout(new GridLayout(13, 21, 1, 1));
		addCard();
	}
	public void addCard(){//add label to panel
		for(int i = 0; i < 13 ; i++){
			for(int j = 0; j < 21; j++){
				add(card.getCardSeat()[i][j]);
			}
		}
	}
	
	public void paint2(Graphics g, int R,int x1, int y1, int x2, int y2){//paint two points
		super.paint(g);
		((Graphics2D)g).setStroke(new BasicStroke(3.0f));
		if(R == 0){
			g.setColor(Color.BLACK);
		}
		else
			g.setColor(Color.RED);
			g.drawLine(x1, y1, x2, y2);
	}
	public void paint3(Graphics g, int R, int x1, int y1, int x2, int y2, int x3, int y3){//paint three points
		super.paint(g);
		((Graphics2D)g).setStroke(new BasicStroke(3.0f));
		if(R == 0){
			g.setColor(Color.BLACK);
		}
		else
			g.setColor(Color.RED);
			g.drawLine(x1, y1, x2, y2);
			g.drawLine(x2, y2, x3, y3);
	}
	public void paint4(Graphics g, int R, int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4){//paint four points
		super.paint(g);
		((Graphics2D)g).setStroke(new BasicStroke(3.0f));
		if(R == 0){
			g.setColor(Color.BLACK);
		}
		else
			g.setColor(Color.RED);
		g.drawLine(x1, y1, x2, y2);
		g.drawLine(x2, y2, x3, y3);
		g.drawLine(x3, y3, x4, y4);
	}
}
class Card{
	JLabel[][] seatCard;
	private int level;
	protected int[] randomnum;
	protected ImageIcon[] image = {new ImageIcon("image/01.gif"), new ImageIcon("image/02.gif"),
			 new ImageIcon("image/03.gif"), new ImageIcon("image/04.gif"), new ImageIcon("image/05.gif")
			, new ImageIcon("image/06.gif"), new ImageIcon("image/07.gif"), new ImageIcon("image/08.gif")
			, new ImageIcon("image/09.gif"), new ImageIcon("image/10.gif"), new ImageIcon("image/11.gif")
			, new ImageIcon("image/12.gif"), new ImageIcon("image/13.gif"), new ImageIcon("image/15.gif")};
	Card(int le){
		level = le;
		seatCard = new JLabel[13][21];
		for(int i = 0; i < 13; i++){
			for(int j = 0; j < 21; j++){//initialize label
				seatCard[i][j] = new JLabel();
				seatCard[i][j].setOpaque(true);
				seatCard[i][j].setBackground(Color.BLACK);
			}
		}
	}
	public void setCardSeat(){//initialize cards
		boolean ON = true;
		int counter = 0;
		Random random;
		int[] con;
		switch(getLevel()){
			case 1:
				random = new Random();
				randomnum = new int[22];//Card's number
				con = new int[11];//save counter times
				while(ON){
					int temp = random.nextInt(11);
					if(con[temp] < 2){
						randomnum[counter++] = temp;
						con[temp]++;
					}
					else{}
					for(int i = 0, k = 0; i < 11; i++){
						if(con[i] == 2){
							k++;
							if(k == 11){
								ON = false;
							}
						}
					}
				}
				for(int i = 5, j = 0; i < 16; i++, j++){
					seatCard[4][i].setBackground(Color.PINK);
					seatCard[4][i].setIcon(image[randomnum[j]]);
					
				}
				for(int i = 5, k = 11; i < 16; i++, k++){
					seatCard[9][i].setBackground(Color.PINK);
					seatCard[9][i].setIcon(image[randomnum[k]]);
				}
				break;
			case 2:
				random = new Random();
				randomnum = new int[84];//Card's number
				con = new int[14];//counter times
				while(ON){
					int temp = random.nextInt(14);
					if(con[temp] < 6){
						randomnum[counter++] = temp;
						con[temp]++;
					}
					else{}
					for(int i = 0, k = 0; i < 14; i++){
						if(con[i] == 6){
							k++;
							if(k == 14){
								ON = false;
							}
						}
					}
				}
				for(int i = 2, k = 0; i < 6; i++){
					for(int j = 4; j < 9; j++, k++){
						seatCard[i][j].setBackground(Color.PINK);
						seatCard[i][j].setIcon(image[randomnum[k]]);
						
					}
				}
				for(int i = 2, l = 20; i < 6; i++){
					for(int j = 12; j < 17; j++, l++){
						seatCard[i][j].setBackground(Color.PINK);
						seatCard[i][j].setIcon(image[randomnum[l]]);
						
					}
				}
				for(int i = 8, k = 40; i < 12; i++){
					for(int j = 4; j < 9; j++, k++){
						seatCard[i][j].setBackground(Color.PINK);
						seatCard[i][j].setIcon(image[randomnum[k]]);
						
					}
				}
				for(int i = 8, l = 60; i < 12; i++){
					for(int j = 12; j < 17; j++, l++){
						seatCard[i][j].setBackground(Color.PINK);
						seatCard[i][j].setIcon(image[randomnum[l]]);
						
					}
				}
				seatCard[6][9].setBackground(Color.PINK);
				seatCard[6][9].setIcon(image[randomnum[80]]);
				seatCard[6][11].setBackground(Color.PINK);
				seatCard[6][11].setIcon(image[randomnum[81]]);
				seatCard[7][9].setBackground(Color.PINK);
				seatCard[7][9].setIcon(image[randomnum[82]]);
				seatCard[7][11].setBackground(Color.PINK);
				seatCard[7][11].setIcon(image[randomnum[83]]);
				break;
			case 3:
				random = new Random();
				randomnum = new int[12];//Card's number
				con = new int[6];//counter times
				while(ON){
					int temp = random.nextInt(6);
					if(con[temp] < 2){
						randomnum[counter++] = temp;
						//counter = counter + 1;
						con[temp]++;
					}
					else{}
					for(int i = 0, k = 0; i < 6; i++){
						if(con[i] == 2){
							k++;
							if(k == 6){
								ON = false;
							}
						}
					}
				}
				for(int i = 5, j = 0; i < 11; i++, j++){
					seatCard[4][i].setBackground(Color.PINK);
					seatCard[4][i].setIcon(image[randomnum[j]]);
				}
				for(int i = 5, k = 6; i < 11; i++, k++){
					seatCard[9][i].setBackground(Color.PINK);
					seatCard[9][i].setIcon(image[randomnum[k]]);
				}
			default:
				break;
		}
	}
	public JLabel[][] getCardSeat(){//get card seat
		return seatCard;
	}
	public int getLevel(){
		return level;
	}
}
