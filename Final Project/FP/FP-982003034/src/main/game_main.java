package main;

import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JFrame;

import client.display;
import client.display_cell;
import client.options;
import client.player_cell;
import server.server;
import database.game_db;

public class game_main {

	// game component
	game_db gdb;
	server gsvr;
	display gdsp;
	JFrame frame;
	JFrame sframe;
	ArrayList <display_cell> sdisp;
	JFrame pframe;
	ArrayList <display_cell> pdisp;
	
	// game initializer value
	int no_of_player;
	int no_of_display;
	String map_path;
	
	public static void main(String[] args) {
		game_main main = new game_main();

	}
	
	public game_main () {
		
		options.visibility_level = 0;
		no_of_player = 2;
		no_of_display = 1;
		map_path = "map/testmap";
		// take care of the ordering
		// database must be loaded first
		// then the display
		// then the server
		gdb = new game_db("database/");
		gsvr = new server (2,map_path);
		gdsp = gsvr.display;
		frame = new JFrame("game frame");
		frame.add(gdsp);
		frame.setVisible(true);
		frame.setSize(800, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// server frame, view what is visible to server
		sframe = new JFrame ("server frame");
		sframe.setLayout(new GridLayout (gsvr.worldmap.height, gsvr.worldmap.width));
		sdisp = new ArrayList <display_cell> ();
		for (int i = 0; i < gsvr.worldmap.area(); i++) {
			player_cell pc = new player_cell ();
			pc.t = gsvr.worldmap.get(i).t;
			pc.b = gsvr.worldmap.get(i).b;
			pc.u = gsvr.worldmap.get(i).u;
			pc.m = null;
			sdisp.add(new display_cell(pc));
			sframe.add(sdisp.get(i));
		}
		sframe.setSize(800, 800);
		sframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		sframe.setVisible(true);
		
		// player frame, view what is visible to player
		pframe = new JFrame ("player frame");
		pframe.setLayout(new GridLayout (gsvr.players.get(0).player_map.height, gsvr.players.get(0).player_map.width));
		pdisp = new ArrayList <display_cell> ();
		for (int i = 0; i < gsvr.players.get(0).player_map.area(); i++) {
			player_cell pc = new player_cell ();
			if (gsvr.players.get(0).player_map.get(i) != null) {
				pc.t = gsvr.players.get(0).player_map.get(i).t;
				System.out.println (pc.t);
				pc.b = gsvr.players.get(0).player_map.get(i).b;
				System.out.println (pc.b);
				pc.u = gsvr.players.get(0).player_map.get(i).u;
				System.out.println (pc.u);
				pc.m = null;
			}
			pdisp.add(new display_cell(pc));
			pframe.add(pdisp.get(i));
		}
		pframe.setSize(800, 800);
		pframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pframe.setVisible(true);
		
	}
	
	
}
