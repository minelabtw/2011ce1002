package main;

import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

import server.building;
import server.cell;
import server.map;
import server.map_properties;
import server.navcell;
import server.navmap;
import server.terrain;
import server.unit;
import utils.utils;

import client.display_cell;
import client.player;
import client.player_cell;

import database.building_data;
import database.game_db;
import database.terrain_data;
import database.unit_data;

public class main_test extends JPanel {

	game_db db;
	server.map map;
	ArrayList <player> players;
	ArrayList <display_cell> display;
	
	/*public static void main(String[] args) {
		
		JFrame frame = new JFrame ();
		main_test test = new main_test ();
		frame.add(test);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(700, 700);
		frame.setVisible(true);		
	}*/
	
	public main_test () {
		db  = new game_db ("database/");
		players = new ArrayList <player> ();
		players.add(new player ());
		players.add(new player ());
		map = worldmap_parser ("map/testmap");
		this.setLayout(new GridLayout (map.height, map.width));
		display = new ArrayList <display_cell> ();
		for (int i = 0; i < map.area(); i++) {
			player_cell pc = new player_cell ();
			pc.t = map.get(i).t;
			pc.b = map.get(i).b;
			pc.u = map.get(i).u;
			pc.m = null;
			display.add(new display_cell(pc));
			this.add(display.get(i));
		}
	}
	
	public map worldmap_parser (String path) {
		try {
			
			Scanner sc = new Scanner (new File (path));
			
			map_properties mp = new map_properties();

			mp.width = sc.nextInt();
			mp.height = sc.nextInt();
			mp.worldwidth = mp.width; // because this is the worldmap, worldwidth = width
			// because this is the worldmap, the map begins from (x,y) = (0,0)
			mp.worldx = 0;
			mp.worldy = 0;
			
			map temp = new map (mp);
			
			for (int i = 0; i < mp.area(); i++) {
				cell c = new cell ();
				// set terrain
				// see if it is a null terrain
				int next_terrain = sc.nextInt();
				if (next_terrain == 0) c.t = null;
				else {
					terrain t = new terrain();
					t.id = next_terrain;
					t.me = game_db.lookup_terrain (t.id);
					t.name = t.me.name;
					t.status = terrain_data.NORMAL;
					t.sight_cost = t.me.sight_cost;
					t.walking_cost = t.me.walking_cost;
					c.t = t;
				}				
				
				// set building
				// see if it is a null building
				int next_building = sc.nextInt();
				if (next_building == 0) c.b = null;
				else {
					building b = new building ();
					b.id = next_building;
					b.me = game_db.lookup_building(b.id);
					b.los = null; // TODO building los has not been implemented yet
					b.status = building_data.NORMAL;
					//b.co = players.get(sc.nextInt());
					
					c.b = b;
				}				
				// set unit
				// see if it is a null unit
				int next_unit = sc.nextInt();
				if (next_unit == 0) c.u = null;
				else {
					unit u = new unit ();
					u.id = next_unit;
					u.me = game_db.lookup_unit(u.id);
					int player = sc.nextInt();
					//u.co = players.get(player);
					//u.co.units.add(u);
					u.world = null;
					u.status = unit_data.IDLE;
					u.los = new navmap (u.me.sight, u.me.sight, utils.getx(i, mp.width), utils.gety(i, mp.width), mp.width);
					for (int j = 0; j < u.los.area(); j ++) u.los.navmap.add(new navcell());
					c.u = u;
				}
				
				temp.add(c);
				
			} // for
			return temp;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		
	}

}
