package server;

import java.util.ArrayList;

import utils.utils;

public class map extends map_properties {
	
	protected ArrayList<cell> map;
	
	public map (int w, int h, int wx, int wy, int ww) {
		super (w, h, wx, wy, ww);
		map = new ArrayList <cell>(area());
	}
	public map (map_properties mp) {
		super (mp);
		map = new ArrayList <cell>(area());
	}
	
	public cell get (int x, int y) {
		return map.get(y*width+x);
	} // get
	
	public cell get(int i) {
		return map.get(i);
	}
	
	public void set (int x, int y, cell c) {
		map.set(y*width+x, c);
	} // set

	public void set(int i, cell c) {
		map.set(i, c);
	}
	
	public void add (cell c) {
		map.add(c);
	}
	
	public int worldposof (cell c) {
		int localx = utils.getx(map.indexOf(c), width);
		int localy = utils.gety(map.indexOf(c), width);
		int world = (localy + worldy) * worldwidth + (localx + worldy); 
		return world;
	}
	
	public map_properties get_map_properties () {
		return new map_properties (width, height, worldx, worldy, worldwidth);
	}

}
