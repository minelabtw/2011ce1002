package server;

public class navcell extends cell {
	//public unit u;
	//public building b;
	//public terrain t;
	public int prevcell;

	public navcell () {
		
	}
	
	public navcell (cell c) {
		u = c.u;
		b = c.b;
		t = c.t;
		prevcell = -1;
	}

	public void set (cell c) {
		u = c.u;
		b = c.b;
		t = c.t;
		prevcell = -1;
	}
}
