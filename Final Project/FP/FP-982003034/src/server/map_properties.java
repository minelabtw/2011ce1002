package server;

public class map_properties {

	public int width;
	public int height;
	public int worldx;
	public int worldy;
	public int worldwidth;
	
	public map_properties (int w, int h, int wx, int wy, int ww) {
		width = w;
		height = h;
		worldx = wx;
		worldy = wy;
		worldwidth = ww;
	}
	
	public map_properties (map_properties mp) {
		this.width = mp.width;
		this.height = mp.height;
		this.worldx = mp.worldx;
		this.worldy = mp.worldy;
		this.worldwidth = mp.worldwidth;
	}
	
	public map_properties() {
		width = 0;
		height = 0;
		worldx = 0;
		worldy = 0;
		worldwidth = 0;
	}

	public int area () {
		return width*height;
	} // area
	
	// positional functions
	public int worldpos () {
		return worldy*worldwidth+worldx;
	}
	
	public int center () {
		return (worldy+height/2)*worldwidth+worldx+width/2;
	}
	
	public int centerx () {
		return worldx+width/2;
	}
	
	public int centery () {
		return worldy+height/2;
	}
	
	public void setpos (int wx, int wy) {
		worldx = wx;
		worldy = wy;
	}
	
}
