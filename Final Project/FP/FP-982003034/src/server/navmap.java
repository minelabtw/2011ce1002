package server;

import java.util.ArrayList;
import utils.utils;

public class navmap extends map_properties {

	public int worldx;
	public int worldy;
	public int worldwidth;
	public ArrayList <navcell> navmap;
	
	public navmap (int w, int h, int wx, int wy, int ww) {
		super (w, h, wx, wy, ww);
		navmap = new ArrayList <navcell>(area());
	}
	public navmap(map_properties mp) {
		super (mp);
		navmap = new ArrayList <navcell>(area());
	}
	
	// navigation cell functions
	public navcell get (int x, int y) {
		return navmap.get(y*width+x);
	} // get (x,y)
	
	public navcell get(int index) {
		return navmap.get(index);
	} // get (index)
	
	public void set (int x, int y, navcell nc) {
		navmap.set(y*width+x, nc);
	} // set (x,y)
	
	public void set(int index, navcell nc) {
		navmap.set(index, nc);
	} // set (index)
	
	public void set (int index, cell c) {
		navmap.get(index).u = c.u;
		navmap.get(index).b = c.b;
		navmap.get(index).t = c.t;
		navmap.get(index).prevcell = -1;
	}
	
	public int worldposof (navcell nc) {
		int localx = utils.getx(navmap.indexOf(nc), width);
		int localy = utils.gety(navmap.indexOf(nc), width);
		int world = (localy + worldy) * worldwidth + (localx + worldy); 
		return world;
	}

	
}
