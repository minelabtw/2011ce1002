package server;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

import client.display;
import client.player;
import client.player_cell;
import database.building_data;
import database.game_db;
import database.terrain_data;
import database.unit_data;

import utils.keyval_pair;
import utils.priority_queue;
import utils.utils;
import utils.vertex_key;

public class server {

	protected String map_name;
	public map worldmap;
	
	public ArrayList <player> players;
	public display display;
	private int turn;
	
	public server (int no_of_player, String selected_map) {
		
		display = new display (0,0,7,7);
		display.set_server(this);
		
		players = new ArrayList<player> ();
		for (int i = 0; i < no_of_player; i++) {
			players.add(new player (this, i, display));
		}
		
		map_name = selected_map;
		worldmap = worldmap_parser (map_name);
		// detect los for all unit after the worldmap has been loaded
		for (int i = 0; i < worldmap.area(); i++) {
			if (worldmap.get(i) != null) {
				if (worldmap.get(i).u != null) {
					worldmap.get(i).u.update_map(getlos (worldmap.get(i)));
					worldmap.get(i).u.co.units.add(worldmap.get(i).u);
				}
			}
		}
		for (int i = 0; i < players.size(); i++) {
			players.get(i).generate_map (worldmap.get_map_properties());
			players.get(i).update_map();
		}
		display.set_player(players.get(0));
		
		turn = 0;				
	}

	// worldmap parser
	// this is where things are created (new). in the rest of the game it will mostly be a reference to this
	public map worldmap_parser (String path) {
		try {
			
			Scanner sc = new Scanner (new File (path));
			map_properties mp = new map_properties();

			mp.width = sc.nextInt();
			mp.height = sc.nextInt();
			mp.worldwidth = mp.width; // because this is the worldmap, worldwidth = width
			// because this is the worldmap, the map begins from (x,y) = (0,0)
			mp.worldx = 0;
			mp.worldy = 0;
			
			map temp = new map (mp);
			
			for (int i = 0; i < mp.area(); i++) {
				cell c = new cell ();
				// set terrain
				// see if it is a null terrain
				int next_terrain = sc.nextInt();
				if (next_terrain == 0) c.t = null;
				else {
					terrain t = new terrain();
					t.id = next_terrain;
					t.me = game_db.lookup_terrain (t.id);
					t.name = t.me.name;
					t.status = terrain_data.NORMAL;
					t.sight_cost = t.me.sight_cost;
					t.walking_cost = t.me.walking_cost;
					c.t = t;
				}				
				
				// set building
				// see if it is a null building
				int next_building = sc.nextInt();
				if (next_building == 0) c.b = null;
				else {
					building b = new building ();
					b.id = next_building;
					b.me = game_db.lookup_building(b.id);
					b.los = null; // TODO building los has not been implemented yet
					b.status = building_data.NORMAL;
					b.co = players.get(sc.nextInt());
					
					c.b = b;
				}				
				// set unit
				// see if it is a null unit
				int next_unit = sc.nextInt();
				if (next_unit == 0) c.u = null;
				else {
					unit u = new unit ();
					u.id = next_unit;
					u.me = game_db.lookup_unit(u.id);
					int player = sc.nextInt();
					u.co = players.get(player);
					u.co.units.add(u);
					u.world = this;
					u.status = unit_data.IDLE;
					u.los = new navmap (u.me.sight, u.me.sight, utils.getx(i, mp.width), utils.gety(i, mp.width), mp.width);
					for (int j = 0; j < u.los.area(); j ++) u.los.navmap.add(new navcell());
					c.u = u;
				}
				
				temp.add(c);
				
			} // for
			return temp;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		
	}
	// get map properties
	public map_properties get_map_properties() {
		return worldmap.get_map_properties();
	}
	public int map_width() {
		return worldmap.width;
	}
	
	// function to check whether a move is legal or not
	public boolean check_move (cell src, cell tgt) {
		// 0. there must be a unit moving (of course !!!)
		// 1. a unit can only move once, so a unit that has moved cannot move again
		// 2. the target cell must be empty (no unit on it)
		// 3. the target cell must be in the unit's los
		// 4. the target cell must be walkable
		
		// there must be a unit
		// the unit must has not moved this turn
		// the target cell must have no unit on it first
		if (src.u != null & !src.u.hasmoved && tgt.u == null) {
			// the target cell must be inside source's los
			// ds = delta space
			int ds = worldmap.map.indexOf(tgt) - src.u.los.worldpos();
			if (ds >= 0 && ds < src.u.los.area()) {
				// the target cell must be walkable
				if (src.u.los.get(ds).prevcell > 0)
				return true;
			}
		} // if
		
		return false;
	} // check move

	// move unit from source cell to target index
	public void req_move(cell src, cell tgt) {
		
		// check for legal move
		if (check_move (src, tgt)) {
			// set the unit's to has moved
			src.u.hasmoved = true;
			// obtain the target cell position relative to los
			int tgti = worldmap.map.indexOf(tgt) - src.u.los.worldpos();
			// pick the target cell
			navcell temp = src.u.los.get(tgti);
			
			// create a path between target cell and source cell
			ArrayList <navcell> path = new ArrayList <navcell> ();
			// this is the characteristic of source cell, the cell in prevcell is equal to itself
			// when the path reaches source cell, the loop breaks
			while (temp != src.u.los.get(temp.prevcell)) {
				path.add(temp); // add the cell
				// redirect temp to the prevcell
				temp = src.u.los.get(temp.prevcell);
			}
			
			// create a move thread
			Thread move_thread = new Thread(new move(path, src));
			move_thread.run(); // move according to the path
			// wait until movement is finished
			try {move_thread.join();} catch (InterruptedException e) {}
		}
		
		// update unit los (now the unit is in tgt cell, not in src cell)
		tgt.u.update_map(getlos(tgt));
		//broadcast (tgt);
		
	} // move
	
	// update
	// the class meant to allow the display to update in certain allotted time
	private class move implements Runnable {

		ArrayList<navcell> path;
		cell src;
		
		public move (ArrayList<navcell> p, cell s) {
			path = p;
			src = s;
		}
		
		@Override
		public void run() {
			// move the unit
			// move from behind,
			// because path's order is from target to source,
			// while we need to walk from source to target
			cell current = src;
			cell next;
			for (int i = path.size()-1; i > -1; i--) {
				// calculate from local to world coordinate
				next = worldmap.map.get(src.u.los.worldposof(path.get(i)));
				// start updating, swap the two cell's unit
				next.u = current.u;
				current.u = null;
				// due to references, the cell will be automatically broadcasted
				// wait a few moment to limit movement speed
				try {this.wait(500);} catch (InterruptedException e) {}
			} // for
			
		} // run
	} // update
	
	public map getlos (cell tgt) {
		
		unit u = tgt.u;
		// world position (top left of los) = index of cell in world (center of los) - half of the los
		int worldx = utils.getx(worldmap.map.indexOf(tgt), worldmap.worldwidth) - u.me.sight/2;
		int worldy = utils.gety(worldmap.map.indexOf(tgt), worldmap.worldwidth) - u.me.sight/2;
		
		map temp = new map (u.me.sight , u.me.sight, worldx, worldy, worldmap.worldwidth);
		// get unit position and do line of sight determination
		// create a boolean array which indicates the cell has been walked before
		ArrayList <Boolean> walked = new ArrayList <Boolean> (temp.area());
		for (int i = 0; i < temp.width; i++) {
			for (int j = 0; j < temp.height; j++) {
				if (utils.to_index(worldx+i, worldy+j, worldmap.width) < 0) walked.add(true);
				else walked.add(false);
				temp.map.add(new cell()); // fill with empty cell first
			}			
		}
		
		// create the priority queue
		priority_queue <vertex_key> prim_pq = new priority_queue <vertex_key> ();
				
		// set other variables (position and weight)
		int weight = 0;
		// unit position on los
		int xpos = temp.width/2;
		int ypos = temp.height/2;
		int pos = temp.area()/2;
		// unit position on the world
		int xworld = temp.centerx();
		int yworld = temp.centery();
		int worldpos = temp.center();
		
		// set the unit position to walked
		walked.set(pos, true);
		// add the unit's own position to the map
		temp.map.set(pos,worldmap.get(worldpos));
		
		for (int i=0; i < temp.area(); i++) {
			// add the surrounding four neighbors
			// make sure the coordinates is not out of bound, then add
			if (ypos-1 > 0 && yworld-1 > 0) { // check for north boundary
				int north = utils.to_index (xpos, ypos-1, temp.width); // one cell up
				// check for walkability
				if (!walked.get(north)) {
					walked.set(north, true);
					// enqueue new element
					prim_pq.add(new vertex_key (pos, north), weight + worldmap.map.get(utils.to_index(xworld, yworld-1, worldmap.width)).t.sight_cost);
				}
			} // north
				
			if (ypos+1 < temp.width && yworld +1 < worldmap.height) { // check for south boundary
				int south = utils.to_index (xpos, ypos+1, temp.width); // one cell down
				if (!walked.get(south)) {
					walked.set(south, true);
					// enqueue new element
					prim_pq.add(new vertex_key (pos, south), weight + worldmap.map.get(utils.to_index(xworld, yworld+1, worldmap.width)).t.sight_cost);
				}
			}// south
			
			if (xpos+1 < temp.width && xworld+1 < worldmap.width) { // check for east boundary
				int east = utils.to_index (xpos+1, ypos, temp.width); // one cell right
				if (!walked.get(east)) {
					walked.set(east, true);
					// enqueue new element
					prim_pq.add(new vertex_key (pos, east), weight + worldmap.map.get(utils.to_index(xworld+1, yworld, worldmap.width)).t.sight_cost);
				}
			} // east
				
			if (xpos-1 > 0 && xworld-1 > 0) { // check for west boundary
				int west = utils.to_index (xpos-1, ypos, temp.width); // one cell left
				if (!walked.get(west)) {
					walked.set(west, true);
					// enqueue new element
					prim_pq.add(new vertex_key (pos, west), weight + worldmap.get(utils.to_index(xworld-1, yworld, worldmap.width)).t.sight_cost);
				}
			} // west
			
			// get the smallest one from the priority queue
			keyval_pair<vertex_key> next_step = new keyval_pair<vertex_key> (null, i);
			// check if the priority queue is empty
			if (prim_pq.is_empty()) break;
			next_step = prim_pq.pop();
			weight = next_step.val();
			// if the smallest available step is bigger than our unit's limit, stop the calculation
			if (weight > u.me.walk) break;
			// add the destination to map
			temp.map.set(next_step.key().dest(), worldmap.map.get(next_step.key().dest()));
			
			// setup for our new rounds of search
			xpos = utils.getx(next_step.key().dest(), temp.width);
			ypos = utils.gety(next_step.key().dest(), temp.width);
			pos = utils.to_index (xpos, ypos, temp.width);
			xworld = xpos + temp.worldx;
			yworld = xpos + temp.worldy;
			worldpos = utils.to_index (xworld, yworld, worldmap.width);
		} // for (sighting main algorithm)
		
		return temp;
		
	} // sighting

	// function to check whether an attack is legal or not
	public boolean check_attack (cell src, cell tgt) {
		// 0. there must be attacking and defending unit (of course!!!!)
		// 1. the attacker must not have do action
		// 2. the attacker must have ammo left
		// 3. the defender must be in range of the attacker
		// 4. the attacker can move and then attack
		
		// calculate target range
		int range = src.u.los.center() - tgt.u.los.center();
		if (range < 0) range *= -1;
		
		if (
				// check for unit's existence
				src.u != null && tgt.u != null
				// check for action
				&& src.u.hasaction == false
				// check for ammo
				&& src.u.ammo > 0
				// check for target range
				&& range <= src.u.me.max_att_range
				&& range >= src.u.me.min_att_range
			) {
			
			// check if the unit has moved
			if (src.u.hasmoved) {
				if (src.u.me.attack_after_move) return true;
				else return false; // else for attack after move
			} // if has moved
			else return true; // else for has moved
			
		} // if for other rules
		return false;
		
	} // check attack
	
	// attack unit from source cell to target cell
	public void req_attack (cell src, cell tgt) {
		if (check_attack (src,tgt)) {
			// check if the attacked unit is dead
			int stat = tgt.u.defend(game_db.damage_calc(src, tgt));
			if (stat <=0 ) tgt.u = null; // kill the unit
			// broadcast (tgt);
		}
	} // request attack
	
	
	// change turn
	public void change_turn () {
		turn ++;
		if (turn == players.size()) turn = 0;
		display.source = players.get(turn);
	} // change turn

	public void req_wait(cell tgt) {
		tgt.u.hasaction = true;
		tgt.u.hasmoved = true;
	}
	
} // server
