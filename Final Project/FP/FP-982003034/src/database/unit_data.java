package database;

public class unit_data extends object_data {
	
	// unit status
	public final static int IDLE = 0;
	public final static int READY = 1;
	public final static int UP = 2;
	public final static int DOWN = 3;
	public final static int RIGHT = 4;
	public final static int LEFT = 5;

	public int price;
	
	public int sight; // sight range
	
	public String propulsion;
	public int propulsion_id;
	public int walk; // walking range
	
	public String weapon_name; // name of the weapon
	public String at_name; // attack type name
	public int at; // attack type (refer to damage_table's index)
	public int ap; // attack point
	public int ammo;
	
	// attack properties
	public int min_att_range;
	public int max_att_range;
	public boolean attack_after_move;
	public boolean counterattack;

	public String armor_name; // name of the armor
	public String dt_name; // attack type name
	public int dt; // defense type (refer to damage_table's index)
	public int dp; // defense point
	
	public unit_data () {
		super ();
	}
	
}
