package database;

public class terrain_data extends object_data {

	public static final int NORMAL = 0;
	public static final int DESTROYED= 1;
	
	public int walking_cost;
	public int sight_cost;
	public int def_bonus;
	
	public terrain_data () {
		super ();
	}
	
}
