package database;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.imageio.ImageIO;


import server.building;
import server.cell;
import server.map;
import server.map_properties;
import server.terrain;
import server.unit;
import utils.priority_queue;
import utils.vertex_key;

// note on game database
// game object IDs = database index
public class game_db {

	// file directories
	public static String db_path;
	public final static String building_db_path = "buildings/building";
	public final static String unit_db_path = "units/unit";
	public final static String terrain_db_path = "terrains/terrain";
	public final static String damage_db_path = "damage_table";
	public final static String walking_db_path = "walking_table";
	public final static String mask_db_path = "masks/mask";
	// database
	public static ArrayList <terrain_data> terrain_db;
	public static ArrayList <building_data> building_db;
	public static ArrayList <unit_data> unit_db;
	public static damage_table damage_db;
	public static walking_table walking_db;
	public static ArrayList <mask_data> mask_db;
		
	// mask name
	public final static int los_mask = 1;
	
	public game_db (String database_main_path) {

		db_path = database_main_path;
		
		walking_db =  walking_parser (db_path + walking_db_path);
		damage_db =  damage_parser (db_path + damage_db_path);
		
		terrain_db = terrain_parser (db_path + terrain_db_path);
		building_db = building_parser (db_path + building_db_path);
		unit_db = unit_parser (db_path + unit_db_path);
		mask_db = mask_parser (db_path+mask_db_path);
	}

	// terrain lookup
	public static terrain_data lookup_terrain (int id) {
		return terrain_db.get(id);
	}
	public static BufferedImage terrain_img (int id, int status) {
		return terrain_db.get(id).sprite.get(status);
	}
	
	// building lookup
	public static building_data lookup_building (int id) {
		return building_db.get(id);
	}
	public static BufferedImage building_img(int id, int status) {
		return building_db.get(id).sprite.get(status);
	}
	
	// unit lookup
	public static unit_data lookup_unit (int id) {
		return unit_db.get(id);
	}
	
	public static int lookup_id (unit_data data) {
		return unit_db.indexOf(data);
	}
	public static BufferedImage unit_img (int id, int status) {
		return unit_db.get(id).sprite.get(status);
	}
	
	// mask lookup
	public static mask_data get_mask (int id) {
		return mask_db.get(id);
	}

	public static int damage_calc (cell att, cell def) {
		// damage = the attacker's attack point * damage multiplier in the damage database
		// find the damage by using damage database's index
		int result = (int)(att.u.me.ap * damage_table.get_rate(att.u.me.at, def.u.me.dt) - (def.u.me.dp + def.b.me.def_bonus + def.t.me.def_bonus));
		// to avoid "healing" the unit when attacking
		if (result < 0) return 0;
		else return result;
	}

	public static ArrayList <terrain_data> terrain_parser (String path) {
		
		try {
			Scanner sc = new Scanner (new File (path));
			ArrayList <terrain_data> temp = new ArrayList <terrain_data> ();
			int id = 1; // for id. id 0 is reserved for null (empty) terrain
			temp.add(null); // id 0 = null
			while (sc.hasNext()) {
				terrain_data data = new terrain_data ();
				// the file must follow this pattern
				// id
				data.id = id;
				// name
				data.name = sc.nextLine();
				// sight cost
				data.sight_cost = sc.nextInt();
				// walking cost
				data.walking_cost = sc.nextInt();
				// defense bonus
				data.def_bonus = sc.nextInt();
				// buffered images
				String sprite_path = sc.next();
				// normal
				BufferedImage next_sprite = load_sprite (db_path + terrain_db_path + sprite_path + "n.png");
				data.sprite.add (next_sprite);
				// destroyed
				//data.sprite.add (load_sprite (db_path + terrain_db_path + sprite_path + "d.png"));
				// add the data
				temp.add(data);
				id++;
			}
			return temp;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static ArrayList<building_data> building_parser(String path) {
		try {
			Scanner sc = new Scanner (new File (path));
			ArrayList <building_data> temp = new ArrayList <building_data> ();
			int id = 1; // id 0 is reserved for null (empty) building
			temp.add(null); // id 0 = null
			while (sc.hasNext()) {
				building_data data = new building_data ();
				// the file must follow this pattern
				// id
				data.id = id;
				// name
				data.name = sc.nextLine();
				// defense bonus
				data.def_bonus = sc.nextInt();
				// buffered images
				String sprite_path = sc.nextLine();
				// normal
				data.sprite.add (load_sprite (sprite_path + "n"));
				// destroyed
				data.sprite.add (load_sprite (sprite_path + "d"));
				// add the data
				temp.add(data);
				id++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static ArrayList<unit_data> unit_parser(String path) {
		try {
			Scanner sc = new Scanner (new File (path));
			ArrayList <unit_data> temp = new ArrayList <unit_data> ();
			int id = 1; // id 0 is reserved for null (empty) unit
			temp.add(null); // id 0 = null
			while (sc.hasNext()) {
				unit_data data = new unit_data ();
				// the file must follow this pattern
				// id
				data.id = id;
				// name
				data.name = sc.nextLine();
				// price
				data.price = sc.nextInt();
				// sight
				data.sight = sc.nextInt();
				// propulsion id
				data.propulsion_id = sc.nextInt();
				// propulsion name (taken from walking database)
				data.propulsion = walking_db.get_name (data.propulsion_id);
				// walking range
				data.walk = sc.nextInt();
				// weapon name
				data.weapon_name = sc.next();
				// attack id
				data.at = sc.nextInt();
				// attack name (taken from damage database, based from attack id)
				data.at_name = damage_db.get_name (data.at);
				// attack point
				data.ap = sc.nextInt();
				// max ammo
				data.ammo = sc.nextInt();
				// min attack range
				data.min_att_range = sc.nextInt();
				// max attack range
				data.max_att_range = sc.nextInt();
				// attack after move
				data.attack_after_move = to_boolean  (sc.nextInt());
				// counter attack
				data.counterattack = to_boolean (sc.nextInt());
				// armor name
				data.armor_name = sc.next();
				// defense id
				data.dt = sc.nextInt();
				// defense name (taken from damage database, based from defense id)
				data.dt_name = damage_db.get_name(data.dt);
				// defense point
				data.dp = sc.nextInt();
				// buffered images
				String sprite_path = sc.next();
				// idle
				data.sprite.add (load_sprite (db_path + unit_db_path + sprite_path + "i.png"));
				// add the data
				temp.add(data);
				id++;
			}
			return temp;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static ArrayList<mask_data> mask_parser(String path) {
		try {
			Scanner sc = new Scanner (new File (path));
			ArrayList <mask_data> temp = new ArrayList <mask_data> ();
			mask_data data = new mask_data ();
			int id = 0;
			while (sc.hasNext()) {
				data.id = id;
				data.name = sc.nextLine();
				String sprite_path = sc.nextLine();
				data.sprite.add(load_sprite (db_path + mask_db_path + sprite_path));
				temp.add(data);
				id++;
			}
			return temp;			
		} catch (Exception e) {

		}
		return null;
	}
	
	public static damage_table damage_parser(String path) {
		try {
			Scanner sc = new Scanner (new File (path));
			damage_table temp = new damage_table();
			// find the number of attack type
			int no_of_attack_type = sc.nextInt();
			// add the attack type
			for (int i = 0; i < no_of_attack_type; i++) {
				damage_table.attack_type.add(sc.next());
			}
			// find the number of defense type
			int no_of_defense_type = sc.nextInt();
			// add the defense type
			for (int i = 0; i < no_of_defense_type; i++) {
				damage_table.defense_type.add(sc.next());
			}
			// add the damage rate
			for (int i = 0; i < no_of_attack_type * no_of_defense_type; i++) {
				damage_table.damage_rate.add(sc.nextDouble());
			}
			return temp;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static walking_table walking_parser(String path) {
		try {
			Scanner sc = new Scanner (new File (path));
			walking_table temp = new walking_table();
			int no_of_propulsion = sc.nextInt();
			for (int i = 0; i < no_of_propulsion; i++) {
				walking_table.propulsion_type.add(sc.next());
			}
			return temp;			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static BufferedImage load_sprite (String sprite_path) {
		try {
			BufferedImage img;
			img = ImageIO.read(new File (sprite_path));
			return img;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static boolean to_boolean (int i) {
		if (i == 1) return true;
		else return false;
	}
	
}
