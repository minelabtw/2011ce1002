package database;

import java.util.ArrayList;

// if walking cost > 0 then the terrain is available for walking
// terrain id 0 is reserved for impassable terrain
// if walking cost = 0 it means the border of the maps (or used for other impassable terrain)
// if walking cost = -1 means it is unwalkable

public class walking_table {

	protected static ArrayList <String> propulsion_type;
	protected static ArrayList <Integer> walking_cost;
	
	public walking_table () {
		propulsion_type = new ArrayList <String> ();
		walking_cost = new ArrayList <Integer> ();
	}
	// set a cost
	// by name
	public static void set_cost (String pt, terrain_data tt, int r) {
				
		int pt_index = propulsion_type.indexOf(pt);
		int tt_index = game_db.terrain_db.indexOf(tt);
				
		// check if query is valid
		if (pt_index < 0) System.err.println ("Cannot found propulsion type: " + pt);
		else {
			if (tt_index < 0) System.err.println ("Cannot found terrain type: " + tt);
			else set_cost (pt_index, tt_index, r);
		} // if

	} // add cost by name

	// by index
	public static void set_cost(int pt_index, int tt_index, int r) {
		// the program is not responsible for overflow
		walking_cost.set(tt_index*propulsion_type.size()+ game_db.terrain_db.size(), r);
	}

	// get a cost
	// by name
	public static int get_cost (String pt, terrain_data tt) {
				
		int pt_index = propulsion_type.indexOf(pt);
		int tt_index =  game_db.terrain_db.indexOf(tt);
				
		// check if query is valid
		if (pt_index < 0) System.err.println ("Cannot found propulsion type: " + pt);
		else {
			if (tt_index < 0) System.err.println ("Cannot found terrain type: " + tt);
			else return get_cost (pt_index, tt_index);
		} // if
			
		return 0;
	} // by name
			
	// by index
	public static int get_cost(int pt_index, int tt_index) {
		if (tt_index == 0) return 0;
		return walking_cost.get(tt_index*propulsion_type.size()+ pt_index);
	} // by index

	public String get_name(int propulsion_id) {
		return propulsion_type.get(propulsion_id);
	}
		
}
