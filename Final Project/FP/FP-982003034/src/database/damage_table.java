package database;

import java.util.ArrayList;

import utils.utils;


// Damage Table ==========================================================
// damage table simply relates the name of an attack type, defense type,
// and how damage are reduced/increased when attacking / defending
// example:
// at = attack_type
// dt = defence_type
// r = damage_rate
/*
 *     | at0 | at1 | at2 | at3
 * dt0 | r00 | r01 | r02 | r03
 * dt1 | r10 | r11 | r12 | r13
 * dt2 | r20 | r21 | r22 | r23
 * dr3 | r30 | r31 | r32 | r33
 */
// please follow  add the rates in order when filling this database

public class damage_table {
	
	protected static ArrayList <String> attack_type;
	protected static ArrayList <String> defense_type;
	protected static ArrayList <Double> damage_rate;

	public damage_table () {
		attack_type = new ArrayList <String> ();
		defense_type = new ArrayList <String> ();
		damage_rate = new ArrayList <Double> ();
	}
	// set a rate
		// by name
		public void set_rate (String at, String dt, double r) {
				
			int at_index = attack_type.indexOf(at);
			int dt_index = defense_type.indexOf(dt);
				
			// check if query is valid
			if (at_index < 0) System.err.println ("Cannot found attack type: " + at);
			else {
				if (dt_index < 0) System.err.println ("Cannot found defense type: " + dt);
				else set_rate (at_index, dt_index, r);
			} // if
				
				
		} // add rate by name

		// by index
		public static void set_rate(int at_index, int dt_index, double r) {
			// the program is not responsible for overflow
			damage_rate.set(dt_index*attack_type.size()+defense_type.size(), r);
		}
		
		// get a rate
		// by name
		public static double get_rate (String at, String dt) {
			
			int at_index = attack_type.indexOf(at);
			int dt_index = defense_type.indexOf(dt);
			
			// check if query is valid
			if (at_index < 0) System.err.println ("Cannot found attack type: " + at);
			else {
				if (dt_index < 0) System.err.println ("Cannot found defense type: " + dt);
				else return get_rate (at_index, dt_index);
			} // if
				
			return 0;
		} // by name
			
		// by index
		public static double get_rate(int at_index, int dt_index) {
			return damage_rate.get(
				utils.to_index(
					at_index, dt_index, attack_type.size()
				) // to index
			); // damage_rate.get
		} // by index

		public String get_name(int at) {
			return attack_type.get(at);
		}
			
}