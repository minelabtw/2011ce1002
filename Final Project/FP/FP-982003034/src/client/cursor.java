package client;

import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import server.cell;

public class cursor {

	public int x;
	public int y;
	
	// for determining whether the cursor is still in the map border
	protected int width;
	protected int height;
	
	protected boolean lock;
	protected BufferedImage ready;
	protected BufferedImage active;
	
	// for actions
	protected player_cell src;
	protected player_cell tgt;
	
	public cursor (int nx, int ny, int w, int h) {
		x = nx;
		y = ny;
		width = w;
		height = h;
	}
	
	public void lock () {
		lock = true;
	}
	public void unlock () {
		lock = false;
	}
	// positional functions
	public int pos () {
		return y*width+x;
	}
	public void up () {
		if (!lock && y > 0) y--;
	}
	public void down () {
		if (!lock && y < height) y++;
	}
	public void right () {
		if (!lock && x < width) x++;
	}
	public void left () {
		if (!lock && x > 0) x--;
	}
	
	// for grabbing cells
	public void set_src (player_cell src) {
		this.src = src;
	}
	public cell get_src () {
		return src;
	}
	public void set_tgt (player_cell tgt) {
		this.tgt = tgt;
	}
	public cell get_tgt () {
		return tgt;
	}
	
	
}
