package client;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

import database.game_db;

import server.cell;

public class display_cell extends JPanel {
	
	public player_cell data;
	
	public boolean draw_terrain;
	public BufferedImage terrain;
	
	public boolean draw_building;
	public BufferedImage building;
	
	public boolean draw_unit;
	public BufferedImage unit;
	
	public boolean draw_mask;
	public BufferedImage mask;
	
	public BufferedImage blank_mask;
	
	public display_cell(player_cell dc) {
		data = dc;
		if (dc.u != null) unit = data.u.me.sprite.get(data.u.status);
		if (dc.b != null) building = data.b.me.sprite.get(data.b.status);
		if (dc.t != null) terrain = data.t.me.sprite.get(data.t.status);
		if (dc.m != null) mask = data.m.sprite;
		draw_terrain = true;
		draw_mask = true;
		draw_building = true;
		draw_unit = true;
		this.setSize(100,100);
		blank_mask = game_db.get_mask(0).sprite.get(0);
	}
	
	public display_cell() {
		data = null;
		draw_terrain = true;
		draw_mask = true;
		draw_building = true;
		draw_unit = true;
		this.setSize(100,100);
		blank_mask = game_db.get_mask(0).sprite.get(0);
	}

	public void update (player_cell dc) {
		data = dc;
		if (data.u != null) unit = data.u.me.sprite.get(data.u.status);
		if (data.b != null) building = data.b.me.sprite.get(data.b.status);
		if (data.t != null) terrain = data.t.me.sprite.get(data.t.status);
		if (data.m != null) mask = data.m.sprite;
	}
	
	@Override
	public void paintComponent (Graphics g) {
		// TODO
		super.paintComponent(g);
		// see if the data is empty
		if (data != null) {
			if (draw_terrain && terrain != null) {// if there is a terrain draw the terrain
				g.drawImage(terrain, 0, 0, null);
			}
			if (draw_mask && mask != null) {// if there is a mask draw the mask
				g.drawImage(mask, 0, 0, null);
			}
			if (draw_building && building != null) { // if there is a building draw the building
				g.drawImage(building, 0, 0, null);
			}
			if (draw_unit && unit != null) { // if there is a unit draw the unit
				g.drawImage(unit, 0, 0, null);
			}
		} // if data exist
		else { // if the data does not exist, draw a black cell
			g.drawImage(blank_mask, 0, 0, null);
		} // else
	} // paint component

}
