package client;

import java.awt.image.BufferedImage;

import database.mask_data;

public class mask {

	public int id;
	public mask_data me;
	public BufferedImage sprite;
	
	public mask () {
		
	}
	
	public mask(mask_data m) {
		me = m;
		id = me.id;
		sprite = me.sprite.get(0);
	}
}
