package client;

import java.util.ArrayList;

import server.building;
import server.cell;
import server.map_properties;
import server.navcell;
import server.server;
import server.unit;
import utils.utils;

public class player {

	protected server host;
	protected int playerno;
	
	public display display; 
	public player_map player_map;
	
	public ArrayList <unit> units;
	public ArrayList <building> buildings;
	
	public player (server s, int no, display d) {
		host = s;
		playerno = no;
		display = d;
		units = new ArrayList <unit> ();
		buildings = new ArrayList <building> ();
	}
	
	public player() {
		// TODO Auto-generated constructor stub
	}

	// get map properties
	public map_properties get_map_properties() {
		return player_map.get_map_properties();
	}
	public int map_width() {
		return player_map.width;
	}
	public void generate_map(map_properties mp) {
		player_map = new player_map (mp);
	}
	
	public void update_map () {
		
		switch (options.visibility_level) {
		
		case 0: // none visible
			clear_map ();
			poll_map ();
			blend_map ();
			break;
		case 1: // visible after exploration
			poll_map();
			blend_map ();
			break;
		case 2: // always visible
			for (int i = 0; i < player_map.area(); i++) {
				cell c = host.worldmap.get(i);
				player_cell p = new player_cell ();
				p.u = c.u;
				p.b = c.b;
				p.t = c.t;
				p.m = null;
				player_map.set(i,p);
			}
			break; // always visible
		} 
		
	} // update map

	public void clear_map() {
		for (int i = 0; i < player_map.area(); i ++) player_map.set(i,null);
	}
	
	// get the map from every unit and every building
	public void poll_map() {
		
		// get map from units
		for (int uc = 0; uc < units.size(); uc++) {
			unit u = units.get(uc);
			for (int mc = 0; mc < u.los.area(); mc++) {
				int wx = utils.getx(mc, u.los.width) + u.los.worldx; 
				int wy = utils.gety(mc, u.los.width) + u.los.worldy;
				if (utils.to_index(wx, wy, player_map.width) >= 0 && utils.to_index(wx, wy, player_map.width) < player_map.area()) {
					navcell nc = u.los.get(mc);
					player_cell pc = new player_cell ();
					pc.u = nc.u;
					pc.b = nc.b;
					pc.t = nc.t;
					pc.m = null;
					player_map.set(utils.to_index(wx, wy, player_map.width), pc);
				}

			} // for mc
		} // for uc
		
		// get map from buildings
		for (int bc = 0; bc < buildings.size(); bc++) {
			building b = buildings.get(bc);
			for (int mc = 0; mc < b.los.area(); mc++) {
				int wx = utils.getx(mc, b.los.width) + b.los.worldx; 
				int wy = utils.gety(mc, b.los.width) + b.los.worldy;
				if (utils.to_index(wx, wy, player_map.width) >= 0 && utils.to_index(wx, wy, player_map.width) < player_map.area()) {
					cell nc = b.los.get(mc);
					player_cell pc = new player_cell ();
					pc.u = nc.u;
					pc.b = nc.b;
					pc.t = nc.t;
					pc.m = null;
					player_map.set(utils.to_index(wx, wy, player_map.width), pc);
				}
			} // for mc
		} // for bc
		
	} // poll map

	// blend the visibility of all units and buildings
	// by reversing the process of polling map
	public void blend_map() {
		
		// blend map to units
		for (int uc = 0; uc < units.size(); uc++) {
			unit u = units.get(uc);
			for (int mc = 0; mc < u.los.area(); mc++) {
				int wx = utils.getx(mc, u.los.width) + u.los.worldx; 
				int wy = utils.gety(mc, u.los.width) + u.los.worldy;
				if (utils.to_index(wx, wy, player_map.width) >= 0 && utils.to_index(wx, wy, player_map.width) < player_map.area())
				u.los.set(mc, player_map.get(utils.to_index(wx, wy, player_map.width)));
			} // for mc
		} // for uc
		
		// blend map to buildings
		for (int bc = 0; bc < buildings.size(); bc++) {
			building b = buildings.get(bc);
			for (int mc = 0; mc < b.los.area(); mc++) {
				int wx = utils.getx(mc, b.los.width) + b.los.worldx; 
				int wy = utils.gety(mc, b.los.width) + b.los.worldy;
				if (utils.to_index(wx, wy, player_map.width) >= 0 && utils.to_index(wx, wy, player_map.width) < player_map.area())
				b.los.set(mc, player_map.get(utils.to_index(wx, wy, player_map.width)));
			} // for mc
		} // for bc
		
	}
	
} // player
