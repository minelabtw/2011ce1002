package client;

import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JPanel;

import server.server;

public class display extends JPanel implements KeyListener {

	public final int width;
	public final int height;
	public ArrayList <display_cell> display;
	public cursor cur;
	public action_menu menu;
	public player source;
	public server world;
	private Thread pacer;
	
	
	public display (int x, int y, int w, int h) {
		width = w;
		height = h;
		display = new ArrayList <display_cell> (area());
		for (int i = 0; i < area(); i++) display.add(new display_cell ());
		cur = new cursor (x,y,w,h);
		this.setLayout(new GridLayout (width,height));
		for (int i = 0; i< area(); i++) {
			this.add(display.get(i));
		}
		pacer = new Thread (new pacemaker(2));
		pacer.start();
		this.setFocusable(true);
		this.setFocusTraversalKeysEnabled(false);
		this.requestFocusInWindow();
		this.requestFocus();
	}
	
	public int area () {
		return width*height;
	}
	
	public void set_player (player p) {
		source = p;
	}
	public void set_server (server s) {
		world = s;
	}
	
	// a class used to control fps
	private class pacemaker implements Runnable {
		private int fps;
		public pacemaker (int nfps) {
			fps = nfps;
		}
		@Override
		public synchronized void run() {
			repaint();
			try {
				this.wait(1000/fps);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} // catch
		} // run
		
	} // pacemaker

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_Z: // ok
			// get the first cell
			if (cur.src == null) {
				cur.set_src(display.get(area()/2).data);
				// check if we selected a unit
				if (cur.src.u != null) source.player_map.draw_los(cur.src); // if there is a unit draw it's los
			}
			// if there is a cell already selected means we are selecting the second cell
			else {
				cur.set_tgt(display.get(area()/2).data);
				cur.lock();
				// TODO display an action menu
				this.add(menu = new action_menu(this));
			}
			break;
		case KeyEvent.VK_X: // cancel
			// undo the target cell selection
			if (cur.tgt != null) {
				cur.tgt = null;
				cur.unlock();
				// destroy any action menu
				this.remove(menu);
			}
			else if (cur.src != null) {
				cur.src = null;
			}
			// if both cell is display game menu
			
			break;
		case KeyEvent.VK_RIGHT: // go right
			cur.right();
			break;
		case KeyEvent.VK_LEFT: // go left
			cur.left();
			break;
		case KeyEvent.VK_UP: // go up
			cur.up();
			break;
		case KeyEvent.VK_DOWN: // go down
			cur.down();
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void paintComponent (Graphics g) {
		super.paintComponent(g);
		// copy content from player map
		// find top left position
		int x = cur.x - width/2;
		int y = cur.y - height/2;
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				// check for border
				if (x+i < 0
				|| y+j < 0
				|| x+i >= source.player_map.width
				|| y+j >= source.player_map.height
					) display.get(i).data = null;
				
				else display.get(j*width+i).update(source.player_map.get(x+i,y+j));
			} // for j
		} // for i
	}
	
	public void receive_action(String action) {
		if (action.compareTo("wait") == 0) {
			world.req_wait(cur.tgt);
		}
		else if (action.compareTo("move") == 0) {
			world.req_move(cur.src, cur.tgt);
		}
		else if (action.compareTo("attack") == 0) {
			world.req_attack(cur.src, cur.tgt);
		}
		else if (action.compareTo("nextturn") == 0) {
			world.change_turn();
		}
		menu = null;
		
	}
}
