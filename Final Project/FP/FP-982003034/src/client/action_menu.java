package client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;

import javax.swing.JPanel;

public class action_menu extends game_menu{

	protected final static int wait = 0;
	protected final static int move = 1;
	protected final static int attack = 2;
	
	protected display handler;
	
	public action_menu(display disp) {
		super(3);
		add_button ("Wait", "wait");
		add_button ("Move", "move");
		add_button ("Attack", "attack");
	}

	@Override
	protected void ok() {
		handler.receive_action(selection.get(cur.y).getActionCommand());		
	}



}
