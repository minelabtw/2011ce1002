package client;

import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;

public abstract class game_menu extends JPanel implements KeyListener {

	protected ArrayList <JButton> selection;
	protected int num_of_selection;
	protected cursor cur;
	
	public game_menu (int num_of_buttons) {
		num_of_selection = num_of_buttons;
		selection = new ArrayList <JButton> (num_of_selection);
		this.setLayout (new GridLayout (num_of_selection,1));
		this.setRequestFocusEnabled(true);
		this.setFocusable(true);
		cur = new cursor(0,0,1,num_of_selection);
	}
	
	public int add_button (String text, String command) {
		if (selection.size() == num_of_selection) return -1; // check if the list already full
		else {
			JButton temp = new JButton (text);
			temp.setActionCommand(command);
			selection.add(temp);
			this.add(temp);
			return selection.size();
		}
		
	} // add button

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_Z:
			ok();
			break;
		case KeyEvent.VK_UP: // go up
			cur.up();
			break;
		case KeyEvent.VK_DOWN: // go down
			cur.down();
			break;
		}
	}

	protected abstract void ok();

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	
}
