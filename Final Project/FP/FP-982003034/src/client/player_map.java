package client;

import java.util.ArrayList;

import database.game_db;

import server.cell;
import server.map_properties;
import utils.utils;

public class player_map extends map_properties {

	protected ArrayList <player_cell> map;
	
	public player_map (int w, int h, int wx, int wy, int ww) {
		super (w, h, wx, wy, ww);
		map = new ArrayList <player_cell>(area());
		for (int i = 0; i < area (); i++) {
			map.add(new player_cell());
		}
	}
	public player_map (map_properties mp) {
		super (mp);
		map = new ArrayList <player_cell>(area());
		for (int i = 0; i < area (); i++) {
			map.add(new player_cell());
		}
	}
	
	public player_cell get (int x, int y) {
		return map.get(y*width+x);
	} // get
	
	public player_cell get(int i) {
		return map.get(i);
	}
	
	public void set (int x, int y, player_cell c) {
		map.set(y*width+x, c);
	} // set

	public void set(int i, player_cell c) {
		map.set(i, c);
	}
	
	public int worldposof (player_cell c) {
		int localx = utils.getx(map.indexOf(c), width);
		int localy = utils.gety(map.indexOf(c), width);
		int world = (localy + worldy) * worldwidth + (localx + worldy); 
		return world;
	}
	
	public map_properties get_map_properties () {
		return new map_properties (width, height, worldx, worldy, worldwidth);
	}
	
	// function to draw los
	public void draw_los (player_cell c) {
		if (c != null && c.u != null) { // check if the cell and unit exist
			for (int i = 0; i < c.u.los.area(); i++) { // scan the los
				if (c.u.los.get(i) != null) { // check if the unit has seen something
					map.get(c.u.los.worldpos()+i).m = new mask (game_db.get_mask(game_db.los_mask));// set the mask
				} // for i
			} // if
		} // if c
	} // draw los
	
	public void undraw_los () {
		for (int i = 0; i < map.size(); i++) {
			map.get(i).m = null;
		}
	}

}
