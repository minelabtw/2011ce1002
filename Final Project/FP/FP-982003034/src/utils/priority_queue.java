package utils;

import java.util.ArrayList;

public class priority_queue <key_type> {

	protected ArrayList <keyval_pair<key_type>> queue;
	
	public priority_queue () {
		queue = new ArrayList <keyval_pair<key_type>> ();
	}
	
	// k = key
	// val = value
	public void add (key_type k, int val) {
		
		keyval_pair<key_type> kvp = new keyval_pair<key_type> (k,val);
		// the key already exist
		if (queue.contains(kvp)) {
			System.err.println ("The key " + k + " already exist!");
		}
		else {
			int index = find_index (val);
			queue.add(index,kvp);
		}
			
	} // add
	
	// pop
	public keyval_pair <key_type> pop () {
		return queue.remove(0);
	}
	
	public int find_index (int val) {
		
		if (queue.isEmpty()) return 0;
		
		int left = 0;
		int right = queue.size();
		int middle = (left + right) / 2;
			
		while (left < right) {
			if (val > queue.get(middle).val()) left = middle+1;
			else right = middle;
			middle = (left + right) / 2;
		}
		
		return middle;

	} // find_index
	
	public boolean is_empty() {
		return queue.isEmpty();
	}
	
} // priority_queue