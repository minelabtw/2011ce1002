package utils;

public class vertex_key {
	
	protected int source;
	protected int dest;
	
	public vertex_key (int s, int d) {
		source = s;
		dest = d;
	}
	
	public vertex_key () {
		
	}
	
	public void set_key (int s, int d) {
		source = s;
		dest = d;
	}
	
	public int source () {
		return source ;
	}
	
	public int dest () {
		return dest;
	}
	
}
