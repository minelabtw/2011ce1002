package utils;

public class keyval_pair <key_type> {
	
	protected key_type key;
	protected int val;

	public keyval_pair (key_type k, int v) {
		key = k;
		val = v;
	} // keyval_pair
	
	public key_type key () {
		return key;
	}
	
	public int val () {
		return val;
	}
	
}
