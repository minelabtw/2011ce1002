package utils;

public class utils {
	
	// function to convert 2d coordinates to linear array index
	public static int to_index (int x, int y, int width) {
		return y*width+x;
	}
	
	// function to convert linear array index to 2d coordinates
	public static int getx (int index, int width) {
		return index%width;
	}
	public static int gety (int index, int width) {
		return index/width;
	}
	
	public static int linear_search (Object[] array, int size, Object target) {
		
		for (int i = 0; i < size; i++) {
			if (array[i] == target) return i;
		}
		
		return -1;
	}
}
