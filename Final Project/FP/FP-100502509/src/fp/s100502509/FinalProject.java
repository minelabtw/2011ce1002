package fp.s100502509;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.sound.midi.Soundbank;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class FinalProject extends JFrame implements ActionListener {
	private Clip sound;
	private AudioInputStream audioInputStream;

	public GameInterface gameInterface = new GameInterface();// Create Object
	public DescriptionPanel descriptionpanel = new DescriptionPanel();
	private JFrame gFrame = new JFrame();
	private JFrame dFrame = new JFrame();
	JPanel panel1 = new JPanel() {
		public void paintComponent(Graphics g) {
			ImageIcon imageicon = new ImageIcon(getClass().getResource(
					"IMG_9267.JPG"));
			Image cover_Image = imageicon.getImage();

			/* Draw image on the panel */
			super.paintComponent(g);

			if (cover_Image != null)
				g.drawImage(cover_Image, 0, 0, getWidth(), getHeight(), this);
		}
	};
	JPanel panel2 = new JPanel();
	JLabel CoverPicture_Label = new JLabel();
	JButton Start_button = new JButton("");
	JButton Explain_button = new JButton("");
	JButton exit_button = new JButton("");

	JButton instruction_button = new JButton("Instruction");
	ImageIcon Cover_Image = new ImageIcon("image/card/IMG_9267.JPG");
	ImageIcon start_Image = new ImageIcon("image/card/Start.png");
	ImageIcon Exit_Image = new ImageIcon("image/card/Exit.png");
	ImageIcon explain_Image = new ImageIcon("image/card/Explain.png");

	Font font4 = new Font("serif", Font.BOLD, 50);

	int Start_times = 0; // 計算第幾次按下開始鍵

	public FinalProject() {
		InputStream in;

		try {
			in = new FileInputStream("sound/Quick.wav");
			audioInputStream = AudioSystem.getAudioInputStream(in);
			DataLine.Info info = new DataLine.Info(Clip.class,
					audioInputStream.getFormat());
			sound = (Clip) AudioSystem.getLine(info);
			sound.open(audioInputStream);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Start_button.setFont(font4);
		Start_button.setIcon(start_Image);
		exit_button.setFont(font4);
		exit_button.setIcon(Exit_Image);
		Explain_button.setFont(font4);
		Explain_button.setIcon(explain_Image);
		panel2.setLayout(new GridLayout(1, 3));
		CoverPicture_Label.setIcon(Cover_Image);
		panel1.setLayout(null);
		panel1.add(Start_button);
		panel1.add(Explain_button);
		panel1.add(exit_button);
		Start_button.setBounds(50, 425, 145, 145);
		Explain_button.setBounds(340, 425, 145, 145);
		exit_button.setBounds(600, 425, 145, 145);

		add(panel1, BorderLayout.CENTER);

		Start_button.addActionListener(this);
		Explain_button.addActionListener(this);
		exit_button.addActionListener(this);

		gFrame.add(gameInterface);// Create JFrame to show Panel GameInterface
		gFrame.setSize(800, 600);
		gFrame.pack();
		gFrame.setTitle("Memory!!");

		dFrame.add(descriptionpanel);// //Create JFrame to show Panel
										// DescriptionPanel
		dFrame.setSize(800, 600);
		dFrame.pack();
		dFrame.setTitle("遊戲說明");

	}

	public static void main(String[] args) {
		JFrame frame = new FinalProject();

		frame.setTitle("Memory Challenge");
		frame.setSize(800, 600);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);

	}

	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == Start_button) {
			sound.start();
			Start_times++;
			if (Start_times == 1) {// 第一次使用Start
				System.out.println("我進入了Start_times=" + Start_times);
				gameInterface.screen.setText("00:00:00");
				gameInterface.use1.resetCount();
				gFrame.add(gameInterface);
				gFrame.setSize(1000, 800);
				gFrame.pack();
				gFrame.setTitle("Memory!!");
				gFrame.setVisible(true);
			}

			else {
				System.out.println("我進入了i!=1");// 第二次按Start
				gFrame.setVisible(false);
				gFrame.getContentPane().removeAll();
				gameInterface = new GameInterface();
				gameInterface.screen.setText("00:00:00");
				gameInterface.use1.resetCount();
				gFrame.add(gameInterface);
				gFrame.validate();
				gFrame.setVisible(true);
			}

			gFrame.setVisible(true);
		}

		else if (e.getSource() == Explain_button) {
			dFrame.setVisible(true);
		}

		else {
			System.exit(0);
		}

	}

}
