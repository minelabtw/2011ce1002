package fp.s100502509;

import java.awt.BorderLayout;

import java.awt.Color;
import sun.audio.*; //import the sun.audio package
import java.io.*;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.sound.sampled.AudioInputStream;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class GameInterface extends JPanel {
	Timer t = new Timer();
	javax.swing.Timer t1 = new javax.swing.Timer(1000, new TimerListener());

	DescriptionPanel description__Panel = new DescriptionPanel();// Create the
																	// Object!!
	Users use1 = new Users();

	public int rand1, rand2, rand3, rand4, rand5;
	int retouch5 = 0, retouch6 = 0,//判斷是否重複按按鈕
			retouch7 = 0,
			retouch8 = 0,//
			retouch9 = 0, retouch10 = 0, retouch11 = 0, retouch12 = 0,
			retouch13 = 0, retouch14 = 0, retouch15 = 0, retouch16 = 0,
			retouch17 = 0, retouch18 = 0;
	int retouch19 = 0, retouch20 = 0;

	LineBorder bor = new LineBorder(Color.BLACK, 3);
	
	String countTime = "00:00:00";
	String showTime = "恭喜您得到的成績是:";

	Font font1 = new Font("serif", Font.BOLD, 50);//
	Font countTime_fFont = new Font("serif", Font.BOLD, 30);

	JLabel picture1_JLabel = new JLabel("");// Declare 10 Labels to put the picture
	JLabel picture2__JLabel = new JLabel("");
	JLabel picture3__JLabel = new JLabel("");
	JLabel picture4__JLabel = new JLabel("");
	JLabel score_Label = new JLabel();
	JLabel score_Label2 = new JLabel();
	JLabel screen = new JLabel("00:00:00");

	JButton picture5_Button = new JButton("");;
	JButton picture6_Button = new JButton("");;
	JButton picture7_Button = new JButton("");;
	JButton picture8_Button = new JButton("");;
	JButton picture9_Button = new JButton("");;
	JButton picture10__Button = new JButton("");;
	JButton picture11__Button = new JButton("");;
	JButton picture12__Button = new JButton("");;
	JButton picture13__Button = new JButton("");;
	JButton picture14__Button = new JButton("");;
	JButton picture15__Button = new JButton("");;
	JButton picture16__Button = new JButton("");;
	JButton picture17__Button = new JButton("");;
	JButton picture18__Button = new JButton("");;
	JButton picture19__Button = new JButton("");;
	JButton picture20__Button = new JButton("");;

	JFrame Score_frame = new JFrame();

	ImageIcon poker1 = new ImageIcon("image/card/init_1.jpg");// read the cover
	ImageIcon Score_Image = new ImageIcon("image/card/score.jpg");// read the
																	// Score_Image
	JPanel RandomCard_Panel = new JPanel();// Create JPanel
	JPanel UserCard_Panel = new JPanel();
	JPanel Content_Panel = new JPanel();
	JPanel score_Panel = new JPanel() {//Draw score_Panel BackGround
		public void paintComponent(Graphics g) {
			/* create image icon to get image */
			ImageIcon imageicon = new ImageIcon(getClass().getResource(
					"score.jpg"));
			Image image = imageicon.getImage();

			/* Draw image on the panel */
			super.paintComponent(g);

			if (image != null)
				g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		}
	};

	JButton stop_Button = new JButton("Stop");// Create JButton
	JButton Menu_Button = new JButton("Menu");

	Color gameColor = new Color(255, 236, 100);// Create Color
	Color userCard_Panel_Color = new Color(241, 111, 244);
	Color randomCard_Panel_Color = new Color(118, 228, 237);
	Color Content_Panel_Color = new Color(252, 79, 69);

	private int account = 0;

	public GameInterface() {

		setBackground(gameColor);
		use1.setUserCard();
		use1.setRandom();
		screen.setFont(font1);
		rand1 = use1.storeNumber[0];
		rand2 = use1.storeNumber[1];
		rand3 = use1.storeNumber[2];
		rand4 = use1.storeNumber[3];

		UserCard_Panel.setLayout(new GridLayout(4, 4));
		UserCard_Panel.setBackground(userCard_Panel_Color);
		UserCard_Panel.setBorder(bor);
		RandomCard_Panel.setLayout(new GridLayout(4, 1));
		RandomCard_Panel.setBackground(randomCard_Panel_Color);
		Content_Panel.setLayout(new GridLayout(1, 1));
		Content_Panel.setBackground(Content_Panel_Color);

		Content_Panel.add(screen);

		ImageIcon RandomPic1 = new ImageIcon("image/card/" + rand1 + ".png");
		picture1_JLabel.setIcon(RandomPic1);
		picture1_JLabel.setBorder(bor);
		RandomCard_Panel.add(picture1_JLabel);

		ImageIcon RandomPic2 = new ImageIcon("image/card/" + rand2 + ".png");
		picture2__JLabel.setIcon(RandomPic2);
		picture2__JLabel.setBorder(bor);
		RandomCard_Panel.add(picture2__JLabel);

		ImageIcon RandomPic3 = new ImageIcon("image/card/" + rand3 + ".png");
		picture3__JLabel.setIcon(RandomPic3);
		picture3__JLabel.setBorder(bor);
		RandomCard_Panel.add(picture3__JLabel);

		ImageIcon RandomPic4 = new ImageIcon("image/card/" + rand4 + ".png");
		picture4__JLabel.setIcon(RandomPic4);
		picture4__JLabel.setBorder(bor);
		RandomCard_Panel.add(picture4__JLabel);

		picture5_Button = new JButton(poker1);
		picture5_Button.setBorder(bor);
		UserCard_Panel.add(picture5_Button);

		picture6_Button = new JButton(poker1);
		picture6_Button.setBorder(bor);
		UserCard_Panel.add(picture6_Button);

		picture7_Button = new JButton(poker1);
		picture7_Button.setBorder(bor);
		UserCard_Panel.add(picture7_Button);

		picture8_Button = new JButton(poker1);
		picture8_Button.setBorder(bor);
		UserCard_Panel.add(picture8_Button);

		picture9_Button = new JButton(poker1);
		picture9_Button.setBorder(bor);
		UserCard_Panel.add(picture9_Button);

		picture10__Button = new JButton(poker1);
		picture10__Button.setBorder(bor);
		UserCard_Panel.add(picture10__Button);

		picture11__Button = new JButton(poker1);
		picture11__Button.setBorder(bor);
		UserCard_Panel.add(picture11__Button);

		picture12__Button = new JButton(poker1);
		picture12__Button.setBorder(bor);
		UserCard_Panel.add(picture12__Button);

		picture13__Button = new JButton(poker1);
		picture13__Button.setBorder(bor);
		UserCard_Panel.add(picture13__Button);

		picture14__Button = new JButton(poker1);
		picture14__Button.setBorder(bor);
		UserCard_Panel.add(picture14__Button);

		picture15__Button = new JButton(poker1);
		picture15__Button.setBorder(bor);
		UserCard_Panel.add(picture15__Button);

		picture16__Button = new JButton(poker1);
		picture16__Button.setBorder(bor);
		UserCard_Panel.add(picture16__Button);

		picture17__Button = new JButton(poker1);
		picture17__Button.setBorder(bor);
		UserCard_Panel.add(picture17__Button);

		picture18__Button = new JButton(poker1);
		picture18__Button.setBorder(bor);
		UserCard_Panel.add(picture18__Button);

		picture19__Button = new JButton(poker1);
		picture19__Button.setBorder(bor);
		UserCard_Panel.add(picture19__Button);

		picture20__Button = new JButton(poker1);
		picture20__Button.setBorder(bor);
		UserCard_Panel.add(picture20__Button);

		add(RandomCard_Panel, BorderLayout.NORTH);
		add(UserCard_Panel, BorderLayout.CENTER);
		add(Content_Panel, BorderLayout.SOUTH);

		OpenCard open = new OpenCard();
		picture5_Button.addActionListener(open);
		picture6_Button.addActionListener(open);
		picture7_Button.addActionListener(open);
		picture8_Button.addActionListener(open);
		picture9_Button.addActionListener(open);
		picture10__Button.addActionListener(open);
		picture11__Button.addActionListener(open);
		picture12__Button.addActionListener(open);
		picture13__Button.addActionListener(open);
		picture14__Button.addActionListener(open);
		picture15__Button.addActionListener(open);
		picture16__Button.addActionListener(open);
		picture17__Button.addActionListener(open);
		picture18__Button.addActionListener(open);
		picture19__Button.addActionListener(open);
		picture20__Button.addActionListener(open);
		stop_Button.addActionListener(open);
		Menu_Button.addActionListener(open);
		t1.start();
	}

	class OpenCard implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			if (account == 4) {
				Score_frame.setTitle("Grades");
				Score_frame.pack();
				Score_frame.setSize(400, 300);
				score_Label.setText(countTime);
				score_Label.setFont(countTime_fFont);
				score_Label2.setText(showTime);
				score_Label2.setFont(countTime_fFont);
				score_Panel.setLayout(null);
				score_Label.setBounds(130, 140, 120, 100);
				score_Label2.setBounds(30, 80, 300, 60);
				score_Panel.add(score_Label);
				score_Panel.add(score_Label2);
				Score_frame.add(score_Panel);
				Score_frame.setVisible(true);
			}

			if (e.getSource() == picture5_Button) {
				retouch5++;
				int rand5 = use1.userNumber[0];
				ImageIcon RandomPic5 = new ImageIcon("image/card/" + rand5
						+ ".png");
				picture5_Button.setIcon(RandomPic5);
				TimerTask turnBackTask = new TimerTask() {
					public void run() {
						ImageIcon poker2 = new ImageIcon(
								"image/card/init_1.jpg");
						picture5_Button.setIcon(poker2);
					}
				};

				if (rand5 == rand1 || rand5 == rand2 || rand5 == rand3
						|| rand5 == rand4) {
					if (retouch5 == 1) {
						account++;
					}
					System.out.println("account=" + account);
					if (account == 4) {
						t1.stop();
					}
				}

				else {
					t.schedule(turnBackTask, 500);
				}
			}

			else if (e.getSource() == picture6_Button) {
				retouch6++;
				int rand6 = use1.userNumber[1];
				ImageIcon RandomPic6 = new ImageIcon("image/card/" + rand6
						+ ".png");
				picture6_Button.setIcon(RandomPic6);
				TimerTask turnBackTask = new TimerTask() {
					public void run() {
						ImageIcon poker2 = new ImageIcon(
								"image/card/init_1.jpg");
						picture6_Button.setIcon(poker2);
					}
				};

				if (rand6 == rand1 || rand6 == rand2 || rand6 == rand3
						|| rand6 == rand4) {
					if (retouch6 == 1) {
						account++;
					}
					System.out.println("account=" + account);
					if (account == 4) {
						t1.stop();
					}
				}

				else {
					t.schedule(turnBackTask, 500);
				}
			}

			else if (e.getSource() == picture7_Button) {
				retouch7++;
				int rand7 = use1.userNumber[2];
				ImageIcon RandomPic7 = new ImageIcon("image/card/" + rand7
						+ ".png");
				picture7_Button.setIcon(RandomPic7);
				TimerTask turnBackTask = new TimerTask() {
					public void run() {
						ImageIcon poker2 = new ImageIcon(
								"image/card/init_1.jpg");
						picture7_Button.setIcon(poker2);
					}
				};

				if (rand7 == rand1 || rand7 == rand2 || rand7 == rand3
						|| rand7 == rand4) {
					if (retouch7 == 1) {
						account++;
					}
					System.out.println("account=" + account);
					if (account == 4) {
						t1.stop();
					}
				}

				else {
					t.schedule(turnBackTask, 500);
				}
			}

			else if (e.getSource() == picture8_Button) {
				retouch8++;
				int rand8 = use1.userNumber[3];
				ImageIcon RandomPic8 = new ImageIcon("image/card/" + rand8
						+ ".png");
				picture8_Button.setIcon(RandomPic8);
				TimerTask turnBackTask = new TimerTask() {
					public void run() {
						ImageIcon poker2 = new ImageIcon(
								"image/card/init_1.jpg");
						picture8_Button.setIcon(poker2);
					}
				};

				if (rand8 == rand1 || rand8 == rand2 || rand8 == rand3
						|| rand8 == rand4) {
					if (retouch8 == 1) {
						account++;
					}
					System.out.println("account=" + account);
					if (account == 4) {
						t1.stop();
					}
				}

				else {
					t.schedule(turnBackTask, 500);
				}
			}

			else if (e.getSource() == picture9_Button) {
				retouch9++;
				int rand9 = use1.userNumber[4];
				ImageIcon RandomPic9 = new ImageIcon("image/card/" + rand9
						+ ".png");
				picture9_Button.setIcon(RandomPic9);
				TimerTask turnBackTask = new TimerTask() {
					public void run() {
						ImageIcon poker2 = new ImageIcon(
								"image/card/init_1.jpg");
						picture9_Button.setIcon(poker2);
					}
				};

				if (rand9 == rand1 || rand9 == rand2 || rand9 == rand3
						|| rand9 == rand4) {
					if (retouch9 == 1) {
						account++;
					}
					System.out.println("account=" + account);
					if (account == 4) {
						t1.stop();
					}
				}

				else {
					t.schedule(turnBackTask, 500);
				}
			}

			else if (e.getSource() == picture10__Button) {
				retouch10++;
				int rand10 = use1.userNumber[5];
				ImageIcon RandomPic10 = new ImageIcon("image/card/" + rand10
						+ ".png");
				picture10__Button.setIcon(RandomPic10);

				TimerTask turnBackTask = new TimerTask() {
					public void run() {
						ImageIcon poker2 = new ImageIcon(
								"image/card/init_1.jpg");
						picture10__Button.setIcon(poker2);
					}
				};

				if (rand10 == rand1 || rand10 == rand2 || rand10 == rand3
						|| rand10 == rand4) {
					if (retouch10 == 1) {
						account++;
					}
					System.out.println("account=" + account);
					if (account == 4) {
						t1.stop();
					}
				}

				else {
					t.schedule(turnBackTask, 500);
				}
			}

			else if (e.getSource() == picture11__Button) {
				retouch11++;
				int rand11 = use1.userNumber[6];
				ImageIcon RandomPic11 = new ImageIcon("image/card/" + rand11
						+ ".png");
				picture11__Button.setIcon(RandomPic11);

				TimerTask turnBackTask = new TimerTask() {
					public void run() {
						ImageIcon poker2 = new ImageIcon(
								"image/card/init_1.jpg");
						picture11__Button.setIcon(poker2);
					}
				};

				if (rand11 == rand1 || rand11 == rand2 || rand11 == rand3
						|| rand11 == rand4) {
					if (retouch11 == 1) {
						account++;
					}
					System.out.println("account=" + account);
					if (account == 4) {
						t1.stop();
					}
				}

				else {

					t.schedule(turnBackTask, 500);

				}
			}

			else if (e.getSource() == picture12__Button) {
				retouch12++;
				int rand12 = use1.userNumber[7];
				ImageIcon RandomPic12 = new ImageIcon("image/card/" + rand12
						+ ".png");
				picture12__Button.setIcon(RandomPic12);

				TimerTask turnBackTask = new TimerTask() {
					public void run() {
						ImageIcon poker2 = new ImageIcon(
								"image/card/init_1.jpg");
						picture12__Button.setIcon(poker2);
					}
				};

				if (rand12 == rand1 || rand12 == rand2 || rand12 == rand3
						|| rand12 == rand4) {
					if (retouch12 == 1) {
						account++;
					}
					System.out.println("account=" + account);
					if (account == 4) {
						t1.stop();
					}
				}

				else {
					t.schedule(turnBackTask, 500);
				}
			}

			else if (e.getSource() == picture13__Button) {
				retouch13++;
				int rand13 = use1.userNumber[8];
				ImageIcon RandomPic13 = new ImageIcon("image/card/" + rand13
						+ ".png");
				picture13__Button.setIcon(RandomPic13);

				TimerTask turnBackTask = new TimerTask() {
					public void run() {
						ImageIcon poker2 = new ImageIcon(
								"image/card/init_1.jpg");
						picture13__Button.setIcon(poker2);
					}
				};

				if (rand13 == rand1 || rand13 == rand2 || rand13 == rand3
						|| rand13 == rand4) {
					if (retouch13 == 1) {
						account++;
					}
					System.out.println("account=" + account);
					if (account == 4) {
						t1.stop();
					}
				}

				else {
					t.schedule(turnBackTask, 500);
				}
			}

			else if (e.getSource() == picture14__Button) {
				retouch14++;
				int rand14 = use1.userNumber[9];
				ImageIcon RandomPic14 = new ImageIcon("image/card/" + rand14
						+ ".png");
				picture14__Button.setIcon(RandomPic14);

				TimerTask turnBackTask = new TimerTask() {
					public void run() {
						ImageIcon poker2 = new ImageIcon(
								"image/card/init_1.jpg");
						picture14__Button.setIcon(poker2);
					}
				};

				if (rand14 == rand1 || rand14 == rand2 || rand14 == rand3
						|| rand14 == rand4) {
					if (retouch14 == 1) {
						account++;
					}
					System.out.println("account=" + account);
					if (account == 4) {
						t1.stop();
					}
				}

				else {
					t.schedule(turnBackTask, 500);
				}
			}

			else if (e.getSource() == picture15__Button) {
				retouch15++;
				int rand15 = use1.userNumber[10];
				ImageIcon RandomPic15 = new ImageIcon("image/card/" + rand15
						+ ".png");
				picture15__Button.setIcon(RandomPic15);

				TimerTask turnBackTask = new TimerTask() {
					public void run() {
						ImageIcon poker2 = new ImageIcon(
								"image/card/init_1.jpg");
						picture15__Button.setIcon(poker2);
					}
				};

				if (rand15 == rand1 || rand15 == rand2 || rand15 == rand3
						|| rand15 == rand4) {
					if (retouch15 == 1) {
						account++;
					}
					System.out.println("account=" + account);
					if (account == 4) {
						t1.stop();
					}
				}

				else {
					t.schedule(turnBackTask, 500);
				}
			}

			else if (e.getSource() == picture16__Button) {
				retouch16++;
				int rand16 = use1.userNumber[11];
				ImageIcon RandomPic16 = new ImageIcon("image/card/" + rand16
						+ ".png");
				picture16__Button.setIcon(RandomPic16);

				TimerTask turnBackTask = new TimerTask() {
					public void run() {
						ImageIcon poker2 = new ImageIcon(
								"image/card/init_1.jpg");
						picture16__Button.setIcon(poker2);
					}
				};

				if (rand16 == rand1 || rand16 == rand2 || rand16 == rand3
						|| rand16 == rand4) {
					if (retouch16 == 1) {
						account++;
					}
					System.out.println("account=" + account);
					if (account == 4) {
						t1.stop();
					}
				}

				else {
					t.schedule(turnBackTask, 500);
				}
			}

			else if (e.getSource() == picture17__Button) {
				retouch17++;
				int rand17 = use1.userNumber[12];
				ImageIcon RandomPic17 = new ImageIcon("image/card/" + rand17
						+ ".png");
				picture17__Button.setIcon(RandomPic17);

				TimerTask turnBackTask = new TimerTask() {
					public void run() {
						ImageIcon poker2 = new ImageIcon(
								"image/card/init_1.jpg");
						picture17__Button.setIcon(poker2);
					}
				};

				if (rand17 == rand1 || rand17 == rand2 || rand17 == rand3
						|| rand17 == rand4) {
					if (retouch17 == 1) {
						account++;
					}
					System.out.println("account=" + account);
					if (account == 4) {
						t1.stop();
					}
				}

				else {
					t.schedule(turnBackTask, 500);
				}
			}

			else if (e.getSource() == picture18__Button) {
				retouch18++;
				int rand18 = use1.userNumber[13];
				ImageIcon RandomPic18 = new ImageIcon("image/card/" + rand18
						+ ".png");
				picture18__Button.setIcon(RandomPic18);

				TimerTask turnBackTask = new TimerTask() {
					public void run() {
						ImageIcon poker2 = new ImageIcon(
								"image/card/init_1.jpg");
						picture18__Button.setIcon(poker2);
					}
				};

				if (rand18 == rand1 || rand18 == rand2 || rand18 == rand3
						|| rand18 == rand4) {
					if (retouch18 == 1) {
						account++;
					}
					System.out.println("account=" + account);
					if (account == 4) {
						t1.stop();
					}
				}

				else {
					t.schedule(turnBackTask, 500);
				}
			}

			else if (e.getSource() == picture19__Button) {
				retouch19++;
				int rand19 = use1.userNumber[14];
				ImageIcon RandomPic19 = new ImageIcon("image/card/" + rand19
						+ ".png");
				picture19__Button.setIcon(RandomPic19);

				TimerTask turnBackTask = new TimerTask() {
					public void run() {
						ImageIcon poker2 = new ImageIcon(
								"image/card/init_1.jpg");
						picture19__Button.setIcon(poker2);
					}
				};

				if (rand19 == rand1 || rand19 == rand2 || rand19 == rand3
						|| rand19 == rand4) {
					if (retouch19 == 1) {
						account++;
					}
					System.out.println("account=" + account);
					if (account == 4) {
						t1.stop();
					}
				}

				else {
					t.schedule(turnBackTask, 500);
				}
			}

			else if (e.getSource() == picture20__Button) {
				retouch20++;
				int rand20 = use1.userNumber[15];
				ImageIcon RandomPic20 = new ImageIcon("image/card/" + rand20
						+ ".png");
				picture20__Button.setIcon(RandomPic20);

				TimerTask turnBackTask = new TimerTask() {
					public void run() {
						ImageIcon poker2 = new ImageIcon(
								"image/card/init_1.jpg");
						picture20__Button.setIcon(poker2);
					}
				};

				if (rand20 == rand1 || rand20 == rand2 || rand20 == rand3
						|| rand20 == rand4) {
					if (retouch20 == 1) {
						account++;
					}
					System.out.println("account=" + account);
					if (account == 4) {
						t1.stop();
					}
				}

				else {
					t.schedule(turnBackTask, 500);
				}
			}
		}

	}

	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			use1.setCountTime();
			if (use1.getSec() < 10) {
				countTime = String.valueOf("0" + use1.getHour()) + ":" + "0"
						+ String.valueOf(use1.getMin()) + ":" + "0"
						+ String.valueOf(use1.getSec());
				screen.setText(countTime);
			} else {
				countTime = "0" + String.valueOf(use1.getHour()) + ":" + "0"
						+ String.valueOf(use1.getMin()) + ":"
						+ String.valueOf(use1.getSec());
				screen.setText(countTime);
			}

		}
	}
}
