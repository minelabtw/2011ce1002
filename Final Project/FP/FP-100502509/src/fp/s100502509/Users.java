package fp.s100502509;

public class Users {
	protected int hour;
	protected int min;
	protected int sec;

	int[] storeNumber = new int[4];
	int[] userNumber = new int[16];
	int[] sameCard = new int[4];

	public Users() {
	}

	public void setRandom() // produce number randomly!!
	{


		for (int i = 0; i < 4; i++) {
			sameCard[i] = (int) (Math.random() * 16);

			if (i == 0) {
				storeNumber[i] = userNumber[sameCard[i]];
			}

			else {
				for (int j = 0; j < i; j++) {
					if (sameCard[j] != sameCard[i]) {
						storeNumber[i] = userNumber[sameCard[i]];
					} else
						i--;
				}
			}

		}

	}

	public void setUserCard() // produce number randomly!!
	{

		for (int i = 0; i < 16; i++) {
			userNumber[i] = (int) (Math.random() * 21);
			for (int j = 0; j < i; j++) {
				if (i > 0 && userNumber[i] == userNumber[j]) {
					i -= 1;
					break;
				}
			}
		}

		// for (int i = 0; i < 16; i++) {
		// System.out.println("userNumber" + i + "=" + userNumber[i]);
		// }

	}

	public void setCountTime() {// Function to count time
		sec += 1;
		if (sec >= 60) {
			sec = 0;
			min += 1;
		}
		if (min >= 60) {
			min = 0;
			hour += 1;
		}
		if (hour >= 12) {
			hour = min = sec = 0;
		}
	}

	public void resetCount() {
		sec = min = hour = 0;
	}

	public int getHour() {
		return hour;
	}

	public int getMin() {
		return min;
	}

	public int getSec() {
		return sec;
	}

}
