package fp.s100502509;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;

import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class DescriptionPanel extends JPanel {
	JLabel descriJLabel = new JLabel();
	ImageIcon descriptionImage = new ImageIcon("image/card/Descrip.png");

	public DescriptionPanel() {
		descriJLabel.setIcon(descriptionImage);
		add(descriJLabel);

	}

}
