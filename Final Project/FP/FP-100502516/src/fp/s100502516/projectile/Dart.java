package fp.s100502516.projectile;

import java.util.LinkedList;

import javax.swing.ImageIcon;

import fp.s100502516.character.bloom.*;
import fp.s100502516.playerStatus.PlayerStatus;

public class Dart extends Projectile {
	public Dart(PlayerStatus player, double x, double y, int target, double angle, LinkedList<Bloom> bloom, int hp)
	{
		this.player = player;
		this.x = x;
		this.y = y;
		width = 40;
		height = 40;	
		this.angle = angle;
		this.target = target;		
		live = true;
		this.bloom = bloom;
		this.hp = hp;
		icon = new ImageIcon("character/projectile/dart.png");		
		
		//System.out.println("Dart: " + angle);	//TEST
	}	
	public synchronized void checkCollision()
	{
		for(int i = 0; i < bloom.size(); i++)
		{
			double tx = x + height / 2 * Math.cos(angle) - bloom.get(i).getX();
			double ty = y + height / 2 * Math.sin(angle) - bloom.get(i).getY();
			double distance = Math.sqrt(Math.pow(tx, 2) + Math.pow(ty, 2));
			
			if(distance <= bloom.get(i).getWidth() / 2)
			{
				bloom.get(i).bomb();
				
				if(!bloom.get(i).getLive())
				{
					bloom.remove(i);
					player.setMoney(player.getMoney() + 1);
					
					for(int j = i; j < bloom.size(); j++)
					{
						bloom.get(j).setOrder(bloom.get(j).getOrder() - 1);
					}
				}
				
				hp--;
				
				if(hp <= 0)				
					live = false;	
			}
		}
	}
}
