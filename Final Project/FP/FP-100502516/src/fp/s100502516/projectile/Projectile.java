package fp.s100502516.projectile;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;

import java.util.LinkedList;

import javax.swing.ImageIcon;

import fp.s100502516.character.bloom.Bloom;
import fp.s100502516.playerStatus.PlayerStatus;

public abstract class Projectile {
	protected PlayerStatus player;
	protected double x;
	protected double y;
	protected int width;
	protected int height;	
	protected int target;
	protected int hp;	
	protected double angle;
	protected boolean live;		
	protected ImageIcon icon;		
	protected LinkedList<Bloom> bloom;
		
	public void drawDart(Graphics2D g2d, ImageObserver ob)
	{
		BufferedImage bufImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);	//���୸��
		Graphics2D bufG = bufImage.createGraphics();
		bufG.drawImage(icon.getImage(), 0, 0, ob);
		BufferedImage filteredBufImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		AffineTransform transform = new AffineTransform();
		transform.rotate(angle, width / 2, height / 2);
		AffineTransformOp imageOp = new AffineTransformOp(transform, null);
		imageOp.filter(bufImage, filteredBufImage);
		bufImage = filteredBufImage;
				
		g2d.drawImage(bufImage, (int) x - width / 2, (int) y - height / 2, ob);		
	}	
	public void updateMove()
	{			
		x += Math.cos(angle) * 5;
		y += Math.sin(angle) * 5;
		
		//System.out.println("Darts: "  + angle);	//TEST
	}
	public void checkCollision()
	{
		
	}
	public void setLive(boolean live)
	{
		this.live = live;		
	}	
	public double getX()
	{
		return x;
	}
	public double getY()
	{
		return y;
	}
	public int getWidth()
	{
		return width;
	}
	public int getHeight()
	{
		return height;
	}
	public boolean getLive()
	{
		return live;
	}
}
