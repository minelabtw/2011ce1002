package fp.s100502516.playerStatus;

public class PlayerStatus {
	private int money;
	private int hp;
	
	public PlayerStatus(int money, int hp)
	{
		this.money = money;
		this.hp = hp;
	}
	public void setMoney(int money)
	{
		this.money = money;
	}
	public void setHp(int hp)
	{
		this.hp = hp;
	}
	public int getMoney()
	{
		return money;		
	}
	public int getHp()
	{
		return hp;
	}
}
