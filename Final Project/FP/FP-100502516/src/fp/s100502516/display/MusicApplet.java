package fp.s100502516.display;

import java.applet.Applet;
import java.applet.AudioClip;
import java.net.URL;

import javax.swing.*;

public class MusicApplet extends JApplet {	//����I�����
	private AudioClip audioClip;	
	
	public MusicApplet()
	{
		URL urlForAudio = getClass().getResource("music/track001.au");
		audioClip = Applet.newAudioClip(urlForAudio);
		audioClip.loop();
		start();
	}	
	public void start()
	{
		if(audioClip != null)
			audioClip.loop();
	}
	public void stop()
	{
		if(audioClip != null)
			audioClip.stop();
	}
}
