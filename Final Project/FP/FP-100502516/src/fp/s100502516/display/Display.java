package fp.s100502516.display;

import javax.swing.*;
import java.awt.event.*;

public class Display extends JFrame {
	private Timer timer = new Timer(16, new TimerListener());		//刷畫面的TIMER
	private TitlePanel titlePanel = new TitlePanel();	//用各種PANEL來進行遊戲階段的變更
	private GamingPanel gamingPanel = new GamingPanel();
	private GameOverPanel gameOverPanel = new GameOverPanel();
	private WinPanel winPanel = new WinPanel();
	private TitleKeyListener titleKey = new TitleKeyListener();	
	private GamingKeyListener gamingKey = new GamingKeyListener();
	private GameOverListener gameOverKey = new GameOverListener();
	//private MusicApplet music = new MusicApplet(); //因為音樂太大，所以我先註解掉這一行
	private boolean title = false;
	private boolean gameStart = false;	
	private boolean gameOver = false;
	private boolean win = false;
	
	public Display()
	{				
		addKeyListener(titleKey);
		this.requestFocusInWindow();
		add(titlePanel);		
		timer.start();
	}	
	private void setStage()
	{		
		if(title)
		{			
			if(win)
			{
				remove(winPanel);
				win = false;
			}
			else
				remove(gameOverPanel);				
			add(titlePanel);	
			removeKeyListener(gameOverKey);
			addKeyListener(titleKey);			
			title = false;
		}
		else if(gameStart)
		{					
			remove(titlePanel);				
			add(gamingPanel);	
			removeKeyListener(titleKey);
			addKeyListener(gamingKey);			
			gameStart = false;
		}
		else if(gameOver)
		{
			gamingPanel.reset();
			remove(gamingPanel);				
			add(gameOverPanel);	
			removeKeyListener(gamingKey);
			addKeyListener(gameOverKey);			
			gameOver = false;
			this.requestFocusInWindow();
			//System.out.println("enter game over");	//TEST
		}
		else if(win)
		{
			gamingPanel.reset();
			remove(gamingPanel);				
			add(winPanel);	
			removeKeyListener(gamingKey);
			addKeyListener(gameOverKey);				
			this.requestFocusInWindow();			
		}
		
		repaint();
	}
	
	private class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{			
			if(gamingPanel.checkGameOver())
			{
				if(gamingPanel.checkWin())
					win = true;
				else if(!gamingPanel.checkWin())
					gameOver = true;
				setStage();
			}
			
			repaint();
		}
	}
	
	private class TitleKeyListener implements KeyListener
	{
		public void keyPressed(KeyEvent e) 
		{		
			
		}	
		public void keyReleased(KeyEvent e) 
		{
						
		}	
		public void keyTyped(KeyEvent e)
		{		
			gameStart = true;
			setStage();
		}	
	}		
	private class GamingKeyListener implements KeyListener
	{
		public void keyPressed(KeyEvent e) 
		{		
			
		}	
		public void keyReleased(KeyEvent e) 
		{			
			
		}	
		public void keyTyped(KeyEvent e)
		{		
			/*win = true;	//測試階段切換用
			setStage();	*/			
		}	
	}	
	private class GameOverListener implements KeyListener
	{
		public void keyPressed(KeyEvent e) 
		{		
			
		}	
		public void keyReleased(KeyEvent e) 
		{			
			
		}	
		public void keyTyped(KeyEvent e)
		{		
			title = true;
			setStage();			
			//System.out.println("to title");	//TEST
		}	
	}
}
