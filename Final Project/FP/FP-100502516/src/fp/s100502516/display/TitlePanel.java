package fp.s100502516.display;

import javax.swing.*;
import java.awt.*;

public class TitlePanel extends JPanel {
	private ImageIcon icon = new ImageIcon("image/dance.gif");
	
	public TitlePanel()
	{		
		setSize(800, 600);				
	}
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);		
		
		g.setColor(Color.BLACK);
		g.setFont(new Font("Courier", Font.PLAIN, 60));
		g.drawString("猴子射氣球的說~", 160, 100);
		g.setFont(new Font("Courier", Font.PLAIN, 30));
		g.drawString("按任意鍵開始遊戲", 280, 500);
		
		g.drawImage(icon.getImage(), 400 - icon.getIconWidth() / 2, 300 - icon.getIconHeight() / 2, this);
	}
}
