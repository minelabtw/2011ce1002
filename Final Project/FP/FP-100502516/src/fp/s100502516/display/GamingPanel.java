package fp.s100502516.display;

import javax.swing.*;
import javax.swing.Timer;

import fp.s100502516.character.bloom.*;
import fp.s100502516.character.monkey.*;
import fp.s100502516.map.*;
import fp.s100502516.playerStatus.*;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

public class GamingPanel extends JPanel {
	private PlayerStatus player;	//紀錄腳色資訊
	private Screen screen;
	private Status status;		
	private boolean addFlag = false;		
	private boolean gameOver = false;
	private boolean	win = false;	
	private int rank = 1;
	
	public GamingPanel()
	{
		setSize(800, 600);
		
		try 
		{
			screen = new Screen();
		}
		catch (FileNotFoundException e) 
		{			
			e.printStackTrace();
		}
		status = new Status();
		player = new PlayerStatus(2000, 100);
		
		screen.setLocation(0, 0);
		status.setLocation(screen.getWidth(), 0);
		
		status.getJbtStart().addActionListener(new ButtonListener());
		status.getJbtNormalMonkey().addActionListener(new ButtonListener());
		status.getJbtSuperMonkey().addActionListener(new ButtonListener());		
		
		add(screen);
		add(status);	
	}
	public void reset()
	{
		rank = 1;
		gameOver = false;
		win = false;
		remove(screen);
		remove(status);
		
		try 
		{
			screen = new Screen();
		}
		catch (FileNotFoundException e) 
		{			
			e.printStackTrace();
		}
		status = new Status();
		player = new PlayerStatus(1000, 100);
		
		screen.setLocation(0, 0);
		status.setLocation(screen.getWidth(), 0);
		
		status.getJbtStart().addActionListener(new ButtonListener());
		status.getJbtNormalMonkey().addActionListener(new ButtonListener());
		status.getJbtSuperMonkey().addActionListener(new ButtonListener());		
		
		add(screen);
		add(status);	
	}
	public boolean checkGameOver()
	{
		return gameOver;
	}
	public boolean checkWin()
	{
		return win;
	}
	
	private class ButtonListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(e.getSource() == status.getJbtStart())
			{
				screen.stageStart();
			}
			else if(e.getSource() == status.getJbtNormalMonkey() && !addFlag)
			{				
				screen.setMonkeyType(1);
				screen.getNormalMonkey().add(new NormalMonkey(player));
				addFlag = true;
			}
			else if(e.getSource() == status.getJbtSuperMonkey() && !addFlag)
			{
				screen.setMonkeyType(2);
				screen.getSuperMonkey().add(new SuperMonkey(player));
				addFlag = true;				
			}			
		}
	}
	
	private class Screen extends JPanel
	{
		private int width = 600;
		private int height = 600;
		private GameMap map;		
		private boolean start = false;
		private int monkeyType = 0;				
		private int redBloomNum = 0;	//有關氣球的變數應該能用陣列取代，不過我現在沒時間重寫
		private int redBloomCount = 0;
		private int redBloomSpeed = 0;
		private int blueBloomNum = 0;
		private int blueBloomCount = 0;
		private int blueBloomSpeed = 0;
		private int yellowBloomNum = 0;
		private int yellowBloomCount = 0;
		private int yellowBloomSpeed = 0;
		private int pinkBloomNum = 0;
		private int pinkBloomCount = 0;
		private int pinkBloomSpeed = 0;
		private int nextNum = -2;		
		private Scanner input = null;
		private Timer bloomStartTimer = new Timer(250, new BloomStartListener());	//出發頻率	
		private Timer bloomUpdateTimer = new Timer(10, new BloomUpdateListener());	//移動速度更新頻率
		private LinkedList<Bloom> bloom = new LinkedList<Bloom>();
		private LinkedList<NormalMonkey> normalMonkey = new LinkedList<NormalMonkey>();
		private LinkedList<SuperMonkey> superMonkey = new LinkedList<SuperMonkey>();
		
		
		public Screen() throws FileNotFoundException
		{
			setSize(width, height);
			
			map = new GameMap("map001");				
			
			this.addMouseListener(new MonkeyAddListener());
			this.addMouseMotionListener(new MonkeyMoveListener());
		}
		public void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			Graphics2D g2d = (Graphics2D) g;			
					
			map.drawMap(g, this);
			
			for(int i = 0; i < bloom.size(); i++)
			{
				if(start && bloom.get(i).getLive())
					bloom.get(i).drawBloom(g2d, this);		
			}
			
			for(int i = 0; i < normalMonkey.size(); i++)
			{		
				normalMonkey.get(i).drawMonkey(g2d, this);
				normalMonkey.get(i).drawRange(g2d, this);	
				
				if(normalMonkey.get(i).getProjectile().size() > 0)
				{
					for(int j = 0; j < normalMonkey.get(i).getProjectile().size(); j++)
					{
						if(normalMonkey.get(i).getProjectile().get(j).getLive())
						{
							if(normalMonkey.get(i).getProjectile().get(j).getX() <= 0	//判斷是否超出畫面邊界
									|| normalMonkey.get(i).getProjectile().get(j).getX() >= width
									|| normalMonkey.get(i).getProjectile().get(j).getY() <= 0
									|| normalMonkey.get(i).getProjectile().get(j).getY() >= height)
								normalMonkey.get(i).getProjectile().get(j).setLive(false);
							else							
								normalMonkey.get(i).getProjectile().get(j).drawDart(g2d, this);
						}
					}
				}
			}	
			
			for(int i = 0; i < superMonkey.size(); i++)
			{		
				superMonkey.get(i).drawMonkey(g2d, this);
				superMonkey.get(i).drawRange(g2d, this);	
				
				if(superMonkey.get(i).getProjectile().size() > 0)
				{
					for(int j = 0; j < superMonkey.get(i).getProjectile().size(); j++)
					{
						if(superMonkey.get(i).getProjectile().get(j).getLive())
						{
							if(superMonkey.get(i).getProjectile().get(j).getX() <= 0
									|| superMonkey.get(i).getProjectile().get(j).getX() >= width
									|| superMonkey.get(i).getProjectile().get(j).getY() <= 0
									|| superMonkey.get(i).getProjectile().get(j).getY() >= height)
								superMonkey.get(i).getProjectile().get(j).setLive(false);
							else							
								superMonkey.get(i).getProjectile().get(j).drawDart(g2d, this);
						}
					}
				}
			}				
		}	
		public void setMonkeyType(int type)
		{
			monkeyType = type;
		}
		public LinkedList<NormalMonkey> getNormalMonkey()
		{
			return normalMonkey;
		}
		public LinkedList<SuperMonkey> getSuperMonkey()
		{
			return superMonkey;
		}
		public void stageStart()
		{
			start = true;
			bloomStartTimer.start();			
		}		
		public boolean checkMonkeyBorder(Monkey monkey)	//判斷不能放置猴子的地點
		{
			if(monkeyType == 1)
			{
				for(int i = 0; i < normalMonkey.size() - 1; i++)
				{
					if(Math.abs(monkey.getX() - normalMonkey.get(i).getX()) <= (monkey.getWidth() + normalMonkey.get(i).getWidth()) / 2
							&& Math.abs(monkey.getY() - normalMonkey.get(i).getY()) <= (monkey.getHeight() + normalMonkey.get(i).getHeight()) / 2)
						return false;
				}
				
				for(int i = 0; i < superMonkey.size(); i++)
				{
					if(Math.abs(monkey.getX() - superMonkey.get(i).getX()) <= (monkey.getWidth() + superMonkey.get(i).getWidth()) / 2
							&& Math.abs(monkey.getY() - superMonkey.get(i).getY()) <= (monkey.getHeight() + superMonkey.get(i).getHeight()) / 2)
						return false;
				}
			}
			else if(monkeyType == 2)
			{
				for(int i = 0; i < superMonkey.size() - 1; i++)
				{
					if(Math.abs(monkey.getX() - superMonkey.get(i).getX()) <= (monkey.getWidth() + superMonkey.get(i).getWidth()) / 2
							&& Math.abs(monkey.getY() - superMonkey.get(i).getY()) <= (monkey.getHeight() + superMonkey.get(i).getHeight()) / 2)
						return false;
				}
				
				for(int i = 0; i < normalMonkey.size(); i++)
				{
					if(Math.abs(monkey.getX() - normalMonkey.get(i).getX()) <= monkey.getWidth() + normalMonkey.get(i).getWidth()
							&& Math.abs(monkey.getY() - normalMonkey.get(i).getY()) <= monkey.getHeight() + normalMonkey.get(i).getHeight())
						return false;
				}
			}			
			
			return true;
		}
				
		private class BloomStartListener implements ActionListener
		{
			public void actionPerformed(ActionEvent e)
			{				
				if(nextNum == -2)
				{
					File file = new File("rank/" + map.getMap() + "/" + rank + ".txt");
					
					try 
					{
						input = new Scanner(file);
					} 
					catch (FileNotFoundException e1) 
					{						
						win = true;
						gameOver = true;
						bloomStartTimer.stop();
						bloomUpdateTimer.stop();
					}	
					
					if(!win && !gameOver)
					{
						String s = input.next();
						
						if(s.equals(new String("red")))
						{
							redBloomNum = input.nextInt();
							nextNum = input.nextInt();		
							redBloomSpeed = input.nextInt();
						}
						else if(s.equals(new String("blue")))
						{
							blueBloomNum = input.nextInt();
							nextNum = input.nextInt();		
							blueBloomSpeed = input.nextInt();
						}
						else if(s.equals(new String("yellow")))
						{
							yellowBloomNum = input.nextInt();
							nextNum = input.nextInt();		
							yellowBloomSpeed = input.nextInt();
						}
						else if(s.equals(new String("pink")))
						{
							pinkBloomNum = input.nextInt();
							nextNum = input.nextInt();		
							pinkBloomSpeed = input.nextInt();
						}
					}
				}		
				else if(nextNum == -1)
				{
					String s = input.next();
				
					if(s.equals(new String("red")))
					{
						redBloomNum = input.nextInt();
						nextNum = input.nextInt();		
						redBloomSpeed = input.nextInt();
					}
					else if(s.equals(new String("blue")))
					{
						blueBloomNum = input.nextInt();
						nextNum = input.nextInt();		
						blueBloomSpeed = input.nextInt();
					}
					else if(s.equals(new String("yellow")))
					{
						yellowBloomNum = input.nextInt();
						nextNum = input.nextInt();		
						yellowBloomSpeed = input.nextInt();
					}
					else if(s.equals(new String("pink")))
					{
						pinkBloomNum = input.nextInt();
						nextNum = input.nextInt();		
						pinkBloomSpeed = input.nextInt();
					}
				}
				
				if(!win && !gameOver)
				{
					synchronized(bloom)
					{
						if(redBloomNum != 0)
						{
							bloom.addLast(new Bloom(1, redBloomSpeed, map.getPx(), map.getPy(), bloom, bloom.size()));
							bloom.getLast().setLive(true);
							redBloomCount++;
							
							if(redBloomCount == nextNum)
								nextNum = -1;
							
							if(redBloomCount == redBloomNum)
								redBloomNum = 0;
						}
						
						if(blueBloomNum != 0)
						{
							bloom.addLast(new Bloom(2, blueBloomSpeed, map.getPx(), map.getPy(), bloom, bloom.size()));
							bloom.getLast().setLive(true);
							blueBloomCount++;
							
							if(blueBloomCount == nextNum)
								nextNum = -1;
							
							if(blueBloomCount == blueBloomNum)
								blueBloomNum = 0;
						}				
						
						if(yellowBloomNum != 0)
						{
							bloom.addLast(new Bloom(3, yellowBloomSpeed, map.getPx(), map.getPy(), bloom, bloom.size()));
							bloom.getLast().setLive(true);
							yellowBloomCount++;
							
							if(yellowBloomCount == nextNum)
								nextNum = -1;
							
							if(yellowBloomCount == yellowBloomNum)
								yellowBloomNum = 0;
						}				
						
						if(pinkBloomNum != 0)
						{
							bloom.addLast(new Bloom(4, pinkBloomSpeed, map.getPx(), map.getPy(), bloom, bloom.size()));
							bloom.getLast().setLive(true);
							pinkBloomCount++;
							
							if(pinkBloomCount == nextNum)
								nextNum = -1;
							
							if(pinkBloomCount == pinkBloomNum)
								pinkBloomNum = 0;
						}				
					}
					
					if(!bloomUpdateTimer.isRunning())
						bloomUpdateTimer.start();
					
					if(redBloomNum == 0 && blueBloomNum == 0 && yellowBloomNum == 0 && pinkBloomNum == 0 && nextNum == 0)
					{						
						bloomStartTimer.stop();
						redBloomCount = 0;
						blueBloomCount = 0;
						yellowBloomCount = 0;
						pinkBloomCount = 0;
						nextNum = -2;
						rank++;
					}
				}
			}
		}
		private class BloomUpdateListener implements ActionListener
		{
			public void actionPerformed(ActionEvent e)
			{
				if(bloom.size() == 0 && redBloomNum == 0 && blueBloomNum == 0  && yellowBloomNum == 0 && pinkBloomNum == 0 && nextNum == 0)		
				{
					start = false;
					bloomUpdateTimer.stop();
				}
				else
				{
					synchronized (bloom)
					{
						for(int i = 0; i < bloom.size(); i++)
						{
							if(bloom.get(i).getLive())
							{
								bloom.get(i).updateMove();
								
								if(!bloom.get(i).getLive())
								{
									bloom.remove(i);
									player.setHp(player.getHp() - 1);
									
									if(player.getHp() <= 0)
									{
										gameOver = true;
										bloomStartTimer.stop();
										bloomUpdateTimer.stop();
									}
									
									for(int j = i; j < bloom.size(); j++)
									{
										bloom.get(j).setOrder(bloom.get(j).getOrder() - 1);
									}
								}
							}					
						}
					}
				}
			}
		}
		private class MonkeyAddListener implements MouseListener
		{	
			public void mouseClicked(MouseEvent e)
			{			
				if(addFlag)
				{
					if(monkeyType == 1 && !normalMonkey.getLast().getLive() && player.getMoney() >= normalMonkey.getLast().getPrice() 
							&& map.checkRoadBorder(e.getX(), e.getY(), normalMonkey.getLast())
							&& checkMonkeyBorder(normalMonkey.getLast()))
					{
						monkeyType = 0;				
						normalMonkey.getLast().setLive(true);
						normalMonkey.getLast().setLabelCoordinate(e.getX(), e.getY());
						normalMonkey.getLast().setBloom(bloom);
						player.setMoney(player.getMoney() - normalMonkey.getLast().getPrice());								
						addFlag = false;						
					}
					else if(monkeyType == 2 && !superMonkey.getLast().getLive() && player.getMoney() >= superMonkey.getLast().getPrice() 
							&& map.checkRoadBorder(e.getX(), e.getY(), superMonkey.getLast())
							&& checkMonkeyBorder(superMonkey.getLast()))
					{
						monkeyType = 0;				
						superMonkey.getLast().setLive(true);
						superMonkey.getLast().setLabelCoordinate(e.getX(), e.getY());
						superMonkey.getLast().setBloom(bloom);
						player.setMoney(player.getMoney() - superMonkey.getLast().getPrice());								
						addFlag = false;						
					}
				}			
			}		
			public void mouseEntered(MouseEvent e) 
			{
				if(monkeyType == 1)
				{
					add(normalMonkey.getLast().getLabel());
					normalMonkey.getLast().setLabelCoordinate(e.getX(), e.getY());
				}
				else if(monkeyType == 2)
				{
					add(superMonkey.getLast().getLabel());
					superMonkey.getLast().setLabelCoordinate(e.getX(), e.getY());
				}
			}
			public void mouseExited(MouseEvent e) //取消設置猴子
			{
				if(monkeyType == 1)
				{
					remove(normalMonkey.getLast().getLabel());	
					normalMonkey.removeLast();
					monkeyType = 0;
					addFlag = false;
				}
				else if(monkeyType == 2)
				{
					remove(superMonkey.getLast().getLabel());	
					superMonkey.removeLast();
					monkeyType = 0;
					addFlag = false;
				}
			}		
			public void mousePressed(MouseEvent e) //超人猴攻擊
			{				
				if(superMonkey.size() > 0 && superMonkey.getLast().getLive() && start)
				{
					for(int i = 0; i < superMonkey.size(); i++)
					{
						if(superMonkey.get(i).getLive())
							superMonkey.get(i).attackTimerStart();
					}
				}
			}		
			public void mouseReleased(MouseEvent e) 
			{		
				if(superMonkey.size() > 0 && superMonkey.getLast().getLive())
				{
					for(int i = 0; i < superMonkey.size(); i++)
					{
						if(superMonkey.get(i).getLive())
							superMonkey.get(i).attackTimerStop();
					}
				}
			}		
		}
		private class MonkeyMoveListener implements MouseMotionListener
		{
			public void mouseDragged(MouseEvent e) 
			{			
				if(superMonkey.size() > 0 && superMonkey.getLast().getLive())
				{
					for(int i = 0; i < superMonkey.size(); i++)
					{
						superMonkey.get(i).setMouseEvent(e);
						superMonkey.get(i).checkAttack();
					}
				}
			}
			public void mouseMoved(MouseEvent e) 
			{				
				if(monkeyType == 1)
				{
					normalMonkey.getLast().setLabelCoordinate(e.getX(), e.getY());					
				}
				else if(monkeyType == 2)
				{
					superMonkey.getLast().setLabelCoordinate(e.getX(), e.getY());			
				}
				
				if(superMonkey.size() > 0 && superMonkey.getLast().getLive())
				{
					for(int i = 0; i < superMonkey.size(); i++)
					{
						superMonkey.get(i).setMouseEvent(e);
						superMonkey.get(i).checkAttack();
					}
				}
			}		
		}
	}
	private class Status extends JPanel
	{		
		private ImageIcon iconNormalMonkey = new ImageIcon("character/monkey/normalMonkey.png");
		private ImageIcon iconSuperMonkey = new ImageIcon("character/monkey/superMonkey.png");
		private JButton jbtStart = new JButton("START");
		private JButton jbtNormalMonkey = new JButton(iconNormalMonkey);
		private JButton jbtSuperMonkey = new JButton(iconSuperMonkey);		
		
		public Status()
		{
			setSize(200, 600);			
			
			jbtNormalMonkey.setSize(40, 40);
			jbtNormalMonkey.setLocation(60, 120);			
			add(jbtNormalMonkey);
			jbtSuperMonkey.setSize(40, 40);
			jbtSuperMonkey.setLocation(110, 120);
			add(jbtSuperMonkey);
			
			jbtStart.setSize(180, 150);
			jbtStart.setLocation(10, 420);
			jbtStart.setFont(new Font("Courier", Font.BOLD, 30));
			add(jbtStart);					
		}
		public JButton getJbtStart()
		{
			return jbtStart;
		}
		public JButton getJbtNormalMonkey()
		{
			return jbtNormalMonkey;
		}
		public JButton getJbtSuperMonkey()
		{
			return jbtSuperMonkey;
		}		
		public void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			
			setBackground(Color.BLUE);
			
			g.setColor(Color.WHITE);
			g.fillRect(10, 10, 180, 100);
			g.setColor(Color.BLACK);
			g.setFont(new Font("Courier", Font.PLAIN, 30));
			g.drawString("$ " + String.valueOf(player.getMoney()), 20, 50);
			g.drawString("HP: " + String.valueOf(player.getHp()), 20, 90);			
			g.setColor(Color.RED);
			g.drawString("Rank: " + String.valueOf(rank), 20, 200);
		}	
	}
}


