package fp.s100502516.character.monkey;

import javax.swing.*;

import fp.s100502516.playerStatus.PlayerStatus;
import fp.s100502516.projectile.*;

import java.awt.event.*;
import java.util.LinkedList;

public class NormalMonkey extends Monkey {	
	public NormalMonkey(PlayerStatus player)
	{		
		this.player = player;
		x = 0;
		y = 0;
		width = 40;
		height = 40;
		price = 200;
		range = 100;
		power = 1;
		checkDelay = 10;		
		angle = 0;
		target = -1;		
		live = false;
		projectile = new LinkedList<Projectile>();
		icon = new ImageIcon("character/monkey/normalMonkey.png");		
		imageLabel = new JLabel();		
		imageLabel.setSize(40, 40);		
		checkTimer = new Timer(checkDelay, new checkTimerListener());	
		attackTimer = new Timer(500, new attackTimerListener());
		projectileUpdateTimer = new Timer(1, new ProjectileTimerListener());
	}	
	public void attack()
	{
		if(projectile.size() == 0)
			projectileUpdateTimer.start();
				
		projectile.add(new Dart(player, x, y, target, angle, bloom, power));		
	}
	
	private class checkTimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{		
			checkAttack();				
		}
	}	
	private class attackTimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(target >= 0)			
				attack();
		}
	}
	private class ProjectileTimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(projectile.size() == 0)
				projectileUpdateTimer.stop();
			else
			{				
				for(int i = 0; i < projectile.size(); i++)
				{
					if(projectile.get(i).getLive())
					{
						projectile.get(i).updateMove();
						projectile.get(i).checkCollision();
					}
										
					if(!projectile.get(i).getLive())
						projectile.remove(i);					
				}
			}
		}
	}
}
