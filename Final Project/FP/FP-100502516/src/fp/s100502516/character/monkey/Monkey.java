package fp.s100502516.character.monkey;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.Timer;

import fp.s100502516.character.bloom.Bloom;
import fp.s100502516.playerStatus.PlayerStatus;
import fp.s100502516.projectile.*;

import java.util.*;

public abstract class Monkey {
	protected PlayerStatus player;
	protected int x;
	protected int y;
	protected int width;
	protected int height;
	protected int price;
	protected int range;
	protected int target;	
	protected int power;	
	protected double angle;
	protected boolean live;	
	protected int checkDelay;	
	protected ImageIcon icon;		
	protected JLabel imageLabel;	//原先是為了賣掉猴子的時候可以點擊用的，不過還沒實做出來
	protected Timer checkTimer;		//不是每隻猴子都會用到
	protected Timer attackTimer;
	protected Timer projectileUpdateTimer;	
	protected LinkedList<Bloom> bloom;
	protected LinkedList<Projectile> projectile;
	
	public void drawRange(Graphics2D g2d, ImageObserver ob)
	{
		g2d.setColor(Color.BLACK);
		g2d.drawOval(x - range, y - range, range * 2, range * 2);		
	}
	public void drawMonkey(Graphics2D g2d, ImageObserver ob)
	{		
		//圖片旋轉...這是從網路上現學現賣的
		BufferedImage bufImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D bufG = bufImage.createGraphics();
		bufG.drawImage(icon.getImage(), 0, 0, ob);
		BufferedImage filteredBufImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		AffineTransform transform = new AffineTransform();
		transform.rotate(angle, width / 2, height / 2);
		AffineTransformOp imageOp = new AffineTransformOp(transform, null);
		imageOp.filter(bufImage, filteredBufImage);
		bufImage = filteredBufImage;
				
		g2d.drawImage(bufImage, x - width / 2, y - height / 2, ob);
	}
	public void checkAttack()	//旋轉腳色使其對準攻擊目標，也就是離終點最近的氣球
	{			
		if(target >= 0 && bloom.size() > 0)
		{
			try
			{
				if(Math.sqrt(Math.pow(x - bloom.get(target).getX(), 2) + Math.pow(y - bloom.get(target).getY(), 2)) > range)
					target = -1;					
			}			
			catch(IndexOutOfBoundsException e)
			{
				target = -1;
			}			
		}
		else if(bloom.size() == 0)
		{
			target = -1;
		}
			
		if(target < 0 && bloom.size() > 0)	
		{
			for(int i = 0; i < bloom.size(); i++)
			{
				if(Math.sqrt(Math.pow(x - bloom.get(i).getX(), 2) + Math.pow(y - bloom.get(i).getY(), 2)) <= range)
				{
					target = i;												
					break;
				}
			}
		}		
		
		if(target >= 0 && bloom.size() > 0)
		{
			angle = Math.atan2(bloom.get(target).getY() - y, bloom.get(target).getX() - x);	
			//System.out.println("Monkey: " + angle);	//TEST
		}
		
		if(target >= 0 && !attackTimer.isRunning())
		{
			attackTimer.start();
			//System.out.println("attack timer start");	//TEST
		}
		else if(target < 0)
		{
			attackTimer.stop();		
			//System.out.println("attack timer stop");	//TEST
		}
	}
	public void attack()
	{
		
	}
	public void setBloom(LinkedList<Bloom> bloom)
	{
		this.bloom = bloom;
	}
	public void setX(int x)
	{
		this.x = x;
	}
	public void setY(int y)
	{
		this.y = y;
	}
	public void setLabelCoordinate(int mouseX, int mouseY)
	{
		x = mouseX;
		y = mouseY;
		imageLabel.setLocation(x - width / 2, y - height / 2);
	}
	public void setLive(boolean bool)
	{
		live = bool;
		
		if(live)
		{
			checkTimer.start();			
		}
		else
		{
			checkTimer.stop();			
		}
	}
	public int getX()
	{
		return x;
	}
	public int getY()
	{
		return y;
	}
	public boolean getLive()
	{
		return live;
	}
	public int getWidth()
	{
		return width;
	}
	public int getHeight()
	{
		return height;
	}
	public int getPrice()
	{
		return price;
	}
	public JLabel getLabel()
	{
		return imageLabel;
	}
	public LinkedList<Projectile> getProjectile()
	{
		return projectile;
	}
}
