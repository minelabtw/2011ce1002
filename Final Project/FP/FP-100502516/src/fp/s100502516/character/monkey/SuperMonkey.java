package fp.s100502516.character.monkey;

import javax.swing.*;

import fp.s100502516.playerStatus.PlayerStatus;
import fp.s100502516.projectile.*;

import java.awt.event.*;
import java.util.LinkedList;

public class SuperMonkey extends Monkey {	
	private MouseEvent e;
	
	public SuperMonkey(PlayerStatus player)
	{		
		this.player = player;		
		x = 0;
		y = 0;
		width = 40;
		height = 40;
		price = 500;
		range = 0;
		power = 2;
		checkDelay = 10;		
		angle = 0;		
		live = false;
		projectile = new LinkedList<Projectile>();
		icon = new ImageIcon("character/monkey/superMonkey.png");		
		imageLabel = new JLabel();		
		imageLabel.setSize(40, 40);				
		attackTimer = new Timer(250, new attackTimerListener());
		projectileUpdateTimer = new Timer(1, new ProjectileTimerListener());
	}	
	public void attack()
	{
		if(projectile.size() == 0)
			projectileUpdateTimer.start();
				
		projectile.add(new Dart(player, x, y, target, angle, bloom, power));		
	}
	public void checkAttack()//超級猴子的攻擊方式和其他的猴子比較不一樣，所以例外處理
	{
		angle = Math.atan2(e.getY() - y, e.getX() - x);	
	}
	public void setLive(boolean live)
	{
		this.live = live;
	}
	public void setMouseEvent(MouseEvent e)
	{
		this.e = e;
	}
	public void attackTimerStart()
	{
		attackTimer.start();
	}
	public void attackTimerStop()
	{
		attackTimer.stop();
	}	
	
	private class attackTimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			attack();
		}
	}
	private class ProjectileTimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(projectile.size() == 0)
				projectileUpdateTimer.stop();
			else
			{				
				for(int i = 0; i < projectile.size(); i++)
				{
					if(projectile.get(i).getLive())
					{
						projectile.get(i).updateMove();
						projectile.get(i).checkCollision();
					}
										
					if(!projectile.get(i).getLive())
						projectile.remove(i);					
				}
			}
		}
	}
}
