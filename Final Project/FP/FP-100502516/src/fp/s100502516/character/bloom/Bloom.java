package fp.s100502516.character.bloom;

import java.awt.*;
import java.util.*;
import java.awt.image.ImageObserver;

public class Bloom {	
	protected int hp;
	protected int width;
	protected int height;
	protected int radius;
	protected int x;
	protected int y;	
	protected int speed;	
	protected int order;	//����
	protected boolean live;
	protected LinkedList<Bloom> bloom;
	protected ArrayList<Integer> px;
	protected ArrayList<Integer> py;
	protected int nextNode;
	
	public Bloom(int hp, int speed, ArrayList<Integer> px, ArrayList<Integer> py, LinkedList<Bloom> bloom, int order)
	{
		this.hp = hp;		
		this.speed = speed;		
		width = 30;
		height = 30;
		radius = 15;		
		this.bloom = bloom;
		this.order = order;
		live = false;		
		this.px = px;
		this.py = py;
		x = px.get(0) - width / 2;
		y = py.get(0);
		nextNode = 1;		
	}		
	public void drawBloom(Graphics2D g2d, ImageObserver ob)
	{
		if(hp == 1)
			g2d.setColor(Color.RED);
		else if(hp == 2)
			g2d.setColor(Color.BLUE);
		else if(hp == 3)
			g2d.setColor(Color.YELLOW);
		else if(hp == 4)
			g2d.setColor(Color.PINK);
		
		g2d.fillOval(x - width / 2, y - height / 2, width, height);
	}	
	public void bomb()	//��}��y
	{
		hp--;
		
		if(hp == 3)
			speed = 6;
		else if(hp == 2)
			speed = 5;
		else if(hp == 1)
			speed = 3;
		else if(hp == 0)
			live = false;
	}
	public void updateMove()
	{
		boolean up = false;
		boolean down = false;
		boolean left = false;
		boolean right = false;
		
		if(px.get(nextNode) - x > 0)
		{
			right = true;
			
			x += speed;
			
			if(x >= px.get(nextNode))
				x = px.get(nextNode);
		}
		else if(px.get(nextNode) - x < 0)
		{
			left = true;
			
			x -= speed;
			
			if(x <= px.get(nextNode))
				x = px.get(nextNode);
		}
		
		if(py.get(nextNode) - y > 0)
		{
			down = true;
			
			y += speed;
			
			if(y >= py.get(nextNode))
				y = py.get(nextNode);
		}
		else if(py.get(nextNode) - y < 0)
		{
			up = true;
			
			y -= speed;
			
			if(y <= py.get(nextNode))
				y = py.get(nextNode);
		}
		
		synchronized(bloom)	//�L�C��y����
		{			
			if(order != 0 && (((x > bloom.get(order - 1).getX() && right) || (x < bloom.get(order - 1).getX() && left)) 
					&& ((y > bloom.get(order - 1).getY() && down) || (y < bloom.get(order - 1).getY() && up))))
			{
				bloom.add(order, bloom.remove(order - 1));
				bloom.get(order).setOrder(order);
				bloom.get(order - 1).setOrder(order - 1);
			}
		}
			
		
		if(px.get(nextNode) == x && py.get(nextNode) == y && nextNode != px.size() - 1)
			nextNode++;
		else if(px.get(nextNode) == x && py.get(nextNode) == y &&nextNode == px.size() - 1)		
			live = false;			
	}
	public void setLive(boolean live)
	{
		this.live = live;
	}	
	public void setOrder(int order)
	{
		this.order = order;
	}
	public int getOrder()
	{
		return order;
	}
	public Boolean getLive()
	{
		return live;
	}
	public int getX()
	{
		return x;
	}
	public int getY()
	{
		return y;
	}
	public int getWidth()
	{
		return width;
	}
	public int getHeight()
	{
	return height;	
	}	
}
