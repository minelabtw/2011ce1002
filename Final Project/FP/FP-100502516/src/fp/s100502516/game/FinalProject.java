package fp.s100502516.game;

import javax.swing.JFrame;

import fp.s100502516.display.*;

public class FinalProject {
	public static void main(String[] args)
	{
		Display display = new Display();
		display.setTitle("猴子射氣球的說~");
		display.setSize(800, 600);
		display.setResizable(false);
		display.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		display.setLocationRelativeTo(null);
		display.setVisible(true);			
	}
}
