package fp.s100502516.map;

import java.io.*;
import java.util.*;
import java.awt.*;
import java.awt.image.ImageObserver;

import javax.swing.*;

import fp.s100502516.character.monkey.Monkey;

public class GameMap {
	private ArrayList<Integer> px = new ArrayList<Integer>();
	private ArrayList<Integer> py = new ArrayList<Integer>();	
	private ImageIcon mapImageIcon;
	private Image mapImage;
	private Scanner input;	
	private int roadBorder = 20;	//路寬
	private String map;
	
	public GameMap(String map) throws FileNotFoundException
	{
		this.map = map;
		File file = new File("map/" + map + ".txt");
		input = new Scanner(file);		
		mapImageIcon = new ImageIcon("map/" + map + ".PNG");
		mapImage = mapImageIcon.getImage();
		setRoad();		
	}
	public void setRoad()
	{		
		String tempX = input.nextLine();
		String tempY = input.nextLine();
		String tempNum = "";
		
		for(int i = 0; i < tempX.length(); i++)
		{
			if(tempX.charAt(i) != ' ')
				tempNum += tempX.charAt(i);
			else
			{
				px.add(Integer.parseInt(tempNum));
				tempNum = "";
			}
			
			if(i == tempX.length() - 1)
			{
				px.add(Integer.parseInt(tempNum));
				tempNum = "";
			}
		}
		
		for(int i = 0; i < tempY.length(); i++)
		{
			if(tempY.charAt(i) != ' ')
				tempNum += tempY.charAt(i);
			else
			{
				py.add(Integer.parseInt(tempNum));
				tempNum = "";
			}
			
			if(i == tempY.length() - 1)
			{
				py.add(Integer.parseInt(tempNum));
				tempNum = "";
			}
		}		
	}	
	public void setMapFile(String map) throws FileNotFoundException
	{
		File file = new File("map/" + map + ".txt");
		input = new Scanner(file);	
		mapImageIcon = new ImageIcon("map/" + map + ".PNG");
		mapImage = mapImageIcon.getImage();
	}
	public void drawMap(Graphics g, ImageObserver observer)	//畫路線圖，也可以不用做這個
	{		
		g.drawImage(mapImage, 0, 0, observer);
		
		for(int i = 0; i < px.size() - 1; i++)
		{			
			g.setColor(Color.BLACK);			
			g.drawLine(px.get(i), py.get(i), px.get(i + 1), py.get(i + 1));
		}
	}
	public boolean checkRoadBorder(int mx, int my, Monkey monkey)	//猴子不能放路上
	{
		for(int i = 0; i < px.size() - 1; i++)
		{
			if(getDistance(mx, my, px.get(i), py.get(i), px.get(i + 1), py.get(i + 1)) <= roadBorder + monkey.getWidth() / 2
					&& ((mx >= Math.min(px.get(i), px.get(i + 1)) && mx <= Math.max(px.get(i), px.get(i + 1)))
					|| (my >= Math.min(py.get(i), py.get(i + 1)) && my <= Math.max(py.get(i), py.get(i + 1)))))
				return false;			
		}
		
		return true;
	}
	public double getDistance(int mx, int my, int x1, int y1, int x2, int y2)
	{
		int tx = x2 - x1;
		int ty = y2 - y1;
		
		return Math.abs((mx - x1) * ty - (my - y1) * tx) / Math.sqrt(Math.pow(ty, 2) + Math.pow(tx * -1, 2));
	}
	public String getMap()
	{
		return map;
	}
	public ArrayList<Integer> getPx()
	{
		return px;
	}
	public ArrayList<Integer> getPy()
	{
		return py;
	}
}
