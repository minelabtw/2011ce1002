package fp.s100502513;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class StartScreen extends JPanel {
	private ImageIcon backpic = new ImageIcon("pic/startbackgroung.png");
	private ImageIcon startpic = new ImageIcon("pic/start.png");
	private ImageIcon leavepic = new ImageIcon("pic/leave.png");
	protected JButton startbu = new JButton(startpic);
	private JButton leavebu = new JButton(leavepic);
	private JLabel backla = new JLabel(backpic);
	protected JPanel p = new JPanel();
	public StartScreen() {
		setLayout(null);  //任意排版
		startbu.setContentAreaFilled(false); // 不填空白
		startbu.setBorderPainted(false); // 不顯示邊框
		startbu.setBounds(650, 350, startpic.getIconWidth(), startpic.getIconHeight()); // 設置按鈕位置
		leavebu.setContentAreaFilled(false);
		leavebu.setBorderPainted(false);
		leavebu.setBounds(650, 570, leavepic.getIconWidth(), leavepic.getIconHeight());
		backla.setBounds(0, 0, backpic.getIconWidth(), backpic.getIconHeight());
		leavebu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(JFrame.EXIT_ON_CLOSE); // 離開
			}
		});
		add(startbu);
		add(leavebu);
		add(backla);
		setSize(backpic.getIconWidth(), backpic.getIconHeight()); // 把PANEL大小設定為背景圖片大小
		
	}

	public int getpanelwidth() {  //回傳此寬度
		return backpic.getIconWidth();
	}

	public int getpanelheight() {  //回傳此高度
		return backpic.getIconHeight();
	}
}
