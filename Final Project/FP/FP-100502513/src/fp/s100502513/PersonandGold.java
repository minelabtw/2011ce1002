package fp.s100502513;

import java.applet.AudioClip;
import java.awt.*;
import javax.swing.*;

import java.awt.RenderingHints.Key;
import java.awt.event.*;

public class PersonandGold extends GameEngine {
	private ImageIcon overpic = new ImageIcon("pic/over.jpg");
	private JLabel scorela = new JLabel();
	private JLabel people = new JLabel();
	private JLabel[] sg = new JLabel[5]; // 小金礦
	private JLabel[] mg = new JLabel[5]; // 中金礦
	private JLabel[] lg = new JLabel[5]; // 大金礦
	private JLabel clockla = new JLabel("剩餘時間: 秒");
	private JLabel eb = new JLabel();  //電力球
	private Timer pertimer = new Timer(15, new perTimeListener()); // 人物
	private Timer pawtimer = new Timer(15, new pawTimeListener()); // 爪子
	private Timer clocktimer = new Timer(1000, new clock()); // 爪子
	private double pawx; // 爪子座標
	private double pawy;
	private int degree = 90; // 人偏轉角度
	private int second = 30;
	private int state = 1;
	private final int small = 1;
	private final int medium = 2;
	private final int large = 3;
	private boolean change = false; // 是否迴轉=無
	
	public PersonandGold() {
		setLayout(null);
		eb.setIcon(new ImageIcon("pic/e.png")); // 電力球
		add(eb);
		clockla.setForeground(Color.RED); // 時鐘
		clockla.setFont(new Font("微軟正黑體", Font.BOLD, 40));
		clockla.setBounds(500, 0, 500, 50);
		add(clockla);
		setscorela(scorela); // 分數
		people.setIcon(new ImageIcon("pic/person/90.png")); // 人一開始的角度是90度
		add(people);
		setgold(sg, mg, lg);
		nextstates(state); // 設定金礦位置及add		
		settimer(pertimer,pertimer,clocktimer);
		addMouseListener(new mouse());
	}

	public void turn() { // 設定人物轉到特定角度迴轉
		if (degree <= 10) {
			change = true;
		} else if (degree >= 170) {
			change = false;
		}
	}

	public void setpaw() { // 球回原位
		pawtimer.stop();
		pawx = 890;
		pawy = 90;
	}

	class mouse extends MouseAdapter {
		public void mousePressed(MouseEvent e) {
			pertimer.stop(); // 人物停止
			pawtimer.start(); // 球向下抓0
		}
	}

	class perTimeListener implements ActionListener { // 人物移動，爪子移動
		public void actionPerformed(ActionEvent e) {
			people.setBounds(800, 14, 250, 200);
			pawx = 890 + 85 * Math.cos(((180.0 + degree) / 180) * Math.PI);
			pawy = 90 - 85 * Math.sin(((180.0 + degree) / 180) * Math.PI);
			eb.setBounds((int) pawx, (int) pawy, 50, 44);
			turn();
			if (change == true) {
				degree++;
			} else {
				degree--;
			}
			people.setIcon(new ImageIcon("pic/person/" + degree + ".png"));

		}
	}

	class pawTimeListener implements ActionListener { // 爪子抓取
		public void actionPerformed(ActionEvent e) {
			eb.setLocation((int) pawx, (int) pawy);
			pawx += 10 * Math.cos(((180.0 + degree) / 180) * Math.PI);
			pawy -= 10 * Math.sin(((180.0 + degree) / 180) * Math.PI);
			for (int i = 0; i < 5; i++) {
				if (Catch(getgoldxs(sg[i].getX(), small),
						getgoldys(sg[i].getY(), small), (int) pawx, (int) pawy)) { // 抓到小金礦
					setpaw();
					goldmove(sg[i], degree, 1);
					break;
				} else if (Catch(getgoldxs(mg[i].getX(), medium),
						getgoldys(mg[i].getY(), medium), (int) pawx, (int) pawy)) { // 抓到中金礦
					setpaw();
					goldmove(mg[i], degree, 2);
					break;
				} else if (Catch(getgoldxs(lg[i].getX(), large),
						getgoldys(lg[i].getY(), large), (int) pawx, (int) pawy)) { // 抓到大金礦
					setpaw();
					goldmove(lg[i], degree, 3);
					break;
				} else if (0 > pawx || pawy > getHeight() || pawx > getWidth()) {
					setpaw();
					pertimer.start();
					break;
				}
			}
		}
	}

	class clock implements ActionListener { // 時鐘
		public void actionPerformed(ActionEvent e) {
			second--;
			clockla.setText("剩餘時間:" + second + "秒");
			if (second == 0) { // 時間結束跳到結束畫面
				repaint();
				validate();	
				if(state==5 && nextstate(state)){  //全破
					removeAll();
					showscore();  //最後分數
					validate();
					repaint();
					pertimer.stop();
					clocktimer.stop();
				}
				if (nextstate(state)) {  //判斷是否能到下一關
					pertimer.stop();  //停止時間
					clocktimer.stop();
					state++;
					second = 30;  //重設秒數
					nextstates(state);
					setgoldimage();
					repaint();
					validate();
				} 
				else {
					removeAll();  //失敗
					JLabel over = new JLabel(overpic);
					over.setBounds(0, 0, overpic.getIconWidth(), overpic.getIconHeight());
					add(over);  //失敗畫面
					setSize(overpic.getIconWidth(), overpic.getIconHeight());
					validate();
					repaint();
				}
			} 
			else if (second == -2) {
				showscore();  //最後分數
				validate();
				repaint();
				pertimer.stop();
				clocktimer.stop();
			}
		}
	}
}
