package fp.s100502513;

import javax.swing.*;
import java.awt.*;

public class GameScreen extends PersonandGold {
	private ImageIcon backpic = new ImageIcon("pic/gameback.png");
	private JLabel backla = new JLabel(backpic);

	public GameScreen() {
		setLayout(null);
		setSize(backpic.getIconWidth(), backpic.getIconHeight());
		backla.setBounds(0, 0, backpic.getIconWidth(), backpic.getIconHeight()); // 設定背景位置及大小
		add(backla);
		
	}

	public int getpanelwidth() { // 回傳此寬度
		return backpic.getIconWidth();
	}

	public int getpanelheight() { // 回傳此高度
		return backpic.getIconHeight();
	}
}
