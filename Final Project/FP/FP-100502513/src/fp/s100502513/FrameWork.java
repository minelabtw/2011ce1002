package fp.s100502513;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;

public class FrameWork extends JFrame implements ActionListener {
	StartScreen start = new StartScreen();
	GameScreen game;
	Music music = new Music();
	public FrameWork() {
		music.setmusic("music/back.wav");
		add(start); // 先貼開始的畫面
		setSize(start.getWidth(), start.getHeight());
		start.startbu.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == start.startbu) {
			remove(start); // 移除開始畫面
			game = new GameScreen();
			add(game); // 新貼遊戲畫面(切換至遊戲畫面)
			setSize(game.getpanelwidth(), game.getpanelheight());
			setLocationRelativeTo(null);
			validate();
			repaint();
		}
	}
}
