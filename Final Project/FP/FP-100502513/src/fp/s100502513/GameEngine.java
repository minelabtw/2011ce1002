package fp.s100502513;


import java.awt.*;
import javax.swing.*;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MoveAction;

import fp.s100502513.PersonandGold.clock;
import fp.s100502513.PersonandGold.pawTimeListener;

import java.awt.event.*;

public class GameEngine extends JPanel{
	private JLabel goldla = new JLabel();  //金礦
	private JLabel scorela = new JLabel();  //分數
	private JLabel goalla = new JLabel();  //目標分數
	private JButton enter = new JButton();	//進入遊戲
	private JButton restart = new JButton();  //重新
	private JButton exit = new JButton();
	private JLabel[] sg = new JLabel[5]; // 小金礦
	private JLabel[] mg = new JLabel[5]; // 中金礦
	private JLabel[] lg = new JLabel[5]; // 大金礦
	private Timer goldtimer = new Timer(20, new goldTimeListener());
	private Timer pertimer;
	private Timer pawtimer; // 電球
	private Timer clocktimer; // 爪子
	private int degree;  //人物角度
	private int gx, gy;  //電球座標
	private int score = 0;
	private int goldsize;
	private final int small = 1;
	private final int medium = 2;
	private final int large = 3;
	private JFrame goalf = new JFrame();

	public GameEngine() {
		goalf.setLayout(null);  //目標LABEL的設定
		goalf.setSize(1885, 869);
		goalf.setLocationRelativeTo(null);
		goalla.setBounds(0, 0, 1885, 869);
		enter.setIcon(new ImageIcon("pic/cc.png"));  //進入遊戲按鈕設定
		enter.setBounds(1300, 600, 468, 203);
		enter.setBorder(null);
		enter.setContentAreaFilled(false);
		goalf.add(enter);
		goalf.add(goalla);
		enter.addActionListener(new ActionListener() {  //進入遊戲
			public void actionPerformed(ActionEvent e) {
				pertimer.start();  //時間開始
				pawtimer.start();
				clocktimer.start();
				goalf.setVisible(false);  //隱藏目標分數視窗
			}
		});
	}

	public void nextstates(int state) {  //下一關的目標
		goalla.setIcon(new ImageIcon("pic/goal"+state+".png"));
		switch (state) {
		case 1:
			setgold1();
			goalf.setVisible(true);
			break;
		case 2:
			setgold2();
			goalf.setVisible(true);
			break;
		case 3:
			setgold3();
			goalf.setVisible(true);
			break;
		case 4:
			setgold4();
			goalf.setVisible(true);
			break;
		case 5:
			setgold5();
			goalf.setVisible(true);
			break;
		default:
			break;
		}
	}

	public boolean nextstate(int state) { // 是否可以下一關
		boolean next = false;
		switch (state) {
		case 1:
			if (score >= 1100) {
				next = true;
			}
			break;
		case 2:
			if (score >= 2500) {
				next = true;
			}
			break;
		case 3:
			if (score >= 4500) {
				next = true;
			}
			break;
		case 4:
			if (score >= 7000) {
				next = true;
			}
			break;
		case 5:
			if (score >= 10000) {
				next = true;
			}
			break;
		default:
			next = false;
			break;
		}
		return next;
	}

	public void setscorela(JLabel scorela) { // 設定分數樣式
		this.scorela = scorela;
		scorela.setForeground(Color.RED);
		scorela.setFont(new Font("微軟正黑體", Font.BOLD, 40));
		scorela.setText("分數: " + score);
		scorela.setBounds(500, 20, 500, 100);
		add(scorela);
	}

	public void getscore(int gold) { // 得分
		switch (gold) {
		case small: // 小金礦100分
			score += 100;
			break;
		case medium: // 中金礦200分
			score += 200;
			break;
		case large: // 大金礦400分
			score += 400;
			break;
		default:
			break;
		}
		scorela.setText("分數: " + score);
	}

	public void showscore() { // 遊戲結束顯示分數
		removeAll();
		restart.setIcon(new ImageIcon("pic/restart.png"));
		restart.setBounds(50, 100, 468, 203);
		restart.setBorder(null);
		restart.setContentAreaFilled(false);
		exit.setIcon(new ImageIcon("pic/leave.png"));
		exit.setBounds(1300, 100, 468, 203);
		exit.setBorder(null);
		exit.setContentAreaFilled(false);
		JLabel l = new JLabel(new ImageIcon("pic/score.png"));
		l.setBounds(0, 0, 1885, 869);
		JLabel sc = new JLabel("" + score);
		sc.setForeground(Color.RED);
		sc.setFont(new Font("微軟正黑體", Font.BOLD, 150));
		sc.setText(score + "$");
		sc.setBounds(500, 620, 800, 200);
		exit.addActionListener(new ActionListener() {  //離開遊戲
			public void actionPerformed(ActionEvent arg0) {
			System.exit(JFrame.EXIT_ON_CLOSE);
			}
		});
		restart.addActionListener(new ActionListener() {  //重新遊戲
			public void actionPerformed(ActionEvent e) {
				removeAll();
				add(new GameScreen());
				validate();
				repaint();
			}
		});
		add(sc);
		add(restart);
		add(exit);
		add(l);	
	}

	public void setgold(JLabel[] s, JLabel[] m, JLabel[] l) { // 設置金礦圖片及圖片
		sg = s;
		mg = m;
		lg = l;
		for (int i = 0; i < 5; i++) {
			sg[i] = new JLabel();
			mg[i] = new JLabel();
			lg[i] = new JLabel();
			add(sg[i]);
			add(mg[i]);
			add(lg[i]);
		}
		setgoldimage();
	}

	public void setgoldimage() { // 設置金礦圖片
		for (int i = 0; i < 5; i++) {
			sg[i].setIcon(new ImageIcon("pic/sg.png"));
			mg[i].setIcon(new ImageIcon("pic/mg.png"));
			lg[i].setIcon(new ImageIcon("pic/lg.png"));
			sg[i].setSize(52, 46);
			mg[i].setSize(102, 91);
			lg[i].setSize(208, 182);
		}
	}

	public void setgold1() { // 設定第一關金礦位置
		sg[0].setLocation(1200, 544); // 小金礦
		sg[1].setLocation(200, 500);
		sg[2].setLocation(800, 400);
		sg[3].setLocation(900, 378);
		sg[4].setLocation(505, 456);
		mg[0].setLocation(480, 250); // 中金礦
		mg[1].setLocation(50, 278);
		mg[2].setLocation(1200, 350);
		mg[3].setLocation(767, 687);
		mg[4].setLocation(-500, -500);
		lg[0].setLocation(20, 500); // 大金礦
		lg[1].setLocation(1300, 200);
		lg[2].setLocation(1527, 650);
		lg[3].setLocation(-500, -500);
		lg[4].setLocation(-500, -500);
	}

	public void setgold2() { // 設定第二關金礦位置
		sg[0].setLocation(100, 200); // 小金礦
		sg[1].setLocation(500, 200);
		sg[2].setLocation(900, 200);
		sg[3].setLocation(1300, 200);
		sg[4].setLocation(1700, 200);
		mg[0].setLocation(50, 400); // 中金礦
		mg[1].setLocation(450, 400);
		mg[2].setLocation(850, 400);
		mg[3].setLocation(1250, 400);
		mg[4].setLocation(1650, 400);
		lg[0].setLocation(0, 550); // 大金礦
		lg[1].setLocation(400, 550);
		lg[2].setLocation(800, 550);
		lg[3].setLocation(1200, 550);
		lg[4].setLocation(1600, 550);
	}

	public void setgold3() { // 設定第三關金礦位置
		
		sg[0].setLocation(100, 700); // 小金礦
		sg[1].setLocation(500, 700);
		sg[2].setLocation(900, 700);
		sg[3].setLocation(1300, 700);
		sg[4].setLocation(1700, 700);
		mg[0].setLocation(50, 600); // 中金礦
		mg[1].setLocation(450, 600);
		mg[2].setLocation(850, 600);
		mg[3].setLocation(1250, 600);
		mg[4].setLocation(1650, 600);
		lg[0].setLocation(0, 400); // 大金礦
		lg[1].setLocation(400, 400);
		lg[2].setLocation(800, 400);
		lg[3].setLocation(1200, 400);
		lg[4].setLocation(1600, 400);
	}

	public void setgold4() { // 設定第四關金礦位置
		sg[0].setLocation(700, 350); // 小金礦
		sg[1].setLocation(950, 350);
		sg[2].setLocation(750, 425);
		sg[3].setLocation(900, 425);
		sg[4].setLocation(850, 500);
		mg[0].setLocation(800, 200); // 中金礦
		mg[1].setLocation(500, 300);
		mg[2].setLocation(550, 500);
		mg[3].setLocation(1050, 500);
		mg[4].setLocation(1100, 300);
		lg[0].setLocation(20, 200); // 大金礦
		lg[1].setLocation(20, 500);
		lg[2].setLocation(750, 600);
		lg[3].setLocation(1500, 200);
		lg[4].setLocation(1500, 500);
	}

	public void setgold5() { // 設定第五關金礦位置
		sg[0].setLocation(1600, 700); // 小金礦
		sg[1].setLocation(1700, 500);
		sg[2].setLocation(350, 400);
		sg[3].setLocation(500, 500);
		sg[4].setLocation(100, 700);
		mg[0].setLocation(1300, 450); // 中金礦
		mg[1].setLocation(1300, 550);
		mg[2].setLocation(1200, 500);
		mg[3].setLocation(1300, 575);
		mg[4].setLocation(1350, 550);
		lg[0].setLocation(1300, 450); // 大金礦
		lg[1].setLocation(1300, 550);
		lg[2].setLocation(1200, 400);
		lg[3].setLocation(1150, 500);
		lg[4].setLocation(1175, 575);
	}

	public void goldmove(JLabel g, int d, int s) { // 抓到之後讓金塊移動
		goldla = g;
		degree = d;
		gx = goldla.getX(); // 金塊X座標
		gy = goldla.getY(); // 金塊Y座標
		goldsize = s;
		goldtimer.start(); // 金塊移動
	}

	public boolean Catch(int[] x, int[] y, int px, int py) { // 判斷是否抓到
		boolean grab = false; // x、y為金礦座標 px、py為爪子座標
		if ((x[0] < px + 20 && px + 20 < x[1])
				&& (y[0] < py + 15 && py + 15 < y[1])) {
			grab = true;

		}
		return grab;
	}

	public int[] getgoldxs(int x, int goldsize) { // 得到小金礦的X座標
		int[] xs = new int[2];
		xs[0] = x;
		switch (goldsize) {
		case small:
			xs[1] = x + 50;
			break;
		case medium:
			xs[1] = x + 100;
			break;
		case large:
			xs[1] = x + 200;
			break;
		default:
			break;
		}
		return xs;
	}

	public int[] getgoldys(int y, int goldsize) { // 得到小金礦的Y座標
		int[] ys = new int[2];
		ys[0] = y;
		switch (goldsize) {
		case small:
			ys[1] = y + 15;
			break;
		case medium:
			ys[1] = y + 90;
			break;
		case large:
			ys[1] = y + 180;
			break;
		default:
			break;
		}
		return ys;
	}

	public void settimer(Timer t,Timer t2,Timer t3) { // 從PERSONANDGOLD拿TIMER
		pertimer = t;
		pawtimer = t2;
		clocktimer = t3;
	}
	
	class goldTimeListener implements ActionListener {  //金塊被抓到之後移動
		public void actionPerformed(ActionEvent e) {
			goldla.setLocation(gx, gy); // 金塊移動
			switch (goldsize) {
			case small: // 小金塊
				goldla.setIcon(new ImageIcon("pic/esg.png"));
				goldla.setSize(92, 76);
				gx -= 10 * Math.cos(((180.0 + degree) / 180) * Math.PI);
				gy += 10 * Math.sin(((180.0 + degree) / 180) * Math.PI);
				break;
			case medium: // 中金塊
				goldla.setIcon(new ImageIcon("pic/emg.png"));
				goldla.setSize(156, 128);
				gx -= 5 * Math.cos(((180.0 + degree) / 180) * Math.PI);
				gy += 5 * Math.sin(((180.0 + degree) / 180) * Math.PI);
				break;
			case large: // 大金塊
				goldla.setIcon(new ImageIcon("pic/elg.png"));
				goldla.setSize(242, 218);
				gx -= 2 * Math.cos(((180.0 + degree) / 180) * Math.PI);
				gy += 2 * Math.sin(((180.0 + degree) / 180) * Math.PI);
				break;
			default:
				break;
			}
			if (gy < 100) { // 到達地面讓金塊消失並得分人物開始動
				goldla.setLocation(-500, -500);
				goldtimer.stop();
				pertimer.start();
				switch (goldsize) { // 得分
				case small: // 小金塊
					getscore(small);
					break;
				case medium: // 中金塊
					getscore(medium);
					break;
				case large: // 大金塊
					getscore(large);
					break;
				default:
					break;
				}

			}
		}
	}

}
