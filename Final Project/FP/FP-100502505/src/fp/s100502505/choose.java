package fp.s100502505;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.Timer;

public class choose extends JFrame implements ActionListener{
	
	JPanel first1 = new JPanel();//宣告Panel
	JPanel first2 = new JPanel();
	JPanel first3 = new JPanel();
	JPanel first4 = new JPanel();
	JPanel second1 = new JPanel();
	JPanel second2 = new JPanel();
	JPanel second3 = new JPanel();
	
	JLabel choose1 = new JLabel("player1請選擇棋子:");//棋子的圖案
	JLabel choose2 = new JLabel("player2請選擇棋子");
	JLabel title = new JLabel("兩人相親相愛之五子棋");//標題
	JLabel player1 = new JLabel("player1");//玩家
	JLabel player2 = new JLabel("player2");//玩家
	JLabel time = new JLabel("已過時間");//時間
	JLabel boardpic = new JLabel();//棋盤圖案
	
	Font size = new Font("serif", Font.BOLD, 40);//字型大小
	
	JButton tokenpic1 = new JButton(new ImageIcon("token1.gif"));//棋子圖案按鈕
	JButton tokenpic2 = new JButton(new ImageIcon("token2.gif"));
	JButton tokenpic3 = new JButton(new ImageIcon("token3.gif"));
	JButton tokenpic4 = new JButton(new ImageIcon("token4.gif"));
	JButton tokenpic5 = new JButton(new ImageIcon("token5.gif"));
	JButton tokenpic6 = new JButton(new ImageIcon("token6.gif"));
	JButton tokenpic7 = new JButton(new ImageIcon("token7.gif"));
	JButton tokenpic8 = new JButton(new ImageIcon("token8.gif"));
	JButton restart = new JButton("再玩一局");
	JButton confirm = new JButton("確定");
	JButton cancel = new JButton("取消");
	JButton end = new JButton("結束遊戲");
	
	public choose ()
	{	
		add(first1,BorderLayout.NORTH);//排版
		add(first2,BorderLayout.CENTER);
		
		choose1.setFont(size);
		
		first1.add(choose1);
		first2.setLayout(new GridLayout(1,8));
		
		first2.add(tokenpic1);
		first2.add(tokenpic2);
		first2.add(tokenpic3);
		first2.add(tokenpic4);
		first2.add(tokenpic5);
		first2.add(tokenpic6);
		first2.add(tokenpic7);
		first2.add(tokenpic8);
		
		
		tokenpic1.addActionListener(this);
		tokenpic2.addActionListener(this);
		tokenpic3.addActionListener(this);
		tokenpic4.addActionListener(this);
		tokenpic5.addActionListener(this);
		tokenpic6.addActionListener(this);
		tokenpic7.addActionListener(this);
		tokenpic8.addActionListener(this);
		
		setTitle("兩人相親相愛之五子棋");
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void actionPerformed(ActionEvent e) 
	{
		// TODO Auto-generated method stub
		if(e.getSource() == tokenpic1)//按了按鈕之後,換下一個介面,選過的棋子不能再選
		{
			setVisible(false);
			token t = new token();
			t.tokenpic1.setEnabled(false);
			t.setplayerone(1);
		}
		if(e.getSource() == tokenpic2)
		{
			setVisible(false);
			token t = new token();
			t.tokenpic2.setEnabled(false);
			t.setplayerone(2);
		}
		if(e.getSource() == tokenpic3)
		{
			setVisible(false);
			token t = new token();
			t.tokenpic3.setEnabled(false);
			t.setplayerone(3);
		}
		if(e.getSource() == tokenpic4)
		{
			setVisible(false);
			token t = new token();
			t.tokenpic4.setEnabled(false);
			t.setplayerone(4);
		}
		if(e.getSource() == tokenpic5)
		{
			setVisible(false);
			token t = new token();
			t.tokenpic5.setEnabled(false);
			t.setplayerone(5);
		}
		if(e.getSource() == tokenpic6)
		{
			setVisible(false);
			token t = new token();
			t.tokenpic6.setEnabled(false);
			t.setplayerone(6);
		}	
		if(e.getSource() == tokenpic7)
		{
			setVisible(false);
			token t = new token();
			t.tokenpic7.setEnabled(false);
			t.setplayerone(7);
		}
		if(e.getSource() == tokenpic8)
		{
			setVisible(false);
			token t = new token();
			t.tokenpic8.setEnabled(false);
			t.setplayerone(8);
		}
	}
}
