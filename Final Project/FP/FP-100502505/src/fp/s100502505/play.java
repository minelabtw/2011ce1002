package fp.s100502505;

import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class play extends JFrame implements ActionListener{
	
	int minute=0;
	int second=0;
	int playerone;
	int playertwo;
	int nowPlayer=1;
	int mouseX;
	int mouseY;
	int recordX;
	int recordY;
	int recordPlayer;
	
	Board b = new Board();
	
	Image image=Toolkit.getDefaultToolkit().createImage("board.jpg");

	JPanel first1 = new JPanel();//宣告Panel
	JPanel first2 = new JPanel();
	JPanel first3 = new JPanel();
	JPanel first4 = new JPanel();
	JPanel second1 = new JPanel();
	
	
	JPanel second2 = new JPanel()
	{
		protected void paintComponent(Graphics g)//畫棋子
		{
			g.drawImage(image, 0, 0, play.this);
			int x=(mouseX-20+17)/35*35+20;
			int y=(mouseY-20+17)/35*35+20;
			for (int i=0; i<15; i++) 
			{
				for(int j=0;j<15;j++)
				{
					if(b.getToken(i, j)!=0)
					{
						int token=b.getToken(i, j);
						if(token==1)
						{
							switch (playerone) 
							{
							case 1:
								g.drawImage(tokenpic1, 20+i*35-17, 20+j*35-17, 35, 35,play.this);
								break;
							case 2:
								g.drawImage(tokenpic2, 20+i*35-17, 20+j*35-17, 35, 35,play.this);
								break;
							case 3:
								g.drawImage(tokenpic3, 20+i*35-17, 20+j*35-17, 35, 35,play.this);
								break;
							case 4:
								g.drawImage(tokenpic4, 20+i*35-17, 20+j*35-17, 35, 35,play.this);
								break;
							case 5:
								g.drawImage(tokenpic5, 20+i*35-17, 20+j*35-17, 35, 35,play.this);
								break;
							case 6:
								g.drawImage(tokenpic6, 20+i*35-17, 20+j*35-17, 35, 35,play.this);
								break;
							case 7:
								g.drawImage(tokenpic7, 20+i*35-17, 20+j*35-17, 35, 35,play.this);
								break;
							case 8:
								g.drawImage(tokenpic8, 20+i*35-17, 20+j*35-17, 35, 35,play.this);
								break;
							}
						}else
						{
							switch (playertwo) 
							{
							case 1:
								g.drawImage(tokenpic1, 20+i*35-17, 20+j*35-17, 35, 35,play.this);
								break;
							case 2:
								g.drawImage(tokenpic2, 20+i*35-17, 20+j*35-17, 35, 35,play.this);
								break;
							case 3:
								g.drawImage(tokenpic3, 20+i*35-17, 20+j*35-17, 35, 35,play.this);
								break;
							case 4:
								g.drawImage(tokenpic4, 20+i*35-17, 20+j*35-17, 35, 35,play.this);
								break;
							case 5:
								g.drawImage(tokenpic5, 20+i*35-17, 20+j*35-17, 35, 35,play.this);
								break;
							case 6:
								g.drawImage(tokenpic6, 20+i*35-17, 20+j*35-17, 35, 35,play.this);
								break;
							case 7:
								g.drawImage(tokenpic7, 20+i*35-17, 20+j*35-17, 35, 35,play.this);
								break;
							case 8:
								g.drawImage(tokenpic8, 20+i*35-17, 20+j*35-17, 35, 35,play.this);
								break;
							}
						}
					}
				}
			}
			
			if(x<image.getWidth(this)&&y<image.getHeight(this))//畫矩形,確定位置
			{
				g.drawRect(x-17, y-17,35 ,35);
			}
		}
	};
	
	JPanel second3 = new JPanel();

	JLabel choose1 = new JLabel("player1請選擇棋子:");
	JLabel choose2 = new JLabel("player2請選擇棋子");
	JLabel title = new JLabel("兩人相親相愛之五子棋");
	JLabel player1 = new JLabel("          player1");
	JLabel player2 = new JLabel("    player2");
	JLabel time = new JLabel("         已過時間00:00");
	JLabel space1 = new JLabel("");
	JLabel space2 = new JLabel("");
	JLabel boardpic = new JLabel();
	
	Font size1 = new Font("serif", Font.BOLD, 17);//字型大小
	Font size2 = new Font("serif", Font.BOLD, 20);
	Font size3 = new Font("serif", Font.BOLD,18);
	
	Image tokenpic1 = Toolkit.getDefaultToolkit().createImage("token1.gif");//圖片
	Image tokenpic2 = Toolkit.getDefaultToolkit().createImage("token2.gif");
	Image tokenpic3 = Toolkit.getDefaultToolkit().createImage("token3.gif");
	Image tokenpic4 = Toolkit.getDefaultToolkit().createImage("token4.gif");
	Image tokenpic5 = Toolkit.getDefaultToolkit().createImage("token5.gif");
	Image tokenpic6 = Toolkit.getDefaultToolkit().createImage("token6.gif");
	Image tokenpic7 = Toolkit.getDefaultToolkit().createImage("token7.gif");
	Image tokenpic8 = Toolkit.getDefaultToolkit().createImage("token8.gif");
	
	JButton restart = new JButton("再玩一局");//宣告按鈕
	JButton confirm = new JButton("確定");
	JButton cancel = new JButton("悔棋");
	JButton end = new JButton("結束遊戲");
	
	public play ()
	{
		Music music = new Music();//背景音樂
		music.setmusic("007.wav");
		
		add(second1,BorderLayout.NORTH);//排版
		add(second2,BorderLayout.CENTER);
		add(second3,BorderLayout.SOUTH);
		
		restart.setFont(size3);
		player1.setFont(size2);
		player2.setFont(size2);
		time.setFont(size1);
		confirm.setFont(size3);
		cancel.setFont(size3);
		end.setFont(size3);
		
		second1.setLayout(new GridLayout(2,3));
		second1.add(space1);
		second1.add(time);
		second1.add(space2);
		second1.add(player1);
		second1.add(restart);
		second1.add(player2);
		
		second1.setBackground(Color.GREEN);
			
		second2.addMouseListener(mouseAdapter);
		second2.addMouseMotionListener(mouseAdapter);
		
		second3.setLayout(new GridLayout(1,2));
		//second3.add(confirm);
		second3.add(cancel);
		second3.add(end);
		
		second3.setBackground(Color.GREEN);
		
		restart.addActionListener(this);
		confirm.addActionListener(this);
		cancel.addActionListener(this);
		end.addActionListener(this);
		
		cancel.setEnabled(false);
		
		setTitle("兩人相親相愛之五子棋");
		setSize(550,700);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
		timer.start();//開始計時
	}
	
	public void setplayerone(int i)//知道player1,player2棋子的顏色
	{
		playerone = i;
	}
	
	public void setplayertwo(int i)
	{
		playertwo = i;
	}
	
	
	Timer timer = new Timer(1000, new TimerListener());
	
	
	public class TimerListener implements ActionListener{//Timer
		public void actionPerformed(ActionEvent e) 
		{
			second++;
			if(second == 60)
			{
				minute++;
				second = 0;
			}
			if(second < 10)
			{
				if(minute < 10)
				{
					time.setText("         已過時間0"+minute+":0"+second);
				}else{
					time.setText("         已過時間"+minute+":0"+second);
				}
			}else{
				if(minute < 10)
				{
					time.setText("         已過時間0"+minute+":"+second);
				}else{
					time.setText("         已過時間"+minute+":"+second);
				}
			}
			if(minute >= 30)
			{
				time.setText("  時間過太久,結束這遊戲!");
				System.exit(EXIT_ON_CLOSE);
			}
			if(b.check() == 1 || b.check() == 2)//有一方獲勝,時間即停止.
			{
				timer.stop();
			}
		}
		
	}

	public void actionPerformed(ActionEvent e) 
	{
		// TODO Auto-generated method stub
		/*if(e.getSource() == confirm)
		{
			
		}*/
		if(e.getSource() == restart)//再玩一局按鈕
		{
			second = 0;
			minute = 0;
			timer.start();
			b.reset();
			second2.repaint();
			cancel.setEnabled(false);
			player1.setText("          player1");
			player2.setText("    player2");
			nowPlayer=1;
		}
		if(e.getSource() == cancel)//悔棋
		{
			b.setToken(recordX, recordY, 0);
			second2.repaint();
			nowPlayer=recordPlayer;
			cancel.setEnabled(false);
		}
		if(e.getSource() == end)//結束按鈕
		{
			System.exit(EXIT_ON_CLOSE);
		}
	}
	
	public void winner()//獲勝之後,上面顯示的字
	{
		
		if(b.check() == 1)
		{
			player1.setText("          player1 win!!");
			player2.setText("    player2 lose!!");
		}else if(b.check() == 2)
		{
			player1.setText("          player1 lose!!");
			player2.setText("    player2 win!!");
		}
	}
	
	MouseAdapter mouseAdapter=new MouseAdapter()//滑鼠功能
	{
		public void mouseMoved(MouseEvent e) 
		{
			if(b.check() == 1 || b.check() == 2)
			{
				
			}else{
				mouseX=e.getX();
				mouseY=e.getY();
				second2.repaint();
			}
		};
		
		public void mouseClicked(MouseEvent e)
		{
			if(b.check() == 1 || b.check() == 2)
			{
				
			}else{	
				mouseX=e.getX();
				mouseY=e.getY();
				
				if(b.putToken((int)(e.getX()-20+17)/35, (int)(e.getY()-20+17)/35,nowPlayer))
				{
					recordX=(int)(e.getX()-20+17)/35;
					recordY=(int)(e.getY()-20+17)/35;
					recordPlayer=nowPlayer;
					cancel.setEnabled(true);
					
					if(nowPlayer==1)
					{
						nowPlayer=2;
					}else{ 
						nowPlayer=1;
					}
					
				}
				second2.repaint();
				winner();
			}
		}
	};
}

