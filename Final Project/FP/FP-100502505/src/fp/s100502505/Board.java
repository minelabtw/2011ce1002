package fp.s100502505;

import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class Board {
	
	int player1 = 1;
	int player2 = 2;
	
	int board[][] = new int[16][16];//用來儲存棋子的位置
	
	public int getToken(int x,int y)//得到棋子的位置
	{
		return board[x][y];
	}
	
	public boolean putToken(int x,int y,int to)//看有沒有棋子
	{
		if(board[x][y] == 0)
		{
			board[x][y] = to;
			
			return true;
		}else
		{
			return false;
		}
	}
	
	public void setToken(int x,int y,int to)//棋子位置
	{	
			board[x][y] = to;
				
	}
	
	public int check()//看五顆是否連在一起 是否獲勝
	{
		for(int x=0;x<board.length;x++)
		{
			for(int y=0;y<board[x].length;y++)
			{
				if(board[x][y]==1 && board[x+1][y]==1 && board[x+2][y]==1 && 
						board[x+3][y]==1 && board[x+4][y]==1)
				{
					return 1;
				}	
				else if(board[x][y]==2 && board[x+1][y]==2 && board[x+2][y]==2 && 
						board[x+3][y]==2 && board[x+4][y]==2)
				{
					return 2;	
				}
				else if(board[x][y]==1 && board[x][y+1]==1 && board[x][y+2]==1 && 
						board[x][y+3]==1 && board[x][y+4]==1)
				{
					return 1;	
				}
				else if(board[x][y]==2 && board[x][y+1]==2 && board[x][y+2]==2 && 
						board[x][y+3]==2 && board[x][y+4]==2)
				{
					return 2;		
				}
				else if(board[x][y]==1 && board[x+1][y+1]==1 && board[x+2][y+2]==1 && 
						board[x+3][y+3]==1 && board[x+4][y+4]==1)
				{
					return 1;
				}
				else if(board[x][y]==2 && board[x+1][y+1]==2 && board[x+2][y+2]==2 && 
						board[x+3][y+3]==2 && board[x+4][y+4]==2)
				{
					return 2;	
				}
			}
			
		}
		for(int x=0;x<12;x++)
		{
				for(int y=4;y<16;y++)
				{	
			        if(board[x][y]==1 && board[x+1][y-1]==1 && board[x+2][y-2]==1 && 
			        		board[x+3][y-3]==1 && board[x+4][y-4]==1)
			        {
			        	return 1; 
			        }
			        else if(board[x][y]==2 && board[x+1][y-1]==2 && board[x+2][y-2]==2 && 
			        		board[x+3][y-3]==2 && board[x+4][y-4]==2)
			        {
			        	return 2;
			        }        
				}
		}
		return 0;
	}
	
	public void reset()//位置全部歸零
	{
		for(int i=0;i<16;i++)
		{
			for(int j=0;j<16;j++)
			{
				board[i][j]=0;
			}
		}
	}
	
}
