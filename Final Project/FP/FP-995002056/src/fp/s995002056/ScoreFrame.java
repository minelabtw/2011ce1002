package fp.s995002056;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.Scanner;

import javax.swing.JLabel;
import javax.swing.JPanel;

//----------------排行榜介面----------------//
public class ScoreFrame extends JPanel {

	private int[] score = new int[10];
	int i = 0;

	JLabel text1 = new JLabel("排行榜");
	JLabel text2 = new JLabel("第一名:");
	JLabel text3 = new JLabel("第二名:");
	JLabel text4 = new JLabel("第三名:");
	JLabel text5 = new JLabel("第四名:");
	JLabel text6 = new JLabel("第五名:");
	JLabel text7 = new JLabel("第六名:");
	JLabel text8 = new JLabel("第七名:");
	JLabel text9 = new JLabel("第八名:");
	JLabel text10 = new JLabel("第九名:");
	JLabel text11 = new JLabel("第十名:");

	Font f1 = new Font("微軟正黑體", Font.BOLD, 72);
	Font f2 = new Font("微軟正黑體", Font.BOLD, 48);
	JPanel p = new JPanel();

	public ScoreFrame() throws Exception {

		// 將分數存入score裡
		java.io.File file = new java.io.File("score.txt");
		Scanner input = new Scanner(file);

		while (input.hasNext()) {
			score[i] = input.nextInt();
			i++;
		}

		// 製作排行榜介面
		text1.setFont(f1);
		text2.setFont(f2);
		text3.setFont(f2);
		text4.setFont(f2);
		text5.setFont(f2);
		text6.setFont(f2);
		text7.setFont(f2);
		text8.setFont(f2);
		text9.setFont(f2);
		text10.setFont(f2);
		text11.setFont(f2);

		add(text1, BorderLayout.NORTH);

		p.setLayout(new GridLayout(10, 2, 1, 1));

		JLabel l1 = new JLabel(score[0] + "秒");
		JLabel l2 = new JLabel(score[1] + "秒");
		JLabel l3 = new JLabel(score[2] + "秒");
		JLabel l4 = new JLabel(score[3] + "秒");
		JLabel l5 = new JLabel(score[4] + "秒");
		JLabel l6 = new JLabel(score[5] + "秒");
		JLabel l7 = new JLabel(score[6] + "秒");
		JLabel l8 = new JLabel(score[7] + "秒");
		JLabel l9 = new JLabel(score[8] + "秒");
		JLabel l10 = new JLabel(score[9] + "秒");

		l1.setFont(f2);
		l2.setFont(f2);
		l3.setFont(f2);
		l4.setFont(f2);
		l5.setFont(f2);
		l6.setFont(f2);
		l7.setFont(f2);
		l8.setFont(f2);
		l9.setFont(f2);
		l10.setFont(f2);

		p.add(text2);
		p.add(l1);
		p.add(text3);
		p.add(l2);
		p.add(text4);
		p.add(l3);
		p.add(text5);
		p.add(l4);
		p.add(text6);
		p.add(l5);
		p.add(text7);
		p.add(l6);
		p.add(text8);
		p.add(l7);
		p.add(text9);
		p.add(l8);
		p.add(text10);
		p.add(l9);
		p.add(text11);
		p.add(l10);

		add(p, BorderLayout.SOUTH);
		i = 0;
	}
}
