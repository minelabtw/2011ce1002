package fp.s995002056;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

/*-----------------------------初始視窗-----------------------------------*/
public class InitialFrame extends JFrame implements ActionListener {
	ImageIcon i_start = new ImageIcon("按鈕_開始.png");
	ImageIcon i_grade = new ImageIcon("按鈕_排行榜.png");
	ImageIcon i_option = new ImageIcon("按鈕_設定.png");
	ImageIcon i_explain = new ImageIcon("按鈕_遊戲說明.png");
	ImageIcon i_special = new ImageIcon("按鈕_特殊模式.png");
	ImageIcon i_tower = new ImageIcon("tower.jpg");

	JButton b_start = new JButton(i_start);
	JButton b_option = new JButton(i_option);
	JButton b_grade = new JButton(i_grade);
	JButton b_explain = new JButton(i_explain);
	JButton b_special = new JButton(i_special);

	JPanel p_bt = new JPanel();

	JLabel back = new JLabel(i_tower);

	// ---------------------主程式------------------//
	InitialFrame() throws Exception {
		// 初始按鈕
		setLayout(null);

		b_start.addActionListener(this);
		b_option.addActionListener(this);
		b_grade.addActionListener(this);
		b_explain.addActionListener(this);
		b_special.addActionListener(this);

		// 設置三個主畫面按鈕
		p_bt.setLayout(new GridLayout(5, 1, 1, 1));
		p_bt.setBounds(65, 349, 369, 404);

		p_bt.add(b_start);
		p_bt.add(b_grade);
		p_bt.add(b_option);
		p_bt.add(b_explain);
		p_bt.add(b_special);
		add(p_bt);

		// 設定背景圖
		back.setBounds(0, 0, 500, 800);
		add(back);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == b_start) {
			// 開始遊戲畫面
			GameFrame f1 = new GameFrame(true);

			f1.setTitle("Final Project");
			f1.setSize(500, 800);
			f1.setLocationRelativeTo(null);
			f1.setVisible(true);
		}

		if (e.getSource() == b_grade) {

			// 排行榜
			try {
				ScoreFrame f2 = new ScoreFrame();
				JFrame sc = new JFrame();

				sc.add(f2);
				sc.setTitle("排行榜");
				sc.setSize(500, 800);
				sc.setLocationRelativeTo(null);
				sc.setVisible(true);
			} catch (Exception ex) {
			}
		}

		// 遊戲說明
		if (e.getSource() == b_explain) {
			Explain ex = new Explain();
			JFrame sc2 = new JFrame();

			sc2.add(ex);
			sc2.setTitle("排行榜");
			sc2.pack();
			sc2.setLocationRelativeTo(null);
			sc2.setVisible(true);
		}

		// 特殊遊戲模式
		if (e.getSource() == b_special) {
			// 開始遊戲畫面
			GameFrame f1 = new GameFrame(false);

			f1.setTitle("Final Project");
			f1.setSize(500, 800);
			f1.setLocationRelativeTo(null);
			f1.setVisible(true);
		}
	}
}
