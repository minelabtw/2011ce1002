package fp.s995002056;

import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

//------------------遊戲說明---------------//
public class Explain extends JPanel {
	Explain() {
		
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(7, 1, 1, 1));
		Font f = new Font("微軟正黑體", Font.BOLD, 36);
		
		JLabel l0 = new JLabel("故事:");
		JLabel l1 = new JLabel("在某個王國裡，國王為了要替公主尋找勇敢的王子，");
		JLabel l2 = new JLabel("所以將公主放置在某處的高塔上方，讓王子挑戰");
		JLabel l3 = new JLabel("遊戲方法:");
		JLabel l4 = new JLabel("在公主下方會有隨機數量的塔，您必須使用槌子，");
		JLabel l5 = new JLabel("將其一一敲掉，就在全部敲完後，進行下一個塔");
		JLabel l6 = new JLabel("若是塔已經完全敲完，卻又繼續敲的話，遊戲失敗");
		
		l0.setFont(f);
		l1.setFont(f);
		l2.setFont(f);
		l3.setFont(f);
		l4.setFont(f);
		l5.setFont(f);
		l6.setFont(f);
		
		p.add(l0);
		p.add(l1);
		p.add(l2);
		p.add(l3);
		p.add(l4);
		p.add(l5);
		p.add(l6);
		
		add(p);
	}
}
