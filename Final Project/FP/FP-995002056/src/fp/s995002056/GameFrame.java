package fp.s995002056;

import java.net.URL;
import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.*;
import java.awt.event.*;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.swing.*;

/*------------------------遊戲畫面------------------------------------*/
public class GameFrame extends JFrame implements ActionListener {
	// 宣告
	// imageicon
	ImageIcon i_hit = new ImageIcon("按鈕_敲.png");
	ImageIcon i_next = new ImageIcon("按鈕_下一個.png");
	ImageIcon i_prin = new ImageIcon("princess.png");
	ImageIcon i_tower = new ImageIcon("tower.png");
	ImageIcon i_back = new ImageIcon("back.jpg");
	ImageIcon i_ham = new ImageIcon("槌子.gif");

	// button
	JButton b_hit = new JButton(i_hit);
	JButton b_next = new JButton(i_next);
	JButton[] b_tower = new JButton[20];

	// panel
	JPanel p_bt = new JPanel();
	JPanel p_tower = new JPanel();
	JPanel p_time = new JPanel();

	// label
	JLabel l_time = new JLabel();
	JLabel l_prin = new JLabel(i_prin);
	JLabel back = new JLabel(i_back);
	JLabel ham = new JLabel(i_ham);
	JLabel l_hitn = new JLabel();
	JLabel l_towern = new JLabel();

	Font f = new Font("微軟正黑體", Font.BOLD, 20);

	// 變數
	int rn;// 隨機的數字
	int count = 0;// 剩餘層數
	int tn;// 剩餘塔量
	int sec = 0;// 秒數
	int fsec = 0;
	int t_ham = 0;// 控制槌子timer
	double hitn = 0;// 打了幾下的計數器
	boolean choose;

	Timer t = new Timer(100, new TimerListener());

	// multiple windows
	private LoseFrame lf = new LoseFrame();
	private JFrame loseframe = new JFrame();
	private JFrame winframe = new JFrame();

	// ------------------主程式-------------------//
	GameFrame(boolean ch) {// 判斷是否是特殊模式

		choose = ch;

		if (ch)	//正常模式15座塔
			tn = 15;
		else	//特殊模式10座塔
			tn = 10;
		
		t.start();
		setLayout(null);// 取消自動排版

		b_hit.addActionListener(this);
		b_next.addActionListener(this);

		// 顯示時間
		l_time.setFont(f);
		p_time.setBounds(150, 0, 100, 50);
		l_time.setText(sec + ":" + fsec);
		p_time.add(l_time);
		add(p_time);

		// 顯示計算平均打幾下
		l_hitn.setBounds(0, 35, 300, 50);
		l_hitn.setText("平均每秒打了" + 0 + "下");
		l_hitn.setFont(f);
		add(l_hitn);

		// 提醒剩下幾座塔
		l_towern.setBounds(350, 0, 150, 50);
		l_towern.setText("剩下" + tn + "座塔");
		l_towern.setFont(f);
		add(l_towern);

		// 控制按鈕
		p_bt.setLayout(new GridLayout(1, 2, 1, 1));
		p_bt.setBounds(0, 800 - 174, 500, 150);// 設定panel位置
		p_bt.add(b_hit);
		p_bt.add(b_next);
		add(p_bt);

		for (int i = 0; i < 20; i++) {// 初始化按鈕
			b_tower[i] = new JButton();

		}

		// 呼叫印出塔的method
		newtower(ch);
		add(p_tower);

		// 放上槌子圖片
		ham.setBounds(0, 300, 50, 100);
		add(ham);

		// 設定背景圖
		back.setBounds(0, 0, 500, 800);
		add(back);
	}

	public void actionPerformed(ActionEvent e) {
		// 按鈕動作
		// 按鈕:敲
		if (e.getSource() == b_hit) {

			// 控制槌子動作 先將槌子往右移動
			ham.removeAll();
			ham.setBounds(50, 300, 50, 100);
			add(ham);
			back.removeAll();// 重設背景，才不會蓋過槌子
			add(back);

			// 判斷有無違規
			if (hitgetgoal()) {// 無違規

				hitn++;// 計數器
				count--;

				// 先移除panel裡一層,然後重新繪製
				p_tower.remove(b_tower[count]);
				p_tower.repaint();

			} else {// 違規
				// 參數全部初始化
				count = 0;
				tn = 15;
				sec = 0;
				fsec = 0;
				hitn = 0;

				// 呼叫出失敗視窗
				setVisible(false);
				loseframe.setVisible(true);
				loseframe.add(lf);
				loseframe.setLocationRelativeTo(null);
				loseframe.pack();
				loseframe.setTitle("lose");
			}
		}

		// 按鈕:下一個
		if (e.getSource() == b_next) {

			// 若本塔已完全清除,則進行下一個塔
			if (nextgetgoal()) {
				// 判斷是否還有下一個
				if (towernumber()) {// 有下一個塔,則重新繪製
					p_tower.removeAll();
					newtower(choose);
					p_tower.repaint();
					l_towern.removeAll();
					l_towern.setText("剩下" + tn + "座塔");
				} else {// 沒有下一個
					// 參數初始化
					count = 0;
					tn = 15;
					fsec = 0;
					hitn = 0;

					setVisible(false);
					WinFrame wf = new WinFrame(sec);
					winframe.setVisible(true);// 呼叫出獲勝視窗
					winframe.add(wf);
					winframe.setLocationRelativeTo(null);
					winframe.pack();
					winframe.setTitle("win");

					sec = 0;
				}
			}
		}
	}

	public boolean hitgetgoal() {// 若打到最後一層則算輸
		// 辨識是否成功
		if (count > 0) {// 若層數大於一則可繼續打
			return true;
		} else {
			return false;// 若層數=1則失敗
		}
	}

	public boolean nextgetgoal() {// 還沒打完則不能結束
		if (count > 0) {
			return false;
		} else {
			return true;
		}
	}

	public void newtower(boolean ch) {
		// new新的城堡
		if (ch) {
			count = 0;
			rn = (int) (Math.random() * 8);
			p_tower.setLayout(new GridLayout(8, 1, 1, 1));

			l_prin.setBounds(100, 79, 293, 121);
			add(l_prin);

			for (int j = 0; j < rn + 1; j++) {
				p_tower.add(b_tower[j]);
				count++;
			}
		} else {
			count = 0;
			rn = (int) (Math.random() * 10 + 10);

			p_tower.setLayout(new GridLayout(20, 1, 1, 1));

			l_prin.setBounds(100, 79, 293, 121);
			add(l_prin);

			for (int j = 0; j < rn + 1; j++) {
				p_tower.add(b_tower[j]);
				count++;
			}
		}
		p_tower.setBounds(100, 200, 300, 400);
	}

	public boolean towernumber() {
		tn--;// 檢查塔的數量是否已經足夠

		if (tn == 0) {
			return false;
		} else {
			return true;
		}
	}

	// ---------------------------Timer-------------------//
	class TimerListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			fsec++;
			t_ham++;

			if (t_ham == 2) {// 0.2秒後 槌子回歸原位
				t_ham = 0;
				ham.removeAll();
				ham.setBounds(0, 300, 50, 100);
				add(ham);
				back.removeAll();
				add(back);
			}

			if (fsec == 10) {
				fsec = 0;
				sec++;
			}

			if (sec != 0) {
				l_hitn.removeAll();
				l_hitn.setText("平均每秒打了" + hitn / sec + "下");
				add(l_hitn);
			}

			p_time.removeAll();
			l_time.setText("花了" + sec + "秒");
			p_time.add(l_time);

			p_time.repaint();
		}
	}
}
