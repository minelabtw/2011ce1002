package fp.s995002056;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

//-----------------失敗視窗----------------//
public class LoseFrame extends JPanel {

	public LoseFrame() {

		Font f = new Font("微軟正黑體", Font.BOLD, 108);
		JLabel l = new JLabel("失敗!");
		l.setFont(f);
		add(l);
	}
}
