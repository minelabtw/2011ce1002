package fp.s100502506;

public class Player {						//player
	private int HP;
	private int Money;
	private int chapter = 0;

	public Player(int inChapter) {			//player init
		if (inChapter == 1) {
			HP = 20;
			Money = 500;
		}
		if (inChapter == 2) {
			HP = 20;
			Money = 750;
		}
		if (inChapter == 3) {
			HP = 20;
			Money = 1000;
		}
		if (inChapter == 4) {
			HP = 25;
			Money = 1250;
		}
		if (inChapter == 5) {
			HP = 30;
			Money = 1500;
		}
	}
	//get and set
	public int getHP() {
		return HP;
	}

	public int getMoney() {
		return Money;
	}

	public void setHP(int inhp) {
		HP = inhp;
	}

	public void setMoney(int inmoney) {
		Money = inmoney;
	}

}
