package fp.s100502506;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.FileNotFoundException;

import javax.swing.JFrame;

public class FinalProject {

	public static void main(String argc[]) throws FileNotFoundException {//main

		FrameWork meunFrameWork = new FrameWork();//set frame work to middle
		meunFrameWork.setVisible(true);
		meunFrameWork.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		meunFrameWork.setSize(800, 640);// 250,225
		meunFrameWork.setTitle("Tower Defense");
		meunFrameWork.setResizable(false);
		Dimension screensizeDimension = Toolkit.getDefaultToolkit()
				.getScreenSize();
		Dimension size = meunFrameWork.getSize();
		int x = (int) (screensizeDimension.getWidth() / 2 - size.getWidth() / 2);
		int y = (int) (screensizeDimension.getHeight() / 2 - size.getHeight() / 2);
		meunFrameWork.setLocation(x, y);
	}

}
