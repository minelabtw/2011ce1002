package fp.s100502506;

public class Monster {														//monster
	private boolean pathflag[] = new boolean[100];
	private int MonsterX;
	private int MonsterY;
	private int MonsterInitX;
	private int MonsterInitY;
	private double MonsterSpeed;
	private int MonsterHp;
	private int Type;
	private int reward;

	// Type1 BlueDragon
	// Type2 GreenDragon
	// Type3 RockMan
	// Type4 BlackGuide

	// Type5 bat
	// Type6 ghost
	// Type7 girlghost
	// Type8 zombi

	// Type9 big mid sky
	// Type10 shatishu
	// Type11 black witch
	// Type12 big mid sky young

	// Type97 Lord
	// Type98 Bahamuto
	// Type99 Super
	public Monster(int type) {							//Type init
		MonsterSpeed = 1;
		Type = type;
		for (int i = 0; i < 100; i++)
			pathflag[i] = false;

		if (Type == 1) {
			MonsterHp = 500;
			reward = 10;
		}
		if (Type == 2) {
			MonsterHp = 1000;
			reward = 20;
		}

		if (Type == 3) {
			MonsterHp = 2000;
			reward = 30;
		}

		if (Type == 4) {
			MonsterHp = 3000;
			reward = 40;
		}
		if (Type == 5) {
			MonsterHp = 1000;
			reward = 50;
		}
		if (Type == 6) {
			MonsterHp = 500;
			reward = 60;
		}

		if (Type == 7) {
			MonsterHp = 5000;
			reward = 70;
		}

		if (Type == 8) {
			MonsterHp = 2000;
			reward = 80;
		}
		if (Type == 9) {
			MonsterHp = 1000;
			reward = 90;
		}
		if (Type == 10) {
			MonsterHp = 500;
			reward = 100;
		}

		if (Type == 11) {
			MonsterHp = 5000;
			reward = 110;
		}

		if (Type == 12) {
			MonsterHp = 2000;
			reward = 120;
		}
		if (Type == 13) {
			MonsterHp = 1000;
			reward = 90;
		}
		if (Type == 14) {
			MonsterHp = 500;
			reward = 100;
		}

		if (Type == 15) {
			MonsterHp = 5000;
			reward = 110;
		}

		if (Type == 16) {
			MonsterHp = 2000;
			reward = 120;
		}
		if (Type == 17) {
			MonsterHp = 3000;
			reward = 130;
		}
		if (Type == 18) {
			MonsterHp = 4000;
			reward = 140;
		}
		if (Type == 19) {
			MonsterHp = 5500;
			reward = 150;
		}
		if (Type == 20) {
			MonsterHp = 8000;
			reward = 160;
		}
		if (Type == 95) {
			MonsterHp = 50000;
			reward = 5000;
		}
		if (Type == 96) {
			MonsterHp = 40000;
			reward = 4000;
		}
		if (Type == 97) {
			MonsterHp = 30000;
			reward = 3000;
		}
		if (Type == 98) {
			MonsterHp = 20000;
			reward = 1000;
		}
		if (Type == 99) {
			MonsterHp = 10000;
			reward = 500;
		}

	}
	//get and set
	public int getMonsterInitX() {
		return MonsterInitX;
	}

	public int getMonsterInitY() {
		return MonsterInitY;
	}

	public int getMonsterX() {
		return MonsterX;
	}

	public int getMonsterY() {
		return MonsterY;
	}

	public double getMonsterSpeed() {
		return MonsterSpeed;
	}

	public void setMonsterInitX(int x) {
		MonsterInitX = x;
	}

	public void setMonsterInitY(int y) {
		MonsterInitY = y;
	}

	public void setMonsterX(int x) {
		MonsterX = x;
	}

	public void setMonsterY(int y) {
		MonsterY = y;
	}

	public void setMonsterSpeed(int S) {
		MonsterSpeed = S;
	}

	public void setpathflag(boolean flag, int index) {
		pathflag[index] = flag;
	}

	public boolean getpathflag(int index) {
		return pathflag[index];
	}

	public void setHp(int input) {
		MonsterHp = input;
	}

	public int getHp() {
		return MonsterHp;
	}

	public int getreward() {
		return reward;
	}

}
