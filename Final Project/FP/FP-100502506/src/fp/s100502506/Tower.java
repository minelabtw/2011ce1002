package fp.s100502506;

public class Tower {													//Tower
	private int TowerX;
	private int TowerY;
	private int costmoney;

	private double attack;
	private double attackspeend;
	private double attackrange;
	private int Type = 0;
	private String towerData;

	// Type=1 Fire
	// Type=2 Thunder
	// Type=3 Land
	// Type=4 Water
	// Type=5 Black
	// Type=6 Dark

	public Tower(int type, int X, int Y) {								//Tower type init
		TowerX = X;
		TowerY = Y;
		Type = type;
		if (Type == 1) {
			attack = 100;
			attackspeend = 0.9;
			attackrange = 5;
			costmoney = 100;
		}
		if (Type == 2) {
			attack = 30;
			attackspeend = 2;
			attackrange = 5;
			costmoney = 50;
		}
		if (Type == 3) {
			attack = 50;
			attackspeend = 0.5;
			attackrange = 7;
			costmoney = 200;
		}
		if (Type == 4) {
			attack = 100;
			attackspeend = 1.2;
			attackrange = 7;
			costmoney = 300;
		}
		if (Type == 5) {
			attack = 250;
			attackspeend = 0.4;
			attackrange = 6;
			costmoney = 1500;
		}
		if (Type == 6) {
			attack = 600;
			attackspeend = 0.2;
			attackrange = 8;
			costmoney = 3000;
		}
		if (Type == 0) {
			attack = 0;
			attackspeend = 0;
			attackrange = 0;
		}
		towerData=String.format("Attack %.0f    Attackspeend %.1f    Attackrange %.0f    Costmoney %d",attack,attackspeend,attackrange,costmoney);
		
	}
	//set and get
	public int getX() {
		return TowerX;
	}

	public int getY() {
		return TowerY;
	}

	public int setX() {
		return TowerX;
	}

	public int setY() {
		return TowerY;
	}

	public double getAttackSpeed() {
		return attackspeend;
	}

	public double getAttack() {
		return attack;
	}

	public double getAttackRange() {
		return attackrange;
	}

	public int getType() {
		return Type;
	}

	public int getcost() {
		return costmoney;
	}
	public String getTowerData()
	{
		return towerData;
	}
}
