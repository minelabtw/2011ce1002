package fp.s100502506;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.text.PlainDocument;

import fp.s100502506.PlayFrameWork.MonsterAction;


public class FrameWork extends JFrame {// Menu FrameWork
	private int mouse_X;
	private int mouse_Y;
	private PlayFrameWork chapter1;
	private PlayFrameWork chapter2;
	private PlayFrameWork chapter3;
	private PlayFrameWork chapter4;
	private PlayFrameWork chapter5;
	private About aboutPanel;
	private JButton startButton;
	private JButton optionButton;
	private JButton aboutButton;
	private JLabel Title;
	private JPanel Panel1;
	private JPanel Panel2;
	private JPanel Panel3;
	private Font font1;
	private ImageIcon background = new ImageIcon("image/Menu.png");
	private Image image = background.getImage();

	private ImageIcon GameStart = new ImageIcon("image/GameStart2.png");
	private Image image1 = GameStart.getImage();
	private ImageIcon Load = new ImageIcon("image/MapEditor.png");
	private Image image2 = Load.getImage();
	private ImageIcon About = new ImageIcon("image/About.png");
	private Image image4 = About.getImage();
	private ImageIcon Exit = new ImageIcon("image/Exit.png");
	private Image image5 = Exit.getImage();
	private ImageIcon fin = new ImageIcon("image/fin.png");
	private Image image6 = fin.getImage();

	private AudioInputStream audioIn[];
	private Clip sound[];
	private Timer timer = new Timer(10, new TimerListener());
	MapEditor mapEditor = new MapEditor();
	private boolean totwinflag = false;

	public FrameWork() throws FileNotFoundException {

		DrawBackGround test = new DrawBackGround();
		chapter1 = new PlayFrameWork(1);//init chapter1
		chapter1.setVisible(false);
		chapter1.setSize(950, 680);
		chapter1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		chapter2 = new PlayFrameWork(2);//init chapter2
		chapter2.setVisible(false);
		chapter2.setSize(950, 680);
		chapter2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		chapter3 = new PlayFrameWork(3);//init chapter2
		chapter3.setVisible(false);
		chapter3.setSize(950, 680);
		chapter3.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		chapter4 = new PlayFrameWork(4);//init chapter2
		chapter4.setVisible(false);
		chapter4.setSize(950, 680);
		chapter4.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		chapter5 = new PlayFrameWork(5);//init chapter2
		chapter5.setVisible(false);
		chapter5.setSize(950, 680);
		chapter5.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		aboutPanel = new About();
		aboutPanel.setVisible(false);
		aboutPanel.setSize(800, 640);
		mapEditor.setVisible(false);
		mapEditor.setSize(830, 920);
		File soundFile;
		DataLine.Info info;
		audioIn = new AudioInputStream[2];
		sound = new Clip[2];

		try {													//load sound
			soundFile = new File("sound/onestop.wav");
			audioIn[0] = AudioSystem.getAudioInputStream(soundFile);
			// load the sound into memory (opening)
			info = new DataLine.Info(Clip.class, audioIn[0].getFormat());
			sound[0] = (Clip) AudioSystem.getLine(info);
			sound[0].open(audioIn[0]);
		} catch (UnsupportedAudioFileException e) {
			// Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		sound[0].start();
		setLayout(new GridLayout());
		add(test);
		addMouseListener(new mouseAction());
		addMouseMotionListener(new mouseAction());
		timer.start();
	}

	class DrawBackGround extends JPanel {
		public void paintComponent(Graphics g) {
			g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
			if ((mouse_X >= 560) && (mouse_X <= 710) && (mouse_Y >= 280)
					&& (mouse_Y <= 310))// START
			{
				g.drawImage(image1, 560 - 5, 260 - 5, image1.getWidth(this),
						image1.getHeight(this), this);
			}
			if ((mouse_X >= 560) && (mouse_X <= 710) && (mouse_Y >= 335)
					&& (mouse_Y <= 365))// MAPEDITOR
			{
				g.drawImage(image2, 560, 260, image2.getWidth(this),
						image1.getHeight(this), this);
			}
			if ((mouse_X >= 560) && (mouse_X <= 640) && (mouse_Y >= 390)
					&& (mouse_Y <= 420))// about
			{
				g.drawImage(image4, 560, 210, image4.getWidth(this),
						image1.getHeight(this), this);
			}
			if ((mouse_X >= 560) && (mouse_X <= 635) && (mouse_Y >= 440)
					&& (mouse_Y <= 475))// exit
			{
				g.drawImage(image5, 560, 210, image5.getWidth(this),
						image1.getHeight(this), this);

			}
			if (totwinflag)
				g.drawImage(image6, 0, 0, getWidth(), getHeight(), this);
		}
	}

	class TimerListener implements ActionListener // Time Listener control winflag
	{
		public void actionPerformed(ActionEvent e) {
			if (chapter1.getWinflag()) 
			{
				chapter2.setVisible(true);
				chapter2.getMonTimer().start();
				chapter2.getATKTimer().start();
				chapter2.getpositionTimer().start();
				chapter1.setVisible(false);
				chapter1.getMonTimer().stop();
				chapter1.getATKTimer().stop();
				chapter1.getpositionTimer().stop();
				chapter1.setWinflag(false);
			}
			if (chapter2.getWinflag()) 
			{
				chapter3.setVisible(true);
				chapter3.getMonTimer().start();
				chapter3.getATKTimer().start();
				chapter3.getpositionTimer().start();
				chapter2.setVisible(false);
				chapter2.getMonTimer().stop();
				chapter2.getATKTimer().stop();
				chapter2.getpositionTimer().stop();
				chapter2.setWinflag(false);
			}
			if (chapter3.getWinflag()) 
			{
				chapter4.setVisible(true);
				chapter4.getMonTimer().start();
				chapter4.getATKTimer().start();
				chapter4.getpositionTimer().start();
				chapter3.setVisible(false);
				chapter3.getMonTimer().stop();
				chapter3.getATKTimer().stop();
				chapter3.getpositionTimer().stop();
				chapter3.setWinflag(false);
			}
			if (chapter4.getWinflag()) 
			{
				chapter5.setVisible(true);
				chapter5.getMonTimer().start();
				chapter5.getATKTimer().start();
				chapter5.getpositionTimer().start();
				chapter4.setVisible(false);
				chapter4.getMonTimer().stop();
				chapter4.getATKTimer().stop();
				chapter4.getpositionTimer().stop();
				chapter4.setWinflag(false);
			}
			if (chapter5.getWinflag()) 
			{
				chapter5.setVisible(false);
				chapter5.getMonTimer().stop();
				chapter5.getATKTimer().stop();
				chapter5.getpositionTimer().stop();
				totwinflag = true;
				setVisible(true);
				repaint();
				timer.stop();
			}
			repaint();
		}
	}

	class mouseAction implements MouseListener, MouseMotionListener {// mouselistener control change text 
		public void mouseClicked(MouseEvent e) {
		}

		public void mouseEntered(MouseEvent e) {
		}

		public void mouseExited(MouseEvent e) {
		}

		public void mousePressed(MouseEvent e) {

			if ((e.getX() >= 560) && (e.getX() <= 710) && (e.getY() >= 280)
					&& (e.getY() <= 310))// START
			{
				chapter1.setVisible(true);
				setVisible(false);
				chapter1.getMonTimer().start();
				chapter1.getATKTimer().start();
				chapter1.getpositionTimer().start();
			}
			if ((e.getX() >= 560) && (e.getX() <= 625) && (e.getY() >= 335)
					&& (e.getY() <= 365))// LOAD
			{
				mapEditor.setVisible(true);
				// setVisible(false);
			}
			if ((e.getX() >= 560) && (e.getX() <= 640) && (e.getY() >= 390)
					&& (e.getY() <= 420))// About
			{
				aboutPanel.setVisible(true);
				// setVisible(false);
			}
			if ((e.getX() >= 560) && (e.getX() <= 635) && (e.getY() >= 440)
					&& (e.getY() <= 475))// Exit
			{

				System.exit(0);
			}

		}

		public void mouseReleased(MouseEvent e) {
		}

		public void mouseDragged(MouseEvent e) {

		}

		public void mouseMoved(MouseEvent e) {
			mouse_X = e.getX();
			mouse_Y = e.getY();

		}

	}

}
