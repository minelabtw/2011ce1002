package fp.s100502506;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

public class About extends JFrame {//About Panel
	private JTextArea contentArea = new JTextArea();
	private JLabel Title = new JLabel();
	private Font font1;
	private Font font2;
	private String content;

	About() {//About constructor init
		font1 = new Font("monospaced", Font.BOLD + Font.ITALIC, 30);
		font2 = new Font("monospaced", Font.BOLD + Font.ITALIC, 25);
		Title = new JLabel("About");
		Title.setFont(font1);
		contentArea.setEditable(false);
		content = String.format("Game Maker�G     Blake K.C Lin   CSIE-100502506\n"
						+ "Making Time �G Two weeks\n"
						+ "The Picture of the Game �G The RPG Maker\n"
						+ "The Audio �G Windos onestep\n" 
						+"\n"
						+"Map extends to 5 chapters from 2 chapters\n"
						+"\n"
						+"Tower Button can view Damage Cost attackSpeed attackRange"
						+"\n"
						+"now version 1.2");
						
		contentArea.setText(content);
		contentArea.setFont(font2);

		add(Title, BorderLayout.NORTH);
		add(contentArea, BorderLayout.CENTER);
	}
}
