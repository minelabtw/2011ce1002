package fp.s100502506;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.text.AttributedCharacterIterator;
import javax.swing.Timer;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.filechooser.FileNameExtensionFilter;

import fp.s100502506.FrameWork.TimerListener;


import java.io.File;

public class MapEditor extends JFrame {
	private ImageIcon imageIcon;
	private Image image;
	private ImageIcon imageIcon1 = new ImageIcon("image/alert.png");
	private Image image1 = imageIcon1.getImage();
	private ImageIcon imageIcon2 = new ImageIcon("image/Up.png");
	private Image image2 = imageIcon2.getImage();
	private ImageIcon imageIcon3 = new ImageIcon("image/Left.png");
	private Image image3 = imageIcon3.getImage();
	private ImageIcon imageIcon4 = new ImageIcon("image/Right.png");
	private Image image4 = imageIcon4.getImage();
	private ImageIcon imageIcon5 = new ImageIcon("image/Down.png");
	private Image image5 = imageIcon5.getImage();
	private ImageIcon imageIcon6 = new ImageIcon("image/Start.png");
	private Image image6 = imageIcon6.getImage();
	private ImageIcon imageIcon7 = new ImageIcon("image/Stop.png");
	private Image image7 = imageIcon7.getImage();
	private ImageIcon imageIcon8 = new ImageIcon("image/Obstacle.png");
	private Image image8 = imageIcon8.getImage();
	private JPanel panel = new JPanel();
	private JLabel Label[][] = new JLabel[20][25];
	private boolean start[][] = new boolean[20][25];
	private boolean end[][] = new boolean[20][25];
	private boolean turn[][] = new boolean[20][25];
	private boolean obstacle[][] = new boolean[20][25];
	private int turndirection[][] = new int[20][25];
	private int control_X = 0;
	private int control_Y = 0;

	private JMenuBar muBar = new JMenuBar();
	private JMenu menu1 = new JMenu("File");
	private JToolBar toolBar1 = new JToolBar();
	private JMenuItem miEXIT = new JMenuItem("Exit");
	private JMenuItem miLOAD = new JMenuItem("Load");
	private JMenuItem miSAVE = new JMenuItem("Save");

	private Draw draw = new Draw();

	private JButton jbStartButton = new JButton(
			new ImageIcon("image/Start.png"));
	private JButton jbEndbButton = new JButton(new ImageIcon("image/Stop.png"));
	private JButton jbObstacleButton = new JButton(new ImageIcon(
			"image/Obstacle.png"));

	private JButton jbUpButton = new JButton(new ImageIcon("image/Up.png"));
	private JButton jbDownButton = new JButton(new ImageIcon("image/Down.png"));
	private JButton jbLeftButton = new JButton(new ImageIcon("image/Left.png"));
	private JButton jbRightButton = new JButton(
			new ImageIcon("image/Right.png"));

	private boolean isStart = false;
	private boolean isEnd = false;
	private boolean isUp = false;
	private boolean isDown = false;
	private boolean isLeft = false;
	private boolean isObstacle = false;
	private boolean isRight = false;

	private boolean isSet[][] = new boolean[20][25];

	private int type[][] = new int[20][25];

	private JFileChooser fileChooser = new JFileChooser();
	private boolean hasfile = false;
	private Timer timer = new Timer(10, new TimerAction());

	public MapEditor() {														//make path code

		draw.setLayout(new GridLayout(20, 25));
		for (int i = 0; i < 20; i++) {
			for (int j = 0; j < 25; j++) {
				// String xString=String.format("%d %d",j,i );
				start[i][j] = false;
				end[i][j] = false;
				obstacle[i][j] = false;
				Label[i][j] = new JLabel();// xString);
				Label[i][j].addMouseListener(new mouseAction());
				draw.add(Label[i][j]);
				isSet[i][j] = false;
			}
		}
		muBar.add(menu1);

		menu1.add(miLOAD);
		miLOAD.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int returnVal = fileChooser.showOpenDialog(MapEditor.this);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					imageIcon = new ImageIcon(fileChooser.getSelectedFile()
							.getPath());
					image = imageIcon.getImage();
					hasfile = true;
				}
				repaint();
			}
		});
		menu1.add(miSAVE);
		miSAVE.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				File outputFile = new File("Map.txt"); // new a input file
				try {
					java.io.PrintWriter outputString = new java.io.PrintWriter(
							outputFile);
					for (int i = 0; i < 20; i++) {
						for (int j = 0; j < 25; j++) {

							outputString.print(type[i][j]);
						}
						outputString.println();

					}
					outputString.flush();

				} catch (Exception e1) {
					// TODO Auto-generated catch block

				}
				;
				if (!outputFile.exists()) { // if file does not exist, show
											// error message
					System.err.println("File does not exist");
					System.exit(0);
				}

			}
		});

		menu1.add(miEXIT);
		miEXIT.addActionListener(new ActionListener() // add action listener
														// exit
		{

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		toolBar1.add(jbStartButton);
		jbStartButton.addActionListener(new ActionListener() // add action
																// listener line
				{

					@Override
					public void actionPerformed(ActionEvent e) {
						isStart = true;
						;
						isEnd = false;
						isObstacle = false;
						isUp = false;
						isDown = false;
						isLeft = false;
						isRight = false;
						repaint();
					}
				});
		toolBar1.add(jbEndbButton);
		jbEndbButton.addActionListener(new ActionListener() // add action
															// listener Rect
				{

					@Override
					public void actionPerformed(ActionEvent e) {
						isStart = false;
						;
						isEnd = true;
						isObstacle = false;
						isUp = false;
						isDown = false;
						isLeft = false;
						isRight = false;
						repaint();
					}
				});
		toolBar1.add(jbObstacleButton);
		jbObstacleButton.addActionListener(new ActionListener() // add action
																// listener
																// Circle
				{

					@Override
					public void actionPerformed(ActionEvent e) {
						isStart = false;
						;
						isEnd = false;
						isObstacle = true;
						isUp = false;
						isDown = false;
						isLeft = false;
						isRight = false;
						repaint();
					}
				});
		toolBar1.add(jbUpButton);
		jbUpButton.addActionListener(new ActionListener() // add action listener
															// Circle
				{

					@Override
					public void actionPerformed(ActionEvent e) {
						isStart = false;
						;
						isEnd = false;
						isObstacle = false;
						isUp = true;
						isDown = false;
						isLeft = false;
						isRight = false;
						repaint();
					}
				});
		toolBar1.add(jbDownButton);
		jbDownButton.addActionListener(new ActionListener() // add action
															// listener Circle
				{

					@Override
					public void actionPerformed(ActionEvent e) {
						isStart = false;
						;
						isEnd = false;
						isObstacle = false;
						isUp = false;
						isDown = true;
						isLeft = false;
						isRight = false;
						repaint();
					}
				});
		toolBar1.add(jbLeftButton);
		jbLeftButton.addActionListener(new ActionListener() // add action
															// listener Circle
				{

					@Override
					public void actionPerformed(ActionEvent e) {
						isStart = false;
						;
						isEnd = false;
						isObstacle = true;
						isUp = false;
						isDown = false;
						isLeft = true;
						isRight = false;
						repaint();
					}
				});
		toolBar1.add(jbRightButton);
		jbRightButton.addActionListener(new ActionListener() // add action
																// listener
																// Circle
				{

					@Override
					public void actionPerformed(ActionEvent e) {
						isStart = false;
						;
						isEnd = false;
						isObstacle = true;
						isUp = false;
						isDown = false;
						isLeft = false;
						isRight = true;
						repaint();
					}
				});

		toolBar1.setFloatable(true);
		add(toolBar1, BorderLayout.NORTH);

		setJMenuBar(muBar);

		add(draw, BorderLayout.CENTER);

	}

	class Draw extends JPanel// draw component
	{

		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			if (hasfile) {
				g.drawImage(image, 0, 0, getHeight(), getWidth(), this);
			}
		}

	}

	class TimerAction implements ActionListener // Time Listener
	{
		public void actionPerformed(ActionEvent e) {
			repaint();
		}
	}

	class mouseAction implements MouseListener {
		public void mouseEntered(MouseEvent e) {
			for (int i = 0; i < 20; i++) {
				for (int j = 0; j < 25; j++) {
					if (e.getSource() == Label[i][j]) {
						if (!isSet[i][j]) {
							if (isStart) {
								Label[i][j].setIcon(imageIcon6);
							}
							if (isEnd) {
								Label[i][j].setIcon(imageIcon7);
							}
							if (isObstacle) {
								Label[i][j].setIcon(imageIcon8);
							}
							if (isUp) {
								Label[i][j].setIcon(imageIcon2);
							}
							if (isDown) {
								Label[i][j].setIcon(imageIcon5);
							}
							if (isLeft) {
								Label[i][j].setIcon(imageIcon3);
							}
							if (isRight) {
								Label[i][j].setIcon(imageIcon4);
							}
						}
					}
				}
			}

		}

		public void mouseExited(MouseEvent e) {							//temp picture

			for (int i = 0; i < 20; i++) {
				for (int j = 0; j < 25; j++) {
					if (!isSet[i][j]) {
						if (e.getSource() == Label[i][j]) {
							Label[i][j].setIcon(null);
						}
					}
				}
			}

		}

		public void mousePressed(MouseEvent e) {						//set icon

			for (int i = 0; i < 20; i++) {
				for (int j = 0; j < 25; j++) {
					if (e.getSource() == Label[i][j]) {
						if (isStart) {
							Label[i][j].setIcon(imageIcon6);
							isSet[i][j] = true;
							type[i][j] = 1;
						}
						if (isEnd) {
							Label[i][j].setIcon(imageIcon7);
							isSet[i][j] = true;
							type[i][j] = 2;
						}
						if (isObstacle) {
							Label[i][j].setIcon(imageIcon8);
							isSet[i][j] = true;
							type[i][j] = 3;
						}
						if (isUp) {
							Label[i][j].setIcon(imageIcon2);
							isSet[i][j] = true;
							type[i][j] = 4;
						}
						if (isDown) {
							Label[i][j].setIcon(imageIcon5);
							isSet[i][j] = true;
							type[i][j] = 5;
						}
						if (isLeft) {
							Label[i][j].setIcon(imageIcon3);
							isSet[i][j] = true;
							type[i][j] = 6;
						}
						if (isRight) {
							Label[i][j].setIcon(imageIcon4);
							isSet[i][j] = true;
							type[i][j] = 7;
						}
					}
				}
			}

		}

		public void mouseReleased(MouseEvent e) {
		}

		public void mouseClicked(MouseEvent e) {

		}

	}
}
