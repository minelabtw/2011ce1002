package fp.s100502506;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class LoadMapData {													//load map txt
	private int pathcode[][] = new int[20][25];
	private int counter = 0;

	public LoadMapData(int inchapter) throws FileNotFoundException {
		String fileNameString = String.format("Map%d.txt", inchapter);
		java.io.File inputFile = new java.io.File(fileNameString);
		if (!inputFile.exists()) {
			System.err.print("ERROR-input");
			System.exit(0);
		}
		Scanner fileScanner = new Scanner(inputFile);

		while (fileScanner.hasNext()) {										//read path code
			String tempString = new String();
			tempString = fileScanner.next();
			for (int i = 0; i < 25; i++)
				pathcode[counter][i] = Integer.parseInt(""
						+ tempString.charAt(i));
			counter++;
		}
	}

	public int[][] getPathCode() {											//return path code
		return pathcode;
	}
}
