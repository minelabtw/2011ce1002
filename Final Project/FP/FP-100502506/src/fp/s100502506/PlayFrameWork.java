package fp.s100502506;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.color.ColorSpace;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.text.StyledEditorKit.BoldAction;

public class PlayFrameWork extends JFrame {

	private int chapter = 0;
	private int mountOfmonster = 10;
	private int monsterframe = 10;

	private Boolean isbuild[][] = new Boolean[20][25];
	private Boolean isRed[][] = new Boolean[20][25];
	private Boolean cantbuild[][] = new Boolean[20][25];
	// load image
	private ImageIcon background[] = new ImageIcon[5];
	private Image bcimage[] = new Image[5];
	private ImageIcon imageIcon1 = new ImageIcon("image/buildable2.png");
	private Image image1 = imageIcon1.getImage();
	private ImageIcon imageIcon2 = new ImageIcon("image/unbuildable.png");
	private Image image2 = imageIcon2.getImage();
	private ImageIcon imageIcon3 = new ImageIcon("image/creep.png");
	private Image image3 = imageIcon3.getImage();
	//
	private ImageIcon fireIcon = new ImageIcon("image/Tower/fire.png");
	private Image fireImage = fireIcon.getImage();
	private ImageIcon grassIcon = new ImageIcon("image/Tower/grass.png");
	private Image grassImage = grassIcon.getImage();
	private ImageIcon landIcon = new ImageIcon("image/Tower/land.png");
	private Image landImage = landIcon.getImage();
	private ImageIcon waterIcon = new ImageIcon("image/Tower/water.png");
	private Image waterImage = waterIcon.getImage();
	//
	private ImageIcon fireTowerIcon = new ImageIcon("image/Tower/redstone.png");
	private Image fireTowerImage = fireTowerIcon.getImage();
	private ImageIcon landTowerIcon = new ImageIcon(
			"image/Tower/yellowstone.png");
	private Image landTowerImage = landTowerIcon.getImage();
	private ImageIcon grassTowerIcon = new ImageIcon(
			"image/Tower/greenstone.png");
	private Image grassTowerImage = grassTowerIcon.getImage();
	private ImageIcon waterTowerIcon = new ImageIcon(
			"image/Tower/bluestone.png");
	private Image waterTowerImage = waterTowerIcon.getImage();
	private ImageIcon grayTowerIcon = new ImageIcon("image/Tower/graystone.png");
	private Image grayTowerImage = grayTowerIcon.getImage();
	private ImageIcon blackTowerIcon = new ImageIcon(
			"image/Tower/blackstone.png");
	private Image blackTowerImage = blackTowerIcon.getImage();
	//
	private ImageIcon C1_monster_1_icon = new ImageIcon(
			"image/Chapter_1/monster_1.png");
	private Image C1_monster_1_image = C1_monster_1_icon.getImage();
	private ImageIcon C1_monster_2_icon = new ImageIcon(
			"image/Chapter_1/monster_2.png");
	private Image C1_monster_2_image = C1_monster_2_icon.getImage();
	private ImageIcon C1_monster_3_icon = new ImageIcon(
			"image/Chapter_1/monster_3.png");
	private Image C1_monster_3_image = C1_monster_3_icon.getImage();
	private ImageIcon C1_monster_4_icon = new ImageIcon(
			"image/Chapter_1/monster_4.png");
	private Image C1_monster_4_image = C1_monster_4_icon.getImage();
	private ImageIcon C1_monster_5_icon = new ImageIcon(
			"image/Chapter_1/boss.png");
	private Image C1_monster_5_image = C1_monster_5_icon.getImage();

	private ImageIcon C2_monster_1_icon = new ImageIcon(
			"image/Chapter_2/monster_1.png");
	private Image C2_monster_1_image = C2_monster_1_icon.getImage();
	private ImageIcon C2_monster_2_icon = new ImageIcon(
			"image/Chapter_2/monster_2.png");
	private Image C2_monster_2_image = C2_monster_2_icon.getImage();
	private ImageIcon C2_monster_3_icon = new ImageIcon(
			"image/Chapter_2/monster_3.png");
	private Image C2_monster_3_image = C2_monster_3_icon.getImage();
	private ImageIcon C2_monster_4_icon = new ImageIcon(
			"image/Chapter_2/monster_4.png");
	private Image C2_monster_4_image = C2_monster_4_icon.getImage();
	private ImageIcon C2_monster_5_icon = new ImageIcon(
			"image/Chapter_2/boss.png");
	private Image C2_monster_5_image = C2_monster_5_icon.getImage();

	private ImageIcon C3_monster_1_icon = new ImageIcon(
			"image/Chapter_3/monster_1.png");
	private Image C3_monster_1_image = C3_monster_1_icon.getImage();
	private ImageIcon C3_monster_2_icon = new ImageIcon(
			"image/Chapter_3/monster_2.png");
	private Image C3_monster_2_image = C3_monster_2_icon.getImage();
	private ImageIcon C3_monster_3_icon = new ImageIcon(
			"image/Chapter_3/monster_3.png");
	private Image C3_monster_3_image = C3_monster_3_icon.getImage();
	private ImageIcon C3_monster_4_icon = new ImageIcon(
			"image/Chapter_3/monster_4.png");
	private Image C3_monster_4_image = C3_monster_4_icon.getImage();
	private ImageIcon C3_monster_5_icon = new ImageIcon(
			"image/Chapter_3/boss.png");
	private Image C3_monster_5_image = C3_monster_5_icon.getImage();

	private ImageIcon C4_monster_1_icon = new ImageIcon(
			"image/Chapter_4/monster_1.png");
	private Image C4_monster_1_image = C4_monster_1_icon.getImage();
	private ImageIcon C4_monster_2_icon = new ImageIcon(
			"image/Chapter_4/monster_2.png");
	private Image C4_monster_2_image = C4_monster_2_icon.getImage();
	private ImageIcon C4_monster_3_icon = new ImageIcon(
			"image/Chapter_4/monster_3.png");
	private Image C4_monster_3_image = C4_monster_3_icon.getImage();
	private ImageIcon C4_monster_4_icon = new ImageIcon(
			"image/Chapter_4/monster_4.png");
	private Image C4_monster_4_image = C4_monster_4_icon.getImage();
	private ImageIcon C4_monster_5_icon = new ImageIcon(
			"image/Chapter_4/boss.png");
	private Image C4_monster_5_image = C4_monster_5_icon.getImage();

	private ImageIcon C5_monster_1_icon = new ImageIcon(
			"image/Chapter_5/monster_1.png");
	private Image C5_monster_1_image = C5_monster_1_icon.getImage();
	private ImageIcon C5_monster_2_icon = new ImageIcon(
			"image/Chapter_5/monster_2.png");
	private Image C5_monster_2_image = C5_monster_2_icon.getImage();
	private ImageIcon C5_monster_3_icon = new ImageIcon(
			"image/Chapter_5/monster_3.png");
	private Image C5_monster_3_image = C5_monster_3_icon.getImage();
	private ImageIcon C5_monster_4_icon = new ImageIcon(
			"image/Chapter_5/monster_4.png");
	private Image C5_monster_4_image = C5_monster_4_icon.getImage();
	private ImageIcon C5_monster_5_icon = new ImageIcon(
			"image/Chapter_5/boss.png");
	private Image C5_monster_5_image = C5_monster_5_icon.getImage();
	private ImageIcon gameoverIcon = new ImageIcon("image/gameover.png");
	private Image gameover = gameoverIcon.getImage();
	private ImageIcon explodeIcon[] = new ImageIcon[8];
	private Image explodeImage[] = new Image[8];
	private ImageIcon flameIcon = new ImageIcon("image/firen_flame2.png");
	private Image flame = flameIcon.getImage();

	private ImageIcon bleedingIcon = new ImageIcon("image/bleed.png");
	private Image bleedingImage = bleedingIcon.getImage();
	private int bleedingcounter = 0;
	private ImageIcon thunderIcon[] = new ImageIcon[25];
	private Image thunderImage[] = new Image[25];

	private ImageIcon darkIcon = new ImageIcon("image/Gif/Dark.png");
	private Image dark = darkIcon.getImage();
	private ImageIcon iceIcon = new ImageIcon("image/Gif/ice.png");
	private Image ice = iceIcon.getImage();
	private ImageIcon rockIcon = new ImageIcon("image/Gif/rock.png");
	private Image rock = rockIcon.getImage();
	private ImageIcon blackIcon = new ImageIcon("image/Gif/black.png");
	private Image black = blackIcon.getImage();
	private int towerType = -1;

	private JLabel Label[][] = new JLabel[20][25];
	private boolean TowerSelected = false;

	// ///control Panel
	private JPanel towerPanel = new JPanel();
	private JLabel HPlabel = new JLabel();
	private JLabel moneyLabel = new JLabel();
	private JPanel infoPanel = new JPanel();
	private int HPdisplay = 0;
	private int moneydisplay = 0;
	private JButton towerButton[] = new JButton[10];

	private JPanel conPanel = new JPanel();
	// Timer
	private Timer MonsterTimer = new Timer(150, new MonsterAction());
	private Timer AttackTimer = new Timer(150, new AttackAction());
	private Timer explodeTimer = new Timer(50, new explodeAction());
	private Timer AttackEffectTimer = new Timer(120, new AttackEffectAction());
	private Timer positionTimer = new Timer(10, new positionAction());
	private Timer bleedTimer = new Timer(500, new bleedAction());

	private int Frame = 32;
	private boolean bleedingflag = false;
	private boolean[] pathflag = new boolean[100];
	private boolean winflag = false;
	private Monster Monsters[][] = new Monster[5][mountOfmonster];

	private boolean[][] MonsterOut = new boolean[5][mountOfmonster];
	private int counter = 0;
	private int Towercounter = 0;
	private Tower Towers[] = new Tower[100];
	private BuffTower BTowers[] = new BuffTower[100];
	private boolean hasTower[] = new boolean[100];
	private boolean hasMonster[][] = new boolean[5][mountOfmonster];
	private LoadMapData mapData;
	private int startdir = 0;
	private int start_X = 0;
	private int start_Y = 0;
	private int checkpoint_X[] = new int[100];
	private int checkpoint_Y[] = new int[100];
	private int checkpoint_Dir[] = new int[100];
	private Clip[] sound;
	private AudioInputStream[] audioIn;
	private boolean isExplode = false;
	private int[] explode_X = new int[1000];
	private int[] explode_Y = new int[1000];
	private int explodeCounter;
	private int explodeCounter2[] = new int[1000];
	private boolean hadexplode[] = new boolean[1000];
	private boolean isexplode[] = new boolean[1000];
	private Color colors[] = new Color[7];
	private int time = 0;

	private int framecounter[][] = new int[5][mountOfmonster];
	private int thundercounter[][] = new int[5][mountOfmonster];
	private int icecounter[][] = new int[5][mountOfmonster];
	private int darkcounter[][] = new int[5][mountOfmonster];
	private int rockcounter[][] = new int[5][mountOfmonster];
	private int blackcounter[][] = new int[5][mountOfmonster];
	private Player player1;
	private String hp;
	private String money;
	private boolean deathflag = false;

	// playframework init
	PlayFrameWork(int inchapter) throws FileNotFoundException {

		for (int i = 1; i < 6; i++) {// load background png
			String temp = String.format("image/bc/BackGround%d.png", i);
			background[i - 1] = new ImageIcon(temp);
			bcimage[i - 1] = background[i - 1].getImage();
		}
		for (int i = 0; i < 8; i++) {// load explode
			String temp = String.format("image/Gif/explode_%d.png", i + 1);
			explodeIcon[i] = new ImageIcon(temp);
			explodeImage[i] = explodeIcon[i].getImage();
		}
		for (int i = 0; i < 5; i++) {// load thunder effect
			String temp = String.format("image/Gif/thunder_%d.png", i + 1);
			thunderIcon[i] = new ImageIcon(temp);
			thunderImage[i] = thunderIcon[i].getImage();
		}
		for (int i = 0; i < 100; i++) {// init
			hadexplode[i] = false;
			isexplode[i] = false;
			explode_X[i] = 0;
			explode_Y[i] = 0;
		}
		colors[0] = Color.RED;
		colors[1] = Color.ORANGE;
		colors[2] = Color.YELLOW;
		colors[3] = Color.GREEN;
		colors[4] = Color.BLUE;
		colors[5] = Color.CYAN;
		colors[6] = Color.PINK;
		chapter = inchapter;
		mapData = new LoadMapData(chapter);// load mapdata
		// init
		for (int i = 0; i < 100; i++)
			hasTower[i] = false;
		for (int j = 1; j <= 5; j++) {
			if (chapter == j) {
				for (int i = 0; i < mountOfmonster; i++) {
					Monsters[0][i] = new Monster(j + 3 * (j - 1));
					Monsters[1][i] = new Monster(j + 1 + 3 * (j - 1));
					Monsters[2][i] = new Monster(j + 2 + 3 * (j - 1));
					Monsters[3][i] = new Monster(j + 3 + 3 * (j - 1));
					Monsters[4][i] = new Monster(100 - j);
				}
				break;
			}
		}
		for (int j = 0; j < 5; j++) {
			for (int i = 0; i < mountOfmonster; i++) {
				Monsters[j][i].setpathflag(true, 0);
				MonsterOut[j][i] = false;
			}
		}

		for (int i = 0; i < 20; i++) {
			for (int j = 0; j < 25; j++) {
				if (mapData.getPathCode()[i][j] == 1) {
					for (int b = 0; b < 5; b++) {
						for (int a = 0; a < mountOfmonster; a++) {
							Monsters[b][a].setMonsterX(j);
							Monsters[b][a].setMonsterY(i);
						}
					}
				}
			}
		}

		// //Control Panel GUI
		conPanel.setLayout(new GridLayout());
		towerPanel.setLayout(new GridLayout(6, 2, 5, 5));
		for (int i = 0; i < 10; i++) {
			towerButton[i] = new JButton();
			towerButton[i].setBackground(new Color(238, 238, 238));
			towerPanel.add(towerButton[i]);
			towerButton[i].addActionListener(new buttionAction());
		}
		towerPanel.add(moneyLabel);
		towerPanel.add(HPlabel);

		add(towerPanel, BorderLayout.CENTER);

		conPanel.add(towerPanel);
		towerButton[0].setIcon(fireTowerIcon);
		towerButton[1].setIcon(landTowerIcon);
		towerButton[2].setIcon(grassTowerIcon);
		towerButton[3].setIcon(waterTowerIcon);
		towerButton[4].setIcon(grayTowerIcon);
		towerButton[5].setIcon(blackTowerIcon);
		towerButton[6].setIcon(fireIcon);
		towerButton[7].setIcon(grassIcon);
		towerButton[8].setIcon(landIcon);
		towerButton[9].setIcon(waterIcon);

		//
		for (int i = 0; i < 20; i++) {
			for (int j = 0; j < 25; j++) {
				Label[i][j] = new JLabel();
				Label[i][j].addMouseListener(new mouseAction());
				isbuild[i][j] = false;
				isRed[i][j] = false;
				cantbuild[i][j] = false;
			}
		}
		DrawBackGround backGround1 = new DrawBackGround();
		backGround1.setLayout(new GridLayout(20, 25));
		// setLayout(new GridLayout());
		for (int i = 0; i < 20; i++) {
			for (int j = 0; j < 25; j++) {
				backGround1.add(Label[i][j]);
			}
		}
		add(backGround1, BorderLayout.CENTER);
		add(conPanel, BorderLayout.EAST);

		setRed();
		startdir = checkStartkDirection();
		checkpoint_Dir[0] = checkNextDirection(startdir, start_X, start_Y, 0);

		for (int i = 1; i < 100; i++) {
			if (checkpoint_Dir[i - 1] == -1)
				break;
			checkpoint_Dir[i] = checkNextDirection(checkpoint_Dir[i - 1],
					checkpoint_X[i - 1], checkpoint_Y[i - 1], i);
		}
		for (int i = 0; i < 25; i++) {
			if (checkpoint_Dir[i] == -1)
				break;
		}
		File soundFile;
		DataLine.Info info;
		audioIn = new AudioInputStream[2];
		sound = new Clip[2];
		try {
			soundFile = new File("sound/Bomb2.wav");
			audioIn[1] = AudioSystem.getAudioInputStream(soundFile);
			// load the sound into memory (car)
			info = new DataLine.Info(Clip.class, audioIn[1].getFormat());
			sound[1] = (Clip) AudioSystem.getLine(info);
			sound[1].open(audioIn[1]);
		} catch (UnsupportedAudioFileException e) {
			// Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// Auto-generated catch block
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		player1 = new Player(chapter);
		AttackEffectTimer.start();
		explodeTimer.start();

		bleedTimer.start();
		hp = String.format("HP:\n %d", player1.getHP());
		money = String.format("$:\n %d", player1.getMoney());
		HPlabel.setText(hp);
		moneyLabel.setText(money);
		for(int i=0;i<6;i++)
			towerButton[i].setToolTipText(new Tower(i+1, 0,0).getTowerData());
		
		
	}

	public Timer getpositionTimer() {// get position timer
		return positionTimer;
	}

	class explodeAction implements ActionListener {// explode animation

		public void actionPerformed(ActionEvent e) {

			for (int i = 0; i < 1000; i++) {

				if (isexplode[i]) {

					explodeCounter2[i]++;
					if (explodeCounter2[i] >= 5) {
						sound[1].setFramePosition(0);
						sound[1].start();
						isexplode[i] = false;
						explodeCounter2[i] = 0;
					}
				}

			}
			repaint();

		}

	}

	class positionAction implements ActionListener {// check position
		public void actionPerformed(ActionEvent arg0) {
			if (player1.getHP() <= 0) {
				AttackEffectTimer.stop();
				explodeTimer.stop();
				MonsterTimer.stop();
				deathflag = true;
				repaint();
			}
			for (int j = 0; j < 5; j++) {
				for (int i = 0; i < mountOfmonster; i++) {
					if (Monsters[j][i].getHp() > 0) {
						winflag = false;

					} else {
						winflag = true;
					}
				}
			}
		}
	}

	class bleedAction implements ActionListener { // bleed animation
		public void actionPerformed(ActionEvent arg0) {
			bleedingcounter++;
			if (bleedingcounter >= 1) {
				bleedingcounter = 0;
				bleedingflag = false;
			}
			repaint();
		}
	}

	class AttackAction implements ActionListener { // attack action
		public void actionPerformed(ActionEvent e) {
			for (int j = 0; j < 5; j++) {
				for (int i = 0; i < mountOfmonster; i++) {
					if (Monsters[j][i].getHp() <= 0) {
						hasMonster[j][i] = false;

					}
				}
			}
			for (int a = 0; a < 100; a++) {
				if (hasTower[a]) {

					for (int j = 0; j < 5; j++) {
						for (int i = 0; i < mountOfmonster; i++) {
							if (hasMonster[j][i]) {
								if (isAttack(Towers[a], Monsters[j][i])) {
									damge(Towers[a], Monsters[j][i]);
									if (!hadexplode[explodeCounter]) {
										if (Monsters[j][i].getHp() <= 0) {
											explode_X[explodeCounter] = Monsters[j][i]
													.getMonsterX();
											explode_Y[explodeCounter] = Monsters[j][i]
													.getMonsterY();
											hadexplode[explodeCounter] = true;
											isexplode[explodeCounter] = true;
											explodeCounter++;
											player1.setMoney(player1.getMoney()
													+ Monsters[j][i]
															.getreward());
											setMoneyText();

										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	class AttackEffectAction implements ActionListener {// attackeffect
														// animation
		public void actionPerformed(ActionEvent e) {
			for (int a = 0; a < 100; a++) {
				if (hasTower[a]) {
					for (int j = 0; j < 5; j++) {
						for (int i = 0; i < mountOfmonster; i++) {
							if (hasMonster[j][i]) {
								if (isAttack(Towers[a], Monsters[j][i])) {
									framecounter[j][i]++;
									thundercounter[j][i]++;
									darkcounter[j][i]++;
									icecounter[j][i]++;
									rockcounter[j][i]++;
									blackcounter[j][i]++;
									repaint();
								}
							}
						}
					}
				}
			}
		}
	}

	class MonsterAction implements ActionListener {// monster walk path
		public void actionPerformed(ActionEvent e) {
			if (counter <= monsterframe * mountOfmonster - monsterframe) {
				if ((counter % monsterframe) == 0) {
					MonsterOut[0][counter / monsterframe] = true;
					hasMonster[0][counter / monsterframe] = true;
				}
			} else if (counter <= monsterframe * mountOfmonster * 2
					- monsterframe
					&& counter >= monsterframe * mountOfmonster - monsterframe
							+ 1) {
				if ((counter % monsterframe) == 0) {
					MonsterOut[1][(counter - monsterframe * mountOfmonster)
							/ monsterframe] = true;
					hasMonster[1][(counter - monsterframe * mountOfmonster)
							/ monsterframe] = true;
				}
			} else if (counter <= monsterframe * mountOfmonster * 3
					- monsterframe
					&& counter >= monsterframe * mountOfmonster * 2
							- monsterframe + 1) {
				if ((counter % monsterframe) == 0) {
					MonsterOut[2][(counter - monsterframe * mountOfmonster * 2)
							/ monsterframe] = true;
					hasMonster[2][(counter - monsterframe * mountOfmonster * 2)
							/ monsterframe] = true;
				}
			} else if (counter <= monsterframe * mountOfmonster * 4
					- monsterframe
					&& counter >= monsterframe * mountOfmonster * 3
							- monsterframe + 1) {
				if ((counter % monsterframe) == 0) {
					MonsterOut[3][(counter - monsterframe * mountOfmonster * 3)
							/ monsterframe] = true;
					hasMonster[3][(counter - monsterframe * mountOfmonster * 3)
							/ monsterframe] = true;
				}
			} else if (counter <= monsterframe * mountOfmonster * 5
					- monsterframe
					&& counter >= monsterframe * mountOfmonster * 4
							- monsterframe + 1) {
				if ((counter % monsterframe) == 0) {
					MonsterOut[4][(counter - monsterframe * mountOfmonster * 4)
							/ monsterframe] = true;
					hasMonster[4][(counter - monsterframe * mountOfmonster * 4)
							/ monsterframe] = true;
				}
			}
			for (int j = 0; j < 5; j++) {
				for (int i = 0; i < mountOfmonster; i++) {
					if (Monsters[j][i].getHp() > 0) {
						if (MonsterOut[j][i])
							pathCheck(Monsters[j][i]);
					}
				}
			}
			counter++;
			repaint();
		}

	}

	class DrawBackGround extends JPanel {// draw panel
		public void paintComponent(Graphics g) {
			g.drawImage(bcimage[chapter - 1], 0, 0, getWidth(), getHeight(),
					this);
			for (int i = 0; i < 1000; i++) {

				if (isexplode[i]) {
					g.drawImage(explodeImage[explodeCounter2[i]], explode_X[i]
							* Frame - Frame, explode_Y[i] * Frame - Frame, 106,
							106, this);
				}

			}
			for (int j = 0; j < 5; j++) {
				for (int i = 0; i < mountOfmonster; i++) {
					if (Monsters[j][i].getHp() > 0) {
						if (chapter == 1) {

							if (hasMonster[0][i])
								g.drawImage(C1_monster_1_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
							if (hasMonster[1][i])
								g.drawImage(C1_monster_2_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
							if (hasMonster[2][i])
								g.drawImage(C1_monster_3_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
							if (hasMonster[3][i])
								g.drawImage(C1_monster_4_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
							if (hasMonster[4][i])
								g.drawImage(C1_monster_5_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
						}
						if (chapter == 2) {
							if (hasMonster[0][i])
								g.drawImage(C2_monster_1_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
							if (hasMonster[1][i])
								g.drawImage(C2_monster_2_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
							if (hasMonster[2][i])
								g.drawImage(C2_monster_3_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
							if (hasMonster[3][i])
								g.drawImage(C2_monster_4_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
							if (hasMonster[4][i])
								g.drawImage(C2_monster_5_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
						}
						if (chapter == 3) {
							if (hasMonster[0][i])
								g.drawImage(C3_monster_1_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
							if (hasMonster[1][i])
								g.drawImage(C3_monster_2_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
							if (hasMonster[2][i])
								g.drawImage(C3_monster_3_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
							if (hasMonster[3][i])
								g.drawImage(C3_monster_4_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
							if (hasMonster[4][i])
								g.drawImage(C3_monster_5_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
						}
						if (chapter == 4) {
							if (hasMonster[0][i])
								g.drawImage(C4_monster_1_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
							if (hasMonster[1][i])
								g.drawImage(C4_monster_2_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
							if (hasMonster[2][i])
								g.drawImage(C4_monster_3_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
							if (hasMonster[3][i])
								g.drawImage(C4_monster_4_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
							if (hasMonster[4][i])
								g.drawImage(C4_monster_5_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
						}
						if (chapter == 5) {
							if (hasMonster[0][i])
								g.drawImage(C5_monster_1_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
							if (hasMonster[1][i])
								g.drawImage(C5_monster_2_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
							if (hasMonster[2][i])
								g.drawImage(C5_monster_3_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
							if (hasMonster[3][i])
								g.drawImage(C5_monster_4_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
							if (hasMonster[4][i])
								g.drawImage(C5_monster_5_image,
										Monsters[j][i].getMonsterX() * Frame,
										Monsters[j][i].getMonsterY() * Frame,
										Frame, Frame, this);
						}
					}
				}
			}
			for (int a = 0; a < 100; a++) {
				if (hasTower[a]) {
					for (int j = 0; j < 5; j++) {
						for (int i = 0; i < mountOfmonster; i++) {
							if (hasMonster[j][i]) {
								if (isAttack(Towers[a], Monsters[j][i])) {
									if (Towers[a].getType() == 1) {
										if (framecounter[j][i] >= 10)
											framecounter[j][i] = 0;
										g.drawImage(flame, Towers[a].getX()
												* Frame + Frame / 2,
												Towers[a].getY() * Frame
														- Frame / 2,
												Monsters[j][i].getMonsterX()
														* Frame + Frame / 2,
												Monsters[j][i].getMonsterY()
														* Frame + Frame / 2,
												60 * framecounter[j][i], 0,
												60 * (framecounter[j][i] + 1),
												64, this);

									}
									if (Towers[a].getType() == 2) {
										if (thundercounter[j][i] >= 5)
											thundercounter[j][i] = 0;
										g.drawImage(
												thunderImage[thundercounter[j][i]],
												Monsters[j][i].getMonsterX()
														* Frame - Frame,
												Monsters[j][i].getMonsterY()
														* Frame - Frame, 96,
												96, this);
									}
									if (Towers[a].getType() == 6) {
										if (darkcounter[j][i] >= 9)
											darkcounter[j][i] = 0;
										g.drawImage(dark,
												Monsters[j][i].getMonsterX()
														* Frame - Frame,
												Monsters[j][i].getMonsterY()
														* Frame - Frame,
												Monsters[j][i].getMonsterX()
														* Frame + Frame * 2,
												Monsters[j][i].getMonsterY()
														* Frame + Frame * 2,
												96 * darkcounter[j][i], 0,
												96 * (darkcounter[j][i] + 1),
												96, this);

									}
									if (Towers[a].getType() == 4) {
										if (icecounter[j][i] >= 6)
											icecounter[j][i] = 0;
										g.drawImage(ice,
												Monsters[j][i].getMonsterX()
														* Frame - Frame,
												Monsters[j][i].getMonsterY()
														* Frame - Frame,
												Monsters[j][i].getMonsterX()
														* Frame + Frame * 2,
												Monsters[j][i].getMonsterY()
														* Frame + Frame * 2,
												96 * icecounter[j][i], 0,
												96 * (icecounter[j][i] + 1),
												96, this);

									}
									if (Towers[a].getType() == 3) {
										if (rockcounter[j][i] >= 6)
											rockcounter[j][i] = 0;
										g.drawImage(rock,
												Monsters[j][i].getMonsterX()
														* Frame - Frame,
												Monsters[j][i].getMonsterY()
														* Frame - Frame,
												Monsters[j][i].getMonsterX()
														* Frame + Frame * 2,
												Monsters[j][i].getMonsterY()
														* Frame + Frame * 2,
												96 * rockcounter[j][i], 0,
												96 * (rockcounter[j][i] + 1),
												96, this);

									}
									if (Towers[a].getType() == 5) {
										if (blackcounter[j][i] >= 7)
											blackcounter[j][i] = 0;
										g.drawImage(black,
												Monsters[j][i].getMonsterX()
														* Frame - Frame,
												Monsters[j][i].getMonsterY()
														* Frame - Frame,
												Monsters[j][i].getMonsterX()
														* Frame + Frame * 2,
												Monsters[j][i].getMonsterY()
														* Frame + Frame * 2,
												96 * blackcounter[j][i], 0,
												96 * (blackcounter[j][i] + 1),
												96, this);

									}
								}
							}

						}
					}
				}
			}
			if (bleedingflag) {
				g.drawImage(bleedingImage, 0, 0, getWidth(), getHeight(), this);
			}
			if (deathflag) {
				g.drawImage(gameover, 0, 0, getWidth(), getHeight(), this);
			}

		}
	}

	class buttionAction implements ActionListener {// tower button
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == towerButton[0]) {
				TowerSelected = true;
				towerType = 1;
			}
			if (e.getSource() == towerButton[1]) {
				TowerSelected = true;
				towerType = 2;
			}
			if (e.getSource() == towerButton[2]) {
				TowerSelected = true;
				towerType = 3;
			}
			if (e.getSource() == towerButton[3]) {
				TowerSelected = true;
				towerType = 4;
			}
			if (e.getSource() == towerButton[4]) {
				TowerSelected = true;
				towerType = 5;
			}
			if (e.getSource() == towerButton[5]) {
				TowerSelected = true;
				towerType = 6;
			}
			if (e.getSource() == towerButton[6]) {
				TowerSelected = true;
				towerType = 7;
			}
			if (e.getSource() == towerButton[7]) {
				TowerSelected = true;
				towerType = 8;
			}
			if (e.getSource() == towerButton[8]) {
				TowerSelected = true;
				towerType = 9;
			}
			if (e.getSource() == towerButton[9]) {
				TowerSelected = true;
				towerType = 10;
			}
		}

	}

	class mouseAction implements MouseListener {
		public void mouseEntered(MouseEvent e) {// set tower
			if (TowerSelected) {
				for (int i = 0; i < 20; i++) {
					for (int j = 0; j < 25; j++) {
						if (e.getSource() == Label[i][j]) {
							if (!isbuild[i][j]) {
								if (isRed[i][j]) {
									Label[i][j].setIcon(imageIcon2);
								} else if (!isRed[i][j]) {

									Label[i][j].setIcon(imageIcon1);
								}
							}
						}
					}
				}
			}
		}

		public void mouseExited(MouseEvent e) {// tip

			for (int i = 0; i < 20; i++) {
				for (int j = 0; j < 25; j++) {
					if (e.getSource() == Label[i][j]) {
						if (!isbuild[i][j])
							Label[i][j].setIcon(null);
					}
				}
			}

		}

		public void mousePressed(MouseEvent e) {// set towers
			if (TowerSelected) {
				TowerSelected = false;
				for (int i = 0; i < 20; i++) {
					for (int j = 0; j < 25; j++) {
						if (e.getSource() == Label[i][j]) {
							if (!cantbuild[i][j]) {
								if (towerType == 1) {

									Towers[Towercounter] = new Tower(towerType,
											j, i);
									towerType = -1;
									if (player1.getMoney() < Towers[Towercounter]
											.getcost())
										break;
									Label[i][j].setIcon(fireTowerIcon);
									hasTower[Towercounter] = true;
									player1.setMoney(player1.getMoney()
											- Towers[Towercounter].getcost());
									setMoneyText();
									Towercounter++;

								}
								if (towerType == 2) {

									Towers[Towercounter] = new Tower(towerType,
											j, i);
									towerType = -1;
									if (player1.getMoney() < Towers[Towercounter]
											.getcost())
										break;
									Label[i][j].setIcon(landTowerIcon);
									hasTower[Towercounter] = true;
									player1.setMoney(player1.getMoney()
											- Towers[Towercounter].getcost());
									setMoneyText();
									Towercounter++;
								}
								if (towerType == 3) {

									Towers[Towercounter] = new Tower(towerType,
											j, i);
									towerType = -1;
									if (player1.getMoney() < Towers[Towercounter]
											.getcost())
										break;
									Label[i][j].setIcon(grassTowerIcon);
									hasTower[Towercounter] = true;
									player1.setMoney(player1.getMoney()
											- Towers[Towercounter].getcost());
									setMoneyText();
									Towercounter++;
								}
								if (towerType == 4) {

									Towers[Towercounter] = new Tower(towerType,
											j, i);
									towerType = -1;
									if (player1.getMoney() < Towers[Towercounter]
											.getcost())
										break;
									Label[i][j].setIcon(waterTowerIcon);
									hasTower[Towercounter] = true;
									player1.setMoney(player1.getMoney()
											- Towers[Towercounter].getcost());
									setMoneyText();
									Towercounter++;
								}
								if (towerType == 5) {

									Towers[Towercounter] = new Tower(towerType,
											j, i);
									towerType = -1;
									if (player1.getMoney() < Towers[Towercounter]
											.getcost())
										break;
									Label[i][j].setIcon(grayTowerIcon);
									hasTower[Towercounter] = true;
									player1.setMoney(player1.getMoney()
											- Towers[Towercounter].getcost());
									setMoneyText();
									Towercounter++;
								}
								if (towerType == 6) {

									Towers[Towercounter] = new Tower(towerType,
											j, i);
									towerType = -1;
									if (player1.getMoney() < Towers[Towercounter]
											.getcost())
										break;
									Label[i][j].setIcon(blackTowerIcon);
									hasTower[Towercounter] = true;
									player1.setMoney(player1.getMoney()
											- Towers[Towercounter].getcost());
									setMoneyText();
									Towercounter++;
								}
								if (towerType == 7) {

									Towers[Towercounter] = new Tower(towerType,
											j, i);
									towerType = -1;
									if (player1.getMoney() < Towers[Towercounter]
											.getcost())
										break;
									Label[i][j].setIcon(fireIcon);
									hasTower[Towercounter] = true;
									player1.setMoney(player1.getMoney()
											- Towers[Towercounter].getcost());
									setMoneyText();
									Towercounter++;
								}
								if (towerType == 8) {

									Towers[Towercounter] = new Tower(towerType,
											j, i);
									towerType = -1;
									if (player1.getMoney() < Towers[Towercounter]
											.getcost())
										break;
									Label[i][j].setIcon(grassIcon);
									hasTower[Towercounter] = true;
									player1.setMoney(player1.getMoney()
											- Towers[Towercounter].getcost());
									setMoneyText();
									Towercounter++;
								}
								if (towerType == 9) {

									Towers[Towercounter] = new Tower(towerType,
											j, i);
									towerType = -1;
									if (player1.getMoney() < Towers[Towercounter]
											.getcost())
										break;
									Label[i][j].setIcon(landIcon);
									hasTower[Towercounter] = true;
									player1.setMoney(player1.getMoney()
											- Towers[Towercounter].getcost());
									setMoneyText();
									Towercounter++;
								}
								if (towerType == 10) {

									Towers[Towercounter] = new Tower(towerType,
											j, i);
									towerType = -1;
									if (player1.getMoney() < Towers[Towercounter]
											.getcost())
										break;
									Label[i][j].setIcon(waterIcon);
									hasTower[Towercounter] = true;
									player1.setMoney(player1.getMoney()
											- Towers[Towercounter].getcost());
									setMoneyText();
									Towercounter++;
								}

								isbuild[i][j] = true;
							} else {
								TowerSelected = true;
							}
						}
					}
				}
			}
		}

		public void mouseReleased(MouseEvent e) {
		}

		public void mouseClicked(MouseEvent e) {

		}

	}

	public void setRed() {// set can't build
		for (int i = 0; i < 20; i++) {
			for (int j = 0; j < 25; j++) {
				if (mapData.getPathCode()[i][j] != 0) {
					cantbuild[i][j] = true;
					isRed[i][j] = true;
				}
			}
		}
	}

	public Timer getMonTimer() {// get timer
		return MonsterTimer;
	}

	public Timer getATKTimer() {// get timer
		return AttackTimer;
	}

	public void pathCheck(Monster M) {// set path 1 for up 2 for right 3 for
										// down 4 for left
		for (int i = 0; i < 25; i++) {
			if (M.getMonsterX() == checkpoint_Y[i]
					&& M.getMonsterY() == checkpoint_X[i]) {
				M.setpathflag(false, i);
				M.setpathflag(true, i + 1);
			}
		}
		if (M.getpathflag(0)) {
			if (startdir == 1)
				monsterturnUp(M);
			if (startdir == 2)
				monsterturnRight(M);
			if (startdir == 3)
				monsterturnDown(M);
			if (startdir == 4)
				monsterturnLeft(M);
		}
		for (int i = 1; i < 20; i++) {
			if (M.getpathflag(i)) {
				if (checkpoint_Dir[i - 1] == 1) {
					monsterturnUp(M);
				}
				if (checkpoint_Dir[i - 1] == 2) {
					monsterturnRight(M);
				}
				if (checkpoint_Dir[i - 1] == 3) {
					monsterturnDown(M);
				}
				if (checkpoint_Dir[i - 1] == 4) {
					monsterturnLeft(M);
				}
				if (checkpoint_Dir[i - 1] == -1) {
					M.setHp(0);
					player1.setHP(player1.getHP() - 1);
					setHPText();
					bleedingflag = true;
					break;
				}
			}
		}
	}

	public double distance(int Tx, int Ty, int Mx, int My) {// distance fuction
		double distance = 0;
		distance = Math.sqrt(Math.pow((Tx - Mx), 2) + Math.pow((Ty - My), 2));

		return distance;
	}

	public Boolean isAttack(Tower t, Monster m) {// check if attack
		boolean flag = false;
		if (t.getAttackRange() >= distance(t.getX(), t.getY(), m.getMonsterX(),
				m.getMonsterY())) {
			flag = true;
		}
		return flag;
	}

	public void damge(Tower t, Monster m) {// damage
		m.setHp((int) (m.getHp() - t.getAttackSpeed() * t.getAttack()));

	}

	public void monsterturnRight(Monster M) {// right
		M.setMonsterX((int) (M.getMonsterX() + M.getMonsterSpeed()));
	}

	public void monsterturnUp(Monster M) {// up
		M.setMonsterY((int) (M.getMonsterY() - M.getMonsterSpeed()));
	}

	public void monsterturnDown(Monster M) {// down
		M.setMonsterY((int) (M.getMonsterY() + M.getMonsterSpeed()));
	}

	public void monsterturnLeft(Monster M) {// left
		M.setMonsterX((int) (M.getMonsterX() - M.getMonsterSpeed()));
	}

	public int checkStartkDirection() {// check start direction
		int direct = 0;
		for (int i = 0; i < 20; i++) {
			for (int j = 0; j < 25; j++) {
				if (mapData.getPathCode()[i][j] == 1) {
					start_X = i;
					start_Y = j;
					if ((i - 1) >= 0 && (j - 1) >= 0 && (j + 1) <= 24
							&& (i + 1) <= 19) {
						if (mapData.getPathCode()[i - 1][j - 1] == 4) {
							direct = 1;
						}
						if (mapData.getPathCode()[i][j - 1] == 4) {
							direct = 1;
						}
						if (mapData.getPathCode()[i + 1][j - 1] == 4) {
							direct = 1;
						}
						if (mapData.getPathCode()[i - 1][j] == 4) {
							direct = 1;
						}
						if (mapData.getPathCode()[i + 1][j] == 4) {
							direct = 1;
						}
						if (mapData.getPathCode()[i - 1][j + 1] == 4) {
							direct = 1;
						}
						if (mapData.getPathCode()[i][j + 1] == 4) {
							direct = 1;
						}
						if (mapData.getPathCode()[i + 1][j + 1] == 4) {
							direct = 1;
						}

						if (mapData.getPathCode()[i - 1][j - 1] == 5) {
							direct = 3;
						}
						if (mapData.getPathCode()[i][j - 1] == 5) {
							direct = 3;
						}
						if (mapData.getPathCode()[i + 1][j - 1] == 5) {
							direct = 3;
						}
						if (mapData.getPathCode()[i - 1][j] == 5) {
							direct = 3;
						}
						if (mapData.getPathCode()[i + 1][j] == 5) {
							direct = 3;
						}
						if (mapData.getPathCode()[i - 1][j + 1] == 5) {
							direct = 3;
						}
						if (mapData.getPathCode()[i][j + 1] == 5) {
							direct = 3;
						}
						if (mapData.getPathCode()[i + 1][j + 1] == 5) {
							direct = 3;
						}

						if (mapData.getPathCode()[i - 1][j - 1] == 6) {
							direct = 4;
						}
						if (mapData.getPathCode()[i][j - 1] == 6) {
							direct = 4;
						}
						if (mapData.getPathCode()[i + 1][j - 1] == 6) {
							direct = 4;
						}
						if (mapData.getPathCode()[i - 1][j] == 6) {
							direct = 4;
						}
						if (mapData.getPathCode()[i + 1][j] == 6) {
							direct = 4;
						}
						if (mapData.getPathCode()[i - 1][j + 1] == 6) {
							direct = 4;
						}
						if (mapData.getPathCode()[i][j + 1] == 6) {
							direct = 4;
						}
						if (mapData.getPathCode()[i + 1][j + 1] == 6) {
							direct = 4;
						}

						if (mapData.getPathCode()[i - 1][j - 1] == 7) {
							direct = 2;
						}
						if (mapData.getPathCode()[i][j - 1] == 7) {
							direct = 2;
						}
						if (mapData.getPathCode()[i + 1][j - 1] == 7) {
							direct = 2;
						}
						if (mapData.getPathCode()[i - 1][j] == 7) {
							direct = 2;
						}
						if (mapData.getPathCode()[i + 1][j] == 7) {
							direct = 2;
						}
						if (mapData.getPathCode()[i - 1][j + 1] == 7) {
							direct = 2;
						}
						if (mapData.getPathCode()[i][j + 1] == 7) {
							direct = 2;
						}
						if (mapData.getPathCode()[i + 1][j + 1] == 7) {
							direct = 2;
						}
					}

				}
			}
		}
		return direct;
	}

	public void setMoneyText() {// set money text
		money = String.format("$:%d", player1.getMoney());
		moneyLabel.setText(money);
	}

	public void setHPText() {// set HP text
		hp = String.format("HP:%d", player1.getHP());
		HPlabel.setText(hp);
	}

	public int checkNextDirection(int inLastDir, int last_X, int last_Y,// check
																		// next
																		// direction
			int index) {
		int nextDir = -2;
		if (inLastDir == 1) {
			for (int i = 1; last_X - i >= 0; i++) {
				if (last_X - i >= 0) {
					if (mapData.getPathCode()[last_X - i][last_Y] == 2) {
						nextDir = -1;
						checkpoint_X[index] = last_X - i;
						checkpoint_Y[index] = last_Y;
						break;
					}
					if (mapData.getPathCode()[last_X - i][last_Y] == 7) {
						nextDir = 2;
						checkpoint_X[index] = last_X - i;
						checkpoint_Y[index] = last_Y;
						break;
					}
					if (mapData.getPathCode()[last_X - i][last_Y] == 5) {
						nextDir = 3;
						checkpoint_X[index] = last_X - i;
						checkpoint_Y[index] = last_Y;
						break;
					}
					if (mapData.getPathCode()[last_X - i][last_Y] == 6) {
						nextDir = 4;
						checkpoint_X[index] = last_X - i;
						checkpoint_Y[index] = last_Y;
						break;
					}
				}
			}
		}
		if (inLastDir == 2) {
			for (int i = 1; last_Y + i < 25; i++) {
				if (last_Y + i < 25) {
					if (mapData.getPathCode()[last_X][last_Y + i] == 2) {
						nextDir = -1;
						checkpoint_X[index] = last_X;
						checkpoint_Y[index] = last_Y + i;
						break;
					}
					if (mapData.getPathCode()[last_X][last_Y + i] == 4) {
						nextDir = 1;
						checkpoint_X[index] = last_X;
						checkpoint_Y[index] = last_Y + i;
						break;
					}
					if (mapData.getPathCode()[last_X][last_Y + i] == 5) {
						nextDir = 3;
						checkpoint_X[index] = last_X;
						checkpoint_Y[index] = last_Y + i;
						break;
					}
					if (mapData.getPathCode()[last_X][last_Y + i] == 6) {
						nextDir = 4;
						checkpoint_X[index] = last_X;
						checkpoint_Y[index] = last_Y + i;
						break;
					}
				}
			}
		}
		if (inLastDir == 3) {

			for (int i = 1; last_X + i < 20; i++) {
				if (last_X + i < 20) {
					if (mapData.getPathCode()[last_X + i][last_Y] == 2) {
						nextDir = -1;
						checkpoint_X[index] = last_X + i;
						checkpoint_Y[index] = last_Y;
						break;
					}
					if (mapData.getPathCode()[last_X + i][last_Y] == 4) {
						nextDir = 1;
						checkpoint_X[index] = last_X + i;
						checkpoint_Y[index] = last_Y;
						break;
					}
					if (mapData.getPathCode()[last_X + i][last_Y] == 7) {
						nextDir = 2;
						checkpoint_X[index] = last_X + i;
						checkpoint_Y[index] = last_Y;
						break;
					}
					if (mapData.getPathCode()[last_X + i][last_Y] == 6) {
						nextDir = 4;
						checkpoint_X[index] = last_X + i;
						checkpoint_Y[index] = last_Y;
						break;
					}
				}
			}
		}
		if (inLastDir == 4) {
			for (int i = 1; last_Y >= 0; i++) {
				if (last_Y - i >= 0) {
					if (mapData.getPathCode()[last_X][last_Y - i] == 2) {
						nextDir = -1;
						checkpoint_X[index] = last_X;
						checkpoint_Y[index] = last_Y - i;
						break;
					}
					if (mapData.getPathCode()[last_X][last_Y - i] == 4) {
						nextDir = 1;
						checkpoint_X[index] = last_X;
						checkpoint_Y[index] = last_Y - i;
						break;
					}
					if (mapData.getPathCode()[last_X][last_Y - i] == 7) {
						nextDir = 2;
						checkpoint_X[index] = last_X;
						checkpoint_Y[index] = last_Y - i;
						break;
					}
					if (mapData.getPathCode()[last_X][last_Y - i] == 5) {
						nextDir = 3;
						checkpoint_X[index] = last_X;
						checkpoint_Y[index] = last_Y - i;
						break;
					}

				}
			}
		}
		return nextDir;
	}

	public boolean getWinflag() {// get win flag
		return winflag;
	}

	public void setWinflag(boolean flag) {// get win flag
		winflag = flag;
	}
}
