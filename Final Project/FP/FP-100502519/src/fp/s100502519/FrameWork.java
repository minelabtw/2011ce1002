package fp.s100502519;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener,KeyListener{
	
	/*--------New--------*/
	Image img1 = new ImageIcon("image/1.jpg").getImage();
	ImageIcon[] img_level = {new ImageIcon("image/beasy.jpg") , new ImageIcon("image/bnormal.jpg"),new ImageIcon("image/bhard.jpg") , new ImageIcon("image/blunatic.jpg")};
	Image img2;
	private page1 p1 = new page1(img1);
	static page2 p2;
	private static page12 p12;
	private JPanel big = new JPanel();
	ImageIcon img_b_play = new ImageIcon("image/play.png");
	ImageIcon img_b_rule = new ImageIcon("image/rule.png");
	private JButton b_play = new JButton(img_b_play);
	private JButton b_rule = new JButton(img_b_rule);
	private JButton b_start2 = new JButton(img_b_play);
	boolean ifPanelChange = false;
	private static JLabel lb_score = new JLabel();
	private JPanel pn_top = new JPanel();
	private JLabel lb_name1 = new JLabel("Player1");
	private JLabel lb_name2 = new JLabel("Player2");
	Rule multi_rule;
	JPanel pn_bs = new JPanel();
	
	/*--------constructor--------*/
	public FrameWork(){
		addKeyListener(this);
		this.setFocusable(true);
		setLayout(new GridLayout(1,1,0,0));
		getContentPane().add(big);
		
		/*
		 	   → p1 (標題頁) → pn_bs → b_play 和　b_rule           → pn_set → pn_left 和 pn_center  和 pn_right
		   big → p12(設定頁)  --------------------------------→ <
		       → p2                                             → b_starts
		*/
		
		big.setLayout(new GridLayout(1,1,0,0));
		big.add(p1);
		
		p1.setLayout(new BorderLayout(0,0));
		p1.add(pn_bs,BorderLayout.SOUTH);
		p1.setFocusable(true);
		
		pn_bs.setLayout(new GridLayout(1,2,0,0));
		pn_bs.add(b_play);
		pn_bs.add(b_rule);
		pn_bs.setOpaque(false);
		pn_bs.setBorder(null);
		
		b_play.addActionListener(this);
		b_play.setContentAreaFilled(false);
		b_play.setBorder(null);
		
		b_start2.addActionListener(this);
		b_start2.setContentAreaFilled(false);
		b_start2.setBorder(null);
		
		b_rule.addActionListener(this);
		b_rule.setContentAreaFilled(false);
		b_rule.setBorder(null);
		
		//背景圖的選擇
		if(p12.getSelectedIndex() == 0){
			img2 = new ImageIcon("image/beasy.jpg").getImage();
		}
		else if(p12.getSelectedIndex() == 1){
			img2 = new ImageIcon("image/bnormal.jpg").getImage();
		}
		else if(p12.getSelectedIndex() == 2){
			img2 = new ImageIcon("image/bhard.jpg").getImage();
		}
		else if(p12.getSelectedIndex() == 3){
			img2 = new ImageIcon("image/blunatic.jpg").getImage();
		}
	}
	
	/*--------event--------*/
	public void actionPerformed(ActionEvent e) {		
		if(e.getSource() == b_play){
			//big去掉p1換成p12
			
			big.remove(p1);
			
			p12 = new page12();
			p12.setBorder(new javax.swing.border.MatteBorder(4,8,8,8,new Color(72,84,234)));
			
			big.add(p12,BorderLayout.CENTER);
			p12.add(b_start2,BorderLayout.SOUTH);
			
			repaint();
			
			setVisible(true);
			p12.setFocusable(true);
		}
		
		if(e.getSource() == b_start2){
			big.remove(p12);
			big.setLayout(new BorderLayout(0,0));
			
			img2 = img_level[p12.getSelectedIndex()].getImage();			//背景圖
			
			p2 = new page2(img2);
			p2.setBorder(new javax.swing.border.MatteBorder(4,8,8,8,new Color(72,84,234)));
			
			big.add(p2,BorderLayout.CENTER);
			big.add(pn_top,BorderLayout.NORTH);
			
			pn_top.setBorder(new javax.swing.border.MatteBorder(8, 	8, 	4, 	8, new Color(72,84,234)));
			pn_top.setLayout(new GridLayout(1,3,0,0));
			pn_top.add(lb_name1);
			pn_top.add(lb_score);
			pn_top.add(lb_name2);
			
			repaint();
			setVisible(true);
			this.setFocusable(true);
			
			ifPanelChange = true;
		}
		
		if(e.getSource() == b_rule){			//第二個窗
			multi_rule = new Rule();
			multi_rule.setTitle("Rule");
			multi_rule.setSize(800, 600);
			multi_rule.setLocationRelativeTo(null);
			multi_rule.setVisible(true);
			multi_rule.setResizable(false);
		}
	}
	
	public void keyPressed(KeyEvent e) {
		if (ifPanelChange) {
			switch(e.getKeyCode()){
				case KeyEvent.VK_Q: if(p2.y1>0){p2.dy2-=60;} break;			//player1往上
				case KeyEvent.VK_A: if(p2.y2<getHeight()-80){p2.dy2+=60;} break;			//player1往下
				case KeyEvent.VK_O: if(p2.y5>0){p2.dy3-=60;} break;			//player2往上
				case KeyEvent.VK_L: if(p2.y6<getHeight()-80){p2.dy3+=60;} break;			//player2往下
			}
		}
	}
	
	public void keyReleased(KeyEvent e) {
		
	}
	
	public void keyTyped(KeyEvent e) {
		
	}
	
	public static JLabel getScreen(){
		return lb_score;			//血量label
	}
}
