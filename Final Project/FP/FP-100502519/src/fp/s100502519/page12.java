package fp.s100502519;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class page12 extends JPanel{

	/*--------new--------*/
	JPanel pn_left = new JPanel();
	JPanel pn_center = new JPanel();
	JPanel pn_right = new JPanel();
	ImageIcon img_b_start = new ImageIcon("image/start.png");
	JPanel pn_set = new JPanel();

	/*CENTER ComboBox*/
	private static String[] difficulty = {"Easy" , "Normal" , "Hard" , "Lunatic"};
	static JComboBox jcbo = new JComboBox(difficulty);
	
	/*Label_intro*/
	ImageIcon[] img_intros = {new ImageIcon("image/easy.jpg") , new ImageIcon("image/normal.jpg"),
			new ImageIcon("image/hard.jpg") , new ImageIcon("image/lunatic.jpg")};
	ImageIcon img_ini = new ImageIcon("image/q.gif");
	JLabel lb_intro = new JLabel(img_ini);
	
	/*left*/
	private static String[] character1 = {"ophidia" , "angel" , "twins"};
	static JComboBox jcbo1 = new JComboBox(character1);
	ImageIcon[] img_char1 = {new ImageIcon("image/a.jpg") , new ImageIcon("image/b.jpg"),new ImageIcon("image/c.jpg")};
	ImageIcon img_ini1 = new ImageIcon("image/q2.gif");
	JLabel lb_cha1 = new JLabel(img_ini1);
	JTextArea ta_left = new JTextArea("說明");
	String [] left = {new String("球拍較長"),new String("血量較多"),new String("有護衛球拍")};
	
	/*right*/
	private static String[] character2 = {"ophidia" , "angel" , "twins"};
	static JComboBox jcbo2 = new JComboBox(character2);
	ImageIcon[] img_char2 = {new ImageIcon("image/a.jpg") , new ImageIcon("image/b.jpg"),new ImageIcon("image/c.jpg")};
	JLabel lb_cha2 = new JLabel(img_ini1);
	JTextArea ta_right = new JTextArea("說明");
	String [] right = {new String("球拍較長"),new String("血量較多"),new String("有護衛球拍")};
	
	/*______Constructor______*/
	public page12(){
		setLayout(new BorderLayout(0,0));
		add(pn_set,BorderLayout.CENTER);
		pn_set.setLayout(new GridLayout(1,3,0,0));
		pn_set.add(pn_left);
		pn_set.add(pn_center);
		pn_set.add(pn_right);
		pn_left.setBackground(new Color(176,242,233));
		pn_right.setBackground(new Color(176,242,233));
		pn_center.setBackground(new Color(126,239,147));
		
		/*Center*/
		jcbo.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e){
				lb_intro.setIcon(img_intros[jcbo.getSelectedIndex()]);
				
			}
		});
		pn_center.setLayout(new BorderLayout(0, 0));
		pn_center.add(jcbo,BorderLayout.NORTH);
		pn_center.add(lb_intro,BorderLayout.CENTER);
		
		/*Left*/
		jcbo1.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e){
				lb_cha1.setIcon(img_char1[jcbo1.getSelectedIndex()]);
				ta_left.setText(left[jcbo1.getSelectedIndex()]);
			}
		});
		pn_left.setLayout(new BorderLayout(0, 0));
		pn_left.add(jcbo1,BorderLayout.NORTH);
		pn_left.add(lb_cha1,BorderLayout.CENTER);
		pn_left.add(ta_left,BorderLayout.SOUTH);
		ta_left.setBorder(new javax.swing.border.LineBorder(new Color(72,84,234), 3));
		ta_left.setLineWrap(true);
		ta_left.setWrapStyleWord(true);
		ta_left.setEditable(false);
		
		/*Right*/
		jcbo2.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e){
				lb_cha2.setIcon(img_char2[jcbo2.getSelectedIndex()]);
				ta_right.setText(left[jcbo2.getSelectedIndex()]);
			}
		});
		pn_right.setLayout(new BorderLayout(0, 0));
		pn_right.add(jcbo2,BorderLayout.NORTH);
		pn_right.add(lb_cha2,BorderLayout.CENTER);
		pn_right.add(ta_right,BorderLayout.SOUTH);
		ta_right.setBorder(new javax.swing.border.LineBorder(new Color(72,84,234), 3));
		ta_right.setLineWrap(true);
		ta_right.setWrapStyleWord(true);
		ta_right.setEditable(false);
	}
	
	/*comboBox的getSelectedIndex*/
	public static int getSelectedIndex(){
		return jcbo.getSelectedIndex();
	}
	
	public static int getSelectedIndex1(){
		return jcbo1.getSelectedIndex();
	}
	
	public static int getSelectedIndex2(){
		return jcbo2.getSelectedIndex();
	}
}
