package fp.s100502519;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class page2 extends JPanel{
	int delay = 10-(page12.getSelectedIndex())*3;			//依據所選難易度調整delay
	private Timer timer1 = new Timer(delay, new TimerListener());
	
	private Image img_bg = null;
	
	/*ball*/
	int x = 0;
	int y = 0;
	int radius = 10;
	int dx = 1;
	int dy = 1;
	
	/*racket*/
	//player1和2的拍子
	int dy2 = 0;
	int dy3 = 0;
	int x1; int y1;
	int x2; int y2;
	int x3; int y3;
	int x4; int y4;
	int x5; int y5;
	int x6; int y6;
	int x7; int y7;
	int x8; int y8;
	
	//player1和2的護衛拍(若選到角色3)
	int x11_1; int y11_1;
	int x11_2; int y11_2;
	int x11_3; int y11_3;
	int x11_4; int y11_4;
	int x12_1; int y12_1;
	int x12_2; int y12_2;
	int x12_3; int y12_3;
	int x12_4; int y12_4;
	int x21_1; int y21_1;
	int x21_2; int y21_2;
	int x21_3; int y21_3;
	int x21_4; int y21_4;
	int x22_1; int y22_1;
	int x22_2; int y22_2;
	int x22_3; int y22_3;
	int x22_4; int y22_4;
	
	/*障礙物(若選難易度hard或lunatic)*/
	int xex1_1; int yex1_1;
	int xex1_2;	int yex1_2;
	int xex1_3; int yex1_3;
	int xex1_4; int yex1_4;
	int xex2_1; int yex2_1;
	int xex2_2;	int yex2_2;
	int xex2_3;	int yex2_3;
	int xex2_4;	int yex2_4;
	int xex3_1; int yex3_1;
	int xex3_2; int yex3_2;
	int xex3_3; int yex3_3;
	int xex3_4; int yex3_4;
	
	/*screen (血量)*/
	private JLabel lb_screen = new JLabel();
	private int sc1_int = 8;			//多1是因為設計上的問題(遊戲一開始player1就會扣一滴)
	private int sc2_int = 7;
	private String sc1_string = null;
	private String sc2_string = null;
	
	/*--------Constructor--------*/
	public page2(Image image){
		timer1.start();
		
		img_bg = image;
		
		if(page12.getSelectedIndex1() == 1){
			sc1_int = 13;			//角色2的特徵  血較多
		}
		
		if(page12.getSelectedIndex2() == 1){
			sc2_int = 12;			//角色2的特徵  血較多
		}
	}
	
	/*--------event--------*/
	private class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			repaint();
		}
	}
	
	/*graphics & 判斷式*/
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		g.drawImage(img_bg,0,0,this.getWidth(),this.getHeight(),this);
		
		/*拍子原始位置*/
		x1 = 0+50;
		y1 = 0 + dy2+300;
		x2 = 0+50;
		y2 = 80 + dy2+300;
		x3 = 40+50;
		y3 = 80 + dy2+300;
		x4 = 40+50;
		y4 = 0 + dy2+300;
		x5 = 860-50;
		y5 = 0 + dy3+300;
		x6 = 860-50;
		y6 = 80 + dy3+300;
		x7 = 900-50;
		y7 = 80 + dy3+300;
		x8 = 900-50;
		y8 = 0 + dy3+300;
		
		/*護衛拍x座標*/
		x11_1 = 0+50+150;
		x11_2 = 0+50+150;
		x11_3 = 40+50+150;
		x11_4 = 40+50+150;
		x12_1 = 0+50+150;
		x12_2 = 00+50+150;
		x12_3 = 40+50+150;
		x12_4 = 40+50+150;
		x21_1 = 860-50-150;
		x21_2 = 860-50-150;
		x21_3 = 900-50-150;
		x21_4 = 900-50-150;
		x22_1 = 860-50-150;
		x22_2 = 860-50-150;
		x22_3 = 900-50-150;
		x22_4 = 900-50-150;
		
		/*障礙物x座標*/
		xex1_1 = getWidth()/2-20;
		xex1_2 = getWidth()/2-20;
		xex1_3 = getWidth()/2+20;
		xex1_4 = getWidth()/2+20;
		xex2_1 = getWidth()/2-20;
		xex2_2 = getWidth()/2-20;
		xex2_3 = getWidth()/2+20;
		xex2_4 = getWidth()/2+20;
		xex3_1 = getWidth()/2-20;
		xex3_2 = getWidth()/2-20;
		xex3_3 = getWidth()/2+20;
		xex3_4 = getWidth()/2+20;
		
		/*依選角設定有無護衛拍而設護衛拍的y座標*/
		if(page12.getSelectedIndex1() == 0){
			y1 = 0 + dy2+230;
			y4 = 0 + dy2+230;
		}
		
		if(page12.getSelectedIndex1() == 2){
			y11_1 = 0 +dy2+100;
			y11_4 = 0 +dy2+100;
			y11_2 = 40 +dy2+100;
			y11_3 = 40 +dy2+100;
			y12_1 = 0 +dy2+500;
			y12_4 = 0 +dy2+500;
			y12_2 = 40 +dy2+500;
			y12_3 = 40 +dy2+500;
			
		}
		
		if(page12.getSelectedIndex2() == 0){
			y5 = 0 + dy3+230;
			y8 = 0 + dy3+230;
		}
		
		if(page12.getSelectedIndex2() == 2){
			y21_1 = 0 +dy3+100;
			y21_4 = 0 +dy3+100;
			y21_2 = 40 +dy3+100;
			y21_3 = 40 +dy3+100;
			y22_1 = 0 +dy3+500;
			y22_4 = 0 +dy3+500;
			y22_2 = 40 +dy3+500;
			y22_3 = 40 +dy3+500;
			
		}
		
		if(page12.getSelectedIndex() == 2){
			yex1_1 = 300-30;
			yex1_4 = 300-30;
			yex1_2 = 300+30;
			yex1_3 = 300+30;
			
		}
		
		if(page12.getSelectedIndex() == 3){
			yex1_1 = 300-50;
			yex1_4 = 300-50;
			yex1_2 = 300+50;
			yex1_3 = 300+50;
			yex2_1 = 100-20;
			yex2_4 = 100-20;
			yex2_2 = 100+20;
			yex2_3 = 100+20;
			yex3_1 = 500-20;
			yex3_4 = 500-20;
			yex3_2 = 500+20;
			yex3_3 = 500+20;
		}
		
		/*碰撞的判斷 and 換方向*/
		if(x == radius){dx = Math.abs(dx) ; sc1_int-- ;}
		if(x == getWidth()-radius){dx = -Math.abs(dx); sc2_int-- ;}
		if(y < radius){dy = Math.abs(dy);}
		if(y > getHeight()-radius){dy = -Math.abs(dy);}
		
		if(x == x1 && y<y3 && y>y1){dx = -Math.abs(dx);};
		if(x == x3 && y<y3 && y>y1){dx = Math.abs(dx);};
		if(y == y1 && x<x3 && x>x1){dy = -Math.abs(dy);};
		if(y == y3 && x<x3 && x>x1){dy = Math.abs(dy);};
		
		if(x == x7 && y<y7 && y>y5){dx = Math.abs(dx);};
		if(x+radius == x5 && y<y7 && y>y5){dx = -Math.abs(dx);};
		if(y == y5 && x>x5 && x<x7){dy = -Math.abs(dy);};
		if(y == y7 && x>x5 && x<x7){dy = Math.abs(dy);};
		
		if(x == x11_3 && y>y11_1 && y<y11_3){dx = Math.abs(dx);};
		if(x+radius == x11_1 && y>y11_1 && y<y11_3){dx = -Math.abs(dx);}
		if(y == y11_1 && x<x11_3 && x>x11_1){dy = -Math.abs(dy);};
		if(y == y11_3 && x<x11_3 && x>x11_1){dy = Math.abs(dy);};
		
		if(x == x12_3 && y>y12_1 && y<y12_3){dx = Math.abs(dx);};
		if(x+radius == x12_1 && y>y12_1 && y<y12_3){dx = -Math.abs(dx);}
		if(y == y12_1 && x<x12_3 && x>x12_1){dy = -Math.abs(dy);};
		if(y == y12_3 && x<x12_3 && x>x12_1){dy = Math.abs(dy);};
		
		if(x == x21_1 && y>y21_1 && y<y21_2){dx = -Math.abs(dx);}
		if(x == x21_3 && y>y21_1 && y<y21_2){dx = Math.abs(dx);}
		if(y == y21_1 && x<x21_3 && x>x21_1){dy = -Math.abs(dy);};
		if(y == y21_3 && x<x21_3 && x>x21_1){dy = Math.abs(dy);};
		
		if(x == x22_1 && y>y22_1 && y<y22_2){dx = -Math.abs(dx);}
		if(x == x22_3 && y>y22_1 && y<y22_2){dx = Math.abs(dx);}
		if(y == y22_1 && x<x22_3 && x>x22_1){dy = -Math.abs(dy);};
		if(y == y22_3 && x<x22_3 && x>x22_1){dy = Math.abs(dy);};
		
		if(x == xex1_1 && y>yex1_1 && y<yex1_2){dx = -Math.abs(dx);}
		if(x == xex1_3 && y>yex1_1 && y<yex1_2){dx = Math.abs(dx);}
		if(y == yex1_1 && x<xex1_3 && x>xex1_1){dy = -Math.abs(dy);};
		if(y == yex1_3 && x<xex1_3 && x>xex1_1){dy = Math.abs(dy);};
		
		if(x == xex2_1 && y>yex2_1 && y<yex2_2){dx = -Math.abs(dx);}
		if(x == xex2_3 && y>yex2_1 && y<yex2_2){dx = Math.abs(dx);}
		if(y == yex2_1 && x<xex2_3 && x>xex2_1){dy = -Math.abs(dy);};
		if(y == yex2_3 && x<xex2_3 && x>xex2_1){dy = Math.abs(dy);};
		
		if(x == xex3_1 && y>yex3_1 && y<yex3_2){dx = -Math.abs(dx);}
		if(x == xex3_3 && y>yex3_1 && y<yex3_2){dx = Math.abs(dx);}
		if(y == yex3_1 && x<xex3_3 && x>xex3_1){dy = -Math.abs(dy);};
		if(y == yex3_3 && x<xex3_3 && x>xex3_1){dy = Math.abs(dy);};
		
		/*結束判斷*/
		if(sc2_int == 0){
			dx=dy=5;
			timer1.stop();
			
			if(page12.getSelectedIndex1() == 0){
				add(new JLabel(new ImageIcon("image/ap1.jpg")));
			}
			else if(page12.getSelectedIndex1() == 1){
				add(new JLabel(new ImageIcon("image/bp1.jpg")));
			}
			else if(page12.getSelectedIndex1() == 2){
				add(new JLabel(new ImageIcon("image/cp1.jpg")));
			}
		}
		else if(sc1_int == 0){
			dx=dy=5;
			timer1.stop();
			
			if(page12.getSelectedIndex2() == 0){
				add(new JLabel(new ImageIcon("image/ap2.jpg")));
			}
			else if(page12.getSelectedIndex2() == 1){
				add(new JLabel(new ImageIcon("image/bp2.jpg")));
			}
			else if(page12.getSelectedIndex2() == 2){
				add(new JLabel(new ImageIcon("image/cp2.jpg")));
			}
		}
		
		/*球的移動*/
		x+=dx;
		y+=dy;
		
		/*畫出的部分*/
		g.setColor(new Color(228,239,33));
		g.fillOval(x-radius, y-radius, radius*2, radius*2);
		
		g.setColor(new Color(26,35,157));
		Polygon p1 = new Polygon();
		p1.addPoint(x1,y1);
		p1.addPoint(x2,y2);
		p1.addPoint(x3,y3);
		p1.addPoint(x4,y4);
		g.fillPolygon(p1);
		
		Polygon p2 = new Polygon();
		p2.addPoint(x5,y5);
		p2.addPoint(x6,y6);
		p2.addPoint(x7,y7);
		p2.addPoint(x8,y8);
		g.fillPolygon(p2);
		
		Polygon p11 = new Polygon();
		p11.addPoint(x11_1,y11_1);
		p11.addPoint(x11_2,y11_2);
		p11.addPoint(x11_3,y11_3);
		p11.addPoint(x11_4,y11_4);
		g.fillPolygon(p11);
		
		Polygon p12 = new Polygon();
		p12.addPoint(x12_1,y12_1);
		p12.addPoint(x12_2,y12_2);
		p12.addPoint(x12_3,y12_3);
		p12.addPoint(x12_4,y12_4);
		g.fillPolygon(p12);
		
		Polygon p21 = new Polygon();
		p21.addPoint(x21_1,y21_1);
		p21.addPoint(x21_2,y21_2);
		p21.addPoint(x21_3,y21_3);
		p21.addPoint(x21_4,y21_4);
		g.fillPolygon(p21);
		
		Polygon p22 = new Polygon();
		p22.addPoint(x22_1,y22_1);
		p22.addPoint(x22_2,y22_2);
		p22.addPoint(x22_3,y22_3);
		p22.addPoint(x22_4,y22_4);
		g.fillPolygon(p22);
		
		g.setColor(new Color(248,18,64));
		
		Polygon pex1 = new Polygon();
		pex1.addPoint(xex1_1, yex1_1);
		pex1.addPoint(xex1_2, yex1_2);
		pex1.addPoint(xex1_3, yex1_3);
		pex1.addPoint(xex1_4, yex1_4);
		g.fillPolygon(pex1);
		
		Polygon pex2 = new Polygon();
		pex2.addPoint(xex2_1, yex2_1);
		pex2.addPoint(xex2_2, yex2_2);
		pex2.addPoint(xex2_3, yex2_3);
		pex2.addPoint(xex2_4, yex2_4);
		g.fillPolygon(pex2);
		
		Polygon pex3 = new Polygon();
		pex3.addPoint(xex3_1, yex3_1);
		pex3.addPoint(xex3_2, yex3_2);
		pex3.addPoint(xex3_3, yex3_3);
		pex3.addPoint(xex3_4, yex3_4);
		g.fillPolygon(pex3);
		
		/*血量*/
		sc1_string = Integer.toString(sc1_int);
		sc2_string = Integer.toString(sc2_int);
		lb_screen = FrameWork.getScreen();
		lb_screen.setFont(new Font("Serif",Font.BOLD,20));
		lb_screen.setText(sc1_string+" : "+sc2_string);
	}
}
