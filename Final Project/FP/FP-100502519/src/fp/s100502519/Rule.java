package fp.s100502519;
import javax.swing.*;
import java.awt.*;

public class Rule extends JFrame{
	
	/*--------New--------*/
	ImageIcon img_rule = new ImageIcon("image/ex.gif");
	ImageIcon img_title = new ImageIcon("image/intro_rule.png");
	JLabel lb_rule = new JLabel(img_rule);
	JLabel lb_title = new JLabel(img_title);
	JPanel pn_rule = new JPanel();
	static JTextArea ta_rule = new JTextArea();
	JPanel pn_north = new JPanel();
	JScrollPane sp = new JScrollPane(ta_rule);
	
	/*--------Constructor--------*/
	public Rule(){
		setLayout(new GridLayout(1,1,0,0));
		add(pn_rule);
		
		//整個大Panel放全部
		pn_rule.setLayout(new BorderLayout(0,0));
		pn_rule.setBackground(Color.WHITE);
		pn_rule.add(pn_north,BorderLayout.NORTH);
		pn_rule.add(sp,BorderLayout.CENTER);
		
		//兩個放圖的label 再放到一個Panel
		pn_north.setLayout(new BorderLayout(0,0));
		pn_north.setBackground(Color.WHITE);
		pn_north.add(lb_title,BorderLayout.CENTER);
		pn_north.add(lb_rule,BorderLayout.SOUTH);
		
		//TextArea 放從記事本input進來的
		ta_rule.setFont(new Font("Serif",Font.BOLD,50));
		ta_rule.setLineWrap(true);
		ta_rule.setWrapStyleWord(true);
		ta_rule.setEditable(false);
	}
}
