package fp.s100502004;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;

public class Board extends Rectangle2D.Double{
	
	int boardLong = 100;
	int boardHeight = 5;

	
	public Board(){
		
		y=500;
		
		width=100;
		height=30;
		
	}
	

	
	public void setBoardX(int x){
		this.x = x;
	}
	
	
	public int getBoardX(){
		return (int) x;
	}
	
	
	public int getShowX(){
		return (int) (x - boardLong/2);
	}
	
	
	public int getShowY(){
		return (int) (y - boardHeight/2);
	}
	
	
	public int getBoardY(){
		return (int) y;
	}
	
	
	public int getBoardLong(){
		return boardLong;
	}
	
	
	public int getBoardHeight(){
		return boardHeight;
	}
	
	
}
