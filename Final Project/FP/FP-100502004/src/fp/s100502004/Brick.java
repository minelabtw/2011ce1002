package fp.s100502004;

import java.awt.geom.Rectangle2D;

public class Brick extends Rectangle2D.Double{
	
	static final int length=80;
	
	public Brick(int x,int y) {
		this.x=x;
		this.y=y;
		this.width=length;
		this.height=20;
	}
	public int getshowBrickX(){
		return (int) (x - width/2);
	}
	
	
	public int getshowBrickY(){
		return (int) (y - height/2);
	}

	
	
	
	public int getBrickX(){
		return (int) x;
	}
	
	public int getBrickY(){
		return (int) y;
	}
	
	public int getBlong(){
		return (int) width;
	}
	
	public int getBheight(){
		return (int) height;
	}
	

	
	
	
	
	
	
}
