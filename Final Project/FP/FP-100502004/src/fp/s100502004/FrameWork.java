package fp.s100502004;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.Timer;

public class FrameWork extends JFrame implements ActionListener {

	String loseString = "你失敗了喔~~";

	JLabel faceJLabel = new JLabel();
	JPanel startJPanel = new JPanel();
	int point;
	
	
	JButton startButton = new JButton("開始");
	JRadioButton EasyButton = new JRadioButton("Easy");
	JRadioButton NormalButton = new JRadioButton("Normal");
	JRadioButton HardButton = new JRadioButton("Hard");
	JLabel levelJLabel = new JLabel();
	JLabel Title = new JLabel("打磚塊");
	JLabel life = new JLabel("生命");
	JLabel lose = new JLabel("你失敗了喔~~");

	ImageIcon firstIcon = new ImageIcon("image/card/主頁面.1.png");
	ImageIcon startIcon = new ImageIcon("image/card/封面.1.png");
	ImageIcon imageIcon = new ImageIcon("image/card/face.png");
	ImageIcon background2 = new ImageIcon("image/card/星空2.1.png");
	ImageIcon background3 = new ImageIcon("image/card/星空3.1.png");
	ImageIcon brickIcon = new ImageIcon("image/card/太空怪獸.2.png");
	ImageIcon endIcon = new ImageIcon("image/card/結束.1.png");
	ImageIcon loseIcon = new ImageIcon("image/card/失敗.png");
	ImageIcon passIcon = new ImageIcon("image/card/通過.1.png");
	ImageIcon congradulationIcon = new ImageIcon("image/card/恭喜過關.png");
	ImageIcon newIcon = new ImageIcon("image/card/太空梭.1.png");
	
	
	Image firstImage = firstIcon.getImage();
	Image faceImage = startIcon.getImage();
	Image backgroundImage2 = background2.getImage();
	Image backgroundImage3 = background3.getImage();
	Image brickImage = brickIcon.getImage();
	Image endImage = endIcon.getImage();
	Image loseImage = loseIcon.getImage();
	Image passImage = passIcon.getImage();
	Image pacongradulationImage = congradulationIcon.getImage();
	Image newImage = newIcon.getImage();
	
	
	Ball ball;
	ArrayList<Brick> bricks = new ArrayList<Brick>();
	Board board;

	GamePanel gamePanel = new GamePanel();
	MainPanel mainPanel = new MainPanel();

	int level = 0;
	int deadnumber = 0;
	boolean passkey = false;

	public FrameWork() {

		life.setBounds(950, 50, 100, 50);
		add(faceJLabel);
		setTitle("Final Project");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1000, 700);
		setVisible(true);
		mainPanel.getButton().addActionListener(this);
		add(mainPanel);
		mainPanel.getleaveButton().addActionListener(this);
		
	}

	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == mainPanel.getButton()) {
			remove(mainPanel);
			add(gamePanel);
			repaint();
			invalidate();
			validate();
			setSize(1000, 700);
			setLocationRelativeTo(null);
		}
		
		if (e.getSource() == mainPanel.getleaveButton()) {
			System.exit(JFrame.EXIT_ON_CLOSE);
			setVisible(false);
			repaint();
			validate();
			
		}
		
	}
	
		
	
	
	
	public void setLevel(int i) {
		level = i;
	}

	public void addBricksToDraw(Brick brick) {
		bricks.add(brick);
	}

	public void RemoveBrick(Brick i) {
		bricks.remove(i);
		point++;
	}

	public void setBall(Ball ball) {
		this.ball = ball;
	}

	public void setBoard(Board board) {
		this.board = board;
	}

	public boolean ChangeLevel() {

		if (bricks.isEmpty()) {
			return true;
		}

		else {

			return false;

		}

	}

	public void setMouseMotionListener(MouseMotionListener mouseListener) {
		addMouseMotionListener(mouseListener);
	}

	public void setBricks(ArrayList<Brick> bricks) {
		this.bricks = bricks;
	}

	public void setDeadNumber(int i) {
		deadnumber = i;

	}

	public void setBackground(int i) {
		if (i == 0) {
			faceImage = imageIcon.getImage();
		}

		if (i == 1) {
			faceImage = background2.getImage();
		}

		if (i == 2) {
			faceImage = background3.getImage();
		}

		if (i == 3) {
			faceImage = passIcon.getImage();
			deadnumber = (-1);
		}
		
		if (i == 4) {
			faceImage = passIcon.getImage();
			deadnumber = (-1);
		}
		
		
	}

	class MainPanel extends JPanel{
		JButton button = new JButton("開始");
		JButton leave = new JButton("結束");
		
		public MainPanel() {
			add(button);
			
			add(leave);
		}

		public JButton getButton() {
			return button;
		}
		
		public JButton getleaveButton() {
			return leave;
		}
		
		
		protected void paintComponent(Graphics g) {
			// TODO Auto-generated method stub
			super.paintComponent(g);
			g.drawImage(firstImage, 0, 0, 1000, 700, this);

		}

	}

	class GamePanel extends JPanel {
		@Override
		protected void paintComponent(Graphics g) {
			// TODO Auto-generated method stub
			super.paintComponent(g);

			Graphics2D g2d = (Graphics2D) g;

			g2d.drawImage(faceImage, 0, 0, 900, 600, this);

			for (Brick brick : bricks) {// show X and show Y are the real point
										// of the

				g2d.setColor(Color.yellow);
				g2d.drawImage(brickImage, brick.getshowBrickX(),
						brick.getshowBrickY(), brick.getBlong(),
						brick.getBheight(), this);

			}

			g2d.setColor(Color.red);

			g2d.fillOval(ball.getShowX(), ball.getShowY(),
					ball.getRadius() * 2, ball.getRadius() * 2);

			g2d.setColor(Color.blue);
//			g2d.fillRect(board.getShowX(), board.getShowY(),
//					board.getBoardLong(), board.getBoardHeight());
			g2d.drawImage(newImage, board.getShowX(),
					board.getShowY(), board.getBoardLong(),
					board.getBoardHeight(), this);

			g2d.drawLine(900, 0, 900, 600);
			g2d.drawLine(0, 600, 900, 600);

			if (deadnumber == 0) {
				g2d.setColor(Color.red);
				g2d.drawString("生命", 950, 50);
				g2d.fillOval(950, 100 + 30, 20, 20);
				g2d.fillOval(950, 100 + 60, 20, 20);
				g2d.fillOval(950, 100 + 90, 20, 20);
			}

			if (deadnumber == 1) {
				g2d.setColor(Color.red);
				g2d.drawString("生命", 950, 50);
				g2d.fillOval(950, 100 + 30, 20, 20);
				g2d.fillOval(950, 100 + 60, 20, 20);

			}

			if (deadnumber == 2) {
				g2d.setColor(Color.red);
				g2d.drawString("生命", 950, 50);
				g2d.fillOval(950, 100 + 30, 20, 20);

			}

			if (deadnumber >= 3) {

				g2d.drawImage(endImage, 0, 0, 1200, 900, this);
				g2d.drawImage(loseImage, 50, 450, 200, 200, this);
				// g2d.drawString("你失敗了喔~~~", 450, 300);

			}

			if (deadnumber < 0) {
				g2d.drawImage(passImage, 0, 0, 1000, 700, this);
				g2d.drawImage(pacongradulationImage, 450, 75, 250, 100, this);

			}

		}

	}

}
