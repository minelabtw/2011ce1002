package fp.s100502004;

import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

import javax.swing.JPanel;



public class Ball extends Ellipse2D.Double{

	private int speedX = 15;
	private int speedY = 15;
	private int radius = 20;
	
	
	int deadnumber = 0;
	
	
	int TyprOfReflect = 0;
	
	
	public Ball(){
		x=400;
		y=200;
		width=2*radius;
		height=2*radius;
	}
	
	
	
	
	
	public int getShowX() {
		return (int) (x-radius);
	}
	
	
	public int getShowY() {
		return (int) (y-radius);
	}
	
	
	public int getRadius() {
		return radius;
 	}
	
	
	public void reflectX(){
		speedX=-speedX;
		
	}
	
	
	public void reflectY() {
		speedY=-speedY;
	}
	
	
	public int getTyprOfReflect() {
		return TyprOfReflect;
	}
	
	public boolean interscet(Brick brick) {	//use to know if the ball is touch the brick
		
	return intersects((Rectangle2D)brick);
		
		
	}

	
	
	
	public boolean interscet(Board board) {	//use to know if the ball is touch the board
		
		if(y >= board.getBoardY()-(board.getBoardHeight())/2 - radius && y <= board.getBoardY() + (board.getBoardHeight())/2 + radius){
			if((x >= board.getBoardX() - (board.getBoardLong())/2 - radius) && (x <= board.getBoardX() + (board.getBoardLong())/2 + radius)){
				
				return true;
				
			}
			
		}
		

		
		return intersects((Rectangle2D)board);
	}
	
	
	public boolean interscetToFrame(FrameWork frameWork) {	//use to know if the ball is touch the frame bound
		if(x - radius < 0 || x + radius >= 900){
			TyprOfReflect = 1; 
			return true;
		}
		
		else if(y - radius < 0 ){
			TyprOfReflect = 2;
			return true;	
		}
		
		else if(y + radius >= 600){
			
			TyprOfReflect = 0;			
			return true;
			
		}
		return false;
	}
		
	public int getDeadNumber(){
		
		return deadnumber;
		
	}
	
	
	
	
	public void isDead(){
		if(deadnumber < 3){
			x = 400;
			y = 200;
			radius = 20;
			deadnumber ++;
			
		}
		
		if(deadnumber >= 3){
			x = 0;
			y = 0;
			radius = 0;
			
		}
		
	}
	
	
	
	public void step() {
		x += speedX;
		y += speedY;
		
	}

}
