package fp.s100502004;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.TimerTask;

import javax.swing.Timer;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class FinalProject {
	
	
	FrameWork frameWork = new FrameWork();
	Ball ball = new Ball();
	Board board = new Board();	
	ArrayList<Brick> bricks = new ArrayList<Brick>();
	
	int whichlevel = 1;
	int deadnumber = ball.getDeadNumber();
	
	public static void main(String[] args) {
		FinalProject controller = new FinalProject();
	}
	

	public FinalProject() {

		Timer timer = new Timer(35, new TimerListener());
		timer.start();
		
		for (int i = 0; i < 8; i++) {
			int x = 200, y = 300;
			bricks.add(new Brick(x + i * Brick.length, y));
		}

		frameWork.setBall(ball);
		frameWork.setMouseMotionListener(mouseHandler);
		frameWork.setBoard(board);
		frameWork.setBricks(bricks);
		

	}

	MouseMotionListener mouseHandler = new MouseMotionListener() {

		@Override
		public void mouseMoved(MouseEvent e) {
			// TODO Auto-generated method stub
			if(e.getX()<=850 &&e.getX() >= 50){
				board.setBoardX(e.getX());
			}
			
			else if(e.getX() <= 50){
				board.setBoardX(50);
			}
			
			else{
				board.setBoardX(850);
			}
		}

		@Override
		public void mouseDragged(MouseEvent arg0) {
			// TODO Auto-generated method stub

		}
	};
	
	public void setEasyLevelBrick(){
		
		frameWork.setBackground(0);
		
		for (int i = 0; i < 8; i++) {
			int x = 200, y = 300;
			bricks.add(new Brick(x + i * Brick.length, y));
		}

		frameWork.setBall(ball);
		frameWork.setMouseMotionListener(mouseHandler);
		frameWork.setBoard(board);
		frameWork.setBricks(bricks);
		frameWork.repaint();
	
	
	}
	
	public void getTimerStart(){
		
	}
	
	
	
	
	
	public void setNormalLevelBrick(){	
		
		frameWork.setBackground(1);
		
		for(int i = 0; i < 4; i++){
			int x = 100;
			int y = 100;
			bricks.add(new Brick(x + i * Brick.length, y));
			
		}
		
		
		for(int i = 0; i < 4; i++){
			int x = 600;
			int y = 100;
			bricks.add(new Brick(x + i * Brick.length, y));
			
		}
		
		
		for (int i = 0; i < 8; i++) {
			int x = 200, y = 300;
			bricks.add(new Brick(x + i * Brick.length, y));
		}
		
		frameWork.setBall(ball);
		frameWork.setMouseMotionListener(mouseHandler);
		frameWork.setBoard(board);
		frameWork.setBricks(bricks);
		frameWork.repaint();
		
		
	}
	
	
	public void setHardLevelBrick(){
		
		frameWork.setBackground(2);
		
		for(int i = 0; i < 4; i++){
			int x = 100;
			int y = 100;
			bricks.add(new Brick(x + i * Brick.length, y));
			
		}
		
		
		for(int i = 0; i < 4; i++){
			int x = 600;
			int y = 100;
			bricks.add(new Brick(x + i * Brick.length, y));
			
		}
		
		
		for (int i = 0; i < 8; i++) {
			int x = 200,y = 300;
			bricks.add(new Brick(x + i * Brick.length, y));
		}
		
		
		for (int i = 0; i < 4; i++) {
			int x = 100, y = 400;
			bricks.add(new Brick(x + i * Brick.length, y));
		}
		
		
		for (int i = 0; i < 4; i++) {
			int x = 600, y = 400;
			bricks.add(new Brick(x + i * Brick.length, y));
		}
		
		
		
		frameWork.setBall(ball);
		frameWork.setMouseMotionListener(mouseHandler);
		frameWork.setBoard(board);
		frameWork.setBricks(bricks);
		frameWork.repaint();
		
		
		
		
	}
	
	
	
	
	public class TimerListener implements ActionListener {

		public void actionPerformed(ActionEvent e){
			
			ball.step();
			
			frameWork.setDeadNumber(ball.getDeadNumber());			

			if(frameWork.ChangeLevel() && whichlevel == 1){
				
				setNormalLevelBrick();
				whichlevel ++ ;
				frameWork.setLevel(whichlevel);
				
			}
			
			
			if(frameWork.ChangeLevel() && whichlevel == 2){
				
				setHardLevelBrick();
				whichlevel ++ ;
				frameWork.setLevel(whichlevel);
				
			}			
			
			if(frameWork.ChangeLevel() && whichlevel >= 3){
				
				frameWork.setBackground(3);
				
			}	
			
			
			
			Iterator<Brick> brickIterator = bricks.iterator();
			
			
			while(brickIterator.hasNext()) {
				Brick brick=brickIterator.next();
				if(ball.interscet(brick)){
					brickIterator.remove();
					ball.reflectY();
					break;
				}
			}
			
			if(ball.interscet(board)){
				ball.reflectY();
			}
			
			
			
			
			if(ball.interscetToFrame(frameWork)){
				//System.out.println("Reflect");
				if(ball.getTyprOfReflect() == 1){
					ball.reflectX();
				}
				
				if(ball.getTyprOfReflect() == 2){
					ball.reflectY();
				}
				
				if(ball.getTyprOfReflect() == 0){
					ball.isDead();
					deadnumber++;
				}
				
			}			
			
			frameWork.repaint();
			frameWork.invalidate();
		}

	}
	

}
