package fp.s100502033;
import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import javax.swing.*;
public class FinalProject extends JFrame
{	
	private static AudioClip button; 
	private static AudioClip ok; 
	private static AudioClip op; 
	private int number1 = 1 ;
	private int number2 = 1 ;
	private int number3 = 1 ;
	private int number4 = 1 ;
	private int number5 = 0 ;
	private static JButton start = new JButton(""); 
	private static JButton score = new JButton(""); 
	private static JButton leave = new JButton(""); 
	private static JButton player1 = new JButton(""); 
	private static JButton player2 = new JButton(""); 
	private static JButton player3 = new JButton(""); 
	private static JButton change1 = new JButton(""); 
	private static JButton change2 = new JButton(""); 
	private static JButton change3 = new JButton("NEXT"); 
	private static JButton change4 = new JButton("NEXT"); 
	private static JButton startgame = new JButton(""); 
	private static JButton back = new JButton(""); 
	private static JPanel p1 = new JPanel();
	private Framework f2;
	private Framework1 f3;
	private Framework2 f4;
	private static FinalProject f = new FinalProject();
	private JLabel label1 = new JLabel("");
	private JLabel label2 = new JLabel("");
	private JLabel label3 = new JLabel("");
	private JLabel label4 = new JLabel("");
	private JLabel label5 = new JLabel("玩家一");
	private JLabel label6 = new JLabel("玩家二");
	private JLabel label7 = new JLabel("玩家三");
	private JLabel label8 = new JLabel("玩家四");
	private JLabel character1 = new JLabel("");
	private JLabel character2 = new JLabel("");
	private JTextArea label9 = new JTextArea("步數	姓名	人物	時間");
	private JTextArea label10 = new JTextArea("\n第一名\n第二名");
	private String mark[] = new String[2];
	private String string1;
	private String string2;
	private int num1;
	private int num2;
	public FinalProject()
	{
	    button = Applet.newAudioClip(getClass().getResource("music\\button.wav"));//載入音樂檔
	    ok = Applet.newAudioClip(getClass().getResource("music\\ok.wav"));//載入音樂檔
	    op = Applet.newAudioClip(getClass().getResource("music\\OP.mid"));//載入音樂檔
	    op.loop();//重複播放
		JPanel p1 = new JPanel()
		{
			public void paintComponent(Graphics g)
			{
				super.paintComponent(g);
				ImageIcon labelicon1 = new ImageIcon("1\\2-2.jpg");//背景圖片
				labelicon1.paintIcon(this, g, 0, 0);
			}
		};
		//p1.setBorder(BorderFactory.createTitledBorder("Test"));
		p1.setOpaque(false);
		p1.setLayout(null);
		start.setFont(new Font("Serif" , Font.BOLD , 32));//71~82設定字型
		score.setFont(new Font("Serif" , Font.BOLD , 32));
		leave.setFont(new Font("Serif" , Font.BOLD , 32));
		player1.setFont(new Font("Serif" , Font.BOLD , 32));
		player2.setFont(new Font("Serif" , Font.BOLD , 32));
		player3.setFont(new Font("Serif" , Font.BOLD , 32));
		label5.setFont(new Font("Serif" , Font.BOLD , 32));
		label6.setFont(new Font("Serif" , Font.BOLD , 32));
		label7.setFont(new Font("Serif" , Font.BOLD , 32));
		label8.setFont(new Font("Serif" , Font.BOLD , 32));
		label9.setFont(new Font("Serif" , Font.BOLD , 32));
		label10.setFont(new Font("Serif" , Font.BOLD , 32));
		label5.setForeground(Color.red);//83~86設定字型頻色
		label6.setForeground(Color.orange);
		label7.setForeground(Color.yellow);
		label8.setForeground(Color.GREEN);
		start.setBounds(getWidth()/2 +400 , getHeight()/2 +100, 200, 100);//87~142設置圖片、位置
		score.setBounds(getWidth()/2 +400 , getHeight()/2 +250, 200, 100);
		leave.setBounds(getWidth()/2 +400 , getHeight()/2 +400, 200, 100);
		start.setIcon(new ImageIcon("1\\1001.png"));
		score.setIcon(new ImageIcon("1\\1002.png"));
		leave.setIcon(new ImageIcon("1\\1003.png"));
		back.setIcon(new ImageIcon("1\\3001.png"));
		player1.setIcon(new ImageIcon("1\\4001.png"));
		player2.setIcon(new ImageIcon("1\\4002.png"));
		player3.setIcon(new ImageIcon("1\\4003.png"));
		change1.setIcon(new ImageIcon("1\\2001.png"));
		change2.setIcon(new ImageIcon("1\\2001.png"));
		startgame.setIcon(new ImageIcon("1\\2002.png"));
		p1.add(start , BorderLayout.CENTER);
		p1.add(score , BorderLayout.CENTER);
		p1.add(leave , BorderLayout.CENTER);
		player1.setBounds(getWidth()/2 + 400 , getHeight()/2 + 125, 200, 100);
		player2.setBounds(getWidth()/2 +400 , getHeight()/2 + 250, 200, 100);
		player3.setBounds(getWidth()/2 +400 , getHeight()/2 + 400, 200, 100);
		character1.setBounds(590 , 240, 32, 48);
		p1.add(character1);
		character2.setBounds(590 , 290, 32, 48);
		p1.add(character2);
		label1.setBounds(0 , 100, 245, 400);
		p1.add(label1);
		label2.setBounds(245 , 100, 245, 400);
		p1.add(label2);
		label3.setBounds(490 , 100, 245, 400);
		p1.add(label3);
		label4.setBounds(735 , 100, 245, 400);
		p1.add(label4);
		label5.setBounds(92 , 0, 200, 100);
		p1.add(label5);
		label6.setBounds(337 , 0, 200, 100);
		p1.add(label6);
		label7.setBounds(582 , 0, 200, 100);
		p1.add(label7);
		label8.setBounds(830 , 0, 200, 100);
		p1.add(label8);
		label9.setBounds(150 , 200, 800, 175);
		p1.add(label9);
		label10.setBounds(0 , 200, 150, 175);
		p1.add(label10);
		change1.setBounds(92 , 550, 75, 50);
		change2.setBounds(337 , 550, 75, 50);
		change3.setBounds(582 , 550, 75, 50);
		change4.setBounds(830 , 550, 75, 50);
		startgame.setBounds(465, 600, 75, 50);
		back.setBounds(465, 400, 75, 50);
		p1.add(change1);
		p1.add(change2);
		p1.add(change3);
		p1.add(change4);
		p1.add(startgame);
		p1.add(back);
		add(p1);
		startgame.setVisible(false);//143~178 button 設置
		player1.setVisible(false);
		player2.setVisible(false);
		player3.setVisible(false);
		label1.setVisible(false);
		label2.setVisible(false);
		label3.setVisible(false);
		label4.setVisible(false);
		label5.setVisible(false);
		label6.setVisible(false);
		label7.setVisible(false);
		label8.setVisible(false);
		change1.setVisible(false);
		change2.setVisible(false);
		change3.setVisible(false);
		change4.setVisible(false);
		back.setVisible(false);
		label9.setVisible(false);
		label10.setVisible(false);
		p1.add(player1 , BorderLayout.SOUTH);
		p1.add(player2 , BorderLayout.SOUTH);
		p1.add(player3 , BorderLayout.SOUTH);
		add(p1 , BorderLayout.CENTER);
		work f1 = new work();
		start.addActionListener(f1);
		score.addActionListener(f1);
		leave.addActionListener(f1);
		player1.addActionListener(f1);
		player2.addActionListener(f1);
		player3.addActionListener(f1);
		change1.addActionListener(f1);
		change2.addActionListener(f1);
		change3.addActionListener(f1);
		change4.addActionListener(f1);
		startgame.addActionListener(f1);	
		back.addActionListener(f1);	
		try //179~206 查看紀錄
		{
			FileReader inputfile = new FileReader("score.txt");
			FileReader inputfile1 = new FileReader("character.txt");
			BufferedReader read = new BufferedReader(inputfile);
			BufferedReader read1 = new BufferedReader(inputfile1);
			for(int i = 0 ; i < 2 ; i++)
			{
				mark[i] = read.readLine();
			}
			string1 = read1.readLine();
			string2 = read1.readLine();
			num1 = Integer.parseInt(string1);
			num2 = Integer.parseInt(string2);
			read.close();
			read1.close();
		} 
		catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	class work implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			if(event.getSource() == start)//開始時的動作
			{
				button.play();
				p1.setLayout(null);
				start.setVisible(false);
				score.setVisible(false);
				leave.setVisible(false);
				number5 = 1;
				label1.setIcon(new ImageIcon("1\\20.jpg"));
				label2.setIcon(new ImageIcon("1\\20.jpg"));
				player1.setVisible(false);
				player2.setVisible(false);
				player3.setVisible(false);
				label1.setVisible(true);
				label2.setVisible(true);
				label5.setVisible(true);
				label6.setVisible(true);
				change1.setVisible(true);
				change2.setVisible(true);
				startgame.setVisible(true);
				/*player1.setVisible(true);
				player2.setVisible(true);
				player3.setVisible(true);
				player3.setBounds(getWidth()/2 -100 , getHeight()/2 + 50, 200, 100);
				player2.setBounds(getWidth()/2 -100 , getHeight()/2 -100, 200, 100);*/
				p1.validate();
				p1.repaint();
				validate();
				repaint();
			}
			if(event.getSource() == leave)//離開時的動作
			{
				button.play();
				int number = JOptionPane.showConfirmDialog(null, "你確定要離開遊戲?" , "選項" , JOptionPane.YES_NO_OPTION);
				if(number == JOptionPane.YES_OPTION)
				{
					System.exit(0);
				}
			}
			if(event.getSource() == score)
			{
				button.play();
				switch(num1)//選擇人物
				{
					case 1:
						character1.setIcon(new ImageIcon("1\\01.png"));
						break;
					case 2:
						character1.setIcon(new ImageIcon("1\\02.png"));
						break;
					case 3:
						character1.setIcon(new ImageIcon("1\\03.png"));
						break;
					case 4:
						character1.setIcon(new ImageIcon("1\\04.png"));
						break;
					case 5:
						character1.setIcon(new ImageIcon("1\\05.png"));
						break;
					case 0:
						character1.setIcon(new ImageIcon("1\\06.png"));
						break;
					default:
						break;
				}
				switch(num2)//選擇人物
				{
					case 1:
						character2.setIcon(new ImageIcon("1\\01.png"));
						break;
					case 2:
						character2.setIcon(new ImageIcon("1\\02.png"));
						break;
					case 3:
						character2.setIcon(new ImageIcon("1\\03.png"));
						break;
					case 4:
						character2.setIcon(new ImageIcon("1\\04.png"));
						break;
					case 5:
						character2.setIcon(new ImageIcon("1\\05.png"));
						break;
					case 0:
						character2.setIcon(new ImageIcon("1\\06.png"));
						break;
					default:
							break;
				}
				for(int i = 0 ; i < 2 ; i++)
				{
					label9.setText(label9.getText() + "\n" + mark[i] ); 
				}
				label9.setBackground(Color.DARK_GRAY);
				label9.setForeground(Color.white);
				label10.setBackground(Color.DARK_GRAY);
				label10.setForeground(Color.white);
				character1.setVisible(true);
				character2.setVisible(true);
				label9.setVisible(true);
				label10.setVisible(true);
				back.setVisible(true);
				start.setVisible(false);
				score.setVisible(false);
				leave.setVisible(false);
				
			}
			if(event.getSource() == back)//返回主目錄
			{
				
				button.play();
				label9.setText("步數	姓名	人物	時間");
				character1.setVisible(false);
				character2.setVisible(false);
				back.setVisible(false);
				label9.setVisible(false);
				label10.setVisible(false);
				start.setVisible(true);
				score.setVisible(true);
				leave.setVisible(true);
			}
			if(event.getSource() == player1)//二人玩時的動作
			{
				number5 = 1;
				label1.setIcon(new ImageIcon("1\\20.jpg"));
				label2.setIcon(new ImageIcon("1\\20.jpg"));
				player1.setVisible(false);
				player2.setVisible(false);
				player3.setVisible(false);
				label1.setVisible(true);
				label2.setVisible(true);
				label5.setVisible(true);
				label6.setVisible(true);
				change1.setVisible(true);
				change2.setVisible(true);
				startgame.setVisible(true);
				/*f.setVisible(false);
				f2.setTitle("Game"); 
				f2.setSize(900, 735); //Frame size
				f2.setLocationRelativeTo(null);
				f2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				f2.setVisible(true);*/
			}
			if(event.getSource() == player2)//三人玩時的動作，因為在二人玩卡了很久，所以做了一半
			{
				number5 = 2;
				label1.setIcon(new ImageIcon("1\\20.jpg"));
				label2.setIcon(new ImageIcon("1\\20.jpg"));
				label3.setIcon(new ImageIcon("1\\20.jpg"));
				player1.setVisible(false);
				player2.setVisible(false);
				player3.setVisible(false);
				label1.setVisible(true);
				label2.setVisible(true);
				label3.setVisible(true);
				label5.setVisible(true);
				label6.setVisible(true);
				label7.setVisible(true);
				change1.setVisible(true);
				change2.setVisible(true);
				change3.setVisible(true);
				startgame.setVisible(true);
				/*f.setVisible(false);
				f2.setTitle("Game"); 
				f2.setSize(900, 735); //Frame size
				f2.setLocationRelativeTo(null);
				f2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				f2.setVisible(true);*/
			}
			if(event.getSource() == player3)//四人玩時的動作，因為在二人玩卡了很久，所以做了一半
			{
				number5 = 3;
				label1.setIcon(new ImageIcon("1\\20.jpg"));
				label2.setIcon(new ImageIcon("1\\20.jpg"));
				label3.setIcon(new ImageIcon("1\\20.jpg"));
				label4.setIcon(new ImageIcon("1\\20.jpg"));
				player1.setVisible(false);
				player2.setVisible(false);
				player3.setVisible(false);
				label1.setVisible(true);
				label2.setVisible(true);
				label3.setVisible(true);
				label4.setVisible(true);
				label5.setVisible(true);
				label6.setVisible(true);
				label7.setVisible(true);
				label8.setVisible(true);
				change1.setVisible(true);
				change2.setVisible(true);
				change3.setVisible(true);
				change4.setVisible(true);
				startgame.setVisible(true);
				/*f.setVisible(false);
				f2.setTitle("Game"); 
				f2.setSize(900, 735); //Frame size
				f2.setLocationRelativeTo(null);
				f2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				f2.setVisible(true);*/
			}
			if(event.getSource() == change1)//選擇人物
			{
				button.play();
				number1++;
				switch(number1)
				{
					case 1:
						label1.setIcon(new ImageIcon("1\\20.jpg"));
						break;
					case 2:
						label1.setIcon(new ImageIcon("1\\21.jpg"));
						break;
					case 3:
						label1.setIcon(new ImageIcon("1\\22.jpg"));
						break;
					case 4:
						label1.setIcon(new ImageIcon("1\\23.jpg"));
						break;
					case 5:
						label1.setIcon(new ImageIcon("1\\24.jpg"));
						break;
					case 6:
						label1.setIcon(new ImageIcon("1\\25.jpg"));
						number1 = 0;
						break;
				}
			}
			if(event.getSource() == change2)//選擇人物
			{
				button.play();
				number2++;
				switch(number2)
				{
					case 1:
						label2.setIcon(new ImageIcon("1\\20.jpg"));
						break;
					case 2:
						label2.setIcon(new ImageIcon("1\\21.jpg"));
						break;
					case 3:
						label2.setIcon(new ImageIcon("1\\22.jpg"));
						break;
					case 4:
						label2.setIcon(new ImageIcon("1\\23.jpg"));
						break;
					case 5:
						label2.setIcon(new ImageIcon("1\\24.jpg"));
						break;
					case 6:
						label2.setIcon(new ImageIcon("1\\25.jpg"));
						number2 = 0;
						break;
				}
			}
			if(event.getSource() == change3)//三人玩時才用到，因為沒有三人玩，所以也多餘
			{
				button.play();
				number3++;
				switch(number3)
				{
					case 1:
						label3.setIcon(new ImageIcon("1\\20.jpg"));
						break;
					case 2:
						label3.setIcon(new ImageIcon("1\\21.jpg"));
						break;
					case 3:
						label3.setIcon(new ImageIcon("1\\22.jpg"));
						break;
					case 4:
						label3.setIcon(new ImageIcon("1\\23.jpg"));
						break;
					case 5:
						label3.setIcon(new ImageIcon("1\\24.jpg"));
						break;
					case 6:
						label3.setIcon(new ImageIcon("1\\25.jpg"));
						number3 = 0;
						break;
				}
			}
			if(event.getSource() == change4)//四人玩時才用到，因為沒有四人玩，所以也多餘
			{
				button.play();
				number4++;
				switch(number4)
				{
					case 1:
						label4.setIcon(new ImageIcon("1\\20.jpg"));
						break;
					case 2:
						label4.setIcon(new ImageIcon("1\\21.jpg"));
						break;
					case 3:
						label4.setIcon(new ImageIcon("1\\22.jpg"));
						break;
					case 4:
						label4.setIcon(new ImageIcon("1\\23.jpg"));
						break;
					case 5:
						label4.setIcon(new ImageIcon("1\\24.jpg"));
						break;
					case 6:
						label4.setIcon(new ImageIcon("1\\25.jpg"));
						number4 = 0;
						break;
				}
			}
			if(event.getSource() == startgame)//開始遊戲
			{
				ok.play();
				op.stop();
				switch(number5)
				{
					case 1://二人玩時開一個新的視窗
						f.setVisible(false);
						f2  = new Framework( number1 , number2);
						f2.setTitle("Game"); 
						f2.setSize(900, 735); //Frame size
						f2.setLocationRelativeTo(null);
						f2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						f2.setVisible(true);
						break;
					case 2://三人玩時開一個新的視窗，因為沒有三人玩，所以是多餘
						f.setVisible(false);
						f3  = new Framework1( number1 , number2 , number3);
						f3.setTitle("Game"); 
						f3.setSize(900, 735); //Frame size
						f3.setLocationRelativeTo(null);
						f3.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						f3.setVisible(true);
						break;
					case 3://四人玩時開一個新的視窗，因為沒有四人玩，所以是多餘
						f.setVisible(false);
						f4  = new Framework2( number1 , number2 , number3 , number4);
						f4.setTitle("Game"); 
						f4.setSize(900, 735); //Frame size
						f4.setLocationRelativeTo(null);
						f4.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
						f4.setVisible(true);
						break;
				}
			}
		}
	}
	public static void main(String args[])
	{
		f.setTitle("FINALY"); 
		f.setSize(1000, 750); //Frame size
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
