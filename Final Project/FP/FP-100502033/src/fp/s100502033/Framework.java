package fp.s100502033;
import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Date;
//import java.util.Timer;
import javax.swing.*;
public class Framework extends JFrame implements ActionListener
{
	private Timer timer = new Timer(1000, new TimerListener());
	private Timer timera1 = new Timer(5, new TimerListenera1());
	private Timer timera2 = new Timer(5, new TimerListenera2());
	private Timer timera3 = new Timer(5, new TimerListenera3());
	private Timer timera4 = new Timer(5, new TimerListenera4());
	private Timer timera5 = new Timer(5, new TimerListenera5());
	private Timer timera6 = new Timer(5, new TimerListenera6());
	private Timer timerb1 = new Timer(5, new TimerListenerb1());
	private Timer timerb2 = new Timer(5, new TimerListenerb2());
	private Timer timerb3 = new Timer(5, new TimerListenerb3());
	private Timer timerb4 = new Timer(5, new TimerListenerb4());
	private Timer timerb5 = new Timer(5, new TimerListenerb5());
	private Timer timerb6 = new Timer(5, new TimerListenerb6());
	private Timer walk1 = new Timer(370, new Timerwalk1());
	private Timer walk2 = new Timer(370, new Timerwalk2());
	private static JPanel p1 = new JPanel();
	private static JButton start = new JButton("擲骰"); 
	private static JPanel p2 = new JPanel();
	private JLabel label[] = new JLabel[100];
	private JLabel label1 = new JLabel("");
	private JLabel label2 = new JLabel("");
	private JLabel label3 = new JLabel("");
	private JLabel title = new JLabel("玩家一");
	private JLabel name1 = new JLabel("1");
	private JLabel name2 = new JLabel("2");
	private int number[] = new int[90];
	private String string;
	private ImageIcon labelicon1 = new ImageIcon("1\\1.png");
	private ImageIcon labelicon2 = new ImageIcon("1\\2.png");
	private ImageIcon labelicon3 = new ImageIcon("1\\3.png");
	private ImageIcon labelicon4 = new ImageIcon("1\\4.png");
	private ImageIcon labelicon5 = new ImageIcon("1\\5.png");
	private ImageIcon labelicon6 = new ImageIcon("1\\6.png");
	private ImageIcon labelicon7 = new ImageIcon("1\\7.png");
	private ImageIcon labelicon8 = new ImageIcon("1\\8.png");
	private ImageIcon labelicon9 = new ImageIcon("1\\2\\1.png");
	private ImageIcon labelicon10= new ImageIcon("1\\2\\2.png");
	private ImageIcon labelicon11= new ImageIcon("1\\2\\3.png");
	private ImageIcon labelicon12= new ImageIcon("1\\2\\4.png");
	private ImageIcon labelicon13= new ImageIcon("1\\2\\5.png");
	private ImageIcon labelicon14= new ImageIcon("1\\2\\6.png");
	private ImageIcon labelicon15= new ImageIcon("1\\2\\0.gif");
	private ImageIcon set1 = new ImageIcon("1\\9.png");
	private ImageIcon set2 = new ImageIcon("1\\10.png");
	private ImageIcon set3 = new ImageIcon("1\\11.png");
	private ImageIcon set4 = new ImageIcon("1\\12.png");
	private ImageIcon set5 = new ImageIcon("1\\13.png");
	private ImageIcon set6 = new ImageIcon("1\\red.png");
	private ImageIcon set7 = new ImageIcon("1\\dragon.gif");
	private ImageIcon set8 = new ImageIcon("1\\fire.gif");
	private ImageIcon labelicon16;
	private ImageIcon labelicon16a;
	private ImageIcon labelicon16b;
	private ImageIcon labelicon16c;
	private ImageIcon labelicon16d;
	private ImageIcon labelicon17;
	private ImageIcon labelicon17a;
	private ImageIcon labelicon17b;
	private ImageIcon labelicon17c;
	private ImageIcon labelicon17d;
	private FileReader inputfile;
	private BufferedReader read;
	private String character1;
	private String character2;
	private int number1 ;
	private int number2 ;
	private int order = 0;
	private int x = 750;
	private int y = 585;
	private int x1 = 750;
	private int y1 = 585;
	//750 585
	private int move1;
	private int move2;
	private int distance1 = 1;
	private int distance2 = 1;
	private int win = 0 ;
	private int wall = 0;
	private int wall2 = 0;
	private  AudioClip in;
	private  AudioClip car; 
	private  AudioClip six; 
	private  AudioClip wind;
	private  AudioClip stop;
	private  AudioClip fire;
	private  AudioClip drink;
	private  AudioClip down;
	private  AudioClip water;
	private  AudioClip god;
	private  AudioClip finish;
	private  AudioClip dragon;
	private int score1 = 0;
	private int score2 = 0;
	private FileWriter outputfile;
	private BufferedWriter outputstring;
	private FileWriter outputfile1;
	private BufferedWriter outputstring1;
	private FileWriter outputfile2;
	private BufferedWriter outputstring2;
	private FileWriter outputfile3;
	private BufferedWriter outputstring3;
	private Date myDate = new Date();
	public Framework(int num1 , int num2) 
	{
		six = Applet.newAudioClip(getClass().getResource("music\\six.wav"));//117 ~ 128設置音效檔
		car = Applet.newAudioClip(getClass().getResource("music\\car.wav"));
		wind = Applet.newAudioClip(getClass().getResource("music\\wind.wav"));
		stop = Applet.newAudioClip(getClass().getResource("music\\stop.wav"));
		fire = Applet.newAudioClip(getClass().getResource("music\\fire.wav"));
		drink = Applet.newAudioClip(getClass().getResource("music\\drink.wav"));
		down = Applet.newAudioClip(getClass().getResource("music\\down.wav"));
		water = Applet.newAudioClip(getClass().getResource("music\\water.wav"));
		god = Applet.newAudioClip(getClass().getResource("music\\god.wav"));
		finish = Applet.newAudioClip(getClass().getResource("music\\finish.wav"));
		dragon = Applet.newAudioClip(getClass().getResource("music\\dragon.wav"));
		in = Applet.newAudioClip(getClass().getResource("music\\in.mid"));
		in.loop();
		try//130~169 output file
		{
			outputfile = new FileWriter("score.txt");
			outputstring = new BufferedWriter(outputfile);
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try
		{
			outputfile2 = new FileWriter("character.txt");
			outputstring2 = new BufferedWriter(outputfile2);
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try
		{
			outputfile1 = new FileWriter("score.txt" , true);
			outputstring1 = new BufferedWriter(outputfile1);
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try
		{
			outputfile3 = new FileWriter("character.txt" , true);
			outputstring3 = new BufferedWriter(outputfile3);
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		number1 = num1;
		number2 = num2;
		switch(number1)//決定玩家一人物
		{
			case 1:
				labelicon16 = new ImageIcon("1\\01.png");
				labelicon16a = new ImageIcon("1\\A\\1.gif");
				labelicon16b = new ImageIcon("1\\A\\2.gif");
				labelicon16c = new ImageIcon("1\\A\\3.gif");
				labelicon16d = new ImageIcon("1\\A\\4.gif");
				break;
			case 2:
				labelicon16 = new ImageIcon("1\\02.png");
				labelicon16a = new ImageIcon("1\\B\\1.gif");
				labelicon16b = new ImageIcon("1\\B\\2.gif");
				labelicon16c = new ImageIcon("1\\B\\3.gif");
				labelicon16d = new ImageIcon("1\\B\\4.gif");
				break;
			case 3:
				labelicon16 = new ImageIcon("1\\03.png");
				labelicon16a = new ImageIcon("1\\C\\1.gif");
				labelicon16b = new ImageIcon("1\\C\\2.gif");
				labelicon16c = new ImageIcon("1\\C\\3.gif");
				labelicon16d = new ImageIcon("1\\C\\4.gif");
				break;
			case 4:
				labelicon16 = new ImageIcon("1\\04.png");
				labelicon16a = new ImageIcon("1\\D\\1.gif");
				labelicon16b = new ImageIcon("1\\D\\2.gif");
				labelicon16c = new ImageIcon("1\\D\\3.gif");
				labelicon16d = new ImageIcon("1\\D\\4.gif");
				break;
			case 5:
				labelicon16 = new ImageIcon("1\\05.png");
				labelicon16a = new ImageIcon("1\\E\\1.gif");
				labelicon16b = new ImageIcon("1\\E\\2.gif");
				labelicon16c = new ImageIcon("1\\E\\3.gif");
				labelicon16d = new ImageIcon("1\\E\\4.gif");
				break;
			case 0:
				labelicon16 = new ImageIcon("1\\06.png");
				labelicon16a = new ImageIcon("1\\F\\1.gif");
				labelicon16b = new ImageIcon("1\\F\\2.gif");
				labelicon16c = new ImageIcon("1\\F\\3.gif");
				labelicon16d = new ImageIcon("1\\F\\4.gif");
				break;
		}
		switch(number2)//決定玩家二人物
		{
			case 1:
				labelicon17 = new ImageIcon("1\\01.png");
				labelicon17a = new ImageIcon("1\\A\\1.gif");
				labelicon17b = new ImageIcon("1\\A\\2.gif");
				labelicon17c = new ImageIcon("1\\A\\3.gif");
				labelicon17d = new ImageIcon("1\\A\\4.gif");
				break;
			case 2:
				labelicon17 = new ImageIcon("1\\02.png");
				labelicon17a = new ImageIcon("1\\B\\1.gif");
				labelicon17b = new ImageIcon("1\\B\\2.gif");
				labelicon17c = new ImageIcon("1\\B\\3.gif");
				labelicon17d = new ImageIcon("1\\B\\4.gif");
				break;
			case 3:
				labelicon17 = new ImageIcon("1\\03.png");
				labelicon17a = new ImageIcon("1\\C\\1.gif");
				labelicon17b = new ImageIcon("1\\c\\2.gif");
				labelicon17c = new ImageIcon("1\\C\\3.gif");
				labelicon17d = new ImageIcon("1\\C\\4.gif");
				break;
			case 4:
				labelicon17 = new ImageIcon("1\\04.png");
				labelicon17a = new ImageIcon("1\\D\\1.gif");
				labelicon17b = new ImageIcon("1\\D\\2.gif");
				labelicon17c = new ImageIcon("1\\D\\3.gif");
				labelicon17d = new ImageIcon("1\\D\\4.gif");
				break;
			case 5:
				labelicon17 = new ImageIcon("1\\05.png");
				labelicon17a = new ImageIcon("1\\E\\1.gif");
				labelicon17b = new ImageIcon("1\\E\\2.gif");
				labelicon17c = new ImageIcon("1\\E\\3.gif");
				labelicon17d = new ImageIcon("1\\E\\4.gif");
				break;
			case 0:
				labelicon17 = new ImageIcon("1\\06.png");
				labelicon17a = new ImageIcon("1\\F\\1.gif");
				labelicon17b = new ImageIcon("1\\F\\2.gif");
				labelicon17c = new ImageIcon("1\\F\\3.gif");
				labelicon17d = new ImageIcon("1\\F\\4.gif");
				break;
		}
		JPanel p1 = new JPanel()
		{
			public void paintComponent(Graphics g)//264~297設置背景圖
			{
				int x = 0;
				int y = 0;
				super.paintComponent(g);

				labelicon2.paintIcon(this, g, 0, 0);
				for (int i = 0 ; i < 72 ; i++)
				{
					x = x + 75;
					labelicon1.paintIcon(this, g, x, y);
					if( x == 600)
					{
						x = 0 ;
						y = y +75;
					}
				}
				labelicon6.paintIcon(this, g, 675, 0);
				labelicon6.paintIcon(this, g, 675, 150);
				labelicon6.paintIcon(this, g, 675, 300);
				labelicon6.paintIcon(this, g, 675, 450);
				labelicon4.paintIcon(this, g, 0, 75);
				labelicon4.paintIcon(this, g, 0, 225);
				labelicon4.paintIcon(this, g, 0, 375);
				labelicon4.paintIcon(this, g, 0, 525);
				labelicon5.paintIcon(this, g, 675, 75);
				labelicon5.paintIcon(this, g, 675, 225);
				labelicon5.paintIcon(this, g, 675, 375);
				labelicon5.paintIcon(this, g, 675, 525);
				labelicon3.paintIcon(this, g, 0, 150);
				labelicon3.paintIcon(this, g, 0, 300);
				labelicon3.paintIcon(this, g, 0, 450);
				labelicon3.paintIcon(this, g, 0, 600);
				labelicon7.paintIcon(this, g, 675, 600);
				labelicon8.paintIcon(this, g, 375, 375);//298~306設置事件圖
				set1.paintIcon(this, g, 300, 525);
				set2.paintIcon(this, g, 225, 150);
				set3.paintIcon(this, g, 525, 225);
				set4.paintIcon(this, g, 75, 300);
				set5.paintIcon(this, g, 150, 450);
				set6.paintIcon(this, g, 465, 595);
				set7.paintIcon(this, g, 225, 0);
				set8.paintIcon(this, g, 375, 55);
				g.setColor(Color.white);
				x = 675;
				y = 610;
				int z = 75;
				for (int i = 0 ; i < 90 ; i++)
				{
					number[i] = i + 1;
					string = Integer.toString(number[i]);
					g.drawString(string, x, y);
					x = x - z;
					if( x < 0 || x > 675)
					{
						y = y -75;
						if( x < 0 )
						{
							x = 0 ;
							z = -75;
						}
						else if( x > 0)
						{
							x = 675 ;
							z = 75;
						}
					}

				}
			}
		};
		//p1.setBorder(BorderFactory.createTitledBorder("Test"));
		label1 = new JLabel(labelicon9);
		label2 = new JLabel(labelicon16);
		label3 = new JLabel(labelicon17);
		p1.setOpaque(false);
		p1.setLayout(null);
		start.setBounds(780 , 100 , 75 , 25);
		title.setBounds(780, 100, 100, 100);
		title.setFont(new Font("Serif" , Font.BOLD , 24));
		label1.setBounds(780, 0, 75, 75);
		label2.setBounds(750, 585, 75, 75);
		label3.setBounds(770, 585, 75, 75);
		name1.setBounds(785, 550, 75, 75);
		name2.setBounds(805, 550, 75, 75);
		name1.setForeground(Color.red);
		name2.setForeground(Color.orange);
		title.setForeground(Color.red);
		add(title);
		add(label1);
		add(start);
		add(label2);
		add(label3);
		add(name1);
		add(name2);
		add(p1);
		start.addActionListener(this);
	}
	class Timerwalk1 implements ActionListener //計算玩家一的步數
	{
		public void actionPerformed(ActionEvent e) 
	    {
			score1++;
	    }
	}
	class Timerwalk2 implements ActionListener //計算玩家二的步數
	{
		public void actionPerformed(ActionEvent e) 
	    {
			score2++;
	    }
	}
	class TimerListenera1 implements ActionListener 
	{
		public void actionPerformed(ActionEvent e) 
	    {
			label2.setBounds(x, y, 75, 75);//設置人物初始位置
			name1.setBounds(x + 35, y - 35, 75, 75);
			if ( x > 2 && (y == 585 || y == 435 || y == 285 || y == 135))
			{
				x--;
				label2.setIcon(labelicon16b);
				distance1++;//決定移動的步數
				switch(distance1)
				{
					case 75:
					distance1 = 0;
					timera1.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if( x > 425 && x < 475 && y == 585)//402~437觸發事件時的動作
					{
						drink.play();
						JOptionPane.showMessageDialog(null, "喝下RED BULL充滿能量，進至9");
						x = x - 75 * 5;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if( x > 225 && x < 300 && y == 135)
					{
						car.play();
						JOptionPane.showMessageDialog(null, "被汽車撞飛了，退至62");
						x = x + 75 * 5;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if( x > 50 && x < 100 && y == 285)
					{
						water.play();
						JOptionPane.showMessageDialog(null, "踩到水面滑倒了，退至45");
						x = x + 75 * 4;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if( x > 150 && x < 225 && y == 435)
					{
						god.play();
						JOptionPane.showMessageDialog(null, "獲得燈神的幫助，可再擲一次骰");
						order = 0;
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					break;
				}
			}
			else if( x >= 2 && y == 0)
			{
				if( x == 2 )
				{
					wall = 1;
				}
				if(wall == 1)
				{
					x++;
					label2.setIcon(labelicon16c);
				}
				else
				{
					x--;
					label2.setIcon(labelicon16b);
				}
				distance1++;//決定移動的步數
				switch(distance1)
				{
					case 75:
					distance1 = 0;
					timera1.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if( x > 200 && x < 300 && y == 0)//469~486觸發事件時的動作
					{
						dragon.play();
						JOptionPane.showMessageDialog(null, "被兇猛的野獸嚇跑了，退至82");
						x = 615;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					wall = 0;
					if( x < 50)//488~566決定第一名和第二名時的動作
					{
						if(win == 0)
						{
							win++;
							finish.play();
							JOptionPane.showMessageDialog(null, "第一名「玩家一」\n用了" + score1 + "步到達終點");
							String string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
							for(int i = 0 ; i < 2 ; i--)
							{
								if(string.length() > 4)
								{
									JOptionPane.showMessageDialog(null, "輸入錯誤，請重新輸入");
									string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
								}
								else
								{
									break;
								}
							}
							int Year = myDate.getYear() + 1900;
							int Month = myDate.getMonth() + 1;
							int Date = myDate.getDate();
							string = score1 + "\t" + string + "\t\t" + Year + "/" + Month + "/" + Date;
							try 
							{
								outputstring.write(string);
								outputstring2.write(number1 + "");
								outputstring.close();
								outputstring2.close();
							} 
							catch (IOException e1)
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							label2.setVisible(false);
							name1.setVisible(false);
							order = 3;
						}
						else if(win == 1)
						{
							win = 0;
							finish.play();
							JOptionPane.showMessageDialog(null, "第二名「玩家一」\n用了" + score1 + "步到達終點");
							String string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
							for(int i = 0 ; i < 2 ; i--)
							{
								if(string.length() > 4)
								{
									JOptionPane.showMessageDialog(null, "輸入錯誤，請重新輸入");
									string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
								}
								else
								{
									break;
								}
							}
							int Year = myDate.getYear() + 1900;
							int Month = myDate.getMonth() + 1;
							int Date = myDate.getDate();
							string = score1 + "\t" + string + "\t\t" + Year + "/" + Month + "/" + Date;
							try 
							{
								outputstring1.write(string);
								outputstring3.write(number1 + "");
								outputstring3.close();
								outputstring1.close();
							}
							catch (IOException e1) 
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							start.setVisible(false);
							label2.setVisible(false);
							name1.setVisible(false);
							JOptionPane.showMessageDialog(null, ("遊戲結束"));
							System.exit(0);
						}
					}
					break;
				}
			}
			else if ( x < 680 &&( y == 525 || y == 375 || y == 225 || y == 75))
			{
				x++;
				label2.setIcon(labelicon16c);
				distance1++;//決定行走的步數
				switch(distance1)
				{
					case 75:
					distance1 = 0;
					timera1.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if( x > 350 && x < 450 && y == 75)//592~630解發事件時的動作
					{
						wind.play();
						JOptionPane.showMessageDialog(null, "愛到龍捲風的襲擊，退至65");
						x = 394;
						y = 135;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if( x > 513 && x < 573 && y == 225)
					{
						fire.play();
						JOptionPane.showMessageDialog(null, "乘坐火箭直達78");
						x = x -6;
						y = 75;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if( x > 350 && x < 400 && y == 375)
					{
						down.play();
						JOptionPane.showMessageDialog(null, "跌下黑洞，返回起點");
						x = 750;
						y = 585;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if(x < 375 && x > 300 && y == 525)
					{
						stop.play();
						JOptionPane.showMessageDialog(null, "遇到路障，暫停一次");
						order = 2;
					}
					break;
				}
			}
			else if( x == 680 &&  y > 0)
			{
				y--;
				label2.setIcon(labelicon16d);
				distance1++;//決定行走的步數
				switch(distance1)
				{
					case 75:
					distance1 = 0;
					timera1.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					break;
				}
			}
			else if( x == 2 && y > 0)
			{
				y--;
				label2.setIcon(labelicon16d);
				distance1++;
				switch(distance1)
				{
					case 75:
					distance1 = 0;
					timera1.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					break;
				}
			}
	    }
	}
	class TimerListenera2 implements ActionListener //TimerListenera1~TimerListenera6 & TimerListenerb1~TimerListenerb6都是做一樣的CODE，因為是決定玩家一和玩家二所擲的點數才設置12個TIMER
	{
		public void actionPerformed(ActionEvent e) 
	    {
			label2.setBounds(x, y, 75, 75);
			name1.setBounds(x + 35, y - 35, 75, 75);
			if ( x > 2 && (y == 585 || y == 435 || y == 285 || y == 135))
			{
				x--;
				label2.setIcon(labelicon16b);
				distance1++;
				switch(distance1)
				{
					case 150:
					distance1 = 0;
					timera2.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if( x > 425 && x < 475 && y == 585)
					{
						drink.play();
						JOptionPane.showMessageDialog(null, "喝下RED BULL充滿能量，進至9");
						x = x - 75 * 5;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if( x > 225 && x < 300 && y == 135)
					{
						car.play();
						JOptionPane.showMessageDialog(null, "被汽車撞飛了，退至62");
						x = x + 75 * 5;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if( x > 50 && x < 100 && y == 285)
					{
						water.play();
						JOptionPane.showMessageDialog(null, "踩到水面滑倒了，退至45");
						x = x + 75 * 4;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if( x > 150 && x < 225 && y == 435)
					{
						god.play();
						JOptionPane.showMessageDialog(null, "獲得燈神的幫助，可再擲一次骰");
						title.setForeground(Color.red);
						order = 0;
						title.setText("玩家一");
					}
					break;
				}
			}
			else if( x >= 2 && y == 0)
			{
				if( x == 2 )
				{
					wall = 1;
				}
				if(wall == 1)
				{
					x++;
					label2.setIcon(labelicon16c);
				}
				else
				{
					x--;
					label2.setIcon(labelicon16b);
				}
				distance1++;
				switch(distance1)
				{
					case 150:
					distance1 = 0;
					timera2.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if( x > 200 && x < 300 && y == 0)
					{
						dragon.play();
						JOptionPane.showMessageDialog(null, "被兇猛的野獸嚇跑了，退至82");
						x = 615;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					wall = 0;
					if( x < 50)
					{
						if(win == 0)
						{
							win++;
							finish.play();
							JOptionPane.showMessageDialog(null, "第一名「玩家一」\n用了" + score1 + "步到達終點");
							String string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
							for(int i = 0 ; i < 2 ; i--)
							{
								if(string.length() > 4)
								{
									JOptionPane.showMessageDialog(null, "輸入錯誤，請重新輸入");
									string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
								}
								else
								{
									break;
								}
							}
							int Year = myDate.getYear() + 1900;
							int Month = myDate.getMonth() + 1;
							int Date = myDate.getDate();
							string = score1 + "\t" + string + "\t\t" + Year + "/" + Month + "/" + Date;
							try 
							{
								outputstring.write(string);
								outputstring2.write(number1 + "");
								outputstring.close();
								outputstring2.close();
							} 
							catch (IOException e1)
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							label2.setVisible(false);
							name1.setVisible(false);
							order = 3;
						}
						else if(win == 1)
						{
							win = 0;
							finish.play();
							JOptionPane.showMessageDialog(null, "第二名「玩家一」\n用了" + score1 + "步到達終點");
							String string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
							for(int i = 0 ; i < 2 ; i--)
							{
								if(string.length() > 4)
								{
									JOptionPane.showMessageDialog(null, "輸入錯誤，請重新輸入");
									string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
								}
								else
								{
									break;
								}
							}
							int Year = myDate.getYear() + 1900;
							int Month = myDate.getMonth() + 1;
							int Date = myDate.getDate();
							string = score1 + "\t" + string + "\t\t" + Year + "/" + Month + "/" + Date;
							try 
							{
								outputstring1.write(string);
								outputstring3.write(number1 + "");
								outputstring3.close();
								outputstring1.close();
							}
							catch (IOException e1) 
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							start.setVisible(false);
							label2.setVisible(false);
							name1.setVisible(false);
							JOptionPane.showMessageDialog(null, ("遊戲結束"));
							System.exit(0);
						}
					}
					break;
				}
			}
			else if ( x < 680 &&( y == 525 || y == 375 || y == 225 || y == 75))
			{
				x++;
				label2.setIcon(labelicon16c);
				distance1++;
				switch(distance1)
				{
					case 150:
					distance1 = 0;
					timera2.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if( x > 350 && x < 450 && y == 75)
					{		
						wind.play();
						JOptionPane.showMessageDialog(null, "愛到龍捲風的襲擊，退至65");
						x = 394;
						y = 135;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if( x > 513 && x < 573 && y == 225)
					{	
						fire.play();
						JOptionPane.showMessageDialog(null, "乘坐火箭直達78");
						x = x -6;
						y = 75;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if(x > 350 && x < 400 && y == 375)
					{
						down.play();
						JOptionPane.showMessageDialog(null, "跌下黑洞，返回起點");
						x = 750;
						y = 585;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if(x < 375 && x > 300 && y == 525)
					{
						stop.play();
						JOptionPane.showMessageDialog(null, "遇到路障，暫停一次");
						order = 2;
					}
					break;
				}
			}
			else if( x == 680 &&  y > 0)
			{
				y--;
				label2.setIcon(labelicon16d);
				distance1++;
				switch(distance1)
				{
					case 150:
					distance1 = 0;
					timera2.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					break;
				}
			}
			else if( x == 2 && y > 0)
			{
				y--;
				label2.setIcon(labelicon16d);
				distance1++;
				switch(distance1)
				{
					case 150:
					distance1 = 0;
					timera2.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					break;
				}
			}
	    }
	}
	class TimerListenera3 implements ActionListener 
	{
		public void actionPerformed(ActionEvent e) 
	    {
			label2.setBounds(x, y, 75, 75);
			name1.setBounds(x + 35, y - 35, 75, 75);
			if ( x > 2 && (y == 585 || y == 435 || y == 285 || y == 135))
			{
				x--;
				label2.setIcon(labelicon16b);
				distance1++;
				switch(distance1)
				{
					case 225:
					distance1 = 0;
					timera3.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if( x > 425 && x < 475 && y == 585)
					{
						drink.play();
						JOptionPane.showMessageDialog(null, "喝下RED BULL充滿能量，進至9");
						x = x - 75 * 5;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if( x > 225 && x < 300 && y == 135)
					{						
						car.play();
						JOptionPane.showMessageDialog(null, "被汽車撞飛了，退至62");
						x = x + 75 * 5;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if( x > 50 && x < 100 && y == 285)
					{
						water.play();
						JOptionPane.showMessageDialog(null, "踩到水面滑倒了，退至45");
						x = x + 75 * 4;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if( x > 150 && x < 225 && y == 435)
					{					
						god.play();
						JOptionPane.showMessageDialog(null, "獲得燈神的幫助，可再擲一次骰");
						order = 0;
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					break;
				}
			}
			else if( x >= 2 && y == 0)
			{
				if( x == 2 )
				{
					wall = 1;
				}
				if(wall == 1)
				{
					x++;
					label2.setIcon(labelicon16c);
				}
				else
				{
					x--;
					label2.setIcon(labelicon16b);
				}
				distance1++;
				switch(distance1)
				{
					case 225:
					distance1 = 0;
					timera3.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if( x > 200 && x < 300 && y == 0)
					{						
						dragon.play();
						JOptionPane.showMessageDialog(null, "被兇猛的野獸嚇跑了，退至82");
						x = 615;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					wall = 0;
					if( x < 50 )
					{
						if(win == 0)
						{
							win++;
							finish.play();
							JOptionPane.showMessageDialog(null, "第一名「玩家一」\n用了" + score1 + "步到達終點");
							String string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
							for(int i = 0 ; i < 2 ; i--)
							{
								if(string.length() > 4)
								{
									JOptionPane.showMessageDialog(null, "輸入錯誤，請重新輸入");
									string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
								}
								else
								{
									break;
								}
							}
							int Year = myDate.getYear() + 1900;
							int Month = myDate.getMonth() + 1;
							int Date = myDate.getDate();
							string = score1 + "\t" + string + "\t\t" + Year + "/" + Month + "/" + Date;
							try 
							{
								outputstring.write(string);
								outputstring2.write(number1 + "");
								outputstring.close();
								outputstring2.close();
							} 
							catch (IOException e1)
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							label2.setVisible(false);
							name1.setVisible(false);
							order = 3;
						}
						else if(win == 1)
						{
							win = 0;
							finish.play();
							JOptionPane.showMessageDialog(null, "第二名「玩家一」\n用了" + score1 + "步到達終點");
							String string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
							for(int i = 0 ; i < 2 ; i--)
							{
								if(string.length() > 4)
								{
									JOptionPane.showMessageDialog(null, "輸入錯誤，請重新輸入");
									string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
								}
								else
								{
									break;
								}
							}
							int Year = myDate.getYear() + 1900;
							int Month = myDate.getMonth() + 1;
							int Date = myDate.getDate();
							string = score1 + "\t" + string + "\t\t" + Year + "/" + Month + "/" + Date;
							try 
							{
								outputstring1.write(string);
								outputstring3.write(number1 + "");
								outputstring3.close();
								outputstring1.close();
							}
							catch (IOException e1) 
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							start.setVisible(false);
							label2.setVisible(false);
							name1.setVisible(false);
							JOptionPane.showMessageDialog(null, ("遊戲結束"));
							System.exit(0);
						}
					}
					break;
				}
			}
			else if ( x < 680 &&( y == 525 || y == 375 || y == 225 || y == 75))
			{
				x++;
				label2.setIcon(labelicon16c);
				distance1++;
				switch(distance1)
				{
					case 225:
					distance1 = 0;
					timera3.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if( x > 350 && x < 450 && y == 75)
					{				
						wind.play();
						JOptionPane.showMessageDialog(null, "愛到龍捲風的襲擊，退至65");
						x = 394;
						y = 135;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if( x > 513 && x < 573 && y == 225)
					{	
						fire.play();
						JOptionPane.showMessageDialog(null, "乘坐火箭直達78");
						x = x -6;
						y = 75;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if( x > 350 && x < 400 && y == 375)
					{
						down.play();
						JOptionPane.showMessageDialog(null, "跌下黑洞，返回起點");
						x = 750;
						y = 585;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if(x < 375 && x > 300 && y == 525)
					{
						stop.play();
						JOptionPane.showMessageDialog(null, "遇到路障，暫停一次");
						order = 2;
					}
					break;
				}
			}
			else if( x == 680 &&  y > 0)
			{
				y--;
				label2.setIcon(labelicon16d);
				distance1++;
				switch(distance1)
				{
					case 225:
					distance1 = 0;
					timera3.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					break;
				}
			}
			else if( x == 2 && y > 0)
			{
				y--;
				label2.setIcon(labelicon16d);
				distance1++;
				switch(distance1)
				{
					case 225:
					distance1 = 0;
					timera3.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					break;
				}
			}
	    }
	}
	class TimerListenera4 implements ActionListener 
	{
		public void actionPerformed(ActionEvent e) 
	    {
			label2.setBounds(x, y, 75, 75);
			name1.setBounds(x + 35, y - 35, 75, 75);
			if ( x > 2 && (y == 585 || y == 435 || y == 285 || y == 135))
			{
				x--;
				label2.setIcon(labelicon16b);
				distance1++;
				switch(distance1)
				{
					case 300:
					distance1 = 0;
					timera4.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if( x > 425 && x < 475 && y == 585)
					{	
						drink.play();
						JOptionPane.showMessageDialog(null, "喝下RED BULL充滿能量，進至9");
						x = x - 75 * 5;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if( x > 225 && x < 300 && y == 135)
					{
						car.play();
						JOptionPane.showMessageDialog(null, "被汽車撞飛了，退至62");
						x = x + 75 * 5;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if( x > 50 && x < 100 && y == 285)
					{
						water.play();
						JOptionPane.showMessageDialog(null, "踩到水面滑倒了，退至45");
						x = x + 75 * 4;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if( x > 150 && x < 225 && y == 435)
					{
						god.play();
						JOptionPane.showMessageDialog(null, "獲得燈神的幫助，可再擲一次骰");
						order = 0;
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					break;
				}
			}
			else if( x >= 2 && y == 0)
			{
				if( x == 2 )
				{
					wall = 1;
				}
				if(wall == 1)
				{
					x++;
					label2.setIcon(labelicon16c);
				}
				else
				{
					x--;
					label2.setIcon(labelicon16b);
				}
				distance1++;
				switch(distance1)
				{
					case 300:
					distance1 = 0;
					timera4.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if( x > 200 && x < 300 && y == 0)
					{
						dragon.play();
						JOptionPane.showMessageDialog(null, "被兇猛的野獸嚇跑了，退至82");
						x = 615;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					wall = 0;
					if( x < 50)
					{
						if(win == 0)
						{
							win++;
							finish.play();
							JOptionPane.showMessageDialog(null, "第一名「玩家一」\n用了" + score1 + "步到達終點");
							String string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
							for(int i = 0 ; i < 2 ; i--)
							{
								if(string.length() > 4)
								{
									JOptionPane.showMessageDialog(null, "輸入錯誤，請重新輸入");
									string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
								}
								else
								{
									break;
								}
							}
							int Year = myDate.getYear() + 1900;
							int Month = myDate.getMonth() + 1;
							int Date = myDate.getDate();
							string = score1 + "\t" + string + "\t\t" + Year + "/" + Month + "/" + Date;
							try 
							{
								outputstring.write(string);
								outputstring2.write(number1 + "");
								outputstring.close();
								outputstring2.close();
							} 
							catch (IOException e1)
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							label2.setVisible(false);
							name1.setVisible(false);
							order = 3;
						}
						else if(win == 1)
						{
							win = 0;
							finish.play();
							JOptionPane.showMessageDialog(null, "第二名「玩家一」\n用了" + score1 + "步到達終點");
							String string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
							for(int i = 0 ; i < 2 ; i--)
							{
								if(string.length() > 4)
								{
									JOptionPane.showMessageDialog(null, "輸入錯誤，請重新輸入");
									string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
								}
								else
								{
									break;
								}
							}
							int Year = myDate.getYear() + 1900;
							int Month = myDate.getMonth() + 1;
							int Date = myDate.getDate();
							string = score1 + "\t" + string + "\t\t" + Year + "/" + Month + "/" + Date;
							try 
							{
								outputstring1.write(string);
								outputstring3.write(number1 + "");
								outputstring3.close();
								outputstring1.close();
							}
							catch (IOException e1) 
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							start.setVisible(false);
							label2.setVisible(false);
							name1.setVisible(false);
							JOptionPane.showMessageDialog(null, ("遊戲結束"));
							System.exit(0);
						}
					}
					break;
				}
			}
			else if ( x < 680 &&( y == 525 || y == 375 || y == 225 || y == 75))
			{
				x++;
				label2.setIcon(labelicon16c);
				distance1++;
				switch(distance1)
				{
					case 300:
					distance1 = 0;
					timera4.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if( x > 350 && x < 450 && y == 75)
					{
						wind.play();
						JOptionPane.showMessageDialog(null, "愛到龍捲風的襲擊，退至65");
						x = 394;
						y = 135;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if( x > 513 && x < 573 && y == 225)
					{				
						fire.play();
						JOptionPane.showMessageDialog(null, "乘坐火箭直達78");
						x = x -6;
						y = 75;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if( x > 350 && x < 400 && y == 375)
					{	
						down.play();
						JOptionPane.showMessageDialog(null, "跌下黑洞，返回起點");
						x = 750;
						y = 585;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if(x < 375 && x > 300 && y == 525)
					{
						stop.play();
						JOptionPane.showMessageDialog(null, "遇到路障，暫停一次");
						order = 2;
					}
					break;
				}
			}
			else if( x == 680 &&  y > 0)
			{
				y--;
				label2.setIcon(labelicon16d);
				distance1++;
				switch(distance1)
				{
					case 300:
					distance1 = 0;
					timera4.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					break;
				}
			}
			else if( x == 2 && y > 0)
			{
				y--;
				label2.setIcon(labelicon16d);
				distance1++;
				switch(distance1)
				{
					case 300:
					distance1 = 0;
					timera4.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					break;
				}
			}
	    }
	}
	class TimerListenera5 implements ActionListener 
	{
		public void actionPerformed(ActionEvent e) 
	    {
			label2.setBounds(x, y, 75, 75);
			name1.setBounds(x + 35, y - 35, 75, 75);
			if ( x > 2 && (y == 585 || y == 435 || y == 285 || y == 135) )
			{
				x--;
				label2.setIcon(labelicon16b);
				distance1++;
				switch(distance1)
				{
					case 375:
					distance1 = 0;
					timera5.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if( x > 425 && x < 475 && y == 585)
					{
						drink.play();
						JOptionPane.showMessageDialog(null, "喝下RED BULL充滿能量，進至9");
						x = x - 75 * 5;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if( x > 225 && x < 300 && y == 135)
					{
						car.play();
						JOptionPane.showMessageDialog(null, "被汽車撞飛了，退至62");
						x = x + 75 * 5;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if( x > 50 && x < 100 && y == 285)
					{
						water.play();
						JOptionPane.showMessageDialog(null, "踩到水面滑倒了，退至45");
						x = x + 75 * 4;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if( x > 150 && x < 225 && y == 435)
					{
						god.play();
						JOptionPane.showMessageDialog(null, "獲得燈神的幫助，可再擲一次骰");
						order = 0;
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					break;
				}
			}
			else if( x >= 2 && y == 0)
			{
				if( x == 2)
				{
					wall = 1;
				}
				if(wall == 1)
				{
					x++;
					label2.setIcon(labelicon16c);
				}
				else
				{
					x--;
					label2.setIcon(labelicon16b);
				}
				distance1++;
				switch(distance1)
				{
					case 375:
					distance1 = 0;
					timera5.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if( x > 200 && x < 300 && y == 0)
					{
						dragon.play();
						JOptionPane.showMessageDialog(null, "被兇猛的野獸嚇跑了，退至82");
						x = 615;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					wall = 0;
					if( x < 50)
					{
						if(win == 0)
						{
							win++;
							finish.play();
							JOptionPane.showMessageDialog(null, "第一名「玩家一」\n用了" + score1 + "步到達終點");
							String string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
							for(int i = 0 ; i < 2 ; i--)
							{
								if(string.length() > 4)
								{
									JOptionPane.showMessageDialog(null, "輸入錯誤，請重新輸入");
									string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
								}
								else
								{
									break;
								}
							}
							int Year = myDate.getYear() + 1900;
							int Month = myDate.getMonth() + 1;
							int Date = myDate.getDate();
							string = score1 + "\t" + string + "\t\t" + Year + "/" + Month + "/" + Date;
							try 
							{
								outputstring.write(string);
								outputstring2.write(number1 + "");
								outputstring.close();
								outputstring2.close();
							} 
							catch (IOException e1)
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							label2.setVisible(false);
							name1.setVisible(false);
							order = 3;
						}
						else if(win == 1)
						{
							win = 0;
							finish.play();
							JOptionPane.showMessageDialog(null, "第二名「玩家一」\n用了" + score1 + "步到達終點");
							String string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
							for(int i = 0 ; i < 2 ; i--)
							{
								if(string.length() > 4)
								{
									JOptionPane.showMessageDialog(null, "輸入錯誤，請重新輸入");
									string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
								}
								else
								{
									break;
								}
							}
							int Year = myDate.getYear() + 1900;
							int Month = myDate.getMonth() + 1;
							int Date = myDate.getDate();
							string = score1 + "\t" + string + "\t\t" + Year + "/" + Month + "/" + Date;
							try 
							{
								outputstring1.write(string);
								outputstring3.write(number1 + "");
								outputstring3.close();
								outputstring1.close();
							}
							catch (IOException e1) 
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							start.setVisible(false);
							label2.setVisible(false);
							name1.setVisible(false);
							JOptionPane.showMessageDialog(null, ("遊戲結束"));
							System.exit(0);
						}
					}
					break;
				}
			}
			else if ( x < 680 &&( y == 525 || y == 375 || y == 225 || y == 75))
			{
				x++;
				label2.setIcon(labelicon16c);
				distance1++;
				switch(distance1)
				{
					case 375:
					distance1 = 0;
					timera5.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if( x > 350 && x < 450 && y == 75)
					{
						wind.play();
						JOptionPane.showMessageDialog(null, "愛到龍捲風的襲擊，退至65");
						x = 394;
						y = 135;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if( x > 513 && x < 573 && y == 225)
					{					
						fire.play();
						JOptionPane.showMessageDialog(null, "乘坐火箭直達78");
						x = x -6;
						y = 75;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if( x > 350 && x < 400 && y == 375)
					{						
						down.play();
						JOptionPane.showMessageDialog(null, "跌下黑洞，返回起點");
						x = 750;
						y = 585;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if(x < 375 && x > 300 && y == 525)
					{					
						stop.play();
						JOptionPane.showMessageDialog(null, "遇到路障，暫停一次");
						order = 2;
					}
					break;
				}
			}
			else if( x == 680 &&  y > 0)
			{
				y--;
				label2.setIcon(labelicon16d);
				distance1++;
				switch(distance1)
				{
					case 375:
					distance1 = 0;
					timera5.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					break;
				}
			}
			else if( x == 2 && y > 0)
			{
				y--;
				label2.setIcon(labelicon16d);
				distance1++;
				switch(distance1)
				{
					case 375:
					distance1 = 0;
					timera5.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					break;
				}
			}
	    }
	}
	class TimerListenera6 implements ActionListener 
	{
		public void actionPerformed(ActionEvent e) 
	    {
			label2.setBounds(x, y, 75, 75);
			name1.setBounds(x + 35, y - 35, 75, 75);
			if ( x > 2 && (y == 585 || y == 435 || y == 285 || y == 135) )
			{
				x--;
				label2.setIcon(labelicon16b);
				distance1++;
				switch(distance1)
				{
					case 450:
					distance1 = 0;
					timera6.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if( x > 425 && x < 475 && y == 585)
					{				
						drink.play();
						JOptionPane.showMessageDialog(null, "喝下RED BULL充滿能量，進至9");
						x = x - 75 * 5;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if( x > 225 && x < 300 && y == 135)
					{					
						car.play();
						JOptionPane.showMessageDialog(null, "被汽車撞飛了，退至62");
						x = x + 75 * 5;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if( x > 50 && x < 100 && y == 285)
					{					
						water.play();
						JOptionPane.showMessageDialog(null, "踩到水面滑倒了，退至45");
						x = x + 75 * 4;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if( x > 150 && x < 225 && y == 435)
					{					
						god.play();
						JOptionPane.showMessageDialog(null, "獲得燈神的幫助，可再擲一次骰");
						order = 0;
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					break;
				}
			}
			else if( x >= 2 && y == 0)
			{
				if( x == 2)
				{
					wall = 1;
				}
				if(wall == 1)
				{
					x++;
					label2.setIcon(labelicon16c);
				}
				else
				{
					x--;
					label2.setIcon(labelicon16b);
				}
				distance1++;
				switch(distance1)
				{
					case 450:
					distance1 = 0;
					timera6.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if( x > 200 && x < 300 && y == 0)
					{					
						dragon.play();
						JOptionPane.showMessageDialog(null, "被兇猛的野獸嚇跑了，退至82");
						x = 615;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					wall = 0;
					if( x < 50)
					{
						if(win == 0)
						{
							win++;
							finish.play();
							JOptionPane.showMessageDialog(null, "第一名「玩家一」\n用了" + score1 + "步到達終點");
							String string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
							for(int i = 0 ; i < 2 ; i--)
							{
								if(string.length() > 4)
								{
									JOptionPane.showMessageDialog(null, "輸入錯誤，請重新輸入");
									string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
								}
								else
								{
									break;
								}
							}
							int Year = myDate.getYear() + 1900;
							int Month = myDate.getMonth() + 1;
							int Date = myDate.getDate();
							string = score1 + "\t" + string + "\t\t" + Year + "/" + Month + "/" + Date;
							try 
							{
								outputstring.write(string);
								outputstring2.write(number1 + "");
								outputstring.close();
								outputstring2.close();
							} 
							catch (IOException e1)
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							label2.setVisible(false);
							name1.setVisible(false);
							order = 3;
						}
						else if(win == 1)
						{
							win = 0;
							finish.play();
							JOptionPane.showMessageDialog(null, "第二名「玩家一」\n用了" + score1 + "步到達終點");
							String string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
							for(int i = 0 ; i < 2 ; i--)
							{
								if(string.length() > 4)
								{
									JOptionPane.showMessageDialog(null, "輸入錯誤，請重新輸入");
									string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
								}
								else
								{
									break;
								}
							}
							int Year = myDate.getYear() + 1900;
							int Month = myDate.getMonth() + 1;
							int Date = myDate.getDate();
							string = score1 + "\t" + string + "\t\t" + Year + "/" + Month + "/" + Date;
							try 
							{
								outputstring1.write(string);
								outputstring3.write(number1 + "");
								outputstring3.close();
								outputstring1.close();
							}
							catch (IOException e1) 
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							start.setVisible(false);
							label2.setVisible(false);
							name1.setVisible(false);
							JOptionPane.showMessageDialog(null, ("遊戲結束"));
							System.exit(0);
						}
					}
					break;
				}
			}
			else if ( x < 680 &&(y == 525 || y == 375 || y == 225 || y == 75))
			{
				x++;
				label2.setIcon(labelicon16c);
				distance1++;
				switch(distance1)
				{
					case 450:
					distance1 = 0;
					timera6.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if( x > 350 && x < 450 && y == 75)
					{					
						wind.play();
						JOptionPane.showMessageDialog(null, "愛到龍捲風的襲擊，退至65");
						x = 394;
						y = 135;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if( x > 513 && x < 573 && y == 225)
					{
						fire.play();
						JOptionPane.showMessageDialog(null, "乘坐火箭直達78");
						x = x -6;
						y = 75;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if( x > 350 && x < 400 && y == 375)
					{		
						down.play();
						JOptionPane.showMessageDialog(null, "跌下黑洞，返回起點");
						x = 750;
						y = 585;
						label2.setBounds(x, y, 75, 75);
						name1.setBounds(x + 35, y - 35, 75, 75);
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if(x < 375 && x > 300 && y == 525)
					{				
						stop.play();
						JOptionPane.showMessageDialog(null, "遇到路障，暫停一次");
						order = 2;
					}
					break;
				}
			}
			else if( x == 680 &&  y > 0)
			{
				y--;
				label2.setIcon(labelicon16d);
				distance1++;
				switch(distance1)
				{
					case 450:
					distance1 = 0;
					timera6.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					break;
				}
			}
			else if( x == 2 && y > 0)
			{
				y--;
				label2.setIcon(labelicon16d);
				distance1++;
				switch(distance1)
				{
					case 450:
					distance1 = 0;
					timera6.stop();
					walk1.stop();
					label2.setIcon(labelicon16);
					start.setVisible(true);
					title.setForeground(Color.orange);
					title.setText("玩家二");
					if(order == 4)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					if(order == 0)
					{
						title.setForeground(Color.red);
						title.setText("玩家一");
					}
					break;
				}
			}
	    }
	}
	class TimerListenerb1 implements ActionListener 
	{
		public void actionPerformed(ActionEvent e) 
	    {
			label3.setBounds(x1, y1, 75, 75);
			name2.setBounds(x1 + 35, y1 - 35, 75, 75);
			if ( x1 > 2 && (y1 == 585 || y1 == 435 || y1 == 285 || y1 == 135 ))
			{
				x1--;
				label3.setIcon(labelicon17b);
				distance2++;
				switch(distance2)
				{
					case 75:
					distance2 = 0;
					timerb1.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if( x1 > 425 && x1 < 500 && y1 == 585)
					{				
						drink.play();
						JOptionPane.showMessageDialog(null, "喝下RED BULL充滿能量，進至9");
						x1 = x1 - 75 * 5;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if( x1 > 225 && x1 < 300 && y1 == 135)
					{			
						car.play();
						JOptionPane.showMessageDialog(null, "被汽車撞飛了，退至62");
						x1 = x1 + 75 * 5;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if( x1 > 50 && x1 < 100 && y1 == 285)
					{				
						water.play();
						JOptionPane.showMessageDialog(null, "踩到水面滑倒了，退至45");
						x1 = x1 + 75 * 4;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if( x1 > 150 && x1 < 225 && y1 == 435)
					{	
						god.play();
						JOptionPane.showMessageDialog(null, "獲得燈神的幫助，可再擲一次骰");
						order = 1;
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					break;
				}
			}
			else if( x1 >= 2 && y1 == 0)
			{
				if( x1 == 2)
				{
					wall2 = 1;
				}
				if(wall2 == 1)
				{
					x1++;
					label3.setIcon(labelicon17c);
				}
				else if(wall2 == 0)
				{
					x1--;
					label3.setIcon(labelicon17b);
				}
				distance2++;
				switch(distance2)
				{
					case 75:
					distance2 = 0;
					timerb1.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if( x1 > 200 && x1 < 300 && y1 == 0)
					{
						dragon.play();
						JOptionPane.showMessageDialog(null, "被兇猛的野獸嚇跑了，退至82");
						x1 = 615;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					wall2 = 0;
					if( x1 < 50)
					{
						if(win == 0)
						{
							win++;
							finish.play();
							JOptionPane.showMessageDialog(null, "第一名「玩家二」\n用了" + score2 + "步到達終點");
							String string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
							for(int i = 0 ; i < 2 ; i--)
							{
								if(string.length() > 4)
								{
									JOptionPane.showMessageDialog(null, "輸入錯誤，請重新輸入");
									string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
								}
								else
								{
									break;
								}
							}
							int Year = myDate.getYear() + 1900;
							int Month = myDate.getMonth() + 1;
							int Date = myDate.getDate();
							string = score2 + "\t" + string + "\t\t" + Year + "/" + Month + "/" + Date;
							try 
							{
								outputstring.write(string);
								outputstring2.write(number2 + "");
								outputstring2.close();
								outputstring.close();
							} 
							catch (IOException e1) 
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							label3.setVisible(false);
							name2.setVisible(false);
							order = 4;
						}
						else if(win == 1)
						{
							win = 0;
							finish.play();
							JOptionPane.showMessageDialog(null, "第二名「玩家二」\n用了" + score2 + "步到達終點");
							String string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
							for(int i = 0 ; i < 2 ; i--)
							{
								if(string.length() > 4)
								{
									JOptionPane.showMessageDialog(null, "輸入錯誤，請重新輸入");
									string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
								}
								else
								{
									break;
								}
							}
							int Year = myDate.getYear() + 1900;
							int Month = myDate.getMonth() + 1;
							int Date = myDate.getDate();
							string = score2 + "\t" + string + "\t\t" + Year + "/" + Month + "/" + Date;
							try 
							{
								outputstring1.newLine();
								outputstring1.write(string);
								outputstring3.newLine();
								outputstring3.write(number2 + "");
								outputstring3.close();
								outputstring1.close();
							} 
							catch (IOException e1)
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							start.setVisible(false);
							label3.setVisible(false);
							name2.setVisible(false);
							JOptionPane.showMessageDialog(null, "遊戲結束");
							System.exit(0);
						}
					}
					break;
				}
			}
			else if ( x1 < 680 &&( y1 == 525 || y1 == 375 || y1 == 225 || y1 == 75))
			{
				x1++;
				label3.setIcon(labelicon17c);
				distance2++;
				switch(distance2)
				{
					case 75:
					distance2 = 0;
					timerb1.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if( x1 > 350 && x1 < 450 && y1 == 75)
					{						
						wind.play();
						JOptionPane.showMessageDialog(null, "愛到龍捲風的襲擊，退至65");
						x1 = 394;
						y1 = 135;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if( x1 > 513 && x1 < 573 && y1 == 225)
					{						
						fire.play();
						JOptionPane.showMessageDialog(null, "乘坐火箭直達78");
						x1 = x1 -6;
						y1 = 75;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if( x1 > 350 && x1 < 400 && y1 == 375)
					{						
						down.play();
						JOptionPane.showMessageDialog(null, "跌下黑洞，返回起點");
						x1 = 750;
						y1 = 585;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if(x1 < 375 && x1 > 300 && y1 == 525)
					{						
						stop.play();
						JOptionPane.showMessageDialog(null, "遇到路障，暫停一次");
						order = -1;
					}
					break;
				}
			}
			else if( x1 == 680 &&  y1 > 0)
			{
				y1--;
				label3.setIcon(labelicon17d);
				distance2++;
				switch(distance2)
				{
					case 75:
					distance2 = 0;
					timerb1.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					break;
				}
			}
			else if( x1 == 2 && y1 > 0)
			{
				y1--;
				label3.setIcon(labelicon17d);
				distance2++;
				switch(distance2)
				{
					case 75:
					distance2 = 0;
					timerb1.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					break;
				}
			}
	    }
	}
	class TimerListenerb2 implements ActionListener 
	{
		public void actionPerformed(ActionEvent e) 
	    {
			label3.setBounds(x1, y1, 75, 75);
			name2.setBounds(x1 + 35, y1 - 35, 75, 75);
			if ( x1 > 2 && (y1 == 585 || y1 == 435 || y1 == 285 || y1 == 135 ))
			{
				x1--;
				label3.setIcon(labelicon17b);
				distance2++;
				switch(distance2)
				{
					case 150:
					distance2 = 0;
					timerb2.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if( x1 > 425 && x1 < 475 && y1 == 585)
					{						
						drink.play();
						JOptionPane.showMessageDialog(null, "喝下RED BULL充滿能量，進至9");
						x1 = x1 - 75 * 5;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if( x1 > 225 && x1 < 300 && y1 == 135)
					{				
						car.play();
						JOptionPane.showMessageDialog(null, "被汽車撞飛了，退至62");
						x1 = x1 + 75 * 5;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if( x1 > 50 && x1 < 100 && y1 == 285)
					{					
						water.play();
						JOptionPane.showMessageDialog(null, "踩到水面滑倒了，退至45");
						x1 = x1 + 75 * 4;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if( x1 > 150 && x1 < 225 && y1 == 435)
					{			
						god.play();
						JOptionPane.showMessageDialog(null, "獲得燈神的幫助，可再擲一次骰");
						order = 1;
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					break;
				}
			}
			else if( x1 >= 2 && y1 == 0)
			{
				if( x1 == 2)
				{
					wall2 = 1;
				}
				if(wall2 == 1)
				{
					x1++;
					label3.setIcon(labelicon17c);
				}
				else if(wall2 == 0)
				{
					x1--;
					label3.setIcon(labelicon17b);
				}
				distance2++;
				switch(distance2)
				{
					case 150:
					distance2 = 0;
					timerb2.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if( x1 > 200 && x1 < 300 && y1 == 0)
					{					
						dragon.play();
						JOptionPane.showMessageDialog(null, "被兇猛的野獸嚇跑了，退至82");
						x1 = 615;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					wall2 = 0;
					if( x1 < 50 )
					{
						if(win == 0)
						{
							win++;
							finish.play();
							JOptionPane.showMessageDialog(null, "第一名「玩家二」\n用了" + score2 + "步到達終點");
							String string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
							for(int i = 0 ; i < 2 ; i--)
							{
								if(string.length() > 4)
								{
									JOptionPane.showMessageDialog(null, "輸入錯誤，請重新輸入");
									string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
								}
								else
								{
									break;
								}
							}
							int Year = myDate.getYear() + 1900;
							int Month = myDate.getMonth() + 1;
							int Date = myDate.getDate();
							string = score2 + "\t" + string + "\t\t" + Year + "/" + Month + "/" + Date;
							try 
							{
								outputstring.write(string);
								outputstring2.write(number2 + "");
								outputstring2.close();
								outputstring.close();
							} 
							catch (IOException e1) 
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							label3.setVisible(false);
							name2.setVisible(false);
							order = 4;
						}
						else if(win == 1)
						{
							win = 0;
							finish.play();
							JOptionPane.showMessageDialog(null, "第二名「玩家二」\n用了" + score2 + "步到達終點");
							String string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
							for(int i = 0 ; i < 2 ; i--)
							{
								if(string.length() > 4)
								{
									JOptionPane.showMessageDialog(null, "輸入錯誤，請重新輸入");
									string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
								}
								else
								{
									break;
								}
							}
							int Year = myDate.getYear() + 1900;
							int Month = myDate.getMonth() + 1;
							int Date = myDate.getDate();
							string = score2 + "\t" + string + "\t\t" + Year + "/" + Month + "/" + Date;
							try 
							{
								outputstring1.newLine();
								outputstring1.write(string);
								outputstring3.newLine();
								outputstring3.write(number2 + "");
								outputstring3.close();
								outputstring1.close();
							} 
							catch (IOException e1)
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							start.setVisible(false);
							label3.setVisible(false);
							name2.setVisible(false);
							JOptionPane.showMessageDialog(null, "遊戲結束");
							System.exit(0);
						}
					}
					break;
				}
			}
			else if ( x1 < 680 &&( y1 == 525 || y1 == 375 || y1 == 225 || y1 == 75))
			{
				x1++;
				label3.setIcon(labelicon17c);
				distance2++;
				switch(distance2)
				{
					case 150:
					distance2 = 0;
					timerb2.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if( x1 > 350 && x1 < 450 && y1 == 75)
					{					
						wind.play();
						JOptionPane.showMessageDialog(null, "愛到龍捲風的襲擊，退至65");
						x1 = 394;
						y1 = 135;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if( x1 > 513 && x1 < 573 && y1 == 225)
					{				
						fire.play();
						JOptionPane.showMessageDialog(null, "乘坐火箭直達78");
						x1 = x1 -6;
						y1 = 75;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if( x1 > 350 && x1 < 400 && y1 == 375)
					{					
						down.play();
						JOptionPane.showMessageDialog(null, "跌下黑洞，返回起點");
						x1 = 750;
						y1 = 585;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if(x1 < 375 && x1 > 300 && y1 == 525)
					{				
						stop.play();
						JOptionPane.showMessageDialog(null, "遇到路障，暫停一次");
						order = -1;
					}
					break;
				}
			}
			else if( x1 == 680 &&  y1 > 0)
			{
				y1--;
				label3.setIcon(labelicon17d);
				distance2++;
				switch(distance2)
				{
					case 150:
					distance2 = 0;
					timerb2.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					break;
				}
			}
			else if( x1 == 2 && y1 > 0)
			{
				y1--;
				label3.setIcon(labelicon17d);
				distance2++;
				switch(distance2)
				{
					case 150:
					distance2 = 0;
					timerb2.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					break;
				}
			}
	    }
	}
	class TimerListenerb3 implements ActionListener 
	{
		public void actionPerformed(ActionEvent e) 
	    {
			label3.setBounds(x1, y1, 75, 75);
			name2.setBounds(x1 + 35, y1 - 35, 75, 75);
			if ( x1 > 2 && (y1 == 585 || y1 == 435 || y1 == 285 || y1 == 135 ))
			{
				x1--;
				label3.setIcon(labelicon17b);
				distance2++;
				switch(distance2)
				{
					case 225:
					distance2 = 0;
					timerb3.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if( x1 > 425 && x1 < 475 && y1 == 585)
					{					
						drink.play();
						JOptionPane.showMessageDialog(null, "喝下RED BULL充滿能量，進至9");
						x1 = x1 - 75 * 5;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if( x1 > 225 && x1 < 300 && y1 == 135)
					{		
						car.play();
						JOptionPane.showMessageDialog(null, "被汽車撞飛了，退至62");
						x1 = x1 + 75 * 5;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if( x1 > 50 && x1 < 100 && y1 == 285)
					{					
						water.play();
						JOptionPane.showMessageDialog(null, "踩到水面滑倒了，退至45");
						x1 = x1 + 75 * 4;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if( x1 > 150 && x1 < 225 && y1 == 435)
					{			
						god.play();
						JOptionPane.showMessageDialog(null, "獲得燈神的幫助，可再擲一次骰");
						order = 1;
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					break;
				}
			}
			else if( x1 >= 2 && y1 == 0)
			{
				if( x1 == 2)
				{
					wall2 = 1;
				}
				if(wall2 == 1)
				{
					x1++;
					label3.setIcon(labelicon17c);
				}
				else if(wall2 == 0)
				{
					x1--;
					label3.setIcon(labelicon17b);
				}
				distance2++;
				switch(distance2)
				{
					case 225:
					distance2 = 0;
					timerb3.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if( x1 > 200 && x1 < 300 && y1 == 0)
					{
						dragon.play();
						JOptionPane.showMessageDialog(null, "被兇猛的野獸嚇跑了，退至82");
						x1 = 615;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					wall2 = 0;
					if( x1 < 50)
					{
						if(win == 0)
						{
							win++;
							finish.play();
							JOptionPane.showMessageDialog(null, "第一名「玩家二」\n用了" + score2 + "步到達終點");
							String string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
							for(int i = 0 ; i < 2 ; i--)
							{
								if(string.length() > 4)
								{
									JOptionPane.showMessageDialog(null, "輸入錯誤，請重新輸入");
									string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
								}
								else
								{
									break;
								}
							}
							int Year = myDate.getYear() + 1900;
							int Month = myDate.getMonth() + 1;
							int Date = myDate.getDate();
							string = score2 + "\t" + string + "\t\t" + Year + "/" + Month + "/" + Date;
							try 
							{
								outputstring.write(string);
								outputstring2.write(number2 + "");
								outputstring2.close();
								outputstring.close();
							} 
							catch (IOException e1) 
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							label3.setVisible(false);
							name2.setVisible(false);
							order = 4;
						}
						else if(win == 1)
						{
							win = 0;
							finish.play();
							JOptionPane.showMessageDialog(null, "第二名「玩家二」\n用了" + score2 + "步到達終點");
							String string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
							for(int i = 0 ; i < 2 ; i--)
							{
								if(string.length() > 4)
								{
									JOptionPane.showMessageDialog(null, "輸入錯誤，請重新輸入");
									string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
								}
								else
								{
									break;
								}
							}
							int Year = myDate.getYear() + 1900;
							int Month = myDate.getMonth() + 1;
							int Date = myDate.getDate();
							string = score2 + "\t" + string + "\t\t" + Year + "/" + Month + "/" + Date;
							try 
							{
								outputstring1.newLine();
								outputstring1.write(string);
								outputstring3.newLine();
								outputstring3.write(number2 + "");
								outputstring3.close();
								outputstring1.close();
							} 
							catch (IOException e1)
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							start.setVisible(false);
							label3.setVisible(false);
							name2.setVisible(false);
							JOptionPane.showMessageDialog(null, "遊戲結束");
							System.exit(0);
						}
					}
					break;
				}
			}
			else if ( x1 < 680 &&( y1 == 525 || y1 == 375 || y1 == 225 || y1 == 75))
			{
				x1++;
				label3.setIcon(labelicon17c);
				distance2++;
				switch(distance2)
				{
					case 225:
					distance2 = 0;
					timerb3.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if( x1 > 350 && x1 < 450 && y1 == 75)
					{
						wind.play();
						JOptionPane.showMessageDialog(null, "愛到龍捲風的襲擊，退至65");
						x1 = 394;
						y1 = 135;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if( x1 > 513 && x1 < 573 && y1 == 225)
					{
						fire.play();
						JOptionPane.showMessageDialog(null, "乘坐火箭直達78");
						x1 = x1 -6;
						y1 = 75;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if( x1 > 350 && x1 < 400 && y1 == 375)
					{
						down.play();
						JOptionPane.showMessageDialog(null, "跌下黑洞，返回起點");
						x1 = 750;
						y1 = 585;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if(x1 < 375 && x1 > 300 && y1 == 525)
					{
						stop.play();
						JOptionPane.showMessageDialog(null, "遇到路障，暫停一次");
						order = -1;
					}
					break;
				}
			}
			else if( x1 == 680 &&  y1 > 0)
			{
				y1--;
				label3.setIcon(labelicon17d);
				distance2++;
				switch(distance2)
				{
					case 225:
					distance2 = 0;
					timerb3.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					break;
				}
			}
			else if( x1 == 2 && y1 > 0)
			{
				y1--;
				label3.setIcon(labelicon17d);
				distance2++;
				switch(distance2)
				{
					case 225:
					distance2 = 0;
					timerb3.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					break;
				}
			}
	    }
	}
	class TimerListenerb4 implements ActionListener 
	{
		public void actionPerformed(ActionEvent e) 
	    {
			label3.setBounds(x1, y1, 75, 75);
			name2.setBounds(x1 + 35, y1 - 35, 75, 75);
			if ( x1 > 2 && (y1 == 585 || y1 == 435 || y1 == 285 || y1 == 135))
			{
				x1--;
				label3.setIcon(labelicon17b);
				distance2++;
				switch(distance2)
				{
					case 300:
					distance2 = 0;
					timerb4.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if( x1 > 425 && x1 < 475 && y1 == 585)
					{
						drink.play();
						JOptionPane.showMessageDialog(null, "喝下RED BULL充滿能量，進至9");
						x1 = x1 - 75 * 5;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if( x1 > 225 && x1 < 300 && y1 == 135)
					{					
						car.play();
						JOptionPane.showMessageDialog(null, "被汽車撞飛了，退至62");
						x1 = x1 + 75 * 5;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if( x1 > 50 && x1 < 100 && y1 == 285)
					{
						water.play();
						JOptionPane.showMessageDialog(null, "踩到水面滑倒了，退至45");
						x1 = x1 + 75 * 4;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if( x1 > 150 && x1 < 225 && y1 == 435)
					{	
						god.play();
						JOptionPane.showMessageDialog(null, "獲得燈神的幫助，可再擲一次骰");
						order = 1;
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					break;
				}
			}
			else if( x1 >= 2 && y1 == 0)
			{
				if( x1 == 2)
				{
					wall2 = 1;
				}
				if(wall2 == 1)
				{
					x1++;
					label3.setIcon(labelicon17c);
				}
				else if(wall2 == 0)
				{
					x1--;
					label3.setIcon(labelicon17b);
				}
				distance2++;
				switch(distance2)
				{
					case 300:
					distance2 = 0;
					timerb4.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if( x1 > 200 && x1 < 300 && y1 == 0)
					{						
						dragon.play();
						JOptionPane.showMessageDialog(null, "被兇猛的野獸嚇跑了，退至82");
						x1 = 615;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					wall2 = 0;
					if( x1 < 50 )
					{
						if(win == 0)
						{
							win++;
							finish.play();
							JOptionPane.showMessageDialog(null, "第一名「玩家二」\n用了" + score2 + "步到達終點");
							String string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
							for(int i = 0 ; i < 2 ; i--)
							{
								if(string.length() > 4)
								{
									JOptionPane.showMessageDialog(null, "輸入錯誤，請重新輸入");
									string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
								}
								else
								{
									break;
								}
							}
							int Year = myDate.getYear() + 1900;
							int Month = myDate.getMonth() + 1;
							int Date = myDate.getDate();
							string = score2 + "\t" + string + "\t\t" + Year + "/" + Month + "/" + Date;
							try 
							{
								outputstring.write(string);
								outputstring2.write(number2 + "");
								outputstring2.close();
								outputstring.close();
							} 
							catch (IOException e1) 
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							label3.setVisible(false);
							name2.setVisible(false);
							order = 4;
						}
						else if(win == 1)
						{
							win = 0;
							finish.play();
							JOptionPane.showMessageDialog(null, "第二名「玩家二」\n用了" + score2 + "步到達終點");
							String string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
							for(int i = 0 ; i < 2 ; i--)
							{
								if(string.length() > 4)
								{
									JOptionPane.showMessageDialog(null, "輸入錯誤，請重新輸入");
									string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
								}
								else
								{
									break;
								}
							}
							int Year = myDate.getYear() + 1900;
							int Month = myDate.getMonth() + 1;
							int Date = myDate.getDate();
							string = score2 + "\t" + string + "\t\t" + Year + "/" + Month + "/" + Date;
							try 
							{
								outputstring1.newLine();
								outputstring1.write(string);
								outputstring3.newLine();
								outputstring3.write(number2 + "");
								outputstring3.close();
								outputstring1.close();
							} 
							catch (IOException e1)
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							start.setVisible(false);
							label3.setVisible(false);
							name2.setVisible(false);
							JOptionPane.showMessageDialog(null, "遊戲結束");
							System.exit(0);
						}
					}
					break;
				}
			}
			else if ( x1 < 680 &&( y1 == 525 || y1 == 375 || y1 == 225 || y1 == 75))
			{
				x1++;
				label3.setIcon(labelicon17c);
				distance2++;
				switch(distance2)
				{
					case 300:
					distance2 = 0;
					timerb4.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if( x1 > 350 && x1 < 450 && y1 == 75)
					{	
						wind.play();
						JOptionPane.showMessageDialog(null, "愛到龍捲風的襲擊，退至65");
						x1 = 394;
						y1 = 135;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if( x1 > 513 && x1 < 573 && y1 == 225)
					{					
						fire.play();
						JOptionPane.showMessageDialog(null, "乘坐火箭直達78");
						x1 = x1 -6;
						y1 = 75;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if( x1 > 350 && x1 < 400 && y1 == 375)
					{					
						down.play();
						JOptionPane.showMessageDialog(null, "跌下黑洞，返回起點");
						x1 = 750;
						y1 = 585;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if(x1 < 375 && x1 > 300 && y1 == 525)
					{		
						stop.play();
						JOptionPane.showMessageDialog(null, "遇到路障，暫停一次");
						order = -1;
					}
					break;
				}
			}
			else if( x1 == 680 &&  y1 > 0)
			{
				y1--;
				label3.setIcon(labelicon17d);
				distance2++;
				switch(distance2)
				{
					case 300:
					distance2 = 0;
					timerb4.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					break;
				}
			}
			else if( x1 == 2 && y1 > 0)
			{
				y1--;
				label3.setIcon(labelicon17d);
				distance2++;
				switch(distance2)
				{
					case 300:
					distance2 = 0;
					timerb4.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					break;
				}
			}
	    }
	}
	class TimerListenerb5 implements ActionListener 
	{
		public void actionPerformed(ActionEvent e) 
	    {
			label3.setBounds(x1, y1, 75, 75);
			name2.setBounds(x1 + 35, y1 - 35, 75, 75);
			if ( x1 > 2 && (y1 == 585 || y1 == 435 || y1 == 285 || y1 == 135 ))
			{
				x1--;
				label3.setIcon(labelicon17b);
				distance2++;
				switch(distance2)
				{
					case 375:
					distance2 = 0;
					timerb5.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if( x1 > 425 && x1 < 475 && y1 == 585)
					{
						drink.play();
						JOptionPane.showMessageDialog(null, "喝下RED BULL充滿能量，進至9");
						x1 = x1 - 75 * 5;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if( x1 > 225 && x1 < 300 && y1 == 135)
					{		
						car.play();
						JOptionPane.showMessageDialog(null, "被汽車撞飛了，退至62");
						x1 = x1 + 75 * 5;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if( x1 > 50 && x1 < 100 && y1 == 285)
					{				
						water.play();
						JOptionPane.showMessageDialog(null, "踩到水面滑倒了，退至45");
						x1 = x1 + 75 * 4;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if( x1 > 150 && x1 < 225 && y1 == 435)
					{					
						god.play();
						JOptionPane.showMessageDialog(null, "獲得燈神的幫助，可再擲一次骰");
						order = 1;
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					break;
				}
			}
			else if( x1 >= 2 && y1 == 0)
			{
				if( x1 == 2)
				{
					wall2 = 1;
				}
				if(wall2 == 1)
				{
					x1++;
					label3.setIcon(labelicon17c);
				}
				else if(wall2 == 0)
				{
					x1--;
					label3.setIcon(labelicon17b);
				}
				distance2++;
				switch(distance2)
				{
					case 375:
					distance2 = 0;
					timerb5.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if( x1 > 200 && x1 < 300 && y1 == 0)
					{
						dragon.play();
						JOptionPane.showMessageDialog(null, "被兇猛的野獸嚇跑了，退至82");
						x1 = 615;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					wall2 = 0;
					if( x1 < 50 )
					{
						if(win == 0)
						{
							win++;
							finish.play();
							JOptionPane.showMessageDialog(null, "第一名「玩家二」\n用了" + score2 + "步到達終點");
							String string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
							for(int i = 0 ; i < 2 ; i--)
							{
								if(string.length() > 4)
								{
									JOptionPane.showMessageDialog(null, "輸入錯誤，請重新輸入");
									string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
								}
								else
								{
									break;
								}
							}
							int Year = myDate.getYear() + 1900;
							int Month = myDate.getMonth() + 1;
							int Date = myDate.getDate();
							string = score2 + "\t" + string + "\t\t" + Year + "/" + Month + "/" + Date;
							try 
							{
								outputstring.write(string);
								outputstring2.write(number2 + "");
								outputstring2.close();
								outputstring.close();
							} 
							catch (IOException e1) 
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							label3.setVisible(false);
							name2.setVisible(false);
							order = 4;
						}
						else if(win == 1)
						{
							win = 0;
							finish.play();
							JOptionPane.showMessageDialog(null, "第二名「玩家二」\n用了" + score2 + "步到達終點");
							String string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
							for(int i = 0 ; i < 2 ; i--)
							{
								if(string.length() > 4)
								{
									JOptionPane.showMessageDialog(null, "輸入錯誤，請重新輸入");
									string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
								}
								else
								{
									break;
								}
							}
							int Year = myDate.getYear() + 1900;
							int Month = myDate.getMonth() + 1;
							int Date = myDate.getDate();
							string = score2 + "\t" + string + "\t\t" + Year + "/" + Month + "/" + Date;
							try 
							{
								outputstring1.newLine();
								outputstring1.write(string);
								outputstring3.newLine();
								outputstring3.write(number2 + "");
								outputstring3.close();
								outputstring1.close();
							} 
							catch (IOException e1)
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							start.setVisible(false);
							label3.setVisible(false);
							name2.setVisible(false);
							JOptionPane.showMessageDialog(null, "遊戲結束");
							System.exit(0);
						}
					}
					break;
				}
			}
			else if ( x1 < 680 &&( y1 == 525 || y1 == 375 || y1 == 225 || y1 == 75))
			{
				x1++;
				label3.setIcon(labelicon17c);
				distance2++;
				switch(distance2)
				{
					case 375:
					distance2 = 0;
					timerb5.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if( x1 > 350 && x1 < 450 && y1 == 75)
					{			
						wind.play();
						JOptionPane.showMessageDialog(null, "愛到龍捲風的襲擊，退至65");
						x1 = 394;
						y1 = 135;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if( x1 > 513 && x1 < 573 && y1 == 225)
					{						
						fire.play();
						JOptionPane.showMessageDialog(null, "乘坐火箭直達78");
						x1 = x1 -6;
						y1 = 75;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if( x1 > 350 && x1 < 400 && y1 == 375)
					{						
						down.play();
						JOptionPane.showMessageDialog(null, "跌下黑洞，返回起點");
						x1 = 750;
						y1 = 585;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if(x1 < 375 && x1 > 300 && y1 == 525)
					{					
						stop.play();
						JOptionPane.showMessageDialog(null, "遇到路障，暫停一次");
						order = -1;
					}
					break;
				}
			}
			else if( x1 == 680 &&  y1 > 0)
			{
				y1--;
				label3.setIcon(labelicon17d);
				distance2++;
				switch(distance2)
				{
					case 375:
					distance2 = 0;
					timerb5.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					break;
				}
			}
			else if( x1 == 2 && y1 > 0)
			{
				y1--;
				label3.setIcon(labelicon17d);
				distance2++;
				switch(distance2)
				{
					case 375:
					distance2 = 0;
					timerb5.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					break;
				}
			}
	    }
	}
	class TimerListenerb6 implements ActionListener 
	{
		public void actionPerformed(ActionEvent e) 
	    {
			label3.setBounds(x1, y1, 75, 75);
			name2.setBounds(x1 + 35, y1 - 35, 75, 75);
			if ( x1 > 2 && (y1 == 585 || y1 == 435 || y1 == 285 || y1 == 135 ))
			{
				x1--;
				label3.setIcon(labelicon17b);
				distance2++;
				switch(distance2)
				{
					case 450:
					distance2 = 0;
					timerb6.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if( x1 > 425 && x1 < 475 && y1 == 585)
					{						
						drink.play();
						JOptionPane.showMessageDialog(null, "喝下RED BULL充滿能量，進至9");
						x1 = x1 - 75 * 5;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if( x1 > 225 && x1 < 300 && y1 == 135)
					{				
						car.play();
						JOptionPane.showMessageDialog(null, "被汽車撞飛了，退至62");
						x1 = x1 + 75 * 5;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if( x1 > 50 && x1 < 100 && y1 == 285)
					{		
						water.play();
						JOptionPane.showMessageDialog(null, "踩到水面滑倒了，退至45");
						x1 = x1 + 75 * 4;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if( x1 > 150 && x1 < 225 && y1 == 435)
					{
						god.play();
						JOptionPane.showMessageDialog(null, "獲得燈神的幫助，可再擲一次骰");
						order = 1;
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					break;
				}
			}
			else if( x1 >= 2 && y1 == 0)
			{
				if( x1 == 2)
				{
					wall2 = 1;
				}
				if(wall2 == 1)
				{
					x1++;
					label3.setIcon(labelicon17c);
				}
				else if(wall2 == 0)
				{
					x1--;
					label3.setIcon(labelicon17b);
				}
				distance2++;
				switch(distance2)
				{
					case 450:
					distance2 = 0;
					timerb6.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if( x1 > 200 && x1 < 300 && y1 == 0)
					{
						dragon.play();
						JOptionPane.showMessageDialog(null, "被兇猛的野獸嚇跑了，退至82");
						x1 = 615;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					wall2 = 0;
					if( x1 < 50 )
					{
						if(win == 0)
						{
							win++;
							finish.play();
							JOptionPane.showMessageDialog(null, "第一名「玩家二」\n用了" + score2 + "步到達終點");
							String string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
							for(int i = 0 ; i < 2 ; i--)
							{
								if(string.length() > 4)
								{
									JOptionPane.showMessageDialog(null, "輸入錯誤，請重新輸入");
									string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
								}
								else
								{
									break;
								}
							}
							int Year = myDate.getYear() + 1900;
							int Month = myDate.getMonth() + 1;
							int Date = myDate.getDate();
							string = score2 + "\t" + string + "\t\t" + Year + "/" + Month + "/" + Date;
							try 
							{
								outputstring.write(string);
								outputstring2.write(number2 + "");
								outputstring2.close();
								outputstring.close();
							} 
							catch (IOException e1) 
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							label3.setVisible(false);
							name2.setVisible(false);
							order = 4;
						}
						else if(win == 1)
						{
							win = 0;
							finish.play();
							JOptionPane.showMessageDialog(null, "第二名「玩家二」\n用了" + score2 + "步到達終點");
							String string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
							for(int i = 0 ; i < 2 ; i--)
							{
								if(string.length() > 4)
								{
									JOptionPane.showMessageDialog(null, "輸入錯誤，請重新輸入");
									string = JOptionPane.showInputDialog("請輸入你的名字(不超過四位)");
								}
								else
								{
									break;
								}
							}
							int Year = myDate.getYear() + 1900;
							int Month = myDate.getMonth() + 1;
							int Date = myDate.getDate();
							string = score2 + "\t" + string + "\t\t" + Year + "/" + Month + "/" + Date;
							try 
							{
								outputstring1.newLine();
								outputstring1.write(string);
								outputstring3.newLine();
								outputstring3.write(number2 + "");
								outputstring3.close();
								outputstring1.close();
							} 
							catch (IOException e1)
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							start.setVisible(false);
							label3.setVisible(false);
							name2.setVisible(false);
							JOptionPane.showMessageDialog(null, "遊戲結束");
							System.exit(0);
						}
					}
					break;
				}
			}
			else if ( x1 < 680 &&( y1 == 525 || y1 == 375 || y1 == 225 || y1 == 75))
			{
				x1++;
				label3.setIcon(labelicon17c);
				distance2++;
				switch(distance2)
				{
					case 450:
					distance2 = 0;
					timerb6.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if( x1 > 350 && x1 < 450 && y1 == 75)
					{					
						wind.play();
						JOptionPane.showMessageDialog(null, "愛到龍捲風的襲擊，退至65");
						x1 = 394;
						y1 = 135;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if( x1 > 513 && x1 < 573 && y1 == 225)
					{						
						fire.play();
						JOptionPane.showMessageDialog(null, "乘坐火箭直達78");
						x1 = x1 -6;
						y1 = 75;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if( x1 > 350 && x1 < 400 && y1 == 375)
					{					
						down.play();
						JOptionPane.showMessageDialog(null, "跌下黑洞，返回起點");
						x1 = 750;
						y1 = 585;
						label3.setBounds(x1, y1, 75, 75);
						name2.setBounds(x1 + 35, y1 - 35, 75, 75);
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if(x1 < 375 && x1 > 300 && y1 == 525)
					{				
						stop.play();
						JOptionPane.showMessageDialog(null, "遇到路障，暫停一次");
						order = -1;
					}
					break;
				}
			}
			else if( x1 == 680 &&  y1 > 0)
			{
				y1--;
				label3.setIcon(labelicon17d);
				distance2++;
				switch(distance2)
				{
					case 450:
					distance2 = 0;
					timerb6.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					break;
				}
			}
			else if( x1 == 2 && y1 > 0)
			{
				y1--;
				label3.setIcon(labelicon17d);
				distance2++;
				switch(distance2)
				{
					case 450:
					distance2 = 0;
					timerb6.stop();
					walk2.stop();
					label3.setIcon(labelicon17);
					start.setVisible(true);
					title.setForeground(Color.red);
					title.setText("玩家一");
					if(order == 3)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					if(order == 1)
					{
						title.setForeground(Color.orange);
						title.setText("玩家二");
					}
					break;
				}
			}
	    }
	}//到這�堣~結束TimerListenerb6
	class TimerListener implements ActionListener 
	{
		public void actionPerformed(ActionEvent e) 
	    {
			int Integer = (int) (Math.random() * 6 + 1 );//決定骰子所擲的點數
			//Integer = 1;
			switch(Integer)//擲到的點數的動作
			{
				case 1:
					label1.setIcon(labelicon9);
					order ++;
					if(order == 0 || order == 1)
					{
						move1 = 1;
						timera1.start();
						walk1.start();
					}
					else if(order == 2)
					{
						move2 = 1;
						order = 0;
						timerb1.start();
						walk2.start();
					}
					else if(order == 3)
					{
						move2 = 1;
						order = 1;
						timerb1.start();
						walk2.start();
					}
					else if(order == 4)
					{
						move2 = 1;
						order = 3;
						timerb1.start();
						walk2.start();
					}
					else if(order == 5)
					{
						move1 = 1;
						order = 4;
						timera1.start();
						walk1.start();
					}
					timer.stop();
					break;
				case 2:
					label1.setIcon(labelicon10);
					order ++;
					if(order == 0 || order == 1)
					{
						move1 = 2;
						timera2.start();
						walk1.start();
					}
					else if(order == 2)
					{
						move2 = 2;
						order = 0;
						timerb2.start();
						walk2.start();
					}
					else if(order == 3)
					{
						move2 = 2;
						order = 1;
						timerb2.start();
						walk2.start();
					}
					else if(order == 4)
					{
						move2 = 2;
						order = 3;
						timerb2.start();
						walk2.start();
					}
					else if(order == 5)
					{
						move1 = 2;
						order = 4;
						timera2.start();
						walk1.start();
					}
					timer.stop();
					break;
				case 3:
					
					label1.setIcon(labelicon11);
					order ++;
					if(order == 0 || order == 1)
					{
						move1 = 3;
						timera3.start();
						walk1.start();
					}
					else if(order == 2)
					{
						move2 = 3;
						order = 0;
						timerb3.start();
						walk2.start();
					}
					else if(order == 3)
					{
						move2 = 3;
						order = 1;
						timerb3.start();
						walk2.start();
					}
					else if(order == 4)
					{
						move2 = 3;
						order = 3;
						timerb3.start();
						walk2.start();
					}
					else if(order == 5)
					{
						move1 = 3;
						order = 4;
						timera3.start();
						walk1.start();
					}
					timer.stop();
					break;
				case 4:
					label1.setIcon(labelicon12);
					order ++;
					if(order == 0 || order == 1)
					{
						move1 = 4;
						timera4.start();
						walk1.start();
					}
					else if(order == 2)
					{
						move2 = 4;
						order = 0;
						timerb4.start();
						walk2.start();
					}
					else if(order == 3)
					{
						move2 = 4;
						order = 1;
						timerb4.start();
						walk2.start();
					}
					else if(order == 4)
					{
						move2 = 4;
						order = 3;
						timerb4.start();
						walk2.start();
					}
					else if(order == 5)
					{
						move1 = 4;
						order = 4;
						timera4.start();
						walk1.start();
					}
					timer.stop();
					break;
				case 5:
					label1.setIcon(labelicon13);
					order ++;
					if(order == 0 || order == 1)
					{
						move1 = 5;
						timera5.start();
						walk1.start();
					}
					else if(order == 2)
					{
						move2 = 5;
						order = 0;
						timerb5.start();
						walk2.start();
					}
					else if(order == 3)
					{
						move2 = 5;
						order = 1;
						timerb5.start();
						walk2.start();
					}
					else if(order == 4)
					{
						move2 = 5;
						order = 3;
						timerb5.start();
						walk2.start();
					}
					else if(order == 5)
					{
						move1 = 5;
						order = 4;
						timera5.start();
						walk1.start();
					}
					timer.stop();
					break;
				case 6:
					label1.setIcon(labelicon14);
					order ++;
					if(order == 0 || order == 1)
					{
						move1 = 6;
						timera6.start();
						walk1.start();
					}
					else if(order == 2)
					{
						move2 = 6;
						order = 0;
						timerb6.start();
						walk2.start();
					}
					else if(order == 3)
					{
						move2 = 6;
						order = 1;
						timerb6.start();
						walk2.start();
					}
					else if(order == 4)
					{
						move2 = 6;
						order = 3;
						timerb6.start();
						walk2.start();
					}
					else if(order == 5)
					{
						move1 = 6;
						order = 4;
						timera6.start();
						walk1.start();
					}
					timer.stop();
					break;
			}
	    }
	}
	public void actionPerformed(ActionEvent event)
	{
		if(event.getSource() == start)//開始擲骰
		{
			six.play();
			start.setVisible(false);
			label1.setIcon(labelicon15);
			//validate();
			//repaint();
			timer.start();
		}
	}
}
