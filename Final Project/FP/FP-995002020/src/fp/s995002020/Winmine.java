package fp.s995002020;
import javax.swing.*;
import java.awt.*;
import java.awt.Event.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.*;
import java.util.*;
import javax.swing.JOptionPane;

public class Winmine extends JFrame implements MouseListener{
	private int[] Bomb;             //炸彈位置
	private int[] num;              //周圍炸彈數量
	private JButton[] button;
	private int counter;			//按了幾次
	private int hint_counter;		//提示使用次數
	private int BigBomb_counter;	//超級炸彈使用次數
	private int BNUM;
	private boolean[] clicked;		//是否被按過
	JMenuBar bar;
	JMenu help;
	JMenuItem hint;
	JMenuItem BigBomb;
	private int Max_hint;			//最大提示數量
	private int Max_BigBomb;		//超級炸彈數量
	private boolean used_BigBomb;
	private String[] names;
	private static int numberA; 	//格子數量^1/2
	private static int numberB;		//格子數量
	
	public Winmine(int bnum , int number , String title , int Max_hint , int Max_BigBomb){
		numberA = number;
		numberB = numberA * numberA;
		String[] aa = {"功能","提示","超級炸彈"};
		names = aa;
		this.Max_hint = Max_hint;
		BNUM = bnum;
		this.Max_BigBomb = Max_BigBomb;
		setTitle(title);
		used_BigBomb = false;
		counter = hint_counter = BigBomb_counter = 0 ;
		Dimension window = Toolkit.getDefaultToolkit().getScreenSize();
		setSize(window.width,window.height);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new GridLayout(numberA,numberA));
		button = new JButton[numberB + 1];
		clicked = new boolean[numberB + 1];
		clicked[0]=true;
		for(int i=1;i<=numberB;i++){
			button[i]= new JButton();
		    button[i].addMouseListener(this);
				add(button[i]);
				clicked[i]=false;
			}
			createBombs(BNUM);
			setNum(BNUM);

			bar=new JMenuBar();
			help=new JMenu(names[0]);
			hint=new JMenuItem(names[1]+"("+Max_hint+")");
			BigBomb=new JMenuItem(names[2]+"("+Max_BigBomb+")");

			help.add(hint);
			help.add(BigBomb);
			bar.add(help);
			setJMenuBar(bar);

			hint.addActionListener(new hint());
			BigBomb.addActionListener(new use_BigBomb());

			setVisible(true);
			
		}

		public void count(){
			counter++;
		}
		
		public void createBombs(int n){
			int[] a=new int[n];
			Set<Integer> set=new HashSet<Integer>();
			while(set.size()<n){
				int s=(int)(Math.random()*numberB+1);
				set.add(s);
			}
			int x=0;
			for(Integer t:set){
				a[x++]=t;
				System.out.println(a[x-1]);
			}
			Bomb=a;
			
		}
									//getLocat(int) 傳回一個目標周圍格子編號的set
		public static Set<Integer> getLocat(int x){
			Set<Integer> set=new HashSet<Integer>();
			if(x+1<=numberB && x%numberA!=0)
				set.add(x+1);
			if(x-1>0 && x%numberA!=1)
				set.add(x-1);
			if(x+numberA<=numberB)
				set.add(x+numberA);
			if(x-numberA>0)
				set.add(x-numberA);
			if(x+numberA+1<=numberB && x%numberA!=0)
				set.add(x+numberA+1);
			if(x-numberA+1>0 && x%numberA!=0)
				set.add(x-numberA+1);
			if(x+numberA-1<=numberB && x%numberA!=1)
				set.add(x+numberA-1);
			if(x-numberA-1>0 && x%numberA!=1)
				set.add(x-numberA-1);
			return set;
		}

		public void setNum(int n){
			int[] a=new int[numberB+1];
			for(int i=0;i<=numberB;i++){
				a[i]=0;
			}
			int x;
			for(int i=0;i<n;i++){
				x=Bomb[i];
				a[x]=9;
				Set<Integer> set=getLocat(x);
				for(Integer s:set){
					a[s]++;
				}
			}
			num=a;
		}
		
		public void mouseClicked(MouseEvent e) {
			for(int i=1;i<=numberB;i++){
				if((JButton)e.getSource()==button[i]){
					if (e.getButton() == MouseEvent.BUTTON3  && !clicked[i])
						button[i].setText("炸");
					else{
						if(used_BigBomb){
							Click_used_BigBomb(i);
						}else{
							Click(i);
						}
					}
					break;
				}
			}
		}

		public void Click_used_BigBomb(int i){
			Set<Integer> set=getLocat(i);
			set.add(i);
			for(Integer s:set){
				button[s].setBackground(null);
				
				if(num[s]>=9){
					button[s].setText("*");
				}else{
					if(!clicked[s]){
						clicked[s]=true;
						button[s].setText(String.valueOf(num[s]));
						count();
						if(counter==numberB-BNUM){		
							JOptionPane.showMessageDialog(null,"過關","",JOptionPane.INFORMATION_MESSAGE);
							dispose();
							new FinalProject();
						}
					}
				}
			}
			used_BigBomb=false;
			if(BigBomb_counter!=Max_BigBomb)
				BigBomb.setEnabled(true);
		}


		public void Click(int i){
			if(num[i]>=9){
				button[i].setText("*");
				JOptionPane.showMessageDialog(null,"失敗","",JOptionPane.WARNING_MESSAGE);
				dispose();
				new FinalProject();
			}else{
				if(!clicked[i]){
					clicked[i]=true;
					button[i].setText(String.valueOf(num[i]));
					count();
					if(num[i]==0){
							Set<Integer> set=getLocat(i);
							for(Integer s:set)
								Click(s);
					}
					if(counter==numberB-BNUM){		
						JOptionPane.showMessageDialog(null,"過關","",JOptionPane.INFORMATION_MESSAGE);
						dispose();
						new FinalProject();
					}
				}
			}
		}

		public void mouseEntered(MouseEvent e) {
			if(used_BigBomb){
				for(int i=1;i<=numberB;i++){
					if((JButton)e.getSource()==button[i]){
						Set<Integer> set=getLocat(i);
						set.add(i);
						for(Integer s:set){
							button[s].setBackground(Color.RED);
						}
						break;
					}
				}
			}
		}

	    	public void mouseExited(MouseEvent e) {
			if(used_BigBomb){
				for(int i=1;i<=numberB;i++){
					if((JButton)e.getSource()==button[i]){
						Set<Integer> set=getLocat(i);
						set.add(i);
						for(Integer s:set){
							button[s].setBackground(null);
						}
						break;
					}
				}
			}
		}

	    	public void mousePressed(MouseEvent e) {}

	    	public void mouseReleased(MouseEvent e) {}

		public class use_BigBomb implements ActionListener{
			public void actionPerformed(ActionEvent e) {
				BigBomb_counter++;
				BigBomb.setText(names[2]+"("+(Max_BigBomb-BigBomb_counter)+")");
				used_BigBomb=true;
				BigBomb.setEnabled(false);
			}
		}

		public class hint implements ActionListener{
			public void actionPerformed(ActionEvent e) {
				while(true){
					int h=(int)(Math.random()*numberB+1);
					if(!clicked[h] && num[h]<9){
						Click(h);
						hint_counter++;
						hint.setText(names[1]+"("+(Max_hint-hint_counter)+")");
						if(hint_counter==Max_hint)
							hint.setEnabled(false);
						break;
					}
				}			
			}
		}
	}
		