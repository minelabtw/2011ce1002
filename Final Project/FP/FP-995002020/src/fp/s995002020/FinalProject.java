package fp.s995002020;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import javax.swing.JOptionPane;
public class FinalProject extends JFrame implements ActionListener{
	JComboBox choose;
	
	public FinalProject(){
		setTitle("選擇難度");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(300,100);
		setLayout(new GridLayout(1,2));
		choose = new JComboBox();
		choose.addItem("簡單");
		choose.addItem("普通");
		choose.addItem("困難");
		choose.addItem("高手");
		choose.addItem("神手");
		choose.addItem("地獄");
		add(choose);
		
		JButton btn = new JButton("確定");
		btn.addActionListener(this);
		add(btn);
		setVisible(true);
	}
	
	public void actionPerformed(ActionEvent e){
		int a = choose.getSelectedIndex();
		dispose();
		switch(a){
				case 0:
						new Winmine(5,7,"簡單",1,1);
						break;
				case 1:
						new Winmine(12,9,"普通",3,2);
						break;
				case 2:
					    new Winmine(28,12,"困難",5,3);
					    break;
				case 3:
						new Winmine(45,15,"高手",7,4);
						break;
				case 4:
						new Winmine(160,20,"神手",9,5);
						break;
				case 5:
						new Winmine(300,25,"地獄",11,6);
						break;
		}
	}
	
	public static void main(String [] args){
		new FinalProject();
		Audio audio = new Audio();
		audio.loadAudio("1.wav", null);//test.wav是我測試的音檔，請自行拿手邊wav檔測試)
		audio.setPlayCount(0);//一直播放
		audio.play();
	}
}
