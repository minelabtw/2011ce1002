package FP.s100502009;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.border.*;

public class Play extends JPanel implements ActionListener{
	private JLabel l1=new JLabel("Money");
	private JLabel l2=new JLabel("存檔");
	private JLabel l3=new JLabel("已獲得名產");
	String money;
	String thing;	
	JTextField t1=new JTextField(15);
	Dice d=new Dice();
	Map m=new Map();
	JTextField t2=new JTextField(15);
	private JPanel p1=new JPanel(new FlowLayout(FlowLayout.LEFT,20,20));
	private JPanel p2=new JPanel();
	private ImageIcon image=new ImageIcon("project/file.png");	
	private JButton b;	
	Font f=new Font("TimesRoman",Font.BOLD,20);	
	Border border=new LineBorder(Color.BLACK,2);	

	public Play()
	{			
		try {
			write();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		d.setM(m);
		l1.setBorder(border);
		l2.setBorder(border);
		l3.setBorder(border);
		l1.setFont(f);
		l2.setFont(f);
		l3.setFont(f);		
		l1.setForeground(Color.BLUE);
		l2.setForeground(Color.BLUE);
		l3.setForeground(Color.BLUE);
		t1.setText(money);
		t2.setText(thing);
		image.setImage(image.getImage().getScaledInstance(50,50, Image.SCALE_DEFAULT));
		b=new JButton(image);
		b.setBackground(Color.BLACK);
		p1.setPreferredSize(new Dimension(1000,110));
		p2.setPreferredSize(new Dimension(1020,650));
		p1.add(l1);
		p1.add(t1);
		p1.add(l2);
		p1.add(b);
		p1.add(l3);
		p1.add(t2);
		p1.add(d);
		p1.setBackground(Color.YELLOW);
		p2.add(m);
		add(p1,BorderLayout.EAST);
		add(p2,BorderLayout.CENTER);
		b.addActionListener(this);
	}	
	
	public void actionPerformed(ActionEvent e)
	{
		try {
			store();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}		
	}
	

	public void store() throws Exception//store the data
	{
		java.io.PrintWriter output=new java.io.PrintWriter("score.txt");
		output.write(money+"\n");
		output.write(thing);
		output.close();
	}
	
	public void write() throws Exception//read the data
	{
		java.io.File file=new java.io.File("score.txt");
		Scanner input=new Scanner(file);
		this.money=input.next();
		this.thing=input.next();
		input.close();
	}
}
