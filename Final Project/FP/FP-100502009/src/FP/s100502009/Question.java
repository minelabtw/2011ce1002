package FP.s100502009;
import java.awt.*;

import javax.swing.*;

import java.awt.event.*;

public class Question extends JPanel implements ActionListener{
	String[] que={
		"",
		"Q:三個人，豎著站成一排。有五個帽子，三個藍色，兩個紅色，每人帶一個，各自不准看自己的顏色。然後問第一個人帶的什麼顏色的帽子，" +
		"\n他說不知道，然後又問第二個人帶的什麼顏色的帽子，同樣說不知道，又問第三個人帶的是什麼顏色的帽子，他說我知道。問第三個人帶的是什麼色帽子? （第一個人站在排的最後,他可以看見前二個人的帽子的顏色)",
		"Q:有對一模一樣的雙胞胎兄弟，哥哥的屁股有黑痣，而弟弟沒有。但即使這對雙胞胎穿著相同的服飾，仍然有人可立刻知道誰是哥哥，誰是弟弟。究竟是誰呢？ ",
		"Q:一種東西，東方人的短，西方人的長，結婚後女的就可以用男的這東西，和尚有但是不用它 ",
		"Q:一根又黑、又粗、又硬的棍子插進洞裡，感覺暖烘烘的,等到抽出來以後，客人就要付錢啦（一種行業）",
		"Q:三個金「鑫」，三個水叫「淼」，三個人叫「眾」，那麼三個鬼應該叫什麼?",
		"Q:一年前的元月一日，所有的人都在做著一件非常重要的事，你記得是什麼事嗎？ ",
		"Q:小明的爸爸只當了一次官，而且只當了幾天。可是因為當了那次官，鬧得他每天都要掏腰包，他當的是什麼官? ",
		"Q:神偷「妙手空空」把附近一些有錢人家的金銀珠寶偷得一乾二淨，為什麼唯獨一家既無防盜設備，也無保全人員的財主沒受到光顧？ ",
		"Q:出去的時候光著身子，回到家才穿上衣服的是什麼？ "
	};
	String[] ans={
		"",
		"是藍色",
		"是紅色",
		"他們自己",
		"他們朋友",
		"名字",
		"鼻子",
		"烤番薯",
		"削甘蔗",
		"叫救命",
		"叫披薩",
		"都在呼吸",
		"都在睡覺",
		"新郎官",
		"皇上",
		"他自己家",
		"警察家",
		"衣架",
		"衣櫥"
	};
	JPanel p1=new JPanel();
	JPanel p2=new JPanel(new GridLayout(1,2));
	JPanel p3=new JPanel(new FlowLayout());
	JPanel p4=new JPanel(new GridLayout(2,1));
	JPanel p5=new JPanel(new FlowLayout());
	JLabel pic=new JLabel();
	JRadioButton j1,j2;
	JButton close=new JButton("Continue");
	JTextField c;
	JTextField w;
	ImageIcon picture;
	int index;
	Play p;
	ImageIcon[] image={
			new ImageIcon(""),
			new ImageIcon("project/taipei2.jpg"),
			new ImageIcon("project/hualian2.jpg"),
			new ImageIcon("project/iilan2.jpg"),
			new ImageIcon("project/taouan2.jpg"),
			new ImageIcon("project/tamsui2.jpg"),
			new ImageIcon("project/tainan2.jpg"),
			new ImageIcon("project/taidong2.jpg"),
			new ImageIcon("project/taichung2.jpg"),
			new ImageIcon("project/sinchu2.jpg"),
			new ImageIcon("project/pindong2.jpg"),
			new ImageIcon("project/nantao2.jpg"),
			new ImageIcon("project/miaouli2.jpg"),
			new ImageIcon("project/keelung2.jpg"),
			new ImageIcon("project/kaoushong2.jpg"),
			new ImageIcon("project/unlin2.jpg"),
			new ImageIcon("project/chanhua2.jpg"),
			new ImageIcon("project/chiaii2.jpg"),
		};	
	public Question()
	{
		p1.setBackground(new Color(Color.OPAQUE));
		
		p3.setBackground(new Color(Color.TRANSLUCENT));	
	}
	public void chooseindex(int n)//take the sit now to set the picture
	{		
		this.index=n;
		pic.setIcon(image[index]);
		setQuestion();
	}
	
	public void setPic()
	{
		image[1].setImage(image[1].getImage().getScaledInstance(500,350, Image.SCALE_DEFAULT));
		image[2].setImage(image[2].getImage().getScaledInstance(500,350, Image.SCALE_DEFAULT));
		image[3].setImage(image[3].getImage().getScaledInstance(500,350, Image.SCALE_DEFAULT));
		image[4].setImage(image[4].getImage().getScaledInstance(500,350, Image.SCALE_DEFAULT));
		image[5].setImage(image[5].getImage().getScaledInstance(500,350, Image.SCALE_DEFAULT));
		image[6].setImage(image[6].getImage().getScaledInstance(500,350, Image.SCALE_DEFAULT));
		image[7].setImage(image[7].getImage().getScaledInstance(500,350, Image.SCALE_DEFAULT));
		image[8].setImage(image[8].getImage().getScaledInstance(500,350, Image.SCALE_DEFAULT));
		image[9].setImage(image[9].getImage().getScaledInstance(500,350, Image.SCALE_DEFAULT));
		image[10].setImage(image[10].getImage().getScaledInstance(500,350, Image.SCALE_DEFAULT));
		image[11].setImage(image[11].getImage().getScaledInstance(500,350, Image.SCALE_DEFAULT));
		image[12].setImage(image[12].getImage().getScaledInstance(500,350, Image.SCALE_DEFAULT));
		image[13].setImage(image[13].getImage().getScaledInstance(500,350, Image.SCALE_DEFAULT));
		image[14].setImage(image[14].getImage().getScaledInstance(500,350, Image.SCALE_DEFAULT));
		image[15].setImage(image[15].getImage().getScaledInstance(500,350, Image.SCALE_DEFAULT));
		image[16].setImage(image[16].getImage().getScaledInstance(500,350, Image.SCALE_DEFAULT));
		image[17].setImage(image[17].getImage().getScaledInstance(500,350, Image.SCALE_DEFAULT));
	}
	
	public void setQuestion()//initialize the question
	{			
		JTextArea t=new JTextArea(5,50);	
		setLayout(new BorderLayout());
		int num=(int)(Math.random()*9)+1;
		t.setText(que[num]);
		t.setLineWrap(true);
		t.setWrapStyleWord(true);
		t.setEditable(false);		
		JScrollPane scroll=new JScrollPane(t);	
		
		p2.setPreferredSize(new Dimension(500,10));
		p5.setBackground(Color.BLACK);
		close.setBackground(Color.YELLOW);
		p1.add(scroll,BorderLayout.CENTER);
		j1=new JRadioButton("A."+ans[(num*2)-1]);
		j2=new JRadioButton("B."+ans[num*2]);
		p2.add(j1);
		p2.add(j2);	
		p2.setBackground(Color.GREEN);
		p4.add(p1);
		p4.add(p2);
		ButtonGroup group=new ButtonGroup();
		group.add(j1);
		group.add(j2);
		j1.addActionListener(this);
		j2.addActionListener(this);
		close.addActionListener(this);
		add(p4,BorderLayout.NORTH);		
		setPic();
	}
	
	
	public void actionPerformed(ActionEvent e)//show the correct or wrong message when the choice is chosen
	{
		if(e.getSource()==j1)
		{
			try
			{
				decide(0);				
				p3.add(c);				
				p3.add(pic);
				p5.add(close);
				add(p5,BorderLayout.SOUTH);
				add(p3,BorderLayout.CENTER);
				int n=Integer.parseInt(p.thing)+1;
				p.thing=String.valueOf(n);
				p.t2.setText(p.thing);
				p.repaint();
				p.validate();	
				repaint();
				validate();					
			}
			catch(QuestionException ex)
			{
				w=new JTextField(ex.getMessage());
				pic.setIcon(image[index]);
				p3.add(w);				
				p3.add(pic);
				p5.add(close);
				add(p5,BorderLayout.SOUTH);
				add(p3,BorderLayout.CENTER);
				int m1=Integer.parseInt(p.money)-1000000;
				p.money=String.valueOf(m1);
				p.t1.setText(p.money);
				int n=Integer.parseInt(p.thing)+1;
				p.thing=String.valueOf(n);
				p.t2.setText(p.thing);
				p.repaint();
				p.validate();

				p5.add(close);
				repaint();
				validate();
			}
		}
		else if(e.getSource()==j2)
		{			
			try
			{
				decide(-1);
				pic.setIcon(image[index]);
				p3.add(c);				
				p3.add(pic);
				p5.add(close);
				add(p5,BorderLayout.SOUTH);
				add(p3,BorderLayout.CENTER);
				int n=Integer.parseInt(p.thing)+1;
				p.thing=String.valueOf(n);
				p.t2.setText(p.thing);
				p.repaint();
				p.validate();

				p5.add(close);
				repaint();
				validate();
			}
			catch(QuestionException ex)
			{
				w=new JTextField(ex.getMessage());
				pic.setIcon(image[index]);
				p3.add(w);				
				p3.add(pic);
				p5.add(close);
				add(p5,BorderLayout.SOUTH);
				add(p3,BorderLayout.CENTER);
				int m1=Integer.parseInt(p.money)-1000000;
				p.money=String.valueOf(m1);
				p.t1.setText(p.money);
				int n=Integer.parseInt(p.thing)+1;
				p.thing=String.valueOf(n);
				p.t2.setText(p.thing);
				p.repaint();
				p.validate();
				p5.add(close);
				repaint();
				validate();
			}
		}
		else if(e.getSource()==close)//close the frame
		{		
			p1.removeAll();
			p2.removeAll();
			p3.removeAll();
			removeAll();
			repaint();
			validate();
			p.m.i.close();
		}
	}
	public void decide(int n)throws QuestionException//tell if the user is correct or wrong
	{
		if(n==0)
		{
			c=new JTextField("恭喜你~~答對囉!! 可以免費得到名產一份~繼續加油XD");
		}
		else if(n==-1)
		{
			throw new QuestionException();			
		}
	}
	
	public void qsetP(Play p)
	{
		this.p=p;
	}


}
