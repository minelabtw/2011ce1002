package FP.s100502009;
import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.border.*;

import FP.s100502009.Dice.TimerListener;

public class Map extends JPanel{
	ImageIcon[] m={
			new ImageIcon("project/ncu.jpg"),
			new ImageIcon("project/taipei.jpg"),
			new ImageIcon("project/hualian.jpg"),
			new ImageIcon("project/iilan.jpg"),
			new ImageIcon("project/taouan.jpg"),
			new ImageIcon("project/tamsui.jpg"),
			new ImageIcon("project/tainan.jpg"),
			new ImageIcon("project/taidong.jpg"),
			new ImageIcon("project/taichung.jpg"),
			new ImageIcon("project/sinchu.jpg"),
			new ImageIcon("project/pindong.jpg"),
			new ImageIcon("project/nantao.jpg"),
			new ImageIcon("project/miaouli.jpg"),
			new ImageIcon("project/keelung.jpg"),
			new ImageIcon("project/kaoushong.jpg"),
			new ImageIcon("project/unlin.jpg"),
			new ImageIcon("project/chanhua.jpg"),
			new ImageIcon("project/chiaii.jpg")			
		};
	ImageIcon m19=new ImageIcon("project/Taiwan3.jpg");
	ImageIcon[] image={
		new ImageIcon("project/ncu2.jpg"),
		new ImageIcon("project/taipei3.jpg"),
		new ImageIcon("project/hualian3.jpg"),
		new ImageIcon("project/iilan3.jpg"),
		new ImageIcon("project/taouan3.jpg"),
		new ImageIcon("project/tamsui3.jpg"),
		new ImageIcon("project/tainan3.jpg"),
		new ImageIcon("project/taidong3.jpg"),
		new ImageIcon("project/taichung3.jpg"),
		new ImageIcon("project/sinchu3.jpg"),
		new ImageIcon("project/pindong3.jpg"),
		new ImageIcon("project/nantao3.jpg"),
		new ImageIcon("project/miaouli3.jpg"),
		new ImageIcon("project/keelung3.jpg"),
		new ImageIcon("project/kaoushong3.jpg"),
		new ImageIcon("project/unlin3.jpg"),
		new ImageIcon("project/chanhua3.jpg"),
		new ImageIcon("project/chiaii3.jpg")		
	};
	JLabel[] l={
		new JLabel(image[0]),
		new JLabel(m[1]),	
		new JLabel(m[2]),
		new JLabel(m[3]),
		new JLabel(m[4]),
		new JLabel(m[5]),
		new JLabel(m[6]),
		new JLabel(m[7]),
		new JLabel(m[8]),
		new JLabel(m[9]),
		new JLabel(m[10]),
		new JLabel(m[11]),
		new JLabel(m[12]),
		new JLabel(m[13]),
		new JLabel(m[14]),
		new JLabel(m[15]),
		new JLabel(m[16]),
		new JLabel(m[17]),		
	};
	JLabel l19=new JLabel(m19);	
	JPanel p1=new JPanel(new GridLayout(1,6,1,1));
	JPanel p2=new JPanel(new GridLayout(1,6,1,1));
	JPanel p3=new JPanel(new GridLayout(3,1,1,1));
	JPanel p4=new JPanel(new GridLayout(3,1,1,1));
	JPanel p5=new JPanel(new BorderLayout());
	Border b=new LineBorder(Color.BLACK,2);
	int num;
	int sit;
	int[] change=new int[18];
	Timer timer=new Timer(500,new TimerListener());
	JFrame frame3=new JFrame();	
	Intro i =new Intro();		
	public Map()
	{
		setMap();
		l[0].setBorder(b);
		l[1].setBorder(b);
		l[2].setBorder(b);
		l[3].setBorder(b);
		l[4].setBorder(b);
		l[5].setBorder(b);
		l[6].setBorder(b);
		l[7].setBorder(b);
		l[8].setBorder(b);
		l[9].setBorder(b);
		l[10].setBorder(b);
		l[11].setBorder(b);
		l[12].setBorder(b);
		l[13].setBorder(b);
		l[14].setBorder(b);
		l[15].setBorder(b);
		l[16].setBorder(b);
		l[17].setBorder(b);
		l19.setBorder(b);		
		p1.add(l[0]);
		p1.add(l[1]);
		p1.add(l[2]);
		p1.add(l[3]);
		p1.add(l[4]);
		p1.add(l[5]);
		p3.add(l[6]);
		p3.add(l[7]);
		p3.add(l[8]);
		p2.add(l[14]);
		p2.add(l[13]);
		p2.add(l[12]);
		p2.add(l[11]);
		p2.add(l[10]);
		p2.add(l[9]);
		p4.add(l[17]);
		p4.add(l[16]);
		p4.add(l[15]);		
		p5.add(p1,BorderLayout.NORTH);
		p5.add(p2,BorderLayout.SOUTH);
		p5.add(p3,BorderLayout.EAST);
		p5.add(p4,BorderLayout.WEST);
		p5.add(l19,BorderLayout.CENTER);
		add(p5);		
		frame3.setSize(800,500);
		frame3.add(i);
		frame3.setTitle("Description");
	}
	
	public void moving(int n,int s)//move the car
	{
		num=n;
		sit=s;
		timer.start();
		setMap();
	}
	
	public void setMap()//set the size of the pictures
	{
		image[0].setImage(image[0].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		m[1].setImage(m[1].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		m[2].setImage(m[2].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		m[3].setImage(m[3].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		m[4].setImage(m[4].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		m[5].setImage(m[5].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		m[6].setImage(m[6].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		m[7].setImage(m[7].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		m[8].setImage(m[8].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		m[9].setImage(m[9].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		m[10].setImage(m[10].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		m[11].setImage(m[11].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		m[12].setImage(m[12].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		m[13].setImage(m[13].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		m[14].setImage(m[14].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		m[15].setImage(m[15].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		m[16].setImage(m[16].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		m[17].setImage(m[17].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));		
		m19.setImage(m19.getImage().getScaledInstance(640,345, Image.SCALE_DEFAULT));
		m[0].setImage(m[0].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		image[1].setImage(image[1].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		image[2].setImage(image[2].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		image[3].setImage(image[3].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		image[4].setImage(image[4].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		image[5].setImage(image[5].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		image[6].setImage(image[6].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		image[7].setImage(image[7].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		image[8].setImage(image[8].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		image[9].setImage(image[9].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		image[10].setImage(image[10].getImage().getScaledInstance(150,115, Image.SCALE_DEFAULT));
		image[11].setImage(image[11].getImage().getScaledInstance(150,115, Image.SCALE_DEFAULT));
		image[12].setImage(image[12].getImage().getScaledInstance(150,115, Image.SCALE_DEFAULT));
		image[13].setImage(image[13].getImage().getScaledInstance(150,115, Image.SCALE_DEFAULT));
		image[14].setImage(image[14].getImage().getScaledInstance(150,115, Image.SCALE_DEFAULT));
		image[15].setImage(image[15].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		image[16].setImage(image[16].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
		image[17].setImage(image[17].getImage().getScaledInstance(160,115, Image.SCALE_DEFAULT));
	}
	
	public void close()//close the frame
	{
		frame3.setVisible(false);
	}
	class TimerListener implements ActionListener//decide the sit of the car and the steps to move
	{
		public void actionPerformed(ActionEvent e)
		{			
			
			if(num>0)
			{
				if(change[sit]==1&&sit==17)
				{
					l[sit].setIcon(m[sit]);
					l[0].setIcon(image[0]);
					change[0]++;
					change[sit]--;
					sit=0;
				}
				else if(change[sit]==1)
				{
					l[sit].setIcon(m[sit]);
					l[sit+1].setIcon(image[sit+1]);
					change[sit+1]++;
					change[sit]--;
					sit++;
				}
				else
				{
					l[sit].setIcon(m[sit]);
					l[sit+1].setIcon(image[sit+1]);
					change[sit+1]++;
					sit++;
				}
				repaint();
				validate();
				num--;
			}
			
			else if(num==0)
			{
				timer.stop();				
				if(sit>0)
				{
					i.set(m,sit);
					frame3.setVisible(true);
					validate();
				}	
			}
		}
	}
}
