package FP.s100502009;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class Dice extends JPanel implements ActionListener{//create a dice program
	ImageIcon[] image={
		new ImageIcon("project/DOT1.jpg"),
		new ImageIcon("project/DOT2.jpg"),
		new ImageIcon("project/DOT3.jpg"),
		new ImageIcon("project/DOT4.jpg"),
		new ImageIcon("project/DOT5.jpg"),
		new ImageIcon("project/DOT6.jpg")
	}; 
	JPanel p1=new JPanel();
	JLabel l1=new JLabel(image[0]);
	JButton b1=new JButton("Start");
	JButton b2=new JButton("Stop");
	Timer timer=new Timer(50,new TimerListener());
	int number;
	int sit;
	Map m;
	public Dice()
	{
		p1.setLayout(new GridLayout(1,2,1,1));
		setLayout(new BorderLayout());
		b1.setBackground(Color.BLACK);
		b1.setForeground(Color.WHITE);
		b2.setBackground(Color.BLACK);
		b2.setForeground(Color.WHITE);
		p1.add(b1);
		p1.add(b2);
		add(l1,BorderLayout.CENTER);
		add(p1,BorderLayout.EAST);		
		b1.addActionListener(this);
		b2.addActionListener(this);
	}		
	
	public void actionPerformed(ActionEvent e)//car start to move after the dice is been thrown
	{
		if(e.getSource()==b1)
		{
			timer.start();
		}
		else if(e.getSource()==b2)
		{
			timer.stop();
			m.moving(number,sit);
			if(sit+number<=17)
			{				
				sit+=number;
			}
			else
			{				
				sit=number+sit-18;
			}			
		}
	}
	
	class TimerListener implements ActionListener//change the picture of the dice
	{
		public void actionPerformed(ActionEvent e)
		{					
			int num=(int)(Math.random()*6);
			number=num+1;
			l1.setIcon(image[num]);
			l1.repaint();
		}
	}
	
	public void  setM(Map m)
	{
		this.m=m;
	}
	
}
