package FP.s100502009;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Start extends JPanel{//create a start frame
	JButton start=new JButton("Start");
	JButton introduction=new JButton("Introduction");
	private ImageIcon imageIcon=new ImageIcon("project/taiwan4.jpg");
	private Image image=imageIcon.getImage();	
	public Start()
	{	
		setLayout(null);
		start.setBounds(getWidth()/2,getHeight()/2,300,100);
		introduction.setBounds(getWidth()/2,getHeight()/2,300,100);	
		start.setForeground(Color.BLACK);
		start.setBackground(Color.WHITE);
		introduction.setForeground(Color.WHITE);
		introduction.setBackground(Color.black);
		add(start);
		add(introduction);		
		start.setBounds(250,350,100,35);
		introduction.setBounds(245,420,110,35);
	}
	
	protected void paintComponent(Graphics g)//draw the background
	{
		super.paintComponent(g);
		g.drawImage(image,0,0,getWidth(),getHeight(),this);
	}
}
