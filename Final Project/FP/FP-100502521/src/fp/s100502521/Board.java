package fp.s100502521;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Board
{
	private File file;
	private Scanner input;
	private PrintWriter out;
	private String []level=new String[10];
	private int []scores=new int[10];
	private int index=1;
	public Board() throws FileNotFoundException
	{
		file=new File("src/fp/s100502521/score.txt");
		input= new Scanner(file);
		for(int i=0;i<10;i++)
		{
			if(input.hasNext())
			{
				level[i]=input.next();
				scores[i]=input.nextInt();
				index++;
			}
		}
		input.close();
	}
	public void add(String name,int score)
	{
		for(int i=0;i<10;i++)
		{
			if(score>scores[i])
			{
				for(int j=9;j>i;j--)
				{
					scores[j]=scores[j-1];
					level[j]=level[j-1];
				}
				scores[i]=score;
				level[i]=name;
				break;
			}
		}
		try {
			out=new PrintWriter(file);
			for(int i=0;i<index;i++)
			{
				out.println(level[i]+"  "+scores[i]);
			}
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public String getlevel(int i)
	{
		return level[i];
	}
	public int getscore(int i)
	{
		return scores[i];
	}
}
