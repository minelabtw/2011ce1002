package fp.s100502521;

import java.awt.Image;
import java.awt.Toolkit;

public class Enemy 
{
	private Toolkit tk = Toolkit.getDefaultToolkit();
	private Image image;
	private Image []attack_image=new Image[3];
	private int x,y,radius,speed,type,width,height,life,damage;
	private int attack_animation=0;
	private boolean dead;
	Enemy(int input_x,int input_y,int input_r,int input_speed,int input_type)
	{
		dead=false;
		x=input_x;
		y=input_y;
		radius=input_r;
		speed=input_speed;
		type=input_type;
		image=tk.getImage("src//fp//s100502521//image//monster"+type+"_0.png");
		attack_image[0]=tk.getImage("src//fp//s100502521//image//monster"+type+"_1.png");
		attack_image[1]=tk.getImage("src//fp//s100502521//image//monster"+type+"_2.png");
		attack_image[2]=tk.getImage("src//fp//s100502521//image//monster"+type+"_3.png");
		switch(type)
		{
		case 0:
			width=200;
			height=105;
			life=100;
			damage=50;
		case 1:
		case 2:
		}
	}
	public void deLife(int i)
	{
		life-=i;
	}
	public int getlife()
	{
		return life;
	}
	public void setDead(boolean p)
	{
		dead=p;
	}
	public int attack()
	{
		if(x<170)
		{
			attack_animation=1;
			return damage;
		}
		return 0;
	}
	public boolean check(int bullet_x,int bullet_y,int bullet_r)
	{
		int dx=(bullet_x-x)*(bullet_x-x);
		int dy=(bullet_y-y)*(bullet_y-y);
		int dr=(radius-bullet_r)*(radius-bullet_r)+10000;
		if(dx+dy<=dr)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public void progress()
	{
		if(x>=170)
		{
			x-=speed;
		}
	}
	public Image getImage()
	{
		if(attack_animation<=18 && attack_animation!=0)
		{
			attack_animation++;
			if(attack_animation<=6)
			{
				return attack_image[0];
			}
			else if(attack_animation<=12)
			{
				return attack_image[1];
			}
			else if(attack_animation<=18)
			{
				return attack_image[2];
			}
		}
		else
		{
			attack_animation=0;
		}
		return image;
	}
	public int getX()
	{
		return x;
	}
	public int getY()
	{
		return y;
	}
	public int getR()
	{
		return radius;
	}
	public int getWidth()
	{
		return width;
	}
	public int getHeight()
	{
		return height;
	}
	public boolean getDead()
	{
		return dead;
	}

}

