package fp.s100502521;

public class MonsterController 
{
	public Enemy []monster=new Enemy[100];
	private int index=0;
	MonsterController()
	{
		
	}
	public void addgroup()
	{
		int number=(int) (Math.random()*(7)+1);
		for(int i=0;i<number;i++)
		{
			add();
		}
	}
	public void add()
	{
		if(index<100)
		{
			monster[index]=new Enemy(1024, 560, 100, (int) (Math.random()*5+1), 0);
			index++;
		}
	}
	public void delete(int i)
	{
		monster[i]=null;
		for(int j=i;j<index-1;j++)
		{
			monster[j]=monster[j+1];
		}
		index--;
		monster[index]=null;
	}
	public void beAttacked(int bullet_x,int bullet_y,int bullet_r,int damage)
	{
		for(int i=0;i<index;i++)
		{
			if(monster[i].check(bullet_x, bullet_y, bullet_r))
			{
				monster[i].deLife(damage);
			}
			if(monster[i].getlife()<=0)
			{
				monster[i].setDead(true);
			}
		}
	}
	public int getIndex()
	{
		return index;
	}
	public int check()
	{
		int count=0;
		for(int i=0;i<index;i++)
		{
			if(monster[i].getDead())
			{
				delete(i);
				count++;
			}
		}
		return count;
	}
	public void progress()
	{
		for(int i=0;i<index;i++)
		{
			monster[i].progress();
		}
	}
	public int attack()
	{
		int total=0;
		for(int i=0;i<index;i++)
		{
			total+=monster[i].attack();
		}
		return total;
	}
}
