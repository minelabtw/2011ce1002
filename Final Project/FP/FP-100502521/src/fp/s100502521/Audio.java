package fp.s100502521;

import java.applet.Applet;
import java.applet.AudioClip;

public class Audio 
{
	private AudioClip []music=new AudioClip[3];
	Audio()
	{
		music[0]=Applet.newAudioClip(getClass().getResource("music\\m0.WAV"));
		music[1]=Applet.newAudioClip(getClass().getResource("music\\m1.WAV"));
	}
	public void play(int i)
	{
		music[i].play();
	}
	public void loop(int i)
	{
		music[i].loop();
	}
	public void stop(int i)
	{
		music[i].stop();
	}
}
