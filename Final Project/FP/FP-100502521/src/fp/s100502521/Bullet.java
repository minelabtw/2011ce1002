package fp.s100502521;

import java.awt.Image;
import java.awt.Toolkit;

public class Bullet 
{
	private Toolkit tk = Toolkit.getDefaultToolkit();
	private Image image;
	private int x,y,dx,dy,wind,str,width,height,type,damage,radius;
	double degree;
	private int g=1;
	public Bullet(int input_x,int input_y,int input_wind,int input_str,int input_degree,int input_type)
	{
		type=input_type;
		image=tk.getImage("src//fp//s100502521//image//Bullet_"+type+".png");
		x=input_x;
		y=input_y;
		wind=input_wind;
		str=input_str;
		degree=(input_degree/360.0)*2*Math.PI;
		dx=(int) (str*Math.cos(degree)/2);
		dy=(int) (0-str*Math.sin(degree)/2);
		switch(input_type)
		{
		case 0:
			width=59;
			height=61;
			damage=120;
			radius=30;
			break;
		case 1:
			break;
		case 2:
			break;
		}
	}
	public void flyProgress()
	{
		//dx=dx+wind;
		dy=dy+g;
		if(dy>50)
		{
			dy=10;
		}
		if(dx>50)
		{
			dx=10;
		}
		x=x+dx+wind;
		y+=dy;
		
	}
	public int getX()
	{
		return x;
	}
	public int getY()
	{
		return y;
	}
	public int getR()
	{
		return radius;
	}
	public int getDamage()
	{
		return damage;
	}
	public boolean getfall()
	{
		if(dy>0)
		{
			return true;
		}
		return false;
	}
	public int getWidth()
	{
		return width;
	}
	public int getHeight()
	{
		return height;
	}
	public Image getImage()
	{
		return image;
	}
}
