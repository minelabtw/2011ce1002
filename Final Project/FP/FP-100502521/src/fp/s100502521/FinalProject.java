package fp.s100502521;

import java.io.FileNotFoundException;

import javax.swing.JFrame;
public class FinalProject 
{ 
	public static void main(String[] args) throws FileNotFoundException
	{
		JFrame frame1 = new FrameWork();//創造視窗
		frame1.setSize(1024,768);
		frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//設定視窗標題列的關閉按鈕結束程式執行
		frame1.setResizable(false);
		frame1.setVisible(true);
	}
}
