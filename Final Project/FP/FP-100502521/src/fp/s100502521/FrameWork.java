package fp.s100502521;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.FileNotFoundException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class FrameWork extends JFrame implements KeyListener
{
	private gameOfMain mainPanel;
	private Timer time;
	FrameWork() throws FileNotFoundException
	{
		setTitle("�u�u���u");
		time=new Timer(16,new TimerListener());
		addKeyListener(this);
		mainPanel=new gameOfMain();
		add(mainPanel);
		time.start();
	}
	class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			mainPanel.progress();
			mainPanel.repaint();
		}
	}
	public void keyPressed(KeyEvent e)
	{
		switch (e.getKeyCode()) 
		{
		case KeyEvent.VK_UP:
			mainPanel.key.up=true;
			break;
		case KeyEvent.VK_DOWN:
			mainPanel.key.down=true;
			break;
		case KeyEvent.VK_RIGHT:
			mainPanel.key.right=true;
			break;
		case KeyEvent.VK_LEFT:
			mainPanel.key.left=true;
			break;
		case KeyEvent.VK_SPACE:
			mainPanel.key.space=true;
			break;
		case KeyEvent.VK_Z:
			mainPanel.key.key1=true;
			break;
		case KeyEvent.VK_X:
			mainPanel.key.key2=true;
			break;
		case KeyEvent.VK_C:
			mainPanel.key.key3=true;
			break;
		}
	}
	public void keyReleased(KeyEvent e) 
	{
		switch (e.getKeyCode()) 
		{
		case KeyEvent.VK_UP:
			mainPanel.key.up=false;
			break;
		case KeyEvent.VK_DOWN:
			mainPanel.key.down=false;
			break;
		case KeyEvent.VK_RIGHT:
			mainPanel.key.right=false;
			break;
		case KeyEvent.VK_LEFT:
			mainPanel.key.left=false;
			break;
		case KeyEvent.VK_SPACE:
			mainPanel.key.space=false;
			break;
		case KeyEvent.VK_1:
			mainPanel.key.key1=false;
			break;
		case KeyEvent.VK_2:
			mainPanel.key.key2=false;
			break;
		case KeyEvent.VK_3:
			mainPanel.key.key3=false;
			break;
		}
	}
	public void keyTyped(KeyEvent e)
	{
		
	}
}
