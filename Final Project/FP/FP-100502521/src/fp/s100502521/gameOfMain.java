package fp.s100502521;

import java.awt.Color;
import java.awt.Event;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.FileNotFoundException;

import javax.swing.JPanel;

public class gameOfMain extends JPanel
{
	private Toolkit tk = Toolkit.getDefaultToolkit();
	private Image Title_background;
	private Image Game_background;
	private Image Board_background;
	private Image Gameover_image;
	private int gameStatu,roundStatu;
	private BulletControl BC=new BulletControl();
	private MonsterController MC=new MonsterController();
	private Player player1;
	public KeyStatus key=new KeyStatus();
	private TitleStatus title=new TitleStatus();
	private Image [][]TitleButton=new Image[3][2];
	private Image strbar1,strbar2,tag;
	private GameStatus game=new GameStatus();
	private int tagX=0,tempX=0,enemy_move_frame=0;
	private int wind=0;
	private boolean gameover=false;
	private int gameover_frame=0,title_delay=6;
	private Audio audio=new Audio();
	private Board board;
	public gameOfMain() throws FileNotFoundException
	{
		gameStatu=0;
		roundStatu=0;
		Title_background=tk.getImage("src//fp//s100502521//image//Title_Back.png");
		Game_background=tk.getImage("src//fp//s100502521//image//game_Back.png");
		TitleButton[0][0]=tk.getImage("src//fp//s100502521//image//button0_0.png");
		TitleButton[0][1]=tk.getImage("src//fp//s100502521//image//button0_1.png");
		TitleButton[1][0]=tk.getImage("src//fp//s100502521//image//button1_0.png");
		TitleButton[1][1]=tk.getImage("src//fp//s100502521//image//button1_1.png");
		TitleButton[2][0]=tk.getImage("src//fp//s100502521//image//button2_0.png");
		TitleButton[2][1]=tk.getImage("src//fp//s100502521//image//button2_1.png");
		strbar1=tk.getImage("src//fp//s100502521//image//statebar0.png");
		strbar2=tk.getImage("src//fp//s100502521//image//statebar1.png");
		tag=tk.getImage("src//fp//s100502521//image//tag.png");
		Gameover_image=tk.getImage("src//fp//s100502521//image//gameover2.gif");
		Board_background=tk.getImage("src//fp//s100502521//image//board_back.png");
		audio.loop(0);
		board=new Board();
	}
	public void progress()
	{
		switch(gameStatu)
		{
		case 0://Title
			if(key.up)
			{
				title_delay++;
				if(title_delay>=6)
				{
					title.left();
					title_delay=0;
				}
			}
			else if(key.down)
			{
				title_delay++;
				if(title_delay>=6)
				{
					title.right();
					title_delay=0;
				}
			}
			else if(key.space)
			{
				title_delay++;
				if(title_delay>=6)
				{
					title_delay=0;
					switch(title.getSelect())
					{
					case 0:
						gameStatu=1;
						key.space=false;
						player1=null;
						player1=new Player();
						audio.stop(0);
						audio.loop(1);
						player1.setName();
						break;
					case 1:
						gameStatu=2;
						key.space=false;
						break;
					case 2:
						System.exit(0);
						break;
					}
				}
				
			}
			break;
		case 1://Game
			switch(roundStatu)
			{
			case 0://player
				if(game.fireing)
				{
					if(!key.space)
					{
						game.fireing=false;
						BC.add(new Bullet(player1.getX(),player1.getY(),wind,game.str,game.degree,player1.myWeapon.Select));
						game.renew();
						tempX=tagX;
						tagX=0;
						player1.state=0;
						roundStatu=2;
					}
					else
					{
						player1.state=1;
						if(game.str<93)
						{
							tagX+=7;
							game.str++;
						}
						else if(game.str==93)
						{
							tagX=0;
							game.renew();
						}
					}
				}
				else
				{
					if(key.space)
					{
						game.fireing=true;
					}
					else if(key.up)
					{
						if(game.degree<90)
						{
							game.degree++;
						}
					}
					else if(key.down)
					{
						if(game.degree>0)
						{
							game.degree--;
						}
					}
					if(key.key1)
					{
						player1.myWeapon.Select=0;
					}
					else if(key.key2)
					{
						player1.myWeapon.Select=1;
					}
					else if(key.key3)
					{
						player1.myWeapon.Select=2;
					}
				}
					
				break;
			case 1://monster
				if(enemy_move_frame==0)
				{
					MC.addgroup();
				}
				player1.addScore(MC.check());
				MC.progress();
				enemy_move_frame++;
				if(enemy_move_frame>=60)
				{
					player1.Life(MC.attack());
					enemy_move_frame=0;
					if(player1.getLife()<=0)
					{
						gameover=true;
						roundStatu=4;
					}
					else
					{
						roundStatu=0;
					}
					wind=(int) (Math.random()*21-10);
				}
				break;
			case 2://animation
				if(BC.progress())
				{
					MC.beAttacked(BC.bullets[0].getX()+BC.bullets[0].getR(), BC.bullets[0].getY()+BC.bullets[0].getR(), BC.bullets[0].getR(), BC.bullets[0].getDamage());
					BC.delete();
				}
				if(BC.index==0)
				{
					roundStatu=1;
				}
				break;
			case 4://gameover
				if(gameover_frame<=120)
				{
					gameover_frame++;
				}
				else
				{
					gameStatu=0;
					roundStatu=0;
					BC=null;
					BC=new BulletControl();
					MC=null;
					MC=new MonsterController();
					gameover=false;
					gameover_frame=0;
					board.add(player1.getName(),player1.getScore());
					audio.stop(1);
					audio.loop(0);
				}
				break;
			}
			break;
		case 2:
			if(key.space)
			{
				gameStatu=0;
			}
			break;
		default:
			break;
		}
	}
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		switch(gameStatu)
		{
		case 0://Title
			g.drawImage(Title_background, 0, 0, this.getWidth(), this.getHeight(),this);
			switch(title.getSelect())
			{
			case 0:
				g.drawImage(TitleButton[0][1], 51, 218, 212, 55,this);
				g.drawImage(TitleButton[1][0], 51, 291, 212, 55,this);
				g.drawImage(TitleButton[2][0], 51, 364, 212, 55,this);
				break;
			case 1:
				g.drawImage(TitleButton[0][0], 51, 218, 212, 55,this);
				g.drawImage(TitleButton[1][1], 51, 291, 212, 55,this);
				g.drawImage(TitleButton[2][0], 51, 364, 212, 55,this);
				break;
			case 2:
				g.drawImage(TitleButton[0][0], 51, 218, 212, 55,this);
				g.drawImage(TitleButton[1][0], 51, 291, 212, 55,this);
				g.drawImage(TitleButton[2][1], 51, 364, 212, 55,this);
				break;
			}
			break;
		case 1://Game
			g.drawImage(Game_background, 0, 0, this.getWidth(), this.getHeight(),this);
			g.drawImage(player1.getPlayerImage(player1.state),player1.getX()-50,player1.getY()-100,100,100,this);
			g.setColor(Color.yellow);
			g.setFont(new Font(null,Font.BOLD, 40));
			g.drawString(String.valueOf(game.degree), 50, 710);
			g.drawString("姓名: "+player1.getName(), 200, 640);
			g.drawString("血量: "+String.valueOf(player1.getLife()), 200, 685);
			g.setColor(Color.BLACK);
			g.drawString("分數: "+String.valueOf(player1.getScore()), 0, 30);
			g.drawString("風向: "+String.valueOf(wind), 480, 30);
			g.drawImage(tag,182+tagX,702,16,31,this);
			g.drawImage(strbar2,192,711,tempX,31,this);
			g.drawImage(strbar1,192,711,tagX,31,this);
			
			for(int i=0;i<MC.getIndex();i++)
			{
				g.drawImage(MC.monster[i].getImage(),MC.monster[i].getX()-MC.monster[i].getWidth()/2,MC.monster[i].getY()-MC.monster[i].getHeight()/2,MC.monster[i].getWidth(),MC.monster[i].getHeight(),this);
				g.setColor(Color.black);
				g.drawString(String.valueOf(MC.monster[i].getlife()), MC.monster[i].getX()+20, MC.monster[i].getY()-50);
			}
			switch(roundStatu)
			{
			case 0://player
				
				break;
			case 1://monster
				break;
			case 2://animation
				for(int i=0;i<BC.getIndex();i++)
				{
					g.drawImage(BC.bullets[i].getImage(),BC.bullets[i].getX()-BC.bullets[i].getWidth()/2,BC.bullets[i].getY()-BC.bullets[i].getHeight()/2,BC.bullets[i].getWidth(),BC.bullets[i].getHeight(),this);
				}
				break;
			}
			if(gameover)
			{
				g.drawImage(Gameover_image,400,100,282,198,this);
				g.setFont(new Font(null,Font.BOLD, 28));
				g.setColor(Color.WHITE);
				g.drawString(String.valueOf(player1.getScore()), 580, 285);
			}
			break;
		case 2:
			g.drawImage(Board_background,0,0,1024,768,this);
			g.setFont(new Font(null,Font.BOLD, 40));
			g.setColor(Color.BLACK);
			int i=0;
			while(board.getlevel(i)!=null)
			{
				g.drawString("第"+(i+1)+"名: "+board.getlevel(i)+board.getscore(i)+"分", 50, 220+i*50);
				i++;
			}
			break;
		default:
			break;
		}
	}
}
