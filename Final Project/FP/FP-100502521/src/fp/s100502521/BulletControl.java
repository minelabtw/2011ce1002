package fp.s100502521;

public class BulletControl
{
	public Bullet []bullets=new Bullet[10];
	public int index=0;
	BulletControl()
	{
	}
	public void add(Bullet a)
	{
		if(index<10)
		{
			bullets[index]=a;
			index++;
		}
	}
	public void delete()
	{
		bullets[0]=null;
		for(int i=0;i<index-1;i++)
		{
			bullets[i]=bullets[i+1];
		}
		index--;
		bullets[index]=null;
		
	}
	public int getIndex()
	{
		return index;
	}
	public boolean progress()
	{
		for(int i=0;i<index;i++)
		{
			bullets[i].flyProgress();
		}
		if(bullets[0]!=null&&bullets[0].getfall()&&bullets[0].getY()+bullets[0].getHeight()/2>550)
		{
			return true;
		}
		return false;
	}
}
