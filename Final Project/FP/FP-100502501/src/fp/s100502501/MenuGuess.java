package fp.s100502501;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class MenuGuess extends JFrame implements ActionListener{
	private JLabel bg=new JLabel(new ImageIcon("bg2.jpg"));
	private JButton jbt1=new JButton(new ImageIcon("24.png"));
	private JButton jbt2=new JButton(new ImageIcon("25.png"));
	private JButton jbt3=new JButton(new ImageIcon("2menu.png"));
	private JButton jbt4=new JButton(new ImageIcon("2exit.png"));
	public JFrame JMenuG;
	public MenuGuess(){
		JMenuG=new JFrame();
		JMenuG.setTitle("Menu");
		JMenuG.setSize(800,450);
		JMenuG.setLocationRelativeTo(null); // Center the frame   
		JMenuG.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JMenuG.setLayout(null);
		
		jbt1.setBounds(80,210,300,75);
		jbt2.setBounds(420,170,300,75);
		jbt3.setBounds(80,310,300,75);
		jbt4.setBounds(420,270,300,75);
		bg.setBounds(0,0,800,450);
		JMenuG.add(jbt1);
		JMenuG.add(jbt2);
		JMenuG.add(jbt3);
		JMenuG.add(jbt4);
		JMenuG.add(bg);
		jbt1.addActionListener(this);
		jbt2.addActionListener(this);
		jbt3.addActionListener(this);
		jbt4.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==jbt1){ //4���
			Guess g4=new Guess();
			g4.JGuess.setVisible(true);
			JMenuG.setVisible(false);
		}
		if(e.getSource()==jbt2){ //5���
			Guess5 g5=new Guess5();
			g5.JGuess5.setVisible(true);
			JMenuG.setVisible(false);
		}
		if(e.getSource()==jbt3){ //go to menu
			Menu menu=new Menu();
			menu.JMenu.setVisible( true );
			JMenuG.setVisible(false);
		}
		if(e.getSource()==jbt4){ //exit
			System.exit( 0 );
		}
	}
}
