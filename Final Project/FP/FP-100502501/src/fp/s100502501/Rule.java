package fp.s100502501;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class Rule extends JFrame implements ActionListener{
	private JLabel bg=new JLabel(new ImageIcon("background.jpg"));
	private JTextArea jta1=new JTextArea();
	private JScrollPane jsp1;
	private JTextArea jta2=new JTextArea();
	private JScrollPane jsp2;
	private JLabel lUC=new JLabel("終極密碼");
	private JLabel lG=new JLabel("猜數字");
	private JButton jtb1=new JButton(new ImageIcon("back.png"));
	private JPanel p1=new JPanel(new BorderLayout());
	private JPanel p2=new JPanel(new BorderLayout());
	public JFrame JR;
	
	public Rule(){
		JR=new JFrame();
		JR.setTitle("Rule");
		JR.setSize(400,485);
		JR.setLocationRelativeTo(null); // Center the frame   
		JR.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JR.setLayout(null);
		
		lUC.setFont(new Font("標楷體",Font.BOLD,35));
		lG.setFont(new Font("標楷體",Font.BOLD,35));
		lUC.setForeground(Color.green);
		lG.setForeground(Color.green);
		lUC.setHorizontalAlignment(SwingConstants.CENTER);
		lG.setHorizontalAlignment(SwingConstants.CENTER);
		
		jsp1=new JScrollPane(jta1);
		jsp2=new JScrollPane(jta2);
		jta1.setFont(new Font("標楷體", Font.PLAIN, 27));
	    jta1.setLineWrap(true);
	    jta1.setWrapStyleWord(true);
	    jta1.setEditable(false);
	    jta2.setFont(new Font("標楷體", Font.PLAIN,27));
	    jta2.setLineWrap(true);
	    jta2.setWrapStyleWord(true);
	    jta2.setEditable(false);
	    jta1.setForeground(new Color(64,0,128));
	    jta2.setForeground(new Color(64,0,128));
	    p1.setBounds(20,25,340,170);
	    p2.setBounds(20,205,340,170);
	    jtb1.setBounds(280,385,70,35);
	    bg.setBounds(0,0,400,450);
	    
	    jta1.setText("玩家可選擇和電腦玩或多人模式，並可自行輸入終極密碼之最大 M 和最小 m 範圍， (M-m) 須大於1。\n電腦會隨機從輸入範圍中挑選一整數作為終極密碼。由玩家和電腦(或其他玩家)輪流猜，若猜中者即為輸家。");
	    jta2.setText("玩家可選擇猜四位數或五位數，電腦會隨機選取數字不重複的該位數數字作為密碼，當猜的數字和密碼有相同的數字，若位置也相同為A，若不相同則為B。\nex.若密碼為2754，玩家猜1357，則為1A1B。");

	    p1.add(lUC,BorderLayout.NORTH);
	    p1.add(jsp1,BorderLayout.CENTER);
	    p2.add(lG,BorderLayout.NORTH);
	    p2.add(jsp2,BorderLayout.CENTER);
	    JR.add(p1);
	    JR.add(p2);
	    JR.add(jtb1);
	    JR.add(bg);
	    
	    jtb1.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==jtb1){ //back to menu
			JR.setVisible(false);
		}
	}
}
