package fp.s100502501;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class UltimateCode3p extends JFrame implements ActionListener{
	private JLabel bg=new JLabel(new ImageIcon("bg7.png"));
	private String player1="player 1",player2="player 2",player3="player 3",name1,name2,name3;
	private String in1="",in2="";
	private String guess3;
	private JLabel p1l1;
	private JLabel p1l2;
	private JLabel p1l3;
	private JLabel p2l1;
	private JTextField keyin3=new JTextField(8);
	private JButton jtb1=new JButton(new ImageIcon("u3menu.png"));
	private JButton jtb2=new JButton(new ImageIcon("u3exit.png"));
	public JFrame UC3;
	private int code3;
	private int max=100,min=1;
	private int rgmax,rgmin;
	private int num3,a=1,b=1;
	UltimateCode3p(){
		UC3=new JFrame();
		UC3.setTitle("Ultimate Code");
		UC3.setSize(650,400);
		UC3.setLocationRelativeTo(null); // Center the frame   
		UC3.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		UC3.setLayout(null);
		    
		boolean large3=true;
		boolean f1=true;
		boolean f2=true;
		while(large3==true){
			f1=true;
			while(f1==true){ //enter minimum (should be larger than 0) 
				in1=JOptionPane.showInputDialog(null,"Enter the minimum");
				for(int f=0;f<in1.length();f++){
					if(in1.charAt(f)>'9'||in1.charAt(f)<'0'){
						JOptionPane.showMessageDialog(null,"Please enter NUMBERs\n");
						f1=true;
						break;
					}
					else{
						f1=false;
					}
				}
			}
			rgmin=Integer.parseInt(in1);
			f2=true;
			while(f2==true){ //enter maximum (should be larger than minimum)
				in2=JOptionPane.showInputDialog(null,"Enter the maximum");
				for(int f=0;f<in2.length();f++){
					if(in2.charAt(f)>'9'||in2.charAt(f)<'0'){
						JOptionPane.showMessageDialog(null,"Please enter NUMBERs\n");
						f2=true;
						break;
					}
					else{
						f2=false;
					}
				}
			}
			rgmax=Integer.parseInt(in2);
			large3=true;
			if(rgmin<rgmax&&rgmax-rgmin>1){
				large3=false;
			}
			else{
				JOptionPane.showMessageDialog(null,"Error! (Maximum-Minimum)>1");
				large3=true;
			}
		}
		max=rgmax;
		min=rgmin;
		code3=(int)(Math.random()*(rgmax-rgmin-1))+rgmin+1;
		name1=JOptionPane.showInputDialog("What is player1's name ?","player 1"); //player1 enter name
		if(name1!=null){
			player1=name1;
		}
		name2=JOptionPane.showInputDialog("What is player2's name ?","player 2"); //player2 enter name
		if(name2!=null){
			player2=name2;
		}
		name3=JOptionPane.showInputDialog("What is player3's name ?","player 3"); //player3 enter name
		if(name3!=null){
			player3=name3;
		}
				
		p1l1= new JLabel(player1);
		p1l2= new JLabel(player2);
		p1l3= new JLabel(player3);
		p2l1=new JLabel(min+" �� "+max);
				
		p1l1.setFont(new Font("Calibri",Font.BOLD,30));
		p1l2.setFont(new Font("Calibri",Font.BOLD,30));
		p1l3.setFont(new Font("Calibri",Font.BOLD,30));
		p2l1.setFont(new Font("SansSerif",Font.BOLD,72));
		keyin3.setFont(new Font("SansSerif",Font.BOLD,48));
		p1l1.setForeground(Color.orange);
		p1l2.setForeground(Color.black);
		p1l3.setForeground(Color.black);
		p2l1.setForeground(Color.blue);
		keyin3.setForeground(Color.gray);
		p1l1.setHorizontalAlignment(SwingConstants.CENTER);
		p1l2.setHorizontalAlignment(SwingConstants.CENTER);
		p1l3.setHorizontalAlignment(SwingConstants.CENTER);
		p2l1.setHorizontalAlignment(SwingConstants.CENTER);
		keyin3.setHorizontalAlignment(SwingConstants.CENTER);
		
		p1l1.setBounds(0, 0, 650, 50);
		keyin3.setBounds(20, 50, 580, 100);
		p1l3.setBounds(50, 150, 270, 50);
		p1l2.setBounds(320, 150, 280, 50);
		p2l1.setBounds(20, 230, 450, 100);
		jtb1.setBounds(520, 200, 70, 70);
		jtb2.setBounds(520, 280, 70, 70);
		bg.setBounds(0,0,650,400);
		
		UC3.add(keyin3);
		UC3.add(p1l1);
		UC3.add(p1l2);
		UC3.add(p1l3);
		UC3.add(p2l1);
		UC3.add(jtb1);
		UC3.add(jtb2);
		UC3.add(bg);
		keyin3.addActionListener(this);
		jtb1.addActionListener(this);
		jtb2.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e) {
		boolean fg3=true;
		if(e.getSource()==keyin3&&b==1){
			guess3=keyin3.getText();
			for(int f=0;f<guess3.length();f++){ //tell the range after entering the guess number  
				if(guess3.charAt(f)>'9'||guess3.charAt(f)<'0'){ //the guess number should be in the range
					JOptionPane.showMessageDialog(null,"Please enter NUMBERs\n");
					keyin3.setText("");
					fg3=false;
					break;
				}
				else
					fg3=true;
			}
			if(fg3==true){
				if(Integer.parseInt(guess3)>min&&Integer.parseInt(guess3)<max)
					num3=Integer.parseInt(guess3);
				else{
					JOptionPane.showMessageDialog(null,"Wrong number\n");
					keyin3.setText("");
				}
				if(num3!=code3){
					if(num3<max&&num3>min){
						if(num3>code3){
							max=num3;
							p2l1.setText(min+" �� "+max+"\n");
							keyin3.setText("");
						}
						else if(num3<code3){
							min=num3;
							p2l1.setText(min+" �� "+max+"\n");
							keyin3.setText("");
						}
					}
					else{
						JOptionPane.showMessageDialog(null,"Please enter between "+min+" �� "+max+"\n");
						keyin3.setText("");
						a--;
					}
					a++;
					if(a%3==1){ //judge who's turn
						p1l1.setForeground(Color.orange);
						p1l3.setForeground(Color.black);
					}
					else if(a%3==2){
						p1l1.setForeground(Color.black);
						p1l2.setForeground(Color.orange);
					}
					else{
						p1l2.setForeground(Color.black);
						p1l3.setForeground(Color.orange);
					}
				}
				else{ //judge who guess the ultimate code
					if(a%3==1){
						p2l1.setText("  BOMB!!! ");
						p2l1.setIcon(new ImageIcon("loser1.jpg"));
						p2l1.setForeground(Color.RED);
						p1l1.setForeground(Color.orange);
						p1l2.setForeground(Color.black);
						JOptionPane.showMessageDialog(null,"             "+player1+"  lose !");
						keyin3.setText(guess3);
						b=0;
					}
					else if(a%3==2){
						p2l1.setText("  BOMB!!! ");
						p2l1.setIcon(new ImageIcon("loser1.jpg"));
						p2l1.setForeground(Color.RED);
						p1l2.setForeground(Color.orange);
						p1l3.setForeground(Color.black);
						JOptionPane.showMessageDialog(null,"             "+player2+"  lose !");
						keyin3.setText(guess3);
						b=0;
					}
					else{
						p2l1.setText("  BOMB!!! ");
						p2l1.setIcon(new ImageIcon("loser1.jpg"));
						p2l1.setForeground(Color.RED);
						p1l3.setForeground(Color.orange);
						p1l1.setForeground(Color.black);
						JOptionPane.showMessageDialog(null,"             "+player3+"  lose !");
						keyin3.setText(guess3);
						b=0;
					}
				}
			}
		}
		if(e.getSource()==jtb1){ //go to menu
			Menu menu=new Menu();
			menu.JMenu.setVisible( true );
			UC3.setVisible(false);
		}
		if(e.getSource()==jtb2){ //exit
			System.exit(0);
		}
	}
}
