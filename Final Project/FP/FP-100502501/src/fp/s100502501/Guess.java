package fp.s100502501;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
public class Guess extends JFrame implements ActionListener{
	static private int[] number=new int[4];
	private JLabel bg=new JLabel(new ImageIcon("bg1.jpg"));
	private String guess="";
	private JTextField keyin=new JTextField(8);
	private JLabel p11l1=new JLabel();
	private JLabel p11l2=new JLabel();
	private JLabel p12l1;
	private JLabel p3l1=new JLabel();
	private JPanel p1=new JPanel(new GridLayout(3,1));
	private JPanel p11=new JPanel(new GridLayout(1,2));
	private JPanel p12=new JPanel(new BorderLayout());
	private JPanel p31=new JPanel(new GridLayout(2,1,20,20));
	private JPanel p3=new JPanel(new GridLayout(2,1,20,20));
	private JButton jtb1=new JButton(new ImageIcon("3ans.png"));
	private JButton jtb2=new JButton(new ImageIcon("4menu.png"));
	private JButton jtb3=new JButton(new ImageIcon("4exit.png"));
	public JFrame JGuess;
	private static int A=0;
	private static int B=0;
	private int a=1,b=1,c=1;
	private JTextArea jta=new JTextArea();
	private JScrollPane jsp;
	public Guess(){
		JGuess=new JFrame();
		JGuess.setTitle("Guess numbers");
		JGuess.setSize(800,360);
		JGuess.setLocationRelativeTo(null); // Center the frame   
		JGuess.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JGuess.setLayout(null);
		
		jsp=new JScrollPane(jta);
		p12l1=new JLabel(new ImageIcon("uguess1.png"));
		p12l1.setFont(new Font("Calibri",Font.BOLD,32));
		p12l1.setForeground(new Color(20,200,0));
		jta.setFont(new Font("Calibri",Font.BOLD,30));
		jta.setForeground(new Color(128,128,255));
		
		Random rdm=new Random(); //generate an 4-digit number
		number[0]=rdm.nextInt(10);
		for(int i=1;i<4;i++){
			number[i]=rdm.nextInt(10);
			for(int j=0;j<i;j++){
				if(number[j]==number[i]){
					i--;
					break;
				}
			}
		}
		keyin.setHorizontalAlignment(SwingConstants.CENTER);
		keyin.setForeground(Color.blue);
		keyin.setFont(new Font("SansSerif",Font.BOLD,36));
		
		jtb1.setBounds(130, 210, 150, 65);
		jtb2.setBounds(530, 255, 94, 50);
		jtb3.setBounds(650, 255, 94, 50);
		jsp.setBounds(410, 25, 340, 200);
		p12l1.setBounds(20, 25, 250, 70);
		keyin.setBounds(35, 90, 330, 80);
		bg.setBounds(0,0,800,400);
		
		JGuess.add(jsp);
		JGuess.add(keyin);
		JGuess.add(p12l1);
		JGuess.add(jtb1);
		JGuess.add(jtb2);
		JGuess.add(jtb3);
		JGuess.add(bg);
		keyin.addActionListener(this);
		jtb1.addActionListener(this);
		jtb2.addActionListener(this);
		jtb3.addActionListener(this);
		/*for(int i=0;i<4;i++){
			System.out.print(number[i]);
		}
		System.out.print("\n");*/
		
	}
	public static void Judge(String guess1){
		for(int i=0;i<4;i++){ //judge _A_B
			for(int j=0;j<4;j++){ 
				if(number[i]==guess1.charAt(j)-'0'){
					if(i==j){
						A++;
					}
					else
						B++;
				}
			}
		}
	}
	public void actionPerformed(ActionEvent e) {
		boolean flag=true;
		if(e.getSource()==keyin&&b==1){
			A=0;
			B=0;
			guess=keyin.getText();
			for(int k=0;k<guess.length();k++){ //number should be number
				if(guess.charAt(k)>'9'||guess.charAt(k)<'0'){ 
					JOptionPane.showMessageDialog(null,"Please enter NUMBERs\n");
					keyin.setText("");
					flag=false;
					break;
				}
			}
			if(flag==true){
				if (guess.length()!=4){ //number should be 4-digit
					JOptionPane.showMessageDialog(null,"Please enter 4 numbers\n");
					keyin.setText("");
				}
				
				else{ //number shouldn't repeat
					if(guess.charAt(0)==guess.charAt(1)||guess.charAt(0)==guess.charAt(2)||guess.charAt(0)==guess.charAt(3)||
					   guess.charAt(1)==guess.charAt(2)||guess.charAt(1)==guess.charAt(3)||guess.charAt(2)==guess.charAt(3)){
						JOptionPane.showMessageDialog(null,"Please do not enter the same number!\n");
						keyin.setText("");
					}		
					else{
						Judge(guess);
						if(A==4){
							JOptionPane.showMessageDialog(null,"                 BINGO!!!");
							if(a<10)
								jta.append(a+".        "+guess+"\t   ");
							else if((a>=10&&a<100))
								jta.append(a+".      "+guess+"\t   ");
							else
								jta.append(a+".    "+guess+"\t   ");
							jta.append(A+"A"+B+"B"+"      "+"\n");
							jta.append("               BINGO!!!");
							jta.setForeground(Color.orange);
							keyin.setText("");
							b=0;
							c=0;
						}
						else{
							if(a<10)
								jta.append(a+".        "+guess+"\t   ");
							else if((a>=10&&a<100))
								jta.append(a+".      "+guess+"\t   ");
							else
								jta.append(a+".    "+guess+"\t   ");
							jta.append(A+"A"+B+"B"+"      "+"\n");
							keyin.setText("");
						}
						a++;
					}
				}			
			}
		}
		if(e.getSource()==jtb1){ //see the answer
			b=0;
			p12l1.setIcon(new ImageIcon("uanswer.png"));
			keyin.setFont(new Font("Calibri",Font.BOLD,84));
			keyin.setText(Integer.toString(number[0])+Integer.toString(number[1])+Integer.toString(number[2])+Integer.toString(number[3]));
			keyin.setForeground(Color.red);
			
			if(c==1){
				c=0;				
				jta.setForeground(new Color(0,128,255));				
			}
			if(c==1){
				jta.append("       "+Integer.toString(number[0])+Integer.toString(number[1])+Integer.toString(number[2])+Integer.toString(number[3]));
			}
			
		}
		if(e.getSource()==jtb2){ //go to menu
			Menu menu=new Menu();
			menu.JMenu.setVisible( true );
			JGuess.setVisible(false);
		}
		if(e.getSource()==jtb3){ //exit
			System.exit(0);
		}
	}
}