package fp.s100502501;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class MenuUC extends JFrame implements ActionListener{
	private JLabel bg=new JLabel(new ImageIcon("menu23.jpg"));
	private JButton jbt1=new JButton(new ImageIcon("1vs.png"));
	private JButton jbt2=new JButton(new ImageIcon("12P.png"));
	private JButton jbt3=new JButton(new ImageIcon("13P.png"));
	private JButton jbt4=new JButton(new ImageIcon("14P.png"));
	private JButton jbt5=new JButton(new ImageIcon("1menu.png"));
	private JButton jbt6=new JButton(new ImageIcon("1exit.png"));
	public JFrame JMenuUC;
	public MenuUC(){
		JMenuUC=new JFrame();
		JMenuUC.setTitle("Ultimate Code");
		JMenuUC.setSize(800,650);
		JMenuUC.setLocationRelativeTo(null); // Center the frame   
		JMenuUC.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JMenuUC.setLayout(null);
		
		jbt1.setBounds(80,250,300,75);
		jbt2.setBounds(420,210,300,75);
		jbt3.setBounds(80,350,300,75);
		jbt4.setBounds(420,310,300,75);
		jbt5.setBounds(80,450,300,75);
		jbt6.setBounds(420,410,300,75);
		bg.setBounds(0,0,800,650);
		JMenuUC.add(jbt1);
		JMenuUC.add(jbt2);
		JMenuUC.add(jbt3);
		JMenuUC.add(jbt4);
		JMenuUC.add(jbt5);
		JMenuUC.add(jbt6);
		JMenuUC.add(bg);
		jbt1.addActionListener(this);
		jbt2.addActionListener(this);
		jbt3.addActionListener(this);
		jbt4.addActionListener(this);
		jbt5.addActionListener(this);
		jbt6.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==jbt1){ //play with computer
			UltimateCode uc1=new  UltimateCode();
			uc1.UC1.setVisible(true);
			JMenuUC.setVisible(false);
		}
		if(e.getSource()==jbt2){ //2p mode
			UltimateCode2p uc2=new  UltimateCode2p();
			uc2.UC2.setVisible(true);
			JMenuUC.setVisible(false);
		}
		if(e.getSource()==jbt3){ //3p mode
			UltimateCode3p uc3=new  UltimateCode3p();
			uc3.UC3.setVisible(true);
			JMenuUC.setVisible(false);
		}
		if(e.getSource()==jbt4){ //4p mode
			UltimateCode4p uc4=new  UltimateCode4p();
			uc4.UC4.setVisible(true);
			JMenuUC.setVisible(false);
		}
		if(e.getSource()==jbt5){ //go to menu
			Menu menu=new Menu();
			menu.JMenu.setVisible( true );
			JMenuUC.setVisible(false);
		}
		if(e.getSource()==jbt6){ //exit
			System.exit( 0 );		
		}
	}
}
