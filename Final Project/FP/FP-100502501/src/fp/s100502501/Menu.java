package fp.s100502501;
import java.awt.event.*;
import javax.swing.*;
public class Menu extends JFrame implements ActionListener{
	private JLabel bg=new JLabel(new ImageIcon("menu1.jpg"));
	private JButton jbt1=new JButton(new ImageIcon("0uc.png")); //create button
	private JButton jbt2=new JButton(new ImageIcon("0g.png"));
	private JButton jbt3=new JButton(new ImageIcon("0rule.png"));
	private JButton jbt4=new JButton(new ImageIcon("0exit.png"));
	public JFrame JMenu;
	public Menu(){
		JMenu=new JFrame();
		JMenu.setTitle("Menu");
		JMenu.setSize(800,650);
		JMenu.setLocationRelativeTo(null); // Center the frame   
		JMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
		JMenu.setLayout(null);
		
		jbt1.setBounds(265,200,270,75); //set position
		jbt2.setBounds(265,300,270,75);
		jbt3.setBounds(265,400,270,75);
		jbt4.setBounds(265,500,270,75);
		bg.setBounds(0,0,800,650);
		JMenu.add(jbt1); //add component to frame
		JMenu.add(jbt2);
		JMenu.add(jbt3);
		JMenu.add(jbt4);
		JMenu.add(bg);
		jbt1.addActionListener(this);
		jbt2.addActionListener(this);
		jbt3.addActionListener(this);
		jbt4.addActionListener(this);
		
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==jbt1){ //go to ultimate code's menu
			MenuUC menuUC=new MenuUC();
			menuUC.JMenuUC.setVisible(true);
			JMenu.setVisible(false);
		}
		if(e.getSource()==jbt2){ //go to guess number's menu
			MenuGuess menuG=new MenuGuess();
			menuG.JMenuG.setVisible(true);
			JMenu.setVisible(false);
			
		}
		if(e.getSource()==jbt3){ //see the rule
			Rule rl=new Rule();
			rl.JR.setVisible(true);
		}
		if(e.getSource()==jbt4){ //exit
			System.exit( 0 );		
		}
	}
}
