package fp.s100502501;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class UltimateCode4p extends JFrame implements ActionListener{
	private JLabel bg=new JLabel(new ImageIcon("bg7.png"));
	private String player1,player2="player 2",player3,player4,name1,name2,name3,name4;
	private String in1="",in2="";
	private String guess4;
	private JLabel p1l1;
	private JLabel p1l2;
	private JLabel p1l3;
	private JLabel p1l4;
	private JLabel p2l1;
	private JTextField keyin4=new JTextField(8);
	public JFrame UC4;
	private int code4;
	private int max=100,min=1;
	private int rgmax,rgmin;
	private int num4,a=1,b=1;
	private JButton jtb1=new JButton(new ImageIcon("u3menu.png"));
	private JButton jtb2=new JButton(new ImageIcon("u3exit.png"));
	
	public UltimateCode4p(){
		UC4=new JFrame();
		UC4.setTitle("Ultimate Code");
		UC4.setSize(650,400);
		UC4.setLocationRelativeTo(null); // Center the frame   
		UC4.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		UC4.setLayout(null);
		
		boolean large4=true;
		boolean f1=true;
		boolean f2=true;
		while(large4==true){
			f1=true;
			while(f1==true){ //enter minimum (should be larger than 0) 
				in1=JOptionPane.showInputDialog(null,"Enter the minimum");
				for(int f=0;f<in1.length();f++){
					if(in1.charAt(f)>'9'||in1.charAt(f)<'0'){
						JOptionPane.showMessageDialog(null,"Please enter NUMBERs\n");
						f1=true;
						break;
					}
					else{
						f1=false;
					}
				}
			}
			rgmin=Integer.parseInt(in1);
			f2=true;
			while(f2==true){ //enter maximum (should be larger than minimum)
				in2=JOptionPane.showInputDialog(null,"Enter the maximum");
				for(int f=0;f<in2.length();f++){
					if(in2.charAt(f)>'9'||in2.charAt(f)<'0'){
						JOptionPane.showMessageDialog(null,"Please enter NUMBERs\n");
						f2=true;
						break;
					}
					else{
						f2=false;
					}
				}
			}
			rgmax=Integer.parseInt(in2);
			large4=true;
			if(rgmin<rgmax&&rgmax-rgmin>1){
				large4=false;
			}
			else{
				JOptionPane.showMessageDialog(null,"Error! (Maximum-Minimum)>1");
				large4=true;
			}
		}
		max=rgmax;
		min=rgmin;
		code4=(int)(Math.random()*(rgmax-rgmin-1))+rgmin+1;
		
		name1=JOptionPane.showInputDialog("What is player1's name ?","player 1"); //player1 enter name
		if(name1!=null){
			player1=name1;
		}
		name2=JOptionPane.showInputDialog("What is player2's name ?","player 2"); //player2 enter name
		if(name2!=null){
			player2=name2;
		}
		name3=JOptionPane.showInputDialog("What is player3's name ?","player 3"); //player3 enter name
		if(name3!=null){
			player3=name3;
		}
		name4=JOptionPane.showInputDialog("What is player4's name ?","player 4"); //player4 enter name
		if(name4!=null){
			player4=name4;
		}
		
		p1l1=new JLabel(player1);
		p1l2=new JLabel(player2);
		p1l3=new JLabel(player3);
		p1l4=new JLabel(player4);
		p2l1=new JLabel(+min+" �� "+max);
		
		p1l1.setFont(new Font("Calibri",Font.BOLD,30));
		p1l2.setFont(new Font("Calibri",Font.BOLD,30));
		p1l3.setFont(new Font("Calibri",Font.BOLD,30));
		p1l4.setFont(new Font("Calibri",Font.BOLD,30));
		p2l1.setFont(new Font("SansSerif",Font.BOLD,72));
		keyin4.setFont(new Font("SansSerif",Font.BOLD,48));
		p1l1.setForeground(Color.orange);
		p1l2.setForeground(Color.black);
		p1l3.setForeground(Color.black);
		p1l4.setForeground(Color.black);
		p2l1.setForeground(Color.blue);
		keyin4.setForeground(Color.gray);
		p1l1.setHorizontalAlignment(SwingConstants.CENTER);
		p1l2.setHorizontalAlignment(SwingConstants.CENTER);
		p1l3.setHorizontalAlignment(SwingConstants.CENTER);
		p1l4.setHorizontalAlignment(SwingConstants.CENTER);
		p2l1.setHorizontalAlignment(SwingConstants.CENTER);
		keyin4.setHorizontalAlignment(SwingConstants.CENTER);
		
		p1l1.setBounds(35, 0, 280, 50);
		p1l2.setBounds(320, 0, 280, 50);
		keyin4.setBounds(20, 50, 580, 100);
		p1l4.setBounds(35, 150, 280, 50);
		p1l3.setBounds(320, 150, 280, 50);
		p2l1.setBounds(20, 230, 450, 100);
		jtb1.setBounds(520, 200, 70, 70);
		jtb2.setBounds(520, 280, 70, 70);
		bg.setBounds(0,0,650,400);
		
		UC4.add(keyin4);
		UC4.add(p1l1);
		UC4.add(p1l2);
		UC4.add(p1l3);
		UC4.add(p1l4);
		UC4.add(p2l1);
		UC4.add(jtb1);
		UC4.add(jtb2);
		UC4.add(bg);
		keyin4.addActionListener(this);
		jtb1.addActionListener(this);
		jtb2.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e) {
		boolean fg4=true;
		if(e.getSource()==keyin4&&b==1){
			guess4=keyin4.getText();
			for(int f=0;f<guess4.length();f++){ //tell the range after entering the guess number  
				if(guess4.charAt(f)>'9'||guess4.charAt(f)<'0'){ //the guess number should be in the range
					JOptionPane.showMessageDialog(null,"Please enter NUMBERs\n");
					keyin4.setText("");
					fg4=false;
					break;
				}
				else
					fg4=true;
			}
			if(fg4==true){
				if(Integer.parseInt(guess4)>min&&Integer.parseInt(guess4)<max)
					num4=Integer.parseInt(guess4);
				else{
					JOptionPane.showMessageDialog(null,"Wrong number\n");
					keyin4.setText("");
				}
				if(num4!=code4){
					if(num4<max&&num4>min){
						if(num4>code4){
							max=num4;
							p2l1.setText(min+" �� "+max+"\n");
							keyin4.setText("");
						}
						else if(num4<code4){
							min=num4;
							p2l1.setText(min+" �� "+max+"\n");
							keyin4.setText("");
						}
					}
					else{
						JOptionPane.showMessageDialog(null,"Please enter between "+min+" �� "+max+"\n");
						keyin4.setText("");
						a--;
					}
					a++;
					if(a%4==1){ //judge who's turn
						p1l1.setForeground(Color.orange);
						p1l4.setForeground(Color.black);
					}
					else if(a%4==2){
						p1l1.setForeground(Color.black);
						p1l2.setForeground(Color.orange);
					}
					else if(a%4==3){;
						p1l2.setForeground(Color.black);
						p1l3.setForeground(Color.orange);
					}
					else{
						p1l3.setForeground(Color.black);
						p1l4.setForeground(Color.orange);
					}
				}
				else{ //judge who guess the ultimate code
					if(a%4==1){
						p2l1.setText("  BOMB!!! ");
						p2l1.setIcon(new ImageIcon("loser1.jpg"));
						p2l1.setForeground(Color.RED);
						p1l1.setForeground(Color.orange);
						p1l2.setForeground(Color.black);
						JOptionPane.showMessageDialog(null,"             "+player1+"  lose !");
						keyin4.setText(guess4);
						b=0;
					}
					else if(a%4==2){
						p2l1.setText("  BOMB!!! ");
						p2l1.setIcon(new ImageIcon("loser1.jpg"));
						p2l1.setForeground(Color.RED);
						p1l2.setForeground(Color.orange);
						p1l3.setForeground(Color.black);
						JOptionPane.showMessageDialog(null,"             "+player2+"  lose !");
						keyin4.setText(guess4);
						b=0;
					}
					else if(a%4==3){
						p2l1.setText("  BOMB!!! ");
						p2l1.setIcon(new ImageIcon("loser1.jpg"));
						p2l1.setForeground(Color.RED);
						p1l3.setForeground(Color.orange);
						p1l4.setForeground(Color.black);
						JOptionPane.showMessageDialog(null,"             "+player3+"  lose !");
						keyin4.setText(guess4);
						b=0;
					}
					else{
						p2l1.setText("  BOMB!!! ");
						p2l1.setIcon(new ImageIcon("loser1.jpg"));
						p2l1.setForeground(Color.RED);
						p1l4.setForeground(Color.orange);
						p1l1.setForeground(Color.black);
						JOptionPane.showMessageDialog(null,"             "+player4+"  lose !");
						keyin4.setText(guess4);
						b=0;
					}
				}
			}
		}
		if(e.getSource()==jtb1){ //go to menu
			Menu menu=new Menu();
			menu.JMenu.setVisible( true );
			UC4.setVisible(false);
		}
		if(e.getSource()==jtb2){ //exit
			System.exit(0);
		}
	}
}
