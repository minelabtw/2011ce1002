package fp.s100502501;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class UltimateCode extends JFrame implements ActionListener{ //v.s. Computer
	private JLabel bg=new JLabel(new ImageIcon("bg7.png"));
	private String player1="player 1",name1,guess;
	private String in1="",in2="";
	private JLabel p1jlb1;
	private JLabel p1jlb2;
	private JLabel p2jlb1n1;
	private JLabel p2jlb1n2=new JLabel("Computer");
	private JLabel p2jlb2;
	private JButton jtb1=new JButton(new ImageIcon("u4menu.png"));
	private JButton jtb2=new JButton(new ImageIcon("u4exit.png"));
	public JFrame UC1;
	private int code1;
	private int max,min;
	private int rgmax=0,rgmin=0;
	private int num1,b=1;
	private JTextField keyin1=new JTextField(8);
	
	public UltimateCode(){
		UC1=new JFrame();
		UC1.setTitle("Ultimate Code");
		UC1.setSize(700,400);
		UC1.setLocationRelativeTo(null); // Center the frame   
		UC1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		UC1.setLayout(null);
		
		boolean large1=true;
		boolean f1=true;
		boolean f2=true;
		while(large1==true){
			f1=true;
			while(f1==true){ //enter minimum (should be larger than 0) 
				in1=JOptionPane.showInputDialog(null,"Enter the minimum");
				for(int f=0;f<in1.length();f++){
					if(in1.charAt(f)>'9'||in1.charAt(f)<'0'){
						JOptionPane.showMessageDialog(null,"Please enter NUMBERs\n");
						f1=true;
						break;
					}
					else{
						f1=false;
					}
				}
			}
			rgmin=Integer.parseInt(in1);
			f2=true;
			while(f2==true){ //enter maximum (should be larger than minimum)
				in2=JOptionPane.showInputDialog(null,"Enter the maximum");
				for(int f=0;f<in2.length();f++){
					if(in2.charAt(f)>'9'||in2.charAt(f)<'0'){
						JOptionPane.showMessageDialog(null,"Please enter NUMBERs\n");
						f2=true;
						break;
					}
					else{
						f2=false;
					}
				}
			}
			rgmax=Integer.parseInt(in2);
			large1=true;
			if(rgmin<rgmax&&rgmax-rgmin>1){
				large1=false;
			}
			else{
				JOptionPane.showMessageDialog(null,"Error! (Maximum-Minimum)>1");
				large1=true;
			}
		}
		max=rgmax;
		min=rgmin;
		code1=(int)(Math.random()*(rgmax-rgmin-1))+rgmin+1; //computer chooses a number between max & min randomly
		name1=JOptionPane.showInputDialog("What's your name ?","player 1"); //player enter name
		if(name1!=null){
			player1=name1;
		}
		p2jlb1n1= new JLabel("      "+player1);
		p2jlb2= new JLabel(min+" �� "+max);
		p1jlb1=new JLabel("Hi ! "+player1);
		p1jlb2=new JLabel("    Guess the number ");
				
		p2jlb1n1.setForeground(Color.orange);
		p2jlb1n2.setForeground(Color.black);
		p1jlb1.setForeground(new Color(15,180,50));
		p1jlb2.setForeground(new Color(15,20,255));
		p2jlb2.setForeground(new Color(0,50,255));
		keyin1.setForeground(Color.gray);
		p1jlb1.setFont(new Font("Calibri",Font.BOLD,30));
		p2jlb1n1.setFont(new Font("Calibri",Font.BOLD,30));
		p2jlb1n2.setFont(new Font("Calibri",Font.BOLD,30));
		p1jlb2.setFont(new Font("Calibri",Font.BOLD,30));
		keyin1.setFont(new Font("SansSerif",Font.BOLD,50));
		p2jlb2.setFont(new Font("SansSerif",Font.BOLD,72));
		keyin1.setHorizontalAlignment(SwingConstants.CENTER);
		p2jlb1n1.setHorizontalAlignment(SwingConstants.CENTER);
		p2jlb1n2.setHorizontalAlignment(SwingConstants.CENTER);
		p2jlb2.setHorizontalAlignment(SwingConstants.CENTER);
		
		p1jlb1.setBounds(10, 0, 400,35);
		p1jlb2.setBounds(20, 35, 400, 35);
		keyin1.setBounds(20, 70, 630, 100);
		p2jlb1n1.setBounds(20, 170, 360, 50);
		p2jlb1n2.setBounds(420, 170, 200, 50);
		p2jlb2.setBounds(0, 220, 530, 100);
		jtb1.setBounds(530, 225, 110, 54);
		jtb2.setBounds(530, 290, 110, 54);
		bg.setBounds(0,0,700,400);
		
		
		UC1.add(p1jlb1);
		UC1.add(p1jlb2);
		UC1.add(keyin1);
		UC1.add(p2jlb1n1);
		UC1.add(p2jlb1n2);
		UC1.add(p2jlb2);
		UC1.add(jtb1);
		UC1.add(jtb2);
		UC1.add(bg);
		
		keyin1.addActionListener(this);
		jtb1.addActionListener(this);
		jtb2.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e) {
		boolean fg=true;
		int pcguess=0;
		if(e.getSource()==keyin1&&b==1){
			guess=keyin1.getText();
			p2jlb1n1.setForeground(Color.ORANGE);
			p2jlb1n2.setForeground(Color.black);
			for(int f=0;f<guess.length();f++){ //tell the range after entering the guess number  
				if(guess.charAt(f)>'9'||guess.charAt(f)<'0'){ //the guess number should be in the range
					JOptionPane.showMessageDialog(null,"Please enter NUMBERs\n");
					keyin1.setText("");
					fg=false;
					break;
				}
				else
					fg=true;
			}
			if(fg==true){
				if(Integer.parseInt(guess)>min&&Integer.parseInt(guess)<max)
					num1=Integer.parseInt(guess);
				else{
					JOptionPane.showMessageDialog(null,"Wrong number\n");
					keyin1.setText("");
				}
				if(num1!=code1){
					if(num1<max&&num1>min){
						if(num1>code1){
							max=num1;
							p2jlb2.setText(min+" �� "+max+"\n");
							keyin1.setText("");
						}
						else if(num1<code1){
							min=num1;
							p2jlb2.setText(min+" �� "+max+"\n");
							keyin1.setText("");
						}
						pcguess=(int)(Math.random()*(max-min-1))+min+1; //computer guess the number
						p2jlb1n2.setForeground(Color.ORANGE);
						p2jlb1n1.setForeground(Color.black);
						keyin1.setText(Integer.toString(pcguess));
						JOptionPane.showMessageDialog(null,"Turn to Computer");
						p2jlb1n1.setForeground(Color.ORANGE);
						p2jlb1n2.setForeground(Color.black);
						if(pcguess!=code1){
							if(pcguess>code1){
								max=pcguess;
								p2jlb2.setText(min+" �� "+max+"\n");
								keyin1.setText("");
							}
							else if(pcguess<code1){
								min=pcguess;
								p2jlb2.setText(min+" �� "+max+"\n");
								keyin1.setText("");
							}
						}
						else{ //computer bomb
							p2jlb2.setText("BOMB!!!");
							p2jlb2.setForeground(Color.red);
							p2jlb1n2.setForeground(Color.ORANGE);
							p2jlb1n1.setForeground(Color.black);
							JOptionPane.showMessageDialog(null,"             You  win !");
							b=0;
							keyin1.setText(Integer.toString(pcguess));
						}
					}				
					else{
						JOptionPane.showMessageDialog(null,"Please enter between "+min+" �� "+max+"\n");
						keyin1.setText("");
					}
				}
				else{ //player bomb
					p2jlb2.setText("  BOMB!!!");
					p2jlb2.setIcon(new ImageIcon("loser1.jpg"));
					p2jlb2.setForeground(Color.RED);
					JOptionPane.showMessageDialog(null,"             You  lose !");
					b=0;
				}
			}
		}
		if(e.getSource()==jtb1){ //go to menu
			Menu menu=new Menu();
			menu.JMenu.setVisible( true );
			UC1.setVisible(false);
		}
		if(e.getSource()==jtb2){ //exit
			System.exit(0);
		}
	}
}