package fp.s100502501;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class UltimateCode2p extends JFrame implements ActionListener{
	private JLabel bg=new JLabel(new ImageIcon("bg7.png"));
	private String player1="player 1",player2="player 2",name1,name2;
	private String in1="",in2="";
	private String guess2;
	private JLabel p1l1;
	private JLabel p1l2;
	private JLabel p2l1;
	private JTextField keyin2=new JTextField(8);
	private JButton jtb1=new JButton(new ImageIcon("u3menu.png"));
	private JButton jtb2=new JButton(new ImageIcon("u3exit.png"));
	public JFrame UC2;
	private int code2;
	private int max=100,min=1;
	private int rgmax,rgmin;
	private int num1,a=1,b=1;
	
	public UltimateCode2p(){
		UC2=new JFrame();
		UC2.setTitle("Ultimate Code");
		UC2.setSize(650,400);
		UC2.setLocationRelativeTo(null); // Center the frame   
		UC2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		UC2.setLayout(null);
		
		boolean large2=true;
		boolean f1=true;
		boolean f2=true;
		while(large2==true){
			f1=true;
			while(f1==true){ //enter minimum (should be larger than 0) 
				in1=JOptionPane.showInputDialog(null,"Enter the minimum");
				for(int f=0;f<in1.length();f++){
					if(in1.charAt(f)>'9'||in1.charAt(f)<'0'){
						JOptionPane.showMessageDialog(null,"Please enter NUMBERs\n");
						f1=true;
						break;
					}
					else{
						f1=false;
					}
				}
			}
			rgmin=Integer.parseInt(in1);
			f2=true;
			while(f2==true){ //enter maximum (should be larger than minimum)
				in2=JOptionPane.showInputDialog(null,"Enter the maximum");
				for(int f=0;f<in2.length();f++){
					if(in2.charAt(f)>'9'||in2.charAt(f)<'0'){
						JOptionPane.showMessageDialog(null,"Please enter NUMBERs\n");
						f2=true;
						break;
					}
					else{
						f2=false;
					}
				}
			}
			rgmax=Integer.parseInt(in2);
			large2=true;
			if(rgmin<rgmax&&rgmax-rgmin>1){
				large2=false;
			}
			else{
				JOptionPane.showMessageDialog(null,"Error! (Maximum-Minimum)>1");
				large2=true;
			}
		}
		max=rgmax;
		min=rgmin;
		code2=(int)(Math.random()*(rgmax-rgmin-1))+rgmin+1;  //computer chooses a number between max & min randomly
		
		name1=JOptionPane.showInputDialog("What is player1's name ?","player 1"); //player1 enter name
		if(name1!=null){
			player1=name1;
		}
		name2=JOptionPane.showInputDialog("What is player2's name ?","player 2"); //player2 enter name
		if(name2!=null){
			player2=name2;
		}
				
		p1l1= new JLabel(player1);
		p1l2= new JLabel(player2);
		p2l1=new JLabel(min+" �� "+max);
		
		p1l1.setFont(new Font("Calibri",Font.BOLD,30));
		p1l2.setFont(new Font("Calibri",Font.BOLD,30));
		p2l1.setFont(new Font("SansSerif",Font.BOLD,72));
		keyin2.setFont(new Font("SansSerif",Font.BOLD,48));
		p1l1.setForeground(Color.orange);
		p1l2.setForeground(Color.black);
		p2l1.setForeground(Color.blue);
		keyin2.setForeground(Color.gray);
		p1l1.setHorizontalAlignment(SwingConstants.CENTER);
		p1l2.setHorizontalAlignment(SwingConstants.CENTER);
		p2l1.setHorizontalAlignment(SwingConstants.CENTER);
		keyin2.setHorizontalAlignment(SwingConstants.CENTER);
		
		p1l1.setBounds(0, 0, 650, 50);
		keyin2.setBounds(20, 50, 580, 100);
		p1l2.setBounds(0, 150, 650, 50);
		p2l1.setBounds(20, 230, 450, 100);
		jtb1.setBounds(520, 200, 70, 70);
		jtb2.setBounds(520, 280, 70, 70);
		bg.setBounds(0,0,650,400);
		
		UC2.add(keyin2);
		UC2.add(p1l1);
		UC2.add(p1l2);
		UC2.add(p2l1);
		UC2.add(jtb1);
		UC2.add(jtb2);
		UC2.add(bg);
		keyin2.addActionListener(this);
		jtb1.addActionListener(this);
		jtb2.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e) {
		boolean fg2=true;
		if(e.getSource()==keyin2&&b==1){
			guess2=keyin2.getText();
			for(int f=0;f<guess2.length();f++){ //tell the range after entering the guess number  
				if(guess2.charAt(f)>'9'||guess2.charAt(f)<'0'){ //the guess number should be in the range
					JOptionPane.showMessageDialog(null,"Please enter NUMBERs\n");
					keyin2.setText("");
					fg2=false;
					break;
				}
				else
					fg2=true;
			}
			if(fg2==true){
				if(Integer.parseInt(guess2)>min&&Integer.parseInt(guess2)<max)
					num1=Integer.parseInt(guess2);
				else{
					JOptionPane.showMessageDialog(null,"Wrong number\n");
					keyin2.setText("");
				}
				if(num1!=code2){
					if(num1<max&&num1>min){
						if(num1>code2){
							max=num1;
							p2l1.setText(min+" �� "+max+"\n");
							keyin2.setText("");
						}
						else if(num1<code2){
							min=num1;
							p2l1.setText(min+" �� "+max+"\n");
							keyin2.setText("");
						}
					}
					else{
						JOptionPane.showMessageDialog(null,"Please enter between "+min+" �� "+max+"\n");
						keyin2.setText("");
						a--;
					}
					a++;
					if(a%2==1){ //judge who's turn
						p1l1.setForeground(Color.orange);
						p1l2.setForeground(Color.black);
					}
					else{
						p1l1.setForeground(Color.black);
						p1l2.setForeground(Color.orange);
					}
					
				}
				else{ //judge who guess the ultimate code
					if(a%1==0){
						p2l1.setText("  BOMB!!!");
						p2l1.setIcon(new ImageIcon("loser1.jpg"));
						p2l1.setForeground(Color.RED);
						p1l1.setForeground(Color.orange);
						p1l2.setForeground(Color.black);
						JOptionPane.showMessageDialog(null,"             "+player1+"  lose !");
						keyin2.setText(guess2);
						b=0;
					}
					else{
						p2l1.setText("  BOMB!!! ");
						p2l1.setIcon(new ImageIcon("loser1.jpg"));
						p2l1.setForeground(Color.RED);
						p1l1.setForeground(Color.black);
						p1l2.setForeground(Color.orange);
						JOptionPane.showMessageDialog(null,"             "+player2+"  lose !");
						keyin2.setText(guess2);
						b=0;
					}
				}
			}
		}
		if(e.getSource()==jtb1){ //go to menu
			Menu menu=new Menu();
			menu.JMenu.setVisible( true );
			UC2.setVisible(false);
		}
		if(e.getSource()==jtb2){ //exit
			System.exit(0);
		}
	}
}
