package fp.s100502012;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.*;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

//the class is used to paint and repaint the game scene

public class GamePanel extends JPanel{
	Random random=new Random();
	private int size=50;//the max number of the balls
	private int velocityX;//the number of pixels that the ball move can move along x-direction
	private int velocityY;//the number of pixels that the ball move can move along y-direction
	private int radius;//ball's radius
	private int red;
	private int green;
	private int blue;
	private int line;//the four dimension of the 2D game scene 
	private int positionX;//the initial x-position
	private int positionY;//the initial y-position
	private int number=0;//initialize the number of the balls
	private boolean flag=true;//initialize the judgment of the collision
	private int CoreX=350;//target's real x-position
	private int CoreY=350;//target's real y-position
	private int moveX=5;//the number of pixels that the target move can move along x-direction
	private int moveY=5;//the number of pixels that the target move can move along y-direction
	private int ArrowDirection=1;//the direction of the target(arrow) point to (1 means down ,2 means up ,3 means left ,4 means right) 
	
	private Balls ball[]=new Balls[size];
  	private Timer timerRepaint=new Timer(10,new TimerListener1());//repaint the panel
	private Timer timerNewObject=new Timer(5000,new TimerListener2());//renew the object of the ball
	
	private boolean p1 = false;//initialize the press/release judgment of the keyListener to the four direction
	private boolean p2 = false;
	private boolean p3 = false;
	private boolean p4 = false;
	
	private ImageIcon AU=new ImageIcon("image/component/arrow-up.png");
	private Image au=AU.getImage();
	private ImageIcon AD=new ImageIcon("image/component/arrow-down.png");
	private Image ad=AD.getImage();
	private ImageIcon AL=new ImageIcon("image/component/arrow-left.png");
	private Image al=AL.getImage();
	private ImageIcon AR=new ImageIcon("image/component/arrow-right.png");
	private Image ar=AR.getImage();
	private ImageIcon face=new ImageIcon("image/face/darkrainbow.png");
	private Image Face=face.getImage();
	
	public GamePanel(){
		ball[0]=new Balls();//new the first object of the ball
		ball[0].setRandom(getRandomVelocityX(),getRandomVelocityY(),//send the data to the class ball to create a ball
				          getRandomRadius(),
				          getRandomColorRed(),getRandomColorGreen(),getRandomColorBlue(),
				          0,
				          getRandomPositionX(),getRandomPositionY(),
				          getRandomLine());
		
		timerRepaint.start();//start to repaint the scene
		timerNewObject.start();//start to renew the ball(object)
		
		addKeyListener(new KeyAdapter(){//four keyListener of the four direction
			public void keyPressed(KeyEvent e){
				if (e.getKeyCode()==KeyEvent.VK_DOWN){
					setDirection(1);
					p1 = true;//turn the judgment to press
				}	
					
				else if (e.getKeyCode()==KeyEvent.VK_UP){
					setDirection(2);
					p2 = true;//turn the judgment to press
				}                          
					                        
				else if (e.getKeyCode()==KeyEvent.VK_LEFT){
					setDirection(3);
					p3 = true;//turn the judgment to press

				}
	                                         
				else if (e.getKeyCode()==KeyEvent.VK_RIGHT){
					setDirection(4);
	        		p4 = true;//turn the judgment to press
				}
			}
			
			public void keyReleased(KeyEvent e){
				if (e.getKeyCode()==KeyEvent.VK_DOWN){
					p1 = false;//turn the judgment to release
				}	
					
				else if (e.getKeyCode()==KeyEvent.VK_UP){
					p2 = false;//turn the judgment to release
				}                          
					                        
				else if (e.getKeyCode()==KeyEvent.VK_LEFT){
					p3 = false;//turn the judgment to release

				}
	                                         
				else if (e.getKeyCode()==KeyEvent.VK_RIGHT){
	        		p4 = false;//turn the judgment to release
				}
			}
		});
	}
	
	public int getNumber(){
		return number;
	}
	
	public int getRandomRadius(){//create random radius
	    radius=10+random.nextInt(140);
		return radius;
	}
	 
	public int getRandomVelocityX(){//create random move distance(x),which is depending on the radius of the ball 
		if (getRandomRadius()>=80){
			velocityX=1+random.nextInt(2);
		}
		
		else if (getRandomRadius()<80 && getRandomRadius()>=50){
			velocityX=1+random.nextInt(3);
		}
		
		else if (getRandomRadius()<50 && getRandomRadius()>=5){
			velocityX=2+random.nextInt(2);
		}
		
		else if (getRandomRadius()<5){
			velocityX=3+random.nextInt(5);
		}
		
		return velocityX;
	}
	
    public int getRandomVelocityY(){//create random move distance(y),which is depending on the radius of the ball 
    	if (getRandomRadius()>=80){
			velocityY=1+random.nextInt(2);
		}
		
    	else if (getRandomRadius()<80 && getRandomRadius()>=50){
			velocityY=1+random.nextInt(3);
		}
		
    	else if (getRandomRadius()<50 && getRandomRadius()>=5){
			velocityY=2+random.nextInt(2);
		}
		
    	else if (getRandomRadius()<5){
			velocityY=3+random.nextInt(5);
			
		}
		
		return velocityY;
	}
    
    public int getRandomColorRed(){//random the red color
    	red=70+random.nextInt(100);
		return red;
    }
    
    public int getRandomColorGreen(){//random the green color
    	green=100+random.nextInt(155);
		return green;
	}
    
    public int getRandomColorBlue(){//random the blue color
    	blue=100+random.nextInt(155);
		return blue;
	}
    
    public int getRandomPositionX(){//random the initial position-x
    	positionX=random.nextInt(700);
    	return positionX;
	}
    
    public int getRandomPositionY(){//random the initial position-y
    	positionY=random.nextInt(700);
    	return positionY;
	}
    
    public int getRandomLine(){//random the initial line position 
    	line=random.nextInt(4);
    	return line;
	}
    
    
    
    public int setCoreX(int a){
    	CoreX=a;
    	return CoreX;
    }
    
    public int setCoreY(int a){
    	CoreY=a;
    	return CoreY;
    }
    
    public void setDirection(int a){
    	ArrowDirection=a;
    }
    
    
    public void timeStop(){//all stop
    	timerRepaint.stop();
    	timerNewObject.stop();
    }
    
    public void timeConti(){//all conti.
    	timerRepaint.start();
    	timerNewObject.start();
    }
    
    public void setCollideResult(boolean result){
    	flag=result;
    }
    
    public boolean getCollideResult(){
    	return flag;
    }
    
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		g.drawImage(Face,0,0,700,700,this);//setting cover
		if (number==size-1){//restraining the max number of the ball
			timerNewObject.stop();
		}
		
		for(int a=0 ; a<=getNumber() ; a++){//send the data to the class ball to create a ball
			g.setColor(ball[a].getColor());	
			g.fillOval(ball[a].getX0(),ball[a].getY0(),ball[a].getRadius(),ball[a].getRadius());	
		}
		
		for(int a=0 ; a<=number ; a++){//judge if the ball collide with the target or not 
			int r=ball[a].getRadius()/2;
			int bX=ball[a].getX0();
			int bY=ball[a].getY0();
			int distance=0;
			distance=(((bX+r)-CoreX)*((bX+r)-CoreX)+((bY+r)-CoreY)*((bY+r)-CoreY));

			if ( flag == true && distance <= r*r ) {//if the distance of the ball and the target smaller than the ball's radius 
				timeStop();                         //and the target is under the position of "not be collided"
				setCollideResult(false);            //then we can judge that the ball is bump into the target
				                                    //and we should change the target position to"be collided"
			}
		}
		
		if (ArrowDirection==1){
			g.drawImage(ad, CoreX-34, CoreY-19, 70, 40, this );
		}
		
		else if (ArrowDirection==2){
        	g.drawImage(au, CoreX-34, CoreY-19, 70, 40, this );
		}
        
		else if (ArrowDirection==3){
			g.drawImage(al, CoreX-34, CoreY-19, 85, 35, this );
        }
        
		else if (ArrowDirection==4){
			g.drawImage(ar, CoreX-34, CoreY-19, 85, 35, this );
        }
	}
	
	class TimerListener1 implements ActionListener{
		public void actionPerformed(ActionEvent e){//the move of the target
			if (p1==true || p2==true || p3==true || p4==true){//the target should permit to move only under the position of "press the key"  
			    if (ArrowDirection==1){
			        if (CoreY>620){
			        }
			        else{
			    	    CoreY+=moveY;
			        }
			}
			
			    else if (ArrowDirection==2){
				    if (CoreY<25){
				    }
				    else{
				        CoreY-=moveY;
				    }
			}
			
			    else if (ArrowDirection==3){
				    if (CoreX<25){
				    }
				    else{
					    CoreX-=moveX;
				    }
			}
			
			    else if (ArrowDirection==4){
				    if (CoreX>665){
				    }
				    else{
					    CoreX+=moveX;
				    }
			    }
			}
			
			repaint();//repaint the scene
		}
	}
	
	class TimerListener2 implements ActionListener{//send the data to the class ball to create a ball
		public void actionPerformed(ActionEvent e){
			number++;//number of ball +1  
		    ball[getNumber()]=new Balls();//create new object of ball
		    ball[getNumber()].setRandom(getRandomVelocityX(),getRandomVelocityY(),//
			                            getRandomRadius(),
			                            getRandomColorRed(),getRandomColorGreen(),getRandomColorBlue(),
			                            getNumber(),
			                            getRandomPositionX(),getRandomPositionY(),
			                            getRandomLine());                     
		}
	}
}
