package fp.s100502012;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

// the code in the file are main to display the scene of the game and keep the record of the total time that the user playing the game  

public class DarkRainBow extends JFrame implements ActionListener{
	private GamePanel gamePanel=new GamePanel();//new a object of GamePanel
	private JPanel buttonPanel=new JPanel();//new a object of buttonPanel which is used to setting the start and stop button and displaying time 
	private JLabel buttonface=new JLabel();//new a object of label for setting the cover of [buttonFace]
	private ImageIcon BF=new ImageIcon("image/face/buttonface.png");//picture of [buttonFace]
	private JButton tsutsuku=new JButton("CONTINUE");//new object of a button used to continue the game   
	private JButton stop=new JButton("STOP");//new object of a button used to stop the game   
	private ImageIcon resume=new ImageIcon("image/buttons/conti.png");//icon of [tsutsuku]
	private ImageIcon Stop=new ImageIcon("image/buttons/stop.png");//icon of [resume]
	private String minSTR="";//record min. in String
	private String secSTR="";//record sec. in String
	private String microSecSTR="";//record microsecond in String
	private int min=0;//initialize the min.
	private int sec=0;//initialize the sec.
	private int microSec=0;////initialize the microsecond
	private String Text="00:00:00";////initialize the time in String 
	private JLabel count=new JLabel();//new a label [count] for display how many time we play 
	private Timer time=new Timer(10,new TimerListener1());// new a object [timer] as a timekeeper 
	private time t=new time();//new a object of [t] to get a time that in the type of " xx:xx:xx " 
	private Font largeFontLarge=new Font("LilyUPC",Font.BOLD,30);//setting the type of the words
	private String name;
	public DarkRainBow(String t){
		name=t;//pass ID
		time.start();//timeKeeper start to count the time 
		
		gamePanel.setFocusable(true);//make the keyListenercan be used
		gamePanel.setLayout(null);//setting the components freely
		gamePanel.setSize(700,700);//setting Size
		gamePanel.setLocation(0,0);//setting absolute position
		
		tsutsuku.setIcon(resume);//setting the icon of the button 
		tsutsuku.setText(null);//making the icon can be full filled in the button
		tsutsuku.setBorderPainted(false);//making the the border line of the button invisible
		tsutsuku.setContentAreaFilled(false);//making the button being transparent 
	    tsutsuku.setSize(130,35);//setting Size
	    tsutsuku.setLocation(710,30);//setting absolute position 
	    
		stop.setIcon(Stop);//setting the icon of the button 
		stop.setText(null);//making the icon can be full filled in the button
		stop.setBorderPainted(false);//making the the border line of the button invisible
		stop.setContentAreaFilled(false);//making the button being transparent 
		stop.setSize(130,30);//setting Size
		stop.setLocation(710,100);//setting absolute position 
		
		count.setFont(largeFontLarge);//setting the words type 
		count.setForeground(Color.WHITE);//setting the words color
		count.setSize(130,140);//setting Size
		count.setLocation(725,300);//setting absolute position 
		
		buttonface.setLocation(700,0);//setting absolute position 
		buttonface.setSize(160, 700);//setting Size
		buttonface.setIcon(BF);//setting the cover
		
		buttonPanel.setLayout(null);//setting the components freely
		buttonPanel.setSize(160,700);//setting Size
		buttonPanel.setLocation(700,0);//setting absolute position 
		buttonPanel.add(tsutsuku);
		tsutsuku.addActionListener(this);//add ActionListener to resume the game
		buttonPanel.add(stop);
		stop.addActionListener(this);//add ActionListener to stop the game
		buttonPanel.add(count);
		buttonPanel.add(buttonface);
		
		add(gamePanel);
		add(buttonPanel);
	}
	
	public void actionPerformed(ActionEvent e){//at this place ,it seems that we have to remove the button and add it back ,by doing so ,we can move the target(target is set in the gamePanel ) again 
		if (e.getSource()==stop){              
			gamePanel.timeStop();//stop the time of the panel [gamePanel]
			time.stop();
			
		    buttonPanel.remove(stop);
			buttonPanel.remove(tsutsuku);
			buttonPanel.remove(buttonface);
			buttonPanel.remove(count);
			remove(buttonPanel);
			
			buttonPanel.add(count);
			buttonPanel.add(stop);
			buttonPanel.add(tsutsuku);
			buttonPanel.add(buttonface);
			add(buttonPanel);
		}
		
		if (e.getSource()==tsutsuku){//start the time of the panel [gamePanel]
			gamePanel.timeConti();
			time.start();
			
			buttonPanel.remove(stop);
			buttonPanel.remove(tsutsuku);
			buttonPanel.remove(buttonface);
			buttonPanel.remove(count);
			remove(buttonPanel);
			
			buttonPanel.add(count);
			buttonPanel.add(stop);
			buttonPanel.add(tsutsuku);
			buttonPanel.add(buttonface);
			add(buttonPanel);
		}
	}
	
	public void setTimeLabel(){
		if (min<10){
			min=t.getMin();//get current hour
    	    minSTR="0"+min+":";//store information as string
    	}
    	else{
    		min=t.getMin();
    		minSTR=min+":";
    	}
    	
    	if (sec<10){
    		sec=t.getSec();//get current minute
    	    secSTR="0"+sec+":";//store information as string 
    	}
    	else{
    		sec=t.getSec();
    		secSTR=sec+":";
    	}
    	
    	if (microSec<9){
    		microSec=t.getMicroSec();//get current microsec
    	    microSecSTR="0"+microSec;//store information as string 
    	}
    	else{
    		microSec=t.getMicroSec();
    		microSecSTR=microSec+"";
    	}
  
        count.setText(minSTR+secSTR+microSecSTR);
	}
	
	class TimerListener1 implements ActionListener {
		public void actionPerformed(ActionEvent e){
			t.setTime();//call class [time] to count time
			setTimeLabel();
			boolean flag=gamePanel.getCollideResult();
			
			if (flag==false) {//stop recording the time and open the frame [PointedTable], if target been collided by a ball 
	        	time.stop();
	        	
	        	int point=min*6000+sec*100+microSec;
	        	HighScore hp=new HighScore();
	        	hp.sortData(point,name);//send data of time to the class [HighScore]
	        	
	        	PointTable P=new PointTable(minSTR+secSTR+microSecSTR,point,name);//open frame [pointtable]
			    P.setSize(500,400);
				P.setTitle("Escaping Balls");
				P.setLocationRelativeTo(null);
				P.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				P.setVisible(true);
	        }
		}
	}
}
