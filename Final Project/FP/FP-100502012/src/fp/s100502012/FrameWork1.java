package fp.s100502012;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

//initial scene , let user key in ID

public class FrameWork1 extends JFrame implements ActionListener{
	private JPanel initialFace=new JPanel();
	private JLabel Face=new JLabel();
	private JLabel welcome=new JLabel("Balls Escaping");
	private ImageIcon face=new ImageIcon("image/face/face.png");
	private JTextField ID=new JTextField(5);

	public FrameWork1(){
		
		ID.setSize(80,30);
		ID.setLocation(460,143);
		
		Face.setIcon(face);
		Face.setSize(700,500);
		Face.setLocation(0,0);
		
		initialFace.setLayout(null);
		initialFace.add(ID);
		ID.addActionListener(this);
		initialFace.add(Face);
		
		add(initialFace);
	}
	
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==ID){
     		String text=ID.getText();
			setVisible(false);

			ID.setText("");// when press enter key then reset the text field and close this frame ,open the frame [FrameWork2]
			FrameWork2 f2=new FrameWork2(text);
			f2.setSize(700,500);
			f2.setTitle("Escaping Balls");
		    f2.setLocationRelativeTo(null);
		    f2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			f2.setVisible(true);
		}
	}
}
