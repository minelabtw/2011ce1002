package fp.s100502012;

//the class is used to count time and transform it into min , sec ,and microsecond second from microsecond

public class time{
	private int microSec=0;
	private int sec=0;
	private int min=0;
	
	
	public void setTime(){//microsecond +1 when call the method 
		microSec++;
		if (microSec>=100){
			microSec=0;
			sec+=1;
		}
	}
	
	public int getMin(){
		return min;
	}
	
	public int getSec(){
		if (sec==60){
			sec=0;
			min+=1;
		}
		return sec;
	}
	
	public int getMicroSec(){
		return microSec;
	}
}
