package fp.s100502012;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;
import javax.naming.NameParser;

//the class is used to deal with the problem of I/O

public class HighScore {
    private File file=new File("input.txt");
    private File file2=new File("name.txt");
    private String[] name = new String[6];
    private int[] score = new int[6];
    private String text;
    
    public HighScore() {
        try {
           Scanner input = new Scanner(file);
           Scanner input2 = new Scanner(file2);
           for(int a=0 ; a<5 ; a++) {
               score[a]=input.nextInt();//scanner in the list of score
               name[a]=input2.next();//scanner in the list of ID
           }
           input.close();
           input2.close();
        }
        catch(Exception e) {

        }
    }

    public void sortData(int userScore,String t) {
        try{
        	name[5]=t;
        	score[5]=userScore;
            PrintWriter output = new PrintWriter(file);
            PrintWriter output2 = new PrintWriter(file2);
            for(int a=0 ; a<5 ; a++){ 
            	for(int b=0 ; b<5 ; b++){//bubble sort ,sorting data from high to low
            		if (score[b]<score[b+1]){
            			String nameCup;
            			int cup;
            			cup=score[b+1];
            			score[b+1]=score[b];
            			score[b]=cup;
            			
            			nameCup=name[b+1];//although String(ID) can not be compared with each other like score , but we still can sort it by linking it to pointed score  
            			name[b+1]=name[b];
            			name[b]=nameCup;
            		}
            	}
            }
            
            for(int a=0 ; a<5 ; a++) {//print to the file after sorting
                output.println(score[a]);
                output2.println(name[a]);
                System.out.println(name[a]);
            }
            
            output.close();
            output2.close();
        }
        catch(Exception e) {

        }
    }
    
    public int getHighestPoint(){
    	int hp=0;
    	
    	try{
    		Scanner input = new Scanner(file);//get the highest score from the file
            hp=input.nextInt();
			input.close();
        }	
        catch (Exception e) {
        	
        }
    	
    	return hp;
    }
    
    public int getScore(int a){
    	return score[a];
    }
    
    public String getName(int a){
    	return name[a];
    }
}