package fp.s100502012;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

//the code in the class FrameWork2 are main to display the scene of buttons and link to  the game mode and game record 

public class FrameWork2 extends JFrame implements ActionListener{
	private JPanel interFace=new JPanel();//main Panel of FrameWork2
	private JLabel Face=new JLabel();//cover of FrameWork2
	private ImageIcon face=new ImageIcon("image/face/face2.png");//cover
	private JButton DarkRainBowMode=new JButton("ESCAPING MODE");//button of Escaping Mode
	private JButton FraneWorkRecord=new JButton("RECORD");//button of Record
	private ImageIcon number1=new ImageIcon("image/component/1.png");//icon of count down number1 
	private ImageIcon number2=new ImageIcon("image/component/2.png");//icon of count down number2
	private ImageIcon number3=new ImageIcon("image/component/3.png");//icon if count down number3
	private ImageIcon modeIcon=new ImageIcon("image/buttons/MODE.png");//icon of the button of Escaping Mode
	private ImageIcon recordIcon=new ImageIcon("image/buttons/RECORD.png");//icon of the button of Record
	private Timer countDownTimer=new Timer(1000,new TimerListener1());//a timekeeper for counting down the numbers and ready for starting the game
	private int countDown=3;//initialize the first number of count down
	private String text;
	
	public FrameWork2(String t){
		text=t;//sending String ID via [t] and save it in the [text] 
        interFace.setLayout(null);//make the location of the components on the panel can be set freely
        
		DarkRainBowMode.setText(null);//making the icon of the button can be full filled in the button 
		DarkRainBowMode.setIcon(modeIcon);//setting the Icon of Escaping Mode Button 
		DarkRainBowMode.setContentAreaFilled(false);//make the button being transparent
		DarkRainBowMode.setSize(250,50);//setting the size of the Escaping Mode Button
		DarkRainBowMode.setLocation(80,115);//setting the absolute position of the Escaping Mode Button
		interFace.add(DarkRainBowMode);//add the Escaping Mode Button into the interFace Panel
		DarkRainBowMode.addActionListener(this);//add ActionListener to the Escaping Mode Button
		
		FraneWorkRecord.setText(null);//making the icon of the button can be full filled in the button 
		FraneWorkRecord.setIcon(recordIcon);//setting the Icon of the Record Mode Button
		FraneWorkRecord.setContentAreaFilled(false);//make the button being transparent
		FraneWorkRecord.setSize(250,50);//setting the size of the Record Mode Button
		FraneWorkRecord.setLocation(80,195);//setting the absolute position of the Record Mode Button
		interFace.add(FraneWorkRecord);//add the Record Mode Button into the interFace Panel
		FraneWorkRecord.addActionListener(this);//add ActionListener to the Record Mode Button
        
		Face.setIcon(face);//setting the picture of the cover by adding it to the JLabel [Face]
		Face.setSize(700,500);//setting Size
		Face.setLocation(0,0);////setting the absolute position
	
        interFace.add(Face);//put the label(cover) onto panel [interFace] 
        add(interFace);//adding the panel to the Frame [FrameWork2] 
	}
	
	public void actionPerformed(ActionEvent e){
		
		if (e.getSource()==DarkRainBowMode){
			countDownTimer.start();//start count down
		}
		
		if (e.getSource()==FraneWorkRecord){
            setVisible(false);// invisible FrameWork2
			FrameWorkRecord fRecord=new FrameWorkRecord(text);//new a object if FrameWorkRecord
			fRecord.setSize(700,500);//setting size
			fRecord.setTitle("Escaping Balls");//setting name
		    fRecord.setLocationRelativeTo(null);//default position
		    fRecord.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			fRecord.setVisible(true);//visible FrameWorkRecord
		}
	}
	
	class TimerListener1 implements ActionListener{//count down and then execute the escaping mode
		public void actionPerformed(ActionEvent e){
			if (countDown==3){
				interFace.setBackground(Color.WHITE);
				interFace.remove(DarkRainBowMode);//remove original icon
				interFace.remove(FraneWorkRecord);//remove original icon
				
				Face.setIcon(number3);//add new count down icon 3
			}
			
			if (countDown==2){
				interFace.add(Face);
				Face.setIcon(number2);//add new count down icon 2
			}
			
			if (countDown==1){
				interFace.add(Face);
				Face.setIcon(number1);//add new count down icon 1
			}
			
			if (countDown==0){// open new Escaping Ball Mode frame
				setVisible(false);// invisible original Frame
				countDownTimer.stop();//stop count down
				DarkRainBow D=new DarkRainBow(text);// new a Escaping Ball Mode object
				D.setSize(860,700);//setting Frame size
				D.setTitle("Escaping Balls");//setting title
			    D.setLocationRelativeTo(null);//absolute position
			    D.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				D.setVisible(true);//visible DarkRainBow(Escaping Mode)
			}	
			countDown--;
		}
	}
}
