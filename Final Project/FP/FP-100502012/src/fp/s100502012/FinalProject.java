package fp.s100502012;

import java.util.Random;

import javax.swing.JFrame;

//data of the first frame [FrameWork1]

public class FinalProject {
	public static void main(String[] args){
		FrameWork1 f1=new FrameWork1();
		f1.setSize(700,500);
		f1.setTitle("Escaping Balls");
	    f1.setLocationRelativeTo(null);
	    f1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f1.setVisible(true);
	}
}
