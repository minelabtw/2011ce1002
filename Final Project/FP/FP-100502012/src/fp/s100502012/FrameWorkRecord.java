package fp.s100502012;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

//this class is used to display the top 5 name and score

public class FrameWorkRecord extends JFrame implements ActionListener{
	private String[] name=new String[5];
	private int[] score=new int[5];
	private String[] scoreSTR=new String[5];
	private HighScore h=new HighScore();
	private JPanel pScorelist=new JPanel();
	private JButton back=new JButton("BACK");
	private JLabel[] nameList=new JLabel[5];
	private JLabel[] scoreList=new JLabel[5];
	private Font largeFontLarge=new Font("IrisUPC",Font.BOLD,50);
	private ImageIcon topIcon=new ImageIcon("image/face/TOP.png");
	private ImageIcon BACK=new ImageIcon("image/buttons/BACK.png");
	private JLabel faceJLabel=new JLabel();
	private String text;
	
    public FrameWorkRecord(String t){
        text=t;
        
    	pScorelist.setLayout(null);//make the location of the components on the panel can be set freely
    	for(int a=0 ; a<5 ; a++){
    		name[a]=h.getName(a);//get name list of top 5
    		score[a]=h.getScore(a);//get score list of top 5
    		
    		String mSTR;
    		String sSTR;
    		String msSTR;
    		if (score[a]%100<10){//get a String time in the type of " xx:xx:xx " 
    			msSTR="0"+score[a]%100;
    		}
    		else{
    			msSTR=""+score[a]%100;
    		}
    		
    		int c=score[a]/100;
    		
    		if (c%60<10){
    			sSTR="0"+c%60+":";
    		}
    		else{
    			sSTR=c%60+":";
    		}
    		
    		int b=c/60;
    		
    		if (b/60<10){
    			mSTR="0"+b+":";
    		}
    		else{
    			mSTR=b+":";
    		}
    		
    		scoreSTR[a]=mSTR+sSTR+msSTR;
    		nameList[a]=new JLabel();
    		scoreList[a]=new JLabel();
    		nameList[a].setText(name[a]);
    		scoreList[a].setText(scoreSTR[a]);
    	} 
    	
    	nameList[0].setFont(largeFontLarge);
    	nameList[1].setFont(largeFontLarge);
    	nameList[2].setFont(largeFontLarge);
    	nameList[3].setFont(largeFontLarge);
    	nameList[4].setFont(largeFontLarge);
    	nameList[0].setSize(100,30);
    	nameList[1].setSize(100,30);
    	nameList[2].setSize(100,30);
    	nameList[3].setSize(100,30);
    	nameList[4].setSize(100,30);
    	nameList[0].setLocation(170,80);
    	nameList[1].setLocation(170,150);
    	nameList[2].setLocation(170,220);
    	nameList[3].setLocation(170,290);
    	nameList[4].setLocation(170,360);
    	
    	scoreList[0].setFont(largeFontLarge);
    	scoreList[1].setFont(largeFontLarge);
    	scoreList[2].setFont(largeFontLarge);
    	scoreList[3].setFont(largeFontLarge);
    	scoreList[4].setFont(largeFontLarge);
    	scoreList[0].setSize(300,30);
    	scoreList[1].setSize(300,30);
    	scoreList[2].setSize(300,30);
    	scoreList[3].setSize(300,30);
    	scoreList[4].setSize(300,30);
    	scoreList[0].setLocation(380,80);
    	scoreList[1].setLocation(380,150);
    	scoreList[2].setLocation(380,220);
    	scoreList[3].setLocation(380,290);
    	scoreList[4].setLocation(380,360);
    	
    	for(int a=0 ; a<5 ; a++){
    		pScorelist.add(nameList[a]);
    		pScorelist.add(scoreList[a]);
    	}
    	
    	back.setSize(100,30);//setting a button to let us can get back to the last Frame
    	back.setLocation(590,420);
    	back.setIcon(BACK);
    	back.setBorderPainted(false);
    	back.setContentAreaFilled(false);
    	pScorelist.add(back);
    	back.addActionListener(this);
    	
    	faceJLabel.setIcon(topIcon);
    	faceJLabel.setSize(700,500);
    	faceJLabel.setLocation(0, 0);
    	pScorelist.add(faceJLabel);
    	
    	add(pScorelist);
    }
    
    public void actionPerformed(ActionEvent e){
		if (e.getSource()==back){
			setVisible(false);

			FrameWork2 f2=new FrameWork2(text);
			f2.setSize(700,500);
			f2.setTitle("Escaping Balls");
		    f2.setLocationRelativeTo(null);
		    f2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			f2.setVisible(true);
		}
	}
}
