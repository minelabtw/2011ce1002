package fp.s100502012;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

//the class is used to display the "Game Over" scene which include your playing time ,best playing time,and two buttons that can go back to the menu or exit game

public class PointTable extends JFrame implements ActionListener{
	private JLabel face=new JLabel();
	private JPanel p=new JPanel();
	private JLabel point=new JLabel();
	private JLabel bestPoint=new JLabel();
	private JButton again=new JButton("again");
	private JButton exit=new JButton("exit");
	private JButton mainMenuButton=new JButton("main menu");
	private Font largeFontLarge=new Font("LilyUPC",Font.BOLD,30);
	private ImageIcon faceIcon=new ImageIcon("image/face/gameover.png");
	private ImageIcon Exit=new ImageIcon("image/buttons/exit.png");
	private ImageIcon Menu=new ImageIcon("image/buttons/menu.png");
	private String highestPoint;
	private HighScore hp=new HighScore();
	private String NAME;
	public PointTable(String playTime,int time,String name){
		NAME=name;//send ID
		point.setFont(largeFontLarge);
		point.setText(playTime);
		point.setForeground(Color.WHITE);
		point.setSize(150,30);
		point.setLocation(150,142);
		
		int highestPoint=hp.getHighestPoint();
		String HP=""+highestPoint;
		String mSTR;
		String sSTR;
		String msSTR;
		if (highestPoint%100<10){//transform the time into the type "xx:xx:xx"
			msSTR="0"+highestPoint%100;
		}
		else{
			msSTR=""+highestPoint%100;
		}
		
		int a=highestPoint/100;
		
		if (a%60<10){
			sSTR="0"+a%60+":";
		}
		else{
			sSTR=a%60+":";
		}
		
		int b=a/60;
		
		if (b/60<10){
			mSTR="0"+b+":";
		}
		else{
			mSTR=b+":";
		}
		String tString=mSTR+sSTR+msSTR;
		bestPoint.setFont(largeFontLarge);
		bestPoint.setText(tString);
		bestPoint.setForeground(Color.WHITE);
		bestPoint.setSize(150,30);
		bestPoint.setLocation(150,230);
		
		
		exit.setIcon(Exit);
		exit.setText(null);
		exit.setBorderPainted(false);
		exit.setContentAreaFilled(false);
		exit.setForeground(Color.black);
		exit.setSize(75,35);
		exit.setLocation(120,300);
		
		mainMenuButton.setIcon(Menu);
		mainMenuButton.setText(null);
		mainMenuButton.setBorderPainted(false);
		mainMenuButton.setContentAreaFilled(false);
		mainMenuButton.setForeground(Color.black);
		mainMenuButton.setSize(75,35);
		mainMenuButton.setLocation(290,300);
		
		face.setIcon(faceIcon);
		face.setSize(500, 400);
		face.setLocation(0, 0);
		
		p.setLayout(null);
		
		p.add(point);
		p.add(bestPoint);
		again.addActionListener(this);
		p.add(exit);
		exit.addActionListener(this);
		p.add(mainMenuButton);
		mainMenuButton.addActionListener(this);
		p.add(face);
		
		add(p);
	}
	
	public void actionPerformed(ActionEvent e){
        if (e.getSource()==exit){//exit game
			System.exit(0);
		}

        if (e.getSource()==mainMenuButton){//get back to main menu
        	setVisible(false);
        	FrameWork2 f3=new FrameWork2(NAME);
			f3.setSize(700,500);
			f3.setTitle("Escaping Balls");
		    f3.setLocationRelativeTo(null);
		    f3.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			f3.setVisible(true);
        }
	}
}
