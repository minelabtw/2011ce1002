package fp.s100502012;

import java.awt.Color;

// the class is used to set and store the data of each ball which include radius ,color ,velocity 

public class Balls{
	private int velocityX0;
	private int velocityY0;
	private int dirextionX=1;
	private int dirextionY=1;
	private int radius;
	private int x0;
	private int y0;
	private Color color;
	
	public void setRandom(int vx,int vy,int r,int red,int green,int blue,int number,int px,int py,int ln){
		
		velocityX0=vx;
		velocityY0=vy;
		radius=r;
		if (ln==0){
			x0=0;
			y0=py;
			dirextionX=1;
		    dirextionY=1;
		}
		
		if (ln==1){
			x0=px;
			y0=0;
			dirextionX=-1;
		    dirextionY=1;
		}
		
		if (ln==2){
			x0=1000;
			y0=py;
			dirextionX=-1;
		    dirextionY=1;
		}
		
		if (ln==3){
			x0=px;
			y0=700;
			dirextionX=-1;
		    dirextionY=-1;
		}
		
		color=new Color(red,green,blue);
	}
	
	public int getRadius(){
		return radius;
	}
	
	public Color getColor(){
		return color;
	}
	
	public int getX0(){
		if (dirextionX>0){
			x0+=velocityX0;
		}
		
		if (dirextionX<0){
			x0-=velocityX0;
		}
		
		if (x0+radius>700 && dirextionX>0){
			dirextionX=-1;
		}
		
		if (x0<0 && dirextionX<0){
			dirextionX=1;
		}
		
		return x0;
	}
	
	public int getY0(){
		if (dirextionY>0){
			y0+=velocityY0;
		}
		
		if (dirextionY<0){
			y0-=velocityY0;
		}
		
		if (y0+radius>660 && dirextionY>0){
			dirextionY=-1;
		}
		
		if (y0<0 && dirextionY<0){
			dirextionY=1;
		}
		
		return y0;
	}
}
