package fp.s100502518;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class Control implements ActionListener{
	FrameWork frame=new FrameWork();
	Calendar calendar=new GregorianCalendar();
	Calendar cinCalendar=Calendar.getInstance();
	Timer timer=new Timer();
	Runtodo runtodo=new Runtodo();
	int y=0;
	int d=0;
	int h=0;
	int m=0;
	int s=0;
	int newh=0;
	int newd=0;
	int slt=0;
	int stt=0;
	int et=0;
	Font font1=frame.timeLabel1.getFont();
	Font font2=new Font("Serif", 0, 16);
	Font font3=new Font("Serif", 0, 20);
	
	public Control(){
		frame.event.addActionListener(this);
		frame.year.addActionListener(this);
		frame.month.addActionListener(this);
		frame.date.addActionListener(this);
		frame.hour.addActionListener(this);
		frame.modify1.addActionListener(this);
		frame.modify2.addActionListener(this);
		frame.click.addActionListener(this);
		frame.big.addActionListener(this);
		frame.mid.addActionListener(this);
		frame.small.addActionListener(this);
		frame.black.addActionListener(this);
		frame.red.addActionListener(this);
		frame.blue.addActionListener(this);
		frame.green.addActionListener(this);
		frame.purple.addActionListener(this);
		frame.sleep.addActionListener(this);
		frame.day1.addActionListener(this);
		frame.study.addActionListener(this);
		frame.day2.addActionListener(this);
		frame.eat.addActionListener(this);
		frame.day3.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==frame.event)
		{
			frame.renewscreen(2);
		}
		
		else if(e.getSource()==frame.year||e.getSource()==frame.month||e.getSource()==frame.date||e.getSource()==frame.hour)
		{
			frame.renewscreen(1);
		}
		
		else if(e.getSource()==frame.modify1)
		{
			frame.event.setText("");
			frame.renewscreen(2);
		}
		
		else if(e.getSource()==frame.modify2)
		{
			resetall();
		}
		
		else if(e.getSource()==frame.click)
		{
			counttime();
		}
		
		else if(e.getSource()==frame.big)
		{
			frame.timeLabel1.setFont(font3);
			frame.timeLabel2.setFont(font3);
		}
		
		else if(e.getSource()==frame.mid)
		{
			frame.timeLabel1.setFont(font2);
			frame.timeLabel2.setFont(font2);
		}
		
		else if(e.getSource()==frame.small)
		{
			frame.timeLabel1.setFont(font1);
			frame.timeLabel2.setFont(font1);
		}
		
		else if(e.getSource()==frame.black)
		{
			frame.timeLabel1.setForeground(Color.BLACK);
			frame.timeLabel2.setForeground(Color.BLACK);
		}
		
		else if(e.getSource()==frame.red)
		{
			frame.timeLabel1.setForeground(Color.RED);
			frame.timeLabel2.setForeground(Color.RED);
		}
		
		else if(e.getSource()==frame.blue)
		{
			frame.timeLabel1.setForeground(Color.BLUE);
			frame.timeLabel2.setForeground(Color.BLUE);
		}
		
		else if(e.getSource()==frame.green)
		{
			frame.timeLabel1.setForeground(Color.GREEN);
			frame.timeLabel2.setForeground(Color.GREEN);
		}
		
		else if(e.getSource()==frame.purple)
		{
			frame.timeLabel1.setForeground(new Color(255, 0, 255));
			frame.timeLabel2.setForeground(new Color(255, 0, 255));
		}
		
		else if(e.getSource()==frame.sleep||e.getSource()==frame.day1)
		{
			frame.renewscreen(2);
			
			if(isNumber(frame.sleep.getText())&&isNumber(frame.day1.getText()))
			{
				newh=newh-Integer.parseInt(frame.sleep.getText())*Integer.parseInt(frame.day1.getText());
				while(newh<0)
				{
					newh=newh+24;
					newd--;
				}
				slt=Integer.parseInt(frame.sleep.getText())*Integer.parseInt(frame.day1.getText());
				frame.settimelabel2(newd, newh, m, s);
			}
		}
		
		else if(e.getSource()==frame.study||e.getSource()==frame.day2)
		{
			frame.renewscreen(2);
			
			 if(isNumber(frame.study.getText())&&isNumber(frame.day2.getText()))
			{
				newh=newh-Integer.parseInt(frame.study.getText())*Integer.parseInt(frame.day2.getText());
				while(newh<0)
				{
					newh=newh+24;
					newd--;
				}
				stt=Integer.parseInt(frame.study.getText())*Integer.parseInt(frame.day2.getText());
				frame.settimelabel2(newd, newh, m, s);
			}
		}
		
		else if(e.getSource()==frame.eat||e.getSource()==frame.day3)
		{
			frame.renewscreen(2);
			
			if(isNumber(frame.eat.getText())&&isNumber(frame.day3.getText()))
			{
				newh=newh-Integer.parseInt(frame.eat.getText())*Integer.parseInt(frame.day3.getText());
				while(newh<0)
				{
					newh=newh+24;
					newd--;
				}
				et=Integer.parseInt(frame.eat.getText())*Integer.parseInt(frame.day3.getText());
				frame.settimelabel2(newd, newh, m, s);
			}
		}
	}
	
	public static boolean isNumber(String str)//判斷是否為數字
	{
		for(int i=str.length();--i>=0;){
            int chr=str.charAt(i);
            if(chr<48 || chr>57)
                return false;
        }
        return true;
	}
	
	public void counttime()//時間差
	{
		cinCalendar.set(Integer.parseInt(frame.year.getText()), Integer.parseInt(frame.month.getText())-1, Integer.parseInt(frame.date.getText()), Integer.parseInt(frame.hour.getText()), 0);
		
		h=h+cinCalendar.get(Calendar.HOUR_OF_DAY)-calendar.get(Calendar.HOUR_OF_DAY);
		if(h<0)
		{
			h=h+24;
			d=d-1;
		}
		
		d=d+cinCalendar.get(Calendar.DAY_OF_YEAR)-calendar.get(Calendar.DAY_OF_YEAR);
		y=cinCalendar.get(Calendar.YEAR)-calendar.get(Calendar.YEAR);
		if(d<0)
		{
			d=d+365;
			y=y-1;
		}
		d=d+y*365;
		newh=h-slt-stt-et;
		newd=d;
		if(y<0)
		{
			resetall();
		}
		
		else 
		{	
			frame.settimelabel1(d, h, m, s);
			frame.settimelabel2(newd, newh, m, s);
			frame.renewscreen(2);
			timer.schedule(runtodo, 0, 1000);
		}
	}
	
	public void resetall() //回歸原始
	{
		frame.year.setText("year");
		frame.month.setText("month");
		frame.date.setText("date");
		frame.hour.setText("hour");
		frame.renewscreen(2);
	}
	class Runtodo extends TimerTask//倒數計時器
	{
		public void run()
		{
			s--;
			if(s<0)
			{
				s=59;
				m--;
			}
			
			if(m<0)
			{
				m=59;
				h--;
				newh--;
			}
			
			if(h<0)
			{
				h=23;
				d--;
			}
			
			if(d<0)
			{
				d=0;
				this.cancel();
			}
			
			if(newh<0)
			{
				newh=23;
				newd--;
			}
			
			if(newd<0)
			{
				newd=0;
				this.cancel();
			}
			else 
			{
				frame.settimelabel1(d, h, m, s);
				frame.settimelabel2(newd, newh, m, s);
			}
		}
	}
}
