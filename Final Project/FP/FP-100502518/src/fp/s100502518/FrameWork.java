package fp.s100502518;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.TextField;
import java.util.Calendar;

import javax.swing.*;

public class FrameWork extends JFrame{
	JTextField event=new JTextField(8);
	JTextField year=new JTextField(4);
	JTextField month=new JTextField(4);
	JTextField date=new JTextField(4);
	JTextField hour=new JTextField(4);
	JTextField sleep=new JTextField(4);
	JTextField study=new JTextField(4);
	JTextField eat=new JTextField(4);
	JTextField day1=new JTextField(4);
	JTextField day2=new JTextField(4);
	JTextField day3=new JTextField(4);
	JPanel eventPanel=new JPanel();
	JPanel timePanel=new JPanel();
	JPanel fontsizePanel=new JPanel();
	JPanel colorPanel=new JPanel();
	JPanel mix1=new JPanel();
	JPanel mix2=new JPanel();
	JPanel mix3=new JPanel();
	JPanel sleepPanel=new JPanel();
	JPanel studyPanel=new JPanel();
	JPanel eatPanel=new JPanel();
	JRadioButton big,mid,small,black,red,blue,green,purple;
	JLabel timeLabel1=new JLabel();
	JLabel timeLabel2=new JLabel();
	JButton click=new JButton("確認");
	JButton modify1=new JButton("修改");
	JButton modify2=new JButton("修改");
	
	public FrameWork(){
		year.setText("year");
		month.setText("month");
		date.setText("date");
		hour.setText("hour");
		sleep.setText("hour");
		study.setText("hour");
		eat.setText("hour");
		day1.setText("perday");
		day2.setText("perday");
		day3.setText("perday");
		
		eventPanel.setLayout(new FlowLayout());
		eventPanel.add(new JLabel("事件名稱: "));
		eventPanel.add(event);
		
		timePanel.setLayout(new FlowLayout());
		timePanel.add(new JLabel("年: "));
		timePanel.add(year);
		timePanel.add(new JLabel("月: "));
		timePanel.add(month);
		timePanel.add(new JLabel("日: "));
		timePanel.add(date);
		timePanel.add(new JLabel("時: "));
		timePanel.add(hour);
		
		fontsizePanel.setLayout(new GridLayout(1,3));
		fontsizePanel.add(big=new JRadioButton("大"));
		fontsizePanel.add(mid=new JRadioButton("中"));
		fontsizePanel.add(small=new JRadioButton("小"));
		ButtonGroup group1=new ButtonGroup();
		group1.add(big);
		group1.add(mid);
		group1.add(small);
		
		small.setSelected(true);
		
		colorPanel.setLayout(new GridLayout(1,5));
		colorPanel.add(black=new JRadioButton("黑"));
		colorPanel.add(red=new JRadioButton("紅"));
		colorPanel.add(blue=new JRadioButton("藍"));
		colorPanel.add(green=new JRadioButton("綠"));
		colorPanel.add(purple=new JRadioButton("紫"));
		ButtonGroup group2=new ButtonGroup();
		group2.add(black);
		group2.add(red);
		group2.add(blue);
		group2.add(green);
		group2.add(purple);
		
		black.setSelected(true);
		
		mix1.setLayout(new BorderLayout());
		mix1.add(fontsizePanel,BorderLayout.NORTH);
		mix1.add(colorPanel,BorderLayout.CENTER);
		
		sleepPanel.setLayout(new FlowLayout());
		sleepPanel.add(new JLabel("睡覺: "));
		sleepPanel.add(sleep);
		sleepPanel.add(new JLabel("*天數"));
		sleepPanel.add(day1);
		studyPanel.setLayout(new FlowLayout());
		studyPanel.add(new JLabel("上課: "));
		studyPanel.add(study);
		studyPanel.add(new JLabel("*天數"));
		studyPanel.add(day2);
		eatPanel.setLayout(new FlowLayout());
		eatPanel.add(new JLabel("吃飯: "));
		eatPanel.add(eat);
		eatPanel.add(new JLabel("*天數"));
		eatPanel.add(day3);
		
		mix2.setLayout(new GridLayout(4,1));
		mix2.add(eventPanel);
		mix2.add(sleepPanel);
		mix2.add(studyPanel);
		mix2.add(eatPanel);
		
		mix3.setLayout(new GridLayout(3,1));
		mix3.add(timeLabel1);
		mix3.add(timeLabel2);
		
		setLayout(new GridLayout(1,3));
		add(mix2);
		add(timePanel);
		add(mix1);
		
		setTitle("Final Project");
		setSize(1000, 400);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	public void settimelabel1(int d,int h,int m, int s)
	{
		timeLabel1.setText("理論剩餘時間: "+String.valueOf(d)+"日"+String.valueOf(h)+"時 "+String.valueOf(m)+"分 "+String.valueOf(s)+"秒 ");
	}
	
	public void settimelabel2(int d,int h,int m, int s) 
	{
		timeLabel2.setText("實際剩餘時間: "+String.valueOf(d)+"日"+String.valueOf(h)+"時 "+String.valueOf(m)+"分 "+String.valueOf(s)+"秒 ");
	}
	
	public void renewscreen(int c) //觸發按鍵後重刷新介面
	{
		int a=0;
		int b=0;
		remove(mix2);
		remove(mix3);
		remove(mix1);
		remove(timePanel);
		mix2.removeAll();
		mix2.setLayout(new GridLayout(4,1));
		sleepPanel.removeAll();
		sleepPanel.setLayout(new FlowLayout());
		studyPanel.removeAll();
		studyPanel.setLayout(new FlowLayout());
		eatPanel.removeAll();
		eatPanel.setLayout(new FlowLayout());
		timePanel.removeAll();
		timePanel.setLayout(new FlowLayout());
		
		if(event.getText().length()>0)
		{
			JLabel eventLabel=new JLabel(event.getText());
			JPanel mix=new JPanel();
			
			mix.setLayout(new FlowLayout());
			mix.add(eventLabel);
			mix.add(modify1);
			
			mix2.add(mix);
		}
		
		else 
		{
			mix2.add(eventPanel);
		}
		
		if(isNumber(sleep.getText()))
		{
			a=Integer.parseInt(sleep.getText());
			if(a>0)
			{
				sleepPanel.add(new JLabel("睡覺: "+sleep.getText()+"小時"));
			}
			
			else
			{
				sleepPanel.add(new JLabel("睡覺: "));
				sleepPanel.add(sleep);
			}
		}
		
		else
		{
			sleepPanel.add(new JLabel("睡覺: "));
			sleepPanel.add(sleep);
		}
		
		if(isNumber(day1.getText()))
		{
			a=Integer.parseInt(day1.getText());
			if(a>0)
			{
				sleepPanel.add(new JLabel("*"+day1.getText()+"天"));
			}
			
			else
			{
				sleepPanel.add(new JLabel("*天數"));
				sleepPanel.add(day1);
			}
		}
		
		else
		{
			sleepPanel.add(new JLabel("*天數"));
			sleepPanel.add(day1);
		}
		
		if(isNumber(study.getText()))
		{
			a=Integer.parseInt(study.getText());
			if(a>0)
			{
				studyPanel.add(new JLabel("上課: "+study.getText()+"小時"));
			}
			
			else
			{
				studyPanel.add(new JLabel("上課: "));
				studyPanel.add(study);
			}
		}
		
		else
		{
			studyPanel.add(new JLabel("上課: "));
			studyPanel.add(study);
		}
		
		if(isNumber(day2.getText()))
		{
			a=Integer.parseInt(day2.getText());
			if(a>0)
			{
				studyPanel.add(new JLabel("*"+day2.getText()+"天"));
			}
			
			else
			{
				studyPanel.add(new JLabel("*天數"));
				studyPanel.add(day2);
			}
		}
		
		else
		{
			studyPanel.add(new JLabel("*天數"));
			studyPanel.add(day2);
		}
		
		if(isNumber(eat.getText()))
		{
			a=Integer.parseInt(eat.getText());
			if(a>0)
			{
				eatPanel.add(new JLabel("吃飯: "+eat.getText()+"小時"));
			}
			
			else
			{
				eatPanel.add(new JLabel("吃飯: "));
				eatPanel.add(eat);
			}
		}
		
		else
		{
			eatPanel.add(new JLabel("吃飯: "));
			eatPanel.add(eat);
		}
		
		if(isNumber(day3.getText()))
		{
			a=Integer.parseInt(day3.getText());
			if(a>0)
			{
				eatPanel.add(new JLabel("*"+day3.getText()+"天"));
			}
			
			else
			{
				eatPanel.add(new JLabel("*天數"));
				eatPanel.add(day3);
			}
		}
		
		else
		{
			eatPanel.add(new JLabel("*天數"));
			eatPanel.add(day3);
		}
		
		mix2.add(sleepPanel);
		mix2.add(studyPanel);
		mix2.add(eatPanel);
		
		if(isNumber(year.getText()))
		{
			a=Integer.parseInt(year.getText());
			if(a>=2012)
			{
				timePanel.add(new JLabel(year.getText()+"年"));
			}
			
			else
			{
				timePanel.add(new JLabel("年: "));
				timePanel.add(year);
			}
		}
		
		else
		{
			timePanel.add(new JLabel("年: "));
			timePanel.add(year);
		}
		
		if(isNumber(month.getText()))
		{
			a=Integer.parseInt(month.getText());
			if(a>=1&&a<=12)
			{
				timePanel.add(new JLabel(month.getText()+"月"));
			}
			
			else
			{
				timePanel.add(new JLabel("月: "));
				timePanel.add(month);
				b++;
			}
			
		}
		
		else
		{
			timePanel.add(new JLabel("月: "));
			timePanel.add(month);
		}
		
		if(isNumber(date.getText()))
		{
			a=Integer.parseInt(date.getText());
			if(a>=1&&a<=31)
			{
				timePanel.add(new JLabel(date.getText()+"日"));
			}
			
			else
			{
				timePanel.add(new JLabel("日: "));
				timePanel.add(date);
				b++;
			}
		}
		
		else
		{
			timePanel.add(new JLabel("日: "));
			timePanel.add(date);
		}
		
		if(isNumber(hour.getText()))
		{
			a=Integer.parseInt(hour.getText());
			if(a>=0&&a<=23)
			{
				timePanel.add(new JLabel(hour.getText()+"時"));
			}
			
			else
			{
				timePanel.add(new JLabel("時: "));
				timePanel.add(hour);
				b++;
			}
		}
		
		else
		{
			timePanel.add(new JLabel("時: "));
			timePanel.add(hour);
		}
		if(b==0)
		{
			allcin();
		}
		
		setLayout(new GridLayout(1,3));
		add(mix2);
		switch (c) 
		{
		case 1:
		{
			add(timePanel);
			break;
		}
		
		case 2:
		{
			if(isNumber(year.getText())&&isNumber(month.getText())&&isNumber(date.getText())&&isNumber(hour.getText()))
			{
				add(mix3);
			}
			else
			{
				add(timePanel);
			}
			break;
		}
		default:
			break;
		}
		
		add(mix1);
		repaint();
		setVisible(true);
	}
	
	public void allcin() //判斷是否輸入完了
	{
		if(isNumber(year.getText())&&isNumber(month.getText())&&isNumber(date.getText())&&isNumber(hour.getText()))
		{
			timePanel.add(click);
			timePanel.add(modify2);
		}
	}
	
	public static boolean isNumber(String str)//判斷是否為數字
	{
		for(int i=str.length();--i>=0;){
            int chr=str.charAt(i);
            if(chr<48 || chr>57)
                return false;
        }
        return true;
	}
}
	
