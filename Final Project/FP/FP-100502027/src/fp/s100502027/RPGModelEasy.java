package fp.s100502027;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;


public class RPGModelEasy extends JPanel implements ActionListener, MouseListener {
	private JButton[][] check = new JButton[16][16];
	private int[][] checkMonster = new int[16][16];
	private int[] MonsterCount = {10,8,6,4,2};
	private int[][] checkstate = new int[16][16];
	private Color checkstart = new Color(201,189,46);
	private Border Panellines = new LineBorder(Color.WHITE,3);
	private Border checklines = new LineBorder(Color.GRAY,2);
	private Border checklower = new BevelBorder(BevelBorder.LOWERED);
	private Border checkraise = new BevelBorder(BevelBorder.RAISED);
	private Font notice = new Font("TimesRoman",Font.BOLD,25); 
	private Font nomral = new Font("TimesRoman",Font.BOLD,20); 
	private Font level = new Font("TimesRoman",Font.BOLD,15); 
	private int LV=1,HP=10,EXP=0,NEXT=7,TIME=0,tempLV=1;
	private ImageIcon Monster1 = new ImageIcon("src/fp/s100502027/LEVEL1.PNG");
	private ImageIcon Monster2 = new ImageIcon("src/fp/s100502027/LEVEL2.PNG");
	private ImageIcon Monster3 = new ImageIcon("src/fp/s100502027/LEVEL3.PNG");
	private ImageIcon Monster4 = new ImageIcon("src/fp/s100502027/LEVEL4.PNG");
	private ImageIcon Monster5 = new ImageIcon("src/fp/s100502027/LEVEL5.PNG");
	private JLabel imfoLV = new JLabel("LV:");
	private JLabel imfoHP = new JLabel("HP:");
	private JLabel imfoEXP = new JLabel("EXP:");
	private JLabel imfoNEXT = new JLabel("NEXT:");
	private JLabel imfoLVvalue = new JLabel("");
	private JLabel imfoHPvalue = new JLabel("");
	private JLabel imfoEXPvalue = new JLabel("");
	private JLabel imfoNEXTvalue = new JLabel("");
	private JLabel imfoM1 = new JLabel("");
	private JLabel imfoM1count = new JLabel("");
	private JLabel imfoM2 = new JLabel("");
	private JLabel imfoM2count = new JLabel("");
	private JLabel imfoM3 = new JLabel("");
	private JLabel imfoM3count = new JLabel("");
	private JLabel imfoM4 = new JLabel("");
	private JLabel imfoM4count = new JLabel("");
	private JLabel imfoM5 = new JLabel("");
	private JLabel imfoM5count = new JLabel("");
	private JLabel LVimfoM1 = new JLabel("LV�G��");
	private JLabel LVimfoM2 = new JLabel("LV�G��");
	private JLabel LVimfoM3 = new JLabel("LV�G��");
	private JLabel LVimfoM4 = new JLabel("LV�G��");
	private JLabel LVimfoM5 = new JLabel("LV�G��");
	private JLabel GameState = new JLabel("Game State:");
	private JLabel GameStateimfo = new JLabel("");
	private JButton Restart = new JButton("Restart");
	private CardLayout cardLayout = new CardLayout();
	private JPanel CardPanel = new JPanel(cardLayout);
	Timer timer = new Timer(100,new TimerListener());
	
	//All Components combine the frame
	public RPGModelEasy(){
		
		setLayout(new BorderLayout());
		JPanel gamePanel = new JPanel();
		gamePanel.setBackground(Color.WHITE);
		gamePanel.setBorder(checklines);
		gamePanel.setLayout(new GridLayout(16,16,2,2));
		
		for(int x=0;x<16;x++){
			for(int y=0;y<16;y++){
				checkMonster[x][y]=0;
				checkstate[x][y]=0;
				check[x][y] = new JButton("");
				check[x][y].setBackground(checkstart);
				check[x][y].setBorder(checkraise);
				check[x][y].setForeground(Color.BLACK);
				check[x][y].setActionCommand(x+" "+y);
				check[x][y].addActionListener(this);
				check[x][y].addMouseListener(this);
				check[x][y].setFont(level);
				gamePanel.add(check[x][y]);
			}
		}
		for(int t=1;t<6;t++){
			int counts = 0;
			while(counts<MonsterCount[t-1]){
				int xs = (int)(Math.random()*16);
				int ys = (int)(Math.random()*16);
				if(checkMonster[xs][ys]==0){
					checkMonster[xs][ys]=t;
					counts++;
				}
				else{
				}
			}
		}
		JPanel levelPanel = new JPanel();
		levelPanel.setBackground(Color.WHITE);
		JPanel hurtPanel = new JPanel();
		hurtPanel.setBackground(Color.RED);
		JPanel overPanel = new JPanel();
		overPanel.setBackground(Color.BLACK);
		JPanel winPanel = new JPanel();
		winPanel.setBackground(Color.YELLOW);
		CardPanel.add(gamePanel,String.valueOf(1));
		CardPanel.add(levelPanel,String.valueOf(2));
		CardPanel.add(hurtPanel,String.valueOf(3));
		CardPanel.add(overPanel,String.valueOf(4));
		CardPanel.add(winPanel,String.valueOf(5));
		
		JPanel imfoPanel = new JPanel();
		imfoPanel.setBackground(Color.BLACK);
		imfoPanel.setLayout(new GridLayout(1,5));
		
		JPanel imfoPanel1 = new JPanel();
		imfoPanel1.setBackground(Color.BLACK);
		imfoPanel1.setLayout(new GridLayout(1,2));
		imfoPanel1.setBorder(checklines);
		imfoLV.setFont(notice);
		imfoLV.setForeground(Color.WHITE);
		imfoLVvalue.setFont(notice);
		imfoLVvalue.setForeground(Color.WHITE);
		imfoLVvalue.setText(LV+"");
		
		JPanel imfoPanel2 = new JPanel();
		imfoPanel2.setBackground(Color.BLACK);
		imfoPanel2.setLayout(new GridLayout(1,2));
		imfoPanel2.setBorder(checklines);
		imfoHP.setFont(notice);
		imfoHP.setForeground(Color.WHITE);
		imfoHPvalue.setFont(notice);
		imfoHPvalue.setForeground(Color.WHITE);
		imfoHPvalue.setText(HP+"");
		
		JPanel imfoPanel3 = new JPanel();
		imfoPanel3.setBackground(Color.BLACK);
		imfoPanel3.setLayout(new GridLayout(1,2));
		imfoPanel3.setBorder(checklines);
		imfoEXP.setFont(notice);
		imfoEXP.setForeground(Color.WHITE);
		imfoEXPvalue.setFont(notice);
		imfoEXPvalue.setForeground(Color.WHITE);
		imfoEXPvalue.setText(EXP+"");
		
		JPanel imfoPanel4 = new JPanel();
		imfoPanel4.setBackground(Color.BLACK);
		imfoPanel4.setLayout(new GridLayout(1,2));
		imfoPanel4.setBorder(checklines);
		imfoNEXT.setFont(notice);
		imfoNEXT.setForeground(Color.WHITE);
		imfoNEXTvalue.setFont(notice);
		imfoNEXTvalue.setForeground(Color.WHITE);
		imfoNEXTvalue.setText(NEXT+"");
		
		Restart.setFont(nomral);
		Restart.setBorder(checklines);
		Restart.setForeground(Color.WHITE);
		Restart.setBackground(Color.BLACK);
		Restart.addActionListener(this);
		imfoPanel1.add(imfoLV);
		imfoPanel1.add(imfoLVvalue);
		imfoPanel2.add(imfoHP);
		imfoPanel2.add(imfoHPvalue);
		imfoPanel3.add(imfoEXP);
		imfoPanel3.add(imfoEXPvalue);
		imfoPanel4.add(imfoNEXT);
		imfoPanel4.add(imfoNEXTvalue);
		imfoPanel.add(imfoPanel1);
		imfoPanel.add(imfoPanel2);
		imfoPanel.add(imfoPanel3);
		imfoPanel.add(imfoPanel4);
		imfoPanel.add(Restart);
		
		JPanel imfoStatePanel = new JPanel();
		imfoStatePanel.setBackground(Color.BLACK);
		imfoStatePanel.setLayout(new GridLayout(1,2));
		GameState.setFont(notice);
		GameState.setForeground(Color.WHITE);
		GameState.setHorizontalAlignment(JLabel.CENTER);
		GameStateimfo.setFont(notice);
		GameStateimfo.setForeground(Color.WHITE);
		GameStateimfo.setHorizontalAlignment(JLabel.LEFT);
		imfoStatePanel.add(GameState);
		imfoStatePanel.add(GameStateimfo);
		
		JPanel imfoallPanel = new JPanel();
		imfoallPanel.setBackground(Color.BLACK);
		imfoallPanel.setLayout(new GridLayout(2,1));
		imfoallPanel.add(imfoPanel);
		imfoallPanel.add(imfoStatePanel);
		
		JPanel MonsterLVPanel = new JPanel();
		MonsterLVPanel.setBackground(Color.BLACK);
		MonsterLVPanel.setLayout(new GridLayout(1,5));
		LVimfoM1.setFont(notice);
		LVimfoM1.setForeground(Color.WHITE);
		LVimfoM2.setFont(notice);
		LVimfoM2.setForeground(Color.WHITE);
		LVimfoM3.setFont(notice);
		LVimfoM3.setForeground(Color.WHITE);
		LVimfoM4.setFont(notice);
		LVimfoM4.setForeground(Color.WHITE);
		LVimfoM5.setFont(notice);
		LVimfoM5.setForeground(Color.WHITE);
		MonsterLVPanel.add(LVimfoM1);
		MonsterLVPanel.add(LVimfoM2);
		MonsterLVPanel.add(LVimfoM3);
		MonsterLVPanel.add(LVimfoM4);
		MonsterLVPanel.add(LVimfoM5);
		
		JPanel MonsterPanel = new JPanel();
		MonsterPanel.setBackground(Color.BLACK);
		MonsterPanel.setLayout(new GridLayout(1,10));
		imfoM1count.setFont(notice);
		imfoM1count.setForeground(Color.WHITE);
		imfoM1count.setText("x"+MonsterCount[0]);
		imfoM2count.setFont(notice);
		imfoM2count.setForeground(Color.WHITE);
		imfoM2count.setText("x"+MonsterCount[1]);
		imfoM3count.setFont(notice);
		imfoM3count.setForeground(Color.WHITE);
		imfoM3count.setText("x"+MonsterCount[2]);
		imfoM4count.setFont(notice);
		imfoM4count.setForeground(Color.WHITE);
		imfoM4count.setText("x"+MonsterCount[3]);
		imfoM5count.setFont(notice);
		imfoM5count.setForeground(Color.WHITE);
		imfoM5count.setText("x"+MonsterCount[4]);
		Monster1.setImage(Monster1.getImage().getScaledInstance(60,60 , Image.SCALE_DEFAULT));
		imfoM1.setIcon(Monster1);
		Monster2.setImage(Monster2.getImage().getScaledInstance(60,60 , Image.SCALE_DEFAULT));
		imfoM2.setIcon(Monster2);
		Monster3.setImage(Monster3.getImage().getScaledInstance(60,60 , Image.SCALE_DEFAULT));
		imfoM3.setIcon(Monster3);
		Monster4.setImage(Monster4.getImage().getScaledInstance(60,60 , Image.SCALE_DEFAULT));
		imfoM4.setIcon(Monster4);
		Monster5.setImage(Monster5.getImage().getScaledInstance(60,60 , Image.SCALE_DEFAULT));
		imfoM5.setIcon(Monster5);
		MonsterPanel.add(imfoM1);
		MonsterPanel.add(imfoM1count);
		MonsterPanel.add(imfoM2);
		MonsterPanel.add(imfoM2count);
		MonsterPanel.add(imfoM3);
		MonsterPanel.add(imfoM3count);
		MonsterPanel.add(imfoM4);
		MonsterPanel.add(imfoM4count);
		MonsterPanel.add(imfoM5);
		MonsterPanel.add(imfoM5count);
		
		JPanel MonsterimfoPanel = new JPanel();
		MonsterimfoPanel.setBackground(Color.BLACK);
		MonsterimfoPanel.setLayout(new GridLayout(2,1));
		MonsterimfoPanel.add(MonsterLVPanel);
		MonsterimfoPanel.add(MonsterPanel);
		
		
		add(imfoallPanel,BorderLayout.NORTH);
		add(CardPanel,BorderLayout.CENTER);
		add(MonsterimfoPanel,BorderLayout.SOUTH);
		GameStateimfo.setText("Game Start!");
		
	}

	//to rective the button
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==Restart){
			Restart();
		}
		else{
			String commandofgrid = e.getActionCommand();
			String[] twonumber = commandofgrid.split(" ");
	        int checkx = Integer.parseInt(twonumber[0]);
	        int checky = Integer.parseInt(twonumber[1]);
	        click(checkx,checky);
		}
		
	}
	
	//set the click mouse right use
	public void mouseClicked(MouseEvent e){ 
		if(e.getButton() == MouseEvent.BUTTON3){
			String rightcommand = ((JButton)e.getSource()).getActionCommand();
			String[] twonumber = rightcommand.split(" ");
	        int checkx = Integer.parseInt(twonumber[0]);
	        int checky = Integer.parseInt(twonumber[1]);
	        clickright(checkx,checky);			
		}
	}
	
	//when click mouse right  to set the hint
	public void clickright(int x ,int y){
		if(checkstate[x][y]==0){
			int temp=0;
			if(check[x][y].getText()==null){
			}
			else{
				char[] ButtonText ={0};
				int counttemp = 0 ;
				if(check[x][y].getText()!=""){
					ButtonText = check[x][y].getText().toCharArray();
					counttemp = ButtonText[0]-'0';
				}
				if(counttemp>=0||counttemp<=5){
					temp = counttemp;
				}
			}
			
			if(temp<5){
				temp++;
				check[x][y].setText(temp+"");
			}
			else{
				check[x][y].setText("");
			}
		}
	}
	
	//the click button  to get the imfo in this button and take a use
	public void click(int x , int y){
		GameStateimfo.setText("Game Playing");
		if(checkstate[x][y]==0){
			checkstate[x][y]=1;
			check[x][y].setBorder(checklower);
			if(checkMonster[x][y]==0){
				check[x][y].setBackground(Color.BLACK);
				check[x][y].setForeground(Color.WHITE);
				check[x][y].setText(countLevel(x,y));
				if(countLevel(x,y)==""){
					clicknext(x,y);
				}
			}
			else if(checkMonster[x][y]==1){
				check[x][y].setBackground(Color.BLACK);
				Monster1.setImage(Monster1.getImage().getScaledInstance(check[x][y].getWidth(),check[x][y].getHeight() , Image.SCALE_DEFAULT));
				check[x][y].setIcon(Monster1);
				MonsterCount[0]--;
				if(LV<checkMonster[x][y]){
					HP-=(checkMonster[x][y]-LV)*checkMonster[x][y];
					GameStateimfo.setText("Get Hurt!");
					cardLayout.show(CardPanel, "3");
					timer.start();
				}
				EXP+=Math.pow(2, 0);
				imfofreash();
			}
			else if(checkMonster[x][y]==2){
				check[x][y].setBackground(Color.BLACK);
				Monster2.setImage(Monster2.getImage().getScaledInstance(check[x][y].getWidth(),check[x][y].getHeight() , Image.SCALE_DEFAULT));
				check[x][y].setIcon(Monster2);
				MonsterCount[1]--;
				if(LV<checkMonster[x][y]){
					HP-=(checkMonster[x][y]-LV)*checkMonster[x][y];
					GameStateimfo.setText("Get Hurt!");
					cardLayout.show(CardPanel, "3");
					timer.start();
				}
				EXP+=Math.pow(2, 1);
				imfofreash();
			}
			else if(checkMonster[x][y]==3){
				check[x][y].setBackground(Color.BLACK);
				Monster3.setImage(Monster3.getImage().getScaledInstance(check[x][y].getWidth(),check[x][y].getHeight() , Image.SCALE_DEFAULT));
				check[x][y].setIcon(Monster3);
				MonsterCount[2]--;
				if(LV<checkMonster[x][y]){
					HP-=(checkMonster[x][y]-LV)*checkMonster[x][y];
					GameStateimfo.setText("Get Hurt!");
					cardLayout.show(CardPanel, "3");
					timer.start();
				}
				EXP+=Math.pow(2, 2);
				imfofreash();
			}
			else if(checkMonster[x][y]==4){
				check[x][y].setBackground(Color.BLACK);
				Monster4.setImage(Monster4.getImage().getScaledInstance(check[x][y].getWidth(),check[x][y].getHeight() , Image.SCALE_DEFAULT));
				check[x][y].setIcon(Monster4);
				MonsterCount[3]--;
				if(LV<checkMonster[x][y]){
					HP-=(checkMonster[x][y]-LV)*checkMonster[x][y];
					GameStateimfo.setText("Get Hurt!");
					cardLayout.show(CardPanel, "3");
					timer.start();
				}
				EXP+=Math.pow(2, 3);
				imfofreash();
			}
			else if(checkMonster[x][y]==5){
				check[x][y].setBackground(Color.BLACK);
				Monster5.setImage(Monster5.getImage().getScaledInstance(check[x][y].getWidth(),check[x][y].getHeight() , Image.SCALE_DEFAULT));
				check[x][y].setIcon(Monster5);
				MonsterCount[4]--;
				if(LV<checkMonster[x][y]){
					HP-=(checkMonster[x][y]-LV)*checkMonster[x][y];
					GameStateimfo.setText("Get Hurt!");
					cardLayout.show(CardPanel, "3");
					timer.start();
				}
				EXP+=Math.pow(2, 4);
				imfofreash();
			}
		}
		else if(checkstate[x][y]==1){
			if(checkMonster[x][y]>0){
				checkstate[x][y]=2;
				check[x][y].setIcon(null);
				check[x][y].setForeground(Color.RED);
				check[x][y].setText(countLevel(x,y));
			}
		}
		else if(checkstate[x][y]==2){
			if(checkMonster[x][y]>0){
				checkstate[x][y]=1;
				check[x][y].setText("");
				if(checkMonster[x][y]==1){
					Monster1.setImage(Monster1.getImage().getScaledInstance(check[x][y].getWidth(),check[x][y].getHeight() , Image.SCALE_DEFAULT));
					check[x][y].setIcon(Monster1);
				}
				else if(checkMonster[x][y]==2){
					Monster2.setImage(Monster2.getImage().getScaledInstance(check[x][y].getWidth(),check[x][y].getHeight() , Image.SCALE_DEFAULT));
					check[x][y].setIcon(Monster2);
				}
				else if(checkMonster[x][y]==3){
					Monster3.setImage(Monster3.getImage().getScaledInstance(check[x][y].getWidth(),check[x][y].getHeight() , Image.SCALE_DEFAULT));
					check[x][y].setIcon(Monster3);
				}
				else if(checkMonster[x][y]==4){
					Monster4.setImage(Monster4.getImage().getScaledInstance(check[x][y].getWidth(),check[x][y].getHeight() , Image.SCALE_DEFAULT));
					check[x][y].setIcon(Monster4);
				}
				else if(checkMonster[x][y]==5){
					Monster5.setImage(Monster5.getImage().getScaledInstance(check[x][y].getWidth(),check[x][y].getHeight() , Image.SCALE_DEFAULT));
					check[x][y].setIcon(Monster5);
				}
			}
		}
		else{
		}
		gameresult();
	}
	
	// to count the all level of beside this check 
	public String countLevel(int x,int y){
		int levelcount =0;
		for(int tx=x-1;tx<x+2;tx++){
			for(int ty=y-1;ty<y+2;ty++){
				if(tx<0){
				}
				else if(tx>=16){
				}
				else if(ty<0){
				}
				else if(ty>=16){
				}
				else if(tx==x && ty==y){
				}
				else{
						levelcount+=checkMonster[tx][ty];
				}
			}
		}
		if(levelcount>0){
			return levelcount+"";
		}
		else{
			if(checkstate[x][y]==2){
				return "0";
			}
			else{
				return "";
			}
		}
	}
	
	//when clicked check is not next monster , to click the next check
	public void clicknext(int x,int y){
		for(int tx=x-1;tx<x+2;tx++){
			for(int ty=y-1;ty<y+2;ty++){
				if(tx<0){
				}
				else if(tx>=16){
				}
				else if(ty<0){
				}
				else if(ty>=16){
				}
				else if(tx==x && ty==y){
				}
				else{
					if(checkMonster[tx][ty]==0){
						click(tx,ty);
					}
				}
			}
		}
	}
	
	// take the imformation of game renew
	public void imfofreash(){
		LVfresh();
		imfoHPvalue.setText(HP+"");
		imfoEXPvalue.setText(EXP+"");
		NEXTfresh();
		imfoM1count.setText("x"+MonsterCount[0]);
		imfoM2count.setText("x"+MonsterCount[1]);
		imfoM3count.setText("x"+MonsterCount[2]);
		imfoM4count.setText("x"+MonsterCount[3]);
		imfoM5count.setText("x"+MonsterCount[4]);
	}
	
	//to set the [NEXT]
	public void NEXTfresh(){
		switch(LV){
			case 1:
				NEXT=7-EXP;
				break;
			case 2:
				NEXT=20-EXP;
				break;
			case 3:
				NEXT=50-EXP;
				break;
			case 4:
				NEXT=82-EXP;
				break;
			case 5:
				NEXT=0;
				break;
		}
		imfoNEXTvalue.setText(NEXT+"");
	}
	
	//To count the LV with EXP 
	public void LVfresh(){
		if (EXP<7){
			LV=1;
		}
		else if (EXP<20){
			LV=2;
		}
		else if (EXP<50){
			LV=3;
		}
		else if (EXP<82){
			LV=4;
		}
		else{
			LV=5;
		}
		imfoLVvalue.setText(LV+"");
		if(LV>tempLV){
			GameStateimfo.setText("Level UP !!");
			cardLayout.show(CardPanel, "2");
			timer.start();
			tempLV=LV;
		}
		
	}
	
	//check the state of game is which result. sucess or over
	public void gameresult(){
		int checkdone=0;
		for(int x=0;x<16;x++){
			for(int y=0;y<16;y++){
				if(checkstate[x][y]!=0){
					checkdone++;
				}
			}
		}
		if(checkdone==256){
			GameStateimfo.setText("Game Succes!");
			cardLayout.show(CardPanel, "5");
			timer.start();
		}
		
		if(HP<=0){
			timer.stop();
			int LVtemp = LV;
			int EXPtemp = EXP;
			for(int x=0;x<16;x++){
				for(int y=0;y<16;y++){
					if(checkMonster[x][y]>0 && checkstate[x][y]==0){
						click(x,y);
					}
					checkstate[x][y]=3;
				}
			}
			HP=0;
			LV=LVtemp;
			EXP=EXPtemp;
			imfofreash();
			GameStateimfo.setText("Game Over!");
			cardLayout.show(CardPanel, "4");
			timer.start();
		}
	}
	
	//To reStart game 
	public void Restart(){
		LV=1;
		HP=10;
		EXP=0;
		NEXT=7;
		TIME=0;
		tempLV=1;
		MonsterCount[0] = 10;
		MonsterCount[1] = 8;
		MonsterCount[2] = 6;
		MonsterCount[3] = 4;
		MonsterCount[4] = 2;
		
		for(int x=0;x<16;x++){
			for(int y=0;y<16;y++){
				checkMonster[x][y]=0;
				checkstate[x][y]=0;
				check[x][y].setText("");
				check[x][y].setBackground(checkstart);
				check[x][y].setIcon(null);
				check[x][y].setForeground(Color.BLACK);
				check[x][y].setBorder(checkraise);
			}
		}
		for(int t=1;t<6;t++){
			int counts = 0;
			while(counts<MonsterCount[t-1]){
				int xs = (int)(Math.random()*16);
				int ys = (int)(Math.random()*16);
				if(checkMonster[xs][ys]==0){
					checkMonster[xs][ys]=t;
					counts++;
				}
				else{
				}
			}
		}
		cardLayout.show(CardPanel, "1");
		imfofreash();
		GameStateimfo.setText("Game ReStart!");
	}
	
	//when have the thing happen, use timer to change the cardpanel ,make a Effect
	private class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			if(TIME<1){
				TIME++;	
			}
			else{
				TIME=0;
				timer.stop();
				cardLayout.show(CardPanel, "1");
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}
}
