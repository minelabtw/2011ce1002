package fp.s100502027;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class OriginalGame extends JPanel implements ActionListener, MouseListener{
	private JButton[][] grid;
	private int[][] landmine;
	private int[][] flagstate;
	private Border linesg = new LineBorder(Color.GRAY,2);
	private Border checklower = new BevelBorder(BevelBorder.LOWERED);
	private Border checkraise = new BevelBorder(BevelBorder.RAISED);
	private Font notice = new Font("TimesRoman",Font.BOLD,25); 
	private Font nomral = new Font("TimesRoman",Font.BOLD,20); 
	private Font countmine = new Font("TimesRoman",Font.BOLD,15); 
	private Color gridBlack = new Color(0,0,0);
	private Color gridWhite = new Color(255,255,255);
	private Color gridBlue = new Color(77,77,255);
	private Color gridRed = new Color(255,0,0);
	private ImageIcon mine = new ImageIcon("src/fp/s100502027/BOMB.png");
	private ImageIcon flag = new ImageIcon("src/fp/s100502027/FLAG.png");
	private int MaxX,MaxY,MineCount,tempCount ;
	private JButton Restart = new JButton("RESTART");
	private JLabel imfomine = new JLabel(mine);
	private JLabel imfominevalue = new JLabel();
	private JLabel gamestate = new JLabel();
	public OriginalGame(int X,int Y,int Mine){
		MaxX = X;
		MaxY = Y;
		MineCount = Mine ;
		tempCount = Mine ;
		grid = new JButton[X][Y];
		landmine = new int[X][Y];
		flagstate = new int[X][Y];
		setLayout(new BorderLayout());
		
		JPanel upPanel = new JPanel();
		upPanel.setLayout(new GridLayout(2,1));
		JPanel imfoPanel = new JPanel();
		imfoPanel.setLayout(new GridLayout(1,2));
		JPanel mineimfoPanel = new JPanel();
		mineimfoPanel.setLayout(new GridLayout(1,2));
		mineimfoPanel.setBorder(linesg);
		imfominevalue.setFont(notice);
		mineimfoPanel.add(imfomine);
		mineimfoPanel.add(imfominevalue);
		Restart.setBorder(linesg);
		imfoPanel.add(mineimfoPanel);
		imfoPanel.add(Restart);
		gamestate.setHorizontalAlignment(JLabel.CENTER);
		gamestate.setFont(notice);
		upPanel.add(imfoPanel);
		upPanel.add(gamestate);
		
		JPanel GamePanel = new JPanel();
		GamePanel.setLayout(new GridLayout(X,Y,1,1));
		GamePanel.setBackground(Color.BLACK);
		for(int x=0;x<X;x++){
			for(int y=0;y<Y;y++){
				landmine[x][y]=0;
				grid[x][y] = new JButton("");
				grid[x][y].setBackground(gridBlue);
				grid[x][y].setBorder(checkraise);
				grid[x][y].setActionCommand(x+" "+y);
				grid[x][y].addActionListener(this);
				grid[x][y].addMouseListener(this);
				grid[x][y].setFont(countmine);
				flagstate[x][y]=0;
				GamePanel.add(grid[x][y]);
			}
		}
		int t=0;
		while(t<Mine){
			int xs = (int)(Math.random()*X);
			int ys = (int)(Math.random()*Y);
			if(landmine[xs][ys]==0){
				landmine[xs][ys]=1;
				t++;
			}
			else{
			}
		}
		gamestate.setText("Game Start!");
		Restart.addActionListener(this);
		reFresh();
		add(upPanel,BorderLayout.NORTH);
		add(GamePanel,BorderLayout.CENTER);
	}
	public void actionPerformed(ActionEvent e){
		if(e.getSource()==Restart){
			reStart();
		}
		else{
			String commandofgrid = e.getActionCommand();
			String[] twonumber = commandofgrid.split(" ");
	        int gridx = Integer.parseInt(twonumber[0]);
	        int gridy = Integer.parseInt(twonumber[1]);
	        click(gridx,gridy);	
		}
	}
	public void click(int x , int y){
		if(landmine[x][y]==0){
			grid[x][y].setIcon(null);
			landmine[x][y]=2;
			grid[x][y].setBackground(gridWhite);
			grid[x][y].setBorder(checklower);
			grid[x][y].setText(countmine(x,y));
			if(countmine(x,y)==""){
				clicknext(x,y);
			}
			if(flagstate[x][y]==1){
				tempCount++;
			}
			gamestate.setText("-Game Palying-");
			checkwin();
		}
		else if(landmine[x][y]==1){
			grid[x][y].setIcon(null);
			grid[x][y].setBackground(gridRed);
			grid[x][y].setLayout(new GridLayout(1,1));
			mine.setImage(mine.getImage().getScaledInstance(grid[x][y].getWidth(),grid[x][y].getHeight() , Image.SCALE_DEFAULT));
			grid[x][y].setIcon(mine);
			landmine[x][y]=3;
			gamestate.setText("-Game over!-");
			showresult();
		}
		else{
			
		}
	}
	public void clickright(int x,int y){
		if(landmine[x][y]==0||landmine[x][y]==1){
			if(flagstate[x][y]==0){
				flag.setImage(flag.getImage().getScaledInstance(grid[x][y].getWidth(),grid[x][y].getHeight() , Image.SCALE_DEFAULT));
				grid[x][y].setIcon(flag);
				tempCount--;
				flagstate[x][y]=1;
			}
			else if(flagstate[x][y]==1){
				grid[x][y].setIcon(null);
				tempCount++;
				flagstate[x][y]=0;
			}
		}
		reFresh();
	}
	public String countmine(int x,int y){
		int minecount =0;
		for(int tx=x-1;tx<x+2;tx++){
			for(int ty=y-1;ty<y+2;ty++){
				if(tx<0){
				}
				else if(tx>=MaxX){
				}
				else if(ty<0){
				}
				else if(ty>=MaxY){
				}
				else{
					if(landmine[tx][ty]==1){
						minecount++;
					}
				}
			}
		}
		if(minecount>0){
			return minecount+"";
		}
		else{
			return "";
		}
	}
	public void clicknext(int x,int y){
		for(int tx=x-1;tx<x+2;tx++){
			for(int ty=y-1;ty<y+2;ty++){
				if(tx<0){
				}
				else if(tx>=MaxX){
				}
				else if(ty<0){
				}
				else if(ty>=MaxY){
				}
				else if(tx==x && ty==y){
				}
				else{
					if(landmine[tx][ty]==0){
						click(tx,ty);
					}
				}
			}
		}
	}
	public void showresult(){
		for(int x=0;x<MaxX;x++){
			for(int y=0;y<MaxY;y++){
				if(landmine[x][y]==1){
					click(x,y);
				}
				else{
					landmine[x][y]=3;
				}
			}
		}
	}
	
	public void checkwin(){
		int checkdone=0;
		for(int x=0;x<MaxX;x++){
			for(int y=0;y<MaxY;y++){
				if(landmine[x][y]==2){
					checkdone++;
				}
			}
		}
		if(checkdone==(MaxX*MaxY-MineCount)){
			for(int x=0;x<MaxX;x++){
				for(int y=0;y<MaxY;y++){
					landmine[x][y]=3;
				}
				gamestate.setText("Game Success!");
			}
		}
	}
	
	public void reStart(){
		tempCount = MineCount ;
		for(int x=0;x<MaxX;x++){
			for(int y=0;y<MaxY;y++){
				landmine[x][y]=0;
				grid[x][y].setText("");
				grid[x][y].setBackground(gridBlue);
				grid[x][y].setIcon(null);
				grid[x][y].setBorder(checkraise);
				flagstate[x][y]=0;
			}
			gamestate.setText("Game ReSTART!");
		}
		int t=0;
		while(t<MineCount){
			int xs = (int)(Math.random()*MaxX);
			int ys = (int)(Math.random()*MaxY);
			if(landmine[xs][ys]==0){
				landmine[xs][ys]=1;
				t++;
			}
			else{
			}
		}
		reFresh();
	}
	
	public void reFresh(){
		imfominevalue.setText("x"+tempCount);
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		if(e.getButton() == MouseEvent.BUTTON3){
			String rightcommand = ((JButton)e.getSource()).getActionCommand();
			String[] twonumber = rightcommand.split(" ");
	        int checkx = Integer.parseInt(twonumber[0]);
	        int checky = Integer.parseInt(twonumber[1]);
	        clickright(checkx,checky);	
		}
		
	}
	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}