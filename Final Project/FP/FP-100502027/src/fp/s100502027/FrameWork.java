package fp.s100502027;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class FrameWork extends JFrame implements ActionListener{
	private String[] title = {"Classic-MINESWEEPER-","RPG-MINESWEEPER-"};
	private JRadioButton difficulty1 = new JRadioButton("-Easy-");
	private JRadioButton difficulty2 = new JRadioButton("-Normal-");
	private JRadioButton difficulty3 = new JRadioButton("-Huge-");
	private String[] GameKind = {"Basic","RPG"};
	private ImageIcon[] GamePicture = {new ImageIcon("src/fp/s100502027/mine1.png"),new ImageIcon("src/fp/s100502027/mine2.png")};
	private String[] GameDescription = new String[2];
	private DescriptionPanel MineSweeper = new DescriptionPanel();
	private JComboBox GameKindChose = new JComboBox(GameKind);
	private JButton GameStart = new JButton("Start");
	private int difficulty = 0;
	private int Gamekind = 0;
	public FrameWork() {
		GameDescription[0]="經典的基礎採地雷\n格子的數字為周遭地雷數\n點到地雷即失敗\n滑鼠右鍵點擊可標記旗子";
		GameDescription[1]="將地雷轉為怪物，並納入經驗直\n攻擊同等或低等怪物不會受傷\n格子數字是周遭的怪物等級總和\n一邊小心找怪一邊往最高等前進\n(滑鼠右鍵可標記數字\n點擊已擊退的怪物可顯示數字)";
		
		setLayout(new BorderLayout());
		
		setDisplay(0);
		
		JPanel CenterPanel = new JPanel();
		CenterPanel.setLayout(new GridLayout(2,1));
		JPanel ButtonPanel = new JPanel();
		ButtonPanel.setLayout(new GridLayout(1,3));
		ButtonPanel.add(difficulty1);
		ButtonPanel.add(difficulty2);
		ButtonPanel.add(difficulty3);
		CenterPanel.add(ButtonPanel);
		CenterPanel.add(GameStart);
		
		ButtonGroup difficultyGroup = new ButtonGroup();
		difficultyGroup.add(difficulty1);
		difficultyGroup.add(difficulty2);
		difficultyGroup.add(difficulty3);
		difficulty1.addActionListener(this);
		difficulty2.addActionListener(this);
		difficulty3.addActionListener(this);
		add(GameKindChose,BorderLayout.NORTH);
		add(MineSweeper,BorderLayout.CENTER);
		add(CenterPanel,BorderLayout.SOUTH);
		
		GameStart.addActionListener(this);
		GameKindChose.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e) {
				Gamekind = GameKindChose.getSelectedIndex();
				setDisplay(GameKindChose.getSelectedIndex());
			}
			
		});
	
	}
	
	//set the picture and the imformation
	public void setDisplay(int index){             //to respond the combo box , change the imformation on the descriptionPanel
		MineSweeper.setTitle(title[index]);
		MineSweeper.setImageIcon(GamePicture[index]);
		MineSweeper.setDescription(GameDescription[index]);
	}
	
	//to set button funtion to use
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==GameStart){
			switch(Gamekind){
				case 0:
					switch(difficulty){
						case 0:
							JFrame ClassicEasy = new JFrame();
							ClassicEasy.setSize(900,720);
							ClassicEasy.setVisible(true);
							ClassicEasy.add(new OriginalGame(9,9,10));
							ClassicEasy.setTitle("OriginalGame-Easy");
							break;
							
						case 1:
							JFrame ClassicNormal = new JFrame();
							ClassicNormal.setSize(900,720);
							ClassicNormal.setVisible(true);
							ClassicNormal.add(new OriginalGame(16,16,40));
							ClassicNormal.setTitle("OriginalGame-Normal");
							break;
							
						case 2:
							JFrame ClassicHard = new JFrame();
							ClassicHard.setSize(900,720);
							ClassicHard.setVisible(true);
							ClassicHard.add(new OriginalGame(16,30,99));
							ClassicHard.setTitle("OriginalGame-Hard");
							break;
							}
					break;
					
				case 1:
					switch(difficulty){
						case 0:
							JFrame RPGEasy = new JFrame();
							RPGEasy.setSize(900,720);
							RPGEasy.setVisible(true);
							RPGEasy.add(new RPGModelEasy());
							RPGEasy.setTitle("-RPGModelEasy-");
							break;
							
						case 1:
							JFrame RPGNormal = new JFrame();
							RPGNormal.setSize(1000,600);
							RPGNormal.setVisible(true);
							RPGNormal.add(new RPGModelNormal());
							RPGNormal.setTitle("-RPGModelNormal-");
							break;
							
						case 2:
							JFrame RPGHuge = new JFrame();
							RPGHuge.setSize(1024,600);
							RPGHuge.setVisible(true);
							RPGHuge.add(new RPGModelHuge());
							RPGHuge.setTitle("-RPGModelHuge-");
							break;
							}
					break;
					
			}
		}
		if(e.getSource()==difficulty1){
			difficulty = 0;
		}
		if(e.getSource()==difficulty2){
			difficulty = 1;
		}
		if(e.getSource()==difficulty3){
			difficulty = 2;
		}
	}
}