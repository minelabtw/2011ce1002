package fp.s100502001;
import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.LineBorder;
public class Quit extends JPanel{
	private int Xcoor=0;
	private Timer setStr=new Timer(100, new Str_style());
	private boolean forward=true;
	private AudioClip cheer=Applet.newAudioClip(getClass().getResource("cheer.wav"));; //宣告//音效
	private AudioClip back=Applet.newAudioClip(getClass().getResource("iwillbeback.wav"));; //宣告
	
	public Quit(){
		cheer.loop();
		cheer.play();
		back.loop();
		back.play();
		setStr.start();
		
	}
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if(forward){
			Xcoor+=5;
			if(Xcoor>getWidth()-50){ //到了最左邊
				Xcoor=getWidth()-50;
				forward=false;
			}
		}
		else{
			Xcoor-=5;
			if(Xcoor<0){ //到了最右邊
				Xcoor=0;
				forward=true;
			}
		}
		Toolkit tk=Toolkit.getDefaultToolkit(); //Image不能直接打路徑
		Image image=tk.getImage("image/quit.jpg");
		g.setFont(new Font("TimesRoman", Font.BOLD, 48));
		g.setColor(new Color((int)(Math.random()*256),(int)(Math.random()*256),(int)(Math.random()*256)));
		g.drawImage(image,0,0,getWidth(),getHeight(),this);
		g.drawString("Bye Bye see you next time!!", Xcoor, (int)(getHeight()*0.3));
	}
	
	class Str_style implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			repaint();
		}
		
	}
}
