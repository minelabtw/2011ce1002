package fp.s100502001;
import java.io.*;
import java.awt.*;

import javax.swing.*;
public class Player /*extends JPanel*/{ //人物畫在這板面
	protected String name;
	protected String slogon;
	protected int score;
	protected File file=new File("data.txt");
	protected PrintWriter output;
	protected Toolkit tkplayer=Toolkit.getDefaultToolkit(); //Image不能直接打路徑
	protected Image imgplayer;
	public Player(String img_url,String n ,String s){
		imgplayer=tkplayer.getImage(img_url);
		name=n;
		slogon=s;
	} 
	//可以把姓名，分數存到file I/O 裡頭 
	public void setName(String N){
		name=N;
	}
	public void setSlogon(String S){
		slogon=S;
	}
	public void setScore(int reward) throws Exception{
		score=reward;
		StoreData(); //得到最終分數後，即把人名與分數存到file I/O 裡面
	}
	public String getName(){
		return name;
	}
	public String getSlogon(){
		return slogon;
	}
	public int getScore(){
		return score;
	}
	public void StoreData() throws Exception{
		
		if(file.exists()){
			System.out.println("The file already exists");
		}
		output=new PrintWriter(file);
		output.println(name+Integer.toString(score)); //一組資料"name+score",輸入儲存之後換行
		output.close(); //檔案用完要關掉唷
	}
	public Image getimg(){
		return imgplayer;
	}
	
}
