package fp.s100502001;
import javax.naming.InitialContext;
import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.ImageObserver;
public class VolleyballPath extends JPanel{
	//球的路線與背景圖
	//球的位置 //皮卡丘位置  //波妞位置
	private int VBx=787,VBy=99,Comx=738,Comy=248,Userx=196,Usery=248; 
	private final int[] init_position={VBx ,VBy};//發球位置
	private final int[] init_user={Userx,Usery};
	private final int[] init_comp={Comx,Comy};
	private int speedX=1,speedY=1;
	private int width,height,center;
	private int user_W,user_H,comp_W,comp_H,vb_W,vb_H;
	private int user_score=0,comp_score=0,time=30,score=5;//一局的時間、積分
	private int M_second=16;//user choose the ball's speed
	private int[] Xground;
	private int[] Yground;
	private boolean start=false,gameover=false; //開始遊戲與否
	private boolean userUP=true,compUP=true;
	private char vertical='D';//水平移動，預設向下
	private char horizontal='!';//鉛直移動
	private char center_line='C';
	private User user; //把玩家加在這個panel上
	private Computer computer=new Computer("image/image3.png","ㄎㄎ","加油喔");//把電腦方加在這個panel上
	private Timer ballmove;//球的移動
	private Timer setTimer= new Timer(1000, new SetTimer());//定時
	private Timer userTimer=new Timer(7,new UserJump()); //user jump
	private Timer compTimer=new Timer(7,new CompJump()); //computer jump
	//private UserWin userwin;
	//private CompWin compwin;
	//private Safe safe;
	public VolleyballPath(String name ,String slogon){
		ballmove= new Timer(6,new BallMove());
		user=new User("image/image4.png",name,slogon);
		Xground=new int[4];//場地範圍
		Yground=new int[4];
		setLayout(null);
	}
	public void setScore(int s){ //一局要幾分
		score=s;
	}
	public void setTime(int t){
		time=t;
	}
	public void Tostart(boolean s){ //開始遊戲
		start=s;
		gameover=false;
		Clear();
	}
	public void Tostop(){ //遊戲暫停
		ballmove.stop();
		setTimer.stop();
	}
	public void setSpeed(int ms){
		M_second=ms;
		ballmove.setDelay(M_second);
	}
	public void Clear(){
		time=30;
		user_score=0;
		comp_score=0;
		
	}
	/* (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		width=getWidth();
		height=getHeight();
		if(start){ //當按下start遊戲才開始
			setTimer.start();
			ballmove.start();
			start=false;
		}
		if(!gameover){
			Toolkit tkback=Toolkit.getDefaultToolkit(); //Image不能直接打路徑
			Image imgback=tkback.getImage("image/vbbackground.jpg");//背景
			g.drawImage(imgback,0,0,getWidth(),getHeight(),this);
			g.setColor(new Color(130,210,30));//場地
			Xground[0]=(int)(getWidth()*0.15);//左上
			Xground[1]=(int)(getWidth()*0.95); //右上
			Xground[2]=(int)(getWidth()*0.85); //左下
			Xground[3]=(int)(getWidth()*0.05); //右下
			Yground[0]=(int)(getHeight()*0.6);//左上
			Yground[1]=(int)(getHeight()*0.6);//右上
			Yground[2]=(int)(getHeight()*0.75);//左下
			Yground[3]=(int)(getHeight()*0.75);//右下
			g.fillPolygon(Xground, Yground, Xground.length);
			g.setColor(new Color(250,10,200));
			g.fillRect((int)(getWidth()/2), (int)(getHeight()/3), (int)(getWidth()*0.01), (int)(getHeight()*0.35));//center line
			center=(int)(getWidth()/2)+(int)(getWidth()*0.01/2);//中線
			int radius1=(int)(Math.min(getWidth(), getHeight())*0.1);
			int radius2=(int)(Math.min(getWidth(), getHeight())*0.08);
			int radius3=(int)(radius1*0.6);
			user_W=(int)(getWidth()*0.08); //user範圍
			user_H=(int)(getHeight()*0.15);
			comp_W=(int)(getWidth()*0.08);//computer範圍
			comp_H=(int)(getHeight()*0.15);
			vb_W=(int)(getWidth()*0.03);//ball範圍
			vb_H=(int)(getHeight()*0.05);
			//畫user上去
			g.drawImage(user.getimg(), Userx, Usery, user_W, user_H, this);
			g.setColor(Color.blue);
			g.drawRect(Userx, Usery, user_W, user_H);
			g.setColor(new Color((int)(Math.random()*256),(int)(Math.random()*256),(int)(Math.random()*256)));
			g.setFont(new Font("TimesRoman",Font.BOLD,15));
			g.drawString(user.name, (int)(getWidth()*0.05),(int)(getHeight()*0.06));
			g.drawString("我瞎打: "+user.slogon, (int)(getWidth()*0.05),(int)(getHeight()*0.1));
			//畫computer上去
			g.drawImage(computer.getimg(), Comx, Comy, comp_W,user_H, this);
			g.setColor(Color.yellow);
			g.drawRect(Comx, Comy, comp_W, comp_H);
			g.setColor(new Color((int)(Math.random()*256),(int)(Math.random()*256),(int)(Math.random()*256)));
			g.drawString(computer.name, (int)(getWidth()*0.7),(int)(getHeight()*0.06));
			g.drawString("我瞎跳: "+computer.slogon, (int)(getWidth()*0.7),(int)(getHeight()*0.1));
			//顯示時間、分數	
			g.setColor(Color.cyan);
			g.fillOval((int)(getWidth()*0.5-radius1), (int)(getHeight()*0.15-radius1), radius1*2, radius1*2);
			g.setColor(Color.lightGray);
			g.fillOval((int)(getWidth()*0.5-radius3), (int)(getHeight()*0.15-radius3), radius3*2, radius3*2);
			g.setColor(new Color((int)(Math.random()*256),(int)(Math.random()*256),(int)(Math.random()*256)));
			g.setColor(Color.cyan);
			g.fillOval((int)(getWidth()*0.5-radius1), (int)(getHeight()*0.85-radius1), radius1*2, radius1*2);
			g.setColor(new Color((int)(Math.random()*256),(int)(Math.random()*256),(int)(Math.random()*256)));
			g.fillOval((int)(getWidth()*0.5-radius3), (int)(getHeight()*0.85-radius3), radius3*2, radius3*2);
			g.setColor(Color.black);
			g.setFont(new Font("Serif",Font.BOLD,40));
			g.drawString(Integer.toString(time), (int)(getWidth()/2-radius1*0.5), (int)(getHeight()*0.17));
			g.drawString(Integer.toString(score), (int)(getWidth()*0.51-radius3), (int)(getHeight()*0.85));
			g.setFont(new Font("Serif",Font.BOLD,20));
			g.drawString("分/局", (int)(getWidth()*0.5-radius3), (int)(getHeight()*0.89));
			g.setColor(new Color((int)(Math.random()*256),(int)(Math.random()*256),(int)(Math.random()*256)));	
			g.drawOval((int)(getWidth()*0.7), (int)(getHeight()*0.8), radius2*2, radius2*2);
			g.setColor(new Color((int)(Math.random()*256),(int)(Math.random()*256),(int)(Math.random()*256)));
			g.fillArc((int)(getWidth()*0.7), (int)(getHeight()*0.8), radius2*2, radius2*2,0,30);
			g.fillArc((int)(getWidth()*0.7), (int)(getHeight()*0.8), radius2*2, radius2*2,90,30);
			g.fillArc((int)(getWidth()*0.7), (int)(getHeight()*0.8), radius2*2, radius2*2,180,30);
			g.fillArc((int)(getWidth()*0.7), (int)(getHeight()*0.8), radius2*2, radius2*2,270,30);
			g.setColor(new Color((int)(Math.random()*256),(int)(Math.random()*256),(int)(Math.random()*256)));
			g.setFont(new Font("TimesRoman",Font.BOLD,30));
			g.drawString(comp_score+"分", (int)(getWidth()*0.7+radius2*0.5), (int)(getHeight()*0.9));
			g.drawOval((int)(getWidth()*0.2), (int)(getHeight()*0.8), radius2*2, radius2*2);
			g.fillArc((int)(getWidth()*0.2), (int)(getHeight()*0.8), radius2*2, radius2*2,30,60);
			g.fillArc((int)(getWidth()*0.2), (int)(getHeight()*0.8), radius2*2, radius2*2,120,60);
			g.fillArc((int)(getWidth()*0.2), (int)(getHeight()*0.8), radius2*2, radius2*2,210,60);
			g.fillArc((int)(getWidth()*0.2), (int)(getHeight()*0.8), radius2*2, radius2*2,300,60);
			g.setColor(Color.black);
			g.drawString(user_score+"分", (int)(getWidth()*0.2+radius2*0.45), (int)(getHeight()*0.9));
			Toolkit tkvb=Toolkit.getDefaultToolkit(); //Image不能直接打路徑
			Image imgvb=tkvb.getImage("image/VB.png");//球
			g.drawImage(imgvb,VBx,VBy,vb_W,vb_H,this);
			revalidate();
		}
		if(gameover){
			//gameover=false;
			
			Tostop();		
			if(user_score>=score&&user_score<score){
				System.out.println("win");
				/*userwin=new UserWin();
				userwin.setSize(1000, 600);
				userwin.setBounds(0,0, 1000,600);
				add(userwin);
				userwin.repaint();*/
				Toolkit tkback=Toolkit.getDefaultToolkit(); //Image不能直接打路徑
				Image imgback=tkback.getImage("image/userwin.jpg");//背景
				g.drawImage(imgback,0,0,getWidth(),getHeight(),this);
				g.setFont(new Font("TimesRoman", Font.BOLD, 48));
				g.setColor(new Color((int)(Math.random()*256),(int)(Math.random()*256),(int)(Math.random()*256)));
				g.drawString("YOU WIN", (int)(getWidth()*0.5), (int)(getHeight()*0.3));	
			}
			else if(comp_score>=score){
				System.out.println("lose");
				/*compwin=new CompWin();
				compwin.setSize(1000, 600);
				compwin.setBounds(0,0, 1000,600);
				add(compwin);
				compwin.repaint();*/
				Toolkit tkback=Toolkit.getDefaultToolkit(); //Image不能直接打路徑
				Image imgback=tkback.getImage("image/compwin.jpg");//背景
				g.drawImage(imgback,0,0,getWidth(),getHeight(),this);
				g.setFont(new Font("TimesRoman", Font.BOLD, 48));
				g.setColor(new Color((int)(Math.random()*256),(int)(Math.random()*256),(int)(Math.random()*256)));
				g.drawString("YOU LOSE", (int)(getWidth()*0.5), (int)(getHeight()*0.3));		}
			else {
				System.out.println("safe");
				/*safe=new Safe(); //不能在自己panel中移除再add一個新的panel??
				safe.setBounds(0,0,getWidth(),getHeight());
				add(safe);
				safe.repaint();*/
				Toolkit tkback=Toolkit.getDefaultToolkit(); //Image不能直接打路徑
				Image imgback=tkback.getImage("image/safe.jpg");//背景
				g.drawImage(imgback,0,0,getWidth(),getHeight(),this);
				g.setFont(new Font("TimesRoman", Font.BOLD, 48));
				g.setColor(new Color((int)(Math.random()*256),(int)(Math.random()*256),(int)(Math.random()*256)));
				g.drawString("SAFE", (int)(getWidth()*0.5), (int)(getHeight()*0.3));	
			}
			repaint();//重畫
		}
	}

	public boolean user_catchBall(){ //user若救到球
		if(VBx>=Userx && VBx<=Userx+user_W){ //X
			if((VBy+vb_H)>=Usery ){ //Y&& (VBy+vb_H)<=Usery+user_H*0.5
				return true;
			}
		}
		return false;
	}
	public boolean comp_catchBall(){ //computer若救到球
		if(VBx>=Comx && VBx<=Comx+comp_W){ //X
			if((VBy+vb_H)>=Comy){ //Y && (VBy+vb_H)<=Comy+comp_H*0.5
				return true;
			}
		}
		return false;
	}	
	public void setPath(){ 
		if(horizontal=='!') //restart to toss the ball
			ballmove.setDelay(17);//發球時速度稍慢一點
		else
			ballmove.setDelay(M_second);
		if(user_catchBall()){//user接到球
			if(user_score>=score)
				gameover=true;
			else 
				user_score++; //這裡有問題....人如果跟球重復碰到，則分數會一直加
			if((VBx>=Userx)&&(VBx<=Userx+user_W/3)){ //後1/3(最左)
				horizontal='L';
				vertical='U';
			}
			else if((VBx>Userx+user_W/3)&&(VBx<=Userx+user_W*(2/3))){ //中間1/3
				horizontal='@';
				vertical='U';
			}
			else{ //前1/3(最右)
				horizontal='R';
				if((VBy+vb_H)>=Usery+user_H*0.3)
					vertical='D';
				else 
					vertical='U';
			}
		}
		else if(comp_catchBall()){ //comp接到球
			if(comp_score>=score)
				gameover=true;
			else
				comp_score++;
			if((VBx>=Comx)&&(VBx<=Comx+comp_W/3)){ //前1/3(最左)
				horizontal='L';
				if((VBy+vb_H)>=Comy+comp_H*0.3)
					vertical='D';
				else 
					vertical='U';
			}
			else if((VBx>Comx+comp_W/3)&&(VBx<=Comx+comp_W*(2/3))){ //中間1/3
				horizontal='@';
				vertical='U';
			}
			else{ //後1/3(最右)
				horizontal='R';
				vertical='U';
			}
		}
		else if(center_line=='C'+'L'){//打到中線
			horizontal='L';
			center_line='@';
		}
		else if(center_line=='C'+'R'){
			horizontal='R';
			center_line='@';
		}
		switch (horizontal) {
			case 'L'://to left
				VBx-=speedX;
				break;
			case 'R'://to right
				VBx+=speedX;
				break;
			default://X座標不變
				break;
		}
		switch (vertical) {
			case 'U'://to upward
				VBy-=speedY;
				break;
			case 'D'://to downward
				VBy+=speedY;
			default://Y座標不變
				break;
		}		
	}
	public void setPosition(){ //if ball get the wall or ground 球到了邊界
		if(VBy<Yground[0]+vb_H*1.5){ //球未落地
			if(VBy<=0){//碰到上
				vertical='D';//downward
				VBy=0;
			}
			else if(VBx<=0){//碰到左
				horizontal='R';//to right
				VBx=0;
			}
			else if(VBx+vb_W>=getWidth()){//碰到右
				horizontal='L';//to left
				VBx=width-vb_W;
				//System.out.println("getWidth() + "+width); //getWidth=984
				//System.out.println("VBx= "+VBx); //怎麼會跑到一萬多= =  --->(若上一行寫成VB=getWudth()-vb_W);?? getWudth()不是定值??
			}
			//碰到中線的判斷不夠仔細
			else if(VBx+vb_W==center-(int)(getWidth()*0.01/2) && VBy>=(int)(getHeight()/3))//在user邊碰到線
				center_line='C'+'L'; //碰到中線
			else if(VBx==center+(int)(getWidth()*0.01/2) && VBy>=(int)(getHeight()/3)) //在computer邊碰到線
				center_line='C'+'R';
		}
		else{ //ball落地了
			VBx=init_position[0];
			VBy=init_position[1];
			Userx=init_user[0];
			Usery=init_user[1];
			Comx=init_comp[0];
			Comy=init_comp[1];
			horizontal='!'; //重發球時
			vertical='D';
			
				
		}
	}
	public void set_userX(int x){ //控制user移動
		if(x >= 0 && x+user_W < center){
			Userx=x;
		}
		else if(x+user_W>=center){ //user的最左邊
			Userx=center-user_W; 
		}
		else 
			Userx=0;//user的最右邊
	}
	public Timer UserJump(){
		return userTimer;
	}
	public void set_userY(){ //跳躍
		if(userUP){ //跳起來
			if(Usery+user_H>=Yground[0]/2)
				Usery--;
			else {
				Usery=(int)(Yground[0]/2);
				userUP=false;
			}
		}
		else{ //掉回去
			if(Usery<=init_user[1])
				Usery++;
			else{
				Usery=init_comp[1];
				userUP=true;
				userTimer.stop();
			}
		}
	}
	public void set_compX(int x){ //控制computer移動
		
		if(x > center && x< width-comp_W){
			Comx=x;
		}
		else if(x+comp_W>=width){ 
			Comx=width-comp_W;
		}
		else 
			Comx=center;
		
	}
	public Timer CompJump(){
		return compTimer;
	}
	public void set_compY(){ //跳躍
		if(compUP){ //跳起來
			if(Comy+comp_H>=Yground[0]/2)
				Comy--;
			else {
				Comy=(int)(Yground[0]/2);
				compUP=false;
			}
		}
		else{ //掉回去
			if(Comy<=init_comp[1])
				Comy++;
			else{
				Comy=init_comp[1];
				compUP=true;
				compTimer.stop();
			}
		}
		
		
	}
	public void set_vbX(int x){ //控制ball移動
		VBx=x;
		
	}
	public void set_vbY(int y){
		VBy=y;
		
	}
	public int get_userX(){ //得到user位置
		return Userx;
	}
	public int get_userY(){
		return Usery;
	}
	public int get_compX(){ //得到computer位置
		return Comx;
	}
	public int get_compY(){
		return Comy;
	}
	public int get_vbX(){ //得到ball位置
		return VBx;
	}
	public int get_vbY(){
		return VBy;
	}
	public int getTime(){
		return time;
	}
	public int getScore(){
		return score;
	}
	public User getUser(){
		return user;
	}
	public Computer getComp(){
		return computer;
	}
	class BallMove implements ActionListener{//球移動
		public void actionPerformed(ActionEvent e){
			setPosition();//判斷球是否到了邊界
			setPath();//判斷有無接到球
			repaint(); 
		}
	}
	class SetTimer implements ActionListener{ //倒數計時
		public void actionPerformed(ActionEvent e){	
			if(time<=0)
				gameover=true;
			else
				time--;
			repaint(); //因應每1秒圖就重新被repaint一次
		}
	}
	class UserJump implements ActionListener{ //人物跳躍
		public void actionPerformed(ActionEvent e){	
			set_userY();
			repaint(); 
		}
	}
	class CompJump implements ActionListener{ //人物跳躍
		public void actionPerformed(ActionEvent e){	
			set_compY();
			repaint();
		}
	}
}
/*class UserWin extends JPanel{
	public UserWin(){
		repaint();
	}
	protected void paintCoponent(Graphics g){
		super.paintComponent(g);
		Toolkit tkback=Toolkit.getDefaultToolkit(); //Image不能直接打路徑
		Image imgback=tkback.getImage("image/userwin.jpg");//背景
		g.drawImage(imgback,0,0,getWidth(),getHeight(),this);
	}
}
class CompWin extends JPanel{
	public CompWin(){
		repaint();
	}
	protected void paintCoponent(Graphics g){
		super.paintComponent(g);
		Toolkit tkback=Toolkit.getDefaultToolkit(); //Image不能直接打路徑
		Image imgback=tkback.getImage("image/compwin.jpg");//背景
		g.drawImage(imgback,0,0,getWidth(),getHeight(),this);
	}
}
class Safe extends JPanel{
	public Safe(){
		repaint();
	}
	protected void paintCoponent(Graphics g){
		super.paintComponent(g);
		Toolkit tkback=Toolkit.getDefaultToolkit(); //Image不能直接打路徑
		Image imgback=tkback.getImage("image/safe.jpg");//背景
		g.drawImage(imgback,0,0,getWidth(),getHeight(),this);
	}
}*/
