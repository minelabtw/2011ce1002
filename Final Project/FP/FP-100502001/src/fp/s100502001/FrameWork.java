package fp.s100502001;
import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.*;
import javax.swing.border.Border;

import org.omg.CORBA.PUBLIC_MEMBER;
public class FrameWork extends JPanel  implements KeyListener{
	
	private String[] startStr={"要開始了咩^.<","Start","Quit"};
	private String[] setStr={"5","50","100","30","60","90"};
	private String[] setSpeed={"3","4","5"};
	private String[] helpStr={"額外戰鬥力bonus","Help","Infomation"};
	private JComboBox game=new JComboBox(startStr);//開始遊戲
	private JComboBox set=new JComboBox(); //設定一局point、second
	private JComboBox help=new JComboBox(helpStr); //遊戲規則，作者介紹等
	//private JButton[] jbuser={new JButton("left"),new JButton("jump"),new JButton("right")};//user
	//private JButton[] jbusermove={new JButton("←"),new JButton("↑"),new JButton("→")};//控制user的移動
	private int per_time=30; //一局時間
	private int per_score=5; //一局分數
	private VolleyballPath volleyball;
	private AudioClip main=Applet.newAudioClip(getClass().getResource("main.wav"));;
	protected Data data=new Data();//得到user的分數
	public FrameWork(String name ,String slogon) throws Exception{//再加上左上角的功能按建
		main.loop();
		main.play();
		volleyball=new VolleyballPath(name,slogon);
		set.setForeground(Color.magenta);
		set.addItem("Set 梗咯!!");
		set.addItem("一局分數");
		for(int i=0;i<setStr.length/2;i++)
			set.addItem(setStr[i]+" points");
		set.addItem("一局的時間");
		for(int i=setStr.length/2;i<setStr.length;i++)
			set.addItem(setStr[i]+" seconds");
		set.addItem("球速!!");
		for(int i=0;i<setSpeed.length;i++)
			set.addItem(setSpeed[i]+"毫秒");
		set.addItem("風雲榜"); //讀檔
		set.addItem("sounds on");
		set.addItem("sounds off"); 
		setLayout(new BorderLayout());
		JPanel setInfo=new JPanel();
		JPanel setPush=new JPanel();
		JPanel jpuser=new JPanel();
		JPanel jpcomp=new JPanel();
		jpuser.setLayout(new GridLayout(1,3,10,10));
		/*for(int i=0;i<jbuser.length;i++){//加上button(控制user)
			jbuser[i].setBackground(Color.red);
			jpuser.add(jbuser[i]);
		}
		jpcomp.setLayout(new GridLayout(1,3,10,10));
		for(int i=0;i<jbusermove.length;i++){//加上button(控制computer)
			jbusermove[i].setBackground(Color.blue);
			jpcomp.add(jbusermove[i]);
		}*/
		game.setForeground(Color.cyan);
		help.setForeground(Color.orange);
		setInfo.setLayout(new GridLayout(1,3,30,30));
		setInfo.add(game);
		setInfo.add(set);
		setInfo.add(help);
		
		
		setPush.add(jpuser,BorderLayout.WEST);
		//setPush.add(jbkey,BorderLayout.CENTER);
		setPush.add(jpcomp,BorderLayout.EAST);
		this.add(setInfo,BorderLayout.NORTH);
		this.add(volleyball,BorderLayout.CENTER);
		this.add(setPush,BorderLayout.SOUTH);
		game.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				int choice=game.getSelectedIndex();
				switch(choice){
					case 1: //當按下開始 球開始動
						volleyball.Tostart(true);
						break;
				
					case 2://離開
						main.stop();
						volleyball.Tostop();
						removeAll();
						add(new Quit());
						revalidate();
						break;
					default:
						break;
				}
			}
		});
		game.addKeyListener(this);
		set.addItemListener(new ItemListener() { //設定一局的時間與分數
			public void itemStateChanged(ItemEvent e) {
				int choice=set.getSelectedIndex();
				switch(choice){
					case 2:
						per_score=Integer.parseInt(setStr[0]);
						break;
					case 3:
						per_score=Integer.parseInt(setStr[1]);
						break;
					case 4:
						per_score=Integer.parseInt(setStr[2]);
						break;
					case 6:
						per_time=Integer.parseInt(setStr[3]);
						break;
					case 7:
						per_time=Integer.parseInt(setStr[4]);
						break;
					case 8:
						per_time=Integer.parseInt(setStr[5]);
						break;
					case 10:
						volleyball.setSpeed(Integer.parseInt(setSpeed[0]));
						break;
					case 11:
						volleyball.setSpeed(Integer.parseInt(setSpeed[1]));
						break;
					case 12:
						volleyball.setSpeed(Integer.parseInt(setSpeed[2]));
						break;
					case 13://風雲榜
						//JOptionPane.showMessageDialog(null,data.getdata(),"風雲榜",JOptionPane.INFORMATION_MESSAGE);
						break;
					case 14:
						main.play();
						break;
					case 15:
						main.stop();
						break;
					default:
						break;
					
				}
				volleyball.setScore(per_score);//設定一局的分數
				volleyball.setTime(per_time); //設定一局的時間
				volleyball.repaint();	
				revalidate();
				//System.out.println("choice = "+choice);
			}
		});
		set.addKeyListener(this);
		help.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				int choice=help.getSelectedIndex();
				String help="一開始請選擇左上角(要開始了咩)的start。\n" +
						"設定一局的時間(預設60s)與分數(預設5min)\n" +
						"也可設定球速唷\n"+
						"雙方球落地減少積分or即時救援得分。\n" +
						"若積分歸零則結束比賽，或者時間限制內積分最多的一方獲勝。\n" +
						"玩家與電腦對抗的單機模式\n";
				String info="作者: 羅雨晴\n" +
						"綽號: 波波\n"+
						"學號: 100502001\n"+
						"班級: 資工一A\n"+		
						"興趣: volleyball+大睡一場\n";
				
				switch(choice){
					case 1:
						JOptionPane.showMessageDialog(null,help,"Help",JOptionPane.INFORMATION_MESSAGE);
						break;
					case 2:
						JOptionPane.showMessageDialog(null,info,"Information",JOptionPane.INFORMATION_MESSAGE);
						break;
					default:
						break;
					
				}
				
			}
			
		});
		help.addKeyListener(this);
	}
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		switch(e.getKeyCode()){
			case KeyEvent.VK_I:
				volleyball.CompJump().start();
				break;
			case KeyEvent.VK_J:
				volleyball.set_compX(volleyball.get_compX()-5);
				volleyball.repaint();
				break;
			case KeyEvent.VK_L:
				volleyball.set_compX(volleyball.get_compX()+5);
				volleyball.repaint();
				break;
			case KeyEvent.VK_A:
				volleyball.set_userX(volleyball.get_userX()-5);
				volleyball.repaint();
				break;
			case KeyEvent.VK_W:
				volleyball.UserJump().start();
				break;
			case KeyEvent.VK_D:
				volleyball.set_userX(volleyball.get_userX()+5);
				volleyball.repaint();
				break;
			default:
				break;
		}		
	}
	public VolleyballPath getVB(){
		return volleyball;
	}
}
