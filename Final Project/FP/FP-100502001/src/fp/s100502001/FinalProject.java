package fp.s100502001;
import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MoveAction;
public class FinalProject extends JFrame{
	private JButton jbstart=new JButton("Start");
	private JButton jbquit=new JButton("Quit");
	private JButton jbset=new JButton("SetInfo");
	private JPanel jlinput=new JPanel();//輸入個人資料
	private JPanel jlchoose=new JPanel(); //一開始的選單
	private Move jpmove; //設定背景圖
	private JTextField jtname=new JTextField();//輸入user姓名
	private JTextField jtslogon=new JTextField(); //輸入user口號
	private static FinalProject frame=new FinalProject();
	private String name="";
	private String slogon=""; 
	private JButton jbsure=new JButton();
	//=new JButton(new ImageIcon("image/image5.jpg"));
	private Timer setball=new Timer(100, new Ball());
	private FrameWork jpframework;
	//private VolleyballPath vb;
	private AudioClip menu=Applet.newAudioClip(getClass().getResource("rainball.wav")); //音效;
	private AudioClip bling=Applet.newAudioClip(getClass().getResource("whatcanido.wav")); //音效;
	public FinalProject(){
		
		menu.play();
		menu.loop();
		jpmove=new Move();
		setLayout(new BorderLayout());
		jlchoose.setLayout(new FlowLayout());
		jlchoose.setBorder(new LineBorder(Color.cyan));
		jlchoose.setBorder(new TitledBorder("Have fun^.<!!"));
		jbstart.setBounds(10, 10, 20, 20);//設定button位置與大小(不能改耶= =)
		jbstart.setBackground(Color.blue);//設定button顏色
		jbset.setBounds(40, 10, 20, 20);
		jbset.setBackground(Color.green);
		jbquit.setBounds(70, 10, 20, 20);
		jbquit.setBackground(Color.red);
		jlchoose.add(jbstart,FlowLayout.LEFT);
		jlchoose.add(jbset,FlowLayout.CENTER); //輸入個人名字與slogon
		jlchoose.add(jbquit,FlowLayout.RIGHT);
		add(jlchoose,BorderLayout.NORTH);
		add(jpmove,BorderLayout.CENTER);
		jbstart.addActionListener(new ActionListener(){ //若按下開始，則進入遊戲
		
			public void actionPerformed(ActionEvent e) {
				menu.stop();
				setball.stop(); //主畫面的動畫
				bling.stop();
				remove(jlchoose);
				remove(jpmove);
				remove(jlinput);
				revalidate();
				try { 
					jpframework=new FrameWork(name, slogon);
					//vb=jpframework.getVB();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				frame.add(jpframework,BorderLayout.CENTER);
				jpframework.revalidate();
			
			}
			
		});
		jbquit.addActionListener(new ActionListener(){ //按下離開...

			public void actionPerformed(ActionEvent e) {
				menu.stop();
				setball.stop();
				bling.stop();
				remove(jlchoose);
				remove(jpmove);
				remove(jlinput);
				revalidate();
				frame.add(new Quit());
				revalidate();
				
			}
			
		});
		//設定個人資料...
		jbsure.setForeground(new Color((int)(Math.random()*256),(int)(Math.random()*256),(int)(Math.random()*256)));
		jbsure.setFont(new Font("TimesRoman",Font.BOLD,36));
		jbsure.setText("確認");//(jlsure,BorderLayout.CENTER);	
		jbsure.setIcon(new ImageIcon("image/image5.jpg"));
		jbsure.setBounds(0, getHeight(), 290, 164);
		jlinput.setLayout(new GridLayout(3,2,10,10));
		JLabel jlname=new JLabel("Name: ");
		jlname.setFont(new Font("TimesRoman",Font.ITALIC,60));
		jlname.setForeground(Color.blue);
		jlinput.add(jlname);
		jlinput.add(jtname);
		JLabel jlslogon=new JLabel("You want to say: ");
		jlslogon.setFont(new Font("TimesRoman",Font.ITALIC,60));
		jlslogon.setForeground(Color.yellow);
		jlinput.add(jlslogon);
		jlinput.add(jtslogon);
		jlinput.add(jbsure);
		jbset.addActionListener(new ActionListener(){ 
			//得到JTextfield的資料
			public void actionPerformed(ActionEvent e) {
				menu.stop();
				bling.play();
				setball.stop();
				remove(jpmove);
				frame.add(jlinput);
				revalidate();
				jbsure.addActionListener(new ActionListener() { //若不再按一個button來觸發的話，讀取不到資料耶
					public void actionPerformed(ActionEvent e) {
						name=jtname.getText(); //使用者輸入名字
						slogon=jtslogon.getText(); //使用者輸入想說的話
						
					}
				});
				
			}
			
		});	
		
	}
	
	public static void main(String[] args){
		
		frame.setTitle("Get Fighting");
		frame.setSize(1000,600);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
		
	}
	class Move extends JPanel{
		
		private int Xcoor=0,Ycoor=0;
		private boolean forward=false;
		private boolean downward=true;
		public Move(){
			setball.start();
		}
		protected void paintComponent(Graphics g){
			super.paintComponent(g);
			if(forward){
				Xcoor+=5;
				if(Xcoor>getWidth()-70){ //到了最左邊
					Xcoor=getWidth()-70;
					forward=false;
				}
			}
			else{
				Xcoor-=5;
				if(Xcoor<0){ //到了最右邊
					Xcoor=getWidth()-70;
					forward=false;
				}
			}
			if(downward){
				Ycoor+=5;
				if(Ycoor>getHeight()-150){
					Ycoor=getHeight()-150;
					downward=false;
				}
			}
			else {
				Ycoor-=5;
				if(Ycoor<0){
					Ycoor=0;
					downward=true;
				}
			}
			
			Toolkit tk=Toolkit.getDefaultToolkit(); //Image不能直接打路徑
			Image image=tk.getImage("image/background.jpg");
			g.drawImage(image,0,0,getWidth(),getHeight(),this);
			Toolkit tkball=Toolkit.getDefaultToolkit(); //Image不能直接打路徑
			Image img_ball1=tkball.getImage("image/image16.png");
			g.drawImage(img_ball1,Xcoor,0,this);
			Image img_ball2=tkball.getImage("image/image14.png");
			g.drawImage(img_ball2,(int)(getWidth()-69),Ycoor,this);
			Image img_ball3=tkball.getImage("image/image15.png");
			g.drawImage(img_ball3,0,Ycoor,this);
			String illustrate="在進入遊戲後，每次設定完請記得按下Keyboard才可控制人物";
			g.setFont(new Font("TimesRoman",Font.BOLD,30));
			g.setColor(Color.red);
			g.drawString(illustrate, Xcoor, (int)(getHeight()*0.9));
			g.setColor(Color.blue);
			g.drawString("靜", 30, Ycoor);
			g.setColor(Color.yellow);
			g.drawString("忍", getWidth()-33, Ycoor);
		}
	}
	class Ball implements ActionListener{ //動畫
		public void actionPerformed(ActionEvent e){	
			repaint(); //因應每1秒圖就重新被repaint一次
		}
	}
	

}

