import java.awt.*;

import javax.swing.*;

public class home extends JPanel{
	JButton start = new JButton("一般模式");
	JButton god = new JButton("困難模式");
	JButton shop = new JButton("商店");
	JButton exit = new JButton("結束");
	ImageIcon mainimg=new ImageIcon("pic/main.png");
	Font f = new Font("SansSerif", Font.BOLD, 20);
	public home()//首頁
	{
		setLayout(new GridLayout(3,1));
		JPanel bt = new JPanel(new GridLayout(1,4,10,5));
		start.setFont(f);
		god.setFont(f);
		shop.setFont(f);
		exit.setFont(f);
		bt.add(start);
		bt.add(god);
		bt.add(shop);
		bt.add(exit);
		bt.setOpaque(false);
		add(new JLabel());
		add(bt);
		add(new JLabel());
		exit.setContentAreaFilled(false);
		start.setContentAreaFilled(false);
		shop.setContentAreaFilled(false);
		god.setContentAreaFilled(false);
		exit.setBorder(null);
		shop.setBorder(null);
		god.setBorder(null);
		start.setBorder(null);
	}
	protected void paintComponent(Graphics g) {

		super.paintComponent(g);
		g.drawImage(mainimg.getImage(), 0, 0, 585, 585, this);
	}
}
