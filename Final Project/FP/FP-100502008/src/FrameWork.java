import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.net.MalformedURLException;
import java.util.Scanner;

import javax.swing.*;


public class FrameWork extends JFrame implements ActionListener {
	boolean restart=false;
	drawsnake s = new drawsnake();
	home h = new home();
	shop sh;
	int total;
	int buy1,buy2,buy3;
	int choice=1;
	String code;
	File file1 = new File("shop.txt");
	File file = new File("score.txt");
	CardLayout cardlayout = new CardLayout();
	JPanel p1 = new JPanel(cardlayout);
	JPanel snake = new JPanel(new BorderLayout());
	Timer timer = new Timer(100, new TimerListener());
	int type=0;
	JPanel scorep=new JPanel(new FlowLayout(FlowLayout.RIGHT));
	public FrameWork() throws Exception {//絪胯┮Τ礶
		total=read();
		readshop();
		sh=new shop(buy1,buy2,buy3);
		s.setBackground(Color.GREEN); 
		scorep.add(s.totalscorel);
		scorep.add(s.scorel);
		scorep.setBackground(new Color(106,30,21)); 
		snake.add(s,BorderLayout.CENTER);
		snake.add(scorep,BorderLayout.SOUTH); 
		sh.score.setText("瞷Τだ计 : "+total);
		p1.add(h, "1");
		p1.add(snake, "2");
		p1.add(sh, "3");
		cardlayout.show(p1, "1");
		s.addMouseListener(new MouseAdapter() { public void
		 mousePressed(MouseEvent e) { s.requestFocus(); } });
		s.addKeyListener(new KeyAdapter()
		{
			public void keyPressed(KeyEvent e)
			{
				 s.requestFocus();
			}
		});
		h.exit.addActionListener(this);
		h.start.addActionListener(this);
		h.shop.addActionListener(this);
		h.god.addActionListener(this);
		sh.back.addActionListener(this);
		sh.buy2.addActionListener(this);
		sh.buy3.addActionListener(this);
		sh.buy4.addActionListener(this);
		sh.use1.addActionListener(this);
		sh.use2.addActionListener(this);
		sh.use3.addActionListener(this);
		sh.use4.addActionListener(this);
		sh.point.addActionListener(this);
		add(p1);
	}

	public void actionPerformed(ActionEvent e) {//∕﹚秙
		if (e.getSource() == h.start) {
			s.setIcon(choice);
			cardlayout.show(p1, "2");
			s.timer.setDelay(100);
			timer.start();
		}
		else if(e.getSource()==h.god)
		{
			s.setIcon(choice);
			cardlayout.show(p1, "2");
			s.timer.setDelay(50);
			timer.start();
		}
		else if(e.getSource()==h.shop)
		{
			sh.score.setText("瞷Τだ计 : "+total);
			cardlayout.show(p1, "3");
		}
		else if(e.getSource()==h.exit)
		{
			System.exit(0);
		}
		else if(e.getSource()==sh.back)
		{
			cardlayout.show(p1, "1");
			try {
				writeshop();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		else if(e.getSource()==sh.buy2)
		{
			if(total>=5000)
			{
				total-=5000;
				sh.score.setText("瞷Τだ计 : "+total);
				sh.bt2.remove(sh.buy2);
				sh.bt2.remove(sh.nuse2);
				sh.bt2.add(sh.nbuy2);
				sh.bt2.add(sh.use2);
				sh.revalidate();
				sh.repaint();
				buy1=1;
				try {
					writeshop();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
		else if(e.getSource()==sh.buy3)
		{
			if(total>=5000)
			{
				total-=5000;
				sh.score.setText("瞷Τだ计 : "+total);
				sh.bt3.remove(sh.buy3);
				sh.bt3.remove(sh.nuse3);
				sh.bt3.add(sh.nbuy3);
				sh.bt3.add(sh.use3);
				sh.revalidate();
				sh.repaint();
				buy2=1;
				try {
					writeshop();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
		else if(e.getSource()==sh.buy4)
		{
			if(total>=5000)
			{
				total-=5000;
				sh.score.setText("瞷Τだ计 : "+total);
				sh.bt4.remove(sh.buy4);
				sh.bt4.remove(sh.nuse4);
				sh.bt4.add(sh.nbuy4);
				sh.bt4.add(sh.use4);
				sh.revalidate();
				sh.repaint();
				buy3=1;
				try {
					writeshop();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
		else if(e.getSource()==sh.use1)
		{
			choice=1;
		}
		else if(e.getSource()==sh.use2)
		{
			choice=2;
		}
		else if(e.getSource()==sh.use3)
		{
			choice=3;
		}
		else if(e.getSource()==sh.use4)
		{
			choice=4;
		}
		else if(e.getSource()==sh.point)
		{
			code=JOptionPane.showInputDialog(null, "纗");
			int c=Integer.parseInt(code);
			if(code.equals("123"))
			{
				total+=5000;
				sh.score.setText("瞷Τだ计 : "+total);
				sh.revalidate();
				sh.repaint();
			}
			else
			{
				
			}
		}
	}
	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(s.gameend)
			{
				cardlayout.show(p1, "1");
				s.timer.stop();
				s.gameend=false;
				 total+=s.totalscore;
				 s.totalscore=0;
				 s.totalscorel.setText("Total : "+s.totalscore);
				 try {
						write();
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				 sh.score.setText("瞷Τだ计 : "+total);
			}
			if(s.death)
			{
				cardlayout.show(p1, "1");
				s.timer.stop();
				s.death=false;
				total+=s.totalscore;
				s.totalscore=0;
				s.totalscorel.setText("Total : "+s.totalscore);
				try {
					write();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				sh.score.setText("瞷Τだ计 : "+total);
			}
			if(s.stageclear)
			{
				scsound();
				s.stageclear=false;
			}
		}
	}
	void readshop()throws Exception
	{
		Scanner input1=new Scanner(file1);
		buy1=input1.nextInt();
		buy2=input1.nextInt();
		buy3=input1.nextInt();
		input1.close();
	}
	int read()throws Exception
	{
		Scanner input=new Scanner(file);
		return input.nextInt();
	}
	void write()throws Exception
	{
		java.io.PrintWriter output= new java.io.PrintWriter("score.txt");
		output.print(total);
		output.close();
	}
	void writeshop()throws Exception
	{
		java.io.PrintWriter output= new java.io.PrintWriter(file1);
		output.print(buy1+" "+buy2+" "+buy3);
		output.close();
	}
	void scsound()
	{
		try {
			AudioClip   audio=Applet.newAudioClip(new File("stageclear.mid").toURL());
			audio.play();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} 
	}
}
