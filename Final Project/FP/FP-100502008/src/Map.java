import javax.swing.*;

public class Map {
	int type;
	int wall1count;
	int wall2count;
	int wall3count;
	int hole1count=2;
	int hole2count=2;
	ImageIcon wall1 = new ImageIcon("pic/wall1.png");
	ImageIcon wall2 = new ImageIcon("pic/wall2.png");
	ImageIcon wall3 = new ImageIcon("pic/wall3.png");
	ImageIcon hole1 = new ImageIcon("pic/hole1.png");
	ImageIcon hole2 = new ImageIcon("pic/hole2.png");
	int[] hole1x = new int[2];
	int[] hole1y = new int[2];
	int[] hole2x = new int[2];
	int[] hole2y = new int[2];
	int[] wall1x = new int[100];
	int[] wall1y = new int[100];
	int[] wall2x = new int[50];
	int[] wall2y = new int[50];
	int[] wall3x = new int[50];
	int[] wall3y = new int[50];
	public Map(int num)
	{
		type=num;
		if(type==1)
		{
			wall1count=70;
			wall2count=20;
			wall3count=20;
		}
		else if(type==2)
		{
			wall1count=72;
			wall2count=26;
			wall3count=26;
		}
		else if(type==3)
		{
			wall1count=0;
			wall2count=0;
			wall3count=0;
		}
		else if(type==4)
		{
			wall1count=72;
			wall2count=30;
			wall3count=30;
			drawHole1();
			drawHole2();
		}
		drawWall1();
		drawWall2();
		drawWall3();
	}
	void drawWall1()
	{
		if(type==1)
		{
			wall1x[0]=120;
			wall1y[0]=300;
			for(int i =1;i<18;i++)
			{
				wall1x[i]=wall1x[i-1]+15;
				wall1y[i]=wall1y[i-1];
			}
			wall1x[18]=450;
			wall1y[18]=210;
			for(int i =19;i<36;i++)
			{
				wall1x[i]=wall1x[i-1]-15;
				wall1y[i]=wall1y[i-1];
			}
			wall1x[36]=0;
			wall1y[36]=405;
			for(int i =37;i<53;i++)
			{
				wall1x[i]=wall1x[i-1]+15;
				wall1y[i]=wall1y[i-1];
			}
			wall1x[53]=570;
			wall1y[53]=105;
			for(int i =54;i<wall1count;i++)
			{
				wall1x[i]=wall1x[i-1]-15;
				wall1y[i]=wall1y[i-1];
			}
		}
		else if(type==2)
		{
			wall1x[0]=75;
			wall1y[0]=75;
			for(int i =1;i<10;i++)
			{
				wall1x[i]=wall1x[i-1]+15;
				wall1y[i]=wall1y[i-1];
			}
			wall1x[10]=495;
			wall1y[10]=75;
			for(int i =11;i<20;i++)
			{
				wall1x[i]=wall1x[i-1]-15;
				wall1y[i]=wall1y[i-1];
			}
			wall1x[20]=150;
			wall1y[20]=150;
			for(int i =21;i<28;i++)
			{
				wall1x[i]=wall1x[i-1]+15;
				wall1y[i]=wall1y[i-1];
			}
			wall1x[28]=420;
			wall1y[28]=150;
			for(int i =29;i<36;i++)
			{
				wall1x[i]=wall1x[i-1]-15;
				wall1y[i]=wall1y[i-1];
			}
			wall1x[36]=75;
			wall1y[36]=435;
			for(int i =37;i<46;i++)
			{
				wall1x[i]=wall1x[i-1]+15;
				wall1y[i]=wall1y[i-1];
			}
			wall1x[46]=495;
			wall1y[46]=435;
			for(int i =47;i<56;i++)
			{
				wall1x[i]=wall1x[i-1]-15;
				wall1y[i]=wall1y[i-1];
			}
			wall1x[56]=150;
			wall1y[56]=360;
			for(int i =57;i<64;i++)
			{
				wall1x[i]=wall1x[i-1]+15;
				wall1y[i]=wall1y[i-1];
			}
			wall1x[64]=420;
			wall1y[64]=360;
			for(int i =65;i<72;i++)
			{
				wall1x[i]=wall1x[i-1]-15;
				wall1y[i]=wall1y[i-1];
			}
		}
		else if(type==4)
		{
			wall1x[0]=0;
			wall1y[0]=0;
			for(int i =1;i<18;i++)
			{
				wall1x[i]=wall1x[i-1]+15;
				wall1y[i]=wall1y[i-1];
				
			}
			wall1x[18]=wall1x[17]+60;
			wall1y[18]=wall1y[17];
			for(int i =19;i<36;i++)
			{
				wall1x[i]=wall1x[i-1]+15;
				wall1y[i]=wall1y[i-1];
				
			}
			wall1x[36]=0;
			wall1y[36]=510;
			for(int i =37;i<54;i++)
			{
				wall1x[i]=wall1x[i-1]+15;
				wall1y[i]=wall1y[i-1];
			}
			wall1x[54]=wall1x[53]+60;
			wall1y[54]=wall1y[53];
			for(int i =55;i<72;i++)
			{
				wall1x[i]=wall1x[i-1]+15;
				wall1y[i]=wall1y[i-1];
			}
		}
	}
	void drawWall3()
	{
		if(type==1)
		{
			wall3x[0]=120;
			wall3y[0]=0;
			for(int i =1;i<wall3count;i++)
			{
				wall3x[i]=wall3x[i-1];
				wall3y[i]=wall3y[i-1]+15;
			}
		}
		else if(type==2)
		{
			wall3x[0]=75;
			wall3y[0]=90;
			for(int i =1;i<8;i++)
			{
				wall3x[i]=wall3x[i-1];
				wall3y[i]=wall3y[i-1]+15;
			}
			wall3x[8]=75;
			wall3y[8]=420;
			for(int i =9;i<16;i++)
			{
				wall3x[i]=wall3x[i-1];
				wall3y[i]=wall3y[i-1]-15;
			}
			wall3x[16]=150;
			wall3y[16]=165;
			for(int i =17;i<21;i++)
			{
				wall3x[i]=wall3x[i-1];
				wall3y[i]=wall3y[i-1]+15;
			}
			wall3x[21]=150;
			wall3y[21]=345;
			for(int i =22;i<26;i++)
			{
				wall3x[i]=wall3x[i-1];
				wall3y[i]=wall3y[i-1]-15;
			}
		}
		else if(type==4)
		{
			wall3x[0]=0;
			wall3y[0]=15;
			for(int i =1;i<15;i++)
			{
				wall3x[i]=wall3x[i-1];
				wall3y[i]=wall3y[i-1]+15;
			}
			wall3x[15]=0;
			wall3y[15]=wall3y[14]+60;
			for(int i =16;i<30;i++)
			{
				wall3x[i]=wall3x[i-1];
				wall3y[i]=wall3y[i-1]+15;
			}
		}
	}
	void drawWall2()
	{
		if(type==1)
		{
			wall2x[0]=450;
			wall2y[0]=510;
			for(int i =1;i<wall2count;i++)
			{
				wall2x[i]=wall2x[i-1];
				wall2y[i]=wall2y[i-1]-15;
			}
		}
		else if(type==2)
		{
			wall2x[0]=495;
			wall2y[0]=90;
			for(int i =1;i<8;i++)
			{
				wall2x[i]=wall2x[i-1];
				wall2y[i]=wall2y[i-1]+15;
			}
			wall2x[8]=495;
			wall2y[8]=420;
			for(int i =9;i<16;i++)
			{
				wall2x[i]=wall2x[i-1];
				wall2y[i]=wall2y[i-1]-15;
			}
			wall2x[16]=420;
			wall2y[16]=165;
			for(int i =17;i<21;i++)
			{
				wall2x[i]=wall2x[i-1];
				wall2y[i]=wall2y[i-1]+15;
			}
			wall2x[21]=420;
			wall2y[21]=345;
			for(int i =22;i<26;i++)
			{
				wall2x[i]=wall2x[i-1];
				wall2y[i]=wall2y[i-1]-15;
			}
		}
		else if(type==4)
		{
			wall2x[0]=570;
			wall2y[0]=15;
			for(int i =1;i<15;i++)
			{
				wall2x[i]=wall2x[i-1];
				wall2y[i]=wall2y[i-1]+15;
			}
			wall2x[15]=570;
			wall2y[15]=wall2y[14]+60;
			for(int i =16;i<30;i++)
			{
				wall2x[i]=wall2x[i-1];
				wall2y[i]=wall2y[i-1]+15;
			}
		}
	}
	void drawHole1()
	{
		hole1x[0]=75;
		hole1y[0]=75;
		hole1x[1]=495;
		hole1y[1]=435;
	}
	void drawHole2()
	{
		hole2x[0]=495;
		hole2y[0]=75;
		hole2x[1]=75;
		hole2y[1]=435;
	}
}

