import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Random;
import java.util.Scanner;

import javax.swing.*;

public class drawsnake extends JPanel {
	Timer timer = new Timer(100, new TimerListener());
	Map m = new Map(1);
	Boss b = new Boss(1);
	int[] px = new int[150];
	int[] py = new int[150];
	int[] lastx = new int[150];
	int[] lasty = new int[150];
	int bonusx;
	int bonusy;
	int swardx;
	int swardy;
	int count = 0;
	int snakelength = 5;
	int length = 15;
	int choice = 0;
	int lastchoice = 0;
	int up = 1, right = 1, left = 1, temp = 1;
	int icount = 1;
	int snakecount = 1;
	int rightcount = 0;
	int nailtype = 0;
	int stagetype = 1;
	int bosschoice;
	int tempc = 0;
	int tempc1 = 0;
	int stormx;
	int stormy;
	int deathcount=0;
	boolean death = false;
	boolean boomstart = false;
	boolean boomend = true;
	boolean boomishit = false;
	boolean stormstart = false;
	boolean stormend = true;
	boolean stormishit = false;
	boolean bossishit = false;
	boolean swarded = false;
	boolean cont = false;
	boolean bonusb = false;
	boolean hole1out = false;
	boolean hole2out = false;
	boolean stageclear=false;
	boolean gameend=false;
	ImageIcon wp = new ImageIcon("pic/wp.png");
	ImageIcon ac = new ImageIcon("pic/ac.png");
	ImageIcon stage1 = new ImageIcon("pic/stage1.png");
	ImageIcon stage2 = new ImageIcon("pic/stage2.png");
	ImageIcon stage3 = new ImageIcon("pic/stage3.png");
	ImageIcon stage4 = new ImageIcon("pic/stage4.png");
	ImageIcon stage5 = new ImageIcon("pic/stage5.png");
	ImageIcon sward = new ImageIcon("pic/sward.png");
	ImageIcon snakehead;
	ImageIcon snakeheadtype1 = new ImageIcon("pic/snake2head1.png");
	ImageIcon snakeheadtype2 = new ImageIcon("pic/snake2head2.png");
	ImageIcon snakeheadtype3 = new ImageIcon("pic/snake2head3.png");
	ImageIcon snakeheadtype4 = new ImageIcon("pic/snake2head4.png");
	ImageIcon snakenail;
	ImageIcon lastsnakenail;
	ImageIcon snakenailtype1 = new ImageIcon("pic/snake2nail1.png");
	ImageIcon snakenailtype2 = new ImageIcon("pic/snake2nail2.png");
	ImageIcon snakenailtype3 = new ImageIcon("pic/snake2nail3.png");
	ImageIcon snakenailtype4 = new ImageIcon("pic/snake2nail4.png");
	ImageIcon snakebodytype1 = new ImageIcon("pic/newsnake2body1.png");
	ImageIcon snakebodytype2 = new ImageIcon("pic/newsnake2body1.png");
	ImageIcon snakebodytype3 = new ImageIcon("pic/newsnake2body2.png");
	ImageIcon snakebodytype4 = new ImageIcon("pic/newsnake2body2.png");
	ImageIcon snakebodycorner1 = new ImageIcon("pic/snake2corner1.png");
	ImageIcon snakebodycorner2 = new ImageIcon("pic/snake2corner2.png");
	ImageIcon snakebodycorner3 = new ImageIcon("pic/snake2corner3.png");
	ImageIcon snakebodycorner4 = new ImageIcon("pic/snake2corner4.png");
	ImageIcon boss = new ImageIcon("pic/boss.png");
	ImageIcon boom = new ImageIcon("pic/boom.png");
	ImageIcon storm1 = new ImageIcon("pic/storm1.png");
	ImageIcon storm2 = new ImageIcon("pic/storm2.png");
	ImageIcon gameover = new ImageIcon("pic/gg.png");
	ImageIcon bonus = new ImageIcon("pic/bonus.png");
	ImageIcon[] image = new ImageIcon[150];
	ImageIcon[] lastimage = new ImageIcon[150];
	Font f = new Font("SansSerif", Font.BOLD, 20);
	int score = 0;
	int totalscore = 0;
	JLabel scorel = new JLabel("Now : " + score+"\t");
	JLabel totalscorel = new JLabel("Total : " + totalscore+"\t");
	public drawsnake()
	{
		addKeyListener(new KeyAdapter());
	}
	void setChoice(int a) {//方線控制
		if (a == 0) {
			if (lastchoice != 0 && lastchoice != 6 && lastchoice != 3
					&& lastchoice != 7) {
				choice = a;
			}
		} else if (a == 1) {
			if (lastchoice != 1 && lastchoice != 4 && lastchoice != 2
					&& lastchoice != 5) {
				choice = a;
			}
		} else if (a == 2) {
			if (lastchoice != 2 && lastchoice != 5 && lastchoice != 1
					&& lastchoice != 4) {
				choice = a;
			}
		} else if (a == 3) {
			if (lastchoice != 3 && lastchoice != 7 && lastchoice != 0
					&& lastchoice != 6) {
				choice = a;
			}
		}
	}
	void setIcon(int n)//set Icon
	{
		if(n==1)
		{
			snakeheadtype1 = new ImageIcon("pic/snakehead1.png");
			snakeheadtype2 = new ImageIcon("pic/snakehead2.png");
			snakeheadtype3 = new ImageIcon("pic/snakehead3.png");
			snakeheadtype4 = new ImageIcon("pic/snakehead4.png");
			snakenailtype1 = new ImageIcon("pic/snakenail1.png");
			snakenailtype2 = new ImageIcon("pic/snakenail2.png");
			snakenailtype3 = new ImageIcon("pic/snakenail3.png");
			snakenailtype4 = new ImageIcon("pic/snakenail4.png");
			snakebodytype1 = new ImageIcon("pic/newsnakebody1.png");
			snakebodytype2 = new ImageIcon("pic/newsnakebody1.png");
			snakebodytype3 = new ImageIcon("pic/newsnakebody2.png");
			snakebodytype4 = new ImageIcon("pic/newsnakebody2.png");
			snakebodycorner1 = new ImageIcon("pic/snakecorner1.png");
			snakebodycorner2 = new ImageIcon("pic/snakecorner2.png");
			snakebodycorner3 = new ImageIcon("pic/snakecorner3.png");
			snakebodycorner4 = new ImageIcon("pic/snakecorner4.png");
			for (int i = 0; i < 4; i++) {
				image[i] = snakebodytype1;
				lastimage[i] = image[i];
			}
			snakenail = snakenailtype2;
		}
		else if(n==2)
		{
			snakeheadtype1 = new ImageIcon("pic/snake2head1.png");
			snakeheadtype2 = new ImageIcon("pic/snake2head2.png");
			snakeheadtype3 = new ImageIcon("pic/snake2head3.png");
			snakeheadtype4 = new ImageIcon("pic/snake2head4.png");
			snakenailtype1 = new ImageIcon("pic/snake2nail1.png");
			snakenailtype2 = new ImageIcon("pic/snake2nail2.png");
			snakenailtype3 = new ImageIcon("pic/snake2nail3.png");
			snakenailtype4 = new ImageIcon("pic/snake2nail4.png");
			snakebodytype1 = new ImageIcon("pic/newsnake2body1.png");
			snakebodytype2 = new ImageIcon("pic/newsnake2body1.png");
			snakebodytype3 = new ImageIcon("pic/newsnake2body2.png");
			snakebodytype4 = new ImageIcon("pic/newsnake2body2.png");
			snakebodycorner1 = new ImageIcon("pic/snake2corner1.png");
			snakebodycorner2 = new ImageIcon("pic/snake2corner2.png");
			snakebodycorner3 = new ImageIcon("pic/snake2corner3.png");
			snakebodycorner4 = new ImageIcon("pic/snake2corner4.png");
			for (int i = 0; i < 4; i++) {
				image[i] = snakebodytype1;
				lastimage[i] = image[i];
			}
			snakenail = snakenailtype2;
		}
		else if(n==3)
		{
			snakeheadtype1 = new ImageIcon("pic/snake3head1.png");
			snakeheadtype2 = new ImageIcon("pic/snake3head2.png");
			snakeheadtype3 = new ImageIcon("pic/snake3head3.png");
			snakeheadtype4 = new ImageIcon("pic/snake3head4.png");
			snakenailtype1 = new ImageIcon("pic/snake3nail1.png");
			snakenailtype2 = new ImageIcon("pic/snake3nail2.png");
			snakenailtype3 = new ImageIcon("pic/snake3nail3.png");
			snakenailtype4 = new ImageIcon("pic/snake3nail4.png");
			snakebodytype1 = new ImageIcon("pic/newsnake3body1.png");
			snakebodytype2 = new ImageIcon("pic/newsnake3body1.png");
			snakebodytype3 = new ImageIcon("pic/newsnake3body2.png");
			snakebodytype4 = new ImageIcon("pic/newsnake3body2.png");
			snakebodycorner1 = new ImageIcon("pic/snake3corner1.png");
			snakebodycorner2 = new ImageIcon("pic/snake3corner2.png");
			snakebodycorner3 = new ImageIcon("pic/snake3corner3.png");
			snakebodycorner4 = new ImageIcon("pic/snake3corner4.png");
			for (int i = 0; i < 4; i++) {
				image[i] = snakebodytype1;
				lastimage[i] = image[i];
			}
			snakenail = snakenailtype2;
		}
		else if(n==4)
		{
			snakeheadtype1 = new ImageIcon("pic/snake4head1.png");
			snakeheadtype2 = new ImageIcon("pic/snake4head2.png");
			snakeheadtype3 = new ImageIcon("pic/snake4head3.png");
			snakeheadtype4 = new ImageIcon("pic/snake4head4.png");
			snakenailtype1 = new ImageIcon("pic/snake4nail1.png");
			snakenailtype2 = new ImageIcon("pic/snake4nail2.png");
			snakenailtype3 = new ImageIcon("pic/snake4nail3.png");
			snakenailtype4 = new ImageIcon("pic/snake4nail4.png");
			snakebodytype1 = new ImageIcon("pic/newsnake4body1.png");
			snakebodytype2 = new ImageIcon("pic/newsnake4body1.png");
			snakebodytype3 = new ImageIcon("pic/newsnake4body2.png");
			snakebodytype4 = new ImageIcon("pic/newsnake4body2.png");
			snakebodycorner1 = new ImageIcon("pic/snake4corner1.png");
			snakebodycorner2 = new ImageIcon("pic/snake4corner2.png");
			snakebodycorner3 = new ImageIcon("pic/snake4corner3.png");
			snakebodycorner4 = new ImageIcon("pic/snake4corner4.png");
			for (int i = 0; i < 4; i++) {
				image[i] = snakebodytype1;
				lastimage[i] = image[i];
			}
			snakenail = snakenailtype2;
		}
		
	}
	void setlength() {//控制長度
		snakelength++;
	}

	void declength() {
		snakelength--;
	}

	void isDead() {//死了
		death=true;
		timer.stop();
	}
	void bonus() {//果實
		int tempx = 0, tempy = 0, count = 0, tempcount = 0;
		boolean check = true;
		while (check) {//檢查位置
			count = 0;
			{
				tempx = randomx();
				tempy = randomy();
			}
			for (int i = 0; i < snakelength; i++) {
				if (tempx == px[i] && tempy == py[i]) {
					check = true;
				} else {
					count++;
				}
			}
			if (count == snakelength) {
				for (int i = 0; i < m.wall3count; i++) {
					if (tempx == m.wall3x[i] && tempy == m.wall3y[i]) {
						check = true;
					} else {
						count++;
					}
				}
			}

			if (count == (snakelength + m.wall3count)) {
				for (int i = 0; i < m.wall2count; i++) {
					if (tempx == m.wall2x[i] && tempy == m.wall2y[i]) {
						check = true;
					} else {
						count++;
					}
				}
			}

			if (count == (snakelength + m.wall3count + m.wall2count)) {
				for (int i = 0; i < m.wall1count; i++) {
					if (tempx == m.wall1x[i] && tempy == m.wall1y[i]) {
						check = true;
					} else {
						count++;
					}
				}
			}

			if (count == (snakelength + m.wall3count + m.wall2count + m.wall1count)) {
				if (stagetype == 4) {
					if ((tempx == m.hole1x[0] && tempy == m.hole1y[0])
							|| (tempx == m.hole1x[0] + 15 && tempy == m.hole1y[0])
							|| (tempx == m.hole1x[0] && tempy == m.hole1y[0] + 15)
							|| (tempx == m.hole1x[0] + 15 && tempy == m.hole1y[0] + 15)) {
						check = true;
					} else if ((tempx == m.hole1x[1] && tempy == m.hole1y[1])
							|| (tempx == m.hole1x[1] + 15 && tempy == m.hole1y[1])
							|| (tempx == m.hole1x[1] && tempy == m.hole1y[1] + 15)
							|| (tempx == m.hole1x[1] + 15 && tempy == m.hole1y[1] + 15)) {
						check = true;
					} else if ((tempx == m.hole2x[0] && tempy == m.hole2y[0])
							|| (tempx == m.hole2x[0] + 15 && tempy == m.hole2y[0])
							|| (tempx == m.hole2x[0] && tempy == m.hole2y[0] + 15)
							|| (tempx == m.hole2x[0] + 15 && tempy == m.hole2y[0] + 15)) {
						check = true;
					} else if ((tempx == m.hole2x[1] && tempy == m.hole2y[1])
							|| (tempx == m.hole2x[1] + 15 && tempy == m.hole2y[1])
							|| (tempx == m.hole2x[1] && tempy == m.hole2y[1] + 15)
							|| (tempx == m.hole2x[1] + 15 && tempy == m.hole2y[1] + 15)) {
						check = true;
					} else {
						check = false;
					}
				} else {
					check = false;
				}
			}
		}
		bonusx = tempx;
		bonusy = tempy;
	}

	void sward() {//劍果實位置
		int tempx = 0, tempy = 0, count = 0;
		boolean check = true;
		while (check) {
			count = 0;
			tempx = randomx();
			tempy = randomy();
			for (int i = 0; i < snakelength; i++) {
				if (tempx == px[i] && tempy == py[i]) {
					check = true;
				} else {
					count++;
				}
			}
			if (count == snakelength) {
				if (tempx == bonusx && tempy == bonusy) {
					check = true;
				} else {
					check = false;
				}
			}
		}
		swardx = tempx;
		swardy = tempy;
	}

	int randomx() {//隨機
		Random num = new Random();
		int renum = (int) (num.nextInt(37) + 1);
		return 15 * renum;
	}

	int randomy() {//隨機
		Random num = new Random();
		int renum = (int) (num.nextInt(32) + 1);
		return 15 * renum;
	}

	protected void paintComponent(Graphics g) {//畫圖

		super.paintComponent(g);
		int x = getWidth();
		int y = getHeight();
		if (count == 0) {//出使值
			bonus();
			scorel.setFont(f);
			scorel.setForeground(Color.WHITE);
			totalscorel.setFont(f);
			totalscorel.setForeground(Color.WHITE);
			snakelength = 5;
			px[0] = 285;
			py[0] = 255;
			for (int i = 1; i < 5; i++) {
				px[i] = px[i - 1];
				py[i] = py[i - 1] + length;
			}
			for (int i = 0; i < 4; i++) {
				image[i] = snakebodytype1;
				lastimage[i] = image[i];
			}
			snakenail = snakenailtype2;
			timer.stop();
			count++;
			sward();
		}
		if (bonusb) {
			setlength();
			bonusb = false;
		}
		if (count == 3) {// 重新關卡
			stageclear=false;
			if (stagetype == 1) {
				m = new Map(2);
				stagetype = 2;
				
			} else if (stagetype == 2) {
				boss = new ImageIcon("pic/boss.png");
				b = new Boss(0);
				m = new Map(3);
				stagetype = 3;
			} else if (stagetype == 3) {
				boss = new ImageIcon("pic/boss.png");
				b = new Boss(0);
				m = new Map(4);
				stagetype = 4;
			} else if (stagetype == 4) {
				boss = new ImageIcon("pic/boss.png");
				b = new Boss(1);
				m = new Map(3);
				stagetype = 5;
				sward();
				boomstart = false;
				boomend = true;;
				tempc=0;
				stormstart = false;
				stormend = true;;
				tempc1=0;
			} else if (stagetype == 5) {
				gameend=true;
				m = new Map(1);
				stagetype = 1;
			}
			else if(stagetype==0)
			{
				m = new Map(1);
				stagetype = 1;
			}
			bonus();
			score = 0;
			scorel.setText("Now : " + score+"\t");
			choice = 0;
			lastchoice = 0;
			snakelength = 5;
			px[0] = 285;
			py[0] = 255;
			for (int i = 1; i < 5; i++) {
				px[i] = px[i - 1];
				py[i] = py[i - 1] + length;
			}
			for (int i = 0; i < 4; i++) {
				image[i] = snakebodytype1;
				lastimage[i] = image[i];
			}
			snakenail = snakenailtype2;
			timer.stop();
			count++;
		}
		if(count==5)
		{
			isDead();
			m = new Map(1);
			stagetype = 1;
			bonus();
			score = 0;
			scorel.setText("Now : " + score+"\t");
			choice = 0;
			lastchoice = 0;
			snakelength = 5;
			px[0] = 285;
			py[0] = 255;
			for (int i = 1; i < 5; i++) {
				px[i] = px[i - 1];
				py[i] = py[i - 1] + length;
			}
			for (int i = 0; i < 4; i++) {
				image[i] = snakebodytype1;
				lastimage[i] = image[i];
			}
			snakenail = snakenailtype2;
			timer.stop();
			count++;
		}
		for (int i = 0; i < snakelength - 1; i++) // 更新每次畫片
		{
			lastimage[i] = image[i];
		}
		lastsnakenail = snakenail;
		for (int i = 0; i < snakelength; i++) {// 更新每次位置
			lastx[i] = px[i];
			lasty[i] = py[i];
		}

		if (choice == 1) {// 按鍵上下左右
			px[0] += length;
			if (lastchoice == 6 || lastchoice == 0) {
				image[0] = snakebodycorner1;
			} else if (lastchoice == 7 || lastchoice == 3) {
				image[0] = snakebodycorner3;
			}
			choice = 4;
		} else if (choice == 2) {
			px[0] -= length;
			if (lastchoice == 6 || lastchoice == 0) {
				image[0] = snakebodycorner2;
			} else if (lastchoice == 7 || lastchoice == 3) {
				image[0] = snakebodycorner4;
			}
			choice = 5;
		} else if (choice == 3) {
			py[0] += length;
			if (lastchoice == 4 || lastchoice == 1)
				image[0] = snakebodycorner2;
			else if (lastchoice == 5 || lastchoice == 2)
				image[0] = snakebodycorner1;
			choice = 7;
		} else if (choice == 0) {
			py[0] -= length;
			if (lastchoice == 4 || lastchoice == 1)
				image[0] = snakebodycorner4;
			else if (lastchoice == 5 || lastchoice == 2)
				image[0] = snakebodycorner3;
			choice = 6;

		} else if (choice == 4)// right-con//移動上下左右
		{
			px[0] += length;
			image[0] = snakebodytype3;
		} else if (choice == 5)// left-con
		{
			px[0] -= length;
			image[0] = snakebodytype4;
		} else if (choice == 6)// up-con
		{
			py[0] -= length;
			image[0] = snakebodytype1;
		} else if (choice == 7)// down-con
		{
			py[0] += length;
			image[0] = snakebodytype2;
		}
		for (int k = 1; k < snakelength; k++) {// 位置向前移動
			px[k] = lastx[k - 1];
			py[k] = lasty[k - 1];
		}

		for (int i = 1; i < snakelength - 1; i++)// 圖片向前移動
		{
			image[i] = lastimage[i - 1];
		}

		if ((image[snakelength - 2] == snakebodycorner3 && lastsnakenail == snakenailtype3)
				|| (image[snakelength - 2] == snakebodycorner4 && lastsnakenail == snakenailtype4))// 蛇尾
		{
			snakenail = snakenailtype2;
		} else if ((image[snakelength - 2] == snakebodycorner1 && lastsnakenail == snakenailtype3)
				|| (image[snakelength - 2] == snakebodycorner2 && lastsnakenail == snakenailtype4)) {
			snakenail = snakenailtype1;
		} else if ((image[snakelength - 2] == snakebodycorner1 && lastsnakenail == snakenailtype2)
				|| (image[snakelength - 2] == snakebodycorner3 && lastsnakenail == snakenailtype1)) {
			snakenail = snakenailtype4;
		} else if ((image[snakelength - 2] == snakebodycorner2 && lastsnakenail == snakenailtype2)
				|| (image[snakelength - 2] == snakebodycorner4 && lastsnakenail == snakenailtype1)) {
			snakenail = snakenailtype3;
		}

		if (choice == 4)// 蛇頭
		{
			snakehead = snakeheadtype3;
		} else if (choice == 5) {
			snakehead = snakeheadtype4;
		} else if (choice == 6) {
			snakehead = snakeheadtype1;
		} else if (choice == 7) {
			snakehead = snakeheadtype2;
		}
		if (Math.abs(bonusx - px[0]) <= 10 && Math.abs(bonusy - py[0]) <= 10) {//吃到果實
			bonus();
			score += 10;
			totalscore += 10;
			bonusb = true;
			scorel.setText("Now : " + score+"\t");
			totalscorel.setText("Total : " + totalscore+"\t");
		}
		if (stagetype == 5) {//第五關 劍果實
			if (Math.abs(swardx - px[0]) <= 10
					&& Math.abs(swardy - py[0]) <= 10) {
				if (!swarded) {
					sward();
				}
				swarded = true;
			}
		}
		g.drawImage(snakehead.getImage(), px[0], py[0], length, length, this);
		for (int j = 1; j < snakelength - 1; j++) {
			g.drawImage(image[j - 1].getImage(), px[j], py[j], length, length,
					this);
		}
		g.drawImage(snakenail.getImage(), px[snakelength - 1],
				py[snakelength - 1], length, length, this);
		g.drawImage(bonus.getImage(), bonusx, bonusy, length, length, this);
		for (int i = 0; i < m.wall1count; i++) {
			g.drawImage(m.wall1.getImage(), m.wall1x[i], m.wall1y[i], length,
					length, this);
		}
		for (int i = 0; i < m.wall2count; i++) {
			g.drawImage(m.wall2.getImage(), m.wall2x[i], m.wall2y[i], length,
					length, this);
		}
		for (int i = 0; i < m.wall3count; i++) {
			g.drawImage(m.wall3.getImage(), m.wall3x[i], m.wall3y[i], length,
					length, this);
		}
		if (stagetype == 1) {//第一關
			if (score == 0 && !timer.isRunning()) {
				g.drawImage(stage1.getImage(), 0, 0, 585, 60, this);
			}
			if (score >= 500) {
				g.drawImage(wp.getImage(), 0, 175, 585, 175, this);
				stageclear=true;
				timer.stop();
				count = 3;
			}
		} else if (stagetype == 2) {//第二關
			if (score == 0 && !timer.isRunning()) {
				g.drawImage(stage2.getImage(), 0, 0, 585, 60, this);
			}
			if (score >= 500) {
				g.drawImage(wp.getImage(), 0, 175, 585, 175, this);
				stageclear=true;
				timer.stop();
				count = 3;
			}
		} else if (stagetype == 3) {//第三關
			g.drawImage(boss.getImage(), b.bossx, b.bossy, 150, 150, this);
			if (score == 0 && !timer.isRunning()) {
				g.drawImage(stage3.getImage(), 0, 0, 585, 60, this);
			}
			if (score >= 500) {
				g.drawImage(wp.getImage(), 0, 175, 585, 175, this);
				stageclear=true;
				timer.stop();
				count = 3;
			}
			bosschoice = b.bosschoice();
			if (bosschoice == 0 || bosschoice == 2) {//王移動
				if (py[0] > b.bossy) {
					b.bossy += 15;
				} else if (py[0] < b.bossy) {
					b.bossy -= 15;
				} else {
					bosschoice = 1;
				}
			} else if (bosschoice == 1 || bosschoice == 3) {
				if (px[0] > b.bossx) {
					b.bossx += 15;
				} else if (px[0] < b.bossx) {
					b.bossx -= 15;
				}
			}
			if (px[0] >= b.bossx//碰撞
					&& px[0] <= (b.bossx + 135)
					&& py[0] <= (b.bossy + 135)
					&& py[0] >= b.bossy
					&& (image[0] == snakebodytype1
							|| image[0] == snakebodycorner3 || image[0] == snakebodycorner4)) {
				for (int i = 0; i < snakelength; i++) {
					py[i] += 90;
				}
				declength();
			} else if (px[0] >= b.bossx
					&& px[0] <= (b.bossx + 135)
					&& py[0] >= b.bossy
					&& py[0] <= (b.bossy + 135)
					&& (image[0] == snakebodytype2
							|| image[0] == snakebodycorner1 || image[0] == snakebodycorner2)) {
				for (int i = 0; i < snakelength; i++) {
					py[i] -= 90;
				}
				declength();
			} else if (px[0] >= b.bossx
					&& px[0] <= (b.bossx + 135)
					&& py[0] <= (b.bossy + 135)
					&& py[0] >= b.bossy
					&& (image[0] == snakebodytype3
							|| image[0] == snakebodycorner1 || image[0] == snakebodycorner3)) {
				for (int i = 0; i < snakelength; i++) {
					px[i] -= 90;
				}
				declength();
			} else if (px[0] >= b.bossx
					&& px[0] <= b.bossx + 135
					&& py[0] <= (b.bossy + 135)
					&& py[0] >= b.bossy
					&& (image[0] == snakebodytype4
							|| image[0] == snakebodycorner2 || image[0] == snakebodycorner4)) {
				for (int i = 0; i < snakelength; i++) {
					px[i] += 90;
				}
				declength();
			}
		} else if (stagetype == 4) {//地4關
			if (score == 0 && !timer.isRunning()) {
				g.drawImage(stage4.getImage(), 0, 0, 585, 60, this);
			}
			for (int i = 0; i < m.hole1count; i++) {
				g.drawImage(m.hole1.getImage(), m.hole1x[i], m.hole1y[i],
						2 * length, 2 * length, this);
			}
			for (int i = 0; i < m.hole2count; i++) {
				g.drawImage(m.hole2.getImage(), m.hole2x[i], m.hole2y[i],
						2 * length, 2 * length, this);
			}
			g.drawImage(boss.getImage(), b.bossx, b.bossy, 150, 150, this);
			if (score >= 500) {
				g.drawImage(wp.getImage(), 0, 175, 585, 175, this);
				stageclear=true;
				timer.stop();
				count = 3;
			}
			bosschoice = b.bosschoice();
			if (bosschoice == 0 || bosschoice == 2) {//王移動
				if (py[0] > b.bossy) {
					b.bossy += 15;
				} else if (py[0] < b.bossy) {
					b.bossy -= 15;
				} else {
					bosschoice = 1;
				}
			} else if (bosschoice == 1 || bosschoice == 3) {
				if (px[0] > b.bossx) {
					b.bossx += 15;
				} else if (px[0] < b.bossx) {
					b.bossx -= 15;
				}
			}
			if (((px[0] == m.hole1x[0] && py[0] == m.hole1y[0])//穿越洞穴
					|| (px[0] == m.hole1x[0] + 15 && py[0] == m.hole1y[0])
					|| (px[0] == m.hole1x[0] && py[0] == m.hole1y[0] + 15) || (px[0] == m.hole1x[0] + 15 && py[0] == m.hole1y[0] + 15))
					&& !hole1out) {
				px[0] = m.hole1x[1];
				py[0] = m.hole1y[1];
				hole1out = true;
			} else if (((px[0] == m.hole1x[1] && py[0] == m.hole1y[1])
					|| (px[0] == m.hole1x[1] + 15 && py[0] == m.hole1y[1])
					|| (px[0] == m.hole1x[1] && py[0] == m.hole1y[1] + 15) || (px[0] == m.hole1x[1] + 15 && py[0] == m.hole1y[1] + 15))
					&& !hole1out) {
				px[0] = m.hole1x[0];
				py[0] = m.hole1y[0];
				hole1out = true;
			} else if (((px[0] == m.hole2x[0] && py[0] == m.hole2y[0])
					|| (px[0] == m.hole2x[0] + 15 && py[0] == m.hole2y[0])
					|| (px[0] == m.hole2x[0] && py[0] == m.hole2y[0] + 15) || (px[0] == m.hole2x[0] + 15 && py[0] == m.hole2y[0] + 15))
					&& !hole2out) {
				px[0] = m.hole2x[1];
				py[0] = m.hole2y[1];
				hole2out = true;
			} else if (((px[0] == m.hole2x[1] && py[0] == m.hole2y[1])
					|| (px[0] == m.hole2x[1] + 15 && py[0] == m.hole2y[1])
					|| (px[0] == m.hole2x[1] && py[0] == m.hole2y[1] + 15) || (px[0] == m.hole2x[1] + 15 && py[0] == m.hole2y[1] + 15))
					&& !hole2out) {
				px[0] = m.hole2x[0];
				py[0] = m.hole2y[0];
				hole2out = true;
			} else {
				hole1out = false;
				hole2out = false;
			}
			if (px[0] >= b.bossx//碰撞
					&& px[0] <= (b.bossx + 135)
					&& py[0] <= (b.bossy + 135)
					&& py[0] >= b.bossy
					&& (image[0] == snakebodytype1
							|| image[0] == snakebodycorner3 || image[0] == snakebodycorner4)) {
				if (py[0] == 420) {
					for (int i = 0; i < snakelength; i++) {
						py[i] += 75;
					}
				} else if (py[0] == 450 || py[0] == 465 || py[0] == 480) {
					for (int i = 0; i < snakelength; i++) {
						py[i] += 120;
					}
				} else {
					for (int i = 0; i < snakelength; i++) {
						py[i] += 90;
					}
				}
				declength();
			} else if (px[0] >= b.bossx
					&& px[0] <= (b.bossx + 135)
					&& py[0] >= b.bossy
					&& py[0] <= (b.bossy + 135)
					&& (image[0] == snakebodytype2
							|| image[0] == snakebodycorner1 || image[0] == snakebodycorner2)) {

				if (py[0] == 90) {
					for (int i = 0; i < snakelength; i++) {
						py[i] -= 75;
					}
				} else if (py[0] == 75 || py[0] == 60 || py[0] == 45) {
					for (int i = 0; i < snakelength; i++) {
						py[i] -= 120;
					}
				} else {
					for (int i = 0; i < snakelength; i++) {
						py[i] -= 90;
					}
				}
				declength();
			} else if (px[0] >= b.bossx
					&& px[0] <= (b.bossx + 135)
					&& py[0] <= (b.bossy + 135)
					&& py[0] >= b.bossy
					&& (image[0] == snakebodytype3
							|| image[0] == snakebodycorner1 || image[0] == snakebodycorner3)) {
				if (px[0] == 90) {
					for (int i = 0; i < snakelength; i++) {
						px[i] -= 75;
					}
				} else if (px[0] == 60 || px[0] == 45 || px[0] == 75) {
					for (int i = 0; i < snakelength; i++) {
						px[i] -= 120;
					}
				} else {
					for (int i = 0; i < snakelength; i++) {
						px[i] -= 90;
					}
				}
				declength();
			} else if (px[0] >= b.bossx
					&& px[0] <= b.bossx + 135
					&& py[0] <= (b.bossy + 135)
					&& py[0] >= b.bossy
					&& (image[0] == snakebodytype4
							|| image[0] == snakebodycorner2 || image[0] == snakebodycorner4)) {
				if (px[0] == 480) {
					for (int i = 0; i < snakelength; i++) {
						px[i] += 75;
					}
				} else if (px[0] == 510 || px[0] == 525 || px[0] == 540) {
					for (int i = 0; i < snakelength; i++) {
						px[i] += 120;
					}
				} else {
					for (int i = 0; i < snakelength; i++) {
						px[i] += 90;
					}
				}

				declength();
			}
		} else if (stagetype == 5) {//第5關
			if (!swarded) {
				g.drawImage(sward.getImage(), swardx, swardy, length, length,
						this);
			}
			if (score == 0 && !timer.isRunning()) {
				g.drawImage(stage5.getImage(), 0, 0, 585, 60, this);
			}
			if (b.bosshealth <= 0) {
				g.drawImage(ac.getImage(), 0, 175, 585, 175, this);
				score+=snakelength*10;
				totalscore+=score;
				scorel.setText("Now : " + score+"\t");
				totalscorel.setText("Total : " + totalscore+"\t");
				stageclear=true;
				timer.stop();
				count = 3;
			}
			bosschoice = b.bosschoice();//王移動
			if (bosschoice == 0 || bosschoice == 2 || bosschoice == 4
					|| bosschoice == 6 || bosschoice == 8 || bosschoice == 12
					|| bosschoice == 14 || bosschoice == 16 || bosschoice == 18) {
				if (py[0] > b.bossy) {
					b.bossy += 15;
				} else if (py[0] < b.bossy) {
					b.bossy -= 15;
				} else if (b.bossy == py[0]) {
					bosschoice = 1;
				}
			} else if (bosschoice == 1 || bosschoice == 3 || bosschoice == 5
					|| bosschoice == 7 || bosschoice == 11 || bosschoice == 13
					|| bosschoice == 15 || bosschoice == 17 || bosschoice == 19) {
				if (px[0] > b.bossx) {
					b.bossx += 15;
				} else if (px[0] < b.bossx) {
					b.bossx -= 15;
				}
			} else if (bosschoice == 9 && boomend && stormend)//爆炸
			{
				boomstart = true;
				boomend = false;
			} else if (bosschoice == 10 && stormend && boomend)//落雷
			{
				stormstart = true;
				stormend = false;
			}
			if (boomstart && timer.isRunning()) {//爆炸動畫
				tempc++;
				if (tempc <= 20) {
					boss = new ImageIcon("pic/boss1.png");
				}
				if (tempc > 20 && tempc < 30) {
					g.drawImage(boom.getImage(), b.bossx - 75, b.bossy - 75,
							300, 300, this);
					boss = new ImageIcon("pic/boss2.png");
					if ((Math.abs(px[0] - (b.bossx + 75)) <= 150 && Math
							.abs(py[0] - (b.bossy + 75)) <= 150)) {
						if (snakehead == snakeheadtype1) {
							for (int i = 0; i < snakelength; i++) {
								py[i] += 120;
							}
						} else if (snakehead == snakeheadtype2) {
							for (int i = 0; i < snakelength; i++) {
								py[i] -= 120;
							}
						} else if (snakehead == snakeheadtype3) {
							for (int i = 0; i < snakelength; i++) {
								px[i] -= 120;
							}
						} else if (snakehead == snakeheadtype4) {
							for (int i = 0; i < snakelength; i++) {
								px[i] += 120;
							}
						}
						if (!boomishit) {
							declength();
						}
						boomishit = true;
					} else {
						boomishit = false;
					}
				}
				if (tempc == 30) {
					boomend = true;
					boomstart = false;
					tempc = 0;
					boss = new ImageIcon("pic/boss.png");
				}
			}
			if (stormstart && timer.isRunning()) {//落雷動畫
				tempc1++;
				Random num = new Random();
				if (tempc1 < 20) {
					boss = new ImageIcon("pic/boss1.png");
				} else if (tempc1 == 20) {
					int rx = (int) (num.nextInt(7)) - 3;
					int ry = (int) (num.nextInt(7)) - 3;
					stormx = px[0] + rx * 15;
					stormy = py[0] + ry * 15;
				} else if (tempc1 > 20 && tempc1 < 30) {
					g.drawImage(storm1.getImage(), stormx, stormy, 15, 15, this);
				} else if (tempc1 >= 30 & tempc1 < 35) {
					g.drawImage(storm2.getImage(), stormx - 75, stormy - 75,
							150, 150, this);
					if ((Math.abs(px[0] - (stormx)) <= 75 && Math.abs(py[0]
							- (stormy)) <= 75)) {
						if (snakehead == snakeheadtype1) {
							for (int i = 0; i < snakelength; i++) {
								py[i] += 120;
							}
						} else if (snakehead == snakeheadtype2) {
							for (int i = 0; i < snakelength; i++) {
								py[i] -= 120;
							}
						} else if (snakehead == snakeheadtype3) {
							for (int i = 0; i < snakelength; i++) {
								px[i] -= 120;
							}
						} else if (snakehead == snakeheadtype4) {
							for (int i = 0; i < snakelength; i++) {
								px[i] += 120;
							}
						}
						if (!stormishit) {
							declength();
						}
						stormishit = true;
					} else {
						stormishit = false;
					}
				}
				if (tempc1 == 35) {
					stormend = true;
					stormstart = false;
					tempc1 = 0;
					boss = new ImageIcon("pic/boss.png");
				}
			}
			if (px[0] >= b.bossx//碰撞
					&& px[0] <= (b.bossx + 135)
					&& py[0] <= (b.bossy + 135)
					&& py[0] >= b.bossy
					&& (image[0] == snakebodytype1
							|| image[0] == snakebodycorner3 || image[0] == snakebodycorner4)) {
				if (swarded) {
					b.bossy -= 90;
					b.hit();
					swarded = false;
				} else {
					for (int i = 0; i < snakelength; i++) {
						py[i] += 90;
					}
					if (!bossishit) {
						declength();
					}
					bossishit = true;
				}

			} else if (px[0] >= b.bossx
					&& px[0] <= (b.bossx + 135)
					&& py[0] >= b.bossy
					&& py[0] <= (b.bossy + 135)
					&& (image[0] == snakebodytype2
							|| image[0] == snakebodycorner1 || image[0] == snakebodycorner2)) {
				if (swarded) {
					b.bossy += 90;
					b.hit();
					swarded = false;
				} else {
					for (int i = 0; i < snakelength; i++) {
						py[i] -= 90;
					}
					if (!bossishit) {
						declength();
					}
					bossishit = true;
				}

			} else if (px[0] >= b.bossx
					&& px[0] <= (b.bossx + 135)
					&& py[0] <= (b.bossy + 135)
					&& py[0] >= b.bossy
					&& (image[0] == snakebodytype3
							|| image[0] == snakebodycorner1 || image[0] == snakebodycorner3)) {
				if (swarded) {
					b.bossx += 90;
					b.hit();
					swarded = false;
				} else {
					for (int i = 0; i < snakelength; i++) {
						px[i] -= 90;
					}
					if (!bossishit) {
						declength();
					}
					bossishit = true;
				}
			} else if (px[0] >= b.bossx
					&& px[0] <= b.bossx + 135
					&& py[0] <= (b.bossy + 135)
					&& py[0] >= b.bossy
					&& (image[0] == snakebodytype4
							|| image[0] == snakebodycorner2 || image[0] == snakebodycorner4)) {
				if (swarded) {
					b.bossx -= 90;
					b.hit();
					swarded = false;
				} else {
					for (int i = 0; i < snakelength; i++) {
						px[i] += 90;
					}
					if (!bossishit) {
						declength();
					}
					bossishit = true;
				}
			} else {
				bossishit = false;
			}
			if (b.bosshealth > 0) {
				g.drawImage(boss.getImage(), b.bossx, b.bossy, 150, 150, this);
			}
		}
		for (int i = 0; i < snakelength; i++) {
			if (px[i] > 570)
				px[i] -= 585;
			else if (px[i] < 0)
				px[i] += 585;
			if (py[i] > 510)
				py[i] -= 525;
			else if (py[i] < 0)
				py[i] += 525;
		}

		for (int i = 1; i < snakelength; i++) {
			if (px[0] == px[i] && py[0] == py[i]) {
				g.drawImage(gameover.getImage(), 0, 175, 585, 175, this);
				count=5;
				timer.stop();
			}
		}
		for (int i = 0; i < m.wall1count; i++) {
			if (px[0] == m.wall1x[i] && py[0] == m.wall1y[i]) {
				g.drawImage(gameover.getImage(), 0, 175, 585, 175, this);
				count=5;
				timer.stop();
			}
		}
		for (int i = 0; i < m.wall2count; i++) {
			if (px[0] == m.wall2x[i] && py[0] == m.wall2y[i]) {
				g.drawImage(gameover.getImage(), 0, 175, 585, 175, this);
				count=5;
				timer.stop();
			}
		}
		for (int i = 0; i < m.wall3count; i++) {
			if (px[0] == m.wall3x[i] && py[0] == m.wall3y[i]) {
				g.drawImage(gameover.getImage(), 0, 175, 585, 175, this);
				count=5;
				timer.stop();
			}
		}
		if (snakelength <= 1) {
			g.drawImage(gameover.getImage(), 0, 175, 585, 175, this);
			count=5;
			timer.stop();
			snakelength = 5;
		}
		lastchoice = choice;
	}
	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			repaint();
		}
	}
	class KeyAdapter implements KeyListener//鍵盤控制
	{
		public void keyPressed(KeyEvent e) {
			switch(e.getKeyCode())
			{
			case KeyEvent.VK_ENTER:timer.start();death=false;break;
			case KeyEvent.VK_DOWN:setChoice(3);break;
			case KeyEvent.VK_UP:setChoice(0);break;
			case KeyEvent.VK_LEFT:setChoice(2);break;
			case KeyEvent.VK_RIGHT:setChoice(1);break;
			case KeyEvent.VK_ESCAPE:timer.stop();
			case KeyEvent.VK_SPACE:setlength();break;
			case KeyEvent.VK_CONTROL:score+=500;
			totalscore+=score;
			scorel.setText("Now : " + score+"\t");
			totalscorel.setText("Total : " + totalscore+"\t");
			break;
			}
		}
		public void keyReleased(KeyEvent arg0) {
		}
		public void keyTyped(KeyEvent arg0) {
		}
	}
}
