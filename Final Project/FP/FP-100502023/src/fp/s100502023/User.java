package fp.s100502023;

public class User 
{
	protected String username;
	protected int money;
	protected int experience;
	protected int total_fish_number;
	protected int kind1_fish_number;
	protected int kind2_fish_number;
	
	private int fish_number_Max=30;
	
	protected int [][] fish_data = new int[fish_number_Max][2];
	
	protected int user_index;
}
