package fp.s100502023;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class FishStatus extends JFrame implements ActionListener
{
	private JLabel [] jl_fish_image = new JLabel [30];
	private JLabel [] jl_fish_age_text = new JLabel [30];
	private JLabel [] jl_fish_size_text = new JLabel [30];
	private JLabel [] jl_fish_satiety_text = new JLabel [30];
	private JLabel [] jl_money_icon = new JLabel [30];

	private JLabel [] jl_fish_value = new JLabel [30];
	private JLabel [] jl_fish_other  = new JLabel [30];
	private JLabel [] jl_fish_other2 = new JLabel [30];
	
	private JButton [] jb_sell_fish = new JButton[30];
	
	private JLabel [] jp_add_fish_info = new JLabel[30];
	
	protected User user;
	protected Fish [] fish;
	
	
	private JScrollPane jscroll_side = new JScrollPane();
	private JPanel jp_all = new JPanel();
	private JScrollBar scrollbar = new JScrollBar();
	private LineBorder border = new LineBorder(Color.BLACK);
	
	public FishStatus(User input_user,Fish []input_fish)
	{
		user=input_user;
		fish=input_fish;
		addFishStatus();
		jscroll_side.setViewportView(jp_all);
		jscroll_side.setVerticalScrollBar(scrollbar);
		jscroll_side.setSize(100,300);
		jscroll_side.setBounds(0, 100, 150, 300);
		add(jscroll_side);
		//this.setDefaultCloseOperation(FishStatus.EXIT_ON_CLOSE);
		
	}
	
 	public void addFishStatus()
	{
		jp_all.removeAll();
		
		jp_all.setLayout(new GridLayout(user.total_fish_number,1,0,0));
		
		//jp_all.setLayout(new GridLayout(user.total_fish_number,2,0,0));
		
		for (int i=0;i<user.total_fish_number;i++)
		{
			jl_fish_image[i] = new JLabel();
			//jl_fish_image[i].setBorder(border);
			
			if (fish[i].fish_kind==0)
			{
				jl_fish_image[i].setIcon(new ImageIcon("Image/Commodity/fish1_status.png"));
			}
			else if (fish[i].fish_kind==1)
			{
				jl_fish_image[i].setIcon(new ImageIcon("Image/Commodity/fish2_status.png"));
			}
			
			jl_fish_age_text[i] = new JLabel();
			//jl_fish_age_value[i] = new JLabel();
			//jl_fish_age_text[i].setBorder(border);
			//jl_fish_age_value[i].setBorder(border);
			jl_fish_age_text[i].setText("age: "+String.valueOf(fish[i].age));
			//jl_fish_age_value[i].setText(String.valueOf(fish[i].age));
			
			jl_fish_satiety_text[i] = new JLabel();
			//jl_fish_satiety_value[i] = new JLabel();
			//jl_fish_satiety_text[i].setBorder(border);
			//jl_fish_satiety_value[i].setBorder(border);
			
			
			if (fish[i].satiety==true)
			{
				//jl_fish_satiety_value[i].setText("飽足");
				jl_fish_satiety_text[i].setText("飽足感: 飽足");
			}
			else
			{
				//jl_fish_satiety_value[i].setText("飢餓");
				jl_fish_satiety_text[i].setText("飽足感: 飢餓");
			}
			
			jl_fish_size_text[i] = new JLabel();
			//jl_fish_size_value[i] = new JLabel();
			//jl_fish_size_text[i].setBorder(border);
			//jl_fish_size_value[i].setBorder(border);
			
			jl_fish_value[i] = new JLabel();
			
			if (fish[i].age>=0 && fish[i].age<3)
			{
				jl_fish_size_text[i].setText("成長: 幼年期");
				jl_fish_value[i].setText("價值: $0");
				//jl_fish_size_value[i].setText("幼年期");
			}
			else if (fish[i].age>=3 && fish[i].age<6)
			{
				jl_fish_size_text[i].setText("成長: 成長期");
				jl_fish_value[i].setText("價值: $50");
				//jl_fish_size_value[i].setText("成長期");
			}
			else if (fish[i].age>=6)
			{
				jl_fish_size_text[i].setText("成長: 成熟期");
				jl_fish_value[i].setText("價值: $500");
				//jl_fish_size_value[i].setText("成熟期");
			}
			
			//jl_money_icon[i] = new JLabel();
			//jl_money_icon[i].setBorder(border);
			//jl_money_icon[i].setIcon(new ImageIcon("Image/Commodity/money_status_2.png"));
			jb_sell_fish[i] = new JButton();
			//jb_sell_fish[i].setBorder(border);
			jb_sell_fish[i].setText("販賣");
			jb_sell_fish[i].addActionListener(this);
			
			jl_fish_other[i] = new JLabel();
			jl_fish_other2[i] = new JLabel();
		
			jp_add_fish_info[i] = new JLabel();
			jp_add_fish_info[i].setLayout(new GridLayout(2,4,0,0));
			
			jp_add_fish_info[i].setBorder(border);
			jp_add_fish_info[i].add(jl_fish_image[i]);
			jp_add_fish_info[i].add(jl_fish_age_text[i]);
			jp_add_fish_info[i].add(jl_fish_satiety_text[i]);
			jp_add_fish_info[i].add(jl_fish_size_text[i]);
			jp_add_fish_info[i].add(jl_fish_other[i]);
			jp_add_fish_info[i].add(jl_fish_other2[i]);
			jp_add_fish_info[i].add(jl_fish_value[i]);
			jp_add_fish_info[i].add(jb_sell_fish[i]);
			jp_add_fish_info[i].setBackground(Color.WHITE);
			
			jp_all.add(jp_add_fish_info[i]);
			
		}
	}
	
	
	public void close()
	{
		this.setVisible(false);
	}
	public void actionPerformed(ActionEvent e)
	{
		int sell_success = 10;
		int sell_number_fish_kind1=0;
		int sell_number_fish_kind2=0;
		int sell_number_fish_total=0;
		
		for (int i=0;i<user.total_fish_number;i++)
		{
			if (e.getSource()==jb_sell_fish[i])
			{
				sell_success=JOptionPane.showConfirmDialog(null,"你確定要販售?");
				
				if (sell_success==0)
				{
					if (fish[i].fish_kind==0)
					{
						++sell_number_fish_kind1;
						++sell_number_fish_total;
					}
					else if (fish[i].fish_kind==1)
					{
						++sell_number_fish_kind2;
						++sell_number_fish_total;
					}
					
					if (fish[i].age>=3 && fish[i].age<6)
					{
						fish[i].is_live=false;
						user.money+=50;
						
						close();
					}
					else if (fish[i].age>=6)
					{
						fish[i].is_live=false;
						user.money+=500;
						close();
					}
					else if (fish[i].age<3)
					{
						fish[i].is_live=false;
						close();
					}
				}
			}
			
			if (sell_success==0 && fish[i+1]!=null)
			{
				fish[i]=fish[i+1];
				
			}
		}
		
		user.total_fish_number-=sell_number_fish_total;
		user.kind1_fish_number-=sell_number_fish_kind1;
		user.kind2_fish_number-=sell_number_fish_kind2;
		
		
	}
}
