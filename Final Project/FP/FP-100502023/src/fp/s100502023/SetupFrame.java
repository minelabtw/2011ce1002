package fp.s100502023;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class SetupFrame extends JFrame implements ActionListener
{
	private JPanel jp_setup = new JPanel();
	
	private JButton jb_saveFile = new JButton("存檔");
	private JButton jb_setMusic = new JButton("音樂設置");
	private JButton jb_setBackground = new JButton("手動設置背景");
	private JButton jb_exit_save = new JButton("離開");
	
	private GamePanel gamepanel;
	private User user;
	private ReadWriteUserData readWriteUserData;
	
	
	public SetupFrame(GamePanel input_gamepanel)
	{
		gamepanel=input_gamepanel;
		user=gamepanel.user;
		readWriteUserData=gamepanel.readWriteUserData;
		//frameWork=input_frameWork;
		
		jp_setup.setLayout(new GridLayout(4,1,10,10));
		jp_setup.add(jb_saveFile);
		jp_setup.add(jb_setMusic);
		jp_setup.add(jb_setBackground);
		jp_setup.add(jb_exit_save);
		//add listener
		addActionListener_button();
		
		add(jp_setup);
	}
	public void addActionListener_button()
	{
		jb_saveFile.addActionListener(this);
		jb_setMusic.addActionListener(this);
		jb_setBackground.addActionListener(this);
		jb_exit_save.addActionListener(this);

	}
	public void actionPerformed(ActionEvent e) 
	{
		if (e.getSource()==jb_saveFile)
		{
			try 
			{
				readWriteUserData.Write_user_Data(user.user_index,user.username,user.money,user.experience,user.total_fish_number,user.kind1_fish_number,user.kind2_fish_number,user.fish_data);
				JOptionPane.showMessageDialog(null,"Finish!");
			} 
			catch (Exception e1) 
			{
				System.out.println("Wrong:save user data");
			}
		}
		else if (e.getSource()==jb_setMusic)
		{
			//ReadFile music_file = new ReadFile();
			//frameWork.music.set_Background_music(music_file.read_Music_data());
			
		}
		else if (e.getSource()==jb_setBackground)
		{
			ReadFile read_file = new ReadFile();
			read_file.read_Image_data();
			gamepanel.mainPanel.bowl_image=read_file.return_image_file();
			gamepanel.repaint();
		}
		else if (e.getSource()==jb_exit_save)
		{//存檔
			try 
			{
				readWriteUserData.Write_user_Data(user.user_index,user.username,user.money,user.experience,user.total_fish_number,user.kind1_fish_number,user.kind2_fish_number,user.fish_data);
				JOptionPane.showMessageDialog(null,"Save user data . Finish!");
			} 
			catch (Exception e1) 
			{
				System.out.println("Wrong:save user data");
			}
			
			System.exit(0);
		}
		
	}
	
	
}
