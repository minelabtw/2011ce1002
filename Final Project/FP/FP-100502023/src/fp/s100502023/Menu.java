package fp.s100502023;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.*;

import fp.s100502023.Game_MainPanel.TimerListener;

public class Menu extends JPanel implements ActionListener
{

	private JLabel jl_menu = new JLabel();
	
	protected JButton jb_start = new JButton("Start");
	protected JButton jb_directions = new JButton("Directions");
	protected JButton jb_quit = new JButton("Quit");
	
	protected ActionEvent e_menu ;

	
	public Menu()
	{
		jl_menu.setLayout(null);
		jl_menu.setIcon(new ImageIcon("Image/menu_background.jpg"));
		
		jb_start.setBounds(350, 310, 100, 50);
		jb_directions.setBounds(350, 370, 100, 50);
		jb_quit.setBounds(350, 430, 100, 50);
		jl_menu.add(jb_start);
		jl_menu.add(jb_directions);
		jl_menu.add(jb_quit);
		add(jl_menu,BorderLayout.CENTER);
		this.validate();
	}
	

	public void actionPerformed(ActionEvent arg0)
	{
		
		
	}
	
	

	


}
