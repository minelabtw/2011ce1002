package fp.s100502023;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import fp.s100502023.Shop.TimerListener;

public class GamePanel extends JPanel
{
	protected Game_MainPanel mainPanel;	
	private JLabel titleLabel = new JLabel();
	private JLabel optionLabel = new JLabel();
	
	private JLabel jl_title = new JLabel();
	private JLabel jl_option = new JLabel();
	private JLabel jl_total = new JLabel();
	
	private JLabel game_mark = new JLabel();
	private JLabel user_name = new JLabel("User:");
	private JLabel user_name_value = new JLabel();
	private JLabel user_experience = new JLabel("經驗:");
	private JLabel user_experience_value = new JLabel();
	private JLabel user_money = new JLabel("金錢:");
	private JLabel user_money_value = new JLabel();
	
	protected JButton jb_fish_status = new JButton("狀態");
	protected JButton jb_fish_feeding = new JButton("餵食");
	protected JButton jb_fish_shop = new JButton("商店");
	protected JButton jb_setUp = new JButton("設置");
	protected JButton jb_describe = new JButton("說明");
	//private JButton jb_exit = new JButton("離開");
	
	private Shop shop ;
	private Timer timer = new Timer(125,new TimerListener());
	
	//private Timer timer_music = new Timer(1000,new MusicListener());
	private int time_second;
	
	protected User user;
	protected ReadWriteUserData readWriteUserData;
	protected SetupFrame setup;
	protected FishStatus fishStatus;
	//private MusicPlayer music = new MusicPlayer();
	
	public GamePanel(User input_user,ReadWriteUserData input_readWriteUserData,FrameWork input_Frame)
	{
		mainPanel = new Game_MainPanel(input_user);
		shop = new Shop(this,input_user);
		user=input_user;
		readWriteUserData=input_readWriteUserData;
		setup = new SetupFrame(this);
				
		setLayout(null);
		setTitleLabel();
		setOptionLabel();
		
		add_Title_Option();
		
		mainPanel.setBounds(0,60,800,440);
		add(mainPanel);
		timer.start();
		//timer_music.start();
		this.setBackground(Color.WHITE);
		
	}
	public void setTitleLabel() 
	{
		game_mark.setBounds(0, 10, 400, 50);
		//game_mark.setFont(font);
		game_mark.setForeground(Color.BLUE);
		user_name.setBounds(400,10,50,50);
		user_name_value.setBounds(450,10,50,50);
		user_experience.setBounds(500, 10, 50, 50);
		user_experience_value.setBounds(550, 10, 50, 50);
		user_money.setBounds(600, 10, 50, 50);
		user_money_value.setBounds(650,10,50,50);
		
	}
	
	public void setOptionLabel()
	{
		//(500,530)
		jb_fish_status.setBounds(250, 500, 100, 50);
		jb_fish_feeding.setBounds(350, 500, 100, 50);
		jb_fish_shop.setBounds(450, 500, 100, 50);
		jb_setUp.setBounds(550, 500, 100, 50);
		jb_describe.setBounds(650, 500, 100, 50);
	}
	
	public void set_ShopFrame()
	{
		shop.setVisible(true);
		shop.setSize(600, 400);
		shop.setLocationRelativeTo(null);
	}
	
	public void set_SetupFrame()
	{
		setup.setVisible(true);
		setup.setSize(300, 300);
		setup.setLocationRelativeTo(null);
	}
	
	public void set_FishStatus()
	{
		fishStatus = new FishStatus(user,mainPanel.fish);
		//fishStatus.setDefaultCloseOperation(fishStatus.r);
		fishStatus.setVisible(true);
		fishStatus.setSize(720,500);
		//fishStatus.addFishStatus();
	}
	
	public void add_Title_Option()
	{
		add(game_mark);
		add(user_name);
		add(user_name_value);
		add(user_experience);
		add(user_experience_value);
		add(user_money);
		add(user_money_value);
		add(jb_fish_status);
		add(jb_fish_feeding);
		add(jb_fish_shop);
		add(jb_setUp);
		add(jb_describe);
	}
	
	public void add_User_Information(User user)
	{
		user_name_value.setText(user.username);
		user_experience_value.setText(String.valueOf(user.experience));
		user_money_value.setText(String.valueOf(user.money));
		validate();
	}
	public void buy_fish(int add_number,int kind_index)
	{
		//增加魚的數量
		
		mainPanel.add_fish(add_number, kind_index);
		shop.setVisible(false);
	}
	public void buy_background(int kind_index)
	{
		//更換背景
		mainPanel.change_background(kind_index);
		shop.setVisible(false);
	}
	public void setFeedingButton(boolean is_feeding_Mode)
	{
		if (is_feeding_Mode)
		{
			jb_fish_feeding.setText("結束餵食");
		}
		else
		{
			jb_fish_feeding.setText("餵食");
		}
	}
	class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			user_experience_value.setText(String.valueOf(user.experience));
			user_money_value.setText(String.valueOf(user.money));
			repaint();
		}
	}
	class MusicListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			++time_second;
			
			if (time_second==60)
			{
				//music.play_Ring();
				time_second=0;
			}
		}
	}
}
