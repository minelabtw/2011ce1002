package fp.s100502023;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.*;

public class Bowl 
{
	private ImageIcon background1 = new ImageIcon("Image/background/background.jpg");
	private ImageIcon background2 = new ImageIcon("Image/background/background2.jpg");
	
	private Image bowl_image = background1.getImage();	
	
	public Image get_bowel_image()
	{
		return bowl_image;
	}
	public Image change_return_BackgroundImage(int kindindex)
	{
		if (kindindex==0)
		{
			bowl_image= background1.getImage();	
		}
		else if (kindindex==1)
		{
			bowl_image= background2.getImage();	
		}
		
		return bowl_image;
	}
}
