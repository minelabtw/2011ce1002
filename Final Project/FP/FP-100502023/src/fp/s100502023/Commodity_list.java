package fp.s100502023;

import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;

public class Commodity_list 
{
	//fish
	protected int numberOfImage_Fish = 2;
	protected int numberOfImage_Bowl = 2;
	
	
	protected ImageIcon [] fish_image = new ImageIcon[numberOfImage_Fish];
	//background
	protected ImageIcon [] bowl_image = new ImageIcon[numberOfImage_Bowl];
	//protected ImageIcon bowl_image_1 = new ImageIcon("Image/background/background_ad.jpg");
	
	public Commodity_list()
	{
		for (int i=0;i<numberOfImage_Fish;i++)
		{
			fish_image[i] = new ImageIcon("Image/Commodity/fish"+(i+1)+".png");
			bowl_image[i] = new ImageIcon("Image/Commodity/background"+(i+1)+".png");
		}
	}
	
	public ImageIcon Set_returnImageIcon(int name,int index)
	{//direcction(����:0 �k��:1)
	//name(�ӫ~�W��)fish=0;background=1
		ImageIcon returnImage = null;
		
		if(name==0)
		{
			returnImage=fish_image[index];
		}
		else if(name==1)
		{
			returnImage=bowl_image[index];
		}
		
		return returnImage;
	}

}
