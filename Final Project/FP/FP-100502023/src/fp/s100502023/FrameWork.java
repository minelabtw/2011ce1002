package fp.s100502023;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import javax.swing.*;
import javax.swing.border.LineBorder;


public class FrameWork extends JFrame
{
	
	
	private Bowl bowl = new Bowl();
	//private Fish  fish;
	//private int numberOfFish=0;
	
	
	private Menu menu = new Menu();
	private ReadWriteUserData access_data = new ReadWriteUserData();
	private User user = new User();
	private ChooseUser chooseUser = new ChooseUser();
	private GamePanel gamepanel;
	
	private boolean is_feeding_Mode=false;
	protected MusicPlayer music;
	
	public FrameWork()
	{
		
		add(menu);
		addActionListener_Menu_buttons();
		music = new MusicPlayer();
		
	}
	
	public void addActionListenerToButton()
	{
		
	
	}
	public void set_Menu()
	{
		add(menu);
		menu.repaint();
		//addActionListener_Menu_buttons();
		this.validate();
	}

	public void set_ChooseUser() throws Exception
	{
		chooseUser.read_User_IO(1,access_data.user1_data_exist,access_data);
		chooseUser.read_User_IO(2,access_data.user2_data_exist,access_data);
		chooseUser.read_User_IO(3,access_data.user3_data_exist,access_data);
		add(chooseUser);
		chooseUser.repaint();
		addActionListener_ChooseUser_buttons();
		this.validate();
	}
	public void set_GamePanel()
	{
		gamepanel = new GamePanel(user,access_data,this);
		gamepanel.add_User_Information(user);
		add(gamepanel);
		addActionListener_GamePanel_buttons();
		this.validate();
	}


	public void addActionListener_Menu_buttons()
	{
		menu.jb_start.addActionListener(new MenuActionListener());
		menu.jb_directions.addActionListener(new MenuActionListener());
		menu.jb_quit.addActionListener(new MenuActionListener());
	}
	
	public void addActionListener_ChooseUser_buttons()
	{
		chooseUser.jb_select_user1.addActionListener(new ChooseUserActionListener());
		chooseUser.jb_select_user2.addActionListener(new ChooseUserActionListener());
		chooseUser.jb_select_user3.addActionListener(new ChooseUserActionListener());
		chooseUser.jb_enter_game.addActionListener(new ChooseUserActionListener());
		chooseUser.jb_return_menu.addActionListener(new ChooseUserActionListener());
		chooseUser.jb_delete_user.addActionListener(new ChooseUserActionListener());
	}
	
	
	private void addActionListener_GamePanel_buttons() 
	{
		gamepanel.jb_describe.addActionListener(new GamePanelActionListener());
		gamepanel.jb_fish_feeding.addActionListener(new GamePanelActionListener());
		gamepanel.jb_fish_shop.addActionListener(new GamePanelActionListener());
		gamepanel.jb_fish_status.addActionListener(new GamePanelActionListener());
		gamepanel.jb_setUp.addActionListener(new GamePanelActionListener());
	}
	class MenuActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) 
		{
			if (e.getSource()==menu.jb_start)
			{
				remove(menu);
				try 
				{
					set_ChooseUser();
				} 
				catch (Exception e1) 
				{
					System.out.println("set_ChooseUser() is wrong");
				}
			}
			else if (e.getSource()==menu.jb_directions)
			{
				//unfinish
			}
			else if (e.getSource()==menu.jb_quit)
			{
				System.exit(0);
			}
		}	
	}
	class ChooseUserActionListener implements ActionListener
	{

		boolean is_newUser = false;
		
		public void actionPerformed(ActionEvent e) 
		{	
			//出現紅色邊框，代表已選取
			if (e.getSource()==chooseUser.jb_select_user1)
			{
				chooseUser.set_user_selected(1);
				chooseUser.jb_select_user1.setBorder(new LineBorder(Color.RED));
				chooseUser.jb_select_user2.setBorder(null);
				chooseUser.jb_select_user3.setBorder(null);
				
			}
			else if(e.getSource()==chooseUser.jb_select_user2)
			{
				chooseUser.set_user_selected(2);
				chooseUser.jb_select_user1.setBorder(null);
				chooseUser.jb_select_user2.setBorder(new LineBorder(Color.RED));
				chooseUser.jb_select_user3.setBorder(null);
			}
			else if (e.getSource()==chooseUser.jb_select_user3)
			{
				chooseUser.set_user_selected(3);
				chooseUser.jb_select_user1.setBorder(null);
				chooseUser.jb_select_user2.setBorder(null);
				chooseUser.jb_select_user3.setBorder(new LineBorder(Color.RED));
			}
			
			if (e.getSource()==chooseUser.jb_enter_game)
			{	
				
				if (chooseUser.user1_is_selected)
				{
					try 
					{
						access_data.Read_User_Data(1);
					} 
					catch (Exception e1) 
					{
						is_newUser=true;
						System.out.println("Data(1) is not exists");
					}
					
					user.user_index=1;
				}
				else if (chooseUser.user2_is_selected)
				{
					try 
					{
						access_data.Read_User_Data(2);
					} 
					catch (Exception e1)
					{
						is_newUser=true;	
						System.out.println("Data(2) is not exists");
					}
					
					user.user_index=2;
				}
				else if (chooseUser.user3_is_selected)
				{
					try 
					{
						access_data.Read_User_Data(3);
					} 
					catch (Exception e1)
					{
						is_newUser=true;
						System.out.println("Data(3) is not exists");
					}
					
					user.user_index=3;
				}
				
				if (is_newUser==false)
				{
					user.username=access_data.user_name;
					user.experience=access_data.user_experience;
					user.fish_data=access_data.fish_data;
					user.kind1_fish_number=access_data.user_kind1_fish_number;
					user.kind2_fish_number=access_data.user_kind2_fish_number;
					user.money=access_data.user_money;
					user.total_fish_number=access_data.user_total_fish_number;
						
					remove(chooseUser);
					set_GamePanel();
				}
				else
				{
					user.username=JOptionPane.showInputDialog("Enter your name:");
					if (user.username!=null)
					{
						JOptionPane.showMessageDialog(null, "Welcome to bowl!");
						user.experience=0;
						user.kind1_fish_number=0;
						user.kind2_fish_number=0;
						user.money=500;
						user.total_fish_number=0;
						
						remove(chooseUser);
						set_GamePanel();
					}
					
				}
			}
			else if (e.getSource()==chooseUser.jb_return_menu)
			{
				
				set_Menu();
				remove(chooseUser);
			}
			else if (e.getSource()==chooseUser.jb_delete_user)
			{
				
				if (chooseUser.user1_is_selected && access_data.user1_data_exist)
				{
					access_data.user1=null;
					JOptionPane.showMessageDialog(null, "delete the user data");
					//修
					chooseUser.jb_select_user1.setText("New user");
					chooseUser.jl_user1_description.setText("None,please press the \"New user\" button to begin.");
				}
				else if (chooseUser.user2_is_selected && access_data.user2_data_exist)
				{
					access_data.user2=null;
					JOptionPane.showMessageDialog(null, "delete the user data");
					chooseUser.jb_select_user2.setText("New user");
					chooseUser.jl_user2_description.setText("None,please press the \"New user\" button to begin.");
				}
				else if (chooseUser.user3_is_selected && access_data.user3_data_exist)
				{
					access_data.user3=null;
					JOptionPane.showMessageDialog(null, "delete the user data");
					chooseUser.jb_select_user3.setText("New user");
					chooseUser.jl_user3_description.setText("None,please press the \"New user\" button to begin.");
				}
				
				chooseUser.repaint();
				
			}
		}
			
	}
	
	class GamePanelActionListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) 
		{
			if (e.getSource()==gamepanel.jb_fish_status)
			{
				gamepanel.set_FishStatus();
			}
			else if (e.getSource()==gamepanel.jb_fish_feeding)
			{
				is_feeding_Mode=!is_feeding_Mode;
				gamepanel.setFeedingButton(is_feeding_Mode);
				gamepanel.mainPanel.is_feeding(is_feeding_Mode);
			}
			else if (e.getSource()==gamepanel.jb_fish_shop)
			{
				gamepanel.set_ShopFrame();
			}
			else if (e.getSource()==gamepanel.jb_setUp)
			{
				gamepanel.set_SetupFrame();
			}
			else if (e.getSource()==gamepanel.jb_describe)
			{
				Description description = new Description();
				description.setVisible(true);
				description.setSize(80,80);
				description.setLocationRelativeTo(null);
			}
			
		}
		
	}
}
