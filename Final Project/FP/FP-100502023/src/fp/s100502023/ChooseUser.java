package fp.s100502023;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.*;
import javax.swing.border.LineBorder;

public class ChooseUser extends JPanel 
{
	protected JButton jb_select_user1 = new JButton("user1");
	protected JButton jb_select_user2 = new JButton("user2");
	protected JButton jb_select_user3 = new JButton("user3");
	
	protected JButton jb_enter_game = new JButton("進入遊戲");
	protected JButton jb_delete_user = new JButton("刪除使用者");
	protected JButton jb_return_menu = new JButton("返回主選單");
	
	protected JLabel jl_user1_description = new JLabel("none");
	protected JLabel jl_user2_description = new JLabel("none");
	protected JLabel jl_user3_description = new JLabel("none");
	
	private JLabel jl_choose_all = new JLabel();
	
	protected boolean user1_is_selected=false;
	protected boolean user2_is_selected=false;
	protected boolean user3_is_selected=false;
	
	public ChooseUser() 
	{
		setLayout();
	}
	
	public void setLayout()//set panel layout
	{	
		jl_choose_all.setLayout(null);
		jl_choose_all.setIcon(new ImageIcon("Image/background_main4_ad2.jpg"));
		
		jb_select_user1.setBounds(140,60, 100, 100);
		jb_select_user2.setBounds(140,210, 100, 100);
		jb_select_user3.setBounds(140,360, 100, 100);
		
		jl_user1_description.setBounds(270, 60, 400, 100);
		jl_user1_description.setBorder(new LineBorder(Color.BLACK));
		
		jl_user2_description.setBounds(270, 210, 400, 100);
		jl_user3_description.setBounds(270, 360, 400, 100);
		
		jl_user2_description.setBorder(new LineBorder(Color.BLACK));
		jl_user3_description.setBorder(new LineBorder(Color.BLACK));
		
		jb_enter_game.setBounds(420, 500, 100, 50);
		jb_delete_user.setBounds(550, 500, 100, 50);
		jb_return_menu.setBounds(680, 500, 100, 50);
		
		jl_choose_all.add(jb_select_user1);
		jl_choose_all.add(jb_select_user2);
		jl_choose_all.add(jb_select_user3);
		jl_choose_all.add(jl_user1_description);
		jl_choose_all.add(jl_user2_description);
		jl_choose_all.add(jl_user3_description);
		jl_choose_all.add(jb_enter_game);
		jl_choose_all.add(jb_delete_user);
		jl_choose_all.add(jb_return_menu);
		
		add(jl_choose_all,BorderLayout.CENTER);
	}
	
	public void set_user_selected(int index)
	{
		if (index==1)
		{
			user1_is_selected=true;
			user2_is_selected=false;
			user3_is_selected=false;
		}
		else if (index==2)
		{
			user1_is_selected=false;
			user2_is_selected=true;
			user3_is_selected=false;
		}
		else if (index==3)
		{
			user1_is_selected=false;
			user2_is_selected=false;
			user3_is_selected=true;
		}
	}
	public void read_User_IO(int user_index,boolean user_data_is_exist,ReadWriteUserData data) throws Exception 
	{//read or set user information
		//java.io.File user1 = new java.io.File("UserInformation/user1.txt");
		//java.io.File user2 = new java.io.File("UserInformation/user2.txt");
		//java.io.File user3 = new java.io.File("UserInformation/user3.txt");
		
		//Scanner input1;
		//Scanner input2;
		//Scanner input3;
		
		if (user_index==1)
		{//user1
			if (user_data_is_exist)
			{
				data.Read_User_Data(1);
				jb_select_user1.setText(data.user_name);
				jl_user1_description.setText("金額: "+ data.user_money + "  \t經驗值: "+data.user_experience +"  \t總魚數量: " +data.user_total_fish_number);
			}
			else
			{
				jb_select_user1.setText("New user");
				jl_user1_description.setText("None,please press the \"New user\" button to begin.");
			}
		}
		else if (user_index==2)
		{//user2
			if (user_data_is_exist)
			{
				data.Read_User_Data(2);
				jb_select_user2.setText(data.user_name);
				jl_user2_description.setText("金額: "+ data.user_money + "  \t經驗值: "+data.user_experience +"  \t總魚數量: " +data.user_total_fish_number);
			}
			else
			{
				jb_select_user2.setText("New user");
				jl_user2_description.setText("None,please press the \"New user\" button to begin.");
			}
		}
		else if (user_index==3)
		{//user1
			if (user_data_is_exist)
			{
				data.Read_User_Data(3);
				jb_select_user3.setText(data.user_name);
				jl_user3_description.setText("金額: "+ data.user_money + "  \t經驗值: "+data.user_experience +"  \t總魚數量: " +data.user_total_fish_number);
			}
			else
			{
				jb_select_user3.setText("New user");
				jl_user3_description.setText("None,please press the \"New user\" button to begin.");
			}
		}
		
		/*
		try 
		{
			input1= new Scanner(user1);
			while(input1.hasNext())
			{
				user1_name=input1.next();
				user1_money=input1.nextInt();
				user1_experience=input1.nextInt();
				user1_total_fish_number=input1.nextInt();
				user1_kind1_fish_number=input1.nextInt();
				user1_kind2_fish_number=input1.nextInt();
			}

			jb_select_user1.setText(user1_name);
			jl_user1_description.setText("金額: "+ user1_money + "  \t經驗值: "+user1_experience +"  \t總魚數量: " +user1_total_fish_number);
	
			input1.close();
		} 
		catch (FileNotFoundException e) 
		{
			jb_select_user1.setText("New user");
			jl_user1_description.setText("None,please press the \"New user\" button to begin.");
		}
			
		try 
		{
			input2= new Scanner(user2);
			while(input2.hasNext())
			{
				user2_name=input2.next();
				user2_money=input2.nextInt();
				user2_experience=input2.nextInt();
				user2_total_fish_number=input2.nextInt();
				user2_kind1_fish_number=input2.nextInt();
				user2_kind2_fish_number=input2.nextInt();
			}

			jb_select_user2.setText(user2_name);
			jl_user2_description.setText("金額: "+ user2_money + "  \t經驗值: "+user2_experience +"  \t總魚數量: " +user2_total_fish_number);
		
			input2.close();
		} 
		catch (FileNotFoundException e) 
		{
			jb_select_user2.setText("New user");
			jl_user2_description.setText("None,please press the \"New user\" button to begin.");
		}
		
		try 
		{
			input3= new Scanner(user3);
			while(input3.hasNext())
			{
				user3_name=input3.next();
				user3_money=input3.nextInt();
				user3_experience=input3.nextInt();
				user3_total_fish_number=input3.nextInt();
				user3_kind1_fish_number=input3.nextInt();
				user3_kind2_fish_number=input3.nextInt();
			}

			jb_select_user3.setText(user3_name);
			jl_user3_description.setText("金額: "+ user3_money + "  \t經驗值: "+user3_experience +"  \t總魚數量: " +user3_total_fish_number);

			input3.close();
		} 
		catch (FileNotFoundException e) 
		{
			jb_select_user3.setText("New user");
			jl_user3_description.setText("None,please press the \"New user\" button to begin.");
		}
		*/
	}
	
	
}
