package fp.s100502023;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class ReadWriteUserData 
{
	protected java.io.File user1 = new java.io.File("UserInformation/user1.txt");
	protected java.io.File user2 = new java.io.File("UserInformation/user2.txt");
	protected java.io.File user3 = new java.io.File("UserInformation/user3.txt");
	
	protected java.io.PrintWriter writer ;
	protected boolean access_user1_data=false;
	protected boolean access_user2_data=false;
	protected boolean access_user3_data=false;
	
	protected boolean user1_data_exist=false;
	protected boolean user2_data_exist=false;
	protected boolean user3_data_exist=false;
	//user data
	
	protected String user_name;
	protected int user_money;
	protected int user_experience;
	protected int user_total_fish_number;
	protected int user_kind1_fish_number;
	protected int user_kind2_fish_number;

	protected int [][] fish_data = new int [30][2];
			
	public ReadWriteUserData()
	{
		if (user1.exists())
		{
			System.out.println("user1 data exists");
			user1_data_exist=true;
		}
		if (user2.exists())
		{
			System.out.println("user2 data exists");
			user2_data_exist=true;
		}
		if (user3.exists())
		{
			System.out.println("user3 data exists");
			user3_data_exist=true;
		}

	}
	
	public void Write_user_Data(int user_index,String name,int money,int experience,int total_fish_number,int number_fish_kind1,int number_fish_kind2,int[][] fish_data_output) throws Exception
	{
		System.out.println("write data");
		
		if (user_index==1)
		{
			writer = new java.io.PrintWriter(user1);
			System.out.println("user1");
		}
		else if (user_index==2)
		{
			writer = new java.io.PrintWriter(user2);
			System.out.println("user2");
		}
		else if (user_index==3)
		{
			writer = new java.io.PrintWriter(user3);
			System.out.println("user3");
		}
		System.out.println(name);
		System.out.println(money);
		System.out.println(experience);
		System.out.println(total_fish_number);
		System.out.println(number_fish_kind1);
		System.out.println(number_fish_kind2);
		System.out.println(fish_data_output);
		
		writer.print(name+" ");
		writer.print(money+" ");
		writer.print(experience+" ");

		writer.print(total_fish_number+" ");
		writer.print(number_fish_kind1+" ");
		writer.print(number_fish_kind2+" ");
		
		for(int i=0;i<total_fish_number;i++)
		{
			for (int j=0;j<2;j++)
			{
				writer.print(fish_data_output[i][j]+" ");
			}
		}
		
		writer.close();
	}
	
	public void Read_User_Data(int user_index) throws Exception
	{
		Scanner input = null;
		
		if (user_index==1)
		{
			input= new Scanner(user1);
		}
		else if (user_index==2)
		{
			input= new Scanner(user2);
		}
		else if (user_index==3)
		{
			input= new Scanner(user3);
		}
		
		while(input.hasNext())
		{
			user_name=input.next();
			user_money=input.nextInt();
			user_experience=input.nextInt();
			user_total_fish_number=input.nextInt();
			user_kind1_fish_number=input.nextInt();
			user_kind2_fish_number=input.nextInt();
			
			//fish_data=new int[user_total_fish_number][2];
			
			for (int i=0;i<user_total_fish_number;i++)
			{
				for (int j=0;j<2;j++)
				{
					fish_data[i][j]=input.nextInt();
					System.out.println(fish_data[i][j]);
				}
			}
		}

		input.close();
	}
	
	
}
