package f;
import javax.swing.*;
import java.applet.*;
public class Music extends JApplet {//主標題的音樂
	private AudioClip startT;
	private AudioClip cPlus;
	private AudioClip cMinu;
	private AudioClip cChange;
	private AudioClip goGo;
	public Music(){
	    startT = Applet.newAudioClip(getClass().getResource("audio/T/start.mid"));
	    cPlus = Applet.newAudioClip(getClass().getResource("audio/T/cPlus.wav"));
	    cMinu = Applet.newAudioClip(getClass().getResource("audio/T/cMinu.wav"));
	    cChange = Applet.newAudioClip(getClass().getResource("audio/T/cChange.wav"));
	    goGo = Applet.newAudioClip(getClass().getResource("audio/T/go.wav"));
	}
	public void startTGo(){
			startT.loop();
	}
	public void startTStop(){
		startT.stop();
	}
	public void cPlusGo(){
		if (cPlus == null)
			cPlus.play();
		else{
			cPlus.stop();
			cPlus.play();
		} 	
	}
	public void cMinuGo(){
		if (cMinu== null)
			cMinu.play();
		else{
			cMinu.stop();
			cMinu.play();
		} 
	}
	public void cChangeGo(){
		if (cChange== null)
			cChange.play();
		else{
			cChange.stop();
			cChange.play();
		}
	}
	public void goGoGo(){
		if (goGo== null)
			goGo.play();
		else{
			goGo.stop();
			goGo.play();
		}
	}
}