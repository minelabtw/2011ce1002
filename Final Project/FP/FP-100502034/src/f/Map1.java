package f;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
public  class Map1 extends JPanel implements WindowListener, KeyListener{
	private int propsNo =0;
	private FinalProject kp = new FinalProject();//主標題
	private int xNumber;//地圖x大小
	private int yNumber;//地圖y大小
	private double[][] map;//用來存放那一格地圖是甚麼東西(每個數值代表不同的土地)
	private int[][] characterMoneyWay = new int[4][2];//每個角式的金錢 目前移動方向 
	private double[][] characterXY = new double[4][2];//每個角式處於的坐標
	private Image[] image = new Image[13];//道路圖片
	private Image[][][] characters = new Image[4][4][4];//角色圖	片 含行走圖
	private Image[][] territory = new Image[4][4];//土地 含lv土地圖
	private Image[] icon = new Image[6];//四張功能圖片(前進 道具 存檔 離開) &&道具選擇圖的箭頭 &&中麻醉毒
	private Image[] playerIcon = new Image[4];//角式s大頭照
	private int whichCharacter  = 0;//記錄現在該哪個角色行動
	private Timer timer;
	private double walkNumber = 0;//剩下步數
	private int[] walkAction = new int[4];
	private int iconAction = 4;
	private int whichFunction = 0;
	private JFrame mFrame = new JFrame();
	private int whatING = 0;
	private int territoryWay;
	private String[] playerName  = new String[4];
	private String[] playerNameList = new String[12];//十二個角式的名字
 	private int[] playStatus = new int[4];//角式狀態
	private JButton[] save = new JButton[49];
	private int[] moneyBoardPlace = new int[2];
	private int[] functionBoardPlace = new int[2];
	private int[][] mapFunction;
	private Image[][] mapFIcon = new Image[6][4];
	private int mapFIconC = 0;
	private int[][] props = new int[4][2];
	private int[] playerStopTime = new int[4]; 
	private int moneyPlusMin = 0;
	private int coldScreen = 0;
	private int[] propsXY = new int[2];
	private int whichProp;
	private int[] aiOrNot = new int[4];
	private int[] playerImageNo = new int[4];
	private Music2 m2 = new Music2();
	private int win;
	public Map1(){
		m2.backGroundGo();
	}
	class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){	
			int deadNo = 0;
			win = whichCharacter;
			if(whichCharacter >= 4){
				whichCharacter = 0;
			}
			if(playStatus[whichCharacter] == -1){
				whichCharacter++;
			}
			if(whichCharacter >= 4){
				whichCharacter = 0;
			}
			if(whatING == 0){
					switch(characterMoneyWay[whichCharacter][1]){		
					case 0://向下
						goingDown();
						break;
					case 1:
						goingLeft();
						break;
					case 2:
						goingRight();
						break;
					case 3:
						goingUp();
						break;
					}
					for(int i = 0;i<4;i++){
						if(walkAction[i] == 4){
							walkAction[i] = 0;
						}
					}
			}
			if(aiOrNot[whichCharacter] ==1){
				aiGoGo();
			}
			if(whatING != 8&&whatING!=-1){
				for(int i = 0;i<4;i++){
					if(playStatus[i] == -1){
						deadNo++;
					}
					else{
						win = i;
					}
					if(deadNo == 3){
						whatING = 8;
						coldScreen = 30;
					}
				}
			}
			if(whatING == 8){
				coldScreen--;
				if(coldScreen<=0){
					whatING = -1;
					m2.backGroundStop();
					m2.winGo();	
					timer.stop();
				}
			}
			if(whichCharacter >= 4){
				whichCharacter = 0;
			}
			if(playStatus[whichCharacter] == 1){
				walkNumber = 1;
				whatING = 1;
			}
			if(whatING == 1){
				if(whichCharacter >= 4){
					whichCharacter = 0;
				}
				switch(playStatus[whichCharacter]){//判斷角式狀態為何
				case 0://0為一般狀態
					switch(characterMoneyWay[whichCharacter][1]){
					case 0://向下
						characterXY[whichCharacter][1]+=0.5;
						walkNumber-=0.5;
						if((walkNumber/0.5)%2 ==0){//左右腳的音效判斷
							m2.walkAGo();
						}
						else{
							m2.walkBGo();
						}
						walkAction[whichCharacter]++;
						if((characterXY[whichCharacter][1]*10)%10 == 0){
							goingDown();
						}
						break;
					case 1:
						characterXY[whichCharacter][0]-=0.5;
						walkNumber-=0.5;
						if((walkNumber/0.5)%2 ==0){//左右腳的音效判斷
							m2.walkAGo();
						}
						else{
							m2.walkBGo();
						}
						walkAction[whichCharacter]++;
						if((characterXY[whichCharacter][0]*10)%10 == 0){
							goingLeft();
						}
						break;
					case 2:
						characterXY[whichCharacter][0]+=0.5;
						walkNumber-=0.5;
						if((walkNumber/0.5)%2 ==0){//左右腳的音效判斷
							m2.walkAGo();
						}
						else{
							m2.walkBGo();
						}
						walkAction[whichCharacter]++;
						if((characterXY[whichCharacter][0]*10)%10 == 0){
							goingRight();
						}
						break;
					case 3:
						characterXY[whichCharacter][1]-=0.5;
						walkNumber-=0.5;
						if((walkNumber/0.5)%2 ==0){//左右腳的音效判斷
							m2.walkAGo();
						}
						else{
							m2.walkBGo();
						}
						walkAction[whichCharacter]++;
						if((characterXY[whichCharacter][1]*10)%10 == 0){
							goingUp();
						}
						break;
					}
					for(int i = 0;i<4;i++){
						if(walkAction[i] == 4){
							walkAction[i] = 0;
							break;
						}
					}
					break;
				case -1:
					whatING = 0;
					break;
				case 1:	
					playerStopTime[whichCharacter]--;
					if(playerStopTime[whichCharacter] ==0){
						playStatus[whichCharacter] = 0;
					}
					whatING = 0;
					whichCharacter++;	
					break;
				}
				if(whichCharacter == 4){
					whichCharacter = 0;
				}
				chaekMapFunctionWalk();
				if(walkNumber==0){		
					whatING = 0;
					checkMapFunction();
					territoryC();
					if(whatING ==0)
						whichCharacter++;
				}
			}
			for(int i = 0;i<4;i++){
				if(walkAction[i] == 4){
					walkAction[i] = 0;
					break;
				}
			}
			if(whatING == 6){
				coldScreen--;
				if(coldScreen <=0){
					whichCharacter++;
					whatING = 0;
				}	
			}
			mFrame.repaint();
			mFrame.validate();
			repaint();
		}
	}
	public void loadImage() {
		image[0] = new ImageIcon("Graphics/Autotiles/gR/gR_swap_part1x1.jpg").getImage();//0lv草地
		image[1] = new ImageIcon("Graphics/Autotiles/st/st2_swap_part2x1.jpg").getImage();//外左上角道路
		image[2] = new ImageIcon("Graphics/Autotiles/st/st2_swap_part2x3.jpg").getImage();//外右上角道路
		image[3] = new ImageIcon("Graphics/Autotiles/st/st2_swap_part4x3.jpg").getImage();//外右下角道路
		image[4] = new ImageIcon("Graphics/Autotiles/st/st2_swap_part4x1.jpg").getImage();//外左下角道路
		image[5] = new ImageIcon("Graphics/Autotiles/st/st2_swap_part2x2.jpg").getImage();//上方道路
		image[6] = new ImageIcon("Graphics/Autotiles/st/st2_swap_part3x3.jpg").getImage();//右方道路
		image[7] = new ImageIcon("Graphics/Autotiles/st/st2_swap_part4x2.jpg").getImage();//下方道路
		image[8] = new ImageIcon("Graphics/Autotiles/st/st2_swap_part3x1.jpg").getImage();//左方道路
		image[9] = new ImageIcon("Graphics/Autotiles/st/st2_swap_part1x31.JPG").getImage();//左方道路
		image[10] = new ImageIcon("Graphics/Autotiles/st/st2_swap_part1x32.JPG").getImage();//左方道路
		image[11] = new ImageIcon("Graphics/Autotiles/st/st2_swap_part1x33.JPG").getImage();//左方道路
		image[12] = new ImageIcon("Graphics/Autotiles/st/st2_swap_part1x34.JPG").getImage();//左方道路
		for(int i = 1;i<=4;i++){
			for(int j = 1;j<=4;j++){
				characters[0][i-1][j-1] = new ImageIcon("Graphics/Character/c"+playerImageNo[0]+"/c"+playerImageNo[0]+"_part"+i+"x"+j+".png").getImage();
				characters[1][i-1][j-1] = new ImageIcon("Graphics/Character/c"+playerImageNo[1]+"/c"+playerImageNo[1]+"_part"+i+"x"+j+".png").getImage();
				characters[2][i-1][j-1] = new ImageIcon("Graphics/Character/c"+playerImageNo[2]+"/c"+playerImageNo[2]+"_part"+i+"x"+j+".png").getImage();
				characters[3][i-1][j-1] = new ImageIcon("Graphics/Character/c"+playerImageNo[3]+"/c"+playerImageNo[3]+"_part"+i+"x"+j+".png").getImage();
				territory[i-1][j-1] = new ImageIcon("Graphics/Autotiles/territory/c"+i+"t/p"+i+"LV"+j+".png").getImage();
			}
		}
		icon[0] = new ImageIcon("Graphics/Icon/go.png").getImage();//前進
		icon[1] = new ImageIcon("Graphics/Icon/prop.png").getImage();//道具
		icon[2] = new ImageIcon("Graphics/Icon/load.png").getImage();//存檔
		icon[3] = new ImageIcon("Graphics/Icon/leave.png").getImage();//離開
		icon[4] = new ImageIcon("Graphics/Icon/chooseProp.png").getImage();//選擇哪個道具的箭頭
		icon[5] = new ImageIcon("Graphics/Icon/poisoning.png").getImage();//中麻醉毒的
		for(int i = 0;i<=3;i++){
			playerIcon[i] = new ImageIcon("Graphics/Icon/c"+playerImageNo[i]+".png").getImage();
		}
		playerNameList[0] = "RM之新細明體";
		playerNameList[1] = "小小女劍客";
		playerNameList[2] = "呆呆盔甲兵";
		playerNameList[3] = "狂刀怪客";
		playerNameList[4] = "經驗老兵";
		playerNameList[5] = "可愛女劍士";
		playerNameList[6] = "猫女小刀客";
		playerNameList[7] = "冷酷女劍客";
		playerNameList[8] = "呆呆槍兵";
		playerNameList[9] = "帥帥槍兵";
		playerNameList[10] = "將軍";
		playerNameList[11] = "女騎士";
		playerName[0] = playerNameList[playerImageNo[0]-1];
		playerName[1] = playerNameList[playerImageNo[1]-1];
		playerName[2] = playerNameList[playerImageNo[2]-1];
		playerName[3] = playerNameList[playerImageNo[3]-1];
		mapFIcon[0][0] = new ImageIcon("Graphics/Autotiles/mapF/180-Switch03_swap_part1x4.png").getImage();//傳送洞
		mapFIcon[0][1] = new ImageIcon("Graphics/Autotiles/mapF/180-Switch03_swap_part2x4.png").getImage();
		mapFIcon[0][2] = new ImageIcon("Graphics/Autotiles/mapF/180-Switch03_swap_part3x4.png").getImage();
		mapFIcon[0][3] = new ImageIcon("Graphics/Autotiles/mapF/180-Switch03_swap_part4x4.png").getImage();
		mapFIcon[1][0] = new ImageIcon("Graphics/Autotiles/mapF/180-Switch03_swap_part1x3.png").getImage();//扣錢刺
		mapFIcon[1][1] = new ImageIcon("Graphics/Autotiles/mapF/180-Switch03_swap_part2x3.png").getImage();
		mapFIcon[1][2] = new ImageIcon("Graphics/Autotiles/mapF/180-Switch03_swap_part3x3.png").getImage();
		mapFIcon[1][3] = new ImageIcon("Graphics/Autotiles/mapF/180-Switch03_swap_part4x3.png").getImage();
		mapFIcon[2][0] = new ImageIcon("Graphics/Autotiles/mapF/f0.png").getImage();//麻醉瓶
		mapFIcon[3][0] = new ImageIcon("Graphics/Autotiles/mapF/gold.png").getImage();//加錢金
		mapFIcon[4][0] = new ImageIcon("Graphics/Autotiles/mapF/barrier.gif").getImage();//路障
		mapFIcon[5][0] = new ImageIcon("Graphics/Autotiles/mapF/174-Chest011_part1x1.png").getImage();//汁到道具
		mapFIcon[5][1] = new ImageIcon("Graphics/Autotiles/mapF/174-Chest011_part2x1.png").getImage();
		mapFIcon[5][2] = new ImageIcon("Graphics/Autotiles/mapF/174-Chest011_part3x1.png").getImage();
		mapFIcon[5][3] = new ImageIcon("Graphics/Autotiles/mapF/174-Chest011_part4x1.png").getImage();
	}
	public void paintComponent(Graphics g){
		 super.paintComponent(g);
		 for(int i = 0;i<xNumber;i++){
			 for(int j = 0;j<yNumber;j++){
				 for(int k = 0;k<=9;k++){
					 if(map[i][j] == k)
						 g.drawImage(image[k],j*32,i*32,mFrame);
					 else if(map[i][j]%100 == k*10 && (int)(map[i][j]/100) == 1){
						 g.drawImage(image[k],j*32,i*32,mFrame);
					 }
					 else if(map[i][j]%100 == k*10 && (int)(map[i][j]/100) == 2){
						 g.drawImage(image[k+10],j*32,i*32,mFrame);
					 }
					 else if(map[i][j]%100 == k*10 && (int)(map[i][j]/100) == 3){
						 g.drawImage(image[k],j*32,i*32,mFrame);
					 }
					 else if(map[i][j]%100 == k*10 && (int)(map[i][j]/100) == 4){
						 g.drawImage(image[k],j*32,i*32,mFrame);
					 }
					 else if(map[i][j]%100 == k*10 && (int)(map[i][j]/100) == 5){
						 g.drawImage(image[k],j*32,i*32,mFrame);
					 }
				 }
				 for(int k = 1;k<=4;k++){//領地
					 for(int l = 1;l<=4;l++){
						 if(map[i][j] == k*10+l)
							 g.drawImage(territory[k-1][l-1],j*32,i*32,mFrame);
					 }
				 }
			 }
		 }
		 if(playStatus[0] != -1)
			 g.drawImage(characters[0][characterMoneyWay[0][1]][walkAction[0]],(int)(characterXY[0][0]*32),(int)(characterXY[0][1]*32-16),mFrame);
		 else
			 g.drawImage(new ImageIcon("Graphics/Character/dead/"+playerImageNo[0]+".png").getImage(),(int)(characterXY[0][0]*32),(int)(characterXY[0][1]*32-16),mFrame);
		 if(playStatus[1] != -1)
			 g.drawImage(characters[1][characterMoneyWay[1][1]][walkAction[1]],(int)(characterXY[1][0]*32),(int)(characterXY[1][1]*32-16),mFrame);
		 else
			 g.drawImage(new ImageIcon("Graphics/Character/dead/"+playerImageNo[1]+".png").getImage(),(int)(characterXY[1][0]*32),(int)(characterXY[1][1]*32-16),mFrame);
		 if(playStatus[2] != -1)
			 g.drawImage(characters[2][characterMoneyWay[2][1]][walkAction[2]],(int)(characterXY[2][0]*32),(int)(characterXY[2][1]*32-16),mFrame);
		 else
			 g.drawImage(new ImageIcon("Graphics/Character/dead/"+playerImageNo[2]+".png").getImage(),(int)(characterXY[2][0]*32),(int)(characterXY[2][1]*32-16),mFrame);
		 if(playStatus[3] != -1)
			 g.drawImage(characters[3][characterMoneyWay[3][1]][walkAction[3]],(int)(characterXY[3][0]*32),(int)(characterXY[3][1]*32-16),mFrame);
		 else
			 g.drawImage(new ImageIcon("Graphics/Character/dead/"+playerImageNo[3]+".png").getImage(),(int)(characterXY[3][0]*32),(int)(characterXY[3][1]*32-16),mFrame);
		 g.setFont( new Font( "新細明體", Font.PLAIN, 12));
		 g.drawString("金錢板：", (moneyBoardPlace[1]+1)*32, moneyBoardPlace[0]*32+16);
		 g.drawImage(icon[4], (moneyBoardPlace[1])*32, moneyBoardPlace[0]*32+32*whichCharacter+24, mFrame);
		 for(int i = 0;i<4;i++){
			 if(playStatus[i] != -1){
					g.drawImage(playerIcon[i], (moneyBoardPlace[1]+1)*32,  moneyBoardPlace[0]*32+32*i+24,mFrame);
					g.setColor(Color.black);
					g.setFont( new Font( "新細明體", Font.PLAIN, 12));
					g.drawString(playerName[i]+"："+characterMoneyWay[i][0], (moneyBoardPlace[1]+2) *32+5,  moneyBoardPlace[0]*32+32*(i+1)+16);	 
			 }
			 if(playStatus[i] == 1){
				 g.setColor(Color.red);
				 g.setFont( new Font( "新細明體", Font.PLAIN, 20));
				 g.drawString(playerStopTime[i]+"", (moneyBoardPlace[1])*32+10, moneyBoardPlace[0]*32+32*(i+1)+10);
				 g.drawImage(icon[5],  (moneyBoardPlace[1]+1)*32,  moneyBoardPlace[0]*32+32*i+24,mFrame);
				 g.drawImage(icon[5],(int)(characterXY[i][0]*32),(int)(characterXY[i][1]*32),mFrame);
			 }
		 }
		 if(whatING == 0){
			 switch(whichFunction){
			 case 0:
				 g.drawImage(icon[0], functionBoardPlace[1], functionBoardPlace[0]*32+iconAction, mFrame);
				 g.drawImage(icon[1], functionBoardPlace[1]+32, functionBoardPlace[0]*32, mFrame);
				 g.drawImage(icon[2], functionBoardPlace[1]+64, functionBoardPlace[0]*32, mFrame);
				 g.drawImage(icon[3], functionBoardPlace[1]+96, functionBoardPlace[0]*32, mFrame);
				 break;
		 	 case 1:
		 		 g.drawImage(icon[0], functionBoardPlace[1], functionBoardPlace[0]*32, mFrame);
		 		 g.drawImage(icon[1], functionBoardPlace[1]+32, functionBoardPlace[0]*32+iconAction, mFrame);
		 		 g.drawImage(icon[2], functionBoardPlace[1]+64, functionBoardPlace[0]*32, mFrame);
		 		 g.drawImage(icon[3], functionBoardPlace[1]+96, functionBoardPlace[0]*32, mFrame);
		 		 break;
			 case 2:
				 g.drawImage(icon[0], functionBoardPlace[1], functionBoardPlace[0]*32, mFrame);
				 g.drawImage(icon[1], functionBoardPlace[1]+32, functionBoardPlace[0]*32, mFrame);
				 g.drawImage(icon[2], functionBoardPlace[1]+64, functionBoardPlace[0]*32+iconAction, mFrame);
				 g.drawImage(icon[3], functionBoardPlace[1]+96, functionBoardPlace[0]*32, mFrame);
				 break;
			 case 3:
				 g.drawImage(icon[0], functionBoardPlace[1], functionBoardPlace[0]*32, mFrame);
				 g.drawImage(icon[1], functionBoardPlace[1]+32, functionBoardPlace[0]*32, mFrame);
				 g.drawImage(icon[2], functionBoardPlace[1]+64, functionBoardPlace[0]*32, mFrame);
				 g.drawImage(icon[3], functionBoardPlace[1]+96, functionBoardPlace[0]*32+iconAction, mFrame);
				 break;
			 }
			 iconAction*=-1;
		 }
		 if(whatING == 7){
			 g.drawImage(mapFIcon[whichProp][0], propsXY[0]*32, propsXY[1]*32, mFrame);
		 }
		 drawMoney(g);
		 drawMapF(g);
		 if(whatING ==5){
			 drawProp(g);
		 }
		 if(whatING == -1){
			 g.setColor(Color.GREEN);
			 g.fillRoundRect(0,0 ,getWidth(), getHeight(), 100, 100);	 
			 g.drawImage(new ImageIcon("Graphics/CharactersIcon/" + playerImageNo[win] +".png").getImage(),(int)(getWidth()/2-55), (int)(getHeight()/2-80),mFrame);
			 g.setFont( new Font( "新細明體", Font.BOLD, 30));
			 switch(win){
			 case 0:
				 g.setColor(Color.pink);
				 break;
			 case 1:
				 g.setColor(Color.black);
				 break;
			 case 2:
				 g.setColor(Color.yellow);
				 break;
			 case 3:
				 g.setColor(Color.blue);
				 break;
			 }
			 g.drawString(playerName[win]+"是我們的大富翁(請按任何鍵離開遊戲)" , (int)(getWidth()/2-55)-300, (int)(getHeight()/2-80)-40);
		 }
	}
	public void setInitialMap(int saveNumber2) throws FException{
		int a = 0;
		try {
			FileReader file = new FileReader("save/"+saveNumber2+".txt");
			FileReader file2 = new FileReader("save/"+saveNumber2+"001.txt");
			Scanner input= new Scanner(file);
			Scanner input2=new Scanner(file2);
			int j = 0;
			int p = 0;
			int xyMoneyInput = 0;
			while(input.hasNext()){
				if(a>xNumber+2){
					throw new FException();
				}
				if(j==0){
					if(input.hasNext()==false){
						throw new FException();
					}
					xNumber = input.nextInt();
					if(input.hasNext()==false){
						throw new FException();
					}
					yNumber = input.nextInt();
					if(input.hasNext()==false){
						throw new FException();
					}
					moneyBoardPlace[0] = input.nextInt();
					if(input.hasNext()==false){
						throw new FException();
					}
					moneyBoardPlace[1] = input.nextInt();
					if(input.hasNext()==false){
						throw new FException();
					}
					functionBoardPlace[0] = input.nextInt();
					if(input.hasNext()==false){
						throw new FException();
					}
					functionBoardPlace[1] = input.nextInt();
					map = new double[xNumber][yNumber];
					mapFunction= new int[xNumber][yNumber];
				}
				if(j<xNumber){
					for(int i= 0;i<yNumber;i++){
						if(input.hasNext()==false){
							throw new FException();
						}
						map[j][i] = input.nextInt();
					}
					j++;
				}
				if(j >= xNumber && p<4){
					if(input.hasNext()==false){
						throw new FException();
					}
					characterXY[p][0] = input.nextDouble();
					if(input.hasNext()==false){
						throw new FException();
					}
					characterXY[p][1] = input.nextDouble();
					if(input.hasNext()==false){
						throw new FException();
					}
					characterMoneyWay[p][xyMoneyInput] = input.nextInt();
					xyMoneyInput++;
					if(input.hasNext()==false){
						throw new FException();
					}
					characterMoneyWay[p][xyMoneyInput] = input.nextInt();
					xyMoneyInput++;
					if(input.hasNext()==false){
						throw new FException();
					}
					playStatus[p] = input.nextInt();
					xyMoneyInput++;
					if(input.hasNext()==false){
						throw new FException();
					}
					playerImageNo[p] = input.nextInt();
					xyMoneyInput++;
					if(input.hasNext()==false){
						throw new FException();
					}
					aiOrNot[p] = input.nextInt();
					xyMoneyInput++;
					if(input.hasNext()==false){
						throw new FException();
					}
					props[p][0] = input.nextInt();
					xyMoneyInput++;
					if(input.hasNext()==false){
						throw new FException();
					}
					playerStopTime[p] = input.nextInt();
					xyMoneyInput++;
					if(input.hasNext()==false){
						throw new FException();
					}
					props[p][1] = input.nextInt();
					xyMoneyInput++;
				}
				if(xyMoneyInput == 8){
					xyMoneyInput = 0;
					p++;
				}
				if(input.hasNext()==false){
					throw new FException();
				}
				if(p>=4){
					whichCharacter = input.nextInt();
				}
				for(int i = 0;i<4;i++){
					walkAction[i] = 0;			
				}
				a++;
			}
			while(input2.hasNext()){
				for(int i = 0;i<xNumber;i++){
					for(int k = 0;k<yNumber;k++){
						mapFunction[i][k] = input2.nextInt();
					}
				}
			}
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null,"沒找到存檔");
			System.exit(0);
		}
	}
	public void territoryC(){		
		coldScreen = 2000/timer.getDelay();
		int b = (int) map[(int) characterXY[whichCharacter][1]][(int) (characterXY[whichCharacter][0]-1)];
		int a = (int) map[(int) (characterXY[whichCharacter][1])+1][(int) characterXY[whichCharacter][0]];
		int c = (int) map[(int) (characterXY[whichCharacter][1])][(int) characterXY[whichCharacter][0]+1];
		int d = (int) map[(int) (characterXY[whichCharacter][1])-1][(int) characterXY[whichCharacter][0]];
		if(b == 0){
			territoryWay = 1;
			whatING = 2;
		}
		else if(b >= (whichCharacter+1)*10 && b<(whichCharacter+2)*10 && b!=(whichCharacter+1)*10+4){
			territoryWay = 1;
			whatING = 3;
		}
		else if(b<100&&b>0){
			m2.payGo();
			moneyPlusMin = -10 * (b%10);
			characterMoneyWay[(int)(b/10)-1][0] -= moneyPlusMin;
			characterMoneyWay[whichCharacter][0] += moneyPlusMin;
			checkMoney(whichCharacter);
			whatING = 6;
		}
		if(a == 0){
			territoryWay = 0;
			whatING = 2;
		}
		else if(a >= (whichCharacter+1)*10 && a<(whichCharacter+2)*10 && a!=(whichCharacter+1)*10+4){
			territoryWay = 0;
			whatING = 3;
		}
		else if(a<100&&a>0){
			m2.payGo();
			moneyPlusMin = -10 * (a%10);
			characterMoneyWay[(int)(a/10)-1][0] -= moneyPlusMin;
			characterMoneyWay[whichCharacter][0] += moneyPlusMin;
			checkMoney(whichCharacter);
			whatING = 6;
		}
		if(c == 0){
			territoryWay = 2;
			whatING = 2;
		}
		else if(c >= (whichCharacter+1)*10 && c<(whichCharacter+2)*10 && c!=(whichCharacter+1)*10+4){
			territoryWay = 2;
			whatING = 3;
		}
		else if (c<100&&c>0){
			m2.payGo();
			moneyPlusMin = -10 * (c%10);
			characterMoneyWay[(int)(c/10)-1][0] -= moneyPlusMin ;
			characterMoneyWay[whichCharacter][0] += moneyPlusMin;
			checkMoney(whichCharacter);
			whatING = 6;
		}
		if(d == 0){
			territoryWay = 3;
			whatING = 2;
		}
		else if(d >= (whichCharacter+1)*10 && d<(whichCharacter+2)*10 && d!=(whichCharacter+1)*10+4){
			territoryWay = 3;
			whatING = 3;
		}
		else if(d<100 && d>0){
			m2.payGo();
			moneyPlusMin = -10 * (d%10);
			characterMoneyWay[(int)(d/10)-1][0] -= moneyPlusMin;
			characterMoneyWay[whichCharacter][0] += moneyPlusMin;
			checkMoney(whichCharacter);
			whatING = 6;
		}
		if(whatING!=2&&whatING!=3&&whatING!=6){
			moneyPlusMin = 0;
			coldScreen = 0;
			whatING =6;
		}
	}
	public void keyPressed(KeyEvent e) {
		if(whatING == -1){
			System.exit(0);
		}
		if(whatING == 0){
			switch(e.getKeyCode()){
			case KeyEvent.VK_RIGHT:
				if(whichFunction <3){
					m2.functionCGo();
					whichFunction++;
				}	
				break;
			case KeyEvent.VK_LEFT:
				if(whichFunction >0){
					m2.functionCGo();
					whichFunction--;
				}
				break;
			case KeyEvent.VK_SPACE:
				m2.confirmGo();
				if(whichFunction == 0){
					walkNumber = (int)(Math.random()*10+1);
					whatING = 1;	
				}
				if(whichFunction == 1){
					whatING = 5;
				}
				if(whichFunction == 2){
					whatING = 4;
					WriteFile();
				}
				if(whichFunction == 3){
					mFrame.dispose();
					kp.a();
					kp.startMusic();
					m2.backGroundStop();
					timer.stop();
				}
				break;
			}
		}
		if(whichFunction == 1 && whatING ==5){
			switch(e.getKeyCode()){
			case KeyEvent.VK_UP:
				if(propsNo>0){
					m2.functionCGo();
					propsNo--;
				}		
				break;
			case KeyEvent.VK_DOWN:
				if(propsNo<3){
					m2.functionCGo();
					propsNo++;
				}
				break;
			case KeyEvent.VK_ENTER:
				m2.confirmGo();
				switch(propsNo){
				case 0:
					if(props[whichCharacter][0]>=1&&mapFunction[(int)characterXY[whichCharacter][1]][(int)characterXY[whichCharacter][0]]==-1){
						m2.mapF2BGo();
						mapFunction[(int)characterXY[whichCharacter][1]][(int)characterXY[whichCharacter][0]] = 2;			
						props[whichCharacter][0]--;
						whatING = 0;
						whichFunction=0;
						whichCharacter++;	
					}
					else{
						m2.mapF4Go();
						JOptionPane.showMessageDialog(null,"你沒有麻醉瓶啦!");
					}
					break;
				case 1:
					if(props[whichCharacter][1]>=1){
						propsXY[0] = (int) characterXY[whichCharacter][0];
						propsXY[1] = (int) characterXY[whichCharacter][1];
						whatING = 7;
						whichProp = 4;
					}
					else{
						m2.mapF4Go();
						JOptionPane.showMessageDialog(null,"你沒有路障啦!");
					}
					break;
				}
				break;
			case KeyEvent.VK_ESCAPE:
				m2.escGo();
				whatING = 0;
				break;
			}
		}
		if(whatING == 2){
			switch(e.getKeyCode()){
			case KeyEvent.VK_Y:
				TerritoryBuy();
				break;
			case KeyEvent.VK_N:
				m2.noGo();
				whichCharacter++;
				whatING = 0;
				break;
			default:
				m2.mapF4Go();
				JOptionPane.showMessageDialog(null,"買地請按Y, 不買請按N");
				break;
			}
		}
		if(whatING == 3){
			switch(e.getKeyCode()){
			case KeyEvent.VK_Y:
				TerritoryUp();
				whichCharacter++;
				whatING = 0;
				break;
			case KeyEvent.VK_N:
				m2.noGo();
				whichCharacter++;
				whatING = 0;
				break;
			default:
				m2.mapF4Go();
				JOptionPane.showMessageDialog(null,"升級請按Y, 不升請按N");
				break;
			}
		}
		if(whatING == 4){
			if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
				m2.escGo();
				removeAll();
				whatING = 0;	
			}
		}
		if(whatING == 7){
				switch(e.getKeyCode()){
				case KeyEvent.VK_UP:
					m2.mapF4CGo();
					if(propsXY[1]==0||Math.abs(propsXY[0]-(int) (characterXY[whichCharacter][0])) +Math.abs(propsXY[1]-(int) (characterXY[whichCharacter][1])) >= 3){
						propsXY[0] = (int) characterXY[whichCharacter][0];
						propsXY[1] = (int) characterXY[whichCharacter][1];
					}
					else{
						propsXY[1]--;
					}

					break;
				case KeyEvent.VK_DOWN:
					m2.mapF4CGo();
					if(Math.abs(propsXY[0]-(int) (characterXY[whichCharacter][0])) +Math.abs(propsXY[1]-(int) (characterXY[whichCharacter][1])) >= 3){
						propsXY[0] = (int) characterXY[whichCharacter][0];
						propsXY[1] = (int) characterXY[whichCharacter][1];
					}
					else{
						propsXY[1]++;
					}	
					break;
				case KeyEvent.VK_LEFT:
					m2.mapF4CGo();
					if(propsXY[0]==0||Math.abs(propsXY[0]-(int) (characterXY[whichCharacter][0])) +Math.abs(propsXY[1]-(int) (characterXY[whichCharacter][1])) >= 3){
						propsXY[0] = (int) characterXY[whichCharacter][0];
						propsXY[1] = (int) characterXY[whichCharacter][1];
					}
					else{
						propsXY[0]--;
					}
					
					break;
				case KeyEvent.VK_RIGHT:
					m2.mapF4CGo();
					if(Math.abs(propsXY[0]-(int) (characterXY[whichCharacter][0])) +Math.abs(propsXY[1]-(int) (characterXY[whichCharacter][1])) >= 3){
						propsXY[0] = (int) characterXY[whichCharacter][0];
						propsXY[1] = (int) characterXY[whichCharacter][1];
					}
					else{
						propsXY[0]++;
					}
					break;
				case KeyEvent.VK_SPACE:
					m2.confirmGo();
					if( (int)(map[propsXY[1]][propsXY[0]]/100) >0&&mapFunction[propsXY[1]][propsXY[0]]==-1){
						props[whichCharacter][1]--;
						mapFunction[propsXY[1]][propsXY[0]] = 4;
						whatING = 0;
						whichFunction = 0;
						whichCharacter++;
					}
					
					else{
						m2.mapF4Go();
						JOptionPane.showMessageDialog(null,"這裡不能設路障");
					}
					break;
				case KeyEvent.VK_ESCAPE:
					m2.escGo();
					whatING = 5;
					break;
			}	
		}
	}
	public void WriteFile(){
		JPanel p = new JPanel();
		JLabel q = new JLabel("要存儲在那個檔案中?");
		q.setOpaque(true);
		q.setPreferredSize(new Dimension(1,100));
		q.setFont(new Font("標楷體", Font.BOLD, 32));
		q.setHorizontalAlignment(JLabel.CENTER);
		q.setBackground(Color.yellow);
		setLayout(new BorderLayout(5, 10));
		p.setOpaque(false);
		p.setLayout(new GridLayout(7,7,5,5));
		Listener listener = new Listener();
		this.add(q,BorderLayout.NORTH);
		for(int i = 0;i<save.length;i++){
			save[i] = new JButton("" + (i+1));
			save[i].addActionListener(listener);
			p.add(save[i]);
		}
		this.add(p,BorderLayout.CENTER);
	}
	class Listener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			for(int i = 0;i<save.length;i++){
				if(e.getSource() == save[i]){
					m2.saveGo();
					try {
						FileWriter saveF = new FileWriter("save/" + (i+1) + ".txt");
						FileWriter saveF2 = new FileWriter("save/" + (i+1) + "001.txt");
						BufferedWriter out = new BufferedWriter(saveF);
						BufferedWriter out2 = new BufferedWriter(saveF2);
						out.write(xNumber + " ");
						out.write(yNumber + " ");
						out.write(moneyBoardPlace[0]+ " ");
						out.write(moneyBoardPlace[1] + " ");
						out.write(functionBoardPlace[0] + " ");
						out.write(functionBoardPlace[1] + " ");
						for(int j = 0;j<xNumber;j++){
							for(int k = 0;k<yNumber;k++){
								out.write((int)(map[j][k])+" ");
								out2.write(mapFunction[j][k]+ " ");
							}
						}
						for(int j = 0;j<4;j++){
							for(int k = 0;k<2;k++){
								out.write((int)(characterXY[j][k]) + " ");
							}
							for(int k = 0;k<2;k++){	
								out.write(characterMoneyWay[j][k]+ "  ");
							}
							out.write(playStatus[j]+" ");
							out.write(playerImageNo[j]+ " ");
							out.write(aiOrNot[j] + " ");
							out.write(props[j][0]+ " ");
							out.write(playerStopTime[j]+ " ");
							out.write(props[j][1]+" ");
						}
						out.write(whichCharacter + "");
						out.close();
						out2.close();
					}catch (IOException e1) {
						System.err.println("ERROR");
					}
					finally{
						removeAll();
						whatING = 0;
					}
				}
			}
		}
	}
	public void TerritoryUp(){
		m2.lvUpGo();
		switch(territoryWay){
		case 0:
			map[(int) (characterXY[whichCharacter][1])+1][(int) characterXY[whichCharacter][0]]++;
			break;
		case 1:
			map[(int) (characterXY[whichCharacter][1])][(int) characterXY[whichCharacter][0]-1]++;
			break;
		case 2:
			map[(int) (characterXY[whichCharacter][1])][(int) characterXY[whichCharacter][0]+1]++;
			break;
		case 3:
			map[(int) (characterXY[whichCharacter][1])-1][(int) characterXY[whichCharacter][0]]++;	
			break;
		}
		coldScreen = 1;
		whatING = 6;
	}
	public void TerritoryBuy(){
		moneyPlusMin = -20;
		coldScreen = 2000/timer.getDelay();
		m2.tBuyGo();
		switch(territoryWay){
		case 0:
			map[(int) (characterXY[whichCharacter][1])+1][(int) characterXY[whichCharacter][0]] = (whichCharacter+1) *10 + 1;
			characterMoneyWay[whichCharacter][0] += moneyPlusMin;
			checkMoney(whichCharacter);
			break;
		case 1:
			map[(int) (characterXY[whichCharacter][1])][(int) characterXY[whichCharacter][0]-1] = (whichCharacter+1) *10 + 1;
			characterMoneyWay[whichCharacter][0] += moneyPlusMin;
			checkMoney(whichCharacter);
			break;
		case 2:
			map[(int) (characterXY[whichCharacter][1])][(int) characterXY[whichCharacter][0]+1] = (whichCharacter+1) *10 + 1;
			characterMoneyWay[whichCharacter][0] += moneyPlusMin;
			checkMoney(whichCharacter);
			break;
		case 3:
			map[(int) (characterXY[whichCharacter][1])-1][(int) characterXY[whichCharacter][0]] = (whichCharacter+1) *10 + 1;
			characterMoneyWay[whichCharacter][0] += moneyPlusMin;
			checkMoney(whichCharacter);
			break;
		}
		whatING = 6;
	}
	public void checkMoney(int character){
		if(characterMoneyWay[character][0]<0){
				playStatus[character] = -1;
				clearTerritory(character);
		}
	}
	public void clearTerritory(int characterNo){
		for(int i = 0;i<map.length;i++){
			for(int j = 0 ;j<map[0].length;j++){
				if(map[i][j] >= ((characterNo+1) *10)&&map[i][j]<((characterNo+2) *10)){
					map[i][j] = 0;
				}
			}
		}
	}
	public void windowClosing(WindowEvent arg0) {	
		kp.a();
		kp.startMusic();
		m2.backGroundStop();
		timer.stop();
		System.gc();
	}
	public void goingDown(){
		switch((int)(map[(int) characterXY[whichCharacter][1]][(int) characterXY[whichCharacter][0]])){
		case 140:
			characterMoneyWay[whichCharacter][1] = 2;
			break;
		case 130:
			characterMoneyWay[whichCharacter][1] = 1;
			break;
		case 460:
			characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2);
			break;
		case 370:
			characterMoneyWay[whichCharacter][1] = 1;
			break;
		case 380:
			characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)*2;
			break;
		case 470:
			characterMoneyWay[whichCharacter][1] = 2;
			break;
		case 570:
			characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)+1;
			break;
		case 560:
			characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2);
			break;
		case 580:
			characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)*2;
			break;
		case 220:
			if((int)(map[(int) characterXY[whichCharacter][1]][(int) characterXY[whichCharacter][0]-1])==210||(int)(map[(int) characterXY[whichCharacter][1]][(int) characterXY[whichCharacter][0]+1])==210){
				characterMoneyWay[whichCharacter][1] = (int)(Math.random()*3);
			}	
			else
				characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)*2;
			break;
		case 210:
			if((int)(map[(int) characterXY[whichCharacter][1]][(int) characterXY[whichCharacter][0]-1])==220||(int)(map[(int) characterXY[whichCharacter][1]][(int) characterXY[whichCharacter][0]+1])==220){
				characterMoneyWay[whichCharacter][1] = (int)(Math.random()*3);
			}
			else
				characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2);
			break;
		case 170:
			characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)+1;
			break;
		default:
			characterMoneyWay[whichCharacter][1] = 0;
			break;
		}
		for(int i = 0;i<4;i++){
			if(walkAction[i] == 4){
				walkAction[i] = 0;
			}
		}
	}
	public void goingLeft(){
		switch((int)(map[(int) characterXY[whichCharacter][1]][(int) characterXY[whichCharacter][0]])){
		case 110:
			characterMoneyWay[whichCharacter][1] = 0;
			break;
		case 140:
			characterMoneyWay[whichCharacter][1] = 3;
			break;
		case 350:
			characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2);
			break;
		case 470:
			characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)*2+1;
			break;
		case 380:
			characterMoneyWay[whichCharacter][1] = 3;
			break;
		case 480:
			characterMoneyWay[whichCharacter][1] = 0;
			break;
		case 570:
			characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)*2+1;
			break;
		case 580:
			characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)*3;
			break;
		case 550:
			characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2);
			break;
		case 190:
			if((int)(map[(int) characterXY[whichCharacter][1]-1][(int) characterXY[whichCharacter][0]])==220||(int)(map[(int) characterXY[whichCharacter][1]+1][(int) characterXY[whichCharacter][0]])==2200){
				characterMoneyWay[whichCharacter][1] = (int)(Math.random()*3);
				if(characterMoneyWay[whichCharacter][1] == 2){
					characterMoneyWay[whichCharacter][1] = 3;
				}
			}
			else
				characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2);
			break;
		case 220:
			if((int)(map[(int) characterXY[whichCharacter][1]-1][(int) characterXY[whichCharacter][0]])==190||(int)(map[(int) characterXY[whichCharacter][1]+1][(int) characterXY[whichCharacter][0]])==190){
				characterMoneyWay[whichCharacter][1] = (int)(Math.random()*3);
				if(characterMoneyWay[whichCharacter][1] == 2){
					characterMoneyWay[whichCharacter][1] = 3;
				}
			}
			else
				characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)*2+1;
			break;
		case 180:
			characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)*3;
			break;
		default:
			characterMoneyWay[whichCharacter][1] = 1;
			break;
		}
		for(int i = 0;i<4;i++){
			if(walkAction[i] == 4){
				walkAction[i] = 0;
			}
		}
	}
	public void goingRight(){
		switch((int)(map[(int) characterXY[whichCharacter][1]][(int) characterXY[whichCharacter][0]])){
		case 120:
			characterMoneyWay[whichCharacter][1] = 0;
			break;
		case 130:
			characterMoneyWay[whichCharacter][1] = 3;
			break;
		case 450:
			characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)*2;
			break;
		case 360:
			characterMoneyWay[whichCharacter][1] = 0;
			break;
		case 460:
			characterMoneyWay[whichCharacter][1] = 3;
			break;
		case 370:
			characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)+2;
			break;
		case 550:
			characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)*2;
			break;
		case 560:
			characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)*3;
			break;
		case 570:
			characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)+2;
			break;
		case 200:
			if((int)(map[(int) characterXY[whichCharacter][1]-1][(int) characterXY[whichCharacter][0]])==210||(int)(map[(int) characterXY[whichCharacter][1]+1][(int) characterXY[whichCharacter][0]])==210){
				characterMoneyWay[whichCharacter][1] = (int)(Math.random()*3)+1;
				if(characterMoneyWay[whichCharacter][1] == 1){
					characterMoneyWay[whichCharacter][1] = 0;
				}
			}
			else
				characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)*2;
			break;
		case 210:
			if((int)(map[(int) characterXY[whichCharacter][1]-1][(int) characterXY[whichCharacter][0]])==200||(int)(map[(int) characterXY[whichCharacter][1]+1][(int) characterXY[whichCharacter][0]])==200){
				characterMoneyWay[whichCharacter][1] = (int)(Math.random()*3)+1;
				if(characterMoneyWay[whichCharacter][1] == 1){
					characterMoneyWay[whichCharacter][1] = 0;
				}
			}
			else
				characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)+2;
			break;
		case 160:
			characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)*3;
			break;
		default:
			characterMoneyWay[whichCharacter][1] = 2;
			break;
		}
		for(int i = 0;i<4;i++){
			if(walkAction[i] == 4){
				walkAction[i] = 0;
			}
		}
	}
	public void goingUp(){
		switch((int)(map[(int) characterXY[whichCharacter][1]][(int) characterXY[whichCharacter][0]])){
		case 110:
			characterMoneyWay[whichCharacter][1] = 2;
			break;
		case 120:
			characterMoneyWay[whichCharacter][1] = 1;
			break;
		case 350:
			characterMoneyWay[whichCharacter][1] = 2;
			break;
		case 450:
			characterMoneyWay[whichCharacter][1] = 1;
			break;
		case 360:
			characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)*2+1;
			break;
		case 480:
			characterMoneyWay[whichCharacter][1] = 2;
			break;
		case 550:
			characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)+1;
			break;
		case 560:
			characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)*2 +1;
			break;
		case 580:
			characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)+2;
			break;
		case 190:
			if((int)(map[(int) characterXY[whichCharacter][1]][(int) characterXY[whichCharacter][0]-1])==200||(int)(map[(int) characterXY[whichCharacter][1]][(int) characterXY[whichCharacter][0]+1])==200){
				characterMoneyWay[whichCharacter][1] = (int)(Math.random()*3)+1;
			}
			else
				characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)+2;
			break;
		case 200:
			if((int)(map[(int) characterXY[whichCharacter][1]][(int) characterXY[whichCharacter][0]-1])==190||(int)(map[(int) characterXY[whichCharacter][1]][(int) characterXY[whichCharacter][0]+1])==190){
				characterMoneyWay[whichCharacter][1] = (int)(Math.random()*3)+1;
			}
			else
				characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)*2+1;
			break;
		case 150:
			characterMoneyWay[whichCharacter][1] = (int)(Math.random()*2)+1;
			break;
		default:
			characterMoneyWay[whichCharacter][1] = 3;
			break;
		}
		for(int i = 0;i<4;i++){
			if(walkAction[i] == 4){
				walkAction[i] = 0;
			}
		}
	}
	public void checkMapFunction(){
		switch(mapFunction[(int)(characterXY[whichCharacter][1])][(int)(characterXY[whichCharacter][0])]){
		case 0:
			m2.mapF0Go();
			int a;
			int b;
			do{
				timer.stop();
				characterXY[whichCharacter][0] = (int)(Math.random()*(yNumber-1));
				characterXY[whichCharacter][1] = (int)(Math.random()*(xNumber-1));
				a = (int)(map[(int) characterXY[whichCharacter][1]][(int) characterXY[whichCharacter][0]]/100);
				b = (int)(mapFunction[(int) characterXY[whichCharacter][1]][(int) characterXY[whichCharacter][0]]);
			}while(a<=0||b!=-1);
			timer.start();
			break;
		case 1:
			m2.mapF1Go();
			moneyPlusMin = (int)(Math.random()*3+1)*10 *(-1);
			coldScreen = 2000/timer.getDelay();
			characterMoneyWay[whichCharacter][0]+=moneyPlusMin;
			checkMoney(whichCharacter);
			whatING = 6;
			break;
		case 2:
			m2.mapF2Go();
			playStatus[whichCharacter] = 1;
			playerStopTime[whichCharacter] = 2;
			mapFunction[(int)(characterXY[whichCharacter][1])][(int)(characterXY[whichCharacter][0])] = -1;
			whatING = 0;
			break;
		case 3:
			m2.mapF3Go();
			moneyPlusMin = (int)(Math.random()*3+1)*10;
			coldScreen = 2000/timer.getDelay();
			characterMoneyWay[whichCharacter][0]+=moneyPlusMin;
			whatING = 6;
			break;
		}
	}
	public void chaekMapFunctionWalk(){
		if((characterXY[whichCharacter][0]*10)%10 == 0&&(characterXY[whichCharacter][1]*10)%10 == 0){
			switch(mapFunction[(int)(characterXY[whichCharacter][1])][(int)(characterXY[whichCharacter][0])]){
			case 4:
				m2.mapF4Go();
				walkNumber = 0;
				mapFunction[(int)(characterXY[whichCharacter][1])][(int)(characterXY[whichCharacter][0])] = -1;
				whatING = 0;
				break;
			case 5:
				m2.mapF5Go();
				props[whichCharacter][0]++;
				props[whichCharacter][1]++;
				break;
			}	
		}
	}
	public void drawMapF(Graphics g){
		if(whatING !=2 && whatING!=3){
			for(int i = 0;i<xNumber;i++){
				for(int j = 0;j<yNumber;j++){
					switch(mapFunction[i][j]){
					case 0 :
						g.drawImage(mapFIcon[0][mapFIconC],j*32,i*32,mFrame);
						break;
					case 1:
						g.drawImage(mapFIcon[1][mapFIconC],j*32,i*32,mFrame);
						break;
					case 2:
						g.drawImage(mapFIcon[2][0],j*32,i*32,mFrame);
						break;
					case 3:
						g.drawImage(mapFIcon[3][0],j*32,i*32,mFrame);
						break;
					case 4:
						g.drawImage(mapFIcon[4][0],j*32,i*32,mFrame);
						break;
					case 5:
						g.drawImage(mapFIcon[5][mapFIconC],j*32,i*32-12,mFrame);
						break;
					}
				}
			}
			mapFIconC++;
			if(mapFIconC == mapFIcon[0].length){
				mapFIconC = 0;
			}
		}
	}
	public void drawProp(Graphics g){
		g.fillRoundRect((int)(getWidth()*0.1),(int)(getHeight()*0.05) ,(int)( getWidth()*0.80), (int)(getHeight()*0.9), 100, 100);
		g.drawImage(icon[4], (int)(getWidth()*0.1)+32, (int)(getHeight()*0.05+(propsNo+1)*32), mFrame);
		g.drawImage(mapFIcon[2][0], (int)(getWidth()*0.1)+32+32, (int)(getHeight()*0.05+32), mFrame);
		g.setColor(Color.white);
		g.setFont( new Font( "新細明體", Font.PLAIN, 32));
		g.drawString("麻醉瓶x"+props[whichCharacter][0], (int)(getWidth()*0.1)+32*3,(int)(getHeight()*0.05+32+20));
		g.drawImage(mapFIcon[4][0], (int)(getWidth()*0.1)+32+32, (int)(getHeight()*0.05+64), mFrame);
		g.drawString("路障x"+props[whichCharacter][1], (int)(getWidth()*0.1)+32*3,(int)(getHeight()*0.05+64+24));
	}
	public void drawMoney(Graphics g){
		if(whatING == 2){
			g.setColor(Color.lightGray);
			g.fillRoundRect((int)(getWidth()*0.1),(int)(getHeight()*0.1) ,(int)( getWidth()*0.8), (int)(getHeight()*0.8), 100, 100);
			g.drawImage(new ImageIcon("Graphics/Message/tBuy.gif").getImage(),(int)(getWidth()*0.5-186),(int)(getHeight()*0.3-43), mFrame);
			g.drawImage(new ImageIcon("Graphics/Message/yes.gif").getImage(),(int)(getWidth()*0.3-76),(int)(getHeight()*0.7-43), mFrame);
			g.drawImage(new ImageIcon("Graphics/Message/no.gif").getImage(),(int)(getWidth()*0.7-76),(int)(getHeight()*0.7-43), mFrame);
		}
		if(whatING == 3){
			g.setColor(Color.lightGray);
			g.fillRoundRect((int)(getWidth()*0.1),(int)(getHeight()*0.1) ,(int)( getWidth()*0.8), (int)(getHeight()*0.8), 100, 100);
			g.drawImage(new ImageIcon("Graphics/Message/tUp.gif").getImage(),(int)(getWidth()*0.5-186),(int)(getHeight()*0.3-43), mFrame);
			g.drawImage(new ImageIcon("Graphics/Message/yes.gif").getImage(),(int)(getWidth()*0.3-76),(int)(getHeight()*0.7-43), mFrame);
			g.drawImage(new ImageIcon("Graphics/Message/no.gif").getImage(),(int)(getWidth()*0.7-76),(int)(getHeight()*0.7-43), mFrame);
		}
		if(whatING == 6){
			g.setColor(Color.white);
			g.setFont( new Font( "新細明體", Font.BOLD, 20));
			g.fillRoundRect((int)(characterXY[whichCharacter][0]*32),(int)(characterXY[whichCharacter][1]*32-32) ,45, 22, 10, 10);
			if(moneyPlusMin<=0){
				g.setColor(Color.red);
			}
			else{
				g.setColor(Color.blue);
			}
			g.drawString(Math.abs(moneyPlusMin)+"",(int)(characterXY[whichCharacter][0]*32+5),(int)(characterXY[whichCharacter][1]*32-16));
		}
	}
	public void aiGoGo(){
		int b = (int) map[(int) characterXY[whichCharacter][1]][(int) (characterXY[whichCharacter][0]-1)];
		int a = (int) map[(int) (characterXY[whichCharacter][1])+1][(int) characterXY[whichCharacter][0]];
		int c = (int) map[(int) (characterXY[whichCharacter][1])][(int) characterXY[whichCharacter][0]+1];
		int d = (int) map[(int) (characterXY[whichCharacter][1])-1][(int) characterXY[whichCharacter][0]];
		if(whatING == 0){
			int aiAction = (int)(Math.random()*5);
			if(aiAction ==0){
				walkNumber = (int)(Math.random()*10+1);
				whatING = 1;
			}
			else if(aiAction ==1){
				if(props[whichCharacter][0]>=1&&mapFunction[(int)characterXY[whichCharacter][1]][(int)characterXY[whichCharacter][0]]==-1&&playStatus[whichCharacter] != 1 ){
					m2.mapF2BGo();
					mapFunction[(int)characterXY[whichCharacter][1]][(int)characterXY[whichCharacter][0]] = 2;			
					props[whichCharacter][0]--;
					whatING = 0;
					whichFunction=0;
					whichCharacter++;
				}
				else{
					walkNumber = (int)(Math.random()*10+1);
					whatING = 1;
				}
			}
			else if(b<100&&b>0||a<100&&a>0||c<100&&c>0||d<100&&d>0){
				if(whichCharacter==(int)(a/10)-1||whichCharacter==(int)(b/10)-1||whichCharacter==(int)(c/10)-1||whichCharacter==(int)(d/10)-1){
					if(props[whichCharacter][1]>0&&	mapFunction[(int)characterXY[whichCharacter][1]][(int)characterXY[whichCharacter][0]]==-1&&playStatus[whichCharacter] != 1 ){
						m2.mapF4BGo();
						mapFunction[(int)characterXY[whichCharacter][1]][(int)characterXY[whichCharacter][0]] = 4;
						props[whichCharacter][1]--;
						whatING = 0;
						whichFunction=0;
						whichCharacter++;
					}
					else{
						walkNumber = (int)(Math.random()*10+1);
						whatING = 1;
					}
				}
				else{
					walkNumber = (int)(Math.random()*10+1);
					whatING = 1;
				}
			}
			else{
				walkNumber = (int)(Math.random()*10+1);
				whatING = 1;
			}
		}
		if(whatING == 2){
			if(characterMoneyWay[whichCharacter][0]>10){
				TerritoryBuy();
			}
			else{
				whichCharacter++;
				whatING = 0;	
			}
		}
		if(whatING == 3){
			TerritoryUp();
			whichCharacter++;
			whatING = 0;
		}
	}
	public void setPlayStatus(int pA,int pB, int pC ,int pD){
		playStatus[0] = pA;
		playStatus[1] = pB;
		playStatus[2] = pC;
		playStatus[3] = pD;
	}
	public void setAi(int pA,int pB,int pC, int pD){
		aiOrNot[0] = pA;
		aiOrNot[1] = pB;
		aiOrNot[2] = pC;
		aiOrNot[3] = pD;
	}
	public void setPlayerNo(int pA,int pB,int pC, int pD){
		playerImageNo[0] = pA;
		playerImageNo[1] = pB;
		playerImageNo[2] = pC;
		playerImageNo[3] = pD;
	}
	public void setPlayXY(int who,int x ,int y){
		characterXY[who][0] = x;
		characterXY[who][1] = y;
	}
	public void starting(){
		timer = new Timer(70, new TimerListener());
		timer.start();
		mFrame.setResizable(false);
		mFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
		mFrame.setTitle("kp大富翁--野區爭霸");
		mFrame.setPreferredSize(new Dimension(yNumber*32+200, xNumber*32+150));
		mFrame.setVisible(true);
		mFrame.addWindowListener(this);
		kp.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		kp.setTitle("kp大富翁");
		kp.setSize(750, 550);
		kp.setVisible(false);
		kp.stopMusic();
		mFrame.setFocusable(true);
		mFrame.pack();
		mFrame.add(this);
		mFrame.addKeyListener(this);	
	}
	public void windowActivated(WindowEvent arg0){}
	public void windowClosed(WindowEvent arg0) {}
	public void windowDeactivated(WindowEvent arg0) {}
	public void windowDeiconified(WindowEvent arg0) {}
	public void windowIconified(WindowEvent arg0) {}
	public void windowOpened(WindowEvent arg0) {}
	public void keyTyped(KeyEvent arg0){}
	public void keyReleased(KeyEvent arg0) {}
}