package f;

import java.applet.Applet;
import java.applet.AudioClip;

import javax.swing.JApplet;

public class Music2 extends JApplet{//遊戲時的音樂
	private AudioClip backGround;
	private AudioClip lvUp;
	private AudioClip tBuy;
	private AudioClip mapF1;
	private AudioClip mapF3;
	private AudioClip mapF0;
	private AudioClip mapF5;
	private AudioClip mapF2;
	private AudioClip mapF4;
	private AudioClip mapF2B;
	private AudioClip mapF4B;
	private AudioClip walkA;
	private AudioClip walkB;
	private AudioClip functionC;
	private AudioClip win;
	private AudioClip pay;
	private AudioClip save;
	private AudioClip confirm;
	private AudioClip esc;
	private AudioClip no;
	private AudioClip mapF4C;
	public Music2(){
		backGround = Applet.newAudioClip(getClass().getResource("audio/GameGoing/backGround.mid"));
		lvUp  = Applet.newAudioClip(getClass().getResource("audio/GameGoing/lvUp.wav"));
		tBuy  = Applet.newAudioClip(getClass().getResource("audio/GameGoing/tBuy.wav"));
		mapF1 = Applet.newAudioClip(getClass().getResource("audio/GameGoing/mapF1.wav"));
		mapF3 = Applet.newAudioClip(getClass().getResource("audio/GameGoing/mapF3.wav"));
		mapF0 = Applet.newAudioClip(getClass().getResource("audio/GameGoing/mapF0.wav"));
		mapF5 = Applet.newAudioClip(getClass().getResource("audio/GameGoing/mapF5.wav"));
		mapF2 = Applet.newAudioClip(getClass().getResource("audio/GameGoing/mapF2.wav"));
		mapF4 = Applet.newAudioClip(getClass().getResource("audio/GameGoing/mapF4.wav"));
		mapF2B = Applet.newAudioClip(getClass().getResource("audio/GameGoing/mapF2B.wav"));
		mapF4B = Applet.newAudioClip(getClass().getResource("audio/GameGoing/mapF4B.wav"));
		walkA = Applet.newAudioClip(getClass().getResource("audio/GameGoing/walkA.wav"));
		walkB = Applet.newAudioClip(getClass().getResource("audio/GameGoing/walkB.wav"));
		functionC = Applet.newAudioClip(getClass().getResource("audio/GameGoing/functionC.wav"));
		win = Applet.newAudioClip(getClass().getResource("audio/GameGoing/win.mid"));
		pay = Applet.newAudioClip(getClass().getResource("audio/GameGoing/pay.wav"));
		save = Applet.newAudioClip(getClass().getResource("audio/GameGoing/save.wav"));
		confirm = Applet.newAudioClip(getClass().getResource("audio/GameGoing/confirm.wav"));
		esc =  Applet.newAudioClip(getClass().getResource("audio/GameGoing/esc.wav"));
		no = Applet.newAudioClip(getClass().getResource("audio/GameGoing/no.wav"));
		mapF4C = Applet.newAudioClip(getClass().getResource("audio/GameGoing/mapF4C.wav"));
	}
	public void backGroundGo(){
		if (backGround == null)
			backGround.loop();
		else{
			backGround.stop();
			backGround.loop();
		} 
	}
	public void backGroundStop(){
		backGround.stop();
	}
	public void lvUpGo(){
		if (lvUp == null)
			lvUp.play();
		else{
			lvUp.stop();
			lvUp.play();
		} 
	}
	public void tBuyGo(){
		if (tBuy == null)
			tBuy.play();
		else{
			tBuy.stop();
			tBuy.play();
		} 
	}
	public void mapF1Go(){
		if (mapF1 == null)
			mapF1.play();
		else{
			mapF1.stop();
			mapF1.play();
		}
	}
	public void mapF3Go(){
		if (mapF3 == null)
			mapF3.play();
		else{
			mapF3.stop();
			mapF3.play();
		}
	}
	public void mapF0Go(){
		if (mapF0 == null)
			mapF0.play();
		else{
			mapF0.stop();
			mapF0.play();
		}
	}
	public void mapF5Go(){
		if (mapF5 == null)
			mapF5.play();
		else{
			mapF5.stop();
			mapF5.play();
		}
	}
	public void mapF2Go(){
		if (mapF2 == null)
			mapF2.play();
		else{
			mapF2.stop();
			mapF2.play();
		}
	}
	public void mapF4Go(){
		if (mapF4 == null)
			mapF4.play();
		else{
			mapF4.stop();
			mapF4.play();
		}
	}
	public void mapF2BGo(){
		if (mapF2B == null)
			mapF2B.play();
		else{
			mapF2B.stop();
			mapF2B.play();
		}
	}
	public void mapF4BGo(){
		if (mapF4B == null)
			mapF4B.play();
		else{
			mapF4B.stop();
			mapF4B.play();
		}
	}
	public void walkAGo(){
		if (walkA == null)
			walkA.play();
		else{
			walkA.stop();
			walkA.play();
		}
	}
	public void walkBGo(){
		if (walkB == null)
			walkB.play();
		else{
			walkB.stop();
			walkB.play();
		}
	}
	public void functionCGo(){
		if (functionC == null)
			functionC.play();
		else{
			functionC.stop();
			functionC.play();
		}
	}
	public void winGo(){
		if (win == null)
			win.play();
		else{
			win.stop();
			win.play();
		}
	}
	public void payGo(){
		if (pay == null)
			pay.play();
		else{
			pay.stop();
			pay.play();
		}
	}
	public void saveGo(){
		if (save == null)
			save.play();
		else{
			save.stop();
			save.play();
		}
	}
	public void confirmGo(){
		if (confirm == null)
			confirm.play();
		else{
			confirm.stop();
			confirm.play();
		}
	}
	public void escGo(){
		if (esc == null)
			esc.play();
		else{
			esc.stop();
			esc.play();
		}
	}
	public void noGo(){
		if (no == null)
			no.play();
		else{
			no.stop();
			no.play();
		}
	}
	public void mapF4CGo(){
		if (mapF4C == null)
			mapF4C.play();
		else{
			mapF4C.stop();
			mapF4C.play();
		}
	}
}
