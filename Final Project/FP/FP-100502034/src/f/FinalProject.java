package f;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
public class FinalProject extends JFrame{
	private JButton startGame = new JButton("開始新遊戲");
	private JButton conGame = new JButton("繼續遊戲");
	private JButton overGame = new JButton("結束遊戲");
	private JPanel pStart = new JPanel();//裝開始繼續離開Button
	private ImageIcon start = new ImageIcon("Graphics/title/start.JPG");//標題圖
	private JLabel image = new JLabel(start);//裝槱題圖的Label
	private JPanel pPLoad = new JPanel();//讀檔panel
	private JButton[] load = new JButton[49];//讀檔button
	private JButton[] setPlayer = new JButton[4];//設定角色button
	private String[] playerNameList = new String[12];//十二個角色的名字宣告
	private int[] playerNo = new int[4];//角色編號
	private int[] playerS = new int[4];//設定角色狀態
	private JPanel plusMinus = new JPanel();//選擇哪個角色
	private JPanel player = new JPanel();//設定角色的panel
	private JButton[] plus = new JButton[4];//下一個角色
	private JButton[] minu = new JButton[4];//前一個角色
	private JButton go = new JButton("啟動遊戲GOGO");//啟動遊戲button
	public static Music m = new Music();
	public FinalProject(){
		m.startTGo();
		for(int i = 0;i<playerNo.length;i++){//設定初始化角色編號及狀態
			playerNo[i] = i+1;
			playerS[i] = 0;
		}
		startGame.setFont(new Font("標楷體", Font.BOLD, 28));
		conGame.setFont(new Font("標楷體", Font.BOLD, 28));
		overGame.setFont(new Font("標楷體", Font.BOLD, 28));
		go.setFont(new Font("標楷體", Font.BOLD, 28));
		playerNameList[0] = "RM之新細明體";
		playerNameList[1] = "小小女劍客";
		playerNameList[2] = "呆呆盔甲兵";
		playerNameList[3] = "狂刀怪客";
		playerNameList[4] = "經驗老兵";
		playerNameList[5] = "可愛女劍士";
		playerNameList[6] = "猫女小刀客";
		playerNameList[7] = "冷酷女劍客";
		playerNameList[8] = "呆呆槍兵";
		playerNameList[9] = "帥帥槍兵";
		playerNameList[10] = "將軍";
		playerNameList[11] = "女騎士";
		setLayout(new BorderLayout(0, 0));
		pStart.setLayout(new GridLayout(1,3));
		pStart.add(startGame);
		pStart.add(conGame);
		pStart.add(overGame);
		pPLoad.add(image, BorderLayout.SOUTH);
		this.add(pStart, BorderLayout.WEST);
		this.add(pPLoad, BorderLayout.NORTH);
		Listener listener = new Listener();
		startGame.addActionListener(listener);
		conGame.addActionListener(listener);
		overGame.addActionListener(listener);
		
	}
	class Listener implements ActionListener {
		public void actionPerformed(ActionEvent e){
			if(e.getSource() == startGame){//開始遊戲button後的畫面
				m.goGoGo();
				pPLoad.removeAll();
				player.removeAll();
				plusMinus.removeAll();
				player.setLayout(new GridLayout(1,4));
				plusMinus.setLayout(new GridLayout(1,8));
				JLabel q = new JLabel("請選擇四位大富翁候選人的出場方式");
				q.setOpaque(true);
				q.setFont(new Font("標楷體", Font.BOLD, 28));
				q.setHorizontalAlignment(JLabel.CENTER);
				q.setBackground(Color.green);
				pPLoad.setLayout(new BorderLayout(5, 0));
				Listener3 listener = new Listener3();
				for(int i = 0;i<setPlayer.length;i++){
					plus[i] = new JButton("下一位角色");
					plus[i].addActionListener(listener);
					minu[i] = new JButton("上一位角色");
					minu[i].addActionListener(listener);
					setPlayer[i] = new JButton();
					setPlayer[i].addActionListener(listener);
					setPlayer[i].setIcon(new ImageIcon("Graphics/CharactersIcon/" + playerNo[i] +".png"));
					setPlayer[i].setHorizontalTextPosition(SwingConstants.CENTER);
					setPlayer[i].setVerticalTextPosition(SwingConstants.BOTTOM);
					player.add(setPlayer[i]);
					plusMinus.add(minu[i]);
					plusMinus.add(plus[i]);
					setPlayer[i].setText("玩家：" + playerNameList[playerNo[i]-1]);
				}
				go.addActionListener(listener);
				pStart.add(go);
				pPLoad.add(player, BorderLayout.CENTER);
				pPLoad.add(plusMinus, BorderLayout.SOUTH);
				pPLoad.add(q, BorderLayout.NORTH);
				add(pPLoad, BorderLayout.NORTH);
				pPLoad.setVisible(true);
				player.setVisible(true);
				plusMinus.setVisible(true);
				setVisible(true);
				System.gc();
			}
			if(e.getSource() == conGame){//繼續遊戲button後的畫面
				m.goGoGo();
				pPLoad.removeAll();
				pStart.removeAll();
				JPanel p = new JPanel();
				p.setLayout(new GridLayout(7,7,5,5));
				JLabel q = new JLabel("要讀取哪個檔案?");
				q.setOpaque(true);
				q.setFont(new Font("標楷體", Font.BOLD, 32));
				q.setHorizontalAlignment(JLabel.CENTER);
				q.setBackground(Color.green);
				pPLoad.setLayout(new BorderLayout(5, 0));
				Listener2 listener = new Listener2();
				for(int i = 0;i<load.length;i++){
					load[i] = new JButton("" + (i+1));
					load[i].addActionListener(listener);
					p.add(load[i]);
				}
				pStart.add(startGame);
				pStart.add(conGame);
				pStart.add(overGame);
				pPLoad.add(q,  BorderLayout.NORTH);	
				pPLoad.add(p,  BorderLayout.CENTER);
				add(pPLoad,BorderLayout.NORTH);
				p.setVisible(true);
				pPLoad.setVisible(true);
				setVisible(true);
			}
			if(e.getSource() == overGame)//離開遊戲
				System.exit(0);
		}
	}
	class Listener2 implements ActionListener {//讀檔listener
		public void actionPerformed(ActionEvent e) {
			for(int i = 0;i<load.length;i++){
				if(e.getSource() == load[i]){
					Map1 m1 = new Map1();
					m.goGoGo();
					try {
						m1.setInitialMap(i+1);
					} catch (FException e1) {
						e1.printStackTrace();
					}
					m1.loadImage();
					m1.starting();
					setVisible(false);
				}
			}
		}
	}
	class Listener3 implements ActionListener {//開始遊戲Button後的listener
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == go){
				m.startTStop();
				m.goGoGo();
				int[] ai = new int[4];
				int[] status = new int[4];
				for(int i = 0;i<ai.length;i++){
					if(playerS[i] == 1){
						ai[i] =1;
					}
					else if(playerS[i] == 0){
						ai[i] =0;
					}
				}
				for(int i = 0;i<status.length;i++){
					if(playerS[i] == 0){
						status[i] = 0;
					}
					else if(playerS[i] == -1){
						status[i] = -1;
					}
				}
				Map1 m1 = new Map1();
				try {
					m1.setInitialMap(-1);
				} catch (FException e1) {			
				}
				m1.setPlayerNo(playerNo[0], playerNo[1], playerNo[2], playerNo[3]);
				m1.setAi(ai[0], ai[1], ai[2], ai[3]);
				m1.setPlayStatus(status[0], status[1], status[2], status[3]);
				m1.loadImage();
				for(int i = 0;i<4;i++){
					if(status[i]==-1){
						m1.setPlayXY(i, 0, 0);
					}
				}
				m1.starting();
				setVisible(false);
				
			}
			if(e.getSource() == plus[0]){
				m.cPlusGo();
				plus0();
			}
			if(e.getSource() == plus[1]){
				m.cPlusGo();
				plus1();
			}
			if(e.getSource() == plus[2]){
				m.cPlusGo();
				plus2();
			}
			if(e.getSource() == plus[3]){
				m.cPlusGo();
				plus3();
			}
			if(e.getSource() == minu[0]){
				m.cMinuGo();
				min0();
			}
			if(e.getSource() == minu[1]){
				m.cMinuGo();
				min1();
			}
			if(e.getSource() == minu[2]){
				m.cMinuGo();
				min2();
			}
			if(e.getSource() == minu[3]){
				m.cMinuGo();
				min3();
			}
			if(e.getSource() == setPlayer[0]){
				m.cChangeGo();
				switch(playerS[0]){
				case 0:
					playerS[0] = -1;
					setPlayer[0].setText("角色不出場");
					break;
				case -1:
					playerS[0] = 1;
					setPlayer[0].setText("AI");
					break;
				case 1:
					playerS[0] = 0;
					setPlayer[0].setText("玩家：" + playerNameList[playerNo[0]-1]);
					break;
				}		
			}
			if(e.getSource() == setPlayer[1]){
				m.cChangeGo();
				switch(playerS[1]){
				case 0:
					playerS[1] = -1;
					setPlayer[1].setText("角色不出場");
					break;
				case -1:
					playerS[1] = 1;
					setPlayer[1].setText("AI");
					break;
				case 1:
					playerS[1] = 0;
					setPlayer[1].setText("玩家：" + playerNameList[playerNo[1]-1]);
					break;
				}		
			}
			if(e.getSource() == setPlayer[2]){
				m.cChangeGo();
				switch(playerS[2]){
				case 0:
					playerS[2] = -1;
					setPlayer[2].setText("角色不出場");
					break;
				case -1:
					playerS[2] = 1;
					setPlayer[2].setText("AI");
					break;
				case 1:
					playerS[2] = 0;
					setPlayer[2].setText("玩家：" + playerNameList[playerNo[2]-1]);
					break;
				}	
			}
			if(e.getSource() == setPlayer[3]){
				m.cChangeGo();
				switch(playerS[3]){
				case 0:
					playerS[3] = -1;
					setPlayer[3].setText("角色不出場");
					break;
				case -1:
					playerS[3] = 1;
					setPlayer[3].setText("AI");
					break;
				case 1:
					playerS[3] = 0;
					setPlayer[3].setText("玩家：" + playerNameList[playerNo[3]-1]);
					break;
				}		
			}
		}
	}
	public void a(){
		setVisible(true);
	}
	public static void main(String[] args) {
		FinalProject f = new FinalProject();
		f.setResizable(false);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setTitle("kp大富翁");
		f.setSize(900, 550);
		f.setVisible(true);
	}
	public void plus0(){//八個角色選擇button (角色不能重複
		if(playerNo[0]+ 1 == playerNo[1]||playerNo[0]+1 == playerNo[2]||playerNo[0]+1 == playerNo[3]){
			if(playerNo[0]+ 2 == playerNo[1]||playerNo[0]+2 == playerNo[2]||playerNo[0]+2 == playerNo[3]){
				if(playerNo[0]+ 3 == playerNo[1]||playerNo[0]+3 == playerNo[2]||playerNo[0]+3 == playerNo[3]){
					if(playerNo[0]+ 4!=13){
						playerNo[0]+=4;
					}
				}
				else if(playerNo[0]+ 3!=13){
					playerNo[0]+=3;
				}
			}
			else if(playerNo[0]+2!=13){
				playerNo[0]+=2;
			}
		}
		else if(playerNo[0]+1!=13){
			playerNo[0]+=1;
		}
		setPlayer[0].setIcon(new ImageIcon("Graphics/CharactersIcon/" + playerNo[0] +".png"));
		switch(playerS[0]){
		case 0:
			setPlayer[0].setText("玩家：" + playerNameList[playerNo[0]-1]);
			break;
		case -1:			
			setPlayer[0].setText("角色不出場");
			break;
		case 1:
			setPlayer[0].setText("AI");			
			break;
		}	
	}
	public void plus1(){
		if(playerNo[1]+ 1 == playerNo[0]||playerNo[1]+1 == playerNo[2]||playerNo[1]+1 == playerNo[3]){
			if(playerNo[1]+ 2 == playerNo[0]||playerNo[1]+2 == playerNo[2]||playerNo[1]+2 == playerNo[3]){
				if(playerNo[1]+ 3 == playerNo[0]||playerNo[1]+3 == playerNo[2]||playerNo[1]+3 == playerNo[3]){
					if(playerNo[1]+ 4!=13){
						playerNo[1]+=4;
					}
				}
				else if(playerNo[1]+ 3!=13){
					playerNo[1]+=3;
				}
			}
			else if(playerNo[1]+2!=13){
				playerNo[1]+=2;
			}
		}
		else if(playerNo[1]+1!=13){
			playerNo[1]+=1;
		}
		setPlayer[1].setIcon(new ImageIcon("Graphics/CharactersIcon/" + playerNo[1] +".png"));
		switch(playerS[1]){
		case 0:
			setPlayer[1].setText("玩家：" + playerNameList[playerNo[1]-1]);
			break;
		case -1:			
			setPlayer[1].setText("角色不出場");
			break;
		case 1:
			setPlayer[1].setText("AI");			
			break;
		}
	}
	public void plus2(){
		if(playerNo[2]+ 1 == playerNo[1]||playerNo[2]+1 == playerNo[0]||playerNo[2]+1 == playerNo[3]){
			if(playerNo[2]+ 2 == playerNo[1]||playerNo[2]+2 == playerNo[0]||playerNo[2]+2 == playerNo[3]){
				if(playerNo[2]+ 3 == playerNo[1]||playerNo[2]+3 == playerNo[0]||playerNo[2]+3 == playerNo[3]){
					if(playerNo[2]+ 4!=13){
						playerNo[2]+=4;
					}
				}
				else if(playerNo[2]+ 3!=13){
					playerNo[2]+=3;
				}
			}
			else if(playerNo[2]+2!=13){
				playerNo[2]+=2;
			}
		}
		else if(playerNo[2]+1!=13){
			playerNo[2]+=1;
		}
		setPlayer[2].setIcon(new ImageIcon("Graphics/CharactersIcon/" + playerNo[2] +".png"));
		switch(playerS[2]){
		case 0:
			setPlayer[2].setText("玩家：" + playerNameList[playerNo[2]-1]);
			break;
		case -1:			
			setPlayer[2].setText("角色不出場");
			break;
		case 1:
			setPlayer[2].setText("AI");			
			break;
		}
	}
	public void plus3(){
		if(playerNo[3]+ 1 == playerNo[1]||playerNo[3]+1 == playerNo[2]||playerNo[3]+1 == playerNo[0]){
			if(playerNo[3]+ 2 == playerNo[1]||playerNo[3]+2 == playerNo[2]||playerNo[3]+2 == playerNo[0]){
				if(playerNo[3]+ 3 == playerNo[1]||playerNo[3]+3 == playerNo[2]||playerNo[3]+3 == playerNo[0]){
					if(playerNo[3]+ 4!=13){
						playerNo[3]+=4;
					}
				}
				else if(playerNo[3]+ 3!=13){
					playerNo[3]+=3;
				}
			}
			else if(playerNo[3]+2!=13){
				playerNo[3]+=2;
			}
		}
		else if(playerNo[3]+1!=13){
			playerNo[3]+=1;
		}
		setPlayer[3].setIcon(new ImageIcon("Graphics/CharactersIcon/" + playerNo[3] +".png"));
		switch(playerS[3]){
		case 0:
			setPlayer[3].setText("玩家：" + playerNameList[playerNo[3]-1]);
			break;
		case -1:			
			setPlayer[3].setText("角色不出場");
			break;
		case 1:
			setPlayer[3].setText("AI");			
			break;
		}	
	}
	public void min0(){
		if(playerNo[0]- 1 == playerNo[1]||playerNo[0]-1 == playerNo[2]||playerNo[0]-1 == playerNo[3]){
			if(playerNo[0]- 2 == playerNo[1]||playerNo[0]-2 == playerNo[2]||playerNo[0]-2 == playerNo[3]){
				if(playerNo[0]- 3 == playerNo[1]||playerNo[0]-3 == playerNo[2]||playerNo[0]-3 == playerNo[3]){
					if(playerNo[0]- 4!=0){
						playerNo[0]-=4;
					}
				}
				else if(playerNo[0]- 3!=0){
					playerNo[0]-=3;
				}
			}
			else if(playerNo[0]-2!=0){
				playerNo[0]-=2;
			}
		}
		else if(playerNo[0]-1!=0){
			playerNo[0]-=1;
		}
		setPlayer[0].setIcon(new ImageIcon("Graphics/CharactersIcon/" + playerNo[0] +".png"));
		switch(playerS[0]){
		case 0:
			setPlayer[0].setText("玩家：" + playerNameList[playerNo[0]-1]);
			break;
		case -1:			
			setPlayer[0].setText("角色不出場");
			break;
		case 1:
			setPlayer[0].setText("AI");			
			break;
		}	
	}
	public void min1(){
		if(playerNo[1]- 1 == playerNo[0]||playerNo[1]-1 == playerNo[2]||playerNo[1]-1 == playerNo[3]){
			if(playerNo[1]- 2 == playerNo[0]||playerNo[1]-2 == playerNo[2]||playerNo[1]-2 == playerNo[3]){
				if(playerNo[1]- 3 == playerNo[0]||playerNo[1]-3 == playerNo[2]||playerNo[1]-3 == playerNo[3]){
					if(playerNo[1]- 4!=0){
						playerNo[1]-=4;
					}
				}
				else if(playerNo[1]- 3!=0){
					playerNo[1]-=3;
				}
			}
			else if(playerNo[1]-2!=0){
				playerNo[1]-=2;
			}
		}
		else if(playerNo[1]-1!=0){
			playerNo[1]-=1;
		}
		setPlayer[1].setIcon(new ImageIcon("Graphics/CharactersIcon/" + playerNo[1] +".png"));
		switch(playerS[1]){
		case 0:
			setPlayer[1].setText("玩家：" + playerNameList[playerNo[1]-1]);
			break;
		case -1:			
			setPlayer[1].setText("角色不出場");
			break;
		case 1:
			setPlayer[1].setText("AI");			
			break;
		}	
	}
	public void min2(){
		if(playerNo[2]- 1 == playerNo[1]||playerNo[2]-1 == playerNo[0]||playerNo[2]-1 == playerNo[3]){
			if(playerNo[2]- 2 == playerNo[1]||playerNo[2]-2 == playerNo[0]||playerNo[2]-2 == playerNo[3]){
				if(playerNo[2]- 3 == playerNo[1]||playerNo[2]-3 == playerNo[0]||playerNo[2]-3 == playerNo[3]){
					if(playerNo[2]- 4!=0){
						playerNo[2]-=4;
					}
				}
				else if(playerNo[2]- 3!=0){
					playerNo[2]-=3;
				}
			}
			else if(playerNo[2]-2!=0){
				playerNo[2]-=2;
			}
		}
		else if(playerNo[2]-1!=0){
			playerNo[2]-=1;
		}
		setPlayer[2].setIcon(new ImageIcon("Graphics/CharactersIcon/" + playerNo[2] +".png"));
		switch(playerS[2]){
		case 0:
			setPlayer[2].setText("玩家：" + playerNameList[playerNo[2]-1]);
			break;
		case -1:			
			setPlayer[2].setText("角色不出場");
			break;
		case 1:
			setPlayer[2].setText("AI");			
			break;
		}	
	}
	public void min3(){
		if(playerNo[3]- 1 == playerNo[1]||playerNo[3]-1 == playerNo[2]||playerNo[3]-1 == playerNo[0]){
			if(playerNo[3]- 2 == playerNo[1]||playerNo[3]-2 == playerNo[2]||playerNo[3]-2 == playerNo[0]){
				if(playerNo[3]- 3 == playerNo[1]||playerNo[3]-3 == playerNo[2]||playerNo[3]-3 == playerNo[0]){
					if(playerNo[3]- 4!=0){
						playerNo[3]-=4;
					}
				}
				else if(playerNo[3]- 3!=0){
					playerNo[3]-=3;
				}
			}
			else if(playerNo[3]-2!=0){
				playerNo[3]-=2;
			}
		}
		else if(playerNo[3]-1!=0){
			playerNo[3]-=1;
		}
		setPlayer[3].setIcon(new ImageIcon("Graphics/CharactersIcon/" + playerNo[3] +".png"));
		switch(playerS[3]){
		case 0:
			setPlayer[3].setText("玩家：" + playerNameList[playerNo[3]-1]);
			break;
		case -1:			
			setPlayer[3].setText("角色不出場");
			break;
		case 1:
			setPlayer[3].setText("AI");			
			break;
		}	
	}
	public void stopMusic(){
		m.startTStop();
	}
	public void startMusic(){
		m.startTGo();
	}
}