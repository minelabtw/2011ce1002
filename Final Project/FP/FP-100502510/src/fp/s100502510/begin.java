package fp.s100502510;

import javax.swing.*;
import sun.audio.*;
import java.io.*;

public class begin extends JPanel {// 遊戲開始畫面
	ImageIcon begin2 = new ImageIcon("image/begin2.png");
	ImageIcon buttonicon = new ImageIcon("image/buttonicon.png");
	JButton begin = new JButton();
	JLabel gulu2 = new JLabel();

	public begin() {

		this.setLayout(null);
		begin.setIcon(buttonicon);
		gulu2.setIcon(begin2);
		begin.setBorder(null);
		gulu2.setBounds(0, 0, begin2.getIconWidth(), begin2.getIconHeight());
		begin.setBounds(540, 460, buttonicon.getIconWidth(),
				buttonicon.getIconHeight());
		this.add(begin);
		this.add(gulu2);
	}

	public JButton getbegin() {
		return begin;
	}

}
