package fp.s100502510;

import javax.sound.sampled.*;

import java.io.*;
import java.net.*;

public class AudioPlayer {// 程式來源:http://blog.xuite.net/ray00000test/blog/52499573-%E6%92%AD%E6%94%BE%E9%9F%B3%E6%A8%82%E3%80%81%E9%9F%B3%E6%95%88(%E6%94%AF%E6%8F%B4WAV,+AIFF,+AU)
	private AudioInputStream currentSound;

	private Clip clip;

	private float gain;
	private FloatControl gainControl;

	// 控制聲道,-1.0f:只有左聲道, 0.0f:雙聲道,1.0f右聲道
	private float pan;
	private FloatControl panControl;

	// 控制靜音 開/關
	private boolean mute;
	private BooleanControl muteControl;

	// 播放次數,小於等於0:無限次播放,大於0:播放次數
	private int playCount = 0;

	private DataLine.Info dlInfo;
	private Object loadReference;
	private AudioFormat format;

	public AudioPlayer() {
		AudioPlayerInit();
	}

	public void AudioPlayerInit() {
		currentSound = null;
		clip = null;
		gain = 0.5f;
		gainControl = null;
		pan = 0.0f;
		panControl = null;
		mute = false;
		muteControl = null;
		playCount = 1;
		dlInfo = null;
	}

	/**
	 * 指定路徑讀取音檔,使用目前物件放置的package當相對路徑root,null時不使用物件路徑為root
	 * 
	 * @param filePath
	 * @param obj
	 *            目前物件放置的package路徑
	 */
	public void loadAudio(String filePath, Object obj) {
		try {
			if (obj != null) {
				loadAudio(obj.getClass().getResourceAsStream(filePath));
			} else {
				loadAudio(new File(filePath));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 從遠端讀取音檔
	 */
	public void loadAudio(URL url) throws Exception {
		loadReference = url;
		currentSound = AudioSystem.getAudioInputStream(url);
		finishLoadingAudio();
	}

	/**
	 * 讀取本地端音檔
	 * 
	 * @param file
	 * @throws Exception
	 */
	public void loadAudio(File file) throws Exception {
		loadReference = file;
		currentSound = AudioSystem.getAudioInputStream(file);
		finishLoadingAudio();
	}

	/**
	 * 從串流讀取音檔
	 * 
	 * @param iStream
	 * @throws Exception
	 */
	public void loadAudio(InputStream iStream) throws Exception {
		loadReference = iStream;

		if (iStream instanceof AudioInputStream) {
			currentSound = (AudioInputStream) iStream;
		} else {
			currentSound = AudioSystem.getAudioInputStream(iStream);
		}
		finishLoadingAudio();
	}

	/**
	 * load完音檔後，進行播放設定
	 */
	protected void finishLoadingAudio() throws Exception {

		format = currentSound.getFormat();

		dlInfo = new DataLine.Info(Clip.class, format,
				((int) currentSound.getFrameLength() * format.getFrameSize()));

		clip = (Clip) AudioSystem.getLine(dlInfo);

		clip.open(currentSound);
		clip.addLineListener(new LineListener() {
			public void update(LineEvent event) {
				if (event.getType().equals(LineEvent.Type.STOP)) {
					// System.out.println("音樂播完"+playCount);

					if (clip != null) {
						clip.stop();
						if (clip.getFramePosition() == clip.getFrameLength()) {
							clip.setFramePosition(0);
						}
					}

					// 判斷播放次數已用完，若無限次則不關閉
					if (playCount > 0) {
						playCount--;
						if (playCount == 0) {
							close();
							return;
						}
					}
					play();
				}
			}
		});
	}

	/**
	 * 播放音檔
	 */
	public void play() {
		if (clip != null) {
			clip.start();
		}
	}

	/**
	 * 停止播放音檔,且將音檔播放位置移回開始處
	 */
	public void stop() {
		if (clip != null) {
			clip.stop();
			clip.setFramePosition(0);
		}
	}

	/**
	 * 
	 * @return
	 */
	public int getFramePosition() {
		try {
			return clip.getFramePosition();
		} catch (Exception e) {
			return -1;
		}
	}

	/**
	 * 取得音檔格式
	 * 
	 * @return
	 */
	public AudioFormat getCurrentFormat() {
		return format;
	}

	/**
	 * 取得音檔的串流
	 * 
	 * @return
	 */
	public AudioInputStream getAudioInputStream() {
		try {
			AudioInputStream aiStream;

			if (loadReference == null) {
				return null;
			} else if (loadReference instanceof URL) {
				URL url = (URL) loadReference;
				aiStream = AudioSystem.getAudioInputStream(url);
			} else if (loadReference instanceof File) {
				File file = (File) loadReference;
				aiStream = AudioSystem.getAudioInputStream(file);
			} else if (loadReference instanceof AudioInputStream) {
				AudioInputStream stream = (AudioInputStream) loadReference;
				aiStream = AudioSystem.getAudioInputStream(stream.getFormat(),
						stream);
				stream.reset();
			} else {

				InputStream inputStream = (InputStream) loadReference;
				aiStream = AudioSystem.getAudioInputStream(inputStream);
			}

			return aiStream;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 目前音檔是否已存在
	 * 
	 * @return
	 */
	public boolean isAudioLoaded() {
		return loadReference != null;
	}

	/**
	 * 關閉音檔
	 */
	public void close() {
		try {
			if (clip != null)
				clip.close();
			if (currentSound != null)
				currentSound.close();
			loadReference = null;
		} catch (Exception e) {
			System.out.println("unloadAudio: " + e);
			e.printStackTrace();
		}

		currentSound = null;
		clip = null;
		gainControl = null;
		panControl = null;
		dlInfo = null;
		loadReference = null;
		muteControl = null;
	}
}
