package fp.s100502510;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

public class Screen extends JPanel {// 主要遊戲畫面

	ImageIcon enemy = new ImageIcon("image/gollum2.png");// 咕嚕的圖
	ImageIcon image = new ImageIcon("image/background2.jpg");// 背景圖
	ImageIcon demonhunter = new ImageIcon("image/Demon Hunter.png");
	ImageIcon barbarian = new ImageIcon("image/野蠻人.png");
	ImageIcon monk = new ImageIcon("image/monk.png");
	int[] enemyy = new int[101];// 用來儲存咕嚕的y座標位置
	int[] start = new int[101];// 開始位置
	int[] flag = new int[101];// 設定敵人的狀態,0是還沒出現,1是出現,2是移動中,3是消除掉
	int charflag;// 用來設定選取角色的flag,1就是狩魔獵人,2是野蠻人,3是武僧
	JLabel[] enemy3 = new JLabel[101];// 有咕嚕圖片的JLabel
	JLabel gungun2 = new JLabel();// 主角的JLabel
	JLabel[] letters = new JLabel[101];// 字串出現的JLabel
	JLabel background = new JLabel();// 背景的JLabel
	Timer timer = new Timer(30, new TimerListener());
	int appear;// 出現字串的亂數
	String[] text = new String[101];// 會出現的字串
	double times = 0;// 用來設置出現的頻率
	int life = 3;// 原始生命值有三條
	Font letter = new Font("Serif", Font.BOLD, 35);
	int miss = 0;// 計算miss的狀態,1是有miss,0是沒有miss
	int flag2 = 1;// 讓主角的JLabel只add一次

	public Screen() {
		for (int i = 0; i < 101; i++) {
			flag[i] = 0;// 設訂初始狀態
		}
		text[0] = "gungun";
		text[1] = "gollum";
		text[2] = "419";
		text[3] = "Hello";
		text[4] = "this must be the car";
		text[5] = "The cell phone is ringing";
		text[6] = "C++ How to Programming";
		text[7] = "#include<iostream>";
		text[8] = "Rolling in the deep";
		text[9] = "public static";
		text[10] = "BW Hawkeye";
		text[11] = "Kp";
		text[12] = "CSIE";
		text[13] = "NCU";
		text[14] = "Rock";
		text[15] = "vans off the wall";
		text[16] = "Thrasher";
		text[17] = "Tokyo is hot";
		text[18] = "Kid-o";
		text[19] = "computer science";
		text[20] = "9GAG";
		text[21] = "The Avengers";
		text[22] = "Sherlock Holmes";
		text[23] = "Android";
		text[24] = "Funk";
		text[25] = "Dragon Ash";
		text[26] = "stairway to heaven";
		text[27] = "Welcome to the jungle";
		text[28] = "Highway to Hell";
		text[28] = "Sweet Child O' Mine";
		text[29] = "ACDC";
		text[30] = "Electric guitar is good";
		text[31] = "Super Mario";
		text[32] = "Debug";
		text[33] = "Eclipse";
		text[34] = "C++";
		text[35] = "HTML";
		text[36] = "FB";
		text[37] = "Facebook";
		text[38] = "TimeLine is horrible";
		text[39] = "ONE PIECE";
		text[40] = "Ice cream on the hot pot";
		text[41] = "True story";
		text[42] = "Dear God";
		text[43] = "VEVO";
		text[44] = "Tizzy bac";
		text[45] = "ABC";
		text[46] = "apple";
		text[47] = "www.yahoo.com.tw";
		text[48] = "Gmail";
		text[49] = "photo shop";
		text[50] = "9:00";
		text[51] = "woman's logic";
		text[52] = "the typing of the dead";
		text[53] = "Dragon ball";
		text[54] = "911Tabs";
		text[55] = "our work is our play";
		text[56] = "google map";
		text[57] = "?!!";
		text[58] = "we love this so bad";
		text[58] = "otherside";
		text[59] = "Final Exam";
		text[60] = "a room model";
		text[61] = "immense";
		text[62] = "Freak";
		text[63] = "creep";
		text[64] = "Radiohead";
		text[65] = "Oasis";
		text[66] = "Slash";
		text[67] = "Allsharer";
		text[68] = "Java";
		text[69] = "yahoo";
		text[70] = "someone that I used to know";
		text[71] = "Guns N' Roses";
		text[72] = "Wii";
		text[73] = "Dexter's Labratory";
		text[74] = "Toy story";
		text[75] = "Galaxy Life";
		text[76] = "Lookbook";
		text[77] = "Doodle";
		text[78] = "PCman combo";
		text[79] = "weekend fever";
		text[80] = "Coldplay";
		text[81] = "underground";
		text[82] = "cabas";
		text[83] = "Fallen";
		text[84] = "weekend stuner";
		text[85] = "calculus";
		text[86] = "#include<cstdlib>";
		text[87] = "using namespace std;";
		text[88] = "int main()";
		text[89] = "Logitech";
		text[90] = "Don't cry";
		text[91] = "It's my life";
		text[92] = "Beatles";
		text[93] = "iphone";
		text[94] = "Adam Sandler";
		text[95] = "Element";
		text[96] = "MILO";
		text[97] = "white beard";
		text[98] = "PSP";
		text[99] = "red hot chili peppers";
		text[100] = "basketball";

		for (int i = 0; i < 101; i++) {// 亂數設定出現的y座標
			enemyy[i] = (int) (Math.random() * 700 + 10);
		}
		this.setLayout(null);// 這樣才能用setBounds

	}

	public void paint(Graphics g) {
		super.paint(g);
		if (flag2 == 1) {// 還沒add主角時
			seticon(charflag);
			this.add(gungun2);
			flag2 = 2;
		} else {// add後什麼都不用做

		}
		for (int i = 0; i < 101; i++) {
			if (flag[i] == 1) {// 出現咕嚕
				letters[i] = new JLabel();
				letters[i].setText(text[i]);
				letters[i].setFont(letter);
				letters[i].setForeground(Color.WHITE);// 使字體成白色
				letters[i].setBounds(start[i], enemyy[i] + 20, 10000, 100);// 字體出現在咕嚕下面
				enemy3[i] = new JLabel();
				enemy3[i].setIcon(enemy);
				enemy3[i].setBounds(start[i], enemyy[i] - 100,
						enemy.getIconWidth(), enemy.getIconHeight());
				this.add(enemy3[i]);
				this.add(letters[i]);
				background.setIcon(image);
				background.setBounds(0, 0, this.getWidth(), this.getHeight());
				this.add(background);
				flag[i] = 2;
			} else if (flag[i] == 2) {
				this.remove(enemy3[i]);// remove掉上一次執行出現的JLabel
				this.remove(letters[i]);
				letters[i] = new JLabel();
				letters[i].setText(text[i]);
				letters[i].setFont(letter);
				letters[i].setForeground(Color.WHITE);
				letters[i].setBounds(start[i], enemyy[i] + 20, 10000, 100);
				enemy3[i] = new JLabel();
				enemy3[i].setIcon(enemy);
				enemy3[i].setBounds(start[i], enemyy[i] - 100,
						enemy.getIconWidth(), enemy.getIconHeight());
				this.add(enemy3[i]);
				this.add(letters[i]);
				background.setIcon(image);
				background.setBounds(0, 0, this.getWidth(), this.getHeight());
				this.add(background);
			} else if (flag[i] == 3) {// 咕嚕死亡

			}

		}

	}

	public String getstring(int i) {// 得到字串
		return text[i];
	}

	public int getlife() {
		return life;
	}

	public void setlife() {// 用來重新開始時設定生命值
		life = 3;
	}

	public JLabel getletter(int i) {// 得到字串的JLabel
		return letters[i];
	}

	public JLabel getenemy(int i) {// 得到咕嚕的label
		return enemy3[i];
	}

	public void setflag(int i) {// 設定咕嚕死亡
		flag[i] = 3;
	}

	public int getmiss() {
		return miss;
	}

	public void setmiss() {// 重設miss
		miss = 0;
	}

	public void setcharflag(int flag) {// 設定角色
		charflag = flag;
	}

	public void timestart() {
		timer.start();
	}

	public void timestop() {
		timer.stop();
	}

	public void seticon(int flag) {// 設定角色
		if (flag == 1) {
			gungun2.setIcon(demonhunter);
			gungun2.setBounds(0, 200, demonhunter.getIconWidth(),
					demonhunter.getIconHeight());
		} else if (flag == 2) {
			gungun2.setIcon(barbarian);
			gungun2.setBounds(0, 300, barbarian.getIconWidth(),
					barbarian.getIconHeight());
		} else if (flag == 3) {
			gungun2.setIcon(monk);
			gungun2.setBounds(-50, 250, monk.getIconWidth(),
					monk.getIconHeight());
		}

	}

	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			appear = (int) (Math.random() * 101);
			if (times % 100 == 0) {// 設定咕嚕出現的頻率,times一直++,整除100時出現
				for (int i = 0; i < 101; i++) {
					if (appear == i && flag[i] != 2) {// 還沒出現時
						flag[i] = 1;
						start[i] = 1600;
					}
				}
				times++;
			} else {
				times++;
			}
			for (int i = 0; i < 101; i++) {
				if (flag[i] == 1 || flag[i] == 2) {// 出現時或移動時移動
					start[i] = start[i] - 5;
				}
			}
			for (int i = 0; i < 101; i++) {
				if (start[i] == 100) {// 碰到主角的y座標就扣血
					life--;
					miss = 1;
				}
			}
			repaint();
		}

	}

}
