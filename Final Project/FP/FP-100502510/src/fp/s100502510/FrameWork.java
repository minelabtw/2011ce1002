package fp.s100502510;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener {// 主要架構

	int score = 0;
	int flag;// 用來設定選取角色的flag,1就是狩魔獵人,2是野蠻人,3是武僧
	Screen screen = new Screen();// 主要遊戲畫面
	JPanel statue = new JPanel();// 包括分數和生命值的Panel
	JPanel down = new JPanel();// 畫面下方的panel
	JLabel Score = new JLabel("Score:" + this.setscore(score));
	JLabel Life = new JLabel("Life:" + this.setlife(screen.getlife()) + "/3");
	Font letter = new Font("Serif", Font.BOLD, 25);
	Font Type = new Font("Serif", Font.BOLD, 15);
	JTextField type = new JTextField(100);// 打字的地方
	Timer timer = new Timer(30, new TimerListener());
	begin begin = new begin();// 開始畫面
	ImageIcon begin2 = new ImageIcon("image/begin2.png");// 開始畫面的background
	ImageIcon finalimage = new ImageIcon("image/finish.jpg");// 遊戲結束畫面的background
	finalgrade finalgrade = new finalgrade();// 最後彈出的顯示分數的視窗
	Character character = new Character();// 選擇角色的panel
	AudioPlayer p = new AudioPlayer();// 打字時發出的槍聲
	AudioPlayer gun = new AudioPlayer();// 開始遊戲按鈕的槍聲
	AudioPlayer ga = new AudioPlayer();// 被打到的慘叫聲
	AudioPlayer blood = new AudioPlayer();// 咕嚕噴血的聲音
	AudioPlayer clear = new AudioPlayer();// 遊戲結束的音效
	AudioPlayer damage = new AudioPlayer();// 咕嚕受傷的聲音
	AudioPlayer bgm = new AudioPlayer();// 遊戲背景音樂
	AudioPlayer zombie = new AudioPlayer();// 咕嚕慘叫的聲音
	AudioPlayer start = new AudioPlayer();// 遊戲剛開始的聲音
	AudioPlayer blood2 = new AudioPlayer();// 主角噴血的聲音
	java.awt.Dimension scr_size = java.awt.Toolkit.getDefaultToolkit()
			.getScreenSize();// 讓我可以設定setLocation

	public FrameWork() {
		blood2.loadAudio("audio/blood02_16.wav", null);
		start.loadAudio("audio/Title_TOD.wav", null);
		start.play();// 播放開始音效
		zombie.loadAudio("audio/zombie_018_16.wav", null);
		bgm.loadAudio("audio/ST1.wav", null);
		damage.loadAudio("audio/damage2_22.wav", null);
		ga.loadAudio("audio/184_J.wav", null);
		gun.loadAudio("audio/start1_22.wav", null);
		blood.loadAudio("audio/blood01_16.wav", null);
		clear.loadAudio("audio/OVR_AR.wav", null);
		type.addKeyListener(new KeyAdapter() {// 這一大串是為了讓玩家每打一個字就發出槍聲
			public void keyPressed(KeyEvent e) {
				switch (e.getKeyCode()) {
				case KeyEvent.VK_A:
					p.play();
					p = new AudioPlayer();// 每播放完一個音效要在new一次才能重複播放
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_B:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_C:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_D:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_E:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_F:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_G:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_H:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_I:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_J:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_K:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_L:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_M:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_N:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_O:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_P:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_Q:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_R:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_S:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_T:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_U:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_V:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_W:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_X:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_Y:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_Z:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_SPACE:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_1:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_2:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_3:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_4:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_5:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_6:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_7:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_8:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_9:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;
				case KeyEvent.VK_0:
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					break;

				}

			}

		});
		p.loadAudio("audio/gun5_22.wav", null);
		finalgrade.gettry().addActionListener(this);
		finalgrade.getout().addActionListener(this);
		begin.getbegin().addActionListener(this);
		character.gethunter().addActionListener(this);
		character.getbar().addActionListener(this);
		character.getmonk().addActionListener(this);
		this.setSize(begin2.getIconWidth() + 15, begin2.getIconHeight() + 15);
		this.add(begin);// 加入開始畫面
	}

	public void paint(Graphics g) {
		super.paint(g);
		for (int i = 0; i < 101; i++) {
			if (screen.getmiss() == 1) {// 受傷時播放慘叫聲和噴血聲
				blood2 = new AudioPlayer();
				blood2.loadAudio("audio/blood02_16.wav", null);
				blood2.play();
				ga.play();
				ga = new AudioPlayer();
				ga.loadAudio("audio/184_J.wav", null);
				screen.setmiss();// 回復設定miss的flag
				Life.setText("Life:" + setlife(screen.getlife()) + "/3");// 重新設定Life標籤的生命值
			}

		}

	}

	public String setscore(int score) {// 用來設定分數
		String score2 = "";
		score2 = score2 + score;
		return score2;
	}

	public String setlife(int life) {// 用來設定生命值
		String life2 = "";
		life2 = life2 + life;
		return life2;
	}

	public int getscore() {
		return score;
	}

	public void setflag(int i) {// 設定選取角色的flag
		flag = i;
	}

	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			if (screen.getlife() == 0) {// 生命值=0時
				screen.timestop();// 停止遊戲畫面運行的timer
				timer.stop();// 停止這個class的timer
				bgm.stop();// 停止播放bgm
				blood2.stop();// 停止播放噴血聲
				clear.play();// 播放結束音樂
				clear = new AudioPlayer();
				clear.loadAudio("audio/OVR_AR.wav", null);
				setLocation((scr_size.width - getWidth()) / 2 + 400,
						(scr_size.height - getHeight()) / 2 + 200);
				setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				setSize(finalimage.getIconWidth() + 10,
						finalimage.getIconHeight() + 10);
				remove(screen);// 移除遊戲畫面跟下面打字和顯示狀況的部分
				remove(down);
				add(finalgrade);// 新增結束畫面的panel
				JOptionPane.showMessageDialog(null, "Your score is:"
						+ getscore());// 顯示分數

			}
			repaint();
		}

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == begin.getbegin()) {// 按下開始按鍵時
			gun.play();
			gun = new AudioPlayer();
			gun.loadAudio("audio/start1_22.wav", null);
			remove(begin);
			setLocation((scr_size.width - this.getWidth()) / 2 - 400,
					(scr_size.height - this.getHeight()) / 2 - 220);
			setSize(1680, 1050);
			add(character);// 新增選擇角色的panel

		} else if (e.getSource() == finalgrade.gettry()) {// 按下重複遊玩時
			bgm = new AudioPlayer();
			bgm.loadAudio("audio/ST1.wav", null);
			bgm.play();
			gun.play();
			gun = new AudioPlayer();
			gun.loadAudio("audio/start1_22.wav", null);
			type.setText("");
			screen = new Screen();// 重新new一個screen,不然會延續上一場遊戲的狀態
			screen.setcharflag(flag);
			screen.timestart();
			score = 0;// 重設分數
			Life.setText("Life:" + this.setlife(screen.getlife()) + "/3");
			remove(finalgrade);
			timer.start();
			Score.setFont(letter);
			Life.setFont(letter);
			statue.setLayout(new GridLayout(2, 1));
			Score.setText("Score:" + setscore(score));
			statue.add(Score);
			statue.add(Life);
			this.setLayout(new BorderLayout());
			this.setSize(1600, 1000);
			setLocation((scr_size.width - this.getWidth()) / 2,
					(scr_size.height - this.getHeight()) / 2);
			this.add(screen, BorderLayout.CENTER);
			down.setLayout(new BorderLayout());
			down.add(statue, BorderLayout.WEST);
			down.add(type, BorderLayout.CENTER);
			this.add(down, BorderLayout.SOUTH);
			type.addActionListener(new ActionListener() // 按enter時發生事件
			{

				public void actionPerformed(ActionEvent ev) {
					p.play();// 發出槍聲
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					for (int i = 0; i < 101; i++) {
						if (type.getText().equals(screen.getstring(i))) {// 當打的字跟咕嚕出現的字一樣時
							zombie.play();// 播放咕嚕的慘叫聲
							zombie = new AudioPlayer();
							zombie.loadAudio("audio/zombie_018_16.wav", null);
							damage.play();
							damage = new AudioPlayer();
							damage.loadAudio("audio/damage2_22.wav", null);
							blood.play();
							blood = new AudioPlayer();
							blood.loadAudio("audio/blood01_16.wav", null);
							screen.setflag(i);// 設定出現的敵人的狀態
							screen.remove(screen.getletter(i));// remove掉咕嚕跟字
							screen.remove(screen.getenemy(i));
							score += 100;
							Score.setText("Score:" + setscore(score));
						}

					}
					type.setText("");// 將textfield清空
				}

			});

			type.requestFocusInWindow();// 讓光標停在打字區

		} else if (e.getSource() == finalgrade.getout()) {
			System.exit(0);// 結束遊戲
		} else if (e.getSource() == character.gethunter()) {// 選取狩魔獵人
			bgm.play();
			gun.play();
			gun = new AudioPlayer();
			gun.loadAudio("audio/start1_22.wav", null);
			screen.timestart();
			remove(character);
			screen.setcharflag(1);
			setflag(1);// 設定角色
			this.setSize(1600, 1000);
			setLocation((scr_size.width - this.getWidth()) / 2,
					(scr_size.height - this.getHeight()) / 2);

			timer.start();
			Score.setFont(letter);
			Life.setFont(letter);
			statue.setLayout(new GridLayout(2, 1));
			statue.add(Score);
			statue.add(Life);
			this.setLayout(new BorderLayout());
			this.add(screen, BorderLayout.CENTER);
			down.setLayout(new BorderLayout());
			down.add(statue, BorderLayout.WEST);
			down.add(type, BorderLayout.CENTER);
			this.add(down, BorderLayout.SOUTH);

			type.addActionListener(new ActionListener() // 當enter時發生事件
			{
				public void actionPerformed(ActionEvent ev) {
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);

					for (int i = 0; i < 101; i++) {
						if (type.getText().equals(screen.getstring(i))) {
							zombie.play();
							zombie = new AudioPlayer();
							zombie.loadAudio("audio/zombie_018_16.wav", null);
							damage.play();
							damage = new AudioPlayer();
							damage.loadAudio("audio/damage2_22.wav", null);
							blood.play();
							blood = new AudioPlayer();
							blood.loadAudio("audio/blood01_16.wav", null);
							screen.setflag(i);
							screen.remove(screen.getletter(i));
							screen.remove(screen.getenemy(i));
							score += 100;
							Score.setText("Score:" + setscore(score));
						}

					}
					type.setText("");
				}

			});
			type.requestFocusInWindow();
		} else if (e.getSource() == character.getbar()) {// 選取野蠻人
			bgm.play();
			setflag(2);
			gun.play();
			gun = new AudioPlayer();
			gun.loadAudio("audio/start1_22.wav", null);
			screen.timestart();
			remove(character);
			screen.setcharflag(2);
			this.setSize(1600, 1000);
			setLocation((scr_size.width - this.getWidth()) / 2,
					(scr_size.height - this.getHeight()) / 2);
			timer.start();
			Score.setFont(letter);
			Life.setFont(letter);
			statue.setLayout(new GridLayout(2, 1));
			statue.add(Score);
			statue.add(Life);
			this.setLayout(new BorderLayout());
			this.add(screen, BorderLayout.CENTER);
			down.setLayout(new BorderLayout());
			down.add(statue, BorderLayout.WEST);
			down.add(type, BorderLayout.CENTER);
			this.add(down, BorderLayout.SOUTH);
			type.addActionListener(new ActionListener() // 當enter時發生事件
			{
				public void actionPerformed(ActionEvent ev) {
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);

					for (int i = 0; i < 101; i++) {
						if (type.getText().equals(screen.getstring(i))) {
							zombie.play();
							zombie = new AudioPlayer();
							zombie.loadAudio("audio/zombie_018_16.wav", null);
							damage.play();
							damage = new AudioPlayer();
							damage.loadAudio("audio/damage2_22.wav", null);
							blood.play();
							blood = new AudioPlayer();
							blood.loadAudio("audio/blood01_16.wav", null);
							screen.setflag(i);
							screen.remove(screen.getletter(i));
							screen.remove(screen.getenemy(i));
							score += 100;
							Score.setText("Score:" + setscore(score));
						}

					}
					type.setText("");
				}

			});
			type.requestFocusInWindow();
		} else if (e.getSource() == character.getmonk()) {// 選取武僧
			bgm.play();
			setflag(3);
			gun.play();
			gun = new AudioPlayer();
			gun.loadAudio("audio/start1_22.wav", null);
			remove(character);
			screen.timestart();
			screen.setcharflag(3);
			setLocation((scr_size.width - this.getWidth()) / 2,
					(scr_size.height - this.getHeight()) / 2);
			timer.start();
			Score.setFont(letter);
			Life.setFont(letter);
			statue.setLayout(new GridLayout(2, 1));
			statue.add(Score);
			statue.add(Life);
			this.setLayout(new BorderLayout());
			this.setSize(1600, 1000);
			this.add(screen, BorderLayout.CENTER);
			down.setLayout(new BorderLayout());
			down.add(statue, BorderLayout.WEST);
			down.add(type, BorderLayout.CENTER);
			this.add(down, BorderLayout.SOUTH);
			type.addActionListener(new ActionListener() // 當enter時發生事件
			{
				public void actionPerformed(ActionEvent ev) {
					p.play();
					p = new AudioPlayer();
					p.loadAudio("audio/gun5_22.wav", null);
					for (int i = 0; i < 101; i++) {
						if (type.getText().equals(screen.getstring(i))) {
							zombie.play();
							zombie = new AudioPlayer();
							zombie.loadAudio("audio/zombie_018_16.wav", null);
							damage.play();
							damage = new AudioPlayer();
							damage.loadAudio("audio/damage2_22.wav", null);
							blood.play();
							blood = new AudioPlayer();
							blood.loadAudio("audio/blood01_16.wav", null);
							screen.setflag(i);
							screen.remove(screen.getletter(i));
							screen.remove(screen.getenemy(i));
							score += 100;
							Score.setText("Score:" + setscore(score));
						}

					}
					type.setText("");

				}

			});
			type.requestFocusInWindow();

		}

	}

}
