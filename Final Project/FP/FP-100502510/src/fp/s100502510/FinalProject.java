package fp.s100502510;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class FinalProject {

	public static void main(String[] args) {
		FrameWork f = new FrameWork();
		f.setTitle("Final Project");
		f.setLocationRelativeTo(null);
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
