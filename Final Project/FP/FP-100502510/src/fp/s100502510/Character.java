package fp.s100502510;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Character extends JPanel {// 角色選取畫面

	ImageIcon demonhunter = new ImageIcon("image/Demon Hunter.png");
	ImageIcon barbarian = new ImageIcon("image/野蠻人.png");
	ImageIcon monk = new ImageIcon("image/monk.png");
	ImageIcon background = new ImageIcon("image/charbackground.jpg");
	ImageIcon choosehunter = new ImageIcon("image/choosehunter.png");
	ImageIcon choosebar = new ImageIcon("image/buttonicon2.png");
	ImageIcon choosemonk = new ImageIcon("image/choosemonk.png");
	JLabel background2 = new JLabel();
	JLabel demonhunter2 = new JLabel();// 角色所在的JLabel
	JLabel barbarian2 = new JLabel();
	JLabel monk2 = new JLabel();
	JLabel choose = new JLabel("Choose your Charactor");
	JButton demonhunter3 = new JButton("Demon Hunter");
	JButton barbarian3 = new JButton("Barbarian");
	JButton monk3 = new JButton("Monk");
	Font buttonfont = new Font("Serif", Font.BOLD, 25);
	Font choosefont = new Font("Serif", Font.BOLD, 100);

	public Character() {
		this.setLayout(null);// 漫長的排版
		choose.setForeground(Color.WHITE);
		choose.setFont(choosefont);
		demonhunter3.setFont(buttonfont);
		barbarian3.setFont(buttonfont);
		monk3.setFont(buttonfont);
		demonhunter2.setIcon(demonhunter);
		barbarian2.setIcon(barbarian);
		monk2.setIcon(monk);
		background2.setIcon(background);
		choose.setBounds(350, 50, 2000, 100);
		background2.setBounds(0, 0, background.getIconWidth(),
				background.getIconHeight());
		demonhunter2.setBounds(300, 300, demonhunter.getIconWidth(),
				demonhunter.getIconHeight());
		barbarian2.setBounds(750, 350, barbarian.getIconWidth(),
				barbarian.getIconHeight());
		monk2.setBounds(1100, 270, monk.getIconWidth(), monk.getIconHeight());
		demonhunter3.setBorder(null);
		demonhunter3.setIcon(choosehunter);
		demonhunter3.setBounds(350, 800, choosehunter.getIconWidth() - 30,
				choosehunter.getIconHeight());
		barbarian3.setBorder(null);
		barbarian3.setIcon(choosebar);
		barbarian3.setBounds(750, 800, choosebar.getIconWidth() - 30,
				choosebar.getIconHeight());
		monk3.setBorder(null);
		monk3.setIcon(choosemonk);
		monk3.setBounds(1150, 800, choosemonk.getIconWidth() - 30,
				choosemonk.getIconHeight());
		this.add(choose);
		this.add(monk3);
		this.add(demonhunter3);
		this.add(barbarian3);
		this.add(monk2);
		this.add(barbarian2);
		this.add(demonhunter2);
		this.add(background2);

	}

	public JButton gethunter() {
		return demonhunter3;
	}

	public JButton getbar() {
		return barbarian3;
	}

	public JButton getmonk() {
		return monk3;
	}

}
