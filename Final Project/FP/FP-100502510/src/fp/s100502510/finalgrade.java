package fp.s100502510;

import javax.swing.*;

public class finalgrade extends JPanel {// 遊戲結束畫面

	ImageIcon finalimage = new ImageIcon("image/finish.jpg");// 背景的圖
	ImageIcon tryicon = new ImageIcon("image/tryagain.png");// 重新開始按鈕的icon
	ImageIcon outicon = new ImageIcon("image/out.png");
	JLabel background = new JLabel();
	JButton tryagain = new JButton();
	JButton out = new JButton();

	public finalgrade() {
		this.setLayout(null);
		background.setIcon(finalimage);
		background.setBounds(0, 0, finalimage.getIconWidth(),
				finalimage.getIconHeight());
		tryagain.setBounds(250, 350, tryicon.getIconWidth() - 11,
				tryicon.getIconHeight());
		out.setBounds(450, 350, outicon.getIconWidth() - 11,
				outicon.getIconHeight());
		tryagain.setBorder(null);// 讓按鈕不要出現邊界
		out.setBorder(null);
		tryagain.setIcon(tryicon);
		out.setIcon(outicon);
		this.add(out);
		this.add(tryagain);
		this.add(background);
	}

	public JButton gettry() {
		return tryagain;
	}

	public JButton getout() {
		return out;
	}
}
