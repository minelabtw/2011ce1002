package project.s100502007;

import javax.swing.JFrame;

public class FinalProject{
	public static void main(String[] args){
		//make a window
		FrameWork frame = new FrameWork();
		
		frame.setTitle("project");
	    frame.setLocationRelativeTo(null); // Center the frame
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setSize(400, 300);
	    frame.setVisible(true);
	}
}