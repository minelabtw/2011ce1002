package project.s100502007;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FrameWork extends JFrame{
	private JTextField tf1 = new JTextField(1);//to input number
	private JTextField tf2 = new JTextField(1);
	private JTextField tf3 = new JTextField(1);
	private JTextField tf4 = new JTextField(1);
	
	public JLabel l1 = new JLabel("  1.",SwingConstants.LEFT);//show what you input and output answer
	public JLabel l2 = new JLabel("  2.",SwingConstants.LEFT);
	public JLabel l3 = new JLabel("  3.",SwingConstants.LEFT);
	public JLabel l4 = new JLabel("  4.",SwingConstants.LEFT);
	public JLabel l5 = new JLabel("  5.",SwingConstants.LEFT);
	public JLabel l6 = new JLabel("  6.",SwingConstants.LEFT);
	public JLabel l7 = new JLabel("  7.",SwingConstants.LEFT);
	public JLabel l8 = new JLabel("  8.",SwingConstants.LEFT);
	public JLabel l9 = new JLabel("  9.",SwingConstants.LEFT);
	public JLabel l10 = new JLabel("  10.",SwingConstants.LEFT);
	
	private JPanel labelPanel = new JPanel();//put label at here
	private JPanel textPanel = new JPanel();//put text at here
	private JPanel buttonPanel = new JPanel();//put button at here
	private JButton enter = new JButton("Enter");
	private random  rand = new random();
	private operation operation = new operation();
	
	public FrameWork(){
		setLayout( new BorderLayout());
		labelPanel.setLayout(new GridLayout(5,2,1,1));//compose label
		labelPanel.add(l1);
		labelPanel.add(l2);
		labelPanel.add(l3);
		labelPanel.add(l4);
		labelPanel.add(l5);
		labelPanel.add(l6);
		labelPanel.add(l7);
		labelPanel.add(l8);
		labelPanel.add(l9);
		labelPanel.add(l10);
		
		setLayout(new BorderLayout());
		textPanel.setLayout(new FlowLayout());//compose text
		textPanel.add(tf1);
		textPanel.add(tf2);
		textPanel.add(tf3);
		textPanel.add(tf4);
		
		setLayout(new BorderLayout());
		buttonPanel.setLayout(new FlowLayout());
		buttonPanel.add(enter);
		
		add(labelPanel, BorderLayout.NORTH);//compose all
		add(textPanel, BorderLayout.CENTER);
		add(buttonPanel,BorderLayout.SOUTH);
		
		//read number that input text
		tf1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				operation.getnumber1(Integer.parseInt(tf1.getText()));
				operation.setnumber1();
			}
		});
		tf2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				operation.getnumber2(Integer.parseInt(tf2.getText()));
				operation.setnumber2();
			}
		});
		tf3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				operation.getnumber3(Integer.parseInt(tf3.getText()));
				operation.setnumber3();
			}
		});
		tf4.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				operation.getnumber4(Integer.parseInt(tf4.getText()));
				operation.setnumber4();
			}
		});
		
		//when press enter
		enter.addActionListener(new ActionListener(){
			int count=0;
			public void actionPerformed(ActionEvent e){
				count++;
				//count = what,what = label what
				if(count==1){
				operation.Acount=0;
				operation.Bcount=0;
				operation.operation1();
				operation.operation2();
				operation.operation3();
				operation.operation4();
				operation.total();
				l1.setText("  1."+operation.totals+operation.Acount+"A"+operation.Bcount+"B");
				if(operation.Acount==4){
					buttonPanel.add(new Label ("你答對了"));
				}
				repaint();
				}
				else if(count==2){
					operation.Acount=0;
					operation.Bcount=0;
					operation.operation1();
					operation.operation2();
					operation.operation3();
					operation.operation4();
					operation.total();
					l2.setText("  2."+operation.totals+operation.Acount+"A"+operation.Bcount+"B");
					if(operation.Acount==4){
						buttonPanel.add(new Label ("你答對了"));
					}
					repaint();
				}
				else if(count==3){
					operation.Acount=0;
					operation.Bcount=0;
					operation.operation1();
					operation.operation2();
					operation.operation3();
					operation.operation4();
					operation.total();
					l3.setText("  3."+operation.totals+operation.Acount+"A"+operation.Bcount+"B");
					if(operation.Acount==4){
						buttonPanel.add(new Label ("你答對了"));
					}
					repaint();
				}
				else if(count==4){
					operation.Acount=0;
					operation.Bcount=0;
					operation.operation1();
					operation.operation2();
					operation.operation3();
					operation.operation4();
					operation.total();
					l4.setText("  4."+operation.totals+operation.Acount+"A"+operation.Bcount+"B");
					if(operation.Acount==4){
						buttonPanel.add(new Label ("你答對了"));
					}
					repaint();
				}
				else if(count==5){
					operation.Acount=0;
					operation.Bcount=0;
					operation.operation1();
					operation.operation2();
					operation.operation3();
					operation.operation4();
					operation.total();
					l5.setText("  5."+operation.totals+operation.Acount+"A"+operation.Bcount+"B");
					if(operation.Acount==4){
						buttonPanel.add(new Label ("你答對了"));
					}
					repaint();
				}
				else if(count==6){
					operation.Acount=0;
					operation.Bcount=0;
					operation.operation1();
					operation.operation2();
					operation.operation3();
					operation.operation4();
					operation.total();
					l6.setText("  6."+operation.totals+operation.Acount+"A"+operation.Bcount+"B");
					if(operation.Acount==4){
						buttonPanel.add(new Label ("你答對了"));
					}
					repaint();
				}
				else if(count==7){
					operation.Acount=0;
					operation.Bcount=0;
					operation.operation1();
					operation.operation2();
					operation.operation3();
					operation.operation4();
					operation.total();
					l7.setText("  7."+operation.totals+operation.Acount+"A"+operation.Bcount+"B");
					if(operation.Acount==4){
						buttonPanel.add(new Label ("你答對了"));
					}
					repaint();
				}
				else if(count==8){
					operation.Acount=0;
					operation.Bcount=0;
					operation.operation1();
					operation.operation2();
					operation.operation3();
					operation.operation4();
					operation.total();
					l8.setText("  8."+operation.totals+operation.Acount+"A"+operation.Bcount+"B");
					if(operation.Acount==4){
						buttonPanel.add(new Label ("你答對了"));
					}
					repaint();
				}
				else if(count==9){
					operation.Acount=0;
					operation.Bcount=0;
					operation.operation1();
					operation.operation2();
					operation.operation3();
					operation.operation4();
					operation.total();
					l9.setText("  9."+operation.totals+operation.Acount+"A"+operation.Bcount+"B");
					if(operation.Acount==4){
						buttonPanel.add(new Label ("你答對了"));
					}
					repaint();
				}
				else if(count==10){
					operation.Acount=0;
					operation.Bcount=0;
					operation.operation1();
					operation.operation2();
					operation.operation3();
					operation.operation4();
					operation.total();
					l10.setText("  10."+operation.totals+operation.Acount+"A"+operation.Bcount+"B");
					if(operation.Acount==4){
						buttonPanel.add(new Label ("你答對了"));
					}
					else if(operation.Acount!=4){
						buttonPanel.add(new Label ("Answer:"+rand.a+rand.b+rand.c+rand.d));
					}
					repaint();
				}
			}
		});
	}
}
