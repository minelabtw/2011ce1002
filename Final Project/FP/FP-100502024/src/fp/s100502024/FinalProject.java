package fp.s100502024;
import javax.swing.JFrame;

public class FinalProject 
{
	public static void main(String[] args) throws Exception 
	{
		FrameWork f = new FrameWork();
		f.setTitle("Final");
		f.setSize(800,600);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setLocationRelativeTo(null);
		f.setVisible(true);
	}
}
