package fp.s100502024;
import java.util.Scanner;
import javax.swing.JOptionPane;
import java.io.*;

public class Rank 
{
	protected int TotalSecond;
	
	protected int[] Hour_go = new int[5];
	protected int[] Minute_go = new int[5];
	protected int[] Second_go = new int[5];
	protected String[] name_go = new String[5];
	protected int[] F_Second_go = new int[5];
	// 讀score_go.txt檔裡的名字,時,分,秒並存入array
	
	protected int[] Hour_back = new int[5];
	protected int[] Minute_back = new int[5];
	protected int[] Second_back = new int[5];
	protected String[] name_back = new String[5];
	protected int[] F_Second_back = new int[5];
	// 讀score_back.txt檔裡的名字,時,分,秒並存入array
	
	protected int Hour_In,Minute_In,Second_In;
	protected int[] number = new int[5];
	
	protected boolean CountStyle = true; // 計時型態
	
	File filein_go = new File("score_go.txt");
	File filein_back = new File("score_back.txt");
	Scanner input_go = new Scanner(filein_go);
	Scanner input_back = new Scanner(filein_back);
	// I/O
	
	public Rank() throws Exception
	{
		readfile_go();
		readfile_back();
	}
	public void setTime(int H,int M,int S)
	{
		TotalSecond = H*3600 + M*60 + S;
		Hour_In = H;
		Minute_In = M;
		Second_In = S;
	}
	public void readfile_go() // 讀score_go.txt檔
	{
		CountStyle = true;
		for(int i=0;i<5;i++)
		{
			number[i] = input_go.nextInt();
			this.name_go[i] = input_go.next();
			this.Hour_go[i] = input_go.nextInt();
			this.Minute_go[i] = input_go.nextInt();
			this.Second_go[i]= input_go.nextInt();
			F_Second_go[i] = Hour_go[i]*3600 + Minute_go[i]*60 + Second_go[i];
		}
		sort_go(); // 排序(時間由少 → 多)
	}
	public void readfile_back() // 讀score_back.txt檔
	{
		CountStyle = false;
		for(int i=0;i<5;i++)
		{
			number[i] = input_back.nextInt();
			this.name_back[i] = input_back.next();
			this.Hour_back[i] = input_back.nextInt();
			this.Minute_back[i] = input_back.nextInt();
			this.Second_back[i]= input_back.nextInt();
			F_Second_back[i] = Hour_back[i]*3600 + Minute_back[i]*60 + Second_back[i];
		}
		sort_back(); // 排序(時間由多 → 少)
	}
	public void writefile_go() throws Exception // 寫資料到score_go.txt檔裡
	{
		int h,m,s;
		File fileout_go = new File("score_go.txt");
		PrintWriter output_go = new PrintWriter(fileout_go);
		for(int n=0;n<5;n++)
		{
			h = (int)(F_Second_go[n]/3600);
			m = (int)(F_Second_go[n]%3600)/60;
			s = (int)(F_Second_go[n]%3600)%60;
			output_go.println(number[n]+" "+name_go[n]+" "+h+" "+m+" "+s);
		}
		output_go.close();
	}
	public void writefile_back() throws Exception // 寫資料到score_back.txt檔裡
	{
		int h,m,s;
		File fileout_back = new File("score_back.txt");
		PrintWriter output_back = new PrintWriter(fileout_back);
		for(int n=0;n<5;n++)
		{
			h = (int)(F_Second_back[n]/3600);
			m = (int)(F_Second_back[n]%3600)/60;
			s = (int)(F_Second_back[n]%3600)%60;
			output_back.println(number[n]+" "+name_back[n]+" "+h+" "+m+" "+s);
		}
		output_back.close();
	}
	public void sort_go() // 排序score_go.txt檔裡的資料
	{
		int temp_second;
		int temp_Hour;
		int temp_Minute;
		int temp_Second;
		String temp_name;
		for(int m=0;m<5;m++) // Bubble Sort
		{
			for(int j=0;j<4;j++)
			{
				if(F_Second_go[j] > F_Second_go[j+1])
				{
					temp_second = F_Second_go[j+1];
					temp_name = name_go[j+1]; 
					temp_Hour = Hour_go[j+1];
					temp_Minute = Minute_go[j+1];
					temp_Second = Second_go[j+1];
						
					F_Second_go[j+1] = F_Second_go[j];
					name_go[j+1] =name_go[j];
					Hour_go[j+1] = Hour_go[j];
					Minute_go[j+1] = Minute_go[j];
					Second_go[j+1] = Second_go[j];
						
					F_Second_go[j] = temp_second;
					name_go[j] = temp_name;
					Hour_go[j] = temp_Hour;
					Minute_go[j] = temp_Minute;
					Second_go[j] = temp_Second;
				}
			}
		}
	}
	public void sort_back() // 排序score_back.txt檔裡的資料
	{
		int temp_second;
		int temp_Hour;
		int temp_Minute;
		int temp_Second;
		String temp_name;
		for(int m=0;m<5;m++) // Bubble Sort
		{
			for(int j=0;j<4;j++)
			{
				if(F_Second_back[j] < F_Second_back[j+1])
				{
					temp_second = F_Second_back[j+1];
					temp_name = name_back[j+1]; 
					temp_Hour = Hour_back[j+1];
					temp_Minute = Minute_back[j+1];
					temp_Second = Second_back[j+1];
					
					F_Second_back[j+1] = F_Second_back[j];
					name_back[j+1] =name_back[j];
					Hour_back[j+1] = Hour_back[j];
					Minute_back[j+1] = Minute_back[j];
					Second_back[j+1] = Second_back[j];
					
					F_Second_back[j] = temp_second;
					name_back[j] = temp_name;
					Hour_back[j] = temp_Hour;
					Minute_back[j] = temp_Minute;
					Second_back[j] = temp_Second;
				}
			}
		}
	}
	public void compare_go() throws Exception // 比較玩家的秒數是否比排行榜的好(決定是否寫入到紀錄檔) (計時模式秒數)
	{
		if(TotalSecond < F_Second_go[4]) 
		{
			F_Second_go[4] = TotalSecond;
			String player_name = JOptionPane.showInputDialog("Please Enter Your Name:");
			name_go[4] = player_name;
			Hour_go[4] = Hour_In;
			Minute_go[4] = Minute_In;
			Second_go[4] = Second_In;
			sort_go();
			writefile_go();
		}
	}
	public void compare_back() throws Exception // 比較玩家的秒數是否比排行榜的好(決定是否寫入到紀錄檔) (倒數模式秒數)
	{
		if(TotalSecond > F_Second_back[4])
		{
			F_Second_back[4] = TotalSecond;
			String player_name = JOptionPane.showInputDialog("Please Enter Your Name:");
			name_back[4] = player_name;
			Hour_back[4] = Hour_In;
			Minute_back[4] = Minute_In;
			Second_back[4] = Second_In;
			sort_back();
			writefile_back();
		}
	}
}
