package fp.s100502024;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener 
{
	private JButton[] cards = new JButton[16];
	private JButton start = new JButton("Start");
	private JButton rank_go = new JButton("Rank(Go)");
	private JButton rank_back = new JButton("Rank(Back)");
	private JButton pause = new JButton("     Pause     ");
	private JRadioButton counter1 = new JRadioButton("    計時");
	private JRadioButton counter2 = new JRadioButton("    倒數");
	ButtonGroup group = new ButtonGroup();
	//  Button //
	
	private JLabel description = new JLabel("         計時方式");
	private JLabel counttime = new JLabel();
	private JLabel startframe = new JLabel();
	private JPanel CardPanel = new JPanel();
	private JPanel RightPanel = new JPanel();
	private JPanel DownPanel = new JPanel();
	private JPanel Total = new JPanel();
	
	private String[] LevelName = {"Level1","Level2","Level3"};
	private JComboBox Level = new JComboBox(LevelName);
	
	CountTime clock = new CountTime();
	RankPanel R_Panel = new RankPanel();
	Rank r = new Rank();
	// class object
	
	private ImageIcon[] startIcon = new ImageIcon[5]; // 初始畫面(倒數畫面)
	private boolean store = true;
	private boolean CountStyle = true; // 計數型態
	
	private int k_1 = 0;
	private int k_2 = 0;
	private int[] random1 = new int[8];
	private int[] random2 = new int[8];
	private int[] number = new int[16];
	private int[] count = new int[8]; 
	private int[] clicktime = new int[16];
	private ImageIcon[] cardfront = new ImageIcon[16];
	private ImageIcon cardback = new ImageIcon("image/9.png"); // 牌的背面
	
	Timer starttimer = new Timer(1000,new changestart()); // 倒數 Timer
	Timer temp = new Timer(200,new tempopen());
	
	private int miss = 0;
	private int second = 4;
	private int cover_second; // 設定在遊戲前給玩家看牌的秒數
	
	public FrameWork() throws Exception
	{
		clock.cur_time.start();
		counttime.setLayout(new GridLayout(3,1)); // 計時方式panel
		counttime.add(description);
		counttime.add(counter1);
		counttime.add(counter2);
		group.add(counter1);
		group.add(counter2);
		CardPanel.setLayout(new GridLayout(4,4)); // 牌的panel
		for(int i=0;i<5;i++)
		{
			startIcon[i] = new ImageIcon("image/_"+i+".png"); // 讀初始的圖片
		}
		startframe.setIcon(startIcon[0]);
		RightPanel.setLayout(new GridLayout(5,1));
		RightPanel.add(clock);
		RightPanel.add(counttime);
		RightPanel.add(Level);
		RightPanel.add(rank_go);
		RightPanel.add(rank_back);
		RightPanel.add(pause);
		DownPanel.setLayout(new BorderLayout());
		DownPanel.add(start,BorderLayout.CENTER);
		DownPanel.add(pause,BorderLayout.EAST);
		Total.setLayout(new BorderLayout());
		Total.add(startframe,BorderLayout.CENTER);
		Total.add(RightPanel,BorderLayout.EAST);
		Total.add(DownPanel,BorderLayout.SOUTH);
		add(Total);
		creatrandom(); // 產生亂數
		for(int i=0;i<16;i++)
		{
			cardfront[i] = new ImageIcon("image/"+number[i]+".png");  // 讀牌的圖片
		}
		for(int i=0;i<16;i++)
		{
			cards[i] = new JButton(cardfront[i]); // 設定Button的圖片
		}
		
		start.addActionListener(this);
		rank_go.addActionListener(this);
		rank_back.addActionListener(this);
		pause.addActionListener(this);
		
		for(int i=0;i<16;i++)
		{
			cards[i].addActionListener(this);
		}
		counter1.addActionListener(this);
		counter2.addActionListener(this);
		Level.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent i)
			{
				if(Level.getSelectedIndex() == 0)
				{
					if(CountStyle == false)
					{
						clock.minute = 4;
						cover_second = -7;
						clock.setText("    "+Integer.toString(clock.hour)+":"+Integer.toString(clock.minute)+":"+Integer.toString(clock.second));
					}
					else
					{
						cover_second = -4;
					}
				}
				if(Level.getSelectedIndex() == 1)
				{
					if(CountStyle == false)
					{
						clock.minute = 3;
						cover_second = -5;
						clock.setText("    "+Integer.toString(clock.hour)+":"+Integer.toString(clock.minute)+":"+Integer.toString(clock.second));
					}
					else
					{
						cover_second = -4;
					}
				}
				if(Level.getSelectedIndex() == 2)
				{
					if(CountStyle == false)
					{
						clock.minute = 2;
						cover_second = -4;
						clock.setText("    "+Integer.toString(clock.hour)+":"+Integer.toString(clock.minute)+":"+Integer.toString(clock.second));
					}
					else
					{
						cover_second = -4;
					}
				}
			}
		});
		// ActionListener	
	}
	public void random_1()
	{
		while(k_1!=8)
		{
			int rand = (int)(Math.random()*66)%8+1;
			if(k_1 == 0)
			{
				random1[k_1] = rand;
				k_1++;
			}
			else
			{
				for(int index=0;index<k_1;index++)
				{
					if(random1[index] == rand)
					{
						store = false;
						break;
					}
					else
					{
						store = true;
					}
				}
				if(store == true)
				{
					random1[k_1] = rand;
					k_1++;
				}
			}
		}
	}
	public void random_2()
	{
		while(k_2!=8)
		{
			int rand = (int)(Math.random()*99)%8+1;
			if(k_2 == 0)
			{
				random2[k_2] = rand;
				k_2++;
			}
			else
			{
				for(int index=0;index<k_2;index++)
				{
					if(random2[index] == rand)
					{
						store = false;
						break;
					}
					else
					{
						store = true;
					}
				}
				if(store == true)
				{
					random2[k_2] = rand;
					k_2++;
				}
			}
		}
	}
	public void actionPerformed(ActionEvent e) // 觸發事件
	{
		if(e.getSource() == start)
		{
			if(clock.countgo.isRunning() == false || clock.countback.isRunning() == false)
			{
				second = 4;
				R_Panel.setVisible(false);
				startframe.setVisible(true);
				starttimer.start();
				CardPanel.setVisible(true);
				Total.add(CardPanel,BorderLayout.CENTER);
				add(Total);
			}
		}
		for(int i=0;i<16;i++)
		{
			if(e.getSource() == cards[i])
			{
				cards[i].setIcon(cardfront[i]);
				count[number[i]-1]++;
				clicktime[i]++;
				check();
			}
		}
		if(e.getSource() == counter1)
		{
			CountStyle = true;
			clock.cur_time.stop();
			clock.set_ini();
			clock.setText("    "+Integer.toString(clock.hour)+":"+Integer.toString(clock.minute)+":"+Integer.toString(clock.second)+"  ");
		}
		if(e.getSource() == counter2)
		{
			CountStyle = false;
			clock.cur_time.stop();
			clock.set_ini();
			clock.setText("    "+Integer.toString(clock.hour)+":"+Integer.toString(clock.minute)+":"+Integer.toString(clock.second)+"  ");
		}
		if(e.getSource() == rank_go)
		{
			clock.countgo.stop();
			clock.countback.stop();
			R_Panel.CountStyle = true;
			R_Panel.setVisible(true);
			CardPanel.setVisible(false);
			startframe.setVisible(false);
			pass_score_go();
			R_Panel.repaint();
			Total.add(R_Panel,BorderLayout.CENTER);
			add(Total);
		}
		if(e.getSource() == rank_back)
		{
			clock.countgo.stop();
			clock.countback.stop();
			R_Panel.CountStyle = false;
			R_Panel.setVisible(true);
			CardPanel.setVisible(false);
			startframe.setVisible(false);
			pass_score_back();
			R_Panel.repaint();
			Total.add(R_Panel,BorderLayout.CENTER);
			add(Total);
		}
		if(e.getSource() == pause)
		{
			if(clock.countgo.isRunning() == true)
			{
				pause.setText("   continue   ");
				clock.countgo.stop();
			}
			else if(clock.countback.isRunning() == true)
			{
				pause.setText("   continue   ");
				clock.countback.stop();
			}
			else if(clock.countgo.isRunning() == false)
			{
				pause.setText("     Pause     ");
				clock.countgo.start();
			}
			else if(clock.countback.isRunning() == false)
			{
				pause.setText("     Pause     ");
				clock.countback.start();/////
			}
		}
	}
	public void pass_score_go() // 傳排行榜中的資料到 RankPanel顯示(計時模式)
	{
		for(int index=0;index<5;index++)
		{
			R_Panel.Rank_Hour_go[index] = r.Hour_go[index];
			R_Panel.Rank_Minute_go[index] = r.Minute_go[index];
			R_Panel.Rank_Second_go[index] = r.Second_go[index];
			R_Panel.Rank_Name_go[index] = r.name_go[index];
		}
	}
	public void pass_score_back() // 傳排行榜中的資料到 RankPanel顯示(倒數模式)
	{
		for(int index=0;index<5;index++)
		{
			R_Panel.Rank_Hour_back[index] = r.Hour_back[index];
			R_Panel.Rank_Minute_back[index] = r.Minute_back[index];
			R_Panel.Rank_Second_back[index] = r.Second_back[index];
			R_Panel.Rank_Name_back[index] = r.name_back[index];
		}
	}
	public void check() // 確認是否點兩次
	{
		int sum = 0;
		for(int i=0;i<16;i++)
		{
			sum = sum + clicktime[i];
		}
		if(sum == 2)
		{
			temp.start();
		}
	}
	public void creatrandom()
	{
		random_1();
		for(int i=0;i<8;i++)
		{
			number[i] = random1[i];
		}
		random_2();
		for(int i=0;i<8;i++)
		{
			number[i+8] = random2[i];
		}
	}
	private class changestart implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			second--;
			if(second == 3)
			{
				startframe.setIcon(startIcon[1]);
			}
			if(second == 2)
			{
				startframe.setIcon(startIcon[2]);
			}
			if(second == 1)
			{
				startframe.setIcon(startIcon[3]);
			}
			if(second == 0)
			{
				startframe.setIcon(startIcon[4]);
			}
			if(second == -1)
			{
				startframe.setVisible(false);
				for(int i=0;i<16;i++)
				{
					CardPanel.add(cards[i]);
				}
				CardPanel.setVisible(true);
				Total.add(CardPanel,BorderLayout.CENTER);
				add(Total);
			}
			if(second == cover_second)
			{
				for(int k=0;k<16;k++)
				{
					cards[k].setIcon(cardback);
				}
				if(CountStyle == true)
				{
					clock.countgo.start();
				}
				else
				{
					clock.countback.start();
				}
			}
		}
	}
	private class tempopen implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			boolean clean = false;
			for(int i=0;i<16;i++)
			{
				if(count[number[i]-1] == 2 && clicktime[i] == 1)
				{
					cards[i].setVisible(false);
					clean = true;
					miss++;
				}
				if(miss == 16) // 若牌全部消失時
				{
					clock.countgo.stop();
					clock.countback.stop();
					r.setTime(clock.hour,clock.minute,clock.second);
					try 
					{
						if(CountStyle == true)
						{
							r.compare_go();
							R_Panel.CountStyle = true;
							pass_score_go();
							R_Panel.setVisible(true);
							R_Panel.repaint();
						}
						else
						{
							r.compare_back();
							R_Panel.CountStyle = false;
							pass_score_back();
							R_Panel.setVisible(true);
							R_Panel.repaint();
						}
						CardPanel.setVisible(false);
						startframe.setVisible(false);
						Total.add(R_Panel,BorderLayout.CENTER);
						add(Total);
						break;
					} 
					catch (Exception e1) 
					{
						
					}
				}
			}
			if(clean == false) // 若點到的兩張牌不是相同時,讓牌翻回
			{
				for(int i=0;i<16;i++)
				{
					if(clicktime[i]!=0)
					{
						cards[i].setIcon(cardback);
					}
				}
			}
			for(int i=0;i<8;i++) // 重設點擊次數為0
			{
				count[i] = 0;					
				clicktime[i] = 0;
				clicktime[i+8] = 0;
			}
			temp.stop();
		}
	}
}








