package fp.s100502024;
import javax.swing.*;
import java.awt.*;

public class RankPanel extends JPanel
{
	private ImageIcon background = new ImageIcon("image/Rank.png"); // 排行榜背景圖片
	private Image BackgroundImage = background.getImage();
	
	protected String[] Rank_Name_go = new String[5];
	protected int[] Rank_Hour_go = new int[5];
	protected int[] Rank_Minute_go = new int[5];
	protected int[] Rank_Second_go = new int[5];
	// 接收由Rank傳來的數據 (計時模式數據)
	
	protected String[] Rank_Name_back = new String[5];
	protected int[] Rank_Hour_back = new int[5];
	protected int[] Rank_Minute_back = new int[5];
	protected int[] Rank_Second_back = new int[5];
	// 接收由Rank傳來的數據 (倒數模式數據)
	
	protected boolean CountStyle = true; // 計時型態
	
	Font font_name = new Font("AR CENA",Font.HANGING_BASELINE,45);
	Font font_time = new Font("Serif",Font.BOLD,35);
	// 設定顯示在排行榜上字的字體
	
	Color color_name = new Color(140,255,26);
	Color color_time = new Color(255,128,255);
	// 設定顯示在排行榜上字的顏色
	
	public RankPanel()
	{
		
	}
	protected void paintComponent(Graphics g) // 寫資料在Panel上
	{
		super.paintComponent(g);
		g.drawImage(BackgroundImage,0,0,getWidth(),getHeight(),this);
		if(CountStyle == true) // 若計時型態為"計時",寫計時的紀錄檔在排行榜上
		{
			for(int i=0;i<5;i++)
			{
				g.setFont(font_name);
				g.setColor(color_name);
				g.drawString(Rank_Name_go[i],235,173+i*75);
				g.setFont(font_time);
				g.setColor(color_time);
				g.drawString(Rank_Hour_go[i]+":"+Rank_Minute_go[i]+":"+Rank_Second_go[i],490,173+i*75);
			}
		}
		else // 若計時型態為"倒數",寫倒數的紀錄檔在排行榜上
		{
			for(int i=0;i<5;i++)
			{
				g.setFont(font_name);
				g.setColor(color_name);
				g.drawString(Rank_Name_back[i],235,173+i*75);
				g.setFont(font_time);
				g.setColor(color_time);
				g.drawString(Rank_Hour_back[i]+":"+Rank_Minute_back[i]+":"+Rank_Second_back[i],490,173+i*75);
			}
		}
	}
}
