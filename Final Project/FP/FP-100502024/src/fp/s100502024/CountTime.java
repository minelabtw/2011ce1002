package fp.s100502024;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class CountTime extends JLabel 
{
	protected int hour,minute,second;
	protected Timer cur_time = new Timer(1000,new TimerListenerCurrentTime());
	protected Timer countgo = new Timer(1000,new TimerListenerGoTime());
	protected Timer countback = new Timer(1000,new TimerListenerBackTime());
	Font font = new Font("Serif",Font.BOLD,25);
	public CountTime()
	{
		
	}
	public void gotime() 
	{
		if(second < 59) // 璝计 < 59  玥尿糤计
		{
			second++;
		}
		else // ぃ琌玥计耴0,だ+1
		{
			second = 0; 
			minute++;
		}
		if(minute > 59)
		{
			minute = 0;
			hour++;
		}
	}
	public void backtime() // 计璸
	{
		if(second > 0)
		{
			second--;
		}
		else
		{
			minute--;
			second = 59;
		}
	}
	public void set_ini() // 砞﹚﹍て
	{
		hour = 0;
		minute = 0;
		second = 0;
		setFont(font);
	}
	public void setCurrentClock() // 眔╰参丁
	{
		Calendar calendar = new GregorianCalendar();
		this.hour = calendar.get(Calendar.HOUR_OF_DAY);
		this.minute = calendar.get(Calendar.MINUTE);
		this.second = calendar.get(Calendar.SECOND);
	}
	class TimerListenerGoTime implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			gotime();
			setFont(font);
			setText("  "+Integer.toString(hour)+":"+Integer.toString(minute)+":"+Integer.toString(second));
		}
	}
	class TimerListenerBackTime implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			backtime();
			setFont(font);
			setText("  "+Integer.toString(hour)+":"+Integer.toString(minute)+":"+Integer.toString(second));
		}
	}
	class TimerListenerCurrentTime implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			setCurrentClock();
			setFont(font);
			setText("  "+Integer.toString(hour)+":"+Integer.toString(minute)+":"+Integer.toString(second));
		}
	}
}
