package finalproject.s100502020;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Balloon extends JLabel{
	
	
	public ImageIcon icon;
	public int type = (int)(Math.random()*9)+1;//random type
	public int x= 0, y=0;
	
	public int speed = (int)(Math.random() *5 +5);//each balloon has different speed 
//	public int speed(int a)
//	{
//		return speed = (int)(Math.random() *a +a); 
//	}
	public Balloon()
	{		
		switch(type)//random type and call random balloon image
		{
		case 1:
			icon = new ImageIcon("balloon/brightblue_B.png");
			break;
		case 2:
			icon = new ImageIcon("balloon/blue_B.png");
			break;
		case 3:
			icon = new ImageIcon("balloon/black_B.png");
			break;
		case 4:
			icon = new ImageIcon("balloon/green_B.png");
			break;
		case 5:
			icon = new ImageIcon("balloon/yellow_B.png");
			break;
		case 6:
			icon = new ImageIcon("balloon/red_B.png");
			break;
		case 7:
			icon = new ImageIcon("balloon/skin_B.png");
			break;
		case 8:
			icon = new ImageIcon("balloon/pink_B.png");
			break;
		case 9:
			icon = new ImageIcon("balloon/orange_B.png");
			break;
		default:
			break;
			
		}
		x = (int)(Math.random()*540);//random x pos 
		y = 500;//balloon came out under the screen
		setBounds(0,0,600,500);//set size and pos
		setOpaque(false);//open the label transparency
		icon = new ImageIcon(icon.getImage().getScaledInstance(60, 80, Image.SCALE_SMOOTH));
		//set size
	}
	protected void paintComponent(Graphics g)//draw balloon
	{
		super.paintComponent(g);
		g.drawImage(icon.getImage(),x,y, null);
	}
	
}
