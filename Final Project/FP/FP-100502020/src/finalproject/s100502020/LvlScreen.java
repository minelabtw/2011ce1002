package finalproject.s100502020;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LvlScreen extends JPanel implements ActionListener{

	private JButton easy = new JButton("easy");
	private JButton normal = new JButton("normal");
	private JButton hard = new JButton("hard");
	private JButton back = new JButton();
	private JPanel E = new JPanel();
	private JPanel N = new JPanel();
	private JPanel H = new JPanel();
	private ImageIcon background = new ImageIcon("balloon/background2.jpg");
	private ImageIcon easypic = new ImageIcon();
	private ImageIcon normalpic = new ImageIcon();
	private ImageIcon hardpic = new ImageIcon();
	Font font = new Font("SansSerif",Font.BOLD,60);
	StartingScreen startscreen = new StartingScreen();
	
	
	public LvlScreen() 
	{
		background = new ImageIcon(background.getImage().getScaledInstance(600, 500, Image.SCALE_SMOOTH));
		easy.setOpaque(false);
		easy.setFont(font);
		normal.setFont(font);
		hard.setFont(font);
		this.setLayout(new GridLayout(3, 1));
		E.setLayout(null);
		easy.setBounds(190, 40, 200, 70);
		E.add(easy);
		N.setLayout(null);
		normal.setBounds(140, 40, 300, 70);
		N.add(normal);
		H.setLayout(null);
		hard.setBounds(190, 40, 200, 70);
		back.setBounds(470, 40, 70, 70);
		H.add(hard);
		H.add(back);
		E.setOpaque(false);
		N.setOpaque(false);
		H.setOpaque(false);
		this.add(E);
		this.add(N);
		this.add(H);
		back.addActionListener(this);
	}

	public JButton REeasy() 
	{
		return easy;
	}
	public JButton REnormal() 
	{
		return normal;
	}
	public JButton REhard() 
	{
		return hard;
	}
	public JButton REback()
	{
		return back;
	}
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.drawImage(background.getImage(),0,0,null);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == back)
		{
			removeAll();
			add(startscreen);
			startscreen.revalidate();
		}
	}
}
