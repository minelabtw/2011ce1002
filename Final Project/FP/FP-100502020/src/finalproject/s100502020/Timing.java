package finalproject.s100502020;
import javax.swing.*;
import java.awt.*;

public class Timing extends JLabel{
	
	private int minute;
	private int second;
	
	
	public Timing(){//initial timing
		minute = 1;
		second = 0;
	}
	
	public int getMinute()
	{
		return minute;
	}
	
	public int getSecond()
	{
		return second;
	}
	
	public void setCurrentClock()//call method and start counting
	{		
		second--;
		
		if(second == -1)
		{ 
			minute--;
			second = 59;		
		}

	}
}
