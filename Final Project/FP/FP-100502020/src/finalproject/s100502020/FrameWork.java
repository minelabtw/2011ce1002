package finalproject.s100502020;

import javax.swing.*;

import org.omg.CORBA.PUBLIC_MEMBER;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrameWork extends JFrame {
	StartingScreen startscreen = new StartingScreen();
	LvlScreen lvlscreen = new LvlScreen();
	TempScreen mainscreen = new TempScreen();
	private JPanel Continue = new JPanel();
	private JButton again = new JButton("Play Again");
	private JButton chooselvl = new JButton("Choose LVL");
	private JPanel gameover = new JPanel();
	private JLabel gameovertext = new JLabel();	
	private JLabel scoretext = new JLabel();
	private JPanel thanks = new JPanel();
	private JLabel thankstext = new JLabel("THX FOR PLAYING");
	Font font = new Font("SansSerif",Font.BOLD,50);
	Font fontt = new Font("SansSerif",Font.BOLD,20);
	
	
	public FrameWork()
	{
		thanks.setLayout(null);//the panel after press exit
		thankstext.setFont(font);
		thankstext.setBounds(80, 100, 500, 100);
		thanks.add(thankstext);
		
		
		gameover.setLayout(new BorderLayout());
		Continue.setLayout(new GridLayout(1,2,10,10));
		Continue.add(again);
		Continue.add(chooselvl);
		gameover.add(gameovertext, BorderLayout.CENTER);	
		gameover.add(Continue, BorderLayout.SOUTH);
		again.setFont(fontt);
		Continue.setFont(fontt);
		gameovertext.setFont(font);
		Continue.add(again);
		Continue.add(chooselvl);
		
		ActionListener actionListener=new actionlistener();		
		startscreen.REstart().addActionListener(actionListener);
		startscreen.REexit().addActionListener(actionListener);
		startscreen.REhelp().addActionListener(actionListener);
		lvlscreen.REeasy().addActionListener(actionListener);
		lvlscreen.REnormal().addActionListener(actionListener);
		lvlscreen.REhard().addActionListener(actionListener);
		again.addActionListener(actionListener);
		chooselvl.addActionListener(actionListener);
		add(startscreen);
		gameovertext.setText("SCORE: "+mainscreen.score);
	}
	
	

	private class actionlistener implements ActionListener
	{ // while the timer action every time
		public void actionPerformed(ActionEvent e)
		{
			if(e.getSource() == startscreen.REstart())
			{
				remove(startscreen);
				add(lvlscreen);
				lvlscreen.revalidate();
				
			}
			else if(e.getSource() == startscreen.REexit())
			{
				
				
				remove(startscreen);
				add(thanks);
				thanks.revalidate();
//				add(gameover);
//				gameover.revalidate();
				
			}
			else if(e.getSource() == startscreen.REhelp())
			{
				JOptionPane.showMessageDialog(null, "This is a game called 'THE BALLOOOOON', " + 
						"we\n control the game with mouse, using mouse to hit " +
						"the balloon\nand rank up ur point, but each balloon has " +
						"their own function,\n make sure that u hit the balloon that makes" +
						"u rank up!!\n         HAVE FUN!!");
			}
			else if(e.getSource() == lvlscreen.REeasy())
			{
				remove(lvlscreen);				
				add(mainscreen);
				mainscreen.revalidate();
				JOptionPane.showMessageDialog(null, "bright blue balloon + 10\n" +
						"blue balloon + 20\ngreen balloon + 30\nyellow balloon + 40\n" +
						"skin balloon + 50\norange + 60\norange balloon - 10\n" +
						"red balloon - 50\nblack balloon - 100\nGOGOGOGOGOGO!");
				
				
			}
			else if(e.getSource() == lvlscreen.REnormal())
			{
				remove(lvlscreen);
				add(mainscreen);
				mainscreen.revalidate();
				JOptionPane.showMessageDialog(null, "bright blue balloon + 10\n" +
						"blue balloon + 20\ngreen balloon + 30\nyellow balloon + 40\n" +
						"skin balloon + 50\norange + 60\norange balloon - 10\n" +
						"red balloon - 50\nblack balloon - 100\nGOGOGOGOGOGO!\nNOT easy now~");
			}
			else if(e.getSource() == lvlscreen.REhard())
			{
				remove(lvlscreen);
				add(mainscreen);
				mainscreen.revalidate();
				JOptionPane.showMessageDialog(null, "bright blue balloon + 10\n" +
						"blue balloon + 20\ngreen balloon + 30\nyellow balloon + 40\n" +
						"skin balloon + 50\norange + 60\norange balloon - 10\n" +
						"red balloon - 50\nblack balloon - 100\nGOGOGOGOGOGO!\nReal hard!!");
			}
			else if(e.getSource() == again)
			{
				remove(gameover);
				remove(Continue);
				add(mainscreen);
				mainscreen.revalidate();
			}
			else if(e.getSource() == chooselvl)
			{
				remove(gameover);
				remove(Continue);
				add(lvlscreen);
				lvlscreen.revalidate();
			}
			
		}
	}





	
	
	


	
	
	
	
}
