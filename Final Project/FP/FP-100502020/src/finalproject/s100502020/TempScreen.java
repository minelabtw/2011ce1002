package finalproject.s100502020;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class TempScreen extends JPanel {
	Timer timer = new Timer();
	JButton play = new JButton("Play");
	JLabel timescreen = new JLabel("1:00");
	JLabel scorescreen = new JLabel("score: 0");
	public int score = 0;
	Balloon[] balls = new Balloon[10];
	ImageIcon target;
	int x = 0, y = 0;
	private JLabel showscore = new JLabel();
	Font font = new Font("SansSerif",Font.BOLD,70);
	ImageIcon background = new ImageIcon("balloon/background1.jpg");
	public TempScreen() 
	{
		background = new ImageIcon(background.getImage().getScaledInstance(600, 500, Image.SCALE_SMOOTH));
		this.setSize(600, 500);
		this.setOpaque(false);
		this.setLayout(null);
		play.addMouseListener(new MouseAdapter() //play button mouselistener react
		{
			public void mouseReleased(MouseEvent e)//release mouse and let
			//play button disappear, mainscreen repaint and start the two timers,
			//including timer that control balloon and timer that control the time
			{
				remove(play);
				TempScreen.this.repaint();
				timer.schedule(counter, 0, 1000);
				timer.schedule(updater, 0, 50);
			}
		});
		play.setBounds(20, 20, 100, 20);
		timescreen.setBounds(20, 50, 100, 20);
		scorescreen.setBounds(20, 70, 100, 20);
		for (int i = 0; i < 10; i++) 
		{
			add(balls[i] = new Balloon());
		}

		target = new ImageIcon("balloon/target.png");
		target = new ImageIcon(target.getImage().getScaledInstance(50, 50,
				Image.SCALE_SMOOTH));

		addMouseListener(adapter);
		addMouseMotionListener(adapter);
		add(play);
		add(timescreen);
		add(scorescreen);
	}

	protected void paintComponent(Graphics g)//draw target image 
	{
		super.paintComponent(g);		
		g.drawImage(background.getImage(),0,0,null);//setbackground
		g.drawImage(target.getImage(), x - 25, y - 25, null);//side 50, mid 25
	}

	TimerTask updater = new TimerTask() 
	{//control the time of the changing
	//position of the balloon
		public void run() 
		{
			for (int i = 0; i < 10; i++) 
			{
				balls[i].y -= balls[i].speed;
				if (balls[i].y <= -100) 
				{
					remove(balls[i]);
					add(balls[i] = new Balloon());
				}
			}
			repaint();
		}
	};

	TimerTask counter = new TimerTask() 
	{
		Timing t = new Timing();

		public void run() 
		{
			t.setCurrentClock(); // set current time
			timescreen.setText(t.getMinute() + ":" + t.getSecond());//show current time
			if (t.getMinute() == 0 && t.getSecond() == 0) //if the time ends
			//stop the clicking react of the mouse, and stop all the countings
			{
				counter.cancel();
				updater.cancel();
				removeMouseListener(adapter);
				showscore.setText("SCORE:" + score);
				showscore.setFont(font);
				showscore.setBounds(100, 50, 550, 400);
				add(showscore);
				
				// gameover...
			}
		}
	};

	MouseAdapter adapter = new MouseAdapter() //including mouselistener
	//and mousemotionlistener that can use almost all ways of
	//using mouse 
	{
		public void mouseMoved(MouseEvent e) //when moving a mouse
		{
			x = e.getX();//set the target image on the mouse whenever
			// the mouse move 
			y = e.getY();//set the target image on the mouse whenever
			// the mouse move 
			repaint();//repaint target image whenever the mouse move
		}

		public void mousePressed(MouseEvent e) //when click the mouse..
		{
			for (int i = 0; i < 10; i++) // catch 
			{
				if (x >= balls[i].x && x <= balls[i].x + 60) 
				{
					if (y >= balls[i].y && y <= balls[i].y + 80) 
					{
						switch (balls[i].type) {//score ++
						case 1://bright blue balloon + 10
							score = score + 10;
							break;
						case 2://blue + 20
							score = score + 20;
							break;
						case 3://black - 100
							score = score - 100;
							break;
						case 4://green + 30
							score = score + 30;
							break;
						case 5://yellow + 40
							score = score + 40;
							break;
						case 6://red - 50
							score = score - 50;
							break;
						case 7://skin + 50
							score = score + 50;
							break;
						case 8://pink - 10
							score = score - 10;
							break;
						case 9:// orange + 60
							score = score + 60;
							break;
						default:
							score++;
							break;
						}
						scorescreen.setText("score: " + score);//show score 
						remove(balls[i]);//balloon explode
						add(balls[i] = new Balloon());//new balloon created 
						repaint();//draw
						break;
					}
				}
			}
		}
	};
}
