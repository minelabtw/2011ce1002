package finalproject.s100502020;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

public class StartingScreen extends JPanel{//starting panel
	
	private JPanel p1 = new JPanel(); 
	private JPanel p2 = new JPanel();
	//two panel one for name and one for buttons
	ImageIcon background = new ImageIcon("balloon/background1.jpg");//background image
	private JButton start = new JButton("START");
	private JButton exit = new JButton("EXIT");
	private JButton help = new JButton("HELP");
	private JButton back = new JButton();
	private JLabel name = new JLabel("");
	
	private ImageIcon startpic = new ImageIcon();
	private ImageIcon exitpic = new ImageIcon();	
	private ImageIcon namepic = new ImageIcon();
	
	Font font = new Font("SansSerif",Font.BOLD,20);
	
	public StartingScreen()
	{
		background = new ImageIcon(background.getImage().getScaledInstance(600, 500, Image.SCALE_SMOOTH));
		
		this.setLayout(new GridLayout(2,1,1,1));
		p1.setLayout(new BorderLayout());
		p2.setLayout(null);
		p1.add(name,BorderLayout.CENTER);
		start.setBounds(225, 10, 120, 50);//pos
		help.setBounds(225, 75, 120, 50);
		exit.setBounds(225, 140, 120, 50);
		p1.setOpaque(false);//transparency
		p2.setOpaque(false);
		p2.add(start);
		p2.add(help);
		p2.add(exit);
		
		add(p1);
		add(p2);
		
		
		
	}
	public JButton REstart()
	{
		return start;
	}
	public JButton REexit()
	{
		return exit;
	}
	public JButton REhelp()
	{
		return help;
	}
	public JButton REback()
	{
		return back;
	}
	protected void paintComponent(Graphics g)//draw background
	{
		super.paintComponent(g);
		g.drawImage(background.getImage(),0,0,null);
	}
	
	
	
}
