package fp.s100502503;

import javax.swing.ImageIcon;

public class RandomEasy 
{
	protected ImageIcon[] cardFront = new ImageIcon[16]; 
	protected static int[] randomNumberArray = new int[16];
	
	public RandomEasy()
	{		
		for(int i = 0; i < 16; i++)
		{
			randomNumberArray[i] = i;
		}
		cardFront[0] = new ImageIcon("pictures/00b.png");
		cardFront[1] = new ImageIcon("pictures/00b.png");
		cardFront[2] = new ImageIcon("pictures/01b.png");
		cardFront[3] = new ImageIcon("pictures/01b.png");
		cardFront[4] = new ImageIcon("pictures/02b.png");
		cardFront[5] = new ImageIcon("pictures/02b.png");
		cardFront[6] = new ImageIcon("pictures/03b.png");
		cardFront[7] = new ImageIcon("pictures/03b.png");
		cardFront[8] = new ImageIcon("pictures/04b.png");
		cardFront[9] = new ImageIcon("pictures/04b.png");
		cardFront[10] = new ImageIcon("pictures/05b.png");
		cardFront[11] = new ImageIcon("pictures/05b.png");
		cardFront[12] = new ImageIcon("pictures/06b.png");
		cardFront[13] = new ImageIcon("pictures/06b.png");
		cardFront[14] = new ImageIcon("pictures/07b.png");
		cardFront[15] = new ImageIcon("pictures/07b.png");
	
		randomNumber();
	}
	
	public void randomNumber()
	{
		for(int i = 0; i < 16; i++)
		{
			int j = (int)(Math.random() * 100) % 15;
			
			int k = randomNumberArray[i];
			randomNumberArray[i] = randomNumberArray[j];
			randomNumberArray[j] = k;
		}
	}
}
