package fp.s100502503;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class startPanel extends JPanel //implements ActionListener
{
	public startPanel()
	{
		this.setBackground(Color.BLACK);
	}
	public void paintComponent(Graphics g) 
	{
		  super.paintComponent(g);
		  ImageIcon img = new ImageIcon("pictures/start.png");
		  g.drawImage(img.getImage(), 0, 0, null);
	}
}
