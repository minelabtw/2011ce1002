package fp.s100502503;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.EtchedBorder;

public class GameStepNormal extends JPanel implements ActionListener
{
	private RandomNormal random = new RandomNormal();

	protected int scoreInt = 0;
	protected int stepInt = 50;
	
	protected boolean[] cardVisible = new boolean[36];
	
	protected ImageIcon[] cardFrontUser = new ImageIcon[36];
	protected JButton[] cardButton = new JButton[36];
	protected JButton renew = new JButton("Add New Cards");
	protected JLabel score = new JLabel("Score:" + scoreInt);
	protected JLabel step = new JLabel("Step:" + stepInt);
	protected JPanel p1 = new JPanel(new GridLayout(6, 6, 0, 0)); //save the cards
	protected JPanel p2 = new JPanel(new GridLayout(3, 1, 0, 0));
	protected ImageIcon cardBack = new ImageIcon("pictures/back.png");
	
	protected int flag = 0;//The flag to determined show the front of the card or the back of the card.
	protected int flagOfEnd = 0;
	protected int a = -1;
	protected int b = -1;
	protected int flagVisible = 0;
	
	private Color labelc = new Color(198, 228, 233); 
	private EtchedBorder labelb = new EtchedBorder(); 
	
	public GameStepNormal()
	{	
		
		for(int i = 0; i < 36; i++)
		{
			cardVisible[i] = true;
			cardFrontUser[i] = random.cardFrontN[random.randomNumberArrayN[i]];
			cardButton[i] = new JButton(cardBack);
			cardButton[i].setOpaque(false);
			cardButton[i].setContentAreaFilled(false);
			cardButton[i].setBorder(labelb);
			p1.add(cardButton[i]);
			cardButton[i].setSize(130, 130);
		}
	
		add(p1,BorderLayout.WEST);
		
		step.setFont(new Font("SERIF", Font.BOLD, 14));
		score.setFont(new Font("SERIF", Font.BOLD, 14));
		renew.setFont(new Font("SERIF", Font.BOLD, 14));
		step.setOpaque(true);
		score.setOpaque(true);
		step.setBackground(labelc);
		score.setBackground(labelc);
		step.setBorder(labelb);
		score.setBorder(labelb);
		
		p2.add(step);
		p2.add(score);
		p2.add(renew);
		
		add(p2, BorderLayout.EAST);
		
		for(int i = 0; i < 36; i++)
		{
			cardButton[i].addActionListener(this);
		}
		renew.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e)
	{		
		//check if the card that user turned is same or not, if same then destroy, if not, turn them back.
		for(int i = 0; i < 36; i++)
		{
			if(e.getSource() == cardButton[i])
			{
				//The flag to sure the card should be clicked only two terms
				flag++;
				if(flag == 1)
				{
					a = i;
					cardButton[a].setIcon(cardFrontUser[a]);
				}
				
				else if(flag == 2)
				{
					b = i;
					stepInt--;
					step.setText("Step:" + stepInt);
					
					cardButton[b].setIcon(cardFrontUser[b]);
					int card_numberA = random.randomNumberArrayN[a];
					int card_numberB = random.randomNumberArrayN[b];
					if((card_numberA%2 == 0 && card_numberB == card_numberA+1) || (card_numberB%2 == 0 && card_numberA == card_numberB+1))
					{
						JOptionPane.showMessageDialog(null, "YA");
						cardVisible[a] = false;
						cardVisible[b] = false;
						
						cardButton[a].setVisible(cardVisible[a]);
						cardButton[b].setVisible(cardVisible[b]);
						scoreInt += 10;
						score.setText("Score:" + scoreInt);
					}
						
					else
					{
						JOptionPane.showMessageDialog(null, "WRONG");
						cardButton[a].setIcon(cardBack);
						cardButton[b].setIcon(cardBack);
				}
					flag = 0;      
				}
			}
		}
		if(e.getSource() == renew)
		{
			for(int i = 0; i < 36; i++)
			{
				cardButton[i].setVisible(true);
				cardButton[i].setIcon(cardBack);
				for(int x = 0; x < 36; x++)
				{
					random.randomNumberArrayN[x] = x;
				}
				random = new RandomNormal();
			}
			for(int i = 0; i< 36; i++)
			{
				cardFrontUser[i] = random.cardFrontN[random.randomNumberArrayN[i]];
			}
		}
		if(stepInt == 0)
		{
			int ans = JOptionPane.showOptionDialog(null, "Do you want to try again?", "Game over", 
					JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
					null, new Object[]{"Yes", "No"}, "Yes");
			if (ans == 0);
			{
				random = new RandomNormal();
				scoreInt = 0;
				stepInt = 20;

				score.setText("Score:" + scoreInt);
				step.setText("Step:" + stepInt);

				flag = 0;
				flagOfEnd = 0;
				a = -1;
				b = -1;

				for (int i = 0; i < 16; i++) 
				{
					cardButton[i].setVisible(true);
				}

				for (int i = 0; i < 16; i++) 
				{
					cardFrontUser[i] = random.cardFrontN[random.randomNumberArrayN[i]];
				}

				for (int i = 0; i < 16; i++) 
				{
					cardButton[i].setIcon(cardBack);
				}
			}	 
		
			if(ans == 1)
			{	
				System.exit(1);	
			}
		}
	}
	public void paintComponent(Graphics g) 
	{
		  super.paintComponent(g);
		  ImageIcon img = new ImageIcon("pictures/game.png");
		  g.drawImage(img.getImage(), 0, 0, null);
	}
}

