package fp.s100502503;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.EtchedBorder;

public class GameTime extends JPanel implements ActionListener
{
	protected RandomEasy randomTE = new RandomEasy();
	protected FrameWork ff;

	protected int scoreIntTE = 0;
	protected int timeInt = 60;

	protected ImageIcon[] cardFrontUserTE = new ImageIcon[16];
	
	protected JButton[] cardButtonTE = new JButton[16];
	private JButton renewTE = new JButton("Add New Card");
		
	protected JLabel scoreTE = new JLabel("Score:" + scoreIntTE);
	protected JLabel time = new JLabel("Time:" + timeInt);

	protected JPanel p1TE = new JPanel(new GridLayout(4, 4, 0, 0)); // save the cards
	protected JPanel p2TE = new JPanel(new GridLayout(3, 1, 0, 0));
	protected ImageIcon cardBackTE = new ImageIcon("pictures/backb.png");
	protected Timer timer = new Timer(1000, new TimerListener());

	protected int flagTE = 0;
	protected int flagOfEndTE = 0;
	protected int aTE = -1;
	protected int bTE = -1;
	
	private Color labelc = new Color(198, 228, 233); 
	private EtchedBorder labelb = new EtchedBorder(); 

	protected int[] clickNumberTE = new int[16];

	public GameTime() 
	{
		// The flag to determined show the front of the card or the back of the
		// card.

		for (int i = 0; i < 16; i++)
		{
			clickNumberTE[i] = 0;
		}
		for (int i = 0; i < 16; i++) 
		{
			cardFrontUserTE[i] = randomTE.cardFront[randomTE.randomNumberArray[i]];
		}
		for (int i = 0; i < 16; i++) 
		{
			cardButtonTE[i] = new JButton(cardBackTE);
			cardButtonTE[i].setOpaque(false);
			cardButtonTE[i].setContentAreaFilled(false);
			cardButtonTE[i].setBorder(labelb);
			p1TE.add(cardButtonTE[i]);
			cardButtonTE[i].setSize(130, 130);
		}

		add(p1TE, BorderLayout.WEST);

		time.setFont(new Font("SERIF", Font.BOLD, 14));
		scoreTE.setFont(new Font("SERIF", Font.BOLD, 14));
		renewTE.setFont(new Font("SERIF", Font.BOLD, 14));
		time.setOpaque(true);
		scoreTE.setOpaque(true);
		time.setBackground(labelc);
		scoreTE.setBackground(labelc);
		time.setBorder(labelb);
		scoreTE.setBorder(labelb);
		
		p2TE.add(time);
		p2TE.add(scoreTE);
		p2TE.add(renewTE);	
		p2TE.add(renewTE, BorderLayout.NORTH);


		add(p2TE, BorderLayout.EAST);

		for (int i = 0; i < 16; i++) 
		{
			cardButtonTE[i].addActionListener(this);
		}
	
	}

	public void actionPerformed(ActionEvent e)
	{
		timer.start();

		// check if the card that user turned is same or not, if same then
		// destroy, if not, turn them back.
		for (int i = 0; i < 16; i++) 
		{
			if (e.getSource() == cardButtonTE[i]) 
			{
				// The
				flagTE++;
				if (flagTE == 1) 
				{
					aTE = i;
					cardButtonTE[aTE].setIcon(cardFrontUserTE[aTE]);
				}

				else if (flagTE == 2) 
				{
					bTE = i;

					cardButtonTE[bTE].setIcon(cardFrontUserTE[bTE]);

					int card_numberA = randomTE.randomNumberArray[aTE];
					int card_numberB = randomTE.randomNumberArray[bTE];

					if ((card_numberA % 2 == 0 && card_numberB == card_numberA + 1)
							|| (card_numberB % 2 == 0 && card_numberA == card_numberB + 1)) 
					{
						JOptionPane.showMessageDialog(null, "YA");
						cardButtonTE[aTE].setVisible(false);
						cardButtonTE[bTE].setVisible(false);
						scoreIntTE += 10;
						scoreTE.setText("Score:" + scoreIntTE);
					}

					else 
					{	
						JOptionPane.showMessageDialog(null, "WRONG");
						cardButtonTE[aTE].setIcon(cardBackTE);
						cardButtonTE[bTE].setIcon(cardBackTE);
					}
					flagTE = 0;
				}
			}
		}
	}

	// To control the seconds
	private class TimerListener implements ActionListener 
	{
		public void actionPerformed(ActionEvent e) 
		{
			timeInt--;
			time.setText("Time:" + timeInt);

			if (e.getSource() == renewTE) 
			{
				for (int i = 0; i < 16; i++) 
				{
					cardButtonTE[i].setVisible(true);
					cardButtonTE[i].setIcon(cardBackTE);
					
					for (int x = 0; x < 16; x++)
					{
						randomTE.randomNumberArray[x] = x;
					}
					randomTE = new RandomEasy();
				}
				for (int i = 0; i < 16; i++) 
				{
					cardFrontUserTE[i] = randomTE.cardFront[randomTE.randomNumberArray[i]];
				}
			}
			
			if (timeInt == 0) 
			{
				timer.stop();
				JOptionPane.showMessageDialog(null, "GameOver");

						
				
				int ans = JOptionPane.showOptionDialog(null, "Do you want to try again?", "Game over", 
														JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
														null, new Object[]{"Yes", "No"}, "Yes");
				if (ans == 0);
				{
					randomTE = new RandomEasy();
					scoreIntTE = 0;
					timeInt = 60;

					scoreTE.setText("Score:" + scoreIntTE);
					time.setText("Time:" + timeInt);

					flagTE = 0;
					flagOfEndTE = 0;
					aTE = -1;
					bTE = -1;

					for (int i = 0; i < 16; i++) 
					{
						clickNumberTE[i] = 0;
						cardButtonTE[i].setVisible(true);
					}

					for (int i = 0; i < 16; i++) 
					{
						cardFrontUserTE[i] = randomTE.cardFront[randomTE.randomNumberArray[i]];
					}

					for (int i = 0; i < 16; i++) 
					{
						cardButtonTE[i].setIcon(cardBackTE);
					}
				} 
				if(ans == 1)
					System.exit(1);
				
			}
		}
	}
	public void paintComponent(Graphics g) 
	{
		  super.paintComponent(g);
		  ImageIcon img = new ImageIcon("pictures/game.png");
		  g.drawImage(img.getImage(), 0, 0, null);
	}
}