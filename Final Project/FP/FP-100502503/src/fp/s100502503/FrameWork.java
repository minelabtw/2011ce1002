package fp.s100502503;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class FrameWork extends JFrame
{
	//protected GameTime gameTimePanel = new GameTime();
	//protected GameStep gameStepPanel = new GameStep();

	protected startPanel startPanel = new startPanel();

	private JButton start = new JButton("Start");
	private JButton quit = new JButton("Quit");
	
	private JButton timeMode = new JButton("Time");
	private JButton stepMode = new JButton("Step");

	private JButton time_60 = new JButton("60");
	private JButton time_120 = new JButton("120");
	
	private JButton step_20 = new JButton("20");
	private JButton step_35 = new JButton("50");
	
	public FrameWork()
	{
		
		startPanel.add(start);
		startPanel.add(quit);
		
		add(startPanel);
		
		start.setSize(80, 40);//Set the buttons' size
		quit.setSize(80, 40);
		timeMode.setSize(80, 40);
		stepMode.setSize(80, 40);
		
		//when the user click the start button, the game will start
		start.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0)
			{
				start.setVisible(false);
				quit.setVisible(false);
				
				startPanel.add(timeMode);
				startPanel.add(stepMode);
				
				timeMode.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent arg0)
					{
						timeMode.setVisible(false);
						stepMode.setVisible(false);
						
						startPanel.add(time_60);
						startPanel.add(time_120);
						
						time_60.addActionListener(new ActionListener(){
							public void actionPerformed(ActionEvent arg0)
							{
								GameTime gameTimePanel = new GameTime();
								startPanel.setVisible(false);
								add(gameTimePanel);
							}
						});
						
						time_120.addActionListener(new ActionListener(){
							public void actionPerformed(ActionEvent arg0)
							{
								GameTimeNormal gameTimeNormalPanel = new GameTimeNormal();
								startPanel.setVisible(false);
								add(gameTimeNormalPanel);	
							}
						});
					}
				});
				
				stepMode.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent arg0)
					{
						timeMode.setVisible(false);
						stepMode.setVisible(false);
						
						startPanel.add(step_20);
						startPanel.add(step_35);
						
						step_20.addActionListener(new ActionListener(){
							public void actionPerformed(ActionEvent e)
							{
								GameStep gameStepPanel = new GameStep();
								startPanel.setVisible(false);
								add(gameStepPanel);	
							}
						});
						step_35.addActionListener(new ActionListener(){
							public void actionPerformed(ActionEvent e)
							{
								GameStepNormal gameStepNormalPanel = new GameStepNormal();
								startPanel.setVisible(false);
								add(gameStepNormalPanel);	
							}
						});
					}
				});
			}
		});
		
		quit.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) 
			{
				System.exit(1);
			}
		});
	}
}