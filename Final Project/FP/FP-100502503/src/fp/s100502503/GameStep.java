package fp.s100502503;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.EtchedBorder;

public class GameStep extends JPanel implements ActionListener
{
	private RandomEasy random = new RandomEasy();
	
	//The top list variables
	//protected int[] userScoreSave = new int[10];
	//protected int[] toplist = new int[11];
	//protected String[] userNameString = new String[10];
	
	protected int scoreInt = 0;	//The score and the step to show out.
	protected int stepInt = 20;
	
	protected boolean[] cardVisible = new boolean[16]; //every members in this array corresponds a card, determining whether the card disappear or exsist.
	
	protected ImageIcon[] cardFrontUser = new ImageIcon[16];	//The array to save the front of the cards.
	protected JButton[] cardButton = new JButton[16];	//The button of the cards
	protected JButton renew = new JButton("Add New Cards");	//The button that let the user to add new card on the board.
	protected JLabel score = new JLabel("Score:" + scoreInt);//The label that show out the score and the step.
	protected JLabel step = new JLabel("Step:" + stepInt);
	protected JPanel p1 = new JPanel(new GridLayout(4, 4, 0, 0));	//The panel that uses GridLayout to arrange and save the cards.
	protected JPanel p2 = new JPanel(new GridLayout(3, 1, 0, 0));	//This panel's function is same as p1, but it is used to save the labels and a button/
	protected ImageIcon cardBack = new ImageIcon("pictures/backb.png");	//The back of the card.
	
	protected int flag = 0;//The flag to determined show the front of the card or the back of the card.
	protected int flagOfEnd = 0;
	protected int a = -1;	//These two variables are used to save the number of the card, for check the cards if they are the same.
	protected int b = -1;
	protected int flagVisible = 0;	//The variable is correspond the array that determined whether the card is visible.
	
	private Color labelc = new Color(198, 228, 233); //This color is determined the color of the label.
	private EtchedBorder labelb = new EtchedBorder(); 
	
	public GameStep() 
	{	
		for(int i = 0; i < 16; i++)
		{
			cardVisible[i] = true;
			cardFrontUser[i] = random.cardFront[random.randomNumberArray[i]];
			cardButton[i] = new JButton(cardBack);
			cardButton[i].setOpaque(false);
			cardButton[i].setContentAreaFilled(false);
			cardButton[i].setBorder(labelb);
			p1.add(cardButton[i]);
			cardButton[i].setSize(130, 130);
		}
	
		add(p1,BorderLayout.WEST);
	
		//To set the style of the labels and button
		step.setFont(new Font("SERIF", Font.BOLD, 14));
		score.setFont(new Font("SERIF", Font.BOLD, 14));
		renew.setFont(new Font("SERIF", Font.BOLD, 14));
		step.setOpaque(true);
		score.setOpaque(true);
		step.setBackground(labelc);
		score.setBackground(labelc);
		step.setBorder(labelb);
		score.setBorder(labelb);
		
		p2.add(step);
		p2.add(score);
		p2.add(renew);
		
		add(p2, BorderLayout.EAST);
		
		for(int i = 0; i < 16; i++)
		{
			cardButton[i].addActionListener(this);
		}
		renew.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e)
	{
		//check if the card that user turned is same or not, if same then destroy, if not, turn them back.
		for(int i = 0; i < 16; i++)
		{
			if(e.getSource() == cardButton[i])
			{
				//The flag to sure the card should be clicked only two terms
				//When the flag = 1, turn the card which user click
				flag++;
				if(flag == 1)
				{
					a = i;
					cardButton[a].setIcon(cardFrontUser[a]);
				}
				
				//When the flag = 2, check the numbers of card one and card two. If they are continuous, and the small one is double, they are the same card.
				else if(flag == 2)
				{
					b = i;
					stepInt--;
					step.setText("Step:" + stepInt);
					
					cardButton[b].setIcon(cardFrontUser[b]);
					int card_numberA = random.randomNumberArray[a];
					int card_numberB = random.randomNumberArray[b];
					if((card_numberA%2 == 0 && card_numberB == card_numberA+1) || (card_numberB%2 == 0 && card_numberA == card_numberB+1))
					{
						JOptionPane.showMessageDialog(null, "YA");
						cardVisible[a] = false;
						cardVisible[b] = false;
						
						cardButton[a].setVisible(cardVisible[a]);
						cardButton[b].setVisible(cardVisible[b]);
						scoreInt += 10;
						score.setText("Score:" + scoreInt);
					}
						
					else
					{
						JOptionPane.showMessageDialog(null, "WRONG");
						cardButton[a].setIcon(cardBack);
						cardButton[b].setIcon(cardBack);
					}
					flag = 0;      
				}
			}
		}
		
		//When the user click the renew button, add new cards on the table.
		if(e.getSource() == renew)
		{
			for(int i = 0; i < 16; i++)
			{
				cardButton[i].setVisible(true);
				cardButton[i].setIcon(cardBack);
				for(int x = 0; x < 16; x++)
				{
					random.randomNumberArray[x] = x;
				}
				random = new RandomEasy();
			}
			for(int i = 0; i< 16; i++)
			{
				cardFrontUser[i] = random.cardFront[random.randomNumberArray[i]];
			}
		}
		
		//When the step return to zero, game over.
		if(stepInt == 0)
		{
			int ans = JOptionPane.showOptionDialog(null, "Do you want to try again?", "Game over", 
					JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
					null, new Object[]{"Yes", "No"}, "Yes");
			if (ans == 0)
			{
				random = new RandomEasy();
				scoreInt = 0;
				stepInt = 20;

				score.setText("Score:" + scoreInt);
				step.setText("Step:" + stepInt);

				flag = 0;
				flagOfEnd = 0;
				a = -1;
				b = -1;

				for (int i = 0; i < 16; i++) 
				{
					cardButton[i].setVisible(true);
				}

				for (int i = 0; i < 16; i++) 
				{
					cardFrontUser[i] = random.cardFront[random.randomNumberArray[i]];
				}

				for (int i = 0; i < 16; i++) 
				{
					cardButton[i].setIcon(cardBack);
				}
			}	 
		
			if(ans == 1)
				System.exit(1);	
		}
	}

	public void paintComponent(Graphics g) 
	{
		  super.paintComponent(g);
		  ImageIcon img = new ImageIcon("pictures/game.png");
		  g.drawImage(img.getImage(), 0, 0, null);
	}
}


