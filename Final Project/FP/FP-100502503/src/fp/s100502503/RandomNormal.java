package fp.s100502503;

import javax.swing.ImageIcon;

public class RandomNormal
{
	protected ImageIcon[] cardFrontN = new ImageIcon[36]; 
	protected static int[] randomNumberArrayN = new int[36];
	
	public RandomNormal()
	{		
		for(int i = 0; i < 36; i++)
		{
			randomNumberArrayN[i] = i;
		}
		cardFrontN[0] = new ImageIcon("pictures/00.png");
		cardFrontN[1] = new ImageIcon("pictures/00.png");
		cardFrontN[2] = new ImageIcon("pictures/01.png");
		cardFrontN[3] = new ImageIcon("pictures/01.png");
		cardFrontN[4] = new ImageIcon("pictures/02.png");
		cardFrontN[5] = new ImageIcon("pictures/02.png");
		cardFrontN[6] = new ImageIcon("pictures/03.png");
		cardFrontN[7] = new ImageIcon("pictures/03.png");
		cardFrontN[8] = new ImageIcon("pictures/04.png");
		cardFrontN[9] = new ImageIcon("pictures/04.png");
		cardFrontN[10] = new ImageIcon("pictures/05.png");
		cardFrontN[11] = new ImageIcon("pictures/05.png");
		cardFrontN[12] = new ImageIcon("pictures/06.png");
		cardFrontN[13] = new ImageIcon("pictures/06.png");
		cardFrontN[14] = new ImageIcon("pictures/07.png");
		cardFrontN[15] = new ImageIcon("pictures/07.png");
		cardFrontN[16] = new ImageIcon("pictures/08.png");
		cardFrontN[17] = new ImageIcon("pictures/08.png");
		cardFrontN[18] = new ImageIcon("pictures/09.png");
		cardFrontN[19] = new ImageIcon("pictures/09.png");
		cardFrontN[20] = new ImageIcon("pictures/10.png");
		cardFrontN[21] = new ImageIcon("pictures/10.png");
		cardFrontN[22] = new ImageIcon("pictures/11.png");
		cardFrontN[23] = new ImageIcon("pictures/11.png");
		cardFrontN[24] = new ImageIcon("pictures/12.png");
		cardFrontN[25] = new ImageIcon("pictures/12.png");
		cardFrontN[26] = new ImageIcon("pictures/13.png");
		cardFrontN[27] = new ImageIcon("pictures/13.png");
		cardFrontN[28] = new ImageIcon("pictures/14.png");
		cardFrontN[29] = new ImageIcon("pictures/14.png");
		cardFrontN[30] = new ImageIcon("pictures/15.png");
		cardFrontN[31] = new ImageIcon("pictures/15.png");
		cardFrontN[32] = new ImageIcon("pictures/16.png");
		cardFrontN[33] = new ImageIcon("pictures/16.png");
		cardFrontN[34] = new ImageIcon("pictures/17.png");
		cardFrontN[35] = new ImageIcon("pictures/17.png");

		randomNumber();
	}
	
	public void randomNumber()
	{
		for(int i = 0; i < 36; i++)
		{
			int j = (int)(Math.random() * 100) % 35;
			
			int k = randomNumberArrayN[i];
			randomNumberArrayN[i] = randomNumberArrayN[j];
			randomNumberArrayN[j] = k;
		}
	}
}