package fp.s100502503;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.EtchedBorder;

public class GameTimeNormal extends JPanel implements ActionListener
{
	protected RandomNormal randomTN = new RandomNormal();

	protected int scoreIntTE = 0;
	protected int timeInt = 120;

	protected ImageIcon[] cardFrontUserTE = new ImageIcon[36];
	
	protected JButton[] cardButtonTE = new JButton[36];
	private JButton renewTE = new JButton("Add New Card");
		
	protected JLabel scoreTE = new JLabel("Score:" + scoreIntTE);
	protected JLabel time = new JLabel("Time:" + timeInt);

	protected JPanel p1TE = new JPanel(new GridLayout(6, 6, 0, 0)); // save the cards
	protected JPanel p2TE = new JPanel(new GridLayout(3, 1, 0, 0));
	protected ImageIcon cardBackTE = new ImageIcon("pictures/back.png");
	protected Timer timer = new Timer(1000, new TimerListener());

	protected int flagTE = 0;
	protected int flagOfEndTE = 0;
	protected int aTE = -1;
	protected int bTE = -1;

	private Color labelc = new Color(198, 228, 233); 
	private EtchedBorder labelb = new EtchedBorder(); 
	
	protected int[] clickNumberTE = new int[36];

	public GameTimeNormal() 
	{
		// The flag to determined show the front of the card or the back of the
		// card.

		for (int i = 0; i < 36; i++)
		{
			clickNumberTE[i] = 0;
		}
		for (int i = 0; i < 36; i++) 
		{
			cardFrontUserTE[i] = randomTN.cardFrontN[randomTN.randomNumberArrayN[i]];
		}
		for (int i = 0; i < 36; i++) 
		{
			cardButtonTE[i] = new JButton(cardBackTE);
			cardButtonTE[i].setOpaque(false);
			cardButtonTE[i].setContentAreaFilled(false);
			cardButtonTE[i].setBorder(labelb);
			p1TE.add(cardButtonTE[i]);
			cardButtonTE[i].setSize(80,80);
		}

		add(p1TE, BorderLayout.WEST);

		//Set the style of the labels
		time.setFont(new Font("SERIF", Font.BOLD, 14));
		scoreTE.setFont(new Font("SERIF", Font.BOLD, 14));
		renewTE.setFont(new Font("SERIF", Font.BOLD, 14));
		time.setOpaque(true);
		scoreTE.setOpaque(true);
		time.setBackground(labelc);
		scoreTE.setBackground(labelc);
		time.setBorder(labelb);
		scoreTE.setBorder(labelb);
		
		p2TE.add(time);
		p2TE.add(scoreTE);
		p2TE.add(renewTE);	
		p2TE.add(renewTE, BorderLayout.NORTH);


		add(p2TE, BorderLayout.EAST);

		for (int i = 0; i < 36; i++) 
		{
			cardButtonTE[i].addActionListener(this);
		}
	
	}

	public void actionPerformed(ActionEvent e)
	{
		timer.start();

		// check if the card that user turned is same or not, if same then
		// destroy, if not, turn them back.
		for (int i = 0; i < 36; i++) 
		{
			if (e.getSource() == cardButtonTE[i]) 
			{
				// The
				flagTE++;
				if (flagTE == 1) 
				{
					aTE = i;
					cardButtonTE[aTE].setIcon(cardFrontUserTE[aTE]);
				}

				else if (flagTE == 2) 
				{
					bTE = i;

					cardButtonTE[bTE].setIcon(cardFrontUserTE[bTE]);

					int card_numberA = randomTN.randomNumberArrayN[aTE];
					int card_numberB = randomTN.randomNumberArrayN[bTE];

					if ((card_numberA % 2 == 0 && card_numberB == card_numberA + 1)
							|| (card_numberB % 2 == 0 && card_numberA == card_numberB + 1)) 
					{
						JOptionPane.showMessageDialog(null, "YA");
						cardButtonTE[aTE].setVisible(false);
						cardButtonTE[bTE].setVisible(false);
						scoreIntTE += 10;
						scoreTE.setText("Score:" + scoreIntTE);
					}

					else 
					{
						
						JOptionPane.showMessageDialog(null, "WRONG");
						cardButtonTE[aTE].setIcon(cardBackTE);
						cardButtonTE[bTE].setIcon(cardBackTE);
					}
					flagTE = 0;
				}
			}
		}
	}

	// To control the seconds
	private class TimerListener implements ActionListener 
	{
		public void actionPerformed(ActionEvent e) 
		{
			timeInt--;
			time.setText("Time:" + timeInt);

			//When the user press the renew button, add new card but do not return the score or time to origin.
			if (e.getSource() == renewTE) 
			{
				for (int i = 0; i < 36; i++) 
				{
					cardButtonTE[i].setVisible(true);
					cardButtonTE[i].setIcon(cardBackTE);
					
					for (int x = 0; x < 36; x++)
					{
						randomTN.randomNumberArrayN[x] = x;
					}
					randomTN = new RandomNormal();
				}
				for (int i = 0; i < 36; i++) 
				{
					cardFrontUserTE[i] = randomTN.cardFrontN[randomTN.randomNumberArrayN[i]];
				}
			}
			
			if (timeInt == 0) 
			{
				
				timer.stop();
				JOptionPane.showMessageDialog(null, "GameOver");

				int ans = JOptionPane.showOptionDialog(null, "Do you want to try again?", "Game over", 
														JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
														null, new Object[]{"Yes", "No"}, "Yes");
				//If user press the YES button, renew all the game.
				if (ans == 0);
				{
					randomTN = new RandomNormal();
					scoreIntTE = 0;
					timeInt = 60;

					scoreTE.setText("Score:" + scoreIntTE);
					time.setText("Time:" + timeInt);

					flagTE = 0;
					flagOfEndTE = 0;
					aTE = -1;
					bTE = -1;

					for (int i = 0; i < 36; i++) 
					{
						clickNumberTE[i] = 0;
						cardButtonTE[i].setVisible(true);
					}

					for (int i = 0; i < 36; i++) 
					{
						cardFrontUserTE[i] = randomTN.cardFrontN[randomTN.randomNumberArrayN[i]];
					}

					for (int i = 0; i < 36; i++) 
					{
						cardButtonTE[i].setIcon(cardBackTE);
					}
				} 
				if(ans == 1)
					System.exit(1);				
			}
		}
	}
	public void paintComponent(Graphics g) 
	{
		  super.paintComponent(g);
		  ImageIcon img = new ImageIcon("pictures/game.png");
		  g.drawImage(img.getImage(), 0, 0, null);
	}
}