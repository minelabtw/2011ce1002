package fp.s100502003;
import java.awt.*;
import javax.swing.*;

// 鍵盤輸入時對應的介面(顯現出輸入的方向鍵)
public class Entering extends JPanel {
	// the default color are orange
	private Color color_up = Color.ORANGE;
	private Color color_down = Color.ORANGE;
	private Color color_space = Color.ORANGE;
	
	public Entering() {
		
	}

	// change the color
	// 但後面call這些method的時候，都不會改到顏色...目前還在找原因
	public void setUpColor() { 
		color_up = Color.YELLOW;
		repaint(); // add in here?
	}
	
	public void setDownColor() {
		color_down = Color.YELLOW;
		repaint();
	}
	
	public void setSpaceColor() {
		color_space = Color.YELLOW;
		repaint();
	}
	
	// 畫上下左右箭頭的圖案，以及space
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		int xCenter = getWidth()/2;
		int yCenter = getHeight()/2;
		
		int x_right[] = {xCenter+50, xCenter+30, xCenter+30, 
				xCenter+10, xCenter+10, xCenter+30, xCenter+30}; 
		int x_left[] = {xCenter-50, xCenter-30, xCenter-30, 
				xCenter-10, xCenter-10, xCenter-30, xCenter-30};
		int y_horizontal[] = {yCenter-20, yCenter, yCenter-12, 
				yCenter-12, yCenter-28, yCenter-28, yCenter-40};
		
		g.setColor(Color.DARK_GRAY);
		g.fillPolygon(x_right, y_horizontal, x_right.length);
		g.fillPolygon(x_left, y_horizontal, x_right.length);
		
		int x_vertical[] = {xCenter, xCenter+20, xCenter+8, 
				xCenter+8, xCenter-8, xCenter-8, xCenter-20};
		int y_up[] = {yCenter-70, yCenter-50,  yCenter-50,  
				yCenter-30,  yCenter-30,  yCenter-50,  yCenter-50};
		int y_down[] = {yCenter+30, yCenter+10,  yCenter+10,
				yCenter-10,  yCenter-10,  yCenter+10,  yCenter+10};
		
		g.setColor(color_up);
		g.fillPolygon(x_vertical, y_up, x_vertical.length);
		g.setColor(color_down);
		g.fillPolygon(x_vertical, y_down, x_vertical.length);
		
		g.setColor(color_space);
		g.fillRect(xCenter-50, yCenter+50, 100, 20);
		
		FontMetrics fm = g.getFontMetrics();
		int stringWidth = fm.stringWidth("Space");
		int stringAscent = fm.getAscent();
		g.setColor(Color.BLACK);
		g.drawString("Space", xCenter-stringWidth/2, yCenter+70-stringAscent/2);
	}
}
