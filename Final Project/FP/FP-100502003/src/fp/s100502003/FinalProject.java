package fp.s100502003;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;

public class FinalProject extends JFrame {
	private ImageIcon start_pic = new ImageIcon("beginning.png");
	private JButton start_button = new JButton("Start");
	public JPanel p = new JPanel();
	public Playing playing = new Playing();
	public static CountDown countDown = new CountDown();
//	public ShootingPlane balloons = new ShootingPlane();
	public Entering entering = new Entering();
	
	public FinalProject() {
		JLabel beginning = new JLabel(start_pic); // 一開始的底圖
		beginning.setBounds(0,0,start_pic.getIconWidth(),start_pic.getIconHeight()); // 設為視窗大小
		
		p.setLayout(null);
		p.add(beginning);
		p.add(start_button); // 加上button
		start_button.setBounds(550,420,200,60);
		
		start_button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				countDown.startCounting(true);
				remove(p);
				add(playing);
				validate();
			}
		});
		
		add(p);
		
//		 System.out.println(getFocusOwner()); // NO FOCUS!! WHY??
	}
	
	public static void main(String[] args) throws Exception {
		File file = new File("record.txt");
//		if(file.exists()) {
//			System.out.println("File already exists");
//			System.exit(0);
//		}
		
		if(countDown.checkStop()) {
			PrintWriter output = new PrintWriter(file);
			output.print("The time you left:　");
			output.println(countDown.getSecond()); // change countDown to static wrong???? right???
			output.close();
		}
		
		music Music = new music();
		Music.setmusic("Music/MIKA-Lollipop.wav");
		
		// 想要把原本寫的background music加進去，可是有點問題
//		BackgroundMusic music = new BackgroundMusic();
//		music.setMusic(1);
		
		FinalProject frame = new FinalProject();
		frame.setSize(815,638);
		frame.setTitle("Shooting balloon");
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
