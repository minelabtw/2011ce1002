package fp.s100502003;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.*;
import javax.swing.border.*;

// 這個class用來將目前暫時寫好的panel全都加在一起，之後只要call這個class就好了
public class Playing extends JPanel {
	private Image image = null; // 設一個空的Image來存背景圖
	private ImageIcon background = new ImageIcon("background.jpg"); // 底圖
	private ImageIcon shooter = new ImageIcon("shooter.png");
	public ShootingPlane balloons = new ShootingPlane();
	public CountDown countDown = new CountDown();
	public Entering entering = new Entering();
	
	// a big problem: 沒辦法直接將用來描準的線畫上去，因為java是一層一層疊上去的，所以會被後來疊上的panel蓋過
	// 試著解決：原本是要試著加一個透明的panel上去，再把瞄準的線畫在上面，可是後來發現好像還是會被蓋過去
//	public drawLinePanel line = new drawLinePanel();
	
	public Playing() {
		JLabel shoot_pic = new JLabel(shooter);
		JLabel guideLine = new JLabel("  Please shoot the balloons in order!!"); // 遊戲導引
		guideLine.setFont(new Font("TimesNewRoman", Font.BOLD, 16));
		guideLine.setBorder(new LineBorder(Color.RED,2));
		
		setLayout(null);
		
		//　固定每個Panel出現的位置
		countDown.setBounds(660, 20, 120, 120);
		balloons.setBounds(50, 50, 600, 400);
		entering.setBounds(660, 300, 120, 160);
		guideLine.setBounds(50, 10, 600, 30);
		shoot_pic.setBounds(280, 470, 120, 126); // not yet
//		line.setBounds(0,0,815,638); // 加透明的panel
		
		add(shoot_pic);
		add(countDown);
		add(balloons);
		add(entering);
		add(guideLine);
//		add(line); // 加透明的panel
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		image = background.getImage();
		g.drawImage(image, 0, 0, null); // draw background
		
		// 一開始時直接在這個panel上畫，可是會被蓋過去，還在想有什麼好方法解決
//		g.setColor(Color.red);
//		g.drawArc(getXPointer()-30, getYPointer()-30, 60, 60, 0, 360);
//		g.drawLine(getXPointer(), getYPointer()-10, getXPointer(), getYPointer()+10);
//		g.drawLine(getXPointer()-10, getYPointer(), getXPointer()+10, getYPointer());
//		g.setColor(Color.red);
//		g.drawLine(330, 470, balloons.getXPointer()+50, balloons.getYPointer()+50);
	}
	
}

//      這個class是用來當作另一個透明的panel的，試著解決前面的問題
//      可是好像沒什麼用，還是會被蓋過，但不是造理說最後add的會在最上面嗎?
//      不知道為什麼沒有，難道是因為把它設為透明的，會連上面畫的線一起變透明嗎?有沒有只讓底層變透明的方法???
//class drawLinePanel extends JPanel { // 加透明的panel 
//	ShootingPlane balloons = new ShootingPlane();
//	drawLinePanel() {
//		setOpaque(false);	
//	}
//	
//	protected void paintComponent(Graphics g) {
//		super.paintComponent(g);
//		g.setColor(Color.red);
//		g.drawLine(330, 470, balloons.getXPointer()+50, balloons.getYPointer()+50);
//	}
//}
