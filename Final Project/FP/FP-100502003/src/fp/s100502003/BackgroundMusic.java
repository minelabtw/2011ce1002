package fp.s100502003;

import java.applet.Applet;
import java.applet.AudioClip;
import javax.swing.*;
import java.net.URL;

public class BackgroundMusic {
	static AudioClip sound;
	
	public void setMusic(int musicIndex) {
		URL urlForAudio = getClass().getResource("Music/MIKA-Lollipop.wav");
		sound = Applet.newAudioClip(urlForAudio);
		
		// 這個寫法跟上面那個應該是一樣的，只是不知道為什麼弄不出來，所以先試試看不同寫法
//		sound = Applet.newAudioClip(this.getClass().getResource("Music/MIKA-Lollipop.wav"));
		
		switch(musicIndex) {
			case 1:
				stop();
				sound = Applet.newAudioClip(this.getClass().getResource("Music/MIKA-Lollipop.wav"));
				sound.loop();
				break;
			case 0:
				stop();
				sound = Applet.newAudioClip(this.getClass().getResource("Music/MIKA-Lollipop.wav"));
				sound.loop();
				break;
		}
	}
	
	public void stop() {
		sound.stop();
	}
}
