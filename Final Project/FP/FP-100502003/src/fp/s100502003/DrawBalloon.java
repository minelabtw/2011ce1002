package fp.s100502003;
import java.awt.*;

import javax.swing.*;

// class that draw each balloon on the panel
public class DrawBalloon extends JPanel {
	private Color COLOR;
	private String number;
	private int xPointer = getWidth()/2;
	private int yPointer = getHeight()/2;
	private boolean pointed = false;
	
	public DrawBalloon() {
		
	}
	
	public void setColor(Color color) {
		this.COLOR = color;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	public void setPointed(boolean set) {
		pointed = set;
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		int xCenter = getWidth()/2;
		int yCenter = getHeight()/2;
		
		int upRadius = (int)(Math.min(getWidth(), getHeight())*0.3); // 上半弧的半徑
		int downRadius = (int)(Math.min(getWidth(), getHeight())*0.4); // 下半弧的半徑
		int x = xCenter-upRadius;
		int y = yCenter-upRadius;
		
		g.setColor(COLOR);
		g.fillArc(x, y, 2*upRadius, 2*upRadius, 0, 180);
		g.fillArc(x, yCenter-downRadius, 2*upRadius, 2*downRadius, 180, 180);
		
		int x_poly[] = {xCenter-downRadius/3, xCenter, xCenter+downRadius/3};
		int y_poly[] = {yCenter+downRadius*4/3, yCenter+downRadius, yCenter+downRadius*4/3};
		g.fillPolygon(x_poly, y_poly, x_poly.length); // 氣球下面的三角形
		
		g.setColor(Color.black);
		g.setFont(new Font("TimesNewRoman", Font.BOLD, 16));
		g.drawString(number, xCenter-3, yCenter+5);
		
		
		// 原本因為沒辦法在全部氣球組合好的那個panel畫瞄準的東西，所以想說畫在最底的panel再一個一個判斷要不要顯現
		// 可是後來加上去後會不曉得要怎麼消掉(瞄準別的的時候，之前畫的還會存在)，就先放著，試著想別的方法
		if(pointed) {
			g.setColor(Color.red);
			g.drawArc(xPointer-30, yPointer-30, 60, 60, 0, 360);
			g.drawLine(xPointer, yPointer-10, xPointer, yPointer+10);
			g.drawLine(xPointer-10, yPointer, xPointer+10, yPointer);
		}
	}
}
