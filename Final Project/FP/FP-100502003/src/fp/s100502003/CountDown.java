package fp.s100502003;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

//倒數計時的class
public class CountDown extends JPanel {
	private int second = 60; // 時間限定為60s
	private boolean stop = false;
	private boolean start = false;
	private Color COLOR = Color.GRAY;

	Timer timer = new Timer(1000, new TimerListener());
	
	public CountDown() {
		startCounting(start); // 原本要用來使得在按完按鈕後才開始倒數，可是不知道為什麼，用了卻沒用
		if(stop) {
			timer.stop();
		}
	}
	
	public void startCounting(boolean Start) { // initial counting is wrong!!!!!!!!!!
		start = Start;
		if(start = true) { // 如果輸入要開始的指令再使timer start
			timer.start();
		}
	}
	
	public void setTime() {
		if(stop) { // 暫停的時候second不變
			
		}
		else if(this.second != 0 && !stop) {
			second--;
		}
		else {
			stop = true;
		}
		repaint();
	}
	
	public int getSecond() {
		if(stop) {
			return this.second;
		}
		else {
			return 0;
		}
	}
	
	public Timer getTimer() {
		return timer;
	}
	
	public void setColor() { // 隨時間變換顏色
		if(second%4 == 1)
			COLOR = Color.DARK_GRAY;
		else if(second%4 == 2)
			COLOR = Color.GRAY;
		else if(second%4 == 3)
			COLOR = Color.LIGHT_GRAY;
		else {
			COLOR = Color.BLACK;
		}
	}
	
//	public void setStop(boolean Pause) {
//		this.stop = Pause;
//	}
	
	public boolean checkStop() { // get the variable stop
		return stop;
	}
	
	public void Stop() {
		timer.stop();
		repaint();
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		int xCenter = getWidth()/2;
		int yCenter = getHeight()/2;
		int radius1 = (int)(Math.min(getWidth(), getHeight())*0.4);
		int radius2 = (int)(Math.min(getWidth(), getHeight())*0.25);
		
		Polygon polygon1 = new Polygon(); // draw the outside 六角形 
		polygon1.addPoint(xCenter+radius1, yCenter);
		for(int i=1; i<=5; i++) {
			polygon1.addPoint((int)(xCenter+radius1*Math.cos(2*i*Math.PI/6)),
					(int)(yCenter-radius1*Math.sin(2*i*Math.PI/6)));
		}
		
		Polygon polygon2 = new Polygon(); // draw the inside 六角形 
		polygon2.addPoint(xCenter+radius2, yCenter);
		for(int i=1; i<=5; i++) {
			polygon2.addPoint((int)(xCenter+radius2*Math.cos(2*i*Math.PI/6)),
					(int)(yCenter-radius2*Math.sin(2*i*Math.PI/6)));
		}
		
		g.setColor(COLOR);
		g.fillPolygon(polygon1);
		g.setColor(Color.WHITE);
		g.fillPolygon(polygon2);
		
		// 顯示現在剩餘的秒數
		g.setColor(Color.BLACK);
		g.setFont(new Font("TimesNewRoman", Font.BOLD+Font.ITALIC, 20));
		if(second <= 10) {
			g.setColor(Color.RED);
		}
		g.drawString(Integer.toString(second), xCenter-10, yCenter+3);
	}
	
	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			setTime();
			setColor();
		}
	}
}
