package fp.s100502003;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

// 把所有的balloon加在一起，形成一個新panel
public class ShootingPlane extends JPanel {
//	public drawLinePanel line = new drawLinePanel();
//	JButton circleButton = new JButton();
	
	private JPanel[] eachBalloon = new JPanel[24];
	private int[] random = new int[24]; // array that used to store the random values
	
	private int xPointer = 50;
	private int yPointer = 50;
	private boolean pause = false;
//	private int clear = 0;
	private int times=0; // 判斷pause按的次數，偶數是pause，奇數是繼續
	
	KeyboardFocusManager manager; 
	
	private int[] check = new int[24]; // whether the balloons are been shot
	private int[] order = new int[24]; // the number of balloons in each panel(read in order)
	private int[] grid = new int[24]; // let us know the numbers(1~24) are in which grid

	public Entering entering = new Entering();
	public CountDown countDown = new CountDown();
	Timer timer2 = new Timer(1000, new TimerListener2());  
	
	public ShootingPlane() {
		int num1, num2, temp;
		
		timer2.start();
		
		// 跟前面的問題一樣，在想辦法解決瞄準的東西畫不上去的困難
		// 想說改成畫準心就可以直接加在每個panel上，而不用考慮線必須要經過frame再經過panel的問題
		// 試著想要弄一個button，在上面加上底為透明的圖片(準心)，可是後來沒有辦法把圖片的底弄成透明的，就先放著，想別的方法
//		JButton circleButton = new JButton() {
//			public boolean contains(int x, int y) {
//				x = (x-30)*(x-30)+(y-30)*(y-30);
//				if(x<900) {
//					return true;
//				}
//				return false;
//			}
//		};
//		circleButton.setIcon(new ImageIcon("pointer.png"));
//		circleButton.setUI(new javax.swing.plaf.basic.BasicButtonUI());
//		circleButton.setFocusPainted(false);
//		circleButton.setContentAreaFilled(false);
//		circleButton.setBorderPainted(false);
		
		try{ // 得到24個沒有按順序排列，不同的數(0~23)
			for(int i=0; i<24; i++) {
				random[i] = i;
			}
			for(int j=0; j<24; j++) {
				num1 = (int)(Math.random()*24);
				num2 = (int)(Math.random()*24);
				temp = random[num1];
				random[num1] = random[num2];
				random[num2] = temp;
			}
		}
		catch(ArrayIndexOutOfBoundsException e) { // 可能超過邊界
			System.err.println("The array's index is out of bounder!");
		}
		catch(NegativeArraySizeException e) { // 考慮負數
			System.err.println("The array's value may be negative!");
		}
		
		setLayout(null);
		
		for(int i=0; i<24; i++) {
			eachBalloon[i] = new DrawBalloon();
			eachBalloon[i].setBorder(new LineBorder(Color.BLACK, 1));
			
			// 讓它設為不同的顏色
			if(random[i]%5 == 1) {
				((DrawBalloon) eachBalloon[i]).setColor(Color.pink);
			}
			else if(random[i]%5 == 2) {
				((DrawBalloon) eachBalloon[i]).setColor(Color.yellow);
			}
			else if(random[i]%5 == 3) {
				((DrawBalloon) eachBalloon[i]).setColor(Color.blue);
			}
			else if(random[i]%5 == 4) {
				((DrawBalloon) eachBalloon[i]).setColor(Color.green);
			}
			else {
				((DrawBalloon) eachBalloon[i]).setColor(Color.white);
			}
			
			((DrawBalloon) eachBalloon[i]).setNumber(Integer.toString(random[i]+1));
			
			// 把每個氣球的panel一個一個加上去
			int j=i%6, k=i/6;
			eachBalloon[i].setBounds(j*100,k*100,100,100);
			add(eachBalloon[i]);
			check[i]=1; // not been shot
		}
		
		for(int j=0; j<24; j++) {
			for(int k=0; k<24; k++) {
				if(random[j] == k) {
					order[j] = k+1; // 每一個drawBalloon的panel依序代表的氣球號碼
				}
			}
		}
		
		// 因為keyListener有focus的問題，想辦法讓focus轉過來
		KeyEventDispatcher keyEventDispatcher=new KeyEventDispatcher() {
			@Override
			public boolean dispatchKeyEvent(KeyEvent e) {
				// TODO Auto-generated method stub
				if (e.getID() == KeyEvent.KEY_PRESSED) {
					switch (e.getKeyCode()) {
					
						case KeyEvent.VK_UP:
							
//							這是前面要直接把瞄準的東西加在panel上時會用到的，加是加的上去，可是消不掉
//							clear = getPointed();
//							((DrawBalloon) eachBalloon[getPointed()]).setPointed(true);
							
							if(yPointer >=  50) {
								yPointer -= 100;
							}
							entering.setUpColor(); // 沒有改變，why??
							repaint();
							System.out.println("up"); // 在下面顯示，知道按了"up"(表示這個功能是有效的)
							break;

						case KeyEvent.VK_DOWN:
							
//							clear = getPointed(); // 用途同上
//							((DrawBalloon) eachBalloon[getPointed()]).setPointed(true);
							if(yPointer <=  getHeight()/8*7) { 
								yPointer += 100; 
							}
							entering.setDownColor(); // no change, why?
							repaint();
							System.out.println("down");
							break;
							
						case KeyEvent.VK_SPACE:
							JPanel nothing = new JPanel(); // 想要加一個空白的panel上去蓋掉，留邊線
							nothing.setBounds(xPointer-50, yPointer-50, 100, 100);
							nothing.setBorder(new LineBorder(Color.BLACK, 1));
							
							check[getPointed()] = 0;
//							if(Pause() == true) { // 目前加了會有點問題，timer2有時會stop，先放著
//								timer2.stop();
//							}
					
							remove(eachBalloon[getPointed()-1]); // 氣球不見
													
//							add(nothing); // 直接加會有問題，先不弄，只是remove掉
							entering.setSpaceColor(); // no change, why??
							repaint();
							break;
							
						case KeyEvent.VK_ENTER:
							if(times%2 == 0) {
								countDown.Stop(); // no change, why?
								countDown.getTimer().stop();
								timer2.stop();
								pause = true;
							}
							else {
								countDown.getTimer().start();
								timer2.start();
								pause = false;
							}
							times++;
							repaint();
							break;
							
						default: 
							break;
					}
				}
				return false;
			}
		};

		manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
		manager.addKeyEventDispatcher(keyEventDispatcher);
		
		 // 原本因為keyListener focus 的問題，功能一直加不上去，想說改用這個看看，結果還是沒用
		addMouseMotionListener(new MouseMotionAdapter() {
			public void mouseClicked(MouseEvent e) {
				JPanel nothing = new JPanel();
				nothing.setSize(100, 100);
				
				remove(eachBalloon[getPointed()]);
				add(nothing); // ?????這樣會剛好加到那上一格上面??????
				validate();
			}
		});
		requestFocus();
		
//		add(line);
	}
	
	public boolean Pause() {
		for(int i=0; i<24; i++) {
			if(grid[i] != getPointed() && check[i] == 0) {
				pause = true;
			}
		}
		return pause;
	}
	
	public void setXPointer(int xChange) {
		xPointer = xChange;
	}
	
	public void setYPointer(int yChange) {
		yPointer = yChange;
	}
	
	public int getXPointer() {
		return this.xPointer;
	}
	
	public int getYPointer() {
		return this.yPointer;
	}
	
	public void setOrder() { // set the corresponding grid of number 1 to 24
		int n=0;
		for(int m=0; m<24; m++) {
			n = 0;
			while(order[n] != m+1) {
				n++;
			}
			grid[m] = n+1; // (m+1)(從1開始，到24)在第n+1格
		}
	}
	
	public int getPointed() { // get the grid pointed by the pointer
		int x= getXPointer()/100+1, y;
		
		if(getYPointer() == getHeight()/8) {
			y = 0;
		} 
		else if(getYPointer() == getHeight()/8*3) {
			y = 6;
		}
		else if(getYPointer() == getHeight()/8*5) {
			y = 12;
		}
		else {
			y = 18;
		}
		return x+y;
	}
	
	class TimerListener2 implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
//			for(int a=0; a<6; a++) { // 寫到一半，試著要解決前面瞄準的東西加上去可是消不掉的問題
//				((DrawBalloon) eachBalloon[a+1]).setPointed(true);
//				((DrawBalloon) eachBalloon[a]).setPointed(false);
//			}
			
			if(!countDown.checkStop()) {
				if(xPointer > 500) {
					xPointer = 50;
				}	
				else {
					xPointer += 100;
				}
//				((DrawBalloon) eachBalloon[getPointed()-1]).setPointed(true);
				
				// 因為瞄準的東西被蓋住了，目前想要知道他跑到哪一格，就先輸出他目前所在的格數
				System.out.print("The order of grid been pointed: ");
				System.out.println(getPointed());
				revalidate(); // ???????
				repaint();
			}
		}
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		g.setColor(Color.red);
		g.drawArc(getXPointer()-30, getYPointer()-30, 60, 60, 0, 360);
		g.drawLine(getXPointer(), getYPointer()-10, getXPointer(), getYPointer()+10);
		g.drawLine(getXPointer()-10, getYPointer(), getXPointer()+10, getYPointer());
	}
	
	// 把透明的Panel 加在這裡會怎樣? try, but may be useless
//	class drawLinePanel extends JPanel {
//		drawLinePanel() {
////			setOpaque(false);	
//		}
//		
//		protected void paintComponent(Graphics g) {
//			super.paintComponent(g);
//			g.setColor(Color.red);
//			g.drawLine(330, 400, getXPointer()+50, getYPointer()+50);
//		}
//	}
	
}	


