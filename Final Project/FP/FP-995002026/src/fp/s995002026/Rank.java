package fp.s995002026;

import javax.swing.*;

import java.awt.*;

import java.io.*;

import java.util.Scanner;

/*排行榜*/

public class Rank {
	int[] score = new int[10];// 紀錄排行榜名單

	File file = new File("rank.txt");

	FileWriter out = new FileWriter(file, true);

	FileReader in = new FileReader("rank.txt");

	Scanner input = new Scanner(in);

	Rank() throws Exception {

		Read();// 初始化排行榜紀錄

		Write(score);
	}

	private void Write(int[] input) throws Exception {
		for (int i = 0; i < 10; i++) {
			out.write(Integer.toString(input[i]) + "\n");
		}
		out.close();
	}

	public void Read() {
		for (int i = 0; i < 10; i++) {
			if (input.hasNext())
				score[i] = Integer.parseInt(input.next());
		}

	}

	public void data(int input) throws Exception { // 玩家資訊
		if (input > score[9]) {
			score[9] = input;
			for (int i = 0; i < 9; i++) {
				for (int j = 0; j < 9; j++) {
					if (score[j] < score[j + 1]) {
						int temp = score[j];
						score[j] = score[j + 1];
						score[j + 1] = temp;
					}
				}
			}
		}
		file.delete();
		file.createNewFile();
		out = new FileWriter(file);
		Write(score);
	}
}
