package fp.s995002026;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/*遊戲視窗*/

public class FrameWork extends JFrame {
	int t1, t2 = 30, j;
	int[] a = new int[100];
	int score = 0;
	public Paint[] paint = new Paint[100];
	public JPanel p1 = new JPanel(new GridLayout(10, 10));
	public JLabel currenttime = new JLabel(
			"                                                 記憶時間:" + t1);
	public JFrame gameover = new JFrame();
	public int[] cls = new int[100]; // 重設畫面

	// private Game game=new Game();
	public JButton over = new JButton("");
	private boolean bool = false;
	public int subtime=1,subtime2=1;

	Timer time = new Timer(100, new TimerListener());

	FrameWork(int[] input) {
		a = input;
		currenttime.setFont(new Font("Conslas", Font.BOLD, 24));
		currenttime.setSize(800, 100);

		set(); // 遊戲視窗
		drawItems(input);
		add(currenttime, BorderLayout.NORTH);
		add(p1);

	}

	public void set() { // 遊戲視窗
		setSize(800, 800);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
	}

	public int getT1() { // 讀取倒數計時
		return t1;
	}

	public int getT2() { // 讀取遊戲時間
		return t2;
	}

	public void setT1() { // 設定記憶時間
		t1 = 10;
	}

	public void setT2() { // 設定遊戲時間
		t2 = 30;
	}

	public void drawItems(int[] input) { // 放置寶物
		for (int i = 0; i < 100; i++) {
			p1.add(paint[i] = new Paint(input[i]));
			cls[i] = 5;

		}
	}

	public void clsPanel() {
		for (int i = 0; i < 100; i++) {
			p1.add(paint[i] = new Paint(cls[i]));
			paint[i].addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					for (int i = 0; i < 100; i++) {
						if (e.getSource() == paint[i]) {
							if (a[i] != 2) {
								score = score + a[i];
								cls[i] = a[i];
								paint[i] = new Paint(cls[i]);
								p1.removeAll();
								for (int j = 0; j < 100; j++) {
									p1.add(paint[j]);
								}
							} else {
								//time.stop();
								cls[i] = 2;
								paint[i] = new Paint(cls[i]);
								for (int j = 0; j < 100; j++) {
									p1.add(paint[j]);
								}
								bool=true;
								
							}
						}
					}
				}
			});
		}
	}
	
	public void SubTime(){
		subtime=subtime+1;
	}
	public void SubTime2(){
		subtime2=subtime2+1;
	}

	public void GameOver() { // 遊戲結束
		over.setText("        score = " + score);
		over.setFont(new Font("Conslas", Font.BOLD, 24));
		gameover.setSize(500, 200);
		gameover.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		gameover.setLocationRelativeTo(null);
		gameover.add(over);
		gameover.setVisible(true);

	}

	public class TimerListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			SubTime();
			if(subtime==10){
				
				t1 = t1 - 1;subtime=0;
			}
			if (t1 >= 0)
				currenttime
						.setText("                                                 記憶時間:"
								+ t1);
			else {
				if (t1 == -1) {
					p1.removeAll();
					clsPanel();
				}
				if (t2 >= 0){
					if(bool){
						
						//p1.removeAll();
						
						p1.removeAll();
						GameOver();
						time.stop();
					}
					currenttime
							.setText("                                                 遊戲開始:"
									+ t2);
				}
				else
					time.stop();
				SubTime2();
				if(subtime2==10){
					subtime2=0;
					t2 = t2 - 1;
				}
			}

			if (t2 == -1) {
				removeAll();
				GameOver();
			}
		}
	}

}
