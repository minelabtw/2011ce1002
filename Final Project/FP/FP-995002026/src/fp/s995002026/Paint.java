package fp.s995002026;

import javax.swing.*;

import java.awt.*;

public class Paint extends JButton {
	int treasure; // 放置寶物或炸彈
	int i = 0; // 個位置該放甚麼物品

	 ImageIcon icon1=new ImageIcon("image\\bomb.png");
	 ImageIcon icon2=new ImageIcon("image\\treasure.png");
	 ImageIcon icon3=new ImageIcon("image\\space.png");
	Paint(int input) {
		treasure = input;
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (treasure == 2) { // 炸彈
			//g.setColor(Color.BLACK);
			//g.fillOval(0, 0, getWidth(), getHeight());
			icon1.paintIcon(this, g, 0, 0);
		} else if (treasure == 1) { // 寶物1
			//g.setColor(Color.ORANGE);
			//g.fillOval(0, 0, getWidth(), getHeight());
			icon2.paintIcon(this, g, 0, 0);
		} else if (treasure == 0) { // 空白
			icon3.paintIcon(this, g, 0, 0);
		} else if (treasure == 5) {
			g.setColor(Color.blue);
			g.fillRect(0, 0, getWidth(), getHeight());
			repaint();
		} else
			;
	}
}
