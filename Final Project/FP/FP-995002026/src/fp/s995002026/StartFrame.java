package fp.s995002026;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/*開始視窗*/

public class StartFrame extends JFrame implements ActionListener {
	public ImageIcon sun=new ImageIcon("image\\images.png");
	public ImageIcon star=new ImageIcon("image\\star.png");
	public ImageIcon moon=new ImageIcon("image\\moon.png");
	
	public JButton start = new JButton("開始");
	public JButton rank = new JButton("排行榜");
	public JButton setting = new JButton("設定");
	public JPanel p1 = new JPanel(new GridLayout(3, 1));
	public String name; // 玩家名稱
	public int[] a = new int[100];
	public Rank rk;
	JPanel options = new JPanel(new GridLayout(1, 3,100,100));
	FrameWork f;
	RankFrame rankframe;

	StartFrame(int[] input) {
		
		a = input;
		f = new FrameWork(a);
		try {
			rk = new Rank();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		start.addActionListener(this);
		rank.addActionListener(this);
		setting.addActionListener(this);

		p1.add(start);
		p1.add(rank);
		p1.add(setting);
		add(p1);
		setSize(800, 800);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == start) {
			//f.setVisible(false);
			//f.gameover.setVisible(false);
			//f.time.stop();
			//f=new FrameWork(a);
			options.add(start);
			options.add(rank);
			options.add(setting);

			setVisible(false); // 關閉開始視窗
			/*
			 * f.removeAll();
			 * 
			 * f.time.stop();
			 */
			f.setVisible(false);

			f.setT1();
			f.time.stop();
			f.add(options, BorderLayout.SOUTH); // 選單
			f.setVisible(true);
			setName(); // 玩家名稱
			f.setT2();
			f.time.start();
		}
		if (e.getSource() == rank) {
			

			try {
				
				// Rank rk = new Rank();
				rk.data(f.score);new RankFrame(rk);
				rk.Read();
				// repaint();
			} catch (Exception ex) {

			}

		}
		if (e.getSource() == setting) { // 設定環境
			SettingFrame s = new SettingFrame();
		}
	}

	public void setName() {
		name = JOptionPane.showInputDialog("請輸入名稱");
	}
}
