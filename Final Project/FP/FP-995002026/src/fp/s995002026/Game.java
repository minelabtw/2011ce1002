package fp.s995002026;

import javax.swing.*;

import fp.s995002026.FrameWork.TimerListener;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class Game implements ActionListener {
	StartFrame f;
	Random random = new Random();
	public int[] items = new int[100];
	public int score = 0;

	Game() {
		for (int i = 0; i < 100; i++) { // 隨機物品
			items[i] = random.nextInt(3);
		}

		f = new StartFrame(items);
		// f.f=new FrameWork(items);

		f.f.over.addActionListener(this);
		for (int i = 0; i < 100; i++) {
			f.f.paint[i].addActionListener(this);
		}
	}

	public int GetItems(int a) { // 幫助紀錄分數
		return items[a];
	}


	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == f.f.over) {
			f.f.gameover.setVisible(false);
			f.f.setVisible(false);
			for (int i = 0; i < 100; i++) { // 隨機物品
				items[i] = random.nextInt(3);
			}

			f = new StartFrame(items);
		}
	}
}
