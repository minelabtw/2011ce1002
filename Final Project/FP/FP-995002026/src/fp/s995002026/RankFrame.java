package fp.s995002026;

import javax.swing.*;

import java.awt.*;

public class RankFrame extends JFrame{
	public JPanel p=new JPanel();
	public JLabel[] L=new JLabel[10];
	
	RankFrame(Rank rk){
		setLayout(null);
		
		L[0]=new JLabel(" 第一名  : "+rk.score[0]);
		L[1]=new JLabel(" 第二名  : "+rk.score[1]);
		L[2]=new JLabel(" 第三名  : "+rk.score[2]);
		L[3]=new JLabel(" 第四名  : "+rk.score[3]);
		L[4]=new JLabel(" 第五名  : "+rk.score[4]);
		L[5]=new JLabel(" 第六名  : "+rk.score[5]);
		L[6]=new JLabel(" 第七名  : "+rk.score[6]);
		L[7]=new JLabel(" 第八名  : "+rk.score[7]);
		L[8]=new JLabel(" 第九名  : "+rk.score[8]);
		L[9]=new JLabel(" 第十名  : "+rk.score[9]);
		SetFont();
		Position();
		setSize(400,560);
		setLocationRelativeTo(null);
		setVisible(true);
		for(int i=0;i<10;i++){
			add(L[i]);
		}
	}
	
	public void SetFont(){
		for(int i=0;i<10;i++){
			L[i].setFont(new Font("Conslas", Font.BOLD, 24));
		}
	}
	
	public void Position(){
		L[0].setBounds(10,10,500,50);
		L[1].setBounds(10,60,500,50);
		L[2].setBounds(10,110,500,50);
		L[3].setBounds(10,160,500,50);
		L[4].setBounds(10,210,500,50);
		L[5].setBounds(10,260,500,50);
		L[6].setBounds(10,310,500,50);
		L[7].setBounds(10,360,500,50);
		L[8].setBounds(10,410,500,50);
		L[9].setBounds(10,460,500,50);
	}
	
}
