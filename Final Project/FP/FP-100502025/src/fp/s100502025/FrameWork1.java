package fp.s100502025;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

public class FrameWork1 extends JFrame {  //第一個畫面的視窗
	private JButton jbtStart = new JButton("Start Game");  //按鈕:START GAME
	private JButton jbtExit = new JButton("Exit");  //按鈕:EXIT
	private JLabel jlb = new JLabel();  
	private ImageIcon main = new ImageIcon("image/main2.jpg");  //第一個畫面的背景圖
	private JPanel p1 = new JPanel();  
	private JPanel p2 = new JPanel();
	private illustrate illustrate = new illustrate();  //說明遊戲規則的Panel
	 
	public FrameWork1() {
		Font font1 = new Font("Serif",Font.BOLD,30);  //設定字型
			
		jbtStart.setFont(font1);  //START GAME設定字型
		jbtExit.setFont(font1);  //EXIT設定字型
		
		p2.setLayout(new GridLayout(1,2));  //排版
		p2.add(jbtStart);		
		p2.add(jbtExit);
		
		jlb.setIcon(main);  //加入背景圖案
		p1.add(jlb);  
		
		setLayout(new BorderLayout());  //排版
		add(p1 , BorderLayout.CENTER);	
		add(p2 , BorderLayout.SOUTH);
		
		jbtStart.addActionListener(new ActionListener() {  //讓按鈕START GAME有功能
			public void actionPerformed(ActionEvent e) {  //點了這個按鈕，會出現說明
				setVisible(false);  //讓第一個視窗隱藏
				illustrate.setTitle("Snake");  //設定說明的視窗初始值
				illustrate.setVisible(true);
				illustrate.setLocation(300,100);
				illustrate.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);				
				illustrate.setSize(815,880);					
			}
		});
		
		jbtExit.addActionListener(new ActionListener() {  //讓按鈕EXIT有功能
			public void actionPerformed(ActionEvent e) {  //點了這個按鈕，會離開，結束程式
				System.exit(0);
			}
		});
	}	
}
