package fp.s100502025;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class illustrate extends JFrame {  //說明畫面
	private JPanel p1 = new JPanel();
	private JLabel jlb1 = new JLabel("         當長度");
	private JLabel jlb2 = new JLabel("         大於20");
	private JLabel jlb3 = new JLabel("       就能獲勝");
	private JButton jbtOK = new JButton("OK !");
	
	public illustrate() {
		p1.setLayout(new GridLayout(3,1));
		p1.setBackground(Color.WHITE);
		Font font1 = new Font("Serif",Font.BOLD,100);
		Font font2 = new Font("Serif",Font.BOLD,30);
		jbtOK.setFont(font2);
		jlb1.setFont(font1);
		jlb2.setFont(font1);
		jlb3.setFont(font1);
		p1.add(jlb1);
		p1.add(jlb2);
		p1.add(jlb3);
		
		setLayout(new BorderLayout());
		
		add(p1,BorderLayout.CENTER);
		add(jbtOK,BorderLayout.SOUTH);
		
		jbtOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				GameFrame gameframe1 = new GameFrame();
				gameframe1.setTitle("Snake");
				gameframe1.setVisible(true);
				gameframe1.setLocation(300,100);
				gameframe1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);				
				gameframe1.setSize(815,880);
			}
		});
	}
 }
