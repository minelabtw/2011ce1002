package fp.s100502025;

import java.awt.*;
import javax.swing.*;

public class Snake extends JPanel{
	private int startGame = 0; 
	private int length = 3;
	private int width;
	private int height;
	private int Direction = 1;
	private int xeatlocation;
	private int yeatlocation;
	private int MushroomXlocation = 300;
	private int MushroomYlocation = 300;
	private int tempX;
	private int tempY;
	private int score = 0;
	private int time = 0;
	private int delay = 150;
	private int a ;
	private boolean start = true;
	private boolean poison = false;
	private boolean GameoverOrNot = false;
	private int[] xlocation = new int[100]; 
	private int[] ylocation = new int[100];
	private ImageIcon star = new ImageIcon("image/star.png");
	private ImageIcon SnakeHeadUp = new ImageIcon("image/bluesnakeheadup.png");
	private ImageIcon SnakeHeadDown = new ImageIcon("image/bluesnakeheaddown.png");
	private ImageIcon SnakeHeadLeft = new ImageIcon("image/bluesnakeheadleft.png");
	private ImageIcon SnakeHeadRight = new ImageIcon("image/bluesnakeheadright.png");
	private ImageIcon Mushroom = new ImageIcon("image/food.png");
	private ImageIcon Poison = new ImageIcon("image/Poison.png");
	private ImageIcon SnakeBody = new ImageIcon("image/bluesnakebody.png");
	private ImageIcon PoisonSnakeHeadUp = new ImageIcon("image/yellowsnakeheadup.png");
	private ImageIcon PoisonSnakeHeadDown = new ImageIcon("image/yellowsnakeheaddown.png");
	private ImageIcon PoisonSnakeHeadLeft = new ImageIcon("image/yellowsnakeheadleft.png");
	private ImageIcon PoisonSnakeHeadRight = new ImageIcon("image/yellowsnakeheadright.png");
	private ImageIcon PoisonSnakeBody = new ImageIcon("image/yellowsnakebody.png");
	
	public Snake() {  //遊戲畫面
		setSize(800,800);		
		width = getWidth();		
		height = getHeight();
		for(int i = 0 ; i < length ; i++ ) {  //蛇的初始位置
			xlocation[i] = getWidth() / 20 * 16;
			ylocation[i] = getHeight() / 20 * (i + 15);
		}
	}
	
	public void setDirection(int direction) {  //蛇的方向
		Direction = direction;
	}
	
	public int getDirection() {
		return Direction;
	}
	
	public int Won() {  //蛇的長度
		return length;
	}
	
	public int setXeatLocation() {  //星星的X位置
		int xeat = (int)(Math.random() * 20);
		boolean Xsame = true;
		int x = 0;
		while(Xsame == true) {  //判斷是否有跟甚麼身體重複到
			if(xlocation[x] == width / 20 * xeat) {
				xeat = (int)(Math.random() * 20);
				x = 0;
			}
			else {
				if(x == length -1) {
					Xsame = false;
				}
				else {
					x = x + 1 ;
				}
			}
		}
		return width / 20 * xeat;
	}
	
	public int setYeatlocation() {  //星星的Y位置
		int yeat = (int)(Math.random() * 20);
		boolean Ysame = true;
		int y = 0;
		while(Ysame == true) {  //判斷是否有跟甚麼身體重複到
			if(ylocation[y] == height / 20 * yeat) {
				yeat = (int)(Math.random() * 20);
				y = 0;
			}
			else {
				if(y == length -1) {
					Ysame = false;
				}
				else {
					y = y + 1 ;
				}
			}
		}
		return height / 20 * yeat;
	}
	
	public void Bonus() {  //道具的位置
		int xeat = (int)(Math.random() * 20);
		int yeat = (int)(Math.random() * 20);
		boolean Xsame = true;
		boolean Ysame = true;
		int i = 0;

		while(Xsame == true && Ysame == true) {	  //不能跟蛇的身體重複，也不能跟星星位置重複到
			if(xeatlocation == width / 20 * xeat && yeatlocation == height / 20 * yeat) {
				xeat = (int)(Math.random() * 20);
				yeat = (int)(Math.random() * 20);
			}
			else {
				if(xlocation[i] == width / 20 * xeat && ylocation[i] == height / 20 * yeat) {
					xeat = (int)(Math.random() * 20);
					yeat = (int)(Math.random() * 20);
					i = 0;
				}
				else {
					if(i == length -1) {
						Xsame = false;
						Ysame = false;
					}
					else {
						i = i + 1;
					}
				}
			}		
		}
		MushroomXlocation = width / 20 * xeat;
		MushroomYlocation = height / 20 * yeat;
	}
	
	public boolean GameOver() {  //是否結束遊戲
			return GameoverOrNot;	
		
	}
	
	public String Score() {  //分數
		int Score = ( score ) * 100;
		if(Score < 0) {
			score = 0;
			Score = 0;
		}
		String S = Integer.toString(Score);
		return S;
	}
	
	public String Length() {  //長度
		String L = Integer.toString(length);
		return L;
	}
	
	public int Delay() {  //設定速度
		return delay;
	}
	
	protected void paintComponent(Graphics g) {  //畫蛇
		super.paintComponent(g);
		
		time = time + 1;  
		
		if(time == 0 ) {
			delay = 100;
		}
		
		if(time == 2 ) {
			poison = false;
		}
		
		if(time == 20 ) {  //道具的位置，還有出現的時間
			this.Bonus();	
			a = (int)(Math.random() * 100 + 1);
		}
		
		if(time >= 50) {  //當時間大於50，出現道具，吃完道具後，時間從0開始算	
			if(a % 5 == 0) {  //第一種道具，變長三個
				g.drawImage(Mushroom.getImage(), MushroomXlocation, MushroomYlocation, width / 20, height/ 20, this);
				if(MushroomXlocation == xlocation[0] && MushroomYlocation == ylocation[0] ) {
					length = length + 3;
					if(xlocation[length - 5 ] > xlocation[length - 4 ] && ylocation[ length - 5 ] == ylocation[length - 4 ]) {
						for(int i = length - 3 ; i < length ; i++ ) {
							xlocation[i] = xlocation[i - 1] - width / 20;
							ylocation[i] = ylocation[i - 1];
						}
					}
					
					if(xlocation[length - 5 ] < xlocation[length - 4 ] && ylocation[ length - 5 ] == ylocation[length - 4 ]) {
						for(int i = length - 3 ; i < length ; i++ ) {
							xlocation[i] = xlocation[i - 1] + width / 20;
							ylocation[i] = ylocation[i - 1];
						}
					}
					
					if(xlocation[length - 5 ] == xlocation[length - 4 ] && ylocation[ length - 5 ] > ylocation[length - 4 ]) {
						for(int i = length - 3 ; i < length ; i++ ) {
							xlocation[i] = xlocation[i - 1] ;
							ylocation[i] = ylocation[i - 1] - height / 20;
						}
					}
					
					if(xlocation[length - 5 ] == xlocation[length - 4 ] && ylocation[ length - 5 ] < ylocation[length - 4 ]) {
						for(int i = length - 3 ; i < length ; i++ ) {
							xlocation[i] = xlocation[i - 1] ;
							ylocation[i] = ylocation[i - 1] + height / 20;
						}	
					}
					score = score + 3;
					time = 0;
					
				}
			}
			
			if(a % 5 == 1) {  //第二種道具，變短三個
				g.drawImage(Mushroom.getImage(), MushroomXlocation, MushroomYlocation, width / 20, height/ 20, this);
				if(MushroomXlocation == xlocation[0] && MushroomYlocation == ylocation[0] ) {
					if(length >= 4 ) {
						length = length - 3;
					}
					else {
						length = 1; 						
					}
					score = score - 3;
					time = 0;
				}
			}
			
			if(a % 5 == 2 ) {  //第三種道具，加速
				g.drawImage(Mushroom.getImage(), MushroomXlocation, MushroomYlocation, width / 20, height/ 20, this);
				if(MushroomXlocation == xlocation[0] && MushroomYlocation == ylocation[0] ) {				
					delay = 50;
					time = -300;
				}				
			}
			
			if(a % 5 == 3 ) { 
				g.drawImage(Mushroom.getImage(), MushroomXlocation, MushroomYlocation, width / 20, height/ 20, this);
				if(MushroomXlocation == xlocation[0] && MushroomYlocation == ylocation[0] ) {				
					delay = 50;
					time = -300;
				}
			}
			
			if(a % 5 == 4 ) {  //第四種道具，中毒，速度會變慢
				g.drawImage(Poison.getImage(), MushroomXlocation, MushroomYlocation, width / 20, height/ 20, this);
				if(MushroomXlocation == xlocation[0] && MushroomYlocation == ylocation[0] ) {
					poison = true;
					delay = 1000;
					time = -8;
				}
			}
			
			if(time == 125) {  //如果都沒吃到，時間到125，會自己消失
				time = 0;
			}
		}
		
		if(start == true) {  //開始，第一顆星星
			xeatlocation = this.setXeatLocation();
			yeatlocation = this.setYeatlocation();
			start = false;
		}
		
		if(xeatlocation == xlocation[0] && yeatlocation == ylocation[0]) {  //畫蛇
			length = length + 1;
			xeatlocation = this.setXeatLocation();
			yeatlocation = this.setYeatlocation();
			
			if(delay == 50) {  //加速的時候吃到星星，每吃到一顆有500分
				score = score + 5;
			}
			
			if(delay == 100 ) {  //平常吃到星星，每吃到一顆有100分
				score = score + 1;
			}
			
			if(delay == 1000 ) {  //中讀吃到星星，每吃到一顆沒有分
				score = score ;
			}			
		}
		
		g.drawLine(0, 0, width, 0);  //畫最上面的邊線
		
		if(xlocation[0] == 0 && Direction == 3 ) {  //是否撞到牆
			GameoverOrNot = true;
		}
		
		if(xlocation[0] == width / 20 * 19 && Direction == 4 ) {  //是否撞到牆
			GameoverOrNot = true;
		}
		
		if(ylocation[0] == height / 20 * 19 && Direction == 2 ) {  //是否撞到牆
			GameoverOrNot = true;
		}
		
		if(ylocation[0] == 0 && Direction == 1 ) {  //是否撞到牆
			GameoverOrNot = true;
		}
		
		if( GameoverOrNot == false ) {  //沒撞到牆
			if(Direction == 1) {  //上	
				tempX = xlocation[0];
				tempY = ylocation[0] - height / 20;
				for(int j = 1 ; j < length -1 ; j++ ) {  //判斷是否撞到自己的身體
					if(tempX == xlocation[j] && tempY == ylocation[j]) {
						GameoverOrNot = true;
						break;
					}
				}	
				
				if(GameoverOrNot == false) {
					for(int i = length - 1 ; i > 0 ; i-- ) {
						xlocation[i] = xlocation[i-1];
					}
					
					for(int i = length -1 ; i > 0 ; i-- ) {
						ylocation[i] = ylocation[i-1];
					}
					
					ylocation[0] = ylocation[0] - height / 20;	
				}									
			}
			
			if(Direction == 2) {  //下
				tempX = xlocation[0];
				tempY = ylocation[0] + height / 20;
				for(int j = 1 ; j < length -1 ; j++ ) {
					if(tempX == xlocation[j] && tempY == ylocation[j]) {
						GameoverOrNot = true;
						break;
					}
				}
				
				if(GameoverOrNot == false) {
					for(int i = length - 1 ; i > 0 ; i-- ) {
						xlocation[i] = xlocation[i-1];
					}	
					
					for(int i = length -1 ; i > 0 ; i-- ) {
						ylocation[i] = ylocation[i-1];
					}
					
					ylocation[0] = ylocation[0] + height / 20;
				}										
			}
			
			if(Direction == 3) {  //左	
				tempX = xlocation[0] - width / 20;
				tempY = ylocation[0];
				for(int j = 1 ; j < length -1 ; j++ ) {
					if(tempX == xlocation[j] && tempY == ylocation[j]) {
						GameoverOrNot = true;
						break;
					}
				}
				
				if(GameoverOrNot == false) {
					for(int i = length - 1 ; i > 0 ; i-- ) {
						xlocation[i] = xlocation[i-1];
					}
					
					for(int i = length -1 ; i > 0 ; i-- ) {
						ylocation[i] = ylocation[i-1];
					}
					
					xlocation[0] = xlocation[0] - width / 20;										
				}			
			}
			
			if(Direction == 4) {  //右
				tempX = xlocation[0] + width / 20;
				tempY = ylocation[0];
				for(int j = 1 ; j < length -1 ; j++ ) {
					if(tempX == xlocation[j] && tempY == ylocation[j]) {
						GameoverOrNot = true;
						break;
					}
				}
				
				if(GameoverOrNot == false) {
					for(int i = length - 1 ; i > 0 ; i-- ) {
						xlocation[i] = xlocation[i-1];
					}
					
					for(int i = length -1 ; i > 0 ; i-- ) {
						ylocation[i] = ylocation[i-1];
					}
					
					xlocation[0] = xlocation[0] + width / 20;
				}										
			}	
		}
		
		if(poison == false ) {  //畫沒有中毒的蛇
			if(Direction == 1) {
				g.drawImage(SnakeHeadUp.getImage(), xlocation[0], ylocation[0], width / 20, height/ 20, this);
			}
			
			if(Direction == 2) {
				g.drawImage(SnakeHeadDown.getImage(), xlocation[0], ylocation[0], width / 20, height/ 20, this);
			}
			
			if(Direction == 3) {
				g.drawImage(SnakeHeadLeft.getImage(), xlocation[0], ylocation[0], width / 20, height/ 20, this);
			}
			
			if(Direction == 4) {
				g.drawImage(SnakeHeadRight.getImage(), xlocation[0], ylocation[0], width / 20, height/ 20, this);
			}
				
			for(int i = 1 ; i < length ; i++) {  //畫蛇
				g.drawImage(SnakeBody.getImage(), xlocation[i], ylocation[i], width / 20, height/ 20, this);
			}			
		}
		
		if(poison == true ) {  //畫中毒的蛇
			if(Direction == 1) {
				g.drawImage(PoisonSnakeHeadUp.getImage(), xlocation[0], ylocation[0], width / 20, height/ 20, this);
			}
			
			if(Direction == 2) {
				g.drawImage(PoisonSnakeHeadDown.getImage(), xlocation[0], ylocation[0], width / 20, height/ 20, this);
			}
			
			if(Direction == 3) {
				g.drawImage(PoisonSnakeHeadLeft.getImage(), xlocation[0], ylocation[0], width / 20, height/ 20, this);
			}
			
			if(Direction == 4) {
				g.drawImage(PoisonSnakeHeadRight.getImage(), xlocation[0], ylocation[0], width / 20, height/ 20, this);
			}
				
			for(int i = 1 ; i < length ; i++) {  //畫蛇
				g.drawImage(PoisonSnakeBody.getImage(), xlocation[i], ylocation[i], width / 20, height/ 20, this);
			}
		}
		g.drawImage(star.getImage(), xeatlocation, yeatlocation, width / 20, height/ 20, this);//畫星星						
	}
}