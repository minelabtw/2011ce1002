package fp.s100502025;

import javax.swing.*;
import java.awt.*;

public class FinalProject extends JFrame {  
	public static void main(String[] args) {
		FrameWork1 frame1 = new FrameWork1();  //第一個畫面視窗的初始值設定
		frame1.setTitle("Snake");
		frame1.pack();
		frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame1.setLocation(300,100);
		frame1.setVisible(true);
	}
	
}
