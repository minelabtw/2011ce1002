package fp.s100502025;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class Win extends JFrame {  //獲勝的畫面
	private JLabel jlb = new JLabel("       YOU WIN");
	private JButton jbtTry = new JButton("Try Again?");  //按鈕:再試一次
	private JButton jbtExit = new JButton("Exit");  //按鈕:離開
	private JPanel p1 = new JPanel();	
	public Win() {
		JPanel p2 = new JPanel();
		p2.setLayout(new BorderLayout());		
		p2.setBackground(Color.BLACK);
		p2.add(jlb, BorderLayout.CENTER);
		Font font1 = new Font("Serif",Font.BOLD,100);
		Font font2 = new Font("Serif",Font.BOLD,30);
		jlb.setFont(font1);
		jlb.setForeground(Color.RED);
		jlb.setBackground(Color.BLACK);
		p1.setLayout(new GridLayout(1,2));
		jbtTry.setFont(font2);
		jbtExit.setFont(font2);
		p1.add(jbtTry);
		p1.add(jbtExit);
		setLayout(new BorderLayout());		
		add(p2,BorderLayout.CENTER);
		add(p1,BorderLayout.SOUTH);
		
		jbtTry.addActionListener(new ActionListener() {  //按鈕:再試一次，會再回到遊戲畫面
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				GameFrame gameframe2 = new GameFrame();
				gameframe2.setTitle("Snake");
				gameframe2.setVisible(true);
				gameframe2.setLocation(300,100);
				gameframe2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				gameframe2.setSize(815,880);
			}
		});
		
		jbtExit.addActionListener(new ActionListener() {  //按鈕:離開
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});		
	}
}
