package fp.s100502025;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;


public class GameFrame extends JFrame {  //遊戲畫面視窗
	private Snake snake = new Snake();  //畫蛇的圖
	private GameOverFrame gameover = new GameOverFrame();  //結束遊戲的視窗
	private Win win = new Win();  //贏得遊戲的視窗
	
	private Timer timer2 = new Timer(150 , new TimerListener2());	
	private JPanel p1 = new JPanel();
	private JPanel p3 = new JPanel();
	private JPanel p4 = new JPanel();
	private JLabel jblScore = new JLabel("0");  //計分
	private JLabel jlbLength = new JLabel("3");  //計長度
	private JLabel jlbscore = new JLabel("SCORE:");
	private ImageIcon SnakeHead = new ImageIcon("image/bluesnakeheadup.png");
		
	public GameFrame() {
		p1.setLayout(new GridLayout(1,2));  //排版
		Font font1 = new Font("Serif",Font.BOLD,25);
		
		JPanel p2 = new JPanel() {  //這是遊戲畫面左上角計長度的面板
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
					g.drawImage(SnakeHead.getImage(), 0, 0, 35, 35, this);
			}
		};
		
		p1.add(p2);
		jlbLength.setFont(font1);  //設定字型
		jblScore.setFont(font1);
		p1.add(jlbLength);
		
		p3.setLayout(new GridLayout(1,2));  //遊戲畫面右上角的板子
		p3.add(jlbscore);
		p3.add(jblScore);
		
		p4.setLayout(new GridLayout(1,2));  //再將計長度的板子&計分的板子家道遊戲中
		p4.add(p1);
		p4.add(p3);
		setLayout(new BorderLayout());
		snake.setBackground(Color.WHITE);
		add(p4,BorderLayout.NORTH);
		add(snake,BorderLayout.CENTER);	
		
		addKeyListener(new KeyAdapter() {  //使得貪食蛇可以用鍵盤控制方向
			public void keyPressed(KeyEvent e) {
				timer2.start();  //開始移動
				switch (e.getKeyCode()) {					
					case KeyEvent.VK_UP:
						if(snake.getDirection() == 2) {  //當蛇頭方向向下
							snake.setDirection(2);	//就算是按了向上的鍵也不會有反應，會繼續向下
						}
						else {
							snake.setDirection(1);  //向上走
						}	
						break;
						
					case KeyEvent.VK_DOWN:
						if(snake.getDirection() == 1) {  //當蛇頭方向向上
							snake.setDirection(1);	//就算是按了向下的鍵也不會有反應，會繼續向上	
						}
						else {
							snake.setDirection(2);  //向下走	
						}	
						break;
					
					case KeyEvent.VK_LEFT:
						if(snake.getDirection() == 4) {  //當蛇頭方向向右
							snake.setDirection(4);	//就算是按了向左的鍵也不會有反應，會繼續向右	
						}
						else {
							snake.setDirection(3);  //向左走		
						}	
						break;
						
					case KeyEvent.VK_RIGHT:
						if(snake.getDirection() == 3) {  //當蛇頭方向向左
							snake.setDirection(3);	//就算是按了向右的鍵也不會有反應，會繼續向左	
						}
						else {
							snake.setDirection(4);  //向右走		
						}	
						break;
					default:  //其他
						break;
				}
			}
		});	
	}
	
	class TimerListener2 implements ActionListener {  //每0.15秒刷新一次
		public void actionPerformed(ActionEvent e) {
			if(snake.Won() == 20 ) {  //當蛇長度大於20，贏了遊戲，叫出獲勝的畫面
				timer2.stop();  //停止刷新畫面
				setVisible(false);  //讓遊戲畫面隱藏，出現獲勝畫面
				win.setTitle("Snake");  //獲勝畫面的初始值
				win.setSize(815,880);
				win.setVisible(true);
				win.setLocation(300,100);
				win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			}
			
			if(snake.GameOver() == true) {  //當蛇頭撞到牆壁或是撞到自己，結束遊戲
				timer2.stop();  //停止刷新畫面
				setVisible(false);  //讓遊戲畫面隱藏，出現結束畫面
				gameover.setTitle("Snake");  //結束畫面的初始值
				gameover.setSize(815,880);
				gameover.setVisible(true);
				gameover.setLocation(300,100);
				gameover.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			}
			
			repaint();  //刷新圖案
			
			timer2.setDelay(snake.Delay());  //設定刷新速度
			jlbLength.setText(snake.Length());  //設定左上角的長度數值
			jblScore.setText(snake.Score());  //設定分數值
		}	
	}	
}