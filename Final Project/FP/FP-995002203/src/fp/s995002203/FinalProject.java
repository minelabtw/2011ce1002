package fp.s995002203;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;


public class FinalProject extends JFrame {
	private JMenuBar jmb=new JMenuBar();
	private JMenu menu=new JMenu("Menu");
	private JMenu menu2=new JMenu("Setting");
	private JMenu bgcolor=new JMenu("Background color");
	private JMenuItem stop, newGame, exit, resume, blue, green, pink;
	private boolean beforeStateIsStop=false;
	private JButton startGame=new JButton("START"); 
	private JButton exitGame=new JButton("EXIT ");
	private JLabel gameName=new JLabel("GAME");
	private JTextField userName=new JTextField(10);
	private JPanel indexPanel=new JPanel(new GridLayout(4, 1, 5, 5));
	private JPanel namePanel=new JPanel();
	private JPanel gamePanel=new JPanel();
	private JPanel lvPanel=new JPanel();
	private JPanel gameOverPanel=new JPanel(new BorderLayout());
	private JPanel msgPanel=new JPanel();
	private JPanel congratePanel=new JPanel();
	
	private ActionListener acListener=new acListener();
	private ActionListener btListener=new btListener();

	
	private Color[] wordColor={Color.blue, Color.red, Color.yellow, Color.green, Color.black, Color.white, Color.orange, Color.pink};
	private String[] word={"blue","red", "yellow", "green", "black", "white","orange", "pink"};
	private JButton[] colorButton;
	private JPanel buttonPanel=new JPanel(new FlowLayout());

	private JPanel wordPanel=new JPanel();
	private JLabel wordLabel=new JLabel("GAME");
	
	private JPanel infoPanel=new JPanel(new GridLayout(1, 2));
	private JLabel scoreLabel=new JLabel();
	private JLabel lvLabel=new JLabel();
	private JLabel timeLabel=new JLabel();
	private JLabel wrongLabel1=new JLabel();
	private JLabel wrongLabel2=new JLabel();
	private JLabel wrongLabel3=new JLabel();
	private JLabel countDownLabel=new JLabel();
	
	private int ans=0, ansIndex, exist=0, count=1;
	private int[] existRandNum=new int[8];
	private dazzle player;

	Timer timer = new Timer(1000, new TimerListener());
	Timer delayer = new Timer(1000, new DelayerListener());
	Timer countDowner = new Timer(1000, new CountDownListener());
	
	private int time=10;
	private int tmpTotaltime=0;
	private int delay=3;
	private int countDown=3;
	private int bgColorChoice=0;
	
	private ImageIcon icon =new ImageIcon("D:\\eclipse_work_place\\fp\\src\\winer.jpg");
///////////////////////////////////////////////////////////////////////////////////////////////////////
	public FinalProject(){
		setJMenuBar(jmb);
		
		menu.setMnemonic('M');
		menu2.setMnemonic('S');
		bgcolor.setMnemonic('B');
		jmb.add(menu);
		jmb.add(menu2);
		
		menu.add(stop=new JMenuItem("Stop", 'S'));
		menu.add(resume=new JMenuItem("Resume",'R'));
		menu.add(newGame=new JMenuItem("New game", 'N'));
		menu.add(exit=new JMenuItem("Exit", 'E'));
		
		menu2.add(bgcolor);
		bgcolor.add(blue=new JMenuItem("Blue", 'B'));
		bgcolor.add(green=new JMenuItem("Green", 'G'));
		bgcolor.add(pink=new JMenuItem("Pink", 'P'));
		
		
		stop.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE,0));
		resume.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
		newGame.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
		exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));
		
		newGame.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				beforeStateIsStop=false;
				switch(getPanel()){
					case 1://index panel
						break;//do nothing
						
					case 2://name panel
						remove(namePanel);
						setIndexPanel();
						repaint();
		                setVisible(true);
						break;
						
					case 3://game panel
						timer.stop();
						remove(gamePanel);
						setIndexPanel();
						repaint();
		                setVisible(true);
						break;
						
					case 4://lv panel
						countDowner.stop();
						countDownReset();
						remove(lvPanel);
						setIndexPanel();
						repaint();
		                setVisible(true);
						break;
						
					case 5://game over panel
						delayer.stop();
						delayReset();
						remove(gameOverPanel);
						setIndexPanel();
						repaint();
		                setVisible(true);
						break;
						
					case 6://msg panel
						delayer.stop();
						delayReset();
						remove(msgPanel);
						setIndexPanel();
						repaint();
		                setVisible(true);
						break;
						
					case 7://congrate panel
						delayer.stop();
						delayReset();
						remove(congratePanel);
						setIndexPanel();
						repaint();
		                setVisible(true);
						break;
				}	
			}
		});
		
		exit.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				System.exit(1);
			}
		});
		
		resume.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(beforeStateIsStop==true){
					switch(getPanel()){
						case 3://game panel
							timer.start();
							break;
						case 4://lv panel
							countDowner.start();
							break;
						case 5://game over panel
							delayer.start();
							break;
						case 6://msg panel
							delayer.start();
							break;
						case 7://congrate panel
							delayer.start();
							break;
					}
					beforeStateIsStop=false;
				}
			}
		});
		
		stop.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				beforeStateIsStop=true;
				switch(getPanel()){
					case 3://game panel
						timer.stop();
						break;
					case 4://lv panel
						countDowner.stop();
						break;
					case 5://game over panel
						delayer.stop();
						break;
					case 6://msg panel
						delayer.stop();
						break;
					case 7://congrate panel
						delayer.stop();
						break;
				}
			}
		});
		
		blue.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				bgColorChoice=1;
				setBg(1);
			}
		});
		
		green.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				bgColorChoice=2;
				setBg(2);
			}
		});
		
		pink.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				bgColorChoice=3;
				setBg(3);
			}
		});
		
		startGame.addActionListener(acListener);
		exitGame.addActionListener(acListener);
		setIndexPanel();
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////
	public int getPanel(){
		return player.location;
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////
	public void timeReset(){
		switch(player.level){
			case 1:
				time=10;
				break;
			case 2:
				time=9;
				break;
			case 3:
				time=8;
				break;
			case 4:
				time=7;
				break;
			case 5:
				time=6;
				break;
		}
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////
	public void delayReset(){
		delay=3;
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////
	public void countDownReset(){
		countDown=3;
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////
	public void countReset(){
		count=0;
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////
	public int rand(int level){
		switch(level){
			case 1:
				return (int)(Math.random()*4);
			case 2:
				return (int)(Math.random()*5);
			case 3:
				return (int)(Math.random()*6);
			case 4:
				return (int)(Math.random()*7);
			case 5:
				return (int)(Math.random()*8);
		}
		return (int)(Math.random()*4);//default
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////
	public int randNumNotExist(int level){
		int ranNum=(int)(Math.random()*(level+3));
		boolean flag=false;
		while(true){
			for(int i=0;i<exist;i++){
				if(ranNum==existRandNum[i]){//數字出現過了
					flag=true;
					break;
				}	
			}
			if(flag==false){//尚未出現過的數字
				//existRandNum[exist++]=ranNum;
				return ranNum;
			}
			else{
				ranNum=(int)(Math.random()*(level+3));//重新產生數字
				flag=false;
			}
		}
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////
	public void setBg(int choice){
		switch(choice){
			case 1:
				indexPanel.setBackground(new Color(187,255,255));
				namePanel.setBackground(new Color(187,255,255));
				gamePanel.setBackground(new Color(187,255,255));
				lvPanel.setBackground(new Color(187,255,255));
				msgPanel.setBackground(new Color(187,255,255));
				gameOverPanel.setBackground(new Color(187,255,255));
				break;
				
			case 2:
				indexPanel.setBackground(new Color(127,255,0));
				namePanel.setBackground(new Color(127,255,0));
				gamePanel.setBackground(new Color(127,255,0));
				lvPanel.setBackground(new Color(127,255,0));
				msgPanel.setBackground(new Color(127,255,0));
				gameOverPanel.setBackground(new Color(127,255,0));
				break;
				
			case 3:
				indexPanel.setBackground(new Color(255,193,193));
				namePanel.setBackground(new Color(255,193,193));
				gamePanel.setBackground(new Color(255,193,193));
				lvPanel.setBackground(new Color(255,193,193));
				msgPanel.setBackground(new Color(255,193,193));
				gameOverPanel.setBackground(new Color(255,193,193));
				break;
		}
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////
	public void setIndexPanel(){//設置首頁
		setBg(bgColorChoice);
		player=new dazzle();//新使用者
		timeReset();
		countReset();
		gameName.setForeground(Color.black);
		gameName.setFont(new Font("Arial",Font.BOLD, 25));
		indexPanel.setLayout(new FlowLayout(FlowLayout.CENTER,10,100));
		
		indexPanel.add(gameName);
		indexPanel.add(startGame);
		indexPanel.add(exitGame);
		player.location=1;
		timeReset();//
		add(indexPanel);
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////
	public void setNamePanel(){//設置使用者輸入姓名畫面
		namePanel=new JPanel(new GridLayout(1,2,5,5));
		setBg(bgColorChoice);
		userName=new JTextField(10);//初始化
		JLabel userNameLabel=new JLabel("your name is ");
		userNameLabel.setForeground(Color.black);
		userNameLabel.setFont(new Font("Arial",Font.BOLD, 20));
		userName.setFont(new Font("Arial",Font.BOLD, 20));
		userName.addActionListener(acListener);
		namePanel.add(userNameLabel);
		namePanel.add(userName);
		namePanel.setLayout(new FlowLayout(FlowLayout.CENTER,10,100));
		player.location=2;
		add(namePanel);
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////
	public void setGamePanel(int level){//設置遊戲畫面
		
		int colorNumber=0;
		gamePanel=new JPanel(new BorderLayout());
		infoPanel=new JPanel(new GridLayout(1, 4, 5, 5));
		wordPanel=new JPanel(new GridLayout(1, 2, 5, 5));
		exist=0;
		setBg(bgColorChoice);
		
		switch(level){
			case 1:
				ans=rand(1);//選擇正解顏色
				ansIndex=rand(1);//隨機選擇正解要在哪個位置
				buttonPanel=new JPanel(new GridLayout(2,2,5,5));
				colorNumber=4;
				colorButton=new JButton[4];
				break;
				
			case 2:
				ans=rand(2);//選擇正解顏色
				ansIndex=rand(2);//隨機選擇正解要在哪個位置
				buttonPanel=new JPanel(new GridLayout(2,3,5,5));
				colorNumber=5;
				colorButton=new JButton[5];
				break;
				
			case 3:
				ans=rand(3);//選擇正解顏色
				ansIndex=rand(3);//隨機選擇正解要在哪個位置
				buttonPanel=new JPanel(new GridLayout(2,3,5,5));
				colorNumber=6;
				colorButton=new JButton[6];
				break;
				
			case 4:
				ans=rand(4);//選擇正解顏色
				ansIndex=rand(4);//隨機選擇正解要在哪個位置
				buttonPanel=new JPanel(new GridLayout(2,4,5,5));
				colorNumber=7;
				colorButton=new JButton[7];
				break;
				
			case 5:
				ans=rand(5);//選擇正解顏色
				ansIndex=rand(5);//隨機選擇正解要在哪個位置
				buttonPanel=new JPanel(new GridLayout(2,4,5,5));
				colorNumber=8;
				colorButton=new JButton[8];
				break;
				
			default:
				break;
		}
		existRandNum[exist++]=ans;

		for(int i=0;i<colorNumber;i++){
			colorButton[i]=new JButton("      ");
			if(i==ansIndex){
				colorButton[i].setBackground(wordColor[ans]);	
			}
			else{
				existRandNum[exist]=randNumNotExist(level);//隨機選擇一個尚未出現的顏色
				colorButton[i].setBackground(wordColor[existRandNum[exist++]]);	
			}
			colorButton[i].addActionListener(btListener);
			colorButton[i].setPreferredSize(new Dimension(50,50));
			buttonPanel.add(colorButton[i]);
		}
		
		gamePanel.add(buttonPanel, BorderLayout.CENTER);
		JLabel wordNumLabel=new JLabel("No. "+Integer.toString(count+1)+":");
		wordNumLabel.setHorizontalAlignment(JLabel.RIGHT);
		wordNumLabel.setForeground(Color.gray);
		wordNumLabel.setFont(new Font("Arial",Font.BOLD, 20));
		wordLabel=new JLabel(word[ans]);
		wordLabel.setForeground(wordColor[rand(level)]);
		wordLabel.setFont(new Font("Arial",Font.BOLD, 25));
		wordPanel.setBorder(new TitledBorder("target"));
		wordPanel.add(wordNumLabel);
		wordPanel.add(wordLabel);
		gamePanel.add(wordPanel, BorderLayout.SOUTH);
		
		scoreLabel=new JLabel(Integer.toString(player.score));
		scoreLabel.setBorder(new TitledBorder("score"));
		timeLabel=new JLabel(time+" sec");
		timeLabel.setBorder(new TitledBorder("count down"));
		lvLabel=new JLabel(Integer.toString(player.level));
		lvLabel.setBorder(new TitledBorder("level"));
		scoreLabel.setFont(new Font("Arial",Font.BOLD, 25));
		timeLabel.setFont(new Font("Arial",Font.BOLD, 25));
		lvLabel.setFont(new Font("Arial",Font.BOLD, 25));
		
		JPanel wrongPanel=new JPanel(new GridLayout(1,3,5,5));
		wrongPanel.setBorder(new TitledBorder("wrong time"));
		wrongLabel1.setBorder(new LineBorder(Color.gray,2));
		wrongLabel2.setBorder(new LineBorder(Color.gray,2));
		wrongLabel3.setBorder(new LineBorder(Color.gray,2));
		if(player.wrong==1){
			wrongLabel1.setBackground(Color.gray);
			wrongLabel1.setOpaque(true);
		}
		else if(player.wrong==2){
			wrongLabel1.setBackground(Color.gray);
			wrongLabel1.setOpaque(true);
			wrongLabel2.setBackground(Color.gray);
			wrongLabel2.setOpaque(true);
		}
		else if(player.wrong==3){
			wrongLabel1.setBackground(Color.gray);
			wrongLabel1.setOpaque(true);
			wrongLabel2.setBackground(Color.gray);
			wrongLabel2.setOpaque(true);
			wrongLabel3.setBackground(Color.gray);
			wrongLabel3.setOpaque(true);
		}
		else{
			wrongLabel1.setOpaque(false);
			wrongLabel2.setOpaque(false);
			wrongLabel3.setOpaque(false);
		}
		
		wrongPanel.add(wrongLabel1);
		wrongPanel.add(wrongLabel2);
		wrongPanel.add(wrongLabel3);
		
		infoPanel.add(scoreLabel);
		infoPanel.add(lvLabel);
		infoPanel.add(wrongPanel);
		infoPanel.add(timeLabel);
		gamePanel.add(infoPanel, BorderLayout.NORTH);
		player.location=3;
		this.add(gamePanel);
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////
	public void setLvPanel(){//設置晉級畫面
		lvPanel.removeAll();
		setBg(bgColorChoice);
		JLabel lvLabel=new JLabel("Lv."+player.level);
		countDownLabel=new JLabel(Integer.toString(countDown));
		lvLabel.setForeground(Color.RED);
		lvLabel.setFont(new Font("Arial",Font.BOLD, 50));
		lvPanel.setLayout(new FlowLayout(FlowLayout.CENTER,10,100));
		lvPanel.add(lvLabel);
		lvPanel.add(countDownLabel);
		player.location=4;
		add(lvPanel);
}
///////////////////////////////////////////////////////////////////////////////////////////////////////
	public void setGameOverPanel(){//設置遊戲結束畫面
		setBg(bgColorChoice);
		JLabel gameOver=new JLabel("GAME OVER");
		gameOver.setForeground(Color.RED);
		gameOver.setFont(new Font("Arial",Font.BOLD, 30));
		gameOverPanel.setLayout(new FlowLayout(FlowLayout.CENTER,10,100));
		gameOverPanel.add(gameOver);
		player.location=5;
		add(gameOverPanel);
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////
	public void setMsgPanel(){//設置分數訊息畫面(用在遊戲結束且未破完全關時)
		msgPanel=new JPanel();
		setBg(bgColorChoice);
		JLabel line1=new JLabel(player.name+" 's record");
		JLabel line2=new JLabel("score :"+player.score);
		JLabel line3=new JLabel("total time:"+player.totalTime);
		JLabel line4=new JLabel("Level:"+player.level);
		JLabel line5=new JLabel("Keep trying!!! ");
		JPanel msg=new JPanel(new GridLayout(5,1,5,5));
		
		if(bgColorChoice==1){
			msg.setBackground(new Color(187,255,255));
		}
		else if(bgColorChoice==2){
			msg.setBackground(new Color(127,255,0));
		}
		else if(bgColorChoice==3){
			msg.setBackground(new Color(255,193,193));
		}
			
		
		line1.setHorizontalAlignment(JLabel.CENTER); 
		line2.setHorizontalAlignment(JLabel.CENTER); 
		line3.setHorizontalAlignment(JLabel.CENTER); 
		line4.setHorizontalAlignment(JLabel.CENTER); 
		line5.setHorizontalAlignment(JLabel.CENTER); 
		
		line1.setFont(new Font("Arial",Font.BOLD, 25));
		line2.setFont(new Font("Arial",Font.ITALIC, 25));
		line3.setFont(new Font("Arial",Font.ITALIC, 25));
		line4.setFont(new Font("Arial",Font.ITALIC, 25));
		line5.setFont(new Font("Arial",Font.BOLD, 25));
		
		line1.setForeground(Color.blue);
		line2.setForeground(Color.blue);
		line3.setForeground(Color.blue);
		line4.setForeground(Color.blue);
		line5.setForeground(Color.blue);
		
		msg.add(line1);
		msg.add(line2);
		msg.add(line3);
		msg.add(line4);
		msg.add(line5);
		
		msgPanel.setLayout(new FlowLayout(FlowLayout.CENTER,10,30));
		msgPanel.add(msg);
		delay=5;
		player.location=6;
		add(msgPanel);
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////255 255 0
	public void setCongratePanel(){//設置恭喜畫面(用在破完全關時)
		
		congratePanel=new JPanel();
		congratePanel.setBackground(new Color(187,255,255));
		JLabel line1=new JLabel("Congratulations! You win this game!");
		JLabel line2=new JLabel(player.name+" 's record");
		JLabel line3=new JLabel("score :"+player.score);
		JLabel line4=new JLabel("total time:"+player.totalTime);
		JLabel line5=new JLabel("level:"+player.level);
		JPanel msg=new JPanel(new GridLayout(5,1,5,5));
		msg.setBackground(new Color(187,255,255));
		
		line1.setHorizontalAlignment(JLabel.CENTER); 
		line2.setHorizontalAlignment(JLabel.CENTER); 
		line3.setHorizontalAlignment(JLabel.CENTER); 
		line4.setHorizontalAlignment(JLabel.CENTER); 
		line5.setHorizontalAlignment(JLabel.CENTER); 
		
		line1.setFont(new Font("Arial",Font.BOLD, 25));
		line2.setFont(new Font("Arial",Font.ITALIC, 25));
		line3.setFont(new Font("Arial",Font.ITALIC, 25));
		line4.setFont(new Font("Arial",Font.ITALIC, 25));
		line5.setFont(new Font("Arial",Font.ITALIC, 25));

		line1.setForeground(new Color(240,128,128));
		line2.setForeground(new Color(240,128,128));
		line3.setForeground(new Color(240,128,128));
		line4.setForeground(new Color(240,128,128));
		line5.setForeground(new Color(240,128,128));

		msg.add(line1);
		msg.add(line2);
		msg.add(line3);
		msg.add(line4);
		msg.add(line5);

		congratePanel.setLayout(new FlowLayout(FlowLayout.CENTER,10,30));
		congratePanel.add(msg);
		delay=5;
		player.location=7;
		add(congratePanel);
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////
	private class TimerListener implements ActionListener{ // 遊戲計時
		public void actionPerformed(ActionEvent e){
			time--;
			tmpTotaltime++;
			
			if(time==0){//遊戲結束			
				remove(gamePanel);
				if(player.level>1&&player.score>=0)
					setMsgPanel();
				else
					setGameOverPanel();	
				repaint();
                setVisible(true);
                timer.stop();
				delayer.start();
			}	
			else{
				infoPanel.remove(timeLabel);
				timeLabel=new JLabel(Integer.toString(time)+" sec");
				timeLabel.setBorder(new TitledBorder("count down"));
				timeLabel.setFont(new Font("Arial",Font.BOLD, 25));
				infoPanel.add(timeLabel);
				gamePanel.add(infoPanel, BorderLayout.NORTH);
				add(gamePanel);
				repaint();
				setVisible(true);
			}
		}
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////
	private class DelayerListener implements ActionListener{ //gameover後切換到首頁的延遲計時器
		public void actionPerformed(ActionEvent e){
			delay--;
			if(delay==0&&player.level!=5){
					remove(msgPanel);
					remove(gameOverPanel);
				setIndexPanel();
				repaint();
                setVisible(true);
				delayReset();
				delayer.stop();
			}
			else if(delay==0&&player.level==5){
				//congratePanel.removeAll();
				//congratePanel.add(new JLabel(icon));
				//add(congratePanel);
				remove(congratePanel);
				setIndexPanel();
				repaint();
                setVisible(true);
                delayReset();
				delayer.stop();
			}
		}
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////
	private class CountDownListener implements ActionListener{ //倒數計時器
		public void actionPerformed(ActionEvent e){
			if(countDown>0){
				countDown--;
				lvPanel.remove(countDownLabel);
				countDownLabel=new JLabel(Integer.toString(countDown));
				lvPanel.add(countDownLabel, BorderLayout.SOUTH);
				add(lvPanel);
				repaint();
				setVisible(true);
			}
			else{//倒數結束 下一級開始
				remove(lvPanel);
				countDownReset();
				countDowner.stop();
				setGamePanel(player.level);
				timer.start();
				repaint();
				setVisible(true);

			}
		}
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////
	class btListener implements ActionListener {//按下首頁(index)按鈕時要做的事
		public void actionPerformed(ActionEvent e) {
			if(e.getSource()==colorButton[ansIndex]){//選擇正確
				player.score+=100;

			}
			else{
				player.score-=100;
				player.wrong++;

			}
			
			if(player.wrong==4||player.score<0){//遊戲結束
				remove(gamePanel);
				
				if(player.level>1){
					if(player.score>=0)
						setMsgPanel();
					else
						setGameOverPanel();	
				}
				else if(player.level==1)
					setGameOverPanel();	

				repaint();
                setVisible(true);
                timer.stop();
				delayer.start();
				
			}	
			
			else{//遊戲中
				if(count==5){//晉級
					player.totalTime=tmpTotaltime;
					if(player.level==5){//破完最後一關(共五關)
						timer.stop();//關閉遊戲計時器
						remove(gamePanel);
						setCongratePanel();	
						repaint();
						setVisible(true);
						delayer.start();
					}
					else{
						player.level++;
						timeReset();
						countReset();//每關都要重新數X次
						timer.stop();//關閉遊戲計時器
						countDowner.start();//開啟晉級畫面倒數計時器
						remove(gamePanel);
						setLvPanel();	
						repaint();
						setVisible(true);
					}
				}
				else{
					count++;//關卡++
					remove(gamePanel);
					setGamePanel(player.level);	
					repaint();
					setVisible(true);
				}
			}	
		}
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////
	class acListener implements ActionListener {//按下首頁(index)按鈕時要做的事
		public void actionPerformed(ActionEvent e) {
			if(e.getSource()==startGame){
				remove(indexPanel);
				setNamePanel();
				repaint();
                setVisible(true);

			}
			else if(e.getSource()==exitGame)
				System.exit(1);
			else if(e.getSource()==userName){
				player.name=userName.getText();
				remove(namePanel);
				setGamePanel(player.level);
				repaint();
				setVisible(true);
				timer.start();
			}
		}
	}
///////////////////////////////////////////////////////////////////////////////////////////////////////
	public static void main(String[] args){
		FinalProject f=new FinalProject();
		f.setTitle("game");
		f.setSize(500,300);
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
