package a5.s100502023;

import java.util.*;

public class A5 
{

	public static void main(String[] args) 
	{
		boolean done=false;
		StackOfIntegers elements = new  StackOfIntegers();  //宣告一個StackOfIntegers class 的物件 elements
		
		elements.produce_PrimeNumbers();   //產生100個質數
		
		while(!done)
		{
			System.out.printf("\n已有 %d 個質數儲存在stack中  \n\n",elements.getPush_number());
			System.out.println("1.push");
			System.out.println("2.pop");
			System.out.println("3.Show all");
			System.out.println("4.exit");
			
			Scanner input= new Scanner(System.in);
			
			int choice=input.nextInt();  //使用者輸入的選項代碼
			int amount;
			
			switch (choice)
			{		
				case 1:
					System.out.print("amount:");
					amount=input.nextInt();
					elements.Push(amount);
					break;
				
				case 2:
					System.out.print("amount:");
					amount=input.nextInt();
					elements.Pop(amount);
					break;
				
				case 3:
					elements.showAll();
					break;
					
				case 4:
					done=true;
					break;
					
				default:
					System.out.print("請輸入1~4的選項，請重新操作\n");
					break;
			}
			
		}
		
		

	}

}
