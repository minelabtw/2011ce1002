package a5.s100502023;

import java.util.*;

public class StackOfIntegers 
{
	public static final int default_capacity=100;  //預設陣列大小100
	private int[] stack;  //stack
	private int[] primeNumber;  //儲存質數陣列
	
	private int push_number=0;  //儲存多少個質數到stack中
	
	
	public StackOfIntegers()  //initialize
	{
		this(default_capacity);
	}
	
	public StackOfIntegers(int capacity)  //initialize
	{
		stack= new int[capacity];
	}
	
	public void showAll()  //display
	{	
		
		System.out.println("The stack here is :");
		
		for (int i=0;i<stack.length;i++)
		{
			if (stack[i]==0)
			{
				break;
			}
			else
			{
				
				System.out.print(stack[i] + " ");
			}
		}
			
		System.out.print("\n");
		
		
	}
	
	public void Pop(int num)  //從stack取出
	{
		int amount=num;
		
		for (int i=0;i<amount;i++)
		{
			if(isEmpty()==true)
			{
				System.out.println("the stack is empty");
				push_number=0;
				break;
			}
			else
			{
				stack[push_number-1]=0;
				push_number--;
			}
				
		}
		
		showAll();
	}
	
	public void Push(int num)  //儲存到stack
	{
		int amount=num;  //使用者輸入的數量
		
		
		for (int i=0;i<amount;i++)
		{
			if(isFull()==true)
			{
				System.out.print("the stack is full!!\n");
				break;
			}
			else
			{
				stack[push_number]=primeNumber[push_number];
				push_number++;
			}
			
		}
				
		showAll();
		
	}
	
	public boolean isEmpty()  //check stack 是否清空
	{
		if (push_number==0)
		{
			return true;
		}
		else
		{
			return false;
		}
		
		
	}
	
	public boolean isFull()  //check stack 是否已滿
	{
		if (push_number==stack.length)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public int getPush_number()  //取push_number的值  
	{
		return push_number;
	}
	
	public void produce_PrimeNumbers()  //產生100個質數
	{
		primeNumber=new int[default_capacity];  //儲存100個質數
			
		int i=0;
		
		for (int number=2;i<primeNumber.length;number++)  //從2開始挑質數
		{
			if (check_PrimeNumber(number)==true)
			{
				primeNumber[i]=number;
				i++;
			}
		}
		
	}
	
	public boolean check_PrimeNumber(int number)  //判斷是否為質數
	{
		boolean primenumber = true;
		
		if (number==2)
		{
			primenumber=true;
		}
		else if (number==3)
		{
			primenumber=true;
		}
		
		for (int j=2;j<=Math.sqrt(number);j++)
		{
			
			if (number%j==0)  //非質數
			{
				primenumber=false;
				break;
			}
			
		}
		
		return primenumber;
	}
}
