package a5.s100502509;
import java.util.*;
public class A51 {
	public static void main(String[] args){
		System.out.println("1.push\n2.pop\n3.Show all\n4.exit");
		
		Scanner input = new Scanner(System.in);
		StackOfIntegers stack = new StackOfIntegers();//declare object stack
		for(;;)
		{
			System.out.println("\nPlease choose function you want to choose: ");
			int choose = input.nextInt();
			switch(choose)
			{
				case 1://function to push prime numbers into stack
					System.out.println("Please enter the numbers: ");
					int num1 = input.nextInt();
				
					if(stack.isFull(num1)==true)//whether the stack is full
					{
						stack.push(num1);//call function push to calculate primenumbers
						stack.showAll();
					}
					
					else
					{
						System.out.println("the stack is Full!!");
					}
					
					
					break;
				case 2://function to pop prime numbers into stack
					System.out.println("Please enter the numbers: ");
					int num2 = input.nextInt();
					if(stack.isEmpty(num2)==true)//whether the stack is Empty
					{
						stack.pop(num2);//call function pop to descard prime numbers
						stack.showAll();
					}
					
					else
					{
						System.out.println("the stack is Empty!!");
					}
					break;
				case 3://function to show prime numbers into stack
					stack.showAll();
					break;
				case 4://function to exit
					System.exit(0);
					break;
				default:
					System.out.println("Please choose again!!");
					break;
			}
		}
		
	}
}
