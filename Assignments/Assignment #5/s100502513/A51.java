package a5.s100502513;

import java.util.*;

public class A51 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		StackOfIntegers stack = new StackOfIntegers();  //建立新的物件STACK
		
		System.out.print("1.push\n2.pop\n3.Show all\n4.exit\n");  //清單
		for (;;) {
			System.out.print("\nPlease choose: ");
			int choose = input.nextInt();
			switch (choose) {
				case 1:  //PUSH
					System.out.print("push: ");
					int punumber = input.nextInt();
					stack.Push(punumber);
					break;
				case 2:  //POP
					System.out.print("pop: ");
					int ponumber = input.nextInt();							
					stack.Pop(ponumber);
					break;
				case 3:  //SHOWALL
					System.out.print("the stack here is :\n");
					stack.showAll();
					break;
				case 4: //EXIT
					System.out.print("Bye-Bye!");
					System.exit(0);
				default:  //ELSE CONDITION
					break;
			}
		}
	}
}
