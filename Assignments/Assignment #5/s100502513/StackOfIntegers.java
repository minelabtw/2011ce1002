package a5.s100502513;

public class StackOfIntegers {
	private int[] number = new int[100];  //存質數陣列
	private int[] stack = new int[100];  //空陣列
	private int size = 1;  //陣列大小
	private int current = 0;  //目前陣列項數

	StackOfIntegers() {  //產生100個質數存到number陣列
		number[0] = 2;
		for (int i = 3; i <= 1000; i += 2) {
			for (int j = 3; j <= i; j += 2) {
				if (i == j) {
					number[size] = i;
					size++;
					break;
				} 
				else if (i % j == 0 && j < i)
					break;
			}
			if (size == 100)
				break;
		}
	}

	public void Push(int num) {  
		current += num;
		for(int i=(current-num);i<current;i++){  //把number陣列的值給stack陣列
			stack[i]=number[i];
		}
		if (isFull()) {  //number陣列滿了
			System.out.print("the stack is full!!\n");
			stack = number;
			current = number.length;
		}
		else if (isEmpty()) {  //number陣列空了
			System.out.print("the stack is empty!!\n");
			
			current = current;
		} 
		else {
			current = current;
		}
	}

	public void Pop(int num) {  //減掉stack陣列的項數
		current -= num;
		if (isEmpty()) {
			System.out.print("the stack is empty!!\n");
			current = current;
		} 
		else if (isFull()) {
			System.out.print("the stack is full!!\n");
			current = number.length;
		} 
		else {
			current = current;
		}
	}

	public void showAll() {  //輸出陣列
		if (isEmpty()) {
			System.out.print("the stack is empty!!\n");
		} 
		else {
			for (int i = 0; i < current; i++) {
				System.out.print(stack[i] + " ");
			}
		}
	}

	public boolean isEmpty() {  //判斷number陣列是否空的
		if (current <= 0) {  //如果空的回傳true
			return true;
		} 
		else {
			return false;
		}
	}

	public boolean isFull() {  //判斷陣列是否滿了
		if (current >= number.length) {
			return true;  //滿了回傳true
		}
		else {
			return false;
		}
	}
}
