package a5.s100502001;

public class StackOfIntegers {
	private int[] Prime_number;//store 100 prime numbers
	private int[] stack;//for user input
	private int pop; //remove 
	private int push; //add
	private int cur_numOfstack; //the amount of current number
	public  StackOfIntegers(){ 
		cur_numOfstack=0;
		int times=0;
		int[] temp_Prime=new int[100];
		stack=new int[100];//initialize the stack numbers
		for(int i=2;times<100;i++){ //initialize the Prime number(100個)
			if(isPrime(i)){
				temp_Prime[times]=i;
				times++;
			}
		}
		Prime_number=temp_Prime;
	}
	public void showAll(){ //show the stack
		if(!isEmpty()){
			for(int i=0;i<cur_numOfstack;i++){
				if(stack[i]!=0){
					if(i%10==0)  //每行10個數字一數
						System.out.print("\n"+stack[i]+" ");
					else
						System.out.print(stack[i]+" ");
				}
			}
		}
		else
			System.out.println("The stack is null!!");
	}
	public void Pop(int numOfpop){ //remove the amounts of numOfpop from top of stack
		pop=numOfpop;
		cur_numOfstack-=pop;
		if(!isEmpty()){
				for(int i=push-1;i>=(push-pop);i--)
					stack[i]=0;
				for(int i=0;i<cur_numOfstack;i++)
					System.out.print(stack[i]+" ");
		}
		else
			System.out.println("The stack is empty.");
	}
	public void Push(int numOfpush){ //add the amounts of numOfpush to top of stack in order
		push=numOfpush;
		cur_numOfstack+=push;
		if(!isFull()){
			for(int i=0;i<cur_numOfstack;i++)
				stack[i]=Prime_number[i];
			showAll();
		}
		else
			System.out.println("The stack is full!!");
	}
	public boolean isEmpty(){ //whether the stack is empty
		for(int i=0;i<cur_numOfstack;i++){
			if(stack[i]!=0)
				return false;
		}
		return true;
	}
	public boolean isFull(){ //whether the stack is full
		for(int i=0;i<stack.length;i++){
			if(stack[i]==0)
				return false;
		}
		return true;
			
	}
	public boolean isPrime(int num){ //Prime_number 從大於3之後開始判斷
		for(int i=2;i<=Math.sqrt(num);i++){
			if(num%i==0) //若可以被1和自己本身已外的樹整除，則不為質數
				return false;	
		}
		return true;
	}
}
