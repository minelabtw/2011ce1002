package a5.s100502001;
import java.util.Scanner;
public class A51 {
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);
		System.out.println("1.push\n2.pop\n3.Show all\n4.exit");
		StackOfIntegers stack=new StackOfIntegers(); //declare the object of StackOfIntegers
		boolean judge=true;
		while(judge){
			int choice=input.nextInt();
			switch(choice){
				case 1:
					System.out.print("Push: "); //add numbers to stack
					int push=input.nextInt();
					stack.Push(push);
					break;
				case 2:
					System.out.print("Pop: ");//remove numbers from stack
					int pop=input.nextInt();
					stack.Pop(pop);
					break;
				case 3:
					System.out.println("The stack here is :"); //show the element of stack
					stack.showAll();
					break;
				case 4:
					System.out.println("See you next time!!");
					judge=false;
					break;
				default:
					System.out.println("There is not the choice.= =");
					break;
						
			}
		}
	}

}
