package a5.s100502018;

public class StackOfIntegers {
	private int[] items = new int[100];
	private int quantity = 0;
	private int testnumber = 1;

	public void Push(int x) { // Push the prime numbers into stack
		if (x < 0) {
			System.out.println("Something wrong, please try again.");
		} else if (x == 0) {
			System.out.println("Nothing happens.");
			showAll();
		} else {
			if (quantity + x > 100) {
				Push(100 - quantity);
				isFull();
			} else {
				int success = 0;
				int account = 0;
				while (success != x) { // set the prime numbers into stack
					int times = 0;
					for (account = 1; account <= testnumber; account++) { // get
																			// the
																			// prime
																			// numbers
						if (testnumber % account == 0) {
							times++;
						}
					}
					if (times == 2) {
						success++;
						items[quantity + success - 1] = testnumber;
					}
					testnumber++;
				}
				quantity += x;
				isFull();
				showAll();
			}
		}
	}

	public void Pop(int x) { // clean some element from the top of stack
		if (x < 0) {
			System.out.println("Something wrong, please try again.");
		} else if (x == 0) {
			System.out.println("Nothing happens.");
			showAll();
		} else {
			if (quantity - x < 0) {
				Pop(quantity);
				isEmpty();
			} else {
				testnumber = items[quantity - x]; // let the start of number be
													// the end of stack, make
													// sure it can push again
				for (int i = quantity - x; i < quantity; i++) {
					items[i] = 0;
				}
				quantity -= x;
				isEmpty();
				showAll();
			}
		}
	}

	public void showAll() { // show the stack
		if (quantity > 0) {
			for (int i = 0; i < quantity; i++) {
				System.out.print(items[i] + " ");
			}
			System.out.print("\n\n");
		}
	}

	public void isEmpty() { // check the stack is empty or not
		if (quantity == 0) {
			System.out.println("The stack is empty!!!");
		}
	}

	public void isFull() { // check the stack is full or not
		if (quantity == 100) {
			System.out.println("The stack is full!!!");
		}
	}
}