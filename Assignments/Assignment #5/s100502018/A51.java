package a5.s100502018;

import java.util.Scanner;

public class A51 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		StackOfIntegers myStack = new StackOfIntegers();
		int choice = 0;
		int quan = 0;
		while (choice != 4) {
			System.out
					.print("1. Push\n2. Pop\n3. Show all\n4. Exit\nPlease choose a service: "); //show the selection
			choice = input.nextInt();
			switch (choice) {
			case 1:
				System.out.print("Please input the quantity: ");
				quan = input.nextInt();
				myStack.Push(quan); //input the quantity
				break;
			case 2:
				System.out.print("Please input the quantity: ");
				quan = input.nextInt();
				myStack.Pop(quan); //input the quantity
				break;
			case 3:
				myStack.showAll(); 
				break;
			case 4:
				System.out.println("Thanks for your using, bye~");
				break;
			default:
				System.out.println("Something wrong, please try again.");
			}
		}
	}
}
