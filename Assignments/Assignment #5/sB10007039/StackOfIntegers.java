package a5.sB10007039;

public class StackOfIntegers {
	private int[] Stack = new int[100];
	private int StackCount = 0;
	
	public void push (int input) throws Exception{//Take input as argument, and pop such number of elements from the stack.
		if (StackCount <= 100) {
			Stack[StackCount] = input;
			StackCount++;
		}else {
			throw new Exception("Stack is full");
		}
	}
	
	public int pop() throws Exception{//Tack a Int from Stack and return
		int ReturnNum;
		if ((StackCount-1) >= 0) {
			ReturnNum = Stack[StackCount-1];
			StackCount--;
		}else {
			throw new Exception("Stack is empty");
		}
		return ReturnNum;
		
	}
	
	public void pop(int input) {//Take input as argument, and pop such number of elements from the stack.
		for (int i = 0; i < input; i++) {
			try {
				this.pop();
			} catch (Exception e) {
				
			}
		}
	}
	
	public void showALL() {//Show all the elements in stack.
		for (int i = 0; i < StackCount; i++) {
			System.out.print(Stack[i] + " ");
			if ((i+1)%10 == 0 && i != 0 && i != (StackCount-1)) {
				System.out.println();
			}
		}
		System.out.println();
	}
	
	public boolean isFull() {//Check the stack whether it is full or not.
		if(StackCount == 100){
			return true;
		}else {
			return false;
		}
	}
	
	public boolean isEmpty() {//Check the stack whether it is empty or not.
		if (StackCount == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public int getStackCount() {//Get StackCount value
		return StackCount;
	}
}

