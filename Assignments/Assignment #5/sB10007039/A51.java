package a5.sB10007039;

import java.util.Scanner;

public class A51 {
	public static void main(String[] args) {
		Scanner Input = new Scanner(System.in);
		StackOfIntegers MyStack = new StackOfIntegers();
		boolean WhileContorler = true;
		
		while (WhileContorler) {
			System.out.println("1.push");
			System.out.println("2.pop");
			System.out.println("3.showALL");
			System.out.println("4.exit");
			
			switch (Input.nextInt()) {
				case 1://push Prime Numbers into stack
					int Want;
					System.out.print("push ");
					Want = Input.nextInt();
					int Start;
					boolean Full = false;
					try {//tack a int for start from stack
							Start = MyStack.pop();
							MyStack.push(Start);
							Start++;
					} catch (Exception e) {
							Start = 2;
					}
					if (MyStack.isFull()) {
						System.out.println("Stack is Full");
						break;
					}
					for (int i = 0; i < Want; i++) {//find a Prime Numbers
						boolean Find = false;
						while (!Find) {
							int Divisible = 0;
							for (int j = 1; j <= Start; j++) {
								if (Start%j == 0) {
									Divisible++;
								}
							}
							if (Divisible == 2) {
								Find = true;
								try {
									MyStack.push(Start);//try putin a Prime Numbers to stack
								} catch (Exception e) {
									Full = true;//if stack is full then show message
								}
							}
							Start++;
						}
					}
					MyStack.showALL();
					if (Full) {
						System.out.println("Stack is Full");
					}
					break;
					
				case 2://pop Prime Numbers from stack
					int WantPop = 0;
					System.out.print("pop ");
					WantPop = Input.nextInt();
					MyStack.pop(WantPop);
					if (MyStack.isEmpty()) {
						System.out.println("Stack is empty");
					}else {
						MyStack.showALL();
					}
					break;
					
				case 3://showall from stack
					System.out.print("showAll \n");
					if (MyStack.isEmpty()) {
						System.out.println("Stack is empty");
					}else {
						MyStack.showALL();
						if (MyStack.isFull()) {
							System.out.println("Stack is Full");
						}
					}
					break;
					
				case 4://exit
					WhileContorler = false;
					break;
					
				default:
					System.out.println("Choose Error");
					break;
			}
			
		}
		
	}
	
}
