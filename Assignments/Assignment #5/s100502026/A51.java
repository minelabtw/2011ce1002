package a5.s100502026;

import java.util.Scanner;

public class A51
{
	
	public static void main(String[] args)
	{
		
		//Declare variables
		Scanner input=new Scanner(System.in);
		boolean exit=true;
		int choice;
		int number;
		
		
		StackOfIntegers stack1 = new StackOfIntegers();
		
		//Start to run loop
		while(exit)
		{
		
			//Choice
			System.out.println("1.push");
			System.out.println("2.pop");
			System.out.println("3.Show all");
			System.out.println("4.exit");		
		
			choice=input.nextInt();
			
			//Action
			switch(choice)
			{
				case 1:
					System.out.print("push ");
					number=input.nextInt();
					if( number+stack1.getNumberOfStack()<=100 && number>=0 )
					{
						stack1.Push(number);
					}
					else if( number + stack1.getNumberOfStack()>100)
					{
						stack1.Push( 100 - stack1.getNumberOfStack() ) ;
					}
					
					stack1.ShowAll();
					break;
					
				case 2:
					System.out.print( "pop " );
					number=input.nextInt();
					if(number <= stack1.getNumberOfStack() )
					{
						stack1.Pop(number);
					}
					else
					{
						number = stack1.getNumberOfStack();
						stack1.Pop(number);
						System.out.print("the stack is empty!!");
					}
					
					stack1.ShowAll();
					break;
					
				case 3:
					System.out.print("push all\nthe stack here is :\n");
					
					if( stack1.getNumberOfStack() != 0 )
					{
						stack1.ShowAll();
					}
					else
					{
						System.out.print("the stack is empty!!");
					}
					break;
					
				default :
					exit=false;
					break;
			}
			System.out.print("\n");
		}
	}
//HAPPY ENDING >_<
}
