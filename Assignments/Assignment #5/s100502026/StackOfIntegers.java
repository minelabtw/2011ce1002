package a5.s100502026;

//import java.util.ArrayList;

public class StackOfIntegers
{
	private java.util.ArrayList<Integer> stack = new java.util.ArrayList<Integer>();
	
	//Constructor to initialize
	StackOfIntegers()
	{
		stack.clear();
	}
	
	//Show all elements
	public void ShowAll()
	{		
		System.out.print(stack.toString());
	}
	
	//Remove the element choosed
	public void Pop( int number )
	{
		int counter = 0;
		while(counter < number)
		{
			stack.remove(stack.size()-1);
			counter = counter + 1 ;
		}
	}
	
	//Add the element choosed
	public void Push( int number)
	{
		int counter = 0;
		while( counter < number )
		{
			stack.add( PrimeNumber( stack.size() +1 ) );
			counter = counter + 1;
		}		
	}
	
	//Check the array is empty or not
	public boolean isEmpty()
	{
		return stack.isEmpty();
	}
	
	//Check the array is full or not
	public boolean isFull()
	{
		if( stack.size() != 100)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	//Create a prime number
	public int PrimeNumber( int number)
	{
		int NumberOfPrimeNumber = 0;
		int base = 1;
		
		while( NumberOfPrimeNumber < number )
		{
			if( Check( base ) )
			{
				NumberOfPrimeNumber = NumberOfPrimeNumber + 1;
			}
			
			base = base + 1;
		}
		
		return base - 1;
	}
	
	//Judge the number is a prime number or not
	public boolean Check( int base )
	{
		int temp = 0;
		temp += Math.pow(base, 0.5);
		int counter = 2;
		
		if( base != 1 )
		{
			while( counter <= temp )
			{
				if( ( base % counter ) == 0 )
				{
					return false;
				}
				counter = counter + 1;
			}
		}
		else
		{
			return false;
		}
		
		return true;
	}
	
	//Know the length of this array
	public int getNumberOfStack()
	{
		return stack.size();
	}
}
