package a5.s100502013;
import java.util.Scanner;

public class A5 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		StackOfIntegers mystack = new StackOfIntegers(); //create the object
		System.out.print("1.push\n2.pop\n3.show all\n4.exit\n"); //The list
		int choose = 1; //use it to choose functions
		while(choose!=4){
			System.out.print("Please enter which function you want to use: ");
			choose = input.nextInt();
			switch(choose){
				case 1: //Push function
					System.out.print("*PUSH*\nPlease enter a number: ");
					int pushnum = input.nextInt();
					System.out.println("Push " + pushnum);
					if(mystack.isFull(pushnum)){ //check whether it is full
						System.out.println("The stack is full(100)!");
						pushnum = 100 - mystack.getnumber();
					}
					mystack.Push(pushnum);
					mystack.showAll();
					break;
					
				case 2: //Pop function
					System.out.print("*POP*\nPlease enter a number: ");
					int popnum = input.nextInt();
					if(mystack.isEmpty(popnum)){ //check whether it is empty
						System.out.println("Pop " + popnum);
						popnum = mystack.getnumber()+1;
						mystack.Pop(popnum);
						System.out.println("The stack is empty!!");
						mystack.setnumber(0);
					}
					else{
						mystack.Pop(popnum);
						System.out.println("Pop " + popnum);
						mystack.showAll();
					}
					break;
					
				case 3: //Show-all function
					System.out.print("Show All\n");
					mystack.showAll();
					break;
				
				case 4: //Exit function
					System.out.println("Goodbye!");
					break;
					
				default: //invalid input
					System.out.println("This is invalid.");
					break;
			}
		}
	}
}
