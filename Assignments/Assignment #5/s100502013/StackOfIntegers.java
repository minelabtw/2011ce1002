package a5.s100502013;

public class StackOfIntegers{
	private int[] stack = new int[100];
	private int number;
	
	StackOfIntegers(){
		number = 0; //initialized
	}
	
	public void showAll(){
		System.out.println("The stack here is:");
		if(getnumber()>=0){
			for(int i=0;i<=getnumber();i++)
				System.out.print(stack[i] + "\t"); //output
		}
		else
			System.out.print("The stock is empty!");
		System.out.println("");
		}
	
	public void Pop(int num){
		setnumber(getnumber()-num); //set new number
		countprime();
	}
	
	public void Push(int num){
		setnumber(getnumber()+num-1); //set new number
		countprime();
	}
	
	public boolean isEmpty(int x){
		if(getnumber()-x<=0)
			return true;
		else
			return false;
	}
	
	public boolean isFull(int x){
		if(getnumber()+x>99)
			return true;
		else
			return false;
	}
	
	public void setnumber(int x){
		number = x;
	}
	
	public int getnumber(){
		return number;
	}
	
	public int[] getstack(){
		return stack;
	}
	
	public int[] primenumbers(){ //calculate 100 prime numbers and save in an array
		int counting = 0;
		int countnumber = 0;
		int[] primenum = new int[100];
		int i=2;
		while(countnumber<100){
			for(int j=1;j<=i;j++){
				if(i%j==0)
					counting++;
			}
			if(counting==2){
				primenum[countnumber] = i;
				countnumber++;
			}
			counting = 0;
			i++;
		}
		return primenum;
	}
	
	public void countprime(){ //output prime numbers with user's argument
		for(int i=0;i<=getnumber();i++){
			getstack()[i] = primenumbers()[i];
		}
	}
}
