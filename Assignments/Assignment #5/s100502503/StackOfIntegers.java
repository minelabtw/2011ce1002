package a5.s100502503;

public class StackOfIntegers
{
	private int[] primeArray = new int[100];
	private int size = 0;//the variables to determine how many prime numbers to show 
	
	public StackOfIntegers()//the constructor
	{
	}
	
	public void showAll()//show all the prime numbers
	{
		for(int i = 0; i < size; i++ )
		{
			System.out.print(primeArray[i] + " ");
		}
	}
	public void pop(int num)//decrease the numbers to show out
	{
		size -= num;  
	}
	
	public void push(int num)//increase the numbers to show out
	{
		size += num;
	}
	
	boolean isEmpty()//check if size is empty
	{
		return size == 0;
	}
	
	boolean isFull()//check if size is over then 100
	{
		return size > 100;
	}
	
	public void primeNumber()
	{
		int testNumber = 0;
	
		for(int a = 2; testNumber < 100; a++)//start from to, if the number's divisor are only 1 and itself, it is prime number. 
		{
			boolean flag = true;
			double test = Math.sqrt(a+1);
			
			for(int b = 2; b < test; b++)
			{
				if(a % b == 0)
					flag = false;
			}
			
			if(flag == true)
			{
				primeArray[testNumber] = a;
				testNumber++;
			}
		}
	}
}
