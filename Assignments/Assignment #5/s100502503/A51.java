package a5.s100502503;

import java.util.Scanner;

public class A51 
{	
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		boolean flag = true;
		StackOfIntegers stack = new StackOfIntegers(); //Constructor class stackOfIntegers, the stack is empty
		stack.primeNumber();//save 100 prime numbers to the stack
		while(flag)
		{
			System.out.print("\n1.push" + "\n2.pop" + "\n3.Show all" + "\n4.exit");
			int chose = input.nextInt();
			
			switch(chose)
			{
				case 1://the push function
					System.out.print("push ");
					int userInputPush = input.nextInt();
					stack.push(userInputPush);
					while(stack.isFull())//If push overload, show 100 prime numbers
					{
						stack.push(100);
					}
					stack.showAll();
					break;
					
				case 2://the pop function
					System.out.print("pop: ");
					int userInputPop = input.nextInt();
					stack.pop(userInputPop);
					while(stack.isEmpty())
					{
						System.out.print("IT'S EMPTY!");//If pop to empty, show the message
					}
					stack.showAll();
					break;
					
				case 3:
					System.out.print("Show all: ");//show all tje prime number
					stack.showAll();
					break;
					
				case 4:
					flag = false;//exit
					break;
			}
		}
		
	}
	
	
}
