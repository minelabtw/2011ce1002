package a5.s100502521;

public class StackOfIntegers
{
	private int[] elements;//存放用
	private int size;//多少元素
	public StackOfIntegers()//無傳入值設初始值100
	{
		elements=new int[100];
		size=0;
	}
	public StackOfIntegers(int a)//傳入可設定大小
	{
		elements=new int[a];
		size=0;
	}
	public int[] showAll()//回傳全部內容  不直接print 可以傳到視窗上做顯示
	{
		return elements;
	}
	public boolean Pop(int num)//pop 取出  如果中途結束 會回傳false
	{
		for(int i=0;i<num;i++)
		{
			if(isEmpty())
			{
				return false;
			}
			elements[size-1]=0;
			size--;
		}
		return true;
	}
	public boolean Push(int num)//push 將數字加入 如果滿了則傳回false
	{
		if(isFull())
		{
			return false;
		}
		else
		{
			elements[size]=num;
			size++;
			return true;
		}
	}
	public boolean isEmpty()//如果是空的則傳回true
	{
		if(size==0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public boolean isFull()//如果是滿的則傳回true
	{
		if(size==elements.length)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public int getSize()//回傳目前元素量
	{
		return size;
	}
}
