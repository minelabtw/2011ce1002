package a5.s100502521;
import java.util.Scanner;
import java.lang.Math;

public class A51 
{
	public static void main(String[] args)
	{
		int choice=-1;
		Scanner input=new Scanner(System.in);
		int inputNumber,temp;
		int[] array;
		StackOfIntegers list=new StackOfIntegers();
		while(choice!=4)
		{
			System.out.print("1.push\n2.pop\n3.Show all\n4.exit\n請選擇功能: ");
			choice=input.nextInt();
			switch(choice)
			{
			case 1://PUSH
				System.out.println("請輸入要push幾個數字? ");
				inputNumber=input.nextInt();
				temp=list.getSize();
				for(int i=temp;i<temp+inputNumber;i++)
				{
					if( list.Push( prime_num(i))==false )
					{
						System.out.println("Stack已滿!!!");
						break;
					}
				}
				
				array=list.showAll();				//印出所有元素
				for(int i=0;i<list.getSize();i++)
				{
					System.out.print(array[i]+" ");
				}
				System.out.println("");
				break;
			case 2://POP
				System.out.println("請輸入要pop幾個數字? ");
				inputNumber=input.nextInt();
				if(!list.Pop(inputNumber))
				{
					System.out.println("輸入的數超過，所有內容將被清空");
				}
				else
				{
					array=list.showAll();			//印出所有元素
					for(int i=0;i<list.getSize();i++)
					{
						System.out.print(array[i]+" ");
					}
					System.out.println("");
				}
				break;
			case 3:
				array=list.showAll();					//印出所有元素
				for(int i=0;i<list.getSize();i++)
				{
					System.out.print(array[i]+" ");
				}
				System.out.println("");
				break;
			case 4:
				System.out.println("掰掰~~~");
				break;
			default:
				break;
			}
		}
	}
	public static int prime_num(int input) // 計算第input個質數  缺點 每次都從頭算
	{
		boolean check = false;
		int prime=3;
		int count=0;
		if(input==0)
		{
			return 2;
		}
		else
		{
			while(true)
			{
				for(int i=2;i<Math.sqrt(prime)+1;i++)// 從2開始除，看輸入的數開根號+1除於比它小的數後，有沒有可以整除的
				{
					check = true;
					if((prime%i)== 0)
					{ 
						check = false; // 如果有，則不為質數
						break;
					}
				}
				if(check)
				{
					count++;
					if(count==input)//算到底第幾個了則跳出
					{
						break;
					}
				}
				prime++;
			}
			return prime;
		}
	}
}
