package a5.s100502511;

import java.util.Scanner;

public class A51 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int choosefunctionnumber;
		StackOfIntegers stack = new StackOfIntegers();
		for (;;) {
			System.out.println("1.Push");
			System.out.println("2.Pop");
			System.out.println("3.Show all");
			System.out.println("4.Exit");
			System.out.print("Choose a function : ");
			choosefunctionnumber = input.nextInt();
			switch (choosefunctionnumber) {
			case 1: // 放入數字
				System.out.print("push ");
				int num1 = input.nextInt();
				stack.push(num1);// }
				System.out.println("");
				break;
			case 2: // 拿出數字
				System.out.print("pop ");
				int num2 = input.nextInt();
				stack.pop(num2);
				System.out.println("");
				break;
			case 3: // 顯示目前數字
				System.out.println("show all");
				stack.showAll();
				break;
			case 4: // 離開程式
				System.out.println("Exit!");
				System.exit(0);
				break;
			default:
				System.out.println("Please input a correct function!");
				break;
			}
		}
	}
}
