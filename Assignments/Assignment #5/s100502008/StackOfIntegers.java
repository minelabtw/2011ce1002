package a5.s100502008;

public class StackOfIntegers {
	private int[] array = new int[100];// array of stacks
	private int size = 0;// size of array
	private String outstr = "";

	public void Push(int limitnum) {// push num
		if (size == 0) {// begin to push
			if (limitnum > 100) {
				limitnum = 100;
			}
			int allcount = 1, count = 0, num = 2;
			while (allcount <= limitnum) {// compute the number to store
				count = 0;
				for (int check = 1; check <= num; check++) {
					if (num % check == 0) {
						count++;
					}
				}

				if (count == 2) {// store
					array[size++] = num;
					allcount++;

				}
				num++;
			}
		} else {// continue to push case

			int allcount = 1, count = 0, num = array[size - 1] + 1;
			while (allcount <= limitnum) {// compute the number to store
				count = 0;
				for (int check = 1; check <= num; check++) {
					if (num % check == 0) {
						count++;
					}
				}
				if (isFull()) {// full
					break;
				} else {// store
					if (count == 2) {
						array[size++] = num;
						allcount++;
					}
				}
				num++;
			}
		}
	}

	public void Pop(int limitnum) {// pop num
		if (!isEmpty()) {
			size -= limitnum;
			if (isEmpty()) {// empty
				size = 0;
			}
		}
	}

	public boolean isEmpty() {// check empty or not
		if (size <= 0) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isFull() {// check full or not
		if (size == 100) {
			return true;
		} else {
			return false;
		}
	}

	public String showAll() {// show result
		outstr = "";
		for (int i = 0; i < size; i++) {
			outstr += array[i] + " ";
		}
		if (isFull()) {
			outstr += "\nIs Full";
		} else if (isEmpty()) {
			outstr += "\nIs Empty";
		}
		outstr += "\n";
		return outstr;
	}
}
