package a5.s100502008;

import a5.s100502008.StackOfIntegers;
import java.util.Scanner;

public class A51 {
	public static void main(String[] args) {
		StackOfIntegers myStackOfIntegers = new StackOfIntegers();// declare
																	// object
		Scanner input = new Scanner(System.in);
		int chos1, chos2;
		for (int i = 1; i < 2; i++)// loop
		{
			System.out.println("\n1.push");
			System.out.println("2.pop");
			System.out.println("3.Show all");
			System.out.println("4.exit");
			System.out.print("Your choice is: ");
			chos1 = input.nextInt();
			switch (chos1)// choice
			{
			case 1:// push num
				System.out.print("How many number you want to push: ");
				chos2 = input.nextInt();
				myStackOfIntegers.Push(chos2);
				System.out.print(myStackOfIntegers.showAll());
				i = 0;
				break;
			case 2:// pop number
				System.out.print("How many number you want to pop: ");
				chos2 = input.nextInt();
				myStackOfIntegers.Pop(chos2);
				System.out.print(myStackOfIntegers.showAll());
				i = 0;
				break;
			case 3:// show result
				System.out.print(myStackOfIntegers.showAll());
				i = 0;
				break;
			case 4:
				i = 2;// exit
				break;
			default:// uncorrect case
				System.out.println("Error!");
				i = 0;
				break;
			}
		}
	}
}
