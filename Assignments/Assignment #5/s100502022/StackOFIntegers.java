package a5.s100502022;

public class StackOFIntegers {
	  private int[] elements;
	  private int size;
	  public static final int DEFAULT_CAPACITY = 100;

	  /** Construct a stack with the default capacity 100 */
	  public StackOFIntegers() {
		  this(DEFAULT_CAPACITY);
	  }

	  /** Construct a stack with the specified maximum capacity */
	  public StackOFIntegers(int capacity) {
		  elements = new int[capacity];
	  }

	  /** Push a new integer into the top of the stack */
	  public void push(int value) {
		  elements[size++]=value;
	  }

	  /** Return and remove the top element from the stack */
	  public void pop(int number) {
		  //declare a varible to store the const value because it's value will change
		  int const_size=size;
		  if(isEmpty()){
			  System.out.print("failed,it's empty");
		  }
		  else if(const_size<number){
			  for(int i=0;i<const_size;i++){
				  elements[--size]=0;
			  }
			  System.out.print("it's empty");
		  }
			  else 
				  for(int i=0;i<number;i++){
					  elements[--size]=0;
				  }  
	  }

	  /** Return the top element from the stack */
	  public int peek() {
		  return elements[size-1];
	  }

	  /** Test whether the stack is empty */
	  public boolean isEmpty() {
		  return size == 0;
	  }
	  /** Test whether the stack is full */
	  public boolean isFull(){
		  return size==100;
	  }
	  /** Return the number of elements in the stack */
	  public int getSize() {
		  return size;
	  }
	  public void showall(){
		  if(isEmpty()){
			  System.out.print("EMPTY");
		  }
		  
		  for(int i=0;i<size;i++){
			  System.out.print(elements[i]+" ");
		  }
	  }
	}
