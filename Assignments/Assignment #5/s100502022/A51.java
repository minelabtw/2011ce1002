package a5.s100502022;
import java.util.Scanner;
public class A51 {
	public static void main (String[] args){
		Scanner input =new Scanner(System.in);
		System.out.print("1.push\n2.pop\n3.show all\n4.exit\n");
		StackOFIntegers stack=new StackOFIntegers(100);
		int item= 0;
		boolean loopCtrl=true;
		while (loopCtrl){
			item=input.nextInt();
			switch(item){
				case 1:
					int piece_add=input.nextInt();
					int a=0;
					int newnumber;
					if(stack.isEmpty()){
						newnumber=2;
					}
					else 
						newnumber=stack.peek()+1;
					
					while(a<piece_add){
						boolean ctrl=true;
						if(stack.isFull()){
							System.out.print("it's full");
							break;
						}
						
						for(int testnum=2;testnum<=Math.pow(newnumber, 0.5);testnum++){
							if(newnumber%testnum==0){
								ctrl=false;
								break;
							}
						}
						if(ctrl){
							stack.push(newnumber);
							a++;
						}
						newnumber++;
					}
					stack.showall();
					break;
				case 2:
					int piece_sub=input.nextInt();
					stack.pop(piece_sub);
					stack.showall();
					break;
				case 3:
					stack.showall();
					break;
				case 4:
					loopCtrl=false;
					break;
			}
		}
	}
}
