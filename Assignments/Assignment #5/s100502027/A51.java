package a5.s100502027;

import java.util.Scanner;

public class A51 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		StackOfIntegers test = new StackOfIntegers();         //a list
		System.out.println("1.PUSH");
		System.out.println("2.POP");
		System.out.println("3.SHOW ALL");
		System.out.println("4.EXIT");
		System.out.println("");
		int enter,choose,turnon=1,primes;
		while(turnon!=0){                                   // infinite loop
			System.out.println("Please choose the control : ");
			System.out.println("");
			choose = input.nextInt();
			switch(choose){
				case 1:        //push
					System.out.println("PUSH : ");
					enter = input.nextInt();
					for(int t=0;t<enter;t++ ){
						if(test.getSize()>0){               //make sure the size and get the last value use it to get next prime
							primes = primenumber(test.peek());
						}
						else{
							primes = 2 ;
						}
						test.push(primes);
					}
					test.showAll();
					break;
					
				case 2:    //pop
					System.out.println("POP : ");
					enter = input.nextInt();
					test.pop(enter);
					test.showAll();
					break;
					
				case 3:
					System.out.println("SHOW ALL : ");
					test.showAll();
					break;
					
				case 4:
					System.out.println("Thank for use . Bye~");
					turnon=0;
					break;
					
				default:
					System.out.println("Please enter the right choose");
					break;
			}
		}

	}
	
	public static int primenumber(int peeknumber){     //use the last value to find the next prime 
		int temp=peeknumber;
		int times=1;
		while(times!=0){
			times=0;
			temp++;
			for (int n=2;n<=Math.sqrt(temp);n++){
				if(temp%n==0){
					times++;
				}
				else{
					
				}
			}
		}
		return temp;
	}
}
