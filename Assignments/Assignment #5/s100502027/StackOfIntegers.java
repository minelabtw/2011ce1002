package a5.s100502027;

public class StackOfIntegers {
	public static final int DEFAULT_CAPACITY = 100;
	private int size = 0 ;
	private int [] elements;
	
	public StackOfIntegers(){
		this(DEFAULT_CAPACITY);
	}
	
	public StackOfIntegers(int capacity){
		elements = new int [capacity];  //construter to set the array length
	}
	
	public void push(int num){    //under the line, push the value into array
		if(size<100){
			elements[size++] = num ;
		}
		else{
			
		}
	}
	
	public void pop(int num){   //pop a value out , make sure the time under size
		int times=0;
		if(num>=size){
			times=size;
		}
		else{
			times=num;
		}
		for(int t=0;t<times;t++){
			elements[--size] = 0;
		}
	}
	
	public int getSize(){  
		return size;
	}
	
	public int peek(){
		return elements[size-1];
	}
	
	public void showAll(){   //show array all value with make sure the array empty or full or not
		if(isEmpty()==true){
			System.out.println("the stack is empty!!");
			System.out.println("");
			}
		else if(isFull()==true){
			System.out.println("the stack is full!!");
			System.out.println("the stack here is :");
			for (int t=0;t<size;t++){
				System.out.print(elements[t]+" ");
				}
			System.out.println("");
			}
		else{
			System.out.println("the stack here is :");
			for (int t=0;t<size;t++){
				System.out.print(elements[t]+" ");
				}
			System.out.println("");
			}
	}
	
	public boolean isEmpty(){
		if(size==0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public boolean isFull(){
		if(size==100){
			return true;
		}
		else{
			return false;
		}
	}
}
