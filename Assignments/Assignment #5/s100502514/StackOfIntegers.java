/**
  * class StackOfIntegers
  * [The Following Rules are from ce1002 website]
  *	class StackOfIntegers with the following method:

		+showAll(): Show all the elements in stack.
		 [MODIFIED: returns a string using overriden toString().]
		+pop(int num): Take input as argument, and pop such number of elements from the stack.
		+push(int num): Take input as argument, and push the input into the stack.
		+isEmpty(): Check the stack whether it is empty or not.
		+isFull(): Check the stack whether it is full or not.
		
		[Self Extra Methods]
		+length(): get the length of the stack
		+maxlength(): get the maximum length for the stack can be
		+resize(int new_length): resize the stack with new length, return a boolean for whether succeed.
		+getLast(): get the last integer of the stack.
 */
package a5.s100502514;

public class StackOfIntegers {
	/** Declare a private array for stacking. */
	private int stack_array[];
	//Declare a state for how many elements that were stacked.
	private int current_stacked_num = 0;
	
	/** A Specified Constructor */
	public StackOfIntegers(int max){
		stack_array = new int[max];
	}
	/** Default Constructor, set the stack size to 100 */
	public StackOfIntegers(){
		this(100);
	}
	
	/** Push an element of number into the stack */
	public boolean push(int element) throws A51_Negative_Exception{
		try{
			stack_array[current_stacked_num++] = element;
		}catch(ArrayIndexOutOfBoundsException err){
			/** If fails when pushing, namely the stack is full, then return false. */
			//Set the stack length to the maximum array length
			current_stacked_num = stack_array.length;
			return false;
		}
		/** If succeed, then return true. */
		return true;
	}
	
	/** Pop an element of number from the stack */
	public boolean pop(int amount) throws A51_Negative_Exception{
		//Negative amount is not allowed
		if(amount<0) throw new A51_Negative_Exception();
		
		//No need to change the value; just move back the length index by one.
		current_stacked_num-=amount;
		if(current_stacked_num<0){
			//If the index becomes negative then returns false for failure.
			//But the stack remains empty.
			current_stacked_num = 0;
			return false;
		}else{
			//Return true for success.
			return true;
		}
	}
	/** Default function pop() is equivalent to pop(1) */
	public boolean pop() throws A51_Negative_Exception{
		return pop(1);
	}
	
	/** Check if the stack is empty */
	public boolean isEmpty(){
		return current_stacked_num==0;
	}
	/** Check if the stack is full */
	public boolean isFull(){
		return current_stacked_num==stack_array.length;
	}
	
	/** Return a string for the stack, overriding java.lang.Object.toString() */
	public String toString(){
		//Return another message for empty.
		if(isEmpty())return "The stack is empty now.";
		
		String message = "{";
		for(int i=0;i<current_stacked_num;i++){
			if(i>0){
				message += ", ";
				if(i%20==0) message += "\n"; //Separates into lines of 20 indexes.
			}
			message += stack_array[i];
		}
		message += "}";
		return message;
	}
	//Only for the assignment requirement; this method shows the message using toString()
	public String showAll(){
		return "Stack Length: "+length()+" Elements\n" +
				"Contents:\n"+toString();
	}
	
	/** Self extra method: get the length of the stack */
	public int length(){
		return current_stacked_num;
	}
	/** Self extra method: get the maximum length for the stack can be */
	public int maxlength(){
		return stack_array.length;
	}
	/** Self extra method: resize the stack with new length */
	public boolean resize(int new_length){
		if(new_length<current_stacked_num){
			/** If the action of resize would cut off the stack, the resizing fails. */
			return false;
		}
		/** Copy all the data into a new array. */
		int[] new_array = new int[new_length];
		System.arraycopy(stack_array, 0, new_array, 0, current_stacked_num);
		
		/** Replace the old stack array into the new one. */
		stack_array = new_array;
		/** Since it succeed, return true. */
		return true;
	}
	/** Self extra method: get the last integer of the stack */
	public int getLast(){
		if(current_stacked_num==0)return 0;//Returns 0 if the stack is empty
		return stack_array[current_stacked_num-1];
	}
}
