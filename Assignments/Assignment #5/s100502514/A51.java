/**
 * CE1002-100502514 FlashTeens Chiang
 * [The Following Rules are from ce1002 website]
 * Assignment 5-1
	
	1. Declare a stack with class StackOfIntegers, which includes the following method:

		+showAll(): Show all the elements in stack.
		+pop(int num): Take input as argument, and pop such number of elements from the stack.
		+push(int num): Take input as argument, and push the input into the stack.
		+isEmpty(): Check the stack whether it is empty or not.
		+isFull(): Check the stack whether it is full or not.

	2. Write a test program and meet the following requirements:

	  *	At first, the stack should be empty.
	  *	User can store some Prime Numbers, at most 100 numbers, in the stack.
	  *	Pop: let user input a number and pop such number of elements from stack.
		Show the result after pop.
	  *	Push: let user input a number and push such number of elements into stack.
		Show the result after push.
	  *	ShowAll: show the current elements in the stack.
	  *	A list and infinite loop
	
 */
package a5.s100502514;

import javax.swing.*;

public class A51 {
	public static void main(String[] args){
		/** In this assignment, I declare an object of StackOfPrimes,
			instead of StackOfIntegers, but the inheritance of it. */
		StackOfPrimes stack = new StackOfPrimes();
		
		/** Declare the button labels as an array of strings. */
		String[] button_labels = {"Push", "Pop", "Show All"};
		
		/** Show the dialog in a while-loop */
		while(true){
			try{
				int choice = JOptionPane.showOptionDialog(null,
					"Please choose which function to use:",
					"StackOfIntegers by 100502514",
					JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null,
					button_labels, null);
				int input;
				switch(choice){
				case 0: /** Push */
					if(stack.isFull()){ //If the stack is full before pushing
						JOptionPane.showMessageDialog(null, "The Stack is already full!!\n" +
								"You cannot push until you pop something out.");
					}else{
						String input_str = JOptionPane.showInputDialog(
								"Please input how many primes you'd like to push:");
						if(input_str==null){
							throw new Exception(); //Skip the codes below if click "cancel"
						}
						input = Integer.parseInt(input_str);
						if(stack.push(input)){
							JOptionPane.showMessageDialog(null, "Pushed Successfully!!\n" +
									stack.showAll());
						}else{
							JOptionPane.showMessageDialog(null, "Warning: The Stack is full now.\n" +
									"Some of the elements weren't be pushed yet!!\n" +
									stack.showAll());
						}
					}
					break;
				case 1: /** Pop */
					if(stack.isEmpty()){ //If the stack is full before pushing
						JOptionPane.showMessageDialog(null, "The Stack is already empty!!\n" +
								"You cannot pop until you push something in.");
					}else{
						String input_str = JOptionPane.showInputDialog(
								"Please input how many primes you'd like to pop:");
						if(input_str==null){
							throw new Exception(); //Skip the codes below if click "cancel"
						}
						input = Integer.parseInt(input_str);
						if(stack.pop(input)){
							JOptionPane.showMessageDialog(null, "Popped Successfully!!\n" +
									stack.showAll());
						}else{
							JOptionPane.showMessageDialog(null, "Warning: The Stack is empty now.\n" +
									"The popping action ommitted after the stack is empty!!\n" +
									stack.showAll());
						}
					}
					break;
				case 2: /** Show All */
					JOptionPane.showMessageDialog(null, stack.showAll());
					break;
				case JOptionPane.CLOSED_OPTION:
					return;
				}
			}catch(A51_Negative_Exception err){
				//Run here if user inputs a negative value.
				JOptionPane.showMessageDialog(null, "Negative value or Zero is NOT allowed!!");
			}catch(NumberFormatException err){
				//Run here if user inputs a not-a-number value.
				JOptionPane.showMessageDialog(null, "Invalid Value!!");
			}catch(Exception cancel_err){
				/** Continue the next loop when user click "cancel" */
			}
		}
	}
}
