package a5.s100502514;

/** Extra: Inheritance of StackOfIntegers */
public class StackOfPrimes extends StackOfIntegers {
	
	/** Private counting of current last prime. */
	private int current_last_prime = 0; //Zero stands for nothing.
	
	public StackOfPrimes(){
		//Call the constructor of StackOfIntegers with default length 100
		super(100);
	}
	
	/** Push the next prime into the stack */
	public boolean push() throws A51_Negative_Exception{
		do{
			current_last_prime++;
		}while(!is_prime(current_last_prime));
		//Call the StackOfIntegers.push method
		return super.push(current_last_prime);
	}
	/** Repeat the times that runs push() */
	public boolean push(int num_elements) throws A51_Negative_Exception{
		//Negative or Zero Amount Value is not allowed.
		if(num_elements<=0)throw new A51_Negative_Exception();
		
		for(int i=0;i<num_elements;i++){
			//If the push failed then return false.
			if(!push())return false;
		}
		return true;
	}
	
	/** Overriden Pop methods */
	public boolean pop(int amount) throws A51_Negative_Exception{
		boolean succeed = super.pop(amount);
		current_last_prime = getLast();
		return succeed;
	}
	
	/** Private member method to determine if a number is a prime */
	private boolean is_prime(int num){
		if(num<=1)return false; //Number that no more than 1 is not a (positive) prime.
		for(int i=2;i*i<=num;i++){
			if(num%i==0)return false; //Return false if found a number>=2 can divide num.
		}
		//If no any integers that can divide num, then num is a prime.
		return true;
	}
}
