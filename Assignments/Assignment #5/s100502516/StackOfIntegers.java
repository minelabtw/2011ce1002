package a5.s100502516;

public class StackOfIntegers {
	private java.util.ArrayList<Integer> list = new java.util.ArrayList<Integer>();
	private int curPrime = 1;
	
	public void showAll()
	{
		if(isEmpty())		
			System.out.println("the stack is empty!!");		
		else if(isFull())
		{			
			System.out.println("the stack here is :\n" + list.toString() + "\nthe stack is full!!");	
		}
		else
		{
			System.out.println("the stack here is :\n" + list.toString());			
		}		
	}		
	
	public void Push(int num)//it only allow prime number push
	{
		back:
		{
			if(isFull())//check is it full before push
				break back;
			
			int element = curPrime;//start from nearest prime number
			
			while(num != 0)
			{			
				boolean prime = true;
				
				for(int i = element - 1; i >= 2; i--)//check is it prime number
				{				
					if(element % i == 0)
					{
						prime = false;
						break;
					}
				}
				
				if(prime)
				{					
					if((element != 1 || element == 2) && element != curPrime)//treat the special case 1, 2 and equal value
					{						
						list.add(element);
						curPrime = element;//update current prime number
						num--;
					}					
				}
				
				if(isFull())//check is it full in process
					break back;
				
				element++;//to next number
			}
		}
	
		showAll();		
	}
	
	public void Pop(int num)
	{
		back:
		{
			if(isEmpty())
				break back;
		
			for(int i = 0; i < num; i++)
			{
				if(list.size() > 0)
					list.remove(list.size() - 1);
				
				if(list.size() > 0)
					curPrime = list.get(list.size() - 1);//update current prime number
				else
					curPrime = 1;//initialize current number
			}			
		}
	
		showAll();
	}
	
	public boolean isEmpty()
	{		
		return list.isEmpty();
	}
	
	public boolean isFull()
	{
		if(list.size() == 100)
			return true;
		else
			return false;
	}
}
