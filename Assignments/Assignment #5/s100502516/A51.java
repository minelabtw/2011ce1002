package a5.s100502516;

import java.util.Scanner;

public class A51 {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		StackOfIntegers stack = new StackOfIntegers();
		
		int select;//user selection
		boolean stop = false;//end of while
		
		System.out.print("1.push\n" + "2.pop\n" + "3.Show all\n" + "4.exit");
		
		while(!stop)
		{
			System.out.print("\nEnter: ");
			select = input.nextInt();
			
			while(4 < select || select < 1)
			{
				System.out.print("Invalid Enter, try again: ");
				select = input.nextInt();
			}
			
			switch(select)
			{		
				case 1:
					System.out.print("\nPush: ");
					stack.Push(input.nextInt());
					break;
				case 2:
					System.out.print("\nPop: ");
					stack.Pop(input.nextInt());
					break;
				case 3:
					stack.showAll();
					break;
				case 4:
					stop = true;
					break;
			}
		}		
	}
}
