package a5.s100502030;

public class StackOfIntegers {
	private int[] elements;
	private int size;
	public static final int DEFAULT_CAPACITY = 100;

	public StackOfIntegers() {
		this(DEFAULT_CAPACITY);
	}

	public StackOfIntegers(int capacity) {
		elements = new int[capacity];// set capacity
	}

	public void showAll() {
		if (isEmpty()) {
			System.out.println("The stack is empty!");
		}// show information if "empty"
		if (isFull()) {
			System.out.println("The stack is full!");
		}// show information if "full"
		for (int i = 0; i < size; i++)
			System.out.print(elements[i] + " ");// show all elements
	}

	public void pop(int number) {
		int size_now = size;
		for (int i = size_now; i > size_now - number; i--) {
			if (isEmpty())
				break;
			elements[--size] = 0;// eliminate element
		}
	}

	public void push(int value) {
		elements[size++] = value;// store element
	}

	public int peek() {
		return elements[size - 1];// get last element
	}

	public boolean isEmpty() {
		return size == 0;// judge is empty or not
	}

	public boolean isFull() {
		return size == elements.length;// judge is full or not
	}

	public int getSize() {
		return size;
	}

}
