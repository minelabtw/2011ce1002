package a5.s100502030;

import java.util.*;

public class A51 {
	public static void main(String[] args) {
		StackOfIntegers stackTest = new StackOfIntegers();// creat a object
		int primeNumber;
		int number;
		boolean isPrimeNumbers;
		Scanner input = new Scanner(System.in);
		System.out.println("1.push\n2.pop\n3.Show all\n4.exit");// show list
		while (true) {
			int choice = input.nextInt();// input choice
			if (choice == 1) {
				System.out.print("push ");
				number = input.nextInt();// input the amount of primeNumber
				int counter = 0;
				if (stackTest.isEmpty()) {
					primeNumber = 2;// set original primeNumber
				} else
					primeNumber = stackTest.peek() + 1;// find next primeNumber
				while (counter < number && !stackTest.isFull()) {
					isPrimeNumbers = true;
					for (int i = 2; i <= Math.pow(primeNumber, 0.5); i++) {
						if (primeNumber % i == 0) {
							isPrimeNumbers = false;
							break;
						}// get rid of the number is not prime number
					}
					if (isPrimeNumbers) {
						stackTest.push(primeNumber);// push the input into the
													// stack
						counter++;
					}
					primeNumber++;
				}
				stackTest.showAll();
			}
			if (choice == 2) {
				System.out.print("pop ");
				number = input.nextInt();
				stackTest.pop(number);// pop such number of elements from the
										// stack
				stackTest.showAll();
			}
			if (choice == 3) {
				System.out.println("Show all");
				stackTest.showAll();// show all the elements in stack
			}
			if (choice == 4) {
				System.out.println("Bye bye !");
				System.exit(0);
			}

			System.out.println("");

		}
	}
}
