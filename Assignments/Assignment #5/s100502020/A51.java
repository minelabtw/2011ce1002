package a5.s100502020;
import java.util.Scanner;
import a5.s100502020.StackOfIntegers;
public class A51 {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		StackOfIntegers stack = new StackOfIntegers();
		boolean flag = true;
		while(flag)
		{
			System.out.print("1.Push\n2.Pop\n3.Show all\n4.exit\n");
			int choice = input.nextInt();//input choice
			switch(choice)
			{
			case 1://push
				System.out.print("input push number: ");
				int num = input.nextInt();
				stack.push(num);//call push method
				stack.showAll();
				break;				
			case 2://pop
				System.out.print("Input pop number: ");
				int numb = input.nextInt();
				stack.pop(numb);//call pop method
				break;				
			case 3://show all
				stack.showAll();
				break;				
			case 4://exit
				flag = false;
				break;		
			
			}
		}
	}
}
