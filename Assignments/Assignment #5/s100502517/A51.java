package a5.s100502517;

import java.util.Scanner;

public class A51 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		
		StackOfIntegers stackofinteger = new StackOfIntegers();
		
		boolean stop = true;//control the while
		
		while(stop == true){
			System.out.println("1.push\n2.pop\n3.Show all\n4.exit\nEnter: ");
			int choice = input.nextInt();
			
			switch(choice){
				case 1:
					System.out.println("Push: ");
					int push = input.nextInt();
					stackofinteger.Push(push);
					break;
				case 2:
					System.out.println("Pop: ");
					int pop = input.nextInt();
					stackofinteger.Pop(pop);
					break;
				case 3:
					stackofinteger.showall();
					break;
				case 4:
					stop = false;
					break;
				default:
					System.out.println("Wrong Number~!!!");//when you enter the number except 1~5
					break;
			}
			System.out.println("\n");
			
		}
		System.out.println("Thanks for using~ BYE~!!!");	
		
	}	

}
