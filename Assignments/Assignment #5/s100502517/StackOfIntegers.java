package a5.s100502517;

public class StackOfIntegers {
	private java.util.ArrayList<Integer> list = new java.util.ArrayList<Integer>();
	
	public void showall(){//show the final array
		
		if(this.isEmpty()){
			System.out.println("The stack is Empty~!!!");
		}
		else{
			System.out.println("The stack here is "+list.toString());
		}
	}
	
	public void Pop(int num){//remove the array
		
		if(list.size() - num < 0){
			System.out.println("The stack is Empty~!!!");
		}
	
		for(int remove = 1; remove <= num; remove++){
			list.remove(list.size()-1);//remove the array you don't use it
			if(list.size()==0){
				break;
			}
		}
		
		System.out.println(list.toString());//show all the array that you have saved
	}
	
	public void Push(int num){//push number into the array which belongs to the object list
		
		if(list.size()+num > 100){
			System.out.println("The stack is Full~!!!");
		}
		
		boolean stop = true;
		
		for(int add = 1; add <= num ; add++ ){
			
			if(list.size() == 100){
				break;
			}
			int input;
			if(list.size()==0){
				input = 1;
			}
			else{
				input = list.get(list.size()-1);
			}
			int start = 2;
			
			while(stop){
				if(start > input){
					if(start == 2){
						list.add(2);//2 is the special prime number
						break;
					}
					else{
						boolean addorNot = true;
						
						for(int run = 0; run < list.size(); run++){
							if((start%list.get(run)==0)){
								addorNot = false;
								break;
							}
						}
						if(addorNot){
							list.add(start);//save in the array in object list
							break;
						}
					}
				}
				start++;
			}			
		}
		System.out.println(list.toString());
	}
	
	public boolean isEmpty(){//when the list.size() is < 0
		return list.isEmpty();
	}
	
	public boolean isFull(){//when the list.size() is > 100
		if(list.size() > 100)
			return true;
		else
			return false;
	}

}
