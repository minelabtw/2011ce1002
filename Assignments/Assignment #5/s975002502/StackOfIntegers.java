package a5.s975002502;

public class StackOfIntegers {
	private int[] elements;
	private int size;
	public static final int DEFAULT_CAPACITY = 100;

	/** Construct a stack with the default capacity 100. */
	public StackOfIntegers() {
	    this(DEFAULT_CAPACITY);
	}

	/** Construct a stack with the specified maximum capacity. */
	public StackOfIntegers(int capacity) {
	    elements = new int[capacity];
	}
	
	/** showAll(): Show all the elements in stack. */
	public void showAll() {
		if(!this.isEmpty()){
			for (int i=0; i<size; i++) {
				System.out.print(elements[i] + " ");
			}
			System.out.println("");
		}
		else{
			System.out.println("the stack is EMPTY!!!!");
		}
	}

	/** pop(): Return and remove the top element from the stack. */
	public int pop() {
	    return elements[--size];
	}
	
	/** Pop(int num): Take input as argument, and pop such number of elements from the stack. */
	public void Pop(int num) {
		for(int i=num; i>0; i--){
			this.pop();
		}
	}
	
	/** push(int value): Take input as argument, and push the input into the stack. */
	public void push(int value) {
	    if (size >= elements.length) {
	    	int[] temp = new int[elements.length * 2];
	    	System.arraycopy(elements, 0, temp, 0, elements.length);
	    	elements = temp;
	    }

	    elements[size++] = value;
	}

	/** isEmpty(): Check the stack whether it is empty or not. */
	public boolean isEmpty() {
		return size==0;
	}
	
	/** empty(): Empty the stack. */
	public void empty() {
		size = 0;
	}
	
	/** isFull(): Check the stack whether it is full or not. */
	public boolean isFull() {
		return size==100;
	}
	
	/** peek(): Return the top element from the stack. */
	public int peek() {
		if (this.getSize()!= 0){
			return elements[size - 1];
		}
		else{
			return 0;
		}
	}

	/** getSize(): Return the number of elements in the stack */
	public int getSize() {
	    return size;
	}
}
