package a5.s975002502;
import javax.swing.*;

public class A51_GUI {
	public static void main(String[] args) {
		final int LIMIT = 100;	// the limitation of the stack
		int count = 0;			// count the prime numbers' number
		int number = 2;			// used to produce prime numbers
		int num = 0;			// the number user entered for push or pop
		int func = 0;			// the function user selected
		StackOfIntegers_GUI stack = new StackOfIntegers_GUI();
				
		func = getFunc();
		
		while(func!=6){
			switch(func){
				/** the push function */
				case 1:
					num = Integer.parseInt(JOptionPane.showInputDialog(null, "push",
							"Stack", JOptionPane.QUESTION_MESSAGE));
					
					// if the push number is larger than the limitation
					// or stack is full do not push the number into the stack
					if(num>LIMIT || stack.isFull()){
						JOptionPane.showMessageDialog(null, "The stack is FULL!!", 
										"Stack", JOptionPane.INFORMATION_MESSAGE);
					}
					// do push
					else{
						count = 0;
						while(count<num){
							// if stack is full do not push the number into the stack
							if (stack.isFull()){
								JOptionPane.showMessageDialog(null, "The stack is FULL!!", 
												"Stack", JOptionPane.INFORMATION_MESSAGE);
								break;
							}
							// Repeatedly find prime numbers and push them to stack
							if (isPrime(number)) {
								stack.push(number);
								count++; // Increase the prime number count
							}
							number++;
						}
						JOptionPane.showMessageDialog(null, stack.showAll(), 
								  "Stack", JOptionPane.INFORMATION_MESSAGE);
					}

					func = getFunc();
					break;
					
				/** the pop function */
				case 2:
					// if stack is not empty do pop
					if(!stack.isEmpty()){
						num = Integer.parseInt(JOptionPane.showInputDialog(null, "pop",
											   "Stack", JOptionPane.QUESTION_MESSAGE));
						
						stack.Pop(num);
						number = stack.peek()+1;
						JOptionPane.showMessageDialog(null, stack.showAll(), 
								  "Stack", JOptionPane.INFORMATION_MESSAGE);
					}
					// if the stack is empty do not pop
					else{
						JOptionPane.showMessageDialog(null, "The stack is EMPTY!!", 
										 "Stack", JOptionPane.INFORMATION_MESSAGE);
					}
					
					func = getFunc();
					break;
					
				/** the show all function */
				case 3:
					JOptionPane.showMessageDialog(null, 
									"the stack here is: \n" + stack.showAll(), 
									"Stack", JOptionPane.INFORMATION_MESSAGE);
					
					func = getFunc();
					break;
					
				/** the Get size function */
				case 4:
					JOptionPane.showMessageDialog(null, 
									"the stack size is : " + stack.getSize(), 
									"Stack", JOptionPane.INFORMATION_MESSAGE);
										
					func = getFunc();
					break;
					
				/** the Reset function */
				case 5:
					JOptionPane.showMessageDialog(null, "the stack is RESET!! ", 
									  "Stack", JOptionPane.INFORMATION_MESSAGE);
					stack.empty();
					number = 2;
					
					func = getFunc();
					break;
					
				/** no SUCH function */
				default:
					JOptionPane.showMessageDialog(null, "no SUCH function!!", 
								   "Stack", JOptionPane.INFORMATION_MESSAGE);
					
					func = getFunc();
					break;
			}
		}
		
		/** exit function */
		JOptionPane.showMessageDialog(null, "Bye-bye~", 
					"Stack", JOptionPane.INFORMATION_MESSAGE);
	}

	/** isPrime(int number): Check the number whether it is Prime or not. */
	public static boolean isPrime(int number) {
		// Assume the number is prime
		boolean isPrime = true;

		for (int divisor=2; divisor <= number/2; divisor++) {
			//If true, the number is not prime
		    if (number%divisor == 0) {
		    	// Set isPrime to false, if the number is not prime
		    	isPrime = false;
		    	break;
		    }
		}
		
	    return isPrime;
	}
	
	/** display the function list and get the user selected */
	public static int getFunc(){
		int func;
		
		String funclist = "1. push\n" +
						  "2. pop\n" +
						  "3. Show all\n" +
						  "4. Get size\n" +
						  "5. Reset\n" +
						  "6. exit\n";
		func = Integer.parseInt(JOptionPane.showInputDialog(null, 
						 funclist+"\nPlease choose a function: ",
		    "Stack", JOptionPane.QUESTION_MESSAGE));
		
		return func; 
	}
}
