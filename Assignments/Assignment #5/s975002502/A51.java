package a5.s975002502;
import java.util.*;

public class A51 {
	public static void main(String[] args) {
		final int LIMIT = 100;	// the limitation of the stack
		int count = 0;			// count the prime numbers' number
		int number = 2;			// used to produce prime numbers
		int num = 0;			// the number user entered for push or pop
		int func = 0;			// the function user selected
		StackOfIntegers stack = new StackOfIntegers();
		Scanner input = new Scanner(System.in);
		
		// print the function list
		System.out.print("1.push\n" +
						 "2.pop\n" +
						 "3.Show all\n" +
						 "4.Get size\n" +
						 "5.Reset\n" +
				  		 "6.exit\n");
		
		System.out.println("\nPlease choose a function: ");
		func = input.nextInt();
		
		while(func!=6){
			switch(func){
				/** the push function */
				case 1:
					System.out.print("push ");
					num = input.nextInt();
					
					// if the push number is larger than the limitation
					// or stack is full do not push the number into the stack
					if(num>LIMIT || stack.isFull()){
						System.out.println("The stack is FULL!!");
					}
					// do push
					else{
						count = 0;
						while(count<num){
							// if stack is full do not push the number into the stack
							if (stack.isFull()){
								System.out.println("The stack is FULL!!");
								break;
							}
							// Repeatedly find prime numbers and push them to stack
							if (isPrime(number)) {
								stack.push(number);
								count++; // Increase the prime number count
							}
							number++;
						}
						stack.showAll();
					}

					System.out.println("\nPlease choose a function: ");
					func = input.nextInt();
					break;
					
				/** the pop function */
				case 2:
					// if stack is not empty do pop
					if(!stack.isEmpty()){
						System.out.print("pop ");
						num = input.nextInt();
						
						stack.Pop(num);
						number = stack.peek()+1;
						stack.showAll();
					}
					// if the stack is empty do not pop
					else{
						System.out.println("The stack is EMPTY!!");
					}
					
					System.out.println("\nPlease choose a function: ");
					func = input.nextInt();
					break;
					
				/** the show all function */
				case 3:
					System.out.println("the stack here is :");
					stack.showAll();
					
					System.out.println("\nPlease choose a function: ");
					func = input.nextInt();
					break;
					
				/** the Get size function */
				case 4:
					System.out.println("the stack size is : " + stack.getSize());
					
					System.out.println("\nPlease choose a function: ");
					func = input.nextInt();
					break;
					
				/** the Reset function */
				case 5:
					System.out.println("the stack is RESET!! ");
					stack.empty();
					number = 2;
					
					System.out.println("\nPlease choose a function: ");
					func = input.nextInt();
					break;
					
				/** no SUCH function */
				default:
					System.out.println("no SUCH function!!");
					
					System.out.println("\nPlease choose a function: ");
					func = input.nextInt();
					break;
			}
		}
		
		/** exit function */
		System.out.println("Bye-bye~");
	}

	/** isPrime(int number): Check the number whether it is Prime or not. */
	public static boolean isPrime(int number) {
		// Assume the number is prime
		boolean isPrime = true;

		for (int divisor=2; divisor <= number/2; divisor++) {
			//If true, the number is not prime
		    if (number%divisor == 0) {
		    	// Set isPrime to false, if the number is not prime
		    	isPrime = false;
		    	break;
		    }
		}

	    return isPrime;
	}
}
