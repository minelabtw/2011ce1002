package a5.s100502515;

public class StackOfIntegers {

	private static int[] stack = new int[100];

	public void showAll() {
		System.out.println();
		for (int i = 0; i < 100; i++) {
			if (stack[i] != 0)
				System.out.print(stack[i] + " ");
		}
		System.out.println();
	}

	public void pop(int num) {
		for (int j = 0; j < 100; j++) {
			if (stack[j] == 0) {
				for (int k = 1; k <= num; k++) {
					stack[j - k] = 0;
				}
				break;
			}
		}
		for (int i = 0; i < 100; i++) {
			if (stack[i] != 0)
				System.out.print(stack[i] + " ");
		}
		System.out.println();
	}

	private boolean checkPrime(int num) {
		for (int l = 2; l < num; l++) {
			if (num % l == 0)
				num = -1;
		}
		if (num == -1)
			return false;
		else
			return true;
	}

	public void push(int num) {
		int times = 0;

		for (int j = 2;; j++) {
			if (checkPrime(j) == true) {
				stack[times] = j;
				times++;
			}
			if (times == num)
				break;
		}
		for (int i = 0; i < 100; i++) {
			if (stack[i] != 0)
				System.out.print(stack[i] + " ");
		}
		System.out.println();

	}

	public boolean isEmpty() {
		if (stack[0] == 0)
			return true;
		else
			return false;
	}

	public boolean isFull() {
		if (stack[99] != 0)
			return true;
		else
			return false;
	}

}
