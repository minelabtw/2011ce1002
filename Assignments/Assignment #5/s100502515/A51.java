package a5.s100502515;

import java.util.Scanner;

public class A51 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		StackOfIntegers stackOfIntegers = new StackOfIntegers();

		System.out.println("1.Push\n2.Pop\n3.Show all\n4.Exit");

		boolean flag = true;

		while (flag == true) {
			System.out.println("Your choice: ");
			int choice = input.nextInt();

			switch (choice) {
			case 1:
				System.out.print("How many prime numbers you want:");
				int pushNums = input.nextInt();
				stackOfIntegers.push(pushNums);
				if (stackOfIntegers.isEmpty() == true)
					System.out.println("It's empty.");
				else if (stackOfIntegers.isFull() == true)
					System.out.println("It's full.");
				break;

			case 2:
				System.out.print("How many prime numbers you want to pop:");
				int popNums = input.nextInt();
				stackOfIntegers.pop(popNums);
				if (stackOfIntegers.isEmpty() == true)
					System.out.println("It's empty.");
				else if (stackOfIntegers.isFull() == true)
					System.out.println("It's full.");
				break;

			case 3:
				System.out.println("Show all numbers:");
				stackOfIntegers.showAll();
				if (stackOfIntegers.isEmpty() == true)
					System.out.println("It's empty.");
				else if (stackOfIntegers.isFull() == true)
					System.out.println("It's full.");
				break;

			case 4:
				flag = false;
				System.out.println("Thanks for using.");
				break;
			}

		}

	}

}
