package a5.s100502510;

import java.util.Scanner;

public class A51 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		int choice;// 用來做為選擇的依據
		int numbers;// 用來儲存輸入的數
		int numbertwo;// 用來儲存pop的數

		while (1 == 1) {

			System.out.println("1.push:");
			System.out.println("2.pop:");
			System.out.println("3.Show all:");
			System.out.println("4.exit:");
			System.out.print("Please input your choice:");
			choice = input.nextInt();

			switch (choice) {
			case 1:
				System.out.print("Please input numbers:");
				numbers = input.nextInt();

				StackOfIntegers.Push(numbers);
				if (StackOfIntegers.isFull()) {
					System.out.println("The stack is full!");// 顯示iffull的訊息
					break;
				}
				break;

			case 2:
				System.out.print("Please input numbers:");
				numbertwo = input.nextInt();

				StackOfIntegers.Pop(numbertwo);// 使用pop
				if (StackOfIntegers.isEmpty()) {
					System.out.println("The stack is empty!");
					break;
				}

				break;
			case 3:
				StackOfIntegers.showAll();
				break;
			case 4:
				System.exit(0);// 結束這回合

			}
		}

	}
}
