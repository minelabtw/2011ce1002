package a5.s992001033;
import java.util.Scanner;
import java.lang.Math;

public class A51
{
	static StackOfIntegers testStack = new StackOfIntegers(100);//建立object
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		boolean exitFlag = false;//確認離開的boolean
		while(exitFlag!=true)
		{
			System.out.println("1.push");
			System.out.println("2.pop");
			System.out.println("3.Show all");
			System.out.println("4.exit");
			System.out.print("Enter the choose number : ");
			int choose = input.nextInt();
			int amount = 0;
			switch(choose)
			{
				case 1:
					System.out.print("push ");
					amount = input.nextInt();
					if(amount<=0)//輸入不大於0，會發生錯誤
					{
						error();
						break;
					}
					Push(amount);
					break;
				case 2:
					System.out.print("pop ");
					amount = input.nextInt();
					if(amount<=0)//輸入不大於0，會發生錯誤
					{
						error();
						break;
					}
					Pop(amount);
					break;
				case 3:
					System.out.println("show all");
					System.out.println("the stack here is : ");
					testStack.showAll();
					break;
				case 4:
					exitFlag=true;
					break;
				default:
					
					break;
			}
		}
	}
	public static void Pop(int num)//由StackOfIntegers的pop和showall組成
	{
		testStack.Pop(num);
		testStack.showAll();
	}
	public static void Push(int num)
	{
		int count = 0;//找到幾個質數
		int primeNumber = 0;//質數值
		if(testStack.isEmpty())//如果是空的，初始質數為2
			primeNumber = 2;
		else//如果已經有，從最後一個質數的下一個開始找
			primeNumber = testStack.getStack(testStack.getSize())+1;
		while(count!=num)//還沒找所需數量質數，就繼續找
		{
			if(isPrimeNumber(primeNumber))//找到
			{
				testStack.Push(primeNumber);//push進去並count+1
				count += 1;
			}
				primeNumber += 1;
		}
		testStack.showAll();
	}
	public static boolean isPrimeNumber(int number)//確認是否為質數的method
	{
		for(int i=2;i<=Math.sqrt(number);i++)
		{
			if(number%i==0)
				return false;
		}
		return true;
	}
	public static void error()//錯誤訊息用
	{
		System.out.println("The number is illegal.");
	}
}
