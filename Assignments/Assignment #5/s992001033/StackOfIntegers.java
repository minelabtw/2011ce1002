package a5.s992001033;

public class StackOfIntegers
{
		private int[] elements;
		private int size = 0;
		public StackOfIntegers()//沒設定的話，預設空間為200
		{
			elements = new int[200];
		}
		public StackOfIntegers(int capacity)
		{
			elements = new int[capacity];
		}
		public void showAll()
		{
			if(isEmpty())
				System.out.println("the stack is empty!!");
			else if(isFull())
			{
				for(int i=0;i<size;i++)
				{
					if(i==0)
						System.out.print(elements[i]);
					else
						System.out.print("、"+elements[i]);
				}
				System.out.println();
				System.out.println("the stack is full!!");
			}
			else//非空非滿
			{
				for(int i=0;i<size;i++)
				{
					if(i==0)
						System.out.print(elements[i]);
					else
						System.out.print("、"+elements[i]);
				}
			}
			System.out.println();
		}
		public void Pop(int num)
		{
			if(size-num>=0)//不會超過stack內的數量時
			{
				for (int i=size;i>size-num;i--)
					elements[i-1]= 0;
				size -= num;
			}
			else
			{
				for (int i=size;i>0;i--)
					elements[i-1]= 0;
				size = 0;
			}
		}
		public void Push(int num)
		{
			if(size<elements.length)
			{
				elements[size] = num;
				size += 1;
			}
		}
		public boolean isEmpty()
		{
			if(size==0)
				return true;
			else
				return false;
		}
		public boolean isFull()
		{
			if(size==elements.length)
				return true;
			else
				return false;
		}
		public int getStack(int index)//用來抓stack內的值
		{
			if(index==0)
				return elements[index];
			return elements[index-1];
		}
		public int getSize()//用來抓stack已有的element數量
		{
			return size;
		}
}
