package a5.s100502505;

import java.util.Scanner;

public class A51 {
	public static void main(String args[])
	{
		StackOfIntegers stack = new StackOfIntegers();//宣告物件
		int terms,num;//宣告變數
		while(true)//無限迴圈
		{
			Scanner input = new Scanner(System.in);
			System.out.println("1.push");
			System.out.println("2.pop");
			System.out.println("3.Show all");
			System.out.println("4.exit");
			System.out.print("請輸入你要哪一個:");
			terms = input.nextInt();//輸入選項
			switch(terms)
			{
				case 1: System.out.print("請輸入一個數字:");
						num = input.nextInt();
						stack.Push(num);//PUSH的功能
						break;
				case 2: System.out.print("請輸入一個數字:");
						num = input.nextInt();
						stack.Pop(num);//Pop的功能
						break;
				case 3: stack.showAll();
						break;//秀出全部
				case 4: System.exit(0);//離開
			}
		}
	}
}//完成!
