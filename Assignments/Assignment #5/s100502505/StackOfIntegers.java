package a5.s100502505;

public class StackOfIntegers {
	private static int[] array = new int[100];//宣告陣列
	private static int size = 0;//宣告項數

	public static void showAll() {//秀出全部
		if (size == 0) {
			System.out.println("the stack is empty!!");
		} else {
			System.out.println("the stack here is :");
			for (int i = 0; i < size; i++) {//秀出所有的質數
				System.out.print(array[i] + " ");
			}
			System.out.println("");
		}
	}
	public static void Pop(int num) {//刪掉質數
		if (size > num) {
			size = size - num;
			for (int i = 0; i < size; i++) {
				System.out.print(array[i] + " ");
			}
			System.out.println("");
		} else {
			System.out.println("the stack is empty!!");
			size = 0;
		}
	}
	public static void Push(int num) {//增加質數
		size = size + num;
		int prime1 = 2;
		int time;
		int terms = 0;
		for (int i = 2; i <= prime1; i++) {//判斷是否為質數
			time = 0;
			for (int j = 2; j <= prime1; j++) {
				if ((prime1 % j) == 0) {
					time++;
				}
			}
			if (time == 1) {
				array[terms] = prime1;
				terms = terms + 1;
				prime1 = prime1 + 1;
			} else if (time != 1) {
				prime1 = prime1 + 1;
			}
			if (terms == 100) {
				break;
			}
		}
		for (int i = 0; i < size; i++) {//秀出質數
			System.out.print(array[i] + " ");
		}
		System.out.println("");
	}
	public static boolean isEmpty() {//如果陣列是空的
		if (size == 0) {
			return true;
		} else {
			return false;
		}
	}
	public static boolean isFull() {//如果陣列是滿的
		if (size >= 100) {
			return true;
		} else {
			return false;
		}
	}
}//完成!
