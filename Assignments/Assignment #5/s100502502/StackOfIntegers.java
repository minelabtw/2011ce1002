//StackOfIntegers.java
package a5.s100502502;
public class StackOfIntegers {
	private int[] Pnumber = new int[100];
	private int counter = 0;
	StackOfIntegers(){//initialize object
		
	}
	public void showAll(){
		for(int i = 0; i < counter; i++){
			System.out.print(Pnumber[i] + " ");
		}
		System.out.println("\n");
	}
	public void Pop(int num){//pop such number of elements from the stack
		counter = counter - num;
	}
	public void Push(int num){//push such number of elements from the stack
		counter = counter + num;
	}
	public boolean isEmpty(){//whether it empty or not
		return counter == 0;
	}
	public boolean isFull(){//whether it full or not
		return counter >= 100;
	}
	public void Pnum(){//save Prime numbers to an array
		int temp = 2;
		Pnumber[0] = 2;
		Pnumber[1] = 3;
		for(int i = 4; temp < 100; i++){
			int N = 0;
			for(int j = 2; j <= (int)(Math.sqrt(i)); j++){
				if(i % j == 0){
					break;
				}
				else if(i % j != 0){
					N++;
					if(N == (int)(Math.sqrt(i))-1){
						Pnumber[temp] = i;
						temp++;
						break;
					}
				}
				else{}
			}
		}
	}
}
