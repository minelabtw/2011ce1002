//A51.java
package a5.s100502502;
import java.util.Scanner;
public class A51 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		StackOfIntegers stack = new StackOfIntegers();
		System.out.println("1.push");//show list
		System.out.println("2.pop");
		System.out.println("3.Show all");
		System.out.println("4.exit");
		stack.Pnum();//save Prime numbers
		boolean ON = true;//control while loop
		while(ON){
			System.out.print("Choose number :");
			int choose = input.nextInt();//save user's choice
			switch(choose){
				case 1:
					System.out.print("The number you want to push : ");
					int PU = input.nextInt();
					if(stack.isFull()){//if the account of prime number > 100
						System.out.println("The stack is full!!");
					}
					else{
						stack.Push(PU);
					}
					stack.showAll();//show all elements
					break;
				case 2:
					System.out.print("The number you want to pop : ");
					stack.Pop(input.nextInt());
					if(stack.isEmpty()){//if the stack is empty
						System.out.println("The stack is empty!!");//show message
					}
					else{
						stack.showAll();
					}
					break;
				case 3:
					System.out.println("The stack here is :");
					stack.showAll();
					break;
				case 4:
					System.out.println("Bye~");
					ON = false;
					break;
			}
		}
	}	
}
