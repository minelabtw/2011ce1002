package a5.s100502518;

public class StackOfIntegers {
		private static int stack[]=new int [100];
		private static int count=0;//用來計數
		
		public boolean isEmpty()//判斷是否空的
		{
			if(count<=0)
			{
				return true;
			}
			
			else 
			{
				return false;
			}
		}
		
		public boolean isFull() //判斷是否滿的
		{
			if(count>=100)
			{
				return true;
			}
			
			else
			{
				return false;
			}
		}
		
		public void showAll()
		{
			for(int a=0,b=count;b>0;b--,a++)//與count配合顯示出全部
			{
				System.out.print(stack[a]+" ");
			}
		}
		
		public void push(int a)//把值傳入並計數
		{
			stack[count]=a;
			count=count+1;
		}
		
		public void pop(int a)//扣掉
		{
			count=count-a;
			if(count<0)
			{
				count=0;
			}
		}
		
		public int getcount()//回傳計到多少了
		{
			return count;
		}
}
