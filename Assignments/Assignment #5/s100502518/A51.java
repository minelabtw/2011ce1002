package a5.s100502518;

import java.util.Scanner;
public class A51 {
	public static void main(String[] args)
	{
		int []store=new int [100];//把質數都算出來
		int a=0;
		int num=2;
		while(true)
		{
			int c=0;
			for(int b=1;b<=Math.pow(num, 0.5);b++)
			{
				if(num%b==0)
				{
					c++;
				}
			}
			
			if(c==1)
			{
				store[a]=num;
				a++;
			}
			
			num++;
			
			if(a==100)
			{
				break;
			}
		}
		
		StackOfIntegers stack=new StackOfIntegers();
		
		int d=1;
		while(d!=4)
		{
			System.out.print("\n1.push\n2.pop\n3.Show all\n4.exit\n請輸入選擇: ");
			Scanner input=new Scanner(System.in);
			d=input.nextInt();
			
			int e=0;
			switch (d) 
			{
				
			case 1://把質數傳進class，並且會堆疊
			{
				System.out.print("請輸入一個數: ");
				e=input.nextInt();
				for(;e>=1;e--)
				{
					if(stack.isFull())
					{
						break;
					}
					stack.push(store[stack.getcount()]);
				}
				if(stack.isEmpty())
				{
					System.out.print("the stack is empty!!");
				}
				stack.showAll();
				break;
			}
				
			case 2://看要丟掉多少
			{
				System.out.print("請輸入一個數: ");
				stack.pop(e=input.nextInt());
				if(stack.isEmpty())
				{
					System.out.print("the stack is empty!!");
				}
				else 
				{
					stack.showAll();
				}
				break;
			}
			
			case 3://顯示全部
			{
				if(stack.isEmpty())
				{
					System.out.print("the stack is empty!!");
				}
				stack.showAll();
				break;
			}
			
			case 4://跳出
			{
				break;
			}
					
			default:
			{
				break;
			}
			}
		}
	}

}
