package a5.s100502017;

public class StackOfIntegers {
	private int[] stack;
	private int member;
	public StackOfIntegers(){//Constructor to new the private data
		stack=new int[100];
		member=0;
	}
	public void showAll(){//Show all the elements in stack
		if(!isEmpty()){
			for(int i=0;i<member;i++){
				System.out.print(stack[i]+" ");
			}
			System.out.println("");
		}
		else
			System.out.println("the stack is empty!!");
	}
	public void Pop(int num){//Take input as argument, and pop such number of elements from the stack
		if(num<=member){
			for (int j=member-1;j>=member-num;j--){
				stack[j]=0;
			}
			member-=num;
		}
		else{//if element not enough ,let member=0
			member=0;
			stack=new int[100];
			System.out.println("element not enough,it is emply now,can't pop more");
		}
	}
	public void Push(int num){//Take input as argument, and push the input into the stack
		if(!isFull()){
			stack[member]=num;
			member++;
		}
		else//if stack is full print error
			System.out.println("push error,stack is full");
	}
	public boolean isEmpty(){//Check the stack whether it is empty or not
		return member==0;
	}
	public boolean isFull(){//Check the stack whether it is full or not
		return member==100;
	}
	public int PeekUp(){//return the up element
		if(member>0)
			return stack[member-1];
		else
			return -1;
	}
}
