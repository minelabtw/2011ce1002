package a5.s100502017;

import java.util.Scanner;

public class A51 {
	public static void main (String[] args){
		Scanner input=new Scanner(System.in);
		boolean key=true;
		StackOfIntegers stack=new StackOfIntegers();// declare a object
		while(key){//use key to determine whether the loop continue or not
			System.out.println("1.push\n2.pop\n3.Show all\n4.exit");
			System.out.print("choise functionality: ");
			int choise=input.nextInt();
			switch(choise){//use switch statement to determine the choice
				case 1://let user input a number and push such number of elements into stack Show the result after push
					System.out.print("push ");
					int push=input.nextInt();
					for(;push>0;push--){
						if(stack.isFull()){//if stack is full exit the loop
							System.out.println("the stack is full");
							break;
						}
						else if(stack.isEmpty()){
							stack.Push(GetNextPrime(1));
						}
						else{
							stack.Push(GetNextPrime(stack.PeekUp()));
						}
					}
					stack.showAll();
					break;
				case 2://let user input a number and pop such number of elements from stack. Show the result after pop
					System.out.print("pop ");
					int pop=input.nextInt();
					stack.Pop(pop);
					stack.showAll();
					break;
				case 3://show the current elements in the stack
					System.out.println("the stack here is ");
					stack.showAll();
					break;
				case 4://exit
					key=false;
					break;
				default:
					System.out.println("The number is wrong!!");
					break;
			}	
		}
		System.out.println("Thank for using!!");
	}
	public static int GetNextPrime(int Previous){//to get next prime
		if(Previous<2){//the smallest prime is 2
			return 2;
		}
		for(int i=Previous+1;true;i++){//use two for loop to determine prime or not
			for(int j=i-1;j>1;j--){
				if(i%j==0){
					break;
				}
				else if(j==2){
					return i;
				}
			}
		}
	}
}
