package a5.s100502007;

public class StackOfIntegers {
	private int[] elements;// array of stacks
	private int size;// size of array
	public static final int DEFAULT_CAPACITY=100;//max
	
	public StackOfIntegers(){
		this(DEFAULT_CAPACITY);
	}
	public StackOfIntegers(int capacity){
		elements = new int[capacity];
	}
	public void push(int num){
		size+=num;	
		if(isFull()==true){//check if full or not
			System.out.println("the stack is full!!");
		}
		else{//if not full,calculate prime number
			int i=2,k=0;
			while(k<size){
				int count=0,j=1;
				while(j<=i){
					if(i%j==0){
						count++;
						j++;
					}
					else{
						j++;
					}			
				}
				if(count==2){
					elements[k]=i;
					k++;
				}
				i++;		
			}
		}
		System.out.println(ShowAll());
	}
	public void pop(int num){
		if (isEmpty()==false) {//if not empty
			size -= num;
			if (isEmpty()==true) {//if empty after minus
				size = 0;
				System.out.println("the stack is empty!!");
			}
			else{
				System.out.println(ShowAll());
			}
		}
		else{//if empty
			System.out.println("the stack is empty!!");
		}
	}
	public String ShowAll(){
		String string=" ";
		for(int i=0;i<size;i++){//plus every number into a string
			string+=elements[i]+" ";
		}
		return string;
	}
	public boolean isEmpty(){//check empty
		if(size<=0){
			return true;
		}
		else{
			return false;
		}
	}
	public boolean isFull(){//check full
		if(size==100){
			return true;
		}
		else{
			return false;
		}
	}
	
}
