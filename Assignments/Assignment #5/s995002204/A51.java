package a5.s995002204;

import java.util.Scanner;

public class A51 {
	public static void main(String args[])
	{
		StackOfIntegers StackOfIntegers1 = new StackOfIntegers();	//declare a class
		Scanner input = new Scanner(System.in);
		int decision = 0;
		int number = 0;
		
		// let user choose what he want to do
		do
		{
			System.out.println("1.push"); 
			System.out.println("2.pop");
			System.out.println("3.Show all");
			System.out.println("4.exit");
			decision = input.nextInt();
			
			switch(decision)
			{
			
			case 1:	//push
				number = input.nextInt();
				StackOfIntegers1.Push(number);
				break;
			case 2:	//pop
				number = input.nextInt();
				StackOfIntegers1.Pop(number);
				break;
			case 3:	//show
				StackOfIntegers1.showAll();
				break;
			default://leave loop
				break;
			}
			
		}while(decision!=4);
	}
}
