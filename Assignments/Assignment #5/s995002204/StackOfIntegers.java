package a5.s995002204;

public class StackOfIntegers {
	
	private static int top;	//it is like a pointer
	private static int Max = 100;	//stack's limit
	private static int item[] = new int[Max];	//declare an array
	
	
	StackOfIntegers()
	{
		top = -1;
		for(int i=0;i<100;i++)
		{
			item[i] = 0;
		}
	}
	
	public static void showAll()	//show stack's element
	{
		if (isEmpty())
			System.out.println("the stack is empty!!");
		else
		{
			System.out.print("the stack here is :");
			for(int i=0;i<=top;i++)
			{
				System.out.print(item[i]+" ");
			}
			System.out.println();
		}
	}
	
	public static void Pop(int num)		//pop element
	{
		if(isEmpty())
			System.out.println("the stack is empty!!");
		else
		{
			for(int i=0;i<num;i++)
			{
				item[top]=0;
				top--;
			}
		}
	}
	
	public static void Push(int num)	//push prime number to the stack
	{
		int count=0;	//count how many prime number put into stack
		int k=0;
		if(isFull())	//make sure that stack is full or not
			System.out.println("the stack is full!!");
		else
		{
			if(top<0)	//when top is less than zero(����)
			{
				int s=0;
				do
				{
					s = s+1;	//initial value
					for(int j=1;j<=s;j++)	//examine every number which is less or equal to variable s
											//can be the variable s 's factor
					{
						if(s%j==0)	//j is variable s's factor
							k++;
					}
					if(k==2)	//prime number has two factor just 1 and itself
					{
						top++;
						item[top]=s;
						count++;	//count how many element are pushed into stack
					}
					k=0;	//initialize
				}while(num!=count);	
				count = 0;
			}
			else	//when top is not negative number(����)
			{
				int s=item[top];
				do
				{
					s+=1;
					for(int j=1;j<=s;j++)
					{
						if(s%j==0)
							k++;
					}
					if(k==2)
					{
						top++;
						item[top]=s;
						count++;
					}
					k=0;
				}while(num!=count);
				count = 0;
			}
		}
	}
	
	public static boolean isEmpty()	//examine stack is empty or not
	{
		if (top<0)
			return true;
		else
			return false;
	}
	
	public static boolean isFull()	//examine stack is full or not
	{
		if (top>=Max-1)
			return true;
		else
			return false;
	}
}
