package a5.s100502002;

public class StackOfIntegers
{	
	private int min; 
	private int add; 
	private int now; //設變數
	private int[] Prime;
	private int[] ans;//設陣列
	public  StackOfIntegers()//初始化所有東西
	{ 
		int[] Prime2=new int[100];
		int th=0;
		ans=new int[100];
		for(int i=2;th<100;i++)
		{ 
			if(isPrime(i))
			{
				Prime2[th]=i;
				th++;//計算次數
			}
		}
		Prime=Prime2;
		now=0;
	}
	public void showall(){ 
		if(!isempty())//如果不是空的就show初結果
		{
			for(int i=0;i<now;i++)
			{
				if(ans[i]!=0)
				{
					System.out.print(ans[i]+" ");
				}
				if((i+1)%10==0)
					System.out.print("\n");
			}
		}
		else//如果是空的就顯示空
			System.out.println("The stack is empty!!");
	}
	public void push(int push1)//加數字
	{ 
		add=push1;
		now=now+add;
		if(now>100)
		{
			now = 100;
		}
		if(!isfull())
		{
			for(int i=0;i<now;i++)
			{
				ans[i]=Prime[i];//把質數存進去
			}
			showall();
		}
		
		else
		{
			now = 100;
			System.out.println("the stack is full!!!!but,you still can see 100 terms");
			showall();
		}
	}
	public void pop(int pop1)
	{ 
		min=pop1;
		now=now-min;
		if(!isempty())
		{
			showall();
		}//如果不是空的印出結果
		else
		{
			now=0;
			System.out.println("The stack is empty.");
		}
	}
	
	public boolean isempty()//0為空，判斷是否有0
	{ 
		for(int i=0;i<now;i++)
		{
			if(ans[i]!=0)
				return false;
		}
		return true;
	}
	public boolean isfull()//判斷是否沒有0，跟empty相反
	{
		for(int i=0;i<100;i++)
		{
			if(ans[i]==0)
			{
				return false;	
			}
		}
		return true;
			
	}
	public boolean isPrime(int num)//製作質數
	{ 
		for(int i=2;i<=Math.sqrt(num);i++)
		{
			if(num%i==0)
				return false;	
		}
		return true;
	}

}

