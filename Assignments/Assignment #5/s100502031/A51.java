package a5.s100502031;

import java.util.Scanner; 

public class A51 {
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);
		//clain the class StackOfIntegers
		StackOfIntegers stack=new StackOfIntegers();
		System.out.println("The intital element of the stack is 0 !!");
		//set the exist flag
		boolean ExitFlag=false;
		//to perform repeatly
		while(ExitFlag==false){
			System.out.println("Chose the function :");
			System.out.println("1.push;2.pop;3.Show all;4.exit");
			int chose=input.nextInt();
			switch(chose){
			//push
			case 1:
				System.out.println("Input an integer: ");
				int INinteger=input.nextInt();
				stack.Push(INinteger);
				break;
			//pop
			case 2:
				System.out.println("Input an integer: ");
				int INinteger2=input.nextInt();
				stack.Pop(INinteger2);
				break;
			//show all elements
			case 3:
				stack.showAll();
				break;
			//exist	
			case 4:
				ExitFlag=true;
				System.out.println("Thank you !!!");
				break;
			}
		}
	}
}
