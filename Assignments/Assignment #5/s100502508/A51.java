package a5.s100502508;

import java.util.Scanner;
public class A51 
{
	public static void main(String[] args)
	{
		Scanner input=new Scanner(System.in);
		StackOfIntegers stackOfIntegers=new StackOfIntegers();//new a StackOfIntegers object
		System.out.print("1.push\n"+"2.pop\n"+"3.Show all\n"+"4.exit\n");
		boolean flag=true;
		while(flag)//repeat
		{
			int choose=input.nextInt();
			switch(choose)//categorize
			{
				case 1://let user input a number and pop such number of elements from stack
					System.out.print("push ");
					choose=input.nextInt();
					if(choose<0)
					{
						System.out.println("Please enter a non-negative number");
					}
					else
					{
						System.out.println(stackOfIntegers.push(choose));
					}
					flag=true;
					break;
				case 2://let user input a number and push such number of elements into stack
					System.out.print("pop ");
					choose=input.nextInt();
					if(choose<0)
					{
						System.out.println("Please enter a non-negative number");
					}
					else
					{
						System.out.println(stackOfIntegers.pop(choose));
					}
					flag=true;
					break;
				case 3://show the current elements in the stack
					System.out.println("show all\nthe stack here is :");
					System.out.println(stackOfIntegers.showAll());
					flag=true;
					break;
				case 4://exit
					flag=false;
					System.out.println("Good bye!!");
					break;
				default:
					System.out.println("Enter 1 to 4");
					break;
			}//end switch
		}//end while
	}//end main
}
