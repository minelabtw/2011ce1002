package a5.s100502508;

public class StackOfIntegers 
{
	private int[] elements;
	private int item;
	public static final int ARRAYSIZE=100; 
	
	public StackOfIntegers()//constructer a stack with the arraysize 100
	{
		this(ARRAYSIZE);
		item=0;
	}
	
	public StackOfIntegers(int capacity)//constructer a stack with the specified maximun capacity
	{
		elements=new int[capacity];
	}
	
	public String push(int term)//Take input as argument, and push the input into the stack
	{
		plusitem(term);//call plusitem method
		String primenumbers=new String();
		if(isFull())
		{
			if(item>100)//item is beyond the maximun
			{
				minusitem(item-100);//call minusitem method
				PrimeNumbers(item);
				for(int a=0;a<item;a++)
				{
					primenumbers=primenumbers+elements[a]+" ";
				}
				return primenumbers;
			}
			else//item is equal to the maximun
			{
				PrimeNumbers(item);
				for(int a=0;a<item;a++)
				{
					primenumbers=primenumbers+elements[a]+" ";
				}
				return primenumbers;
			}
		}
		else//item is less the maximun
		{
			if(item==0)
			{
				return "the stack is empty!!";
			}
			else
			{
				PrimeNumbers(item);//call PrimeNumbers method
				for(int a=0;a<item;a++)
				{
					primenumbers=primenumbers+elements[a]+" ";
				}
				return primenumbers;
			}
		}
	}
	
	public String pop(int term)//Take input as argument, and pop such number of elements from the stack
	{
		minusitem(term);//call minusitem method
		String primenumbers=new String();
		if(isEmpty())
		{
			if(item<0)//item is less zero
			{
				plusitem(0-item);//call plusitem method
				return "the stack is empty!!";
			}
			else//item is equal to zero
			{
				return "the stack is empty!!";
			}
		}
		else
		{
			PrimeNumbers(item);//call PrimeNumbers method
			for(int a=0;a<item;a++)
			{
				primenumbers=primenumbers+elements[a]+" ";
			}
			return primenumbers;
		}
	}
	
	public String showAll()//Show all the elements in stack
	{
		String primenumbers=new String();
		if(isEmpty())//item is equal to zero
		{
			return "the stack is empty!!";
		}
		else//item isn't equal to zero 
		{
			for(int a=0;a<item;a++)
			{
				primenumbers=primenumbers+elements[a]+" ";
			}
			return primenumbers;
		}
	}
	
	public boolean isEmpty()//judge if item is equal to zero
	{
		if(item<=0)
			return true;
		else
			return false;
	}
	
	public boolean isFull()//judge if item is equal to maximun
	{
		if(item>=ARRAYSIZE)
			return true;
		else
			return false;
	}
	
	public void plusitem(int size)//plus the item
	{
		item=item+size;
	}
	
	public void minusitem(int size)//minus the item
	{
		item=item-size;
	}
	
	public void PrimeNumbers(int term)//find prime numbers and save them into array  
	{
		
		int size=0;
		for(int number=1;number>=0;number++)
		{
			int count=0;
			for(int check=1;check<=number;check++)
			{
				if(number%check==0)
				{
					count++;
				}
			}//end for
			if(count==2)
			{
				elements[size]=number;
				size++;
				if(size==term)
					break;
			}
		}
	}//end PrimeNumbers
}
