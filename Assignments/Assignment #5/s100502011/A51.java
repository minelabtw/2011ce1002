package a5.s100502011;
import java.util.Scanner;

public class A51 {
	public static void main(String[] args){
		StackOfIntegers stack = new StackOfIntegers();
		boolean flag = true; // to determine loop run or not
		Scanner input = new Scanner(System.in);
		while(flag){ // infite input
			System.out.print("1.push\n"+"2.pop\n"+"3.Show all\n"+"4.exit\n"+"Which function do you want : ");
			int chos = input.nextInt(); // the function you want to choose
			switch(chos){
				case 1: //push
					System.out.print("How many numbers do you want to push :");
					int punum =input.nextInt(); //to determine how many you want to push
					System.out.println("push " + punum);
					stack.Push(punum);
					break;
				case 2: //pop
					System.out.print("How many numbers do you want to pop :");
					int popnum =input.nextInt(); //to determine how many you want to pop
					System.out.println("pop "+ popnum);
					stack.Pop(popnum);
					break;
				case 3: //show all
					System.out.println("The stack here is :");
					stack.showAll();
					break;
				case 4: //exit
					flag = false;
					break;
			} // end switch
		} // end while loop	 
	}// end main
	
}
