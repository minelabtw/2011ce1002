package a5.s100502011;

public class StackOfIntegers {
	private static int[] Store = new int[100];
	public static int quan = 0; // the size of array
	public static String answer =""; //the String of answer
	public StackOfIntegers(){ // default constructer
		
	}
	
	public void showAll(){ // show the stack
		System.out.print(answer+"\n");
	}
	
	public void Pop(int num){ // decrease the number in stack
		answer ="";
		for(int i=0;i<quan-num;i++){
			answer = answer + Store[i]+" ";
		}
		quan -= num; //size =size-num
		if(quan <0) // negative number
			quan=0;
		if(isEmpty()) // no number in stack
			System.out.println("The stack is empty");
		else
			System.out.println(answer);
	 } // end pop
	
	public void Push(int num){ // increase the number to stack
		if(quan==0){ // initial
			if(num>100) // out of size
				num = 100;
			Store =CalPrimeNum(num); // store prime to array
			quan=num; // size
			for(int i =0;i<num;i++){ // store to string
				answer = answer +Store[i]+" ";
			} // end for loop
			if(isFull()) // is full
				System.out.println("The stack is full");
			System.out.print(answer+"\n");

		} // end if
		else{ //second push
			quan=num+quan;
			if(quan>100) //out of size
				quan=100;
			Store = CalPrimeNum(quan);	// store prime to array
			answer="";
			for(int a=0;a<quan;a++){

				answer = answer + Store[a]+" ";
			}
			if(isFull()) // is full
				System.out.println("The stack is full");
			System.out.print(answer+"\n");
		} // end else
		
	} // end push
	
	public static boolean isEmpty(){ // determine it is empty or not
		if(quan<=0) // empty
			return true;
		else
			return false;
	}// end isEmpty
	
	public static boolean isFull(){ // determine it is full or not
		if(quan>=100) // full
			return true;
		else
			return false;
	} // end isFull
	
	public static int[] CalPrimeNum(int number){ // to cal the prime number
		int[] Numarr = new int[100];
		int count=0; // to count how many factor
	    int num =0; // to count size
	    for(int i=2;i>1;i++) // from 2 to determine it is prime number or not
	    {
	        count=0; // initial value
	        for(int j=1;j<=i;j++) // judge the factor
	        {
	            if(i%j ==0)//factor
	                count = count + 1; 
	        } // end for loop
	        if(count == 2){ // prime number
	        	 Numarr[num]=i; // store to array
	        	 num++; // size++
	        }
	        if(num ==number){ // break the loop
	        	break;
	        }
	    } // end for loop
	    return Numarr;
	} // end method

}
