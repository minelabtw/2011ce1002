package a5.s100502507;
import java.lang.Math;
import javax.swing.JOptionPane;
import java.util.LinkedList;
public class StackOfIntegers {
		
	StackOfIntegers() {//Constructor to initialize the stack
		stack = new LinkedList<Integer>();
	}
	
	public void showAll() {//Output all elements of stack
		if(isEmpty()) {
			JOptionPane.showMessageDialog(null, "The stack is empty!!", "A51", JOptionPane.INFORMATION_MESSAGE);
		}
		else {
			String output = new String("(");//A string to store the elements of stack
			for(int i=0; i<stack.size(); i++) {
				output += String.valueOf(stack.get(i));
				if(i==60) {//Avoid the frame exceeding the window
					output += "\n";
				}
				else if(i<stack.size()-1) {//Separate the elements
					output += ", ";
				}
				else {//End of output
					output += ")";
				}
			}
			if(isFull()) {//Inform user if the stack is full
				output += "\nThe stack is full!";
			}
			JOptionPane.showMessageDialog(null, output, "A51", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	public void Pop(int num) {//Popping out the top "num" elements of stack
		for(int i=0; i<num; i++) {
			if(!isEmpty()) {
				stack.removeLast();
			}
			else {
				break;
			}
		}
	}
	
	public void Push(int num) {//Add elements into stack
		for(int i=0; i<num; i++) {
			if(isEmpty()) {
				stack.addFirst(2);
			}
			else if(!isFull()) {
				stack.add(primeNumber(stack.getLast()+1));
			}
			else {
				break;
			}
		}
	}
	
	public boolean isEmpty() {//Check if the stack is empty or not
		if(stack.size()==0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean isFull() {//Check if the stack is full or not
		if(stack.size()>=DEFAULT_CAPACITY) {
			return true;
		}
		else {
			return false;
		}
	}
	
	private LinkedList<Integer> stack;
	private static final int DEFAULT_CAPACITY = 100;//Maximum capacity
	
	private int primeNumber(int start) {//Figure out a prime number bigger then the last element of stack
		while(!checkPrimeNumber(start)) {
			start += 1;
		}
		return start;
	}
	
	private boolean checkPrimeNumber(int input) {//Check if the number is a prime number or not
		for(int i=2; i<=(int)(Math.sqrt(input)*100.0)/100; i++) {
			if(input%i == 0) {
				return false;
			}
		}
		return true;
	}
}