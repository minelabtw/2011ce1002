package a5.s100502507;
import javax.swing.JOptionPane;
public class A51 {
	public static void main(String[] args) {//Function main begins execution
		StackOfIntegers stack = new StackOfIntegers();//Declare a stack
		boolean exit = false;//A boolean to decide to exit or not
		while(!exit) {
			String choice = JOptionPane.showInputDialog(null, "1.push\n2.pop\n3.Show all\n4.exit", "A51", JOptionPane.QUESTION_MESSAGE);//Let user has a choice
			switch(Integer.parseInt(choice)) {
				case 1://Pushing
					choice = JOptionPane.showInputDialog(null, "Push", "A51", JOptionPane.QUESTION_MESSAGE);
					if(Integer.parseInt(choice)<0) {
						JOptionPane.showMessageDialog(null, "Incorrect input!", "A51", JOptionPane.ERROR_MESSAGE);
						break;
					}
					stack.Push(Integer.parseInt(choice));
					stack.showAll();
					break;
				case 2://Popping
					choice = JOptionPane.showInputDialog(null, "Pop", "A51", JOptionPane.QUESTION_MESSAGE);
					if(Integer.parseInt(choice)<0) {
						JOptionPane.showMessageDialog(null, "Incorrect input!", "A51", JOptionPane.ERROR_MESSAGE);
						break;
					}
					stack.Pop(Integer.parseInt(choice));
					stack.showAll();
					break;
				case 3://Showing
					stack.showAll();
					break;
				case 4://Exiting
					exit = true;
					break;
				case ' ':
				case '\n':
				case '\t':
					break;
				default://Avoiding error
					JOptionPane.showMessageDialog(null, "Incorrect input", "A51", JOptionPane.ERROR_MESSAGE);
					break;
			}
		}
	}//End function main
}
