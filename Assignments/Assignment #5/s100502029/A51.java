package a5.s100502029;
import java.util.Scanner;

public class A51 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		StackOfIntegers stack = new StackOfIntegers();
		
		boolean exitFlag = true; // if true, execute the program; if false, exit program
		while (exitFlag) {
			// display functionality list
			System.out.println();
			System.out.println("1.push");
			System.out.println("2.pop");
			System.out.println("3.show all");
			System.out.println("4.exit");
			System.out.print("Choose a function: ");
			int choice = input.nextInt();
			
			switch (choice) {
				case 1: // ask user the number of elements that he want to push, and then execute and show all stack
					System.out.print("Enter the number of elements: ");
					int pushNumber = input.nextInt();
					if (pushNumber > stack.getRest()) {
						System.out.println("the number is larger than the rest of terms.");
						pushNumber = stack.getRest();
					}
					for (int i = 0; i < pushNumber; i++) {
						if (!stack.isEmpty())
							stack.push(nextPrimeNumber(stack));
						else
							stack.push(2);
					}
					stack.showAll();
					break;
				case 2: // ask user the number of elements that he want to pop. Execute and show all stack
					System.out.print("Enter the number of elements: ");
					int popNumber = input.nextInt();
					if (popNumber > stack.getSize()) {
						System.out.println("the number is larger than the current terms.");
						popNumber = stack.getSize();
					}
					stack.pop(popNumber);
					stack.showAll();
					break;
				case 3: // show all stack
					System.out.println("the stack here is:");
					stack.showAll();
					break;
				case 4: // exit the program
					exitFlag = false;
					break;
				default:
					System.out.println("Input Error!");
					break;
			}
		}
	}
	
	// calculate the next prime number of s top element
	public static int nextPrimeNumber(StackOfIntegers s) {
		int mark = 0;
		boolean flag = true; // flag will become false when program find the next prime number and exit the while loop
		while (flag) {
			flag = false;
			mark++;
			for (int j = 0; s.getElements(j) <= Math.sqrt(s.peek() + mark); j++) {
				if (((s.peek() + mark) % s.getElements(j)) == 0) {
					flag = true;
					break;
				}
			}
		}
		return s.peek() + mark;
	}
}
