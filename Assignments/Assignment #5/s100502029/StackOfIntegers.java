package a5.s100502029;

public class StackOfIntegers {
	private int[] elements;
	private int size = 0;
	public static final int DEFAULT_CAPACITY = 100;
	
	// Construct a stack with the default capacity 100
	public StackOfIntegers() {
		this(DEFAULT_CAPACITY);
	}
	
	// Construct a stack with the specified maximum capacity
	public StackOfIntegers(int capacity) {
		elements = new int[capacity];
	}
	
	// show all stack
	public void showAll() {
		if (!isEmpty()) {
			for (int i = 0; i < size; i++)
				System.out.print(elements[i] + " ");
		}
		else
			System.out.println("the stack is empty!!");
	}
	
	// remove the top element from the stack
	public void pop(int num) {
		size -= num;
	}
	
	// push a new integer into the top of the stack
	public void push(int num) {
		elements[size++] = num;
	}
	
	// test whether the stack is empty
	public boolean isEmpty() {
		return size == 0;
	}
	
	// test whether the stack is full
	public boolean isFull() {
		return size == elements.length;
	}
	
	// return the top element from the stack
	public int peek() {
		return elements[size - 1];
	}
	
	// return the input term of elements
	public int getElements(int term) {
		return elements[term];
	}
	
	// return the number of rest elements
	public int getRest() {
		return DEFAULT_CAPACITY - size;
	}
	
	// return the number of elements in the stack
	public int getSize() {
		return size;
	}
}
