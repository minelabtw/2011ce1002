package a5.s100502003;

import java.util.*;

public class A51 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		StackOfIntegers Stack = new StackOfIntegers(); //declare an object of StackOfIntegers
		System.out.println("The system have the following functions:");
		System.out.println("1.push\n2.pop\n3.Show all\n4.exit\n");
		int choice = 0;
		int run = 0; // 累計迴圈跑了幾次，下次迴圈就從上一次停止的地方跑
		int[] run_array = new int[100]; // 用來儲存每次跑出質數後，run的值
		while(choice != 4) {
			System.out.print("Enter your choice: ");
			choice = input.nextInt();
			switch(choice) {
				case 1: // push
					System.out.print("Please enter a number: ");
					int number = input.nextInt();
					int order = 0;
					int place = 0;
					for(int i=0; ;run++) { // continually running until the condition is correspond to our strict
						i = run; // 把run的值存到i(因為i會變回0，要讓它變成上一次累計到的數的下一個數)
						if(Stack.isFull() == false) { // stack 還沒滿時
							if(prime_num(i)==true) { // 從上一次取到的數後面開始，是質數的話，就push進stack
								order = order+1; // 用來看累計的次數是否有超過number
								Stack.Push(i); // 把質數push進去
								run_array[place] = run;
								place++;
							}
							if(order == number) // 質數取到符合輸入的數後，就break
								break;
						}
						if(Stack.isFull() == true) { // stack滿了後輸出警告訊息
							System.out.println("The number you entered is too big, we will just show the first 100 numbers!!");
							break;
						}
					}
					System.out.println(Stack.showAll()); // show the string in stack
					run = run+1; // 要跑下一個數要再加1
					break;
				case 2: // pop
					System.out.print("Please enter a number: ");
					int Number = input.nextInt();
					
					if(Stack.isEmpty() == false) { // stack不是空的
						if(Number <= Stack.getSize()) { // condition that the amount of number going to be popped out is not bigger than we have now
							run = run_array[Stack.getSize()-Number]; // 讓push質數的迴圈計算考慮減掉的質數(減掉的要讓它可以再被push一次)
							for(int a=1; a<=Number; a++) { // pop the numbers one by one
								Stack.Pop(1);
							}
							System.out.println(Stack.showAll()); // show the string in stack
						}
						else { // pop到一半就會沒了的情況
							int size = Stack.getSize(); // 把目前stack內的總數量存成一個變數
							for(int b=1; b<=size; b++) { // 在小於size這個變數的情況下，一直減，減到沒有為止
								Stack.Pop(1);
							}
							run = 0; // 空了，讓迴圈的計算變到一開始的狀況
							System.out.println(Stack.showAll()); // show the string in stack
							System.out.println("The stack don't have that much numbers,we just pop all the numbers from the stock!");
						}	
					}
					if(Stack.isEmpty() == true) { // stack空了就輸出訊息
						run = 0;
						System.out.println("The stack is empty!");
					}
					break;
				case 3: // show all
					System.out.println(Stack.showAll()); 
					break;
				case 4: // exit
					System.out.println("Bye~");
					break;
				default: // other conditions
					System.out.println("You can't enter the number that is not between 1~4!!");
					System.out.println("Try again!");
					break;
			}
		}
	}
	
	public static boolean prime_num(int input) { // check 是否為質數
		boolean check = false;
		if(input>2) { // 如果輸入的數大於2再做判斷
			for(int i=2; i<input; i++) {
				check = true;
				if((input%i)== 0) { // 從2開始除，看輸入的數除於比它小的數後，有沒有可以整除的
					check = false; // 如果有，則不為質數
					break;
				}
			}
			return check;
		}
		else if(input==2)
			return true;
		else // 如果輸入的數小於2，必不為質數
			return false;
	}
}
