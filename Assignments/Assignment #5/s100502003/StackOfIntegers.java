package a5.s100502003;

import java.util.*;

public class StackOfIntegers {
	private ArrayList <Object> list = new ArrayList <Object> ();
	
	public StackOfIntegers() {
		
	}

	public String showAll() {
		return list.toString();
	}
	
	public int getSize() {
		return list.size();
	}
	
	public Object Pop(int num) {
		Object o = list.get(getSize()-num);
		list.remove(getSize()-num);
		return o;
	}
	
	public void Push(Object o) {
		list.add(o);
	}
	
	public boolean isEmpty() {
		return list.isEmpty();
	}
	
	public boolean isFull() {
		int size = getSize();
		if(size == 100) 
			return true;
		else
			return false;
	}
}
