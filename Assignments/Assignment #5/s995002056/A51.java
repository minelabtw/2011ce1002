package a5.s995002056;
import java.util.Scanner;

public class A51 {
	public static void main(String[] argv){
		Scanner input = new Scanner(System.in);
		StackOfIntegers st = new StackOfIntegers();
		int sum = 0;										//計數											
		
		boolean control = true;
		while(control){
			System.out.println("1.push");
			System.out.println("2.pop");
			System.out.println("3.show all");
			System.out.println("4.exit");
			
			String ch1 = input.next();						//輸入選項
			int ch2 = 0;
			
			if(ch1.equals("push")){							//PUSH
				String ch3 = input.next(); 
				if(true != ch3.matches("[a-zA-Z]+")){		//判斷是否為英文字串
					ch2 = Integer.parseInt(ch3);
					if(sum + ch2 < 100){					//若STACK沒滿
						st.push(ch2);
						sum += ch2;
					}
					else{									//滿了
						st.push(100-sum);					//只輸入到100
						sum = 100;
						System.out.println("The stack is full!");
					}
				}
				else										//輸入錯誤
					System.out.println("error");	
			}
			else if(ch1.equals("pop")){						//POP
				String ch3 = input.next(); 
				if(true != ch3.matches("[a-zA-Z]+")){		//判斷是否為英文字串
					ch2 = Integer.parseInt(ch3);
					if(sum - ch2 > 0){						//若STACK沒空
						st.pop(ch2);
						sum -= ch2;
					}
					else{									//空了
						st.pop(sum);
						sum = 0;
						System.out.println("The stack is empty!");
					}
				}
				else										//錯誤
					System.out.println("error");
			}
			else if(ch1.equals("show")){					//SHOW ALL
				String ch3 = input.next();
				if(ch3.equals("all"))
					st.showAll();
				else										//錯誤
					System.out.println("error");	
			}
			else if(ch1.equals("exit")){					//EXIT
				System.out.print("Thank you!");
				control = false;							//離開迴圈
			}
			else
				System.out.println("Please input again.");	//錯誤
		}
	}
}
