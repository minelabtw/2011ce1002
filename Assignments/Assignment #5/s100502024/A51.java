package a5.s100502024;
import java.util.Scanner;
public class A51 
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		boolean flag = true;
		int counter = 0;
		int number;
		while(flag == true)
		{
			System.out.println("1.push"+"\n"+"2.pop"+"\n"+"3.Show all"+"\n"+"4.exit");
			System.out.println("Please select a function :");
			int select = input.nextInt();
			StackOfIntegers test = new StackOfIntegers();
			switch (select)
			{
				case 1:
					System.out.println("How many elements would you want to push?");
					number = input.nextInt();
					counter = counter + number;
					test.Push(counter);
					if(test.isEmpty() == true)
					{
						System.out.println("The stack is empty!");
					}
					else if (test.isFull() == true)
					{
						System.out.println("The stack is full!");
					}
					break;
				case 2:
					System.out.println("How many elements would you want to pop?");
					number = input.nextInt();
					counter = counter - number;
					test.Pop(counter);
					break;
				case 3:
					test.showAll();
					break;
				case 4:
					flag = false;
					break;
			}
		}
	}
}
