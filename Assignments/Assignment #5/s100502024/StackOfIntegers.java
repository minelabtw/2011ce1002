package a5.s100502024;
import java.lang.Math;
public class StackOfIntegers 
{
	private static java.util.ArrayList<Integer> list = new java.util.ArrayList<Integer>();
	private static int i = 2;
	private static int keep = 0;
	public void showAll()
	{
		System.out.println(list.toString());
	}
	public void Pop(int delete)
	{
		for(int i=1;i<=delete;i++)
		{
			list.remove(keep-i);
		}
		keep = keep - delete;
		System.out.println(list.toString());
	}
	public static void Push(int counter)
	{
		if (list.size()+counter>100)
		{
			System.out.println("The stack is full!");
		}
		boolean check = true;
		list.add(2);
		keep++;
		i = list.get(list.size()-1);
		while (list.size()!=counter)
		{
			i++;
			for(int j=2;j<=Math.pow(i,0.5);j++)
			{
				if(i%j == 0)
				{
					check = false;
					break;
				}
				else
				{
					check = true;
				}
			}
			if(check == true)
			{
				list.add(i);
				keep++;
			}	
		}
		System.out.println(list.toString());
	}
	public boolean isEmpty()
	{
		if(list.size() == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public boolean isFull()
	{
		if (list.size() == 100)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
