package a5.s995002026;

public class stackofintegers {
	private int[] primenumber=new int[100];
	private int input=0;							//紀錄stack有多少elements
	
	stackofintegers(){
		setprimenumber(100);
	}
	
	public  void showall(){							//印出目前的質數
		for(int i=0;i<input;i++){
			System.out.print(primenumber[i]+" ");
		}
	}
	
	public  int[] pop(int num){
		input=input-num;						//pop後的總數量
		
		isempty();
		
		isfull();
		
		int[] re =new int[input];				//回傳用的陣列
		
		for(int i=0;i<input;i++){
			
			re[i]=primenumber[i];				//需要幾個質數就放幾個質數給回傳值得陣列
		}
		
		return re;
	}
	
	public  int[] push(int num){
		input=input+num;						//push後的總數量
		
		isempty();
		
		isfull();
		
		int[] re=new int[input];					//回傳用的陣列
		
		for(int i=0;i<input;i++){					
			re[i]=primenumber[i];					//需要幾個質數就放幾個質數給回傳值得陣列
		}
		
		return re;
	}
	
	public void isempty(){							//檢查是否空了
		if(input<=0){
			System.out.println("stack已經空了");		
			
			input=0;								//避免input被減到變負數
		}
	}
	
	public void isfull(){							//檢查是否超過總數
		if(input>100){
			System.out.println("stack已經滿了");
			
			input=100;								//避免input被加到超過100
		}
	}
	
	public void setprimenumber(int num){			//先儲存質數
		int j,k;
		int original=num;
		/*---------------------找質數-----------------------------------*/
		for(j=2;j<10000;j++){
			for(k=2;k<j;k++){
				if(j%k==0)
					break;
			}
			if(k==j){
				primenumber[num-original]=k;
				num=num+1;
			}
			if(num-original==original)
				break;
		}
		/*---------------------找質數-----------------------------------*/
	}
	
}
