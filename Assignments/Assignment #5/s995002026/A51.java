package a5.s995002026;

import java.util.Scanner;

public class A51 {
	public static void main(String[] argv){
		Scanner input=new Scanner(System.in);
		
		stackofintegers stack=new stackofintegers();
		
		
		/*--------------------選項-------------------------*/
		String pop="pop";
		
		String push="push";
		
		String showall="showall";
		
		String exit="exit";
		/*--------------------選項-------------------------*/
		
		System.out.print("1. pop\n2. push\n3. showall\n4. exit\n");
		
		
		/*--------------------判斷用stackofintegers的哪一個method-------------------------*/
		while(true){
			
			String choice=input.next();										//記錄選項選項
			
			if(pop.equals(choice)){
			
				int num=input.nextInt();
				
				int[] show=stack.pop(num);									//紀錄回傳的陣列
				
				for(int i=0;i<show.length;i++){									//印出結果
					System.out.print(show[i]+" ");
					
					if(i>0 && i%9==0)
						System.out.print("\n");
				}
				
				System.out.print("\n");
			}
			else if(push.equals(choice)){
			
				int num=input.nextInt();
				
				int[] show=stack.push(num);									//紀錄回傳的陣列
				
				for(int i=0;i<show.length;i++){									//印出結果
					System.out.print(show[i]+" ");
					
					if(i>0 && i%9==0)
						System.out.print("\n");
				}
				
				System.out.print("\n");
			}
			else if(showall.equals(choice)){
			
				stack.showall();
			}
			else if(exit.equals(choice)){
				
				System.out.println("byebye");
				
				break;
			}
			else{	
				System.out.println("輸入錯誤，請重新輸入");
			}
		}
		/*--------------------判斷用stackofintegers的哪一個method-------------------------*/
	}
}
