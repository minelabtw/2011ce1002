package a5.s100502520;

import java.security.acl.LastOwnerException;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;

import javax.jws.soap.SOAPBinding.Use;

public class A51 {
	private static StackOfIntegers use = new StackOfIntegers();
	private static int n = 2;
	public static void main(String[] args){
		boolean jud = true;
		int choose;
		int numb;
		Scanner input = new Scanner(System.in);
		while(jud){
			System.out.println("what do you want ?");
			choose = 0;
			numb = 0;
			System.out.println("1.push\n"+"2.pop\n"+"3.Show all\n"+"4.exit\n");
			choose = input.nextInt();
			switch(choose){   //選擇功能
				case 1:  //push質數
					System.out.println("how many numbers ?");
					numb = input.nextInt();
					FindPrimeNumbers(numb);
					use.showAll();
					break;
				case 2:  //pop質數
					System.out.println("how many numbers ?");
					numb = input.nextInt();
					use.Pop(numb);
					use.showAll();
					break;
				case 3:  //顯示目前所有質數
					use.showAll();
					break;
				case 4:  //離開
					System.out.println("bye bye");
					jud = false;
					break;
			}
		}
	}
	
	//判斷何謂質數
	public static void FindPrimeNumbers(int number){
		int judge = 0;
		int count = 1;
		while(count <= number){
			for(int i =1; i<=Math.pow(n,0.5); i++){
				if(n%i==0){
					judge++;
				}
			}
			if(judge == 1){
				use.Push(n);  //儲存質數
				count++;
			}
			judge = 0;
			n++;
		}
	}
}
