package a5.s100502520;

public class StackOfIntegers {
	private int[] store = new int[100];
	private int counter = 0;

	public void showAll() {  //顯示所有數
		for (int i = 0; i < counter; i++) {
			System.out.println(store[i]);
		}
	}

	public void Push(int num) {  //將數存入陣列
		if (isFull()) {
			System.out.println("it is full");
		} else {
			store[counter] = num;
			counter++;
		}
	}

	public void Pop(int num) {  //將數從陣列移除
		if (isEmpty()) {
			System.out.println("it is empty");
			counter = 0;
		} else {
			for (int i = 0; i < num; i++) {
				store[counter] = 0;
				counter--;
			}
		}
	}

	public boolean isEmpty() {  //判斷陣列是否有數
		if (counter == 0) {
			return true;
		} else if (counter == 100) {
			counter = 99;
			return false;
		} else {
			return false;
		}
	}

	public boolean isFull() {  //判斷陣列是否已滿
		if (counter <= 99) {
			return false;
		}

		else {
			return true;
		}
	}
}
