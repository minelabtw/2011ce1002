//showAll(): Show all the elements in stack.
//Pop(int num): Take input as argument, and pop such number of elements from the stack.
//Push(int num): Take input as argument, and push the input into the stack.
//isEmpty(): Check the stack whether it is empty or not.
//isFull(): Check the stack whether it is full or not.
//Write a test program and meet the following requirements:
//
//At first, the stack should be empty.
//User can store some Prime Numbers, at most 100 numbers, in the stack.
//Pop: let user input a number and pop such number of elements from stack. Show the result after pop.
//Push: let user input a number and push such number of elements into stack Show the result after push.
//ShowAll: show the current elements in the stack.
//A list and infinite loop
package a5.s100502506;
import java.util.ArrayList;
import java.util.Scanner;

public class StackOfIntegers 
{
	private ArrayList<Integer> elements =new ArrayList<>();
	private int size;
	public StackOfIntegers()
	{
		size=0;
	}
	public ArrayList<Integer> showAll() //Show all the elements in stack()
	{
		return elements;
	}
	public void Pop(int num)// Take input as argument, and pop such number of elements from the stack
	{
		for(int i=size-num;i<size;i++)
			elements.remove(size-num);
		size-=num;
		
	}
	public void Push(int num)// Take input as argument, and push the input into the stack
	{
		for(int i=size;i<size+num;i++)
		{
			elements.add(PRIME(i+1));
		}
		size+=num;
	}
	public boolean isEmpty()// Check the stack whether it is empty or not
	{
		return elements.isEmpty();
	}
	public boolean isFull()// Check the stack whether it is full or not
	{
		boolean flag=false;
		if(size<100)
			flag=false;
		else if(size==100)
			flag=true;
		return flag;
	}
	public int getSize()//return size
	{
		return size;
	}
	public int PRIME(int num)// prime number  
	{
		int counter;
		int counter2=0;
		int times=0;
		int[] array=new int[100];
		int pri_num=0;
		for(int i=1;counter2<=num;i++)
		{
			counter=0;
			for(int j=1;j<=i;j++)
			{
				if((i%j)==0)
				{
					
					counter+=1;
				}
			}
			if(counter==2)
			{
				array[times]=i;
				
				times+=1;
				counter2+=1;
				if(times==100)
					break;
			}
			
		}
		return array[num-1];//return the numth prime number
	}

	
}


