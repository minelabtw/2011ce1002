package a5.s100502506;

import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.util.Scanner;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;


import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class A51 extends JFrame
{
	private JTextField textField1;
	private JRadioButton radioButton1;
	private JRadioButton radioButton2;
	private JRadioButton radioButton3;
	private JRadioButton radioButton4;
	private JRadioButton radioButton5;
	private JButton button1;
	private JPanel panel1;
	private JPanel panel2;
	private JFrame frame1;
	private GridLayout gridLayout;
	private FlowLayout flowLayout;
	private ButtonGroup buttonGroup;
	public A51()
	{
		super("A51-StackOfIntegers");// set title
		
		panel1=new JPanel();
		panel2=new JPanel();
		radioButton1=new JRadioButton();
		radioButton1.setText("1.push");
		radioButton2=new JRadioButton();
		radioButton2.setText("2.pop");
		radioButton3=new JRadioButton();
		radioButton3.setText("3.Show all");
		radioButton4=new JRadioButton();
		radioButton4.setText("4.Show size");
		radioButton5=new JRadioButton();
		radioButton5.setText("5.exit");
		buttonGroup=new ButtonGroup();
		
		
		//use buttonGroup to group the radioButtons
		buttonGroup.add(radioButton1);
		buttonGroup.add(radioButton2);
		buttonGroup.add(radioButton3);
		buttonGroup.add(radioButton4);
		buttonGroup.add(radioButton5);
		//panel1 set gridlayout 5*1
		gridLayout=new GridLayout(5,1);
		panel1.setLayout(gridLayout);
		//add to panel1
		panel1.add(radioButton1);
		panel1.add(radioButton2);
		panel1.add(radioButton3);
		panel1.add(radioButton4);
		panel1.add(radioButton5);

		textField1=new JTextField(10);
		button1=new JButton();
		textField1.setEditable(true);
		button1.setText("OK");
		flowLayout =new FlowLayout();
		//panel2 set flowlayout 
		panel2.setLayout(flowLayout);
		//add to panel2
		panel2.add(textField1);
		panel2.add(button1);
		setLayout(flowLayout);
		//set actionlistener
		button1.addActionListener(new buttonListener());
		//add panel1 and panel2 to JFRAME
		add(panel1);
		add(panel2);
		
	}
	public static void main(String argc[])
	{
		
		A51 test2=new A51();// create A51 object
		test2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		test2.setLocation(400,400);//set location
		test2.setSize(400,200);//set size
		test2.setResizable(false); //set resizable
		test2.setVisible(true);	//set visible
		
	}
	private class buttonListener implements ActionListener// set Actionlistener
	{
		StackOfIntegers test =new StackOfIntegers();
		//handle button event
		public void actionPerformed(ActionEvent e)//event
		{	
			int input=0;
			input=Integer.parseInt(textField1.getText());//get Textfield text to integer
			String message1=new String();
			String message2=new String();
			String message3="CAN'T ENTER NAGARIVE";
			if(radioButton1.isSelected())//if push checked
			{
				if(input>=100)// if input>100
					test.Push(100);
				if(test.isFull())//if the stack is full
				{
					message1=String.format("The stack is full(100)\n");
					
				}
				else if(input<0)//if the stack is <0
				{
					JOptionPane.showMessageDialog(null, message3,"ERROR", JOptionPane.ERROR_MESSAGE);
				}
				else
				{
					test.Push(input);
				}
			}
			else if(radioButton2.isSelected())//is pop checked
			{
				

				if(test.isEmpty()==true)//if the stack is empty
				{
					message1=String.format("The stack is empty(0)\n");
					test.Pop(test.getSize());
				}
				else if(input<0)// if the input <0 
				{
					JOptionPane.showMessageDialog(null, message3,"ERROR", JOptionPane.ERROR_MESSAGE);
				}
				else if(input-test.getSize()>0)// if the input > size 
				{
					message1=String.format("The stack is empty(0)\n");
					test.Pop(test.getSize());
				}
				else
				{
					test.Pop(input);
				}
				
			}
			else if(radioButton3.isSelected())// 
			{
				if(test.isEmpty())// if the stack is empty
				{
					message1=String.format("The stack is empty(0)\n");
					test.Pop(test.getSize());					
				}
				else
				{
					message1=String.format("The stack here is :\n");
				}

			}
			else if(radioButton4.isSelected())//show size of stack
			{
				message1=String.format("The size of the stack is: %d\n",test.getSize());
			}
			else if(radioButton5.isSelected())//EXIT
			{
				System.exit(0);
			}
			
			if(input>0)//if input > 0
			{
				message2=test.showAll().toString();
				JOptionPane.showMessageDialog(null, message1+message2);
		
			}
		}
	}
}
