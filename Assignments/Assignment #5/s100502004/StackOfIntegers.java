package a5.s100502004;

public class StackOfIntegers {
	private int num=0;
	private int con=1;
	private int run=(-1);
	private int [] a = new int [100];
	private int [] b = new int [100];

	public void Push(int x){ //the method to push the number
		num = num +x;
		if(isFull()){
			num=100;
			for(int i=con;;i++){
				int check=0;
				for(int j=1;j<i+1;j++){ 
					if(i%j==0){
						check++;
					}
					if(check>2){
						break;
					}

				}

				if(check==2){
					run++;
					a[run]=i;
				
				}
				if(run==num-1){
					con=i+1;
					break;
				} 
			}
			for(int i=0;i<num;i++){
				System.out.print(a[i]+" ");
			}
			System.out.print("\n");
		}
		else{
			for(int i=con;;i++){
				int check=0;
				for(int j=1;j<i+1;j++){ 
					if(i%j==0){
						check++;
					}
					if(check>2){
						break;
					}

				}

				if(check==2){
					run++;
					a[run]=i;
				
				}
				if(run==num-1){
					con=i+1;
					break;
				} 
			}
			for(int i=0;i<num;i++){
				System.out.print(a[i]+" ");
			}
			System.out.print("\n");
		}
	}

	
	public void Pop(int x){//the method to pop the number
		num=num-x;
		if(isEmpty()){
			System.out.println("The stack is empty, so it can not take out anything else.");
			num = 0;
			run = 0;
			con = 3;
		}
		else{
			for(int i=0;i<num;i++){
				b[i]=a[i];
			}		
			run=num-1;
			con=b[num-1]+1;
			
			for(int i=0;i<(num);i++){
				System.out.print(b[i]+" ");
			}
			System.out.print("\n");
		}
	}

	public void showAll(){//the method to show the stack
		if(isEmpty()){
			System.out.println("The array is empty, so it can not show anything.");
		}
		else{
		for(int i=0;i<num;i++){
			System.out.print(a[i]+" ");
		}
		}
		
	}
	
	
	public boolean isFull(){//the method to check if the stack is full or not
		if(num>100){
			return true;
		}
		else{
			return false;
		}
		
	}
	
	
	
	public boolean isEmpty(){//the method to check if the stack is empty or not
		if(num<=0){
			return true;
		}
		
		else{
			return false;
		}
	}
	
	
	
}
