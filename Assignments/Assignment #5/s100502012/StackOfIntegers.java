package a5.s100502012;

public class StackOfIntegers {
	private int[] primeSerisTest=new int[100];//default prime array
	private int[] primeSeris=new int[100];//output prime array
	private int count;
	
	public StackOfIntegers(int b){// accept count and test prime numbers 
		count=b;//record the numbers of prime numbers;
		
		int coun=1;
		int d=2;
		primeSerisTest[0]=2;
		while(coun<100){
			for(int c=2 ; c<d ; c++){
				if (d%c==0){
					break;
				}
				if (c==(d-1)){
					primeSerisTest[coun]=d;
				    coun++;
				} 
			}
			d++;
		}	
	}
	
	public int[] showAll(){
		return primeSeris;
	}
	
	public void popNum(int pop){
		int p=count;
		if (p-pop==0){
			for(int e=0 ; e<primeSeris.length ; e++){
				primeSeris[e]=0;
			}
		}
		else{
		    for(int c=0 ; c<=pop ; c++){
		        primeSeris[count-c]=0;
		    }
		}
		count-=pop;
	}
	
	public void pushNum(int push){
		for(int a=0 ; a<push ; a++){
			primeSeris[count+a]=primeSerisTest[count+a];
		}
		count+=push;
	}
	
	public boolean isEmpty(){
		if (count==0)
			return true;
		else
			return false;
	}
	
	public boolean isFull(){
		if (count>=100)
			return true;
		else
			return false;
	}
	
	public int getcount(){
	    return count;	
	}
	
	public int[] getPrimeSeris(){
		return primeSeris;
	}
}
