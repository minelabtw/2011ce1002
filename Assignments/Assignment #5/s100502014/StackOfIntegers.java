package a5.s100502014;
import javax.swing.*;
public class StackOfIntegers {
	private int[] integers = new int[100];
	private int total = 0;
	StackOfIntegers() {
		for(int i=0;i<100;i++) {
			integers[i] = 0;
		}
	}
	//Show all the elements in stack.
	public void showAll() {
		if(total == 0)
			JOptionPane.showMessageDialog(null,
										  "The stack is empty!!",
										  "Show all!!!",
										  JOptionPane.WARNING_MESSAGE);
		else {
			String output = "";
			for(int i=0;i<total;i++) {
				output += integers[i] + "    ";
				if(i%10==9)
					output += "\n";
			}
			JOptionPane.showMessageDialog(null,
										  output,
										  "Show all!!!",
										  JOptionPane.INFORMATION_MESSAGE);
		}
	}
	//Take input as argument, and pop such number of elements from the stack.
	public void Pop() {
		integers[total-1] = 0;
		total--;
	}
	//Take input as argument, and push the input into the stack.
	public void Push(int num) {
		integers[total]=num;
		total++;
	}
	//Check the stack whether it is empty or not.
	public boolean isEmpty() {
		if(total==0)
			return true;
		else
			return false;
	}
	//Check the stack whether it is full or not.
	public boolean isFull() {
		if(total==100)
			return true;
		else
			return false;
	}
	public int getTotal() {
		return total;
	}
}
