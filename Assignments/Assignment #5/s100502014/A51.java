package a5.s100502014;
import javax.swing.*;
public class A51 {
	public static void main(String[] args) {
		//store prime numbers
		int[] primeNum = new int[100];
		int num=3;
		primeNum[0]=2;
		for(int i=1;i<100;num+=2){
			boolean isPrime=true;
			for(int j=0;primeNum[j]<=Math.sqrt(num);j++) {
				//is not prime number
				if(num%primeNum[j]==0) {
					isPrime=false;
					break;
				}
			}
			//is prime number
			if(isPrime) {
				primeNum[i]=num;
				i++;
			}
		}
		
		//create an object
		StackOfIntegers stack = new StackOfIntegers();
		boolean loop=true;
		while(loop) {
			
			//option
			String optionString = JOptionPane.showInputDialog(null,
								  "Please choose your option:\n" +
								  "1.Push\n" +
								  "2.Pop\n" +
								  "3.Show all\n" +
								  "4.Exit",
								  "Option!!!",
								  JOptionPane.QUESTION_MESSAGE);
			switch(Integer.parseInt(optionString)) {
				//push
				case 1:
					String pushString = JOptionPane.showInputDialog(null,
							  			"Please enter a number:",
									    "Push!!!",
									    JOptionPane.QUESTION_MESSAGE);
					for(int i=0;i<Integer.parseInt(pushString);i++) {
						if(stack.isFull()) {
							JOptionPane.showMessageDialog(null,
														  "The stack is full!!",
														  "Full!!!",
														  JOptionPane.WARNING_MESSAGE);
							break;
						}
						stack.Push(primeNum[stack.getTotal()]);						  
					}
					stack.showAll();
					break;
					
				//pop
				case 2:
					String popString = JOptionPane.showInputDialog(null,
							  		   "Please enter a number:",
									   "Pop!!!",
									   JOptionPane.QUESTION_MESSAGE);
					for(int i=0;i<Integer.parseInt(popString);i++) {
						if(stack.isEmpty())
							break;
						stack.Pop();						  
					}
					stack.showAll();
					break;
				
				//show all
				case 3:
					stack.showAll();
					break;
					
				//exit
				case 4:
					loop=false;
					break;
				default:
					JOptionPane.showMessageDialog(null,
							  					  "Errorl!!",
							  					  "Error!!!",
							  					  JOptionPane.WARNING_MESSAGE);
			}
		}
	}
}
