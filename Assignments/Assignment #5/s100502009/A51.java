package a5.s100502009;
import java.util.Scanner;

public class A51 {
	public static void main(String[] args)
	{
		Scanner input=new Scanner(System.in);
		StackOfIntegers soi=new StackOfIntegers();
		for(int i=1;i>0;i++)
		{
			System.out.println("\n1.push"+"\n2.pop"+"\n3.Show all"+"\n4.exit");
			int choose=input.nextInt();
			switch(choose)
			{
				case 1:
					System.out.println("Please enter a number of elements to push into: ");
					int number=input.nextInt();//enter the number of elements
					soi.push(number);//to push the prime numbers into the stack
					System.out.println(soi.showAll());//to show all the elements
					break;
				case 2:
					System.out.println("Please enter a number of elements to pop out: ");
					soi.pop(input.nextInt());//enter the number of elements and pop the prime numbers from the stack
					System.out.println(soi.showAll());//to show all the elements
					break;
				case 3:
					System.out.println(soi.showAll());//to show all the elements
					break;
				case 4:
					i=-1;
					System.out.print("BYE~~");
					break;
				default:
					System.out.println("Errors: invalid status");
					System.exit(0);
					break;
			}
		}
	}

}
