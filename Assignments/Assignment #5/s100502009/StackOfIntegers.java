package a5.s100502009;

public class StackOfIntegers {
	private int[] elements=new int[100];
	private int size=0;//the number of the elements
	public StackOfIntegers()//constructor to set object
	{
	}
	public void push(int num)
	{		
		int count=1;//count the numbers of the input elements
		for(int i=elements[size];count<=num;i++)
		{
			boolean check=true;//to check the prime number
			for(int j=2;j<i;j++)
			{
				if(i%j==0)
				{
					check=false;
				}
			}
			if(i==0||i==1)
			{
				continue;
			}
			else if(i==elements[size])
			{
				size++;
			}
			else if(check==true)
			{
				elements[size]=i;//to push the prime numbers into the stack
				count++;
				if(count<=num)
					size++;
			}
			if(isFull())
			{
				break;
			}
		}		
	}
	
	public void pop(int num)//method to pop from the stack
	{
		for(int i=1;i<=num;i++)
		{
			elements[size]=0;
			size--;
			if(isEmpty())
				break;
		}
	}
	
	public String showAll()//method to show all the elements
	{
		String result="";
		if(isEmpty())
		{
			return result="the stack is empty!!";
		}
		else
		{
			for(int i=0;i<=size;i++)
			{	
				result+=elements[i]+" ";				
			}	
			return result;
		}
	}
	
	public boolean isEmpty()//method to check if the stack is empty or not
	{
		if(size==0)
			return true;
		else
			return false;
	}
	
	public boolean isFull()//method to check if the stack is full or not
	{
		if(size==99)
			return true;
		else
			return false;
	}

}
