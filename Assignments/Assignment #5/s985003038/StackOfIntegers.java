	// ========================================================================================= //
	//		Author: Pluto Chan, Ka Hou															 //
	//		E-mail: jesic01@jesic-tech.com														 //
	//		Date: 2012/3/15																		 //
	//		Description: Assignment IV for course CE1002 - Introduction to Computer Science II	 //
	//					 A simple class for StackOfIntegers, which can push elements into the	 //
	//					 stack, pop elements out of it, show all the elements in the stack,		 //
	//					 and decide the stack is empty or full or not							 //
	// ========================================================================================= //

package a5.s985003038;

import java.util.Scanner;

public class StackOfIntegers {
	protected int elements[];										// array for keeping the elements
	protected int numberOfElements;									// counter to the elements in the stack, and also the pointer to the top of the element array
	protected int maximumOfElements;								// the size of the stack

	public StackOfIntegers(int num){								// constructor for the stack, which will create an integer array for keeping the elements
		elements = new int[num];
		maximumOfElements = num;
	}
	
	// ========================================================================================= //
	//		Name: getTop																		 //
	//		Input: none																			 //
	//		Output: the value of the top element												 //
	//		Description: get the value of the top element in the stack							 //
	// ========================================================================================= //
	public int getTop(){
		if(numberOfElements > 0)
			return elements[numberOfElements - 1];
		else
			return -1;
	}
	
	// ========================================================================================= //
	//		Name: showAll																		 //
	//		Input: none																			 //
	//		Output: none																		 //
	//		Description: print out all the elements in the stack								 //
	// ========================================================================================= //
	public void showAll(){
		if(isEmpty()){												// show a notice if the stack is empty
			System.out.println("The stack is empty.");
		} else {
			System.out.println("The stack here is:");
			System.out.print("BOTTOM\t");
			for(int i = 0; i < numberOfElements; i++)				// otherwise, loop and show all the elements in the stack
				System.out.print(elements[i] + "\t");
			System.out.println("TOP");
		}
	}
	
	// ========================================================================================= //
	//		Name: Pop																			 //
	//		Input: how many elements you want to pop out										 //
	//		Output: none																		 //
	//		Description: pop out the specified number of elements out of the stack				 //
	// ========================================================================================= //
	public void Pop(int num){
		if(isEmpty())												// if the stack is empty, you cannot pop even one element out of the stack anymore
			System.out.println("Error: the stack is empty.");		// so, just show an error message
		else if(numberOfElements >= num){							// if there are enough elements in the stack, just pop them out
			numberOfElements -= num;
			showAll();
		} else {														// if there are not enough elements to pop out, show the warning message
			System.out.println("Warning: popping elements more than that in the stack. The stack become empty now.");
			numberOfElements = 0;									// and pop until the stack is empty
			showAll();
		}
	}
	
	// ========================================================================================= //
	//		Name: Push																			 //
	//		Input: how many elements you want to push into the stack							 //
	//		Output: none																		 //
	//		Description: push a specified number of elements into the stack						 //
	// ========================================================================================= //
	public void Push(int num){
		Scanner input = new Scanner(System.in);
		if(isFull())												// if the stack is full, you cannot push even one element into the stack anymore
			System.out.println("Error: the stack is full.");		// so, just show an error message
		else {
			if(num > maximumOfElements - numberOfElements)			// if the stack has not full yet, but there are not enough space to push all the elements into the stack
				System.out.println("Warning: the stack is already full. You can only push " + (maximumOfElements - numberOfElements) + " numbers into the stack.");
																	// warn the user that he can only push a number of elements into the stack
			int canInputNumber = Math.min(num, maximumOfElements - numberOfElements);
			System.out.print("Please input " + canInputNumber + " number" + (canInputNumber == 1 ? "" : "s") + ": ");
			for(int i = 0; i < canInputNumber; i++)
				elements[numberOfElements + i] = input.nextInt();	// then, let the user to push elements into the stack one by one
			numberOfElements += canInputNumber;						// and update the counter and the pointer of the stack
			showAll();
		}
	}
	
	// ========================================================================================= //
	//		Name: Push																			 //
	//		Input: an element array going to push into the stack								 //
	//		Output: none																		 //
	//		Description: push all the integers in the input array into the stack				 //
	// ========================================================================================= //
	public void Push(int[] num){
		if(isFull())												// if the stack is full, you cannot push even one element into the stack anymore
			System.out.println("Error: the stack is full.");		// so, just show an error message
		else {
			if(num.length > maximumOfElements - numberOfElements)	// if the stack has not full yet, but there are not enough space to push all the elements into the stack
				System.out.println("Warning: the stack is already full. You can only push " + (maximumOfElements - numberOfElements) + " numbers into the stack.");
																	// warn the user that only a number of elements have been pushed into the stack
			int canInputNumber = Math.min(num.length, maximumOfElements - numberOfElements);
			for(int i = 0; i < canInputNumber; i++)
				elements[numberOfElements + i] = num[i];			// then, push elements into the stack one by one
			numberOfElements += canInputNumber;						// and update the counter and the pointer of the stack
			showAll();
		}
	}
	
	// ========================================================================================= //
	//		Name: isEmpty																		 //
	//		Input: none																			 //
	//		Output: result as boolean															 //
	//		Description: check whether the stack is empty										 //
	// ========================================================================================= //
	public boolean isEmpty(){
		if(numberOfElements == 0)
			return true;
		else
			return false;
	}
	
	// ========================================================================================= //
	//		Name: isFull																		 //
	//		Input: none																			 //
	//		Output: result as boolean															 //
	//		Description: check whether the stack is full										 //
	// ========================================================================================= //
	public boolean isFull(){
		if(numberOfElements == maximumOfElements)
			return true;
		else
			return false;
	}
}