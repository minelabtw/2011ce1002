	// ========================================================================================= //
	//		Author: Pluto Chan, Ka Hou															 //
	//		E-mail: jesic01@jesic-tech.com														 //
	//		Date: 2012/3/15																		 //
	//		Last-Mod: 2012/3/18																	 //
	//		Description: Assignment V for course CE1002 - Introduction to Computer Science II	 //
	//					 A testing program for class of StackOfPrime							 //
	// ========================================================================================= //

package a5.s985003038;

import java.util.Scanner;

public class A5 {
	public static void main(String[] argv){
		Scanner input = new Scanner(System.in);
		StackOfPrime stack;									// a StackOfPrime stack object, which inherit the properties of StackOfIntegers
		String inputString;
		
		System.out.print("Please define the size of your stack: ");
		stack = new StackOfPrime(input.nextInt());			// define the size of the stack
		
		printMenu();										// show the menu
		
		while(true){
			System.out.print("Please input your comment: ");
			inputString = input.next();
			if(inputString.toLowerCase().equals("menu"))
				printMenu();								// show the menu again
			else if(inputString.toLowerCase().equals("push"))
				stack.Push(findPrime(input.nextInt(), stack.isEmpty() ? 0 : stack.getTop() + 1));
															// push #number of prime integers into the stack
			else if(inputString.toLowerCase().equals("pop"))
				stack.Pop(input.nextInt());					// pop #number of elements out of the stack
			else if(inputString.toLowerCase().equals("show") && input.next().toLowerCase().equals("all"))
				stack.showAll();							// show all the elements in the stack
			else if(inputString.toLowerCase().equals("exit"))
				break;										// exit the program
			else if(!inputString.isEmpty())					// for other operation input, show error message
				System.out.println("Error: no such operations");
		}
		
		System.out.println("Bye.");
	}
	
	// ========================================================================================= //
	//		Name: findPrime																		 //
	//		Input: how many prime do you want to find											 //
	//		Output: an integer array that contain primes										 //
	//		Description: find a specified number of primes										 //
	// ========================================================================================= //
	private static int[] findPrime(int num, int start){
		int integer[];
		int primeCounter;
		int range = start + num;
		
		do{
			range = range * 10;
			integer = new int[range + 1];					// create a temporary array to keep all the integers less than or equal to the input number
			primeCounter = range - 2;						// a counter counting the number of prime in the array
			for(int i = 2; i <= range; i++)
				integer[i] = i;
			
			for(int i = 2; i <= Math.sqrt(range); i++)		// kick out all the integers which are multiples of the other integers, until the square root of the input integer
				if(integer[i] != 0){						// since there is no way to form the input integers by multiplying two numbers greater than the square root of that integer
					int step = i * i;						// we use 0 to replace the value of the elements in the temporary array if we know that is not a prime
					while(step <= range){					// after doing this, the elements, which are not 0, in the temporary array are prime numbers
						if(integer[step] != 0){
							integer[step] = 0;
							primeCounter--;
						}
						step += i;
					}
				}
			
			for(int i = 2; i < start; i++)					// kick out all the prime numbers that are out of the lower bound
				if(integer[i] != 0)
					primeCounter--;
		} while(primeCounter < num);
		
		int prime[] = new int[num];							// create a resulting array
		int pointer = 0;
		for(int i = start; i < range; i++){
			if(integer[i] != 0)								// catch all the prime elements out of the temporary array
				prime[pointer++] = integer[i];				// and put it into the resulting array
			if(pointer >= num) break;
		}
		
		return prime;										// finally, return the resulting array
	}
	
	// ========================================================================================= //
	//		Name: printMenu																		 //
	//		Input: none																			 //
	//		Output: none																		 //
	//		Description: show the menu															 //
	// ========================================================================================= //
	private static void printMenu(){
		System.out.println("=================== Operations that you can take ===================");
		System.out.println("PUSH #NUMBER: push #NUMBER of integers into the stack");
		System.out.println("POP #NUMBER: pop #NUMBER of integers out of the stack");
		System.out.println("SHOW ALL: show all the elements in the stack");
		System.out.println("MENU: show the menu");
		System.out.println("EXIT: exit the program");
		System.out.println("=====================================================================");
	}
}