package a5.s100502010;

public class StackOfIntegers 
{
	private static int memory=0;
	static int[] primenum=new int[101];
	public static void showALL()//show the prime number in the array
	{
		for(int counter=0;counter<memory;counter++)
		{
			System.out.print(primenum[counter]+" ");
		}
		System.out.print("\n");
	}
	public static void Pop(int number)//pop and make sure that move won't empty the stack
	{
		isEmpty(number);
	}
	public static void Push(int number)//push and make sure that move won't let the stack full
	{
		int should=isFull(memory+number);
		if(memory+number<=100)
		{
			memory=memory+number;
		}
		int which=0;
		if(should==1)
		{
			for(int counter1=1;which<=memory;counter1++)//find prime numbers
			{
				int count=0;
				for(int counter2=1;counter2<=counter1;counter2++)
				{
					if(counter1%counter2==0)
					{
						count++;
					}
				}
				if(count==2)
				{
					primenum[which]=counter1;
					which++;
				}
				count=0;
			}	
		}
		else if(should==0)
		{
			memory=100;
			for(int counter1=1;which<=memory;counter1++)//find prime numbers
			{
				int count=0;
				for(int counter2=1;counter2<=counter1;counter2++)
				{
					if(counter1%counter2==0)
					{
						count++;
					}
				}
				if(count==2)
				{
					primenum[which]=counter1;
					which++;
				}
				count=0;
			}	
		}
		
		
		for(int counter3=0;counter3<memory;counter3++)
		{
			System.out.print(primenum[counter3]+" ");
		}
		System.out.print("\n");
	}
	public static void isEmpty(int input)//check if the stack is empty
	{
		if(memory-input<=0)
		{
			System.out.print("this will cause the stack empty!!");
			System.out.print("initial the stack");
			memory=0;
		}
		else
			Push(-input);
	}
	public static int isFull(int input)//check if the stack is full
	{
		if(input>100)
		{
			System.out.println("this will cause the stack full!!");
			return 0;
		}
		else
			return 1;
	}
}
