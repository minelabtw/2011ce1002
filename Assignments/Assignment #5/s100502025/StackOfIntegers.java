package a5.s100502025;

public class StackOfIntegers {
	
	private static java.util.ArrayList<Integer> list = new java.util.ArrayList<Integer>();  //ArrayList class
	
	public static void GetPrimeNumber(int num){  //質數產生器
		if(list.size() == 0){  //若陣列裡面還沒有值
			list.add(2);  //先將2存入第一個陣列中
			num = num -1;  //這裡消耗了一次，所以要減掉一次
		}
		if(num > 0){  //若剩下的次數大於0
			for(int k = 1 ; k <= num ; k++ ){  //產生num個質數
				for(int i = list.get(list.size()-1) + 1 ;i < 600 ;i++){  //從原本最後一個質數之後開始判斷
					int check = 0;  //檢查是否為質數
					for(int j = 1 ; j <= i ; j++){  
						if((i%j) == 0){  //從1開始判斷，如果能夠整除某數，check會加1
							check++;  //若check > 3 ，則非質數，因為check = 2 才是質數，
						}
					}
					if(check == 2){  //如果是質數，就只有1和本身能夠整除
						push(i);  //將這個質數存入陣列中
						break;  //結束這個質數判斷的for迴圈
					}
				}
			}	
		}
	}
	
	public void showAll(){  //顯示現有的質數
		if(list.size() == 0){  //若現在一個質數也沒有
			System.out.println("The stack is empty!");  //顯示這句話
		}
		else{   
			System.out.print("The stack here is:\n");
			for(int i = 0 ; i < list.size(); i++ ){
				if(i == 100){  //若超過一百次
					break;  //結束顯示之後的質數
				}
				System.out.print(list.get(i) + " ");  //顯示現有的質數，若超過第100個就不會顯示
			}
			System.out.print("\n");
		}		
	}
	
	public static void push(int num){  //存入質數
		list.add(num);  
	}
	
	public static void pop(int num){  //移除質數
		if(num > list.size()){  //如果輸入次數大於現有的項數
			num = list.size();  //那把剩下的都刪掉
		}
		
		for(int i = 1 ; i <= num ; i++){  //每次移除一個，直到移除num個為止
			list.remove(list.size()-1);							
		}
	}
	
	public boolean isFull(){  //判斷是否滿了
		if(list.size() == 100){  
			return true;  //如果滿了，回傳true
		}
		return false;  //如果沒有滿，回傳false
	}
	
	public static boolean isEmpty(){  //判斷是否空了
		return list.isEmpty();  //回傳true或是false
	}
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
