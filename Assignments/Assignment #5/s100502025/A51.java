package a5.s100502025;

import java.util.Scanner;

public class A51 {
	public static void main(String[] args){  //主程式
		Scanner input = new Scanner(System.in);  
		StackOfIntegers stack = new StackOfIntegers();  //將class實體化
		boolean stop = true;  //是否終止程式
			
		while(stop){
			System.out.print("1.Push\n2.Pop\n3.Show all\n4.exit\n");
			System.out.print("Choice : ");
			int choice = input.nextInt();  //選擇
			switch(choice){
				case 1:  //push
					System.out.print("Push~~~How many numbers:");
					int push = input.nextInt();
					stack.GetPrimeNumber(push);  //質數產生器
					stack.showAll();
					break;
				case 2:  //pop
					System.out.print("Pop~~~How many numbers:");
					int pop = input.nextInt();
					stack.pop(pop);
					stack.showAll();
					break;
				case 3:  //show all
					stack.showAll();
					break;
				case 4:  //exit
					stop = false;  //終止程式的判斷
					break;
				default:
					break;
			}
		}				
	}
}