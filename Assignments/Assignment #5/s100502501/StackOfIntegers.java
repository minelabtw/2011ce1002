package a5.s100502501;

public class StackOfIntegers {
	private int[] prime=new int[100];
	private int size;
	
	public StackOfIntegers(){ //constructor
		int n=0;
		for(int i=2;i<1000;i++){
			if(isPrime(i)){ //if number is prime,store the number into the stack
				prime[n]=i;
				n++;
			}
			if(n==100) //if stack has 100 elements already ,stop the loop
				break;
		}
	}
	public void showAll(){ //Show all the elements in stack
		if(isFull()){
			for(int a=0;a<(size-(size-100));a++){
				System.out.print(prime[a]+" ");
			}
			System.out.print("\nThe stack is FULL!\n");
		}
		else if(isEmpty())
			System.out.print("The stack is EMPTY!\n");
		else{
			for(int i=0;i<size;i++)
			   System.out.print(prime[i]+" ");
			System.out.print("\n");
		}
	  }	
	public void Push(int num) { //push the input number into the stack
		size+=num;
		if(isFull()){ // >100 elements 
			for(int a=0;a<(size-(size-100));a++){
				System.out.print(prime[a]+" ");
			}
			System.out.print("\nThe stack is FULL!\n");
			size=100;
		}
		else{
			for(int a=0;a<size;a++)
				System.out.print(prime[a]+" ");
		}
		System.out.print("\n");
	  }

	public void Pop(int num) { //pop the input number from the stack
		size-=num;
		if(isEmpty()){ //situation of no element
			System.out.print("The stack is EMPTY!\n");
			size=0;
		}
		else{
			for(int a=0;a<size;a++)
				System.out.print(prime[a]+" ");
			System.out.print("\n");
		}
	  }
	public boolean isEmpty(){ //check if the stack is empty
		   return size<=0;
	  }
	public boolean isFull(){ //check if the stack is full
		   return size>=100;
	  }
	public static boolean isPrime(int n){ //check if the number is prime 
		  boolean flag=true;
		  for (int i=2;i<=Math.sqrt(n);i++){
			  if (n%i==0){
				  flag=false;
			      break;
			  }
		  }
		  return flag;
		 }	
}
