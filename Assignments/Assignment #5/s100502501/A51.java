package a5.s100502501;
import java.util.Scanner;
public class A51 {
	public static void main(String[] args){
		Scanner user=new Scanner(System.in); //create a new scanner
		StackOfIntegers stack=new StackOfIntegers(); //create a new StackOfIntegers object stack
		boolean flag=false;
		while(!flag){
			System.out.print("\n1.push\n2.pop\n3.Show all\n4.exit\nChoose one : ");
			int choice=user.nextInt();
			switch(choice){
				case 1: //push elements from the stack
					System.out.print("How many integers do u want push? ");
					int num1=user.nextInt();
					stack.Push(num1);
					break;
				case 2: //pop elements from the stack
					System.out.print("How many integers do u want pop? ");
					int num2=user.nextInt();
					stack.Pop(num2);
					break;
				case 3: //show the current elements in the stack
					stack.showAll();
					break;
				case 4: //exit
					flag=true;
					break;
			}
		}
		System.out.print("Bye!");
	}
}
