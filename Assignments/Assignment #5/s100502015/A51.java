package a5.s100502015;
import java.util.Scanner;
import a5.s100502015.StackOfIntegers;

public class A51 
{
	public static void main(String[]args)
	{
		StackOfIntegers s1 = new StackOfIntegers();	
		Scanner input = new Scanner(System.in);				
		boolean flag = true;
		while(flag)
		{
			System.out.print("\n1.show all 2.push 3.pop 4.exit");//option	
			int select = input.nextInt();
			switch(select)
			{
			case 1:			
				s1.showall();//show all
				break;
			case 2:			
				System.out.print("input number");//push
				int number = input.nextInt();
				s1.push(number);
				break;
			case 3:
				System.out.print("input a number");//pop
				int number1 = input.nextInt();				
				s1.pop(number1);
				break;
			case 4://exit
				flag = false;
				break;
			}
		}
	}
}
