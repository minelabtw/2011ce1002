package a5.s100502015;

public class StackOfIntegers 
{
	private int[]store = new int [101];//store 
	private int control = 0;//control how many number to show
	public  void showall()
	{
		for(int i=1;i<=control;i++)
		{
			System.out.print(store[i]+" ");
		}
	}
	public void push(int input)
	{
		int number = 2;// start from 2
		int check = 0;//check if it is a prime number
		int count = 1;		
		control += input;		
		while(count <= control)
		{				
			for(int j=1;j<number;j++)
			{
				if(number%j==0)
				{
					check +=1;					
				}								
			}			
			if(check==1)//if number is a prime number then store in store array
			{
				store[count] = number;
				count++;//we get 1 prime number
			}			
			number+=1;
			check = 0;
		}
		isFull();	
		showall();
		
	}
	public void pop(int input)
	{
		control -= input;
		isEmpty();
		showall();
	}
	public void isEmpty()
	{
		if(control ==0)
		{
			System.out.print("Empty\n");
		}
			
	}
	public void isFull()
	{
		if(control==100)
		{
			System.out.print("full\n");
		}
		
	}
}
