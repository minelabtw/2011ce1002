package a5.s100502034;
import java.util.Scanner;
import a5.s100502034.StackOfIntegers;
public class A51 {
	public static void main(String[] args){
		//這裡開始是計算質數的東西
		int[] ans= new int[25];//因為GOOGLE說1~100的質數只有一百個 我就懶得再做計算啦
		ans[0] = 2;//2這個計算起來好像比較麻煩 所以自己定義算了
		int number = 1;//這個是array ans的參數
		for (int i = 2; i <= 100; i++){//i是用來被測試是否質數的數字
			for (int j = 2; j <i; j++){
				if (i % j == 0){//能被整除就不是質數嚕
					break; 
				}
				if (j >= i/2){//通過一大堆考
					ans[number] = i;
					number++;
					break;
				}
			}
		}
		//這裡是the end of 質數計算
		boolean leave = false;//功能循環的東西
		int choose = 0;//選項設定的變數
		int push = 0;//push的數量
		int pop = 0;//pop的數量
		Scanner input = new Scanner(System.in);
		StackOfIntegers stacksTest = new StackOfIntegers(ans);//宣告一個StackOfIntegers
		while (leave == false){
			System.out.println("1.push\n2.pop\n3.Show all\n4.exit\n");
			choose = input.nextInt();
			switch(choose){
			case 1:
				System.out.println("please enter the number of push member");
				push = input.nextInt();
				stacksTest.push(push);//利用StackOfIntegers的push功能push
				stacksTest.showAll();
				System.out.println();
				break;
			case 2:
				System.out.println("please enter the number of pop member");
				pop = input.nextInt();
				stacksTest.pop(pop);//利用StackOfIntegers的pop功能pop
				stacksTest.showAll();
				System.out.println();
				break;
			case 3:
				stacksTest.showAll();//利用StackOfIntegers的showAll功能showAll
				System.out.println();
				break;
			case 4:
				System.out.println("good bye");
				leave = true;
				break;
			default:
				System.out.println("Sorry, you have enter a wrong picking\n");//只要輸入'4'才能離開 其他當作錯誤輸入(如果使用者輸入不是int的話.....我也沒辨法了 ERROR)
				
			}
		}
	}

}
