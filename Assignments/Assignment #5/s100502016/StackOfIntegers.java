package a5.s100502016;

import java.lang.Math;

public class StackOfIntegers {
	private static int[] stack = new int[100];
	private static int currentNumber = 0;

	public void showAll() { // Show all the elements in stack.
		if (isEmpty()) {
			System.out.print("the stack is empty!!");
		} else {
			for (int i = 0; i < 100; i++) {
				if (stack[i] == 0) {
					break;
				} else {
					System.out.print(stack[i] + " ");
				}

			}
		}
		System.out.println("");
	}

	public void Push(int num) { // Take input as argument, and push the
										// input into the stack.
		currentNumber += num;
		for (int i = 2, remains = currentNumber, stackSequence = 0; remains > 0; i++) {
			if (isPrimeNumber(i)) {
				if (isFull()) {
					currentNumber = 100;
					break;
				}
				stack[stackSequence] = i;
				remains--;
				stackSequence++;
			}
		}

	}

	public void Pop(int num) { // Take input as argument, and pop such
										// number of elements from the stack.
		for (int i = currentNumber - 1; i >= currentNumber - 1 - num; i--) {
			if (isEmpty()) {
				break;
			}
			stack[i] = 0;
		}
		if (currentNumber - num >= 0)
			currentNumber -= num;
		else
			currentNumber = 0;

	}

	public boolean isEmpty() { // Check the stack whether it is empty or
										// not.
		if (stack[0] == 0)
			return true;
		else
			return false;
	}

	public boolean isFull() { // Check the stack whether it is full or
										// not.
		if (stack[99] != 0)
			return true;
		else
			return false;

	}

	public boolean isPrimeNumber(int num) { // Check the number is prime
													// or not.
		for (int i = 2; i <= Math.sqrt(num); i++) {
			if ((num % i) == 0)
				return false;
		}
		return true;

	}

}
