package a5.s100502016;

import java.util.Scanner;

public class A51 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);	
		StackOfIntegers stackOfIntegers = new StackOfIntegers();
		boolean exit = false;
	
		while (!exit) {
			System.out.println("1.push \n2.pop \n3.Show all \n4.exit");
			int option = input.nextInt();
			
			switch (option) {
			case 1:
				int num = input.nextInt();
				stackOfIntegers.Push(num);
				stackOfIntegers.showAll();
				break;
			case 2:
				num = input.nextInt();
				stackOfIntegers.Pop(num);
				stackOfIntegers.showAll();
				break;
			case 3:
				System.out.println("the stack here is :");
				stackOfIntegers.showAll();
				break;
			case 4:
				exit = true;
				break;
			}
		}

	}
}
