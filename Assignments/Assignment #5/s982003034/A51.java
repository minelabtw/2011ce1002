package a5.s982003034;

import javax.swing.JOptionPane;

public class A51 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		final int TA_limit = 100; // because TA's decision is final
		StackOfIntegers stack = new StackOfIntegers (TA_limit);
		int last = 0; // we begin to find the prime number from 0
		int sel = 0;
		
		while (true) {

			sel = getInt ("Please make a selection\n1.push\n2.pop\n3.show all\n4.exit\n", 4);
			switch (sel) {
			
			case 1:
			{ // this is a new fact that I learned today
			// when using switch-case, a variable declared inside a case can be used in another case
			// that is the scope of the variable is the entire switch block
			// that is why there is these {} in order to "seal" the variable "int i" only in this case
					int i = getInt ("How many numbers you would like to push?", 0);
				
				// check if your input is within TA's limit
				if (i + stack.getSize() > TA_limit) {
					JOptionPane.showMessageDialog(null, "Warning, you exceeded the TA's limit");
					i = 0;
				}
				
				for (; i > 0 ; i--) {
					last = nextPrime (last);
					stack.push(last);
				}
					
				JOptionPane.showMessageDialog(null, "New stack after push: " + stack.showAll());
				break;
			} // case 1
				
			case 2:
			{ // per reason above
				int i = getInt ("How many numbers you would like to pop?", 0);
				
				int copy = last; // copy is necessary because stack.pop(int) may return null pointer in case of error
				
				// get the last (the current biggest prime number) from the stack for the next push
				try {last = stack.pop(i)[i-1] - 2;}
				// the -2 is necessary because nextPrime will find a prime number bigger than last
				// suppose the -2 isn't there, the stack will not contain "last" because nextPrime will find another prime number bigger than "last"
				// while we should remember that without -2, "last" or whatever is popped by "stack", is also a prime number
				
				// undo in case of failure
				catch (NullPointerException e) {
					last = copy;
					JOptionPane.showMessageDialog(null, "Warning, you tried to pop more than what inside the stack!");
				}
				
				JOptionPane.showMessageDialog(null, "New stack after pop: " + stack.showAll());
				
				break;
			} // case 2
				
			case 3:
				JOptionPane.showMessageDialog(null, "Current stack:\n" + stack.showAll());
				break;
				
			case 4:
				JOptionPane.showMessageDialog(null, "Bye bye!");
				System.exit(0);
				break;
				
				default:break;
				
			} // switch
			
		} // while
		
	}
	
	// function to get an integer
	// inputs:
	// String message = for displaying a message to user
	// int nullreply = decide what the function will return if null input is encountered
	// -> this additional argument is necessary
	// because the function doesn't really know what integer that you expect
	public static int getInt (String message, int wrongformatreply) {
		String input = null;
		int temp;
		input = JOptionPane.showInputDialog(message);
		try {temp = Integer.parseInt (input);}
		// try to handle error more gracefully
		catch (NumberFormatException e) {return wrongformatreply;}
		return temp;
	}
	
	// get the next prime number
	// begin searching from "begin"
	// returns a prime number bigger than begin
	public static int nextPrime (int begin) {
		
		// this part is to check for the only special case where a prime number is an even number
		if (begin < 2) return 2;
		// the tries part is to make sure that the number we are searching begins from odd number
		else if (begin%2 == 0) begin ++;
		else begin += 2;
		
		// work until begin is a prime number, that is it can only be divided by one or itself
		// if tries != begin, means that begin can be divided by other number than itself
		// when that happens, get the next odd number
		// 											try to divide "begin" by every odd number smaller than it
		// 											keep trying new odd number until we found begin's multiplicand
		for (int tries = 3; tries == begin; begin +=2) for (tries = 3; tries%begin == 0; tries+=2);
		return begin;
		
		
	} // nextPrime

}
