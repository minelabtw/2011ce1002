package a5.s982003034;

public class StackOfIntegers {
	
	private int [] stack;
	private int size;
	private int capacity;
	
	StackOfIntegers (int caps) {
		capacity = caps;
		size = 0;
		stack = new int [capacity];
	}
	
	StackOfIntegers () {
		this(10);
	}
	
	public String showAll() {
		String result = "";
		for (int i = 0; i < size; i ++) {
			result = result + stack[i] + " ";
		}
		return result;
	}
	
	public int[] getAll() {
		int [] result = new int [size];
		for (int i = 0; i < size; i ++) {
			result[i] = stack[i];
		}
		return result;
	}
	
	public int getSize () {
		return size;
	}
	
	public int getCaps () {
		return capacity;
	}
	
	public boolean isEmpty() {
		if (size <= 0) return true;
		else return false;
	}
	
	public boolean isFull() {
		if (size >= capacity) return true;
		else return false;
	}
	
	// pop a number of integers
	// returns the pointer to an array containing popped integers
	public int[] pop (int num) {
		
		// check if the requested number is:
		// negative
		// larger than what's inside
		if (num < 0 || num > size) return null; // returns null in case of error
		
		else {
			
			int [] temp = new int [num];
			
			for (int i = 0; i < num; i ++) {
				// reduce size first to avoid overflow
				size --;
				temp [i] = stack [size];
				
			}
			return temp;
		} // else
		
	} // pop
	
	// pop an integers
	// returns the  popped integers
	public int pop () {
		// check whether the stack is empty or not
		if (isEmpty()) return 0; // return zero if empty
		else { 
			// reduce size first to avoid overflow
			size --;
			return stack[size];
		}
		
	} // pop
	
	// push integer n into the stack
	// returns the new size
	public int push (int n) {
		
		// if the stack is full
		if (this.isFull()) resize (capacity *2); // make a larger stack
		// push
		stack [size] = n;
		size ++;
		return size;
		
	}
	
	// resize the max capacity of the stack to new_caps
	// 		the function has the capability of cutting down the size of the stack
	//		but provides no guarantee of what will happen to the rest of the data
	// returns the difference between the old and new capacity
	// if the return value is negative, means the new stack is smaller
	// WARNING the function do not check for attempts of resizing a stack larger than your computer memory
	public int resize (int new_caps) {
		
		// do nothing if the new capacity is negative
		if (new_caps < 0) return 0;
		
		// create a room for a new stack
		int [] temp = new int [new_caps];
			
		// decide which one is smaller
		// this is to avoid overflow when resizing to a smaller array
		int smaller = size;
		if (new_caps < size) smaller = new_caps;
		
		// copy the stack
		for (int i = 0; i < smaller; i++) {
			temp [i] = stack [i];
		}
		// repoint stack to temp
		stack = temp;
		// leave the rest of the data to the garbage collector
		
		// reset the size data
		size = smaller;
		// set the new capacity
		int old = capacity;
		capacity = new_caps;
		
		// returns the difference between the old and new capacity
		// if the return value is negative, means the new stack is smaller
		return new_caps-old;
		
	}
	
}
