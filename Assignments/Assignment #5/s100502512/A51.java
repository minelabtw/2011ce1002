package a5.s100502512;

import java.util.Scanner;

public class A51 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		for (;;) {//無限輸入
			System.out.println("Please choose what you want to see:" + "\n"
					+ "1.push" + "\n" + "2.pop" + "\n" + "3.Show all" + "\n"
					+ "4.exit" + "\n");
			int i = input.nextInt();
			switch (i) {
			case 1:
				System.out
						.println("Please input how many numbers you want to push:");
				int n = input.nextInt();
				StackOfIntegers.Push(n);//從StackOfIntegers叫出Push
				break;
			case 2:
				System.out
						.println("Please input how many numbers you want to pop:");
				int m = input.nextInt();
				StackOfIntegers.Pop(m);//從StackOfIntegers叫出Pop
				break;
			case 3:
				StackOfIntegers.showAll();//從StackOfIntegers叫出showAll
				break;
			case 4:
				System.exit(0);//跳出
				break;

			}

		}
	}
}
