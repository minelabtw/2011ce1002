package a5.s100502512;

public class StackOfIntegers {
	static int stacks[] = new int[100];//輸出的stack
	static int time = 0;// 儲存總項數
	static int P;//stack裡的項數

	public static void showAll() {//顯示結果
		System.out.println("The stack there is:");
		for (int i = 0; i < time; i++) {
			System.out.print(stacks[i] + " ");
		}
		System.out.println(" ");
	}

	public static void Pop(int num) {
		int stacksS[] = new int[100];
		time -= num;//總數減掉所想要去掉的數目
		int[] stacksA = new int[100];
		for (int i = 0; i < time; i++) {
			stacksA[i] = stacks[i];//減去後的stack
		}
		for (int x = 0; x < 100; x++) {
			stacks[x] = stacksS[x];//初始化
		}
		for (int i = 0; i < time; i++) {
			stacks[i] = stacksA[i];//使結果回傳輸出的stack
		}
		if (StackOfIntegers.isEmpty()) {//檢查是不是空的
			System.out.println("the stack is empty");
		} else {
			for (int x = 0; x < time; x++) {
				System.out.print(stacks[x] + " ");//輸出結果
			}
			System.out.println(" ");
		}
	}

	public static void Push(int num) {
		int D;//整除次數
		int first = 2;//第一個質數
		time += num;//總數加上所想要加的數目
		for (int z = 2; z <= first; z++) {//判斷是不是質數
			D = 0;
			for (int j = 2; j <= first; j++) {
				if ((first % j) == 0) {
					D++;
				}
			}
			
			if (D == 1) {//如果整除次數不等於一次則代表不是質數(ex:4,6)質數整除次數只有一次
				stacks[P] = first;
				P = P + 1;
				first = first + 1;
			} else if (D != 1) {
				first = first + 1;
			}
			if (z == 99) {//如果項數等於99項則停止
				break;
			}
			
		}
		
		for (int k = 0; k < time; k++) {
			System.out.print(stacks[k] + " ");
		}
		System.out.println(" ");
	}

	public static boolean isEmpty() {//判斷項數是不是空的
		if (time == 0) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isFull() {//判斷有沒有超過最大項數(100)
		if (time >= 100) {
			return true;
		} else {
			return false;
		}
	}
}
