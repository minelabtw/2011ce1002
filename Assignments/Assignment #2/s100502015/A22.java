package a2.s100502015;
import javax.swing.JOptionPane;
import java.lang.Math;
public class A22 
{
	public static void main(String[]arg)
	{
		String rstring = JOptionPane.showInputDialog(null,"input radius");
		double radius = Integer.parseInt(rstring);
		String coordinateXstring = JOptionPane.showInputDialog(null,"input coordinate X");
		String coordinateYstring = JOptionPane.showInputDialog(null,"input coordinate Y");
		double coordinateX = Integer.parseInt(coordinateXstring);//convert string to double
		double coordinateY = Integer.parseInt(coordinateYstring); 
		double a = Math.sqrt(coordinateX*coordinateX+coordinateY*coordinateY);//count how long is coordinate
		if (a < radius)
		{
			JOptionPane.showMessageDialog(null,"在園內");
		}
		else if( a == radius)
		{
			JOptionPane.showMessageDialog(null,"在園上");
		}
		else
		{
			JOptionPane.showMessageDialog(null,"在園外");
		}
	}
}
