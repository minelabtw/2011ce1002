package a2.s100502510;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class A22 {
	public static String circle(double x, double y, double r) {// 判斷(x,y)對圓的相對位子
		String result = "";
		double sum = (x * x + y * y);
		double R = r * r;
		if (sum < R) {
			result = "The dot is inside the circle.";
		} else if (sum == R) {
			result = "The dot is on the circle.";
		} else if (sum > R) {
			result = "The dot is outside the circle.";
		}
		return result;// 輸出相對位子

	}

	public static void main(String[] args) {
		System.out.print("Please input the radius:");
		Scanner input = new Scanner(System.in);
		double radius = input.nextDouble();
		System.out.print("Please input x:");
		double x = input.nextDouble();
		System.out.print("Please input y:");
		double y = input.nextDouble();
		JOptionPane.showMessageDialog(null, circle(x, y, radius));// 視窗化最後的結果
	}
}
