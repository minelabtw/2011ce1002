package a2.s100502025;

import java.util.Scanner;
import java.lang.Math;
import javax.swing.JOptionPane;

public class A22 {
	public static void main(String[] args){
		
		Scanner input = new Scanner(System.in);
		
		String Radius = JOptionPane.showInputDialog(null,"輸入圓的半徑:");
		Double R = Double.parseDouble(Radius);  //輸入半徑長
		
		String Xcoor = JOptionPane.showInputDialog(null,"輸入座標X:");
		Double X = Double.parseDouble(Xcoor);  //輸入X座標
		
		String Ycoor = JOptionPane.showInputDialog(null,"輸入座標Y:");
		Double Y = Double.parseDouble(Ycoor);  //輸入Y座標
		
		if(R > sqrt(X,Y)){
			JOptionPane.showMessageDialog(null , "( " + X + " , " + Y + " ) 在圓內");  //若距離小於半徑，則顯示這個點在園內
		}
		if(R == sqrt(X,Y)){
			JOptionPane.showMessageDialog(null , "( " + X + " , " + Y + " ) 在圓上");  //若距離等於半徑，則顯示這個點在園上
		}
		if(R < sqrt(X,Y)){
			JOptionPane.showMessageDialog(null , "( " + X + " , " + Y + " ) 在圓外");  //若距離大於半徑，則顯示這個點在園外
		}
	}
	
	public static double sqrt(double xcoordinate,double ycoordinate){
		double Distance = Math.sqrt(xcoordinate*xcoordinate + ycoordinate*ycoordinate);  //算出輸入的座標對原點的距離
		return Distance;  //回傳距離
	}
}
