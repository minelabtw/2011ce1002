package a2.s100502025;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class A21 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		
		int[] array = new int[10];  //宣告一個陣列array
		
		String stringofnumber1 = "";  //宣告一個字串，並設初始值為空的
		
		for(int i = 0 ; i < 10 ; i ++){
			
			String A = JOptionPane.showInputDialog(null,"輸入第" + ( i + 1 ) + "個數字:" );  //輸入十個數字		
			array[i] = Integer.parseInt(A);  //輸入整數型態的數字
			stringofnumber1 = stringofnumber1 + " " + array[i];  
		}
		JOptionPane.showMessageDialog(null,"原始的數列為\n" + stringofnumber1);  //顯示出原始的數列
		
		for(int j = 1 ; j < 10 ; j++){
			boolean use = false;  //use為Swap是否使用的判斷，使用為true，沒使用為false
			String stringofnumber2 = "";  //宣告一個字串，並設初始值為空的
			for(int i = 0 ; i < 9 ; i++){			
				if(array[ i ] > array[i+1]){  //若前者大於後者則交換
					Swap( i , i + 1 , array );  
					use = true;  //Swap有使用到
				}
				stringofnumber2 = stringofnumber2 + "  " + Integer.toString(array[i]);			
			}
			stringofnumber2 = stringofnumber2 + "  " + Integer.toString(array[9]);  //因為array[9]還沒存到字串中，所以額外加一次

			if(use == true){  //當Swap有使用到的時候才顯示出數列
				JOptionPane.showMessageDialog(null,stringofnumber2);  //顯示出轉換後的數列	
			}
		}
		JOptionPane.showMessageDialog(null,"轉換完成"); 	 //當轉換結束後，顯示出轉換結束
	}
	
	public static void Swap(int a , int b ,int[] arrayy){  //前者大於後者，兩個數值交換
				int temp = arrayy[a];  //先將前者存到temp中
				arrayy[a] = arrayy[b];  //後者數值存到前者裡面
				arrayy[b] = temp;  //再將temp存到後者
	}		
}
