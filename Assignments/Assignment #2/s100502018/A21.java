package a2.s100502018;

import javax.swing.JOptionPane;

public class A21 {
	public static void main(String[] args) {
		String input = JOptionPane.showInputDialog(
				null, // extra to decide the array size
				"Please decide how much integer you want to sort", "Size",
				JOptionPane.INFORMATION_MESSAGE);
		final int ARRAY_SIZE = Integer.parseInt(input); // array size is FINAL,
														// so can't be assign
														// again
		int[] numbers = new int[ARRAY_SIZE];
		String INME = "Bubble Sort";
		for (int i = 1; i <= ARRAY_SIZE; i++) {
			input = JOptionPane.showInputDialog(null, "Please enter integer "
					+ i, INME, JOptionPane.INFORMATION_MESSAGE); // input
																	// continuously
																	// until the
																	// array is
																	// filled
			numbers[i - 1] = Integer.parseInt(input);
		}
		String initial = "";
		for (int i = 1; i <= ARRAY_SIZE; i++) {
			initial += numbers[i - 1] + " "; // make a initial condition to let
												// users know what did they
												// input
		}
		JOptionPane.showMessageDialog(null, initial, "Initial",
				JOptionPane.INFORMATION_MESSAGE);
		String result = initial;
		for (int i = 1; i < ARRAY_SIZE; i++) { // start to test the statistics
			int swaptimes = 0;
			for (int j = 1; j < ARRAY_SIZE; j++) {
				if (numbers[j - 1] > numbers[j]) {
					Swap(j - 1, j, numbers);
					swaptimes++;
				}
			}
			String face = "";
			if (swaptimes > 0) { // make a face if the array has been "Swap"ed
				for (int k = 1; k <= ARRAY_SIZE; k++) {
					face += numbers[k - 1] + " ";
				}
				result += "\n" + face; // make a result after every "swap"
			}
		}
		JOptionPane.showMessageDialog(null, result, "Result", // show the result
				JOptionPane.INFORMATION_MESSAGE);
	}

	public static void Swap(int x, int y, int[] array) { // a method to swap the
															// statistics
		int temp = array[y];
		array[y] = array[x];
		array[x] = temp;
	}
}