package a2.s100502018;

import javax.swing.JOptionPane;
import java.lang.Math;

public class A22 {
	public static void main(String[] args) {
		String input = JOptionPane.showInputDialog(
				null, // decide the radius and assume the (0,0) as origin
				"Please input the radius", "Radius",
				JOptionPane.INFORMATION_MESSAGE);
		int radius = Integer.parseInt(input);
		input = JOptionPane.showInputDialog(null, "Please input the x",
				"X-axis", JOptionPane.INFORMATION_MESSAGE);// input the x
		int x = Integer.parseInt(input);
		input = JOptionPane.showInputDialog(null, "Please input the y",
				"Y-axis", JOptionPane.INFORMATION_MESSAGE);// input the y
		int y = Integer.parseInt(input);

		if (Math.sqrt(x * x + y * y) > radius) { // test the location of point
													// which user inputed
			JOptionPane.showMessageDialog(null, "It is outside the circle.",
					"Result", JOptionPane.INFORMATION_MESSAGE);
		} else if (Math.sqrt(x * x + y * y) == radius) {
			JOptionPane.showMessageDialog(null, "It is on the circle.",
					"Result", JOptionPane.INFORMATION_MESSAGE);
		} else
			JOptionPane.showMessageDialog(null, "It is inside the circle",
					"Result", JOptionPane.INFORMATION_MESSAGE);
	}
}
