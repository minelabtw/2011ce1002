package a2.s100502501;
import javax.swing.JOptionPane;
import java.lang.Math;
public class A22 {
	public static void main(String[] args){
		double[][] point=new double[1][2]; //create an array to store numbers
		String rad=JOptionPane.showInputDialog("Please input the radius: ");
		int radius=Integer.parseInt(rad); //store radius
		String px=JOptionPane.showInputDialog("Please input point X: ");
		point[0][0]=Double.parseDouble(px); //store point X
		String py=JOptionPane.showInputDialog("Please input point Y: ");
		point[0][1]=Double.parseDouble(py); //store point Y
		//judge the position of the point
		if(distance(point[0][0],point[0][1])==radius)
			JOptionPane.showMessageDialog(null,"The point is ON the circle !");
		else if(distance(point[0][0],point[0][1])<radius)
			JOptionPane.showMessageDialog(null,"The point is within the circle !");
		else
			JOptionPane.showMessageDialog(null,"The point is NOT within the circle !");
	}
	public static double distance(double x,double y){ //method to calculate the distance between point and the center of a circle
		return Math.sqrt((x*x)+(y*y));
	}
}