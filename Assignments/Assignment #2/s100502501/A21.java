package a2.s100502501;

import javax.swing.JOptionPane;

public class A21 {
	public static String array = "";
	public static void main(String[] args){
		int[] number=new int[10]; //create an array to store numbers
		boolean flag=false; //judge whether we did bubble sort or not
		for(int c=0;c<10;c++){
			String show=JOptionPane.showInputDialog("Please input "+(c+1)+"'s integer: ");
			number[c]=Integer.parseInt(show); //convert array from string to integer
		}
		for(int l=0;l<9;l++){
			flag=false;
			convert(number); 
			for(int i=0;i<9;i++){
				if(number[i]>number[i+1]){ //check whether number[i] is larger than number[i+1] or not
					Swap(i,i+1,number); //if it is,called method "Swap"
					flag=true;
				}	
			}
			array+=" �� ";
			convert(number);
			array+="\n";
			if(flag==false)
				break;			
		}
		JOptionPane.showMessageDialog(null,array); //output the result
	}
	
	public static void convert(int[] data){ //method to convert integer array to string array
		for(int p=0; p<10; p++){
			array+=data[p] + " ";
		}
	}
	public static int[] Swap(int a,int b,int[] array){ //method to change their values
		int temp=array[a];
		array[a]=array[b];
		array[b]=temp;
		return array;
	}
}

