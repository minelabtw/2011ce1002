package a2.s982003034;

import javax.swing.JFrame;

public class A21 {

	public static void main(String[] args) {
		
		int n = 10; // number of elements
		int[] array = new int[n]; // array for holding integers
		String result = ""; // result
		
		// create a new frame to hold the GUI
		JFrame myframe = new JFrame ("This is the second ***** assignment...");
		
		// get the customdialog for UI
		customdialog dialog = new customdialog (myframe);
		myframe.setSize(400, 300);
		myframe.add(dialog);
		myframe.setVisible (true);
		
		// get user input 10 times
		for (int i = 0; i < 10; i++) {
			array[i] = Integer.parseInt (dialog.getinput ("Please enter integer no " + (i+1)));
		}
		
		// sort
		result = bubblesort (array, n, 1);
		//System.out.println (result);
		// print output
		dialog.setoutput(result);

	}
	
	// bubble sort main algorithm
	// input: the integer array, it's size, and output level
	// output level:
	// 0 = print every step, 1 = print each pass, 2 = print only the final result, other = ignored (prints nothing)
	public static String bubblesort (int[] array, int n, int lvl) {
		
		String temp = "";
		
		for (int i=0; i < (n-1); i++){
			// the second 'for' loop will loop for (all elements - sorted elements)
			// in each step, the (n-1-i)th element will be sorted
			for (int j=0; j < (n-1-i); j++){
				
				// if an element is bigger than it's next element then swap
				if (array[j] > array[j+1]) {
					swap (j, j+1, array);
				} // swap
					
				if (lvl == 0) temp += print (array, n);
					
			} // j
				
			if (lvl == 1) temp += print (array, n);
				
		} // i
			
		if (lvl == 2) temp += print (array, n);
		return temp;
		
	}
	
	// function to print array
	// input: the array and it's size
	// output: string containing the array
	public static String print (int[] array, int n) {
		String temp = "";
		for (int i = 0; i < n; i++) {
			temp += "[";
			temp += array[i];
			temp += "] ";
		}
		temp += "\n";
		return temp;
	}
	
	public static void swap (int a, int b, int[] array) {
		
		int temp = array[a];
		array[a] = array[b];
		array[b] = temp;
		
	}

}
