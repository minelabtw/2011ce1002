package a2.s982003034;

import javax.swing.JFrame;

public class A22 {

	public static void main(String[] args) {
		
		// create a new frame to hold the GUI
		JFrame myframe = new JFrame ("This is the second ***** assignment...");
		
		// get the customdialog for UI
		customdialog dialog = new customdialog (myframe);
		myframe.setSize(400, 300);
		myframe.add(dialog);
		myframe.setVisible (true);
		
		// our circle
		int r = 0; // radius
		int x = 0; // point x position
		int y = 0; // point y position
		String result = ""; // result
		
		r = Integer.parseInt (dialog.getinput ("Please input circle's radius"));
		x = Integer.parseInt (dialog.getinput ("Please input point's x pos"));
		y = Integer.parseInt (dialog.getinput ("Please input point's y pos"));
		
		if (x*x+y*y < r*r) result = "Point is inside the circle";
		else if (x*x+y*y == r*r) result = "Point is right on the edge of the circle";
		else result = "Point is outside the circle";
		
		// print output
		dialog.setinfo ("XXX");
		dialog.setoutput(result);

	}

}
