package a2.s982003034;

import java.util.Random;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class customdialog extends JPanel implements ActionListener {

	private String inputtext; // String that store user input
	private JLabel infolabel; // label to put the system info
	private JTextArea outputarea; // textfield to display progarm output
	private JTextField textfield; // text field that receive user button
	private JButton ok; // OK button
	private boolean go; // for determining whether the button has been pressed or not
	
	// constructor
	customdialog (JFrame myframe) {
		
		// allows this class to gain control over myframe -- for drawing itself in the container

		// set components
		
		inputtext = "0";
		
		infolabel = new JLabel ();
		
		outputarea = new JTextArea ();
		outputarea.setEditable(false); // lock the text area
		
		textfield = new JTextField (10);
		textfield.setText("0"); // set this to zero to avoid conflict (in case the user press ok without typing anything)
		
		ok = new JButton ("Insert");
		ok.addActionListener(this);
		ok.setEnabled (true);
		
		go = false;
		
		// add components
		this.add (infolabel);
		this.add (textfield);
		this.add (ok);
		this.add (outputarea);
		
	}
	
	// this is the user interface main function
	// it displays "programinfo" to the label info
	// and then wait for user to type something in the text field and press ok button
	// then it will return the string that user had typed

	String getinput (String programinfo) {
		
		infolabel.setText(programinfo); // display the message to user
		// just wait until the action listener return something
		while (!go) {
			
			// do stupid things to waste computer resources
			// well, just simply acts like a loading screen
			Random generator = new Random (); // generate random number
			int x;
			String whatever = null;
			for (int i = 0; i < 50; i ++){
				x = generator.nextInt(); // get a random number
				x = x%128; // convert the x to fit ASCII range
				whatever += (char)x; // convert it to ASCII and append to the string
				outputarea.setText(whatever); // display the random number as ASCII char
			}
			
		}
		go = false; // reset for the next loop
		return inputtext;
		
	}
	
	// simply display a string to the output
	void setoutput (String output) {
		outputarea.setText (output);
	}
	
	// simply display a string to the info
	void setinfo (String info) {
		infolabel.setText (info);
	}
	
	// function to lock / unlock button
	void lockbutton () {
		if (!ok.isEnabled()) ok.setEnabled(true);
		else ok.setEnabled(false);
	}
	
	@Override
	// override the action listener
	// in this class the only event source is the OK button
	// so this part will process what happens if the user press the OK button
	public void actionPerformed(ActionEvent e) {
		inputtext = textfield.getText(); // get the string typed in the text field
		textfield.setText("");
		textfield.requestFocusInWindow();
		go = true; // get away from the loop
	}

}
