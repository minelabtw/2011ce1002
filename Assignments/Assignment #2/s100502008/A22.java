package a2.s100502008;
import java.lang.Math;
import javax.swing.JOptionPane;


public class A22 {
	public static void main(String[] args)
	{
		int radius=0;
		double x,y,dis;
		String input=JOptionPane.showInputDialog(null,"Enter the radius:","Input",JOptionPane.QUESTION_MESSAGE);//input radius
		String output;
		radius=Integer.parseInt(input);//transform integer to string
		String inputx=JOptionPane.showInputDialog(null,"Enter the x coordinate:","Input",JOptionPane.QUESTION_MESSAGE);//input x
		x=Double.parseDouble(inputx);//transform double to string
		String inputy=JOptionPane.showInputDialog(null,"Enter the y coordinate:","Input",JOptionPane.QUESTION_MESSAGE);//input y
		y=Double.parseDouble(inputy);//transform double to string
		dis= Math.sqrt(x*x+y*y);//compute the distance
		if(dis>radius)//outside
		{
			output="The point is outside the circle.";
		}
		else if(dis==radius)//on
		{
			output="The point is on the circle.";
		}
		else//inside
		{
			output="The point is in the circle";
		}
		JOptionPane.showMessageDialog(null, output);//output result
	}
}
