package a2.s100502008;
import javax.swing.JOptionPane;

public class A21 {
	public static void main(String[] args)
	{
		String input;
		String output="";
		int check=0;
		int[] num=new int[10];//save 10 integers
		for(int i=0;i<10;i++)//input 10 integers
		{
			input=JOptionPane.showInputDialog(null,"Enter intger"+(i+1)+":","Input",JOptionPane.QUESTION_MESSAGE);
			num[i]=Integer.parseInt(input);//transform string to integers
			output=output+num[i]+" ";
		}
		output=output+"\n";
		for(int i=0;i<10;i++)
		{
			check=0;//initialize
			for(int k=i;k<10;k++)
			{
				if(num[i]>num[k])//check to swap or not
				{
					Swap(i,k,num);
				}
			}
			for(int j=0;j<10;j++)//output array phase
			{
				output=output+num[j]+" ";
			}
			output=output+"\n";
			for(int l=0;l<9;l++)//check is done or not
			{
				if(num[l]<num[l+1]||num[l]==num[l+1])
				{
					check++;
				}
			}
			if(check==9)//finish
			{
				break;
			}
		}
		JOptionPane.showMessageDialog(null,output);//output result
	}
	public static void Swap(int a,int b,int[] array)//swap two numbers of array
	{
		int temp=array[a];
		array[a]=array[b];
		array[b]=temp;
	}
}
