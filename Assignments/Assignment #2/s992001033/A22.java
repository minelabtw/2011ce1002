package a2.s992001033;

import javax.swing.JOptionPane;//for GUI
import java.lang.Math;//for sqrt
public class A22
{
	public static void main (String[] args)
	{
		double radius = 0;//半徑
		while(radius<=0)
		{
			radius=Double.parseDouble(JOptionPane.showInputDialog(null,"Please input the radius."));
			if(radius<=0)
				JOptionPane.showMessageDialog(null,"radius can't lower than 0 or equal");//半徑不能小於等於0
		}
		double[] x = new double[1];//儲存x的座標值
		double[] y = new double[1];//儲存y的座標值
		x[0] = Double.parseDouble(JOptionPane.showInputDialog(null,"Enter the coordinate x."));
		y[0] = Double.parseDouble(JOptionPane.showInputDialog(null,"Enter the coordinate y."));
		JOptionPane.showMessageDialog(null,"座標是否在圓內?\n Ans: "+judgement(x[0],y[0],radius));//輸出結果
	}
	public static String judgement(double x,double y,double r)
	{
		if(Math.sqrt(x*x+y*y)<=r)//小於或等於代表在圓內
			return "Yes";
		else
			return "No";
	}
}
