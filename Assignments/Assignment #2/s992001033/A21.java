package a2.s992001033;

import javax.swing.JOptionPane;//for GUI
public class A21
{
	public static void main (String[] args)
	{
		int number=10;
		JOptionPane.showMessageDialog(null,"Please input "+number+" integer numbers!");//要求輸入幾個整數
		int[] sort = new int[number];//儲存的陣列
		for(int i=0;i<number;i++)
		{
			sort[i] = Integer.parseInt(JOptionPane.showInputDialog(null,"Please input 第"+(i+1)+"個 integer"));//接收第幾個整數
		}
		boolean judgement= false;//判斷是否有做交換，沒有則停止輸出
		Show(sort,number,0);
		for(int i=0;i<number-1;i++)//第幾次交換
		{
			judgement= false;//重置
			for(int j=0;j<number-1;j++)//交換的過程
			{
				if(sort[j]>sort[j+1])//如果大於則換
				{
					Swap(j,j+1,sort);
					judgement=true;//代表有交換動作
				}
			}
			Show(sort,number,i+1);
			if(judgement==false)//沒有就跳出
			{
				break;
			}
		}
	}
	
	public static int[] Swap(int a, int b, int[] array)//交換用的method
	{
		int temp=0;
		temp=array[b];
		array[b]=array[a];
		array[a]=temp;
		return array;
	}
	
	public static void Show(int[] array,int number,int time)//表示排列狀態的method
	{
		String result="";//最後丟到gui的訊息
		for(int i=0;i<number;i++)//慢慢疊加
		{
			if(i==0)//第一個數字
				result+="["+array[0];
			else if(i==number-1)//最後一個
				result+="、"+array[number-1]+"]";
			else
				result+="、"+array[i];		
		}
		JOptionPane.showMessageDialog(null,result,"第"+Integer.toString(time)+"次",JOptionPane.INFORMATION_MESSAGE);
	}
}
