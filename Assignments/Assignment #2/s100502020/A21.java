package a2.s100502020;

import javax.swing.*;
public class A21 {
	public static void swap(int a, int b, int[] array)
	{
		int c;
		c = array[a];
		array[a] = array[b];
		array[b] = c;		
	}
	public static void BubbleSortAlgorithm(int[] array)
	{
		for(int a=1;a<9;a++)//control each pass
	    {
	        System.out.println(a+"th pass:\n");
	        for(int c=0 ; c<=9 ; c++)
	        {
	        	System.out.print(array[c]+" ");	
	        }	                    
	        System.out.print("---> ");
	        for(int b=0;b<9;b++)//control each line
	        {            	            
	            if(array[b]>array[b+1])//if formal>latter swap it and output
	            {
	                swap(b, b+1, array);	                
	            }	                       
	        }
	        for(int c=0 ; c<=9 ; c++)
	        {
	        	System.out.print(array[c]+" ");	
	        }
	        System.out.println("\n\n");
	    }
	}
	
	public static void main(String[] args)
	{
		int [] Array = new int[10];//create an array
		
		for(int a=0 ; a<=9 ; a++)//dialog input and store to the array
		{
			String NumsInput = JOptionPane.showInputDialog("Plz input 10 int numbers: ");
			int nums = Integer.parseInt(NumsInput);
			Array[a] = nums;
		}
		BubbleSortAlgorithm(Array);
	}
}
