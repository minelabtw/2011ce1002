package a2.s100502512;

import java.util.Scanner;

import javax.swing.JOptionPane;

public class A21 {
	public static void main(String args[]) {

		Scanner input = new Scanner(System.in);
		System.out.println("please input 10 numbers:");// 輸入十個數字
		int[] data = new int[10];// 以陣列儲存
		String original = "";// 用一字串儲存原陣列
		for (int i = 0; i < 10; i++) {
			data[i] = input.nextInt();
			original = original + (data[i] + " ");
		}

		JOptionPane.showMessageDialog(null, "original:\n" + original + "\n"
				+ Swap(data));// 視窗顯示結果(GUI)
	}

	public static String Swap(int[] data) {
		String output = "";// 字串output來排列顯示結果
		for (int j = 0; j < 9; j++) {
			int a;
			a = data[9];
			output = output + ((j + 1) + "Pass:" + "\n");// 步驟
			int b = 10;
			for (int i = b - 1; i > 0; i--) {
				if (a < data[i - 1]) {
					data[i] = data[i - 1];
					data[i - 1] = a;
				} else {
					a = data[i - 1];
					
				}

			}// 排列數字大小順序,如果後者小於前者則位置互換

			for (int k = 0; k < 10; k++) {
				output = output + (data[k] + " ");
			}// 將計算過的陣列儲存於output中

			output = output + ("\n");

		}
		return output;// 回傳至結果
	}

}
