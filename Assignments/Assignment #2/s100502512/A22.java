package a2.s100502512;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class A22 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("please input radius:");// 輸入r
		int number = input.nextInt();
		System.out.println("please input x and y:");// 輸入x,y
		int number1 = input.nextInt();
		int number2 = input.nextInt();
		System.out.println(result(number1, number2, number)); // 輸出由result所判斷出的結果
	}

	public static String result(double x, double y, double r) {
		String output = "";// 輸出結果為一字串,例如:在圓外...
		double a = Math.sqrt((x * x) + (y * y));// x平方加y平方開根號(用Math.sqrt)
		if (a > r)// 若a>r則在圓外,a=r在圓上,a<r在圓內
		{
			JOptionPane.showMessageDialog(null, "在圓外");
		} else if (a == r) {
			JOptionPane.showMessageDialog(null, "在圓上");
		} else if (a < r) {
			JOptionPane.showMessageDialog(null, "在圓內");
		}
		return output;// 回傳結果
	}
}
