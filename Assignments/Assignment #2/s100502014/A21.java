package a2.s100502014;

import javax.swing.JOptionPane;

public class A21 {
	public static void main(String[] args) {
		String[] arrayString = new String[10];
		int[] array = new int[10];
		int flag=-1,phase=1;
		
		//Enter numbers
		for(int i=0;i<10;i++) {
			String output = "Please enter number " + (i+1);
			arrayString[i] =
				JOptionPane.showInputDialog(null,
					output,
					"Enter numbers!!!",
					JOptionPane.QUESTION_MESSAGE);
		}
		
		//change string to integer
		for(int i=0;i<10;i++) {
			array[i] = Integer.parseInt(arrayString[i]);
		}
		
		//bubble sort
		while(flag!=0) {
			flag=0; //initial the flag
			for(int i=0;i<9;i++) {
				//if left is bigger than right, they swap
				if(array[i]>array[i+1]) {
					swap(i,i+1,array);
					flag=-1; //has swapped
				}
			}
			
			//the result of string
			String output = phase + " phase\n" + array[0] + " " + array[1] +  " " + array[2] +
									" " + array[3] +  " " + array[4] +  " " + array[5] + 
									" " + array[6] +  " " + array[7] +  " " + array[8] +  " " + array[9];
			
			//show the result
			JOptionPane.showMessageDialog(null,
					output,
					"Phase!!!",
					JOptionPane.INFORMATION_MESSAGE);
			phase++;
		}
		
		//show the end
		JOptionPane.showMessageDialog(null,
				"The end!!!!",
				"End!!!",
				JOptionPane.INFORMATION_MESSAGE);
	}
	
	//swap
	public static void swap(int a, int b, int[] array) {
		int temp = array[a];
		array[a] = array[b];
		array[b] = temp;
	}
}
