package a2.s100502014;
import javax.swing.*;

public class A22 {
	public static void main(String[] args) {
		while(true) {
			//enter radius
			String radiusString = 
				JOptionPane.showInputDialog(null,
					"Please enter radius",
					"Radius!!!",
					JOptionPane.QUESTION_MESSAGE);
			int radius = Integer.parseInt(radiusString);	//change string to integer
			
			//enter x
			String xString = 
				JOptionPane.showInputDialog(null,
					"Please enter X",
					"X!!!",
					JOptionPane.QUESTION_MESSAGE);
			int x = Integer.parseInt(xString);		//change string to integer
			
			//enter y
			String yString = 
				JOptionPane.showInputDialog(null,
					"Please enter Y",
					"Y!!!",
					JOptionPane.QUESTION_MESSAGE);
			int y = Integer.parseInt(yString);		//change string to integer
			
			//out of the circle
			if(x*x+y*y>radius*radius) {
				JOptionPane.showMessageDialog(null,
					"Out of the circle!!",
					"Result!!!",
					JOptionPane.INFORMATION_MESSAGE);
			}
			
			//on the circle
			else if(x*x+y*y==radius*radius) {
				JOptionPane.showMessageDialog(null,
					"On the circle!!",
					"Result!!!",
					JOptionPane.INFORMATION_MESSAGE);
			}
			
			//in the circle
			else {
				JOptionPane.showMessageDialog(null,
					"In the circle!!",
					"Result!!!",
					JOptionPane.INFORMATION_MESSAGE);
			}
			
			//try it or not
			String optionString = 
				JOptionPane.showInputDialog(null,
					"Please choose your option\n" +
					"1. Try again\n" +
					"2. Exit",
					"Option!!!",
					JOptionPane.QUESTION_MESSAGE);
			int option = Integer.parseInt(optionString);	//change string to integer
			if(option!=1) {
				break;
			}
		}
	}
}
