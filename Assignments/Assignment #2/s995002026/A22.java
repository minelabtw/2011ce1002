package a2.s995002026;

import java.lang.Math;

import javax.swing.JOptionPane;

public class A22 {
	public static void main(String[] argv){
		
		JOptionPane.showMessageDialog(null, "請輸入圓的半徑");
		
		String r=JOptionPane.showInputDialog(null, "半徑");		//讀入半徑
		
		int radius=Integer.parseInt(r);							//把半徑從字串轉整數讀入
		
		double[] point=new double[2];
		
		JOptionPane.showMessageDialog(null,"請輸入X,Y座標: ");				
		
		String p1=JOptionPane.showInputDialog(null," X座標 ");		//輸入X座標
		
			point[0]=Integer.parseInt(p1);					//x座標從字串轉整數讀入
		
		String p2=JOptionPane.showInputDialog(" Y座標 ");				//輸入Y座標
		
			point[1]=Integer.parseInt(p2);					//y座標從字串轉整數讀入
		
		where(radius,point);
	}
	
	public static void where(int a,double[] c){
		
		if(Math.sqrt(c[0]*c[0]+c[1]*c[1])<a){			//判斷座標是否在圓內
			
			JOptionPane.showMessageDialog(null,"座標在圓內");
		
		}
		
		else if(Math.sqrt(c[0]*c[0]+c[1]*c[1])==a){			//判斷座標是否在圓上
			
			JOptionPane.showMessageDialog(null,"座標在圓上");

		}
		
		else if(Math.sqrt(c[0]*c[0]+c[1]*c[1])>a){			//判斷座標是否在圓外
			
			JOptionPane.showMessageDialog(null, "座標在圓外");
		}
	}
}
