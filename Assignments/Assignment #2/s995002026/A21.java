package a2.s995002026;

import javax.swing.JOptionPane;

public class A21 {
public static void main(String[] argv){
		
		
		int[] number=new int[10];				//紀錄全部的數字
		
		JOptionPane.showMessageDialog(null, "請輸入10個整數","Display Message",JOptionPane.PLAIN_MESSAGE);   //跳出訊息視窗
		
		for(int i=0;i<10;i++){					//讀入input的整數
			
			String value = JOptionPane.showInputDialog(null, "請輸入整數","慢慢輸入十次吧~",JOptionPane.PLAIN_MESSAGE);   //在視窗輸入數字

			number[i] =  Integer.parseInt(value);          //把字串轉成整數紀錄
		}
		
		for(int i=0;i<number.length-1;i++){		//最多交換的次數
				number=swap(i,number);
			
				String[] phase=  new String[10];		//用字串把每一個階段的結果儲存，跳出視窗的時候才能把每個階段的各個數字一起顯示出來
				
				for(int j=0;j<number.length;j++){
					
					phase[j]=Integer.toString(number[j]);
					
				}
				JOptionPane.showMessageDialog(null, phase, "介面好難用", JOptionPane.PLAIN_MESSAGE);
		}
	}
	
	public static int[] swap(int a,int[] array){		//泡沫排序法,array紀錄main輸入的全部整數,a讓迴圈不用多跑沒意義的迴圈
		int temp;										//暫存用變數
		
		for(int i=0;i<array.length-a-1;i++){				//將最大的整數往後放
			
			if(array[i]>array[i+1]){
				
				temp=array[i];
				
				array[i]=array[i+1];
				
				array[i+1]=temp;
			}
		}
		return array;									//回傳陣列讓main函式的陣列可以記到當下階段的狀態
	}
}
