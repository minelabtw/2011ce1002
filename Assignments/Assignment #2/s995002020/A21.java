package A2.s995002020;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

public class A21 extends JFrame implements ActionListener{
	private JButton button;
	private JLabel label;
	private TextField text;
	private String input,detail;
	private int[] num;
	private boolean show_detail;
	
	public A21(){//介面設定
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //出現左上角叉叉     
			detail="";
			show_detail=false;
			setBounds(100,100,300,600);
			setLayout(null);
			setResizable(false);
			text=new TextField();
			text.setBounds(0,0,220,50);
			add(text);
			button=new JButton("排序");
			button.setBounds(220,0,80,50);
			button.addActionListener(this);
			add(button);
			label=new JLabel("說明:輸入10個數字，數字間以1個空白鍵分格");
			label.setBounds(0,50,300,550);
			add(label);
			setVisible(true);
	}
	
	public static void main(String[] args){
		new A21();
	}
	
	public void change() throws Exception{//交換
		String x=text.getText();
		String[] x1=x.split(" ");
		if(x1.length>10 || x1.length<10){
			throw new Exception();
		}
		int[] c=new int[10];
		for(int i=0;i<10;i++){
			c[i]=Integer.parseInt(x1[i]);
		}
		num=c;
	}
	
	public void sort()throws Exception{ //排序
		int tmp,x=0;
		detail="";
        for(int i=0;i<10;i++) { 
            for(int j=0; j<10-i-1;j++) { 
                if(num[j+1]<num[j]) { 
                    tmp=num[j+1];
					num[j+1]=num[j];
					num[j]=tmp;
                } 
            } 
			detail=detail+(++x)+" : ";
					for(int k=0;k<10;k++)
						detail+="["+num[k]+"] ";
					detail+="<br>";
        }
		detail+="完成";
		show_detail=true;
	}
	public void showAns(){  //顯示答案
		String str="";
		for(int i=0;i<10;i++)
			str=str+num[i]+" ";
		text.setText(str);
	}	
	
	public void actionPerformed(ActionEvent e) { //show phase
		try{
			change();
			sort();
			showAns();
			if(show_detail){
				label.setText("<html>"+detail+"</html>");
				show_detail=false;
			}
		}catch(Exception event){//找出錯誤輸入
			text.setText("錯誤:請輸入10個數字");
		}
	}
}
