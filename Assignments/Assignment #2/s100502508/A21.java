package a2.s100502508;

import javax.swing.JOptionPane;
public class A21 
{
	public static void main(String[] args)
	{
		int[] series=new int[10];
		boolean flag=true;
		int times=0;
		JOptionPane.showMessageDialog(null,"Enter ten numbers");
		
		for(int a=1;a<=10;a++)//enter ten numbers and save them to array
		{
			String input=JOptionPane.showInputDialog("�� "+a+" ��");
			series[a-1]=Integer.parseInt(input);//convert string to integer
		}//end for
		
		while(flag)//repeat to arrange
		{
			int counter=0;
			String expressarray="-> ";
			times++;
			
			for(int b=1;b<10;b++)//exchange 
			{
				if(series[b-1]>series[b])
				{
					Swap(b-1,b,series);//call Swap method
					counter++;
				}
			}//end for
			
			if(counter!=0)
			{
				flag=true;
			}
			else
			{
				flag=false;
			}
			
			for(int c=0;c<10;c++)//add value of every item of array to expressarray
			{
				expressarray=expressarray+series[c]+"  ";
			}//end for
			
			JOptionPane.showMessageDialog(null,"��"+times+"��\n"+expressarray);//display result
		}//end while
		
		JOptionPane.showMessageDialog(null,"The end");
	}//end main
	
	public static void Swap(int first,int second,int[] receivearray)//exchange two value
	{
		int hold=receivearray[first];
		receivearray[first]=receivearray[second];
		receivearray[second]=hold;
	}//end Swap method
}
