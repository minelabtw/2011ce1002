package a2.s100502508;

import javax.swing.JOptionPane;
import java.lang.Math;
public class A22 
{
	public static void main(String[] args)
	{
		String radius=JOptionPane.showInputDialog("Enter the radius of a circle");
		double doubleradius=Double.parseDouble(radius);//convert string to double
		
		JOptionPane.showMessageDialog(null,"Enter two-dimensional coordinate X and Y");
		
		String x=JOptionPane.showInputDialog("Enter coordinate X");
		double doublex=Double.parseDouble(x);//convert string to double
		
		String y=JOptionPane.showInputDialog("Enter coordinate y");
		double doubley=Double.parseDouble(y);//convert string to double
		
		if(doubleradius<Math.sqrt(doublex*doublex+doubley*doubley))//Check whether the coordinate is within the circle
		{
			JOptionPane.showMessageDialog(null,"point ("+doublex+" , "+doubley+") is out the circle");//display result
		}
		else if(doubleradius==Math.sqrt(doublex*doublex+doubley*doubley))
		{
			JOptionPane.showMessageDialog(null,"point ("+doublex+" , "+doubley+") is on the circle");//display result
		}
		else 
		{
			JOptionPane.showMessageDialog(null,"point ("+doublex+" , "+doubley+") is in the circle");//display result
		}
	}//end main
}
