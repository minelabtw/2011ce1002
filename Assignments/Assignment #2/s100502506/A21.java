package a2.s100502506;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.awt.FlowLayout;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;

public class A21 extends JFrame
{
	private JLabel label1;					//use to show message
	
	private JTextField textField1;			//use to save the integers
	private JTextField textField2;
	private JTextField textField3;
	private JTextField textField4;
	private JTextField textField5;
	private JTextField textField6;
	private JTextField textField7;
	private JTextField textField8;
	private JTextField textField9;
	private JTextField textField10;
	
	private JButton checkButton1;			//use to check OK and do event
	private JPanel myPanel;					//use to install GUI on it
	
	
	public A21()							//the constructor
	{
		
		super("Buble Sort");				//set title
		myPanel=new JPanel();
		label1=new JLabel();
		
		label1.setText("請輸入10個整數");		//show message
		myPanel.add(label1);				//add label1 to myPanel
		setLayout(new FlowLayout());		//set frame layout
		
		////set 10 Textfield and add to myPanel////
		textField1=new JTextField(5);		
		myPanel.add(textField1);

		
		textField2=new JTextField(5);
		myPanel.add(textField2);

		
		textField3=new JTextField(5);
		myPanel.add(textField3);
		
		
		textField4=new JTextField(5);
		myPanel.add(textField4);
		
		
		textField5=new JTextField(5);
		myPanel.add(textField5);

		
		textField6=new JTextField(5);
		myPanel.add(textField6);

		
		textField7=new JTextField(5);
		myPanel.add(textField7);

		
		textField8=new JTextField(5);
		myPanel.add(textField8);

		
		textField9=new JTextField(5);
		myPanel.add(textField9);

		
		textField10=new JTextField(5);
		myPanel.add(textField10);
		/////////
		
		checkButton1=new JButton("OK");			//set button with OK
		checkButton1.addActionListener(new buttonListener());//set the ActionListener to do the event
		myPanel.add(checkButton1);				//add the checkButton to myPanel
		
		add(myPanel);//add myPanel to JFrame							
	}
	
	public static void main(String[] argc)		//main
	{
		A21 myJFrame = new A21();				//create A21
		myJFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		myJFrame.setSize(950,100);				//set frame size
		myJFrame.setVisible(true);				//display frame
	}
	
	public static void Swap(int x, int y, int[] array)//the swap method
	{
		int temp;
		temp=array[x];
		array[x]=array[y];
		array[y]=temp;
	}
	//inner class for button event handling
	private class buttonListener implements ActionListener
	{
		//handle button event
		public void actionPerformed(ActionEvent e)
		{
			int[] inputarray=new int[10];// create an array with 10 integers
			//take the 10 String of the TextFields to Integer
			inputarray[0]=Integer.parseInt(textField1.getText());
			inputarray[1]=Integer.parseInt(textField2.getText());
			inputarray[2]=Integer.parseInt(textField3.getText());
			inputarray[3]=Integer.parseInt(textField4.getText());
			inputarray[4]=Integer.parseInt(textField5.getText());
			inputarray[5]=Integer.parseInt(textField6.getText());
			inputarray[6]=Integer.parseInt(textField7.getText());
			inputarray[7]=Integer.parseInt(textField8.getText());
			inputarray[8]=Integer.parseInt(textField9.getText());
			inputarray[9]=Integer.parseInt(textField10.getText());
			//
			
			String message1=String.format("%d %d %d %d %d %d %d %d  %d %d", inputarray[0],inputarray[1] ,inputarray[2],inputarray[3],inputarray[4],inputarray[5],inputarray[6],inputarray[7],inputarray[8],inputarray[9]);
			
			boolean flag=false;
			int counter=0;
			do
			{
				flag=false;
				for(int i=0;i<inputarray.length-1;i++)//run array's length times
				{
					if(inputarray[i]>inputarray[i+1])//check if forward > backward
					{
						Swap(i,i+1,inputarray);//use swap method
						flag=true;	
					}	
				}
				counter++;
				//set message
				message1=String.format("第%d步    %d , %d , %d , %d , %d , %d , %d , %d , %d , %d",counter ,inputarray[0],inputarray[1] ,inputarray[2],inputarray[3],inputarray[4],inputarray[5],inputarray[6],inputarray[7],inputarray[8],inputarray[9]);
				
				JOptionPane.showMessageDialog(null, message1);//show MessageDialog
			}while(flag==true);
		}
	}
}
