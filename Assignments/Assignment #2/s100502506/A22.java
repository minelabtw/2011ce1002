package a2.s100502506;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Graphics;
public class A22 extends JPanel
{
	private int cir_radius;
	private int xlocation;
	private int ylocation;
	private JLabel label1;									//use to show message
	public void paintComponent(Graphics g)					//draw
	{
		super.paintComponent(g);
		
		int height=getHeight();								//get the panel's height
		int width=getWidth();								//get the panel's width
		
		double length_cir_point=Math.sqrt(Math.pow(xlocation,2)+Math.pow(ylocation,2));//calculate the length  between the point and the center
		//check if the  radius of circle > the length  between the point and the center
		boolean check_flag=false;
		if(length_cir_point<=cir_radius)
		{
			check_flag=true;
		}
		else if(length_cir_point>cir_radius)
		{
			check_flag=false;
		}
		//
		//set the message
		String message=String.format("此圓的半徑是 %d, 點位於(%d,%d),點距圓心%.3f,是否在圓心裡面 %b", cir_radius,xlocation,ylocation,length_cir_point,check_flag);
		
		label1 =new JLabel();
		label1.setText(message);							//show the message
		add(label1);										//add the message to panel
		
		
		g.setColor(Color.BLACK);							//set black color for the Graphics as follows
		g.drawLine(0,height/2,width,height/2);				//draw y line with black color
		g.drawLine(width/2,height,width/2,0);				//draw x line with black color
		g.drawOval(width/2-cir_radius, height/2-cir_radius, cir_radius*2, cir_radius*2);//draw the circle with black color
		g.setColor(Color.RED);								//set red color for the Graphics as follows
		
		//Draw the points 
		if(xlocation>0&&ylocation<0)
			g.fillOval(width/2+xlocation-3, height/2-ylocation-3, 5, 5);
		else if(xlocation>0&&ylocation>0)
			g.fillOval(width/2+xlocation-3, height/2-ylocation-3, 5, 5);
		else if(xlocation<0&&ylocation<0)
			g.fillOval(width/2+xlocation-3, height/2-ylocation-3, 5, 5);
		else if(xlocation<0&&ylocation>0)
			g.fillOval(width/2+xlocation-3, height/2-ylocation-3, 5, 5);
		else if(xlocation==0&&ylocation!=0)
			g.fillOval(width/2+xlocation-3, height/2-ylocation-3, 5, 5);
		else if(xlocation!=0&&ylocation==0)
			g.fillOval(width/2+xlocation-3, height/2-ylocation-3, 5, 5);
		//because we draw the circle with 5 pixels radius ,if we need to draw precisely,we need to -3 pixels to make the center of point on the right coordinate
		
		
		g.setColor(Color.blue);								//set blue color for the Graphics as follows
		g.drawLine(width/2, height/2, width/2+xlocation,height/2-ylocation);//draw the line between the point and the center
		
	}
	
	public void setCiRadius(int x)							//set the radius of circle
	{
		cir_radius=x;
	}
	
	public void setXloaction(int x)							//set the x coordinate of the point
	{
		xlocation=x;
	}
	
	public void setYlocation(int x)							//set the x coordinate of the point
	{
		ylocation=x;
	}
	
	public static void main(String[] argc)
	{
		int input_R;
		int x_loc;
		int y_loc;
		A22 myPanel=new A22();								//create myPanel
		
		do													//check if the radius is negative
		{
			String question1=JOptionPane.showInputDialog("Please enter the radius of Circle");//set question1
			input_R=Integer.parseInt(question1);			//set String to Integer
			if(input_R<0)									//if the radius is negative
			{
				JOptionPane.showMessageDialog(null,"The radius of circle can't be Negative.");
			}
			myPanel.setCiRadius(input_R);					//set radius
			
		}while(input_R<0);
		
		String question2=JOptionPane.showInputDialog("Enter the X location of point");
		x_loc=Integer.parseInt(question2);					//set String to Integer
		myPanel.setXloaction(x_loc);						//set x coordinate of point
		String question3=JOptionPane.showInputDialog("Enter the Y location of point");
		y_loc=Integer.parseInt(question3);					//set String to Integer
		myPanel.setYlocation(y_loc);						//set y coordinate of point
		
		JFrame application=new JFrame();					//create JFrame
		
		
		application.add(myPanel);							//add myPanel to JFrame
		application.setSize(500,500);						//set the size myPanel 
		application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		application.setVisible(true);						//show application
	}
}
