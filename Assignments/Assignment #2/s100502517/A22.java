package a2.s100502517;

//import java.lang.Math;
import javax.swing.JOptionPane;

public class A22 {
	public static void main(String[] args){
	String radius = JOptionPane.showInputDialog("Enter a radius: ");//let user enter the radius
	int r = Integer.parseInt(radius);
	
	String Xaxle = JOptionPane.showInputDialog("Enter your Xale: ");//let user enter the X axle
	double X = Double.parseDouble(Xaxle);
	
	String Yaxle = JOptionPane.showInputDialog("Enter your Yale: ");//let user enter the Y axle
	double Y = Double.parseDouble(Yaxle);
	
	double Z = X * X + Y * Y;
	double distance = Math.sqrt(Z);//find out the distance from (0,0) to (X,Y)
	
	//compare and show the result
	if(distance > r){
		JOptionPane.showMessageDialog(null,"("+X+" , "+Y+") is out of the circle");
	}
	if(distance == r){
		JOptionPane.showMessageDialog(null,"("+X+" , "+Y+") is on the circle");
	}
	if(distance < r){
		JOptionPane.showMessageDialog(null,"("+X+" , "+Y+") is in the circle");
	}
	
	}
}
