package a2.s100502517;

import javax.swing.JOptionPane;

public class A21 {
	
	
	public static void main(String[] args){
		int[] score = new int[10];
		int k;
		String output="";
		for(k = 0 ; k < 10 ; k++){// let user input the number
			String enternumber = JOptionPane.showInputDialog("Enter your No."+ (k+1) + " number: ");
			score[k] = Integer.parseInt(enternumber);//store in the array score[]	
		}
		
		int stop = 0;
		int order = 1;
		
		while(stop == 0){
			stop++;
			int run;
			
			output += "Step" + order + "\n";			
				
			for(run = 0 ; run < 10 ; run++){//put the primary number into the string output
				output += score[run]+" ";
			}
			
			output += " ----> ";
			
			int check;
			for(check = 0 ; check < 9 ; check++){//change the number
				if(score[check] > score[check+1]){
					int change = score[check];
					score[check] = score[check+1];
					score[check+1] = change;
					stop = 0;//if it works, the while will continue
				}					
			}			
			
			for(run = 0 ; run < 10 ; run++){//put the changed number into the string output
				output += score[run]+" ";
			}
			output += "\n";
			order++;
			
		}		
		JOptionPane.showMessageDialog(null, output);//show the result			
		
	}

}
