package a2.s100502503;

import javax.swing.JOptionPane;
import java.lang.Math;

public class A22 {
	public static void main(String[] args)
	{
		//Show out the input dialog.
		String radiusString = JOptionPane.showInputDialog("Please enter the radius of the circle: ");
		double radius = Double.parseDouble(radiusString);
		String xLocationString = JOptionPane.showInputDialog("Please enter the two-dimensional coordinate of X : ");
		double xLocation = Double.parseDouble(xLocationString);
		String yLocationString = JOptionPane.showInputDialog("Please enter the two-dimensional coordinate of Y : ");
		double yLocation = Double.parseDouble(yLocationString);
		
		double data = distance(xLocation, yLocation);
		
		//determine wheather the point in the circle or not.
		if(data < radius)
			JOptionPane.showMessageDialog(null, "The point is in the circle.");
		else if(data == radius)
			JOptionPane.showMessageDialog(null, "The point is on the circle.");
		else
			JOptionPane.showMessageDialog(null, "The point is out of the circle.");
	}
	
	//Assume the center of the circle is (0,0), use pythagorean theorem to calculate the distance.
	public static double distance(double x, double y)
	{
		double distance = Math.sqrt((x*x)+(y*y));
		return distance;
	}
}
