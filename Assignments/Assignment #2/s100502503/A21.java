/*
 * Let user input ten integers, then sort then by bubble sort.
 */

package a2.s100502503;

import javax.swing.JOptionPane;

public class A21 {
	public static boolean flag = true;
	
	public static void main(String[] args)
	{
		int[] bubbleSort = new  int[10];
		
		for(int i = 0; i < 10; i++)//Let user input ten integers.
		{
			String bubbleSortInputString = JOptionPane.showInputDialog("Please enter 10 integer: ");
			int bubbleSortInput = Integer.parseInt(bubbleSortInputString);
			bubbleSort[i] = bubbleSortInput;
		}
		for(int j = 0; j < 9; j++)//The most terms of checking is 9 times, and set a flag so when the array did'nt swap, stop the loop. 
		{
				System.out.println("This is " + (j+1) + " term.");
				
				flag = false;

				show(bubbleSort);
			for(int k = 0; k < 9; k++)//Check the number one by one in the array, if the last one is smaller, swap them.
			{
				if(bubbleSort[k] > bubbleSort[k+1])
				{
					swap(k, k+1, bubbleSort);
					flag = true;
				}
			}
			if(flag == true)
			{
				System.out.print(" => ");
				show(bubbleSort);
				System.out.print("\n");
			}
			
			else
			{
				System.out.print("\nThe ansser is: ");
				show(bubbleSort);
				break;
			}
		}
	}
	
	public static void swap(int a, int b, int[] array)//This is the method to swap the members in the array.
	{
		int number = array[a];
		array[a] = array[b];
		array[b] = number;
	}

	public static void show(int[] array)//This method can show out the array.
	{
		for(int th = 0; th < 10; th++)
		{
			System.out.print(" " + array[th] + " ");
		}
	}
}
