package a2.s100502022;
import java.util.Scanner;
import javax.swing.JOptionPane;
public class A21 {
	public static void swap(int num1, int num2,int[] array){
		int temp = array[num1]; // declare a integer variable to store array[i] temporarily
	    array[num1] = array[num2]; // assign array[i] to array[i+1]
	    array[num2] = temp; // assign temp to array[i+1]
	}
	public static void show(int[] array, int arraySize){
		for(int i=0;i<arraySize;i++){
			System.out.print(array[i]+".");
		}
	}
	public static void main(String[] args){
		int arraySize = 10; // declare a  integer for the array size
	    int numberArray[]=new int[arraySize]; // declare the numberArray and initial it
	    Scanner input=new Scanner(System.in);
	    Boolean finishFlag = false; // a flag that represent whether we did bubble sort or not, true means we did use and false means we didn't use.
	    System.out.println("Plz enter ten numbers: ");
	    for(int k=0;k<arraySize;k++){//store the number to array
	    	numberArray[k]=input.nextInt();
	    }
	    // for loop for each phase
	    for(int i=0;i<(arraySize-1);i++) 
	    {
	        System.out.println("this is phase "+(i+1)+" now!!\n"); // show the current phase number

	        // for loop for checking every step in current phase
	        finishFlag = false; // at the begin of each phase, we reset the finishFlag to false   
	        show(numberArray, arraySize); // show the current sequence
	        System.out.print("��");//appear the access
	        for(int j=0;j<(arraySize-1);j++) 
	        {
	            
	            
	            // check out whether numberArray[j] is larger than numberArray[j+1] or not
	            if(numberArray[j] > numberArray[j+1]) 
	            {
	                swap(j, (j+1),numberArray); // if it is, call function swap to change the pointer of the two cells
	                finishFlag = true; // then set the finishFlag to true and it means we used swap in this phase
	            }
	        }
	        show(numberArray, arraySize); // show the current sequence
	        System.out.println();//change to next line
	        if(finishFlag == false) // if the finishFlag is false means we didn't use swap in this phase.
	        {
	        	System.out.println("there is no swap in this phase!! bubble sort is over!!\n");
	            break; // It means that we didn't change anything in this phase, and we break the loop, end the program
	        }
	    }
	    
	    System.out.println("the final result is : "); // show the final result
	    show(numberArray, arraySize);
	}
}
