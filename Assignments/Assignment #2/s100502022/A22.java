package a2.s100502022;
import java.util.Scanner;
import javax.swing.JOptionPane;
import java.lang.Math;
public class A22 {
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);
		String num=JOptionPane.showInputDialog(null,"Plz enter the radius: ");
		
		int radius=Integer.parseInt(num);
		String x=JOptionPane.showInputDialog(null,"Plz set the X: ");
		int X=Integer.parseInt(x);
		String y=JOptionPane.showInputDialog(null,"Plz set the Y: ");
		int Y=Integer.parseInt(y);
		double lenOFline=Math.sqrt(X*X+Y*Y);
		if(lenOFline>radius){
			JOptionPane.showMessageDialog(null,"This point is without the circle!!");
		}
		else if(lenOFline==radius){
			JOptionPane.showMessageDialog(null,"This point is on the circle!!");
		}
		else JOptionPane.showMessageDialog(null,"This point is within the circle!!");
	}
}
