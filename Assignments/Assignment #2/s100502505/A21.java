package a2.s100502505;

import javax.swing.JOptionPane;

public class A21 {
	public static void swap(int a,int b,int[] array)//泡泡轉換法
	{
		int hold = array[b];
		array[b] = array[a];
		array[a] = hold;
	}
	
	public static void main(String[] args)
	{
		int[] Bubble = new int[10];//宣告一個陣列
		
		for(int i=0;i<10;i++)//將每個值輸入進去
		{
			String input = JOptionPane.showInputDialog(null,"總共要輸入10個數字,請輸入第" + (i+1) + "個:");
			Bubble[i] = Integer.parseInt(input);
		}
		
		boolean exitFlag = true;
		String number = "";
		while(exitFlag)
		{
			exitFlag = false;
			for(int i=0;i<9;i++)
			{
				if(Bubble[i] > Bubble[i+1])//如果是前一個比後一個大,就進行泡泡轉換
				{
					swap(i,i+1,Bubble);
					exitFlag = true;
				}
			}
			
			for(int i=0;i<10;i++)//讓數字好看
			{
				number = number + Bubble[i];
				if(i<9)
				{
					number = number + "."; 
				}
			}
	
			if(exitFlag == true)//秀出泡泡轉換的過程
			{
				JOptionPane.showMessageDialog(null,"(" + number + ")");
				number = "";
			}else
			{
				JOptionPane.showMessageDialog(null, "大功告成!!");
			}
		}
	}
	
}//完成!
	


