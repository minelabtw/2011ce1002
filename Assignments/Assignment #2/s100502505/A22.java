package a2.s100502505;

import javax.swing.JOptionPane;
import java.lang.Math;

public class A22 {
	
	public static void main(String arg[])
	{
		int r,x,y;
		
		String inputr = JOptionPane.showInputDialog(null,"請輸入半徑長:");//輸入半徑
		r = Integer.parseInt(inputr);
		String inputx = JOptionPane.showInputDialog(null,"請輸入x的位置:");//輸入x座標
		x = Integer.parseInt(inputx);
		String inputy = JOptionPane.showInputDialog(null,"請輸入y的位置:");//輸入y座標
		y = Integer.parseInt(inputy);
		
		if(Math.sqrt(x*x + y*y) > r)//(x,y)的距離大於半徑 就是在圓外
		{
			JOptionPane.showMessageDialog(null,"這個座標在圓外!");
		}else if(Math.sqrt(x*x + y*y) == r)//(x,y)的距離等於半徑 就是在圓上
		{
			JOptionPane.showMessageDialog(null,"這個座標在圓上!");
		}else if(Math.sqrt(x*x + y*y) < r)//(x,y)的距離小於半徑 就是在園內
		{
			JOptionPane.showMessageDialog(null,"這個座標在圓內!");
		}
		
	}
}//完成!!
