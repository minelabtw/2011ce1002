package a2.s100502001;
import javax.swing.JOptionPane;
public class A21 {
	public static void main(String[] args){
		int[] num=new int[10];//declare an array to store the numbers
		for(int i=0;i<10;i++){//input ten times
			String input=JOptionPane.showInputDialog(null,"Number"+(i+1),"BUBBLE_SORT!!!",JOptionPane.QUESTION_MESSAGE);//show the window to ask user input the intger
			num[i]=Integer.parseInt(input);//change the type of string to integer
		}
		boolean judge=true;
		while(judge){ 
			for(int i=0;i<num.length-1;i++){ //check every element of array whether the order is from the smallest to the largest
				if(num[i]>num[i+1]){ //if it is not
					swap(i,(i+1),num);//then change the order of element
					judge=false;//assign the judge "false"
				}
				
			}
			display(num);//display the result of the element after checking the array from the smallest to the largest
			System.out.println("\n\t��");//nextline and show the arrow
			if(!judge)//if there is a swap
				judge=true;//assign the judge "true" and continue to the loop
			else{ //if there is not a swap
				judge=false;//assign the judge "false" 
				System.out.print("\tTHE END ^^!!");
			}
		}
		
	}
	public static void swap(int index1,int index2, int[] array){ //swap the order of two elements
		int temp;
		temp=array[index1];
		array[index1]=array[index2];
		array[index2]=temp;
	}
	public static void display(int[] array){ //show the consequence of the array
		for(int i=0;i<array.length;i++)
			System.out.print(array[i]+" ");
	}
	

}
