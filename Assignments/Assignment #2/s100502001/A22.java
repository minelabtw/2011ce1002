package a2.s100502001;
import javax.swing.JOptionPane;
import java.lang.Math;
public class A22 {
	public static void main(String[] args){
		String R=JOptionPane.showInputDialog(null,"Enter a number!!","RADIUS^.<!!",JOptionPane.QUESTION_MESSAGE);//show the window to ask user input the length of radius
		double radius=Double.parseDouble(R);//change the type of string to double
		double[][] coordinate=new double[1][2];//declare a two-dimensional array to store the coordinate
		String X=JOptionPane.showInputDialog(null,"x","Coordinate_X ^.<!!",JOptionPane.QUESTION_MESSAGE);//show the window to ask user input the x
		coordinate[0][0]=Double.parseDouble(X);//change the type of string to double
		String Y=JOptionPane.showInputDialog(null,"y","Coordinate_Y ^.<!!",JOptionPane.QUESTION_MESSAGE);//show the window to ask user input the y
		coordinate[0][1]=Double.parseDouble(Y);//change the type of string to double
		if(InCircle(radius,coordinate[0][0],coordinate[0][1])==1)//the point is inside the circle
			JOptionPane.showMessageDialog(null,"( "+coordinate[0][0]+" , "+coordinate[0][1]+" )"+" is inside the circle of radius."+radius,"^.<",JOptionPane.INFORMATION_MESSAGE);
		else if(InCircle(radius,coordinate[0][0],coordinate[0][1])==2)//the point is on the circle
			JOptionPane.showMessageDialog(null,"( "+coordinate[0][0]+" , "+coordinate[0][1]+" )"+" is on the circle.",">.<",JOptionPane.INFORMATION_MESSAGE);
		else//the point is outside the circle
			JOptionPane.showMessageDialog(null,"( "+coordinate[0][0]+" , "+coordinate[0][1]+" )"+" is outside the circle.","= =",JOptionPane.INFORMATION_MESSAGE);
	}
	public static int InCircle(double r,double x,double y){ //judge the point's situation
		double R=Math.sqrt(x*x+y*y);
		if(R<r)//if the point is inside the circle
			return 1;
		else if(R==r)//if the point is on the circle
			return 2;
		else//if the point is outside the circle
			return 3;
	}

}
