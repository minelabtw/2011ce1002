package a2.s100502007;
import javax.swing.JOptionPane;
import java.util.Scanner;

public class A21 {
	public static void main(String[] args){
		
		int[] integer = new int [10];//name array as integer
		for(int i=1 ;i <= 10 ;i++){
			String Integers = JOptionPane.showInputDialog("Enter 10 of "+i+" integers: ");//enter ten integer
			integer[i-1] = Integer.parseInt(Integers);//change string to integer
		}
		for(int i=1 ;i <= 10 ;i++){
			for(int j=1;j<=9;j++){
				if(integer[j-1]>integer[j]){
					swap(j-1,j,integer);//sort array
				}
				if(j==9){
					String output = " ";
					for(int k=1;k<=10;k++){
						 output = output + integer[k-1];//將十個數字排出來
					}
					JOptionPane.showMessageDialog(null,output);//顯示每一次排後的結果
				}
			}
			if(integer[0]<=integer[1]&&integer[1]<=integer[2]&&integer[2]<=integer[3]&&integer[3]<=integer[4]&&integer[4]<=integer[5]&&integer[5]<=integer[6]&&integer[6]<=integer[7]&&integer[7]<=integer[8]&&integer[8]<=integer[9]){//判斷啥時停止
				JOptionPane.showMessageDialog(null,"Finish Bubble Sort.");
				break;
			}
		}
			
	}
	public static void swap(int a,int b,int[] array){//the method of sort
		int temp;
			temp=array[a];
			array[a]=array[b];
			array[b]=temp;
	}
}

