package a2.s100502033;
import java.lang.Math;
import javax.swing.JOptionPane;
public class A22 
{
	public static double number(double x , double y)
	{
		double r2 = Math.sqrt( x*x + y*y); //回的方程計算R
		return r2;
	}
	public static void main(String[] args)
	{
		for(int i = 0 ; i < 3 ; i--)
		{
			String r = JOptionPane.showInputDialog("請輸入正數R半徑");
			if ( r == null) //按取消的情況
			{
				JOptionPane.showMessageDialog(null, "Thank you\nyou play");
				break;
			}
			double r1 = Double.parseDouble(r); //將STRING轉變成DOUBLE
			if (r1 <= 0)//不可以有負數
			{
				JOptionPane.showMessageDialog(null,"輸入錯誤，請重新輸入");
				continue; 
			}
			String y = JOptionPane.showInputDialog("請輸入Y的坐標");
			if ( y == null)//按取消的情況
			{
				JOptionPane.showMessageDialog(null, "Thank you\nyou play");
				break;
			}
			String x = JOptionPane.showInputDialog("請輸入X的坐標");
			if ( x == null)//按取消的情況
			{
				JOptionPane.showMessageDialog(null, "Thank you\nyou play");
				break;
			}
			double x1 = Double.parseDouble(x);//將STRING轉變成DOUBLE
			double y1 = Double.parseDouble(y);//將STRING轉變成DOUBLE
			if(number(x1,y1) < r1)
			{
				JOptionPane.showMessageDialog(null, "此坐標在圓內");
			}
			else if(number(x1,y1) > r1)
			{
				JOptionPane.showMessageDialog(null, "此坐標在圓外");
			}
			else 
			{
				JOptionPane.showMessageDialog(null, "此坐標在圓上");
			}
		}
	}
}
