package a2.s100502002;
import javax.swing.JOptionPane;
public class A21 {
	public static void main(String[]args)
	{
		boolean finishFlag = false;
		int numbers[]=new int[10];
		for(int i=0;i<10;i++)//用gui輸入10次並且存入陣列
		{
			
			String n = JOptionPane.showInputDialog(null, "please enter "+(i+1)+"'s number:");
			int number=Integer.parseInt(n);
			numbers[i]=number;
		}
		System.out.print("the begining numbers oder is:");
		for(int k=0;k<10;k++)
		{
			System.out.print(numbers[k]+" ");
		}//印出初始樣子
		System.out.print("\n");
		
		for(int j=0;j<9;j++)
		{
			finishFlag = false; 
			for(int i=0;i<9;i++)
			{
				
				if(numbers[i]>numbers[i+1])
				{
					swap(i,(i+1),numbers);
					finishFlag = true;
				}//判斷是否使用method
			}
			System.out.print("the "+(j+1)+"'s time result is:");
			for(int k=0;k<10;k++)//印出結果
			{
				System.out.print(numbers[k]+" ");
			}
			System.out.print("\n");
			if(finishFlag == false) //判斷是否跳出迴圈
				break;
		}
		
	}
	public static int[] swap(int a,int b,int[] array)//交換陣列內容
	{
		int temp;
		temp=array[a];
		array[a]=array[b];
		array[b]=temp;
		return array;
		
	}

}
