package a2.s100502002;
import javax.swing.JOptionPane;
import java.lang.Math;//引入函式庫(但不引入也可以跑欸...
public class A22 {
	public static void main(String[] args)
	{
		String data = JOptionPane.showInputDialog(null, "please enter the radius of circle:");//輸入字串
		double radius=Double.parseDouble(data);//把字串轉乘DOUBLE
		String x = JOptionPane.showInputDialog(null, "please enter the coordinate (x,y) 'x:");//輸出訊息
		String y = JOptionPane.showInputDialog(null, "please enter the coordinate (x,y) 'y:");
		double coordinate[][]=new double[1][2];//宣告一維陣列(大小2
		coordinate[0][0]=Double.parseDouble(x);
		coordinate[0][1]=Double.parseDouble(y);
		int afterfunction=function(coordinate,radius);
		if(afterfunction==1)
			JOptionPane.showMessageDialog(null, "the point is out of the circle");
		else if(afterfunction==2)
			JOptionPane.showMessageDialog(null, "the point is right on the circle");
		else
			JOptionPane.showMessageDialog(null, "the point is inside the circle");//判斷並輸出
		
		
	}
	public static int function(double x[][],double radius)//計算點在圓內圓外圓上的method
	{
		int last;
		double result=Math.sqrt(x[0][0]*x[0][0]+x[0][1]*x[0][1]);
		if(result>radius)
			last=1;
		else if(result==radius)
			last=2;
		else
			last=3;
			
		return last;
	}

}
