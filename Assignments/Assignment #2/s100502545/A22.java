package A2.s100502545;

import java.lang.Math;

import javax.swing.JOptionPane;

public class A22 
{
	public static void main(String[] args)
	{
		//Enter a radius
		String radiusString =JOptionPane.showInputDialog("Enter a radius:");
		
		//Will enter radius Store result
		double radius = Double.parseDouble(radiusString);
		
		//Enter x-coordinate
		String coordinateXString =JOptionPane.showInputDialog("Enter x-coordinate :");
		
		//Will enter Store coordinateX
		double coordinateX = Double.parseDouble(coordinateXString);
		
		//Enter y-coordinate
		String coordinateYString =JOptionPane.showInputDialog("Enter Y-coordinate :");
		
		//Will enter Store coordinateY 
		double coordinateY = Double.parseDouble(coordinateYString);
		
		//Calculate  two points distance formula
		double distance;
		distance = (double)(Math.sqrt(coordinateX*coordinateX+coordinateY*coordinateY));
		
		if(distance>radius)
		{
			//Display Where is the point in circle?
			JOptionPane.showMessageDialog(null,"The point is outside the circle!");
			
		}
		else if(distance==radius)
		{
			//Display Where is the point in circle?
			JOptionPane.showMessageDialog(null,"The point is at the circle!");
			
		}
		else
		{
			//Display Where is the point in circle?
			JOptionPane.showMessageDialog(null,"The point is inside the circle!");
		}
	}
}
