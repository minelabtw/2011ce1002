package A2.s100502545;

import javax.swing.JOptionPane;

public class A21 
{
	public static void main(String[] args)
	{
		String answer="";
		int count=0;
		//Declare 10 array length!
		int []a = new int[10];	
		
		
		for(int t=0;t<=9;t++)
		{
			//User Enter 10 numbers!
			String resultString =JOptionPane.showInputDialog(null,"Input Number:");
			int result=Integer.parseInt(resultString);
			a[t]=result;	
		}
		
		//
		for(int p=0;p<=9;p++)
		{
			count=0;
			
			//判斷若前項大於後項
			for(int o=0;o<=8;o++)
			{			
				
				if(a[o]>a[o+1])					
				{
					Swap(o,o+1,a);
					count++;
				}	
			}
			
			if(count==0)
			{
				break;
			}
				
			for(int h=0;h<=9;h++)
			{
				answer+=a[h];				
				
			}
			
			answer+="\n";
		}
		
		//output result
		JOptionPane.showMessageDialog(null,answer);
	}
	
	public static void Swap(int a,int b,int[] n)
	{
		//前項大於後項 就交換
		int tmp = n[b];
		n[b] = n[a];
		n[a] = tmp;
	}
}
