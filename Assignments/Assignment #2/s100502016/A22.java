package a2.s100502016;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.awt.Label;
import java.lang.Math;

public class A22 {
	public static void main(String[] args) {

		JFrame form = new JFrame("Judgement");
		form.setSize(200, 150);
		form.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		String radius;
		String coordinateX;
		String coordinateY;

		radius = JOptionPane.showInputDialog("請輸入半徑(圓心位置(0,0))");
		coordinateX = JOptionPane.showInputDialog("請輸入X座標");
		coordinateY = JOptionPane.showInputDialog("請輸入Y座標");

		double r = Double.parseDouble(radius);
		double x = Double.parseDouble(coordinateX);
		double y = Double.parseDouble(coordinateY);
		if (Math.sqrt(x * x + y * y) <= r * r) {
			form.add(new Label("The coordinate is within the circle"));
		} else {
			form.add(new Label("The coordinate is out of the circle"));
		}
		form.setVisible(true);
	}
}
