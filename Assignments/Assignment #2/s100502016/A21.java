package a2.s100502016;

import java.awt.*;
import javax.swing.*;

public class A21 {
	public static void main(String[] args) {
		JFrame form = new JFrame("Bubble Sort");
		form.setSize(300, 100);
		form.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JLabel displayPhase = new JLabel("label");

		displayPhase.setLocation(0, 10);

		int size = 10;
		String[] numberString = new String[size];
		String output = null;
		int[] number = new int[size];

		for (int i = 0; i < size; i++) {
			numberString[i] = JOptionPane.showInputDialog("��J��" + (i + 1)
					+ "�Ӽ�");
			number[i] = Integer.parseInt(numberString[i]);
		}

		for (int i = 1; i < size - 1; i++) {
			for (int j = 0; j < size - 1; j++) {
				if (number[j] > number[j + 1]) {
					swap(j, j + 1, number);
				}
			}
			for (int k = 0; k < size; k++) {
				output += String.valueOf(number[k]) + " ";
			}
			System.out.println(output);
			displayPhase.setText(output);
			JOptionPane.showMessageDialog(null, "phase " + i + ": " + output);
			form.add(new Label("The result is : " + output));
			output = "";
		}
		form.setVisible(true);
	}

	public static void swap(int a, int b, int array[]) {
		int temp = array[a];
		array[a] = array[b];
		array[b] = temp;

	}
}
