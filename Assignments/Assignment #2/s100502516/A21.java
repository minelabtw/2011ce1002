package a2.s100502516;

import javax.swing.JOptionPane;

public class A21 {
	public static void main(String[] args)
	{		
		final int size = 10;//array size
		boolean stop = false;//stop of sort
		String output = "";//store the sort
		int pass = 1;//times of sort
	
		int[] nums = new int[size];//store the input numbers		
		
		for(int i = 0; i < size; i++)
		{
			String input = JOptionPane.showInputDialog(null, "Enter numder" + (i+1), 
					"Enter ten numbers", JOptionPane.QUESTION_MESSAGE);
			nums[i] = Integer.parseInt(input);
		}
		
		while(!stop)
		{
			stop = true;
			
			for(int k = 0; k < size-1; k++)
			{
				if(k == 0)//store the numbers before sort
				{
					for(int j = 0; j < size; j++)
					{
						if(j == 0)
							output += pass++ + " PASS\n{ ";
						
						output += nums[j] + " ";
						
						if(j == size-1)
							output += "} ---> ";
							
					}
				}
				
				if(nums[k] > nums[k+1])//swap
				{
					Swap(k, k+1, nums);					
					stop = false;
				}
				
				if(k == size - 2)//store the numbers after sort
				{
					for(int h = 0; h < size; h++)
					{
						if(h == 0)
							output += "{ ";
						
						output += nums[h] + " ";
						
						if(h == size-1)
							output += "}\n";
							
					}
				}
			}
		}
		
		JOptionPane.showMessageDialog(null, output);//out put the result
	}
	
	public static void Swap(int a, int b, int[] array)//swap the numbers
	{
		int num = array[a];	
		
		array[a] = array[b];		
		array[b] = num;		
	}
}
