package a2.s100502516;

import javax.swing.JOptionPane;
import java.lang.Math;

public class A22 {
	public static void main(String[] args)
	{
		int radius;
		double x;
		double y;
		
		String inputRadius = JOptionPane.showInputDialog(null, "Enter radius");
		radius = Integer.parseInt(inputRadius);
		
		while(radius <= 0)//if radius <= 0, it isn't a circle
		{
			JOptionPane.showMessageDialog(null, "Radius can't smaller than 0 or equal to 0");
			
			String reInputRadius = JOptionPane.showInputDialog(null, "Enter radius");
			radius = Integer.parseInt(reInputRadius);
		}
		
		String inputx = JOptionPane.showInputDialog(null, "Enter x");
		x = Double.parseDouble(inputx);
		
		String inputy = JOptionPane.showInputDialog(null, "Enter y");
		y = Double.parseDouble(inputy);
		
		if(Math.sqrt(x*x + y*y) == radius)//to determine where is the coordinate
			JOptionPane.showMessageDialog(null, "coordinate is on the circle");
		else if(Math.sqrt(x*x + y*y) < radius)
			JOptionPane.showMessageDialog(null, "coordinate is in the circle");
		else
			JOptionPane.showMessageDialog(null, "coordinate is out the circle");
	}
}
