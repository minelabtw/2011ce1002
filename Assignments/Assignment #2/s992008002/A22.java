
package a2.s992008002;

import javax.swing.JOptionPane;//引入GUI元件與MATH方法
import java.lang.Math;

public class A22 {

	

	
	public static void main(String args[]){
	

		
	//使用GUI介面讓使用者輸入半徑與XY座標	
		
	String radius =JOptionPane.showInputDialog("Please keyin a integer as radius\n");	
	int R = Integer.parseInt(radius);
	
	String X = JOptionPane.showInputDialog("Please keyin corrdinate X:\n");
	int x = Integer.parseInt(X);
	
	String Y = JOptionPane.showInputDialog("Please keyin corrdinate Y:\n");
	int y = Integer.parseInt(Y);

    
	//計算座標到原點的距離
	double Taroka = Math.sqrt(x*x + y*y);
	
	//與半徑比較決定在圓的內外或圓上
    if(Taroka < R)
	JOptionPane.showMessageDialog(null,"The coordinate is WITHIN the circle","Result",JOptionPane.PLAIN_MESSAGE);
    else if(Taroka == R)
    	JOptionPane.showMessageDialog(null,"The coordinate is ON the circle","Result",JOptionPane.PLAIN_MESSAGE);
    else if(Taroka > R)
    	JOptionPane.showMessageDialog(null,"The coordinate is NOT within the circle","Result",JOptionPane.PLAIN_MESSAGE);
    
	}
		
	
	
}
