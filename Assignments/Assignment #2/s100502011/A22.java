package a2.s100502011;

import java.lang.Math;
import javax.swing.JOptionPane;
public class A22 {
	public static void main(String[] args){
		String change=JOptionPane.showInputDialog(null,"Enter a integer for radius","circle",JOptionPane.QUESTION_MESSAGE);
		int radius=Integer.parseInt(change); //radius
		String dimension_x=JOptionPane.showInputDialog(null,"Then, input two-dimentional of X","circle",JOptionPane.QUESTION_MESSAGE);
		String dimension_y=JOptionPane.showInputDialog(null,"Then, input two-dimentional of Y","circle",JOptionPane.QUESTION_MESSAGE);
		double dim_X=Double.parseDouble(dimension_x);//dimention of x
		double dim_Y=Double.parseDouble(dimension_y);//dimention of y
		
		double distance=Math.sqrt(dim_X*dim_X+dim_Y*dim_Y); 
		if(distance<radius){ //inside of circle
			JOptionPane.showMessageDialog(null,"This point is inside of the circle");
		}
		else if(distance>radius){ // outside of circle
			JOptionPane.showMessageDialog(null,"This point is outside of the circle");
		}
		else // on circle
			JOptionPane.showMessageDialog(null,"This point is on the circle");
	}
}