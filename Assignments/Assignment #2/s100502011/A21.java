package a2.s100502011;

import javax.swing.JOptionPane;

public class A21 {		

	public static void main(String[] arg){
		String output=""; //to store number for string to output
		int counttime=0;//check continue or not
		int[] array = new int[10];//store number
		for(int a=1;a<=10;a++){ //store number to array
			String nums =JOptionPane.showInputDialog(null, "Enter "+a +" number","Bubble Sort",JOptionPane.QUESTION_MESSAGE);
			int num =Integer.parseInt(nums);
			array[a-1]=num;
		}
		for(int a=1;a<=10;a++){ // bubble sort
			counttime=0;
			for(int b=1;b<10;b++){							
				if(array[b-1]>array[b]){//swap
					Swap(b-1,b,array);
					counttime++;
				}
			}			
			if(counttime==0){ //if don't change it will break
				break;
			}
			for(int c=1;c<=10;c++){//store number to string
				output+=array[c-1];
				output+=" ";
			}
			output+="\n";	// change line
		}
		JOptionPane.showMessageDialog(null,output); // output the answer
	}
	public static void Swap(int a,int b,int[] array){ // change numbers
		int trans;
		trans=array[a];
		array[a]=array[b];
		array[b]=trans;
	}
}