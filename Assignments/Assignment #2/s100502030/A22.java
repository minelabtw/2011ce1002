package a2.s100502030;

import javax.swing.JOptionPane;

public class A22 {
	public static String getResult(double x,double y,int r){
		if(Math.sqrt(x * x + y * y)>r)
			return "The coordinate is not within the circle";
		else if(Math.sqrt(x * x + y * y)<r)
			return "The coordinate is within the circle";
		else
			return "The coordinate is on the circle";
	}// 得到座標到原點之距離和半徑得比較

	public static void main(String[] args) {
		String radiusStr = JOptionPane
				.showInputDialog("Please input integer as radius.");
		int radius = Integer.parseInt(radiusStr);//輸入半徑
		String xStr = JOptionPane.showInputDialog("Please input x-coordinate.");
		double xc = Double.parseDouble(xStr);//輸入x-coordinate
		String yStr = JOptionPane.showInputDialog("Please input y-coordinate.");
		double yc = Double.parseDouble(yStr);//輸入x-coordinate
		JOptionPane.showMessageDialog(null,getResult(xc,yc,radius));//顯示結果
	}
}
