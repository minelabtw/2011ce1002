package a2.s100502030;

import javax.swing.JOptionPane;

public class A21 {
	public static void swap(int element1, int element2, int[] array) {
		int shortStore = array[element1];
		array[element1] = array[element2];
		array[element2] = shortStore;
	}//將陣列中兩元素交換

	public static void main(String[] main) {
		final int arraysize = 10;
		int counter = 0;
		String total = "";

		JOptionPane.showMessageDialog(null, "Please input 10 integer numbers");//告知使用者要輸樹的東西'
		String[] numberStr = new String[arraysize];
		int[] numbers = new int[arraysize];
		for (int i = 0; i < 10; i++) {
			numberStr[i] = JOptionPane.showInputDialog("number " + (i + 1) + ":");//輸入數字
			numbers[i] = Integer.parseInt(numberStr[i]);
		}
		boolean doStop = false;//判定是否停止
		while (!doStop) {
			doStop = true;
			String phase = ("phase " + (counter + 1) + ":\n");//儲存要顯示的資料
			for (int i = 0; i < arraysize; i++) {
				phase += numbers[i] + " ";
			}// 儲存swap前的陣列情形
			phase += "==> ";
			for (int i = 0; i < (arraysize - 1); i++) {
				if (numbers[i] > numbers[i + 1]) {
					swap(i, i + 1, numbers);
					doStop = false;
				}
			}
			for (int i = 0; i < arraysize; i++) {
				phase += numbers[i] + " ";// 儲存swap後的陣列情形
			}
			total += phase + "\n";//將每一次的資料儲存
			counter++;
		}
		JOptionPane.showMessageDialog(null, total);//顯示結果
	}
}
