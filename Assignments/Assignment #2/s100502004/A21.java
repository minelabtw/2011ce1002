package s100502004;
import javax.swing.JOptionPane;

public class A21 {
	public static void Swap(int a,int b,int [] array){//swap method,use to change the order
		int value=array[a];
		array[a]=array[b];
		array[b]=value;
		
	}
	
	public static void main(String[] args){
		int [] number=new int [10];//create an array
		String finOutput="";//use to store the final answer
		for(int i=0;i<10;i++){
			String newinput=JOptionPane.showInputDialog("請輸入第"+(i+1)+"個數");//create a dialog box
			int output =Integer.parseInt(newinput);//use the dialog box to input numbers
			number[i]=output;//store the number
		}
		
		for(int tern=0;tern<10;tern++){
			int check=0;//use to check if it should run again
			
			for(int i=1;i<10;i++){
				if(number[i-1]>number[i]){
					Swap(i-1,i,number);	
					check=1;
				}
				
			}
			
			if(check==0){//break the for loop
				break;
			}
			
			for (int j=0;j<10;j++)
			{
				if(check==0){
					break;
				}
				finOutput+=number[j]+" ";//store the final answer
				//finOutput+=finOutput+" ";
				
			}	
			
			
			finOutput+="\n";//wrap the line
			
		}
		
		
		JOptionPane.showMessageDialog(null,finOutput);//print out the final answer	
		
	}
	
	
	
	
	
}
