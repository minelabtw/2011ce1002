package a2.s995002204;

import javax.swing.JOptionPane;		//
import java.lang.Math;

public class A22 {
	public static void main(String args[])
	{
		String pi = JOptionPane.showInputDialog("請輸入圓的半徑:");	//宣告變數pi
		String x = JOptionPane.showInputDialog("請輸入x座標:");		//宣告變數x
		String y = JOptionPane.showInputDialog("請輸入y座標:");		//宣告變數y
		
		int r = Integer.parseInt(pi);
		int x1 = Integer.parseInt(x);
		int y1 = Integer.parseInt(y);
		
		if (x1*x1+y1*y1>r*r)		//座標位置判斷
		{
			System.out.println("此點在圓外");
		}
		else if(x1*x1+y1*y1==r*r)
		{
			System.out.println("此點在圓上");
		}
		else
		{
			System.out.println("此點在圓內");
		}
	}
}
