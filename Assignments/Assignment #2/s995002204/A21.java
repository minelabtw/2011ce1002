package a2.s995002204;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class A21 {
	public static void main(String args[])
	{
		int onearray[];		//宣告陣列 儲存輸入數字
		onearray = new int[10];
		boolean flag = true; 	//判斷執行
		
		String z1 = JOptionPane.showInputDialog("請輸入第一個數字:");		//跳出視窗輸入數字
		String z2 = JOptionPane.showInputDialog("請輸入第二個數字:");
		String z3 = JOptionPane.showInputDialog("請輸入第三個數字:");
		String z4 = JOptionPane.showInputDialog("請輸入第四個數字:");
		String z5 = JOptionPane.showInputDialog("請輸入第五個數字:");
		String z6 = JOptionPane.showInputDialog("請輸入第六個數字:");
		String z7 = JOptionPane.showInputDialog("請輸入第七個數字:");
		String z8 = JOptionPane.showInputDialog("請輸入第八個數字:");
		String z9 = JOptionPane.showInputDialog("請輸入第九個數字:");
		String z10 = JOptionPane.showInputDialog("請輸入第十個數字:");
		
		onearray[0] = Integer.parseInt(z1);		//將輸入轉成整數型態儲存
		onearray[1] = Integer.parseInt(z2);
		onearray[2] = Integer.parseInt(z3);
		onearray[3] = Integer.parseInt(z4);
		onearray[4] = Integer.parseInt(z5);
		onearray[5] = Integer.parseInt(z6);
		onearray[6] = Integer.parseInt(z7);
		onearray[7] = Integer.parseInt(z8);
		onearray[8] = Integer.parseInt(z9);
		onearray[9] = Integer.parseInt(z10);
		
		
		
		for(int i=0;i<onearray.length&& flag;i++)		//主要計算執行次數
		{
			flag = false;								
			System.out.println("第"+(i+1)+"次排序");
			for(int k=0;k<onearray.length;k++)
				System.out.printf("%d  ",onearray[k]);
			System.out.println();
			for(int j=0;j<onearray.length-1;j++)
			{
				 if ( onearray[j] > onearray[j+1] ) 
				 {
					 Swap(j , j + 1 , onearray);
					 flag = true; 
				 }
			}
		}
	}
	public static void Swap(int a, int b, int[] onearray)		//數值交換
	{
		int temp;
		temp = onearray[b];
		onearray[b] = onearray[a];
		onearray[a] = temp;
	}
}
