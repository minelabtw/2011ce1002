package a2.s100502028;
import javax.swing.JOptionPane;

public class A21 {
	// Method to swap two value in the array
	public static void swap(int a, int b, int[] array){
		int hold = array[a];
		array[a] = array[b];
	    array[b] = hold;
	}
	
	// Method to show the ordinal and suffix
	public static String ordinal(int i){
		if(i == 1)
			return ("1st");
		else if(i == 2)	
			return ("2nd");
		else if(i == 3)
			return ("3rd");
		else 
			return (i + "th");
	}
	
	// Main method
	public static void main(String[] args){
		final int arraySize = 10;
		int[] arrayOfNumber = new int[arraySize]; // Declare a integer array
		String[] arrayOfString = new String[arraySize]; // Declare a string array
		String userInputOfBefore = "";
		String userInputOfAfter = "";
		JOptionPane.showMessageDialog(null,"You are going to enter 10 integer numbers.");
		
		/** For loop store user's inputs in the string array 
		 * and  convert string array to integer array by using GUI
		 * then show the user's inputs before bubble sort */
		for(int i=0; i<10; i++){
		arrayOfString[i] = JOptionPane.showInputDialog("Enter " + ordinal(i+1) + " integer number: ");
		arrayOfNumber[i] = Integer.parseInt(arrayOfString[i]);
		userInputOfBefore += arrayOfNumber[i] + " ";
		}
		
		int pass = 1;
		String output = "";
	    int smaller; // Index of smaller element
	    boolean x = true; // Initialize x to be boolean data type

	    // While loop to sort the array continuously
	    while (x)
	    {
	        x = false; // Set the x to be false and stop when while loop is finished
	        output += "Pass" + pass + "\n";
	        for(int before=0; before<arraySize; before++) // The former for loop to print the array before sorting
	        {
	        	output += arrayOfNumber[before] + " ";
	        } //end the former for loop
	        output += " ==> ";
	        for(int i=0; i<arraySize-1; i++){
	        	smaller = i; // First index of remaining array
	        	int j=i+1;
	        	if (arrayOfNumber[j] < arrayOfNumber[smaller]){ // If statement when array is still need to be sort
	            		smaller = j;
	            		x = true; // Set the x to be true and run the while loop again
	            		swap(i,smaller,arrayOfNumber); // Call the function swap to exchange the values
	            	 }// End if statement
	            } // End for repetition
	        for(int after=0; after<arraySize; after++) // The latter for loop to print the array after sorting
    		{
    			output += arrayOfNumber[after] + " ";
    		} // End the latter for loop
	        output += "\n\n";
	        pass = pass + 1;
	    } // End while loop
	    for(int i=0; i<10; i++){ // For loop to show the final result after bubble sort
	    	userInputOfAfter += arrayOfNumber[i] + " ";
	    }
	    JOptionPane.showMessageDialog(null,"Your input is: " + userInputOfBefore + "\n\n" + output 
	    		+ "The result of bubble sort is: " + userInputOfAfter);
	} // End main method 
} //End class
