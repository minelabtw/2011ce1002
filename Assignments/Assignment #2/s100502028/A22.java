package a2.s100502028;
import javax.swing.JOptionPane;

public class A22 {
	// Main method
	public static void main(String[] args){
		// Prompt the user for radius by using GUI then convert string to integer
		String intString = JOptionPane.showInputDialog("Enter a integer as radius: ");
		int intValue = Integer.parseInt(intString);
		
		// Prompt the user for X-coordinate by using GUI then convert string to integer
		String xCoordinate = JOptionPane.showInputDialog("Enter the X-coordinate: ");
		int intXCoordinate = Integer.parseInt(xCoordinate);
		
		// Prompt the user for Y-coordinate by using GUI then convert string to integer
		String yCoordinate = JOptionPane.showInputDialog("Enter the Y-coordinate: ");
		int intYCoordinate = Integer.parseInt(yCoordinate);
		
		System.out.println("You input radius: " + intValue);
		check(intValue,intXCoordinate,intYCoordinate); // Call method check
	} // End main
	
	// Method to check whether the point is inside, outside or on the circle 
	public static void check(int r,int x, int y){
		if(r > Math.sqrt(x * x + y * y) ){ // Case that the point is inside the circle
			JOptionPane.showMessageDialog(null,"The point (" + x + "," + y + ") you enter is inside the circle of radius " + r + " !");
		}
		else if(r < Math.sqrt(x * x + y * y) ){ // Case that the point is outside the circle
			JOptionPane.showMessageDialog(null,"The point (" + x + "," + y + ") you enter is outside the circle of radius " + r + " !");
		}
		else{ // Case that the point is on the circle
			JOptionPane.showMessageDialog(null,"The point (" + x + "," + y + ") you enter is exactly on the circle of radius " + r + " !");
		}
	} // End method check
} // End class
