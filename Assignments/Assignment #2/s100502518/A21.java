package a2.s100502518;

import javax.swing.JOptionPane;
public class A21 {
	public static void main (String[] args)
	{
		int[] store=new int[11];
		
		for(int a=1;a<=10;a++)//GUI，並把數字放到陣列內
		{
			String cin= JOptionPane.showInputDialog("請輸入第"+a+"個數字: ");
			store[a]=Integer.parseInt(cin);
		}
		
		while(true)//把每跑完一次的結果用GUI顯現
		{
			int a=1,b=1;
			String phase=Integer.toString(store[a]);
			
			for(a=2;a<=10;a++)
			{
				phase=phase+" "+Integer.toString(store[a]);
			}
			
			JOptionPane.showMessageDialog(null,phase);
			
			a=1;
			while(a<10)
			{
				if(store[a]>store[a+1])
				{
					swap(a, a+1, store);
					b++;
				}
				a++;
			}
			if(b==1)
			{
				break;
			}
		}
	}
	
	public static void swap(int a,int b, int[] array)
	{
		int temp=array[a];
		array[a]=array[b];
		array[b]=temp;
	}
}
