package a2.s100502518;

import javax.swing.JOptionPane;
import java.lang.Math;
public class A22 {
	public static void main (String[] args)
	{
		String cin= JOptionPane.showInputDialog("請輸入圓心在(0,0)之圓的半徑: ");//GUI輸入一些東西
		int r=Integer.parseInt(cin);
		
		String cinx= JOptionPane.showInputDialog("請輸入X座標: ");
		
		int x=Integer.parseInt(cinx); 
		
		String ciny= JOptionPane.showInputDialog("請輸入Y座標: ");
		
		int y=Integer.parseInt(ciny);
		double distance=(double)(x*x+y*y);
		
		if(Math.sqrt(distance)>(double)r)//把輸入後的數據做判斷
		{
			String output="點在園外";
			JOptionPane.showMessageDialog(null, output);
		}
		
		else if(Math.sqrt(distance)==(double)r)
		{
			String output="點在園上";
			JOptionPane.showMessageDialog(null, output);
		}
		
		else
		{
			String output="點在園內";
			JOptionPane.showMessageDialog(null, output);
		}
	}

}
