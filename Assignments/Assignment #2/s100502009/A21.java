package a2.s100502009;

import javax.swing.JOptionPane;

public class A21 {
	public static void main(String[] args)
	{
		
		int[] number=new int[10] ;
		for(int i=1;i<=10;i++)//store the integers into the array
		{			
			String input=JOptionPane.showInputDialog("Please enter 10 integers("+i+"): ");
			number[i-1]=Integer.parseInt(input);

		}
		String  output=" ";
		for(int j=1;j<number.length;j++)
		{
			for(int p=1;p<number.length;p++)
			{
				if(number[p-1]>number[p])//sort the numbers
				{
					swap(p-1,p,number);		
				}				
			}
			for(int k=0;k<number.length;k++)//output the sorted numbers
			{
				output=output+number[k];
			}
			JOptionPane.showMessageDialog(null,output);
			output=output+"\n";
		}		
	}	
	public static void swap(int a,int b,int[] array)//method to sort numbers
	{
		int temp=array[a];
		array[a]=array[b];
		array[b]=temp;
	}
}

