package a2.s100502009;

import javax.swing.JOptionPane;
import java.lang.Math;

public class A22 {
	public static void main(String[] args)
	{
		int radius=0;
		for(int i=1;i>0;i++)
		{
			String radiusString=JOptionPane.showInputDialog("Please enter an integer as radius:");
			radius=Integer.parseInt(radiusString);//input the radius
			if(radius<0)
			{
				JOptionPane.showMessageDialog(null,"Please enter an integer again~");
			}
			else
				i=-1;
		}
		String xCoordinateString=JOptionPane.showInputDialog("Please enter a number as x-coordinate:");
		double  xCoordinate=Double.parseDouble( xCoordinateString);//input the x-coordinate
		String yCoordinateString=JOptionPane.showInputDialog("Please enter a number as y-coordinate:");
		double  yCoordinate=Double.parseDouble( yCoordinateString);//input the y-coordinate
		int check=sqrt(xCoordinate,yCoordinate,radius);//check the two dimensional coordinates
		if(check==1)
		{
			String output="The set is within the circle";
			JOptionPane.showMessageDialog(null,output);
		}
		else
		{
			String output="The set is outside the circle";
			JOptionPane.showMessageDialog(null,output);
		}
	}
	public static int sqrt(double x,double y,int r)//method to check whether the point is within the circle
	{
		double distance=Math.sqrt(x*x+y*y);
		if(r<distance)
			return 0;
		else
			return 1;		
	}
}
