//A21.java
package a2.s100502502;
import javax.swing.JOptionPane;
public class A21 {
	public static String result = "";
	public static void main(String[] args){
		int[] array = new int[10];//initialize array
		//save user input
		for(int i = 0; i < 10; i++){
			String showmessage = JOptionPane.showInputDialog("Enter "+ (i+1) +"'s integer:");
			//transfer user input to values
			array[i] = Integer.parseInt(showmessage);
		}
		Swap(0, 0, array);//swap array elements
		JOptionPane.showMessageDialog(null, result);
	}
	//swap array elements
	public static void Swap(int a, int b, int[] array){
		while(on(array)){
			change(array);//change array elements to string
			for(int j = 0; j < 9; j++){
				//save values
				a = array[j];
				b = array[j+1];
				if(a > b){
				//change their values
					array[j] = b;
					array[j+1] = a;
				}
				else {}//no action
			}
			result = result + " => ";
			change(array);//change array elements to string
			result = result + "\n";
		}
	}
	//control while loop run or not
	public static boolean on(int[] array){
		int counter = 0;//initialize it
		//check array elements
		for(int i = 0; i<9; i++){
			if(array[i] > array[i+1]){
				counter = counter + 1;
			}
		}
		if(counter == 0)
			return false;
		else 
			return true;
	}
	//show array elements
	public static void change(int[] array){
		for(int i = 0; i < 10; i++){
			result = result + array[i] + " ";
		}
	}
}
