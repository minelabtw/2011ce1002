//A22.java
package a2.s100502502;
import javax.swing.JOptionPane;
public class A22 {
	public static void main(String[] args){
		double [][] array = new double[1][2];
		String input = JOptionPane.showInputDialog("Enetr a integer as radius : ");
		int radius = Integer.parseInt(input);//save radius
		input = JOptionPane.showInputDialog("Enetr coordinate X : ");
		array[0][0] = Double.parseDouble(input);//save coordinate X
		input = JOptionPane.showInputDialog("Enetr coordinate Y : ");
		array[0][1] = Double.parseDouble(input);//save coordinate Y
		//show answer
		answer(array, radius);
	}
	//calculate distance and show message
	public static void answer(double array[][], int radius){
		double length = Math.sqrt(array[0][0]*array[0][0]+array[0][1]*array[0][1]);
		//whether the point within the circle
		if(length <= radius)
			JOptionPane.showMessageDialog(null, "The point is within the circle");
		else
			JOptionPane.showMessageDialog(null, "The point is not within the circle");
	}
}
