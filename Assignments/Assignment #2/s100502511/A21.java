package a2.s100502511;

import javax.swing.*;

import java.util.Scanner;

public class A21 {
	public static int[] swap(int a, int b, int[] array) { // swap method :
															// 將前面大於後面的數相互對調
		int[] result = new int[b];

		for (int i = a; i < a + 1; i++) {
			if (array[i] > array[i + 1]) {
				int temp = 0;
				temp = array[i];
				array[i] = array[i + 1];
				array[i + 1] = temp;
			} else {
				break;
			}
		}
		return result;
	}

	public static void main(String[] args) {
		String output = "";
		Scanner input = new Scanner(System.in);
		int[] inputnumber = new int[10]; // 設定一個長度為10的陣列，用來儲存數字
		System.out.println("Please input 10 numbers: ");
		for (int i = 0; i < 10; i++) { // 輸入10個數字
			inputnumber[i] = input.nextInt();
		}
		output = output + "Original: ";
		for (int i = 0; i < 10; i++) {
			output = output + inputnumber[i] + " ";
		}
		output = output + "\n";
		for (int i = 0; i < 9; i++) {
			output = output + (i + 1) + "th Pass \n";
			for (int j = 0; j < 9; j++) {
				swap(j, inputnumber.length, inputnumber); // 對調數字
				output = output + inputnumber[j] + " ";
			}
			output = output + inputnumber[9] + " ";
			output = output + "\n";
		}
		output = output + ("Final: ");
		for (int i = 0; i < 10; i++) {
			output = output + (inputnumber[i] + " ");
		}
		JOptionPane.showMessageDialog(null, output); // 將output的結果列在視窗上
	}
}