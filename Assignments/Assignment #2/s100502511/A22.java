package a2.s100502511;

import java.util.Scanner;

import java.lang.Math;

import javax.swing.*;

public class A22 {
	public static void main(String[] args) {
		String output;
		Scanner input = new Scanner(System.in);
		System.out.print("Please input radius: ");
		double radius = input.nextDouble();
		System.out.print("Please input x and y: "); // 輸入x,y座標
		double x = input.nextDouble();
		double y = input.nextDouble();
		double z = (x * x + y * y); // z為x的平方加上y的平方
		if (Math.sqrt(z) < radius) { // 距離小於半徑 則點在圓內
			output = "(" + x + ", " + y + ")\n" + "距離: " + Math.sqrt(z) + "\n"
					+ ("點在圓內!");
		} else if (Math.sqrt(z) == radius) { // 距離等於半徑 則點在圓上
			output = "(" + x + ", " + y + ")\n" + "距離: " + Math.sqrt(z) + "\n"
					+ ("點在圓上!");
		} else { // 距離大於半徑 則點在圓外
			output = "(" + x + ", " + y + ")\n" + "距離: " + Math.sqrt(z) + "\n"
					+ ("點在圓外!");
		}
		JOptionPane.showMessageDialog(null, output); // 將output的結果列在視窗上
	}
}
