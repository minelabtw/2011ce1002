package a2.s100502521;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.lang.Math;
import javax.swing.JOptionPane;
import java.awt.image.BufferedImage;
public class A22_Frame extends JFrame implements ActionListener//類別繼承類別，類別實做介面
{
	private static final long serialVersionUID = 1L;//eclipse加的
	private JButton[] button;//按鈕
	private JTextField[] textField;//textbox
	private static JLabel[] label;//label標題
	private JLabel labelImage;//label標題    用來存圖片 當畫布
	public A22_Frame() //初始化各個物件
	{
		super("判斷座標是否在圓內");
		setLayout(new FlowLayout(FlowLayout.LEFT));
		textField=new JTextField[3];
		for(int i=0;i<3;i++)
		{
			textField[i] = new JTextField(3);
		}		
		button =new JButton[2];
		button[0] = new JButton("確定");
		button[1] = new JButton("清空");
		button[0].addActionListener(this);// 加入按鈕
		button[1].addActionListener(this);
		label = new JLabel[4];
		label[0]= new JLabel("請輸入圓的半徑:");
		label[1]= new JLabel("請輸入點的X座標");
		label[2]= new JLabel("請輸入點的Y座標");
		label[3]= new JLabel("");
		add(label[0]);
		add(textField[0]);
		add(label[1]);
		add(textField[1]);
		add(label[2]);
		add(textField[2]);
		add(button[0]);
		add(button[1]);
		add(label[3]);
		labelImage=new JLabel();
		add(labelImage);
	}
	 
	@Override
	public void actionPerformed(ActionEvent e)//毒入3個值 判斷跟畫圖
	{
		double r,x,y;
		// TODO Auto-generated method stub
		if (e.getSource() == button[0])
		{
			String temp;
			temp = textField[0].getText();
			r = Double.parseDouble(temp);
			if(r<0)
			{
				JOptionPane.showMessageDialog(null,"半徑不得小於0");
				for(int i=0;i<3;i++)
				{
					textField[i].setText("");
				}
				return;
			}
			temp = textField[1].getText();
			x = Double.parseDouble(temp);
			temp = textField[2].getText();
			y = Double.parseDouble(temp);
			switch(Check(r,x,y))		//呼叫Check去判斷
			{
			case 0://在圓內
				label[3].setText("該點在圓內!!");
				break;
			case 1://在圓上
				label[3].setText("該點在圓上!!");
				break;
			case 2://在圓外
				label[3].setText("該點在圓外!!");
				break;
			default:
				break;
			}
			//下面是畫圖部分
			remove(labelImage);
			this.repaint();
			BufferedImage image=new BufferedImage(400,200,BufferedImage.TYPE_INT_ARGB);//設定圖片大小
			Graphics graphics=image.getGraphics();
			graphics.setColor(Color.white);
			graphics.fillRect(0,0,400,200);//全畫白
			graphics.setColor(Color.black);
			graphics.drawOval(200-(int)r,100-(int)r,(int)r*2,(int)r*2);//畫圓
			graphics.drawLine(200, 0, 200, 200);//畫線
			graphics.drawLine(0, 100, 400, 100);//畫線
			graphics.drawString("X", 390, 115);//畫字
			graphics.drawString("Y", 190, 10);//畫字
			graphics.setColor(Color.red);
			graphics.fillOval(196+(int)x, 96-(int)y, 8, 8);//畫紅點
			graphics.dispose();//釋放資源
			labelImage=new JLabel(new ImageIcon(image));//設定labelImage
			add(labelImage);
			button[0].setEnabled(false);//不讓使用者再按一次確定
			this.repaint();//重畫視窗
		}	
		else if (e.getSource() == button[1])//清空
		{
			for(int i=0;i<3;i++)
			{
				textField[i].setText("");
			}
			label[3].setText("");
			remove(labelImage);
			button[0].setEnabled(true);//讓使用者可以按確定
			this.repaint();//重畫視窗
		}
			
	}
	public int Check(double r,double x,double y)//判斷(X,Y)到圓心(0,0)的距離是否大於半徑
	{
		if(r > Math.sqrt(x*x+y*y))
		{
			return 0;//在圓內
		}
		else if(r == Math.sqrt(x*x+y*y))
		{
			return 1;//在圓上
		}
		else
		{
			return 2;//在圓外
		}
	}
}
