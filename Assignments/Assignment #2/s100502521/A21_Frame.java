package a2.s100502521;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class A21_Frame extends JFrame implements ActionListener//類別繼承類別，類別實做介面
{
	private static final long serialVersionUID = 1L;//eclipse加的
	private JButton[] button;//按鈕
	private JTextField[] textField;//textbox
	private static JLabel[] label;//label標題
	
	public int[] myArray=new int[10];//10個  用來存使用者輸入
	public A21_Frame() 
	{
		super("泡沫排序法");//設定視窗標題
		setLayout(new FlowLayout(FlowLayout.LEFT));//設定物件的排序方式
		button = new JButton[2];
		button[0] = new JButton("確定");
		button[1] = new JButton("清空");
		button[0].addActionListener(this);// 加入按鈕
		button[1].addActionListener(this);
		//設定各個物件
		textField = new JTextField[10];
		for(int i=0;i<10;i++)
		{
			textField[i] = new JTextField(3);
		}		
		label= new JLabel[12];
		label[0]= new JLabel("請輸入10個數字:");
		
		add(label[0]);
		for(int i=0;i<10;i++)
		{
			add(textField[i]);// 加入輸入欄
		}
		add(button[0]);
		add(button[1]);
		for(int i=1;i<=11;i++)
		{
			label[i]= new JLabel("");
			add(label[i]);
		}
	}
	@Override
	public void actionPerformed(ActionEvent e) //觸發物件
	{
		if (e.getSource() == button[0])//確定鍵
		{
			for(int i=0;i<10;i++)
			{
				String temp;
				temp = textField[i].getText();
				myArray[i] = Integer.parseInt(temp);
			}
			Bubble_Sort(myArray);
		}	
		else if (e.getSource() == button[1])//清空鍵
		{
			for(int i=0;i<10;i++)
			{
				textField[i].setText("");
				label[i+1].setText("");
			}
			label[11].setText("");
		}
			
	}
	public static void Bubble_Sort (int[] array)//泡沫排序法
	{
		boolean exit;
		String temp="原先陣列:";
		temp=temp+" "+array[0];
		for(int x=1;x<10;x++)
		{
			temp=temp+","+array[x];
		}
		label[1].setText(temp);
		for(int i=0;i<array.length;i++)
		{
			exit=true;
			for(int j=0;j<array.length-1;j++)
			{
				if(array[j]>array[j+1])
				{
					exit=false;
					Swap(j,j+1,array);
				}
			}
			temp="第"+(i+1)+"次檢查後:";
			temp=temp+" "+array[0];
			for(int x=1;x<10;x++)
			{
				temp=temp+","+array[x];
			}
			temp=temp+"                                  ";//一堆空格...因為我不知道如何換行...藥用其他麻煩的排序方式-.-
			label[i+2].setText(temp);
			if(exit==true)
			{
				break;
			}
		}
	}
	public static void Swap(int a, int b, int[] array) //SWAP 將陣列的第a跟第b項互換
	{
		int temp=array[a];
		array[a]=array[b];
		array[b]=temp;
	}
}
