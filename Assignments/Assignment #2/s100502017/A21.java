package a2.s100502017;

import javax.swing.JOptionPane;//for GUI

public class A21 {
	public static void main(String[] args){
		int[] numbers=new int[10];//declare an array which length is 10
		JOptionPane.showMessageDialog(null,"Please enter 10 integer numbers");
		for(int i=0;i<10;i++){
			String temp=JOptionPane.showInputDialog(null,"Please input","input", JOptionPane.QUESTION_MESSAGE);
			numbers[i]=Integer.parseInt(temp);//turn string into integer
		}
		boolean key=true;// declare a boolean to control while loop
		while(key){
			key=false;//let key=false , if for loop doesn't run ,exit while loop
			for(int j=0;j<9;j++){
				if(numbers[j]>numbers[j+1]){
					Swap(j,j+1,numbers);
					key=true;//make while loop running again
				}
				if(j==8&&key==true){
					JOptionPane.showMessageDialog(null,arraystring(numbers));//output array
				}
			}
		}
	}
	public static void Swap(int a, int b, int[] array){//to change two number in array
		int hold=array[a];
		array[a]=array[b];
		array[b]=hold;
	}
	public static String arraystring(int[] array){//turn array into list(string)
		String result="";
		for(int k=0;k<array.length;k++){
			result+=array[k]+" ";
		}
		return result;
	}
}
