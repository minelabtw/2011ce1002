package a2.s100502017;

import javax.swing.JOptionPane;//for GUI
import java.lang.Math;//for sqrt method

public class A22 {
	public static void main(String[] args){
		String radius_s=JOptionPane.showInputDialog(null,"Please enter a integer as radius","input",JOptionPane.QUESTION_MESSAGE);//let user input
		String x_s=JOptionPane.showInputDialog(null,"input a set of two-dimensional coordinate X","input",JOptionPane.QUESTION_MESSAGE);	
		String y_s=JOptionPane.showInputDialog(null,"input a set of two-dimensional coordinate Y","input",JOptionPane.QUESTION_MESSAGE);
		double radius=Double.parseDouble(radius_s);//turn string into double
		double x=Double.parseDouble(x_s);
		double y=Double.parseDouble(y_s);
		if (Math.sqrt(x*x+y*y)>radius){//check whether the coordinate is within the circle
			JOptionPane.showMessageDialog(null,"this coordinate isn't in the circle whith radius is "+radius);
		}
		else if (Math.sqrt(x*x+y*y)==radius){
			JOptionPane.showMessageDialog(null,"this coordinate is on the circle whith radius is "+radius);
		}
		else{
			JOptionPane.showMessageDialog(null,"this coordinate is in the circle whith radius is "+radius);
		}
	}
}
