package a2.s975002502;

import java.lang.Math;
import javax.swing.JOptionPane;

public class A22 {
	public static void Judge(double radius, double X, double Y){
		
		double distance = Math.sqrt((X*X)+(Y*Y));
		if(radius>distance){
			JOptionPane.showMessageDialog(null, "座標 ("+X+","+Y+") 在圓裡面", "Bubble Sort", JOptionPane.INFORMATION_MESSAGE);
		}
		else if(radius==distance){
			JOptionPane.showMessageDialog(null, "座標 ("+X+","+Y+") 剛好在圓上", "Bubble Sort", JOptionPane.INFORMATION_MESSAGE);
		}
		else{
			JOptionPane.showMessageDialog(null, "座標 ("+X+","+Y+") 在圓外面", "Bubble Sort", JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	public static void main(String[] args){
		String string;
		double radius;
		double X,Y;
		
		string = JOptionPane.showInputDialog(null, "Please input the radius: ",  "Input", JOptionPane.QUESTION_MESSAGE);
		radius = Double.parseDouble(string);
		
		string = JOptionPane.showInputDialog(null, "Please input the X-coordinate: ",  "Input", JOptionPane.QUESTION_MESSAGE);
		X = Double.parseDouble(string);
		string = JOptionPane.showInputDialog(null, "Please input the Y-coordinate: ",  "Input", JOptionPane.QUESTION_MESSAGE);
		Y = Double.parseDouble(string);
		
		Judge(radius, X, Y);
		
	}
}
