package a2.s975002502;

import javax.swing.JOptionPane;

public class A21 {
	//將兩個數字位置交換
	public static void Swap(int a, int b, int[] array){
		int temp;
			       
        temp = array[a];
        array[a] = array[b];
        array[b] = temp;
	}
	
	//印出使用者輸入的數字即搜尋完後的結果
	public static String Output(int[] array){
		String result="";
		
		for(int i = 0; i < array.length; i++) { 
			result += Integer.toString(array[i]) + "  ";
        }
		return result;
	}
	
	//印出搜尋過程中sorted和unsorted的數字結果
	public static String Output(int sorted, int[] array){
		String result="";
		
		for(int i = 0; i < array.length; i++) { 
			result += Integer.toString(array[i]) + "  ";
            if (i==(array.length-sorted-2)){
            	result +=  "sorted>>>> ";
            }
        }
		return result;
	}
	
	//執行bubble sort 並印出蒐尋過程及結果
	public static void Bubble(int[] array){
		for(int i=0; i<array.length-1; i++) {
            for(int j=0; j<array.length-1-i; j++) {
            	if(array[j]>array[j+1]) {	//比較數字大小，大的往後放
                	Swap(j ,j+1, array); 
                }
            }
            JOptionPane.showMessageDialog(null, "ROUND: " + (i+1) + " : " + Output(i, array), "Bubble Sort", JOptionPane.INFORMATION_MESSAGE);
        }
        JOptionPane.showMessageDialog(null, "Complete!!!\nResult: " + Output(array), "Bubble Sort", JOptionPane.INFORMATION_MESSAGE);
	}
	
	public static void main(String[] args){
		String string;				//暫存使用者輸入的東西
		int[] array = new int[10];	//儲存使用者輸入的數字
		
		for(int i=0; i<array.length; i++){	//使用者輸入
			string = JOptionPane.showInputDialog(null, "Please input 10 numbers: \nThe "+(i+1)+" number",  "Input", JOptionPane.QUESTION_MESSAGE);
			array[i] = Integer.parseInt(string);
		}
		
		JOptionPane.showMessageDialog(null, "Your inputs are: " + Output(array), "Bubble Sort", JOptionPane.INFORMATION_MESSAGE);
		JOptionPane.showMessageDialog(null, "Start sorting.... ", "Bubble Sort", JOptionPane.INFORMATION_MESSAGE);
        
		Bubble(array);
  	}

}
