	// ========================================================================================= //
	//		Author: Pluto Chan, Ka Hou															 //
	//		E-mail: jesic01@jesic-tech.com														 //
	//		Date: 2012/3/1																		 //
	//		Last-Mod: 2012/3/3																	 //
	//		Description: Assignment II for course CE1002 - Introduction to Computer Science II	 //
	//					 A basic bubble sorting program using method, array and basic GUI		 //
	// ========================================================================================= //

package a2.s985003038;

import javax.swing.*;

public class A21 {
	private static String programName = "Pluto's Programming";				// title of the message dialog and the input dialog
	public static void main(String[] argv){
		while(true){
			int countOfNumbers = Integer.parseInt(JOptionPane.showInputDialog(null, "How many integers do you want to sort? < 0 to exit >", programName, JOptionPane.QUESTION_MESSAGE));
			if(countOfNumbers == 0) break;									// let user to decide how many numbers are going to be sorted, and 0 to exit the program 
			int[] numbers = new int[countOfNumbers];						// creating a new integer array with the size according to the user's input
			for(int i = 1; i <= countOfNumbers; i++)						// asking the user to input the numbers until all numbers are input
				numbers[i-1] = Integer.parseInt(JOptionPane.showInputDialog(null, "What is the value of the " + i + generateSuffix(i) + " integer?", programName, JOptionPane.QUESTION_MESSAGE));
			
			JOptionPane.showMessageDialog(null, "Before sorting:\n" + showArrayElement(numbers), programName, JOptionPane.INFORMATION_MESSAGE);
																			// list out all the elements and the position of the elements in the array before sorting
			bubbleSort(numbers, true);										// take the bubble sort by passing the array to a bubbleSort method
			JOptionPane.showMessageDialog(null, "After sorting:\n" + showArrayElement(numbers), programName, JOptionPane.INFORMATION_MESSAGE);
		}																	// list out all the elements and the position of the elements in the array after sorting
		JOptionPane.showMessageDialog(null, "Bye", programName, JOptionPane.INFORMATION_MESSAGE);
																			// if user wnat to exit the program, show out a GoodBye message
	}
	
	// ========================================================================================= //
	//		Name: bubbleSort																	 //
	//		Input: an integer array, a boolean flag to control the sorting order				 //
	//		Output: none																		 //
	//		Description: using bubble sort to sort a integer array according to the order flag,	 //
	//					 also, show out the result of each phase by using a dialog				 //
	// ========================================================================================= //
	public static void bubbleSort(int[] array, boolean ascending){
		for(int i = 0; i < array.length - 1; i++){							// the simple bubble sorting loops
			for(int j = i + 1; j < array.length; j++){						// please see the definition of the bubble sorting algorithm
				if((ascending && array[i] > array[j]) || (!ascending && array[i] < array[j]))
					swap(i, j, array);										// according to the order flag, compare the two continuous elements
			}																// and decide whether it should take the swap operation
			JOptionPane.showMessageDialog(null, "Phase " + (i + 1) + ":\n" + showArrayElement(array), programName, JOptionPane.INFORMATION_MESSAGE);
		}																	// list out all the elements and the position of the elements after each phase
	}
	
	// ========================================================================================= //
	//		Name: swap																			 //
	//		Input: two array index in integer and one integer array								 //
	//		Output: none																		 //
	//		Description: swapping two elements in the input array according to the two indexes	 //
	// ========================================================================================= //
	public static void swap(int a, int b, int[] array){
		int temp = array[a];												// basic swapping operation by passing the value of one element to a temporary register
		array[a] = array[b];												// copy the value of the other element
		array[b] = temp;													// and replace the value of the other element from the register 
	}
	
	// ========================================================================================= //
	//		Name: showArrayElement																 //
	//		Input: an integer array																 //
	//		Output: a list of element in string													 //
	//		Description: list out all the elements in the input array as a string, and return	 //
	//					 the list to the caller													 //
	// ========================================================================================= //
	public static String showArrayElement(int[] array){
		String result = "";
		for(int i = 0; i < array.length; i++){								// a loop to step over the array
			result = result + array[i] + " ";								// copy the element into the string list
		}
		return result;														// return the list so the caller can have the list of the elements in the array
	}
	
	// ========================================================================================= //
	//		Name: generateSuffix																 //
	//		Input: a integer number																 //
	//		Output: a suffix in string															 //
	//		Description: consider the suffix of the number in English, such as firST, senoND,	 //
	//					 thiRD, and etc															 //
	// ========================================================================================= //
	public static String generateSuffix(int number){
		if(String.valueOf(number).charAt(String.valueOf(number).length()-1) == '1' && number != 11)
			return "st";													// if the number is end with one but not eleven, return the suffix "st"
		else if(String.valueOf(number).charAt(String.valueOf(number).length()-1) == '2' && number != 12)
			return "nd";													// if the number is end with two but not twelve, return the suffix "nd"
		else if(String.valueOf(number).charAt(String.valueOf(number).length()-1) == '3' && number != 13)
			return "rd";													// if the number is end with three but not thirteen, return the suffix "rd"
		else
			return "th";													// otherwise, return the suffix "th"
	}
}