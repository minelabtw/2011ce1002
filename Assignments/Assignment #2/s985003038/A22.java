	// ========================================================================================= //
	//		Author: Pluto Chan, Ka Hou															 //
	//		E-mail: jesic01@jesic-tech.com														 //
	//		Date: 2012/3/1																		 //
	//		Last-Mod: 2012/3/3																	 //
	//		Description: Assignment II for course CE1002 - Introduction to Computer Science II	 //
	//					 A simple program to analysis if a point is within a circle or out of	 //
	//					 the circle, and using GUI to draw out the circle and the points		 //
	// ========================================================================================= //

package a2.s985003038;

import java.text.DecimalFormat;
import java.awt.*;
import javax.swing.*;

public class A22 extends JPanel{
	private static final long serialVersionUID = 1L;
	private static int state = 0;										// a finite state to control the drawing method
	private static int multiple = 5;									// ratio of the length, that is, the ratio when drawing the circle, you can adjust it by yourself
																		// formula: the displaying radius of the circle = the actual radius of the circle * the value of this ratio variable
	private static int radius;
	private static String points = "";									// since i need to draw out all the points on the circle, i need an infinite-size array to keep all the coordinates of the points
																		// however, the infinite-size array actually does not exist, so i design my data structure by using a string
	private static String programName = "Pluto's Programming";			// title of the message dialog and the input dialog
	
	public static void main(String[] argv){
		DecimalFormat DoubleFormat = new DecimalFormat("#.##");			// format to the distance with decimal places
		JFrame frame = new JFrame();									// creating a GUI frame
		frame.add(new A22());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);			// close the frame when the program is terminated
		frame.setTitle(programName);									// set the title of the frame
        frame.setSize(500, 500);										// set the size of the frame
        frame.setVisible(true);											// set the visibility so you can see the frame
		
		radius = Integer.parseInt(JOptionPane.showInputDialog(null, "What is the radius of the circle?", programName, JOptionPane.QUESTION_MESSAGE));
		state = 1;														// ask you to set the radius of the circle and define the state as 1
		frame.repaint();												// draw the circle on the frame
		String inputValue = "rule";										// force the user to read the rule at the first time of using the program
		while(true){
			if(inputValue.toLowerCase().equals("exit")){				// if the user input EXIT, show the GoodBye message and terminate the program
				JOptionPane.showMessageDialog(null, "Bye", programName, JOptionPane.INFORMATION_MESSAGE);
				System.exit(0);
			} else if(inputValue.toLowerCase().equals("rule")){			// if the user input RULE, show out the rules
				JOptionPane.showMessageDialog(null, "Rule:\n" +
						"You should input a two-dimensional coordinate\n" +
						"You should use a COMMA sparating the X and Y coordinate. For example: 8, 12\n" + 
						"If you want to reset the radius, input RADIUS\n" +
						"If you want to read the rules again, input RULE\n" +
						"To exit the program, input EXIT\n" +
						"It doesn't matter whatever your input is in lowercase or uppercase letter", programName, JOptionPane.INFORMATION_MESSAGE);
			} else if(inputValue.toLowerCase().equals("radius")){		// if the user input RADIUS
				radius = Integer.parseInt(JOptionPane.showInputDialog(null, "What is the radius of the circle?", programName, JOptionPane.QUESTION_MESSAGE));
				state = 1;												// let the user to reset the radius of the circle, reset the state as 1
				points = "";											// and also reset the infinite points array
				frame.repaint();										// then, redraw the circle
			} else if(inputValue.contains(",")){						// if the user input a two-dimensional coordinate of a point
				String[] coordinate = inputValue.split(",");			// separate it as x-coordinate and y-coordinate
				int x = Integer.parseInt(coordinate[0].trim());
				int y = Integer.parseInt(coordinate[1].trim());
				double distance = Math.sqrt(x * x + y * y);				// use the coordinate the calculate the distance between the point and the center of the circle
				points = points + (points.isEmpty() ? "" : ",") + x + "," + y;
				state = 2;												// insert the point into the infinite points array, and set the state as 2
				frame.repaint();										// then draw out the points, and show the result message
				JOptionPane.showMessageDialog(null, "The point (" + x + ", " + y + ") is " + (distance > radius ? "outside" : "within") + " the circle, the distance between (" + x + ", " + y + ") and (0, 0) is " + DoubleFormat.format(distance), programName, JOptionPane.INFORMATION_MESSAGE);
			} else {													// otherwise, a wrong input is detected, show an error message
				JOptionPane.showMessageDialog(null, "Wrong Input", programName, JOptionPane.ERROR_MESSAGE);
			}
			inputValue = JOptionPane.showInputDialog(null, "What is the coordinate of the point?\n<Input RULE to read the rule>", programName, JOptionPane.QUESTION_MESSAGE);
		}																// after that, show the message and ask for the coordinate of the next points
	}
	
	// ========================================================================================= //
	//		Name: paint																			 //
	//		Input: none																			 //
	//		Output: none																		 //
	//		Description: a redefined method in JPanel, used to draw a graph on the frame.		 //
	//					 never call this method directly! this method will automatically execute //
	//					 when the frame is created and whenever the repaint() method are called	 //
	// ========================================================================================= //
	public void paint(Graphics graph){
		if(state == 0){													// if state is 0, it is just starting the program
			graph.setFont(new Font("Arial", Font.BOLD, 45));			// set the font-family as Arial, font-weight as Bold, and font-size as 45pt
			graph.setColor(Color.blue);									// set the font-color as Blue
			graph.drawString(programName, 15, 230);						// and draw out the title of the program
		} else {														// otherwise, draw the circle and the points
			graph.setColor(Color.blue);									// using blue color to draw the circle
			graph.drawOval(230 - radius * multiple, 230 - radius * multiple, radius * multiple * 2, radius * multiple * 2);
																		// set the center of the circle to be (230, 230), and draw the circle according to the input radius and the ratio of the length
			if(state != 1){												// if the user has already input a point, draw the point also
				String[] coordinate = points.split(",");				// separate the points array by splitting it at the COMMA, so you can get all the coordinates of the points
				for(int i = 0; i < coordinate.length; i += 2){			// group all the coordinates by two, that is, the two-dimensional coordinate of each points
					int x = Integer.parseInt(coordinate[i]);			// the former one is the x-coordinate
					int y = Integer.parseInt(coordinate[i+1]);			// and the later one is the y-coordinate
					graph.setColor(Math.sqrt(x * x + y * y) > radius ? Color.red : Color.blue);
																		// use different color to draw the points according to the distance between the point and the center of the circle
																		// red color for points outside the circle, and blue color for points within the circle
					int xLocation = 230 + x * multiple;					// calculating the mapping coordinate on the displaying graph
					int yLocation = 230 + y * -multiple;
					graph.drawLine(xLocation - 5, yLocation - 5, xLocation + 5, yLocation + 5);
					graph.drawLine(xLocation + 5, yLocation - 5, xLocation - 5, yLocation + 5);
																		// then, draw a cross symbol on the location of the point
					graph.setFont(new Font("Arial", Font.PLAIN, 12));	// and write the actual coordinate of the point beside the cross symbol
					graph.drawString("(" + x + ", " + y + ")", xLocation + 10, yLocation + 10);
				}
			}
		}
	}
}
