package a2.s100502029;
import javax.swing.JOptionPane;

public class A22 {
	public static void main(String[] args) {
		String radiusFromDialog = JOptionPane.showInputDialog("Enter a integer as radius:"); // read and store radius in string type
		int radius = Integer.parseInt(radiusFromDialog); // change radius from string type to int type
		
		String xCoordinate = JOptionPane.showInputDialog("Enter x-coordinate:"); // read and store x-coordinate
		double coordinate_x = Double.parseDouble(xCoordinate); // change x-coordinate from string type to double type
		
		String yCoordinate = JOptionPane.showInputDialog("Enter y-coordinate:");
		double coordinate_y = Double.parseDouble(yCoordinate);
		
		String result = insideOrOutside(radius, coordinate_x, coordinate_y); // invoke insideOrOutside method
		
		JOptionPane.showMessageDialog(null, result); // show result
	}
	
	// check whether the coordinate is within the circle, whose radius is r
	public static String insideOrOutside(int r, double x, double y) {
		if (r > Math.sqrt(x * x + y * y))
			return "The coordinate is within the circle.";
		else if (r < Math.sqrt(x * x + y * y))
			return "The coordinate is outside of  the circle.";
		else
			return "The coordinate is on the circle.";
	}
}
