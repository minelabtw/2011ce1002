package a2.s100502029;
import javax.swing.JOptionPane;

public class A21 {
	public static void main(String[] args) {
		final int arraysize = 10;
		String[] numberFromDialog = new String[ arraysize ]; // this array will store the number from user in string type
		int[] number = new int[ arraysize ]; // this array store the number in "int" type

		JOptionPane.showMessageDialog(null,"Please enter 10 integer numbers.");
		for (int i = 0; i < arraysize; i++) {
			numberFromDialog[ i ] = JOptionPane.showInputDialog("Enter the " + (i + 1) + suffix(i + 1) + " integer number:"); // read and store number in array "numberFromDialog"
			number[ i ] = Integer.parseInt(numberFromDialog[ i ]); // change number from string type to int type
		}
		
		int numberOfPhase = 1; // initial the number of phase to 1
		String output = "";
		boolean flag = true; // true means the latest loop used swap method; false means not used
		while (flag) {
			flag = false;
			output += "\n" + numberOfPhase + suffix(numberOfPhase) + " Phase:\n";
			
			// display the former array
			for (int i = 0; i < arraysize; i++)
				output += number[ i ] + " ";
			output += "   ===>   ";
			
			// 檢查陣列裡相鄰的兩元素是否排序好,若排序錯誤則呼叫swap method將他們顛倒
			for (int j = 0; j < arraysize - 1; j++) {
				if (number[ j ] > number[ j + 1 ]) {
					swap(j, j + 1, number);
					flag = true;
				}
			}
			
			// display the latter array
			for (int k = 0; k < arraysize; k++)
				output += number[ k ] + " ";
			// display no swap message
			if (flag == false)
				output += "\nThere is no swap occur in " + numberOfPhase + suffix(numberOfPhase) + " phase.";
			
			numberOfPhase++;
		}
		JOptionPane.showMessageDialog(null, output); // show the result
	}
	
	// swap two element's number
	public static void swap(int a, int b, int[] array) {
		int hold = array[ a ];
		array[ a ] = array [ b ];
		array[ b ] = hold;
	}
	
	// 判斷第幾phase的後面要跟什麼尾標
	public static String suffix(int sequence) {
		if (sequence == 1)
			return "st";
		else if (sequence == 2)
			return "nd";
		else if (sequence == 3)
			return "rd";
		else 
			return "th";
	}
}