package a2.s995002201;

import javax.swing.JOptionPane;
import java.util.Scanner;

public class A21
{
	public static void main(String[] args)
	{
		int[] number1 = new int[10];
		int temp;
		Scanner input = new Scanner( System.in );
		
		//跳出對話框題是使用者再下方輸入數字
		JOptionPane.showMessageDialog(null, "Input 10 integer numbers below");
		
		//將輸入的整數存入陣列
		for(int i=0;i<10;i++)
		{
			number1[i] = input.nextInt();	
		}
		
		//bubble sort
		for(int i=8;i>=0;i--)
		{
			for(int j=0;j<=i;j++)
			{
				if(number1[j]>number1[j+1])
				{
					Swap(j,j+1,number1);
				}
			}
		}
		
		//印出結果
		for(int i=0;i<10;i++)
		{
			System.out.print(number1[i]+" ");
		}
	}
	public static void Swap( int a, int b,int[] array)
	{
		int t = array[a];
		array[a] = array [b];
		array[b] = t;
	}
}