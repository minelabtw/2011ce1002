package a2.s995002201;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class A22 
{
	public static void main( String[] args )
	{
		double rad,x,y,circle,ans1,ans2;	
		Scanner input = new Scanner( System.in );
		
		//跳出使用者輸入的視窗
		String first = JOptionPane.showInputDialog("Input a integer as radius");
		rad = Integer.parseInt(first);
		
		//跳出顯示文字的視窗
		JOptionPane.showMessageDialog(null, "Input a set of two-dimensional coordinate X and Y");
		
		//輸入座標點X,Y
		String second = JOptionPane.showInputDialog("X= ");
		x = Integer.parseInt(second);
		
		String third = JOptionPane.showInputDialog("Y= ");
		y = Integer.parseInt(third);
		
		//個自計算圓
		circle = Mathsqrt(rad);
		ans1 = Mathsqrt(x);
		ans2 = Mathsqrt(y);
		
		//判別
		if((ans1+ans2<=circle))
		{
			JOptionPane.showMessageDialog(null, "(" + x + "," + y + ") is within the circle");
		}
		else
		{
			JOptionPane.showMessageDialog(null, "(" + x + "," + y + ") is not within the circle");
		}
	}
	public static double Mathsqrt(double a)
	{
		return a = a*a;
	}
}
