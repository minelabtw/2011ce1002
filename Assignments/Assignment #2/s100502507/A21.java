package a2.s100502507;

import javax.swing.JOptionPane;

public class A21 {
	//Function main begins execution
	public static void main(String[] args){
		int[] numbers = new int[10];//Array to store numbers which user inputs
				
		for(int i=0; i<10; i++){
			String inputNumberString = JOptionPane.showInputDialog("Please input 10 numbers in total\n" + "Now input number" + (i+1));//Tell user to input number
			numbers[i] = Integer.parseInt(inputNumberString);//Store number into array
		}
		
		boolean keepGoing = true;//Stop sorting condition
		String output = "(";
		
		while(keepGoing){
			keepGoing = false;//First, let "keepGoing" be false
			//And once there's sorting take place, "keepGoing" will be true to check array again
			for(int i=0; i<9; i++){
				if(numbers[i]>numbers[i+1]){
					swap(i, i+1, numbers);
					keepGoing = true;
				}
			}
			
			for(int i=0; i<10; i++){
				output = output + numbers[i];
				if(i<9){
					output = output + ", ";
				}
			}
			
			//If array has been sorted, output the result to let user know what happened
			if(keepGoing==true){
				JOptionPane.showMessageDialog(null, output + ")");
				output = "(";
			}
			else{//If array didn't been sorted, tell user that all works was done
				JOptionPane.showMessageDialog(null, "Numbers you inputed don't need to be swaped anymore!");
			}
		}
	}//End function main
	//Function "swap" receive two elements of array "numbers" and exchange them
	public static void swap(int element01, int element02, int[] Array){
		int hold = Array[element02];
		Array[element02] = Array[element01];
		Array[element01] = hold;
	}
}
