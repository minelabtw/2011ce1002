package a2.s100502507;

import javax.swing.JOptionPane;
import java.lang.Math;

public class A22 {
	public static void main(String[] args){//Function main begins execution
		String inputRadius = JOptionPane.showInputDialog(null, "Input radius please: ");
		int radius = Integer.parseInt(inputRadius);
		
		String inputX = JOptionPane.showInputDialog(null, "Now input X please: ");
		double X = Double.parseDouble(inputX);
		
		String inputY = JOptionPane.showInputDialog(null, "Now input Y please: ");
		double Y = Double.parseDouble(inputY);
		
		if(Math.sqrt(X*X + Y*Y)<radius){//Point inside the circle
			JOptionPane.showMessageDialog(null, "(" + X + ", " + Y + ")" + " is within the circle");
		}
		else if(Math.sqrt(X*X + Y*Y)==radius){//Point on the circle
			JOptionPane.showMessageDialog(null, "(" + X + ", " + Y + ")" + " is on the circle");
		}
		else{//Point outside the circle
			JOptionPane.showMessageDialog(null, "(" + X + ", " + Y + ")" + " is out of the circle");
		}
		
	}

}
