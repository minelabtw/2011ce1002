package a2.s100502520;

import java.util.Scanner;

import javax.swing.JOptionPane;

public class A22 {
	public static int jud;

	public static void main(String[] args) {
		A22 useA22 = new A22();
		// input a r
		String rad = JOptionPane
				.showInputDialog("please input a integer as radius: ");
		// change string to integer
		int radius = Integer.parseInt(rad);
		String x = JOptionPane.showInputDialog("the X is: ");
		// change string to double
		double X = Double.parseDouble(x);
		String y = JOptionPane.showInputDialog("the Y is: ");
		// change string to double
		double Y = Double.parseDouble(y);
		useA22.inorout(radius, X, Y);
		if (jud == 1) { // judge out or in
			String output1 = "the coordinate is in the circle";
			JOptionPane.showMessageDialog(null, output1);
		} else {
			String output2 = "the coordinate is out of the circle";
			JOptionPane.showMessageDialog(null, output2);
		}

	}

	//the method to judge in or out
	public void inorout(int r, double a, double b) {
		if (Math.sqrt((a * a) + (b * b)) <= r) {
			jud = 1;
		} else {
			jud = 2;
		}
	}

}
