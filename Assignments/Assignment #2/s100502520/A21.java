package a2.s100502520;

import javax.swing.*;
import java.awt.event.*;
import java.awt.BorderLayout;

public class A21 extends javax.swing.JFrame {
	private JPanel jPanelN;
	private JPanel jPanelC;
	private JPanel jPanelS;
	private JTextField[] textarr = new JTextField[10];
	public int[] array = new int[10];

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				A21 use21 = new A21();
				//center
				use21.setLocationRelativeTo(null);
				// show the window
				use21.setVisible(true);
			}
		});
	}
	//constructer
	public A21() {
		super();
		gui();
	}

	// to swap tow number
	public void swap(int a, int b, int[] arr) { 
		int transfer;
		transfer = arr[a];
		arr[a] = arr[b];
		arr[b] = transfer;
		array[a] = arr[a];
		array[b] = arr[b];
	}

	public void gui() {
		//click x will close the program
		setDefaultCloseOperation(EXIT_ON_CLOSE); 
		//divide three panel
		jPanelC = new JPanel();
		getContentPane().add(jPanelC, BorderLayout.CENTER);
		jPanelS = new JPanel();
		getContentPane().add(jPanelS, BorderLayout.SOUTH);
		jPanelN = new JPanel();
		getContentPane().add(jPanelN, BorderLayout.NORTH);
		// create two label
		jPanelN.add(new JLabel("Bubble Sort algorithm"));
		jPanelN.add(new JLabel("please input 10 number: "));
		for (int i = 0; i < 10; i++) {
			textarr[i] = new JTextField();
			jPanelC.add(textarr[i]);
			textarr[i].setColumns(5);
		}
		JButton button = new JButton("");
		button.setText("Start");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Start();
			}
		});
		jPanelS.add(button);
		setSize(350, 200);
	}

	//if user click the button will run
	public void Start() {
		//change the string to integer
		for (int i = 0; i < 10; i++) {  
			array[i] = Integer.valueOf(textarr[i].getText());
			System.out.println(textarr[i].getText());
		}
		int judge = 1;
		int step = 1;
		//to judge swap() run or did not run
		while (judge == 1) { 
			judge = 2;
			for (int j = 0; j < 9; j++) {

				if (array[j] > array[j + 1]) {
					swap(j, (j + 1), array);
					judge = 1;
				}
			}
			System.out.print("the " + step + " phase is : ");
			for (int k = 0; k < 10; k++) {
				System.out.print(array[k] + " ");
			}
			System.out.println(" ");
			step++;
		}

	}
}
