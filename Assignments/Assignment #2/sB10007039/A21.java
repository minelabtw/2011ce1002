package a2.sB10007039;

import java.awt.BorderLayout;
import java.awt.event.*;
import javax.swing.*;

@SuppressWarnings("serial")
public class A21 extends javax.swing.JFrame {//建立class並繼承GUI class javax.swing.JFrame
	private JPanel jPanelm1;
	private JPanel jPanelm2;
	private JPanel jPanel1;
	private JPanel jPanel2;
	private JPanel jPanel3;
	private JPanel jPanel4;
	private JTextField[] NumberText = new JTextField[10];
	private JLabel Lable1;
	private JTextPane TextPane;
	private JButton Start;
	private JButton Clear;
	private int Window_height = 150;
	private int Window_width = 700;
	private int window_up = 0;
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {//分配執行序
			public void run() {
				A21 inst = new A21();
				inst.setLocationRelativeTo(null);
				inst.setVisible(true);
			}
		});
	}

	public A21() {
		super();//GUI class 建構元呼叫
		initGUI();//初始化視窗
	}
	
	private void Start_button() {
		int[] Temp = new int[10];
		boolean check_nopass = false;
		boolean	check_swap = false;
		{//LOG顯示
			TextPane.setText("");
			window_up = 0;
			setSize(700, Window_height);
			jPanelm2.setPreferredSize(new java.awt.Dimension(700,0));
			jPanel4.setPreferredSize(new java.awt.Dimension(700,0));
			TextPane.setPreferredSize(new java.awt.Dimension(700,0));
		}
		for (int i = 0; i < 10; i++) {//string轉換int
			try{//例外處理
				Temp[i]=Integer.valueOf(NumberText[i].getText());
			}catch (NumberFormatException e){
				check_nopass = true;
				System.out.println(" parse int error!!  " + e);
			}
		}
		if (check_nopass) {
			Lable1.setText("!!Some TextField value is not Integer!!");
		  	Start.setText("Try Agin");
		} else {
			Lable1.setText("!!Success!!");
		  	Start.setText("Start");
		  	for (int i = 0; i < 9; i++) {//比較數字大小
		  		if(Temp[i] > Temp[i+1]){
		  			Swap(i, i+1, Temp);
		  			check_swap = true;
		  			for (int j = 0; j < Temp.length; j++) {
		  				TextPane.setText(TextPane.getText()+"   "+String.valueOf(Temp[j]));
					}
		  			{//LOG顯示
			  			TextPane.setText(TextPane.getText()+"\n");
			  			window_up+=1;
			  			setSize(700, Window_height+(18*window_up));
						jPanelm2.setPreferredSize(new java.awt.Dimension(700,(18*window_up)));
						jPanel4.setPreferredSize(new java.awt.Dimension(700,(18*window_up)));
						TextPane.setPreferredSize(new java.awt.Dimension(700, (18*window_up)));
		  			}
		  		}
		  		if (check_swap == true) {
					i = -1;
				}
		  		check_swap = false;
			}
		  	for (int i = 0; i < 10; i++) {
				NumberText[i].setText(String.valueOf(Temp[i]));//把排序完成的數字放回
			}
		}
		
		
	}
	
	private void Clear_button() {//重置所有元件
		for (int i = 0; i < 10; i++) {
			NumberText[i].setText("");
			TextPane.setText("");
			window_up = 0;
			setSize(700, Window_height);
			Lable1.setText("Enter 10 number for Bubble Sort algorithm");
			jPanelm2.setPreferredSize(new java.awt.Dimension(700,0));
			jPanel4.setPreferredSize(new java.awt.Dimension(700,0));
			TextPane.setPreferredSize(new java.awt.Dimension(700,0));
		}
	}
	
	private void initGUI() {
		try {
				this.setSize(Window_width, Window_height);//設定視窗大小
				this.setTitle("B10007029 - A21");//設定視窗標題
				this.setDefaultCloseOperation(EXIT_ON_CLOSE);//設定按下「X」結束城市
				this.setResizable(false);//關閉最大化按鈕
				{//建立面板
					jPanelm1 = new JPanel();
					jPanelm2 = new JPanel();
					jPanel1 = new JPanel();
					jPanel2 = new JPanel();
					jPanel3 = new JPanel();
					jPanel4 = new JPanel();
				}
				{//放置面板到視窗
					getContentPane().add(jPanelm1, BorderLayout.NORTH);
					getContentPane().add(jPanelm2, BorderLayout.SOUTH);
					
					jPanelm1.add(jPanel1, BorderLayout.NORTH);
					jPanelm1.add(jPanel2, BorderLayout.CENTER);
					jPanelm1.add(jPanel3, BorderLayout.SOUTH);
					jPanelm2.add(jPanel4, BorderLayout.SOUTH);
				}
				{//設定面板大小
					jPanelm1.setPreferredSize(new java.awt.Dimension(700, 130));
					jPanelm2.setPreferredSize(new java.awt.Dimension(700, 0));
					//jPanel1.setPreferredSize(new java.awt.Dimension(700, 20));
					//jPanel2.setPreferredSize(new java.awt.Dimension(700, 30));
					//jPanel3.setPreferredSize(new java.awt.Dimension(700, 30));
					jPanel4.setPreferredSize(new java.awt.Dimension(700, 0));
				}
				{//建立lable文字區塊
					Lable1 = new JLabel();
					jPanel1.add(Lable1);
					Lable1.setText("Enter 10 number for Bubble Sort algorithm");
					Lable1.setOpaque(true);
				}

				{//建立十個Textfield文字輸入方塊
					for (int i = 0; i < 10; i++) {
						NumberText[i] = new JTextField();
						jPanel2.add(NumberText[i]);
						NumberText[i].setColumns(5);
					}
					
				}
				{//建立按鈕
					Start = new JButton();
					jPanel3.add(Start);
					Start.setText("Start");
					Start.addActionListener(new ActionListener() {//設定動作監聽
						@Override
						public void actionPerformed(ActionEvent arg0) {
							// TODO Auto-generated method stub
							Start_button();
						}
					});
					Clear = new JButton();
					jPanel3.add(Clear);
					Clear.setText("Clear");
					Clear.addActionListener(new ActionListener() {//設定動作監聽
						@Override
						public void actionPerformed(ActionEvent arg0) {
							// TODO Auto-generated method stub
							Clear_button();
						}
					});
					
				}
				{//建立LOG輸出
					TextPane = new JTextPane();
					jPanel4.add(TextPane, BorderLayout.CENTER);
					TextPane.setText("");
					TextPane.setPreferredSize(new java.awt.Dimension(700, 100));
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void Swap(int a,int b,int[] array) {
		int temp=0;
		temp = array[a];
		array[a] = array[b];
		array[b] = temp;
		
	}
}
