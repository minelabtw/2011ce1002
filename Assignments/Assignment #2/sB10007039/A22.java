package a2.sB10007039;

import java.awt.BorderLayout;
import java.awt.event.*;
import javax.swing.*;

@SuppressWarnings("serial")
public class A22 extends javax.swing.JFrame {//建立class並繼承GUI class javax.swing.JFrame
	private JPanel jPanelm1;
	private JPanel jPanelm2;
	private JPanel jPanel1;
	private JPanel jPanel2;
	private JPanel jPanel3;
	private JPanel jPanel4;
	private JTextField[] NumberText = new JTextField[3];
	private JLabel[] Lable = new JLabel[4];
	private JButton Start;

	public static void main(String[] args) {
		/*SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				A22 inst = new A22();
				inst.setLocationRelativeTo(null);
				inst.setVisible(true);
			}
		});*/
		A22 inst = new A22();
		inst.setLocationRelativeTo(null);
		inst.setVisible(true);
	}

	public A22() {
		super();//父類別建構元呼叫
		initGUI();//初始化視窗
	}
	private void Start_button() {
		double[] Temp = new double[4];
		double cou = 0.0;
		boolean check_nopass = false;
		for (int i = 0; i < 3; i++) {//string轉換int
			try{//例外處理
				Temp[i]=Double.valueOf(NumberText[i].getText()+".");
			}catch (NumberFormatException e){
				check_nopass = true;
				System.out.println(" parse int error!!  " + e);
			}
		}
		if (check_nopass) {
			Lable[3].setText("!!Some TextField value is not Integer!!");
		  	Start.setText("Try Agin");
		} else {
			Lable[3].setText("!!Success!!");
		  	Start.setText("Start");
		  	cou = Temp[1]*Temp[1]+Temp[2]*Temp[2];
		  	if (Math.sqrt(cou) <= Temp[0]) {
		  		Lable[3].setText("In Circle!!");
			} else {
				Lable[3].setText("Not In Circle!!");
			}
		  	;
		}
		
		
	}

	private void initGUI() {
		try {
				this.setSize(400, 250);//設定視窗大小
				this.setTitle("B10007029 - A22");//設定視窗標題
				this.setDefaultCloseOperation(EXIT_ON_CLOSE);//設定按下「X」結束城市
				this.setResizable(false);//關閉最大化按鈕
				{//建立面板
					jPanelm1 = new JPanel();
					jPanelm2 = new JPanel();
					jPanel1 = new JPanel();
					jPanel2 = new JPanel();
					jPanel3 = new JPanel();
					jPanel4 = new JPanel();
				}
				{//放置面板到視窗
					getContentPane().add(jPanelm1, BorderLayout.NORTH);
					getContentPane().add(jPanelm2, BorderLayout.SOUTH);
					
					jPanelm1.add(jPanel1, BorderLayout.NORTH);
					jPanelm1.add(jPanel2, BorderLayout.SOUTH);
					jPanelm2.add(jPanel3, BorderLayout.NORTH);
					jPanelm2.add(jPanel4, BorderLayout.SOUTH);
				}
				{//設定面板大小
					jPanelm1.setPreferredSize(new java.awt.Dimension(700, 100));
					jPanelm2.setPreferredSize(new java.awt.Dimension(700, 100));
					jPanel1.setPreferredSize(new java.awt.Dimension(700, 50));
					jPanel2.setPreferredSize(new java.awt.Dimension(700, 50));
					jPanel3.setPreferredSize(new java.awt.Dimension(700, 50));
					jPanel4.setPreferredSize(new java.awt.Dimension(700, 50));
				}
				{//建立按鈕
					Start = new JButton();
					jPanel4.add(Start);
					Start.setText("Start");
					Start.addActionListener(new ActionListener() {//設定動作監聽
						@Override
						public void actionPerformed(ActionEvent arg0) {
							// TODO Auto-generated method stub
							Start_button();
						}
					});
					
				}
				{//建立Textfield文字輸入方塊
					for (int i = 0; i < 3; i++) {
						NumberText[i] = new JTextField();
						NumberText[i].setColumns(5);
					}
				}
				{//建立lable文字區塊
					for (int i = 0; i < 4; i++) {
						Lable[i] = new JLabel();
						Lable[i].setOpaque(true);
					}
				}
				{//放置元件
					Lable[0].setText("Radius:");
					Lable[1].setText("Coordinate X:");
					Lable[2].setText("Coordinate Y:");
					Lable[3].setText("");
					jPanel1.add(Lable[0]);
					jPanel1.add(NumberText[0]);
					jPanel2.add(Lable[1]);
					jPanel2.add(NumberText[1]);
					jPanel2.add(Lable[2]);
					jPanel2.add(NumberText[2]);
					jPanel3.add(Lable[3]);
					jPanel4.add(Start);
					
				}


		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void Swap(int a,int b,int[] array) {
		int temp=0;
		temp = array[a];
		array[a] = array[b];
		array[b] = temp;
		
	}
}
