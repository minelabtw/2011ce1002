package a2.s100502021;
import java.util.Scanner;
import javax.swing.JOptionPane;
public class A21 {
	public static void swap(int a,int b,int[] array){ //bubble sort function
		int c;
		c=array[a];
		array[a]=array[b];
		array[b]=c;		
	}
	public static void main(String args[]){
		Scanner input=new Scanner(System.in);
		JOptionPane.showMessageDialog(null,"enter ten number : "); 
		int num[]=new int[10];
		for (int i = 0; i < 10; i++){ 
			  String number = JOptionPane.showInputDialog(null, "number "," input ",JOptionPane.QUESTION_MESSAGE); //enter string and convert integer 
			  num[i]=Integer.parseInt(number);
		}
		
		boolean fin=false;
		for (int i = 0; i < 10; i++){
			System.out.print(num[i]+"\t"); 
		}
		
		System.out.print("\n");
		String strnum="";
		while(fin==false){
			fin=true; //go into while then change fin into true 
			for (int x = 0; x<9 ; x++){		
					if(num[x]>num[x+1]){						
					swap(x,x+1,num);
					fin=false; 
					}												
			}			
			for (int i = 0; i < 10; i++){
				strnum=strnum+Integer.toString(num[i]);				
			}			
			strnum=strnum+"\n";			
			if (fin==true){ //without swap , check and break
				System.out.print("the end");
				break;
			}	
		}
		JOptionPane.showMessageDialog(null,strnum);

	}
}
