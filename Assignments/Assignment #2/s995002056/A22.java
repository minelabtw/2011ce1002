package a2.s995002056;
import java.lang.Math;												       // 判斷座標是否在圓內
import javax.swing.JOptionPane;

public class A22 {
	
	public static double WithinCircle(double X , double Y){ 		      // 求三角第三邊的mathod
		
		double sum = X * X + Y * Y ;
		double d = Math.sqrt(sum);
		
		return d ;
	}
	
	public static void main(String [] argv){                              // 主程式
		

		
		double radius ;
		double X , Y ;
		double d ;
		
		JOptionPane.showMessageDialog(null,"此程式將幫您判斷座標是否在圓內","Welcome",JOptionPane.INFORMATION_MESSAGE);
		
		String input1 = JOptionPane.showInputDialog("Enter an radius");  //輸入各項資料
		radius = Integer.parseInt(input1);
		String input2 = JOptionPane.showInputDialog("Enter an X");
		X = Integer.parseInt(input2);
		String input3 = JOptionPane.showInputDialog("Enter an Y");
		Y = Integer.parseInt(input3);
		
		d = WithinCircle(X , Y);                                         // 計算座標離圓點的距離
		
		
		if (radius > d)												     // 判斷座標是否在圓內
			JOptionPane.showMessageDialog(null,"您輸入的座標在圓內","結果",JOptionPane.INFORMATION_MESSAGE);
		else if (radius < d)
			JOptionPane.showMessageDialog(null,"您輸入的座標在圓外","結果",JOptionPane.INFORMATION_MESSAGE);
		else
			JOptionPane.showMessageDialog(null,"您輸入的座標在圓上","結果",JOptionPane.INFORMATION_MESSAGE);
			
	}

}
