package a2.s995002056;											  // Bubble Sort algorithm
import javax.swing.JOptionPane;                                   //import
 
public class A21 {            
	
	public static int[] BubbleSort(int [] array , int b){     		  // Bubble sort mathod
		int a;
		
		if(array[b] > array[b + 1]){								  // 執行一次 只換一次
			a = array[b];
			array[b] = array[b+1];
			array[b+1] = a; 
		}		
		return array;                                                 // 回傳array
	}
	
	public static void main(String [] argv){        				  // 主程式 
	
	JOptionPane.showMessageDialog(null,"Bubble sort","Welcome",JOptionPane.INFORMATION_MESSAGE);	
																	  // 將10個數字儲存在array
	int [] array = new int [10];
	for(int i = 0 ; i < array.length ; i++){
		String input = JOptionPane.showInputDialog("Enter an input");
		array [i] = Integer.parseInt(input);
	}
	
	String [] out = new String[10] ;	
	
	for(int k = 0 ; k < 9 ; k++){                 				      // 將最大→第二大...一次移到後面
		
		for(int i = 0 ; i < 9 ; i++){            				      // 執行10次bubble sort,將最大移至後面 
			array = BubbleSort(array , i);                            // 將 mathod return 的 array , 輸入到 main 的 array
	}
	
		for(int i = 0 ; i < 10 ; i++)								  // 把 array 轉存到 string 才可以顯示
			out[i] = Integer.toString(array[i]);                      // int to string
	
		JOptionPane.showMessageDialog(null,out,"phase",JOptionPane.INFORMATION_MESSAGE); // 結果輸出
	
		System.out.println("");                          			  // 換行
	
	}
	}
}
