package a2.s100502012;

import java.util.Scanner;
import javax.swing.JOptionPane;
import java.lang.Math;

public class A22 {

public static void main(String[] args){
	String number1=JOptionPane.showInputDialog("Key in a random number as a radius.");//key in radius
    double radius=Integer.parseInt(number1);
	String number2=JOptionPane.showInputDialog("Key in X");//key in the x of (x,y)
	double x=Integer.parseInt(number2);
	String number3=JOptionPane.showInputDialog("Key in Y");//key in the y of (x,y)
	double y=Integer.parseInt(number3);
	
	double distance=Math.sqrt(x*x+y*y);
	if (distance>radius){
		JOptionPane.showMessageDialog(null,"The point ( "+x+" , "+y+" ) is out of the circle.");
	}
	else if(distance==radius){
		JOptionPane.showMessageDialog(null,"The point ( "+x+" , "+y+" ) is on the circle exactly.");
	}
	else{
		JOptionPane.showMessageDialog(null,"The point ( "+x+" , "+y+ ") is in the circle.");
	}	
}
}
