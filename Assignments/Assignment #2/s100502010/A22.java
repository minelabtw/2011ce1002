package a2.s100502010;
import javax.swing.JOptionPane;
import java.lang.Math;
public class A22 
{
	public static void main(String[] args)
	{
		String radiusstring = JOptionPane.showInputDialog(null,"enter the radius of the circler","judge the distance",JOptionPane.QUESTION_MESSAGE);
		double radius = Double.parseDouble(radiusstring);//get the radius of the circle and turn the string into double
		
		String x = JOptionPane.showInputDialog(null,"enter the x coordinate","X",JOptionPane.QUESTION_MESSAGE);
		double xcor = Double.parseDouble(x);//get the x coordinate and turn the string into double
		
		String y = JOptionPane.showInputDialog(null,"enter the y coordinate","Y",JOptionPane.QUESTION_MESSAGE);
		double ycor = Double.parseDouble(y);//get the y coordinate and turn the string into double
		

		double answer=Math.sqrt(xcor*xcor+ycor*ycor);//method which could do radical expression calculation
		
		if(answer==radius)//if radius is equal to distance 
		{
			JOptionPane.showMessageDialog(null,"the point is one the circle");
			
		}
		else if(answer<radius)//if radius is bigger than distance 
		{
			JOptionPane.showMessageDialog(null,"the point is inside of the circle");
		}
		else if(answer>radius)//if radius is smaller than distance 
		{
			JOptionPane.showMessageDialog(null,"the point is outside of the circle");
		}
	}
}
