package a2.s100502010;

import javax.swing.JOptionPane;

public class A21 
{
	public static void main(String[] args)
	{//先收十個數字 
		String input1 = JOptionPane.showInputDialog(null,"enter the first number","bubble sort",JOptionPane.QUESTION_MESSAGE);
		String input2 = JOptionPane.showInputDialog(null,"enter the second number","bubble sort",JOptionPane.QUESTION_MESSAGE);
		String input3 = JOptionPane.showInputDialog(null,"enter the third number","bubble sort",JOptionPane.QUESTION_MESSAGE);
		String input4 = JOptionPane.showInputDialog(null,"enter the fourth number","bubble sort",JOptionPane.QUESTION_MESSAGE);
		String input5 = JOptionPane.showInputDialog(null,"enter the fifth number","bubble sort",JOptionPane.QUESTION_MESSAGE);
		String input6 = JOptionPane.showInputDialog(null,"enter the sixth number","bubble sort",JOptionPane.QUESTION_MESSAGE);
		String input7 = JOptionPane.showInputDialog(null,"enter the seventh number","bubble sort",JOptionPane.QUESTION_MESSAGE);
		String input8 = JOptionPane.showInputDialog(null,"enter the eighth number","bubble sort",JOptionPane.QUESTION_MESSAGE);
		String input9 = JOptionPane.showInputDialog(null,"enter the nineth number","bubble sort",JOptionPane.QUESTION_MESSAGE);
		String input10 = JOptionPane.showInputDialog(null,"enter the tenth number","bubble sort",JOptionPane.QUESTION_MESSAGE);
		//將十個數字由string轉換成為integer
		int num1 = Integer.parseInt(input1);
		int num2 = Integer.parseInt(input2);
		int num3 = Integer.parseInt(input3);
		int num4 = Integer.parseInt(input4);
		int num5 = Integer.parseInt(input5);
		int num6 = Integer.parseInt(input6);
		int num7 = Integer.parseInt(input7);
		int num8 = Integer.parseInt(input8);
		int num9 = Integer.parseInt(input9);
		int num10 = Integer.parseInt(input10);
		
		int []numbers = new int[10];//將數字存進array
		numbers[0]=num1;
		numbers[1]=num2;
		numbers[2]=num3;
		numbers[3]=num4;
		numbers[4]=num5;
		numbers[5]=num6;
		numbers[6]=num7;
		numbers[7]=num8;
		numbers[8]=num9;
		numbers[9]=num10;
		
		String answer="";//用來輸出答案的空白string
		
		for(int a=0;a<10;a++)
		{
			int exit=0;
			for(int counter1=0;counter1<9;counter1++)//掃過array中的數字 看是否需要swap
			{
				if(numbers[counter1]>numbers[counter1+1])
				{
					Swap(counter1,counter1+1,numbers);
					exit++;
				}
			}
			if(exit==0)//若是提前結束 即在那個phase中不需要swap 就結束
			{
				break;
			}
			for(int counter=0;counter<10;counter++)
			{
				answer=answer+numbers[counter]+" ";
			}
			answer=answer+"\n";
		}
		JOptionPane.showMessageDialog(null,answer);//最後只需要輸出用來顯示答案的string
		
	}
	public static void Swap(int a,int b,int[]numbers) //swap的method
	{
		int temp=0;
		temp=numbers[a];
		numbers[a]=numbers[b];
		numbers[b]=temp;
	}

}
