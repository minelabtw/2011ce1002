/**
 * CE1002-100502514 FlashTeens Chiang
 * This is a class for bigger text label.
 */
package a2.s100502514;

import java.awt.*;
import javax.swing.*;

//A New Label Class for Bigger Text Style
public class MyBigLabel extends JLabel{
	public MyBigLabel(String txt){
		super(txt);
		setFont(new Font("Arial Black",0,20));
		setHorizontalAlignment(JLabel.CENTER);
		setVerticalAlignment(JLabel.CENTER);
	}
	public MyBigLabel(){
		//Allows default constructor with no strings
		this("");
	}
}
