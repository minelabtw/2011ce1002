/**
 * CE1002-100502514 FlashTeens Chiang
 * [The Following Rules are from ce1002 website]
 * Assignment 2-2

	Please write a Java program and meet the following requirements:
	
	1. Let user input a integer as radius.
	2. Then, let user input a set of two-dimensional coordinate X and Y.
	3. Check whether the coordinate is within the circle, whose radius is user input in requirement 1. Hint: import java.lang.math, and use Math.sqrt(double a) method
	4. Please implement an easy GUI.
 */
package a2.s100502514;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class A22 extends MyBaseWindow {
	
	/** Declare a label for showing message */
	JLabel message_label = new JLabel("Please press the \"Input\" button for input.");
	
	/** Declare a drawing pane that shows circle and coordinate */
	A22_EquationPane drawingPane = new A22_EquationPane();
	
	/** Constructor to Initialize Window */
	public A22(){
		/** Initializing Window */
		initialize("Coordinate Checker by 100502514 on Mar 2 2012", 500, 350);
		
		/** Initializing Input Area */
		JPanel inputArea = new JPanel();
		//Add Message
		inputArea.add(message_label,BorderLayout.CENTER);
		
		//Add "Input" button
		JButton input_btn = new JButton("Input");
		input_btn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				onClickInput();
			}
		});
		JPanel myFlowPanel = new JPanel(new FlowLayout());
		myFlowPanel.add(input_btn);
		inputArea.add(myFlowPanel,BorderLayout.EAST);
		
		//Finally add Input area to window
		add(inputArea, BorderLayout.NORTH);
		
		//Then add an area for painting circle and point
		add(drawingPane, BorderLayout.CENTER);
	}
	
	/** This method will run when clicked on "Input" button */
	public void onClickInput(){
		//Declare a temporary input buffer to determine "invalid value" or "cancel pressed"
		String tempInput="";
		try{
			//Input
			int r = Integer.parseInt(tempInput = JOptionPane.showInputDialog("Please input the circle RADIUS as Integer:"), 10);
			if(r<0){
				//Throw an error that indicates radius cannot be negative
				/** This is the only usage of A22_Negative_Exception.java */
				throw new A22_Negative_Exception();
			}
			double x = Double.parseDouble(tempInput = JOptionPane.showInputDialog("Please input the X position of coordinate as Real Number:"));
			double y = Double.parseDouble(tempInput = JOptionPane.showInputDialog("Please input the Y position of coordinate as Real Number:"));
			
			//Calculation
			double distance = Math.sqrt(x*x+y*y);
			
			//Messages
			if(r==0&&distance==0){
				JOptionPane.showMessageDialog(this, "Coordinate ("+x+", "+y+") is AT the \"circle\" with no radius!!\n" +
						"The \"circle\" won't be shown.");
			}else if(r==0){
				JOptionPane.showMessageDialog(this, "Coordinate ("+x+", "+y+") is NOT AT the \"circle\" with no radius!!\n" +
						"The \"circle\" won't be shown.");
			}else if(distance>r){
				JOptionPane.showMessageDialog(this, "Coordinate ("+x+", "+y+") is OUTSIDE the circle with radius="+r+"!!");
			}else if(distance<r){
				JOptionPane.showMessageDialog(this, "Coordinate ("+x+", "+y+") is INSIDE the circle with radius="+r+"!!");
			}else{
				JOptionPane.showMessageDialog(this, "Coordinate ("+x+", "+y+") is EXACTLY ON the circle with radius="+r+"!!");
			}
			
			//Draw the pane at last
			//Window's Y-axis direction is opposite from Cartesian Y-axis
			drawingPane.initialize(r, x, -y);
			
		}catch(A22_Negative_Exception negError){
			if(tempInput!=null){
				//If input invalid value and press OK
				JOptionPane.showMessageDialog(this, "The Radius of Circle must be NON-NEGATIVE!!!!");
				drawingPane.initialize(0, 0, 0);
			}
			//If press Cancel then ignore and exit the dialog
		}catch(Exception numError){
			if(tempInput!=null){
				//If input invalid value and press OK
				JOptionPane.showMessageDialog(this, "Invalid value!!");
				drawingPane.initialize(0, 0, 0);
			}
			//If press Cancel then ignore and exit the dialog
		}
	}
	
	/** Main Function will call the Constructor */
	public static void main(String args[]){
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				A22 myWindow = new A22();
				//Show the application window
				myWindow.setVisible(true);
			}
		});
	}
}
