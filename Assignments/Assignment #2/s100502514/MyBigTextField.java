/**
 * CE1002-100502514 FlashTeens Chiang
 * This is a class for text field with bigger text.
 */
package a2.s100502514;

import java.awt.*;
import javax.swing.*;

//A New Text Field Class for Bigger Text Style
public class MyBigTextField extends JTextField{
	public MyBigTextField(String txt){
		super(txt);
		setFont(new Font("Arial Black",0,20));
		setHorizontalAlignment(JLabel.RIGHT);
	}
	public MyBigTextField(){
		//Allows default constructor with no strings
		this("");
	}
}
