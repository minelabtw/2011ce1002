/**
 * CE1002-100502514 FlashTeens Chiang
 * [The Following Rules are from ce1002 website]
 * Assignment 2-1

	Please write a Java program and meet the following requirements:
	
	1. Let user input 10 integer numbers, and store the numbers in a one-dimensional array.
	2. Use Bubble Sort algorithm to sort the numbers with Swap(int a, int b, int[] array) method. Hint: CH6, p236
	3. Please show every phase in Bubble Sort algorithm.
	4. Please implement an easy GUI. Hint: CH1~CH3.
 */
package a2.s100502514;

import java.awt.*;
import java.awt.event.*;
import java.text.*;

import javax.swing.*;

public class A21 extends MyBaseWindow {
	
	/** Constant for array length */
	public int ARRAY_LEN = 10;
	
	/** Declare an input area with 10 text fields */
	public JPanel inputArea;
	
	/** Declare a ScrollPane to contain each steps in Bubble Sort */
	public ScrollPane displayPane;
	JPanel displayArea;
	
	/**Declare an array for input*/
	public int[] arr = new int[10];
	
	/** Constructor to Initialize Window */
	public A21(){
		/**Initializing Window*/
		initialize("Bubble Sort Test by 100502514 on Mar 2 2012", 500, 350);
		
		/**Set Main Layout*/
		inputArea = new JPanel();
		inputArea.setLayout(new GridLayout(1,ARRAY_LEN));
		add(inputArea,BorderLayout.NORTH);
		for(int i=0;i<ARRAY_LEN;i++){
			//Restrict that user can only input number in the text field.
			JFormattedTextField myTxt = new JFormattedTextField(NumberFormat.getIntegerInstance());
			myTxt.setHorizontalAlignment(JLabel.CENTER);
			
			
			inputArea.add(myTxt);
			myTxt.addKeyListener(new KeyListener(){
				public void keyPressed(KeyEvent keyEvent){
					if(keyEvent.getKeyCode()==KeyEvent.VK_ENTER || keyEvent.getKeyCode()==KeyEvent.VK_SPACE){
						
						for(int k=0;k<inputArea.getComponentCount();k++){
							//Find current event target and focus to next text field
							if(keyEvent.getSource()==inputArea.getComponent(k)){
								if(k==inputArea.getComponentCount()-1){
									//If current input is the last one
									//cancel text focus
									inputArea.requestFocus();
									//Copy input values into array
									for(int c=0;c<=k;c++){
										try{
											//Get the content in current text field
											//With stripping the auto-generated commas away.
											arr[c] = Integer.parseInt(((JTextField)inputArea.getComponent(c)).getText().replace(",", ""));
										}catch(Exception err){
											//This scope will run if found invalid value.
											showAlert("The value No."+(c+1)+" is invalid!!");
											//Focus will be changed to the first invalid input.
											inputArea.getComponent(c).requestFocusInWindow();
											return;
										}
									}
									//Sort and show steps
									bubbleSort(arr);
									//Ask User if he/she would reset the text
									if(showConfirm("Bubble-Sorting has Completed!!\n" +
											"Would you like to reset the input?")){
										//If pressed "Yes", reset the inputs.
										for(Component i:inputArea.getComponents()){
											try{
												//Reset all input texts in the input area.
												((JTextField)(i)).setText("");
											}catch(Exception unexpected){
												//Ignore when chosen a non-text component.
											}
										}
										//Then focus will be the 1st input.
										inputArea.getComponent(0).requestFocusInWindow();
									}
								}else{
									//Otherwise
									//Strip all spaces in the current input
									String stripped_str = ((JTextField)keyEvent.getSource()).getText().replace(" ", "");
									((JTextField)keyEvent.getSource()).setText(stripped_str);
									//focus to next input
									inputArea.getComponent(k+1).requestFocusInWindow();
								}
							}
						}
					}
				}
				public void keyReleased(KeyEvent keyEvent){
					//nothing, but required for the compiler
				}
				public void keyTyped(KeyEvent keyEvent){
					//nothing, but required for the compiler
				}
			});
		}
		displayPane = new ScrollPane();
		displayArea = new JPanel();
		displayArea.setLayout(new GridLayout(10,1));
		displayPane.add(displayArea);
		add(displayPane,BorderLayout.CENTER);

	}
	
	//Show Message Alert
	public void showAlert(String message){
		JOptionPane.showMessageDialog(this, message);
	}
	
	//Show Confirm Dialog with title "Assignment 2-1"
	public boolean showConfirm(String message){
		int confirm = JOptionPane.showConfirmDialog(this, message, "Assignment 2-1", 0);
		//Return true if user select "Yes", otherwise return false.
		return confirm==JOptionPane.YES_OPTION;
	}
	
	/** Draw a new row with 2 marked numbers */
	public void drawNewRow(int[] arr, int indexA, int indexB, boolean highlight){
		JPanel row = new JPanel();
		row.setLayout(new GridLayout(1,arr.length));
		if(highlight){
			//Set the certain row to yellow if marked
			row.setBackground(new Color(255,240,153));
		}
		for(int i=0;i<arr.length;i++){
			JLabel myTxt = new JLabel();
			myTxt.setText(arr[i]+"");
			//Determine if the index is marked.
			if(i==indexA){
				//First index will be painted red
				myTxt.setForeground(new Color(224,0,51));
			}else if(i==indexB){
				//Second index will be painted purple
				myTxt.setForeground(new Color(204,0,224));
			}
			myTxt.setHorizontalAlignment(JLabel.CENTER);
			row.add(myTxt);
		}
		int num_comp = displayArea.getComponentCount();
		if(num_comp>10){
			//Resize the layout to contain a new row
			displayArea.setLayout(new GridLayout(num_comp+1,1));
		}
		displayArea.add(row);
	}
	
	/** Draw a new row without any marked numbers */
	public void drawNewRow(int[] arr, boolean highlight){
		drawNewRow(arr,-1,-1,highlight);
	}
	
	/** Clear All Rows of Sorting Steps */
	public void clearAllRows(){
		displayArea = new JPanel();
		displayArea.setLayout(new GridLayout(10,1));
		displayPane.add(displayArea);
		add(displayPane,BorderLayout.CENTER);
	}
	
	/** Bubble Sort and Show Steps */
	public void bubbleSort(int[] arr){
		//clear the list
		clearAllRows();
		//Declare a boolean to determine if a phase works
		boolean exchanged=false;
		do{
			exchanged=false;
			//show the beginning for each phase. (with highlight)
			drawNewRow(arr,true);
			//make an empty row to separate phase title and steps.
			drawNewRow(new int[0],false);
			
			for(int i=0;i<arr.length-1;i++){
				//Exchange if necessary
				if(arr[i]>arr[i+1]){
					//Show the state before changing
					drawNewRow(arr,i,i+1,false);
					
					//Exchange values
					exchanged=true;
					int temp = arr[i];
					arr[i] = arr[i+1];
					arr[i+1] = temp;
					
					//Show the state after changing
					drawNewRow(arr,i+1,i,false);
					
					//make an empty row to separate different steps.
					drawNewRow(new int[0],false);
				}
			}
		}while(exchanged);
		
		//show the final result. (with highlight)
		drawNewRow(arr,true);
		
		//Refresh Window
		setVisible(true);
	}
	
	/** Main Function will call the Constructor */
	public static void main(String args[]){
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				A21 myWindow = new A21();
				//Show the application window
				myWindow.setVisible(true);
			}
		});
	}
}
