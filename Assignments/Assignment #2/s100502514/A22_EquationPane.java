package a2.s100502514;

import java.awt.*;

import javax.swing.*;

/** Declare another class for equation pane */
public class A22_EquationPane extends JPanel{
	
	private int radius;
	private double coordx, coordy;
	private boolean equa_valid=false;
	
	/** Constructor with default initialization */
	public A22_EquationPane(){
		initialize(0,0,0);
		equa_valid=false;
	}
	
	/** Constructor with specified initialization */
	public A22_EquationPane(int r, double x, double y){
		initialize(r, x, y);
	}
	
	/** Initialization method */
	public void initialize(int r, double x, double y){
		radius=r;
		coordx=x;
		coordy=y;
		if(r>0)equa_valid=true;
		else equa_valid=false;
		repaint();
	}
	
	/** The method to paint this component (Overriden the default method) */
	protected void paintComponent(Graphics g){
		//Call the original painting method
		super.paintComponent(g);
		
		setBackground(new Color(255,240,224));
		
		/** TODO To be Continued... */
		if(equa_valid){
			//Determine the circle size
			int CIRCLE_SIZE = 100;
			//Draw the circle in the display
			g.setColor(new Color(255,102,51));
			g.drawOval(getWidth()/2-CIRCLE_SIZE, getHeight()/2-CIRCLE_SIZE, CIRCLE_SIZE*2, CIRCLE_SIZE*2);
			
			//Determine the point's relative position to circle
			int _x = getWidth()/2 + (int)(CIRCLE_SIZE*coordx/radius);
			int _y = getHeight()/2 + (int)(CIRCLE_SIZE*coordy/radius);
			
			//Draw the point in the display if the radius is valid
			if(radius>0){
				g.setColor(new Color(204,0,51));
				g.fillOval(_x-2, _y-2, 4, 4);
			}
			
			//Show the text message if the point is out of window
			if(_x<0||_x>getWidth()||_y<0||_y>getHeight()){
				g.setColor(new Color(255,204,102));
				g.setFont(new Font("Arial Black",0,20));
				g.drawString("The Coordinate is TOO FAR from here!!", getWidth()/2-200, getHeight()/2-5);
			}
		}
	}
	
}
