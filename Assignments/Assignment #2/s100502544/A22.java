package a2.s100502544;
import javax.swing.JOptionPane;
public class A22 {
	public static double sqrt(double num){//顯示(x,y)到圓點(0,0)的距離
		
		JOptionPane.showMessageDialog(null,"(x,y)到原點的距離= "+Math.sqrt(num));
		return Math.sqrt(num);//傳回距離值
	}
	public static void main(String[] args){
		double[][] two=new double [1][2];//宣告一個二維陣列two  大小為[1][2]
		String title=JOptionPane.showInputDialog("輸入radius: ");//GUI的radius 輸入
		double radius=Double.parseDouble(title);//GUI的string title轉為double值為radius
		JOptionPane.showMessageDialog(null,"圓的半徑長= "+radius);//show GUI的輸出
		String x=JOptionPane.showInputDialog("x=");//GUI的x 輸入
		two[0][0]=Double.parseDouble(x);
		String y=JOptionPane.showInputDialog("y=");//GUI的y 輸入
		two[0][1]=Double.parseDouble(y);
		double a=two[0][0];
		double b=two[0][1];
		double c=Math.pow(a,2)+Math.pow(b,2);//用Math.pow算a b值
		double result=sqrt(c);
		if(result==radius){//判斷result和radius關係
			JOptionPane.showMessageDialog(null,"在圓上!!");
		}
		else if(result>radius){
			JOptionPane.showMessageDialog(null,"在圓外!!");
		}
		else{
			JOptionPane.showMessageDialog(null,"在圓內!!");
		}
	}
}
