package a2.s100502519;

import java.lang.Math;
import javax.swing.JOptionPane;

public class A22 {
	public static void main (String[]args){

		double distance = 0;			// point等於點到原點的距離

		String question1 = JOptionPane.showInputDialog("Please enter an integer as radius: ");			//顯示問題     要求輸入半徑
		double radius = Double.parseDouble(question1);			//輸入半徑

		if(radius>0){
			String question3 = JOptionPane.showInputDialog("X=");
			double x = Double.parseDouble(question3);			//輸入X座標

			String question4 = JOptionPane.showInputDialog("Y=");
			double y = Double.parseDouble(question4);			//輸入Y座標

			distance = Math.sqrt(x*x+y*y);			//點到原點的距離
			Where(radius,distance);			//call method   傳入半徑和距離
		}
		
		else{
			String warning = "Error! radius should > 0\n";		//若半徑沒>0  則顯示錯誤訊息
			JOptionPane.showMessageDialog(null,warning);
		}
	}
	
	
	
	public static void Where(double rad,double dis){
		
		if(dis<rad){
				String answer1 = "The point is IN the circle!!\n";			//若距離小於半徑    則在圓內
				JOptionPane.showMessageDialog(null,answer1);
		}
		else if(dis==rad){
			String answer2 = "The point is ON the circle!!\n";			//若距離等於半徑   則在圓上
			JOptionPane.showMessageDialog(null,answer2);
		}
		else{
			String answer3 = "The point is OUT OF the circle!!\n";			//若距離大於半徑    則在圓外
			JOptionPane.showMessageDialog(null,answer3);
		}
	}
}