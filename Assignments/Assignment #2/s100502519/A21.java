package a2.s100502519;

import javax.swing.JOptionPane;

public class A21 {
	public static void main (String[]args){
		
		boolean finish = false;			//用來判斷sort結束了嗎
		int [] tennumbers = new int [10];			//宣告一個大小是10的陣列
		String lines = "";			//用來存數字  成一整個過程

		for(int i=0 ; i<10 ; i++){			//使陣列一項一項跑
			String question = JOptionPane.showInputDialog("Enter number " + (i+1));			//顯示問題-要求輸入第i個數
			tennumbers[i] = Integer.parseInt(question);			//輸入至陣列
		}
		
		while(finish == false){
			
			finish = true;			//先假設這次會結束   假如沒進到下面的if(即沒換了)   就真的會出去
			
			for(int i=0;i<9;i++){
				if(tennumbers[i] > tennumbers[i+1]){			/*假如前項比後項大 就叫method來swap*/
					Swap(i,i+1,tennumbers);			//call method 傳入   前項  後項  陣列
					finish = false;			//因為有換   所以把結束設為false
				}
			}
			
			for(int j=0;j<10;j++){
				lines = lines + tennumbers[j] + " ";		//把每個數一一存入string
			}
			
			lines = lines + "\n";			//存一列之後換行
			
		}
		
		String output = lines;
		JOptionPane.showMessageDialog(null , output);			//顯示出完整的過程
		
	}
	
	
	
	public static void Swap(int a,int b,int[] tennums){			/*接收   前項  後項  和陣列          b即a+1 */
		int temp = tennums[a];
		tennums[a] = tennums[b];
		tennums[b] = temp;			/*轉換法*/
	}
}
