package a2.s100502031;


import java.util.Scanner;
import javax.swing.JOptionPane;

public class A22 {
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);
		
		String temp=JOptionPane.showInputDialog("Please enter an integer as the radus of circle");
		//input the radius
		int radius=Integer.parseInt(temp);
		
		System.out.println("Please input two integers as a point");
		//state an array to store the coordinate X and Y
		double point[]=new double[2];
		
		//input the coordinate x
		String temp1=JOptionPane.showInputDialog("Please enter a number for x");
		point[0]=Double.parseDouble(temp1);
		
		//input the coordinate y
		String temp2=JOptionPane.showInputDialog("Please enter a number for y");
		point[1]=Double.parseDouble(temp2);
		
		//caculate the distance
		double Distance=distance(point[0],point[1]);
		
		if(radius>Distance){
			String output1="The coordinate is within the circle";
			JOptionPane.showMessageDialog(null, output1);
		}
		else if(radius==Distance){
			String output2="The coordinate is right on the circle";
			JOptionPane.showMessageDialog(null, output2);
		}
		else{
			String output3="The coordinate is out of the circle";
			JOptionPane.showMessageDialog(null, output3);
		}
		
			
		
}
		
			
//use this method to caculate the distance and return the result		
public static double distance(double x,double y){
	return Math.sqrt((x*x)+(y*y));
	}
}
