package a2.s100502027;

import java.awt.Button;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

class swapwindow extends Frame implements ActionListener {  //To extend frame,comfortable to control the surface and use
	TextField[] numbers=new TextField[10];
	Label[] messages=new Label[11];
	Button order1;
	public swapwindow(){
		Label tip = new Label("Please enter 10 numbers into the under fields :");
		tip.setBounds(150,30,260,20);
		this.add(tip);
		for(int t=0;t<10;t++){ //use [for] to set the array's seat and content
			numbers[t]= new TextField("");
			numbers[t].setBounds(10+53*t,55,50,20);
			messages[t]=new Label("");
			messages[t].setBounds(130,120+20*t,300,20);
			}
		for(int t=0;t<10;t++){
			this.add(numbers[t]);
			this.add(messages[t]);
		}
		order1 = new Button("START!"); 
		order1.setBounds(240,80,70,20);
		this.add(order1);
		order1.addActionListener(this);//to contact under method to use button
	}
	public void actionPerformed(ActionEvent e) {  //when button be chicken,start to run to swap
		int switchwhile=1,times=0;
		int[] swapnumbers=new int[10];
		for(int t=0;t<10;t++)
		{
			swapnumbers[t]=Integer.parseInt(numbers[t].getText());
		}
		while(switchwhile!=0){
			switchwhile=0; //to control [while]
			for(int sit=0;sit<9;sit++)
			{
				if(swapnumbers[sit]>swapnumbers[sit+1]){
					Swap(sit,sit+1,swapnumbers);
					switchwhile++;
				}
			}
			if(switchwhile!=0)
			{
				messages[times].setText(result(swapnumbers));
			}
			else{
				messages[times].setText("-end-");
				switchwhile=0;
			}
			times++;//to control Label[]
		}
	}
	public void Swap(int a, int b, int[] array){  //bubble sort,catch the array to become the seat of count
		int hold=array[a];
		array[a]=array[b];
		array[b]=hold;
	}
	public String result(int[] array)   //let the count of the array  turn to the string ,string used to display result
	{
		return (array[0]+"--"+array[1]+"--"+array[2]+"--"+array[3]+"--"+array[4]+"--"+array[5]+"--"+array[6]+"--"+array[7]+"--"+array[8]+"--"+array[9]);
	}
}
public class A21 extends WindowAdapter{   //set the frame.title.size ant the layout , 
	public static void main(String [] args){
		swapwindow frame = new swapwindow();
		A21 layoutuse = new A21();
		frame.setTitle("Bubble Sort");
		frame.setLayout(null);
		frame.setSize(550,500);
		frame.setVisible(true);
		frame.setLocation(300,300);
		frame.setBackground(Color.yellow);
	    frame.addWindowListener(layoutuse);
	}
	public void windowClosing(WindowEvent e){  //let the window button[X] have the work to close window
		System.exit(0);
	}
}
