package a2.s100502027;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

class circle extends Frame implements ActionListener {  //To extend frame,comfortable to control the surface and use
	TextField radious,coordinatex,coordinatey;
	Label radiousMes,xMes,yMes,result;
	Button order1;
	public circle(){
		radiousMes = new Label("Please input a integer as radius :");
		xMes = new Label("Please input coordinate-X :");
		yMes = new Label("Please input coordinate-Y :");
		result= new Label("result :         ");
		order1 = new Button("OK!");
		radious= new TextField(7);
		coordinatex= new TextField(7);
		coordinatey= new TextField(7);
		radiousMes.setBounds(20,40,200,20);
		xMes.setBounds(20,60,170,20);
		yMes.setBounds(20,80,170,20);
		result.setBounds(80,140,150,20);
		order1.setBounds(120,110,50,20);
		radious.setBounds(230,40,50,20);
		coordinatex.setBounds(230,60,50,20);
		coordinatey.setBounds(230,80,50,20);
		this.add(radiousMes);
		this.add(xMes);
		this.add(yMes);
		this.add(result);
		this.add(order1);
		this.add(radious);
		this.add(coordinatex);
		this.add(coordinatey);
		order1.addActionListener(this);//to contact under method to use button
	}
	public void actionPerformed(ActionEvent e) {  //when button be chicken,start to run to swap
		double countx=Double.parseDouble(coordinatex.getText()); //to read the number user input
		double county=Double.parseDouble(coordinatey.getText());
		double countr=Double.parseDouble(radious.getText());
		double sqrtuse = countx * countx + county * county;
		double countresult=Math.sqrt(sqrtuse); //to use the method to get the Square root of X^2+y^2 
		if(countresult>countr){
			result.setText("result : out the circle");
		}
		else if(countresult==countr){
			result.setText("result : on the circle line");
		}
		else{
			result.setText("result : in the circle");
		}
	}
	
}
public class A22 extends WindowAdapter{     //set the frame.title.size ant the layout 
	public static void main(String[] args){
		circle Frame = new circle();
		A22 layoutuse = new A22();
		Frame.setTitle("Coordinate Check");
		Frame.setLayout(null);
		Frame.setSize(300,200);
		Frame.setVisible(true);
		Frame.setLocation(300,300);
		Frame.setBackground(Color.yellow);
	    Frame.addWindowListener(layoutuse);
	}
	public void windowClosing(WindowEvent e){  //let the window button[X] have the work to close window
		System.exit(0);
	}
}
