package a2.s100502034;

import java.lang.Math;//這是題示給的
import javax.swing.JOptionPane;
public class A22 {
	public static double XYtoO(double a, double b){ //計算點的圓心距離的method (本來想寫在main裡面的 不知道為甚麼突然有個想法想寫在method裡
		return Math.sqrt(a*a + b*b);//java好像沒有^2這個東西 (然後pow之類的懶得去查了 應該很快就要知道了
	}
	public static void main(String[] args){
		double raduis = 1;//初始值設在合理的範圍裡
		String xString = "";//X坐標
		String yString = "";//Y坐標
		String rString = JOptionPane.showInputDialog("please input the raduis of a circle(you can enter 0 to exit)");//半徑
		raduis = Math.abs(Double.parseDouble(rString));//給半徑絕對值
		while ( raduis != 0){
			xString = JOptionPane.showInputDialog("enter the x of two-dimensional coordinate(point p(x,y))");//X坐標的輸入
			double x = Double.parseDouble(xString);//換Double
			yString = JOptionPane.showInputDialog("enter the y of two-dimensional coordinate(point p(x,y))");//Y坐標的輸入
			double y = Double.parseDouble(yString);//換Double
			if (XYtoO(x, y) > raduis){//判斷是不是在圓外
				JOptionPane.showMessageDialog(null,"point p is outside the circle");
			}	
			else if (XYtoO(x,y) == raduis){//判斷是不是在圓上
				JOptionPane.showMessageDialog(null,"point p is on the circle");
			}
			else{//剩下就在圈內
				JOptionPane.showMessageDialog(null,"point p is inside the circle");
			}
			rString = JOptionPane.showInputDialog("please input the raduis of a circle(you can enter 0 to exit)");
			raduis = Math.abs(Double.parseDouble(rString));//因為想要給使用者在這邊輸入0就離開了 所以有兩行半徑的輸入
		}
	}
}