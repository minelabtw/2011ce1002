package a2.s100502023;

import javax.swing.JOptionPane;

public class A21 
{

	public static void main(String[] args) 
	{
		int [] numbers= new int[10];  //numbers[] 用來儲存使用者輸入的數字
				
		for(int i=0;i<10;i++)
		{
			String input = JOptionPane.showInputDialog(null,"Enter an input------剩餘:" + (10-i) + "個數字","Input 10 integer numbers,Bubble Sort algorithm to sort the numbers",JOptionPane.QUESTION_MESSAGE); //gui
			numbers[i]=Integer.parseInt(input);	 //將字串型態的數字轉換為整數數字型態		
		}
		
		//Use Bubble Sort algorithm to sort the numbers
		boolean change=true;  //用來判斷是否結束Bubble Sort
			
		while(change)  //當change=true，繼續Bubble Sort
		{	
			storephase(numbers,change); //呼叫;將每一phase存成string型態
			
			change=false;  //初始，未交換
			
			for (int i=0;i<9;i++)
			{
				if (numbers[i]>numbers[i+1])  //交換
				{
					Swap(i,i+1,numbers);
					change=true;  //有進行交換，change=true，會再次進入while迴圈	
				}
			}
			
			
		}
		//當change=false,結束Bubble Sort
		storephase(numbers,change);  //輸出最後結果
		showphaseofBubbleSort();  //show every phase in Bubble Sort algorithm
	}
	
	public static void Swap(int a,int b,int[] numbers)  //Use Bubble Sort algorithm to sort the numbers
	{
		int tmp=numbers[a];  //暫存數字，用於交換
		numbers[a]=numbers[b];
		numbers[b]=tmp;	
	}
	
	public static String storephaseAsString=" Your input : ";  //宣告storephaseAsString 為  public static String；以字串的方式儲存phase
	
	public static void storephase(int[] numbers,boolean change)  //將每一phase存成string型態，若change=false，也就是Bubble Sort結束時，輸出The Result
	{		
		if (change==false)
		{
			storephaseAsString+="The result : "+numbers[0]+"	"+numbers[1]+"	"+numbers[2]+"	"+numbers[3]+"	"+numbers[4]+"	"+numbers[5]+"	"+numbers[6]+"	"+numbers[7]+"	"+numbers[8]+"	"+numbers[9]+"\n";
		}
		else
		{
			storephaseAsString+=numbers[0]+"	"+numbers[1]+"	"+numbers[2]+"	"+numbers[3]+"	"+numbers[4]+"	"+numbers[5]+"	"+numbers[6]+"	"+numbers[7]+"	"+numbers[8]+"	"+numbers[9]+"\n";
		}
	}
	
	public static void showphaseofBubbleSort()  //show every phase in Bubble Sort algorithm.
	{		
		JOptionPane.showMessageDialog(null,storephaseAsString,"show every phase",JOptionPane.INFORMATION_MESSAGE);  //GUI			
	}

}
