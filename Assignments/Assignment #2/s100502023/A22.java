package a2.s100502023;

import java.lang.Math;
import javax.swing.JOptionPane;

public class A22 
{

	public static void main(String[] args)
	{
		String input = JOptionPane.showInputDialog(null,"Enter a radius","Input a integer as radius",JOptionPane.QUESTION_MESSAGE); //gui
		int radius=Integer.parseInt(input);  //將字串型態的數字轉換為整數數字型態
		
		input = JOptionPane.showInputDialog(null,"x:","Input a set of two-dimensional coordinate X and Y",JOptionPane.QUESTION_MESSAGE);
		Double pointOfX=Double.parseDouble(input);  //將字串型態的數字轉換為Double數字型態
		
		input = JOptionPane.showInputDialog(null,"y:","Input a set of two-dimensional coordinate X and Y",JOptionPane.QUESTION_MESSAGE);
		Double pointOfY=Double.parseDouble(input);  ////將字串型態的數字轉換為Double數字型態
		
		checkAndShow(radius,pointOfX,pointOfY);  //Check whether the coordinate is within the circle, whose radius is user input

	}
	
	public static void checkAndShow(int radius,double x,double y)  //Check whether the coordinate is within the circle and show
	{
		if (Math.sqrt(Math.pow(x, 2)+Math.pow(y, 2))>radius)  //點到原點的距離大於半徑,圓外
		{
			JOptionPane.showMessageDialog(null,"Point ( "+ x+ "," + y + ") 在圓外 ","The result",JOptionPane.INFORMATION_MESSAGE);
		}
		else if (Math.sqrt(Math.pow(x, 2)+Math.pow(y, 2))==radius)  //點到原點的距離等於半徑,圓上
		{
			JOptionPane.showMessageDialog(null,"Point ( "+ x+ "," + y + ") 在圓上 ","The result",JOptionPane.INFORMATION_MESSAGE);
		}
		else if (Math.sqrt(Math.pow(x, 2)+Math.pow(y, 2))<radius)  //點到原點的距離小於半徑,圓內
		{
			JOptionPane.showMessageDialog(null,"Point ( "+ x+ "," + y + ") 在圓內 ","The result",JOptionPane.INFORMATION_MESSAGE);
		}
	}
}
