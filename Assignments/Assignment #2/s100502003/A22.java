package a2.s100502003;

import javax.swing.JOptionPane; // import it in order to use GUI
import java.lang.Math; // import it in order to use sqrt

public class A22 {
	public static void main(String[] args) {
		// show informations and store the input of the radius
		String radiusInputString = JOptionPane.showInputDialog("Please enter the radius for the circle:");
		double radius = Double.parseDouble(radiusInputString);
		
		double [][] point = new double [1][2]; // declare the two-dimensional array to store the value of x and y
		String xInputString = JOptionPane.showInputDialog("Please enter a number for the x axis: ");
		point [0][0] = Double.parseDouble(xInputString); // input the value of x in the array
		String yInputString = JOptionPane.showInputDialog("Please enter a number for the y axis: ");
		point [0][1] = Double.parseDouble(yInputString); // input the value of y in the array 
		check(point[0][0], point [0][1], radius); // call the method to check the result	
	}
	
	public static void check(double x, double y, double r) { // method that help to check the position of the point
		if(Math.sqrt(x*x+y*y) < r) // inside
			JOptionPane.showMessageDialog(null,"The point is inside of the circle.");
		else if(Math.sqrt(x*x+y*y) > r) // outside
			JOptionPane.showMessageDialog(null,"The point is outside of the circle.");
		else // on the circle
			JOptionPane.showMessageDialog(null,"The point is on the circle.");
	}

}
