package a2.s100502003;

import javax.swing.JOptionPane; //import it in order to use GUI

public class A21 {
	public static void main(String[] args) {
		int[] numbers = new int[10]; // declare an array to store the numbers entered by the user
		JOptionPane.showMessageDialog(null, "You have to enter 10 integers!!"); // show information
		for(int i=0; i<10; i++) { // let the user enter a number for ten times
			String numberInputString = JOptionPane.showInputDialog("Please enter an integer for the order of " + (i+1) + " : ");
			numbers[i] = Integer.parseInt(numberInputString);
		}
		bubbleSort(numbers); // call the method to sort
	}
	
	public static void bubbleSort(int[] origin) {
		boolean stop = false;
		// show the original numbers enter by the user
		System.out.println("Original numbers: ");
		for(int j=0; j<origin.length; j++) {
			System.out.print(origin[j] + " ");
		}
		System.out.print("\n\n");
		// swap the numbers if necessary and break the loop if no more numbers to swap
		for(int k=0; k<origin.length-1; k++) {
			stop = true;
			for(int l=0; l<origin.length-1; l++) {
				if(origin[l] > origin[l+1]) {
					swap(l,l+1,origin);
					stop = false;
				}
			}
			if(stop != true) { // the condition that the numbers haven't been totally sorted 
				System.out.println("Phase " + (k+1) + " : "); // show the result temporarily
				for(int m=0; m<origin.length; m++) {
					System.out.print(origin[m] + " ");
				}
				System.out.print("\n");
			}
			else // the condition that the numbers are totally been sorted
				break;
		}	
	}
	
	public static void swap(int a, int b, int[] array) { // method that help to change numbers
		int temp;	
		temp = array[a];
		array[a] = array[b];
		array[b] = temp;
	}
}
