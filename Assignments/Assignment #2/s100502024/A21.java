package a2.s100502024;
import javax.swing.JOptionPane;
public class A21 
{
	public static void Swap (int a,int b,int[] Array) // 交換 function
	{
		int temp = Array[a];
		Array[a] = Array[b];
		Array[b] = temp;
	}
	public static void main (String[] args)
	{
		int i;
		boolean check = true;
		int array[] = new int[10]; // 宣告大小為10的陣列
		for (i=0;i<10;i++)
		{
			String Number = JOptionPane.showInputDialog("請輸入第 "+(i+1)+"  個數字: "); // 提示輸入
			array[i] = Integer.parseInt(Number); // 將輸入結果存到陣列裡
		}
		while (check == true) 
		{
			check = false; // 初始化是否使用
			for (i=1;i<10;i++)
			{
				if (array[i-1]>array[i])
				{
					Swap(i-1,i,array);
					check = true; // 判斷是否有用到Swap
				}
			}
			if (check == false)
			{
				break;
			}
			String phase = "";
			for (i=0;i<10;i++)
			{
				phase = phase + "  " + Integer.toString(array[i]); // 將陣列轉成整數型態並輸出成一行
			}
			JOptionPane.showMessageDialog(null,phase); // 顯示結果
		}
	}
}