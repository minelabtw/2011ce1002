package a2.s100502024;
import javax.swing.JOptionPane;
import java.lang.Math;
public class A22 
{
	public static double Distance (double X,double Y)
	{
		double distance = Math.sqrt(X*X+Y*Y); // 計算點到原點的距離
		return distance; // 回傳距離的值
	}
	public static void main(String[] args)
	{
		String rstring = JOptionPane.showInputDialog("Please enter the radius: "); // 提示輸入
		double r = Double.parseDouble(rstring); // 將r從String的型態轉為Double
		String xstring = JOptionPane.showInputDialog("Please enter the x (x,y):"); 
		double x = Double.parseDouble(xstring); // 型態轉換
		String ystring = JOptionPane.showInputDialog("Please enter the y (x.y):"); 
		double y = Double.parseDouble(ystring); // 型態轉換
		if (Distance(x,y)>r)
		{
			JOptionPane.showMessageDialog(null,"點在圓外"); // 顯示點的位置
		}
		else if (Distance(x,y)==r)
		{
			JOptionPane.showMessageDialog(null,"點在圓上"); // 顯示點的位置
		}
		else
		{
			JOptionPane.showMessageDialog(null,"點在圓內"); // 顯示點的位置
		}
	}
}
