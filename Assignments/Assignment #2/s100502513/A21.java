package a2.s100502513;

import java.util.Scanner;
import javax.swing.*;

public class A21 {
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		int[] original = new int[10];  //儲存輸入的數字
		String[] ss = new String[100]; //儲存處理過的文字
		System.out.print("請輸入10個數字: ");
		for (int i = 0; i < 10; i++) {
			original[i] = input.nextInt();
		}

		for (int j = 0; j < 100; j++) {  
			String set = (j + 1) + "st Pass\n[ ";
			String set1 = " ] ---> [ ";
			String set2 = " ]";
			String number = original[0] + " " + original[1] + " " + original[2]  //左邊的數字
					+ " " + original[3] + " " + original[4] + " " + original[5]
					+ " " + original[6] + " " + original[7] + " " + original[8]
					+ " " + original[9];
			BubbleSortalgorithm(original);  //泡沫排列
			String number1 = original[0] + " " + original[1] + " "  //右邊的數字
					+ original[2] + " " + original[3] + " " + original[4] + " "
					+ original[5] + " " + original[6] + " " + original[7] + " "
					+ original[8] + " " + original[9];			
			if (smla(original)>0){  //如果RETURN值大於0，跳出迴圈
				ss[j] = set + number + set1 + number1 + set2;
				if(j==0)
					break;
				else //顯示最後一次PASS(左邊=右邊)
					ss[j+1] = (j + 2) + "st Pass\n[ " + number1 + set1 + number1 + set2;
				break;
			} 
			else {
				ss[j] = set + number + set1 + number1 + set2;
			}
		}
		JOptionPane.showMessageDialog(null,ss);  //GUI
	}

	public static void BubbleSortalgorithm(int[] array) {  //泡泡排列
		for (int i = 0; i < 9; i++) {
			if (array[i] > array[i + 1]) {  //如果前項大於後項
				Swap(i, i + 1, array);      //前後交換
			} 
			else
				continue;
		}
	}

	public static void Swap(int a, int b, int[] array) {  //交換方程式
		int change;
		change = array[a];
		array[a] = array[b];
		array[b] = change;
	}

	public static int smla(int[] array){  //比較大小
		for(int i=0;i<9;i++){
			if(array[i]<=array[i+1])
				continue;
			else                          //如果沒從小排到大，就RETURN -1
				return -1;
		}
		return 1;                         //正確由小到大，就RETURN 1
	}
}
