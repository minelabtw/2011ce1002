package a2.s100502513;

import java.lang.Math;
import java.util.Scanner;
import javax.swing.*;


public class A22 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);   
		System.out.print("請輸入半徑: ");
		double r = input.nextDouble();
		if (r < 0)      //半徑小於0不成立
			JOptionPane.showMessageDialog(null, "半徑必須為正!!");
		else {
			System.out.println("請輸入點座標(X,Y)");
			System.out.print("X = ");
			double x = input.nextDouble();
			System.out.print("y = ");
			double y = input.nextDouble();
			if (Math.sqrt(x * x + y * y) < r)  //如果點在圓內
				JOptionPane.showMessageDialog(null, "("+x+","+y+") " + " 點在圓內!!");
			else if (Math.sqrt(x * x + y * y) == r)  //點在圓上
				JOptionPane.showMessageDialog(null, "("+x+","+y+") " + " 點在圓上!!");
			else  //點在圓外
				JOptionPane.showMessageDialog(null, "("+x+","+y+") " + " 點在圓外!!");
		}
	}
}
