package a1.s100502509;
import java.util.Scanner;
import static java.lang.Math.*;  
import javax.swing.JOptionPane;
import javax.swing.*;
public class A22 
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		String Enter = JOptionPane.showInputDialog(null,"Please enter the radius of the circle: ");
		int radius = Integer.parseInt(Enter);
		String X = JOptionPane.showInputDialog(null,"Please enter the two-dimensional coordinate X: ");
		int x = Integer.parseInt(X);//store Y coordinate in variable y
		String Y = JOptionPane.showInputDialog(null,"Please enter the two-dimensional coordinate Y: ");
		int y = Integer.parseInt(Y);//store Y coordinate in variable y
		double distance = (double)(Math.sqrt( x*x + y*y));//Use Math.sqrt!!
		if(distance>radius)//compare
		{
			JOptionPane.showMessageDialog(null,"The point is out of the circle ");
		}
		else if(distance==radius)
		{
			JOptionPane.showMessageDialog(null,"The point is at the circle ");
		}
		else
		{
			JOptionPane.showMessageDialog(null,"The point is inside the circle ");
		}
	}
}
