package a2.s100502026;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class A22 
{
	public static int Check(int radius , double x , double y)
	{
		if( ( radius*radius ) > ( x*x + y*y) )
			return -1;
		else if( ( radius*radius ) < ( x*x + y*y ) )
			return 1;
		else
			return 0;
	}
	public static void main(String[] args)
	{
		//Creat a Scanner
		Scanner input = new Scanner(System.in);
		
		//Key in radius
		JOptionPane.showMessageDialog( null , "Please key in your radius as a integer." );
		int radius = input.nextInt();
		
		//Key in x and y
		JOptionPane.showMessageDialog( null , "Please key in two numbers as a point,s x and y." );
		double x = input.nextDouble();
		double y = input.nextDouble();
		
		//Check if the point is in,on or out of the circle
		//Show result
		if( Check( radius , x , y) == -1 )
		{
			JOptionPane.showMessageDialog( null , "The point is out of the circle." );
		}
		else if( Check( radius , x , y) == 0 )
		{
			JOptionPane.showMessageDialog( null , "The point is on the circle.");
		}
		else
		{
			JOptionPane.showMessageDialog( null , "The point is in the circle.");
		}
	}
}
