package a2.s100502026;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class A21 
{
	public static void BubbleSort( double[] array)
	{
		int counter = 0 ;
		boolean bubble = false;
		
		//Keep Bubble sort before array is orderd
		while( bubble != true )
		{
			//Do a phase and check wheather the array is orderd or not
			while( counter < ( array.length - 1 ) )
			{
				//Exchange two numbers to keep later number is bigger than front one
				if( array[ counter ] > array[ counter + 1 ] )
				{
					Swap( array , counter);
				}
				
				//Keep this phase going on
				counter = counter + 1 ;
			}
			
			//Deside wheather keep going on
			bubble = Judgement( bubble , array );
			
			counter = 0 ;
			//Show a result after a phase
			ShowArray( array );
		}
	}
	
	public static boolean Judgement( boolean bubble , double[] array)
	{
		int counter = 0 ;
		while( counter < array.length - 2)
		{
			if( array [ counter ] > array [ counter + 1 ] )
			{
				return bubble = false ;
			}
			else
			{
				counter = counter + 1 ;
			}
		}
		return bubble = true;
	}
	
	public static void Swap( double[] array , int number)
	{
		double hold = array[ number ] ;
		array[ number ] = array[ ( number + 1 ) ] ;
		array[ ( number+1 ) ] = hold ; 
	}
	
	public static void ShowArray( double[] array)
	{
		int counter = 0 ;
		while( counter < array.length )
		{
			System.out.print( array[ counter ] + "\t" ) ;
			counter = counter + 1 ;
		}
		System.out.print("\n");
	}
	
	public static void main(String[] args)
	{
		//Create a Scanner
		Scanner input = new Scanner(System.in);

		//Declare
		final int constant = 10 ;
		double[] ArrayOfNumbers = new double[constant];
		int counter = 0 ;
		double pass;
		String ward;
		
		//Enter 10 numbers and store numbers into an array
		JOptionPane.showMessageDialog(null,"Please key in 10 numbers.");
		while ( counter < ArrayOfNumbers.length )
		{
			ward = JOptionPane.showInputDialog( null );
			pass = Double.parseDouble( ward );
			ArrayOfNumbers [ counter ] = pass;
			counter = counter + 1 ;
		}
		//Bubble sort algorithm
		BubbleSort(ArrayOfNumbers);
	}
}
