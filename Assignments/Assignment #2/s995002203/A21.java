package a2.s995002203;

import javax.swing.JOptionPane;

public class A21 {
	public static void main(String[] args)
	{
		int[] input = new int[10];
		int flag;
		String tmpStr="";
		
		JOptionPane.showMessageDialog(null, "請輸入10個整數");//一點gui
		for(int i = 0 ; i < 10 ; i++)
		{
			tmpStr = JOptionPane.showInputDialog(null,"Input integer","數字");
			input[i] = Integer.parseInt(tmpStr);
		}	
		
		for(int i = 0 ; i < 9 ; i++)//氣泡排序
		{
			flag = 0;//flag用以紀錄每次氣泡排序是否有swap過
			System.out.printf("NO.%d pass:\n",i+1);
			for(int j = 0 ; j < 9 ; j++)
			{
				display(input);
				if(input[j+1] < input[j])
					flag=swap(j, j+1, input );
				System.out.print("→");
				display(input);
				System.out.print("\n");
			}
			if(flag==0) //若從頭到尾都沒有swap過則跳出迴圈，陣列已排序好
				break;
		}
		JOptionPane.showMessageDialog(null, "完成");//一點gui
	}
	public static int swap( int a, int b, int[] array)
	{
		int tmp = array[a];
		array[a] = array [b];
		array[b] = tmp;
		return 1;
	}
	
	public static void display(int[] array)//輸出陣列的函式
	{
		System.out.print("( ");
	    for(int i=0;i<array.length;i++)
	    	System.out.print(array[i]+" ");
	    System.out.print(")");
	}
}
