package a2.s995002203;

import javax.swing.JOptionPane;
import java.lang.Math;

public class A22 {
	public static void main( String[] args )
	{
		double radius,x,y,tmp;	
		
		String in1 = JOptionPane.showInputDialog("請輸入圓的半徑");
		radius = Integer.parseInt(in1);
		
		String in2 = JOptionPane.showInputDialog("請輸入座標 X ");//輸入X,Y
		x = Integer.parseInt(in2);
		
		String in3 = JOptionPane.showInputDialog("請輸入座標 Y ");
		y = Integer.parseInt(in3);
		
		tmp = Math.sqrt(x*x + y*y);//計算座標到原點的距離
		if(tmp<=radius)
			JOptionPane.showMessageDialog(null, "(" + x + " , " + y + ") 在圓中");
		else
			JOptionPane.showMessageDialog(null, "(" + x + " , " + y + ") 在圓外");
		
	}
}
