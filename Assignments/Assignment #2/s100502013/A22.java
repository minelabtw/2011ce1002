package a2.s100502013;
import java.lang.Math;
import javax.swing.JOptionPane;

public class A22 {
	public static void main(String[] args){
		String radiusstring = JOptionPane.showInputDialog("Please enter an integer as radius.");
		double radius = Double.parseDouble(radiusstring); //Enter a radius
		String xcoodstring = JOptionPane.showInputDialog("Please enter the x-axis two-dimentional-point.");
		double xcood = Double.parseDouble(xcoodstring); //Enter the x-axis point
		String ycoodstring = JOptionPane.showInputDialog("Please enter the y-axis two-dimentional-point.");
		double ycood = Double.parseDouble(ycoodstring); //Enter the y-axis point
		JOptionPane.showMessageDialog(null , distance(xcood, ycood, radius)); //use swap function to get the answer
	}
	
	public static String distance(double x, double y, double s){ //Evaluate the distance and return the answer
		double dis = Math.sqrt(x * x + y * y);
		if(dis-s>0)
			return ("The point (" + x + " , " + y + ") is outside the circle.");
		else if(dis-s==0)
			return ("The point (" + x + " , " + y + ") is on the circle.");
		else
			return ("The point (" + x + " , " + y + ") is inside the circle.");
	}
}
