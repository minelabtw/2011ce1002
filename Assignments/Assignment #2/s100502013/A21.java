package a2.s100502013;
import javax.swing.JOptionPane;

public class A21 {
	public static void main(String[] args){
		final int size = 10; //constant number
		int swapset = 0; //use it to count swap times
		int[] mynumber = new int[size]; //save user's input
		String[] myarray = new String[size]; //save all phases swap result temporary
		for(int a=0;a<size;a++){ //enter numbers
			String myint = JOptionPane.showInputDialog("Please enter " + (a+1) +"th integer number.");
			mynumber[a] = Integer.parseInt(myint);
		}
		int j = 0; //equal: phase (j+1)
		do{
			swapset = 0; //initialize every turn
			for(int k=0;k<size-1;k++){ //check and swap
				if(mynumber[k]>mynumber[k+1]){
					swap(k, k+1, mynumber);
					swapset++;
				}
			}
			myarray[j] = (j+1) + " : {" + mynumber[0]; //save the sorted array into "myarray" as string
			for(int i=1;i<size;i++){
				myarray[j] += "," + mynumber[i];
			}
			myarray[j] += "}";
			j++;
		}while(j<size && swapset!=0); //bubble sort finish
		String [] result = new String[j+1]; //save the organized final result(without useless information)
		for(int l=0;l<j;l++){ //saving
			result[l] = myarray[l];
		}
		JOptionPane.showMessageDialog(null , result); //output all the phase and the final result
	}
	
	public static void swap(int a, int b, int[] array){ //swap two numbers
		int hold = array[a];
		array[a] = array[b];
		array[b] = hold;
	}
}
