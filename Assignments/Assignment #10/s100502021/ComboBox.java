package a10.s100502021;

import javax.swing.*;

import org.omg.CORBA.PUBLIC_MEMBER;
import org.omg.CosNaming.NamingContextExtPackage.AddressHelper;

import java.awt.*;
import java.awt.event.*;
public class ComboBox extends JFrame implements ActionListener{
	
	private JFrame histogramFrame=new JFrame();
	Histogram histogram=new Histogram();
	protected double[] count=new double[5];
	private String[] flagTitles= {"Canada","China","England","German","USA"};
	private ImageIcon[] flagImage={
		new ImageIcon("image/Canada.gif"),
		new ImageIcon("image/China.gif"),
		new ImageIcon("image/England.gif"),
		new ImageIcon("image/German.gif"),
		new ImageIcon("image/USA.gif")		
	};
	Panel panel2=new Panel();
	private JButton button=new JButton("history");
	private String[] flagDescription=new String[5];
	private DescriptionPanel descriptionPanel=new DescriptionPanel();
	private JComboBox jcbo=new JComboBox(flagTitles);
	
	public ComboBox(){
		flagDescription[0]="Canada is a North American country consisting of ten provinces and three territories. Located in the northern part of the continent, it extends from the Atlantic Ocean in the east to the Pacific Ocean in the west, and northward into the Arctic Ocean. Spanning over 9.9 million square kilometres, Canada is the world's second-largest country by total area, and its common border with the United States is the longest land border in the world.";
		flagDescription[1]="China see also Names of China), officially the People's Republic of China (PRC), is the world's most-populous country, with a population of over 1.3 billion. The East Asian state covers approximately 9.6 million square kilometres, and is the world's second-largest country by land area, and the third- or fourth-largest in total area, depending on the definition of total area.";
		flagDescription[2]="England is a country that is part of the United Kingdom. It shares land borders with Scotland to the north and Wales to the west; the Irish Sea is to the north west, the Celtic Sea to the south west, while the North Sea to the east and the English Channel to the south separate it from continental Europe. Most of England comprises the central and southern part of the island of Great Britain in the North Atlantic. The country also includes over 100 smaller islands such as the Isles of Scilly and the Isle of Wight.";
		flagDescription[3]="Germany, officially the Federal Republic of Germany (German: Bundesrepublik Deutschland, pronounced is a federal parliamentary republic in Europe. The country consists of 16 states while the capital and largest city is Berlin. Germany covers an area of 357,021 km2 and has a largely temperate seasonal climate.";
		flagDescription[4]="The United States of America is a federal constitutional republic comprising fifty states and a federal district. The country is situated mostly in central North America, where its forty-eight contiguous states and Washington, D.C., the capital district, lie between the Pacific and Atlantic Oceans, bordered by Canada to the north and Mexico to the south. ";	
		
		button.addActionListener(this);
		
		add(jcbo, BorderLayout.NORTH);
		add(descriptionPanel, BorderLayout.CENTER);
		add(button,BorderLayout.SOUTH);
		setDisplay(0);
		histogramFrame.add(histogram);
	    histogramFrame.pack();
	    histogramFrame.setTitle("Histogram");
	    
		jcbo.addItemListener(new ItemListener() {		
		public void itemStateChanged(ItemEvent e) {
        setDisplay(jcbo.getSelectedIndex());       
      }
    });
		
  }
	private int[] countLetters() {
	    // Count for 26 letters
	    int[] count = new int[26];

	    // Get contents from the text area
	    String text=descriptionPanel.jtaDescription.getText();

	    // Count occurrence of each letter (case insensitive)
	    for (int i = 0; i < text.length(); i++) {
	      char character = text.charAt(i);

	      if ((character >= 'A') && (character <= 'Z')) {
	        count[(int)character - 65]++; // The ASCII for 'A' is 65
	      }
	      else if ((character >= 'a') && (character <= 'z')) {
	        count[(int)character - 97]++; // The ASCII for 'a' is 97
	      }
	    }

	    return count; // Return the count array
	  }
	public void actionPerformed(ActionEvent e) {
        if(e.getSource()==button){
        	int[] count = countLetters();

        // Set the letter count to histogram for display
        	histogram.showHistogram(count);

        // Show the frame
        	histogramFrame.setVisible(true);
        }
	}

    // Create a new frame to hold the histogram panel
  
  public void setDisplay(int index) {
    descriptionPanel.setTitle(flagTitles[index]);
    descriptionPanel.setImageIcon(flagImage[index]);
    descriptionPanel.setDescription(flagDescription[index]);
       
  }
}