package a10.s100502011;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class FrameWork extends JFrame{
	private String[] flagTitles = {"Canada", "China" , "England" ,"German" , "USA"};
	private ImageIcon[] flagImage= { // flag of nations
			new ImageIcon("image/Canada.gif"),
			new ImageIcon("image/China.gif"),
			new ImageIcon("image/England.gif"),
			new ImageIcon("image/German.gif"),
			new ImageIcon("image/USA.gif")
	};
	private String[] flagDescription = new String[5]; // nation's description
	private JComboBox jcbo = new JComboBox(flagTitles); // choose
	private JPanel DescripPanel = new JPanel(); // panel to set description 
	private JLabel jlbImage = new JLabel(); // label to set picture
	private JTextArea descrip = new JTextArea(); 
	private JButton Histogram = new JButton("Histogram");
	private Font font = new Font("Sarif",Font.BOLD,50);
	private Histogram histo = new Histogram(); // object
	private JFrame histoFrame = new JFrame();
	
	public FrameWork(){ // constructor
		//set description
		flagDescription[0]="Twentieth century in the two world wars, Canada are involved " +
				"in the Allied camp, mainly British, American, French, and undamaged by th" +
				"e war coupled with the rich national resources grow together with its nei" +
				"ghbors and the U.S. economy after World War II, manufacturingindustry, mi" +
				"ning industry, the rapid growth of services, and education makes Canada t" +
				"he countries with economies in transition from a rural to an industrializ" +
				"ed and urbanized countries, and train in Canada and the aviation industry" +
				" is quite developed, the economic development of Canada's standard of liv" +
				"ing and national incomeare similar with the United States.";
		flagDescription[1]="China is located in eastern Asia, a geographical or national are" +
				"a, the first refers to descendants of the kingdom established in the Centra" +
				"l Plains [4], to the modern international system, forming after the start a" +
				"s a generic term for the State, the present used to refer to Han as the mai" +
				"n population,Chinese culture-based country. As its foundation of Chinese ci" +
				"vilization is one of the world's ancient civilizations, have a profound imp" +
				"act on neighboring countries and the culture of the nation, the formation o" +
				"f the East Asian / Chinese cultural circle.";
		flagDescription[2]="The island by Great Britain, England, Scotland and Wales, and no" +
				"rtheast of the island of Ireland, Northern Ireland, as well as a series of " +
				"subsidiary islands together to form a Western European island. British in C" +
				"hinese term England from its international code to GB.";
		flagDescription[3]="Federal Republic of Germany (German: Bundesrepublik Deutschland," +
				" Germany), located in central Europe, the parliamentary and federal state, " +
				"consisting of 16 states, the capital and largest city is Berlin. Territoria" +
				"l area of 357,021 square kilometers and a population of 81.8 million, nine " +
				"German land-based neighbors, except Russia, neighbors from the north clockw" +
				"ise sequence of Denmark, Poland, the Czech Republic, Austria, Switzerland, " +
				"France, Luxembourg, Belgium, the Netherlands, most countries in Europe.";
		flagDescription[4]="United States of America (English: The United States of America," +
				" United States, the abbreviation: The States, America; abbreviation: U.S., " +
				"USA, Chinese known as the United States) is composed by the 50 U.S. states " +
				"and a federal district, federal republican constitutional system countries." +
				" The United States is located in central North America, East Atlantic Ocean" +
				" to the west coast of the Pacific, north of Canada, south of Mexico and the" +
				" Gulf of Mexico.Washington, DC, District of Columbia.";
		
		//set image's position
		jlbImage.setHorizontalAlignment(JLabel.CENTER);
		jlbImage.setHorizontalTextPosition(JLabel.CENTER);
		jlbImage.setVerticalTextPosition(JLabel.BOTTOM);
		descrip.setLineWrap(true);
		descrip.setWrapStyleWord(true);
		descrip.setEditable(false);
		setDisplay(0);
		
		//add to panel and frame
		DescripPanel.setLayout(new BorderLayout(5,5));
		DescripPanel.add(jlbImage,BorderLayout.WEST);
		DescripPanel.add(descrip,BorderLayout.CENTER);
		DescripPanel.add(Histogram,BorderLayout.SOUTH);
		add(jcbo,BorderLayout.NORTH);
		add(DescripPanel,BorderLayout.CENTER);
		jcbo.addItemListener(new ItemListener(){ // button's action
			public void itemStateChanged(ItemEvent e){
				setDisplay(jcbo.getSelectedIndex()); // change nation
			}
		});
		
		//handler
		Histogram.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				
				int[] count = countLetters();			
				histo.showHistogram(count);
				histoFrame.setVisible(true);
			}
		});
		histoFrame.add(histo);
		histoFrame.pack();
		histoFrame.setTitle("Histogrma");
	}
	
	// set flag and description
	public void setDisplay(int index){
		jlbImage.setText(flagTitles[index]);
		jlbImage.setIcon(flagImage[index]);
		descrip.setText(flagDescription[index]);
	}
	
	private int[] countLetters(){ // count letters of description
		int[]count = new int[26];
		String text = descrip.getText();	
		for(int i=0;i<text.length();i++){
			char character = text.charAt(i);
			if((character >='A')&&(character <='Z')){
				count[character-'A']++;
			}
			else if((character >='a')&&(character<='z')){
				count[character-'a']++;			}
		}
		return count;
	}
}
