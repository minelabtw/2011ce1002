package a10.s100502011;

import java.awt.*;
import javax.swing.*;
public class Histogram extends JPanel{
	private int[] count; // the array to count alphabet
	
	// show histogram
	public void showHistogram(int[] count){
		this.count=count;
		repaint();
	}
	
	// to paint picture
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		int width = getWidth();
		int height = getHeight();
		int interval = (width-40)/count.length;
		int individualWidth =(int)(((width-40)/24)*0.6);		
		int maxCount = 0;
		for(int i = 0;i < count.length;i++){
			if(maxCount < count[i]){
				maxCount = count[i];
			}
		}
		int x = 30;
		g.drawLine(10,height-45, width-10,height-45);
		for(int i=0;i<count.length;i++){
			int barHeight =(int)(((double)count[i]/(double)maxCount)*(height-55));
			
			//set color
			if(i<2){
				g.setColor(Color.RED);
			}
			else if(i<4){
				g.setColor(new Color(255,90,0));
			}
			else if(i<6){
				g.setColor(new Color(255,170,0));
			}
			else if(i<8){
				g.setColor(new Color(230,255,0));
			}
			else if(i<10){
				g.setColor(new Color(170,255,0));
			}
			else if(i<12){
				g.setColor(new Color(90,255,0));
			}
			else if(i<14){
				g.setColor(new Color(0,255,0));
			}
			else if(i<16){
				g.setColor(new Color(0,255,90));
			}
			else if(i<18){
				g.setColor(new Color(0,255,170));
			}
			else if(i<20){
				g.setColor(new Color(0,255,255));
			}
			else if(i<22){
				g.setColor(new Color(0,170,255));
			}
			else if(i<24){
				g.setColor(new Color(0,90,200));
			}
			else{
				g.setColor(new Color(0,0,255));
			}
			g.drawRect(x,height-45-barHeight,individualWidth,barHeight);
			g.fillRect(x,height-45-barHeight,individualWidth,barHeight);
			g.drawString((char)(65+i)+"",x,height-30);
			x+=interval;
		}
	}
	
	//get size
	public Dimension getPreferredSize(){
		return new Dimension(300,300);
	}

}
