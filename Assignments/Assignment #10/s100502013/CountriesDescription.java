package a10.s100502013;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class CountriesDescription extends JFrame implements ActionListener{
	private String[] title = {"Canada", "China","England","German","USA"};
	private String[] description = new String[5];
	private DescriptionPanel countrydes = new DescriptionPanel();
	private Histogram countletter = new Histogram();
	private JFrame hisframe = new JFrame();
	private JComboBox choosecon = new JComboBox(title);
	private JButton histogram = new JButton("Histogram");
	private ImageIcon[] image = { //set the flagicons
		new ImageIcon("image/Canada.gif"),
		new ImageIcon("image/China.gif"),
		new ImageIcon("image/England.gif"),
		new ImageIcon("image/German.gif"),
		new ImageIcon("image/USA.gif")
	};

	CountriesDescription(){ //Constructor: set details
		description[0] = "Canada is a North American country consisting of ten provinces and three territories." +
						 " Located in the northern part of the continent, " +
				         "it extends from the Atlantic Ocean in the east to the Pacific Ocean in the west, " + 
						 "and northward into the Arctic Ocean. " + 
				         "Spanning over 9.9 million square kilometres, " +
						 " Canada is the world's second-largest country by total area, " + 
				         "and its common border with the United States is the longest land border in the world.";
		description[1] = "China, officially the People's Republic of China (PRC), " +
				         "is the world's most-populous country, with a population of over 1.3 billion. " +
				         "The East Asian state covers approximately 9.6 million square kilometres, " +
				         "and is the world's second-largest country by land area, " +
				         "and the third- or fourth-largest in total area, depending on the definition of total area.";
		description[2] = "England is a country that is part of the United Kingdom. " +
				         "It shares land borders with Scotland to the north and Wales to the west; " +
				         "the Irish Sea is to the north west, the Celtic Sea to the south west, " +
				         "while the North Sea to the east and the English Channel to the south separate it from continental Europe. " +
				         "Most of England comprises the central and southern part of the island of Great Britain in the North Atlantic. " +
				         "The country also includes over 100 smaller islands such as the Isles of Scilly and the Isle of Wight.";
		description[3] = "German, officially the Federal Republic of Germany, " +
				         "is a federal parliamentary republic in Europe. " +
				         "The country consists of 16 states while the capital and largest city is Berlin. " +
				         "Germany covers an area of 357,021 km2 and has a largely temperate seasonal climate. " +
				         "With 81.8 million inhabitants, " +
				         "it is the most populous member state and the largest economy in the European Union. " +
				         "It is one of the major political powers of the European continent and a technological leader in many fields.";
		description[4] = "The United States of America is a federal constitutional republic comprising fifty states and a federal district. " +
				         "The country is situated mostly in central North America, " +
				         "where its forty-eight contiguous states and Washington, D.C., the capital district, " +
				         "lie between the Pacific and Atlantic Oceans, bordered by Canada to the north and Mexico to the south. " +
				         "The state of Alaska is in the northwest of the continent, " +
				         "with Canada to the east and Russia to the west, across the Bering Strait. " +
				         "The state of Hawaii is an archipelago in the mid-Pacific. " +
				         "The country also possesses several territories in the Pacific and Caribbean.";
		setDisplay(0); //initial output
		histogram.setFont(new Font("TimesRoman",Font.BOLD,18));
		add(choosecon,BorderLayout.NORTH);
		add(countrydes,BorderLayout.CENTER);
		add(histogram,BorderLayout.SOUTH);
		choosecon.addItemListener(new ItemListener(){ //according to the choose,display associated information
			public void itemStateChanged(ItemEvent e){
				setDisplay(choosecon.getSelectedIndex());
			}
		});
		hisframe.add(countletter);
		hisframe.pack();
		hisframe.setTitle("Histogram");
		histogram.addActionListener(this); //add event
	}
	
	public void setDisplay(int i){ //set country details
		countrydes.setTitle(title[i]);
		countrydes.setImage(image[i]);
		countrydes.setDescription(description[i]);
	}
	
	public int[] countLetters(){ //count letter numbers
		int[] countl = new int[26];
		String text = countrydes.imagedes.getText();
		for(int i=0;i<text.length();i++){
			char character = text.charAt(i);
			if((character>='A') && (character<='Z'))
				countl[character-'A']++;
			else if((character>='a') && (character<='z'))
				countl[character-'a']++;
		}
		return countl;
	}

	public void actionPerformed(ActionEvent e) { //button "histogram" event
		if(e.getSource()==histogram){
			int[] count = countLetters();
				countletter.showHistogram(count);
				hisframe.setVisible(true);
		}
	}
}
