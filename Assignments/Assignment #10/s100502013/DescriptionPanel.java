package a10.s100502013;
import java.awt.*;
import javax.swing.*;

public class DescriptionPanel extends JPanel{ //country description and flagicon
	private JLabel imagetitle = new JLabel();
	protected JTextArea imagedes = new JTextArea();
	
	DescriptionPanel(){ //set the panel
		imagetitle.setHorizontalAlignment(JLabel.CENTER);
		imagetitle.setHorizontalTextPosition(JLabel.CENTER);
		imagetitle.setVerticalTextPosition(JLabel.BOTTOM);
		imagetitle.setFont(new Font("TimesRoman",Font.BOLD,18));
		imagedes.setFont(new Font("TimesRoman",Font.PLAIN,12));
		imagedes.setLineWrap(true);
		imagedes.setWrapStyleWord(true);
		imagedes.setEditable(false);
		JScrollPane scroll = new JScrollPane(imagedes);
		setLayout(new BorderLayout(4,4));
		add(scroll,BorderLayout.CENTER);
		add(imagetitle,BorderLayout.WEST);
	}
	
	public void setTitle(String title){
		imagetitle.setText(title);
	}
	
	public void setImage(ImageIcon icon){
		imagetitle.setIcon(icon);
	}
	
	public void setDescription(String des){
		imagedes.setText(des);
	}
}
