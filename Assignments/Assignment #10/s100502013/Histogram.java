package a10.s100502013;
import java.awt.*;
import javax.swing.*;

public class Histogram extends JPanel{ //histogram
	private int[] count;
	
	public void showHistogram(int[] counting){
		count = counting;
		repaint();
	}
	
	protected void paintComponent(Graphics g){ //draw the graphics
		if(count==null)
			return;
		
		super.paintComponent(g);
		
		int maxc = 0;
		int x = 30;
		int barw = (int)(((getWidth()-40)/24)*0.6);
		for(int i=0;i<count.length;i++){
			if(maxc<count[i])
				maxc = count[i];
		}
		g.drawLine(10,getHeight()-45,getWidth()-10,getHeight()-45);
		for(int i=0;i<count.length;i++){
			int barh = (int)((double)count[i] / (double)maxc * getHeight()-55);
			g.drawRect(x,getHeight()-barh-45,barw,barh);
			g.drawString((char)(65+i) + "",x,getHeight()-30);
			x += (getWidth()-40)/count.length;
		}
	}
	
	public Dimension getPreferredSize(){
		return new Dimension(300,300);
	}
}
