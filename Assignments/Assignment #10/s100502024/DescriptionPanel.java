package a10.s100502024;
import javax.swing.*;
import java.awt.*;
public class DescriptionPanel extends JPanel
{
	private JLabel jlb = new JLabel(); // 顯示圖片和 Title的Label
	protected JTextArea jtaDescription = new JTextArea(); // 顯示敘述的Panel
	public DescriptionPanel()
	{
		jlb.setHorizontalAlignment(JLabel.CENTER);
		jlb.setHorizontalTextPosition(JLabel.CENTER);
		jlb.setVerticalTextPosition(JLabel.BOTTOM);
		
		jlb.setFont(new Font("SansSerif",Font.BOLD,16)); // 設定字型
		jtaDescription.setFont(new Font("Serif",Font.PLAIN,14));
		
		jtaDescription.setLineWrap(true);
		jtaDescription.setWrapStyleWord(true);
		jtaDescription.setEditable(false);
		
		JScrollPane scrollPane = new JScrollPane(jtaDescription); // 把jtaDescription加上卷軸
		
		setLayout(new BorderLayout(5,5));
		add(jlb,BorderLayout.WEST);
		add(scrollPane,BorderLayout.CENTER);
	}
	public void setTitle(String title) // 設定國家Title
	{
		jlb.setText(title);
	}
	public void setImageIcon(ImageIcon icon) // 設定國旗圖片
	{
		jlb.setIcon(icon);
	}
	public void setDescription(String text) // 設定國家敘述
	{
		jtaDescription.setText(text);
	}
}
