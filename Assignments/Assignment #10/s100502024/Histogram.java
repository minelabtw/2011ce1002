package a10.s100502024;
import javax.swing.*;
import java.awt.*;
public class Histogram extends JPanel
{
	private int[] count;
	public void showHistogram(int[] count)
	{
		this.count = count;
		repaint();
	}
	protected void paintComponent(Graphics g)  // 畫圖
	{
		if(count == null)
		{
			return;
		}
		super.paintComponent(g);
		int width = getWidth(); 
		int height = getHeight();
		int interval = (width-40)/count.length;  // 條與條的間格
		int individualWidth = (int)(((width-40)/24)*0.6);  // 每一條的寬
		
		int maxCount = 0;
		for(int i=0;i<count.length;i++)  // 檢查哪一個字母的個數最多，之後要作為標準長度的基準
		{
			if(maxCount < count[i])
			{
				maxCount = count[i]; 
			}
		}
		
		int x = 30;
		g.drawLine(10,height-45,width-10,height-45);  // 畫圖形的起始線
		for(int i=0;i<count.length;i++)
		{
			int barHeight = (int)(((double)count[i]/(double)maxCount)*(height-55));  // 每一條的高(最高的長度為(視窗大小-55))
			g.drawRect(x,height-45-barHeight,individualWidth,barHeight);  // 畫柱狀圖
			g.drawString((char)(65+i)+"",x,height-30);  // 顯示字母 A~Z
			x+=interval;  // 畫完一條後x座標增加以作為下一條柱狀圖的基準點
		}
	}
}
