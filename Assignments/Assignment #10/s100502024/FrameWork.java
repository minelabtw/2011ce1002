package a10.s100502024;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class FrameWork extends JFrame implements ActionListener
{
	private String[] flagTitles = {"Canada","China","England","Germany","USA"}; // 把國家名稱存入array
	private ImageIcon[] flagImage = new ImageIcon[5];  // 把國旗圖片存入array
	private String[] flagDescription = new String[5]; 
	private DescriptionPanel descriptionPanel = new DescriptionPanel();
	private JComboBox jcbo = new JComboBox(flagTitles);
	private JButton histogramButton = new JButton("Histogram");  // 新增一個Histogram按鈕
	private Histogram histogram = new Histogram();
	private JFrame histogramFrame = new JFrame();  // 新增一個新的視窗
	protected String windowsTitle = new String();
	public FrameWork()
	{
		flagImage[0] = new ImageIcon("image/image/Canada.gif");  // 讀入圖片
		flagImage[1] = new ImageIcon("image/image/China.gif");
		flagImage[2] = new ImageIcon("image/image/England.gif");
		flagImage[3] = new ImageIcon("image/image/German.gif");
		flagImage[4] = new ImageIcon("image/image/USA.gif");
		flagDescription[0] = "Canada is a North American country consisting of ten provinces and three territories." +
							 "Located in the northern part of the continent, it extends from the Atlantic Ocean in the east to the Pacific Ocean in the west, and northward into the Arctic Ocean.";
		flagDescription[1] = "China, officially the People's Republic of China (PRC), is the world's most-populous country, with a population of over 1.3 billion.";
		flagDescription[2] = "England is a country that is part of the United Kingdom. It shares land borders with Scotland to the north and Wales to the west; the Irish Sea is to the north west, the Celtic Sea to the south west, while the North Sea to the east and the English Channel to the south separate it from continental Europe.";
		flagDescription[3] = "Germany, officially the Federal Republic of Germany, is a federal parliamentary republic in Europe. The country consists of 16 states while the capital and largest city is Berlin.";
		flagDescription[4] = "The United States of America (commonly abbreviated to the United States, the U.S., the USA, America, and the States) is a federal constitutional republic comprising fifty states and a federal district.";
		
		setDisplay(0);  // 一開始顯示為Canada的圖片,Title,Description
		setTitle(flagTitles[0]);  // 一開始視窗名稱顯示為 Canada
		
		add(jcbo,BorderLayout.NORTH); 
		add(descriptionPanel,BorderLayout.CENTER);
		add(histogramButton,BorderLayout.SOUTH);
		
		jcbo.addItemListener(new ItemListener() {  // 當選擇選項後要做的事情
			public void itemStateChanged(ItemEvent e)
			{
				setDisplay(jcbo.getSelectedIndex());
				setTitle(windowsTitle);  // 當選擇國家名稱時，視窗名稱重新設定
			}
		});
		
		histogramButton.addActionListener(this);
		histogramFrame.add(histogram);  // add panel 到視窗上
		histogramFrame.setSize(400,300);
	}
	public void actionPerformed(ActionEvent e)  // 按下Histogram要做的事
	{
		int[] count = countLetters(); 
		histogram.showHistogram(count);
		histogramFrame.setVisible(true);  // 顯示視窗
		histogramFrame.setTitle(windowsTitle);  // 設定視窗上的名稱
	}
	public void setDisplay(int index)
	{
		windowsTitle = flagTitles[index];  // 存國家名稱
		descriptionPanel.setTitle(flagTitles[index]);
		descriptionPanel.setImageIcon(flagImage[index]);
		descriptionPanel.setDescription(flagDescription[index]);
	}
	public int[] countLetters()  
	{
		int[] count = new int[26];
		String text = descriptionPanel.jtaDescription.getText();
		for(int i=0;i<text.length();i++)  // 計算Description裡的字母個數
		{
			char character = text.charAt(i);
			if((character >= 'A') && (character <= 'Z'))
			{
				count[character - 'A']++;
			}
			else if ((character >= 'a') && (character <= 'z'))
			{
				count[character - 'a']++;
			}
		}
		return count;
	}

}
