package a10.s100502521;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

public class FrameWork extends JFrame implements ItemListener,ActionListener
{
	private JComboBox list;
	private JLabel picture;
	private ImageIcon[] icon=new ImageIcon[5];
	private JTextArea Descrip;
	private String[] names,Description;
	private JButton Histogram=new JButton("Histogram");
	private int up=50,middle=400,buttom=500;
	private FrameWork2 window;
	FrameWork()
	{
		setTitle("A101");
		setSize(800,600);
		setResizable(false);
		setLayout(null);
		names=new String[]{"Canada","China","England","German","USA"};
		Description=new String[names.length];
		Description[0]="Canada is a North American country consisting of ten provinces and three territories. Located in the northern part of the continent, it extends from the Atlantic Ocean in the east to the Pacific Ocean in the west, and northward into the Arctic Ocean. Spanning over 9.9 million square kilometres, Canada is the world's second-largest country by total area, and its common border with the United States is the longest land border in the world.";
		Description[1]="China (Chinese: see also Names of China), officially the People's Republic of China (PRC), is the world's most-populous country, with a population of over 1.3 billion. The East Asian state covers approximately 9.6 million square kilometres, and is the world's second-largest country by land area, and the third- or fourth-largest in total area, depending on the definition of total area.";
		Description[2]="England is a country that is part of the United Kingdom.It shares land borders with Scotland to the north and Wales to the west; the Irish Sea is to the north west, the Celtic Sea to the south west, while the North Sea to the east and the English Channel to the south separate it from continental Europe. Most of England comprises the central and southern part of the island of Great Britain in the North Atlantic. The country also includes over 100 smaller islands such as the Isles of Scilly and the Isle of Wight.";
		Description[3]="German (Deutsch is a West Germanic language related to and classified alongside English and Dutch. With an estimated 90[1] – 98 million[2] native speakers, German is one of the world's major languages and is the most widely-spoken first language in the European Union.";
		Description[4]="The United States of America (commonly abbreviated to the United States, the U.S., the USA, America, and the States) is a federal constitutional republic comprising fifty states and a federal district. The country is situated mostly in central North America, where its forty-eight contiguous states and Washington, D.C., the capital district, lie between the Pacific and Atlantic Oceans, bordered by Canada to the north and Mexico to the south. The state of Alaska is in the northwest of the continent, with Canada to the east and Russia to the west, across the Bering Strait. The state of Hawaii is an archipelago in the mid-Pacific. The country also possesses several territories in the Pacific and Caribbean.";
		list=new JComboBox(names);
		list.setBounds(0,0,794,up);
		list.addItemListener(this);
		add(list,BorderLayout.NORTH);
		icon[0]=new ImageIcon("src\\a10\\s100502521\\image\\Canada.gif");
		icon[1]=new ImageIcon("src\\a10\\s100502521\\image\\China.gif");
		icon[2]=new ImageIcon("src\\a10\\s100502521\\image\\England.gif");
		icon[3]=new ImageIcon("src\\a10\\s100502521\\image\\German.gif");
		icon[4]=new ImageIcon("src\\a10\\s100502521\\image\\USA.gif");
		picture=new JLabel(names[0],icon[0],SwingConstants.CENTER);
		picture.setHorizontalTextPosition(SwingConstants.CENTER);
		picture.setVerticalTextPosition(SwingConstants.BOTTOM);
		picture.setBounds(0,up,middle,buttom-up);
		add(picture,BorderLayout.WEST);
		Descrip=new JTextArea(Description[0]);
		Descrip.setLineWrap(true);
		JScrollPane scrollPane =new JScrollPane(Descrip);
		scrollPane.setBounds(middle,up,794-middle,buttom-up);
		add(scrollPane,BorderLayout.EAST);
		Histogram.setBounds(0,buttom,794,600-buttom);
		Histogram.addActionListener(this);
		add(Histogram);
	}
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource()==Histogram)
		{
			window=new FrameWork2(countText(list.getSelectedIndex()));
			window.setVisible(true);
		}
	}
	public void itemStateChanged(ItemEvent arg0)
	{
		picture.setIcon(icon[list.getSelectedIndex()]);
		picture.setText(names[list.getSelectedIndex()]);
		Descrip.setText(Description[list.getSelectedIndex()]);
	}
	public int[] countText(int index)
	{
		int[] count=new int[26];
		String text=Description[index];
		for(int i=0;i<text.length();i++)
		{
			if(text.charAt(i)>=97&&text.charAt(i)<=122)
			{
				count[text.charAt(i)-97]++;
			}
			else if(text.charAt(i)>=65&&text.charAt(i)<=90)
			{
				count[text.charAt(i)-65]++;
			}
		}
		return count;
	}
}
