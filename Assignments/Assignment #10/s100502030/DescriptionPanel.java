package a10.s100502030;

import java.awt.BorderLayout;
import java.awt.*;
import javax.swing.*;

public class DescriptionPanel extends JPanel {
	private JLabel jlbbImageTitle = new JLabel();
	private JTextArea jtaDescription = new JTextArea();

	public DescriptionPanel() {
		jlbbImageTitle.setHorizontalAlignment(JLabel.CENTER);
		
		jlbbImageTitle.setHorizontalTextPosition(JLabel.CENTER);
		jlbbImageTitle.setVerticalTextPosition(JLabel.BOTTOM);
		// set text position

		jlbbImageTitle.setFont(new Font("SansSerif", Font.BOLD, 16));
		jtaDescription.setFont(new Font("Serif", Font.PLAIN, 14));
		//set text font

		jtaDescription.setLineWrap(true);
		jtaDescription.setWrapStyleWord(true);
		jtaDescription.setEditable(false);

		JScrollPane JSP = new JScrollPane(jtaDescription);

		setLayout(new GridLayout(1,2));

		add(jlbbImageTitle);// add flag Label
		add(JSP);// add description scrollpane
	}

	public void setTitle(String title) {
		jlbbImageTitle.setText(title);// set flag title
	}

	public void setImageIcon(ImageIcon icon) {
		jlbbImageTitle.setIcon(icon);// set flag
	}

	public void setDescription(String text) {
		jtaDescription.setText(text);// set description text
	}
	
	public String getDescription() {
		return jtaDescription.getText();
	}
}