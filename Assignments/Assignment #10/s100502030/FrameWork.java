package a10.s100502030;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class FrameWork extends JFrame {
	private String[] flagTitles = { "Canada", "China", "England", "German",
			"USA" };
	private ImageIcon[] flagImage = { new ImageIcon("image/Canada.gif"),
			new ImageIcon("image/China.gif"),
			new ImageIcon("image/England.gif"),
			new ImageIcon("image/German.gif"), new ImageIcon("image/USA.gif") };
	private String[] flagDescription = new String[5];

	private DescriptionPanel descriptionPanel = new DescriptionPanel();

	private JComboBox jcbo = new JComboBox(flagTitles);

	private JFrame histogramFrame = new JFrame();
	private JButton jbtShowHistogram = new JButton("Histogram");
	private Histogram histogram = new Histogram();

	public FrameWork() {
		
		// set flag description
		flagDescription[0] = "Official name: Canada\n"
				+ "Capital: Ottawa\n"
				+ "Population: 31.5 million\n"
				+ "Currency: Canadian dollar\n"
				+ "Official language: English and French\n\n"
				+ "Canada is the world's second-largest country, stretching north to Cape Columbia on Ellesmere Island, south to Lake Erie, and across six time zones from Newfoundland to the Pacific seaboard. The interior lowlands around Hudson Bay make up 80% of Canada's land area and include the vast Canadian Shield, with the plains of Saskatchewan and Manitoba and the Rocky Mountains to the west. The St. Lawrence, Yukon, Mackenzie, and Fraser Rivers are among the world's 40 largest. The Great Lakes–St. Lawrence River lowlands are the most populous areas. An Inuit homeland, Nunavut, formerly the eastern part of the Northwest Territories, was created in 1999, covering nearly a quarter of Canada's land area. French-speaking Québec's relationship with the rest of the country causes recurring constitutional arguments.";
		flagDescription[1] = "Official name: People's Republic of China\n"
				+ "Capital: Beijing"
				+ "Population: 1.3 billion\n"
				+ "Currency: Renminbi (known as yuan)\n"
				+ "Official language: Mandarin\n\n"
				+ "Covering a vast area of eastern Asia and home to one-fifth of the world's population, China is bordered by 14 countries. Two-thirds of China is uplands: the southwestern mountains include the Tibetan Plateau; in the northwest, the Tien Shan Mountains separate the Tarim and Dzungarian basins. Two-thirds of the population live in the low-lying east. China was dominated by Chairman Mao Zedong from the founding in 1949 of the Communist People's Republic until his death in 1976. Despite the major disasters of the 1950s Great Leap Forward and the 1960s Cultural Revolution, it became an industrial and nuclear power. Today, China is rapidly developing a market-oriented economy. The current leadership remains set on achieving this without political liberalization, instead enforcing single-party rule as was advocated by \"elder statesman\" Deng Xiaoping, who died in 1997.";
		flagDescription[2] = "Official name: United Kingdom of Great Britain and Northern Ireland\n"
				+ "Capital: London\n"
				+ "Population: 59.3 million\n"
				+ "Currency: Pound sterling\n"
				+ "Official language: English, Welsh (in Wales)\n\n"
				+ "Lying in northwestern Europe, the United Kingdom (UK) occupies the major portion of the British Isles. It includes the countries of England, Scotland, and Wales, the constitutionally distinct region of Northern Ireland, and several outlying islands. Its only land border is with the Irish republic. The UK is separated from the European mainland by the English Channel and the North Sea. To the west lies the Atlantic Ocean. The most densely populated region is the southeast, while Scotland is the wildest region, with the Highlands less populated today than in the 18th century. The UK joined the European Communities (EC – later the EU) in 1973, and most of its trade is now with its European partners. Leadership of the Commonwealth and membership of the UN Security Council give the UK a prominent role in international politics.";
		flagDescription[3] = "Official name: Federal Republic of Germany\n"
				+ "Capital: Berlin\n"
				+ "Population: 82.5 million\n"
				+ "Currency: Euro\n"
				+ "Official language: German\n\n"
				+ "With coastlines on both the Baltic and North Seas, Germany is bordered by nine countries. Plains and rolling hills in the north give way to more mountainous terrain in the south. Europe's foremost industrial power, and its most populous country apart from Russia, Germany is the world's second-biggest exporter. Unified in the 1870s, it was divided after the defeat of the Nazi regime in 1945. The communist-ruled east was part of the Soviet bloc until the collapse of the East German regime in 1989, which paved the way for reunification in 1990. Tensions created by wealth differences between east and west were then exacerbated by record levels of unemployment. The government committed itself to European union and adopted the single currency, the euro, even though the stable deutsche mark had been a symbol of German pride.";
		flagDescription[4] = "Official name: United States of America\n"
				+ "Capital: Washington D.C.\n"
				+ "Population: 294 million\n"
				+ "Currency: US dollar\n"
				+ "Official language: English\n\n"
				+ "The world's third-largest country, the United States is neither overpopulated (like China) nor in the main subject to extremes of climate (like much of Russia and Canada). Its main landmass, bounded by Canada and Mexico, contains 48 of its 50 states. The two others, Alaska at the northwest tip of the Americas and Hawaii in the Pacific, became states in 1959. The US was not built on ethnic identity but on a concept of nationhood intimately bound up with the 18th-century founding fathers' ideas of democracy and liberty – still powerful touchstones in both a political and an economic sense. Since the breakup of the Soviet Union, the US holds a unique position – but arouses extreme hatreds – as the sole global superpower.";

		setDisplay(0);
		
		
		histogramFrame.add(histogram);// add histogram to frame
		histogramFrame.setTitle("Histogram");//set histogramFrame title
		histogramFrame.pack();

		add(jcbo, BorderLayout.NORTH);// add jComboBox to frame
		add(descriptionPanel, BorderLayout.CENTER);// descriptionPanel to frame
		add(jbtShowHistogram, BorderLayout.SOUTH);// ShowHistogram button to frame

		jcbo.addItemListener(new ItemListener() {

			public void itemStateChanged(ItemEvent e) {
				setDisplay(jcbo.getSelectedIndex());
			}
		});// change display
		
		jbtShowHistogram.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				histogramFrame.setBounds(getX(), getY()+getHeight(), 480, 200);
				histogram.showHistogram(countLetters());
				histogramFrame.setVisible(true);
			}
		});// show show histogram frame

	}

	public void setDisplay(int index) {
		descriptionPanel.setTitle(flagTitles[index]);
		descriptionPanel.setImageIcon(flagImage[index]);
		descriptionPanel.setDescription(flagDescription[index]);
	}// set descriptionPanel

	private int[] countLetters() {
		int[] count = new int[26];

		String text = descriptionPanel.getDescription();

		for (int i = 0; i < text.length(); i++) {
			char character = text.charAt(i);
			if (character >= 'A' && character <= 'Z')
				count[character - 'A']++;
			else if (character >= 'a' && character <= 'z')
				count[character - 'a']++;
		}

		return count;
	}// 統計字母

}
