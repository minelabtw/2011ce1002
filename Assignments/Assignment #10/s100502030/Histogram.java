package a10.s100502030;

import javax.swing.*;
import java.awt.*;

public class Histogram extends JPanel {

	private int[] count;

	public void showHistogram(int[] count) {
		this.count = count;// 傳入統計資料
		repaint();
	}

	protected void paintComponent(Graphics g) {
		if (count == null)
			return;

		super.paintComponents(g);

		int width = getWidth();
		int height = getHeight();
		int interval = (width - 40) / count.length;
		int individualWidth = (int) (((width - 40) / 24) * 0.6);

		int maxCount = 0;
		for (int i = 0; i < count[i]; i++) {
			if (maxCount < count[i]) {
				maxCount = count[i];
			}
		}

		int x = 30;
		int a = 255;
		int b = 0;
		int c = 0;
		int k = 0;
		g.drawLine(10, height - 45, width - 10, height - 45);

		for (int i = 0; i < count.length; i++) {
			g.drawString((char) (65 + i) + "", x, height - 30);
			int barHeight = (int) (((double) count[i] / (double) maxCount) * (height - 55));// 比例高度
			g.setColor(new Color(a, b, c));
			g.fillRect(x, height - 45 - barHeight, individualWidth, barHeight);// 畫長條圖
			
			x += interval;
			k += 50;
			int[] Case = {0,0,k%255,255,255,255-k%255};
			for(int j=0;j<6;j++){
				if(k/255==j){
					a=Case[(j+4)%6];
					b=Case[(j+2)%6];
					c=Case[j];
				}
			}
			g.setColor(new Color(0,0,0));
		}
	}

	public Dimension getPreferrSize() {
		return new Dimension(300, 300);
	}
}
