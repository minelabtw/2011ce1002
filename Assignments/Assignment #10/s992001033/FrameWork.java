package a10.s992001033;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener
{
	//名稱、圖片、敘述
	private String[] countryList = {"Canada","China","England","German","USA"};
	private ImageIcon[] flagImage =
	{
		new ImageIcon("image/Canada.gif"),
		new ImageIcon("image/China.gif"),
		new ImageIcon("image/England.gif"),
		new ImageIcon("image/German.gif"),
		new ImageIcon("image/USA.gif"),
	};
	private String[] countryDescription = new String[5];
	
	//各式元件
	JLabel image = new JLabel();
	JButton showHistogram = new JButton("Show Histogram");
	//放flag和description的panel
	JPanel p = new JPanel(new GridLayout(1,2));
	JComboBox jcbo = new JComboBox(countryList);
	JTextArea description = new JTextArea();
	JScrollPane scrollPane = new JScrollPane(description);
	Histogram histogram = new Histogram();
	//放histogram的frame
	JFrame histogramFrame = new JFrame();
	
	FrameWork()
	{
		//各國敘述
		countryDescription[0]=("is a North American country consisting of ten provinces and three territories. Located in the northern part of the continent, it extends from the Atlantic Ocean in the east to the Pacific Ocean in the west, and northward into the Arctic Ocean. Spanning over 9.9 million square kilometres, Canada is the world's second-largest country by total area, and its common border with the United States is the longest land border in the world.");
		countryDescription[1]=("officially the People's Republic of China (PRC), is the world's most-populous country, with a population of over 1.3 billion. The East Asian state covers approximately 9.6 million square kilometres, and is the world's second-largest country by land area,[13] and the third- or fourth-largest in total area, depending on the definition of total area.");
		countryDescription[2]=("is a country that is part of the United Kingdom.[6][7][8] It shares land borders with Scotland to the north and Wales to the west; the Irish Sea is to the north west, the Celtic Sea to the south west, while the North Sea to the east and the English Channel to the south separate it from continental Europe. Most of England comprises the central and southern part of the island of Great Britain in the North Atlantic. The country also includes over 100 smaller islands such as the Isles of Scilly and the Isle of Wight.");
		countryDescription[3]=("officially the Federal Republic of Germany (German: Bundesrepublik Deutschland, pronounced [ˈbʊndəsʁepuˌbliːk ˈdɔʏtʃlant] ( listen)),[5] is a federal parliamentary republic in Europe. The country consists of 16 states while the capital and largest city is Berlin. Germany covers an area of 357,021 km2 and has a largely temperate seasonal climate. With 81.8 million inhabitants, it is the most populous member state and the largest economy in the European Union. It is one of the major political powers of the European continent and a technological leader in many fields.");
		countryDescription[4]=("The United States of America (commonly abbreviated to the United States, the U.S., the USA, America, and the States) is a federal constitutional republic comprising fifty states and a federal district. The country is situated mostly in central North America, where its forty-eight contiguous states and Washington, D.C., the capital district, lie between the Pacific and Atlantic Oceans, bordered by Canada to the north and Mexico to the south. The state of Alaska is in the northwest of the continent, with Canada to the east and Russia to the west, across the Bering Strait. The state of Hawaii is an archipelago in the mid-Pacific. The country also possesses several territories in the Pacific and Caribbean.");
		//分行和不可改
		description.setLineWrap(true);
		description.setWrapStyleWord(true);
		description.setEditable(false);
		//第一個frame
		add(jcbo,BorderLayout.NORTH);
		p.add(image,BorderLayout.WEST);
		p.add(scrollPane,BorderLayout.CENTER);
		add(showHistogram,BorderLayout.SOUTH);
		add(p,BorderLayout.CENTER);
		//放histogram的frame
		histogramFrame.add(histogram);
		histogramFrame.setTitle("Histogram");
		histogramFrame.setSize(900, 700);
		//combobox的反應
		jcbo.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent e)
			{
				setDisplay(jcbo.getSelectedIndex());
			}
		}
		);
		showHistogram.addActionListener(this);
		//預設為第一項
		setDisplay(0);
	}
	public void setImageIcon(ImageIcon icon)
	{
		image.setIcon(icon);
	}
	public void setDescription(String text)
	{
		description.setText(text);
	}
	//同時改變圖片和敘述
	public void setDisplay(int index)
	{
		setImageIcon(flagImage[index]);
		setDescription(countryDescription[index]);
	}
	//按下button發生的事
	public void actionPerformed(ActionEvent e)
	{
		int[] count = countLetters();
		
		histogram.showHistogram(count);
		histogramFrame.setVisible(true);
		
	}
	//計算description內的字
	public int[] countLetters()
	{
		//26個字母
		int[] count =new int[26];
		
		String text = description.getText();
		//分大小寫看
		for(int i=0;i<text.length();i++)
		{
			char character = text.charAt(i);
			if((character >='A'&&(character <='Z')))
				count[character-'A']++;
			else if((character >='a'&&(character <='z')))
				count[character-'a']++;
		}
		return count;
	}
}
