package a10.s992001033;

import java.awt.*;
import javax.swing.*;

public class Histogram extends JPanel
{
	private int[] count;
	//存入計量
	public void showHistogram(int[] count)
	{
		this.count = count;
		repaint();
	}
	//沒有就不印
	protected void paintComponent(Graphics g)
	{
		if (count == null)
			return;
		
		super.paintComponent(g);
		int width = getWidth();
		int height = getHeight();
		int interval = (width -40)/count.length;
		int individualWidth =(int)(((width-40)/24)*0.6);
		
		//尋找最大值
		int maxCount = 0;
		for(int i = 0; i<count.length; i++)
		{
			if(maxCount <count[i])
				maxCount = count[i];
		}
		//起始點，為了不黏邊
		int x=30;
		//底線
		g.drawLine(10, height-45, width-10, height-45);
		//畫每一個長方形
		for (int i=0; i<count.length;i++)
		{
			
			int barHeight =(int) ((double)count[i]/(double)maxCount*(height-55));
			
			g.drawRect(x, height-45-barHeight, individualWidth, barHeight);
			
			g.drawString((char)(65+i)+"",x,height-30);
			
			x += interval;
		}
		
	}
	
	public Dimension getPreferredSize()
	{
		return new Dimension(300,300);
	}
	
}
