package a10.s100502029;
import javax.swing.*;
import java.awt.*;

public class DescriptionPanel extends JPanel {
	private JLabel jlblImageTitle = new JLabel(); // Label for displaying an image icon and a text
	private JTextArea jtaDescription = new JTextArea(); // Text area for displaying text
	
	public DescriptionPanel() {
		// Center the icon and text and place the text and place the text under the icon
		jlblImageTitle.setHorizontalAlignment(JLabel.CENTER);
		jlblImageTitle.setHorizontalTextPosition(JLabel.CENTER);
		jlblImageTitle.setVerticalTextPosition(JLabel.BOTTOM);
		
		// Set lineWrap and wrapStyleWord true for the text area
		jtaDescription.setLineWrap(true);
		jtaDescription.setWrapStyleWord(true);
		jtaDescription.setEditable(false);
		
		// Create a scroll pane to hold the text area
		JScrollPane scrollPane = new JScrollPane(jtaDescription);
		
		// Set GridLayout for the panel, add label and scrollPane
		setLayout(new GridLayout(1, 2));
		add(jlblImageTitle);
		add(scrollPane);
	}
	
	// Set the title
	public void setTitle(String title) {
		jlblImageTitle.setText(title);
	}
	
	// Set the image icon
	public void setImageIcon(ImageIcon icon) {
		jlblImageTitle.setIcon(icon);
	}
	
	// Set the text description
	public void setDescription(String text) {
		jtaDescription.setText(text);
	}
}
