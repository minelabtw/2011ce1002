package a10.s100502029;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FrameWork extends JFrame {
	private String[] flagTitles = {"Canada", "China", "England", "Germany", "USA"}; // Declare an array of Strings for flag titles
	private String[] flagDescription = new String[5]; // Declare an array of strings for flag descriptions
	private DescriptionPanel descriptionPanel = new DescriptionPanel(); // Declare and create a description panel
	private JComboBox jcbo = new JComboBox(flagTitles); // Create a combo box for selecting countries
	private JButton jbtShowHistogram = new JButton("Show Histogram"); // Create a button to show histogram
	private Histogram histogram = new Histogram(); // Declare a histogram object
	private JFrame histogramFrame = new JFrame(); // Create a new frame to gold the histogram panel
	private int selectedIndex = 0; // Declare an integer to store the index of selected
	
	// Declare an ImageIcon array for the national flags of 5 countries
	private ImageIcon[] flagImage = {
			new ImageIcon("image/Canada.gif"),
			new ImageIcon("image/China.gif"),
			new ImageIcon("image/England.gif"),
			new ImageIcon("image/German.gif"),
			new ImageIcon("image/USA.gif")
	};
	
	public FrameWork() {
		// Set text description
		flagDescription[0] = "Canada is a North American country consisting of ten provinces and three territories." +
				" Located in the northern part of the continent, it extends from the Atlantic Ocean in the east to the Pacific Ocean in the west," +
				" and northward into the Arctic Ocean. Spanning over 9.9 million square kilometres," +
				" Canada is the world's second-largest country by total area, " +
				"and its common border with the United States is the longest land border in the world.";
		flagDescription[1] = "China's landscape is vast and diverse," +
				" with forest steppes and the Gobi and Taklamakan deserts occupying the arid north and northwest near Mongolia and Central Asia," +
				" and subtropical forests being prevalent in the wetter south near Southeast Asia." +
				" The terrain of western China is rugged and elevated," +
				" with the Himalaya, Karakoram, Pamir and Tian Shan mountain ranges separating China from South and Central Asia.";
		flagDescription[2] = "England is a country that is part of the United Kingdom." +
				" It shares land borders with Scotland to the north and Wales to the west;" +
				" the Irish Sea is to the north west, the Celtic Sea to the south west," +
				" while the North Sea to the east and the English Channel to the south separate it from continental Europe." +
				" Most of England comprises the central and southern part of the island of Great Britain in the North Atlantic." +
				" The country also includes over 100 smaller islands such as the Isles of Scilly and the Isle of Wight.";
		flagDescription[3] = "Germany, officially the Federal Republic of Germany," +
				" is a federal parliamentary republic in Europe. The country consists of 16 states while the capital and largest city is Berlin." +
				" Germany covers an area of 357,021 km2 and has a largely temperate seasonal climate." +
				" With 81.8 million inhabitants, it is the most populous member state and the largest economy in the European Union." +
				" It is one of the major political powers of the European continent and a technological leader in many fields.";
		flagDescription[4] = "The United States of America (commonly abbreviated to the United States, the U.S., the USA, America, and the States)" +
				" is a federal constitutional republic comprising fifty states and a federal district." +
				" The country is situated mostly in central North America, where its forty-eight contiguous states and Washington, D.C.," +
				" the capital district, lie between the Pacific and Atlantic Oceans, bordered by Canada to the north and Mexico to the south." +
				" The state of Alaska is in the northwest of the continent, with Canada to the east and Russia to the west," +
				" across the Bering Strait. The state of Hawaii is an archipelago in the mid-Pacific." +
				" The country also possesses several territories in the Pacific and Caribbean.";
		
		setDisplay(0); // Set the first country for display
		
		// Add combo box, description panel, and show histogram button to the list
		add(jcbo, BorderLayout.NORTH);
		add(descriptionPanel, BorderLayout.CENTER);
		add(jbtShowHistogram, BorderLayout.SOUTH);
		
		// Register listener
		jcbo.addItemListener(new ItemListener() {
			// Handle item selection
			public void itemStateChanged(ItemEvent e) {
				selectedIndex = jcbo.getSelectedIndex();
				setDisplay(selectedIndex);
			}
		});
		
		// Register listener
		jbtShowHistogram.addActionListener(new ActionListener() {
			// Handle the button action
			public void actionPerformed(ActionEvent e) {
				int[] count = countLetters(selectedIndex); // Count the letters in the text area
				histogram.showHistogram(count); // Set the letter count to histogram for display
				histogramFrame.setBounds(0, getY(), getX(), getHeight()); // Set the frame's location and size
				histogramFrame.setVisible(true); // Show the frame
			}
		});
		
		// Create a new frame to gold the histogram panel
		histogramFrame.add(histogram);
		histogramFrame.pack();
		histogramFrame.setTitle("Histogram");
	}
	
	// Set display information on the description panel
	public void setDisplay(int index) {
		descriptionPanel.setTitle(flagTitles[index]);
		descriptionPanel.setImageIcon(flagImage[index]);
		descriptionPanel.setDescription(flagDescription[index]);
	}
	
	// Count the letters in the text area
	private int[] countLetters(int index) {
		int[] count = new int[26]; // Count for 26 letters
		String text = flagDescription[index]; // Get contents from the text area
		
		// Count occurences of each letter (case insensitive)
		for (int i = 0; i < text.length(); i++) {
			char character = text.charAt(i);
			if ((character >= 'A') && (character <= 'Z')) {
				count[character - 'A']++;
			}
			else if ((character >= 'a') && (character <= 'z')) {
				count[character - 'a']++;
			}
		}
		return count; // Return the count array
	}
}
