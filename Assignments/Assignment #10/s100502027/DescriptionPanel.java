package a10.s100502027;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class DescriptionPanel extends JPanel {
	private JLabel JLImageTitle = new JLabel();
	private JTextArea JTADescription = new JTextArea();
	public DescriptionPanel(){
		JLImageTitle.setHorizontalAlignment(JLabel.CENTER);
		JLImageTitle.setHorizontalTextPosition(JLabel.CENTER);
		JLImageTitle.setVerticalTextPosition(JLabel.BOTTOM);
		
		JTADescription.setLineWrap(true);  //auto change conlum
		JTADescription.setWrapStyleWord(true); //avoid word be cut
		JTADescription.setEditable(false);
		
		JScrollPane scrollPane = new JScrollPane(JTADescription);
		
		setLayout(new BorderLayout(5,5));
		add(scrollPane, BorderLayout.CENTER);
		add(JLImageTitle, BorderLayout.WEST);
	}
	
	public void setTitle(String Title){
		JLImageTitle.setText(Title);
	}
	
	public void setImageIcon(ImageIcon icon){
		JLImageTitle.setIcon(icon);
	}
	
	public void setDescription(String text){
		JTADescription.setText(text);
	}
	public JTextArea getJTA(){   //return the JTextArea to the frame to given another frame use
		return JTADescription;
	}
}
