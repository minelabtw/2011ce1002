package a10.s100502027;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;

public class FrameWork extends JFrame implements ActionListener {
	private JButton JBHistogram = new JButton("Histogram");
	private String[] countrys = {"Canada","China","England","German","USA"};
	private ImageIcon[] flagnames = {new ImageIcon("image/Canada.gif"),new ImageIcon("image/China.gif"),new ImageIcon("image/England.gif"),new ImageIcon("image/German.gif"),new ImageIcon("image/USA.gif")};
	private String[] flagDescription = new String[5];
	private DescriptionPanel descriptionPanel = new DescriptionPanel();
	private JComboBox jcbo = new JComboBox(countrys); //set the combo box list
	private Histogram histogram = new Histogram();
	private JFrame histogramFrame = new JFrame();
	public FrameWork(){
		flagDescription[0] = "The National Flag of Canada, also known as the Maple Leaf , is a red flag with a white square in its centre, featuring a stylized 11-pointed red maple leaf. Its adoption in 1965 marked the first time a national flag had been officially adopted in Canada to replace the Union Flag. The Canadian Red Ensign had been unofficially used since the 1890s and was approved by a 1945 Order in Council for use wherever place or occasion may make it desirable to fly a distinctive Canadian flag";
		flagDescription[1] = "The flag of the People's Republic of China is a red field charged in the canton (upper corner nearest the flagpole) with five golden stars. The design features one large star, with four smaller stars in a semicircle set off towards the fly (the side farthest from the flag pole). The red represents revolution; the five stars and their relationship represent the unity of the Chinese people under the leadership of the Communist Party of China (CPC). Sometimes, the flag is referred to as the Five Star Red Flag";
		flagDescription[2] = "The Flag of England is the St George's Cross (heraldic blazon: Argent, a cross gules). The red cross appeared as an emblem of England in the Middle Ages, specifically during the Crusades (although the original symbol used to represent English crusaders was a white cross on a red background) and is one of the earliest known emblems representing England. It also represents the official arms of the Most Noble Order of the Garter, and it achieved status as the national flag of England during the sixteenth century.";
		flagDescription[3] = "The flag of Germany is a tricolour consisting of three equal horizontal bands displaying the national colours of Germany: black, red, and gold. The flag was first adopted as the national flag of modern Germany in 1919, during the Weimar Republic.";
		flagDescription[4] = "The national flag of the United States of America, often simply referred to as the American flag, consists of thirteen equal horizontal stripes of red (top and bottom) alternating with white, with a blue rectangle in the canton (referred to specifically as the union) bearing fifty small, white, five-pointed stars arranged in nine offset horizontal rows of six stars (top and bottom) alternating with rows of five stars. The 50 stars on the flag represent the 50 states of the United States of America and the 13 stripes represent the thirteen British colonies that declared independence from the Kingdom of Great Britain and became the first states in the Union.";
		
		setDisplay(0);
		setLayout(new BorderLayout(5,5));
		add(jcbo, BorderLayout.NORTH);
		add(descriptionPanel, BorderLayout.CENTER);
		add(JBHistogram , BorderLayout.SOUTH);
		
		histogramFrame.add(histogram);
		histogramFrame.pack();
		histogramFrame.setTitle("Histogram");
		
		JBHistogram.addActionListener(this);
		jcbo.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e){
				setDisplay(jcbo.getSelectedIndex());
			}
		});
	}
	public void actionPerformed(ActionEvent e){  //when use the button to show the histogram
		if(e.getSource()==JBHistogram){
			int[] count = countLetters();
			histogram.showHistogram(count);
			histogramFrame.setVisible(true);
		}
	}
	public void setDisplay(int index){             //to respond the combo box , change the imformation on the descriptionPanel
		descriptionPanel.setTitle(countrys[index]);
		descriptionPanel.setImageIcon(flagnames[index]);
		descriptionPanel.setDescription(flagDescription[index]);
	}
	private int[] countLetters(){              // to count the letters in the description 'a'-'z'
		int[] count = new int[26];
		String text = descriptionPanel.getJTA().getText();
		
		for(int i=0;i<text.length();i++){
			char character = text.charAt(i);
			if((character>='A' &&(character<='Z'))){
				count[character-'A']++;
			}
			else if((character>='a'&&(character<='z'))){
				count[character-'a']++;
			}
		}
		return count;
	}
}
