package a10.s995002204;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.*;

public class A101 {
	public static void main(String args[])
	{
		FrameWork f = new FrameWork();
		f.setTitle("A101");
		f.setSize(800, 450);
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}

class FrameWork extends JFrame{
	
	//declare variable
	private Histogram histogram = new Histogram();
	
	// Create a new frame to hold the histogram panel
	private JFrame histogramFrame = new JFrame();
	
	private DescriptionPanel descriptionPanel = new DescriptionPanel();
	
	private JLabel photo = new JLabel();
	private JButton number = new JButton("Histogram");
	private String[] flagTitles= {"Canada","China","England","Germany","USA"};
	private JComboBox jcbo = new JComboBox(flagTitles);
	
	private String[] flagDescription = new String[5];
	private ImageIcon[] flagName = {new ImageIcon("src/image/Canada.gif"),new ImageIcon("src/image/China.gif"),
			new ImageIcon("src/image/England.gif"),new ImageIcon("src/image/German.gif"),new ImageIcon("src/image/USA.gif")};
	
	
	public FrameWork()
	{
		setLayout(new BorderLayout());
		
		// Set text description
		flagDescription[0]="Canada is a North American country consisting of ten "
				+"provinces and three territories. Located in the northern part of "
				+"the continent, it extends from the Atlantic Ocean in the east to "
				+"the Pacific Ocean in the west, and northward into the Arctic Ocean.";
		flagDescription[1]="China , officially the People's Republic of China (PRC), "
				+"is the world's most-populous country, with a population of over 1.3 billion.";
		flagDescription[2]="England is a country that is part of the United Kingdom.It "
				+"shares land borders with Scotland to the north and Wales to the west;"
				+"the Irish Sea is to the north west, the Celtic Sea to the south west,"
				+"while the North Sea to the east and the English Channel to the south "
				+"separate it from continental Europe.";
		flagDescription[3]="Germany , officially the Federal Republic of Germany , is a"
				+"federal parliamentary republic in Europe. The country consists of 16 "
				+"states while the capital and largest city is Berlin. ";
		flagDescription[4]="The United States of America (commonly abbreviated to the United"
				+"States, the U.S., the USA, America, and the States) is a federal constitutional"
				+"republic comprising fifty states and a federal district.";
		
		// Set the first country (Canada) for display
		setDisplay(0);
		
		// Add combo box and description panel and button to the list
		add(jcbo, BorderLayout.NORTH);
		add(descriptionPanel, BorderLayout.CENTER);
		add(number, BorderLayout.SOUTH);
		
		
		jcbo.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent e)
			{
				setDisplay(jcbo.getSelectedIndex());
			}
		});
		
		
		number.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int[] count = countLetters(jcbo.getSelectedIndex());
				histogram.showHistogram(count);
				histogramFrame.setLocationRelativeTo(null);
				histogramFrame.setSize(400, 400);
				histogramFrame.setVisible(true);
			}
		});
		
		histogramFrame.add(histogram);
		histogramFrame.setTitle("Histogram");
	}
	
	public void setDisplay(int index)
	{
		descriptionPanel.setTitle(flagTitles[index]);
		descriptionPanel.setImageIcon(flagName[index]);
		descriptionPanel.setDescription(flagDescription[index]);
	}
	
	//count the number of alphabet
	public int[] countLetters(int index)
	{
		int[] count = new int[26];
		String text = flagDescription[index];
		
		for (int i = 0; i < text.length(); i++)
		{
			char character = text.charAt(i);
			if ((character >= 'A') && (character <= 'Z'))
			{
				count[character - 'A']++;
			}
			
			else if ((character >= 'a') && (character <= 'z'))
			{
				count[character - 'a']++;
			}
		}
		
		return count; 
	}

}

class DescriptionPanel extends JPanel
{
	// Label for displaying an image icon and a text
	private JLabel jlblImageTitle = new JLabel();
	
	// Text area for displaying text
	private JTextArea jtaDescription = new JTextArea();
	
	public DescriptionPanel() 
	{
		// Center the icon and text and place the text under the icon
		jlblImageTitle.setHorizontalAlignment(JLabel.CENTER);
		jlblImageTitle.setHorizontalTextPosition(JLabel.CENTER);
		jlblImageTitle.setVerticalTextPosition(JLabel.BOTTOM);
		
		// Set the font in the label and the text field
		jlblImageTitle.setFont(new Font("SansSerif", Font.BOLD, 16));
		jtaDescription.setFont(new Font("Serif", Font.PLAIN, 14));
		
		// Set lineWrap and wrapStyleWord true for the text area
		jtaDescription.setLineWrap(true);
		jtaDescription.setWrapStyleWord(true);
		jtaDescription.setEditable(false);
		
		// Create a scroll pane to hold the text area
		JScrollPane scrollPane = new JScrollPane(jtaDescription);
		
		// Set BorderLayout for the panel, add label and scroll pane
		setLayout(new BorderLayout(5, 5));
		
		add(scrollPane, BorderLayout.CENTER);
		add(jlblImageTitle, BorderLayout.WEST);
	}
	
	public void setTitle(String title)
	{
		jlblImageTitle.setText(title);
	}
	
	public void setImageIcon(ImageIcon icon)
	{
		jlblImageTitle.setIcon(icon);
	}
	
	public void setDescription(String text)
	{
		jtaDescription.setText(text);
	}
}

//Paint
class Histogram extends JPanel
{
	private int[] count;
	
	public void showHistogram(int[] count)
	{
		this.count = count;
		repaint();
	}
	
	protected void paintComponent(Graphics g)
	{
		if (count == null) return;
		
		super.paintComponent(g);
		
		int width = getWidth();
		int height = getHeight();
		int interval = (width - 40) / count.length;
		int individualWidth = (int)(((width - 40) / 24) * 0.60);
		
		int maxCount = 0;
		for (int i = 0; i < count.length; i++)
		{
			if (maxCount < count[i])
				maxCount = count[i];
		}
		
		int x = 30;
		g.drawLine(10, height - 45, width - 10, height - 45);
		for (int i = 0; i < count.length; i++)
		{
			int barHeight = (int)(((double)count[i] / (double)maxCount) * (height - 55));
			g.drawRect(x, height - 45 - barHeight, individualWidth,barHeight);
			g.drawString((char)(65 + i) + "", x, height - 30);
			
			x += interval;
		}
	}
	
	public Dimension getPreferredSize()
	{
		return new Dimension(300, 300);
	}
}

