package a10.s100502004;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;

public class FrameWork extends JFrame {
	
	public String[] flagTitle = {"Canada" , "China" , "England" , "German" , "USA"};
	
	private ImageIcon[] flagImages = {//set the images
	 		new ImageIcon("image/Canada.gif"),
	 		new ImageIcon("image/China.gif"),
	 		new ImageIcon("image/England.gif"),
	 		new ImageIcon("image/German.gif"),
	 		new ImageIcon("image/USA.gif"),	
	 	};
		
	
	private String[] flagDescription = new String[5];
	
	private Description description = new Description();
	
	private JComboBox jcbo = new JComboBox(flagTitle);
	
	private JButton Histogram = new JButton("Histogram");
	
	private Histogram histogram = new Histogram();
	
	private JFrame histogramFrame = new JFrame();	
	
	int number = 0;
	
	public FrameWork() {//set the descriptions
		// TODO Auto-generated constructor stub		
		
		flagDescription[0] = "Canada is located in the northern half of the Western Hemisphere, North America, north of the Arctic Ocean, the east Atlantic, west Pacific, ten million square kilometers of land area, population of about $ 31 million. Canada is a nation of immigrants, about 150,000 immigrants from over 150 countries around the world each year was sworn in as Canadian citizens. The establishment of Canadian citizenship only started 50 years ago, in 1947, the Canadians is part of British jurisdiction. True Canadian original inhabitants, also known as the indigenous people before the arrival of the first immigrants living in Canada for thousands of years, the original inhabitants is an important part of the Canadian population, they are trying to protect, promote their language , culture and traditions, and established their own government. ";
		flagDescription[1] = "China, also known as China, China, and Kyushu, due to the ancient Chinese people believe that the hemispherical dome, located in the middle, hence the name. China's long-term is independent of the center of the civilized world, the spread of Chinese culture had a profound impact on the surrounding country. China is the earliest one of the cradles of world civilization. The Central Plains region of the Yellow River basin is the birthplace of Chinese civilization, The Yanhuang times the initial formation period of the Chinese nation, matured after a period of Yao, Shun and Yu and Xia, Shang, Zhou and other Chinese culture. Summer before the legendary era of the Three Sovereigns and Five Emperors calendar year no less than thousands of years.";
		flagDescription[2] = "Britain is a developed nations, the world's first industrialized countries, is the world's most powerful country in the 19th century and early 20th century, but after the collapse of half Ye Taiying Empire in two world wars and the 20th century, Britain has lostonce the leading force in international affairs. However, Britain still has a huge influence, is a worldwide important political, economic, cultural and military power."	;	
		flagDescription[3] = "Capital in Berlin, Germany, the main town outside of Berlin, Hamburg, Munich, Cologne, Frankfurt, Dortmund, Essen. Germany's diverse terrain and moving scenery, rolling hills, plateaus, terraces, hills, mountains, lakes, and vast and broad plains. Can be divided into five terrain from north to south: North German lowland , medium mountain uplift zone , Medium Mountains of southwestern trapezoidal zone , NEG Alps frontier as well as the Bavarian Alps. ";
		flagDescription[4] = "There are currently 50 states, a Federal Territory SAR and overseas territory, a land area of approximately 9.63 million square kilometers (some say 9.37 million square kilometers), the highest in the world's third or fourth. U.S. population of about 310 million people, the number for the third in the world. Multi-cultural and multi-ethnic country, predominantly white, but a large immigrant population. Each of the 50 states of the 50 white five-pointed star on behalf of the American flag, red and white bars for seven and six, a total of 13, to commemorate the initial 13 independent states";
		setDisplay(0);
		
		
		add(jcbo,BorderLayout.NORTH);
		add(description , BorderLayout.CENTER);
		add(Histogram,BorderLayout.SOUTH);
		
		
		jcbo.addItemListener(new ItemListener() {//action listener
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				setDisplay(jcbo.getSelectedIndex());				
			}	
		});
		
		Histogram.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
				int[] count = countLetters(flagDescription, number);
				
				histogram.showHistogram(count);
				
				histogramFrame.setVisible(true);
				
			}
		});
		
		histogramFrame.add(histogram);
		histogramFrame.pack();
		histogramFrame.setTitle("Histogram");
		
	}
	
	public void setDisplay(int index){//set the text and the picture
		number = index;
		description.setTitle(flagTitle[index]);
		description.setImageIcon(flagImages[index]);
		description.setDescription(flagDescription[index]);
	}
	
	protected int[] countLetters(String[] a, int number){//use to calculate the numbers of each letter
		int[] count = new int[26];
		
		String text = a[number];
		
		for(int i = 0; i < text.length(); i++){
			char character = text.charAt(i);
			
			if((character>='A')&&(character<='Z')){
				count[character-'A']++;
			}
			
			else if((character>='a')&&(character<='z')){
				count[character-'a']++;				
			}
		}
		
		return count;
	}
	
	
}