package a10.s100502004;

import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

public class Histogram extends JPanel{//use this to set the other window 
	private int[] count;
	
	public void showHistogram(int[] count){
		this.count = count;
		repaint();	
	}
	
	protected void paintComponent(Graphics g) {//draw the histogram pictures
		if(count == null){
			return ;			
		}
		
		super.paintComponent(g);
		
		int width = getWidth();
		int heigh = getHeight();
		int interval = (width-40)/count.length;
		int individualWidth = (int)(((width-40)/24)*0.6);
		
		int maxCount = 0;
		
		for(int i = 0; i < count.length; i++){
			if(maxCount < count[i])
				maxCount = count[i];
			}
			
			int x = 30;
			g.drawLine(10, heigh-45, width-10, heigh-45);
			for(int i = 0; i < count.length; i++){
				int barHeight = (int)(((double)count[i]/(double)maxCount)*(heigh-55));		
				
				g.drawRect(x, heigh - 45 - barHeight, individualWidth, barHeight);
				
				g.drawString((char)(65+i)+"", x, heigh - 30);
				
				x+=interval;
				
			}
		}
		
		
		public Dimension getPreferredSize(){
			return new Dimension(300,300);
			
		}
		
		
	
	
		
}
	
	
	
	
	
	
	

