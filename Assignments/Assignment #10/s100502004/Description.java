package a10.s100502004;

import java.awt.BorderLayout;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Description extends JPanel{//use to set the JPanel in the middle
	private JLabel jlbImageTitle = new JLabel();
	private JTextArea jtaDescription = new JTextArea();
	
	public Description(){
		jlbImageTitle.setHorizontalAlignment(JLabel.CENTER);
		jlbImageTitle.setHorizontalTextPosition(JLabel.CENTER);
		jlbImageTitle.setVerticalTextPosition(JLabel.BOTTOM);
		
		jtaDescription.setLineWrap(true);
		jtaDescription.setWrapStyleWord(true);
		jtaDescription.setEditable(false);
		
		JScrollPane scrollPane = new JScrollPane(jtaDescription);
		
		setLayout(new BorderLayout(5,5));
		add(scrollPane,BorderLayout.CENTER);
		add(jlbImageTitle,BorderLayout.WEST);
	}
	
	
	public void setTitle(String title){
		jlbImageTitle.setText(title);
	}
	
	
	public void setImageIcon(ImageIcon icon){
		jlbImageTitle.setIcon(icon);		
	}
	
	
	public void setDescription(String text){
		jtaDescription.setText(text);
	}
	
}
