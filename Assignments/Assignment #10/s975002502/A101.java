package a10.s975002502;
import javax.swing.JFrame;

public class A101 {
	public static void main(String[] args) {
		FrameWork frame = new FrameWork();
		frame.pack();
		frame.setTitle("A10");
		frame.setLocationRelativeTo(null);	// Center the frame
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
