package a10.s975002502;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class FrameWork extends JFrame {
	// Declare an array of Strings for flag titles
	private String[] flagTitles = {"Canada", "China", "England", "Germany", "USA"};
	
	// Declare an ImageIcon array for the national flags of 5 countries
	private ImageIcon[] flagImage = {
			new ImageIcon("image/Canada.gif"),
			new ImageIcon("image/China.gif"),
			new ImageIcon("image/England.gif"),
			new ImageIcon("image/German.gif"),
			new ImageIcon("image/USA.gif"),
	};
	
	// Declare an array of strings for flag descriptions
	private String[] flagDescription = new String[9];
	
	// Declare and create a description panel
	private DescriptionPanel descriptionPanel = new DescriptionPanel();
	
	// Create a combo box for selecting countries
	private JComboBox jcbo = new JComboBox(flagTitles);
	
	// Create a text area to get the description
	private JTextArea jta = descriptionPanel.getjtaDescription();
	
	// Create a button to show histogram
	private JButton jbtShowHistogram = new JButton("Show Histogram");
	
	// Declare and create a histogram panel
	private Histogram histogram = new Histogram();
	
	// Create a new frame to hold the histogram panel
	private JFrame histogramFrame = new JFrame();
	
	public FrameWork() {
		// Set text description
		flagDescription[0] = "The Maple Leaf flag \n\n" +
			"The Canadian National Flag was adopted by the Canadian " +
			"Parliament on October 22, 1964 and was proclaimed into law " +
			"by Her Majesty Queen Elizabeth II (the Queen of Canada) on " +
			"February 15, 1965. The Canadian Flag (colloquially known " +
			"as The Maple Leaf Flag) is a red flag of the proportions " + 
			"two by length and one by width, containing in its centre a " +
			"white square, with a single red stylized eleven-point " +
			"maple leaf centred in the white square.";
		
		flagDescription[1] = "Five Star Red Flag \n\n" + 
			"The flag of the People's Republic of China is a red field " + 
			"charged in the canton (upper corner nearest the flagpole) " + 
			"with five golden stars. The design features one large star, " + 
			"with four smaller stars in a semicircle set off towards the " + 
			"fly (the side farthest from the flag pole). The red represents " +
			"revolution; the five stars and their relationship represent " + 
			"the unity of the Chinese people under the leadership of the " + 
			"Communist Party of China (CPC)."; 
		flagDescription[2] = "Union Jack \n\n" +
			"The United Kingdom of Great Britain and Northern Ireland uses " +
			"as its national flag the royal banner known as the Union Flag " +
			"or, popularly, Union Jack. The term Union Jack is used in " +
			"colloquial speech and comes from the written name of James I of " +
			"England. The latin form of the name James is Jacobus and the " +
			"British version of Jacobus is Jack. The current design of the " +
			"Union Flag dates from the union of Ireland and Great Britain in " +
			"1801. It consists of the red cross of Saint George (patron saint " +
			"of England), edged in white, superimposed on the Cross of " +
			"St Patrick (patron saint of Ireland), which are superimposed on " +
			"the Saltire of Saint Andrew (patron saint of Scotland). Wales, " +
			"however, is not represented in the Union Flag by Wales' patron " +
			"saint, Saint David, as at the time the flag was designed Wales " +
			"was part of the Kingdom of England.";
		flagDescription[3] = "National Flag and Civil Ensign/Federal Flag \n\n" +
			"The black-red-gold is historically associated with \"liberal\" " +
			"nationalism in Germany, rather than republicanism per se. It was " +
			"first adopted by the Frankfurt Parliament in 1848 for the " +
			"proposed united German Empire. That the 1870 German Empire went " +
			"for a flag asserting north German traditions (the black and " +
			"white of Prussia with the white and red of the Hanseatic League) " +
			"was due to Bismarck wanting a Kleindeutschland solution - excluding " +
			"the Austrian lands, rather than the Frankfurt liberals' " +
			"Grossdeutschland which would have included the Austrian lands " +
			"within the old German Confederation.";
		flagDescription[4] = "American flag \n\n" + 
			"The national flag of the United States of America, often simply " +
			"referred to as the American flag, consists of thirteen equal " +
			"horizontal stripes of red (top and bottom) alternating with white, " +
			"with a blue rectangle in the canton (referred to specifically " +
			"as the \"union\") bearing fifty small, white, five-pointed stars " +
			"arranged in nine offset horizontal rows of six stars " +
			"(top and bottom) alternating with rows of five stars. The 50 stars " +
			"on the flag represent the 50 states of the United States of America " +
			"and the 13 stripes represent the thirteen British colonies that " +
			"declared independence from the Kingdom of Great Britain and became " +
			"the first states in the Union. Nicknames for the flag include the " +
			"\"Stars and Stripes\", \"Old Glory\", and \"The Star-Spangled Banner\" " +
			"(also the name of the national anthem).";
		
		// Set the first country (Canada) for display
		setDisplay(0);
	
		// Add combo box and description panel to the list
		add(jcbo, BorderLayout.NORTH);
		add(descriptionPanel, BorderLayout.CENTER);
		add(jbtShowHistogram, BorderLayout.SOUTH);
	
		// Register listener
		jcbo.addItemListener(new ItemListener() {
			/** Handle item selection */
			public void itemStateChanged(ItemEvent e) {
				setDisplay(jcbo.getSelectedIndex());
			}
		});
		
		jbtShowHistogram.addActionListener(new ActionListener() {
			/** Handle the button action */
			public void actionPerformed(ActionEvent e) {
				// Count the letters in the text area
				int[] count = countLetters();
				
				// Set the letter count to histogram for display
				histogram.showHistogram(count);
				
				// Show the frame
				histogramFrame.setVisible(true);
			}
		});
		
		// Create a new frame to hold the histogram panel
		histogramFrame.add(histogram);
		histogramFrame.pack();
		histogramFrame.setTitle("Histogram");
	}
	
	/** Set display information on the description panel */
	public void setDisplay(int index) {
		descriptionPanel.setTitle(flagTitles[index]);
		descriptionPanel.setImageIcon(flagImage[index]);
		descriptionPanel.setDescription(flagDescription[index]);
	}
	
	/** Count the letters in the text area */
	private int[] countLetters() {
		// Count for 26 letters
		int[] count = new int[26];
		
		// Get contents from the text area
		String text = jta.getText();
		
		// Count occurrence of ech letter (case insensitive)
		for(int i = 0; i < text.length(); i++) {
			char character = text.charAt(i);
			
			if((character >= 'A')&&(character <= 'Z')) {
				count[character - 'A']++;
			}
			else if((character >= 'a')&&(character <= 'z')) {
				count[character - 'a']++;
			}
		}
		
		return count;	// Return the count array
	}
}

