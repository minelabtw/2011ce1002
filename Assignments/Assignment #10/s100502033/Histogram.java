package a10.s100502033;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.*;
public class Histogram extends JPanel
{
	private String string;
	private int number[] = new int[26];
	public Histogram(String string1)
	{
		string = string1;
		for(int k = 0 ; k < 25 ; k++)
		{
			number[k] = 0;//每一項歸零
		}
	}
	public void paintComponent(Graphics g)
	{
		//計算每一個字母出現了多少次，大小寫是一樣的
		for(int i = 0 ; i < string.length() ; i++ )
		{
			int number1 = string.charAt(i);
			if(number1 == 65 || number1 == 97)
			{
				number[0]++;
			}
			else if(number1 == 66 || number1 == 98)
			{
				number[1]++;
			}
			else if(number1 == 67 || number1 == 99)
			{
				number[2]++;
			}
			else if(number1 == 68 || number1 == 100)
			{
				number[3]++;
			}
			
			else if(number1 == 69 || number1 == 101)
			{
				number[4]++;
			}
			else if(number1 == 70 || number1 == 102)
			{
				number[5]++;
			}
			else if(number1 == 71 || number1 == 103)
			{
				number[6]++;
			}
			else if(number1 == 72 || number1 == 104)
			{
				number[7]++;
			}
			else if(number1 == 73 || number1 == 105)
			{
				number[8]++;
			}
			else if(number1 == 74 || number1 == 106)
			{
				number[9]++;
			}
			else if(number1 == 75 || number1 == 107)
			{
				number[10]++;
			}
			else if(number1 == 76 || number1 == 108)
			{
				number[11]++;
			}
			else if(number1 == 77 || number1 == 109)
			{
				number[12]++;
			}
			else if(number1 == 78 || number1 == 110)
			{
				number[13]++;
			}
			else if(number1 == 79 || number1 == 111)
			{
				number[14]++;
			}
			else if(number1 == 80 || number1 == 112)
			{
				number[15]++;
			}
			else if(number1 == 81 || number1 == 113)
			{
				number[16]++;
			}
			else if(number1 == 82 || number1 == 114)
			{
				number[17]++;
			}
			else if(number1 == 83 || number1 == 115)
			{
				number[18]++;
			}
			else if(number1 == 84 || number1 == 116)
			{
				number[19]++;
			}
			else if(number1 == 85 || number1 == 117)
			{
				number[20]++;
			}
			else if(number1 == 86 || number1 == 118)
			{
				number[21]++;
			}
			else if(number1 == 87 || number1 == 119)
			{
				number[22]++;
			}
			else if(number1 == 88 || number1 == 120)
			{
				number[23]++;
			}
			else if(number1 == 89 || number1 == 121)
			{
				number[24]++;
			}
			else if(number1 == 90 || number1 == 122)
			{
				number[25]++;
			}
		}
		//從侳標的值
		g.drawString("0" , getWidth()/getWidth()+10 , getHeight()-120);
		g.drawString("20" , getWidth()/getWidth()+10 , getHeight()-180);
		g.drawString("40" , getWidth()/getWidth()+10 , getHeight()-240);
		g.drawString("60" , getWidth()/getWidth()+10 , getHeight()-300);
		g.drawString("80" , getWidth()/getWidth()+10 , getHeight()-360);
		g.drawString("100" , getWidth()/getWidth()+10 , getHeight()-420);
		g.drawString("120" , getWidth()/getWidth()+10 , getHeight()-480);
		g.drawString("140" , getWidth()/getWidth()+10 , getHeight()-540);
		g.drawString("160" , getWidth()/getWidth()+10 , getHeight()-600);
		g.drawString("180" , getWidth()/getWidth()+10 , getHeight()-660);
		g.setColor(Color.red);
		//棋坐標的資料
		g.drawString("A", getWidth()/26, getHeight()-100);
		g.drawString("B", getWidth()/26*2, getHeight()-100);
		g.drawString("C", getWidth()/26*3, getHeight()-100);
		g.drawString("D", getWidth()/26*4, getHeight()-100);
		g.drawString("E", getWidth()/26*5, getHeight()-100);
		g.drawString("F", getWidth()/26*6, getHeight()-100);
		g.drawString("G", getWidth()/26*7, getHeight()-100);
		g.drawString("H", getWidth()/26*8, getHeight()-100);
		g.drawString("I", getWidth()/26*9, getHeight()-100);
		g.drawString("J", getWidth()/26*10, getHeight()-100);
		g.drawString("K", getWidth()/26*11, getHeight()-100);
		g.drawString("L", getWidth()/26*12, getHeight()-100);
		g.drawString("M", getWidth()/26*13, getHeight()-100);
		g.drawString("N", getWidth()/26*14, getHeight()-100);
		g.drawString("O", getWidth()/26*15, getHeight()-100);
		g.drawString("P", getWidth()/26*16, getHeight()-100);
		g.drawString("Q", getWidth()/26*17, getHeight()-100);
		g.drawString("R", getWidth()/26*18, getHeight()-100);
		g.drawString("S", getWidth()/26*19, getHeight()-100);
		g.drawString("T", getWidth()/26*20, getHeight()-100);
		g.drawString("U", getWidth()/26*21, getHeight()-100);
		g.drawString("V", getWidth()/26*22, getHeight()-100);
		g.drawString("W", getWidth()/26*23, getHeight()-100);
		g.drawString("X", getWidth()/26*24, getHeight()-100);
		g.drawString("Y", getWidth()/26*25, getHeight()-100);
		g.drawString("Z", getWidth()/26*26, getHeight()-100);
		g.setColor(Color.MAGENTA);
		//計算棒形圖
		g.drawLine(getWidth()/26,(getHeight()-120 - number[0]*3), getWidth()/26 , getHeight()-120 );
		g.drawLine(getWidth()/26*2,(getHeight()-120 - number[1]*3) , getWidth()/26*2 , getHeight()-120 );
		g.drawLine(getWidth()/26*3,(getHeight()-120 - number[2]*3) , getWidth()/26*3 , getHeight()-120);
		g.drawLine(getWidth()/26*4,(getHeight()-120 - number[3]*3) , getWidth()/26*4 , getHeight()-120);
		g.drawLine(getWidth()/26*5,(getHeight()-120 - number[4]*3) , getWidth()/26*5 , getHeight()-120);
		g.drawLine(getWidth()/26*6,(getHeight()-120 - number[5]*3) , getWidth()/26*6 , getHeight()-120);
		g.drawLine(getWidth()/26*7,(getHeight()-120 - number[6]*3) , getWidth()/26*7 , getHeight()-120);
		g.drawLine(getWidth()/26*8,(getHeight()-120 - number[7]*3) , getWidth()/26*8 , getHeight()-120);
		g.drawLine(getWidth()/26*9,(getHeight()-120 - number[8]*3) , getWidth()/26*9 , getHeight()-120);
		g.drawLine(getWidth()/26*10,(getHeight()-120 - number[9]*3) , getWidth()/26*10 , getHeight()-120 );
		g.drawLine(getWidth()/26*11,(getHeight()-120 - number[10]*3) , getWidth()/26*11 , getHeight()-120 );
		g.drawLine(getWidth()/26*12,(getHeight()-120 - number[11]*3) , getWidth()/26*12 , getHeight()-120);
		g.drawLine(getWidth()/26*13,(getHeight()-120 - number[12]*3) , getWidth()/26*13 , getHeight()-120);
		g.drawLine(getWidth()/26*14,(getHeight()-120 - number[13]*3) , getWidth()/26*14 , getHeight()-120);
		g.drawLine(getWidth()/26*15,(getHeight()-120 - number[14]*3) , getWidth()/26*15 , getHeight()-120);
		g.drawLine(getWidth()/26*16,(getHeight()-120 - number[15]*3) , getWidth()/26*16 , getHeight()-120);
		g.drawLine(getWidth()/26*17,(getHeight()-120 - number[16]*3) , getWidth()/26*17 , getHeight()-120);
		g.drawLine(getWidth()/26*18,(getHeight()-120 - number[17]*3) , getWidth()/26*18 , getHeight()-120);
		g.drawLine(getWidth()/26*19,(getHeight()-120 - number[18]*3) , getWidth()/26*19, getHeight()-120);
		g.drawLine(getWidth()/26*20,(getHeight()-120 - number[19]*3) , getWidth()/26*20, getHeight()-120 );
		g.drawLine(getWidth()/26*21,(getHeight()-120 - number[20]*3) , getWidth()/26*21 , getHeight()-120 );
		g.drawLine(getWidth()/26*22,(getHeight()-120 - number[21]*3) , getWidth()/26*22 , getHeight()-120);
		g.drawLine(getWidth()/26*23,(getHeight()-120 - number[22]*3) , getWidth()/26*23, getHeight()-120);
		g.drawLine(getWidth()/26*24,(getHeight()-120 - number[23]*3) , getWidth()/26*24 , getHeight()-120 );
		g.drawLine(getWidth()/26*25,(getHeight()-120 - number[24]*3) , getWidth()/26*25 , getHeight()-120 );
		g.drawLine(getWidth()/26*26,(getHeight()-120 - number[25]*3) , getWidth()/26*26 , getHeight()-120 );
	}
	
}
