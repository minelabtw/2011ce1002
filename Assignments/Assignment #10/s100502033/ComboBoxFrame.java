package a10.s100502033;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class ComboBoxFrame extends JFrame
{
	private JComboBox imagesJComboBox;
	private JLabel label;
	private JFrame f2 = new JFrame();
	private String string[] = new String[5];
	private String string1[] = new String[5];
	private JTextArea label1 = new JTextArea("");
	private static JPanel p1 = new JPanel();
	private static JPanel p2 = new JPanel();
	private static JPanel p3 = new JPanel();
	private JButton HTG = new JButton("Histogram");
	private String name[] = {"Canada" , "China" , "England" , "German" , "USA"};//comboBOx內容名稱
	private String names[] = {"image\\Canada.gif" , "image\\China.gif" , "image\\England.gif" , "image\\German.gif" , "image\\USA.gif" };//取得圖片
	private Icon icons[] = new Icon[5];
	public ComboBoxFrame()
	{
		//國家的介絕
		string1[0] = "Canada, where English and French are the official languages, is the world's second largest country in land area with urban cities, small towns, large mountain ranges and vast coastlines. It is a highly developed, stable democracy with a vibrant economy. Tourist facilities are widely available in much of the country, but the northern and wilderness areas are less developed and facilities there can be vast distances apart. Read the Department of State's Background Notes on Canada for additional information. ";
		string1[1] = "The red of the Chinese flag symbolizes the communist revolution, and it's also the traditional color of the people. The large gold star represents communism, while the four smaller stars represent the social classes of the people. In addition, the five stars together reflect the importance placed on the number five in Chinese thought and history. ";
		string1[2] = "England is the largest part of the island of Britain. In recent years it has become anation with something of an identity crisis. For example the other nations of theUnion - Wales, Scotland and Northern Ireland have strong cultural symbols whichare lacking in England. Many English people are unsure whether to describethemselves as 'English' or 'British'. It seems as though the English have no nationalidentity. The British are citizens of the UK - the United Kingdom of Great Britainand Northern Irelan";
		string1[3] = "Germany is a modern and stable democracy located in Western Europe. Tourist facilities are highly developed. In larger towns, many people can communicate in English. Read the Department of State’s Background Notes on Germany for additional information. ";
		string1[4] = "13 equal horizontal stripes of red (top and bottom) alternating with white; there is a blue rectangle in the upper hoist-side corner bearing 50 small, white, five-pointed stars arranged in nine offset horizontal rows of six stars (top and bottom) alternating with rows of five stars; the 50 stars represent the 50 states, the 13 stripes represent the 13 original colonies; known as Old Glory; the design and colors have been the basis for a number of other flags, including Chile, Liberia, Malaysia, and Puerto Rico.";
		//國家的介絕不知道如何換行所以用這個方法
		string[0] = "Canada, where English and French are the\n" +
				" official languages, is the world's second \n" +
				"largest country in land area with urban cities\n" +
				", small towns, large mountain ranges and vast\n " +
				"coastlines. It is a highly developed, stable\n" +
				" democracy with a vibrant economy. Tourist\n" +
				" facilities are widely available in much of\n" +
				" the country, but the northern and wilderness\n" +
				" areas are less developed and facilities there\n" +
				" can be vast distances apart. Read the\n " +
				"Department of State's Background Notes on \n" +
				"Canada for additional information. ";
		string[1] = "The red of the Chinese flag symbolizes \n" +
				"the communist revolution, and it's also the\n" +
				" traditional color of the people. The large\n" +
				" gold star represents communism, while the \n" +
				"four smaller stars represent the social\n " +
				"classes of the people. In addition, the five\n" +
				" stars together reflect the importance placed\n" +
				" on the number five in Chinese thought and\n " +
				"history. ";
		string[2] = "England is the largest part of the island\n" +
				" of Britain. In recent years it has become\n" +
				" anation with something of an identity crisis.\n" +
				" For example the other nations of theUnion -\n" +
				" Wales, Scotland and Northern Ireland have \n" +
				"strong cultural symbols whichare lacking in\n " +
				"England. Many English people are unsure whether\n" +
				" to describethemselves as 'English' or 'British'.\n" +
				" It seems as though the English have no \n" +
				"nationalidentity. The British are citizens of\n" +
				" the UK - the United Kingdom of Great Britainand\n" +
				" Northern Irelan";
		string[3] = "Germany is a modern and stable democracy \n" +
				"located in Western Europe. Tourist facilities \n" +
				"are highly developed. In larger towns, many people\n" +
				" can communicate in English. Read the Department\n " +
				"of State’s Background Notes on Germany for\n " +
				"additional information. ";
		string[4] = "13 equal horizontal stripes of\n" +
				" red (top and bottom) alternating with white; there\n" +
				" is a blue rectangle in the upper hoist-side corner\n " +
				"bearing 50 small, white, five-pointed stars arranged\n" +
				" in nine offset horizontal rows of \n" +
				"six stars (top and bottom) alternating with rows of\n" +
				"five stars; the 50 stars represent the 50 states, the\n" +
				" 13 stripes represent the 13 original colonies; known \n" +
				"as Old Glory; the design and colors have been the" +
				" basis for a number of other flags, including Chile, \n" +
				"Liberia, Malaysia, and Puerto Rico.";
		icons[0] = new ImageIcon("image\\Canada.gif");
		icons[1] = new ImageIcon("image\\China.gif");
		icons[2] = new ImageIcon("image\\England.gif");
		icons[3] = new ImageIcon("image\\German.gif");
		icons[4] = new ImageIcon("image\\USA.gif");
		setLayout(new FlowLayout());
		label1.setText(string[0]);
		imagesJComboBox = new JComboBox(name);//COMBOBOX的內容
		imagesJComboBox.setMaximumRowCount(4);//每次最多顯示的數目
		imagesJComboBox.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent event)
			{
				if(event.getStateChange() == ItemEvent.SELECTED)
				{
					label.setIcon(icons[imagesJComboBox.getSelectedIndex()]);//選取對應的圖
					if(imagesJComboBox.getSelectedIndex() == 0)//根據所選取的圖來變更頻色
					{
						label1.setForeground(Color.darkGray);
						label1.setText(string[0]);
					}
					else if(imagesJComboBox.getSelectedIndex() == 1)
					{
						label1.setForeground(Color.red);
						label1.setText(string[1]);
					}
					else if(imagesJComboBox.getSelectedIndex() == 2)
					{
						label1.setForeground(Color.orange);
						label1.setText(string[2]);
					}
					else if(imagesJComboBox.getSelectedIndex() == 3)
					{
						label1.setForeground(Color.MAGENTA);
						label1.setText(string[3]);
					}
					else if(imagesJComboBox.getSelectedIndex() == 4)
					{
						label1.setForeground(Color.blue);
						label1.setText(string[4]);
					}
				}	
			}
		}
		);
		label1.setForeground(Color.darkGray);
		imagesJComboBox.setPreferredSize(new Dimension(300, 50));
		label1.setPreferredSize(new Dimension(300, 500));
		p2.add(HTG , BorderLayout.NORTH);
		label = new JLabel(icons[0]);
		add(imagesJComboBox , BorderLayout.NORTH);
		p1.add(label , BorderLayout.WEST);	
		p1.add(label1 , BorderLayout.EAST);
		add(p1 , BorderLayout.CENTER);
		add(p2 , BorderLayout.SOUTH);
		
		Listner listner = new Listner();
		HTG.addActionListener(listner);
	}
	class Listner implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			if(event.getSource() == HTG)//按下HTG後的動作
			{
				f2.setTitle("Histogram"); //Frame title
				f2.setSize(1000, 750); //Frame size
				f2.setLocationRelativeTo(null);
				f2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				Histogram f3 = new Histogram(string1[imagesJComboBox.getSelectedIndex()]);//放入介絕的文字
				f3.repaint();//重新整理
				p3.removeAll();//消除資料
				p3.setLayout(new GridLayout(1,1));
				p3.add(f3);
				f2.add(p3);
				f2.setVisible(true);
				
			}
			
		}
	}
}
