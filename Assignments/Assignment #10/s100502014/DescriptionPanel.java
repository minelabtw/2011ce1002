package a10.s100502014;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.*;
public class DescriptionPanel extends JPanel {
	private int flags = 5; // how many flags
	private String[] flagTitles = {"Canada","China","England","German","USA"};
	private ImageIcon[] flagImages = {
		new ImageIcon("image/Canada.gif"),
		new ImageIcon("image/China.gif"),
		new ImageIcon("image/England.gif"),
		new ImageIcon("image/German.gif"),
		new ImageIcon("image/USA.gif")
	};
	private String[] flagDescriptions = {
			"Canada is a North American country consisting of ten provinces and three territories. Located in the northern part of the continent, it extends from the Atlantic Ocean in the east to the Pacific Ocean in the west, and northward into the Arctic Ocean. Spanning over 9.9 million square kilometres, Canada is the world's second-largest country by total area, and its common border with the United States is the longest land border in the world.",
			"China, officially the People's Republic of China (PRC), is the world's most-populous country, with a population of over 1.3 billion. The East Asian state covers approximately 9.6 million square kilometres, and is the world's second-largest country by land area, and the third- or fourth-largest in total area, depending on the definition of total area.",
			"England is a country that is part of the United Kingdom. It shares land borders with Scotland to the north and Wales to the west; the Irish Sea is to the north west, the Celtic Sea to the south west, while the North Sea to the east and the English Channel to the south separate it from continental Europe. Most of England comprises the central and southern part of the island of Great Britain in the North Atlantic. The country also includes over 100 smaller islands such as the Isles of Scilly and the Isle of Wight.",
			"Germany, officially the Federal Republic of Germany, is a federal parliamentary republic in Europe. The country consists of 16 states while the capital and largest city is Berlin. Germany covers an area of 357,021 km2 and has a largely temperate seasonal climate. With 81.8 million inhabitants, it is the most populous member state and the largest economy in the European Union. It is one of the major political powers of the European continent and a technological leader in many fields.",
			"The United States of America (commonly abbreviated to the United States, the U.S., the USA, America, and the States) is a federal constitutional republic comprising fifty states and a federal district. The country is situated mostly in central North America, where its forty-eight contiguous states and Washington, D.C., the capital district, lie between the Pacific and Atlantic Oceans, bordered by Canada to the north and Mexico to the south. The state of Alaska is in the northwest of the continent, with Canada to the east and Russia to the west, across the Bering Strait. The state of Hawaii is an archipelago in the mid-Pacific. The country also possesses several territories in the Pacific and Caribbean."
		};
	private JLabel jlb = new JLabel();
	private JTextArea jta =new JTextArea();
	
	public DescriptionPanel() {
		setDisplay(0); // initialize
		
		//set position of image and string
		jlb.setHorizontalAlignment(JLabel.CENTER);
		jlb.setHorizontalTextPosition(JLabel.CENTER);
		jlb.setVerticalTextPosition(JLabel.BOTTOM);
		
		//set font
		jlb.setFont(new Font("Serif", Font.BOLD, 40));
		jta.setFont(new Font("Serif", Font.PLAIN, 20));
		
		//set text properties
		jta.setLineWrap(true);
		jta.setWrapStyleWord(true);
		jta.setEditable(false);
		
		setLayout(new GridLayout(1,2));
		add(jlb);
		add(new JScrollPane(jta));
	}
	
	public int getFlags() {
		return flags;
	}
	public String[] getTitles() {
		return flagTitles;
	}
	public String getText() {
		return jta.getText();
	}
	public void setDisplay(int i) {
		jlb.setText(flagTitles[i]);
		jlb.setIcon(flagImages[i]);
		jta.setText(flagDescriptions[i]);
	}
	
}
