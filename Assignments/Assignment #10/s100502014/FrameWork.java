package a10.s100502014;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.*;
public class FrameWork extends JFrame {
	private DescriptionPanel descriptionPanel = new DescriptionPanel();
	private String[] flagTitles = descriptionPanel.getTitles();
	private JComboBox jcb = new JComboBox(flagTitles);
	private JButton jbn = new JButton("Show Histogram");
	private Histogram histogram = new Histogram();
	private JFrame histogramFrame = new JFrame();
	
	public FrameWork() {
		//main frame
		add(jcb, BorderLayout.NORTH);
		add(descriptionPanel, BorderLayout.CENTER);
		add(jbn, BorderLayout.SOUTH);
		
		//histogram frame
		histogramFrame.add(histogram);
		histogramFrame.setSize(800,700);
		
		//add listener
		jcb.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				descriptionPanel.setDisplay(jcb.getSelectedIndex());
			}
		});
		jbn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				histogram.showHistogram(countLetters());
				histogramFrame.setVisible(true);
			}
		});		
	}
	
	//count how many letters in description
	public int[] countLetters() {
		int[] count = new int[26];
		String text = descriptionPanel.getText();
		for(int i=0;i<text.length();i++) {
			char character = text.charAt(i);
			if((character >= 'A') && (character<= 'Z')) { //A to Z
				count[character - 'A']++;
			}
			
			else if((character >= 'a') && (character<= 'z')) { //a to z
				count[character - 'a']++;
			}
		}
		return count;
	}
}
