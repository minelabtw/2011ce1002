package a10.s100502014;
import java.awt.*;
import java.awt.color.ColorSpace;

import javax.swing.*;
public class Histogram extends JPanel {
	private int[] count;
	public Histogram() {
	}
	
	//repaint the histogram
	public void showHistogram(int[] count1) {
		count = count1;
		repaint();
	}
	
	public void paintComponent(Graphics g) {
		if(count == null) return; //no display if count is null
		
		super.paintComponent(g);
		
		int width = getWidth() / 50; //bar's width
		int maxCount = 0; //the max count in all letters
		
		//draw a line under bars
		g.drawLine(0, getHeight()*8/10, getWidth(), getHeight()*8/10);
		
		//find the max count
		for(int i=0;i<26;i++) {
			if(maxCount < count[i])
				maxCount = count[i];
		}
		
		//set rainbow color
		int[][] color = {{255,0,0}, {255,30,0}, {255,60,0}, {255,90,0}, {255,120,0},
					   {255,150,0}, {255,180,0}, {255,210,0}, {255,240,100}, {255,255,0},
					   {0,255,0}, {0,255,50}, {0,255,100}, {0,255,150}, {0,255,200},
					   {0,255,255},{0,0,255}, {30,0,255}, {60,0,255}, {90,0,255}, 
					   {120,0,255},{150,0,255}, {180,0,255}, {210,0,255}, {240,0,255}, {255,0,255}};
		
		for(int i=0;i<26;i++) {
			int height = (int)(getHeight()*0.6*((double)count[i]/maxCount)); // bar's height
			
			// draw the count of letters
			g.setColor(Color.BLACK);
			g.setFont(new Font("Sarif",Font.BOLD,20));
			g.drawString(count[i]+"", getWidth()*(i+1)/28, getHeight()*8/10-height-10);
			
			// draw bars
			g.setColor(new Color(color[i][0],color[i][1],color[i][2]));
			g.fillRect(getWidth()*(i+1)/28,  getHeight()*8/10-height, width, height); 
			
			// draw letters
			g.setColor(Color.BLACK);
			g.drawString((char)(65+i)+"", getWidth()*(i+1)/28, getHeight()*8/10+20);
		}
	}
}
