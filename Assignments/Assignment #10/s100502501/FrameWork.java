package a10.s100502501;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class FrameWork extends JFrame{
	private String[] flagTitles={"Canada","China","United Kingdom (UK)","Germany","The United States of America (USA)"};
	private ImageIcon[] flagImage={new ImageIcon("image/Canada.gif"),new ImageIcon("image/China.gif"),
		    new ImageIcon("image/England.gif"),new ImageIcon("image/German.gif"),new ImageIcon("image/USA.gif")};
	private String[] fDescription=new String[5];
	private DescriptionPanel descriptionPanel=new DescriptionPanel(); //create a description panel
	private JComboBox jcbo=new JComboBox(flagTitles); //create a combo box for selecting countries
	private JButton jbt=new JButton("Show Histogram");
	private Histogram histogram=new Histogram();
	private JFrame histogramFrame=new JFrame();
	
	public FrameWork(){ //set text description
		fDescription[0]="The Maple Leaf flag \n\n"+"The Canadian National Flag was adopted by the Canadian " +
      "Parliament on October 22, 1964 and was proclaimed into law "+"by Her Majesty Queen Elizabeth II (the Queen of Canada) on " +
      "February 15, 1965. The Canadian Flag (colloquially known "+"as The Maple Leaf Flag) is a red flag of the proportions " +
      "two by length and one by width, containing in its center a "+"white square, with a single red stylized eleven-point " +
      "mapleleaf centered in the white square.";
		fDescription[1]="China,officially the People's Republic of China (PRC), is the world's most-populous country,with a population of over 1.3 billion. "+
      "The East Asian state covers approximately 9.6 million square kilometres, and is the world's second-largest country by land area, and the third- or fourth-largest in total area.";
		fDescription[2]="United Kingdom is a developed country and has the world's seventh-largest economy by nominal GDP and eighth-largest economy by purchasing power parity. "+
	  "It was the world's first industrialised country and the world's foremost power during the 19th and early 20th centuries.";
		fDescription[3]="Germany, officially the Federal Republic of Germany,is a federal parliamentary republic in Europe. "+
	  "The country consists of 16 states while the capital and largest city is Berlin. Germany covers an area of 357,021 km2 and has a largely temperate seasonal climate.";
		fDescription[4]="The United States of America ,independent from the Kingdom of Great Britain, is declared on July 4, 1776. "+
	  "At 3.79 million square miles (9.83 million km2) and with over 312 million people,it's the third or fourth largest country by total area, and the third largest by both land area and population. \n"+
	  "\nThe U.S. economy is the world's largest national economy, with an estimated 2011 GDP of $15.1 trillion (22% of nominal global GDP and over 19% of global GDP at purchasing-power parity).Per capita income is the world's sixth-highest.";
		
		setDisplay(0); //set the first item for display
		add(jcbo,BorderLayout.NORTH);
	    add(descriptionPanel,BorderLayout.CENTER);
	    add(jbt,BorderLayout.SOUTH);

	    jcbo.addItemListener(new ItemListener(){
	      public void itemStateChanged(ItemEvent e) {
	        setDisplay(jcbo.getSelectedIndex()); //set display information
	      }
	    });
	    jbt.addActionListener(new ActionListener(){
	    	public void actionPerformed(ActionEvent e){
		        int[] count=countLetters();
		        histogram.showHistogram(count);
		        histogramFrame.setVisible(true);
		    }
		});
		histogramFrame.add(histogram); //create a new frame to hold the histogram panel
	    histogramFrame.pack();
	    histogramFrame.setTitle("Histogram");
	}
	
	public void setDisplay(int index){
	    descriptionPanel.setImageIcon(flagImage[index]);
	    descriptionPanel.setDescription(fDescription[index]);
	}
	private int[] countLetters(){ //count the letters in the text area
	    int[] count=new int[26]; //count for 26 letters
	    String text=descriptionPanel.jta.getText(); //get contents from the text area

	    for (int i=0;i<text.length();i++){
	      char character=text.charAt(i);
	      if((character>='A')&&(character<='Z')){
	        count[(int)character-65]++;
	      }
	      else if((character>='a')&&(character<='z')){
	        count[(int)character-97]++;
	      }
	    }
	    return count;
	}
}