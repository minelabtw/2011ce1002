package a10.s100502501;
import javax.swing.JFrame;
public class A101 {
	public static void main(String[] args) {
		FrameWork frame = new FrameWork();
		frame.pack();
	    frame.setTitle("Assignment #10");
	    frame.setSize(650,300);
	    frame.setLocationRelativeTo(null); // Center the frame   
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setVisible(true);
	}
}