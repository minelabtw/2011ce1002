package a10.s100502501;
import javax.swing.*;
import java.awt.*;
public class DescriptionPanel extends JPanel{
	private JLabel jlb = new JLabel();
	protected JTextArea jta = new JTextArea();
	public DescriptionPanel(){
		jlb.setHorizontalAlignment(JLabel.CENTER); // Center the icon
		jta.setFont(new Font("Calibri",Font.BOLD,15)); // Set the font of the text field
		jta.setLineWrap(true); // Set lineWrap and wrapStyleWord true
		jta.setWrapStyleWord(true);
		jta.setEditable(false);

		JScrollPane scrollPane=new JScrollPane(jta); // Create a scroll pane
		setLayout(new BorderLayout()); // Set BorderLayout for the panel
		add(jlb,BorderLayout.WEST);
		add(scrollPane,BorderLayout.CENTER); //add label and scrollpane
	}
	public void setImageIcon(ImageIcon icon){
		jlb.setIcon(icon);
	}
	public void setDescription(String text){
		jta.setText(text);
	}
}