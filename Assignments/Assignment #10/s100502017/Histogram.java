package a10.s100502017;
import javax.swing.*;
import java.awt.*;

public class Histogram extends JPanel{
	private final int countnum=26;
	private int[] count=new int[countnum];
	public void setcount(int[] c){//to set array and renew picture
		count=c;
		repaint();
	}
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		int w=getWidth();
		int h=getHeight();
		double interval=(w-40)/countnum;
		double individualWidth=((w-40)/24)*0.60;
		int maxCount=1;
		for(int i=0;i<countnum;i++){//determine the max count
			if(maxCount<count[i])
				maxCount=count[i];
		}
		int x=30;
		g.drawLine(10, h-45, w-10, h-45);
		for(int j=0;j<countnum;j++){//draw gram
			int barHeight=(int)((double)count[j]/(double)maxCount*(h-55));
			g.setColor(new Color(225-(180/countnum)*j,(220/countnum*2)*(countnum/2-Math.abs(countnum/2-j)),(220/countnum)*j));
			g.fillRect(x,h-45-barHeight,(int)individualWidth,barHeight);
			g.drawString((char)(65+j)+"",x,h-30);
			x+=interval;
		}
	}
	public Dimension getPreferredSize(){//override getPreferredSize
		return new Dimension(300,300);
	}
}
