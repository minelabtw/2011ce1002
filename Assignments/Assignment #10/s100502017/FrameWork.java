package a10.s100502017;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class FrameWork extends JFrame implements ActionListener{
	private String[] FlagTitle={"Canada","China","England","German","USA"};
	private String[] Description=new String[5];
	private ImageIcon[] FlagImage={//read image data
			new ImageIcon("image/Canada.gif"),
			new ImageIcon("image/China.gif"),
			new ImageIcon("image/England.gif"),
			new ImageIcon("image/German.gif"),
			new ImageIcon("image/USA.gif"),
	};
	JLabel lb=new JLabel();
	JTextArea ta=new JTextArea();
	JComboBox cb=new JComboBox(FlagTitle);
	JButton His=new JButton("Histogram");
	JPanel desPan=new JPanel();
	JFrame HisFrame=new JFrame();//Another Frame
	Histogram gram=new Histogram();
	public FrameWork(){
		Description[0]="The land that is now Canada has been inhabited for millennia by various Aboriginal peoples. Beginning in the late 15th century, British and French expeditions explored, and later settled, along the region's Atlantic coast. France ceded nearly all of its colonies in North America in 1763 after the Seven Years' War. In 1867, with the union of three British North American colonies through Confederation, Canada was formed as a federal dominion of four provinces. This began an accretion of provinces and territories and a process of increasing autonomy from the United Kingdom. This widening autonomy was highlighted by the Balfour Declaration of 1926 and reaffirmed by the Statute of Westminster of 1931, which declared self-governing dominions within the British Empire to be equal. The Canada Act of 1982 finally severed the vestiges of legal dependence on the British Parliament.";
		Description[1]="China's landscape is vast and diverse, with forest steppes and the Gobi and Taklamakan deserts occupying the arid north and northwest near Mongolia and Central Asia, and subtropical forests being prevalent in the wetter south near Southeast Asia. The terrain of western China is rugged and elevated, with the Himalaya, Karakoram, Pamir and Tian Shan mountain ranges separating China from South and Central Asia. The world's apex, Mt. Everest (8,848 m), lies on the China–Nepal border, while the world's second-highest point, K2 (8,611 m), is situated on China's border with Pakistan. The country's lowest and the world's third-lowest point, Lake Ayding (−154 m), is located in the Turpan Depression. The Yangtze and Yellow Rivers, the third- and sixth-longest in the world, have their sources in the Tibetan Plateau and continue to the densely populated eastern seaboard. China's coastline along the Pacific Ocean is 14,500 kilometres (9,000 mi) long—the 11th-longest in the world—and is bounded by the Bohai, Yellow, East and South China Seas.";
		Description[2]="The area now called England was first inhabited by modern humans during the Upper Palaeolithic period, but it takes its name from the Angles, one of the Germanic tribes who settled during the 5th and 6th centuries. England became a unified state in AD 927, and since the Age of Discovery, which began during the 15th century, has had a significant cultural and legal impact on the wider world. The English language, the Anglican Church, and English law—the basis for the common law legal systems of many other countries around the world—developed in England, and the country's parliamentary system of government has been widely adopted by other nations. The Industrial Revolution began in 18th-century England, transforming its society into the world's first industrialised nation. England's Royal Society laid the foundations of modern experimental science.";
		Description[3]="A region named Germania, inhabited by several Germanic peoples, was documented before AD 100. During the Migration Period, the Germanic tribes expanded southward, and established successor kingdoms throughout much of Europe. Beginning in the 10th century, German territories formed a central part of the Holy Roman Empire.During the 16th century, northern German regions became the centre of the Protestant Reformation while southern and western parts remained dominated by Roman Catholic denominations, with the two factions clashing in the Thirty Years' War,";
		Description[4]="The United States of America (commonly abbreviated to the United States, the U.S., the USA, America, and the States) is a federal constitutional republic comprising fifty states and a federal district. The country is situated mostly in central North America, where its forty-eight contiguous states and Washington, D.C., the capital district, lie between the Pacific and Atlantic Oceans, bordered by Canada to the north and Mexico to the south. The state of Alaska is in the northwest of the continent, with Canada to the east and Russia to the west, across the Bering Strait. The state of Hawaii is an archipelago in the mid-Pacific. The country also possesses several territories in the Pacific and Caribbean.";
		ta.setLineWrap(true);//to set TextArea wrap 
		ta.setWrapStyleWord(true);
		ta.setEditable(false);
		setDisplay(0);//default display
		desPan.setLayout(new GridLayout(1,2));
		desPan.add(lb);
		JScrollPane sp=new JScrollPane(ta);//add Scroll
		desPan.add(sp);
		add(cb,BorderLayout.NORTH);
		add(desPan,BorderLayout.CENTER);
		add(His,BorderLayout.SOUTH);
		HisFrame.add(gram);//set Another Frame
		HisFrame.pack();
		HisFrame.setTitle("Histogram");
		His.addActionListener(this);
		cb.addItemListener(new ItemListener(){//item event is here
			public void itemStateChanged(ItemEvent e){
				setDisplay(cb.getSelectedIndex());
			}
		});
	}
	public void setDisplay(int i){//according box choice decide display
		lb.setIcon(FlagImage[i]);
		ta.setText(Description[i]);
	}
	public int[] counterLetters(){//to count letters times
		int[] count=new int[26];
		String s=ta.getText();
		for(int i=0;i<s.length();i++){
			if(s.charAt(i)>='A'&&s.charAt(i)<='Z')
				count[s.charAt(i)-'A']++;
			else if(s.charAt(i)>='a'&&s.charAt(i)<='z')
				count[s.charAt(i)-'a']++;
		}
		return count;
	}
	public void actionPerformed(ActionEvent e){//button event is here
		if (e.getSource()==His){
			gram.setcount(counterLetters());
			HisFrame.setVisible(true);
		}
		
	}
}
