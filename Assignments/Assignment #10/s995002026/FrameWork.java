package a10.s995002026;

import javax.swing.*;

import java.awt.*;

public class FrameWork extends JFrame{

	private ImageIcon icon[]={new ImageIcon("image/Canada.gif"),new ImageIcon("image/China.gif"),new ImageIcon("image/England.gif"),new ImageIcon("image/German.gif"),new ImageIcon("image/USA.gif")};
	
	private JButton histogram=new JButton("histogram");
	
	private String des[]={"Canada","China","England","German","USA"};
	
	private JComboBox box=new JComboBox(des);
	
	private JPanel p1=new JPanel();
	
	private JPanel p3=new JPanel(new GridLayout(1,2));
	
	private JPanel p2=new JPanel();
	
	private JPanel p4=new JPanel(new GridLayout(3,1));
	
	private String description[]={"Canada  is a North American country consisting of ten provinces and three territories. Located in the northern part of the continent, it extends from the Atlantic Ocean in the east to the Pacific Ocean in the west, and northward into the Arctic Ocean. Spanning over 9.9 million square kilometres, Canada is the world's second-largest country by total area, and its common border with the United States is the longest land border in the world.",
				"China , officially the People's Republic of China (PRC), is the world's most-populous country, with a population of over 1.3 billion. The East Asian state covers approximately 9.6 million square kilometres, and is the world's second-largest country by land area,[13] and the third- or fourth-largest in total area, depending on the definition of total area.",
				"England is a country that is part of the United Kingdom. It shares land borders with Scotland to the north and Wales to the west; the Irish Sea is to the north west, the Celtic Sea to the south west, while the North Sea to the east and the English Channel to the south separate it from continental Europe. Most of England comprises the central and southern part of the island of Great Britain in the North Atlantic. The country also includes over 100 smaller islands such as the Isles of Scilly and the Isle of Wight.",
				"something from or relating to Germany",
				"The United States of America (commonly abbreviated to the United States, the U.S.)"};
	
	private JTextArea ta=new JTextArea();
	
	private JLabel label=new JLabel();
	
	
	
	public FrameWork(){
		p1.add(box);
		p4.add(p1);
		
		ta.setText(description[0]);
		label.setIcon(icon[0]);
		p3.add(label);
		p3.add(ta);
		p4.add(p3);
		ta.setLineWrap(true);
		ta.setWrapStyleWord(true);
		ta.setEditable(true);
		
		p2.add(histogram,BorderLayout.SOUTH);
		p4.add(p2);
		
		add(p4);
	}

}
