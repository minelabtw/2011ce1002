package a10.s100502511;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class A101 extends JFrame {
	private String[] flagTitles = { "Canada", "China", "England", "USA",
			"German" };
	private ImageIcon[] flagImage = { new ImageIcon("image/Canada.gif"),
			new ImageIcon("image/China.gif"),
			new ImageIcon("image/England.gif"), new ImageIcon("image/USA.gif"),
			new ImageIcon("image/German.gif") };
	private String[] flagDescription = new String[5];
	private DescriptionPanel descriptionPanel = new DescriptionPanel();  
	private JComboBox jcbo = new JComboBox(flagTitles);  // 建立ComboBox
	private Histogram histogram = new Histogram();  // 建立Histogram
	private JButton jbtShowHistogram = new JButton("Histogram");  
	private JFrame histogramFrame = new JFrame();
	private JTextArea jtaDescription;

	public static void main(String args[]) {
		A101 f = new A101();
		f.setTitle("Asignment 10-1");
		f.setSize(700, 500);
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}

	public A101() {
		jtaDescription.setLineWrap(true);
		jtaDescription.setWrapStyleWord(true);
		jtaDescription.setEditable(false);
		jtaDescription.setFont(new Font("Serif", Font.PLAIN, 14));  // 設定字型

		flagDescription[0] = "Canada is a North American country consisting of ten provinces and three territories. Located in the northern part of the continent, it extends from the Atlantic Ocean in the east to the Pacific Ocean in the west, and northward into the Arctic Ocean. Spanning over 9.9 million square kilometres, Canada is the world's second-largest country by total area, and its common border with the United States is the longest land border in the world.";
		flagDescription[1] = "China , officially the People's Republic of China (PRC), is the world's most-populous country, with a population of over 1.3 billion. The East Asian state covers approximately 9.6 million square kilometres, and is the world's second-largest country by land area, and the third- or fourth-largest in total area, depending on the definition of total area.";
		flagDescription[2] = "England is a country that is part of the United Kingdom. It shares land borders with Scotland to the north and Wales to the west; the Irish Sea is to the north west, the Celtic Sea to the south west, while the North Sea to the east and the English Channel to the south separate it from continental Europe. Most of England comprises the central and southern part of the island of Great Britain in the North Atlantic. The country also includes over 100 smaller islands such as the Isles of Scilly and the Isle of Wight.";
		flagDescription[3] = "Germany , officially the Federal Republic of Germany ,is a federal parliamentary republic in Europe. The country consists of 16 states while the capital and largest city is Berlin. Germany covers an area of 357,021 km2 and has a largely temperate seasonal climate. With 81.8 million inhabitants, it is the most populous member state and the largest economy in the European Union. It is one of the major political powers of the European continent and a technological leader in many fields.";
		flagDescription[4] = "The United States of America (commonly abbreviated to the United States, the U.S., the USA, America, and the States) is a federal constitutional republic comprising fifty states and a federal district. The country is situated mostly in central North America, where its forty-eight contiguous states and Washington, D.C., the capital district, lie between the Pacific and Atlantic Oceans, bordered by Canada to the north and Mexico to the south. The state of Alaska is in the northwest of the continent, with Canada to the east and Russia to the west, across the Bering Strait. The state of Hawaii is an archipelago in the mid-Pacific. The country also possesses several territories in the Pacific and Caribbean.";

		setDisplay(0);  // 設定Canada為第一個顯示的圖形

		add(jcbo, BorderLayout.NORTH);
		add(descriptionPanel, BorderLayout.CENTER);

		jcbo.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				setDisplay(jcbo.getSelectedIndex());
			}
		});

		jbtShowHistogram.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int[] count = countLetters();
				histogram.showHistogram(count);
				histogramFrame.setVisible(true);
			}
		});

		add(jbtShowHistogram, BorderLayout.SOUTH);
		histogramFrame.add(histogram);
		histogramFrame.pack();
		histogramFrame.setTitle("Histogram");
	}

	private int[] countLetters() {
		int[] count = new int[26];
		String text = jtaDescription.getText();

		for (int i = 0; i < text.length(); i++) {  // 計算出各字母有幾個
			char character = text.charAt(i);
			if ((character >= 'A') && (character <= 'Z')) {
				count[character - 'A']++;
			} else if ((character >= 'a') && (character <= 'z')) {
				count[character - 'a']++;
			}
		}
		return count;
	}

	public void setDisplay(int index) {
		descriptionPanel.setTitle(flagTitles[index]);
		descriptionPanel.setImageIcon(flagImage[index]);
		descriptionPanel.setDescription(flagDescription[index]);
	}

	public class DescriptionPanel extends JPanel {
		private JLabel jlblImageTitle = new JLabel();

		public DescriptionPanel() {
			jlblImageTitle.setHorizontalAlignment(JLabel.CENTER);
			jlblImageTitle.setHorizontalTextPosition(JLabel.CENTER);
			jlblImageTitle.setVerticalTextPosition(JLabel.BOTTOM);

			jlblImageTitle.setFont(new Font("SansSerif", Font.BOLD, 16));

			JScrollPane scrollPane = new JScrollPane(  // 建立ScrollPane
					jtaDescription = new JTextArea());

			setLayout(new BorderLayout(5, 5));
			add(scrollPane, BorderLayout.CENTER);
			add(jlblImageTitle, BorderLayout.WEST);
		}

		public void setTitle(String title) {
			jlblImageTitle.setText(title);
		}

		public void setImageIcon(ImageIcon icon) {
			jlblImageTitle.setIcon(icon);
		}

		public void setDescription(String text) {
			jtaDescription.setText(text);
		}
	}

	public class Histogram extends JPanel {
		private int[] count;

		public void showHistogram(int[] count) {  // 顯示Histogram
			this.count = count;
			repaint();
		}

		protected void paintComponent(Graphics g) {  // 畫出Histogram
			if (count == null)
				return;

			super.paintComponent(g);

			int width = getWidth();
			int height = getHeight();
			int interval = (width - 40) / count.length;
			int individualWidth = (int) (((width - 40) / 24) * 0.6);

			int maxCount = 0;
			for (int i = 0; i < count.length; i++) {
				if (maxCount < count[i])
					maxCount = count[i];
			}
			int x = 30;
			g.setColor(new Color(0, 0, 0));
			g.drawLine(10, height - 45, width - 10, height - 45);  
			for (int i = 0; i < count.length; i++) {
				int barHeight = (int) (((double) count[i] / (double) maxCount) * (height - 55));
				g.fillRect(x, height - 45 - barHeight, individualWidth,
						barHeight);
				g.drawString((char) (65 + i) + "", x, height - 30);
				x += interval;
			}
		}

		public Dimension getPreferredSize() {
			return new Dimension(300, 300);
		}
	}
}
