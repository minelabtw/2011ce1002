package a10.s995002020;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
public class A101 extends JPanel{
	private Country countrys[]= {//Country items 
	new Country("Canada is a North American country consisting of ten provinces and three territories." , "C:/Users/Kenter/workspace/A10/src/a10/image/Canada.gif"),
	new Country("The United Kingdom of Great Britain and Northern Ireland is a sovereign state located off the north-western coast of continental Europe.","C:/Users/Kenter/workspace/A10/src/a10/image/England.gif"),
	new Country("China is the world's most-populous country.","C:/Users/Kenter/workspace/A10/src/a10/image/China.gif"),
	new Country("Germany is a federal parliamentary republic in Europe. ","C:/Users/Kenter/workspace/A10/src/a10/image/German.gif"),
	new Country("USA is a federal constitutional republic comprising fifty states and a federal district.","C:/Users/Kenter/workspace/A10/src/a10/image/USA.gif"),
	};
	Map CountryMap = new HashMap();//parameter
	
	public A101(){
		for(int i = 0 ; i<countrys.length;i++){
			CountryMap.put(countrys[i].getdescription(),countrys[i]);
		}
	setLayout(new BorderLayout());
	
	JComboBox countryCombo = new JComboBox(countrys);
	countryCombo.setEditable(true);
	countryCombo.setEditor(new Combobox(CountryMap,countrys[0]));
	countryCombo.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
			System.out.println(((JComboBox)e.getSource()).getSelectedItem() + "!");
			
			
		}
	});
	countryCombo.setActionCommand("Hello");
	add(countryCombo,BorderLayout.CENTER);
	}
	
	public static void main (String []argv){//main function
		JFrame frame = new JFrame ("A101");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setContentPane(new A101());
		frame.pack();
		frame.setVisible(true);
	}


}
