package a10.s995002020;
import javax.swing.ImageIcon;
public class Country {
	private final String imagePath;
	private final String des;
	private ImageIcon image;
	
	public Country(String des,String imagePath){
		this.des=des;
		this.imagePath=imagePath;
		}
	public String getdescription(){//description
		return des;
	}
	
	public ImageIcon getImage(){
		if(image==null)
			image = new ImageIcon(imagePath);//image's path
		
	return image;
	}
	
    public String toString(){return des;}//override the Label
}
