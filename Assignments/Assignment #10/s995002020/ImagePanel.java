package a10.s995002020;
import   java.awt.*; 
import   java.awt.event.*;  
import   javax.swing.*; 
import   javax.swing.border.*; 

class   ImagePanel extends JPanel   { //use this class to draw Image's Panel
    
    JLabel   imageIconLabel; 
    JTextField   textField; 

    public   ImagePanel(Country   initialEntry)   { 
            setLayout(new  BorderLayout()); 

            imageIconLabel   =   new   JLabel(initialEntry.getImage()); 
            imageIconLabel.setBorder(new   BevelBorder(BevelBorder.RAISED)); 

            textField   =   new   JTextField(initialEntry.getdescription()); 
            textField.setColumns(45); 
            textField.setBorder(new   BevelBorder(BevelBorder.LOWERED)); 

            add(imageIconLabel,   BorderLayout.WEST); 
            add(textField,   BorderLayout.EAST); 
    } 

    public   void   setText(String   s)   {   textField.setText(s);   } 
    public   String   getText()   {   return   (textField.getText());   } 

    public   void   setIcon(Icon   i)   { 
            imageIconLabel.setIcon(i); 
            repaint();
            
    } 
    public   void   selectAll()   {   textField.selectAll();   } 

    public   void   addActionListener(ActionListener   l)   { 
            textField.addActionListener(l);   
    } 
    public   void   removeActionListener(ActionListener   l)   { 
            textField.removeActionListener(l);   
    } 
}