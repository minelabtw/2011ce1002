package a10.s995002020;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

public class Combobox implements ComboBoxEditor{//ComboBox
	Map map;
	ImagePanel panel;
	ImageIcon questionIcon;
	
	public Combobox(Map m,Country defaultchoose){//constructor
		map = m;
		panel = new ImagePanel(defaultchoose);
		questionIcon = new ImageIcon("question.gif");
		
	}
	
	public void setItem(Object anObject){//set country item
		if(anObject != null){
			panel.setText(anObject.toString());
			Country country =(Country)map.get(anObject.toString());
			if(country != null)
				panel.setIcon(country.getImage());
			else
				panel.setIcon(questionIcon);
		}
	}
	
	public Component getEditorComponent(){return panel;}
	
	public Object getItem(){return panel.getText();}
	public void selectAll(){panel.selectAll();}
	 public   void   addActionListener(ActionListener   l)   { 
         panel.addActionListener(l);   
 } 

 public   void   removeActionListener(ActionListener   l)   { 
         panel.removeActionListener(l);   
 } 
	
	

}
