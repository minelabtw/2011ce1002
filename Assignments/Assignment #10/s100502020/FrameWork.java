package a10.s100502020;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class FrameWork extends JFrame implements ActionListener{
	private String[] contries = { "Canada", "China", "England", "German", "USA" };
	private ImageIcon[] flags = { new ImageIcon("image/Canada.gif"),new ImageIcon("image/China.gif"),new ImageIcon("image/England.gif"),new ImageIcon("image/German.gif"),new ImageIcon("image/USA.gif")};
	private String[] Fdescript = new String[5];//store countries description
	private DescriptionPanel DescriptPanel = new DescriptionPanel();		
	private JFrame HFrame = new JFrame();
	private Histogram histogram = new Histogram();
	private JButton display = new JButton("Histogram");
	private JComboBox combobox = new JComboBox(contries);//choose the country
	
	
	public FrameWork()
	{
		Fdescript[0] = "Canada, its cold there..."; //description
		Fdescript[1] = "China, the country with the most popularity...";
		Fdescript[2] = "England, London is the capital city there...";
		Fdescript[3] = "German, famous in making cars...";
		Fdescript[4] = "USA, powful military and the biggest movie industries...";
		
		setDisplay(0);//default choice , Canada
		
		
		add(combobox, BorderLayout.NORTH);
		add(display, BorderLayout.SOUTH);
		add(DescriptPanel, BorderLayout.CENTER);
		
		
		display.addActionListener(this);
		
		
		combobox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				setDisplay(combobox.getSelectedIndex());
			}
		});
		HFrame.add(histogram);
		HFrame.pack();
		HFrame.setTitle("Histogram");
	}
	public void setDisplay(int index)
	{
		DescriptPanel.setTitle(contries[index]);
		DescriptPanel.setImageIcon(flags[index]);
		DescriptPanel.setDescription(Fdescript[index]);
	}
	private int[] countLetters() {//calculate letters
		int[] count = new int[26];
		String text = DescriptPanel.getarea().getText();
		for (int i = 0; i < text.length(); i++) 
		{
			char character = text.charAt(i);
			if ((character >= 'A') && (character <= 'Z'))
			{
				count[character - 'A']++;
			} 
			else if ((character >= 'a') && (character <= 'z'))
			{
				count[character - 'a']++;
			}
		}
		return count;
	}
	public void actionPerformed(ActionEvent e) {//if press histogram, output..
		if (e.getSource() == display) {
			int[] count = countLetters();
			histogram.showHistogram(count);
			HFrame.setVisible(true);

		}
	}
}
