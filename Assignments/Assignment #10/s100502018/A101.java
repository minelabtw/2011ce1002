package a10.s100502018;

import javax.swing.*;

public class A101 extends JFrame {
	public static void main(String[] args) {
		FrameWork myFrameWork = new FrameWork();
		myFrameWork.setTitle("Assignment #10");
		myFrameWork.setSize(700, 400);
		myFrameWork.setLocationRelativeTo(null);
		myFrameWork.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		myFrameWork.setVisible(true);
	}
}