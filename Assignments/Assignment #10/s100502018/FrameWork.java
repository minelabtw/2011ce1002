package a10.s100502018;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener {
	private String[] flagTitles = { "Canada", "China", "Germany",
			"United Kingdom", "USA" };
	private ImageIcon[] flagImage = {
			new ImageIcon("src/a10/s100502018/image/Canada.gif"),
			new ImageIcon("src/a10/s100502018/image/China.gif"),
			new ImageIcon("src/a10/s100502018/image/Germany.gif"),
			new ImageIcon("src/a10/s100502018/image/UK.gif"),
			new ImageIcon("src/a10/s100502018/image/USA.gif") };
	private String[] flagDescription = {
			"Canada is a North American country consisting of ten provinces and three territories. Located in the northern part of the continent, it extends from the Atlantic Ocean in the east to the Pacific Ocean in the west, and northward into the Arctic Ocean. Spanning over 9.9 million square kilometres, Canada is the world's second-largest country by total area, and its common border with the United States is the longest land border in the world.",
			"China, officially the People's Republic of China (PRC), is the world's most-populous country, with a population of over 1.3 billion. The East Asian state covers approximately 9.6 million square kilometres, and is the world's second-largest country by land area, and the third- or fourth-largest in total area, depending on the definition of total area.",
			"Germany, officially the Federal Republic of Germany, is a federal parliamentary republic in Europe. The country consists of 16 states while the capital and largest city is Berlin. Germany covers an area of 357,021 km2 and has a largely temperate seasonal climate. With 81.8 million inhabitants, it is the most populous member state and the largest economy in the European Union. It is one of the major political powers of the European continent and a technological leader in many fields.",
			"The United Kingdom of Great Britain and Northern Ireland is a sovereign state located off the north-western coast of continental Europe. The country includes the island of Great Britain, the north-eastern part of the island of Ireland and many smaller islands. Northern Ireland is the only part of the UK that shares a land border with another sovereign state�Xthe Republic of Ireland. Apart from this land border the UK is surrounded by the Atlantic Ocean, the North Sea, the English Channel and the Irish Sea.",
			"The United States of America is a federal constitutional republic comprising fifty states and a federal district. The country is situated mostly in central North America, where its forty-eight contiguous states and Washington, D.C., the capital district, lie between the Pacific and Atlantic Oceans, bordered by Canada to the north and Mexico to the south. The state of Alaska is in the northwest of the continent, with Canada to the east and Russia to the west, across the Bering Strait. The state of Hawaii is an archipelago in the mid-Pacific. The country also possesses several territories in the Pacific and Caribbean." };
	private JComboBox jcbo = new JComboBox(flagTitles);
	private JLabel jlblImageTitle = new JLabel();
	private JTextArea jtaDescriptionArea = new JTextArea();
	private JButton jbtShowHistogram = new JButton("Show Histogram");
	private JPanel jpnlCountry = new JPanel();
	private JPanel jpnlTotal = new JPanel();
	private Histogram histogram = new Histogram();
	private JFrame histogramFrame = new JFrame();

	public FrameWork() {
		jpnlCountry.add(jlblImageTitle);
		jlblImageTitle.setHorizontalAlignment(JLabel.CENTER);
		jlblImageTitle.setHorizontalTextPosition(JLabel.CENTER);
		jlblImageTitle.setVerticalTextPosition(JLabel.BOTTOM);
		jpnlCountry.add(jtaDescriptionArea);
		jtaDescriptionArea.setLineWrap(true);
		jtaDescriptionArea.setWrapStyleWord(true);
		jtaDescriptionArea.setEditable(false);
		JScrollPane scrollPane = new JScrollPane(jtaDescriptionArea);
		jpnlCountry.setLayout(new BorderLayout(5, 5));
		jpnlCountry.add(scrollPane, BorderLayout.CENTER);
		jpnlCountry.add(jlblImageTitle, BorderLayout.WEST);
		jpnlTotal.setLayout(new BorderLayout(5, 5));
		jpnlTotal.add(jcbo, BorderLayout.NORTH);
		jpnlTotal.add(jpnlCountry, BorderLayout.CENTER);
		jpnlTotal.add(jbtShowHistogram, BorderLayout.SOUTH);
		jbtShowHistogram.addActionListener(this);
		add(jpnlTotal);
		setDisplay(0);

		jcbo.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				setDisplay(jcbo.getSelectedIndex());
			}
		});
	}

	public void setDisplay(int index) { // show the flag, name and description
		jlblImageTitle.setText(flagTitles[index]);
		jlblImageTitle.setIcon(flagImage[index]);
		jtaDescriptionArea.setText(flagDescription[index]);
	}

	private int[] countLetters() { // count the letters
		int[] count = new int[26];
		String text = jtaDescriptionArea.getText();

		for (int i = 0; i < text.length(); i++) {
			char character = text.charAt(i);
			if ((character >= 'A') && (character <= 'Z')) {
				count[character - 'A']++;
			} else if ((character >= 'a') && (character <= 'z')) {
				count[character - 'a']++;
			}
		}
		return count;
	}

	public void actionPerformed(ActionEvent e) { // create the second window
		if (e.getSource() == jbtShowHistogram) {
			int[] count = countLetters();
			histogram.showHistogram(count);
			histogramFrame.setVisible(true);
			histogramFrame.add(histogram);
			histogramFrame.pack();
			histogramFrame.setTitle("Histogram");
		}

	}
}