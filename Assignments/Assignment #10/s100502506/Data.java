package a10.s100502506;

public class Data 																		//save the data
{
	private String[] flagTitle={"Canada","China","England","German","USA"};
	private String[] flagDescription=new String[5];
	Data()
	{
		flagDescription[0]="Canada is a North American country consisting of ten provinces and three territories."+			//Canada
						   "Located in the northern part of the continent,"+
						   "it extends from the Atlantic Ocean in the east to the Pacific Ocean in the west,"+
						   "and northward into the Arctic Ocean. Spanning over 9.9 million square kilometres,"+
						   "Canada is the world's second-largest country by total area, "+
						   "and its common border with the United States is the longest land border in the world.";
		flagDescription[1]="China see also Names of China), officially the People's Republic of China (PRC)"+				//China
						   ", is the world's most-populous country, with a population of over 1.3 billion."+
						   " The East Asian state covers approximately 9.6 million square kilometres,"+
						   " and is the world's second-largest country by land area,"+
						   " and the third- or fourth-largest in total area,"+
						   " depending on the definition of total area.";
		flagDescription[2]="The United Kingdom of Great Britain and Northern Ireland (commonly known as the United Kingdom,"+//England
						   " the UK, or Britain) is a sovereign state located off the north-western coast of continental Europe."+
						   " The country includes the island of Great Britain,"+
						   " the north-eastern part of the island of Ireland and many smaller islands."+
						   " Northern Ireland is the only part of the UK that shares a land border with another sovereign state�Xthe Republic of Ireland."+
						   " Apart from this land border the UK is surrounded by the Atlantic Ocean,"+
						   "the North Sea, the English Channel and the Irish Sea.";
		flagDescription[3]="Germany, officially the Federal Republic of Germany (German: Bundesrepublik Deutschland,"+		//German
						   " pronounced is a federal parliamentary republic in Europe. "+
						   "The country consists of 16 states while the capital and largest city is Berlin. "+
						   "Germany covers an area of 357,021 km2 and has a largely temperate seasonal climate."+
						   " With 81.8 million inhabitants, "+
						   "it is the most populous member state and the largest economy in the European Union."+
						   " It is one of the major political powers of the European continent and a technological leader in many fields.";
		flagDescription[4]="The United States of America (commonly abbreviated to the United States,"+						//Usa
						   " the U.S., the USA, America, and the States)"+
						   " is a federal constitutional republic comprising fifty states and a federal district."+
						   " The country is situated mostly in central North America,"+
						   " where its forty-eight contiguous states and Washington, D.C., the capital district,"+
						   " lie between the Pacific and Atlantic Oceans,"+
						   " bordered by Canada to the north and Mexico to the south. "+
						   "The state of Alaska is in the northwest of the continent, "+
						   "with Canada to the east and Russia to the west, across the Bering Strait."+
						   " The state of Hawaii is an archipelago in the mid-Pacific. "+
						   "The country also possesses several territories in the Pacific and Caribbean.";
	}
	public String[] getflagTitle()				//return flag title
	{
		return flagTitle;
		
	}
	public String[] getflagDescription()		//return description
	{
		return flagDescription;
		
	}
}
