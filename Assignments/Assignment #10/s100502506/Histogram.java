package a10.s100502506;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Histogram extends JFrame
{
	private int count[]=new int[26];
	private Color[] colors =new Color[26];
	Histogram() 
	{
																//26 kinds of color 
		colors[0]=Color.RED;
		colors[0]=new Color(255, 55, 0);
		colors[0]=new Color(255, 110, 0);
		colors[3]=new Color(255, 165, 0);
		colors[4]=new Color(255, 187, 0);
		colors[5]=new Color(255, 209, 0);
		colors[6]=new Color(255, 231, 0);
		colors[7]=new Color(255, 255, 0);
		colors[8]=new Color(193, 255, 0);
		colors[9]=new Color(131, 255, 0);
		colors[10]=new Color(69, 255, 0);
		colors[11]=new Color(0, 255, 0);
		colors[12]=new Color(0, 220, 69);
		colors[13]=new Color(0, 189, 131);
		colors[14]=new Color(0, 158, 193);
		colors[15]=new Color(0, 127, 255);
		colors[16]=new Color(0, 93, 255);
		colors[17]=new Color(0, 62, 255);
		colors[18]=new Color(0, 31, 255);
		colors[19]=new Color(0, 0, 255);
		colors[20]=new Color(31, 0, 255);
		colors[21]=new Color(62, 0, 255);
		colors[22]=new Color(93, 0, 255);
		colors[23]=new Color(139, 0, 255);
		colors[25]=Color.gray;
		colors[25]=Color.BLACK;
		
		DrawHistogram testDrawHistogram =new DrawHistogram();	//create histogram
		add(testDrawHistogram);									//add histogram to JFrame 
	}
	public void setCount(int[] input)							//set count
	{
		count=input;
		repaint();
	}
	class DrawHistogram extends JPanel
	{
		public void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			int gap=(int)(getWidth()/26);
			double percentOFHeight=0.8;
			g.drawLine(0, (int)(getHeight()*percentOFHeight), getWidth(), (int)(getHeight()*percentOFHeight));
			
			for(int i=0;i<26;i++)								//26 rectangle and letter
			{
				g.setColor(colors[i]);
				g.fillRect(gap*i, (int)(getHeight()*percentOFHeight)-count[i]*10, 10, count[i]*10);
				g.drawString((char)(65+i)+"",gap*i, (int)(getHeight()*percentOFHeight+10));
			}
		}
	}
}
