package a10.s100502506;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class FrameWork extends JFrame
{
	private Data flagData=new Data();
	private JComboBox flagcomboBox=new JComboBox(flagData.getflagTitle());
	private String[] flagdescription=flagData.getflagDescription();
	private JTextArea contentTextArea;
	private JPanel Panel1=new JPanel();
	private Color color1=new Color(238,238,238);
	private JScrollBar hort=new JScrollBar(JScrollBar.HORIZONTAL);
	private JScrollBar vert=new JScrollBar(JScrollBar.VERTICAL);
	private JPanel messagePanel=new JPanel();
	private JButton Histogram=new JButton("Histogram");
	private Histogram histest=new Histogram();
	private ImageIcon[] flagImageIcons=								//load image
	{
		 new ImageIcon("image/Canada.gif"),
		 new ImageIcon("image/China.gif"),
		 new ImageIcon("image/England.gif"),
		 new ImageIcon("image/German.gif"),
		 new ImageIcon("image/USA.gif"),
	};
	private JLabel Label=new JLabel();
	
	
	
	public FrameWork() 												//initialize
	{
		Label.setIcon(flagImageIcons[0]);
																	//setTextArea
		contentTextArea=new JTextArea(flagdescription[0],5,20);
		contentTextArea.setLineWrap(true);
		contentTextArea.setWrapStyleWord(true);
		contentTextArea.setBackground(color1);
		
		Panel1.setLayout(new GridLayout(1,2));						//set panel1
		Panel1.add(Label,BorderLayout.WEST);
		Panel1.add(contentTextArea,BorderLayout.EAST);
		Panel1.setLayout(new GridLayout(1,2,5,5));
		
		add(Panel1,BorderLayout.CENTER);							//add to JFrame
		add(flagcomboBox,BorderLayout.NORTH);
		add(Histogram,BorderLayout.SOUTH);
		flagcomboBox.addItemListener(new itemAction());
		Histogram.addActionListener(new ButtonAction());
		
		histest.setVisible(false);									//histogram
		histest.setSize(800,700);
		histest.setTitle("Histogram");
		
	}
	public int[] countLetters()										//count letter
	{
		int[] count=new int[26];
		String textString=contentTextArea.getText();
		for(int i=0;i<textString.length();i++)
		{
			char character=textString.charAt(i);
			if((character>='A')&&(character<='Z'))
			{
				count[character-'A']++;
			}
			else if((character>='a')&&(character<='z'))
			{
				count[character-'a']++;
			}
		}
		return count;
	}
	public void setDisplay(int index)								//reset content
	{
		Label.setIcon(flagImageIcons[index]);
		contentTextArea.setText(flagdescription[index]);
	}
	class itemAction implements ItemListener						//item action
	{
		public void itemStateChanged(ItemEvent e) 
		{
			setDisplay(flagcomboBox.getSelectedIndex());
		}
		
	}
	class ButtonAction implements ActionListener					//button action
	{
		public void actionPerformed(ActionEvent e) 
		{
			if(e.getSource()==Histogram)
			{
				histest.setCount(countLetters());
				histest.setVisible(true);
			}
			
		}
		
	}
}
