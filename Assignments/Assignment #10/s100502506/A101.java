package a10.s100502506;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class A101
{
	public static void main(String argc[])//main
	{
		FrameWork test =new FrameWork();	//create framework
		test.setVisible(true);				//set framework
		test.setSize(800,325);
		test.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		test.setTitle("A10");
		Dimension screensizeDimension =Toolkit.getDefaultToolkit().getScreenSize();
		Dimension size=test.getSize();
		int x=(int)(screensizeDimension.getWidth()/2-size.getWidth()/2);
		int y=(int)(screensizeDimension.getHeight()/2-size.getHeight()/2);
		test.setLocation(x,y);
	}
}
