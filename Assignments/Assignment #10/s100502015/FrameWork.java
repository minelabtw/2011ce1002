package a10.s100502015;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class FrameWork extends JFrame {
	private String[] flagTitle = { "Canada", "China", "England", "Garman",
			"USA" };
	private Histogram h1 = new Histogram();
	private JFrame h1f = new JFrame();
	private ImageIcon[] flagImage = // flags
	{ new ImageIcon("image/Canada.gif"), new ImageIcon("image/China.gif"),
			new ImageIcon("image/England.gif"),
			new ImageIcon("image/German.gif"), new ImageIcon("image/USA.gif") };
	private String[] flagsdes = new String[5];// flag name
	private DescriptionPanel flagdespanel = new DescriptionPanel();
	private JComboBox jb1 = new JComboBox(flagTitle);// select flags
	private JButton b1 = new JButton("Histogram");// button histogram

	public FrameWork() {
		// flag description
		flagsdes[0] = "Canada  is a North American country consisting "
				+ "of ten provinces and three territories. Located in the northern part of the c"
				+ "ontinent, it extends from the Atlantic Ocean in the east to the Pacific Ocean in the west, and "
				+ "northward into the Arctic Ocean. Spanning over 9.9 million square kilometres, Canada is the world's "
				+ "second-largest country by total area, and its common border with the United States is "
				+ "the longest land border in the world.";
		flagsdes[1] = "China ; Chinese:  see also Names of China), "
				+ "officially the People's Republic of China (PRC), is the world's most-populous country,"
				+ " with a population of over 1.3 billion. The East Asian state covers approximately 9.6 million square kilometres,"
				+ " and is the world's second-largest country by land area, and the third- or fourth-largest in total area, depending on"
				+ " the definition of total area";
		flagsdes[2] = "England  is a country that is part of the United Kingdom."
				+ " It shares land borders with Scotland to the north and Wales to the west; the Irish Sea is to the north west,"
				+ " the Celtic Sea to the south west, while the North Sea to the east and the English Channel to the south separate "
				+ "it from continental Europe. Most of England comprises the central and southern part of the island of Great Britain"
				+ " in the North Atlantic. The country also includes over 100 smaller islands such as the Isles of Scilly and the"
				+ " Isle of Wight.";
		flagsdes[3] = "Germany , officially the Federal Republic of Germany "
				+ "(German: Bundesrepublik Deutschland, pronounced , is a federal parliamentary "
				+ "republic in Europe. The country consists of 16 states while the capital and largest city is Berlin. Germany covers an area "
				+ "of 357,021 km2 and has a largely temperate seasonal climate. With 81.8 million inhabitants, it is the most populous member "
				+ "state and the largest economy in the European Union. It is one of the major political powers of the European continent and a "
				+ "technological leader in many fields.";
		flagsdes[4] = "The United States of America (commonly abbreviated to the United States, the U.S., "
				+ "the USA, America, and the States) is a federal constitutional republic comprising fifty states"
				+ " and a federal district. The country is situated mostly in central North America, where its forty-eight "
				+ "contiguous states and Washington, D.C., the capital district, lie between the Pacific and Atlantic Oceans, "
				+ "bordered by Canada to the north and Mexico to the south. The state of Alaska is in the northwest of the continent,"
				+ " with Canada to the east and Russia to the west, across the Bering Strait. The state of Hawaii is an archipelago in "
				+ "the mid-Pacific. The country also possesses several territories in the Pacific and Caribbean.";
		// show first combobox
		setDisplay(0);
		add(jb1, BorderLayout.NORTH);
		add(flagdespanel, BorderLayout.CENTER);
		add(b1, BorderLayout.SOUTH);
		// select flags in combox
		jb1.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				setDisplay(jb1.getSelectedIndex());
			}
		});
		// push histogram and jump a frame to show how many word in description
		b1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int[] count = countLetters();

				h1.showHistogram(count);
				h1f.setVisible(true);
			}
		});
		h1f.add(h1);
		h1f.pack();
		h1f.setTitle("Histogram");
		h1f.setSize(500, 400);

	}

	// count flag description words
	private int[] countLetters() {
		int[] count = new int[26];

		String text = flagdespanel.jdes.getText();

		for (int i = 0; i < text.length(); i++) {
			char character = text.charAt(i);
			if ((character >= 'A') && (character <= 'Z')) {
				count[character - 'A']++;
			} else if ((character >= 'a') && (character <= 'z')) {
				count[character - 'a']++;
			}
		}
		return count;
	}

	// select which flag to show
	public void setDisplay(int index) {
		flagdespanel.setTitle(flagTitle[index]);
		flagdespanel.setImageIcon(flagImage[index]);
		flagdespanel.setDescription(flagsdes[index]);

	}
}
