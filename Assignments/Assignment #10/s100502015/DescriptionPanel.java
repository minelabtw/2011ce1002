package a10.s100502015;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class DescriptionPanel extends JPanel {
	private JLabel j1 = new JLabel();
	protected JTextArea jdes = new JTextArea();// flag description screen

	public DescriptionPanel() {
		j1.setHorizontalAlignment(JLabel.CENTER);
		j1.setHorizontalTextPosition(JLabel.CENTER);
		j1.setVerticalTextPosition(JLabel.BOTTOM);

		jdes.setLineWrap(true);
		jdes.setWrapStyleWord(true);
		jdes.setEditable(false);

		JScrollPane sp = new JScrollPane(jdes);

		setLayout(new BorderLayout(5, 5));
		add(sp, BorderLayout.CENTER);
		add(j1, BorderLayout.WEST);
	}

	public void setTitle(String title) {
		j1.setText(title);
	}

	public void setImageIcon(ImageIcon icon) {
		j1.setIcon(icon);
	}

	public void setDescription(String text) {
		jdes.setText(text);
	}
}