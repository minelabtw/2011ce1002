package a10.s100502034;
import javax.swing.JPanel;
import java.awt.*;
public class Draw extends JPanel{
	private int[] numberOfLetterD = new int[26];//這個是每個字母的數量
	private char[] letter = new char[26];//為了用for來寫... char應該是必需的吧
	public Draw(int[] number){
		numberOfLetterD = number;//宣告的時候要把每個字母的數量都掉進來
	}
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int max = numberOfLetterD[0];//因為要跟視窗大小匹配 一個最大值也是必需的吧 (就是哪個字母出現的次數最高
		for (int i = 1;i<numberOfLetterD.length;i++){
			if (max < numberOfLetterD[i]){
				max = numberOfLetterD[i];
			}
		}
		max++;//最後++是因為不想讓最大的那個字母的棒形摸到視窗頂邊
		letter[0] = 'A';//我用大寫放下面表示字母
		for(int i = 1;i<letter.length;i++){
			letter[i] = (char)(letter[i-1] + 1); 
		}
		int[] x = new int[26];
		for (int i = 1;i<27;i++){
			x[i-1] = (i+1) * getWidth()/29;//把width分成29塊 , 共28個點 ,第一個點放縱軸點 剩下27個點放字母
		}
		for ( int i =0;i<x.length;i++){
			g.setColor(new Color(255 , i * 255/27, 0));//隨便挑了這個色  ((其實本來想三個數都randon 不過這樣應該比較好看吧
			g.fillRect(x[i],getHeight()-100 -((getHeight()-100)* numberOfLetterD[i] /max), 10,(getHeight()-100)* numberOfLetterD[i] /max);//棒形
			g.setColor(Color.black);//下面的字母都用黑色
			g.drawString(letter[i] + "", x[i], getHeight()-70);//字母
		}
		for(int i = max-1; i>=0;i--){
			g.drawString(i+"",x[0] - getWidth()/28,(getHeight()-100)* (max - i) /max +5);//縱軸標點 (字母出現一次為一格
			g.drawLine(x[0], (getHeight()-100)* (max - i) /max, x[x.length-1]+getWidth()/29, (getHeight()-100)* (max - i) /max);
		}
	}
}

