package a10.s100502034;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Scanner;
import javax.swing.*;
public class FrameWork extends JFrame{
	private int fileControl =0;//因為在TextArea的修改是可以output到檔案中的 所以宣告了一個fileControl代表用comboBox轉換前在觀看的是哪一個description
	private JPanel pp = new JPanel();//這是題目給的那個有comboBox icon  description histogram的JPanel
	private int c = 0;//代表現在看的是哪一個description
	private JFrame ff = new JFrame();//這個是棒形圖的Frame
	private String[] nameS = new String[]{"countries choosing", "China", "Canada", "England","German","USA"};//五個國家 和上一個初始的countries choosing
	private JComboBox comboBox= new JComboBox(nameS);//comboBox
	private JLabel pic = new JLabel();//用來放圖片的JLabel
	private TextArea description = new TextArea();//用來顯示簡介的TextArea ((轉換國家/顯示棒形圖都可以把修改過的TextArea內容output到txt裡
	private JButton histogram = new JButton("histogram");
	private ImageIcon[] icon = new ImageIcon[6];//六張圖片 ((p.s.我綽號叫K.P.   ((就是反PK的和平主義者嘛 哈哈
	private String[] descriptionS = new String [6];//用來放六篇description(含初始ountries choosing)
	public FrameWork() throws FileNotFoundException{
		JPanel p = new JPanel();//這個是棒形圖的Panel ((其實也不過是把圖形掉進來而已啦
		p.setLayout(new GridLayout(1,2));
		FileReader[] file = new FileReader[6];//把簡介的掉進來
		Scanner[] input = new Scanner[6];
		for (int i =0 ;i< file.length;i++){
			file[i] = new FileReader("image/" + (i) + ".txt");
			input[i] = new Scanner(file[i]);
			descriptionS[i] = "";
			while (input[i].hasNext()){
					descriptionS[i] = descriptionS[i] +input[i].nextLine()+ "\n" ;
			}
		}
		file.clone();//有關閉
		description.setText(descriptionS[0]);
		icon[0] = new ImageIcon("image/kp.gif");
		icon[1] = new ImageIcon("image/China.gif");
		icon[2] = new ImageIcon("image/Canada.gif");
		icon[3] = new ImageIcon("image/England.gif");
		icon[4] = new ImageIcon("image/German.gif");
		icon[5] = new ImageIcon("image/USA.gif");
		pic = new JLabel(icon[0]);//初始的介面
		p.add(pic);
		add(comboBox, BorderLayout.NORTH);
		p.add(description);
		add(histogram, BorderLayout.SOUTH);
		add(p, BorderLayout.CENTER);
		Listener listener = new Listener();
		comboBox.addActionListener(listener);
		histogram.addActionListener(listener);
	}
	class Listener implements ActionListener {
		PrintWriter[] output = new PrintWriter[6];
		public void actionPerformed(ActionEvent e) {
			for(int i = 0 ;i<nameS.length;i++){
				if(e.getSource() == comboBox){
						if(((JComboBox) e.getSource()).getSelectedItem() == nameS[i]){//找出comboBox轉到哪個選項
							c = i;
							WriteFile();//把之前有修改過/沒修改過重新output到txt裡
							ChangeGUI(i);//把圖片跟簡介都換
						}
					}
				}
			if(e.getSource() == histogram){
				WriteFile();//把之前有修改過/沒修改過重新output到txt裡
				Result(c);//顯示棒形圖
			}
		}
	}
	public void ChangeGUI(int b){
		description.setText(descriptionS[b]);//set簡介
		pic.setIcon(icon[b]);//set圖旗
		repaint();//重新繪畫
	}
	public void WriteFile(){
		try{
			FileWriter fstream = new FileWriter("image/" + fileControl + ".txt");
			BufferedWriter out = new BufferedWriter(fstream);
			descriptionS[fileControl] = description.getText();//用來存放簡介descriptionS[fileControl]也要更新
			out.write(description.getText());
			out.close();
		}catch (Exception ee){
			System.err.println("Error: " + ee.getMessage());
		}
		fileControl = c;//output後把目前正在觀看的國家編號掉給fileControl
	}
	public void Result(int a){
		descriptionS[a] = description.getText();
		int[][] numberOfLetter = new int[6][26];//用二維陣列來放六篇文章每個字母出現次數
		int b = 0;
		for ( int i = 0; i < descriptionS.length;i++){
			for(int j = 0;j<descriptionS[i].length();j++){
				b = descriptionS[i].charAt(j)-'A';//先來把大寫搞定
				if (b<0||b>25){	//如果不是大寫的時候
					b = descriptionS[i].charAt(j)-'a';//來看看跟小寫的關係
				}
				if( b>=0 && b<=26){//確定是小寫
					numberOfLetter[i][b]++;
				}
			}
		}
		ff.setTitle("Histogram");
		ff.setLocationRelativeTo(null);
		ff.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ff.setSize(660, 600);
		Draw draw = new Draw(numberOfLetter[a]);//Draw就是棒形圖
		draw.repaint();
		/*其實為甚麼在用一個JPanel只是放一個圖形
		 * (直接放進Frame不就得了嗎?) 
		 * 我不知道為甚麼 直接放在Frame的時候  
		 * 在顯示第二個國家的棒形圖的時候 用滑鼠調形視窗大小就是Visible嘛) 
		 * 它會跳回第一個圖形 !('_>')
		 */
		pp.removeAll();//所以再用一個JPanel來每次都 洗一遍 再畫的做
		pp.setLayout(new GridLayout(1,1));
		pp.add(draw);
		ff.add(pp);
		ff.setVisible(true);
	}
	
}
