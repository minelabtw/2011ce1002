package a10.s100502519;
import javax.swing.*;
import java.awt.*;

public class Histogram extends JPanel{
	private int[] count;
	
	public void showHistogram(int[] count_input){
		count = count_input;			//接收傳來的array (計量結果)
		repaint();
	}
	
	protected void paintComponent(Graphics g){
		if(count == null){
			return;
		}
		
		super.paintComponent(g);
		
		int width = getWidth();
		int height = getHeight();
		int interval = (width-40)/count.length;			//間格
		int individualWidth = (int)(((width-40)/24)*0.60);			//個體寬
		int maxCount = 0;
		int x = 30;
		
		for(int i=0 ; i<count.length ; i++){			/*找最大*/
			if(maxCount < count[i]){
				maxCount = count[i];
			}
		}
		
		g.drawLine(10, height-45, width-10, height-45);			//底軸
		
		for(int i=0 ; i<count.length ; i++){
			int barHeight = (int)(((double)count[i]/(double)maxCount)*(height-55));			//柱高
			
			Color color = new Color((int)(Math.random()*256),(int)(Math.random()*256),(int)(Math.random()*256));			//產生亂數顏色
			
			g.setColor(color);
			g.fillRect(x, height-45-barHeight, individualWidth, barHeight);			//畫出長柱
			g.drawString((char)(65+i)+"", x, height-30);			//底下的值
			
			x+=interval;			//一直往右再畫
		}
	}
}
