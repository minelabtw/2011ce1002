package a10.s100502519;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener{
	
	/*------new區------*/
	private String[] flagTitles = {"Canada" , "China" , "England" , "German" , "USA"};			//國旗名
	private ImageIcon[] flagImage = {new ImageIcon("image/Canada.gif") , new ImageIcon("image/China.gif"),			/*國旗圖*/
									new ImageIcon("image/England.gif") , new ImageIcon("image/German.gif"),
									new ImageIcon("image/USA.gif")};
	private String[] flagDescription = new String[5];			//國家描述
	private JComboBox jcbo = new JComboBox(flagTitles);			//下拉選單     並將flagTitles放入
	private JPanel panel_description = new JPanel();			// 含    flagImage  和     flagDescription
	private JButton button_histogram = new JButton("Histogram");			//產生第二個frame的button
	private JLabel label_imageTitle = new JLabel();			//放圖的label
	private JTextArea textArea = new JTextArea();			//textArea
	private JScrollPane scrollPane = new JScrollPane(textArea);			//有捲軸的pane  並將textArea收入
	private JFrame frame_histogramFrame = new JFrame();			//第二個frame
	private Histogram histogram = new Histogram();			//畫圖的
	
	/*------constructor------*/
	public FrameWork(){
		/*設好每個國家的描述*/
		flagDescription[0] = "Canada is a North American country consisting of ten provinces and three territories. Located in the northern part of the continent, it extends from the Atlantic Ocean in the east to the Pacific Ocean in the west, and northward into the Arctic Ocean. Spanning over 9.9 million square kilometres, Canada is the world's second-largest country by total area, and its common border with the United States is the longest land border in the world.";
		flagDescription[1] = "China, officially the People's Republic of China (PRC), is the world's most-populous country, with a population of over 1.3 billion. The East Asian state covers approximately 9.6 million square kilometres, and is the world's second-largest country by land area, and the third- or fourth-largest in total area, depending on the definition of total area.";
		flagDescription[2] = "England, It shares land borders with Scotland to the north and Wales to the west; the Irish Sea is to the north west, the Celtic Sea to the south west, while the North Sea to the east and the English Channel to the south separate it from continental Europe. Most of England comprises the central and southern part of the island of Great Britain in the North Atlantic. The country also includes over 100 smaller islands such as the Isles of Scilly and the Isle of Wight.";
		flagDescription[3] = "German, officially the Federal Republic of Germany (German: Bundesrepublik Deutschland,  is a federal parliamentary republic in Europe. The country consists of 16 states while the capital and largest city is Berlin. Germany covers an area of 357,021 km2 and has a largely temperate seasonal climate. With 81.8 million inhabitants, it is the most populous member state and the largest economy in the European Union. It is one of the major political powers of the European continent and a technological leader in many fields.";
		flagDescription[4] = "The United States of America (commonly abbreviated to the United States, the U.S., the USA, America, and the States) is a federal constitutional republic comprising fifty states and a federal district. The country is situated mostly in central North America, where its forty-eight contiguous states and Washington, D.C., the capital district, lie between the Pacific and Atlantic Oceans, bordered by Canada to the north and Mexico to the south. The state of Alaska is in the northwest of the continent, with Canada to the east and Russia to the west, across the Bering Strait.";
		
		/*text呈現的設定*/
		textArea.setLineWrap(true);			//換行自動
		textArea.setWrapStyleWord(true);			//不切字
		textArea.setEditable(false);			//不給改
		
		/*panel_description部份*/
		panel_description.setLayout(new BorderLayout(5,5));
		panel_description.add(scrollPane,BorderLayout.CENTER);
		panel_description.add(label_imageTitle,BorderLayout.WEST);
		setDisplay(0);			//預設
		
		/*comboBox部份*/
		jcbo.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e){
				setDisplay(jcbo.getSelectedIndex());			//看選哪個就call對映的
			}
		});
		
		/*主frame部份*/
		setLayout(new BorderLayout(0, 0));
		add(jcbo,BorderLayout.NORTH);
		add(panel_description,BorderLayout.CENTER);
		add(button_histogram,BorderLayout.SOUTH);
		button_histogram.addActionListener(this);
		
		/*第二個frame部份*/
		frame_histogramFrame.add(histogram);			//加畫圖的panel
		frame_histogramFrame.setTitle("Histogram");
		frame_histogramFrame.setSize(400, 400);
		frame_histogramFrame.setLocation(0,150);
		frame_histogramFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	/*------設對映國旗圖和描述的method------*/
	private void setDisplay(int index){
		setImageIcon(flagImage[index]);			//國旗圖
		setDescription(flagDescription[index]);			//描述
	}
	
	/*------加對映的圖到lable_imageTitle------*/
	private void setImageIcon(ImageIcon icon){
		label_imageTitle.setIcon(icon);
	}
	
	/*------加對映的text到textArea------*/
	private void setDescription(String text){
		textArea.setText(text);
	}
	
	/*------計量------*/
	private int[] countLetters(){
		int[] count = new int[26];			//英文字母26個
		
		String text = textArea.getText();
		
		/*計量的for迴圈*/
		for(int i=0 ; i<text.length() ; i++){
			char character = text.charAt(i);
			
			if((character >= 'A') && (character <= 'Z')){
				count[character-'A']++;
			}
			else if((character >= 'a') && (character <= 'z')){
				count[character-'a']++;
			}
		}
		return count;
	}
	
	/*------event------*/
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == button_histogram){
			int[] count = countLetters();			//接收countLetters出來的array
			
			histogram.showHistogram(count);			//傳過去畫圖
			
			frame_histogramFrame.setVisible(true);			//把第二個frame設visible
		}
	}
}