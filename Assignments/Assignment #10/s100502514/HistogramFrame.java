/** class HistogramFrame to count characters as statistics in the discription. */
package a10.s100502514;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class HistogramFrame extends JFrame {

	/** UID */
	private static final long serialVersionUID = 1L;
	
	String title;
	String description;
	HistogramPanel hist_panel;
	
	public HistogramFrame(Countries country){
		setSize(700, 600);
		setLocationRelativeTo(null);
		setLayout(new BorderLayout());
		
		//Set the title for window and the caption of frame
		title = "Histogram for Description of "+country.nation_name;
		setTitle(title);
		
		//A Grid Panel for Caption and Radio Buttons
		JPanel grid_panel = new JPanel(new GridLayout(2, 1, 5, 5));
		grid_panel.add(new MyBigLabel(title));
		grid_panel.add(new MySelect());
		
		//Declare the Histogram Panel
		description = country.description;
		hist_panel = new HistogramPanel(description, false);
		
		//Add MyBigLabel, which is modified from package a1.s100502514, as the title
		add(grid_panel, BorderLayout.NORTH);
		add(hist_panel, BorderLayout.CENTER);
		//pack();
		
		addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent arg0) {
				DescriptionFrame.numHistogramFrames--;
				//System.out.println(DescriptionFrame.numHistogramFrames);
			}
			@Override
			public void windowOpened(WindowEvent arg0) {
				DescriptionFrame.numHistogramFrames++;
				//System.out.println(DescriptionFrame.numHistogramFrames);
			}
		});
	}
	
	//Inner class for a panel containing Radio button group.
	class MySelect extends JPanel implements ActionListener{
		/** UID */
		private static final long serialVersionUID = 1L;
		
		ButtonGroup group = new ButtonGroup();
		JRadioButton sortByAlphabets = new JRadioButton("Sort by Alphabets");
		JRadioButton sortByNumbers = new JRadioButton("Sort by Amounts (Top 30)");
		
		public MySelect(){
			//Arrange the GUI Objects in the radio panel
			setLayout(new FlowLayout());
			add(sortByAlphabets);
			add(sortByNumbers);
			
			//Set the fast key for radio buttons
			sortByAlphabets.setMnemonic('A');
			sortByNumbers.setMnemonic('N');
			sortByAlphabets.setToolTipText("Fast Key: Alt+A");
			sortByNumbers.setToolTipText("Fast Key: Alt+N");
			
			//Group the radio buttons
			group.add(sortByAlphabets);
			group.add(sortByNumbers);
			
			//Set the default selection of Radio buttons
			sortByAlphabets.setSelected(true);
			
			//Add Listeners
			sortByAlphabets.addActionListener(this);
			sortByNumbers.addActionListener(this);
		}
		
		/** ActionEvent Method */
		@Override
		public void actionPerformed(ActionEvent e) {
			if((JRadioButton)(e.getSource())==sortByAlphabets){
				hist_panel.readDescription(description, false);
			}else if((JRadioButton)(e.getSource())==sortByNumbers){
				hist_panel.readDescription(description, true);
			}
			hist_panel.repaint();
		}
	}
	
}
