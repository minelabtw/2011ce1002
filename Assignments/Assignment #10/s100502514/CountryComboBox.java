package a10.s100502514;

import java.awt.*;

import javax.swing.*;

public class CountryComboBox extends JComboBox<Countries> {

	/** UID */
	private static final long serialVersionUID = 1L;
	
	static Countries[] labels = {null, Countries.Canada, Countries.China,
			Countries.England, Countries.German, Countries.USA};
	
	public CountryComboBox(){
		super(labels);
		setRenderer(new MyComboBoxRenderer()); //set renderer
	}
	
	/** Inner class to render the comboBox with identified colors */
	class MyComboBoxRenderer extends JLabel implements ListCellRenderer<Countries> {
		
		/** UID */
		private static final long serialVersionUID = 1L;

		public MyComboBoxRenderer() {
			setOpaque(true);
			setHorizontalAlignment(CENTER);
			setVerticalAlignment(CENTER);
		}
		
		/*
		 * REFERENCED from JavaDocs,
		 * actually I don't really know what the "?" in "? extends SuperClass" means.
		 * 
		 * This method makes the instances of class Countries displayable in CountryComboBox.
		 * If the object is null, blue text will be shown;
		 * If the description file is not found, red text will be shown;
		 * Otherwise, green text will be shown. (for successfully loaded the description)
		 *
		 * That is, this method is actually a formatted form of paintComponent() for JComboBox.
		 */
		public Component getListCellRendererComponent(
				JList<? extends Countries> list, Countries value,
				int index, boolean isSelected, boolean cellHasFocus) {
			
			if (isSelected) {
				setBackground(list.getSelectionBackground());
				if(value == null){
					setForeground(Color.cyan);
				}else if(value.isSucceed()){
					setForeground(Color.green); //Bright Green
				}else{
					setForeground(Color.yellow);
				}
			} else {
				setBackground(list.getBackground());
				if(value == null){
					setForeground(Color.blue);
				}else if(value.isSucceed()){
					setForeground(new Color(102, 153, 0)); //Olive Green
				}else{
					setForeground(Color.red);
				}
			}
			
			if(value == null){
				setText("Please select a country...");
			}else{
				setText(value.toString());
			}
			
			return this;
		}
	}
}


