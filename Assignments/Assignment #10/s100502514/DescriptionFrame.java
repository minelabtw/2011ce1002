/** class DescriptionFrame to contain combo-box, flags, descriptions, and histogram entry button. */
package a10.s100502514;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class DescriptionFrame extends JFrame {

	/** UID */
	private static final long serialVersionUID = 1L;
	
	/** GUI Members */
	CountryComboBox menu = new CountryComboBox();
	A101_FlagPanel flagPanel = new A101_FlagPanel();
	DescriptionPanel txtPanel = new DescriptionPanel();
	JButton hist_btn = new JButton("See Histogram");
	
	/** Records how many the histogram frames is opened. */
	static int numHistogramFrames = 0;
	
	public DescriptionFrame(){
		// Basic window settings
		setSize(800, 350);
		setTitle("Assignment 10-1: National Flag Test");
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); //But Call the WindowListener
		
		//add GUI members
		setLayout(new BorderLayout());
		add(menu, BorderLayout.NORTH);
		menu.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				onComboSelect(menu.getSelectedIndex());
			}
		});
		
		JPanel middle_panel = new JPanel();
		middle_panel.setLayout(new GridLayout(1, 2));
		middle_panel.add(flagPanel);
		middle_panel.add(txtPanel);
		add(middle_panel, BorderLayout.CENTER);
		
		add(hist_btn, BorderLayout.SOUTH);
		hist_btn.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				try{
					(new HistogramFrame(CountryComboBox.labels[menu.getSelectedIndex()])).setVisible(true);
				}catch(ArrayIndexOutOfBoundsException ex){
					JOptionPane.showMessageDialog(null, "Please select a country!!",
							"Assignment 10-1", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		
		addWindowListener(new WindowAdapter(){
			@Override
			public void windowClosing(WindowEvent arg0) {
				//Confirm closing if there is/are any histograms shown.
				if(numHistogramFrames>0){
					if(JOptionPane.showConfirmDialog(null,
							"There are still "+numHistogramFrames+" histogram(s) opened.\n" +
							"would you really like to close all?", "Assignment 10-1",
							JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
						System.exit(0); //Terminate the application if clicked yes.
					}else{
						setVisible(true);
					}
				}else{
					System.exit(0); //Terminate without prompting dialog if no histogram is shown.
				}
			}
		});
	}
	
	/** Call this method for CountryComboBox's ItemEvent, to change the flag and the description. */
	protected void onComboSelect(int itemNo){
		Countries selectedItem;
		try{
			selectedItem = CountryComboBox.labels[itemNo];
		}catch(Exception ex){
			//when there is an error (ex. invalid selection like the "Please select" label.)
			selectedItem = null;
		}
		flagPanel.resetFlag(selectedItem);
		txtPanel.resetDescription(selectedItem);
	}
	
}
