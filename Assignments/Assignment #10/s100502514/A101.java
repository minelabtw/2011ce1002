/**
 * CE1002-100502514 FlashTeens Chiang
 * [The Following Rules are from ce1002 website]
 * Assignment 10-1
 * 
 * Please write a program and meet the following requirements:
	* Find five countries, their flags, and a short description about each country.
	* Implement a JComboBox component, let user choose which country is going to be presented.
	* After user choice, show the corresponding flag and description.
	* Implement a button ˇ§Histogramˇ¨ to count how many times does each alphabet shows in the description.
	  While user clicking it, show the result in another window.
	* The flags of countries are given from TA.
	
 */
package a10.s100502514;

//import javax.swing.JFrame;

public class A101 {
	
	public static void main(String[] args){
		DescriptionFrame myWindow1 = new DescriptionFrame();
		myWindow1.setVisible(true);
	}
}
