package a10.s100502514;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.TitledBorder;

public class DescriptionPanel extends JPanel {

	/** UID */
	private static final long serialVersionUID = 1L;
	
	/** TODO to be continued to add something if necessary, in DescriptionPanel on the RIGHT side */
	JTextArea textContent;
	JScrollPane scrollPane;
	
	public DescriptionPanel(){
		/** TODO to be continued from JTextField textContent and JScrollBar scroll */
		//...
		
		//layout setting
		setLayout(new BorderLayout());
		
		
		//add text area
		textContent = new JTextArea();
		setText("Please select a country..."); //use a method in this class, to give default values.
		textContent.setLineWrap(true); //multiline mode
		textContent.setWrapStyleWord(true); //word-wrapping is on
		textContent.setEditable(false); //non-editable by user
		
		//add scroll pane to contain the text area
		scrollPane = new JScrollPane(textContent);
		add(scrollPane, BorderLayout.CENTER);
		
		//set border of this panel
		setBorder(new TitledBorder("Description of the country"));
	}
	
	//Read the description from the (static final) object instances of class Countries.
	public void resetDescription(Countries country){
		if(country==null){
			textContent.setText("Invalid Selection!!\nPlease choose any of the countries EXCEPT this!!");
			textContent.setForeground(Color.red); //Red if loaded in vain
			return;
		}
		textContent.setText(country.description);
		if(country.isSucceed()){
			textContent.setForeground(new Color(102, 80, 51)); //Grayish Brown if successfully loaded
		}else{
			textContent.setForeground(Color.red); //Red if loaded in vain
		}
	}
	
	//Set text by program
	public void setText(String txt){
		textContent.setText(txt);
		textContent.setForeground(Color.blue);
		//Blue if modified by code itself (ex. "Please select a country...")
	}

}
