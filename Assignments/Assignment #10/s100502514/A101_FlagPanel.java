package a10.s100502514;

import java.awt.*;

import javax.swing.*;

public class A101_FlagPanel extends JPanel {

	/** UID */
	private static final long serialVersionUID = 1L;
	
	/** GUI Members */
	JLabel iconLabel = new JLabel();
	JLabel titleLabel = new JLabel();
	
	/** Boolean for checking if image exists */
	private boolean hasImage = false;
	
	public A101_FlagPanel(){
		this("", "<Not-set-yet>");
	}
	public A101_FlagPanel(String imageName, String title){
		//set flag panel layout
		setLayout(new BorderLayout());
		resetFlag(imageName, title);
		
		//make the labels to center alignment
		JPanel icon_center = new JPanel();
		icon_center.setAlignmentX(CENTER_ALIGNMENT);
		icon_center.add(iconLabel);
		JPanel title_center = new JPanel();
		title_center.setAlignmentX(CENTER_ALIGNMENT);
		title_center.add(titleLabel);
		
		//add the labels
		add(icon_center, BorderLayout.CENTER);
		add(title_center, BorderLayout.SOUTH);
	}
	
	/** Reset the flag image: this private method will be called by public method
	 	with a parameter of object of class Countries. */
	private boolean resetFlag(String imageName, String title){
		//Set the text for title
		titleLabel.setText(title);
		
		//Set a new icon
		ImageIcon icon = new ImageIcon(imageName);
		if(icon.getImageLoadStatus() != MediaTracker.COMPLETE){
			//File not found
			iconLabel.setForeground(Color.red);
			titleLabel.setForeground(Color.red);
			iconLabel.setIcon(null);
			iconLabel.setText("Image Not Found!!");
			hasImage = false;
		}else{
			//File successfully loaded
			iconLabel.setForeground(Color.black);
			titleLabel.setForeground(Color.blue);
			iconLabel.setIcon(icon);
			iconLabel.setText("");
			hasImage = true;
		}
		return hasImage;
	}
	
	/** Reset the flag image: this is the public method that calls the private form
	 	and catches NullPointerException if an invalid selection is selected. */
	public boolean resetFlag(Countries country){
		try{
			return resetFlag(country.flag_image, country.nation_name);
		}catch(NullPointerException anyForTest){
			//In case country is null, that is, cannot read any properties inside.
			iconLabel.setForeground(Color.red);
			titleLabel.setForeground(Color.red);
			iconLabel.setIcon(null);
			iconLabel.setText("");
			titleLabel.setText("Invalid Selection!!");
			return false;
		}
	}
	
	/** Check if there is a flag image in the panel */
	public boolean hasImage(){
		return hasImage;
	}
	
}
