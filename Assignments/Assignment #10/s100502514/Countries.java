/** class Countries, including static informations of countries */
package a10.s100502514;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;

public class Countries {
	
	/** Static Informations of countries */
	static final Countries Canada = new Countries(
			"Canada", "image/Canada.gif", "src/a10/s100502514/descriptions/txtCanada.txt", "utf-8");
	static final Countries China = new Countries(
			"China", "image/China.gif", "src/a10/s100502514/descriptions/txtChina.txt", "big5");
	static final Countries England = new Countries(
			"England", "image/England.gif", "src/a10/s100502514/descriptions/txtEngland.txt", "big5");
	static final Countries German = new Countries(
			"German", "image/German.gif", "src/a10/s100502514/descriptions/txtGerman.txt", "utf-8");
	static final Countries USA = new Countries(
			"United States", "image/USA.gif", "src/a10/s100502514/descriptions/txtUSA.txt", "utf-8");
	
	/** Public attributes (Even though this does not match the Java-style standard,
		this makes much easier for use however.) */
	public String nation_name;
	public String flag_image;
	public String description; //loaded contents
	public String charset;
	
	/** Private boolean to check if successfully loaded the text content or not. */
	private boolean succeed = false;
	public Countries(String nation_name, String flag_image, String description_file){
		this(nation_name, flag_image, description_file, Charset.defaultCharset().name());
	}
	public Countries(String nation_name, String flag_image, String description_file, String charset){
		this.nation_name = nation_name;
		this.flag_image = flag_image;
		this.description = "";
		this.charset = charset;
		File descriptionLoader = new File(description_file);
		try {
			Scanner descriptionReader = new Scanner(descriptionLoader, charset);
			while(descriptionReader.hasNextLine()){
				//Read every line from the description files
				this.description += descriptionReader.nextLine()+'\n';
			}
			succeed = true;
		} catch (FileNotFoundException e) {
			this.description = "Sorry, but description file \""+description_file+"\" in " +
					"folder src/a10/s100502514 is NOT FOUND!!";
		}
		
	}
	
	//Getter of the read-only boolean "succeed", for whether the file is successfully read.
	public boolean isSucceed(){
		return succeed;
	}
	
	//Override java.lang.toString() to automatically transversed for combo-box use.
	public String toString(){
		if(succeed)return nation_name+" (Including Description)";
		else return nation_name+" (Description Not Found)";
	}
}
