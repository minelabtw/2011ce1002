/**
 * [Borrowed from Assignment 1-1: a1.s100502514.MyBigLabel]
 * This is a class for bigger text label.
 * For Histogram Frame use.
 */
package a10.s100502514;

import java.awt.*;
import javax.swing.*;

//A New Label Class for Bigger Text Style
public class MyBigLabel extends JLabel{
	
	/**
	 * UID created here as the 2nd Version, and the font changed into Times New Roman
	 */
	private static final long serialVersionUID = 2L; //Second Version
	
	public MyBigLabel(String txt){
		super(txt);
		setFont(new Font("Times New Roman",0,20));
		setHorizontalAlignment(JLabel.CENTER);
		setVerticalAlignment(JLabel.CENTER);
	}
	public MyBigLabel(){
		//Allows default constructor with no strings
		this("");
	}
}
