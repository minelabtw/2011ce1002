package a10.s100502514;

import java.awt.*;
import java.util.*;

import javax.swing.*;

public class HistogramPanel extends JPanel {

	/** UID */
	private static final long serialVersionUID = 1L;
	
	/** Array of Statistics */
	//Set as an empty array, the size will be changed when reading description.
	StatisticItem[] data = {};
	int other_chars = 0;
	static final int MAX_SCALE = 30; //The maximum of sorts of characters is restricted to 30.
	
	public HistogramPanel(String description, boolean useRanking){
		readDescription(description, useRanking);
	}
	
	/**
	 * Reset the statistics or histogram with the description string txt.
	 * If useRanking == true, then the statistics will be shown as the top-30 amount of characters.
	 * Otherwise the statistics will be shown only from A to Z.
	 * other_chars may be used if there is any characters not in the other statistic data.
	 */
	public void readDescription(String txt, boolean useRanking){
		
		//First reset the data.
		other_chars = 0;
		
		if(useRanking){
			ArrayList<StatisticItem> tmp = new ArrayList<StatisticItem>();
			
			int leng = txt.length();
			for(int i=0; i<leng; i++){
				char c = txt.charAt(i);
				
				//skip the empty char
				if((int)c<=(int)' ')continue;
				
				try{
					StatisticItem item; //to record each items in inner loop
					for(int j=0; (item = tmp.get(j)) != null; j++){
						if(item.character == c){
							item.count++; //If found, count it into data.
							break;
						}
					}
				}catch(IndexOutOfBoundsException ex){
					//This scope will be run if not found in old data, that is, a character is found new.
					//Add a new element at last.
					tmp.add(new StatisticItem(c, 1));
				}
			}
			
			//Copy tmp to data, and then arrange the order using java.util.Arrays.sort() method.
			data = tmp.toArray(data);
			Arrays.sort(data, new Comparator<StatisticItem>(){
				@Override
				public int compare(StatisticItem a, StatisticItem b) {
					// Sort by the Amount of characters.
					// If one of the elements is null, see it as zero.
					return (b==null ? 0 : b.count) - (a==null ? 0 : a.count);
				}
			});
			
			//Count "other_chars"
			for(int i=MAX_SCALE; i<data.length; i++){
				if(data[i]!=null) other_chars+=data[i].count;
			}
			
			//Slice the original data array to the first 30 elements.(MAX_SCALE)
			data = Arrays.copyOfRange(data, 0, MAX_SCALE);
			
		}else{
			data = new StatisticItem[26];
			int leng = txt.length();
			for(int i=0; i<leng; i++){
				char c = txt.charAt(i);
				
				//try capital letters as index first
				int n = (int)(c-'A');
				if(n>25 || n<0){
					//if found not capital, then try small letters.
					n = (int)(c-'a');
				}
				if(n>25 || n<0){
					//if small letters still not matches, then this character belongs to "other".
					if(c>' '){ //empty chars are omitted.
						other_chars++;
					}
				}else if(data[n] == null){
					//if there is no character c before, create a new instance and starts with 1.
					data[n] = new StatisticItem(Character.toUpperCase(c), 1);
				}else{
					//if there is any record of character c before, increase the number by 1.
					data[n].count++;
				}
			}
		}
		
		/*
		//Test
		for(int i=0; i<data.length; i++){
			if(data[i]==null){
				System.out.println("["+i+"\t] NO DATA");
			}else{
				System.out.println("["+i+"\t] "+data[i].character+": "+data[i].count);
			}
		}
		System.out.println("[OTHER\t] ** "+other_chars);
		*/
	}
	
	private static final int bar_base_y = 400;
	private static final int bar_max_length = 300;
	private static final int bar_width = 10;
	private static final int bar_space = 20;
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		//Determine the start x coordinate
		int begin_x = (getWidth()-bar_space*data.length)/2;
		if(begin_x<0) begin_x=0;
		
		//Scan for the maximum count
		int max = 0;
		for(int i=0; i<data.length; i++){
			if(data[i] != null && data[i].count > max){
				max = data[i].count;
			}
		}
		
		//Draws an error message if the statistics is empty.
		if(max==0){
			Font msgFont = new Font("Arial Black", Font.PLAIN, 20);
			String msg = "The Text is EMPTY!!";
			int begin_msgx = (getWidth()-g.getFontMetrics(msgFont).stringWidth(msg))/2;
			g.setColor(Color.red);
			g.setFont(msgFont);
			g.drawString(msg, begin_msgx, 30);
			return;
		}
		
		//Draw the base line
		g.setColor(Color.black);
		g.drawLine(begin_x, bar_base_y, bar_space*data.length+begin_x, bar_base_y);
		
		//Draw the statistic bars and numbers, characters.
		for(int i=0; i<data.length; i++){
			
			int count;
			try{
				count = data[i].count;
			}catch(NullPointerException ex){
				count = 0;
			}
			
			int bar_length = bar_max_length*count/max;
			
			//draw bar rectangles
			g.setColor(new Color(255-255*count/max, 0+204*count/max, 0)); //to be modified
			g.fillRect(i*bar_space+5+begin_x, bar_base_y-bar_length, bar_width, bar_length);
			
			//set the font in the graphic
			Font numFont = new Font("Arial", Font.PLAIN, 12);
			
			//draw number above the bar
			g.setFont(numFont);
			String num_str = ""+count;
			g.drawString(num_str,
					i*bar_space+10-(g.getFontMetrics(numFont).stringWidth(num_str))/2+begin_x,
					bar_base_y-bar_length-10);
			//g.fillRect(i*bar_space+5+begin_x, bar_base_y-bar_length, bar_width, bar_length);
			
			//draw character under the bar
			g.setColor(Color.blue);
			String ch_str = "";
			try{
				ch_str = ""+data[i].character;
			}catch(NullPointerException ex){
				g.setColor(Color.red);
				ch_str = "--";
			}

			Font chFont = new Font("�L�n������", Font.PLAIN, 12);
			g.setFont(chFont);
			g.drawString(ch_str,
					i*bar_space+10-(g.getFontMetrics(chFont).stringWidth(ch_str))/2+begin_x,
					bar_base_y+20);
		}
		
		/** An independent scope for showing "other_chars" statistics. */{
			Font msgFont = new Font("Times New Roman", Font.BOLD, 20);
			String msg = "There are "+other_chars+" of OTHER characters.";
			int begin_msgx = (getWidth()-g.getFontMetrics(msgFont).stringWidth(msg))/2;
			g.setColor(Color.red);
			g.setFont(msgFont);
			g.drawString(msg, begin_msgx, bar_base_y+50);
			return;
		}
	}
	
}

class StatisticItem{
	public char character;
	public int count;
	public StatisticItem(char c, int n){
		character = c;
		count = n;
	}
}
