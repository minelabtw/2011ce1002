package a10.s100502508;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class A101 extends JFrame implements ActionListener
{
	private String[] flagTitles={"Canada","China","England","German","USA"};//declare an array of Strings for flag titles
	
	//create ImageIcons and initialize them
	private ImageIcon[] image={
		new ImageIcon("image/Canada.gif"),
		new ImageIcon("image/China.gif"),
		new ImageIcon("image/England.gif"),
		new ImageIcon("image/German.gif"),
		new ImageIcon("image/USA.gif"),
	};
	private String[] description=new String[5];
	private JComboBox box=new JComboBox(flagTitles);//create a combo box for selecting countries
	private JButton button=new JButton("Histogram button");//create button 
	private JTextArea textArea=new JTextArea();//Text area for displating text
	private JScrollPane scrollPane=new JScrollPane(textArea);//creat a scroll pane to hold the text area
	
	JLabel label1=new JLabel();//create label
	Histogram histogram=new Histogram();//create a Histogram object
	JFrame histogramFrame=new JFrame();//create a new frame to hold the histogram panel
	
	
	public A101() 
	{
		//set text description
		description[0]="Canada is a North American country consisting of ten provinces and three territories. " +
			"Located in the northern part of the continent, it extends from the Atlantic Ocean in the east to the Pacific Ocean in the west, and northward into the Arctic Ocean. " +
			"Spanning over 9.9 million square kilometres, Canada is the world's second-largest country by total area, and its common border with the United States is the longest land border in the world";
		description[1]="China , officially the People's Republic of China , is the world's most-populous country, with a population of over 1.3 billion. " +
			"The East Asian state covers approximately 9.6 million square kilometres, and is the world's second-largest country by land area, and the third- or fourth-largest in total area, depending on the definition of total area.";
		description[2]="England is a country that is part of the United Kingdom. " +
			"It shares land borders with Scotland to the north and Wales to the west; the Irish Sea is to the north west, the Celtic Sea to the south west, while the North Sea to the east and the English Channel to the south separate it from continental Europe. " +
			"Most of England comprises the central and southern part of the island of Great Britain in the North Atlantic. " +
			"The country also includes over 100 smaller islands such as the Isles of Scilly and the Isle of Wight.";
		description[3]="Germany , officially the Federal Republic of Germany , is a federal parliamentary republic in Europe. " +
			"The country consists of 16 states while the capital and largest city is Berlin. " +
			"Germany covers an area of 357,021 km2 and has a largely temperate seasonal climate. " +
			"With 81.8 million inhabitants, it is the most populous member state and the largest economy in the European Union. " +
			"It is one of the major political powers of the European continent and a technological leader in many fields.";
		description[4]="The United States of America is a federal constitutional republic comprising fifty states and a federal district. " +
			"The country is situated mostly in central North America, where its forty-eight contiguous states and Washington, D.C., the capital district, lie between the Pacific and Atlantic Oceans, bordered by Canada to the north and Mexico to the south. " +
			"The state of Alaska is in the northwest of the continent, with Canada to the east and Russia to the west, across the Bering Strait. " +
			"The state of Hawaii is an archipelago in the mid-Pacific. The country also possesses several territories in the Pacific and Caribbean.";
		
		setDisplay(0);//set the first country(Canada)for display
		
		textArea.setLineWrap(true);//set lineWrap and wrapStyleWord true for the text area
		textArea.setWrapStyleWord(true);
		textArea.setEditable(false);
		
		add(box,BorderLayout.NORTH);//add the combo box
		add(label1,BorderLayout.WEST);//add the label
		add(textArea,BorderLayout.CENTER);//add the description
		add(button,BorderLayout.SOUTH);//add the button
		
		button.addActionListener(this);//active button event here
		
		box.addItemListener(new ItemListener() //Register listener
		{
			public void itemStateChanged(ItemEvent e)//Handle item selection 
			{
				setDisplay(box.getSelectedIndex());
			}
		});
		
		//create a new frame to hold the histogram panel
		histogramFrame.add(histogram);
		histogramFrame.setTitle("Histogram");
		histogramFrame.setSize(1000, 700);
	}
	
	public void setDisplay(int index)//set display information 
	{
		textArea.setText(description[index]);
		label1.setIcon(image[index]);
	}
	
	public int[] countLetters()//count the letters in the text area
	{
		int[] count=new int[26];
		String text=textArea.getText();
		for(int a=0;a<text.length();a++)
		{
			char character=text.charAt(a);
			if((character>='A')&&(character<='Z'))
			{
				count[character-'A']++;
			}
			else if((character>='a')&&(character<='z'))
			{
				count[character-'a']++;
			}
		}
		return count;
	}
	
	public static void main(String[] args)
	{
		A101 framework =new A101();//create a frame
		framework.setTitle("A10");//set the frame title
		framework.setSize(800, 500);//set the frame size
		framework.setLocationRelativeTo(null);//center a frame
		framework.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		framework.setVisible(true);//display the frame
	}//end main

	public void actionPerformed(ActionEvent e)//Handle the button action 
	{
		if(e.getSource()==button)
		{
			int[] count=countLetters();
			histogram.showHistogram(count);//set the letter count to histogram for display
			histogramFrame.setVisible(true);//show the frame
		}
	}
}
