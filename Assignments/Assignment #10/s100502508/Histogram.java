package a10.s100502508;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.color.ColorSpace;

import javax.swing.JPanel;

public class Histogram extends JPanel//count the occurrences of 26 letters
{
	private int[] count;
	private Font font=new Font("SansSerif",Font.BOLD,17);//create font
	
	public void showHistogram(int[] count)//set the count and display histogram
	{
		this.count=count;
		repaint();//repaint panel
	}
	
	protected void paintComponent(Graphics g)//paint the histogram 
	{
		super.paintComponent(g);
		
		int width=getWidth();
		int height=getHeight();
		int interval=(int)(getWidth()/53);
		int smallinterval=(int)(interval/3);
		int counter;
		int times=0;
		
		g.setFont(font);//set font
		g.drawLine(0, height-100, width, height-100);//draw line
		g.drawString("A",interval+smallinterval , height-80);//draw strings
		g.drawString("B",interval*3+smallinterval , height-80);
		g.drawString("C",interval*5+smallinterval , height-80);
		g.drawString("D",interval*7+smallinterval , height-80);
		g.drawString("E",interval*9+smallinterval , height-80);
		g.drawString("F",interval*11+smallinterval , height-80);
		g.drawString("G",interval*13+smallinterval , height-80);
		g.drawString("H",interval*15+smallinterval , height-80);
		g.drawString("I",interval*17+smallinterval , height-80);
		g.drawString("J",interval*19+smallinterval , height-80);
		g.drawString("K",interval*21+smallinterval , height-80);
		g.drawString("L",interval*23+smallinterval , height-80);
		g.drawString("M",interval*25+smallinterval , height-80);
		g.drawString("N",interval*27+smallinterval , height-80);
		g.drawString("O",interval*29+smallinterval , height-80);
		g.drawString("P",interval*31+smallinterval , height-80);
		g.drawString("Q",interval*33+smallinterval , height-80);
		g.drawString("R",interval*35+smallinterval , height-80);
		g.drawString("S",interval*37+smallinterval , height-80);
		g.drawString("T",interval*39+smallinterval , height-80);
		g.drawString("U",interval*41+smallinterval , height-80);
		g.drawString("V",interval*43+smallinterval , height-80);
		g.drawString("W",interval*45+smallinterval , height-80);
		g.drawString("X",interval*47+smallinterval , height-80);
		g.drawString("Y",interval*49+smallinterval , height-80);
		g.drawString("Z",interval*51+smallinterval , height-80);
		
		for(int a=0;a<=50;a++)//draw the histogram
		{
			a++;
			counter=count[times];
			if(times%7==0)//set color
			{
				g.setColor(Color.RED);
			}
			else if(times%7==1)
			{
				g.setColor(Color.ORANGE);
			}
			else if(times%7==2)
			{
				g.setColor(Color.YELLOW);
			}
			else if(times%7==3)
			{
				g.setColor(Color.GREEN);
			}
			else if(times%7==4)
			{
				g.setColor(Color.CYAN);
			}
			else if(times%7==5)
			{
				g.setColor(Color.BLUE);
			}
			else 
			{
				g.setColor(Color.MAGENTA);
			}
			int[] xPoints={interval*a,interval*(a+1),interval*(a+1),interval*a};
			int[] yPoints={height-100,height-100,height-100-counter*7,height-100-counter*7};
			g.fillPolygon(xPoints, yPoints, xPoints.length);
			times++;
		}//end for	
	}
}
