package a10.s100502010;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.*;

public class FrameWork extends JFrame
{
	private DescriptionPanel dpanel=new DescriptionPanel();
	private JButton histo=new JButton("Histogram button");
	private ImageIcon[] flages={
			new ImageIcon("image/Canada.gif"),new ImageIcon("image/China.gif"),new ImageIcon("image/England.gif"),
			new ImageIcon("image/German.gif"),new ImageIcon("image/USA.gif")};
	private String[] flagtitle={"Canada","China","England","German","USA"};
	private JComboBox jcbo=new JComboBox(flagtitle);
	private String flagdes[]=new String[5];
	Histogram histogram=new Histogram();
	private JFrame histogramFrame=new JFrame();

	
	public FrameWork()
	{//description
		flagdes[0]="Canada is a North American country consisting of ten provinces and three territories.Located in \n" +
				"the northern part of the continent, it extends from the Atlantic Ocean in the east to the Pacific Ocean\n " +
				"in the west, and northward into the Arctic Ocean. Spanning over 9.9 million square kilometres, Canada is\n" +
				" the world's second-largest country by total area, and its common border with the United States is the \n" +
				"longest land border in the world.";
		flagdes[1]="China,officially the People's Republic of China (PRC), is the world's most-populous country, with a population\n" +
				" of over 1.3 billion. The East Asian state covers approximately 9.6 million square kilometres, and is the\n" +
				"world's second-largest country by land area,and the third- or fourth-largest in total area, depending on\n" +
		        "the definition of total area.";
		flagdes[2]="England is a country that is part of the United Kingdom.It shares land borders with Scotland to the north\n" +
				" and Wales to the west; the Irish Sea is to the north west, the Celtic Sea to the south west, while the North\n" +
				" Sea to the east and the English Channel to the south separate it from continental Europe. Most of England \n" +
				"comprises the central and southern part of the island of Great Britain in the North Atlantic. The country also\n" +
				" includes over 100 smaller islands such as the Isles of Scilly and the Isle of Wight.";
		flagdes[3]="Germany,officially the Federal Republic of Germany,is a federal parliamentary republic in Europe. The country\n" +
				" consists of 16 states while the capital and largest city is Berlin. Germany covers an area of 357,021 km2 and has\n" +
				" a largely temperate seasonal climate. With 81.8 million inhabitants, it is the most populous member state and the\n" +
				" largest economy in the European Union. It is one of the major political powers of the European continent and a\n" +
				" technological leader in many fields.";
		flagdes[4]="The United States of America is a federal constitutional republic comprising fifty states and a federal district.\n" +
				" The country is situated mostly in central North America, where its forty-eight contiguous states and Washington, D.C.,\n" +
				" the capital district, lie between the Pacific and Atlantic Oceans, bordered by Canada to the north and Mexico to the \n" +
				" south. The state of Alaska is in the northwest of the continent, with Canada to the east and Russia to the west, across\n" +
				" the Bering Strait. The state of Hawaii is an archipelago in the mid-Pacific. The country also possesses several " +
				"territories in the Pacific and Caribbean.";
		setDisplay(0);
		add(jcbo,BorderLayout.NORTH);
		add(dpanel,BorderLayout.CENTER);
		add(histo,BorderLayout.SOUTH);
		histo.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)//when histogram button was pressed
			{
				if(e.getSource()==histo)
				{
					int[] count=countLetters();
					histogram.showhistogram(count);
					histogramFrame.setVisible(true);
				}
			}
		});
		jcbo.addItemListener(new ItemListener()//check which item was selected
		{
			public void itemStateChanged(ItemEvent e)
			{
				setDisplay(jcbo.getSelectedIndex());
			}
		});
	}
	public int[] countLetters()
	{
		int[] count=new int[26];
		String text=flagdes[jcbo.getSelectedIndex()];
		for(int counter=0;counter<text.length();counter++)
		{
			char character=text.charAt(counter);
			if((character>='A')&&(character<='Z'))
			{
				count[character-'A']++;
			}
			else if((character>='a')&&(character<='z'))
			{
				count[character-'a']++;
			}
			histogramFrame.add(histogram);
			histogramFrame.pack();
			
		}
	
		return count;		
	}	
	public void setDisplay(int index)//switch the flag and description when selecting different item
	{
		dpanel.setTitle(flagtitle[index]);
		dpanel.setsetImageIcon(flages[index]);
		dpanel.setDescription(flagdes[index]);
	}
	
}
