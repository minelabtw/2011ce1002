package a10.s100502010;

import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;


public class Histogram extends JPanel
{
	private int[] count;
	
	public void showhistogram(int[] count1) 
	{
		count=count1;
		repaint();
	}
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		int width=getWidth();
		int height=getHeight();
		int interval=(width-40)/count.length;
		int individualWidth=(int)((width-40)/24*0.60);
		
		int max=0;
		for(int counter=0;counter<count.length;counter++)
		{
			if(max<count[counter])
			{
				max=count[counter];
			}
		}
		int x=30;
		g.drawLine(10, height-45, width-10, height-45);  //draw histogram
		for(int counter=0;counter<count.length;counter++)
		{
			int barheight=(int)(((double)count[counter]/(double)(max))*(height-30));
			
			g.drawRect(x,height-45-barheight,individualWidth,barheight);
			g.drawString((char)(65+counter)+"",x,height-30);
			x+=interval;		
		}
	}
	public Dimension getPreferredSize()
	{
		return new Dimension(300,300);
	}
	

}
