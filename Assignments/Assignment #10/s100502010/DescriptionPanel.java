package a10.s100502010;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class DescriptionPanel extends JPanel
{
	JLabel flag=new JLabel();
	JTextArea desc=new JTextArea();
	
	public DescriptionPanel()
	{
		flag.setHorizontalAlignment(JLabel.CENTER);     //
		flag.setHorizontalTextPosition(JLabel.CENTER);
		flag.setVerticalTextPosition(JLabel.BOTTOM);
		desc.setLineWrap(true);
		desc.setWrapStyleWord(true);
		desc.setEditable(false);
		setLayout(new BorderLayout(20,20));
		add(flag,BorderLayout.WEST);
		add(desc,BorderLayout.CENTER);
	}	
	public void setTitle(String string) 
	{
		flag.setText(string);//change the country name
		
	}
	public void setsetImageIcon(ImageIcon icon) 
	{
		flag.setIcon(icon);//change flag
	}

	public void setDescription(String string) 
	{
		desc.setText(string);//change the description
	}
}
