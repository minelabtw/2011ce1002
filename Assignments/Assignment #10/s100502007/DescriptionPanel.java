package a10.s100502007;
import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DescriptionPanel extends JPanel{
	private JLabel jlblImageTitle = new JLabel();
	private JTextArea jtaDescription = new JTextArea();
	private JButton jbtShowHistogram = new JButton("Histogram");
	private Histogram histogram = new Histogram();
	private JFrame histogramFrame = new JFrame();
	
	public DescriptionPanel(){
		//center the icon,text,placethe text under the icon
		jlblImageTitle.setHorizontalAlignment(JLabel.CENTER);
		jlblImageTitle.setHorizontalTextPosition(JLabel.CENTER);
		//set the font in the label and the text field
		jlblImageTitle.setFont(new Font("SansSerif",Font.BOLD,16));
		jlblImageTitle.setFont(new Font("Serif",Font.PLAIN,14));
		//set linewrap and wrapstyleword true for the text area
		jtaDescription.setLineWrap(true);
		jtaDescription.setWrapStyleWord(true);
		jtaDescription.setEditable(false);
		//create scroll pane
		JScrollPane scrollPane = new JScrollPane(jtaDescription);
		//setborderlayut for panel,label,scrollpane
		setLayout(new BorderLayout(5,5));
		add(scrollPane,BorderLayout.CENTER);
		add(jlblImageTitle,BorderLayout.WEST);
		add(jbtShowHistogram,BorderLayout.SOUTH);
		
		jbtShowHistogram.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				int[] count = countLetters();//count the letters in the text area
				histogram.showHistogram(count);//set the letter count to histogram for display
				histogramFrame.setVisible(true);//show the frame
			}
		});
		//creater a new frame to hold the panel
		histogramFrame.add(histogram);
		histogramFrame.pack();
		histogramFrame.setTitle("Histogram");
	}
	private int[] countLetters(){
		int[] count = new int[26];//count for 26 letters
		String text = jtaDescription.getText();//get contents from the text area
		for(int i=0;i<text.length();i++){//count occurrences of each letter(case insensitive)
			char character = text.charAt(i);
			if((character>='A')&&(character<='Z')){
				count[character-'A']++;
			}
			else if((character>='a')&&(character<='z')){
				count[character-'a']++;
			}
		}
		return count;//return count array
	}
	public void setTitle(String title){//set title
		jlblImageTitle.setText(title);
	}
	public void setImageIcon(ImageIcon icon){//set icon
		jlblImageTitle.setIcon(icon);
	}
	public void setDescription(String text){//settext description
		jtaDescription.setText(text);
	}
}
