package a10.s100502007;
import javax.swing.*;
import java.awt.*;

public class Histogram extends JPanel{
	//count 26 letters
	private int[] count;
	//set the count and display histogram
	public void showHistogram(int[] count){
		this.count = count;
		repaint();
	}
	//paint the histogram
	protected void paintComponent(Graphics g){
		if(count==null) return;//no display if count is null
		super.paintComponent(g);
		//find the panel size and bar width and interval dynamically
		int width = getWidth();
		int height = getHeight();
		int interval = (width-40)/count.length;
		int individualWidth = (int)(((width-40)/24)*0.60);
		//find the maximum count.The maximum count has the highest bar
		int maxCount = 0;
		for(int i= 0;i<count.length;i++){
			if(maxCount<count[i]){
				maxCount = count[i];
			}
		}
		int x = 30;//x is the start position for the first bar in the histogram
		//draw a horizontal base line
		g.drawLine(10,height-45,width-40,height-45);
		for(int i =0;i<count.length;i++){
			int barHeight = (int)(((double)count[i]/(double)maxCount)*(height-55));//find the bar height
			g.drawRect(x, height-45-barHeight, individualWidth, barHeight);//displat a bar
			g.drawString((char)(65+i)+"",x,height-30);//display a letter under the base line
			x+=interval;//move x to next character
		}
	}
	public Dimension getPreferredSize(){//override getPreferredSize
		return new Dimension(300,300);
	}
}
