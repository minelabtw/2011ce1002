package a10.s100502509;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.*;

public class DescriptionPanel extends JPanel{
	public JLabel jlblImageTitle = new JLabel();
	public JTextArea jtaDescription = new JTextArea();
	
	public DescriptionPanel(){
		
		jlblImageTitle.setHorizontalAlignment(JLabel.CENTER);
		jlblImageTitle.setHorizontalTextPosition(JLabel.CENTER);
		jlblImageTitle.setVerticalTextPosition(JLabel.BOTTOM);
		jlblImageTitle.setFont(new Font("SansSerif",Font.BOLD,16));
		jlblImageTitle.setFont(new Font("Serif",Font.BOLD,14));
		
		jtaDescription.setLineWrap(true);
		jtaDescription.setWrapStyleWord(true);
		jtaDescription.setEditable(true);
		
		JScrollPane scroll = new JScrollPane(jtaDescription);
		
		setLayout(new BorderLayout(5,5));
		add(scroll,BorderLayout.CENTER);
		add(jlblImageTitle,BorderLayout.WEST);
		
	}
	
	public void setTitle(String title){//function to set Text
		jlblImageTitle.setText(title);
	}
	
	public void setImageIcon(ImageIcon icon){//function to set Image
		jlblImageTitle.setIcon(icon);
	}
	
	public void setDescription(String text){//function to set flag description
		jtaDescription.setText(text);
	}
}
	


