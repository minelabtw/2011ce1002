package a10.s100502509;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.*;

public class ComboBox extends JFrame{
	
	public DescriptionPanel descriptionPanel = new DescriptionPanel();//Declare object descriptionPanel
	public String[] flagTitle = {"Canada","China","England","German","USA"};
	public JComboBox jcbo = new JComboBox(flagTitle);//use JComboBox to show item
	private ImageIcon[] flagImage={
			new ImageIcon("image/Canada.gif"),
			new ImageIcon("image/China.gif"),
			new ImageIcon("image/England.gif"),
			new ImageIcon("image/German.gif"),
			new ImageIcon("image/USA.gif")
	};
	
	public String [] flagDescription = new String[5];
	private JButton jbtShowHistogram=new JButton("Show Histogram");
	private Histogram histogram=new Histogram();
	private JFrame histogramFrame=new JFrame();
	public ComboBox(){
		
		flagDescription[0] = "The Maple Leaf flag";
		flagDescription[1] = "This is the flag of China";
		flagDescription[2] = "This is the flag of Emgland";
		flagDescription[3] = "This is the flag of German";
		flagDescription[4] = "This is the flag of USA";	
		
		//JComboBox jcbo = new JComboBox(new Object[]{"Canada","China","England","German","USA"});
		
		
		setDisplay(0);

		add(jcbo, BorderLayout.NORTH);//add JComboBox to JFrame
		add(descriptionPanel ,BorderLayout.CENTER );//add descriptionPanel to JFrame
		add(jbtShowHistogram,BorderLayout.SOUTH);//add jbtShowHistogram to JFrame
		jcbo.addItemListener(new ItemListener() {
			

			public void itemStateChanged(ItemEvent e) {//Change item
				// TODO Auto-generated method stub
				setDisplay(jcbo.getSelectedIndex());
			}
		});
		
			jbtShowHistogram.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {//use another windows
				// TODO Auto-generated method stub
				int[] count=countLetters(jcbo.getSelectedIndex());
				histogram.showHistogram(count);
				histogramFrame.setVisible(true);
				
			}
		});
			
		histogramFrame.add(histogram);
		histogramFrame.pack();
		histogramFrame.setTitle("Histogram");
		 
	}
	
	public void setDisplay(int index){
		descriptionPanel.setTitle(flagTitle[index]);//set flagTitle 
		descriptionPanel.setImageIcon(flagImage[index]);//set Image
		descriptionPanel.setDescription(flagDescription[index]);//set flag description
		
	}
	
	private int[] countLetters(int index)
	{
		int[] count=new int[26];
		String text=flagDescription[index];
		
		for(int i=0;i<text.length();i++)
		{
			char character=text.charAt(i);
			if((character>='A')&&(character<='Z'))
			{
				count[character-'A']++;
			}
			else if ((character>='a')&&(character<='z'))
			{
				count[character-'a']++;
			}
		}
		
		return count;
	}

}
