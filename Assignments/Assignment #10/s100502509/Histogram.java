package a10.s100502509;

import java.awt.Dimension;

import java.awt.Graphics;

import javax.swing.JPanel;

public class Histogram extends JPanel{
	private int[] count;
	public void showHistogram(int[] count)//set the count and display histogram
	{
		this.count=count;
		repaint();
	}
	
	protected void paintComponent(Graphics g) {//graph picture to count A to Z 
		if(count==null) return;//No display if count is null
		
		super.paintComponent(g);
		//Find the panel size and bar width and interval dynamically
		int width=getWidth();
		int height=getHeight();
		int interval=(width-40)/count.length;
		int individualWidth=(int)(((width-40)/24)*0.6);
		
		int maxCount=0;
		for(int i=0;i<count.length;i++)
		{
			if(maxCount<count[i])
				maxCount=count[i];
		}
		
		int x=30;
		
		g.drawLine(10, height-45, width-10, height-45);
		for(int i=0;i<count.length;i++)
		{
			int barHeight=(int)(((double)count[i]/(double)maxCount)*(height-55));
			g.drawRect(x, height-45-barHeight, individualWidth, barHeight);
			g.drawString((char)(65+i)+"", x, height-30);
			
			x+=interval;
		}
	}
	
	public Dimension getPreferredSize()
	{
		return new Dimension(300,300);
	}

}
