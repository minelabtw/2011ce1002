package a10.s100502002;
import javax.swing.*;
import java.awt.*;
public class Histogram extends JPanel
{
	private int[] count;
	
	public void showHistogram(int[] count)//接收次數，然後畫圖
	{
		this.count = count;
		repaint();
	}
	
	protected void paintComponent(Graphics g)//畫圖
	{
		if(count==null) return;
		
		super.paintComponent(g);
		
		int w = getWidth();
		int h = getHeight();
		int interval= (w-40)/count.length;
		int individualwidth = (int) (((w-40)/24)*0.60);
		int maxcount = 0;
		
		for(int i=0;i<count.length;i++)
		{
			if(maxcount<count[i])
				maxcount=count[i];
		}
		
		int x = 30;//第一個開始點
		
		g.drawLine(10, h-45,w-10,h-45);
		
		/*以下都在話長條圖*/
		
		int barh = (int)(((double)count[0]/(double)maxcount)*(h-55));
		g.drawRect(x, h-45-barh, individualwidth, barh);
		
		int barh1 = (int)(((double)count[1]/(double)maxcount)*(h-55));
		g.drawRect(x+interval, h-45-barh1, individualwidth, barh1);
		
		int barh2 = (int)(((double)count[2]/(double)maxcount)*(h-55));
		g.drawRect(x+2*interval, h-45-barh2, individualwidth, barh2);
		
		int barh3 = (int)(((double)count[3]/(double)maxcount)*(h-55));
		g.drawRect(x+3*interval, h-45-barh3, individualwidth, barh3);
		
		int barh4 = (int)(((double)count[4]/(double)maxcount)*(h-55));
		g.drawRect(x+4*interval, h-45-barh4, individualwidth, barh4);
		
		g.drawString("Canada   ",x, h-30);
		g.drawString("China   ",x+interval, h-30);
		g.drawString("England   ",x+2*interval, h-30);
		g.drawString("German   ",x+3*interval, h-30);
		g.drawString("USA   ",x+4*interval, h-30);
		
	}
	
	
	public Dimension getreferredSize()
	{
		return new Dimension(300,300);
	}

}
