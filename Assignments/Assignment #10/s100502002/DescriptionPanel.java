package a10.s100502002;
import javax.swing.*;
import java.awt.*;
public class DescriptionPanel extends JPanel
{
	private JLabel title = new JLabel();
	private JTextArea  text = new JTextArea();
	public DescriptionPanel()
	{
		title.setHorizontalAlignment(JLabel.CENTER);
		title.setHorizontalTextPosition(JLabel.CENTER);
		title.setVerticalTextPosition(JLabel.BOTTOM);
		
		title.setFont(new Font("SanSerif",Font.BOLD,16));
		text.setFont(new Font("Serif",Font.PLAIN,14));//字形
		
		text.setLineWrap(true);
		text.setWrapStyleWord(true);
		text.setEditable(false);//設定性質
		
		JScrollPane scrollPane = new JScrollPane(text);//卷軸
		
		setLayout(new BorderLayout(5,5));
		add(scrollPane,BorderLayout.CENTER);
		add(title,BorderLayout.WEST);//排版
		
	}
	
	/*國家名稱*/
	public void settitle(String titlename)
	{
		title.setText(titlename);
	}
	
	
	/*圖片攔*/
	public void setimage(ImageIcon icon)
	{
		title.setIcon(icon);
	}
	
	/*文字欄*/
	
	public void setdescription(String data)
	{
		text.setText(data);
	}
	

}
