package a10.s100502002;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
public class A101 extends JFrame
{
	/* 上面是宣告一堆東西 */
	private ImageIcon country[] ={new ImageIcon("image/Canada.gif"),new ImageIcon("image/China.gif"), new ImageIcon("image/England.gif"), new ImageIcon("image/German.gif"),new ImageIcon("image/USA.gif")};
	private String[] flagtitle = {"Canada","China","England","German","USA"};
	
	private JComboBox jcbo =  new JComboBox(flagtitle);
	private String [] description = new String[9];
	private DescriptionPanel descriptionPanel = new DescriptionPanel();
	
	private JButton jbt = new JButton("show histogram");
	private Histogram h = new Histogram();
	
	private JFrame hf = new JFrame();
	private int [] count = new int [5];
	
	
	/*這理是主程式*/
	public static void main(String[] args)
	{
	 	A101 frame = new A101();
		frame.pack();
		frame.setTitle("A101");
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setSize(800,600);
		
	}
	
	
	
	/*把內容寫在construct*/
	public A101()
	{
		description[0]= "Canada is a North American country consisting of ten provinces and three territories. Located in the northern part of the continent, it extends from the Atlantic Ocean in the east to the Pacific Ocean in the west, and northward into the Arctic Ocean. Spanning over 9.9 million square kilometres, Canada is the world's second-largest country by total area, and its common border with the United States is the longest land border in the world.";
		description[1]= "China officially the People's Republic of China (PRC), is the world's most-populous country, with a population of over 1.3 billion. The East Asian state covers approximately 9.6 million square kilometres, and is the world's second-largest country by land area, and the third- or fourth-largest in total area, depending on the definition of total area.";
		description[2]= "England is a country that is part of the United Kingdom.It shares land borders with Scotland to the north and Wales to the west; the Irish Sea is to the north west, the Celtic Sea to the south west, while the North Sea to the east and the English Channel to the south separate it from continental Europe. Most of England comprises the central and southern part of the island of Great Britain in the North Atlantic. The country also includes over 100 smaller islands such as the Isles of Scilly and the Isle of Wight.";
		description[3]= "Germany officially the Federal Republic of Germany (German: Bundesrepublik Deutschland, pronounced is a federal parliamentary republic in Europe. The country consists of 16 states while the capital and largest city is Berlin. Germany covers an area of 357,021 km2 and has a largely temperate seasonal climate. With 81.8 million inhabitants, it is the most populous member state and the largest economy in the European Union. It is one of the major political powers of the European continent and a technological leader in many fields.";
		description[4]= "The United States of America (commonly abbreviated to the United States, the U.S., the USA, America, and the States) is a federal constitutional republic comprising fifty states and a federal district. The country is situated mostly in central North America, where its forty-eight contiguous states and Washington, D.C., the capital district, lie between the Pacific and Atlantic Oceans, bordered by Canada to the north and Mexico to the south. The state of Alaska is in the northwest of the continent, with Canada to the east and Russia to the west, across the Bering Strait. The state of Hawaii is an archipelago in the mid-Pacific. The country also possesses several territories in the Pacific and Caribbean.";
		//文字敘述
		setDisplay(0);//如果沒選就顯示第一個
		
		add(jcbo,BorderLayout.NORTH);
		add(descriptionPanel,BorderLayout.CENTER);
		
		jcbo.addItemListener(new ItemListener() 
		{
			public void itemStateChanged(ItemEvent e)
			{
				setDisplay(jcbo.getSelectedIndex());//判斷選到哪個
				count[jcbo.getSelectedIndex()]++;//按一次加一次count
			}
		});
		
		add(jbt,BorderLayout.SOUTH);
		
		jbt.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)//新的視窗開啟
			{
				h.showHistogram(count);//傳次數進去
				hf.setSize(600,800);
				hf.setVisible(true);
			}
		});
		
		hf.add(h);
		hf.pack();
		hf.setTitle("histogram");
		
	}
	
	public void setDisplay(int index)//顯示各種東西
	{
		descriptionPanel.settitle(flagtitle[index]);
		descriptionPanel.setimage(country[index]);
		descriptionPanel.setdescription(description[index]);
	}
}
