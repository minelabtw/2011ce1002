package a10.s995002056;

import javax.swing.*;
import java.awt.*;

public class Histogram extends JPanel {

	private int[] count;

	// 設定字母出現次數，然後畫出圖形
	public void showHistogram(int[] count) {
		this.count = count;
		repaint();
	}

	// 畫圖
	protected void paintComponent(Graphics g) {
		if (count == null)
			return; // 若字母沒出現，則不畫圖

		super.paintComponent(g);

		// 設定長寬
		int width = getWidth();
		int height = getHeight();
		int interval = (width - 40) / count.length;
		int w = (int) (((width - 40) / 24) * 0.60);

		// 找出出現次數最多的字母，然後設定最高
		int maxCount = 0;
		for (int i = 0; i < count.length; i++) {
			if (maxCount < count[i])
				maxCount = count[i];
		}

		// 開始位置
		int x = 30;

		// 最低的水平線
		g.drawLine(10, height - 45, width - 10, height - 45);
		for (int i = 0; i < count.length; i++) {
			// 計算出長條圖的高
			int barHeight = (int) (((double) count[i] / (double) maxCount) * (height - 55));

			g.drawRect(x, height - 45 - barHeight, w, barHeight);
			// 字母
			g.drawString((char) (65 + i) + "", x, height - 30);

			// 下一個字母
			x += interval;
		}
	}

	// 設定視窗大小
	public Dimension getPreferredSize() {
		return new Dimension(300, 300);
	}
}