package a10.s995002056;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener {
	// 宣告
	JButton Histogram = new JButton("Histogram");
	JPanel p1 = new JPanel();
	JLabel flag = new JLabel();
	JTextArea des = new JTextArea();

	JComboBox cb = new JComboBox(new Object[] { "USA", "Canada", "German",
			"England", "China" });

	String[] description = new String[5];

	ImageIcon[] flagImage = { new ImageIcon("image/USA.gif"),
			new ImageIcon("image/Canada.gif"),
			new ImageIcon("image/German.gif"),
			new ImageIcon("image/England.gif"),
			new ImageIcon("image/China.gif") };

	private JFrame HistogramFrame = new JFrame();
	private Histogram histogram = new Histogram();

	FrameWork() {
		// 新增描述
		description[0] = "The United States of America (commonly abbreviated to the United States, the U.S., the USA, America, and the States) is a federal constitutional republic comprising fifty states and a federal district. ";
		description[1] = "Canada is a North American country consisting of ten provinces and three territories.";
		description[2] = "Germany, officially the Federal Republic of Germany ,is a federal parliamentary republic in Europe.";
		description[3] = "England is a country that is part of the United Kingdom.It shares land borders with Scotland to the north and Wales to the wes";
		description[4] = "China officially the People's Republic of China (PRC), is the world's most-populous country, with a population of over 1.3 billion.";
		des.setFont(new Font("微軟正黑體", Font.PLAIN, 14));
		des.setLineWrap(true);
		des.setWrapStyleWord(true);
		des.setEditable(true);

		p1.setLayout(new GridLayout(1, 2));
		p1.add(flag);
		p1.add(des);

		setDisplay(0);// 預設旗子及描述
		Histogram.addActionListener(this);

		add(Histogram, BorderLayout.SOUTH);
		add(cb, BorderLayout.NORTH);
		add(p1, BorderLayout.CENTER);

		// 若comboBox改變則發生事件
		cb.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				setDisplay(cb.getSelectedIndex());
			}
		});

		// 跳出新視窗
		HistogramFrame.add(histogram);
		HistogramFrame.pack();
		HistogramFrame.setTitle("Histogram");
	}

	// 設定旗子及描述
	public void setDisplay(int i) {
		flag.setIcon(flagImage[i]);
		des.setText(description[i]);
	}

	// 按鈕事件
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == Histogram) {
			int[] count = countLetters();

			// 將結果輸入
			histogram.showHistogram(count);

			// 顯示視窗
			HistogramFrame.setVisible(true);
		}
	}

	private int[] countLetters() {
		int[] count = new int[26];

		// 把文字輸入
		String text = des.getText();

		// 計算每個字母的出現次數
		for (int i = 0; i < text.length(); i++) {
			char character = text.charAt(i);

			if ((character >= 'A') && (character <= 'Z')) {
				count[(int) character - 65]++;
			} else if ((character >= 'a') && (character <= 'z')) {
				count[(int) character - 97]++;
			}
		}

		return count;
	}
}
