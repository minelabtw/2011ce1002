package a10.s985003038;

import javax.swing.JFrame;

public class A101 {
	public static void main(String[] args) {
		FrameWork frame = new FrameWork();
		frame.pack();
		frame.setTitle("Assignment #10");
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
