package a10.s985003038;

import javax.swing.*;
import java.awt.*;

public class Histogram extends JPanel {
	private static final long serialVersionUID = 1L;
	
	// Count the occurrence of 5 flags
	private int[] count;
	
	private String[] flagTitles = {"Canada", "China", "England", "Germany", "USA"};
	private Color[] color = {Color.blue, Color.red, Color.green, Color.yellow, Color.orange};

	/** Set the count and display histogram */
	public void showHistogram(int[] count) {
		this.count = count;
		repaint();
	}

	/** Paint the histogram */
	protected void paintComponent(Graphics g) {
		if (count == null) return; // No display if count is null

		super.paintComponent(g);

		// Find the panel size and bar width and interval dynamically
		int width = getWidth();
		int height = getHeight();
		int interval = (width - 40) / count.length;
		int individualWidth = (int)(((width - 40) / 24) * 0.60);

		// Find the maximum count. The maximum count has the highest bar
		int maxCount = 0;
		for (int i = 0; i < count.length; i++) {
			if (maxCount < count[i])
				maxCount = count[i];
		}

		// x is the start position for the first bar in the histogram
		int x = 30;

		// Draw a horizontal base line
		g.drawLine(10, height - 45, width - 10, height - 45);
		for (int i = 0; i < count.length; i++) {
			// Find the bar height
			int barHeight =
				(int)(((double)count[i] / (double)maxCount) * (height - 65));
			
			// Display a bar (i.e. rectangle)
			g.setColor(color[i]);
			g.fillRect(x, height - 45 - barHeight, individualWidth, barHeight);
			g.drawString(String.valueOf(count[i]), x + 2 * individualWidth, height - 50 - barHeight);
			
			// Display a letter under the base line
			g.setColor(Color.black);
			g.drawString(flagTitles[i], x, height - 30);

			// Move x for displaying the next character
			x += interval;
		}
	}

	/** Override getPreferredSize */
	public Dimension getPreferredSize() {
		return new Dimension(300, 300);
	}
}
