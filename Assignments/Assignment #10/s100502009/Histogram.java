package a10.s100502009;
import javax.swing.*;
import java.awt.*;

public class Histogram extends JPanel{
	private int[] count;	
	public void showHistogram(int[] count){//show the histogram
		this.count=count;
		repaint();
	}
	
	protected void paintComponent(Graphics g)//draw the frame
	{
		if(count==null) return;
		super.paintComponent(g);
		
		int width=getWidth();
		int height=getHeight();
		int interval=(width-40)/count.length;
		int individualWidth=(int)(((width-40)/24)*0.60);
		
		int maxCount=0;
		for(int i=0;i<count.length;i++)
		{
			if(maxCount<count[i])
				maxCount=count[i];
		}
		
		int x=30;
		g.drawLine(10, height-45, width-10, height-45);
		for(int i=0;i<count.length;i++)
		{
			int barHeight=(int)(((double)count[i]/(double)maxCount)*(height-55));
			if(i%2==0)
				g.setColor(Color.BLUE);
			else
				g.setColor(Color.RED);
			g.fillRect(x, height-44-barHeight, individualWidth, barHeight);
			g.drawString((char)(65+i)+"", x,height-30);
			x+=interval;
		}
	}
}
