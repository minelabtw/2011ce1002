package a10.s100502009;
import javax.swing.*;
import java.awt.*;

public class DescriptionPanel extends JPanel{
	private JLabel jlbImageTitle=new JLabel();
	private JTextArea jtaDescription=new JTextArea();//the descriptions of each countries
	
	public DescriptionPanel()
	{
		jlbImageTitle.setHorizontalAlignment(JLabel.CENTER);
		jlbImageTitle.setHorizontalTextPosition(JLabel.CENTER);
		jlbImageTitle.setVerticalTextPosition(JLabel.BOTTOM);
		
		jlbImageTitle.setFont(new Font("SansSerif",Font.BOLD,16));
		jtaDescription.setFont(new Font("Serif",Font.PLAIN,14));
		
		jtaDescription.setLineWrap(true);
		jtaDescription.setWrapStyleWord(true);
		jtaDescription.setEditable(false);
		
		JScrollPane scrollPane=new JScrollPane(jtaDescription);
		
		setLayout(new BorderLayout(5,5));
		add(scrollPane,BorderLayout.CENTER);
		add(jlbImageTitle,BorderLayout.WEST);
		
	}
	
	public void setTitle(String title)//set the title of a country 
	{
		jlbImageTitle.setText(title);
	}
	
	public void setImageIcon(ImageIcon icon)//set the picture
	{
		jlbImageTitle.setIcon(icon);
	}
	
	public void setDescription(String text)//set description
	{
		jtaDescription.setText(text);
	}
	
	public JTextArea getJta()//get the description
	{
		return jtaDescription;
	}

}
