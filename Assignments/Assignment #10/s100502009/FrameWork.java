package a10.s100502009;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class FrameWork extends JFrame{
	private String[] flagTitles={"Russia","Portugal","South Korean","Singapore","Holland"};	//the countries' names
	private ImageIcon[] flagImage={//the pictures of the countries
		new ImageIcon("image/russia.gif"),
		new ImageIcon("image/portugal.gif"),
		new ImageIcon("image/southkorea.gif"),
		new ImageIcon("image/singapore.gif"),
		new ImageIcon("image/holland.gif")
	};	
	private String[] flagDescription=new String[5];	//the description of each country
	private DescriptionPanel descriptionPanel =new DescriptionPanel();	
	private JComboBox jcbo=new JComboBox(flagTitles);
	private JButton jbtShowHistogram=new JButton("Show Histogram");
	private Histogram histogram=new Histogram();
	private JFrame histogramFrame=new JFrame();
	
	public FrameWork()
	{
		flagDescription[0]="Russia is the largest country in the world;"+
			" its total area is 17,075,400 square kilometres (6,592,800 sq mi)."+
			" There are 23 UNESCO World Heritage Sites in Russia, 40 UNESCO biosphere reserves,"+
			"[116] 40 national parks and 101 nature reserves. It lies between latitudes 41�X and 82�X N,"+
			" and longitudes 19�X E and 169�X W.Russia has a wide natural resource base,"+
			" including major deposits of timber, petroleum, natural gas, coal, ores and other mineral resources.";
		flagDescription[1]="Mainland Portugal is split by its main river,"+
			" the Tagus that flows from Spain and disgorges in Tagus Estuary,"+
			" near Lisbon, before escaping into the Atlantic."+
			" The northern landscape is mountainous towards the interior with several plateaus indented by river valleys,"+
			" whereas the south, that includes the Algarve and the Alentejo regions,"+
			" is characterized by rolling plains.";
		flagDescription[2]="South Korea occupies the southern portion of the Korean Peninsula,"+
			" which extends some 1,100 km (680 mi) from the Asian mainland."+
			" This mountainous peninsula is flanked by the Yellow Sea to the west,"+
			" and Sea of Japan (East Sea) to the east."+
			" Its southern tip lies on the Korea Strait and the East China Sea.";
		flagDescription[3]="Singapore consists of 63 islands,"+
			" including the main island, widely known as Singapore Island but also as Pulau Ujong."+
			"There are two man-made connections to Johor, Malaysia: the Johor�VSingapore Causeway in the north,"+
			" and the Tuas Second Link in the west. Jurong Island, Pulau Tekong,"+
			" Pulau Ubin and Sentosa are the largest of Singapore's smaller islands."+
			" The highest natural point is Bukit Timah Hill at 166 m (545 ft).";
		flagDescription[4]="Holland is situated in the west of the Netherlands."+
			"A maritime region, Holland lies on the North Sea at the mouths of the Rhine and the Meuse (Maas)."+
			" It has numerous rivers and lakes and an extensive inland canal and waterway system."+
			" To the south is Zealand."+
			" The region is bordered on the east by the IJsselmeer and four different provinces of the Netherlands.";
		
		setDisplay(0);//set the Russia the first item
		
		add(jcbo,BorderLayout.NORTH);
		add(descriptionPanel,BorderLayout.CENTER);
		add(jbtShowHistogram,BorderLayout.SOUTH);
		
		jbtShowHistogram.addActionListener(new ActionListener(){//show the histogram
			public void actionPerformed(ActionEvent e)
			{
				int[] count=countLetters();
				histogram.showHistogram(count);
				histogramFrame.setVisible(true);
			}
		});
		
		histogramFrame.add(histogram);
		histogramFrame.setSize(600,500);
		histogramFrame.setTitle("Histogram");
		jcbo.addItemListener(new ItemListener(){//show the combo
			public void itemStateChanged(ItemEvent e)
			{
				setDisplay(jcbo.getSelectedIndex());
			}
		});		
		
	}
	
	public void setDisplay(int index)//show the descriptions
	{
		descriptionPanel.setTitle(flagTitles[index]);
		descriptionPanel.setImageIcon(flagImage[index]);
		descriptionPanel.setDescription(flagDescription[index]);
	}
	
	public int[] countLetters()//to count the letters
	{
		int[] count=new int[26];
		String text=descriptionPanel.getJta().getText();
		
		for(int i=0;i<text.length();i++)
		{
			char character=text.charAt(i);
			if((character>='A')&&(character<='Z'))
			{
				count[character-'A']++;
			}
			else if((character>='a')&&(character<='z'))
			{
				count[character-'a']++;
			}
		}
		return count;
	}


}
