package a10.s982003034;

import java.net.URL;

import javax.swing.ImageIcon;

public class nation_info {
	
	private ImageIcon flag;
	private String desc;
	
	public nation_info (String path, String desc) {
		this.flag = load_image (path);
		this.desc = desc;
	}
	
	private ImageIcon load_image (String path) {
		URL imgurl = getClass().getResource(path);
		if (imgurl != null) return new ImageIcon (imgurl, "");
		else {
			System.err.println ("Cannot find " + path);
			return null;
		}
	}
	
	public ImageIcon get_flag () {
		return flag;
	}
	
	public String get_desc () {
		return desc;
	}
	
}
