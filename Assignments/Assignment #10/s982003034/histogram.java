package a10.s982003034;

import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;

public class histogram extends JPanel {
	
	// height and width of the histogram bars
	private int maxh ; // max height
	private int width;
	private int xaxis; // position of the x axis
			
	private ArrayList <Integer> data_count;
	private ArrayList <String> data_desc;
	private int largest; // largest data in data_count used to determine max graph height
	
	public histogram () {
		this.setSize(800,500);
		data_count = new ArrayList <Integer> ();
		data_desc = new ArrayList <String> ();
		largest = 1;
		maxh = (int) (this.getHeight()*0.8);
		width = 15;
		xaxis = this.getHeight()-20;
	}
	
	public void add_data (String desc, int count) {
		if (count > largest) largest = count;
		data_count.add(count);
		data_desc.add(desc);
		this.repaint();
	}
	
	public int get_count (int index) {
		return data_count.get(index);
	}
	
	public void set_count (int index, int count) {
		if (count > largest) largest = count;
		data_count.set(index,count);
		this.repaint();
	}
	
	public String get_desc (int index) {
		return data_desc.get(index);
	}
	
	public void set_desc (int index, String desc) {
		data_desc.set(index,desc);
		this.repaint();
	}
	
	public void paintComponent (Graphics g) {
		
		super.paintComponent(g);
		
		// draw bars
		for (int i = 0; i < data_count.size(); i++) {
			int length = maxh*data_count.get(i)/largest;
			g.fillRect(	//x,y,w,h
					(2*i+1)*width, (xaxis - length),
					width, length
			); // fillRect
			g.drawString(data_desc.get(i) + data_count.get(i), (2*i+1)*width, (xaxis - (maxh*data_count.get(i)/largest))-10);
		}
		
	}

}
