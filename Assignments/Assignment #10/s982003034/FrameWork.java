package a10.s982003034;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class FrameWork extends JFrame implements ActionListener {

	private ArrayList <nation_info> data;
	private histogram_frame graph;
	private JComboBox<String> cbox;
	private JPanel nation_panel;
	private JLabel flag_label;
	private JTextArea desc_text;
	private JButton show_histogram;
		
	public FrameWork () {
		
		data = new ArrayList<nation_info> ();
		// input data
		// Canada
		data.add(new nation_info (
				"image/Canada.gif",
				"Canada is a North American country consisting of ten provinces and three territories. Located in the northern part of the continent, it extends from the Atlantic Ocean in the east to the Pacific Ocean in the west, and northward into the Arctic Ocean. Spanning over 9.9 million square kilometres, Canada is the world's second-largest country by total area, and its common border with the United States is the longest land border in the world."
				));
		// China
		data.add(new nation_info (
				"image/China.gif",
				"China, officially the People's Republic of China (PRC), is the world's most-populous country, with a population of over 1.3 billion. The East Asian state covers approximately 9.6 million square kilometres, and is the world's second-largest country by land area, and the third- or fourth-largest in total area, depending on the definition of total area"
				));
		// England
		data.add(new nation_info (
				"image/England.gif",
				"England is a country that is part of the United Kingdom. It shares land borders with Scotland to the north and Wales to the west; the Irish Sea is to the north west, the Celtic Sea to the south west, while the North Sea to the east and the English Channel to the south separate it from continental Europe. Most of England comprises the central and southern part of the island of Great Britain in the North Atlantic. The country also includes over 100 smaller islands such as the Isles of Scilly and the Isle of Wight."
				));
		// German
		data.add(new nation_info (
				"image/German.gif",
				"Germany is a federal parliamentary republic in Europe. The country consists of 16 states while the capital and largest city is Berlin. Germany covers an area of 357,021 km2 and has a largely temperate seasonal climate. With 81.8 million inhabitants, it is the most populous member state and the largest economy in the European Union. It is one of the major political powers of the European continent and a technological leader in many fields."
				));
		// USA
		data.add(new nation_info (
				"image/USA.gif",
				"The United States of America is a federal constitutional republic comprising fifty states and a federal district. The country is situated mostly in central North America, where its forty-eight contiguous states and Washington, D.C., the capital district, lie between the Pacific and Atlantic Oceans, bordered by Canada to the north and Mexico to the south. The state of Alaska is in the northwest of the continent, with Canada to the east and Russia to the west, across the Bering Strait. The state of Hawaii is an archipelago in the mid-Pacific. The country also possesses several territories in the Pacific and Caribbean."
				));
		
		// setup histogram frame
		graph = new histogram_frame();
				
		// create menu for combobox
		cbox = new JComboBox <String> (new String[] {"Canada", "China", "England", "German", "USA"});
		cbox.setSelectedIndex(0);
		cbox.addActionListener(this);
		
		// setup flag label
		flag_label = new JLabel ();
		// setup description text field
		desc_text = new JTextArea ();
		desc_text.setEditable(false);
		desc_text.setLineWrap(true);
		desc_text.setWrapStyleWord(true);
		// set the panel to put flag and description
		nation_panel = new JPanel();
		nation_panel.setLayout(new GridLayout (1,2));
		nation_panel.add(flag_label);
		nation_panel.add(desc_text);
		// setup show histogram button
		show_histogram = new JButton ("Show Histogram");
		show_histogram.addActionListener(this);
		
		// setup the frame
		this.setLayout(new BorderLayout());
		this.add(cbox, BorderLayout.NORTH);
		this.add(nation_panel, BorderLayout.CENTER);
		this.add(show_histogram, BorderLayout.SOUTH);
		this.setSize(1000, 400);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		select_nation();
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == cbox){
			select_nation();
			
		}
		if (e.getSource() == show_histogram) {
			graph.setVisible(true);
		}
		
	}

	private void select_nation() {
		// get the nation info from the selected item
		nation_info temp = data.get(cbox.getSelectedIndex());
		// set the flag, text and graph char count
		flag_label.setIcon(temp.get_flag());
		desc_text.setText(temp.get_desc());
		graph.count_char(temp.get_desc());
		
	}
	
	
	
}
