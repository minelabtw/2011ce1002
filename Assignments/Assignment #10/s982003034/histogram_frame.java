	package a10.s982003034;

import javax.swing.JFrame;

public class histogram_frame extends JFrame {

	private histogram graph;
	
	public histogram_frame () {
		
		graph = new histogram ();
		
		// initialize histogram
		for (int i = 0; i < 26; i++) {
			// create data from A to Z
			graph.add_data(""+(char)(65+i), 0);
			//System.out.println(""+(char)(65+i) + (char)(65+32+i));
		} // for
		this.setSize(graph.getSize());
		this.add(graph);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
	public void count_char (String text) {
		
		for (int i = 0; i < text.length(); i++) {
			int index = (int)(text.charAt(i));
			// check if the char is an alphabet
			// check for uppercase
			if (index >= 65 && index <91) {
				//System.out.print(""+(char)(index));
				index = index-65;
				// get the original count
				int count = graph.get_count(index);
				count ++; // add count
				graph.set_count(index, count); // reset count
			} // if
			// check for lower case
			if (index >= 97 && index <123) {
				//System.out.print(""+(char)(index));
				index = (index - 97);				
				//System.out.println(text.charAt(i) + " = " + index + " = " + (char)(index));
				int count = graph.get_count(index);
				count ++; // add count
				graph.set_count(index, count); // reset count
			}
			
		} // for
		
		graph.repaint();
		
	} // count_char
	
	
}
