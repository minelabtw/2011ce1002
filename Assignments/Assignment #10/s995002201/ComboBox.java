package a10.s995002201;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.ItemSelectable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class ComboBox extends JFrame
{
	// Declare an array of Strings for flag titles
	private String[] flagTitles = 
	{
		"Canada", "China", "German", "England", "USA"
	};

	// Declare an ImageIcon array for the national flags of 9 countries
	private ImageIcon[] flagImage = 
	{
		new ImageIcon("Canada.gif"),
		new ImageIcon("China.gif"),
		new ImageIcon("German.gif"),
		new ImageIcon("England.gif"),
		new ImageIcon("USA.gif")
	};

	// Declare an array of strings for flag descriptions
	private String[] flagDescription = new String[9];
	
	// Declare and create a description panel
	private DescriptionPanel descriptionPanel = new DescriptionPanel();

	// Create a combo box for selecting countries
	private JComboBox jcbo = new JComboBox( flagTitles );
	private JButton jbtShowHistogram = new JButton("Histogram Button");
	private JFrame histogramFrame = new JFrame();
	private Histogram histogram = new Histogram();
	
	public ComboBox() 
	{
		// Set text description
		flagDescription[0] = "Canada\n\n a North American country consisting of ten provinces and three territories. Located in the northern part of the continent, it extends from the Atlantic Ocean in the east to the Pacific Ocean in the west, and northward into the Arctic Ocean. Spanning over 9.9 million square kilometres, Canada is the world's second-largest country by total area, and its common border with the United States is the longest land border in the world." ;
		flagDescription[1] = "China\n\n officially the People's Republic of China (PRC), is the world's most-populous country, with a population of over 1.3 billion. The East Asian state covers approximately 9.6 million square kilometres, and is the world's second-largest country by land area,and the third- or fourth-largest in total area, depending on the definition of total area.";
		flagDescription[2] = "Germany\n\n a federal parliamentary republic in Europe. The country consists of 16 states while the capital and largest city is Berlin. Germany covers an area of 357,021 km2 and has a largely temperate seasonal climate. With 81.8 million inhabitants, it is the most populous member state and the largest economy in the European Union. It is one of the major political powers of the European continent and a technological leader in many fields.";
		flagDescription[3] = "England\n\n is a country that is part of the United Kingdom.It shares land borders with Scotland to the north and Wales to the west; the Irish Sea is to the north west, the Celtic Sea to the south west, while the North Sea to the east and the English Channel to the south separate it from continental Europe. Most of England comprises the central and southern part of the island of Great Britain in the North Atlantic. The country also includes over 100 smaller islands such as the Isles of Scilly and the Isle of Wight.";
		flagDescription[4] = "USA\n\n is a federal constitutional republic comprising fifty states and a federal district. The country is situated mostly in central North America, where its forty-eight contiguous states and Washington, D.C., the capital district, lie between the Pacific and Atlantic Oceans, bordered by Canada to the north and Mexico to the south. The state of Alaska is in the northwest of the continent, with Canada to the east and Russia to the west, across the Bering Strait. The state of Hawaii is an archipelago in the mid-Pacific. The country also possesses several territories in the Pacific and Caribbean.";
	 
		// Set the first country (Canada) for display
		setDisplay(0);
	 
		// jcomboBox在最上面
		add(jcbo, BorderLayout.NORTH);
		//中間是國家敘述
		add(descriptionPanel, BorderLayout.CENTER);
		//下面是histogram按鈕
		add(jbtShowHistogram, BorderLayout.SOUTH);
		
		jbtShowHistogram.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				Object selected = jcbo.getSelectedItem();////// 有 selected 才能知道使用者在comboBox清單選了什麼
				if(selected ==flagTitles[0])
				{
					int[] count = countLetters0();
					histogram.showHistogram(count);
					histogramFrame.setVisible(true);
				}
				if(selected ==flagTitles[1])
				{
					int[] count = countLetters1();
					histogram.showHistogram(count);
					histogramFrame.setVisible(true);
				}
				if(selected ==flagTitles[2])
				{
					int[] count = countLetters2();
					histogram.showHistogram(count);
					histogramFrame.setVisible(true);
				}
				if(selected ==flagTitles[3])
				{
					int[] count = countLetters3();
					histogram.showHistogram(count);
					histogramFrame.setVisible(true);
				}
				if(selected ==flagTitles[4])
				{
					int[] count = countLetters4();
					histogram.showHistogram(count);
					histogramFrame.setVisible(true);
				}
			}
		});
		
		//create a new frame to hold the histogram panel
		histogramFrame.add(histogram);
		histogramFrame.pack();
		histogramFrame.setTitle("Histogram");
		
		// Register listener
		jcbo.addItemListener(new ItemListener() 
		{
			/** Handle item selection */
			public void itemStateChanged(ItemEvent e) 
			{
				setDisplay(jcbo.getSelectedIndex());
			}
		});
    }
	
	//選擇的國家分別計算字母多寡
	private int[] countLetters0()
	{
		int[] count = new int[26];
		
		// Get contents from the text area
		String text = flagDescription[0];
		// Count occurrences of each letter (case insensitive)
		for (int i = 0; i < text.length(); i++) 
		{
			char character = text.charAt(i);
			if ((character >= 'A') && (character <= 'Z')) 
			{
				count[character - 'A']++;
			}
			else if ((character >= 'a') && (character <= 'z')) 
			{
				count[character - 'a']++;
			}
		}
	 return count; // Return the count array
	}
	private int[] countLetters1()
	{
		int[] count = new int[26];
		
		// Get contents from the text area
		String text = flagDescription[1];
		// Count occurrences of each letter (case insensitive)
		for (int i = 0; i < text.length(); i++) 
		{
			char character = text.charAt(i);
			if ((character >= 'A') && (character <= 'Z')) 
			{
				count[character - 'A']++;
			}
			else if ((character >= 'a') && (character <= 'z')) 
			{
				count[character - 'a']++;
			}
		}
	 return count; // Return the count array
	}
	private int[] countLetters2()
	{
		int[] count = new int[26];
		
		// Get contents from the text area
		String text = flagDescription[2];
		// Count occurrences of each letter (case insensitive)
		for (int i = 0; i < text.length(); i++) 
		{
			char character = text.charAt(i);
			if ((character >= 'A') && (character <= 'Z')) 
			{
				count[character - 'A']++;
			}
			else if ((character >= 'a') && (character <= 'z')) 
			{
				count[character - 'a']++;
			}
		}
	 return count; // Return the count array
	}
	private int[] countLetters3()
	{
		int[] count = new int[26];
		
		// Get contents from the text area
		String text = flagDescription[3];
		// Count occurrences of each letter (case insensitive)
		for (int i = 0; i < text.length(); i++) 
		{
			char character = text.charAt(i);
			if ((character >= 'A') && (character <= 'Z')) 
			{
				count[character - 'A']++;
			}
			else if ((character >= 'a') && (character <= 'z')) 
			{
				count[character - 'a']++;
			}
		}
	 return count; // Return the count array
	}
	private int[] countLetters4()
	{
		int[] count = new int[26];
		
		// Get contents from the text area
		String text = flagDescription[4];
		// Count occurrences of each letter (case insensitive)
		for (int i = 0; i < text.length(); i++) 
		{
			char character = text.charAt(i);
			if ((character >= 'A') && (character <= 'Z')) 
			{
				count[character - 'A']++;
			}
			else if ((character >= 'a') && (character <= 'z')) 
			{
				count[character - 'a']++;
			}
		}
	 return count; // Return the count array
	}
	/** Set display information on the description panel */
	public void setDisplay(int index) 
	{
		descriptionPanel.setTitle(flagTitles[index]);
		descriptionPanel.setImageIcon(flagImage[index]);
		descriptionPanel.setDescription(flagDescription[index]);
	}
 }
