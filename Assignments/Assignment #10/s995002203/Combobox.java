package a10.s995002203;
import java.awt.*;
import java.awt.event.*;
import java.util.EventListener;

import javax.swing.*;


public class Combobox extends JFrame implements ItemListener{
	private int[] count=new int[5];
	private JButton showHistogram = new JButton("Show Histogram");
	private Histogram histogram = new Histogram();
	private JFrame histogramFrame = new JFrame();
	
	private String[] flagTitles = {"Canada", "China", "England",
			   "Germany", "United States of America"};
	 private ImageIcon[] flagImage = {
			    new ImageIcon("image/Canada.gif"),
			    new ImageIcon("image/China.gif"),
			    new ImageIcon("image/England.gif"),
			    new ImageIcon("image/German.gif"),
			    new ImageIcon("image/USA.gif"),
			  };
	 private String[] flagDescription = new String[9];
	 private JComboBox jcbo = new JComboBox(flagTitles);
	 ActionListener acListener = new acListener();
	 private DescriptionPanel descriptionPanel = new DescriptionPanel();
	 
	 public Combobox(){
		 flagDescription[0] = "this is Canada";
		 flagDescription[1] = "this is China ";
		 flagDescription[2] = "this is England";
		 flagDescription[3] = "this is Germany";
		 flagDescription[4] = "this is U.S.A.";

		 setDisplay(0);//從第一個國家開始顯示

		 add(jcbo, BorderLayout.NORTH);//add combo box
		 add(descriptionPanel, BorderLayout.CENTER);//add descriptionPanel
		 add(showHistogram,BorderLayout.SOUTH);//add button
		 
		 jcbo.addItemListener( this);
		 showHistogram.addActionListener(acListener);
		 
		 histogramFrame.add(histogram);//顯示另一個視窗
		 histogramFrame.pack();
		 histogramFrame.setTitle("Histogram");
	 }
	 
	  public void setDisplay(int index) {
		    descriptionPanel.setTitle(flagTitles[index]);
		    descriptionPanel.setImageIcon(flagImage[index]);
		    descriptionPanel.setDescription(flagDescription[index]);
	 }

	  
	public void itemStateChanged(ItemEvent e) {//combox狀態改變時要做的事
		setDisplay(jcbo.getSelectedIndex());
		 count[jcbo.getSelectedIndex()]++;
	}
	
	class acListener implements ActionListener {//按下按鈕時要做的事
		public void actionPerformed(ActionEvent e) {
			int[] countValue =count;
			histogram.showHistogram(countValue);
			histogramFrame.setVisible(true);
      }
	}

}
