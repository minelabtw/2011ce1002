package a10.s100502544;

import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

public class Histogram extends JPanel{
	private int[] count;
	public void showHistogram(int[] count){
		this.count=count;
		repaint();
	}
	protected void paintComponent(Graphics g){//畫跳出新視窗的統計圖
		if(count==null)return;
		super.paintComponent(g);
		int width=getWidth();
		int height=getHeight();
		int interval=(width-40)/count.length;
		int individualWidth=(int)(((width-40)/24)*0.60);
		int maxcount =0;
		for(int i=0;i<count.length;i++){
			if(maxcount<count[i])
				maxcount=count[i];
		}
		int x=30;
		g.drawLine(10, height-45, width-10, height-45);
		for(int i=0;i<count.length;i++){
			int barHeight=(int)(((double)count[i]/(double)maxcount)*(height-55));
			g.drawRect(x, height-45-barHeight, individualWidth, barHeight);
			g.drawString((char)(65+i)+"", x, height-30);
			
			x+=interval;
		}
	}
	public Dimension getPreferredSize(){
		return new Dimension(300,300);
	}
	public Histogram(){
		
	}
}
