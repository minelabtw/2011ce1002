package a10.s100502544;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;


public class CombBox extends JFrame{
	
	private String[] flagtitles={"Canada","Taiwan","China","England","German","USA"};//array 儲存 圖片文字
	private ImageIcon[] flagImageIcons={
			
			new ImageIcon("image/Canada.gif"),
			new ImageIcon("image/Taiwan.png"),
			new ImageIcon("image/China.gif"),
			new ImageIcon("image/England.gif"),
			new ImageIcon("image/German.gif"),
			new ImageIcon("image/USA.gif"),
			
	};//圖片陣列儲存圖片
	private String[] flagDescription=new String[6];
	private DescriptionPanel descriptionPanel=new DescriptionPanel();
	private JComboBox jcBox=new JComboBox(flagtitles);
	
	private Histogram histogram=new Histogram();
	
	private JButton jbtshowhistogram=new JButton("Histogram button");
	private JFrame histogramJFrame=new JFrame();//新視窗
	
	
	public CombBox(){//CombBox code
		flagDescription[0]="Canada is a North American country consisting of ten provinces and three territories. Located in the northern part of the continent, it extends from the Atlantic Ocean in the east to the Pacific Ocean in the west, and northward into the Arctic Ocean. Spanning over 9.9 million square kilometres, Canada is the world's second-largest country by total area, and its common border with the United States is the longest land border in the world.";
		flagDescription[1]="Taiwan officially the Republic of China (ROC) (Chinese: 中華民國; pinyin: Zhōnghuá Mínguó), is a state in East Asia.[21] Originally based in mainland China, the Republic of China now governs the island of Taiwan (known in the past as Formosa), which forms over 99% of its current territory,[f] as well as Penghu, Kinmen, Matsu, and other minor islands. Neighboring states include the People's Republic of China to the west, Japan to the east and northeast, and the Philippines to the south. Taipei is the capital city and economic and cultural centre of the country,[1] and New Taipei is the largest city by population.";
		flagDescription[2]="China (i/ˈtʃaɪnə/; Chinese: 中国; pinyin: Zhōngguó; see also Names of China), officially the People's Republic of China (PRC), is the world's most-populous country, with a population of over 1.3 billion. The East Asian state covers approximately 9.6 million square kilometres, and is the world's second-largest country by land area,[13] and the third- or fourth-largest in total area, depending on the definition of total area.";
		flagDescription[3]="England i/ˈɪŋɡlənd/ is a country that is part of the United Kingdom.[6][7][8] It shares land borders with Scotland to the north and Wales to the west; the Irish Sea is to the north west, the Celtic Sea to the south west, while the North Sea to the east and the English Channel to the south separate it from continental Europe. Most of England comprises the central and southern part of the island of Great Britain in the North Atlantic. The country also includes over 100 smaller islands such as the Isles of Scilly and the Isle of Wight.";
		flagDescription[4]="German (Deutsch [ˈdɔʏtʃ] ( listen)) is a West Germanic language related to and classified alongside English and Dutch. With an estimated 90[1] – 98 million[2] native speakers, German is one of the world's major languages and is the most widely-spoken first language in the European Union.";
		flagDescription[5]="The United States of America (commonly abbreviated to the United States, the U.S., the USA, America, and the States) is a federal constitutional republic comprising fifty states and a federal district. The country is situated mostly in central North America, where its forty-eight contiguous states and Washington, D.C., the capital district, lie between the Pacific and Atlantic Oceans, bordered by Canada to the north and Mexico to the south. The state of Alaska is in the northwest of the continent, with Canada to the east and Russia to the west, across the Bering Strait. The state of Hawaii is an archipelago in the mid-Pacific. The country also possesses several territories in the Pacific and Caribbean.";
		setDisplay(0);//開始的圖片跟文字
		add(jcBox,BorderLayout.NORTH);
		add(descriptionPanel,BorderLayout.CENTER);
		add(jbtshowhistogram,BorderLayout.SOUTH);
		jcBox.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				setDisplay(jcBox.getSelectedIndex());
				//下拉選單
			}
		});
		
		jbtshowhistogram.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				int[] count=countLetter();
				histogram.showHistogram(count);
				histogramJFrame.setVisible(true);
			}
		});
		histogramJFrame.add(histogram);
		histogramJFrame.pack();
		histogramJFrame.setTitle("Histogram");
		
	}
	public void setDisplay(int index){//選單上的順序 決定name 圖  和文字描述
		descriptionPanel.setTitle(flagtitles[index]);
		descriptionPanel.setImageIcon(flagImageIcons[index]);
		descriptionPanel.setDescription(flagDescription[index]);
	}
	
	
	
	
	private int[] countLetter(){
		int[] count=new int[26];
		//descriptionPanel.getjtaDescriptionPanel() 把字串傳過來 
		String text=descriptionPanel.getjtaDescriptionPanel().getText();
		for(int i=0;i<text.length();i++){
			char character=text.charAt(i);
			if((character>='A')&&(character<='Z')){
				count[character-'A']++;
			}
			else if((character>='a')&&(character<='z')){
				count[character-'a']++;
			}
			
		}
		return count;
	}
	
}