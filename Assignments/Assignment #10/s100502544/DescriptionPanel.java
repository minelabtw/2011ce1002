package a10.s100502544;

import java.awt.BorderLayout;


import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class DescriptionPanel extends JPanel{
	private JLabel jlblImageTitle=new JLabel();
	private JTextArea jtaDescriptionPanel=new JTextArea();
	
	public DescriptionPanel(){
		jlblImageTitle.setHorizontalAlignment(JLabel.CENTER);
		jlblImageTitle.setHorizontalTextPosition(JLabel.CENTER);
		jlblImageTitle.setVerticalTextPosition(JLabel.BOTTOM);
		jtaDescriptionPanel.setLineWrap(true);
		jtaDescriptionPanel.setWrapStyleWord(true);
		jtaDescriptionPanel.setEnabled(false);
		JScrollPane jscrollPane=new JScrollPane(jtaDescriptionPanel);
		setLayout(new BorderLayout(5,5));
		add(jscrollPane,BorderLayout.CENTER);
		add(jlblImageTitle,BorderLayout.WEST);
		
	}
	
	public void setTitle(String title){
		jlblImageTitle.setText(title);
	}
	public void setImageIcon(ImageIcon icon){
		jlblImageTitle.setIcon(icon);
	}
	public void setDescription(String text){
		jtaDescriptionPanel.setText(text);
	}
	public JTextArea getjtaDescriptionPanel(){
		return jtaDescriptionPanel;//��histogram���r�� �p�⤸��
	}
}
