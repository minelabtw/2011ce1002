package a10.s100502518;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class DescriptionPanel extends JPanel{
	private JLabel ImageTitle=new JLabel();
	private JTextArea Description=new JTextArea();
	
	public DescriptionPanel() 
	{
		ImageTitle.setHorizontalAlignment(JLabel.CENTER);
		ImageTitle.setHorizontalTextPosition(JLabel.CENTER);
		ImageTitle.setVerticalTextPosition(JLabel.BOTTOM);
		
		Description.setLineWrap(true);
		Description.setWrapStyleWord(true);
		Description.setEditable(false);
		
		JScrollPane scrollPane=new JScrollPane(Description);
		
		setLayout(new BorderLayout());
		add(scrollPane,BorderLayout.CENTER);
		add(ImageTitle,BorderLayout.WEST);
	}
	
	public void setTitle(String title)
	{
		ImageTitle.setText(title);
	}
	
	public void setImageIcon(ImageIcon icon)
	{
		ImageTitle.setIcon(icon);
	}
	
	public void setDescription(String text)
	{
		Description.setText(text);
	}
}
