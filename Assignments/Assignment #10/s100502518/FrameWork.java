package a10.s100502518;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;

public class FrameWork extends JFrame{
	private String[] flagTitles={"Canada","China","England","German","USA"};
	private ImageIcon[] flagImage={
			new ImageIcon("image/Canada.gif"),
			new ImageIcon("image/China.gif"),
			new ImageIcon("image/England.gif"),
			new ImageIcon("image/German.gif"),
			new ImageIcon("image/USA.gif")
	};
	private String[] flagDescription=new String[9];
	private DescriptionPanel descriptionpanel=new DescriptionPanel();
	private JComboBox jcbo=new JComboBox(flagTitles);
	private JButton jbtShowHistogram=new JButton("Show Histogram");
	private Histogram histogram=new Histogram();
	private JFrame histogramFrame=new JFrame();
	
	public FrameWork()
	{
		flagDescription[0]="Canada is a North American country consisting" +
				" of ten provinces and three territories. Located in the " +
				"northern part of the continent, it extends from the Atlantic " +
				"Ocean in the east to the Pacific Ocean in the west, and northward " +
				"into the Arctic Ocean. Spanning over 9.9 million square kilometres, " +
				"Canada is the world's second-largest country by total area, and its " +
				"common border with the United States is the longest land border in the world.";
		flagDescription[1]="China , officially the People's Republic of China (PRC), " +
				"is the world's most-populous country, with a population of over 1.3 billion. " +
				"The East Asian state covers approximately 9.6 million square kilometres, " +
				"and is the world's second-largest country by land area, " +
				"and the third- or fourth-largest in total area, depending on the definition of total area.";
		flagDescription[2]="England is a country that is part of the United Kingdom. " +
				"It shares land borders with Scotland to the north and Wales to the west; " +
				"the Irish Sea is to the north west, the Celtic Sea to the south west, while " +
				"the North Sea to the east and the English Channel to the south separate it from " +
				"continental Europe. Most of England comprises the central and southern part of the " +
				"island of Great Britain in the North Atlantic. The country also includes over 100 " +
				"smaller islands such as the Isles of Scilly and the Isle of Wight.";
		flagDescription[3]="Germany , officially the Federal Republic of Germany, " +
				"is a federal parliamentary republic in Europe. The country consists " +
				"of 16 states while the capital and largest city is Berlin. Germany covers " +
				"an area of 357,021 km2 and has a largely temperate seasonal climate. With 81.8 " +
				"million inhabitants, it is the most populous member state and the largest economy " +
				"in the European Union. It is one of the major political powers of the European continent " +
				"and a technological leader in many fields.";
		flagDescription[4]="The United States of America (commonly abbrev" +
				"iated to the United States, the U.S., the USA, America, " +
				"and the States) is a federal constitutional republic com" +
				"prising fifty states and a federal district. The country" +
				" is situated mostly in central North America, where its " +
				"forty-eight contiguous states and Washington, D.C., the " +
				"capital district, lie between the Pacific and Atlantic O" +
				"ceans, bordered by Canada to the north and Mexico to the " +
				"south. The state of Alaska is in the northwest of the co" +
				"ntinent, with Canada to the east and Russia to the west," +
				" across the Bering Strait. The state of Hawaii is an archipelago" +
				" in the mid-Pacific. The country also possesses several territories in the Pacific and Caribbean.";
		
		setDisplay(0);
		
		add(jcbo,BorderLayout.NORTH);
		add(descriptionpanel,BorderLayout.CENTER);
		add(jbtShowHistogram,BorderLayout.SOUTH);
		
		jcbo.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				setDisplay(jcbo.getSelectedIndex());
			}
		});
		
		jbtShowHistogram.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				int[] count=countLetters(jcbo.getSelectedIndex());
				
				histogram.showHistogram(count);
				histogramFrame.setVisible(true);
			}
		});
		
		histogramFrame.add(histogram);
		histogramFrame.pack();
		histogramFrame.setTitle("Histogram");
	}
	
	public void setDisplay(int index)
	{
		descriptionpanel.setTitle(flagTitles[index]);
		descriptionpanel.setImageIcon(flagImage[index]);
		descriptionpanel.setDescription(flagDescription[index]);
	}
	
	private int[] countLetters(int index)
	{
		int[] count=new int[26];
		String text=flagDescription[index];
		
		for(int i=0;i<text.length();i++)
		{
			char character=text.charAt(i);
			if((character>='A')&&(character<='Z'))
			{
				count[character-'A']++;
			}
			else if ((character>='a')&&(character<='z'))
			{
				count[character-'a']++;
			}
		}
		
		return count;
	}
}
