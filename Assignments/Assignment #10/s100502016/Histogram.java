package a10.s100502016;

import java.awt.Graphics;

import javax.swing.JPanel;

public class Histogram extends JPanel {
	private int[] count = {};

	public void showHistogram(int[] count) {
		this.count = count;
		repaint();
	}

	protected void paintComponent(Graphics g) {
		if (count == null)
			return;
		super.paintComponents(g);
		int width = getWidth();
		int height = getHeight();
		int interval = (width - 40) / count.length;
		int individualWidth = (int) ((width - 40) / 5 * 0.6);
		// decide the most count
		int maxCount = 0;
		for (int i = 0; i < count.length; i++) {
			if (maxCount < count[i]) {
				maxCount = count[i];
			}
		}
		// start point
		int x = 30;
		g.drawLine(10, height - 45, width - 10, height - 45);

		for (int i = 0; i < count.length; i++) {
			int barHeight = (int) ((double) count[i] / (double) maxCount * (height - 55));
			g.fillRect(x, height - 45 - barHeight, individualWidth, barHeight);
			x += interval;
		}
		x = 30;
		g.drawString("Canada", x, height - 30);
		x += interval;
		g.drawString("China", x, height - 30);
		x += interval;
		g.drawString("England", x, height - 30);
		x += interval;
		g.drawString("German", x, height - 30);
		x += interval;
		g.drawString("USA", x, height - 30);
		x += interval;
	}

}
