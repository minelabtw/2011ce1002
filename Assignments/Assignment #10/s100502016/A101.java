package a10.s100502016;

import javax.swing.JFrame;

public class A101 {
	public static void main(String arg[]) {
		FrameWork f = new FrameWork();
		f.pack();
		f.setTitle("A10");
		f.setSize(500, 350);
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}

}
