package a10.s100502016;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.ObjectInputStream.GetField;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import javax.swing.JComboBox;
import javax.swing.JFrame;

public class FrameWork extends JFrame implements ActionListener {
	private String[] flagTitles = { "Canada", "China", "England", "German",
			"USA" };
	private ImageIcon[] flagImage = { new ImageIcon("image/Canada.gif"),
			new ImageIcon("image/China.gif"),
			new ImageIcon("image/England.gif"),
			new ImageIcon("image/German.gif"), new ImageIcon("image/USA.gif") };
	private String[] flagDescriptionStrings = new String[9];
	private DescriptionPanel descriptionPanel = new DescriptionPanel();
	private JComboBox jcbo = new JComboBox(flagTitles);
	private JButton jbtShowhisogram = new JButton("ShowHistogram");
	private int[] Count = { 0, 0, 0, 0, 0 };

	private JFrame HistogramFrame = new JFrame();
	private Histogram histogram = new Histogram();

	public String getflagTitles(int index) {
		return flagTitles[index];
	}

	public FrameWork() {
		flagDescriptionStrings[0] = "Canada is a North American country consisting of ten provinces and three territories";
		flagDescriptionStrings[1] = "China , officially the People's Republic of China (PRC), is the world's most-populous country.";
		flagDescriptionStrings[2] = "England is a country that is part of the United Kingdom.";
		flagDescriptionStrings[3] = "Germany is a federal parliamentary republic in Europe.";
		flagDescriptionStrings[4] = "The United States of America is a federal constitutional republic comprising fifty states and a federal district.";

		setDisplay(0); // first Country
		setLayout(new BorderLayout(5, 10));
		add(jcbo, BorderLayout.NORTH);
		add(descriptionPanel, BorderLayout.CENTER);
		add(jbtShowhisogram, BorderLayout.SOUTH);
		jbtShowhisogram.addActionListener(this);
		jcbo.addItemListener(new ItemListener() {

			public void itemStateChanged(ItemEvent e) {
				setDisplay(jcbo.getSelectedIndex());
				Count[jcbo.getSelectedIndex()] += 1;
				System.out.println(jcbo.getSelectedIndex());
				System.out.println("---------");
				for (int i = 0; i < 5; i++)
					System.out.print(Count[i] + "");

			}
		});
		HistogramFrame.add(histogram);
		HistogramFrame.pack();
		HistogramFrame.setSize(700, 500);
		HistogramFrame.setTitle("Histogram");
	}

	public void actionPerformed(ActionEvent e) {
		histogram.showHistogram(Count);
		HistogramFrame.setVisible(true);

	}

	public void setDisplay(int index) {
		descriptionPanel.setTitle(flagTitles[index]);
		descriptionPanel.setImageIcon(flagImage[index]);
		descriptionPanel.setDescription(flagDescriptionStrings[index]);
	}

}
