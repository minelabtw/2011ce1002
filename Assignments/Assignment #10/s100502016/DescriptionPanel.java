package a10.s100502016;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.*;

public class DescriptionPanel extends JPanel {
	private JLabel jlblImageTitle = new JLabel();
	private JTextArea jtaDescription = new JTextArea();

	public DescriptionPanel() {
		JScrollPane scrollPane = new JScrollPane(jtaDescription);

		//
		jlblImageTitle.setHorizontalAlignment(JLabel.CENTER);
		jlblImageTitle.setHorizontalTextPosition(JLabel.CENTER);
		jlblImageTitle.setVerticalAlignment(JLabel.BOTTOM);
		// FONT(Style,Type,x)
		jlblImageTitle.setFont(new Font("SansSerif", Font.BOLD, 16));
		jtaDescription.setFont(new Font("Serif", Font.PLAIN, 14));
		//

		jtaDescription.setLineWrap(true); // Change the line automatically.
		jtaDescription.setWrapStyleWord(true); // If a word is too long,
												// transfer it to next line.
		jtaDescription.setEditable(false);

		setLayout(new GridLayout(1, 2)); // Before use BorderLayout. We should
		// setLayout(remember).
		add(jlblImageTitle);
		add(jtaDescription);
	}

	public void setTitle(String title) {
		jlblImageTitle.setText(title);
	}

	public void setImageIcon(ImageIcon icon) {
		jlblImageTitle.setIcon(icon);
	}

	public void setDescription(String text) {
		jtaDescription.setText(text);
	}

}
