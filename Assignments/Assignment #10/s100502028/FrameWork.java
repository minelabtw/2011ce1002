package a10.s100502028;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class FrameWork extends JFrame {
	
	// Declare an array of Strings for flag titles
	private String[] flagTitles = {"Canada", "China", "England", "German", "USA"};
	
	// Declare an ImageIcon array for the national flags of 5 countries
	private ImageIcon[] flagImage = {
		new ImageIcon("image/Canada.gif"),
		new ImageIcon("image/China.gif"),
		new ImageIcon("image/England.gif"),
		new ImageIcon("image/German.gif"),
		new ImageIcon("image/USA.gif")
	};
	
	// Declare an array of strings for flag description
	private String[] flagDescription = new String[9];
	
	private DescriptionPanel descriptionPanel = new DescriptionPanel(); // Declare and create a description panel
	private JComboBox jcbo = new JComboBox(flagTitles); // Create a combo box for selecting countries
	private JButton jbtShowHistogram = new JButton("Histogram button");
	private Histogram histogram = new Histogram(); // Create a histogram object
	private JFrame histogramFrame = new JFrame(); // Create a new frame to hold the histogram panel
	
	public FrameWork() {
		// Set text description
		flagDescription[0] = "The National Flag of Canada,also known as the Maple Leaf, is a red flag with a white square in its centre, featuring a stylized 11-pointed red maple leaf. " + 
				"Its adoption in 1965 marked the first time a national flag had been officially adopted in Canada to replace the Union Flag. The Canadian Red Ensign had been unofficially used since the 1890s and was approved by a 1945 Order in Council for use wherever place or occasion may make it desirable to fly a distinctive Canadian flag." +
				"In 1964, Prime Minister Lester B. Pearson appointed a committee to resolve the issue, sparking a serious debate about a flag change. Out of three choices, the maple leaf design by George F. G. Stanley and John Matheson based on the flag of the Royal Military College of Canada was selected." + 
				"The flag made its first appearance on February 15, 1965; the date is now celebrated annually as National Flag of Canada Day." + 
				"Many different flags have been created for use by Canadian officials, government bodies, and military forces. Most of these flags contain the maple leaf motif in some fashion, either by having the Canadian flag charged in the canton, or by including maple leaves in the design." + 
				"The Royal Union Flag is also an official flag in Canada, used as a symbol of Canada's membership in the Commonwealth of Nations, and of its allegiance to the Crown. The Union Flag remains a component of other Canadian flags, including the provincial flags of British Columbia, Manitoba and Ontario.";
		flagDescription[1] = "The flag of the People's Republic of China is a red field charged in the canton (upper corner nearest the flagpole) with five golden stars." + 
				"The design features one large star, with four smaller stars in a semicircle set off towards the fly (the side farthest from the flag pole)." + 
				" The red represents revolution; the five stars and their relationship represent the unity of the Chinese people under the leadership of the Communist Party of China (CPC)." + 
				"Sometimes, the flag is referred to as the 'Five Star Red Flag'. The flag was designed by Zeng Liansong, a citizen from Rui'an, Zhejiang." + 
				"He designed it in response to a circular distributed by the Preparatory Committee of the New Political Consultative Conference in July 1949, shortly after they came to power following the Chinese Civil War." + 
				"2,992 (or 3,012, see below) entries were received for the design competition, and Zeng's design was put into a pool of 38 finalists. After several meetings and slight modifications, Zeng's design was chosen as the national flag." +
				"The first flag was hoisted by the PLA on a pole overlooking Beijing's Tiananmen Square on October 1, 1949, at a ceremony announcing the founding of the People's Republic.";
		flagDescription[2] = "The Flag of England is the St George's Cross (heraldic blazon: Argent, a cross gules)." + 
				"The red cross appeared as an emblem of England in the Middle Ages, specifically during the Crusades (although the original symbol used to represent English crusaders was a white cross on a red background) and is one of the earliest known emblems representing England." + 
				"It also represents the official arms of the Most Noble Order of the Garter, and it achieved status as the national flag of England during the sixteenth century." +
				"Saint George was adopted as the patron saint of England in the thirteenth century, and the legend of Saint George slaying a dragon dates from the twelfth century.";
		flagDescription[3] = "The flag of Germany is a tricolour consisting of three equal horizontal bands displaying the national colours of Germany: black, red, and gold." + 
				"The flag was first adopted as the national flag of modern Germany in 1919, during the Weimar Republic.The black-red-gold tricolour first appeared in the early 19th century and achieved prominence during the 1848 Revolutions." + 
				"The short-lived Frankfurt Parliament of 1848�V1850 proposed the tricolour as a flag for a united and democratic German state. With the formation of the Weimar Republic after World War I, the tricolour was adopted as the national flag of Germany." + 
				"Following World War II, the tricolour was designated as the flag of both West and East Germany. The two flags were identical until 1959, when the East German flag was augmented with the coat of arms of East Germany." + 
				"Since reunification on 3 October 1990, the black-red-gold tricolour has remained the flag of Germany.The flag of Germany has not always used black, red, and gold as its colours." + 
				"After the Austro-Prussian War in 1866, the Prussian-dominated North German Confederation adopted a tricolour of black-white-red as its flag." + 
				"This flag later became the flag of the German Empire, formed following the unification of Germany in 1871, and was used until 1918. Black, white, and red were reintroduced as the German national colours with the establishment of Nazi Germany in 1933.";
		flagDescription[4] = "The national flag of the United States of America, often simply referred to as the American flag, consists of thirteen equal horizontal stripes of red (top and bottom) alternating with white, with a blue rectangle in the canton (referred to specifically as the 'union')" + 
				"bearing fifty small, white, five-pointed stars arranged in nine offset horizontal rows of six stars (top and bottom) alternating with rows of five stars." + 
				"The 50 stars on the flag represent the 50 states of the United States of America and the 13 stripes represent the thirteen British colonies that declared independence from the Kingdom of Great Britain and became the first states in the Union." + 
				"Nicknames for the flag include the 'Stars and Stripes', 'Old Glory', and 'The Star-Spangled Banner' (also the name of the national anthem).";
		
		setDisplay(0); // Set the first country (Canada) for display
		
		// Add combo box, description panel, button to the frame
		add(jcbo, BorderLayout.NORTH);
		add(descriptionPanel, BorderLayout.CENTER);
		add(jbtShowHistogram, BorderLayout.SOUTH);
		
		// Register listener
		jcbo.addItemListener(new ItemListener() {
			// Handle item selection
			public void itemStateChanged(ItemEvent e) {
				setDisplay(jcbo.getSelectedIndex());
			}
		});
		
		// Register listener
		jbtShowHistogram.addActionListener(new ActionListener() {
			// Handle the button action
			public void actionPerformed(ActionEvent e) {
				int[] count = countLetters(); // Count the letters in the text area
				histogram.showHistogram(count); // Set the letter count to histogram for display
				histogramFrame.setVisible(true); // Show the frame
			}
		});
		
		// Create a new frame to hold the histogram panel
		histogramFrame.add(histogram);
		histogramFrame.pack();
		histogramFrame.setTitle("Histogram");
	}
	
	// Set display information on the description panel
	public void setDisplay(int index) {
		descriptionPanel.setTitle(flagTitles[index]);
		descriptionPanel.setImageIcon(flagImage[index]);
		descriptionPanel.setDescription(flagDescription[index]);
	}
	
	// Count the letters in the text area
	public int[] countLetters() {
		int[] count = new int[26];
		String text = descriptionPanel.jtaDescription.getText(); // Get contents from the text area of the description panel
		
		// Count occurrences of each letteer
		for(int i = 0; i < text.length(); i++) {
			char character = text.charAt(i);
			if((character >= 'A') && (character <= 'Z')) {
				count[character - 'A']++;
			}
			else if ((character >= 'a') && (character <= 'z')) {
				count[character - 'a']++;
			}
		}
		return count; // Return the count array
	}
}
