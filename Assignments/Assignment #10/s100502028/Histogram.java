package a10.s100502028;

import java.awt.*;

import javax.swing.*;

public class Histogram extends JPanel{
	private int[] count;
	
	// Set the count and display histogram
	public void showHistogram(int[] count) {
		this.count = count;
		repaint();
	}
	
	// Paint the histogram
	protected void paintComponent(Graphics g) { // Override paintComponent
		if(count == null) return; // No display if count is null
		super.paintComponent(g); // Draw things in the superclass
		
		// Find the panel size and bar width and interval dynamically
		int width = getWidth();
		int height = getHeight();
		int interval = (width - 40) / count.length;
		int individualWidth = (int)(((width - 40) / 24) * 0.60);
		
		// Find the maximum count. The maximum count has the highest bar
		int maxCount = 0;
		for(int i = 0; i < count.length; i++) {
			if(maxCount < count[i])
				maxCount = count[i];
		}
		
		int x = 30; // x is the start position for the first bar in the histogram
		
		// Draw a horizontal base line
		g.drawLine(10, height - 45, width - 10, height - 45);
		for(int i = 0; i< count.length; i++) {
			int barHeight = (int)(((double)count[i] / (double)maxCount) * (height - 55));
			
			// Display a bar
			g.drawRect(x, height - 45 - barHeight, individualWidth, barHeight);
			
			// Display a letter under the base line
			g.drawString((char)(65 + i) + "", x, height - 30);
			
			x += interval;// Move x for displaying the next character
		}
	}
	
	// Override getPreferredSize
	public Dimension getPreferredSize() {
		return new Dimension(300, 300);
	}
}
