package a10.s100502028;

import javax.swing.JFrame;

public class A101 {
	public static void main(String[] args) {
		FrameWork frame = new FrameWork(); // Create a frame
		frame.setTitle("Assignment #10"); // Set the frame name
		frame.pack(); // Set the frame size
		frame.setLocationRelativeTo(null); // Center a frame
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true); // Display the frame
	}
}
