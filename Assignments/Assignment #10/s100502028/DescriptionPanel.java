package a10.s100502028;

import java.awt.*;

import javax.swing.*;

public class DescriptionPanel extends JPanel {
	private JLabel jlblImageTitle = new JLabel();// Label for displaying an image icon and a text
	protected JTextArea jtaDescription = new JTextArea(); // Text area for displaying text
	
	public DescriptionPanel() {
		// Center the icon and text and place the text under the icon
		jlblImageTitle.setHorizontalAlignment(JLabel.CENTER);
		jlblImageTitle.setHorizontalTextPosition(JLabel.CENTER);
		jlblImageTitle.setVerticalTextPosition(JLabel.BOTTOM);
		
		// Set the font in the label and the text field
		jlblImageTitle.setFont(new Font("SansSerif", Font.BOLD, 16));
		jtaDescription.setFont(new Font("Serif", Font.PLAIN, 14));
		
		// Set lineWarp and wrapStyleWord true for the text area
		jtaDescription.setLineWrap(true);
		jtaDescription.setWrapStyleWord(true);
		jtaDescription.setEditable(false);
		
		// Create a scroll pane to hold the text area
		JScrollPane scrollPane = new JScrollPane(jtaDescription);
		
		// Set BorderLayout for the panel, add panel and scroll pane
		setLayout(new BorderLayout(5, 5));
		add(scrollPane, BorderLayout.CENTER);
		add(jlblImageTitle, BorderLayout.WEST);
	}
	
	// Set the title
	public void setTitle(String tirle) {
		jlblImageTitle.setText(tirle);
	}
	
	// Set the image icon
	public void setImageIcon(ImageIcon icon) {
		jlblImageTitle.setIcon(icon);
	}
	
	// Set the text description
	public void setDescription(String text) {
		jtaDescription.setText(text);
	}
}
