package a10.s100502025;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class FrameWork extends JFrame {  //主要的視窗
	
	private ImageIcon[] flagImage = {new ImageIcon("image/USA.gif"),new ImageIcon("image/German.gif"),new ImageIcon("image/England.gif"),new ImageIcon("image/China.gif"),new ImageIcon("image/Canada.gif")};  //存取五張國旗的圖片
	
	private JButton jbtShowHistoGram = new JButton("Histogram");  //宣告一個按鈕叫"Histogram"
	
	private Histogram histogram = new Histogram();
	
	private String[] flagTitle = {"USA","German","England","China","Canada"};  //各國名稱
	
	private String[] flagDescription = new String[5];  //各國簡短的敘述
	
	private JComboBox jcbo = new JComboBox(flagTitle);  //宣告一個選單
	
	private DescriptionPanel description =  new DescriptionPanel();  //一個可以存取敘述還有圖片的板子
	
	private JFrame histogramFrame = new JFrame();  //可以有一個新的視窗
	
	public FrameWork() {
		flagDescription[0] = "The United States of America commonly abbreviated to the United States, the U.S., the USA, America, and the States) is a federal constitutional republic comprising fifty states and a federal district."
				             +"The country is situated mostly in central North America, where its forty-eight contiguous states and Washington, D.C., the capital district, lie between the Pacific and Atlantic Oceans, bordered by Canada to the north and Mexico to the south."
				             +"The state of Alaska is in the northwest of the continent, with Canada to the east and Russia to the west, across the Bering Strait."
				             +"The state of Hawaii is an archipelago in the mid-Pacific."  //美國的簡短敘述
				             +"The country also possesses several territories in the Pacific and Caribbean.";
		flagDescription[1] = "Germany, officially the Federal Republic of Germany ,is a federal parliamentary republic in Europe."
				             +"The country consists of 16 states while the capital and largest city is Berlin."
				             +"Germany covers an area of 357,021 km2 and has a largely temperate seasonal climate."
				             +"With 81.8 million inhabitants, it is the most populous member state and the largest economy in the European Union."
				             +"It is one of the major political powers of the European continent and a technological leader in many fields.";  //德國的簡短敘述
		flagDescription[2] = "England is a country that is part of the United Kingdom."
				             +"It shares land borders with Scotland to the north and Wales to the west; the Irish Sea is to the north west, the Celtic Sea to the south west, while the North Sea to the east and the English Channel to the south separate it from continental Europe."
				             +"Most of England comprises the central and southern part of the island of Great Britain in the North Atlantic."
				             +"The country also includes over 100 smaller islands such as the Isles of Scilly and the Isle of Wight.";  //英國的簡短敘述
		flagDescription[3] = "China , officially the People's Republic of China (PRC), is the world's most-populous country, with a population of over 1.3 billion."
				             +"The East Asian state covers approximately 9.6 million square kilometres, and is the world's second-largest country by land area, and the third- or fourth-largest in total area, depending on the definition of total area.";
		flagDescription[4] = "Canada is a North American country consisting of ten provinces and three territories."  //中國的簡短敘述
				             +"Located in the northern part of the continent, it extends from the Atlantic Ocean in the east to the Pacific Ocean in the west, and northward into the Arctic Ocean."
				             +"Spanning over 9.9 million square kilometres, Canada is the world's second-largest country by total area, and its common border with the United States is the longest land border in the world.";  //加拿大的簡短敘述
		
		setDisplay(0);  //在沒甚麼動作的時候，先讓視窗顯示美國的相關敘述
		
		add(jcbo , BorderLayout.NORTH);  //排版
		add(description, BorderLayout.CENTER);
		add(jbtShowHistoGram,BorderLayout.SOUTH);
		
		jcbo.addItemListener(new ItemListener() {  //讓選單可以執行動作
			public void itemStateChanged(ItemEvent e) {
				setDisplay(jcbo.getSelectedIndex());				
			}
		});
		
		jbtShowHistoGram.addActionListener(new ActionListener() {  //按鈕可有動作，並顯示新視窗
			public void actionPerformed(ActionEvent e) {
				
				histogramFrame.setTitle("Histigram");  //新視窗的規格
				histogramFrame.setSize(500,500);
				histogramFrame.setLocationRelativeTo(null);
				histogramFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				histogramFrame.setVisible(true);
				histogramFrame.add(histogram);
				
				int[] count = countLetters(jcbo.getSelectedIndex());  //選到某個國家的敘述中，字母的統計
				
				histogram.showHistogram(count);  //畫出圖表

				
			}
		});	
	}
	
	public void setDisplay(int index) {  //選到哪個國家就顯示哪個國家
		description.setTitle(flagTitle[index]);
		description.setImageIcon(flagImage[index]);
		description.setDescription(flagDescription[index]);
	}
	
	private int[] countLetters(int index) {
		int[] count = new int[26];  //各字母的統計
		
		String text = flagDescription[index];  //存取敘述
				
		for(int i = 0 ; i < text.length();i++) {  //一個字一個字的判斷
			char character = text.charAt(i);
			
			if((character >= 'A')&&(character <='Z')) {
				count[character - 'A']++;
			}
			else if((character >= 'a')&&(character <= 'z')){
				count[character - 'a']++;
			}		
		}
		return count;  //回傳統計
	}
}
