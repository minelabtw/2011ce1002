package a10.s100502025;

import java.awt.*;
import javax.swing.*;

public class DescriptionPanel extends JPanel {  //中間國家相關敘述的板子
	private JLabel jlblImageTitle = new JLabel();
	
	private JTextArea jtaDescription = new JTextArea();  //儲存敘述
	
	public DescriptionPanel() {
		jlblImageTitle.setHorizontalAlignment(JLabel.CENTER);  //排版
		jlblImageTitle.setHorizontalTextPosition(JLabel.CENTER);
		jlblImageTitle.setVerticalTextPosition(JLabel.BOTTOM);
		
		jlblImageTitle.setFont(new Font("SansSerif",Font.BOLD,16));  //設定字體
		jtaDescription.setFont(new Font("Serif",Font.PLAIN,14));
		
		jtaDescription.setLineWrap(true);
		jtaDescription.setWrapStyleWord(true);
		jtaDescription.setEditable(false);
		
		JScrollPane scrollPane = new JScrollPane(jtaDescription);
		
		setLayout(new BorderLayout(5,5));
		add(scrollPane , BorderLayout.CENTER);
		add(jlblImageTitle,BorderLayout.WEST);
	}
	
	public void setTitle(String title) {
		jlblImageTitle.setText(title);
	}
	
	public void setImageIcon(ImageIcon icon) {
		jlblImageTitle.setIcon(icon);
	}
	
	public void setDescription(String text) {
		jtaDescription.setText(text);
	}
}
