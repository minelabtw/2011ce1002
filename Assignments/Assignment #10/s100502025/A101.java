package a10.s100502025;

import java.awt.*;
import javax.swing.*;

public class A101 {
	
	public static void main(String[] args) {  //主要的視窗格式
		FrameWork frame = new FrameWork();
		frame.setTitle("Assignment #10");
		frame.setSize(1000,500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
}
