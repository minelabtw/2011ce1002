package a10.s100502516;

import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.io.*;

public class DescriptionPanel extends JPanel {
	private JTextArea jtaDescription = new JTextArea();	
	private JLabel jlelImageTitle;
	private Scanner input;	
	
	public DescriptionPanel(String country) throws FileNotFoundException
	{		
		File textFile = new File("country/" + country + ".txt");//load text file
		input = new Scanner(textFile);
		
		jlelImageTitle = new JLabel(country, new ImageIcon("image/" + country + ".gif"), SwingConstants.CENTER);
		jlelImageTitle.setHorizontalTextPosition(SwingConstants.CENTER);
		jlelImageTitle.setVerticalTextPosition(SwingConstants.BOTTOM);		
				
		jtaDescription.setText(getDescription());
		jtaDescription.setLineWrap(true);
		jtaDescription.setWrapStyleWord(true);
		
		setLayout(new BorderLayout());
		JScrollPane scrollPane = new JScrollPane(jtaDescription);//set scroll
		add(scrollPane, BorderLayout.CENTER);
		add(jlelImageTitle, BorderLayout.WEST);		
	}	
	public String getDescription()//get text
	{
		String temp = "";
		
		while(input.hasNext())
		{
			temp += input.next() + " ";
		}
		
		return temp;
	}
	public void setDisplay(String country) throws FileNotFoundException//change file
	{		
		File textFile = new File("country/" + country + ".txt");
		input = new Scanner(textFile);
		
		jlelImageTitle.setIcon(new ImageIcon("image/" + country + ".gif"));
		jlelImageTitle.setText(country);
		jtaDescription.setText(getDescription());
	}
	public JTextArea getJta()
	{
		return jtaDescription;
	}
}
