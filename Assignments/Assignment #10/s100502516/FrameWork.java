package a10.s100502516;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.FileNotFoundException;

public class FrameWork extends JFrame {		
	private String[] flagTitle = {"Canada", "China", "England", "German", "USA"};
	private DescriptionPanel DPanel;
	private JComboBox jcbo = new JComboBox(flagTitle);
	private JButton jbtHistogram = new JButton("Histogram");
	private Histogram histogram = new Histogram();
	private JFrame histogramFrame = new JFrame();
	
	public FrameWork() throws FileNotFoundException
	{
		DPanel = new DescriptionPanel(flagTitle[0]);
		jcbo.addItemListener(new ComboListener());
		
		jbtHistogram.addActionListener(new ButtonListener());
		
		histogramFrame.add(histogram);
		histogramFrame.pack();//set the size least
		histogramFrame.setTitle("Histogram");
		
		setLayout(new BorderLayout());
		add(jcbo, BorderLayout.NORTH);
		add(DPanel, BorderLayout.CENTER);
		add(jbtHistogram, BorderLayout.SOUTH);
	}	
	
	class ComboListener implements ItemListener 
	{
		public void itemStateChanged(ItemEvent e)
		{
			try 
			{
				DPanel.setDisplay(flagTitle[jcbo.getSelectedIndex()]);
			} 
			catch (FileNotFoundException e1) 
			{				
				e1.printStackTrace();
			}
		}
	}
	class ButtonListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) 
		{
			int[] count = countLetters();
			histogram.showHistogram(count);//update display
			histogramFrame.setVisible(true);
		}
	}
	
	private int[] countLetters()//to count letters
	{
		int[] count = new int[26];
		
		String text = DPanel.getJta().getText();
		
		for(int i = 0; i < text.length(); i++)
		{
			char character = text.charAt(i);
			
			if((character >= 'A') && (character <= 'Z'))
				count[character - 'A']++;
			else if((character >= 'a') && (character <= 'z'))
				count[character - 'a']++;
		}
		
		return count;
	}
}
