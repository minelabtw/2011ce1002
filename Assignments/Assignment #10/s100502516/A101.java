package a10.s100502516;

import java.io.FileNotFoundException;

import javax.swing.JFrame;

public class A101 {
	public static void main(String[] args) throws FileNotFoundException
	{
		FrameWork frame = new FrameWork();
		frame.setTitle("Country");
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
