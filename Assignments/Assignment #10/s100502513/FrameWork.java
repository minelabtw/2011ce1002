package a10.s100502513;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener {
	private JButton bu = new JButton("Histogram");
	private Country country = new Country();
	private Histogram hi = new Histogram();
	private JFrame hifFrame = new JFrame();  //長方圖
	public FrameWork() {
		add(country.getcombox(), BorderLayout.NORTH);
		add(country.getdesPanel(), BorderLayout.CENTER);
		add(bu, BorderLayout.SOUTH);
		
		bu.addActionListener(this);
		hifFrame.add(hi);
		hifFrame.pack();
		hifFrame.setSize(getPreferredSize());
		hifFrame.setTitle("Histogram");		
	}
	public void actionPerformed(ActionEvent e) {  //按按鈕
		if(e.getSource()==bu){
			hi.countletter(country.getdeString().getText());  
			hi.show();  //顯示長方圖
			hifFrame.setVisible(true);
		}
	}
}
