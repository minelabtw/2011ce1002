package a10.s100502513;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class Country extends JPanel {
	private String[] choose = { "Canada", "China", "England", "Germany", "USA" };
	private String[] des_st = new String[5]; // 國家敘述
	private JComboBox combox = new JComboBox(choose);
	private ImageIcon[] flag = {
			new ImageIcon("image/Canada.gif"), // 國旗圖片
			new ImageIcon("image/China.gif"),
			new ImageIcon("image/England.gif"),
			new ImageIcon("image/German.gif"), new ImageIcon("image/USA.gif") };
	private JTextArea des_TA = new JTextArea(); // 國家敘述區
	private JLabel flag_la = new JLabel(); // 國旗
	private JPanel p1 = new JPanel();
	private JScrollPane scrollPane;
	private int index;

	public Country() {
		setdesStr();
		setstyle();

		scrollPane = new JScrollPane(des_TA);

		display(0); // 初始顯示加拿大

		p1.setLayout(new GridLayout(1, 2));
		p1.add(flag_la);
		p1.add(scrollPane);

		combox.addItemListener(new ItemListener() { // 如果選擇某項目做的事
			public void itemStateChanged(ItemEvent e) {
				display(combox.getSelectedIndex()); // 設定選擇的項目以顯示
			}
		});
	}

	public JPanel getdesPanel() { // 輸出P1
		return p1;
	}

	public JComboBox getcombox() { // 輸出JComboBox
		return combox;
	}

	public JTextArea getdeString() {
		return des_TA;
	}

	public void setstyle() { // 設定Label,Textarea樣式
		flag_la.setHorizontalAlignment(JLabel.CENTER);
		flag_la.setHorizontalTextPosition(JLabel.CENTER);
		flag_la.setVerticalTextPosition(JLabel.BOTTOM);
		flag_la.setFont(new Font("Cambria", Font.BOLD, 60));

		des_TA.setFont(new Font("Cambria", Font.PLAIN, 20));
		des_TA.setBackground(Color.DARK_GRAY);
		des_TA.setLineWrap(true);
		des_TA.setWrapStyleWord(true);
		des_TA.setEnabled(false);
	}

	public void display(int x) { // 輸出選擇的國家
		flag_la.setText(choose[x]);
		flag_la.setIcon(flag[x]);
		des_TA.setText(des_st[x]);
		index = x;
	}

	public void setdesStr() { // 國家敘述
		des_st[0] = "Canada is a North American country consisting of ten provinces and three territories. Located in "
				+ "the northern part of the continent, it extends from the Atlantic Ocean in the east to the Pacific "
				+ "Ocean in the west, and northward into the Arctic Ocean. Spanning over 9.9 million square kilometres, "
				+ "Canada is the world's second-largest country by total area, and its common border with the United "
				+ "States is the longest land border in the world.";
		des_st[1] = "China, officially the People's Republic of China (PRC), is the world's most-populous country, "
				+ "with a population of over 1.3 billion. The East Asian state covers approximately 9.6 million square "
				+ "kilometres, and is the world's second-largest country by land area, and the third- or fourth-largest "
				+ "in total area, depending on the definition of total area.";
		des_st[2] = "England is a country that is part of the United Kingdom. It shares land borders with Scotland to the "
				+ "north and Wales to the west; the Irish Sea is to the north west, the Celtic Sea to the south west, "
				+ "while the North Sea to the east and the English Channel to the south separate it from continental Europe. "
				+ "Most of England comprises the central and southern part of the island of Great Britain in the North Atlantic. "
				+ "The country also includes over 100 smaller islands such as the Isles of Scilly and the Isle of Wight.";
		des_st[3] = "Germany , officially the Federal Republic of Germany (German: Bundesrepublik Deutschland) is a federal "
				+ "parliamentary republic in Europe. The country consists of 16 states while the capital and largest city is "
				+ "Berlin. Germany covers an area of 357,021 km2 and has a largely temperate seasonal climate. With 81.8 million "
				+ "inhabitants, it is the most populous member state and the largest economy in the European Union. "
				+ "It is one of the major political powers of the European continent and a technological leader in many fields.";
		des_st[4] = "The United States of America (commonly abbreviated to the United States, the U.S., the USA, America, and the States) "
				+ "is a federal constitutional republic comprising fifty states and a federal district. The country is situated "
				+ "mostly in central North America, where its forty-eight contiguous states and Washington, D.C., the capital "
				+ "district, lie between the Pacific and Atlantic Oceans, bordered by Canada to the north and Mexico to the south. "
				+ "The state of Alaska is in the northwest of the continent, with Canada to the east and Russia to the west, across "
				+ "the Bering Strait. The state of Hawaii is an archipelago in the mid-Pacific. The country also possesses several "
				+ "territories in the Pacific and Caribbean.";
	}

}
