package a10.s100502513;

import javax.swing.*;
import java.awt.*;

public class Histogram extends JPanel {
	private int[] count;

	public void countletter(String s) { // 計算各個字母有幾個
		count = new int[26];
		for (int i = 0; i < s.length(); i++) {
			char letter = s.charAt(i);
			if ((letter >= 'A') && (letter <= 'Z')) {
				count[letter - 'A']++;
			} else if ((letter >= 'a') && (letter <= 'z')) {
				count[letter - 'a']++;
			}
		}
	}

	public void show() { // 顯示
		repaint();
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (count == null)
			return;
		int w = getWidth();
		int h = getHeight();
		int interval = (w - 40) / count.length;
		int individualw = (int) (((w - 40) / 24) * 0.4);
		int max = 0;
		for (int i = 0; i < count.length; i++) {
			if (max < count[i])
				max = count[i];
		}
		int x = 30;
		int x2 = 28;
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, w, h);
		g.setColor(new Color(255, 201, 14));
		g.drawLine(15, h - 45, w - 25, h - 45);
		for (int i = 0; i < count.length; i++) {
			int barh = (int) (((double) count[i] / (double) max) * (h - 55)); // 數據長度
			g.setColor(new Color(255, 201, 14));
			g.setFont(new Font("Cambria", Font.PLAIN, 20));
			g.fillRect(x2, h - 45 - barh, individualw, barh);
			g.drawString((char) (65 + i) + "", x, h - 30);
			x += interval;
			x2 += interval;
		}

	}

	public Dimension getPreferredSize() {
		return new Dimension(400, 400);
	}
}
