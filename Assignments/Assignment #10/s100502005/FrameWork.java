package a10.s100502005;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class FrameWork extends JFrame
{	
	private String[] flagTitles = {"Taiwan", "Japan", "France", "America", "Canada"};
	
	private ImageIcon[] flagImage =
	{
		new ImageIcon("TAIWAN.gif"),
		new ImageIcon("JAPAN.gif"),
		new ImageIcon("FRANCE.gif"),
		new ImageIcon("AMERICA.gif"),
		new ImageIcon("CANADA.gif")
	};
	
	private String[] flagDescription = new String[9];
	private DescriptionPanel descriptionPanel = new DescriptionPanel();
	private JComboBox jcbo = new JComboBox(flagTitles);
	
	
	private int[] count;
	
	
	public  FrameWork()
	{		
		flagDescription[0] = "Taiwan,officially the Republic of China,is a state in East Asia.\nOriginally based in mainland China,the Republic of China now governs the island of Taiwan,which forms over 99% of its current territory,as well as Penghu,\nKinmen, Matsu,and other minor islands. Neighboring states include the People's Republic of China to the west,\nJapan to the east and northeast, and the Philippines to the south.\nTaipei is the capital city and economic and cultural centre of the country,and New Taipei is the largest city by population.";
		flagDescription[1] = "Japan,is an island nation in East Asia. Located in the Pacific Ocean,it lies to the east of the Sea of Japan, China, North Korea, South Korea and Russia,stretching from the Sea of Okhotsk in the north to the East China Sea and Taiwan in the south.The characters that make up Japan's name mean sun-origin,which is why Japan is sometimes referred to as the Land of the Rising Sun.";
		flagDescription[2] = "France,officially the French Republic,is a unitary semi-presidential republic in Western Europe with several overseas territories and islands located on other continents and in the Indian, Pacific, and Atlantic oceans.Metropolitan France extends from the Mediterranean Sea to the English Channel and the North Sea,and from the Rhine to the Atlantic Ocean. It is often referred to as l��Hexagone because of the geometric shape of its territory.It is the largest western European country and it possesses the second-largest exclusive economic zone in the world,covering 11,035,000 km2 (4,260,000 sq mi), just behind that of the United States.";
		flagDescription[3] = "The United States of America is a federal constitutional republic comprising fifty states and a federal district.The country is situated mostly in central North America, where its forty-eight contiguous states and Washington, D.C.,the capital district, lie between the Pacific and Atlantic Oceans, bordered by Canada to the north and Mexico to the south.The state of Alaska is in the northwest of the continent, with Canada to the east and Russia to the west, across the Bering Strait.The state of Hawaii is an archipelago in the mid-Pacific. The country also possesses several territories in the Pacific and Caribbean.";
		flagDescription[4] = "Canada is a North American country consisting of ten provinces and three territories. Located in the northern part of the continent,it extends from the Atlantic Ocean in the east to the Pacific Ocean in the west,and northward into the Arctic Ocean. Spanning over 9.9 million square kilometres,Canada is the world's second-largest country by total area,and its common border with the United States is the longest land border in the world.";
		
		setDisplay(0);
		
		add(jcbo, BorderLayout.NORTH);
		add(descriptionPanel, BorderLayout.CENTER);
		
		jcbo.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent e)
			{
				setDisplay(jcbo.getSelectedIndex());
			}				
		});	
		
		descriptionPanel.setSize(300, 100);
	}
	
	
	
	
	public void setDisplay(int index)
	{
		descriptionPanel.setTitle(flagTitles[index]);
		descriptionPanel.setImageIcon(flagImage[index]);
		descriptionPanel.setDescription(flagDescription[index]);
	}
	
	
	
	public void showHisgram(int[] couunt)
	{
		this.count = count;
		repaint();
	}
	
	
	
	protected void paintComponent(Graphics g)
	{
		if(count == null) return;
		
		super.paintComponents(g);
		
		int width = getWidth();
		int height = getHeight();
		int ineterval = (width - 40)/count.length;
		int individualWidth = (int)(((width - 40) / 24) * 0.60);
		
		int maxCount = 0;
		for(int i = 0 ; i < count.length ; i++ )
		{
			if(maxCount < count[i])
				maxCount = count[i];
		}		
	}
}
