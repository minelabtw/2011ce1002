package a10.s100502545;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.*;

public class ComboBox extends JFrame 
{
	private String[] flagTitles = {"Canada","China","USA","German","England"};
	
	private ImageIcon[] flagImage = 
	{
			new ImageIcon("image/Canada.gif"),
			new ImageIcon("image/China.gif"),
			new ImageIcon("image/England.gif"),
			new ImageIcon("image/German.gif"),
			new ImageIcon("image/USA.gif")
	};
	
	private DescriptionPanel descriptionPanel = new DescriptionPanel();
	private JComboBox jcbo = new JComboBox(flagTitles);//use JComboBox to show item
	private String[] flagDescription = new String[5];
	private JButton jbtShowhistogram=new JButton("Show Histogram");
	private Histogram histogram = new Histogram();
	private JFrame histogramFrame=new JFrame();

	
	
	
	public ComboBox()
	{
		flagDescription[0] = "The Maple Leaf flag \n" +
				"Canada is a North American country consisting of ten provinces and three territories." +
				"Located in the northern part of the continent," +
				"it extends from the Atlantic Ocean in the east to the Pacific Ocean in the west, and northward into the Arctic Ocean." +
				"Spanning over 9.9 million square kilometres, Canada is the world's second-largest country by total area, and its common border with the United States is the longest land border in the world.";
		
		flagDescription[1] = "This is the flag of China\n" +
				"China , officially the People's Republic of China (PRC)" +
				" is the world's most-populous country, with a population of over 1.3 billion." +
				"The East Asian state covers approximately 9.6 million square kilometres, and is the world's second-largest country by land area," +
				"and the third- or fourth-largest in total area, depending on the definition of total area.";
		
		flagDescription[2] = "This is the flag of USA\n" +
				"The United States of Americais a federal constitutional republic comprising fifty states and a federal district." +
				"The country is situated mostly in central North America, where its forty-eight contiguous states and Washington," +
				"the capital district, lie between the Pacific and Atlantic Oceans," +
				"bordered by Canada to the north and Mexico to the south. The state of Alaska is in the northwest of the continent," +
				"with Canada to the east and Russia to the west, across the Bering Strait. The state of Hawaii is an archipelago in the mid-Pacific. " +
				"The country also possesses several territories in the Pacific and Caribbean.";
		
		flagDescription[3] = "This is the flag of German\n" +
				"Germany, officially the Federal Republic of Germany , is a federal parliamentary republic in Europe. " +
				"The country consists of 16 states while the capital and largest city is Berlin. Germany covers an area of 357,021 km2 and has a largely temperate seasonal climate. " +
				"With 81.8 million inhabitants, it is the most populous member state and the largest economy in the European Union. " +
				"It is one of the major political powers of the European continent and a technological leader in many fields.";
		
		flagDescription[4] = "This is the flag of England\n" +
				"England is a country that is part of the United Kingdom. It shares land borders with Scotland to the north and Wales to the west; the Irish Sea is to the north west, " +
				"the Celtic Sea to the south west, while the North Sea to the east and the English Channel to the south separate it from continental Europe." +
				" Most of England comprises the central and southern part of the island of Great Britain in the North Atlantic. " +
				"The country also includes over 100 smaller islands such as the Isles of Scilly and the Isle of Wight.";	
		
		//set the first country (Canada) for display
		setDisplay(0);
		
		add(jcbo, BorderLayout.NORTH);//add JComboBox to JFrame
		add(descriptionPanel ,BorderLayout.CENTER );//add descriptionPanel to JFrame
		add(jbtShowhistogram,BorderLayout.SOUTH);//add jbtShowhistogram to JFrame
		
		
		//
		jcbo.addItemListener(new ItemListener() 
		{
			public void itemStateChanged(ItemEvent e) 
			{
				//Change item
				// TODO Auto-generated method stub
				setDisplay(jcbo.getSelectedIndex());
			}
		});
		
		jbtShowhistogram.addActionListener(new ActionListener() 
		{
			
			public void actionPerformed(ActionEvent e) 
			{
				//use another windows
				// TODO Auto-generated method stub
				int[] count=countLetters(jcbo.getSelectedIndex());
				histogram.showHistogram(count);
				histogramFrame.setVisible(true);
				
			}
		});
		
		histogramFrame.add(histogram);
		histogramFrame.pack();
		histogramFrame.setTitle("Histogram");
	
	}
	
	public void setDisplay(int index)
	{
		descriptionPanel.setTitle(flagTitles[index]);//set flagTitle 
		descriptionPanel.setImageIcon(flagImage[index]);//set Image
		descriptionPanel.setDescription(flagDescription[index]);//set flag description
	}
	
	private int[] countLetters(int index)
	{
		int[] count=new int[26];
		
		String text=flagDescription[index];
		
		for(int i=0;i<text.length();i++)
		{
			char character=text.charAt(i);
			if((character>='A')&&(character<='Z'))
			{
				count[character-'A']++;
			}
			else if ((character>='a')&&(character<='z'))
			{
				count[character-'a']++;
			}
		}
		
		return count;
	}
	
}
