package a10.s100502545;

import java.awt.*;

import javax.swing.*;


public class DescriptionPanel extends JPanel
{
	private JLabel jlbImageTitle = new JLabel();
	
	private JTextArea jtaDescription = new JTextArea();
	
	public DescriptionPanel()
	{
		//center the icon and text and place the text under the icon
		jlbImageTitle.setHorizontalAlignment(JLabel.CENTER);
		jlbImageTitle.setHorizontalTextPosition(JLabel.CENTER);
		jlbImageTitle.setVerticalTextPosition(JLabel.BOTTOM);
		
		//set the font in the label and the text field 
		jlbImageTitle.setFont(new Font("SanSerif", Font.BOLD, 16));
		jtaDescription.setFont(new Font("Serif", Font.PLAIN, 14));
		
		//set lineWrap and wrapStyleWord true for the text area  
		jtaDescription.setLineWrap(true);
		jtaDescription.setWrapStyleWord(true);
		jtaDescription.setEditable(false);
		
		//create a scroll pane to hold the text area 
		JScrollPane scrollPane = new JScrollPane(jtaDescription);
		
		//set borderLayout for the panel,add label and scrollPane
		setLayout(new BorderLayout(5,5));
		add(scrollPane, BorderLayout.CENTER);
		add(jlbImageTitle, BorderLayout.WEST);
			
	}
	//set the title
	public void setTitle(String title)
	{
		jlbImageTitle.setText(title);
	}
	
	//set the image icon
	public void setImageIcon(ImageIcon icon)
	{
		jlbImageTitle.setIcon(icon);
	}
	
	//set the text description
	public void setDescription(String text)
	{
		jtaDescription.setText(text);
	}
}
