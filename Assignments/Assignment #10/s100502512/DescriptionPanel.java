package a10.s100502512;

import javax.swing.*;
import java.awt.*;

public class DescriptionPanel extends JPanel  {
	private JLabel jlblImageTitle = new JLabel();
	JTextArea jtaDescription = new JTextArea();//顯示description的區域
	public DescriptionPanel(){
	jlblImageTitle.setHorizontalAlignment(JLabel.CENTER);
	jlblImageTitle.setHorizontalTextPosition(JLabel.CENTER);
	jlblImageTitle.setVerticalTextPosition(JLabel.BOTTOM);
	jlblImageTitle.setFont(new Font("SansSerif",Font.BOLD,16));
	jtaDescription.setFont(new Font("Serif",Font.BOLD,14));
	jtaDescription.setLineWrap(true);
	jtaDescription.setWrapStyleWord(true);
	jtaDescription.setEditable(false);
	JScrollPane scrollPane = new JScrollPane(jtaDescription);
	setLayout(new BorderLayout(10,10));
	add(scrollPane, BorderLayout.CENTER);//排版
	add(jlblImageTitle,BorderLayout.WEST);
}
	public void setTitle(String title) {
		jlblImageTitle.setText(title);
		
	}
	public void setImageIcon(ImageIcon Icon) {
		jlblImageTitle.setIcon(Icon);
		
	}
	public void setDescription(String text) {
		jtaDescription.setText(text);
		
	}
	
}