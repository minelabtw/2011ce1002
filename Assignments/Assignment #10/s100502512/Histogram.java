package a10.s100502512;

import javax.swing.*;
import java.awt.*;

public class Histogram extends JPanel {
	
	private int[] count;
	public void showHistogram(int[] count){
		this.count = count;
		repaint();
	}
	protected void paintComponent(Graphics g){//用countLetters()計算過的結果來畫圖
		if(count == null)
			return;
		super.paintComponent(g);
		int width = getWidth();
		int height = getHeight();
		int interval = (width - 40) / count.length;
		int individualWidth  = (int)(((width - 40) / 24)* 0.60);
		int maxCount =  0;
		for(int k = 0;k<count.length;k++){
			if(maxCount<count[k])
				maxCount = count[k];
		}
		int x = 30;
		g.drawLine(10,height-45,width-10,height-45);
		for(int k = 0;k<count.length;k++){
			int barHeight = (int)(((double)count[k] / (double)maxCount) * (height - 55));
			g.drawRect(x,height - 45 - barHeight, individualWidth, barHeight);
			g.drawString((char)(65+k)+"", x,height-30);
			x+=interval;
		}
	}
	public Dimension getPreferredSize(){//Histogram視窗的大小
		return new Dimension(500,400);
	}
}
