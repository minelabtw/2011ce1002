package a10.s100502512;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class A101 extends JFrame {
	private String[] flagTitle = { "Canada", "China", "England", "German",
			"USA" };
	private ImageIcon[] flagImage = { new ImageIcon("image/Canada.GIF"),//傳入圖片
			new ImageIcon("image/China.GIF"),
			new ImageIcon("image/England.GIF"),
			new ImageIcon("image/German.GIF"), new ImageIcon("image/USA.GIF"), };
	private String[] flagDescription = new String[5];
	private DescriptionPanel descriptionpanel = new DescriptionPanel();
	private JComboBox jcbo = new JComboBox(flagTitle);//使用JComboBox
	private JButton jbtShowHistogramButton = new JButton("Show Histogram");
	private Histogram histogram = new Histogram();//使用Histogram
	private JFrame hFrame = new JFrame();//另一個視窗
	public static void main(String[] args) {
		A101 f = new A101();
		f.pack();
		f.setTitle("A101");
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}

	public A101() {
		flagDescription[0] = "Canada is a North American country consisting of ten provinces and three territories.\n"   //description
				+ " Located in the northern part of the continent,\n"
				+ "it extends from the Atlantic Ocean in the east to the Pacific Ocean in the west,\n"
				+ "and northward into the Arctic Ocean. ";
		flagDescription[1] = "China, officially the People's Republic of China (PRC), is the world's most-populous country,\n"
				+ "with a population of over 1.3 billion. The East Asian state covers approximately 9.6 million square kilometres,\n"
				+ " and is the world's second-largest country by land area,and the third- or fourth-largest in total area, depending on the definition of total area.";
		flagDescription[2] = "England is a country that is part of the United Kingdom.\n"
				+ "It shares land borders with Scotland to the north and Wales to the west;\n"
				+ "the Irish Sea is to the north west, the Celtic Sea to the south west, while the North Sea to the east and the English Channel to the south separate it from continental Europe.\n"
				+ "Most of England comprises the central and southern part of the island of Great Britain in the North Atlantic.\n"
				+ "The country also includes over 100 smaller islands such as the Isles of Scilly and the Isle of Wight.";
		flagDescription[3] = "Germany, officially the Federal Republic of Germany ,is a federal parliamentary republic in Europe.\n"
				+ "The country consists of 16 states while the capital and largest city is Berlin.\n"
				+ "Germany covers an area of 357,021 km2 and has a largely temperate seasonal climate. With 81.8 million inhabitants, it is the most populous member state and the largest economy in the European Union.\n"
				+ "It is one of the major political powers of the European continent and a technological leader in many fields";
		flagDescription[4] = "The United States of America (commonly abbreviated to the United States, the U.S., the USA, America, and the States) is a federal constitutional republic comprising fifty states and a federal district.\n"
				+ "The country is situated mostly in central North America, where its forty-eight contiguous states and Washington, D.C., the capital district, lie between the Pacific and Atlantic Oceans, bordered by Canada to the north and Mexico to the south.\n"
				+ "The state of Alaska is in the northwest of the continent, with Canada to the east and Russia to the west, across the Bering Strait.\n"
				+ "The state of Hawaii is an archipelago in the mid-Pacific. The country also possesses several territories in the Pacific and Caribbean.";
		setDisplay(0);
		
		add(jcbo, BorderLayout.NORTH);//排版(參考課本)
		add(descriptionpanel, BorderLayout.CENTER);
		add(jbtShowHistogramButton, BorderLayout.SOUTH);

		jcbo.addItemListener(new ItemListener() {//讓jcbo能使用
			public void itemStateChanged(ItemEvent e) {
				setDisplay(jcbo.getSelectedIndex());
			}
		});
		jbtShowHistogramButton.addActionListener(new ActionListener() {//讓按鈕有用處
			public void actionPerformed(ActionEvent a) {
				int[] count = countLetters();
				histogram.showHistogram(count);
				hFrame.setVisible(true);
			}

			
		});
		hFrame.add(histogram);//另一個視窗
		hFrame.pack();
		hFrame.setTitle("Histogram");
	}
	private int[] countLetters() {//計算字母的字數
		
		int[] count = new int[26];
		String text = descriptionpanel.jtaDescription.getText();
		for(int i = 0;i<text.length();i++){
			char character = text.charAt(i);
			if((character >= 'A') && (character <= 'Z')){
				count[character - 'A']++;
			}
			else if((character >= 'a') &&(character <= 'z')){
				count[character - 'a']++;
			}
		}
		return count;//回傳至Histogram的showHistogram(int[] count)
	}
	private void setDisplay(int i) {//選取哪一國會顯示的國名,圖片,解釋
		descriptionpanel.setTitle(flagTitle[i]);
		descriptionpanel.setImageIcon(flagImage[i]);
		descriptionpanel.setDescription(flagDescription[i]);

	}
}
