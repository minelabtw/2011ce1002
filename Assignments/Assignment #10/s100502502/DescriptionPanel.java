//DescriptionPanel.java
package a10.s100502502;
import java.awt.Font;
import java.awt.*;
import javax.swing.*;
public class DescriptionPanel extends JPanel{
	private JLabel B1 = new JLabel();
	private JTextArea JDescription = new JTextArea();
	public DescriptionPanel(){//initialize
		B1.setHorizontalAlignment(JLabel.CENTER);
		B1.setHorizontalTextPosition(JLabel.CENTER);
		B1.setVerticalTextPosition(JLabel.BOTTOM);
		B1.setFont(new Font("SansSerif", Font.BOLD, 16));
		JDescription.setFont(new Font("Serif", Font.PLAIN, 14));
		JDescription.setLineWrap(true);
		JDescription.setWrapStyleWord(true);
		JDescription.setEditable(false);
		JScrollPane scrollPane = new JScrollPane(JDescription);
		setLayout(new BorderLayout(5, 5));
		add(B1, BorderLayout.WEST);
		add(scrollPane, BorderLayout.CENTER);
		
	}
	public void setTitle(String title){//set description's title
		B1.setText(title);
	}
	public void setImageIcon(ImageIcon icon){//set description image
		B1.setIcon(icon);
	}
	public void setDescription(String text){//set description's text
		JDescription.setText(text);
	}
	public JTextArea getDescription(){//get JTextArea's object
		return JDescription;
	}
}
