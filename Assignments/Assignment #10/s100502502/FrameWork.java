//FrameWork.java
package a10.s100502502;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class FrameWork extends JFrame{
	//create flag's name, image, description, and frame, button
	private String[] name = {"Canada", "China", "England", "German", "USA"};
	private ImageIcon[] flagImage = {new ImageIcon("image/Canada.gif"), 
			new ImageIcon("image/China.gif"), new ImageIcon("image/England.gif"), 
			new ImageIcon("image/German.gif"), new ImageIcon("image/USA.gif")};
	private String[] flagDescription = new String[5];
	private DescriptionPanel desPanel = new DescriptionPanel();
	private JComboBox jcbox = new JComboBox(name);
	private JFrame histogramFrame = new JFrame();
	private JButton B_histo = new JButton("Show Histogram");
	private Histogram histogram = new Histogram();
	FrameWork(){
		//save description to an array
		flagDescription[0] = "This is Canada, there are a lot maples.";
		flagDescription[1] = "This is China, I hope I can take a trip to China.";
		flagDescription[2] = "This is England, London is beautiful.";
		flagDescription[3] = "This is German, the BMW is famous.";
		flagDescription[4] = "This is USA, the Marvel's The Avengers is good.";
		setDisplay(0);//initialize the flag and it's message
		setLayout(new BorderLayout());
		add(jcbox, BorderLayout.NORTH);
		add(desPanel, BorderLayout.CENTER);
		add(B_histo, BorderLayout.SOUTH);
		jcbox.addItemListener(new ItemListener(){//when select  
			public void itemStateChanged(ItemEvent e){
				setDisplay(jcbox.getSelectedIndex());//display the new message you select
			}
		});
		B_histo.addActionListener(new ActionListener(){//when click the button
			public void actionPerformed(ActionEvent e){
				int[] count = countLetters();//calculate letters
				histogram.showHistogram(count);//repaint the histogram
				histogramFrame.setVisible(true);//let histogram's frame visible
			}
		});
		histogramFrame.add(histogram);
		histogramFrame.pack();
		histogramFrame.setTitle("Histogram");
	}
	public void setDisplay(int index){//display message and image
		desPanel.setTitle(name[index]);
		desPanel.setImageIcon(flagImage[index]);
		desPanel.setDescription(flagDescription[index]);
	}
	private int[] countLetters(){//calculate letters
		int[] count = new int[26];//A~Z
		String text = desPanel.getDescription().getText();
		for(int i = 0; i < text.length(); i++){
			char character = text.charAt(i);
			if((character >= 'A') && (character <= 'Z')){
				count[character - 'A']++;
			}
			else if((character >= 'a') && (character <= 'z')){
				count[character - 'a']++;
			}
		}
		return count;
	}
}
