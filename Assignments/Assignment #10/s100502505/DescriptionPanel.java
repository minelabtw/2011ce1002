package a10.s100502505;

import java.awt.*;
import javax.swing.*;

public class DescriptionPanel extends JPanel{
	public JLabel jlblImageTitle = new JLabel();
	public JTextArea jtaDescription = new JTextArea();
	
	public DescriptionPanel()
	{
		jlblImageTitle.setHorizontalAlignment(JLabel.CENTER);//排版
		jlblImageTitle.setHorizontalTextPosition(JLabel.CENTER);
		jlblImageTitle.setVerticalTextPosition(JLabel.BOTTOM);
		jlblImageTitle.setFont(new Font("SansSerif",Font.BOLD,16));//字型
		jlblImageTitle.setFont(new Font("Serif",Font.BOLD,14));
		jtaDescription.setLineWrap(true);
		jtaDescription.setWrapStyleWord(true);
		jtaDescription.setEditable(true);
		JScrollPane scroll = new JScrollPane(jtaDescription);
		setLayout(new BorderLayout(5,5));
		add(scroll,BorderLayout.EAST);
		add(jlblImageTitle,BorderLayout.CENTER);
	}
	
	public void setTitle(String title)//設定Text
	{
		jlblImageTitle.setText(title);
	}
	
	public void setImageIcon(ImageIcon icon)//設定Image
	{
		jlblImageTitle.setIcon(icon);
	}
	
	public void setDescription(String text)//設定text
	{
		jtaDescription.setText(text);
	}
}

