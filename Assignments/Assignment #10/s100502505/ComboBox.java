package a10.s100502505;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ComboBox extends JFrame{
	public DescriptionPanel descriptionPanel = new DescriptionPanel();//宣告物件
	public String[] flagTitle = {"Canada","China","England","German","USA"};
	public JComboBox jcbo = new JComboBox(flagTitle);
	
	private ImageIcon[] flagImage=//圖片
	{
			new ImageIcon("image/Canada.gif"),
			new ImageIcon("image/China.gif"),
			new ImageIcon("image/England.gif"),
			new ImageIcon("image/German.gif"),
			new ImageIcon("image/USA.gif")
	};
	
	public String [] flagDescription = new String[5];//宣告陣列儲存圖片敘述
	private JButton jbtShowHistogram=new JButton("Show Histogram");//建立按鈕
	private Histogram histogram=new Histogram();
	private JFrame histogramFrame=new JFrame();
	
	public ComboBox()
	{
		flagDescription[0] = "The Maple Leaf flag";
		flagDescription[1] = "This is the flag of China";
		flagDescription[2] = "This is the flag of Emgland";
		flagDescription[3] = "This is the flag of German";
		flagDescription[4] = "This is the flag of USA";	
		setDisplay(0);

		add(jcbo, BorderLayout.NORTH);//加到視窗裡面
		add(descriptionPanel ,BorderLayout.CENTER);
		add(jbtShowHistogram,BorderLayout.SOUTH);
		jcbo.addItemListener(new ItemListener() 
		{
			public void itemStateChanged(ItemEvent e)//換國家 
			{
				// TODO Auto-generated method stub
				setDisplay(jcbo.getSelectedIndex());
			}
		});
			jbtShowHistogram.addActionListener(new ActionListener() 
			{
			
			public void actionPerformed(ActionEvent e)
			{
				// TODO Auto-generated method stub
				int[] count=countLetters(jcbo.getSelectedIndex());
				histogram.showHistogram(count);
				histogramFrame.setVisible(true);
			}
		});
		histogramFrame.add(histogram);
		histogramFrame.pack();
		histogramFrame.setTitle("Histogram");
	}
	private int[] countLetters(int index)
	{
		int[] count=new int[26];
		String text=flagDescription[index];
		
		for(int i=0;i<text.length();i++)
		{
			char character=text.charAt(i);
			if((character>='A')&&(character<='Z'))
			{
				count[character-'A']++;
			}
			else if ((character>='a')&&(character<='z'))
			{
				count[character-'a']++;
			}
		}
		return count;
	}
	public void setDisplay(int index)//設定視窗
	{
		descriptionPanel.setTitle(flagTitle[index]);//設定標題
		descriptionPanel.setImageIcon(flagImage[index]);//設定圖片
		descriptionPanel.setDescription(flagDescription[index]);//設定圖片敘述
	}
}


