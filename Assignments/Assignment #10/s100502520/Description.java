package a10.s100502520;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Description extends JPanel {
	private String[] descriptions = new String[5];
	private JTextArea des = new JTextArea();
	private JLabel title = new JLabel();

	public Description() {
		setdescriptions();
		
		//設定圖片與敘述的位置
		title.setHorizontalAlignment(JLabel.CENTER);
		title.setHorizontalTextPosition(JLabel.CENTER);
		title.setVerticalTextPosition(JLabel.BOTTOM);
		
		//設定字體
		title.setFont(new Font("f1", Font.BOLD, 16));
		des.setFont(new Font("f2", Font.PLAIN, 14));
		
		//設定外圍
		des.setLineWrap(true);
		des.setWrapStyleWord(true);
		des.setEditable(false);
		JScrollPane scroll = new JScrollPane(des);
		setLayout(new BorderLayout(5, 5));
		add(scroll, BorderLayout.CENTER);
		add(title, BorderLayout.WEST);
	}

	//設定顯示的內容
	public void setimage(String tit, ImageIcon icon, int i) {
		title.setIcon(icon);
		title.setText(tit);
		des.setText(descriptions[i]);
	}

	//儲存國旗的敘述
	public void setdescriptions() {
		descriptions[0] = "The National Flag of Canada,also "
				+ "known as the Maple Leaf, and l'Unifolié (French"
				+ " for the one-leafed), is a red flag with a white"
				+ " square in its centre, featuring a stylized 11-pointed"
				+ " red maple leaf. Its adoption in 1965 marked the first "
				+ "time a national flag had been officially adopted in Canada"
				+ " to replace the Union Flag. The Canadian Red Ensign had been"
				+ " unofficially used since the 1890s and was approved by a 1945"
				+ " Order in Council for use wherever place or occasion may make "
				+ "it desirable to fly a distinctive Canadian flag.";
		descriptions[1] = "The flag of the People's Republic of China is"
				+ " a red field charged in the canton (upper corner nearest the"
				+ " flagpole) with five golden stars. The design features one"
				+ " large star, with four smaller stars in a semicircle set off"
				+ " towards the fly (the side farthest from the flag pole). The"
				+ " red represents revolution; the five stars and their relationship"
				+ " represent the unity of the Chinese people under the leadership"
				+ " of the Communist Party of China (CPC). Sometimes, the flag is referred"
				+ " to as the Five Star Red Flag.";
		descriptions[2] = "The United Kingdom of Great Britain and Northern Ireland"
				+ " uses as its national flag the royal banner known as the Union Flag or,"
				+ " popularly, Union Jack.[1]The term Union Jack is used in colloquial "
				+ "speech and comes from the written name of James I of England. The latin "
				+ "form of the name James is Jacobus and the British version of Jacobus is"
				+ " Jack. The current design of the Union Flag dates from the union of Ireland "
				+ "and Great Britain in 1801. It consists of the red cross of Saint George"
				+ "(patron saint of England), edged in white, superimposed on the Cross of St "
				+ "Patrick (patron saint of Ireland), which are superimposed on the Saltire"
				+ " of Saint Andrew (patron saint of Scotland). Wales, however, is not represented"
				+ " in the Union Flag by Wales' patron saint, Saint David, as at the time the "
				+ "flag was designed Wales was part of the Kingdom of England.";
		descriptions[3] = "The flag of Germany is a tricolour consisting of three equal "
				+ "horizontal bands displaying the national colours of Germany: black, red, and"
				+ " gold.The flag was first adopted as the national flag of modern Germany in"
				+ " 1919, during the Weimar Republic.";
		descriptions[4] = "The national flag of the United States of America, often"
				+ " simply referred to as the American flag, consists of thirteen equal "
				+ "horizontal stripes of red (top and bottom) alternating with white, "
				+ "with a blue rectangle in the canton (referred to specifically as the union)"
				+ " bearing fifty small, white, five-pointed stars arranged in nine offset"
				+ " horizontal rows of six stars (top and bottom) alternating with rows of five"
				+ " stars. The 50 stars on the flag represent the 50 states of the United "
				+ "States of America and the 13 stripes represent the thirteen British colonies"
				+ " that declared independence from the Kingdom of Great Britain and became"
				+ " the first states in the Union.Nicknames for the flag include the Stars and "
				+ "Stripes, Old Glory,and The Star-Spangled Banner (also the name of the "
				+ "national anthem).";
	}
}
