package a10.s100502520;

import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;

public class ComboBox extends JFrame{
	private String[] titles = { "Canada", "China", "England", "German", "USA" };
	private Description description = new Description();
	private JComboBox combo = new JComboBox(titles);
	private ImageIcon[] images = {
		new ImageIcon("image/Canada.gif"),
		new ImageIcon("image/China.gif"),
		new ImageIcon("image/England.gif"),
		new ImageIcon("image/German.gif"),
		new ImageIcon("image/USA.gif"),
	};
	
	public ComboBox(){
		setdiplay(0);
		add(combo, BorderLayout.NORTH);
		add(description, BorderLayout.CENTER);
		
		//設定改變選項時所做的事
		combo.addItemListener(new ItemListener() {
			
			public void itemStateChanged(ItemEvent e) {
				setdiplay(combo.getSelectedIndex());	
			}
		});
	}
	//顯示圖片、標題與敘述
	public void setdiplay(int i){
		description.setimage(titles[i], images[i], i);
	}
	
}
