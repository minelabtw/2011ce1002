package a10.s100502026;

import javax.swing.JFrame;

public class A101 {
	
	public static void main( String[] args )
	{
		FrameWork frame = new FrameWork();
		
		frame.setTitle( "Introduction of some nations." );
		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		frame.pack();
		frame.setLocationRelativeTo( null );
		frame.setVisible( true );
	}

}
