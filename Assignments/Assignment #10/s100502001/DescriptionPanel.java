package a10.s100502001;
import java.awt.*;
import javax.swing.*;
public class DescriptionPanel extends JPanel{ //顯示國旗以及描述
	private JLabel jlbImage=new JLabel();
	private JTextArea jtaDescription=new JTextArea();
	public DescriptionPanel(){
		jtaDescription.setLineWrap(true); //自動換行
		jtaDescription.setWrapStyleWord(true); //字與字之間做判斷再換
		jtaDescription.setEditable(false);
		JScrollPane scrollpane=new JScrollPane(jtaDescription); 
		setLayout(new BorderLayout(5,5));
		add(scrollpane,BorderLayout.CENTER);
		add(jlbImage,BorderLayout.WEST);
		
	}
	public void setTitle(String title){
		jlbImage.setFont(new Font(title,Font.BOLD,20));
		jlbImage.setForeground(Color.BLUE);
		jlbImage.setText(title);
	}
	public void setImageIcon(ImageIcon icon){ //設定國旗
		jlbImage.setIcon(icon);
	}
	public void setDescription(String text){ //內文描述
		jtaDescription.setForeground(Color.magenta);
		jtaDescription.setText(text);
	}
}
