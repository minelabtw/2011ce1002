package a10.s100502001;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
public class Framework extends JFrame {
	private Histogram histogram=new Histogram();
	private JFrame jfrhistogram =new JFrame();
	private DescriptionPanel descriptionPanel=new DescriptionPanel();
	private JButton jbthistogram=new JButton("Histogram Button");
	private JComboBox jcb;
	private String[] flagtitles={"Canada","China","England","German","USA"};
	private String[] flagdescription=new String[5];
	private ImageIcon[] flagimage={
			new ImageIcon("image/Canada.gif"),
			new ImageIcon("image/China.gif"),
			new ImageIcon("image/England.gif"),
			new ImageIcon("image/German.gif"),
			new ImageIcon("image/USA.gif")
	};
	public Framework() {
		jcb=new JComboBox(flagtitles);
		setflagdescription();//敘述
		setLayout(new BorderLayout());
		add(jcb,BorderLayout.NORTH);
		add(descriptionPanel,BorderLayout.CENTER);
		add(jbthistogram,BorderLayout.SOUTH);
		jcb.setForeground(Color.yellow);
		jcb.setBackground(Color.cyan);
		setDisplay(0);
		jcb.addItemListener(new ItemListener(){ //選擇國家
			public void itemStateChanged(ItemEvent e) {
				
				setDisplay(jcb.getSelectedIndex());
				System.out.println(jcb.getSelectedIndex());
			}			
		});
		jbthistogram.addActionListener(new ActionListener(){ //顯示統計圖
			public void actionPerformed(ActionEvent e){
				histogram.setCount(flagdescription[jcb.getSelectedIndex()]);
				histogram.setVisible(true);
			}

		});
	}
	public void setDisplay(int index){ //改內容
		
		descriptionPanel.setTitle(flagtitles[index]);
		descriptionPanel.setImageIcon(flagimage[index]);
		descriptionPanel.setDescription(flagdescription[index]);
	}
	public void setflagdescription(){
		flagdescription[0]="he land that is now Canada has been inhabited for millennia" +
				" by various Aboriginal peoples. Beginning in the late 15th century, British" +
				" and French expeditions explored, and later settled, along the region's Atlantic coast";
		flagdescription[1]="estas vidita diversmaniere kiel antikva civilizo en Orienta Azio, kiu profunde" +
				" kulture influis najbarajn landojn kaj naciojn, aŭ kiel la modernaj nacioj kiuj estas daŭrigo " +
				"de la civilizo.";
		flagdescription[2]=" officially the Federal Republic of Germany (German: Bundesrepublik Deutschland, pronounced " +
				"[ˈbʊndəsʁepuˌbliːk ˈdɔʏtʃlant] ( listen)),[5] is a federal parliamentary republic in Europe. The country " +
				"consists of 16 states while the capital and largest city is Berlin. Germany covers an area of 357,021 km2 ";
		flagdescription[3]="The United Kingdom of Great Britain and Northern " +
				"IrelandTeyrnas Unedig Prydain Fawra Gogledd IwerddonAn Rìoghachd" +
				" Aonaichte na Breatainn Mhòr agus Eirinn mu Thuath";
		flagdescription[4]="he United States of America (commonly abbreviated to the United States, the U.S., the USA, " +
				"America, and the States) is a federal constitutional republic comprising fifty states and a federal" +
				" district. The country is situated mostly in central North America";
	}

}
