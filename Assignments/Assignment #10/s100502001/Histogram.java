package a10.s100502001;
import java.awt.*;
import javax.swing.*;
public class Histogram extends JFrame{
	private int[] count;
	private Paint paint=new Paint();
	public Histogram(){
		setTitle("好繽紛!!呵呵");
		setSize(400,350);
		setLocation(10,250);
		count=new int[26]; //儲存26個字母
		add(paint);
	}
	public void setCount(String t){
		for(int i=0;i<t.length();i++){
			char word=t.charAt(i);
			
			if(Character.isLetter(word)){
				if((word>='a')&&(word<='z')) //判斷是否為字母
					count[word-'a']++; //相對應的字母++
				else if((word>='A')&&(word<='Z'))
					count[word-'A']++; //相對應的字母++
			}
		}
		repaint(); //計算完長條圖後，重畫panel
	}
	class Paint extends JPanel{
		protected void paintComponent(Graphics g){
			if(count==null) return; //No display if count is null
			super.paintComponents(g);
			int width=getWidth();
			int height=getHeight();
			int inteval=(int)((width-40)/count.length); //要區分有幾條長條圖
			int eachWidth=(int)((width-40)/24 *0.6); //每條長條圖的寬度
			int maxCount=0; //最長的長條圖
			for(int i=0;i<count.length;i++){
				if(maxCount<count[i])
					maxCount=count[i];
			}
			int x=30; //x is the first position of 長條圖
			g.drawLine(10, height-45, width-10, height-45); //區分字母與長條圖
			for(int i=0;i<count.length;i++){
				int barheight=(int)(((double)count[i]/(double)maxCount)*(height-55));
				g.setColor(new Color((int)(Math.random()*256),(int)(Math.random()*256),(int)(Math.random()*256)));
				g.fillRect(x, height-45-barheight,eachWidth, barheight); //長條圖
				g.setFont(new Font("",Font.BOLD,15));
				g.drawString((char)(65+i)+"",x, height-30); //字母
				x+=inteval;
			}
		}
	}
}
