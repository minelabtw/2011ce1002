package a10.s100502003;

import javax.swing.*;
import java.awt.*;

public class DescriptionPanel extends JPanel{
	private JLabel ImageTitle = new JLabel(); // label for displaying pictures with their name
	private JTextArea description = new JTextArea();
	
	public DescriptionPanel() {
		// let the picture be displayed in the middle of the label
		ImageTitle.setHorizontalAlignment(JLabel.CENTER);
		// let the word be displayed just below the picture
		ImageTitle.setHorizontalTextPosition(JLabel.CENTER);
		ImageTitle.setVerticalTextPosition(JLabel.BOTTOM);
		
		ImageTitle.setFont(new Font("TimesNewRoman", Font.BOLD, 16));
		description.setFont(new Font("TimiesNewRoman", Font.PLAIN, 14));
		
		description.setLineWrap(true); // wrap the line automatically
		description.setWrapStyleWord(true);
		description.setEditable(false); // can't be edited
		
		JScrollPane scrollPane = new JScrollPane(description);
		scrollPane.setPreferredSize(new Dimension(200,150)); // set the dimension of the text
		setLayout(new BorderLayout(5,5));
		add(scrollPane, BorderLayout.CENTER);
		add(ImageTitle, BorderLayout.WEST);
	}
	
	// be able to set different titles when chosen different choices
	public void setTitle(String title) { 
		ImageTitle.setText(title);
	}
	
	// be able to set different pictures when chosen different choices
	public void setImageIcon(ImageIcon icon) { 
		ImageTitle.setIcon(icon);
	}
	// be able to set different descriptions when chosen different choices
	public void setDescription(String text) {
		description.setText(text);
	}
	// get the descriptions for each different countries
	public String getDescription() {
		return description.getText();
	}
}
