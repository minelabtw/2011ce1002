package a10.s100502003;

import javax.swing.*;
import java.awt.*;

public class HistogramPanel extends JPanel {
	private int[] count;
	public void showHistogram(int[] count) { // set the count and display histogram
		this.count = count;
		repaint();
	}
	
	protected void paintComponent(Graphics g) { // paint the histogram
		if(count == null) { // no display if count = null
			return;
		}
		super.paintComponent(g);
		
		int width = getWidth();
		int height = getHeight();
		int interval = (width-40)/count.length;
		int individualWidth = (int)(((width-40)/24)*0.6);
		
		int maxCount = 0; // the maxCount will used for helping decide the height for each bar
		for(int i=0; i<count.length; i++) {
			if(maxCount < count[i])
				maxCount = count[i];
		}
		
		int x=30; // the started position for first bar in the histogram
		
		g.drawLine(10, height-45, width-10, height-45); // the horizontal base line
		for(int j=0; j<count.length; j++) {
			int barHeight = (int)(((double)count[j]/(double)maxCount)*(height-55));
		g.setColor(Color.BLUE);
		g.fillRect(x, height-45-barHeight, individualWidth, barHeight);
		g.setColor(Color.BLACK);
		g.drawString((char)(65+j)+"", x, height-30);
		x += interval; // move x for displaying the next character
		}
	}
	
	public Dimension getPreferredSize() { // override getPreferredSize
		return new Dimension(300,300);
	}
}
