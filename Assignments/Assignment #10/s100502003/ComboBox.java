package a10.s100502003;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ComboBox extends JFrame {
	private JButton Histogram = new JButton("Histogram");
	private HistogramPanel histogramPanel = new HistogramPanel();
	private JFrame histogramFrame = new JFrame();
	
	private String[] flagTitles = {"Canada", "China", "England", "German", "USA"};
	private ImageIcon[] flagImage = { // read the pictures
		new ImageIcon(ClassLoader.getSystemResource("image/Canada.gif")),
		new ImageIcon(ClassLoader.getSystemResource("image/China.gif")),
		new ImageIcon(ClassLoader.getSystemResource("image/England.gif")),
		new ImageIcon(ClassLoader.getSystemResource("image/German.gif")),
		new ImageIcon(ClassLoader.getSystemResource("image/USA.gif"))
	};
	
	private String[] Description = new String[5];
	private DescriptionPanel  descriptionPanel = new DescriptionPanel();
	private JComboBox jcbo = new JComboBox(flagTitles); // create a comboBox for selecting countries
	
	public ComboBox() {
		Description[0] = "Canada is a North American country consisting of ten provinces " + 
						 "and three territories. Located in the northern part of the continent, " + 
						 "it extends from the Atlantic Ocean in the east to the Pacific Ocean " + 
						 "in the west, and northward into the Arctic Ocean. Spanning over 9.9 " + 
						 "million square kilometres, Canada is the world's second-largest country " +
						 "by total area, and its common border with the United States is the " +
						 "longest land border in the world.";
		
		Description[1] = "China, officially the People's Republic of China (PRC), is the world's " +
						 "most-populous country, with a population of over 1.3 billion. The East " +
						 "Asian state covers approximately 9.6 million square kilometres, and is " +
						 "the world's second-largest country by land area, and the third- or " +
						 "fourth-largest in total area, depending on the definition of total area.";
		
		Description[2] = "England is a country that is part of the United Kingdom.It shares land " +
						 "borders with Scotland to the north and Wales to the west; the Irish Sea " +
						 "is to the north west, the Celtic Sea to the south west, while the North " +
						 "Sea to the east and the English Channel to the south separate it from " +
						 "continental Europe. Most of England comprises the central and southern " +
						 "part of the island of Great Britain in the North Atlantic. The country " +
						 "also includes over 100 smaller islands such as the Isles of Scilly and " +
						 "the Isle of Wight.";
		
		Description[3] = "Germany , officially the Federal Republic of Germany , is a federal " +
						 "parliamentary republic in Europe. The country consists of 16 states " +
						 "while the capital and largest city is Berlin. Germany covers an area " +
						 "of 357,021 km2 and has a largely temperate seasonal climate. With " +
						 "81.8 million inhabitants, it is the most populous member state and " +
						 "the largest economy in the European Union. It is one of the major " +
						 "political powers of the European continent and a technological leader " +
						 "in many fields.";
		
		Description[4] = "The United States of America is a federal constitutional republic " +
						 "comprising fifty states and a federal district. The country is situated " +
						 "mostly in central North America, where its forty-eight contiguous states " +
						 "and Washington, D.C., the capital district, lie between the Pacific and " +
						 "Atlantic Oceans, bordered by Canada to the north and Mexico to the south. " +
						 "The state of Alaska is in the northwest of the continent, with Canada " +
						 "to the east and Russia to the west, across the Bering Strait. The state " +
						 "of Hawaii is an archipelago in the mid-Pacific. The country also possesses " +
						 "several territories in the Pacific and Caribbean.";
		
		setDisplay(0); // set the first country(Canada) for displayed
		add(jcbo, BorderLayout.NORTH); // put the comboBox on the top
		add(descriptionPanel, BorderLayout.CENTER); // the descriptionPanel will be put in the middle
		add(Histogram, BorderLayout.SOUTH); // the button will be put in the bottom
		
		jcbo.addItemListener(new ItemListener() { 
			public void itemStateChanged(ItemEvent e) { // handle item selected
				setDisplay(jcbo.getSelectedIndex());
			}
		});
		
		Histogram.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int[] count = countLetters();
				histogramPanel.showHistogram(count); // paint
				histogramFrame.setVisible(true); // let it be able to seen
			}
		});
		// create a new frame to hold the histogramPanel
		histogramFrame.add(histogramPanel);
		histogramFrame.pack(); // size the frame according to the size of the components placed in it
		histogramFrame.setTitle("Histogram");
	}
	
	public void setDisplay(int index) { // set everything for each choice
		descriptionPanel.setTitle(flagTitles[index]);
		descriptionPanel.setImageIcon(flagImage[index]);
		descriptionPanel.setDescription(Description[index]);
	}
	
	private int[] countLetters() { // count the letters in the text area
		int[] count = new int[26];
		String text =descriptionPanel.getDescription();
		for(int i=0; i<text.length(); i++) {
			char character = text.charAt(i);
			if((character >= 'A')&&(character <= 'Z')) {
				count[character - 'A']++;
			}
			else if((character >= 'a')&&(character <= 'z')) {
				count[character - 'a']++;
			}
		}
		return count;
	}
}
