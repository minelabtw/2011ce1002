package a10.s100502023;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class DescriptionPanel extends JPanel
{
	//label for display image
	private JLabel jlblImageTitle = new JLabel();
	//textarea for display text
	protected JTextArea jtaDescription = new JTextArea();
	
	public DescriptionPanel()
	{	//set title
		jlblImageTitle.setHorizontalAlignment(JLabel.CENTER);
		jlblImageTitle.setHorizontalTextPosition(JLabel.CENTER);
		jlblImageTitle.setVerticalTextPosition(JLabel.BOTTOM);
		//set font
		jlblImageTitle.setFont(new Font("SansSerif",Font.BOLD,16));
		jtaDescription.setFont(new Font("Serif",Font.PLAIN,14));
		//set description
		jtaDescription.setLineWrap(true);
		jtaDescription.setWrapStyleWord(true);
		jtaDescription.setEditable(false);
		
		JScrollPane scrollPane = new JScrollPane(jtaDescription);
		scrollPane.setPreferredSize(new Dimension(300,200));
		
		setLayout(new BorderLayout(5,5));
		add(scrollPane,BorderLayout.CENTER);
		add(jlblImageTitle,BorderLayout.WEST);
	}
	
	public void setTitle(String title)
	{
		jlblImageTitle.setText(title);
	}
	public void setImageIcon(ImageIcon icon)
	{
		jlblImageTitle.setIcon(icon);
	}
	public void setDescription(String text)
	{
		jtaDescription.setText(text);
	}

}
