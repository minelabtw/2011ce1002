package a10.s100502023;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

public class Histogram extends JPanel
{
	//letter
	private int[] count;
	
	//set the count and display histogram
	public void showHistogram(int[] count)
	{
		this.count = count;
		repaint();
	}
	//paint
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		int width = getWidth();
		int height = getHeight();
		int interval = (width-40)/count.length;
		int individualWidth = (int)(((width-40)/24)*0.60);
		
		int maxCount =0;
		//find the maxcount
		for(int i=0;i<count.length;i++)
		{
			if(maxCount<count[i])
			{
				maxCount = count[i];
			}
		}
		//start position
		int x=30;
		//draw base line
		g.drawLine(10, height-45, width-10, height-45);
		for(int i=0;i<count.length;i++)
		{//find the bar height
			int barHeight=(int)(((double)count[i]/(double)maxCount)*(height-55));
			//display the bar
			
			g.drawRect(x, height-45-barHeight, individualWidth, barHeight);
			//display the letter
			g.drawString((char)(65+i) + "",x,height-30);
			
			x+=interval;
		}
	}
	
	public Dimension getPreferredSize()
	{
		return new Dimension(300,300);
	}
}
