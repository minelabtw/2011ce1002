package a10.s100502023;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class FrameWork extends JFrame
{	//flagTitles
	private String[] flagTitles ={"Canada","China","England","German","USA"};
	//flagImage
	private ImageIcon[] flagImage = {
			new ImageIcon("image/Canada.gif"),
			new ImageIcon("image/China.gif"),
			new ImageIcon("image/England.gif"),
			new ImageIcon("image/German.gif"),
			new ImageIcon("image/USA.gif")
	};
	
	//flagDesciption
	private String[] flagDesciption = new String[5];
	//descriptionPanel
	private DescriptionPanel descriptionPanel = new DescriptionPanel();
	//ComboBox
	private JComboBox jcbo = new JComboBox(flagTitles);
	//histogram
	private JFrame histogramFrame = new JFrame();
	private Histogram histogram = new Histogram();
	private JButton jbtShowHistogram = new JButton("Histogram Button");
	
	public FrameWork()
	{
		//set flag Description
		setDescription();
		//set initial country for display
		setDisplay(0);
		//add comboBox,descriptionPanel,jbtShowHistogram
		add(jcbo,BorderLayout.NORTH);
		add(descriptionPanel,BorderLayout.CENTER);
		add(jbtShowHistogram,BorderLayout.SOUTH);
		
		//new frame to hold the histogram panel
		setHistogramFrame();
		
		//add ItemListener (jcbo)
		jcbo.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent e)
			{
				setDisplay(jcbo.getSelectedIndex());
			}	
		});
		
		//add ActionListener (jbtShowHistogram)
		jbtShowHistogram.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				//count the letters in the textarea
				int [] count = new int[26];
				count = counterLetters();
				//set the letter count to histogram for display
				histogram.showHistogram(count);
				//show the frame
				histogramFrame.setVisible(true);
			}
		});
		
		
	}
	
	public void setDescription()
	{
		//Desciption for Canada 
		flagDesciption[0]="Canada is a North American country consisting " +
				"of ten provinces and three territories. Located in the northern " +
				"part of the continent, it extends from the Atlantic Ocean in the " +
				"east to the Pacific Ocean in the west, and northward into the " +
				"Arctic Ocean. Spanning over 9.9 million square kilometres, " +
				"Canada is the world's second-largest country by total area, " +
				"and its common border with the United States is the longest " +
				"land border in the world.";
		//Desciption for China 
		flagDesciption[1]="China ; Chinese:; pinyin:; see also Names " +
				"of China), officially the People's Republic of China (PRC), is " +
				"the world's most-populous country, with a population of over 1.3 " +
				"billion. The East Asian state covers approximately 9.6 million " +
				"square kilometres, and is the world's second-largest country by " +
				"land area,[13] and the third- or fourth-largest in total area, " +
				"depending on the definition of total area.[14]";
		//Desciption for England 
		flagDesciption[2]="England is a country that is part of the United Kingdom." +
				"[6][7][8] It shares land borders with Scotland to the north and " +
				"Wales to the west; the Irish Sea is to the north west, the Celtic " +
				"Sea to the south west, while the North Sea to the east and the " +
				"English Channel to the south separate it from continental Europe. " +
				"Most of England comprises the central and southern part of the " +
				"island of Great Britain in the North Atlantic. The country also " +
				"includes over 100 smaller islands such as the Isles of Scilly and " +
				"the Isle of Wight.";
		//Desciption for German  
		flagDesciption[3]="Germany , officially the Federal Republic of Germany " +
				"(German: Bundesrepublik Deutschland, pronounced" +
				"( listen)),[5] is a federal parliamentary republic in " +
				"Europe. The country consists of 16 states while the capital and " +
				"largest city is Berlin. Germany covers an area of 357,021 km2 and " +
				"has a largely temperate seasonal climate. With 81.8 million " +
				"inhabitants, it is the most populous member state and the largest " +
				"economy in the European Union. It is one of the major political " +
				"powers of the European continent and a technological leader in " +
				"many fields.";
				//Desciption for USA 
		flagDesciption[4]="The United States of America " +
				"(commonly abbreviated to the United States, " +
				"the U.S., the USA, America, and the States) " +
				"is a federal constitutional republic comprising " +
				"fifty states and a federal district. The country " +
				"is situated mostly in central North America, where" +
				" its forty-eight contiguous states and Washington," +
				" D.C., the capital district, lie between the Pacific" +
				" and Atlantic Oceans, bordered by Canada to the north" +
				" and Mexico to the south. The state of Alaska is in the " +
				"northwest of the continent, with Canada to the east and " +
				"Russia to the west, across the Bering Strait. The state " +
				"of Hawaii is an archipelago in the mid-Pacific. The country " +
				"also possesses several territories in the Pacific and Caribbean.";
						
	}
	public void setDisplay(int index)
	{//set descriptionPanel
		descriptionPanel.setTitle(flagTitles[index]);
		descriptionPanel.setImageIcon(flagImage[index]);
		descriptionPanel.setDescription(flagDesciption[index]);
	}
	public void setHistogramFrame()
	{//set histogramFrame
		histogramFrame.add(histogram);
		histogramFrame.pack();
		histogramFrame.setTitle("Histogram");
		histogramFrame.setVisible(false);
	}
	public int[] counterLetters()
	{
		//26 letters
		int [] count = new int[26];
		//get contents from the textarea
		String text = descriptionPanel.jtaDescription.getText();

		//count occurrence of each letter
		for(int i=0;i<text.length();i++)
		{
			char character = text.charAt(i);
			
			System.out.println(character);
			
			if ((character>='A') && (character<='Z'))
			{
				count[character-'A']++;
			}
			else if ((character>='a') && (character<='z'))
			{
				count[character-'a']++;
			}
			else
			{
				//nothing
			}
		}
		
		return count;
	}
}
