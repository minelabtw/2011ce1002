package a10.s100502503;

import java.awt.*;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class DescriptionPanel extends JPanel
{
	private JLabel jlbImageTitle = new JLabel();
	private JTextArea jtaDescription = new JTextArea();
	
	public DescriptionPanel()
	{
		//Set the text field of description
		jtaDescription.setLineWrap(true);
		jtaDescription.setWrapStyleWord(true);
		jtaDescription.setEditable(false);
		
		JScrollPane scrollPane = new JScrollPane(jtaDescription);
		setLayout(new BorderLayout(5, 5));
		add(scrollPane, BorderLayout.CENTER);
		add(jlbImageTitle, BorderLayout.WEST);
	}
	
	//Set the title, image and description
	public void setTitle(String title)
	{
		jlbImageTitle.setText(title);
	}
	
	public void setImageIcon(ImageIcon icon)
	{
		jlbImageTitle.setIcon(icon);
	}
	
	public void setDescription(String text)
	{
		jtaDescription.setText(text);
	}
}
\