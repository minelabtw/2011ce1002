package a10.s100502503;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.*;

public class FrameWork extends JFrame
{
	//Declare the array to store 5 countries
	private String[] contries = {"Canada", "China", "England", "German", "USA"};
	private ImageIcon[] pictures = {new ImageIcon("image/Canada.gif"), new ImageIcon("image/China.gif"), new ImageIcon("image/England.gif"), new ImageIcon("image/German.gif"), new ImageIcon("image/USA.gif")};
	private String[] description = new String[9];
	
	//Declare the button to show out the histogram
	private JButton hisButton = new JButton("Histogram");
	private Histogram histogram = new Histogram();
	
	//New a frame to show out the picture
	private JFrame histogramFrame = new JFrame();
	
	//call the JComboBox class
	private JComboBox jcb = new JComboBox(contries);
	public DescriptionPanel descriptionPanel = new DescriptionPanel();
	
	public FrameWork()
	{
		description[0] = "This contry is Canada";
		description[1] = "This contry is China";
		description[2] = "This contry is England";
		description[3] = "This contry is German";
		description[4] = "This contry is America";
		
		//This is the first country which should display in the screen
		setDisplay(0);
		add(descriptionPanel, BorderLayout.CENTER);
		add(jcb, BorderLayout.NORTH);
		add(hisButton, BorderLayout.SOUTH);
		
		//This is the ItemListner to determine which country to show when the user click
		jcb.addItemListener(new ItemListener()
			{
				public void itemStateChanged(ItemEvent e) 
				{
					setDisplay(jcb.getSelectedIndex());
				}
			}
		);
		
		//The Button of histogram
		hisButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int[] count = countLetters(jcb.getSelectedIndex());
				histogram.showHistogram(count);
				histogramFrame.setVisible(true);
			}	
		});
		
		histogramFrame.add(histogram);
		histogramFrame.pack();
		histogramFrame.setTitle("Histogram");
		
	}
		
	//This is the function to count how many letters is in the description
	private int[] countLetters(int i)
	{
		int[] count = new int[26];
		String text = description[i];
		
		for(int j = 0; j < text.length(); j++)
		{
			char character = text.charAt(j);
			
			if((character >= 'A') && (character <= 'Z'))
			{
				count[character - 'A']++; //Each element represent the numbers of the letter
			}
			else if((character >= 'a') && (character <= 'z'))
			{
				count[character -'a']++;
			}
		}
		return count;//Return the array of count
	}
	
	//The method to determine which country to display
	public void setDisplay(int i)
	{
		descriptionPanel.setTitle(contries[i]);
		descriptionPanel.setImageIcon(pictures[i]);
		descriptionPanel.setDescription(description[i]);
	}	
}
