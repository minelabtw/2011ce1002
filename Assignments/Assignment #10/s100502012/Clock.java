package a10.s100502012;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;
import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Polygon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.util.Calendar;
import java.util.GregorianCalendar;



public class Clock extends JPanel{
	
	private int radius;
	private int hr=0;
	private int min=0;
	private int sec=0;
	private int quadrantX=1;
    private int quadrantY=1;
    String time;
   
	public Clock(){//constructor
	}
    
    public void setCurrentTime(){//adding one second to the whole time
    	sec++;
    	
    	Calendar calendar=new GregorianCalendar();
    	time=calendar.get(Calendar.HOUR_OF_DAY)+" : "+calendar.get(Calendar.MINUTE)+" : "+calendar.get(Calendar.SECOND)+"";	
    }
    
    public String getCurrentTime(){//getting current second
    	return time;
    }
    public int getSec(){//getting current second
    	return sec;
    }
    
    public int getMin(){//getting current minute
    	return min;
    }
    
    public int getHr(){//getting current hour
    	return hr;
    }
    
    public void secondClear(){//resetting the current time
    	sec=0;
    	min=0;
    	hr=0;
    }
    
    
    protected void paintComponent(Graphics g){
		super.paintComponent(g);
        
	    int x0=getWidth()/2;//clock core x
		int y0=getWidth()/2;//clock core y
		int x=getWidth()/2;//needle tip x
		int y=getWidth()/2-getWidth()/4;//needle tip y
		radius=(int)Math.sqrt((x-x0)*(x-x0)+(y-y0)*(y-y0));//the length of the needle
		
		
   	    
   	    //the operator of moving the needle (by redrawing the needle tip x & y per second) (from line 66 to line 105)
		if (quadrantX>0 && quadrantY>0){
			x+=(int)(radius*Math.sin(sec*Math.PI/30));
			y+=(int)(radius-radius*Math.cos(sec*Math.PI/30));
		}	
		
		if (quadrantX>0 && quadrantY<0){
			x-=(int)(radius*Math.sin(sec*Math.PI/30));
			y+=(int)(radius-radius*Math.cos(sec*Math.PI/30));
		}	
		
		if (quadrantX<0 && quadrantY<0){
			x-=(int)(radius*Math.sin(sec*Math.PI/30));
			y-=(int)(radius-radius*Math.cos(sec*Math.PI/30));
		}	
		
		if (quadrantX<0 && quadrantY>0){
			x+=(int)(radius*Math.sin(sec*Math.PI/30));
			y-=(int)(radius-radius*Math.cos(sec*Math.PI/30));
		}	
		
		if (x>x0+radius && y>y0){
			quadrantX=1;
			quadrantY=-1;
		}
		
		if (x<x0 && y>y0+radius){
			quadrantX=-1;
			quadrantY=-1;
		}
		
		if (x<x0-radius && y<y0){
			quadrantX=-1;
			quadrantY=1;
		}
		
		if (x>x0 && y<y0-radius){
			quadrantX=1;
			quadrantY=1;
		}
		
		
		
		//setting and displaying the location , radius , components ,and color of clock 
		g.setColor(Color.black);
		g.fillOval(getWidth()/4,getWidth()/4,getWidth()/2,getWidth()/2);
		g.setColor(Color.white);
		g.drawLine(x , y , x0 , y0);
		g.setColor(Color.white);
		g.drawLine(x0+radius , y0 , (int)x0+radius*6/7 , y0);
		g.setColor(Color.white);
		g.drawLine(x0 , y0+radius , x0 , (int)y0+radius*6/7);
		g.setColor(Color.white);
		g.drawLine(x0-radius , y0 , (int)x0-radius*6/7 , y0);
		g.setColor(Color.white);
		g.drawLine(x0 , y0-radius , x0 , (int)y0-radius*6/7);
		
	
		
		if (sec==60){// 1 min = 60 sec
			sec=0;
			min+=1;
		}
		
		if (min==60){// 60 hr = 60 min
			min=0;
			hr+=1;
		}
		
    	setCurrentTime();//adding one second after per repainting
	}
}
