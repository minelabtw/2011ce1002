package a10.s100502012;

import javax.swing.*;
import java.awt.*;

public class DescriptionPanel extends JPanel {
	private JLabel jlbIT=new JLabel();
	
	private JTextArea jtaDp=new JTextArea();
	
	public DescriptionPanel(){//setting buttons
		jlbIT.setHorizontalAlignment(JLabel.CENTER);
		jlbIT.setHorizontalTextPosition(JLabel.CENTER);
		jlbIT.setVerticalAlignment(JLabel.CENTER);
		
		jlbIT.setFont(new Font("SanSerif",Font.BOLD,16));
		jtaDp.setFont(new Font("Serif",Font.PLAIN,14));
		
		jtaDp.setLineWrap(true);
		jtaDp.setWrapStyleWord(true);
		jtaDp.setEditable(false);
		
		JScrollPane sp=new JScrollPane(jtaDp); 
		
		setLayout(new BorderLayout(5,5));
		add(sp,BorderLayout.CENTER);
		add(jlbIT,BorderLayout.WEST);
	}
	
	public void setTitle(String title){
		jlbIT.setText(title);
	}
	
	public void setImageIcon(ImageIcon icon){
		jlbIT.setIcon(icon);
	}
	
	public void setDescription(String text){
		jtaDp.setText(text);
	}
	
	public String getDescription(){
		return jtaDp.getText();
	}

}
