package a10.s100502507;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
public class FrameWork extends JFrame implements ItemListener, ActionListener {
	FrameWork() {//Initializing variables and organizing frame
		titles = new String[5];
		images = new ImageIcon[5];
		text = new String[5];
		jtxImageDescribtion = new String[5];
		titles[0] = "Canada";
		titles[1] = "China";
		titles[2] = "England";
		titles[3] = "German";
		titles[4] = "USA";
		images[0] = new ImageIcon("image/Canada.gif");
		images[1] = new ImageIcon("image/China.gif");
		images[2] = new ImageIcon("image/England.gif");
		images[3] = new ImageIcon("image/German.gif");
		images[4] = new ImageIcon("image/USA.gif");
		jtxImageDescribtion[0] = "Canada is a North American country consisting of ten provinces and three territories. Located in the northern part of the continent, it" +
				" extends from the Atlantic Ocean in the east to the Pacific Ocean in the west, and northward into the Arctic Ocean. Spanning over 9.9 million square kilometres, Canada" +
				" is the world's second-largest country by total area, and its common border with the United States is the longest land border in the world.";
		jtxImageDescribtion[1] = "China, officially the People's Republic of China (PRC), is the world's most-populous" +
				" country, with a population of over 1.3 billion. The East Asian state covers approximately 9.6 million square kilometres, and is the world's second-largest country by land" +
				" area,[13] and the third- or fourth-largest in total area, depending on the definition of total area.";
		jtxImageDescribtion[2] = "England is a country that is part of the United Kingdom. It shares land borders with Scotland to the north and Wales to the west; the Irish" +
				" Sea is to the north west, the Celtic Sea to the south west, while the North Sea to the east and the English Channel to the south separate it from continental Europe." +
				" Most of England comprises the central and southern part of the island of Great Britain in the North Atlantic. The country also includes over 100 smaller islands such as" +
				" the Isles of Scilly and the Isle of Wight.";
		jtxImageDescribtion[3] = "Germany, officially the Federal Republic of Germany" +
				" , is a federal parliamentary republic in Europe. The country consists of 16 states while the capital and largest city is Berlin. Germany covers an area" +
				" of 357,021 km2 and has a largely temperate seasonal climate. With 81.8 million inhabitants, it is the most populous member state and the largest economy in the European" +
				" Union. It is one of the major political powers of the European continent and a technological leader in many fields.";
		jtxImageDescribtion[4] = "The United States of America (commonly abbreviated to the United States, the U.S., the USA, America, and the States) is a federal constitutional republic" +
				" comprising fifty states and a federal district. The country is situated mostly in central North America, where its forty-eight contiguous states and Washington, D.C., the" +
				" capital district, lie between the Pacific and Atlantic Oceans, bordered by Canada to the north and Mexico to the south. The state of Alaska is in the northwest of the" +
				" continent, with Canada to the east and Russia to the west, across the Bering Strait. The state of Hawaii is an archipelago in the mid-Pacific. The country also possesses" +
				" several territories in the Pacific and Caribbean.";
		describtions = new JTextArea(jtxImageDescribtion[0]);
		describtions.setLineWrap(true);
		describtions.setWrapStyleWord(true);
		describtions.setEditable(false);
		jlbImageDescribtion = new JLabel("Canada");
		jlbImageDescribtion.setHorizontalAlignment(JLabel.CENTER);
		jlbImageDescribtion.setHorizontalTextPosition(JLabel.CENTER);
		jlbImageDescribtion.setVerticalTextPosition(JLabel.BOTTOM);
		JScrollPane jscdescribtion = new JScrollPane(describtions);
		histogram = new JButton("Histogram");
		choice = new JComboBox(titles);
		JPanel jplImage = new JPanel();
		jlbImage = new JLabel(images[0]);
		
		jplImage.setLayout(new BorderLayout());
		setLayout(new BorderLayout());
		
		jplImage.add(jlbImage, BorderLayout.CENTER);
		jplImage.add(jlbImageDescribtion, BorderLayout.SOUTH);
		
		histogram.addActionListener(this);
		choice.addItemListener(this);
		add(choice, BorderLayout.NORTH);
		add(jplImage, BorderLayout.WEST);
		add(jscdescribtion, BorderLayout.EAST);
		add(histogram, BorderLayout.SOUTH);
		
		h = new Histogram(jtxImageDescribtion[choice.getSelectedIndex()]);
		h.setDefaultCloseOperation(h.EXIT_ON_CLOSE);
		h.setLocationRelativeTo(null);
		h.setTitle("Histogram");
		h.setSize(300, 300);
		h.setVisible(false);
		h.setResizable(false);
	}
	
	public void itemStateChanged(ItemEvent e) {//Change data if the selection of ComboBox is changed
		int index = choice.getSelectedIndex();
		jlbImage.setIcon(images[index]);
		jlbImageDescribtion.setText(titles[index]);
		describtions.setText(jtxImageDescribtion[index]);
		h.setString(jtxImageDescribtion[choice.getSelectedIndex()]);
	}
	
	public void actionPerformed(ActionEvent e) {//If the button is clicked, set the hidden frame to visible, and pass the string which needs to be calculated.
		h.setVisible(true);
	}
	
	private String[] titles, text, jtxImageDescribtion;
	private ImageIcon[] images;
	private JComboBox choice;
	private JLabel jlbImage, jlbImageDescribtion;
	private JButton histogram;
	private JTextArea describtions;
	private Histogram h;
}
