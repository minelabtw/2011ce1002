package a10.s100502507;

public class A101 {
	public static void main(String[] args) {
		FrameWork f = new FrameWork();
		f.setSize(600, 300);
		f.setDefaultCloseOperation(f.EXIT_ON_CLOSE);
		f.setLocationRelativeTo(null);
		f.setTitle("A101");
		f.setVisible(true);
		f.setResizable(false);
		f.pack();
	}
}