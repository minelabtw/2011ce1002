package a10.s100502507;

import javax.swing.*;
import java.awt.*;

public class Histogram extends JFrame {
	Histogram(String input) {//Initializing variable
		d = new draw(input);
		add(d);
	}
	
	public void setString(String input) {//Call the method "setString" of class draw
		d.setString(input);
	}
	
	public class draw extends JPanel {
		draw(String input) {//Initializing variable
			count = calculating(input);
		}
		
		public void setString(String s) {//Change the image of this frame once the selection of ComboBox is changed
			count = calculating(s);
			repaint();
		}
		
		private int[] calculating(String input) {//Count the quantities of each character appeared
			int[] quantity = new int[26];
			
			for(int i=0; i<26; i++) {
				quantity[i] = 0;
			}
			
			for(int i=0; i<input.length(); i++) {
				if(input.charAt(i) >= 'A' && input.charAt(i)<='Z') {
					quantity[input.charAt(i) - 'A']++;
				}
				if(input.charAt(i) >= 'a' && input.charAt(i)<='z') {
					quantity[input.charAt(i) - 'a']++;
				}
			}
			
			return quantity;
		}
		
		protected void paintComponent(Graphics g) {//Draw the result
			super.paintComponents(g);
			int width = 300;
			int height = 300;
			int interval = (width - 40) / count.length;
			int individualWidth = (int)(((width - 40) / 24) * 0.60);
			
			int maxCount = 0;
			
			for(int i=0; i<count.length; i++) {
				if(maxCount < count[i]) {
					maxCount = count[i];
				}
			}
			
			int x = 30;
			
			g.drawLine(10, height - 45, width - 10, height - 45);
			for(int i=0; i<count.length; i++) {
				int barHeight = (int)(((double)count[i] / (double)maxCount) * (height - 55));
				g.setColor(Color.WHITE);
				g.fillRect(x,  height - 45 - barHeight, individualWidth, barHeight);
				g.setColor(Color.BLACK);
				g.drawRect(x,  height - 45 - barHeight, individualWidth, barHeight);
				
				g.drawString((char)(65 + i) + "", x, height - 30);
				
				x += interval;
			}
		}
		private int[] count;
	}
	
	private draw d;
}
