package a12.s100502502;
import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.util.*;

public class A122 extends JApplet{
	//initialize JPanel JLabel JTextField JComboBox Timer
	JPanel P1 = new JPanel();
	JPanel P2 = new JPanel();
	JPanel P3 = new JPanel();
	private ResourceBundle res = ResourceBundle.getBundle("MyResource");
	JLabel day = new JLabel(res.getString("Date"));
	JLabel time = new JLabel(res.getString("Time"));
	JLabel country = new JLabel(res.getString("Choose_locale"));
	JLabel zone = new JLabel(res.getString("Time_Zone"));
	JLabel daystyle = new JLabel(res.getString("Date_Style"));
	JLabel timestyle = new JLabel(res.getString("Time_Style"));
	private JTextField dayT = new JTextField(15);
	private JTextField timeT = new JTextField(15);
	JComboBox locales = new JComboBox();
	JComboBox zones = new JComboBox();
	JComboBox dayS = new JComboBox();
	JComboBox timeS = new JComboBox();
	private Timer timer = new Timer(500, new TimerListener());
	private TimeZone timezone = TimeZone.getTimeZone("Etc/GMT+12");
	private Locale[] locale = {Locale.CHINESE, Locale.JAPAN, Locale.ENGLISH, Locale.FRENCH};
	private String[] timezones = TimeZone.getAvailableIDs();
	private String[] styles = {"FULL", "LONG", "MEDIUM", "SHORT"};
	private int[] formatstyle = {DateFormat.FULL, DateFormat.LONG, DateFormat.MEDIUM, DateFormat.SHORT};
	private int dateStyle, timeStyle;
	
	public void init(){
		setSize(550, 300);
		setdatestyle(DateFormat.FULL);
		settimestyle(DateFormat.FULL);
		addLocale();
		addTimeZone();
		addDatestyle();
		addTimestyle();
		dayT.setOpaque(true);
		timeT.setOpaque(true);
		dayT.setBackground(Color.WHITE);
		timeT.setBackground(Color.WHITE);
		dayT.setEditable(false);
		timeT.setEditable(false);
		P1.setLayout(new FlowLayout());
		P2.setLayout(new FlowLayout());
		P3.setLayout(new FlowLayout());
		P1.add(day);
		P1.add(dayT);
		P1.add(time);
		P1.add(timeT);
		P2.add(country);
		P2.add(locales);
		P2.add(zone);
		P2.add(zones);
		P3.add(daystyle);
		P3.add(dayS);
		P3.add(timestyle);
		P3.add(timeS);
		locales.addActionListener(new ActionListener(){//when select
			public void actionPerformed(ActionEvent e){
				setLocale(locale[locales.getSelectedIndex()]);
				updateString();
			}
		});
		zones.addActionListener(new ActionListener(){//when select
			public void actionPerformed(ActionEvent e){
				timezone = TimeZone.getTimeZone(timezones[zones.getSelectedIndex()]);
			}
		});
		dayS.addActionListener(new ActionListener(){//when select
			public void actionPerformed(ActionEvent e){
				setdatestyle(formatstyle[dayS.getSelectedIndex()]);
			}
		});
		timeS.addActionListener(new ActionListener(){//when select
			public void actionPerformed(ActionEvent e){
				settimestyle(formatstyle[timeS.getSelectedIndex()]);
			}
		});
		setLayout(new GridLayout(3,1,1,1));
		add(P1);
		add(P2);
		add(P3);
		timer.start();
	}
	public void updateString(){//update label string
		res = ResourceBundle.getBundle("MyResource", getLocale());
		day.setText(res.getString("Date"));
		time.setText(res.getString("Time"));
		country.setText(res.getString("Choose_locale"));
		zone.setText(res.getString("Time_Zone"));
		daystyle.setText(res.getString("Date_Style"));
		timestyle .setText(res.getString("Time_Style"));
		repaint();
	}
	public void setdatestyle(int style){
		dateStyle = style;
	}
	public int getdatestyle(){
		return dateStyle;
	}
	public void settimestyle(int style){
		timeStyle = style;
	}
	public int gettimestyle(){
		return timeStyle;
	}
	public void addLocale(){//add item to ConboBox
		for(int i = 0; i < locale.length; i++){
			locales.addItem(locale[i].getDisplayName()+"");	
		}
	}
	public void addTimeZone(){//add item to ConboBox
		for(int i = 0; i < timezones.length; i++){
			zones.addItem(timezones[i]);
		}
	}
	public void addDatestyle(){//add item to ConboBox
		for(int i = 0; i < styles.length; i++){
			dayS.addItem(styles[i]);
		}
	}
	public void addTimestyle(){//add item to ConboBox
		for(int i = 0; i < styles.length; i++){
			timeS.addItem(styles[i]);
		}
	}
	class TimerListener implements ActionListener{//when time run
		public void actionPerformed(ActionEvent e){
			Calendar calendar = new GregorianCalendar(timezone, getLocale());//create a calendar
			DateFormat dateformat = DateFormat.getDateInstance(getdatestyle(), getLocale());//set date format
			dateformat.setTimeZone(timezone);
			DateFormat timeformat = DateFormat.getTimeInstance(gettimestyle(), getLocale());//set time format
			timeformat.setTimeZone(timezone);
			dayT.setText(dateformat.format(calendar.getTime()));//update text
			timeT.setText(timeformat.format(calendar.getTime()));//update text
		}
	}
}
