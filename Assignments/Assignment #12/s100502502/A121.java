package a12.s100502502;
import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.event.*;

public class A121 extends JApplet{
	//initialize JMenuBar JToolBar JPabel JLabel ImageIcon
	private JMenuBar menu = new JMenuBar();
	private JToolBar Toolbar = new JToolBar(JToolBar.VERTICAL);
	private JMenu image = new JMenu("Image");
	private JPanel P1 = new JPanel();
	private JLabel L1 = new JLabel();
	private ImageIcon canada = new ImageIcon("image/Canada.gif");
	private ImageIcon china = new ImageIcon("image/China.gif");
	private ImageIcon england = new ImageIcon("image/England.gif");
	private ImageIcon german = new ImageIcon("image/German.gif");
	private ImageIcon usa = new ImageIcon("image/USA.gif");
	private ImageIcon canada2 = new ImageIcon("image/Canada_Icon.gif");
	private ImageIcon china2 = new ImageIcon("image/China_Icon.gif");
	private ImageIcon england2 = new ImageIcon("image/England_Icon.gif");
	private ImageIcon german2 = new ImageIcon("image/German_Icon.gif");
	private ImageIcon usa2 = new ImageIcon("image/USA_Icon.gif");
	public void init(){
		setSize(500, 300);
		//create Action 
		Action canada2Action = new Action("Canada", canada2, new Integer(KeyEvent.VK_L));
		Action china2Action = new Action("China", china2, new Integer(KeyEvent.VK_L));
		Action england2Action = new Action("England", england2, new Integer(KeyEvent.VK_L));
		Action german2Action = new Action("German", german2, new Integer(KeyEvent.VK_L));
		Action usa2Action = new Action("USA", usa2, new Integer(KeyEvent.VK_L));
		setJMenuBar(menu);
		menu.add(image);
		image.add(canada2Action);
		image.add(china2Action);
		image.add(england2Action);
		image.add(german2Action);
		image.add(usa2Action);
		Toolbar.add(canada2Action);
		Toolbar.add(china2Action);
		Toolbar.add(england2Action);
		Toolbar.add(german2Action);
		Toolbar.add(usa2Action);
		add(Toolbar, BorderLayout.EAST);
		P1.add(L1, BorderLayout.CENTER);
		add(P1, BorderLayout.CENTER);
	}
	class Action extends AbstractAction{
		String name;
		Icon image;
		Action(String name, Icon image, Integer mnemonic){
			super(name, image);
			this.name = name;
			this.image = image;
			putValue(Action.MNEMONIC_KEY, mnemonic);
		}
		public void actionPerformed(ActionEvent e){
			if(name.equals("Canada")){
				L1.setIcon(canada);
			}
			else if(name.equals("China")){
				L1.setIcon(china);
			}
			else if(name.equals("England")){
				L1.setIcon(england);
			}
			else if(name.equals("German")){
				L1.setIcon(german);
			}
			else if(name.equals("USA")){
				L1.setIcon(usa);
			}
		}
	}
}
