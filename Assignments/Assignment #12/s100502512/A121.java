package a12.s100502512;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class A121 extends JApplet {
	private JButton Canada = new JButton(new ImageIcon("Image/Canada_Icon.gif")); // 上方五個按鈕
	private JButton China = new JButton(new ImageIcon("Image/China_Icon.gif"));
	private JButton German = new JButton(new ImageIcon("Image/German_Icon.gif"));
	private JButton USA = new JButton(new ImageIcon("Image/USA_Icon.gif"));
	private JButton England = new JButton(new ImageIcon(
			"Image/England_Icon.gif"));
	private JMenuItem exit = new JMenuItem("Exit"); // 離開
	private JMenuItem Ca = new JMenuItem("Canada");
	private JMenuItem Chi = new JMenuItem("China");
	private JMenuItem Ger = new JMenuItem("German");
	private JMenuItem US = new JMenuItem("USA");
	private JMenuItem En = new JMenuItem("England");
	private JLabel pic = new JLabel();
	private JPanel p = new JPanel();

	public A121() {
		p.setBackground(Color.WHITE);
		p.add(pic);
		add(p);
		JMenuBar M = new JMenuBar(); // 主選單
		JMenu m = new JMenu("File");
		m.add(Ca);
		m.add(Chi);
		m.add(Ger);
		m.add(US);
		m.add(En);
		m.add(exit);
		M.add(m);
		setJMenuBar(M);
		JToolBar t = new JToolBar();
		t.setFloatable(false);
		t.add(Canada);
		t.add(China);
		t.add(German);
		t.add(USA);
		t.add(England);
		add(t, BorderLayout.NORTH);

		Ca.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pic.setIcon(new ImageIcon("Image/Canada.gif"));

			}
		});
		Chi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pic.setIcon(new ImageIcon("Image/China.gif"));

			}
		});
		Ger.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pic.setIcon(new ImageIcon("Image/German.gif"));

			}
		});
		US.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pic.setIcon(new ImageIcon("Image/USA.gif"));

			}
		});
		En.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pic.setIcon(new ImageIcon("Image/England.gif"));

			}
		});
		Canada.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pic.setIcon(new ImageIcon("Image/Canada.gif"));

			}
		});

		China.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pic.setIcon(new ImageIcon("Image/China.gif"));
			}
		});
		German.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pic.setIcon(new ImageIcon("Image/German.gif"));
			}
		});
		USA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pic.setIcon(new ImageIcon("Image/USA.gif"));
			}
		});
		England.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pic.setIcon(new ImageIcon("Image/England.gif"));
			}
		});
		exit.addActionListener(new ActionListener() { // 離開
			public void actionPerformed(ActionEvent e) {
				System.exit(JFrame.EXIT_ON_CLOSE);
			}
		});
	}
}
