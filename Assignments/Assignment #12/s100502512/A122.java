package a12.s100502512;

import java.awt.*;
import javax.swing.*;
import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.TimeZone;
import javax.swing.JComboBox;

public class A122 extends JApplet {
	private JComboBox jcb1 = new JComboBox();
	private JComboBox jcb2 = new JComboBox();
	private JComboBox jcb3 = new JComboBox();
	private JComboBox jcb4 = new JComboBox();
	private Locale locale = Locale.getDefault();
	private Locale[] locales = { Locale.ENGLISH, Locale.FRENCH, Locale.JAPAN,
			Locale.CHINESE };
	private String[] availableTimeZones = TimeZone.getAvailableIDs();
	private String[] styles = { "Full", "Long", "Medium", "Short" };
	private JPanel p1 = new JPanel();
	private JPanel p2 = new JPanel();
	private JPanel p3 = new JPanel();
	private ResourceBundle res = ResourceBundle.getBundle("MyResource");
	private JLabel l1 = new JLabel((res.getString("Date")));
	private JLabel l2 = new JLabel(res.getString("Time"));
	private JLabel l3 = new JLabel(res.getString("Choose_locale"));
	private JLabel l4 = new JLabel(res.getString("Time_Zone"));
	private JLabel l5 = new JLabel(res.getString("Date_Style"));
	private JLabel l6 = new JLabel(res.getString("Time_Style"));
	private JTextField t1 = new JTextField();
	private JTextField t2 = new JTextField();
	private TimeZone timeZone = TimeZone.getTimeZone("CST");
	private TimeZone[] timeZones;
	private Timer timer = new Timer(1000, new TimerListener());
	private DateFormat format_date;
	private DateFormat format_time;
	private int dateStyle = DateFormat.FULL;
	private int timeStyle = DateFormat.FULL;

	public A122() {
		setLocale(locale);
		timer.start();
		ResourceBundle.getBundle("MyResource");
		setjcb1();// 設定combobox
		setjcb2();
		setjcb3();
		setjcb4();
		p1.setLayout(new GridLayout(2, 1));// 排版
		p1.add(l1);
		p1.add(t1);
		p1.add(l2);
		p1.add(t2);
		add(p1);
		p2.setLayout(new GridLayout(2, 1));
		p2.add(l3);
		p2.add(jcb1);
		p2.add(l4);
		p2.add(jcb2);
		p2.add(l5);
		p2.add(jcb3);
		p2.add(l6);
		p2.add(jcb4);
		add(p2);
		p3.setLayout(new BorderLayout());
		p3.add(p1, BorderLayout.NORTH);
		p3.add(p2, BorderLayout.CENTER);
		add(p3);

		jcb1.addActionListener(new ActionListener() {// 選擇語言
			public void actionPerformed(ActionEvent e) {
				locale = locales[jcb1.getSelectedIndex()];
				setLocale(locale);
				ChangeString();
				System.out.println(locales[jcb1.getSelectedIndex()]
						.getDisplayName());
			}
		});
		jcb2.addActionListener(new ActionListener() {// 時區
			public void actionPerformed(ActionEvent e) {
				timeZone = timeZones[jcb2.getSelectedIndex()];
			}
		});
		jcb3.addActionListener(new ActionListener() {// 選擇日期格式
			public void actionPerformed(ActionEvent e) {
				if (jcb3.getSelectedIndex() == 0)
					dateStyle = DateFormat.FULL;
				else if (jcb3.getSelectedIndex() == 1)
					dateStyle = DateFormat.LONG;
				else if (jcb3.getSelectedIndex() == 2)
					dateStyle = DateFormat.MEDIUM;
				else if (jcb3.getSelectedIndex() == 3)
					dateStyle = DateFormat.SHORT;
			}
		});

		jcb4.addActionListener(new ActionListener() {// 選擇時間格式
			public void actionPerformed(ActionEvent e) {
				if (jcb4.getSelectedIndex() == 0)
					timeStyle = DateFormat.FULL;
				else if (jcb4.getSelectedIndex() == 1)
					timeStyle = DateFormat.LONG;
				else if (jcb4.getSelectedIndex() == 2)
					timeStyle = DateFormat.MEDIUM;
				else if (jcb4.getSelectedIndex() == 3)
					timeStyle = DateFormat.SHORT;
			}
		});
	}

	public void ChangeString() {// 改變語系
		res = ResourceBundle.getBundle("MyResource", locale);
		l1.setText(res.getString("Date"));
		l2.setText(res.getString("Time"));
		l3.setText(res.getString("Choose_locale"));
		l4.setText(res.getString("Time_Zone"));
		l5.setText(res.getString("Date_Style"));
		l6.setText(res.getString("Time_Style"));
		repaint();
	}

	public void setjcb1() {
		for (int i = 0; i < locales.length; i++) {
			jcb1.addItem(locales[i].getDisplayName());
		}
	}

	public void setjcb2() {
		Arrays.sort(availableTimeZones);
		timeZones = new TimeZone[availableTimeZones.length];
		for (int i = 0; i < availableTimeZones.length; i++) {
			jcb2.addItem(availableTimeZones[i]);
			timeZones[i] = TimeZone.getTimeZone(availableTimeZones[i]);
		}
	}

	public void setjcb3() {
		for (int i = 0; i < styles.length; i++) {
			jcb3.addItem(styles[i]);
		}
	}

	public void setjcb4() {
		for (int i = 0; i < styles.length; i++) {
			jcb4.addItem(styles[i]);
		}
	}

	private class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Calendar calendar = new GregorianCalendar(timeZone, getLocale());
			format_date = DateFormat.getDateInstance(dateStyle, getLocale());
			format_time = DateFormat.getTimeInstance(timeStyle, getLocale());
			format_date.setTimeZone(timeZone);
			format_time.setTimeZone(timeZone);
			t1.setText(format_date.format(calendar.getTime()));
			t2.setText(format_time.format(calendar.getTime()));

		}
	}

}
