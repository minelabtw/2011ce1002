package a12.s100502018;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class A121 extends JApplet {
	private JMenuBar jmb = new JMenuBar();
	private JMenu jmuCountries = new JMenu("Countries");
	private JMenuItem jmiCanada = new JMenuItem("Canada", 'C');
	private JMenuItem jmiChina = new JMenuItem("China", 'C');
	private JMenuItem jmiEngland = new JMenuItem("England", 'E');
	private JMenuItem jmiGermany = new JMenuItem("Germany", 'G');
	private JMenuItem jmiUSA = new JMenuItem("USA", 'U');
	private JToolBar jToolBar = new JToolBar("My ToolBar");
	private ImageIcon[] countries = { new ImageIcon("image/Canada.gif"),
			new ImageIcon("image/China.gif"),
			new ImageIcon("image/England.gif"),
			new ImageIcon("image/Germany.gif"), new ImageIcon("image/USA.gif") };
	private ImageIcon[] countriesIcon = {
			new ImageIcon("image/Canada_Icon.gif"),
			new ImageIcon("image/China_Icon.gif"),
			new ImageIcon("image/England_Icon.gif"),
			new ImageIcon("image/Germany_Icon.gif"),
			new ImageIcon("image/USA_Icon.gif") };
	private JButton jbtCanada = new JButton(countriesIcon[0]);
	private JButton jbtChina = new JButton(countriesIcon[1]);
	private JButton jbtEngland = new JButton(countriesIcon[2]);
	private JButton jbtGermany = new JButton(countriesIcon[3]);
	private JButton jbtUSA = new JButton(countriesIcon[4]);
	private JLabel jlblDisplay = new JLabel();
	private JPanel jpnButtons = new JPanel();

	public A121() {
		setLayout(new BorderLayout());
		setJMenuBar(jmb);
		jmuCountries.setMnemonic('C');
		jmb.add(jmuCountries);
		jmuCountries.add(jmiCanada);
		jmuCountries.add(jmiChina);
		jmuCountries.add(jmiEngland);
		jmuCountries.add(jmiGermany);
		jmuCountries.add(jmiUSA);

		jToolBar.add(jbtCanada);
		jToolBar.add(jbtChina);
		jToolBar.add(jbtEngland);
		jToolBar.add(jbtGermany);
		jToolBar.add(jbtUSA);

		jpnButtons.setLayout(new GridLayout(2, 1));
		jpnButtons.add(jmb);
		jpnButtons.add(jToolBar);

		Action canadaAction = new MyAction("Canada", countries[0]);
		Action chinaAction = new MyAction("China", countries[1]);
		Action englandAction = new MyAction("England", countries[2]);
		Action germanyAction = new MyAction("Germany", countries[3]);
		Action usaAction = new MyAction("USA", countries[4]);
		jmiCanada.addActionListener(canadaAction);
		jbtCanada.addActionListener(canadaAction);
		jmiChina.addActionListener(chinaAction);
		jbtChina.addActionListener(chinaAction);
		jmiEngland.addActionListener(englandAction);
		jbtEngland.addActionListener(englandAction);
		jmiGermany.addActionListener(germanyAction);
		jbtGermany.addActionListener(germanyAction);
		jmiUSA.addActionListener(usaAction);
		jbtUSA.addActionListener(usaAction);

		add(jpnButtons, BorderLayout.NORTH);
		add(jlblDisplay, BorderLayout.CENTER);
	}

	private class MyAction extends AbstractAction {
		String name;

		public MyAction(String name, Icon icon) {
			super(name, icon);
			this.name = name;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (name.equals("Canada"))
				jlblDisplay.setIcon(countries[0]);
			else if (name.equals("China"))
				jlblDisplay.setIcon(countries[1]);
			else if (name.equals("England"))
				jlblDisplay.setIcon(countries[2]);
			else if (name.equals("Germany"))
				jlblDisplay.setIcon(countries[3]);
			else if (name.equals("USA"))
				jlblDisplay.setIcon(countries[4]);
		}
	}

}
