package a12.s100502021;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.*;
public class A121 extends JApplet{
	private JLabel pic=new JLabel();
	private FlowLayout flow=new FlowLayout();
	ImageIcon Canada_Icon=new ImageIcon(getClass().getResource("image/Canada_Icon.gif"));
	ImageIcon China_Icon=new ImageIcon(getClass().getResource("image/China_Icon.gif"));
	ImageIcon England_Icon=new ImageIcon(getClass().getResource("image/England_Icon.gif"));
	ImageIcon German_Icon=new ImageIcon(getClass().getResource("image/German_Icon.gif"));
	ImageIcon USA_Icon=new ImageIcon(getClass().getResource("image/USA_Icon.gif"));
	ImageIcon Canada_pic=new ImageIcon(getClass().getResource("image/Canada.gif"));
	ImageIcon China_pic=new ImageIcon(getClass().getResource("image/China.gif"));
	ImageIcon England_pic=new ImageIcon(getClass().getResource("image/England.gif"));
	ImageIcon German_pic=new ImageIcon(getClass().getResource("image/German.gif"));
	ImageIcon USA_pic=new ImageIcon(getClass().getResource("image/USA.gif"));
	public A121(){	
		Action canadaI=new MyAction("canada",Canada_Icon,"canada",new Integer(KeyEvent.VK_A),KeyStroke.getKeyStroke(KeyEvent.VK_A,ActionEvent.CTRL_MASK)); //設定名稱、放圖、快捷鍵
		Action chinaI=new MyAction("china",China_Icon,"china",new Integer(KeyEvent.VK_B),KeyStroke.getKeyStroke(KeyEvent.VK_B,ActionEvent.CTRL_MASK));
		Action englandI=new MyAction("england",England_Icon,"England",new Integer(KeyEvent.VK_C),KeyStroke.getKeyStroke(KeyEvent.VK_C,ActionEvent.CTRL_MASK));
		Action germanI=new MyAction("german",German_Icon,"german",new Integer(KeyEvent.VK_D),KeyStroke.getKeyStroke(KeyEvent.VK_D,ActionEvent.CTRL_MASK));
		Action usaI=new MyAction("usa",USA_Icon,"usa",new Integer(KeyEvent.VK_E),KeyStroke.getKeyStroke(KeyEvent.VK_E,ActionEvent.CTRL_MASK));
		Action canadaP=new MyAction("canada",Canada_pic,"canada",new Integer(KeyEvent.VK_A),KeyStroke.getKeyStroke(KeyEvent.VK_A,ActionEvent.CTRL_MASK));
		Action chinaP=new MyAction("china",China_pic,"china",new Integer(KeyEvent.VK_B),KeyStroke.getKeyStroke(KeyEvent.VK_B,ActionEvent.CTRL_MASK));
		Action englandP=new MyAction("england",England_pic,"England",new Integer(KeyEvent.VK_C),KeyStroke.getKeyStroke(KeyEvent.VK_C,ActionEvent.CTRL_MASK));
		Action germanP=new MyAction("german",German_pic,"german",new Integer(KeyEvent.VK_D),KeyStroke.getKeyStroke(KeyEvent.VK_D,ActionEvent.CTRL_MASK));
		Action usaP=new MyAction("usa",USA_pic,"usa",new Integer(KeyEvent.VK_E),KeyStroke.getKeyStroke(KeyEvent.VK_E,ActionEvent.CTRL_MASK));
		
		JMenuBar jmb=new JMenuBar(); //新增MENU
		JMenu jm=new JMenu("MENU");
		setJMenuBar(jmb);
		jmb.add(jm);
		
		jm.add(canadaI);
		jm.add(chinaI);
		jm.add(englandI);
		jm.add(germanI);
		jm.add(usaI);
		
		JToolBar jt=new JToolBar(JToolBar.VERTICAL);
		jt.setBorder(BorderFactory.createLineBorder(Color.RED));
		jt.add(canadaI);
		jt.add(chinaI);
		jt.add(englandI);
		jt.add(germanI);
		jt.add(usaI);
		add(jt,BorderLayout.EAST);
		add(pic,BorderLayout.CENTER);
	}
	 private class MyAction extends AbstractAction {
		    String name;

		    MyAction(String name, Icon icon) {
		      super(name, icon);
		      this.name = name;
		    }

		    MyAction(String name, Icon icon, String desc, Integer mnemonic,KeyStroke accelerator) {
		      super(name, icon);
		      putValue(Action.SHORT_DESCRIPTION, desc);
		      putValue(Action.MNEMONIC_KEY, mnemonic);
		      putValue(Action.ACCELERATOR_KEY, accelerator);
		      this.name = name;
		    }

		    public void actionPerformed(ActionEvent e) { //動作
		      if (name.equals("canada"))
		    	  pic.setIcon(Canada_pic);
		      else if (name.equals("china"))
		    	  pic.setIcon(Canada_pic);
		      else if (name.equals("england"))
		    	  pic.setIcon(England_pic);
		      else if (name.equals("german"))
		    	  pic.setIcon(German_pic);
		      else if (name.equals("usa"))
		    	  pic.setIcon(USA_pic);
		    }
	 }
	 
}
