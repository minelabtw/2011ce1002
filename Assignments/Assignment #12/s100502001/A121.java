package a12.s100502001;

import javax.swing.*;
import javax.swing.plaf.ActionMapUIResource;
import javax.swing.text.Element;
import javax.swing.text.html.ImageView;

import java.awt.*; 
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
public class A121 extends JApplet{
	private ImageIcon[] image={
			new ImageIcon(getClass().getResource("../../image/Canada.gif")), //璶тbin戈Ж
			new ImageIcon(getClass().getResource("../../image/China.gif")),
			new ImageIcon(getClass().getResource("../../image/England.gif")),
			new ImageIcon(getClass().getResource("../../image/German.gif")),
			new ImageIcon(getClass().getResource("../../image/USA.gif"))
	};
	private  ImageIcon[] jbimg={ //陪ボ娩瓜
			new ImageIcon(getClass().getResource("../../image/Canada_Icon.gif")),
			new ImageIcon(getClass().getResource("../../image/China_Icon.gif")),
			new ImageIcon(getClass().getResource("../../image/England_Icon.gif")),
			new ImageIcon(getClass().getResource("../../image/German_Icon.gif")),
			new ImageIcon(getClass().getResource("../../image/USA_Icon.gif"))};
	private String[] str_icon={"Canda","China","England","German","USA"};
	private JLabel jlicon=new JLabel("");
	private JLabel jlWord=new JLabel("");
	public A121(){

		Action img_can=new ImgAction(str_icon[0],jbimg[0],str_icon[0],new Integer(KeyEvent.VK_Z)
			,KeyStroke.getKeyStroke(KeyEvent.VK_Z,ActionEvent.CTRL_MASK));
		Action img_chi=new ImgAction(str_icon[1],jbimg[1],str_icon[1],new Integer(KeyEvent.VK_X)
		,KeyStroke.getKeyStroke(KeyEvent.VK_X,ActionEvent.CTRL_MASK));
		Action img_eng=new ImgAction(str_icon[2],jbimg[2],str_icon[2],new Integer(KeyEvent.VK_C)
		,KeyStroke.getKeyStroke(KeyEvent.VK_C,ActionEvent.CTRL_MASK));
		Action img_ger=new ImgAction(str_icon[3],jbimg[3],str_icon[3],new Integer(KeyEvent.VK_V)
		,KeyStroke.getKeyStroke(KeyEvent.VK_V,ActionEvent.CTRL_MASK));
		Action img_usa=new ImgAction(str_icon[4],jbimg[4],str_icon[4],new Integer(KeyEvent.VK_B)
		,KeyStroke.getKeyStroke(KeyEvent.VK_B,ActionEvent.CTRL_MASK));
		JMenuBar jmb=new JMenuBar();
		JMenu iconMenu=new JMenu("Country Image");
		setJMenuBar(jmb);
		jmb.add(iconMenu);
		iconMenu.add(img_can);
		iconMenu.add(img_chi);
		iconMenu.add(img_eng);
		iconMenu.add(img_ger);
		iconMenu.add(img_usa);
		JToolBar jtoolIcon=new JToolBar(JToolBar.VERTICAL);
		jtoolIcon.setBorder(BorderFactory.createLineBorder(Color.yellow));
		jtoolIcon.add(img_can);
		jtoolIcon.add(img_chi);
		jtoolIcon.add(img_eng);
		jtoolIcon.add(img_ger);
		jtoolIcon.add(img_usa);
		this.add(jmb,BorderLayout.NORTH);
		this.add(jlicon,BorderLayout.CENTER);
		jlWord.setFont(new Font("TimesRoman", Font.BOLD, 48));
		jlWord.setForeground(Color.cyan);
		this.add(jlWord,BorderLayout.SOUTH);
		this.add(jtoolIcon,BorderLayout.EAST);
	}
	class ImgAction extends AbstractAction{
		private String name;
		private Icon icon;

		public ImgAction(String name, Icon icon ,String desc,Integer mnemonic,KeyStroke accelerator){
			super(name,icon);
			putValue(Action.SHORT_DESCRIPTION, desc);
			putValue(Action.MNEMONIC_KEY,mnemonic);
			putValue(Action.ACCELERATOR_KEY, accelerator);
			this.name=name;
			this.icon=icon;
		}
		public void actionPerformed(ActionEvent e){
			if( icon.equals(jbimg[0])){ //equals 琌耞琌ン??or 单
				System.out.println("Canada");
				jlicon.setIcon(image[0]);
				jlWord.setText(str_icon[0]);
			}
			else if(  icon.equals(jbimg[1])){ //equals 琌耞琌ン??or 单
				jlicon.setIcon(image[1]);
				jlWord.setText(str_icon[1]);
			}
			else if(  icon.equals(jbimg[2])){ //equals 琌耞琌ン??or 单
				jlicon.setIcon(image[2]);
				jlWord.setText(str_icon[2]);
			}
			else if(  icon.equals(jbimg[3])){ //equals 琌耞琌ン??or 单
				jlicon.setIcon(image[3]);
				jlWord.setText(str_icon[3]);
			}
			else if(  icon.equals(jbimg[4])){ //equals 琌耞琌ン??or 单
				jlicon.setIcon(image[4]);
				jlWord.setText(str_icon[4]);
			}
			jlicon.revalidate();
		}
		
	}
	
}
