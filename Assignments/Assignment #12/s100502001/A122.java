package a12.s100502001;
import java.applet.*;

import javax.annotation.Resource;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.TimeZone;
public class A122 extends Applet{
	//取得檔案
	private ResourceBundle def_language=ResourceBundle.getBundle("Language/MyResource");
	private ResourceBundle en_language=ResourceBundle.getBundle("Language/MyResource_en");
	private ResourceBundle fr_language=ResourceBundle.getBundle("Language/MyResource_fr");
	private ResourceBundle ja_language=ResourceBundle.getBundle("Language/MyResource_ja");
	private ResourceBundle chi_language=ResourceBundle.getBundle("Language/MyResource_zh");
	private String[] language={"English", "France", "Japanese(Japan)", "Chinese",};
	private String[] time_style={"Etc/GMT+12","time zone"};
	private String[] style={"SHORT","MEDIUM","LONG","FULL"};
	private JComboBox jcbcountry=new JComboBox(language);
	private JComboBox jcblocal_time=new JComboBox();
	private JComboBox jcbstyle_time=new JComboBox(style);
	private JComboBox jcbstyle_date=new JComboBox(style);
	private JTextField jtdata=new JTextField();
	private JTextField jttime=new JTextField();
	private JLabel jltime=new JLabel(def_language.getString("Time"));
	private JLabel jldata=new JLabel(def_language.getString("Date"));
	private JLabel jlcountry=new JLabel(def_language.getString("Choose_locale"));
	private JLabel jllocal=new JLabel(def_language.getString("Time_Zone"));
	private JLabel jlstyle_data=new JLabel(def_language.getString("Date_Style"));
	private JLabel jlstyle_time=new JLabel(def_language.getString("Time_Style"));
	private Timer timer=new Timer(1000, new TimerListener());
	private TimeZone timeZone=TimeZone.getTimeZone("EST"); //時區
	private int timeformat=DateFormat.LONG; //default
	private int dataformat=DateFormat.LONG;//default
	private Locale[] availableLocals=Locale.getAvailableLocales();
	private String[] availableTimeZone=TimeZone.getAvailableIDs();
	public A122(){
		JPanel jpshow=new JPanel();
		jpshow.setLayout(new GridLayout(1,4,5,5));
		jpshow.add(jldata);
		jpshow.add(jtdata);
		jpshow.add(jltime);
		jpshow.add(jttime);
		JPanel jpset=new JPanel();
		jpset.setLayout(new GridLayout(2,8,10,10));
		jpset.add(jlcountry);
		jpset.add(jcbcountry);
		jpset.add(jllocal);
		jpset.add(jcblocal_time);
		jpset.add(jlstyle_data);
		jpset.add(jcbstyle_date);
		jpset.add(jlstyle_time);
		jpset.add(jcbstyle_time);
		jtdata.setEditable(false);
		jttime.setEditable(false);
		this.add(jpshow,BorderLayout.CENTER);
		this.add(jpset,BorderLayout.SOUTH);
		timer.start();
		jcbcountry.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(jcbcountry.getSelectedIndex()==0)
					setStyle(en_language);
				else if(jcbcountry.getSelectedIndex()==1)
					setStyle(fr_language);
				else if(jcbcountry.getSelectedIndex()==2)
					setStyle(ja_language);
				else if(jcbcountry.getSelectedIndex()==3)
					setStyle(chi_language);
			}
		}); 
		setAvailableLocal();
		setAvailbleTimeZones();
		//時區
		jcblocal_time.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				timeZone=TimeZone.getTimeZone(availableTimeZone[jcblocal_time.getSelectedIndex()]);//選到哪個
			}
		}); 
		jcbstyle_time.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(jcbstyle_time.getSelectedIndex()==0)
					timeformat=DateFormat.SHORT;
				else if(jcbstyle_time.getSelectedIndex()==1)
					timeformat=DateFormat.MEDIUM;
				else if(jcbstyle_time.getSelectedIndex()==2)
					timeformat=DateFormat.LONG;
				else if(jcbstyle_time.getSelectedIndex()==3)
					timeformat=DateFormat.FULL;
			}
		}); 
		jcbstyle_date.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(jcbstyle_date.getSelectedIndex()==0)
					dataformat=DateFormat.SHORT;
				else if(jcbstyle_date.getSelectedIndex()==1)
					dataformat=DateFormat.MEDIUM;
				else if(jcbstyle_date.getSelectedIndex()==2)
					dataformat=DateFormat.LONG;
				else if(jcbstyle_date.getSelectedIndex()==3)
					dataformat=DateFormat.FULL;
				
			}
		}); 
		
	}
	public void setAvailableLocal(){
		for(int i=0;i<availableLocals.length;i++)
			jcblocal_time.addItem(availableLocals[i].getDisplayName()+" "+availableLocals[i].toString());
	}
	public void setAvailbleTimeZones(){
		for(int i=0;i<availableTimeZone.length;i++)
			jcblocal_time.addItem(availableTimeZone[i]);
	}
	public void setStyle(ResourceBundle temp){ //各標題語言
		jltime.setText(temp.getString("Time"));
		jldata.setText(temp.getString("Date"));
		jlcountry.setText(temp.getString("Choose_locale"));
		jllocal.setText(temp.getString("Time_Zone"));
		jlstyle_data.setText(temp.getString("Date_Style"));
		jlstyle_time.setText(temp.getString("Time_Style"));
	}
	class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			Calendar calander=new GregorianCalendar(timeZone, getLocale());//取得現在時間 
			DateFormat formatter_time=DateFormat.getTimeInstance(timeformat,getLocale()); //轉換成我們看得懂得格式 //getLocale()取得現在語系
			formatter_time.setTimeZone(timeZone);
			DateFormat formatter_data=DateFormat.getDateInstance(dataformat,getLocale()); //轉換成我們看得懂得格式 //getLocale()取得現在語系
			formatter_data.setTimeZone(timeZone);
			jttime.setText(formatter_time.format(calander.getTime())); //calander 是取得時間
			jtdata.setText(formatter_data.format(calander.getTime())); //format是轉換顯示的格是

			repaint();
		}
		
	}
	
	
}
