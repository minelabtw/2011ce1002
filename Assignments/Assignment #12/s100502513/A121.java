package a12.s100502513;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class A121 extends JApplet implements ActionListener {
	private JMenuItem exitItem,canadaItem,chinaItem,englandItem,germanItem,usaItem ;
	private JLabel flagpic = new JLabel(new ImageIcon(getClass().getResource("image/Canada.GIF")));  //國旗大圖片
	private JButton canadaicon = new JButton(new ImageIcon(getClass().getResource("image/Canada_Icon.GIF")));  //toobar小圖示
	private JButton chinaicon = new JButton(new ImageIcon(getClass().getResource("image/China_Icon.GIF")));
	private JButton englandicon = new JButton(new ImageIcon(getClass().getResource("image/England_Icon.GIF")));
	private JButton germanicon = new JButton(new ImageIcon(getClass().getResource("image/German_Icon.GIF")));
	private JButton usaicon = new JButton(new ImageIcon(getClass().getResource("image/USA_Icon.GIF")));

	public A121() {
		
		JMenuBar mBar = new JMenuBar();
				JMenu countryme = new JMenu("Country");
		mBar.add(countryme);
		countryme.add(canadaItem = new JMenuItem("Canada"));  //MENU中的國家選項
		countryme.add(chinaItem = new JMenuItem("China"));
		countryme.add(englandItem = new JMenuItem("England"));
		countryme.add(germanItem = new JMenuItem("German"));
		countryme.add(usaItem = new JMenuItem("USA"));
		setJMenuBar(mBar);

		JToolBar tBar = new JToolBar();
		tBar.setOrientation(JToolBar.VERTICAL);  //設成垂直排列
		tBar.setFloatable(false);
		tBar.add(canadaicon);
		tBar.add(chinaicon);
		tBar.add(englandicon);
		tBar.add(germanicon);
		tBar.add(usaicon);
		
		canadaicon.setToolTipText("Canada");  //設定提示字
		chinaicon.setToolTipText("China");
		englandicon.setToolTipText("England");
		germanicon.setToolTipText("German");
		usaicon.setToolTipText("USA");
		
		canadaicon.setBorderPainted(false);  //不產生邊框
		chinaicon.setBorderPainted(false);
		englandicon.setBorderPainted(false);
		germanicon.setBorderPainted(false);
		usaicon.setBorderPainted(false);
		
		add(tBar, BorderLayout.EAST);
		add(flagpic,BorderLayout.CENTER);

		canadaicon.addActionListener(this);
		chinaicon.addActionListener(this);
		englandicon.addActionListener(this);
		germanicon.addActionListener(this);
		usaicon.addActionListener(this);
		canadaItem.addActionListener(this);
		chinaItem.addActionListener(this);
		englandItem.addActionListener(this);
		germanItem.addActionListener(this);
		usaItem.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == canadaicon) {
			flagpic.setIcon(new ImageIcon(getClass().getResource("image/Canada.gif")));  //變換成特定國家國旗圖片
		} 
		else if (e.getSource() == chinaicon) {
			flagpic.setIcon(new ImageIcon(getClass().getResource("image/China.GIF")));
		} 
		else if (e.getSource() == englandicon) {
			flagpic.setIcon(new ImageIcon(getClass().getResource("image/England.gif")));
		} 
		else if (e.getSource() == germanicon) {
			flagpic.setIcon(new ImageIcon(getClass().getResource("image/German.gif")));
		} 
		else if (e.getSource() == usaicon) {
			flagpic.setIcon(new ImageIcon(getClass().getResource("image/USA.gif")));
		}
		else if (e.getSource() == canadaItem) {
			flagpic.setIcon(new ImageIcon(getClass().getResource("image/Canada.gif")));
		}
		else if (e.getSource() == chinaItem) {
			flagpic.setIcon(new ImageIcon(getClass().getResource("image/China.gif")));
		}
		else if (e.getSource() == englandItem) {
			flagpic.setIcon(new ImageIcon(getClass().getResource("image/England.gif")));
		}
		else if (e.getSource() == germanItem) {
			flagpic.setIcon(new ImageIcon(getClass().getResource("image/German.gif")));
		}
		else if (e.getSource() == usaItem) {
			flagpic.setIcon(new ImageIcon(getClass().getResource("image/USA.gif")));
		}
	}
}
