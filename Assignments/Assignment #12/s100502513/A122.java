package a12.s100502513;

import java.awt.*;
import java.awt.event.*;
import java.beans.IntrospectionException;
import java.text.*;
import java.util.*;

import javax.swing.*;
import javax.swing.Timer;

public class A122 extends JApplet {
	private String[] countrys = { "中文(台灣)", "日文", "法文", "英文" };  //COMBOX裡的文字
	private String[] styles = { "FULL", "LONG", "MEDIUM", "SHORT" };
	private String[] timezones = TimeZone.getAvailableIDs();  //時區
	private String time;
	private String date;
	private int timestyle = 0;
	private int datestyle = 0;
	private JComboBox countrycb = new JComboBox(countrys);
	private JComboBox gmtcb = new JComboBox(timezones);
	private JComboBox datestylecb = new JComboBox(styles);
	private JComboBox timestylecb = new JComboBox(styles);
	private ResourceBundle res = ResourceBundle.getBundle("Language/MyResource",getLocale());  //初始語言
	private ResourceBundle[] ress = new ResourceBundle[4];
	private JLabel datela = new JLabel(res.getString("Date"));
	private JLabel timela = new JLabel(res.getString("Time"));
	private JLabel countryla = new JLabel(res.getString("Choose_locale"));
	private JLabel gmtla = new JLabel(res.getString("Time_Zone"));
	private JLabel datestylela = new JLabel(res.getString("Date_Style"));
	private JLabel timestylela = new JLabel(res.getString("Time_Style"));
	private JTextField datetf = new JTextField();
	private JTextField timetf = new JTextField();
	private TimeZone timeZone = TimeZone.getDefault();  //初始時區
	private Timer timer = new Timer(1000, new TimerListener());
	private Locale locale =Locale.getDefault();
	private Locale[] locales ={Locale.CHINESE,Locale.JAPAN,Locale.FRENCH,Locale.ENGLISH};
	
	public A122() {		
		JPanel[] p = new JPanel[9];
		for (int i = 0; i < 9; i++) {
			p[i] = new JPanel();
		}
		setLayout(new GridLayout(3,4));
		add(datela, BorderLayout.WEST);
		add(datetf, BorderLayout.CENTER);
		add(timela, BorderLayout.WEST);
		add(timetf, BorderLayout.CENTER);
		add(countryla, BorderLayout.WEST);
		add(countrycb, BorderLayout.CENTER);
		add(gmtla, BorderLayout.WEST);
		add(gmtcb, BorderLayout.CENTER);
		add(datestylela, BorderLayout.WEST);
		add(datestylecb, BorderLayout.CENTER);
		add(timestylela, BorderLayout.WEST);
		add(timestylecb, BorderLayout.CENTER);
		timer.start();
		
		datela.setSize(1000, 100);
		timela.setSize(1000, 100);
		
		ress[0] = ResourceBundle.getBundle("Language/MyResource_zh");  //設定語言
		ress[1] = ResourceBundle.getBundle("Language/MyResource_ja");
		ress[2] = ResourceBundle.getBundle("Language/MyResource_fr");
		ress[3] = ResourceBundle.getBundle("Language/MyResource_en");
				
		countrycb.addItemListener(new ItemListener() {  
			public void itemStateChanged(ItemEvent e) {
				res=ress[countrycb.getSelectedIndex()];  //選擇語言
				setLocale(locales[countrycb.getSelectedIndex()]);  //設定地方語言及時間
				updatestring();
			}
		});
		gmtcb.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
			  timeZone=TimeZone.getTimeZone(timezones[gmtcb.getSelectedIndex()]);//選擇時區
			}	
		});	
		datestylecb.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				datestyle=datestylecb.getSelectedIndex();  //選擇日期風格
			}
		});
		timestylecb.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				timestyle=timestylecb.getSelectedIndex();  //選擇時間風格
			}
		});
	}

	public void updatestring(){  //更新語言文字的語系
		datela.setText(res.getString("Date"));
		timela.setText(res.getString("Time"));
		countryla.setText(res.getString("Choose_locale"));
		gmtla.setText(res.getString("Time_Zone"));
		datestylela.setText(res.getString("Date_Style"));
		timestylela.setText(res.getString("Time_Style"));
		repaint();
	}
	
	private class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Calendar calendar = new GregorianCalendar(timeZone, getLocale());
			DateFormat dfd = DateFormat.getDateInstance(datestyle, getLocale());
			DateFormat dft = DateFormat.getTimeInstance(timestyle, getLocale());
			dfd.setTimeZone(timeZone);  //設定日期的時區
			dft.setTimeZone(timeZone);  //設定時間的時區
			date = dfd.format(calendar.getTime());  //設定日期
			datetf.setText(date);
			time = dft.format(calendar.getTime());  //設定時間
			timetf.setText(time);
		}
	}

}
