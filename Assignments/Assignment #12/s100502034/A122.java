package a12.s100502034;
import java.text.DateFormat;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.TimeZone;
import javax.swing.JApplet;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;
import java.util.Date;
public class A122 extends JApplet{
	private JTextField dateT = new JTextField();//日期text
	private JTextField timeT = new JTextField();//時間text
	private JLabel dateL = new JLabel();//日期label
	private JLabel timeL = new JLabel();//時間label
	private JLabel languageL = new JLabel();//語系label
	private JLabel whereTimeL = new JLabel();//時區label
	private JLabel dateStyleL = new JLabel();//日期風格label
	private JLabel timeStyleL = new JLabel();//時間風格label
	private JComboBox countryChoose = new JComboBox();//語系
	private JComboBox whereTimeChoose = new JComboBox();//時區
	private JComboBox dateStyleChoose = new JComboBox();//時期風格
	private JComboBox timeStyleChoose = new JComboBox();//時間風格
	private JPanel[] layout = new JPanel[5];//排版
	private Date date = new Date();//那個new Date()已經有取得當埘時間的功能
	private DateFormat shortFormatTime = DateFormat.getTimeInstance(DateFormat.SHORT);//時間Format
    private DateFormat mediumFormatTime = DateFormat.getTimeInstance(DateFormat.MEDIUM); 
	private DateFormat longFormatTime = DateFormat.getTimeInstance(DateFormat.LONG); 
	private DateFormat fullFormatTime = DateFormat.getTimeInstance(DateFormat.FULL); 
	private DateFormat shortFormatDate = DateFormat.getDateInstance(DateFormat.SHORT);//時間Format
	private DateFormat mediumFormatDate = DateFormat.getDateInstance(DateFormat.MEDIUM);
	private DateFormat longFormatDate = DateFormat.getDateInstance(DateFormat.LONG);
	private DateFormat fullFormatDate = DateFormat.getDateInstance(DateFormat.FULL);
	private Locale locale = Locale.getDefault();
	private ResourceBundle res = ResourceBundle.getBundle("MyResource");//助教給的MyResource
	private Locale locales[] = {Locale.ENGLISH, Locale.JAPAN, Locale.CHINESE,Locale.FRANCE };//如果用Calendar.getAvailableLocales();會太多東西吧....?
	private TimeZone tz = TimeZone.getTimeZone("CST");
	private String[] timeZoneName = TimeZone.getAvailableIDs();
	public void initializeComboBox() {//
		for (int i = 0; i < locales.length; i++)
			countryChoose.addItem(locales[i].getDisplayName());
		for(int i =0;i<timeZoneName.length;i++){
			whereTimeChoose.addItem(timeZoneName[i]);
		}
	}
	class changeTime implements ActionListener {//所以combobox的item都加進來
		public void actionPerformed(ActionEvent e) {
			date = new Date();
			if(timeStyleChoose.getSelectedIndex() == 1)
				timeT.setText(shortFormatTime.format(date));
			else if(timeStyleChoose.getSelectedIndex() == 2)
				timeT.setText(mediumFormatTime.format(date));
			else if(timeStyleChoose.getSelectedIndex() == 3)
				timeT.setText(longFormatTime.format(date));
			else if(timeStyleChoose.getSelectedIndex() == 4)
				timeT.setText(fullFormatTime.format(date));
			timeStyleChoose.addItem("time style choose");
			timeStyleChoose.addItem("shortFormat");
			timeStyleChoose.addItem("mediumFormat");
			timeStyleChoose.addItem("longFormat");
			timeStyleChoose.addItem("fullFormat");
			dateStyleChoose.addItem("time style choose");
			dateStyleChoose.addItem("shortFormat");
			dateStyleChoose.addItem("mediumFormat");
			dateStyleChoose.addItem("longFormat");
			dateStyleChoose.addItem("fullFormat");
		}	
	}
	public A122(){
		initializeComboBox();
		Timer timer = new Timer(1000, new changeTime());//顯示當前時間就用1000啦
		timer.start();
		for(int i = 0;i<layout.length;i++){
			layout[i] = new JPanel();//袑始化
		}
		setLayout(new GridLayout(3,1));
		layout[0].setLayout(new BorderLayout());
		layout[0].add(dateL,BorderLayout.WEST);
		layout[0].add(dateT,BorderLayout.CENTER);
		layout[1].setLayout(new BorderLayout());
		layout[1].add(timeL,BorderLayout.WEST);
		layout[1].add(timeT,BorderLayout.CENTER);
		layout[2].setLayout(new GridLayout(1,2));
		layout[2].add(layout[0]);
		layout[2].add(layout[1]);
		layout[3].add(languageL);
		layout[3].add(countryChoose);
		layout[3].add(whereTimeL);
		layout[3].add(whereTimeChoose);
		layout[4].add(dateStyleL);
		layout[4].add(dateStyleChoose);
		layout[4].add(timeStyleL);
		layout[4].add(timeStyleChoose);
		add(layout[2]);
		add(layout[3]);
		add(layout[4]);
		dateStyleChoose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				date = new Date();//因為日期沒有在timer裡面改 反正日期也不會常常跳啦~ 就在combobox改變的時候改吧
				if(dateStyleChoose.getSelectedIndex() == 1)
					dateT.setText(shortFormatDate.format(date));
				else if(dateStyleChoose.getSelectedIndex() == 2)
					dateT.setText(mediumFormatDate.format(date));
				else if(dateStyleChoose.getSelectedIndex() == 3)
					dateT.setText(longFormatDate.format(date));
				else if(dateStyleChoose.getSelectedIndex() == 4)
					dateT.setText(fullFormatDate.format(date));
			}
		});
		countryChoose.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {//選擇語系啦
				locale = locales[countryChoose.getSelectedIndex()];
				res = ResourceBundle.getBundle("MyResource", locale);
				dateL.setText(res.getString("Date"));//那些檔案裡面的東東
				timeL.setText(res.getString("Time"));
				languageL.setText(res.getString("Choose_locale"));
				whereTimeL.setText(res.getString("Time_Zone"));
				dateStyleL.setText(res.getString("Date_Style"));
				timeStyleL.setText(res.getString("Time_Style"));
				repaint();
			}
		});
		whereTimeChoose.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {//得到時區
				tz = TimeZone.getTimeZone(timeZoneName[whereTimeChoose.getSelectedIndex()]);
				shortFormatTime.setTimeZone(tz);
				mediumFormatTime.setTimeZone(tz);
				longFormatTime.setTimeZone(tz);
				fullFormatTime.setTimeZone(tz);
			}
		});
	}
	public static void main(String arg[]){
		A122 a = new A122();
		/*a.init();
		a.start();*/
	}
}
