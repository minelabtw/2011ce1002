package a12.s100502034;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JToolBar;
public class A121 extends JApplet{
	private JLabel pictureI = new JLabel();//放picture的JLabel
	public A121(){
		ImageIcon caradaImageIcon = new ImageIcon(getClass().getResource("image/Canada_Icon.gif"));//ICON
		ImageIcon chinaImageIcon = new ImageIcon(getClass().getResource("image/China_Icon.gif"));
		ImageIcon engImageIcon =new ImageIcon(getClass().getResource("image/England_Icon.gif"));
		ImageIcon germanImageIcon = new ImageIcon(getClass().getResource("image/German_Icon.gif"));
		ImageIcon usaImageIcon = new ImageIcon(getClass().getResource("image/USA_Icon.gif"));
		JMenuBar jmb = new JMenuBar();
		setJMenuBar(jmb);
	    JMenu pictureM = new JMenu("picture");
	    jmb.add(pictureM);
		add(pictureI);	
		Action canadaA = new ActionP("Canada", caradaImageIcon);//各個國家的action
		Action chinaA = new ActionP("China",chinaImageIcon);
		Action engA = new ActionP("England",engImageIcon);
		Action germanA = new ActionP("German",germanImageIcon);
		Action usaA = new ActionP("USA",usaImageIcon);
		pictureM.add(canadaA);//把各action add 進menu裡
		pictureM.add(chinaA);
		pictureM.add(engA);
		pictureM.add(germanA);
		pictureM.add(usaA);
		JToolBar pictureMTool = new JToolBar(JToolBar.VERTICAL);
		pictureMTool.setBorder(null);//把各個action add 進ToolBar裡
		pictureMTool.add(canadaA);
		pictureMTool.add(chinaA);
		pictureMTool.add(engA);
		pictureMTool.add(germanA);
		pictureMTool.add(usaA);
		add(pictureMTool, BorderLayout.EAST);
		add(pictureI,BorderLayout.CENTER);
	}
	class ActionP extends AbstractAction{
		private String country;//國家名
		public ActionP(String name,Icon icon){
			super(name,icon);
			country = name;//得到action是哪個國家
		}
		public void actionPerformed(ActionEvent e) {
			if (country =="Canada")//改變圖片
				pictureI.setIcon(new ImageIcon(getClass().getResource("image/Canada.gif")));
		    else if (country =="China")
		    	pictureI.setIcon(new ImageIcon(getClass().getResource("image/China.gif")));
		    else if (country =="England")
		    	pictureI.setIcon(new ImageIcon(getClass().getResource("image/England.gif")));
		    else if (country.equals("German"))
		    	pictureI.setIcon(new ImageIcon(getClass().getResource("image/German.gif")));
		    else if (country =="USA")
		    	pictureI.setIcon(new ImageIcon(getClass().getResource("image/USA.gif")));
		}
	}
	public static void main(String arg[]){
		A121 a121 = new A121();
		/*a121.init();其實這個不太懂啦
		a121.start();雖然書有 但是沒加好像也沒差*/
	}
}
