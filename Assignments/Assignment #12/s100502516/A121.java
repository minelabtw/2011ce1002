package a12.s100502516;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class A121 extends JApplet {
	private JMenuBar jmb = new JMenuBar();
	private JMenu menu = new JMenu("Menu");
	private Action canadaAction, chinaAction, englandAction, germanAction, usaAction;
	private ImageViewer imageViewer = new ImageViewer();
	private JButton jbtCanada = new JButton(new ImageIcon("../image/Canada_Icon.gif"));
	private JButton jbtChina = new JButton(new ImageIcon("../image/China_Icon.gif"));
	private JButton jbtEngland = new JButton(new ImageIcon("../image/England_Icon.gif"));
	private JButton jbtGerman = new JButton(new ImageIcon("../image/German_Icon.gif"));
	private JButton jbtUSA = new JButton(new ImageIcon("../image/USA_Icon.gif"));
	private JToolBar jtb = new JToolBar("flag");
	
	public void init()
	{				
		menu.add(canadaAction = new MyAction("Canada", new ImageIcon("../image/Canada_Icon.gif"), "Canada flag", 
				new Integer(KeyEvent.VK_A), KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.CTRL_MASK)));
		menu.add(chinaAction = new MyAction("China", new ImageIcon("../image/China_Icon.gif"), "China flag", 
				new Integer(KeyEvent.VK_S), KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK)));
		menu.add(englandAction = new MyAction("England", new ImageIcon("../image/England_Icon.gif"), "England flag", 
				new Integer(KeyEvent.VK_D), KeyStroke.getKeyStroke(KeyEvent.VK_D, ActionEvent.CTRL_MASK)));
		menu.add(germanAction = new MyAction("German", new ImageIcon("../image/German_Icon.gif"), "German flag", 
				new Integer(KeyEvent.VK_F), KeyStroke.getKeyStroke(KeyEvent.VK_F, ActionEvent.CTRL_MASK)));
		menu.add(usaAction = new MyAction("USA", new ImageIcon("../image/USA_Icon.gif"), "USA flag", 
				new Integer(KeyEvent.VK_G), KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.CTRL_MASK)));
					
		jbtCanada.addActionListener(canadaAction);
		jbtCanada.setBorderPainted(false);
		jbtChina.addActionListener(chinaAction);
		jbtChina.setBorderPainted(false);
		jbtEngland.addActionListener(englandAction);
		jbtEngland.setBorderPainted(false);
		jbtGerman.addActionListener(germanAction);
		jbtGerman.setBorderPainted(false);
		jbtUSA.addActionListener(usaAction);
		jbtUSA.setBorderPainted(false);		
				
		jtb.setFloatable(true);
		jtb.add(jbtCanada);		
		jtb.add(jbtChina);		
		jtb.add(jbtEngland);		
		jtb.add(jbtGerman);		
		jtb.add(jbtUSA);		
				
		imageViewer.setLocation(0, 50);
		this.add(imageViewer);				
		
		jmb.add(menu);
		this.setJMenuBar(jmb);		
		this.add(jtb, BorderLayout.NORTH);		
	}	
	
	private class MyAction extends AbstractAction
	{	
		private String name;
		
		public MyAction(String name, Icon icon)
		{
			super(name, icon);
			this.name = name;
		}
		MyAction(String name, Icon icon, String desc, Integer mnemonic, KeyStroke accelerator)
		{
			super(name, icon);
			putValue(Action.SHORT_DESCRIPTION, desc);
			putValue(Action.MNEMONIC_KEY, mnemonic);
			putValue(Action.ACCELERATOR_KEY, accelerator);
			this.name = name;
		}
		
		public void actionPerformed(ActionEvent e) 
		{
			if(name.equals("Canada"))
			{
				imageViewer.setImage(new ImageIcon("../image/Canada.gif").getImage());
			}
			else if(name.equals("China"))
			{
				imageViewer.setImage(new ImageIcon("../image/China.gif").getImage());
			}
			else if(name.equals("England"))
			{
				imageViewer.setImage(new ImageIcon("../image/England.gif").getImage());
			}
			else if(name.equals("German"))
			{
				imageViewer.setImage(new ImageIcon("../image/German.gif").getImage());
			}
			else if(name.equals("USA"))
			{
				imageViewer.setImage(new ImageIcon("../image/USA.gif").getImage());
			}
		}		
	}
	private class ImageViewer extends JPanel
	{
		private Image image;
		private boolean stretched = true;		
		
		public ImageViewer()
		{
			setSize(450, 450);
		}
		public ImageViewer(Image image)
		{
			this.image = image;			
			setSize(450, 450);
		}		
		protected void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			
			if(image != null)
				if(isStretched())
					g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		}
		public Image getImage()
		{
			return image;
		}
		public void setImage(Image image)
		{
			this.image = image;			
			repaint();
		}
		public boolean isStretched()
		{
			return stretched;
		}
		public void setStretched(boolean stretched)
		{
			this.stretched = stretched;
			repaint();
		}
	}
}
