package a12.s100502516;

import javax.swing.*;
import javax.swing.Timer;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.*;
import java.util.*;

public class A122 extends JApplet {
	private TextField dateText = new TextField(20);
	private TextField timeText = new TextField(20);
	private JLabel jlelDate = new JLabel("日期");
	private JLabel jlelTime = new JLabel("時間");
	private JLabel jlelCountry = new JLabel("國家");
	private JLabel jlelTimeZone = new JLabel("時區");
	private JLabel jlelDateStyle = new JLabel("日期風格");
	private JLabel jlelTimeStyle = new JLabel("時間風格");
	private String[] countryType = {"中文", "日文", "英文", "法文"};
	private String[] timeZoneType = {"GMT+08:00", "GMT+09:00", "GMT", "GMT+01:00"};
	private String[] styleType = {"short", "medium", "long", "full"};	
	private JComboBox jcboCountry = new JComboBox(countryType);
	private JComboBox jcboTimeZone = new JComboBox(timeZoneType);
	private JComboBox jcboDateStyle = new JComboBox(styleType);
	private JComboBox jcboTimeStyle = new JComboBox(styleType);	
	private int timeZoneIndex = 0;
	private int dateStyleIndex = DateFormat.SHORT;
	private int timeStyleIndex = DateFormat.SHORT;	
	private Locale locale = new Locale("zh", "TW");
	private Properties props = new Properties();
	private Timer timer = new Timer(1000, new ActionListener()
	{
		public void actionPerformed(ActionEvent e) 
		{
			setTime();
		}		
	});
	
	public void init()
	{
		this.setLayout(new GridLayout(3, 1));
		
		JPanel p1 = new JPanel(new FlowLayout());
		p1.add(jlelDate);
		p1.add(dateText);
		p1.add(jlelTime);
		p1.add(timeText);
		this.add(p1);
		
		JPanel p2 = new JPanel(new FlowLayout());
		p2.add(jlelCountry);
		p2.add(jcboCountry);
		p2.add(jlelTimeZone);
		p2.add(jcboTimeZone);
		this.add(p2);
		
		JPanel p3 = new JPanel(new FlowLayout());
		p3.add(jlelDateStyle);
		p3.add(jcboDateStyle);
		p3.add(jlelTimeStyle);
		p3.add(jcboTimeStyle);
		this.add(p3);
						
		setTime();
		
		jcboCountry.addItemListener(new ComboBoxListener());
		jcboTimeZone.addItemListener(new ComboBoxListener());
		jcboDateStyle.addItemListener(new ComboBoxListener());
		jcboTimeStyle.addItemListener(new ComboBoxListener());		
		
		timer.start();
	}	
	public void setTime()
	{
		Date date = new Date();
		DateFormat format = DateFormat.getDateInstance(dateStyleIndex, locale);
		format.setTimeZone(TimeZone.getTimeZone(timeZoneType[timeZoneIndex]));
		dateText.setText(format.format(date));
		format = DateFormat.getTimeInstance(timeStyleIndex, locale);
		format.setTimeZone(TimeZone.getTimeZone(timeZoneType[timeZoneIndex]));
		timeText.setText(format.format(date));		
	}
	public void setLocale(int index)
	{
		switch(index)
		{
			case 0:
				loadProperties("MyResource_zh");
				locale = new Locale("zh", "TW");
				break;
			case 1:
				loadProperties("MyResource_ja");
				locale = new Locale("ja", "JP");
				break;
			case 2:
				loadProperties("MyResource_en");
				locale = new Locale("en", "GB");
				break;
			case 3:
				loadProperties("MyResource_fr");
				locale = new Locale("fr", "FR");
				break;
			default:
				loadProperties("MyResource");
				locale = new Locale("en", "GB");
				break;
		}
		
		jlelDate.setText(props.getProperty("Date"));
		jlelTime.setText(props.getProperty("Time"));
		jlelCountry.setText(props.getProperty("Choose_locale"));
		jlelTimeZone.setText(props.getProperty("Time_Zone"));
		jlelDateStyle.setText(props.getProperty("Date_Style"));
		jlelTimeStyle.setText(props.getProperty("Time_Style"));
		
		setTime();
	}
	public void setTimeZone(int index)
	{		
		timeZoneIndex = index;		
		setTime();
	}
	public int getStyle(int index)
	{
		switch(index)
		{
			case 0:
				return DateFormat.SHORT;				
			case 1:
				return DateFormat.MEDIUM;				
			case 2:
				return DateFormat.LONG;				
			case 3:
				return DateFormat.FULL;
			default:
				return DateFormat.SHORT;
		}
	}
	public void setDateStyle(int index)
	{
		dateStyleIndex = getStyle(index);
		setTime();
	}
	public void setTimeStyle(int index)
	{
		timeStyleIndex = getStyle(index);
		setTime();
	}
	
	private void loadProperties(String file)
	{		
		try
		{
			props.load(new FileInputStream(file + ".properties"));
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private class ComboBoxListener implements ItemListener
	{
		public void itemStateChanged(ItemEvent e) 
		{
			if(e.getSource() == jcboCountry)
			{
				setLocale(jcboCountry.getSelectedIndex());
			}
			else if(e.getSource() == jcboTimeZone)
			{
				setTimeZone(jcboTimeZone.getSelectedIndex());
			}
			else if(e.getSource() == jcboDateStyle)
			{
				setDateStyle(jcboDateStyle.getSelectedIndex());
			}
			else if(e.getSource() == jcboTimeStyle)
			{
				setTimeStyle(jcboTimeStyle.getSelectedIndex());
			}
		}		
	}
}
