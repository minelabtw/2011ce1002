package a12.s100502025;

import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.text.*;

public class WorldClockControl extends JPanel {
	private Locale[] availableLocales = new Locale[4];
	private String[] availableTimeZones = TimeZone.getAvailableIDs();
	private TimeZone timeZone = TimeZone.getTimeZone("EST");
	private Timer timer = new Timer(1000 , new TimerListener() );
	
	private JComboBox jcbLocales = new JComboBox();
	private JComboBox jcbTimeZones = new JComboBox();
	private JComboBox jcbDateStyle = new JComboBox();
	private JComboBox jcbTimeStyle = new JComboBox();
	
	private JPanel p1 = new JPanel();
	private JPanel p2 = new JPanel();		
	private JPanel p3 = new JPanel();		
	private JPanel p4 = new JPanel();
	private JPanel p5 = new JPanel();		
	private JPanel p6 = new JPanel();
	
	private JLabel jlb1 = new JLabel("日期");
	private JLabel jlb2 = new JLabel("時間");
	private JLabel jlb3 = new JLabel("選擇國家");
	private JLabel jlb4 = new JLabel("時區");
	private JLabel jlb5 = new JLabel("日期風格");
	private JLabel jlb6 = new JLabel("時間風格");
	
	private JTextField jtf1 = new JTextField();
	private JTextField jtf2 = new JTextField();
	
	public WorldClockControl() {
		availableLocales[0] = Locale.CHINESE;
		availableLocales[1] = Locale.ENGLISH;
		availableLocales[2] = Locale.JAPAN;
		availableLocales[3] = Locale.FRANCE;
		
		setAvailableLocales();	
		setAvailableTimeZones();
		
		p1.setLayout(new BorderLayout());
		p1.add(jlb1 , BorderLayout.WEST);
		p1.add(jtf1 , BorderLayout.CENTER);
		
		p2.setLayout(new BorderLayout());
		p2.add(jlb2 , BorderLayout.WEST);
		p2.add(jtf2 , BorderLayout.CENTER);
		
		p3.setLayout(new BorderLayout());
		p3.add(jlb3 , BorderLayout.WEST);
		p3.add(jcbLocales , BorderLayout.CENTER);
		
		p4.setLayout(new BorderLayout());
		p4.add(jlb4 , BorderLayout.WEST);
		p4.add(jcbTimeZones , BorderLayout.CENTER);
		
		p5.setLayout(new BorderLayout());
		p5.add(jlb5 , BorderLayout.WEST);
		p5.add(jcbDateStyle , BorderLayout.CENTER);
		
		p6.setLayout(new BorderLayout());
		p6.add(jlb6 , BorderLayout.WEST);
		p6.add(jcbTimeStyle , BorderLayout.CENTER);
		
		setLayout(new GridLayout(3,2));
		add(p1);
		add(p2);
		add(p3);
		add(p4);
		add(p5);
		add(p6);
		
	}
	
	private void setAvailableLocales() {
		for(int i = 0 ; i < availableLocales.length ; i++ ) {
			jcbLocales.addItem(availableLocales[i].getDisplayName() + " " + availableLocales[i].toString());
		}
	}
	
	private void setAvailableTimeZones() {
		Arrays.sort(availableTimeZones);
		for(int i = 0 ; i < availableTimeZones.length ; i++ ) {
			jcbTimeZones.addItem(availableTimeZones[i]);
		}
	}
	
	private class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Calendar calendar = new GregorianCalendar(timeZone , getLocale());
			
			DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.MEDIUM , DateFormat.LONG , getLocale());
			formatter.setTimeZone(timeZone);
			jtf1.setText(formatter.format(calendar.getTime()));
		}
	}
}
