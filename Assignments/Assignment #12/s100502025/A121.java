package a12.s100502025;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class A121 extends JApplet {
	private JPanel Panel = new JPanel();
	private FlowLayout flowLayout = new FlowLayout();
	private ImageIcon Canada = new ImageIcon(getClass().getResource("image/Canada.gif"));
	private ImageIcon China = new ImageIcon(getClass().getResource("image/China.gif"));
	private ImageIcon England = new ImageIcon(getClass().getResource("image/England.gif"));
	private ImageIcon German = new ImageIcon(getClass().getResource("image/German.gif"));
	private ImageIcon USA = new ImageIcon(getClass().getResource("image/USA.gif"));
	private ImageIcon Canada_Icon = new ImageIcon(getClass().getResource("image/Canada_Icon.gif"));
	private ImageIcon China_Icon = new ImageIcon(getClass().getResource("image/China_Icon.gif"));
	private ImageIcon England_Icon = new ImageIcon(getClass().getResource("image/England_Icon.gif"));
	private ImageIcon German_Icon = new ImageIcon(getClass().getResource("image/German_Icon.gif"));
	private ImageIcon USA_Icon = new ImageIcon(getClass().getResource("image/USA_Icon.gif"));
	
	
	public A121() {
		Action CanadaAction = new MyAction( "Canada" , Canada_Icon );
		Action ChinaAction = new MyAction( "Chaina" , China_Icon );
		Action EnglandAction = new MyAction( "England" , England_Icon );
		Action GermanAction = new MyAction( "German" , German_Icon );
		Action USAAction = new MyAction( "USA" , USA_Icon );
		
		JMenuBar jMenuBar1 = new JMenuBar();
		JMenu jmenuCountry = new JMenu("Country");
		setJMenuBar(jMenuBar1);
		jMenuBar1.add(jmenuCountry);
		
		jmenuCountry.add( CanadaAction );
		jmenuCountry.add( ChinaAction );
		jmenuCountry.add( EnglandAction );
		jmenuCountry.add( GermanAction );
		jmenuCountry.add( USAAction );
		
		JToolBar jToolBar1 = new JToolBar(JToolBar.VERTICAL);
		jToolBar1.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		jToolBar1.add(CanadaAction);
		jToolBar1.add(ChinaAction);
		jToolBar1.add(EnglandAction);
		jToolBar1.add(GermanAction);
		jToolBar1.add(USAAction);
		
		add(jToolBar1 , BorderLayout.EAST);
		add(Panel , BorderLayout.CENTER);
	}
	
	private class MyAction extends AbstractAction {
		String name;
		
		MyAction(String name , Icon icon) {
			super(name , icon);
			this.name = name;
		}
		
		public void actionPerformed(ActionEvent e ) {
			if(name.equals("Canada")) {
				
			}
			else if(name.equals("Chaina")) {
				
			}
			else if(name.equals("England")) {
				
			}
			else if(name.equals("German")) {
	
			}
			else if(name.equals("USA")) {
				
			}
		}
	}
}
