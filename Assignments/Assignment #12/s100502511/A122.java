package a12.s100502511;

import java.awt.*;
import java.awt.event.*;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.swing.*;

public class A122 extends JApplet {
	private ResourceBundle res = ResourceBundle
			.getBundle("Language/MyResource_ja");
	private ResourceBundle[] ress = new ResourceBundle[4];
	private JPanel screen1 = new JPanel(new GridLayout(1, 4));
	private JPanel screen2 = new JPanel(new GridLayout(1, 4));
	private JPanel screen3 = new JPanel(new GridLayout(1, 4));
	private JPanel Allscreen = new JPanel(new GridLayout(3, 1));
	private JLabel date = new JLabel(res.getString("Date"));
	private JLabel time = new JLabel(res.getString("Time"));
	private JLabel country = new JLabel(res.getString("Choose_locale"));
	private JLabel timezone = new JLabel(res.getString("Time_Zone"));
	private JLabel timestyle = new JLabel(res.getString("Time_Style"));
	private JLabel datestyle = new JLabel(res.getString("Date_Style"));
	private JTextField one = new JTextField();
	private JTextField two = new JTextField();
	private Locale[] availableLocale = { Locale.JAPAN, Locale.ENGLISH,
			Locale.TAIWAN, Locale.FRANCE };
	private JComboBox first = new JComboBox(availableLocale);
	private String[] availableTimeZones = TimeZone.getAvailableIDs();
	private JComboBox second = new JComboBox(availableTimeZones);
	private String[] t1 = { "Full", "Long", "Midterm", "Short" };
	private int t12 = 0;
	private JComboBox third = new JComboBox(t1);
	private String[] t2 = { "Full", "Long", "Midterm", "Short" };
	private int t22 = 0;
	private JComboBox fourth = new JComboBox(t2);

	private TimeZone timeZone = TimeZone.getTimeZone("EST");
	private Timer timer = new Timer(1000, new TimerListener());

	private String timestr = new String();
	private String datestr = new String();

	public A122() {
		
		screen1.add(date);
		screen1.add(one);
		screen1.add(time);
		screen1.add(two);
		screen2.add(country);
		screen2.add(first);
		screen2.add(timezone);
		screen2.add(second);
		screen3.add(timestyle);
		screen3.add(third);
		screen3.add(datestyle);
		screen3.add(fourth);
		Allscreen.add(screen1);
		Allscreen.add(screen2);
		Allscreen.add(screen3);
		add(Allscreen);
		timer.start();

		setres();

		first.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				setLocale(availableLocale[first.getSelectedIndex()]);
				res = ress[first.getSelectedIndex()];
				date.setText(res.getString("Date"));
				time.setText(res.getString("Time"));
				country.setText(res.getString("Choose_locale"));
				timezone.setText(res.getString("Time_Zone"));
				timestyle.setText(res.getString("Time_Style"));
				datestyle.setText(res.getString("Date_Style"));
				repaint();
			}
		});
		second.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) { 
				timeZone = TimeZone.getTimeZone(availableTimeZones[second.getSelectedIndex()]);
			}
		});
		third.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				t12 = third.getSelectedIndex();
			}
		});
		fourth.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				t22 = fourth.getSelectedIndex();
			}
		});
	}

	public void setres() {
		ress[0] = ResourceBundle.getBundle("Language/MyResource_ja");
		ress[1] = ResourceBundle.getBundle("Language/MyResource_en");
		ress[2] = ResourceBundle.getBundle("Language/MyResource_zh");
		ress[3] = ResourceBundle.getBundle("Language/MyResource_fr");
	}

	private class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Calendar calendar = new GregorianCalendar(timeZone, getLocale());

			// Display digit time on the label
			DateFormat dateformatter = DateFormat.getDateInstance(t12,
					getLocale());
			DateFormat timeformatter = DateFormat.getTimeInstance(t22,
					getLocale());
			dateformatter.setTimeZone(timeZone);
			timeformatter.setTimeZone(timeZone);
			datestr = dateformatter.format(calendar.getTime());
			one.setText(datestr);
			timestr = timeformatter.format(calendar.getTime());
			two.setText(timestr);
		}
	}

}
