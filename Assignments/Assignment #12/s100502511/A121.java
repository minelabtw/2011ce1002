package a12.s100502511;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class A121 extends JApplet {
	JButton Canada1 = new JButton();
	JButton China1 = new JButton();
	JButton England1 = new JButton();
	JButton German1 = new JButton();
	JButton USA1 = new JButton();
	JLabel picture = new JLabel();
	JPanel screen = new JPanel();

	public A121() {
		JToolBar jtb = new JToolBar("Tool"); // 建立JToolBar
		jtb.setFloatable(true);
		JMenuBar jmb = new JMenuBar(); // 建立JMenuBar
		JMenu File = new JMenu("File");
		JMenuItem Canada = new JMenuItem("Canada"), China = new JMenuItem(
				"China"), England = new JMenuItem("England"), German = new JMenuItem(
				"German"), USA = new JMenuItem("USA");
		Canada.setIcon(new ImageIcon("image/Canada_Icon.gif"));
		China.setIcon(new ImageIcon("image/China_Icon.gif"));
		England.setIcon(new ImageIcon("image/England_Icon.gif"));
		German.setIcon(new ImageIcon("image/German_Icon.gif"));
		USA.setIcon(new ImageIcon("image/USA_Icon.gif"));
		Canada1.setIcon(new ImageIcon("image/Canada_Icon.gif"));
		China1.setIcon(new ImageIcon("image/China_Icon.gif"));
		England1.setIcon(new ImageIcon("image/England_Icon.gif"));
		German1.setIcon(new ImageIcon("image/German_Icon.gif"));
		USA1.setIcon(new ImageIcon("image/USA_Icon.gif"));

		Canada.setToolTipText("Canada"); // 移到圖案上時會顯示的提示
		China.setToolTipText("China");
		England.setToolTipText("England");
		German.setToolTipText("German");
		USA.setToolTipText("USA");

		Canada1.setToolTipText("Canada");
		China1.setToolTipText("China");
		England1.setToolTipText("England");
		German1.setToolTipText("German");
		USA1.setToolTipText("USA");

		jtb.add(Canada1);
		jtb.add(China1);
		jtb.add(England1);
		jtb.add(German1);
		jtb.add(USA1);

		Canada1.setBorderPainted(false); // 消除邊框
		China1.setBorderPainted(false);
		England1.setBorderPainted(false);
		German1.setBorderPainted(false);
		USA1.setBorderPainted(false);

		add(jtb, BorderLayout.NORTH);
		JMenuItem Exit = new JMenuItem("Exit");
		Exit.setMnemonic('E');
		setJMenuBar(jmb);
		File.add(Canada);
		File.add(China);
		File.add(England);
		File.add(German);
		File.add(USA);
		File.add(Exit);
		jmb.add(File);
		picture.setIcon(new ImageIcon("image/Canada.gif")); // 設立初始圖案
		screen.add(picture);
		add(screen);

		Exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		Canada.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				picture.setIcon(new ImageIcon("image/Canada.gif"));
			}
		});
		China.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				picture.setIcon(new ImageIcon("image/China.gif"));
			}
		});
		England.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				picture.setIcon(new ImageIcon("image/England.gif"));
			}
		});
		German.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				picture.setIcon(new ImageIcon("image/German.gif"));
			}
		});
		USA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				picture.setIcon(new ImageIcon("image/USA.gif"));
			}
		});
		Canada1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				picture.setIcon(new ImageIcon("image/Canada.gif"));
			}
		});
		China1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				picture.setIcon(new ImageIcon("image/China.gif"));
			}
		});
		England1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				picture.setIcon(new ImageIcon("image/England.gif"));
			}
		});
		German1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				picture.setIcon(new ImageIcon("image/German.gif"));
			}
		});
		USA1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				picture.setIcon(new ImageIcon("image/USA.gif"));
			}
		});
	}
}
