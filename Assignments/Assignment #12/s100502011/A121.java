package a12.s100502011;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class A121 extends JApplet{
	JMenuBar nation = new JMenuBar();
	JMenu Nation = new JMenu("Nation");
	JLabel pic = new JLabel();
	JToolBar NaTool = new JToolBar(JToolBar.VERTICAL);
	public ImageIcon Canada = new ImageIcon(getClass().getResource("image/Canada_Icon.gif"));
	public ImageIcon China = new ImageIcon(getClass().getResource("image/China_Icon.gif"));
	public ImageIcon USA = new ImageIcon(getClass().getResource("image/USA_Icon.gif"));
	public ImageIcon England = new ImageIcon(getClass().getResource("image/England_Icon.gif"));
	public ImageIcon German = new ImageIcon(getClass().getResource("image/German_Icon.gif"));
	public ImageIcon BCanada = new ImageIcon(getClass().getResource("image/Canada.gif"));
	public ImageIcon BChina = new ImageIcon(getClass().getResource("image/China.gif"));
	public ImageIcon BUSA = new ImageIcon(getClass().getResource("image/USA.gif"));
	public ImageIcon BEngland = new ImageIcon(getClass().getResource("image/England.gif"));
	public ImageIcon BGerman = new ImageIcon(getClass().getResource("image/German.gif"));
	public A121(){
		//set menu bar
		Action CanAction = new MyAction("Canada",Canada);
		Action ChiAction = new MyAction("China",China);
		Action USAAction = new MyAction("USA",USA);
		Action EngAction = new MyAction("England",England);
		Action GerAction = new MyAction("German",German);
		setJMenuBar(nation);
		nation.add(Nation);
		
		//set menu bar's action
		Nation.add(CanAction);
		Nation.add(ChiAction);
		Nation.add(USAAction);
		Nation.add(EngAction);
		Nation.add(GerAction);
		
		//set tool bar
		NaTool.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		NaTool.add(CanAction);
		NaTool.add(ChiAction);
		NaTool.add(USAAction);
		NaTool.add(EngAction);
		NaTool.add(GerAction);
		pic.setSize(300,300);
		add(pic);
		add(NaTool,BorderLayout.EAST);
	
	}
	private class MyAction extends AbstractAction{
		String name;
		
		MyAction(String name,Icon icon){
			super(name,icon);
			this.name=name;
		}
		// action performed
		public void actionPerformed(ActionEvent e){
			//choose what you want
			if(name.equals("Canada")){
				pic.setIcon(BCanada);
			}
			else if(name.equals("China")){
				pic.setIcon(BChina);
			}
			else if(name.equals("England")){
				pic.setIcon(BEngland);
			}
			else if(name.equals("German")){
				pic.setIcon(BGerman);
			}
			else if(name.equals("USA")){
				pic.setIcon(BUSA);
			}
		}
	}

}

