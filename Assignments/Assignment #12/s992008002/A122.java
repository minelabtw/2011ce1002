package a12.s992008002;

import java.awt.*;
import java.awt.event.*;
import java.applet.Applet;
import javax.swing.*;

public class A122 extends Applet {
	//宣告宣告宣告
    JComboBox datestyle;
    JLabel label_6;
    JTextArea DATE;
    JScrollPane sp_DATE;
    JComboBox timestyle;
    JTextArea TIME;
    JScrollPane sp_TIME;
    JLabel label_1;
    JLabel label_2;
    JLabel label_3;
    JComboBox country;
    JLabel label_4;
    JLabel label_5;
    JComboBox zone;

    public void init() {
        A122Layout customLayout = new A122Layout();

        setFont(new Font("Helvetica", Font.PLAIN, 12));
        setLayout(customLayout);

        datestyle = new JComboBox();
        datestyle.addItem("FULL");
        datestyle.addItem("HALF");
        add(datestyle);

        label_6 = new JLabel("時間風格");
        add(label_6);

        DATE = new JTextArea("DATE");
        sp_DATE = new JScrollPane(DATE);
        add(sp_DATE);

        timestyle = new JComboBox();
        timestyle.addItem("FULL");
        timestyle.addItem("HALF");
        add(timestyle);

        TIME = new JTextArea("TIME");
        sp_TIME = new JScrollPane(TIME);
        add(sp_TIME);

        label_1 = new JLabel("日期");
        add(label_1);

        label_2 = new JLabel("時間");
        add(label_2);

        label_3 = new JLabel("選擇國家");
        add(label_3);

        country = new JComboBox();
        country.addItem("CHINESE");
        country.addItem("ENGLISH");
        country.addItem("JAPANNESS");
        country.addItem("FRENCH");
        add(country);

        label_4 = new JLabel("時區");
        add(label_4);

        label_5 = new JLabel("日期風格");
        add(label_5);
//沒時間加function了....
        
        zone = new JComboBox();
        zone.addItem("GMT+0");
        zone.addItem("GMT+1");
        zone.addItem("GMT+2");
        zone.addItem("GMT+3");
        zone.addItem("GMT+4");
        zone.addItem("GMT+5");
        zone.addItem("GMT+6");
        zone.addItem("GMT+7");
        zone.addItem("GMT+8");
        zone.addItem("GMT+9");
        zone.addItem("GMT+10");
        zone.addItem("GMT+11");
        zone.addItem("GMT-1");
        zone.addItem("GMT-2");
        zone.addItem("GMT-3");
        zone.addItem("GMT-4");
        zone.addItem("GMT-5");
        zone.addItem("GMT-6");
        zone.addItem("GMT-7");
        zone.addItem("GMT-8");
        zone.addItem("GMT-9");
        zone.addItem("GMT-10");
        zone.addItem("GMT-11");
        add(zone);

        setSize(getPreferredSize());

    }

    public static void main(String args[]) {
        A122 applet = new A122();
        Frame window = new Frame("A122");

        window.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        applet.init();
        window.add("Center", applet);
        window.pack();
        window.setVisible(true);
    }
}

class A122Layout implements LayoutManager {

    public A122Layout() {
    }

    public void addLayoutComponent(String name, Component comp) {
    }

    public void removeLayoutComponent(Component comp) {
    }

    public Dimension preferredLayoutSize(Container parent) {
        Dimension dim = new Dimension(0, 0);

        Insets insets = parent.getInsets();
        dim.width = 469 + insets.left + insets.right;
        dim.height = 156 + insets.top + insets.bottom;

        return dim;
    }

    public Dimension minimumLayoutSize(Container parent) {
        Dimension dim = new Dimension(0, 0);
        return dim;
    }

    public void layoutContainer(Container parent) {
        Insets insets = parent.getInsets();

        Component c;
        c = parent.getComponent(0);
        if (c.isVisible()) {c.setBounds(insets.left+64,insets.top+109,128,19);}
        c = parent.getComponent(1);
        if (c.isVisible()) {c.setBounds(insets.left+237,insets.top+109,58,19);}
        c = parent.getComponent(2);
        if (c.isVisible()) {c.setBounds(insets.left+64,insets.top+19,147,38);}
        c = parent.getComponent(3);
        if (c.isVisible()) {c.setBounds(insets.left+294,insets.top+109,141,19);}
        c = parent.getComponent(4);
        if (c.isVisible()) {c.setBounds(insets.left+294,insets.top+19,160,45);}
        c = parent.getComponent(5);
        if (c.isVisible()) {c.setBounds(insets.left+6,insets.top+32,58,19);}
        c = parent.getComponent(6);
        if (c.isVisible()) {c.setBounds(insets.left+237,insets.top+32,58,19);}
        c = parent.getComponent(7);
        if (c.isVisible()) {c.setBounds(insets.left+6,insets.top+77,58,19);}
        c = parent.getComponent(8);
        if (c.isVisible()) {c.setBounds(insets.left+64,insets.top+77,128,19);}
        c = parent.getComponent(9);
        if (c.isVisible()) {c.setBounds(insets.left+237,insets.top+77,58,19);}
        c = parent.getComponent(10);
        if (c.isVisible()) {c.setBounds(insets.left+6,insets.top+109,58,19);}
        c = parent.getComponent(11);
        if (c.isVisible()) {c.setBounds(insets.left+294,insets.top+77,141,19);}
    }
}
