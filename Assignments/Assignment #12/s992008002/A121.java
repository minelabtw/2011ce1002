package a12.s992008002;


import java.awt.*;
import java.awt.event.*;
import java.applet.Applet;
import javax.swing.*;

public class A121 extends Applet {
    Label label_1; //這次心血來潮的把宣告分開了   平常都喜歡寫在一起
    JButton CAN;
    JButton CHI;
    JButton ENG;
    JButton GER;
    JButton USA;
    JToolBar FlagBar;
    public void init() { //applet 的 init  還在習慣中....
        A121Layout customLayout = new A121Layout();

        setFont(new Font("Helvetica", Font.PLAIN, 12));
        setLayout(customLayout);

        FlagBar = new JToolBar("Flag Bar");
        label_1 = new Label("label_1");
        add(label_1);

        CAN = new JButton(new ImageIcon("image/Canada_Icon.gif"));
        add(CAN);
        //FlagBar.add(CAN);

        CHI = new JButton(new ImageIcon("image/China_Icon.gif"));
        //FlagBar.add(CHI);
        add(CHI);
        ENG = new JButton(new ImageIcon("image/England_Icon.gif"));
        //FlagBar.add(ENG);
        add(ENG);
        GER = new JButton(new ImageIcon("image/German_Icon.gif"));
        //FlagBar.add(GER);
        add(GER);
        USA = new JButton(new ImageIcon("image/USA_Icon.gif"));
        //FlagBar.add(USA);
        add(USA);
        setSize(getPreferredSize());
        //add(FlagBar,BorderLayout.WEST);
    }

    public static void main(String args[]) {
        A121 applet = new A121();
        Frame window = new Frame("A121");

        window.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        applet.init();
        window.add("Center", applet);
        window.pack();
        window.setVisible(true);
    }
}
//有待填滿...
class A121Layout implements LayoutManager {

    public A121Layout() {
    }

    public void addLayoutComponent(String name, Component comp) {
    }

    public void removeLayoutComponent(Component comp) {
    }

    public Dimension preferredLayoutSize(Container parent) {
        Dimension dim = new Dimension(0, 0);

        Insets insets = parent.getInsets();
        dim.width = 320 + insets.left + insets.right;
        dim.height = 250 + insets.top + insets.bottom;

        return dim;
    }

    public Dimension minimumLayoutSize(Container parent) {
        Dimension dim = new Dimension(0, 0);
        return dim;
    }

    public void layoutContainer(Container parent) {
        Insets insets = parent.getInsets();

        Component c;
       c = parent.getComponent(0);
        if (c.isVisible()) {c.setBounds(insets.left+38,insets.top+38,275,218);}
        c = parent.getComponent(1);
        if (c.isVisible()) {c.setBounds(insets.left+6,insets.top+45,19,19);}
        c = parent.getComponent(2);
        if (c.isVisible()) {c.setBounds(insets.left+6,insets.top+147,19,19);}
        c = parent.getComponent(3);
        if (c.isVisible()) {c.setBounds(insets.left+6,insets.top+122,19,19);}
        c = parent.getComponent(4);
        if (c.isVisible()) {c.setBounds(insets.left+6,insets.top+96,19,19);}
        c = parent.getComponent(5);
        if (c.isVisible()) {c.setBounds(insets.left+6,insets.top+70,19,19);}
    }
}
