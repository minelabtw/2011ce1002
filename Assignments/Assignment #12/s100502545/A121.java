package a12.s100502545;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JToolBar;

public class A121 extends JApplet implements ActionListener {

	JButton jbt1 = new JButton(new ImageIcon(("image/Canada_Icon.gif")));
	JButton jbt2 = new JButton(new ImageIcon(("image/China_Icon.gif")));
	JButton jbt3 = new JButton(new ImageIcon(("image/England_Icon.gif")));
	JButton jbt4 = new JButton(new ImageIcon(("image/German_Icon.gif")));
	JButton jbt5 = new JButton(new ImageIcon(("image/USA_Icon.gif")));

	ImageIcon pic1 = new ImageIcon("image/Canada.gif");
	ImageIcon pic2 = new ImageIcon("image/China.gif");
	ImageIcon pic3 = new ImageIcon("image/England.gif");
	ImageIcon pic4 = new ImageIcon("image/German.gif");
	ImageIcon pic5 = new ImageIcon("image/USA.gif");

	JMenuBar JmenuBar = new JMenuBar();
	JMenu Jmenu = new JMenu("Menu");

	JMenuItem Canada = new JMenuItem("Canada");
	JMenuItem China = new JMenuItem("China");
	JMenuItem England = new JMenuItem("England");
	JMenuItem German = new JMenuItem("German");
	JMenuItem USA = new JMenuItem("USA");

	JToolBar JToolBar = new JToolBar("ToolBox");
	
	JPanel p1 = new JPanel();//menu
	JPanel p2 = new JPanel();//toolbar
	JLabel label = new JLabel();//顯示圖片Label

	public A121() {
		//放按鈕圖片
		JToolBar.setBorder(null);
		JToolBar.add(jbt1);
		JToolBar.add(jbt2);
		JToolBar.add(jbt3);
		JToolBar.add(jbt4);
		JToolBar.add(jbt5);

		//Menu加入選項
		Jmenu.add(Canada);
		Jmenu.add(China);
		Jmenu.add(England);
		Jmenu.add(German);
		Jmenu.add(USA);
		JmenuBar.add(Jmenu);

		p1.setLayout(new GridLayout(2, 1));
		p2.setLayout(new GridLayout(1, 1));
		setJMenuBar(JmenuBar);

		p1.add(JmenuBar);
		p2.add(JToolBar);

		//版面設置
		add(p1, BorderLayout.NORTH);
		add(p2, BorderLayout.EAST);
		add(label, BorderLayout.CENTER);
		setSize(800, 600);

		jbt1.addActionListener(this);
		jbt2.addActionListener(this);
		jbt3.addActionListener(this);
		jbt4.addActionListener(this);
		jbt5.addActionListener(this);
		Canada.addActionListener(this);
		China.addActionListener(this);
		England.addActionListener(this);
		German.addActionListener(this);
		USA.addActionListener(this);

	}

	public void actionPerformed(ActionEvent e) {//按鈕處理
		if (e.getSource() == jbt1)
		{
			label.setIcon(pic1);
		} else if (e.getSource() == jbt2) {
			label.setIcon(pic2);
		} else if (e.getSource() == jbt3) {
			label.setIcon(pic3);
		} else if (e.getSource() == jbt4) {
			label.setIcon(pic4);
		} else if (e.getSource() == jbt5) {
			label.setIcon(pic5);
		} else if (e.getSource() == Canada) {
			label.setIcon(pic1);
		} else if (e.getSource() == China) {
			label.setIcon(pic2);
		} else if (e.getSource() == England) {
			label.setIcon(pic3);
		} else if (e.getSource() == German) {
			label.setIcon(pic4);
		} else if (e.getSource() == USA) {
			label.setIcon(pic5);
		}
	}

}
