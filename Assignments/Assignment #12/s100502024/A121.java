package a12.s100502024;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class A121 extends JApplet
{
	ImageIcon Image_Canada = new ImageIcon(getClass().getResource("image/Canada.gif"));  // 讀檔
	ImageIcon Image_China = new ImageIcon(getClass().getResource("image/China.gif"));
	ImageIcon Image_England = new ImageIcon(getClass().getResource("image/England.gif"));
	ImageIcon Image_German = new ImageIcon(getClass().getResource("image/German.gif"));
	ImageIcon Image_USA = new ImageIcon(getClass().getResource("image/USA.gif"));
	
	ImageIcon Icon_Canada = new ImageIcon(getClass().getResource("image/Canada_Icon.gif"));
	ImageIcon Icon_China = new ImageIcon(getClass().getResource("image/China_Icon.gif"));
	ImageIcon Icon_England = new ImageIcon(getClass().getResource("image/England_Icon.gif"));
	ImageIcon Icon_German = new ImageIcon(getClass().getResource("image/German_Icon.gif"));
	ImageIcon Icon_USA = new ImageIcon(getClass().getResource("image/USA_Icon.gif"));

	private JLabel show_country = new JLabel();  // 顯示國旗的Label
	
	public A121()
	{
		show_country.setIcon(Image_Canada);
		Action Canada = new MyAction("Canada",Icon_Canada); // 建立Action
		Action China = new MyAction("China",Icon_China);
		Action England = new MyAction("England",Icon_England);
		Action German = new MyAction("German",Icon_German);
		Action USA = new MyAction("USA",Icon_USA);
		
		JMenuBar jmb = new JMenuBar();  // 創建MenuBar
		JMenu Country = new JMenu("Country"); 
		setJMenuBar(jmb);
		jmb.add(Country);
		
		Country.add(Canada);  // 設定Menu的選項
		Country.add(China);
		Country.add(England);
		Country.add(German);
		Country.add(USA);
		
		JToolBar jToolBar = new JToolBar(JToolBar.VERTICAL);  // 設定工具列
		jToolBar.add(Canada);  // 增加工具列的選項
		jToolBar.add(China);
		jToolBar.add(England);
		jToolBar.add(German);
		jToolBar.add(USA);
		
		add(jToolBar,BorderLayout.EAST);
		add(show_country,BorderLayout.CENTER);
	}
	private class MyAction extends AbstractAction
	{
		String country_name;
		public MyAction(String name,Icon icon)
		{
			super(name,icon);
			this.country_name = name;
		}
		public void actionPerformed(ActionEvent e)
		{
			if(country_name.equals("Canada"))  // 當選到Canada時,顯示國旗的面板顯示Canada的國旗
			{
				show_country.setIcon(Image_Canada);
			}
			else if(country_name.equals("China"))
			{
				show_country.setIcon(Image_China);
			}
			else if(country_name.equals("England"))
			{
				show_country.setIcon(Image_England);
			}
			else if(country_name.equals("German"))
			{
				show_country.setIcon(Image_German);
			}
			else if(country_name.equals("USA"))
			{
				show_country.setIcon(Image_USA);
			}
		}
	}
}
