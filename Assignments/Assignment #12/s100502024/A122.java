package a12.s100502024;
import java.awt.*;
import java.text.DateFormat;
import java.util.*;
import javax.swing.*;
import javax.swing.Timer;
import java.awt.event.*;

public class A122 extends JApplet
{
	Locale English = new Locale("en","US"); // 設定Locale
	Locale French = new Locale("fr","CA");
	Locale Japan = new Locale("ja","JP");
	Locale Taiwan = new Locale("zh","TW");
	
	private Locale[] locales = {English,French,Japan,Taiwan};
	int[] dateformat = {DateFormat.FULL,DateFormat.MEDIUM,DateFormat.SHORT};  // 時間日期顯示長度
	
	private ResourceBundle res = ResourceBundle.getBundle("MyResource");
	
	private String[] availableTimeZones = TimeZone.getAvailableIDs();  // 可用的TimeZone
	private Timer timer = new Timer(1000,new TimerListener());
	
	private Font font_1 = new Font("Serif",Font.BOLD,20);  // 設定字體大小和字型
	private Font font_2 = new Font("Serif",Font.BOLD,23);
	
	private JTextField showdate = new JTextField();  // 顯示時間和日期的TextField
	private JTextField showtime = new JTextField();
	
	private JLabel date = new JLabel("Date");  // 顯示資訊的Label
	private JLabel time = new JLabel("Time");
	private JLabel country = new JLabel("Choose Locale");
	private JLabel timezone = new JLabel("Time Zone");
	private JLabel date_style = new JLabel("Date Style");
	private JLabel time_style = new JLabel("Time Style");
	
	private JPanel p1 = new JPanel();
	private JPanel p2 = new JPanel();
	private JPanel p3 = new JPanel();
	
	private String[] name_datestyle = {"Full","Medium","Short"};  // Date Style MenuItem
	private String[] name_timestyle = {"Full","Medium","Short"};  // Time Style MenuItem
	
	private JComboBox select_country = new JComboBox();
	private JComboBox select_timezone = new JComboBox();
	private JComboBox select_date_style = new JComboBox(name_datestyle);
	private JComboBox select_time_style = new JComboBox(name_timestyle);
	
	GregorianCalendar calendar = new GregorianCalendar();
	DateFormat formatter_date = DateFormat.getDateInstance(DateFormat.FULL,English);  // Default date display and locale
	DateFormat formatter_time = DateFormat.getTimeInstance(DateFormat.FULL,English);  // Default time display and locale
	
	public A122()
	{
		timer.start();
		setAvailableLocales();   // 設定 Locale
		setAvailabelTimeZones(); // 設定 TimeZone 
		
		formatter_date.setTimeZone(TimeZone.getTimeZone(availableTimeZones[select_timezone.getSelectedIndex()]));
		formatter_time.setTimeZone(TimeZone.getTimeZone(availableTimeZones[select_timezone.getSelectedIndex()]));
		
		date.setFont(font_1);
		time.setFont(font_1);
		country.setFont(font_1);
		timezone.setFont(font_1);
		date_style.setFont(font_1);
		time_style.setFont(font_1);
		p1.setLayout(new GridLayout(1,4));
		p1.add(date);
		p1.add(showdate);
		showdate.setFont(font_2);
		p1.add(time);
		p1.add(showtime);
		showtime.setFont(font_2);
		p2.add(country);
		p2.add(select_country);
		p2.add(timezone);
		p2.add(select_timezone);
		p3.add(date_style);
		p3.add(select_date_style);
		p3.add(time_style);
		p3.add(select_time_style);
		setLayout(new GridLayout(3,1));
		add(p1);
		add(p2);
		add(p3);
		
		select_country.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				updateStrings();  // 更新資訊
				formatter_date = DateFormat.getDateInstance(dateformat[select_date_style.getSelectedIndex()],locales[select_country.getSelectedIndex()]); // change locale
				formatter_time = DateFormat.getTimeInstance(dateformat[select_time_style.getSelectedIndex()],locales[select_country.getSelectedIndex()]);
				formatter_date.setTimeZone(TimeZone.getTimeZone(availableTimeZones[select_timezone.getSelectedIndex()]));
				formatter_time.setTimeZone(TimeZone.getTimeZone(availableTimeZones[select_timezone.getSelectedIndex()]));
				showdate.setText(formatter_date.format(calendar.getTime()));  // 重新設定日期顯示
				showtime.setText(formatter_time.format(calendar.getTime()));  // 重新設定時間顯示
			}
		});
		
		select_timezone.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				formatter_date.setTimeZone(TimeZone.getTimeZone(availableTimeZones[select_timezone.getSelectedIndex()]));  // change Time Zone
				formatter_time.setTimeZone(TimeZone.getTimeZone(availableTimeZones[select_timezone.getSelectedIndex()]));
				showdate.setText(formatter_date.format(calendar.getTime()));
				showtime.setText(formatter_time.format(calendar.getTime()));
			}
		});
		
		select_date_style.addActionListener(new ActionListener(){  
			public void actionPerformed(ActionEvent e)
			{
				if(select_date_style.getSelectedIndex() == 0)  // change date style
				{
					formatter_date = DateFormat.getDateInstance(DateFormat.FULL,locales[select_country.getSelectedIndex()]);  // Full
					showdate.setText(formatter_date.format(calendar.getTime()));
				}
				if(select_date_style.getSelectedIndex() == 1)
				{
					formatter_date = DateFormat.getDateInstance(DateFormat.MEDIUM,locales[select_country.getSelectedIndex()]);  // Medium
					showdate.setText(formatter_date.format(calendar.getTime()));
				}
				if(select_date_style.getSelectedIndex() == 2)
				{
					formatter_date = DateFormat.getDateInstance(DateFormat.SHORT,locales[select_country.getSelectedIndex()]);  // Short
					showdate.setText(formatter_date.format(calendar.getTime()));
				}
			}
		});
		
		select_time_style.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				if(select_time_style.getSelectedIndex() == 0)  // change time style
				{
					formatter_time = DateFormat.getTimeInstance(DateFormat.FULL,locales[select_country.getSelectedIndex()]);  // Full
					showtime.setText(formatter_time.format(calendar.getTime()));
				}
				if(select_time_style.getSelectedIndex() == 1)
				{
					formatter_time = DateFormat.getTimeInstance(DateFormat.MEDIUM,locales[select_country.getSelectedIndex()]);  // Medium
					showtime.setText(formatter_time.format(calendar.getTime()));
				}
				if(select_time_style.getSelectedIndex() == 2)
				{
					formatter_time = DateFormat.getTimeInstance(DateFormat.SHORT,locales[select_country.getSelectedIndex()]);  // short
					showtime.setText(formatter_time.format(calendar.getTime()));
				}
			}
		});
	}
	private class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			GregorianCalendar calendar = new GregorianCalendar();
			showdate.setText(formatter_date.format(calendar.getTime()));
			showtime.setText(formatter_time.format(calendar.getTime()));
		}
	}
	public void updateStrings()  // 設定更新訊息
	{
		res = ResourceBundle.getBundle("MyResource",locales[select_country.getSelectedIndex()]);
		date.setText(res.getString("Date"));
		time.setText(res.getString("Time"));
		country.setText(res.getString("Choose_locale"));
		timezone.setText(res.getString("Time_Zone"));
		date_style.setText(res.getString("Date_Style"));
		time_style.setText(res.getString("Time_Style"));
	}
	public void setAvailableLocales()
	{
		for(int i=0;i<locales.length;i++)
		{
			select_country.addItem(locales[i].getDisplayName());
		}
	}
	public void setAvailabelTimeZones()
	{
		for(int i=0;i<availableTimeZones.length;i++)
		{
			select_timezone.addItem(availableTimeZones[i]);
		}
	}
}
