package a12.s100502013;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class A121 extends JApplet {
	private JPanel total = new JPanel();
	private imagepanel painter = new imagepanel();
	private JMenuBar menubar = new JMenuBar();
	private JMenu menu = new JMenu("Menu");
	private JToolBar toolbar = new JToolBar();
	private ImageIcon Canadaicon = new ImageIcon(getClass().getResource("Canada_Icon.gif"));
	private ImageIcon Chinaicon = new ImageIcon(getClass().getResource("China_Icon.gif"));
	private ImageIcon Englandicon = new ImageIcon(getClass().getResource("England_Icon.gif"));
	private ImageIcon Germanicon = new ImageIcon(getClass().getResource("German_Icon.gif"));
	private ImageIcon USAicon = new ImageIcon(getClass().getResource("USA_Icon.gif"));
	private Action Canadab = new MyAction("Canada",Canadaicon);
	private Action Chinab = new MyAction("China",Chinaicon);
	private Action Englandb = new MyAction("England",Englandicon);
	private Action Germanb = new MyAction("German",Germanicon);
	private Action USAb = new MyAction("USA",USAicon);
	
	public A121(){ //set details
		setLayout(new BorderLayout(1,1));
		total.setLayout(new BorderLayout(1,1));
		menu.add(Canadab);
		menu.add(Chinab);
		menu.add(Englandb);
		menu.add(Germanb);
		menu.add(USAb);
		menubar.add(menu);
		setJMenuBar(menubar);
		toolbar.add(Canadab);
		toolbar.add(Chinab);
		toolbar.add(Englandb);
		toolbar.add(Germanb);
		toolbar.add(USAb);
		total.add(toolbar, BorderLayout.NORTH);
		total.add(painter, BorderLayout.CENTER);
		add(total);
	}
	
	private class MyAction extends AbstractAction{ //action
		String name;
		
		MyAction(String name,Icon icon){
			super(name, icon);
			this.name = name;
		}

		public void actionPerformed(ActionEvent e) { //change country picture and repaint
			if(name.equals("Canada")){
				painter.background = new ImageIcon(getClass().getResource("Canada.gif"));
				painter.repaint();
			}
			else if(name.equals("China")){
				painter.background = new ImageIcon(getClass().getResource("China.gif"));
				painter.repaint();
			}
			else if(name.equals("England")){
				painter.background = new ImageIcon(getClass().getResource("England.gif"));
				painter.repaint();
			}
			else if(name.equals("German")){
				painter.background = new ImageIcon(getClass().getResource("German.gif"));
				painter.repaint();
			}
			else if(name.equals("USA")){
				painter.background = new ImageIcon(getClass().getResource("USA.gif"));
				painter.repaint();
			}
		}
	}
	
	private class imagepanel extends JPanel{ //picture panel
		protected ImageIcon background = new ImageIcon("Canada.gif");
		
		public void paintComponent(Graphics g){
			g.drawImage(background.getImage(), (int)(getWidth()*0.1), (int)(getHeight()*0.1), (int)(getWidth()*0.8), (int)(getHeight()*0.8), null);
		}
	}
}
