package a12.s100502012;

import java.awt.event.ActionEvent;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class A121 extends JApplet{
	public A121(){
	    add(new FrameWork());   	
	}
}

class FrameWork extends JPanel{
	private JLabel l=new JLabel();
	private JToolBar j=new JToolBar();
	private JMenuBar mb=new JMenuBar();
	private JMenu m=new JMenu("Setting");
	private ImageIcon CanadaIcon=new ImageIcon(getClass().getResource("image/Canada_Icon.gif"));
	private ImageIcon ChinaIcon=new ImageIcon(getClass().getResource("image/China_Icon.gif"));
	private ImageIcon GermanIcon=new ImageIcon(getClass().getResource("image/German_Icon.gif"));
	private ImageIcon EnglandIcon=new ImageIcon(getClass().getResource("image/England_Icon.gif"));
	private ImageIcon USAIcon=new ImageIcon(getClass().getResource("image/USA_Icon.gif"));
	private ImageIcon Canada=new ImageIcon(getClass().getResource("image/Canada.gif"));
	private ImageIcon China=new ImageIcon(getClass().getResource("image/China.gif"));
	private ImageIcon German=new ImageIcon(getClass().getResource("image/German.gif"));
	private ImageIcon England=new ImageIcon(getClass().getResource("image/England.gif"));
	private ImageIcon USA=new ImageIcon(getClass().getResource("image/USA.gif"));
		
	public FrameWork(){
	    Action canadaAction=new MyAction("Canada",CanadaIcon);//setting action of the icon "Canada",which also send the icon's name;
	    Action chinaAction=new MyAction("China",ChinaIcon);
	    Action germanAction=new MyAction("German",GermanIcon);
	    Action englandAction=new MyAction("England",EnglandIcon);
	    Action usaAction=new MyAction("USA",USAIcon);
	    
	    m.add(canadaAction);//add icon into menu
	    m.add(englandAction);
	    m.add(germanAction);
	    m.add(usaAction);
	    m.add(chinaAction);
	    mb.add(m);//add menu into bar 

	    j.add(mb);//add bar into toolbar
	    j.add(canadaAction);//add icon into toolbar
	    j.add(englandAction);
	    j.add(germanAction);
	    j.add(usaAction);
	    j.add(chinaAction);
	    j.setSize(434,25);
	    j.setLocation(0,0);
	    
	    l.setSize(434,217);
	    l.setIcon(null);//default icon "null"
	      
	    add(j);//add toolbar into frame
	    add(l);//add picture into frame
	}
	
    private class MyAction extends AbstractAction{ 
		String name;
		MyAction(String name,Icon icon){//catch the name of icon which was clicked
			super(name,icon);
			this.name=name;
		}
    
        public void actionPerformed(ActionEvent e){
	        if (name.equals("Canada")){//if the name is "Canada",then display the flag of "Canada" in the label 
			    l.setIcon(Canada);
		    }
			
	        if (name.equals("China")){
       	        l.setIcon(China);
       	    }
            
            if (name.equals("England")){
                l.setIcon(England);
	        }
            
            if (name.equals("German")){
       	        l.setIcon(German);
       	    }
            
            if (name.equals("USA")){
        	    l.setIcon(USA);
	        }
        }
    }
}