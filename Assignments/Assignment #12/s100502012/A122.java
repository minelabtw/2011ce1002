package a12.s100502012;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.naming.ldap.SortControl;
import javax.swing.JApplet;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.Timer;

public class A122 extends JApplet {
	private String[] language={"CHINESE","ENGLISH","FRENCH","JAPANESE"};
	private String[] availableTimeZone=TimeZone.getAvailableIDs();
	private String[] datestyle={"FULL","LONG","MEDIUM","SHORT"};
	private String[] timestyle={"FULL","LONG","MEDIUM","SHORT"};
	private JTextField dateField=new JTextField();
	private JTextField timeField=new JTextField();
	private ResourceBundle res=ResourceBundle.getBundle("MyResource");
    private JLabel dateLabel=new JLabel(res.getString("Date"));
    private JLabel timeLabel=new JLabel(res.getString("Time"));
    private JLabel countryLabel=new JLabel(res.getString("Choose_locale"));
    private JLabel zoneLabel=new JLabel(res.getString("Time_Zone"));
    private JLabel datestylelLabel=new JLabel(res.getString("Date_Style"));
    private JLabel timestyleLabel=new JLabel(res.getString("Time_Style"));
    private JComboBox countryBox=new JComboBox(language);
    private JComboBox zoneBox=new JComboBox();
    private JComboBox datestyleBox=new JComboBox(datestyle);
    private JComboBox timestyleBox=new JComboBox(timestyle);
    private TimeZone timeZone=TimeZone.getTimeZone("Asia/Taipei");
    private Calendar calendar=new GregorianCalendar(timeZone);
    private Locale locale=Locale.getDefault();
    private Locale[] locales={Locale.CHINESE,Locale.US,Locale.FRANCE,Locale.JAPAN};
    
    public A122(){
    	Timer timer=new Timer(1000, new TimerListener());//use timer to renew the time by per second
    	timer.start();
    	
    	setLayout(null);
    	
    	dateLabel.setLocation(50,50);
    	dateLabel.setSize(150,25);
    	add(dateLabel);
    	dateField.setLocation(225,50);
    	dateField.setSize(150,25);
		DateFormat dateformatter=DateFormat.getDateInstance(DateFormat.SHORT);//set format
		dateformatter.setTimeZone(timeZone);//set time zone
		dateField.setText(dateformatter.format(calendar.getTime()));//set current time(date)
    	add(dateField);
    	
    	timeLabel.setLocation(450,50);
    	timeLabel.setSize(150,25);
    	add(timeLabel);
    	timeField.setLocation(625,50);
    	timeField.setSize(150,25);
    	DateFormat timeformatter=DateFormat.getTimeInstance(DateFormat.SHORT);//set format
		timeformatter.setTimeZone(timeZone);//set time zone
		timeField.setText(timeformatter.format(calendar.getTime()));//set current time
        add(timeField);
    	
    	zoneLabel.setLocation(50, 150);
    	zoneLabel.setSize(150,25);
    	add(zoneLabel);
    	zoneBox.setLocation(225,150);
    	zoneBox.setSize(150,25);
    	add(zoneBox);
    	Arrays.sort(availableTimeZone);//sort items
    	for(int a=0 ; a<availableTimeZone.length ; a++){//add items
    		zoneBox.addItem(availableTimeZone[a]);
    	}
    	zoneBox.addItemListener(new ItemListener(){//set action
			public void itemStateChanged(ItemEvent e){
				int a=zoneBox.getSelectedIndex();
				timeZone=timeZone.getTimeZone(availableTimeZone[a]);//set time zone
			}
		});
    	
    	countryLabel.setLocation(450, 150);
    	countryLabel.setSize(150,25);
    	add(countryLabel);
    	countryBox.setLocation(625,150);
    	countryBox.setSize(150,25);
    	add(countryBox);
    	countryBox.addItemListener(new ItemListener(){//set action
			public void itemStateChanged(ItemEvent e){
				locale=locales[countryBox.getSelectedIndex()];
				setLocale(locale);//set the chose item
				updateString();
			}
		});
    	
    	datestylelLabel.setLocation(50,250);
    	datestylelLabel.setSize(150,25);
    	add(datestylelLabel);
    	datestyleBox.setLocation(225,250);
    	datestyleBox.setSize(150,25);
    	add(datestyleBox);
    	datestyleBox.addItemListener(new ItemListener(){//set action
			public void itemStateChanged(ItemEvent e){//set format
				if (datestyleBox.getSelectedIndex()==0){
					DateFormat formatter=DateFormat.getDateInstance(DateFormat.SHORT);
					formatter.setTimeZone(timeZone);
					dateField.setText(formatter.format(calendar.getTime()));
				}
				
                if (datestyleBox.getSelectedIndex()==1){
                	DateFormat formatter=DateFormat.getDateInstance(DateFormat.MEDIUM);
                	formatter.setTimeZone(timeZone);
					dateField.setText(formatter.format(calendar.getTime()));
				}
				
                if (datestyleBox.getSelectedIndex()==2){
                	DateFormat formatter=DateFormat.getDateInstance(DateFormat.LONG);
                	formatter.setTimeZone(timeZone);
					dateField.setText(formatter.format(calendar.getTime()));
				}
                
                if (datestyleBox.getSelectedIndex()==3){
                	DateFormat formatter=DateFormat.getDateInstance(DateFormat.FULL);
                	formatter.setTimeZone(timeZone);
					dateField.setText(formatter.format(calendar.getTime()));
				}
			}
		});
    	
    	timestyleLabel.setLocation(450,250);
    	timestyleLabel.setSize(150,25);
    	add(timestyleLabel);
    	timestyleBox.setLocation(625,250);
    	timestyleBox.setSize(150,25);
    	add(timestyleBox);
    	timestyleBox.addItemListener(new ItemListener(){//set action
			public void itemStateChanged(ItemEvent e){//set format
				if (timestyleBox.getSelectedIndex()==0){
					DateFormat formatter=DateFormat.getTimeInstance(DateFormat.SHORT);
					formatter.setTimeZone(timeZone);
					timeField.setText(formatter.format(calendar.getTime()));
				}
				
                if (timestyleBox.getSelectedIndex()==1){
                	DateFormat formatter=DateFormat.getTimeInstance(DateFormat.MEDIUM);
                	formatter.setTimeZone(timeZone);
					timeField.setText(formatter.format(calendar.getTime()));
				}
				
                if (timestyleBox.getSelectedIndex()==2){
                	DateFormat formatter=DateFormat.getTimeInstance(DateFormat.LONG);
                	formatter.setTimeZone(timeZone);
					timeField.setText(formatter.format(calendar.getTime()));
				}
                
                if (timestyleBox.getSelectedIndex()==3){
                	DateFormat formatter=DateFormat.getTimeInstance(DateFormat.FULL);
                	formatter.setTimeZone(timeZone);
					timeField.setText(formatter.format(calendar.getTime()));
				}
			}
		});
    }
    
    private void updateString(){
    	res=ResourceBundle.getBundle("MyResource",locale);//get the resource bundle
        dateLabel.setText(res.getString("Date"));
    	timeLabel.setText(res.getString("Time"));
    	countryLabel.setText(res.getString("Choose_locale"));
    	zoneLabel.setText(res.getString("Time_Zone"));
    	datestylelLabel.setText(res.getString("Date_Style"));
    	timestyleLabel.setText(res.getString("Time_Style"));
    	
    	repaint();//make sure the date and time being renew
    }
    
    private class TimerListener implements ActionListener{
    	public void actionPerformed(ActionEvent e){
			DateFormat Dateformatter = DateFormat.getDateInstance(datestyleBox.getSelectedIndex(), getLocale());//get the chose style and country for date
			DateFormat Timeformatter = DateFormat.getTimeInstance(timestyleBox.getSelectedIndex(), getLocale());//get the chose style and country for time
			Dateformatter.setTimeZone(timeZone);//set the zone for time(date)
			Timeformatter.setTimeZone(timeZone);//set the zone for time
			dateField.setText(Dateformatter.format(calendar.getTime()));//get current time(date)
			timeField.setText(Timeformatter.format(calendar.getTime()));//get current time
    	}
    }
}
