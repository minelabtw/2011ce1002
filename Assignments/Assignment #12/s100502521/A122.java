package a12.s100502521;

import java.awt.GridLayout;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.swing.*;

public class A122 extends JApplet implements ItemListener
{
	private JLabel date,time,chooseLocal,timeZone,dataStyle,timeStyle;
	private ResourceBundle res=ResourceBundle.getBundle("MyResource");//取得語系資源
	private Locale locale=Locale.getDefault();//目前地區
	private Locale[] locales=new Locale[4];
	private DateFormat df;//
	private Timer timer=new Timer(1000,new TimerListener());//每一秒改一次時間
	private TextField dis_date=new TextField();
	private TextField dis_time=new TextField();
	private JComboBox comboLocale=new JComboBox();
	private JComboBox comboZone=new JComboBox();
	private JComboBox combo_dateStyle=new JComboBox();
	private JComboBox combo_timeStyle=new JComboBox();
	private String[] timeZones=TimeZone.getAvailableIDs();
	private TimeZone timezone=TimeZone.getDefault();//時區
	private int dis_dataStyle,dis_timeStyle;
	public A122()
	{
		setLayout(new GridLayout(3,4,0,0));
		date=new JLabel(res.getString("Date"));
		time=new JLabel(res.getString("Time"));
		chooseLocal=new JLabel(res.getString("Choose_locale"));
		timeZone=new JLabel(res.getString("Time_Zone"));
		dataStyle=new JLabel(res.getString("Date_Style"));
		timeStyle=new JLabel(res.getString("Time_Style"));
		locales[0]=Locale.CHINESE;
		locales[1]=Locale.ENGLISH;
		locales[2]=Locale.FRANCE;
		locales[3]=Locale.JAPANESE;
		for(int i=0;i<locales.length;i++)
		{
			comboLocale.addItem(locales[i].getDisplayName());
		}
		for(int i=0;i<timeZones.length;i++)
		{
			comboZone.addItem(timeZones[i]);
		}
		combo_dateStyle.addItem("FULL");
		combo_dateStyle.addItem("LONG");
		combo_dateStyle.addItem("MEDIUM");
		combo_dateStyle.addItem("SHORT");
		combo_timeStyle.addItem("FULL");
		combo_timeStyle.addItem("LONG");
		combo_timeStyle.addItem("MEDIUM");
		combo_timeStyle.addItem("SHORT");
		comboZone.setSelectedItem("CST");
		comboLocale.addItemListener(this);
		comboZone.addItemListener(this);
		combo_dateStyle.addItemListener(this);
		combo_timeStyle.addItemListener(this);
		add(date);
		add(dis_date);
		add(time);
		add(dis_time);
		add(chooseLocal);
		add(comboLocale);
		add(timeZone);
		add(comboZone);
		add(dataStyle);
		add(combo_dateStyle);
		add(timeStyle);
		add(combo_timeStyle);
		timer.start();
	}
	public void updateStrings()//更換語系
	{
		res=ResourceBundle.getBundle("MyResource",locale);
		date.setText(res.getString("Date"));
		time.setText(res.getString("Time"));
		chooseLocal.setText(res.getString("Choose_locale"));
		timeZone.setText(res.getString("Time_Zone"));
		dataStyle.setText(res.getString("Date_Style"));
		timeStyle.setText(res.getString("Time_Style"));
		repaint();
	}
	public void itemStateChanged(ItemEvent arg0) 
	{
		if(arg0.getSource()==comboLocale)
		{
			locale=locales[comboLocale.getSelectedIndex()];
		}
		else if(arg0.getSource()==comboZone)
		{
			timezone=TimeZone.getTimeZone(timeZones[comboZone.getSelectedIndex()]);
		}
		else if(arg0.getSource()==combo_dateStyle)
		{
			switch(combo_dateStyle.getSelectedIndex())
			{
			case 0:
				dis_dataStyle=DateFormat.FULL;
				break;
			case 1:
				dis_dataStyle=DateFormat.LONG;
				break;
			case 2:
				dis_dataStyle=DateFormat.MEDIUM;
				break;
			case 3:
				dis_dataStyle=DateFormat.SHORT;
				break;
			}
		}
		else if(arg0.getSource()==combo_timeStyle)
		{
			switch(combo_timeStyle.getSelectedIndex())
			{
			case 0:
				dis_timeStyle=DateFormat.FULL;
				break;
			case 1:
				dis_timeStyle=DateFormat.LONG;
				break;
			case 2:
				dis_timeStyle=DateFormat.MEDIUM;
				break;
			case 3:
				dis_timeStyle=DateFormat.SHORT;
				break;
			}
		}
		updateStrings();
	}
	private class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent arg0) 
		{
			Calendar calendar=new GregorianCalendar(timezone,locale);//時間
			df=DateFormat.getDateInstance(dis_dataStyle, locale);//DataFormat 轉換格式
			df.setTimeZone(timezone);//設定時區
			dis_date.setText(df.format(calendar.getTime()));//設定日期
			df=DateFormat.getTimeInstance(dis_timeStyle, locale);
			df.setTimeZone(timezone);
			dis_time.setText(df.format(calendar.getTime()));//設定時間
		}
	}
}
