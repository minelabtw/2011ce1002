package a12.s100502521;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JToolBar;

public class A121 extends JApplet 
{
	private JMenuBar menubar;//上面的Menu
	private JMenu menu;//按下去裡面的Menu
	private JToolBar toolbar;//工具列
	private ImageIcon []icons=new ImageIcon[5];
	private ImageIcon []pictrues=new ImageIcon[5];
	private JLabel pic;
	public A121()//初始化
	{
		setLayout(new BorderLayout());
		menubar=new JMenuBar();
		setJMenuBar(menubar);
		menu=new JMenu("country");
		menubar.add(menu);
		icons[0]=new ImageIcon(getClass().getResource("../../image/Canada_Icon.gif"));
		icons[1]=new ImageIcon(getClass().getResource("../../image/China_Icon.gif"));
		icons[2]=new ImageIcon(getClass().getResource("../../image/England_Icon.gif"));
		icons[3]=new ImageIcon(getClass().getResource("../../image/German_Icon.gif"));
		icons[4]=new ImageIcon(getClass().getResource("../../image/USA_Icon.gif"));
		pictrues[0]=new ImageIcon(getClass().getResource("../../image/Canada.gif"));
		pictrues[1]=new ImageIcon(getClass().getResource("../../image/China.gif"));
		pictrues[2]=new ImageIcon(getClass().getResource("../../image/England.gif"));
		pictrues[3]=new ImageIcon(getClass().getResource("../../image/German.gif"));
		pictrues[4]=new ImageIcon(getClass().getResource("../../image/USA.gif"));
		Action []actions=new MyAction[5];
		actions[0]=new MyAction("Canada",icons[0]);
		actions[1]=new MyAction("China",icons[1]);
		actions[2]=new MyAction("England",icons[2]);
		actions[3]=new MyAction("German",icons[3]);
		actions[4]=new MyAction("USA",icons[4]);
		toolbar=new JToolBar("Tool Bar");
		toolbar.setLayout(new GridLayout(5,0,0,0));
		for(int i=0;i<5;i++)
		{
			menu.add(actions[i]);
			toolbar.add(actions[i]);
		}
		add(toolbar,BorderLayout.EAST);
		pic=new JLabel(pictrues[0]);
		add(pic,BorderLayout.CENTER);
	}
	private class MyAction extends AbstractAction//Action
	{
		String name;
		public MyAction(String name,Icon icon)
		{
			super(name,icon);
			this.name=name;
		}
		public void actionPerformed(ActionEvent arg0) 
		{
			if(name.equals("Canada"))
			{
				pic.setIcon(pictrues[0]);
			}
			else if(name.equals("China"))
			{
				pic.setIcon(pictrues[1]);
			}
			else if(name.equals("England"))
			{
				pic.setIcon(pictrues[2]);
			}
			else if(name.equals("German"))
			{
				pic.setIcon(pictrues[3]);
			}
			else if(name.equals("USA"))
			{
				pic.setIcon(pictrues[4]);
			}
		}
		
	}
}
