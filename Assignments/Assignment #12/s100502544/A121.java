package a12.s100502544;


import java.awt.BorderLayout;
import java.awt.Desktop.Action;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;



public class A121 extends JApplet{
	
	
	ImageIcon canadaIcon=new ImageIcon("image/Canada_Icon.gif");
	ImageIcon USAIcon=new ImageIcon("image/USA_Icon.gif");
	ImageIcon ChinaIcon=new ImageIcon("image/China_Icon.gif");
	ImageIcon EnglandIcon=new ImageIcon("image/England_Icon.gif");
	ImageIcon GermanIcon=new ImageIcon("image/German_Icon.gif");
	//menuitem picture
	
	
	ImageIcon CanadaIcon2=new ImageIcon("image/Canada.gif");
	ImageIcon USAIcon2=new ImageIcon("image/USA.gif");
	ImageIcon ChinaIcon2=new ImageIcon("image/China.gif");
	ImageIcon EnglandIcon2=new ImageIcon("image/England.gif");
	ImageIcon GermanIcon2=new ImageIcon("image/German.gif");
	//點到 menuitem 之後顯示的圖片
	
	
	
	JLabel L=new JLabel();
	//照片放到Label上
	
	
	public A121(){
		
		//宣告  action                    new 物件(Sting,icon)
		javax.swing.Action canadaAction=new MyAction("Canada", canadaIcon);
		javax.swing.Action usaAction=new MyAction("USA", USAIcon);
		javax.swing.Action chinaAction=new MyAction("China",ChinaIcon );
		javax.swing.Action englandAction=new MyAction("England",EnglandIcon);
		javax.swing.Action germanAction=new MyAction("German",GermanIcon );
		
		
		
		//JMenuBar
		JMenuBar jmbBar=new JMenuBar();
		JMenu menu=new JMenu("Menu");
		setJMenuBar(jmbBar);
		jmbBar.add(menu);
		menu.add(canadaAction);
		menu.addSeparator();//分割線
		menu.add(usaAction);
		menu.addSeparator();
		menu.add(englandAction);
		menu.addSeparator();
		menu.add(chinaAction);
		menu.addSeparator();
		menu.add(germanAction);
		add(L,BorderLayout.CENTER);
		
		
		//JtoolBar
		JToolBar toolBar=new JToolBar(JToolBar.VERTICAL);
		toolBar.add(canadaAction);
		toolBar.add(usaAction);
		toolBar.add(chinaAction);
		toolBar.add(englandAction);
		toolBar.add(germanAction);
		add(toolBar,BorderLayout.EAST);
		
		
		
	}
	
	private	class MyAction extends AbstractAction{
		String name;
		
		public MyAction(String name,Icon icon) {
			super(name,icon);//繼承上一個class的東西
			this.name=name;
			
		}

		public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
			
			//如果點到(name.equals("string"))這個string的名稱就做什麼事情
			//L 是一個JLabel  用來放"點"不同選項時  改變不同的照片
			if(name.equals("Canada")){
				L.setIcon(CanadaIcon2);//L 是一個JLabel  用來放"點"不同選項時  改變不同的照片
				
			}
			else if(name.equals("USA")){
				L.setIcon(USAIcon2);
				
			}
			else if(name.equals("England")){
				L.setIcon(EnglandIcon2);
				
			}
			else if(name.equals("China")){
				L.setIcon(ChinaIcon2);
				
			}
			else if(name.equals("German")){
				L.setIcon(GermanIcon2);
				
			}
		}
		
	}
}

