package a12.s100502544;


import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.util.*;


import javax.swing.JApplet;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;


public class A122 extends JApplet{
	
	Timer timer = new Timer(1000, new TimerListener());
	TimeZone timezone = TimeZone.getTimeZone("Etc/GMT+12");
	String[] timeZones = TimeZone.getAvailableIDs();
	String[] Styles = {"FULL", "LONG", "MEDIUM", "SHORT"};
	int[] formatstyle = {DateFormat.FULL, DateFormat.LONG, DateFormat.MEDIUM, DateFormat.SHORT};
	
	int dateStyle; 
	int timeStyle;
	//4個選單  JComboBox
	private JComboBox languagelocales=new JComboBox();
	private JComboBox timezoneslocales=new JComboBox();
	private JComboBox timestylelocales=new JComboBox();
	private JComboBox datestylelocales=new JComboBox();
	
	private String[] availableTimeZones=TimeZone.getAvailableIDs();
	
	
	
	//不要new
	ResourceBundle resourceBundle = ResourceBundle.getBundle("MyResource");
	//6個JLabel
	JLabel timeJLabel=new JLabel(resourceBundle.getString("Time"));
	JLabel dateJLabel=new JLabel(resourceBundle.getString("Date"));
	JLabel chooselocaleJLabel=new JLabel(resourceBundle.getString("Choose_locale"));
	JLabel timezoneJLabel=new JLabel(resourceBundle.getString("Time_Zone"));
	JLabel datestyleJLabel=new JLabel(resourceBundle.getString("Date_Style"));
	JLabel timestyleJLabel=new JLabel(resourceBundle.getString("Time_Style"));
	
	
	
	
	//2個JTextField
	JTextField timeJTextField=new JTextField(10);
	JTextField dateJTextField=new JTextField(10);
	
	
	//初始化 Locale  讓選單一開始是有東西
	private Locale locale=Locale.getDefault();
	//4種Locale
	private Locale chineselocale=Locale.CHINESE;
	private Locale englishlocale=Locale.ENGLISH;
	private Locale japanlocale=Locale.JAPAN;
	private Locale frenchlocale=Locale.FRENCH;
	//宣告 Locale的陣列 (放選單下的內容) {Locale.CHINESE,Locale.CHINESE,Locale.JAPAN,Locale.FRENCH}
	Locale locales[]={chineselocale,englishlocale,japanlocale,frenchlocale};
	//method 迴圈 顯示 languagelocales 的選單
	
	
	public void initialize_languagelocales_combobox(){
		for(int i=0;i<locales.length;i++){
			languagelocales.addItem(locales[i].getDisplayName());
		}
	}
	
	
	
//	private void setAvailableTimeZones(){
//		Arrays.sort(availableTimeZones);
//		for(int i=0;i<availableTimeZones.length;i++){
//			timestylelocales.addItem(availableTimeZones[i]);
//		}
//	}
	
	
	
	
	
	//排板   method
	public void init(){
		
//		clock.setTimeZone(TimeZone.getTimeZone(availableTimeZones[timezoneslocales.getSelectedIndex()]));
		setSize(550, 300);
		setdatestyle(DateFormat.FULL);
		settimestyle(DateFormat.FULL);
		
		
		
		
//		dateJTextField.setOpaque(true);
//		timeJTextField.setOpaque(true);
//		dateJTextField.setEditable(false);
//		timeJTextField.setEditable(false);
		
		JPanel panel=new JPanel();
		panel.setLayout(new GridLayout(3,4));
		panel.add(dateJLabel);
		panel.add(dateJTextField);
		panel.add(timeJLabel);
		panel.add(timeJTextField);/////////****
		panel.add(chooselocaleJLabel);
		panel.add(languagelocales);
		initialize_languagelocales_combobox();
		panel.add(timezoneJLabel);
		panel.add(timezoneslocales);
		addTimeZone();
//		setAvailableTimeZones();
		panel.add(datestyleJLabel);
		panel.add(datestylelocales);
		addDatestyle();
		panel.add(timestyleJLabel);
		panel.add(timestylelocales);
		addTimestyle();
		add(panel);
		timer.start();
		
		
		// 按到 languagelocales的選單 要做的事 (ActionListener)
		languagelocales.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				locale=locales[languagelocales.getSelectedIndex()];
				updateString();
			}
		});
		
		
		timezoneslocales.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				timezone = TimeZone.getTimeZone(timeZones[timezoneslocales.getSelectedIndex()]);
				
			}
		});
		
		datestylelocales.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				setdatestyle(formatstyle[datestylelocales.getSelectedIndex()]);
			}
		});
		timestylelocales.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				settimestyle(formatstyle[timestylelocales.getSelectedIndex()]);
			}
		});
		
		
		
//		timezoneslocales.addActionListener(new ActionListener() {
			
//			@Override
//			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
//				clock.setTimeZone(TimeZone.getTimeZone(availableTimeZones[timestylelocales.getSelectedIndex()]));
//				
//			}
//		});
		
	}




	private void updateString(){
		resourceBundle =ResourceBundle.getBundle("MyResource",locale);
		timeJLabel.setText(resourceBundle.getString("Time"));
		dateJLabel.setText(resourceBundle.getString("Date"));
		chooselocaleJLabel.setText(resourceBundle.getString("Choose_locale"));
		timezoneJLabel.setText(resourceBundle.getString("Time_Zone"));
		datestyleJLabel.setText(resourceBundle.getString("Date_Style"));
		timestyleJLabel.setText(resourceBundle.getString("Time_Style"));
		
		
		repaint();//重畫 
		
	}
	
	class TimerListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			Calendar calendar = new GregorianCalendar(timezone, getLocale());
			DateFormat dateformat = DateFormat.getDateInstance(getdatestyle(), getLocale());
			dateformat.setTimeZone(timezone);
			DateFormat timeformat = DateFormat.getTimeInstance(gettimestyle(), getLocale());
			timeformat.setTimeZone(timezone);
			dateJTextField.setText(dateformat.format(calendar.getTime()));
			timeJTextField.setText(timeformat.format(calendar.getTime()));
		}
	}

	public void setdatestyle(int Datestyle){
		dateStyle = Datestyle;
	}
	
	public void settimestyle(int Timestyle){
		timeStyle = Timestyle;
	}
	
	private int getdatestyle() {
		// TODO Auto-generated method stub
		return dateStyle;
	}

	private int gettimestyle() {
		// TODO Auto-generated method stub
		return timeStyle;
	}
	public void addTimeZone(){
		for(int i = 0; i < timeZones.length; i++){
			timezoneslocales.addItem(timeZones[i]);
		}
	}
	
	public void addDatestyle(){
		for(int i = 0; i < Styles.length; i++){
			datestylelocales.addItem(Styles[i]+"");
		}
	}
	
	public void addTimestyle(){
		for(int i = 0; i < Styles.length; i++){
			timestylelocales.addItem(Styles[i]+"");
		}
	}
	
	
	public A122(){
		
	}
}
