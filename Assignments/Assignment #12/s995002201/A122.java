package a12.s995002201;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JApplet;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class A122  extends JApplet implements ActionListener
{
	private JLabel date = new JLabel("日期");
	private JLabel time = new JLabel("時間");
	private JLabel country = new JLabel("選擇國家");
	private JLabel state = new JLabel("時區");
	private JLabel datestyle = new JLabel("日期風格");
	private JLabel timestyle = new JLabel("時間風格");
	private JTextField datesee = new JTextField(10);
	private JTextField timesee = new JTextField(10);
	private JComboBox jcountry = new JComboBox(new Object[]{"中文(台灣)","日文(日本)","英文","法文"});
	private JComboBox jstate = new JComboBox(new Object[]{"Ect/GMT+12","Ect/GMT+8","Ect/GMT+4","Ect/GMT+0"});
	private JComboBox jdatestyle = new JComboBox(new Object[]{"Full","Half"});
	private JComboBox jtimestyle = new JComboBox(new Object[]{"Full","Half"});
	public A122()
	{
		JPanel p1 = new JPanel(new FlowLayout());
		setLayout(new BorderLayout());
		p1.add(date);
		p1.add(datesee);
		p1.add(time);
		p1.add(timesee);
		p1.add(country);
		p1.add(jcountry);
		p1.add(state);
		p1.add(jstate);
		p1.add(datestyle);
		p1.add(jdatestyle);
		p1.add(timestyle);
		p1.add(jtimestyle);
		add(p1,BorderLayout.NORTH);
	}
	@Override
	public void actionPerformed(ActionEvent e)
	{
		// TODO Auto-generated method stub	
	}
}
