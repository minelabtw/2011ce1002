package a12.s100502004;



import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JToolBar;

public class A121 extends JApplet{//use to initial the things 
	private JPanel buttonpPanel = new JPanel();
	private FlowLayout flowLayout = new FlowLayout();
	private JLabel show = new JLabel();
	ImageIcon smallcanadaIcon = new ImageIcon(getClass().getResource("image/Canada_Icon.gif"));
	ImageIcon smallchinaIcon = new ImageIcon(getClass().getResource("image/China_Icon.gif"));
	ImageIcon smallenglandIcon = new ImageIcon(getClass().getResource("image/England_Icon.gif"));
	ImageIcon smallgermanIcon = new ImageIcon(getClass().getResource("image/German_Icon.gif"));
	ImageIcon smallusaIcon = new ImageIcon(getClass().getResource("image/USA_Icon.gif"));
	ImageIcon bigcanadaIcon = new ImageIcon(getClass().getResource("image/Canada.gif"));
	ImageIcon bigchinaIcon = new ImageIcon(getClass().getResource("image/China.gif"));
	ImageIcon bigenglandIcon = new ImageIcon(getClass().getResource("image/England.gif"));
	ImageIcon biggermanIcon = new ImageIcon(getClass().getResource("image/German.gif"));
	ImageIcon bigusaIcon = new ImageIcon(getClass().getResource("image/USA.gif"));
	Action canada = new MyAction("Canada" , smallcanadaIcon);
	Action china = new MyAction("China" , smallchinaIcon);
	Action england = new MyAction("England" , smallenglandIcon);
	Action german = new MyAction("German" , smallgermanIcon);
	Action usa = new MyAction("USA" , smallusaIcon);
	
	public A121() {		
		
		JMenu countryMenu = new JMenu("Country");//use to initial the LMenu 
		JMenuBar menuBar = new JMenuBar();
		countryMenu.add(canada);
		countryMenu.add(china);
		countryMenu.add(england);
		countryMenu.add(german);
		countryMenu.add(usa);
		menuBar.add(countryMenu);
		
		
		
		JToolBar toolBar = new JToolBar(JToolBar.VERTICAL);//use to initial the toolbar 
		toolBar.add(canada);
		toolBar.add(china);
		toolBar.add(england);
		toolBar.add(german);
		toolBar.add(usa);
		
		add(menuBar,BorderLayout.NORTH);
		add(toolBar,BorderLayout.EAST);
		add(show,BorderLayout.CENTER);
		
		
	}
	
	
	public class MyAction extends AbstractAction{
		
		String name; 
		
		MyAction(String country , ImageIcon imageIcon ) {
			// TODO Auto-generated constructor stub			
			super(country,imageIcon);
			name = country;
			
			
		}	
		
		
		public void actionPerformed(ActionEvent e){
			if(name.equals("Canada")){//use the nameString to choose which picture should show on the JLabel
				show.setIcon(bigcanadaIcon);
			}
			else if(name.equals("China")){
				show.setIcon(bigchinaIcon);
			}
			else if(name.equals("England")){
				show.setIcon(bigenglandIcon);			
						}
			else if(name.equals("German")){
				show.setIcon(biggermanIcon);
			}
			else if(name.equals("USA")){
				show.setIcon(bigusaIcon);
			}			
				
		}
		
		
		
		
	}
	
	
	
	
	
	
	
}
