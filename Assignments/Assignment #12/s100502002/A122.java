package a12.s100502002;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.util.*;

public class A122 extends JApplet 
{
	private JTextField showdate  = new JTextField("", JLabel.CENTER);
	private JTextField showTime = new JTextField("", JLabel.CENTER);
	private JLabel jlbDate, jlbTime, jlbLocale, jleTimeZone, jlbDateStyle, jlbTimeStyle;
	private JComboBox jcblocale  = new JComboBox();
	private JComboBox jcbtimeZone = new JComboBox();
	private JComboBox dateStyle ;
	private JComboBox timeStyle;
	private TimeZone timeZone = TimeZone.getDefault();
	private Calendar calender = new GregorianCalendar(timeZone, getLocale());
	private DateFormat Dateformatter = DateFormat.getDateInstance(DateFormat.LONG, getLocale());
	private DateFormat Timeformatter = DateFormat.getTimeInstance(DateFormat.LONG, getLocale());
	private Locale[] locale = Locale.getAvailableLocales();
	private String[] availableTimeZone = TimeZone.getAvailableIDs();
	private ResourceBundle resource = ResourceBundle.getBundle("Language/MyResource",getLocale());
	private String[] selections = {"Full", "Long", "Medium", "Short"};
	public A122() 
	{
		setlocale();
		settimezone();
		setdatestyle();
		settimestyle();
		
		Dateformatter.setTimeZone(timeZone);
		Timeformatter.setTimeZone(timeZone);//設定時區
		
		
		setLayout(new GridLayout(3, 2));
		
		JPanel p1 = new JPanel(); //加入第一排元素(時間日期)
		jlbDate = new JLabel(resource.getString("Date"));
		p1.add(jlbDate);
		p1.add(showdate);
		
		jlbTime = new JLabel(resource.getString("Time")); 
		p1.add(jlbTime);
		p1.add(showTime);
		add(p1);
	
		JPanel p2 = new JPanel(); //加入第二排元素(時區語言)
		jlbLocale = new JLabel(resource.getString("Choose_locale"));
		p2.add(jlbLocale);
		p2.add(jcblocale);
		
		jleTimeZone = new JLabel(resource.getString("Time_Zone"));
		p2.add(jleTimeZone);
		p2.add(jcbtimeZone);
		add(p2);
		
		JPanel p3 = new JPanel(); //加入第三排元素(樣式)
		jlbDateStyle = new JLabel(resource.getString("Date_Style"));
		p3.add(jlbDateStyle);
		p3.add(dateStyle);
		
		jlbTimeStyle = new JLabel(resource.getString("Time_Style")); 
		p3.add(jlbTimeStyle);
		p3.add(timeStyle);
		add(p3);
		
		update();
	}
	public void setlocale()//設定語言
	{
		for(int i=0; i<locale.length; i++) 
		{ 
			jcblocale.addItem(locale[i].getDisplayName() + " " + locale[i].toString());
		}
		jcblocale.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
				setLocale(locale[jcblocale.getSelectedIndex()]);
				update();
			}
		});
	}
	public void setdatestyle()//設定日期樣式
	{
		dateStyle = new JComboBox(selections); 
		dateStyle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				update();
			}
		});
	}
	public void settimestyle()//設定時間樣式
	{
		
		timeStyle = new JComboBox(selections); 
		timeStyle.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent arg0) {
				update();
			}
		});
	}
	public void settimezone()//設定時區
	{
		Arrays.sort(availableTimeZone); 
		for(int i=0; i<availableTimeZone.length; i++) { 
			jcbtimeZone.addItem(availableTimeZone[i]);
		}
		jcbtimeZone.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) {
				timeZone = TimeZone.getTimeZone(availableTimeZone[jcbtimeZone.getSelectedIndex()]);
				update();
			}
		});
		
	}
	public void update() //更新內容
	{ 
		showTime.setText(Timeformatter.format(calender.getTime()));
		jlbDate.setText(resource.getString("Date"));
		jlbTime.setText(resource.getString("Time"));
		jlbLocale.setText(resource.getString("Choose_locale"));
		jleTimeZone.setText(resource.getString("Time_Zone"));
		Dateformatter = DateFormat.getDateInstance(dateStyle.getSelectedIndex(), getLocale());
		Timeformatter = DateFormat.getTimeInstance(timeStyle.getSelectedIndex(), getLocale());
		Dateformatter.setTimeZone(timeZone);
		Timeformatter.setTimeZone(timeZone);
		showdate.setText(Dateformatter.format(calender.getTime()));
		
	}	
	
}
