package a12.s100502002;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
public class A121 extends JApplet
{
	
	private JPanel panel = new JPanel();
	public JLabel show = new JLabel();
	
	ImageIcon Canada = new ImageIcon("image/Canada.gif");
	ImageIcon CanadaIcon = new ImageIcon("image/Canada_Icon.gif");
	ImageIcon China = new ImageIcon("image/China.gif");
	ImageIcon ChinaIcon = new ImageIcon("image/China_Icon.gif");
	ImageIcon England = new ImageIcon("image/England.gif");
	ImageIcon EnglandIcon = new ImageIcon("image/England_Icon.gif");
	ImageIcon German = new ImageIcon("image/German.gif");
	ImageIcon GermanIcon = new ImageIcon("image/German_Icon.gif");
	ImageIcon USA = new ImageIcon("image/USA.gif");
	ImageIcon USAIcon = new ImageIcon("image/USA_Icon.gif");//宣告圖片
	
	public A121() 
	{
		/* 文字、圖片、快捷鍵 全都寫在action*/
		Action CanadaAction = new CountryAction("Canada", CanadaIcon, "Know more about Canada", 
				new Integer(KeyEvent.VK_C), KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK));
		Action ChinaAction = new CountryAction("China", ChinaIcon, "Know more about China", 
				new Integer(KeyEvent.VK_H), KeyStroke.getKeyStroke(KeyEvent.VK_H, ActionEvent.CTRL_MASK));
		Action EnglandAction = new CountryAction("England", EnglandIcon, "Know more about England", 
				new Integer(KeyEvent.VK_E), KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));
		Action GermanAction = new CountryAction("German", GermanIcon, "Know more about German", 
				new Integer(KeyEvent.VK_G), KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.CTRL_MASK));
		Action USAAction = new CountryAction("USA", USAIcon, "Know more about USA", 
				new Integer(KeyEvent.VK_U), KeyStroke.getKeyStroke(KeyEvent.VK_U, ActionEvent.CTRL_MASK));
		/*menu*/
		JMenuBar mbar = new JMenuBar();
		JMenu mcountry = new JMenu("Country");
		setJMenuBar(mbar);
		mbar.add(mcountry);
		
		mcountry.add(CanadaAction);
		mcountry.add(ChinaAction);
		mcountry.add(EnglandAction);
		mcountry.add(GermanAction);
		mcountry.add(USAAction);
		/*tool bar*/
		JToolBar tbar = new JToolBar(JToolBar.VERTICAL);
		tbar.add(CanadaAction);
		tbar.add(ChinaAction);
		tbar.add(EnglandAction);
		tbar.add(GermanAction);
		tbar.add(USAAction);
		
		panel.add(show);
		
		add(tbar, BorderLayout.WEST);
		add(panel, BorderLayout.CENTER);
	}
	
	private class CountryAction extends AbstractAction //用action 取代listener
	{
		String country;
		
		CountryAction(String countryname, Icon icon) 
		{
			super(countryname, icon);
			this.country = countryname;
		}
		
		CountryAction(String name, Icon icon, String desc, Integer mnemonic, KeyStroke accelerator) 
		{
			super(name, icon);
			putValue(Action.SHORT_DESCRIPTION, desc);
			putValue(Action.MNEMONIC_KEY, mnemonic);
			putValue(Action.ACCELERATOR_KEY, accelerator);
			this.country = name;
		}
		
		public void actionPerformed(ActionEvent e) /*就像listener 一樣 判斷要改變什麼*/
		{
			if(country.equals("Canada")) 
				show.setIcon(Canada);
			else if(country.equals("China"))
				show.setIcon(China);
			else if(country.equals("England"))
				show.setIcon(England);
			else if(country.equals("German"))
				show.setIcon(German);
			else if(country.equals("USA"))
				show.setIcon(USA);
		}
	}

}
