package a12.s995002204;

import java.applet.Applet;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.TimeZone;
import java.util.GregorianCalendar;

public class A122 extends Applet{
	
	//declare variable
	private ResourceBundle res = ResourceBundle.getBundle("MyResource");
	
	private JLabel date = new JLabel(res.getString("Date"));
	private JTextField dateDisplay = new JTextField();
	
	private JLabel time = new JLabel(res.getString("Time"));
	private JTextField timeDisplay = new JTextField();
	
	private JLabel country = new JLabel(res.getString("Choose_locale"));
	private JComboBox selectCountry = new JComboBox();
	
	private JLabel timeGap = new JLabel(res.getString("Time_Zone"));
	private String zone[] = {"EST","Japan","Europe/Paris","Europe/London"};
	private JComboBox selectZone = new JComboBox(zone);
	
	private JLabel dateStyle = new JLabel(res.getString("Date_Style"));
	private String tempA[] = {"SHORT","MEDIUM","LONG","FULL"};
	private JComboBox selectdateStyle = new JComboBox(tempA);
	
	private JLabel timeStyle = new JLabel(res.getString("Time_Style"));
	private JComboBox selecttimeStyle = new JComboBox(tempA);
	
	private Locale locale = Locale.getDefault();
	private Locale[] locales = Calendar.getAvailableLocales();
	
	private TimeZone timezone = TimeZone.getTimeZone("EST");
	
	public A122()
	{
		setLayout(new GridLayout(3,4));
		add(date);
		add(dateDisplay);
		add(time);
		add(timeDisplay);
		add(country);
		add(selectCountry);
		add(timeGap);
		add(selectZone);
		add(dateStyle);
		add(selectdateStyle);
		add(timeStyle);
		add(selecttimeStyle);
		
		for (int i = 0; i < locales.length; i++)
		{
			selectCountry.addItem(locales[i].getDisplayName());
		}
		
		//select country
		selectCountry.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				locale = locales[selectCountry.getSelectedIndex()];
				updateStrings();
			}
		});
		
		
		//select time zone
		selectZone.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if(selectZone.getSelectedIndex()==0)
				{
					timezone = timezone.getTimeZone("EST");
				}
				else if(selectZone.getSelectedIndex()==1)
				{
					timezone = timezone.getTimeZone("Japan");
				}
				else if(selectZone.getSelectedIndex()==2)
				{
					timezone = timezone.getTimeZone("Europe/Paris");
				}
				else
				{
					timezone = timezone.getTimeZone("Europe/London");
				}
			}
		});
		
		//select date style
		selectdateStyle.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				Date date = new Date();
				if(selectdateStyle.getSelectedIndex()==0)
				{
					DateFormat shortFormat =  DateFormat.getDateInstance(DateFormat.SHORT, locale);
					dateDisplay.setText(shortFormat.format(date));
				}
				else if(selectdateStyle.getSelectedIndex()==1)
				{
					DateFormat mediumFormat =  DateFormat.getDateInstance(DateFormat.MEDIUM, locale); 
					dateDisplay.setText(mediumFormat.format(date));
				}
				else if(selectdateStyle.getSelectedIndex()==2)
				{
					DateFormat longFormat =  DateFormat.getDateInstance(DateFormat.LONG, locale); 
					dateDisplay.setText(longFormat.format(date));
				}
				else
				{
					DateFormat fullFormat =  DateFormat.getDateInstance(DateFormat.FULL, locale); 
					dateDisplay.setText(fullFormat.format(date));
				}
			}
		});
		
		//select time style
		selecttimeStyle.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				Date date = new Date();
				if(selecttimeStyle.getSelectedIndex()==0)
				{
					DateFormat shortFormat =  DateFormat.getTimeInstance(DateFormat.SHORT, locale);
					shortFormat.setTimeZone(timezone);
					timeDisplay.setText(shortFormat.format(date));
				}
				else if(selecttimeStyle.getSelectedIndex()==1)
				{
					DateFormat mediumFormat =  DateFormat.getTimeInstance(DateFormat.MEDIUM, locale); 
					mediumFormat.setTimeZone(timezone);
					timeDisplay.setText(mediumFormat.format(date));
				}
				else if(selecttimeStyle.getSelectedIndex()==2)
				{
					DateFormat longFormat =  DateFormat.getTimeInstance(DateFormat.LONG, locale); 
					longFormat.setTimeZone(timezone);
					timeDisplay.setText(longFormat.format(date));
				}
				else
				{
					DateFormat fullFormat =  DateFormat.getTimeInstance(DateFormat.FULL, locale);
					fullFormat.setTimeZone(timezone);
					timeDisplay.setText(fullFormat.format(date));
				}
			}
		});
		
	}
	
	//update String
	private void updateStrings()
	{
		res = ResourceBundle.getBundle("MyResource", locale);
		date.setText(res.getString("Date"));
		time.setText(res.getString("Time"));
		country.setText(res.getString("Choose_locale"));
		timeGap.setText(res.getString("Time_Zone"));
		dateStyle.setText(res.getString("Date_Style"));
		timeStyle.setText(res.getString("Time_Style"));
		repaint();
	}
	
	public static void main(String args[])
	{
		JFrame frame = new JFrame();
		A122 applet = new A122();
		
		frame.add(applet);
		frame.setSize(800,600);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
