package a12.s995002204;

import java.applet.Applet;
import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;

public class A121 extends Applet{

	//declare variable
	private JToolBar tool = new JToolBar();
	private ImageIcon imageCanada = new ImageIcon(getClass().getResource("/image/Canada_Icon.gif"));
	private ImageIcon imageChina = new ImageIcon(getClass().getResource("/image/China_Icon.gif"));
	private ImageIcon imageEngland = new ImageIcon(getClass().getResource("/image/England_Icon.gif"));
	private ImageIcon imageGerman = new ImageIcon(getClass().getResource("/image/German_Icon.gif"));
	private ImageIcon imageUSA = new ImageIcon(getClass().getResource("/image/USA_Icon.gif"));
	
	private JLabel label  = new JLabel();
	ImageIcon can = new ImageIcon(getClass().getResource("/image/Canada.gif"));
	ImageIcon chi = new ImageIcon(getClass().getResource("/image/China.gif"));
	ImageIcon eng = new ImageIcon(getClass().getResource("/image/England.gif"));
	ImageIcon ger = new ImageIcon(getClass().getResource("/image/German.gif"));
	ImageIcon us = new ImageIcon(getClass().getResource("/image/USA.gif"));
	
	private FlowLayout flowLayout = new FlowLayout();
	private JMenuBar menubar = new JMenuBar();
	private JMenu country = new JMenu("Country");
	
	//constructor
	public A121()
	{
		Action selectCanada = new MyAction("Canada",imageCanada);
		Action selectChina = new MyAction("China",imageChina);
		Action selectEngland = new MyAction("England",imageEngland);
		Action selectGerman = new MyAction("German",imageGerman);
		Action selectUSA = new MyAction("USA",imageUSA);
		
		country.add(selectCanada);
		country.add(selectChina);
		country.add(selectEngland);
		country.add(selectGerman);
		country.add(selectUSA);
		menubar.add(country);
		
		tool.add(selectCanada);
		tool.add(selectChina);
		tool.add(selectEngland);
		tool.add(selectGerman);
		tool.add(selectUSA);
		tool.setOrientation(JToolBar.VERTICAL);
		tool.setFloatable(true);
		
		add(menubar,BorderLayout.NORTH);
		add(label,BorderLayout.CENTER);
		add(tool,BorderLayout.EAST);
	}
	
	//Action Listener
	private class MyAction extends AbstractAction{
		
		String name;
		
		MyAction(String name,Icon icon)
		{
			super(name,icon);
			this.name = name;
		}
		
		public void actionPerformed(ActionEvent e)
		{
			if(name.equals("Canada"))
			{
				label.setIcon(can);
			}
			else if(name.equals("China"))
			{
				label.setIcon(chi);
			}
			else if(name.equals("England"))
			{
				label.setIcon(eng);
			}
			else if(name.equals("German"))
			{
				label.setIcon(ger);
			}
			else
			{
				label.setIcon(us);
			}
		}
		
	}
	
	//main
	public static void main(String args[])
	{
		JFrame frame = new JFrame();
		A121 applet = new A121();
		
		frame.add(applet);
		frame.setSize(800,600);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
