package a12.s100502507;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.util.*;

public class A122 extends JApplet {
	public A122() {
		jcblocale = new JComboBox(); //Selections for locales
		for(int i=0; i<locale.length; i++) { //Set combobox
			jcblocale.addItem(locale[i].getDisplayName() + " " + locale[i].toString());
		}
		jcblocale.addActionListener(new ActionListener() { //Action take place when selection changed
			public void actionPerformed(ActionEvent arg0) {
				setLocale(locale[jcblocale.getSelectedIndex()]);
				updateStrings();
			}
		});
		
		jcbtimeZone = new JComboBox(); //Selections for time zones
		Arrays.sort(availableTimeZone); //Short time zones
		for(int i=0; i<availableTimeZone.length; i++) { //Set combobox
			jcbtimeZone.addItem(availableTimeZone[i]);
		}
		jcbtimeZone.addActionListener(new ActionListener() { //Action take place when selection changed
			public void actionPerformed(ActionEvent e) {
				timeZone = TimeZone.getTimeZone(availableTimeZone[jcbtimeZone.getSelectedIndex()]);
				updateStrings();
			}
		});
		
		String[] selections = {"Full", "Long", "Medium", "Short"};
		
		jcbdateStyle = new JComboBox(selections); //Selections for styles of date
		jcbdateStyle.addActionListener(new ActionListener() { //Action take place when selection changed
			public void actionPerformed(ActionEvent arg0) {
				updateStrings();
			}
		});
		
		jcbtimeStyle = new JComboBox(selections); //Selections for styles of time
		jcbtimeStyle.addActionListener(new ActionListener() { //Action take place when selection changed
			public void actionPerformed(ActionEvent arg0) {
				updateStrings();
			}
		});
		
		jtfDate = new JTextField("", JLabel.CENTER);
		jtfTime = new JTextField("", JLabel.CENTER);
		
		formatterDate.setTimeZone(timeZone);
		formatterTime.setTimeZone(timeZone);
		
		jtfDate.setText(formatterDate.format(calender.getTime()));
		jtfTime.setText(formatterTime.format(calender.getTime()));
		
		setLayout(new GridLayout(3, 2));
		
		JPanel panel = new JPanel(); //Add word "Date" & current date
		jlbDate = new JLabel(res.getString("Date"));
		panel.add(jlbDate);
		panel.add(jtfDate);
		
		jlbTime = new JLabel(res.getString("Time")); //Add word "Time" & current time
		panel.add(jlbTime);
		panel.add(jtfTime);
		add(panel);
		
		add(panel);
		
		panel = new JPanel(); //Add word "Choose Locate" & change locale
		jlbLocale = new JLabel(res.getString("Choose_locale"));
		panel.add(jlbLocale);
		panel.add(jcblocale);
		
		jleTimeZone = new JLabel(res.getString("Time_Zone")); //Add word "Time Zone" & change time zone
		panel.add(jleTimeZone);
		panel.add(jcbtimeZone);
		add(panel);
		
		panel = new JPanel(); //Add word "Date Style" & selections of styles
		jlbDateStyle = new JLabel(res.getString("Date_Style"));
		panel.add(jlbDateStyle);
		panel.add(jcbdateStyle);
		
		jlbTimeStyle = new JLabel(res.getString("Time_Style")); //Add word "Time Style" & selections of styles
		panel.add(jlbTimeStyle);
		panel.add(jcbtimeStyle);
		add(panel);
		
		updateStrings();
	}
	
	public void updateStrings() { //Refresh all the letters
		formatterDate = DateFormat.getDateInstance(jcbdateStyle.getSelectedIndex(), getLocale());
		formatterTime = DateFormat.getTimeInstance(jcbtimeStyle.getSelectedIndex(), getLocale());
		formatterDate.setTimeZone(timeZone);
		formatterTime.setTimeZone(timeZone);
		res = ResourceBundle.getBundle("MyResource", getLocale());
		jtfDate.setText(formatterDate.format(calender.getTime()));
		jtfTime.setText(formatterTime.format(calender.getTime()));
		jlbDate.setText(res.getString("Date"));
		jlbTime.setText(res.getString("Time"));
		jlbLocale.setText(res.getString("Choose_locale"));
		jleTimeZone.setText(res.getString("Time_Zone"));
	}
	
	private JTextField jtfDate, jtfTime;
	private JLabel jlbDate, jlbTime, jlbLocale, jleTimeZone, jlbDateStyle, jlbTimeStyle;
	private JComboBox jcblocale, jcbtimeZone, jcbdateStyle, jcbtimeStyle;
	private TimeZone timeZone = TimeZone.getDefault();
	private Calendar calender = new GregorianCalendar(timeZone, getLocale());
	private DateFormat formatterDate = DateFormat.getDateInstance(DateFormat.LONG, getLocale());
	private DateFormat formatterTime = DateFormat.getTimeInstance(DateFormat.LONG, getLocale());
	private Locale[] locale = Locale.getAvailableLocales();
	private String[] availableTimeZone = TimeZone.getAvailableIDs();
	private ResourceBundle res = ResourceBundle.getBundle("MyResource");
}
