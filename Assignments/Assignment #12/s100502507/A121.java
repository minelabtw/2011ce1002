package a12.s100502507;

import java.applet.*;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class A121 extends JApplet {
	public A121() {		
		images = new ImageIcon[5]; //Load pictures
		images[0] = new ImageIcon(getClass().getResource("image/Canada.gif"));
		images[1] = new ImageIcon(getClass().getResource("image/China.gif"));
		images[2] = new ImageIcon(getClass().getResource("image/England.gif"));
		images[3] = new ImageIcon(getClass().getResource("image/German.gif"));
		images[4] = new ImageIcon(getClass().getResource("image/USA.gif"));
		
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("Options");
		//Add selections to menu
		menu.add(new MyAction("Canada", new ImageIcon(getClass().getResource("image/Canada_Icon.gif")), "Display the image of Canada", 0));
		menu.add(new MyAction("China", new ImageIcon(getClass().getResource("image/China_Icon.gif")), "Display the image of China", 1));
		menu.add(new MyAction("England", new ImageIcon(getClass().getResource("image/England_Icon.gif")), "Display the image of England", 2));
		menu.add(new MyAction("German", new ImageIcon(getClass().getResource("image/German_Icon.gif")), "Display the image of German", 3));
		menu.add(new MyAction("USA", new ImageIcon(getClass().getResource("image/USA_Icon.gif")), "Display the image of USA", 4));
		//Add menu to the bar
		menuBar.add(menu);
		
		JToolBar toolBar = new JToolBar(JToolBar.VERTICAL);
		//Add selections to tool bar
		toolBar.add(new MyAction("Canada", new ImageIcon(getClass().getResource("image/Canada_Icon.gif")), "Display the image of Canada", 0));
		toolBar.add(new MyAction("China", new ImageIcon(getClass().getResource("image/China_Icon.gif")), "Display the image of China", 1));
		toolBar.add(new MyAction("England", new ImageIcon(getClass().getResource("image/England_Icon.gif")), "Display the image of England", 2));
		toolBar.add(new MyAction("German", new ImageIcon(getClass().getResource("image/German_Icon.gif")), "Display the image of German", 3));
		toolBar.add(new MyAction("USA", new ImageIcon(getClass().getResource("image/USA_Icon.gif")), "Display the image of USA", 4));
		
		display = new JLabel();
		
		add(menuBar, BorderLayout.NORTH);
		add(toolBar, BorderLayout.EAST);
		add(display);
	}
	
	private class MyAction extends AbstractAction { //An action definded by myself to change the picture of display while clicked
		MyAction(String name, ImageIcon image, String describtion, int input) {
			super(name, image);
			this.input = input;
			putValue(Action.SHORT_DESCRIPTION, describtion);
		}
		
		public void actionPerformed(ActionEvent e) {
			display.setIcon(images[input]);
		}
		
		private int input;
	}
	
	private ImageIcon[] images; //An array to store all the picture
	private JLabel display; //A place to show the corresponding picture
}
