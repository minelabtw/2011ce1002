package a12.s100502014;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class A121 extends JApplet {
	//icon
	private ImageIcon canadaIcon = new ImageIcon("bin/image/Canada_Icon.gif");
	private ImageIcon chinaIcon = new ImageIcon("bin/image/China_Icon.gif");
	private ImageIcon englandIcon = new ImageIcon("bin/image/England_Icon.gif");
	private ImageIcon germanIcon = new ImageIcon("bin/image/German_Icon.gif");
	private ImageIcon USAIcon = new ImageIcon("bin/image/USA_Icon.gif");
	
	//picture
	private ImageIcon canada = new ImageIcon("bin/image/Canada.gif");
	private ImageIcon china = new ImageIcon("bin/image/China.gif");
	private ImageIcon england = new ImageIcon("bin/image/England.gif");
	private ImageIcon german = new ImageIcon("bin/image/German.gif");
	private ImageIcon USA = new ImageIcon("bin/image/USA.gif");
	
	//menu
	private JMenu menu = new JMenu("Flags");
	private JMenuBar mnb = new JMenuBar();
	private JMenuItem m_canada = new JMenuItem(canadaIcon);
	private JMenuItem m_china = new JMenuItem(chinaIcon);
	private JMenuItem m_england = new JMenuItem(englandIcon);
	private JMenuItem m_german = new JMenuItem(germanIcon);
	private JMenuItem m_USA = new JMenuItem(USAIcon);
	
	//tool bar
	private JToolBar tb = new JToolBar();
	private JButton b_canada = new JButton(canadaIcon);
	private JButton b_china = new JButton(chinaIcon);
	private JButton b_england = new JButton(englandIcon);
	private JButton b_german = new JButton(germanIcon);
	private JButton b_USA = new JButton(USAIcon);
	
	//image viewer
	private JLabel label = new JLabel();
	
	public A121() {
		//menu
		menu.add(m_canada);
		menu.add(m_china);
		menu.add(m_england);
		menu.add(m_german);
		menu.add(m_USA);
		mnb.add(menu);
		
		//tool bar
		tb.add(b_canada);
		tb.add(b_china);
		tb.add(b_england);
		tb.add(b_german);
		tb.add(b_USA);
		
		add(mnb, BorderLayout.NORTH);
		add(label, BorderLayout.CENTER);
		add(tb, BorderLayout.SOUTH);
		
		//listener
		m_canada.addActionListener(new Listener());
		m_china.addActionListener(new Listener());
		m_england.addActionListener(new Listener());
		m_german.addActionListener(new Listener());
		m_USA.addActionListener(new Listener());
		b_canada.addActionListener(new Listener());
		b_china.addActionListener(new Listener());
		b_england.addActionListener(new Listener());
		b_german.addActionListener(new Listener());
		b_USA.addActionListener(new Listener());
	}
	
	//listener
	class Listener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == m_canada || e.getSource() == b_canada) {
				label.setIcon(canada);
			}
			else if(e.getSource() == m_china || e.getSource() == b_china) {
				label.setIcon(china);
			}
			else if(e.getSource() == m_england || e.getSource() == b_england) {
				label.setIcon(england);
			}
			else if(e.getSource() == m_german || e.getSource() == b_german) {
				label.setIcon(german);
			}
			else if(e.getSource() == m_USA || e.getSource() == b_USA) {
				label.setIcon(USA);
			}
		}
	}
}
