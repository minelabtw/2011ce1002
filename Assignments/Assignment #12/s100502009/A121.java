package a12.s100502009;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class A121  extends JApplet{
	private JLabel image;	
	private JPanel panel=new JPanel();
	ImageIcon[] pic={//store pictures
			new ImageIcon("image/Canada.gif"),
			new ImageIcon("image/Canada_Icon.gif"),
			new ImageIcon("image/China.gif"),
			new ImageIcon("image/China_Icon.gif"),
			new ImageIcon("image/England.gif"),
			new ImageIcon("image/England_Icon.gif"),
			new ImageIcon("image/German.gif"),
			new ImageIcon("image/German_Icon.gif"),
			new ImageIcon("image/USA.gif"),
			new ImageIcon("image/USA_Icon.gif")
		};
	public A121()
	{		
		image=new JLabel(new ImageIcon("image/Canada.gif"));	
		Action Canada=new MyAction(pic[1],"Canada","Select the Canada flag to display");//create menu items
		Action China=new MyAction(pic[3],"China","Select the China flag to display");
		Action England=new MyAction(pic[5],"England","Select the England flag to display");
		Action German=new MyAction(pic[7],"German","Select the German flag to display");
		Action USA=new MyAction(pic[9],"USA","Select the USA flag to display");
	
		JMenuBar bar=new JMenuBar();//create a menu bar to store the items
		JMenu menu=new JMenu("Flags");	
		setJMenuBar(bar);
		bar.add(menu);	
		
		menu.add(Canada);
		menu.add(China);
		menu.add(England);
		menu.add(German);
		menu.add(USA);
		
		JToolBar tbar=new JToolBar(JToolBar.VERTICAL);//create a tool bar to put images into it
		tbar.setBorder(BorderFactory.createLineBorder(Color.BLUE));
		tbar.add(Canada);
		tbar.add(China);
		tbar.add(England);
		tbar.add(German);
		tbar.add(USA);
		panel.setLayout(new BorderLayout());
		panel.add(image,BorderLayout.CENTER);
		add(tbar,BorderLayout.EAST);
		add(panel,BorderLayout.CENTER);
	}
	
	private class MyAction extends AbstractAction
	{
		String name;
		ImageIcon picture;
		
		MyAction(ImageIcon image,String name,String intro)
		{
			super(name,image);
			this.name=name;
			this.picture=image;
			putValue(Action.SHORT_DESCRIPTION,intro);
		}
		
		public void actionPerformed(ActionEvent e)
		{
			if(name.equals("Canada"))
			{
				image.setIcon(pic[0]);
			}
			else if(name.equals("China"))
			{
				image.setIcon(pic[2]);
			}
			else if(name.equals("England"))
			{
				image.setIcon(pic[4]);
			}
			else if(name.equals("German"))
			{
				image.setIcon(pic[6]);
			}
			else if(name.equals("USA"))
			{
				image.setIcon(pic[8]);
			}
			panel.revalidate();
		}
	}
	
	
}
