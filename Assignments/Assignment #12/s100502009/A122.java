package a12.s100502009;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;
import java.text.*;

public class A122 extends JApplet{
	private ResourceBundle res= ResourceBundle.getBundle("MyResource");
	private JLabel date=new JLabel(res.getString("Date"));
	private JLabel time=new JLabel(res.getString("Time"));
	private JLabel loc=new JLabel(res.getString("Choose_locale"));
	private JLabel tz=new JLabel(res.getString("Time_Zone"));
	private JLabel ds=new JLabel(res.getString("Date_Style"));
	private JLabel ts=new JLabel(res.getString("Time_Style"));
	private JTextField t1=new JTextField(20);
	private JTextField t2=new JTextField(20);
	private JComboBox jcblocale=new JComboBox();
	private JComboBox jcbtimezone=new JComboBox();
	private JComboBox jcbdatestyle=new JComboBox();
	private JComboBox jcbtimestyle=new JComboBox();
	private TimeZone timeZone=TimeZone.getTimeZone("CST");
	private Locale[] locales=Locale.getAvailableLocales();
	private String[] timezones=TimeZone.getAvailableIDs();
	private Locale locale=Locale.getDefault();
	int datestyle=DateFormat.FULL;
	int timestyle=DateFormat.FULL;
	Calendar calendar=new GregorianCalendar();	
	DateFormat formatter=DateFormat.getDateTimeInstance(datestyle,timestyle,getLocale());
	String[] style={"FULL","LONG","MEDIUM","SHORT"};
	public A122()
	{		
		formatter.setTimeZone(timeZone);
		t1.setText(formatter.format(new Date())); 
		t2.setText(formatter.format(calendar.getTime())); 
		initializeComboBox();
		//initialize locale and time zone
		locale=locales[jcblocale.getSelectedIndex()];
		formatter.setTimeZone(TimeZone.getTimeZone(timezones[jcblocale.getSelectedIndex()]));
		setLayout(new GridLayout(3,1));
		JPanel p1=new JPanel(new GridLayout(1,4));
		p1.add(date);
		p1.add(t1);
		p1.add(time);
		p1.add(t2);
		
		JPanel p2=new JPanel(new GridLayout(1,4));
		p2.add(loc);
		p2.add(jcblocale);
		p2.add(tz);
		p2.add(jcbtimezone);
		
		JPanel p3=new JPanel(new GridLayout(1,4));
		p3.add(ds);
		p3.add(jcbdatestyle);
		p3.add(ts);
		p3.add(jcbtimestyle);
		
		t1.setEditable(false);
		t2.setEditable(false);
		
		add(p1);
		add(p2);
		add(p3);
		
		jcblocale.addActionListener(new ActionListener()//choose the locale
		{
			public void actionPerformed(ActionEvent e)
			{
				locale=locales[jcblocale.getSelectedIndex()];
				formatter=DateFormat.getDateTimeInstance(datestyle,timestyle,locale);
				updateString();
				updatetime();
				updatedate();
			}
		});
		jcbtimezone.addActionListener(new ActionListener()//choose the time zone
		{
			public void actionPerformed(ActionEvent e)
			{				
				formatter.setTimeZone(TimeZone.getTimeZone(timezones[jcblocale.getSelectedIndex()]));
				updatetime();
				updatedate();
			}
		});
		jcbdatestyle.addActionListener(new ActionListener()//choose the date style
		{
			public void actionPerformed(ActionEvent e)
			{
				if(jcbdatestyle.getSelectedIndex()==0)
				{
					datestyle=DateFormat.FULL;
					formatter=DateFormat.getDateTimeInstance(datestyle,timestyle,locale);
					updatedate();
				}
				else if(jcbdatestyle.getSelectedIndex()==1)
				{
					datestyle=DateFormat.LONG;					
					formatter=DateFormat.getDateTimeInstance(datestyle,timestyle,locale);
					updatedate();
				}
				else if(jcbdatestyle.getSelectedIndex()==2)
				{
					datestyle=DateFormat.MEDIUM;			
					formatter=DateFormat.getDateTimeInstance(datestyle,timestyle,locale);
					updatedate();
				}
				else if(jcbdatestyle.getSelectedIndex()==3)
				{
					datestyle=DateFormat.SHORT;
					formatter=DateFormat.getDateTimeInstance(datestyle,timestyle,locale);
					updatedate();
				}
			}
		});
		jcbtimestyle.addActionListener(new ActionListener()//choose the time style
		{
			public void actionPerformed(ActionEvent e)
			{
				if(jcbtimestyle.getSelectedIndex()==0)
				{
					timestyle=DateFormat.FULL;
					formatter=DateFormat.getDateTimeInstance(datestyle,timestyle,locale);
					updatetime();
				}
				else if(jcbtimestyle.getSelectedIndex()==1)
				{
					timestyle=DateFormat.LONG;				
					formatter=DateFormat.getDateTimeInstance(datestyle,timestyle,locale);
					updatetime();
				}
				else if(jcbtimestyle.getSelectedIndex()==2)
				{
					timestyle=DateFormat.MEDIUM;			
					formatter=DateFormat.getDateTimeInstance(datestyle,timestyle,locale);
					updatetime();
				}
				else if(jcbtimestyle.getSelectedIndex()==3)
				{
					timestyle=DateFormat.SHORT;
					formatter=DateFormat.getDateTimeInstance(datestyle,timestyle,locale);
					updatetime();
				}
			}
		});
		
		
	}
	private void updateString()//renew the strings
	{
		res=ResourceBundle.getBundle("MyResource",locale);
		date.setText(res.getString("Date"));
		time.setText(res.getString("Time"));
		loc.setText(res.getString("Choose_locale"));
		tz.setText(res.getString("Time_Zone"));
		ds.setText(res.getString("Date_Style"));
		ts.setText(res.getString("Time_Style"));		
		repaint();
	}
	
	private void updatetime()//renew the date
	{				
		t2.setText(formatter.format(calendar.getTime())); 
		repaint();
	}
	private void updatedate()//renew the date
	{
		t1.setText(formatter.format(calendar.getTime())); 
		repaint();
	}
	
	public void initializeComboBox()
	{
		for(int i=0;i<locales.length;i++)
		{
			jcblocale.addItem(locales[i].getDisplayName());
		}
		for(int i=0;i<timezones.length;i++)
		{
			jcbtimezone.addItem(timezones[i]);
		}
		for(int i=0;i<4;i++)
		{
			jcbtimestyle.addItem(style[i]);
		}
		for(int i=0;i<4;i++)
		{
			jcbdatestyle.addItem(style[i]);
		}
		
	}
	

}
