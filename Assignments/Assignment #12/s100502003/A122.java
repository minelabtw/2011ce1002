package a12.s100502003;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.text.*;
import javax.swing.*;
import javax.swing.Timer;

public class A122 extends JApplet {
	private TimeZone timeZone = TimeZone.getTimeZone("CST");
	private Timer timer = new Timer(1000, new TimerListener());
	private String[] availableTimeZones = TimeZone.getAvailableIDs();
	private String[] format = {"Full", "Long", "Medium", "Short"};
	private Locale[] locales = {Locale.CHINESE, Locale.JAPAN, Locale.ENGLISH, Locale.FRANCE};
	private int date=0, time=0;
	private String sDate, sTime;
	
	private ResourceBundle res = ResourceBundle.getBundle("Language/MyResource");
	
	private JComboBox jcbLocales = new JComboBox(locales);
	private JComboBox jcbTimeZones = new JComboBox();
	private JComboBox jcbDate = new JComboBox(format);
	private JComboBox jcbTime = new JComboBox(format);
	private JTextField showDate = new JTextField(sDate);
	private JTextField showTime = new JTextField(sTime);
	
	// initialize those value
	private String Time = res.getString("Time");
	private String Date = res.getString("Date");
	private String Choose_locale = res.getString("Choose_locale");
	private String Time_Zone = res.getString("Time_Zone");
	private String Date_Style = res.getString("Date_Style");
	private String Time_Style = res.getString("Time_Style");

	private JLabel jlblTime = new JLabel(Time);
	private JLabel jlblDate = new JLabel(Date);
	private JLabel jlblLocale = new JLabel(Choose_locale);
	private JLabel jlblTimeZone = new JLabel(Time_Zone);
	private JLabel jlblDateStyle = new JLabel(Date_Style);
	private JLabel jlblTimeStyle = new JLabel(Time_Style);
	
	public A122() {
		timer.start();
		
		setAvailableTimeZones(); // initialize jcbTimeZones with all available time zones
		
		// initialize time zone
		setTimeZone(TimeZone.getTimeZone(availableTimeZones[jcbTimeZones.getSelectedIndex()]));
		
		JPanel panel1 =  new JPanel();
		panel1.setLayout(new GridLayout(1,4,5,5));
		panel1.add(jlblDate);
		panel1.add(showDate);
		panel1.add(jlblTime);
		panel1.add(showTime);
		
		JPanel panel2 = new JPanel();
		panel2.setLayout(new GridLayout(1,4,3,3));
		panel2.add(jlblLocale);
		panel2.add(jcbLocales);
		panel2.add(jlblTimeZone);
		panel2.add(jcbTimeZones);
		
		JPanel panel3 =  new JPanel();
		panel3.setLayout(new GridLayout(1,4,1,1));
		panel3.add(jlblDateStyle);
		panel3.add(jcbDate);
		panel3.add(jlblTimeStyle);
		panel3.add(jcbTime);
		
		setLayout(new BorderLayout(5,5));
		add(panel1, BorderLayout.NORTH);
		add(panel2, BorderLayout.CENTER);
		add(panel3, BorderLayout.SOUTH);
		
		jcbTimeZones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setTimeZone(TimeZone.getTimeZone(availableTimeZones[jcbTimeZones.getSelectedIndex()]));
			}
		});
		
		jcbLocales.addActionListener(new ActionListener() { // change the words into the language been choose
			public void actionPerformed(ActionEvent e) {
				setLocale(locales[jcbLocales.getSelectedIndex()]);
				res = ResourceBundle.getBundle("Language/MyResource", getLocale());
				jlblTime.setText(res.getString("Time"));
				jlblDate.setText(res.getString("Date"));
				jlblLocale.setText(res.getString("Choose_locale"));
				jlblTimeZone.setText(res.getString("Time_Zone"));
				jlblDateStyle.setText(res.getString("Date_Style"));
				jlblTimeStyle.setText(res.getString("Time_Style"));
				repaint();
			}
		});
		
		jcbDate.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				 date = jcbDate.getSelectedIndex();
			}
		});
		
		jcbTime.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				time = jcbTime.getSelectedIndex();
			}
		});
	}
	
	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}
	
	private class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Calendar calendar = new GregorianCalendar(timeZone, getLocale());
			DateFormat Dateformatter, Timeformatter;
			
			if(date == 0) { // full
				Dateformatter = DateFormat.getDateInstance(DateFormat.FULL, getLocale());
			}
			else if(date == 1) { // long
				Dateformatter = DateFormat.getDateInstance(DateFormat.LONG, getLocale());
			}
			else if(date == 2) { // medium
				Dateformatter = DateFormat.getDateInstance(DateFormat.MEDIUM, getLocale());
			}
			else { // short
				Dateformatter = DateFormat.getDateInstance(DateFormat.SHORT, getLocale());
			}
			
			if(time == 0) { // full
				Timeformatter = DateFormat.getTimeInstance(DateFormat.FULL, getLocale());
			}
			else if(time == 1) { // long
				Timeformatter = DateFormat.getTimeInstance(DateFormat.LONG, getLocale());
			} 
			else if(time == 2) { // medium
				Timeformatter = DateFormat.getTimeInstance(DateFormat.MEDIUM, getLocale());
			}
			else { // short
				Timeformatter = DateFormat.getTimeInstance(DateFormat.SHORT, getLocale());
			}
			
			Dateformatter.setTimeZone(timeZone);
			Timeformatter.setTimeZone(timeZone);
			
			// reset and change date, time
			sDate = Dateformatter.format(calendar.getTime());
			sTime = Timeformatter.format(calendar.getTime());
			showDate.setText(sDate);
			showTime.setText(sTime);
		}
	}
	
	private void setAvailableTimeZones() { // set time zones
		Arrays.sort(availableTimeZones);
		for(int i=0; i<availableTimeZones.length; i++) {
			jcbTimeZones.addItem(availableTimeZones[i]);
		}
	}
	
}
