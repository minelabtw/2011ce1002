package a12.s100502003;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class A121 extends JApplet {
	private JPanel panel = new JPanel();
	public JLabel picture = new JLabel();
	
	// set the images
	ImageIcon Canada = new ImageIcon("image/Canada.gif");
	ImageIcon CanadaIcon = new ImageIcon("image/Canada_Icon.gif");
	ImageIcon China = new ImageIcon("image/China.gif");
	ImageIcon ChinaIcon = new ImageIcon("image/China_Icon.gif");
	ImageIcon England = new ImageIcon("image/England.gif");
	ImageIcon EnglandIcon = new ImageIcon("image/England_Icon.gif");
	ImageIcon German = new ImageIcon("image/German.gif");
	ImageIcon GermanIcon = new ImageIcon("image/German_Icon.gif");
	ImageIcon USA = new ImageIcon("image/USA.gif");
	ImageIcon USAIcon = new ImageIcon("image/USA_Icon.gif");
	
	public A121() {
		// create Actions
		Action CanadaAction = new CountryAction("Canada", CanadaIcon, "Know more about Canada", 
				new Integer(KeyEvent.VK_C), KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK));
		Action ChinaAction = new CountryAction("China", ChinaIcon, "Know more about China", 
				new Integer(KeyEvent.VK_A), KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.CTRL_MASK));
		Action EnglandAction = new CountryAction("England", EnglandIcon, "Know more about England", 
				new Integer(KeyEvent.VK_E), KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));
		Action GermanAction = new CountryAction("German", GermanIcon, "Know more about German", 
				new Integer(KeyEvent.VK_G), KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.CTRL_MASK));
		Action USAAction = new CountryAction("USA", USAIcon, "Know more about USA", 
				new Integer(KeyEvent.VK_U), KeyStroke.getKeyStroke(KeyEvent.VK_U, ActionEvent.CTRL_MASK));
		
		// create menus
		JMenuBar jMenuBar = new JMenuBar();
		JMenu jmenuCountry = new JMenu("Country");
		setJMenuBar(jMenuBar);
		jMenuBar.add(jmenuCountry);
		
		// add actions to the menu
		jmenuCountry.add(CanadaAction);
		jmenuCountry.add(ChinaAction);
		jmenuCountry.add(EnglandAction);
		jmenuCountry.add(GermanAction);
		jmenuCountry.add(USAAction);
		
		// add actions to the toolbar
		JToolBar jToolBar = new JToolBar(JToolBar.VERTICAL);
		jToolBar.setBorder(BorderFactory.createLineBorder(Color.blue));
		jToolBar.add(CanadaAction);
		jToolBar.add(ChinaAction);
		jToolBar.add(EnglandAction);
		jToolBar.add(GermanAction);
		jToolBar.add(USAAction);
		
		panel.add(picture);
		
		add(jToolBar, BorderLayout.EAST);
		add(panel, BorderLayout.CENTER);
	}
	
	private class CountryAction extends AbstractAction {
		String name;
		
		CountryAction(String name, Icon icon, String desc, Integer mnemonic, KeyStroke accelerator) {
			super(name, icon);
			putValue(Action.SHORT_DESCRIPTION, desc);
			putValue(Action.MNEMONIC_KEY, mnemonic);
			putValue(Action.ACCELERATOR_KEY, accelerator);
			this.name = name;
		}
		
		public void actionPerformed(ActionEvent e) {
			// change icon that correspond to the user's choice
			if(name.equals("Canada")) 
				picture.setIcon(Canada);
			else if(name.equals("China"))
				picture.setIcon(China);
			else if(name.equals("England"))
				picture.setIcon(England);
			else if(name.equals("German"))
				picture.setIcon(German);
			else if(name.equals("USA"))
				picture.setIcon(USA);
		}
	}
}
