package a12.s100502503;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.*;

public class A121  extends JApplet
{
	protected JLabel picL = new JLabel();
	protected ImageIcon[] flagBig = new ImageIcon[5];
	protected ImageIcon[] flagsmall = new ImageIcon[5];
	
	private JMenuBar menuBar = new JMenuBar();
	private JMenu menu = new JMenu("Flag");
	private JToolBar jtb = new JToolBar(JToolBar.VERTICAL);
	
	protected String[] flagS = {"Canda", "China", "England", "German", "USA"};

	public A121()
	{		
		flagBig[0] = new ImageIcon("image/Canada.gif");
		flagBig[1] = new ImageIcon("image/China.gif");
		flagBig[2] = new ImageIcon("image/England.gif");
		flagBig[3] = new ImageIcon("image/German.gif");
		flagBig[4] = new ImageIcon("image/USA.gif");
		
		flagsmall[0] = new ImageIcon("image/Canada_Icon.gif");
		flagsmall[1] = new ImageIcon("image/China_Icon.gif");
		flagsmall[2] = new ImageIcon("image/England_Icon.gif");
		flagsmall[3] = new ImageIcon("image/German_Icon.gif");
		flagsmall[4] = new ImageIcon("image/USA_Icon.gif");
		
		this.setSize(400, 400);
		
		
		MyAction candaA = new MyAction(flagS[0], flagsmall[0], new Integer(KeyEvent.VK_L));
		MyAction chinaA = new MyAction(flagS[1], flagsmall[1], new Integer(KeyEvent.VK_L));
		MyAction englandA = new MyAction(flagS[2], flagsmall[2], new Integer(KeyEvent.VK_L));
		MyAction germanA = new MyAction(flagS[3], flagsmall[3], new Integer(KeyEvent.VK_L));
		MyAction usaA = new MyAction(flagS[4], flagsmall[4], new Integer(KeyEvent.VK_L));

		
		
		this.setJMenuBar(menuBar);
		menuBar.add(menu);
		
		menu.add(candaA);
		menu.add(chinaA);
		menu.add(englandA);
		menu.add(germanA);
		menu.add(usaA);
		
		jtb.add(candaA);
		jtb.add(chinaA);
		jtb.add(englandA);
		jtb.add(germanA);
		jtb.add(usaA);
		
		this.add(picL, BorderLayout.CENTER);
		this.add(jtb, BorderLayout.EAST);
	}
	
	private class MyAction extends AbstractAction
	{
		String name;
		Icon pic;
		MyAction(String name, Icon pic, Integer m)
		{
			super(name, pic);
			this.name = name;
			this.pic = pic;
			putValue(Action.MNEMONIC_KEY, m);
		}
		public void actionPerformed(ActionEvent e)
		{
			if(name.equals(flagS[0]))
				picL.setIcon(flagBig[0]);
			
			else if(name.equals(flagS[1]))
				picL.setIcon(flagBig[1]);
			
			else if(name.equals(flagS[2]))
				picL.setIcon(flagBig[2]);

			else if(name.equals(flagS[3]))
				picL.setIcon(flagBig[3]);

			else if(name.equals(flagS[4]))
				picL.setIcon(flagBig[4]);
			
			else;
		}
	}
}
