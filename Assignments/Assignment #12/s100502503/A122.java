package a12.s100502503;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.swing.*;

public class A122 extends JApplet
{
	private ResourceBundle res = ResourceBundle.getBundle("MyResource");
	
	private Locale[] availableLocales = {Locale.CHINESE, Locale.JAPAN, Locale.ENGLISH, Locale.FRENCH};
	private String[] availableTimeZones = TimeZone.getAvailableIDs();
	private String[] formatStyle = {"FULL", "LONG", "MEDIUM", "SHORT"};
	private int[] dateFormatStyle = {DateFormat.FULL, DateFormat.LONG, DateFormat.MEDIUM, DateFormat.SHORT};
	private int dateStyle;
	private int timeStyle;
	private Locale locale = Locale.getDefault();
	private TimeZone timeZone = TimeZone.getTimeZone("EST");
	private Timer timer = new Timer(1000, new TimerListener()); 
	
	private JPanel p1 = new JPanel(new GridLayout(1, 4, 5, 5));
	private JPanel p2 = new JPanel(new GridLayout(1, 4, 5, 5));
	private JPanel p3 = new JPanel(new GridLayout(1, 4, 5, 5));
	private JTextField jtfDate = new JTextField("");
	private JTextField jtfTime = new JTextField("");
	private JLabel dateL = new JLabel(res.getString("Date"));
	private JLabel timeL = new JLabel(res.getString("Time"));
	private JLabel chooseLocale = new JLabel(res.getString("Choose_locale"));
	private JLabel timeZoneL = new JLabel(res.getString("Time_Zone"));
	private JLabel DateStyle = new JLabel(res.getString("Date_Style"));
	private JLabel TimeStyle = new JLabel(res.getString("Time_Style"));
	
	private JComboBox jcbLocale = new JComboBox();
	private JComboBox jcbTimeZones = new JComboBox(); 
	private JComboBox jcbDateStyle = new JComboBox();
	private JComboBox jcbTimeStyle = new JComboBox();
	
	public A122()
	{
		p1.add(dateL);
		p1.add(jtfDate);
		p1.add(timeL);
		p1.add(jtfTime);
		p2.add(chooseLocale);
		p2.add(jcbLocale);
		p2.add(timeZoneL);
		p2.add(jcbTimeZones);
		p3.add(DateStyle);
		p3.add(jcbDateStyle);
		p3.add(TimeStyle);
		p3.add(jcbTimeStyle);
		
		chooseLocaleComboBox();
		timeZonesComboBox();
		
		add(p1, BorderLayout.NORTH);
		add(p2, BorderLayout.CENTER);
		add(p3, BorderLayout.SOUTH);
		
		jcbLocale.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				setLocale(availableLocales[jcbLocale.getSelectedIndex()]);
				updateStrings();
			}	
		});
		
		jcbTimeZones.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				timeZone = TimeZone.getTimeZone(availableTimeZones[jcbTimeZones.getSelectedIndex()]);
			}
			
		});
		
		jcbDateStyle.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				setDateStyle(dateFormatStyle[jcbDateStyle.getSelectedIndex()]);
			}
			
		});
		
		jcbTimeStyle.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				setTimeStyle(dateFormatStyle[jcbTimeStyle.getSelectedIndex()]);
			}
		});
		
		timer.start();
	}
	private class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			Calendar c = new GregorianCalendar(timeZone, getLocale());
			DateFormat df = DateFormat.getDateInstance(getDateStyle(), getLocale());
			DateFormat tf = DateFormat.getTimeInstance(getTimeStyle(), getLocale());

			df.setTimeZone(timeZone);
			tf.setTimeZone(timeZone);
			jtfDate.setText(df.format(c.getTime()));
			jtfTime.setText(tf.format(c.getTime()));			
		}
	}

	public void chooseLocaleComboBox()
	{
		for(int i = 0; i < availableLocales.length; i++)
		{
			jcbLocale.addItem(availableLocales[i].getDisplayName());
		}
	}
	
	public void timeZonesComboBox()
	{
		for(int i = 0; i < availableTimeZones.length; i++)
		{
			jcbTimeZones.addItem(availableTimeZones[i].getBytes());
		}
	}

	public void dateStyleComboBox()
	{
		for(int i = 0; i < formatStyle.length; i++)
		{
			jcbDateStyle.addItem(formatStyle[i]+"");
		}
	}
	
	public void timeStyleComboBox()
	{
		for(int i = 0; i < formatStyle.length; i++)
		{
			jcbTimeStyle.addItem(formatStyle[i]+"");
		}
	}
	
	public void setDateStyle(int s)
	{
		dateStyle = s;
	}
	
	public int getDateStyle()
	{
		return dateStyle;
	}
	
	public void setTimeStyle(int s)
	{
		timeStyle = s;
	}
	
	public int getTimeStyle()
	{
		return timeStyle;
	}

	private void updateStrings()
	{
		res = ResourceBundle.getBundle("MyResource", getLocale());
		
		dateL.setText(res.getString("Date"));
		timeL.setText(res.getString("Time"));
		timeZoneL.setText(res.getString("Time_Zone"));
		chooseLocale.setText(res.getString("Choose_locale"));
		TimeStyle.setText(res.getString("Time_Style"));
		DateStyle.setText(res.getString("Date_Style"));
		repaint();
	}
}
