package a12.s100502509;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JToolBar;

import org.omg.CORBA.PUBLIC_MEMBER;

public class A121 extends JApplet {
	private JLabel display_Label = new JLabel("");

	ImageIcon Canada = new ImageIcon(getClass().getResource("image/Canada.gif"));// read the picture
	ImageIcon China = new ImageIcon(getClass().getResource("image/China.gif"));
	ImageIcon England = new ImageIcon(getClass().getResource(
			"image/England.gif"));
	ImageIcon German = new ImageIcon(getClass().getResource("image/German.gif"));
	ImageIcon USA = new ImageIcon(getClass().getResource("image/USA.gif"));

	ImageIcon Canada_Action_mage = new ImageIcon(getClass().getResource(
			"image/Canada_Icon.gif"));// read the picture for Action
	ImageIcon China_Action_mage = new ImageIcon(getClass().getResource(
			"image/China_Icon.gif"));
	ImageIcon England_Action_mage = new ImageIcon(getClass().getResource(
			"image/England_Icon.gif"));
	ImageIcon German_Action_mage = new ImageIcon(getClass().getResource(
			"image/German_Icon.gif"));
	ImageIcon USA_Action_mage = new ImageIcon(getClass().getResource("image/USA_Icon.gif"));

	public A121() {

		Action Canada_Action = new MyAction("Canada", Canada_Action_mage,
				"Display the image of Canada", 0);// use Action
		Action China_Action = new MyAction("China", China_Action_mage,
				"Display the image of China", 1);
		Action German_Action = new MyAction("German", German_Action_mage,
				"Display the image of German", 2);
		Action England_Action = new MyAction("England", England_Action_mage,
				"Display the image of England", 3);
		Action USA_Action = new MyAction("USA", USA_Action_mage,
				"Display the image of USA", 4);

		JMenuBar menuBar = new JMenuBar();//Declare JMenuBar
		JMenu menu = new JMenu("Menu");

		menu.add(Canada_Action);//add Action to menu
		menu.add(China_Action);
		menu.add(German_Action);
		menu.add(England_Action);
		menu.add(USA_Action);
		menuBar.add(menu);

		JToolBar jToolBar = new JToolBar(JToolBar.VERTICAL);//Declare JToolBar
		jToolBar.add(Canada_Action);//add Action to JToolBar
		jToolBar.add(China_Action);
		jToolBar.add(German_Action);
		jToolBar.add(England_Action);
		jToolBar.add(USA_Action);

		add(menuBar, BorderLayout.NORTH);//add to JApplet
		add(jToolBar, BorderLayout.EAST);
		add(display_Label);

	}

	private class MyAction extends AbstractAction {
		;
		private int Decide_Flag;

		MyAction(String Flag_Name, ImageIcon flag_Icon,
				String Flag_Description, int Decide_Flag) {
			super(Flag_Name, flag_Icon);
			this.Decide_Flag = Decide_Flag;
			putValue(Action.SHORT_DESCRIPTION, Flag_Description);
		}

		public void actionPerformed(ActionEvent e) {
			if (Decide_Flag == 0) {
				display_Label.setIcon(Canada);//set Picture Canada
			}

			else if (Decide_Flag == 1) {//use the Flag of China
				display_Label.setIcon(China);
			}

			else if (Decide_Flag == 2) {//use the Flag of German
				display_Label.setIcon(German);
			}

			else if (Decide_Flag == 3) {//use the Flag of England
				display_Label.setIcon(England);
			}

			else {//use the Flag of USA
				display_Label.setIcon(USA);
			}

		}

	}

}
