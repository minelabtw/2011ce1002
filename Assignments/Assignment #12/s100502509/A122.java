package a12.s100502509;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.util.*;

public class A122 extends JApplet {

	JTextField TextField_Date, TextField_Time;
	JLabel Label_Date, Label_Time, Label_Locale, Label_TimeZone,
			Label_DateStyle, Label_TimeStyle;
	JComboBox ComboBox_Locale, ComboBox_TimeZone, ComboBox_DateStyle,
			ComboBox_TimeStyle;
	TimeZone timeZone = TimeZone.getDefault();
	Calendar calender = new GregorianCalendar(timeZone, getLocale());
	DateFormat Formatter_Date = DateFormat.getDateInstance(DateFormat.LONG,
			getLocale());
	DateFormat Formatter_Time = DateFormat.getTimeInstance(DateFormat.LONG,
			getLocale());
	Locale[] locale = Locale.getAvailableLocales();
	String[] Take_TimeZone = TimeZone.getAvailableIDs();
	String[] Choose_Style = { "Full", "Long", "Medium", "Short" };
	ResourceBundle resourceBundle = ResourceBundle.getBundle("MyResource");

	public A122() {
		ComboBox_Locale = new JComboBox(); // Choose locales
		for (int i = 0; i < locale.length; i++) { // Choose items of Combobox
			ComboBox_Locale.addItem(locale[i].getDisplayName() + " "
					+ locale[i].toString());
		}
		ComboBox_Locale.addActionListener(new ActionListener() { // Action happens when items changed
					public void actionPerformed(ActionEvent e) {
						setLocale(locale[ComboBox_Locale.getSelectedIndex()]);
						ResetContents();
					}
				});

		ComboBox_TimeZone = new JComboBox(); // Select the time zones
		Arrays.sort(Take_TimeZone); // Short time zones
		for (int i = 0; i < Take_TimeZone.length; i++) { // Set combobox
			ComboBox_TimeZone.addItem(Take_TimeZone[i]);
		}
		ComboBox_TimeZone.addActionListener(new ActionListener() { // Action happens when items changed
					public void actionPerformed(ActionEvent e) {
						timeZone = TimeZone
								.getTimeZone(Take_TimeZone[ComboBox_TimeZone
										.getSelectedIndex()]);
						ResetContents();
					}
				});

		ComboBox_DateStyle = new JComboBox(Choose_Style); // Select the styles
															// of date
		ComboBox_DateStyle.addActionListener(new ActionListener() { // Action happens when items changed
					public void actionPerformed(ActionEvent e) {
						ResetContents();
					}
				});

		ComboBox_TimeStyle = new JComboBox(Choose_Style); // Select the styles
															// of time
		ComboBox_TimeStyle.addActionListener(new ActionListener() { // Action happens when items changed
					public void actionPerformed(ActionEvent e) {
						ResetContents();
					}
				});

		TextField_Date = new JTextField("", JLabel.CENTER);
		TextField_Time = new JTextField("", JLabel.CENTER);

		Formatter_Date.setTimeZone(timeZone);
		Formatter_Time.setTimeZone(timeZone);

		TextField_Date.setText(Formatter_Date.format(calender.getTime()));
		TextField_Time.setText(Formatter_Time.format(calender.getTime()));

		setLayout(new GridLayout(3, 2));

		JPanel panel1 = new JPanel(); // Panel used to add word Date and current
										// date
		Label_Date = new JLabel(resourceBundle.getString("Date"));
		panel1.add(Label_Date);// add Label_Date to Panel1
		panel1.add(TextField_Date);// add TextField_Date to panel1

		Label_Time = new JLabel(resourceBundle.getString("Time")); // Add Word Time and current time
		panel1.add(Label_Time);
		panel1.add(TextField_Time);
		add(panel1);

		panel1 = new JPanel(); // Add word "Choose Locate" and "change locale"
		Label_Locale = new JLabel(resourceBundle.getString("Choose_locale"));
		panel1.add(Label_Locale);
		panel1.add(ComboBox_Locale);

		Label_TimeZone = new JLabel(resourceBundle.getString("Time_Zone")); // Add word "Time Zone" and change time zone
		panel1.add(Label_TimeZone);
		panel1.add(ComboBox_TimeZone);
		add(panel1);

		panel1 = new JPanel(); // Add word "Date Style" & selections of styles
		Label_DateStyle = new JLabel(resourceBundle.getString("Date_Style"));
		panel1.add(Label_DateStyle);
		panel1.add(ComboBox_DateStyle);

		Label_TimeStyle = new JLabel(resourceBundle.getString("Time_Style")); // Add word "Time Style" and selections of styles
		panel1.add(Label_TimeStyle);
		panel1.add(ComboBox_TimeStyle);
		add(panel1);

		ResetContents();
	}

	public void ResetContents() { // Reset all the contents in the Panel
		Formatter_Date = DateFormat.getDateInstance(
				ComboBox_DateStyle.getSelectedIndex(), getLocale());
		Formatter_Time = DateFormat.getTimeInstance(
				ComboBox_TimeStyle.getSelectedIndex(), getLocale());
		Formatter_Date.setTimeZone(timeZone);
		Formatter_Time.setTimeZone(timeZone);
		resourceBundle = ResourceBundle.getBundle("MyResource", getLocale());
		TextField_Date.setText(Formatter_Date.format(calender.getTime()));
		TextField_Time.setText(Formatter_Time.format(calender.getTime()));
		Label_Date.setText(resourceBundle.getString("Date"));
		Label_Time.setText(resourceBundle.getString("Time"));
		Label_Locale.setText(resourceBundle.getString("Choose_locale"));
		Label_TimeZone.setText(resourceBundle.getString("Time_Zone"));
	}

}
