package a12.s100502026;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Frame extends JFrame{
	
	JPanel showPanel = new JPanel( new GridLayout( 1 , 4 ) );
	JPanel ctrlPanel = new JPanel();
	String[] languages = { "USA" , "Chines" , "Japanese" , "French" };
	JLabel jlblDate = new JLabel( "Date" );
	JLabel jlblTime = new JLabel( "Time" );
	JLabel jlblLanguage = new JLabel( "Language" );
	JLabel jlblTimeZone = new JLabel( "Time Zone" );
	JLabel jlblDateStyle = new JLabel( "Date Style" );
	JLabel jlblTimeStyle = new JLabel( "Time Style" );
	JTextField jtfDate = new JTextField();
	JTextField jtfTime = new JTextField();
	JComboBox jcboLanguage = new JComboBox( languages );
	JComboBox jcboTimeZone = new JComboBox();
	JComboBox jcboDateStyle = new JComboBox();
	JComboBox jcboTimeStyle = new JComboBox();

	Frame()
	{		
		jcboLanguage.addItemListener
		(
			new ItemListener()
			{
				public void itemStateChanged( ItemEvent e )
				{
					setLanguageDisplay( jcboLanguage.getSelectedIndex() );
				}
			}
		);

		jcboTimeZone.addItemListener
		(
			new ItemListener()
			{
				public void itemStateChanged( ItemEvent e )
				{
					setTimeZoneDisplay( jcboTimeZone.getSelectedIndex() );
				}
			}
		);

		jcboDateStyle.addItemListener
		(
			new ItemListener()
			{
				public void itemStateChanged( ItemEvent e )
				{
					setDataStyleDisplay( jcboDateStyle.getSelectedIndex() );
				}
			}
		);

		jcboTimeStyle.addItemListener
		(
			new ItemListener()
			{
				public void itemStateChanged( ItemEvent i )
				{
					setTimeStyleDisplay( jcboTimeStyle.getSelectedIndex() );
				}
			}
		);

		showPanel.add( jlblDate );
		showPanel.add( jtfDate );
		showPanel.add( jlblTime );
		showPanel.add( jtfTime );
		
		ctrlPanel.setLayout( new GridLayout( 2 , 4 ) );
		
		ctrlPanel.add( jlblLanguage );
		ctrlPanel.add( jcboLanguage );
		ctrlPanel.add( jlblTimeZone );
		ctrlPanel.add( jcboTimeZone );
		ctrlPanel.add( jlblDateStyle );
		ctrlPanel.add( jcboDateStyle );
		ctrlPanel.add( jlblTimeStyle );
		ctrlPanel.add( jcboTimeStyle );

		this.setLayout( new GridLayout( 2 , 1 ) );
		this.add( showPanel );
		this.add( ctrlPanel );
	}
	
	public void setLanguageDisplay( int index )
	{
		
	}
	
	public void setTimeZoneDisplay( int index )
	{
		
	}
	
	public void setDataStyleDisplay( int index )
	{
		
	}
	
	public void setTimeStyleDisplay( int index )
	{
		
	}

}
