package a12.s100502026;

import java.awt.event.*;
import javax.swing.*;

public class PanelWork extends JPanel{

	JMenuBar jmb = new JMenuBar();
	JMenu menu = new JMenu( "ToolBar" );
	JMenuItem jmiCanada , jmiChina  , jmiEngland , jmiGerman , jmiUSA;
	JToolBar jToolBar = new JToolBar();
	JLabel jlbl = new JLabel();
	JButton jbtCanada = new JButton( new ImageIcon( "image/Canada.gif" ) );
	JButton jbtChina = new JButton( new ImageIcon( "image/China.gif" ) );
	JButton jbtEngland = new JButton( new ImageIcon( "image/England.gif" ) );
	JButton jbtGerman = new JButton( new ImageIcon( "image/German.gif" ) );
	JButton jbtUSA = new JButton( new ImageIcon( "image/USA.gif" ) );
	Action Canada = new MyAction( "Canada" , new ImageIcon( "image/Canada.gif" ) );
	Action China = new MyAction( "China" , new ImageIcon( "image/China.gif" ) );
	Action England = new MyAction( "England" , new ImageIcon( "image/England.gif" ) );
	Action German = new MyAction( "German" , new ImageIcon( "image/German.gif" ) );
	Action USA = new MyAction( "USA" , new ImageIcon( "image/USA.gif") );
	
	PanelWork()
	{
		jToolBar.add( Canada );
		jToolBar.add( China );
		jToolBar.add( England );
		jToolBar.add( German );
		jToolBar.add( USA );

		jmiCanada.addActionListener( Canada );
		jmiChina.addActionListener( China );
		jmiEngland.addActionListener( England );
		jmiGerman.addActionListener( German );
		jmiUSA.addActionListener( USA );
		
		menu.add( jmiCanada );
		menu.add( jmiChina );
		menu.add( jmiEngland );
		menu.add( jmiGerman );
		menu.add( jmiUSA );
		
		jmb.add( menu );
		
		this.add( jlbl );
		this.add( jmb );
		this.add( jToolBar );
	}

	private class MyAction extends AbstractAction
	{
		String name;
		
		MyAction( String name , Icon icon )
		{
			super( name , icon );
			this.name = name;
		}
		
		public void actionPerformed( ActionEvent e )
		{
			if( name.equals( "Canada" ) )
			{
				jlbl.setIcon( new ImageIcon( "image/Canada.gif" ) );
			}
			else if( name.equals( "China" ) )
			{
				jlbl.setIcon( new ImageIcon( "image/China.gif" ) );
			}
			else if( name.equals( "England" ) )
			{
				jlbl.setIcon( new ImageIcon( "image/England.gif" ) );
			}
			else if( name.equals( "German" ) )
			{
				jlbl.setIcon( new ImageIcon( "image/German.gif" ) );
			}
			else
			{
				jlbl.setIcon( new ImageIcon( "image/USA.gif" ) );
			}
		}
	}
	
}
