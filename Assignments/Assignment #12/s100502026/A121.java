package a12.s100502026;

import javax.swing.*;

public class A121{

	public static void main( String[] args )
	{
		JFrame frame = new JFrame();
		PanelWork panel = new PanelWork();
		
		frame.add( panel );
		
		frame.setTitle( "Flags" );
		frame.setLocationRelativeTo( null );
		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		frame.pack();
		frame.setVisible( true );
	}
	
}
