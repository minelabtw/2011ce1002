package a12.s100502028;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.text.*;
import javax.swing.*;
import javax.swing.Timer;

public class A122 extends JApplet {
	private TimeZone timeZone = TimeZone.getTimeZone("America/New_York");
	private Timer timer = new Timer(1000, new TimerListener());
	
	private ResourceBundle res = ResourceBundle.getBundle("MyResource");
	
	private JLabel jlblDate = new JLabel(res.getString("Date"));
	private JLabel jlblTime = new JLabel(res.getString("Time"));
	private JLabel jlblLocale = new JLabel(res.getString("Choose_locale"));
	private JLabel jlblTimeZone = new JLabel(res.getString("Time_Zone"));
	private JLabel jlblDateStyle = new JLabel(res.getString("Date_Style"));
	private JLabel jlblTimeStyle = new JLabel(res.getString("Time_Style"));
	private JTextField jtfDate = new JTextField();
	private JTextField jtfTime = new JTextField();
	
	private JComboBox jcboLocale = new JComboBox();
	private JComboBox jcboTimeZone = new JComboBox();
	private JComboBox jcboDateStyle = new JComboBox();
	private JComboBox jcboTimeStyle = new JComboBox();
	
	private Locale locale = Locale.getDefault(); // Current locale
	private Locale[] availableLocales = {Locale.US, Locale.FRANCE, Locale.JAPAN, Locale.TAIWAN};
	private String[] availableTimeZones = {"America/New_York", "Europe/Paris", "Asia/Tokyo", "Asia/Taipei"};
	private String[] style = {"Full", "Short", "Medium", "Long"};
	
	private int dateStyle = DateFormat.FULL;
	private int timeStyle = DateFormat.FULL;
	
	// Constructor
	public A122() {
		timer.start();
		
		// Set label alignment
		jlblDate.setHorizontalAlignment(JLabel.CENTER);
		jlblTime.setHorizontalAlignment(JLabel.CENTER);
		jlblLocale.setHorizontalAlignment(JLabel.CENTER);
		jlblTimeZone.setHorizontalAlignment(JLabel.CENTER);
		jlblDateStyle.setHorizontalAlignment(JLabel.CENTER);
		jlblTimeStyle.setHorizontalAlignment(JLabel.CENTER);
		
		JPanel p1 = new JPanel(new GridLayout(1, 2, 5, 5));
		p1.add(jlblDate);
		p1.add(jtfDate);
		
		JPanel p2 = new JPanel(new GridLayout(1, 2, 5, 5));
		p2.add(jlblTime);
		p2.add(jtfTime);
		
		JPanel p3 = new JPanel(new GridLayout(1, 2, 20, 20));
		p3.add(p1);
		p3.add(p2);
		
		JPanel p4 = new JPanel(new GridLayout(1, 2, 5, 5));
		p4.add(jlblLocale);
		// Add items to jcboLocale
		for (int i = 0; i < availableLocales.length; i++)
			jcboLocale.addItem(availableLocales[i].getDisplayName());
		p4.add(jcboLocale);
		
		JPanel p5 = new JPanel(new GridLayout(1, 2, 5, 5));
		p5.add(jlblTimeZone);
		setAvailableTimeZones();
		p5.add(jcboTimeZone);
		
		JPanel p6 = new JPanel(new GridLayout(1, 2, 20, 20));
		p6.add(p4);
		p6.add(p5);
		
		JPanel p7 = new JPanel(new GridLayout(1, 2, 5, 5));
		p7.add(jlblDateStyle);
		setDatestyle();
		p7.add(jcboDateStyle);
		
		JPanel p8 = new JPanel(new GridLayout(1, 2, 5, 5));
		p8.add(jlblTimeStyle);
		setTimestyle();
		p8.add(jcboTimeStyle);
		
		JPanel p9 = new JPanel(new GridLayout(1, 2, 20, 20));
		p9.add(p7);
		p9.add(p8);
		
		// Set editable false
		jtfDate.setEditable(false);
		jtfTime.setEditable(false);
		
		// Add all panels to the applet
		setLayout(new GridLayout(3, 1, 5, 5));
		add(p3);
		add(p6);
		add(p9);
		
		// Register listeners
		jcboLocale.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				locale = availableLocales[jcboLocale.getSelectedIndex()];
				setLocale(locale);
				updateStrings(); // Update resource
			}
		});
		
		jcboTimeZone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				timeZone = TimeZone.getTimeZone(availableTimeZones[jcboTimeZone.getSelectedIndex()]);
			}
		});
		
		jcboDateStyle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (jcboDateStyle.getSelectedIndex() == 0)
					dateStyle = DateFormat.FULL;
				else if(jcboDateStyle.getSelectedIndex() == 1)
					dateStyle = DateFormat.SHORT;
				else if(jcboDateStyle.getSelectedIndex() == 2)
					dateStyle = DateFormat.MEDIUM;
				else if(jcboDateStyle.getSelectedIndex() == 3)
					dateStyle = DateFormat.LONG;
			}
		});
		
		jcboTimeStyle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (jcboTimeStyle.getSelectedIndex() == 0)
					timeStyle = DateFormat.FULL;
				else if(jcboTimeStyle.getSelectedIndex() == 1)
					timeStyle = DateFormat.SHORT;
				else if(jcboTimeStyle.getSelectedIndex() == 2)
					timeStyle = DateFormat.MEDIUM;
				else if(jcboTimeStyle.getSelectedIndex() == 3)
					timeStyle = DateFormat.LONG;
			}
		});
	} // End constructor
	
	// Update resource strings
	public void updateStrings() {
		res = ResourceBundle.getBundle("MyResource", locale);
		jlblDate.setText(res.getString("Date"));
		jlblTime.setText(res.getString("Time"));
		jlblLocale.setText(res.getString("Choose_locale"));
		jlblTimeZone.setText(res.getString("Time_Zone"));
		jlblDateStyle.setText(res.getString("Date_Style"));
		jlblTimeStyle.setText(res.getString("Time_Style"));
		repaint(); // Make sure the new labels are displayed
	}
	
	// Method to add items to jcboTimeZone
	public void setAvailableTimeZones() {
		// Sort time zones
		Arrays.sort(availableTimeZones);
		for (int i = 0; i < availableTimeZones.length; i++) {
			jcboTimeZone.addItem(availableTimeZones[i]);
		}
	}
	
	// Method to add items to jcboDateStyle
	public void setDatestyle() {
		for (int i = 0; i < style.length; i++) {
			jcboDateStyle.addItem(style[i]);
		}
	}
	
	// Method to add items to jcboTimeStyle
	public void setTimestyle() {
		for (int i = 0; i < style.length; i++) {
			jcboTimeStyle.addItem(style[i]);
		}
	}
	
	// TimerListener to set the time
	private class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Calendar calendar = new GregorianCalendar(timeZone, getLocale());
			DateFormat Dateformatter = DateFormat.getDateInstance(dateStyle, getLocale());
			DateFormat Timeformatter = DateFormat.getTimeInstance(timeStyle, getLocale());
			Dateformatter.setTimeZone(timeZone);
			Timeformatter.setTimeZone(timeZone);
			jtfDate.setText(Dateformatter.format(calendar.getTime()));
			jtfTime.setText(Timeformatter.format(calendar.getTime()));
		}
	}
} // End class A122
