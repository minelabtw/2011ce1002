package a12.s100502028;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class A121 extends JApplet implements ActionListener {
	private CardLayout cardLayout = new CardLayout(5, 5); // 
	private JPanel panelForCountryPictures = new JPanel(cardLayout); // Create panel for pictures
	
	private ImageIcon canadaImageIcon = new ImageIcon(getClass().getResource("image/Canada_Icon.gif"));
	private ImageIcon chinaImageIcon = new ImageIcon(getClass().getResource("image/China_Icon.gif"));
	private ImageIcon englandImageIcon = new ImageIcon(getClass().getResource("image/England_Icon.gif"));
	private ImageIcon germanImageIcon = new ImageIcon(getClass().getResource("image/German_Icon.gif"));
	private ImageIcon usaImageIcon = new ImageIcon(getClass().getResource("image/USA_Icon.gif"));
	
	private ImageIcon canadaPictureImageIcon = new ImageIcon(getClass().getResource("image/Canada.gif"));
	private ImageIcon chinaPictureImageIcon = new ImageIcon(getClass().getResource("image/China.gif"));
	private ImageIcon englandPictureImageIcon = new ImageIcon(getClass().getResource("image/England.gif"));
	private ImageIcon germanPictureImageIcon = new ImageIcon(getClass().getResource("image/German.gif"));
	private ImageIcon usaPictureImageIcon = new ImageIcon(getClass().getResource("image/USA.gif"));

	private JMenuItem jmiClose;
	
	// Constructor
	public A121() {
		// Use ImageViewer for displaying images into panelForCountryPictures
		panelForCountryPictures.add(new ImageViewer(canadaPictureImageIcon.getImage()), "1");
		panelForCountryPictures.add(new ImageViewer(chinaPictureImageIcon.getImage()), "2");
		panelForCountryPictures.add(new ImageViewer(englandPictureImageIcon.getImage()), "3");
		panelForCountryPictures.add(new ImageViewer(germanPictureImageIcon.getImage()), "4");
		panelForCountryPictures.add(new ImageViewer(usaPictureImageIcon.getImage()), "5");
		
		// Create actions
		Action canadaAction = new MyAction("Canada", canadaImageIcon);
		Action chinaAction = new MyAction("China", chinaImageIcon);
		Action englandAction = new MyAction("England", englandImageIcon);
		Action germanAction = new MyAction("German", germanImageIcon);
		Action usaAction = new MyAction("USA", usaImageIcon);
		
		// Create menus
		JMenuBar jMenuBar = new JMenuBar();
		JMenu jmenuCountry = new JMenu("Country");
		JMenu jmenuExit = new JMenu("Exit");
		setJMenuBar(jMenuBar);
		jMenuBar.add(jmenuCountry);
		jMenuBar.add(jmenuExit);
		
		// Add actions to the menu
		jmenuCountry.add(canadaAction);
		jmenuCountry.add(chinaAction);
		jmenuCountry.add(englandAction);
		jmenuCountry.add(germanAction);
		jmenuCountry.add(usaAction);
		
		// Add menu item with mnemonic to menu "Exit"
		jmenuExit.add(jmiClose = new JMenuItem("Close", 'C'));
		
		// Create toolbar for paint utility button
		JToolBar jToolBar = new JToolBar(JToolBar.VERTICAL);
		jToolBar.setFloatable(false);
		
		// Add actions to the toolbar
		jToolBar.add(canadaAction);
		jToolBar.add(chinaAction);
		jToolBar.add(englandAction);
		jToolBar.add(germanAction);
		jToolBar.add(usaAction);
		
		// Add toolbar to the east and panel to the center
		add(jToolBar, BorderLayout.EAST);
		add(panelForCountryPictures, BorderLayout.CENTER);
		
		jmiClose.addActionListener(this); // Active button event here
	} // End Constructor A121
	
	// Method to let the button works
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == jmiClose) {
				System.exit(0);
		}
	}
	
	// Custom action class
	private class MyAction extends AbstractAction {
		String name;
		
		// Constructor
		MyAction(String name, Icon icon) {
			super(name, icon);
			this.name = name;
		}
		
		// Handler
		public void actionPerformed(ActionEvent e) {
			if (name.equals("Canada")) {
				cardLayout.show(panelForCountryPictures, "1"); // Show the component at specified index
			}
			
			else if (name.equals("China")) {
				cardLayout.show(panelForCountryPictures, "2");
			}
			
			else if (name.equals("England")) {
				cardLayout.show(panelForCountryPictures, "3");
			}
			
			else if (name.equals("German")) {
				cardLayout.show(panelForCountryPictures, "4");
			}
			
			else if (name.equals("USA")) {
				cardLayout.show(panelForCountryPictures, "5");
			}
		}
	} // End class MyAction
}
