package a12.s100502505;

import java.awt.*;
import java.awt.event.*;
import java.text.DateFormat;
import java.util.*;
import javax.swing.*;
import javax.swing.Timer;

public class A122 extends JApplet {
	
	int dateStyle;
	int timeStyle;
	
	JComboBox jcbCountry = new JComboBox();
	JComboBox jcbTimeZone = new JComboBox();
	JComboBox jcbDateStyle = new JComboBox();
	JComboBox jcbTimeStyle = new JComboBox();
	
	JTextField jtf1 = new JTextField();
	JTextField jtf2 = new JTextField();
	
	Locale localelan = Locale.getDefault();
	Locale[] localelans = { Locale.ENGLISH, Locale.JAPAN,Locale.CHINESE, Locale.FRANCE };

	
	public enum Style 
	{
		Full, Long, Short
	};

	DateFormat dateInstance;
	DateFormat timeInstance;
	
	ResourceBundle resbun = ResourceBundle.getBundle("MyResource",localelan);
	TimeZone timezone = TimeZone.getTimeZone("EST");
	String[] availableTimeZones = TimeZone.getAvailableIDs();
	TimeZone[] timeZones;
	

	JLabel label1 = new JLabel(resbun.getString("Date"));
	JLabel label2 = new JLabel(resbun.getString("Time"));
	JLabel label3 = new JLabel(resbun.getString("Choose_locale"));
	JLabel label4 = new JLabel(resbun.getString("Time_Zone"));
	JLabel label5 = new JLabel(resbun.getString("Date_Style"));
	JLabel label6 = new JLabel(resbun.getString("Time_Style"));

	public A122() {
		
		initializeAllCombox();
		setLayout(new GridLayout(3, 2, 3, 3));
		
		jtf1.setEditable(false);
		jtf2.setEditable(false);
		
		jcbCountry.addActionListener(new ActionListener()//選國家 
		{
			public void actionPerformed(ActionEvent arg0) {
				localelan = localelans[jcbCountry.getSelectedIndex()];
				updateString();
			}
		});
		
		jcbTimeZone.addActionListener(new ActionListener()//選時區 
		{
			public void actionPerformed(ActionEvent arg0) {
				timezone = timeZones[jcbTimeZone.getSelectedIndex()];
			}
		});
		
		jcbDateStyle.addActionListener(new ActionListener()//選資料 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if (jcbDateStyle.getSelectedIndex() == 0) 
				{
					dateStyle = DateFormat.FULL;
				} else if (jcbDateStyle.getSelectedIndex() == 1) 
				{
					dateStyle = DateFormat.LONG;
				} else if (jcbDateStyle.getSelectedIndex() == 2) 
				{
					dateStyle = DateFormat.SHORT;
				}
			}
		});
		
		jcbTimeStyle.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				if (jcbTimeStyle.getSelectedIndex() == 0) 
				{
					timeStyle = DateFormat.FULL;
				} else if (jcbTimeStyle.getSelectedIndex() == 1) 
				{
					timeStyle = DateFormat.LONG;
				} else if (jcbTimeStyle.getSelectedIndex() == 2) 
				{
					timeStyle = DateFormat.SHORT;
				}
			}
		});
		
		add(label1);
		add(jtf1);
		add(label2);
		add(jtf2);
		add(label3);
		add(jcbCountry);
		add(label4);
		add(jcbTimeZone);
		add(label5);
		add(jcbDateStyle);
		add(label6);
		add(jcbTimeStyle);
		
		timer.start();
	}

	public void initializeAllCombox() 
	{
		
		for (int i = 0; i < localelans.length; i++) 
		{
			jcbCountry.addItem(localelans[i].getDisplayName());
		}
		
		timeZones = new TimeZone[availableTimeZones.length];
		
		for (int i = 0; i < availableTimeZones.length; i++) 
		{
			jcbTimeZone.addItem(availableTimeZones[i]);
			timeZones[i] = TimeZone.getTimeZone(availableTimeZones[i]);
		}
		
		jcbTimeStyle.addItem(Style.Full);
		jcbTimeStyle.addItem(Style.Long);
		jcbTimeStyle.addItem(Style.Short);
		jcbDateStyle.addItem(Style.Full);
		jcbDateStyle.addItem(Style.Long);
		jcbDateStyle.addItem(Style.Short);
	}

	public void updateString() 
	{
		
		resbun = ResourceBundle.getBundle("MyResource", localelan);
		
		label1.setText(resbun.getString("Date"));
		label2.setText(resbun.getString("Time"));
		label3.setText(resbun.getString("Choose_locale"));
		label4.setText(resbun.getString("Time_Zone"));
		label5.setText(resbun.getString("Date_Style"));
		label6.setText(resbun.getString("Time_Style"));
		
		repaint();
		
	}
	
	class updateTime implements ActionListener 
	{
		public void actionPerformed(ActionEvent arg0) 
		{
			Calendar calendar = new GregorianCalendar(timezone, getLocale());
			
			timeInstance = DateFormat.getTimeInstance(timeStyle);
			timeInstance.setTimeZone(timezone);
			jtf2.setText(timeInstance.format(calendar.getTime()));
			dateInstance = DateFormat.getDateInstance(dateStyle);
			dateInstance.setTimeZone(timezone);
			jtf1.setText(dateInstance.format(calendar.getTime()));

		}
		
		
	}

	public Timer timer = new Timer(1000, new updateTime());//Timer

	
}//完成
