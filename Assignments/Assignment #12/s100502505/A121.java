package a12.s100502505;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class A121 extends JApplet implements ActionListener {
	
	JButton jbt1 = new JButton(new ImageIcon(("image/Canada_Icon.gif")));//按鈕
	JButton jbt2 = new JButton(new ImageIcon(("image/China_Icon.gif")));
	JButton jbt3 = new JButton(new ImageIcon(("image/England_Icon.gif")));
	JButton jbt4 = new JButton(new ImageIcon(("image/German_Icon.gif")));
	JButton jbt5 = new JButton(new ImageIcon(("image/USA_Icon.gif")));
	
	ImageIcon pic1 = new ImageIcon("image/Canada.gif");//圖案
	ImageIcon pic2 = new ImageIcon("image/China.gif");
	ImageIcon pic3 = new ImageIcon("image/England.gif");
	ImageIcon pic4 = new ImageIcon("image/German.gif");
	ImageIcon pic5 = new ImageIcon("image/USA.gif");
	
	JMenuBar menuBar = new JMenuBar();
	
	JMenu menu = new JMenu("File");

	JMenuItem Canada = new JMenuItem("Canada");
	JMenuItem China = new JMenuItem("China");
	JMenuItem England = new JMenuItem("England");
	JMenuItem German = new JMenuItem("German");
	JMenuItem USA = new JMenuItem("USA");
	JMenuItem exit = new JMenuItem("exit");
	
	JToolBar jToolBar = new JToolBar("box");
	JPanel panel = new JPanel();
	JLabel label = new JLabel();

	public A121(){
		
		jToolBar.add(jbt1);//加入選項
		jToolBar.add(jbt2);
		jToolBar.add(jbt3);
		jToolBar.add(jbt4);
		jToolBar.add(jbt5);

		menu.add(Canada);//加入選項
		menu.add(China);
		menu.add(England);
		menu.add(German);
		menu.add(USA);
		menu.add(exit);
		menuBar.add(menu);

		panel.setLayout(new GridLayout(2, 1));//排版
		
		setJMenuBar(menuBar);
		panel.add(menuBar);
		panel.add(jToolBar);
		add(panel, BorderLayout.NORTH);//排版
		add(label, BorderLayout.CENTER);
		setSize(1000,600);
		
		jbt1.addActionListener(this);
		jbt2.addActionListener(this);
		jbt3.addActionListener(this);
		jbt4.addActionListener(this);
		jbt5.addActionListener(this);
		Canada.addActionListener(this);
		China.addActionListener(this);
		England.addActionListener(this);
		German.addActionListener(this);
		USA.addActionListener(this);
		exit.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == jbt1)//若按下按鈕,則...
		{
			label.setIcon(pic1);
		} else if (e.getSource() == jbt2) 
		{
			label.setIcon(pic2);
		} else if (e.getSource() == jbt3) 
		{
			label.setIcon(pic3);
		} else if (e.getSource() == jbt4) 
		{
			label.setIcon(pic4);
		} else if (e.getSource() == jbt5) 
		{
			label.setIcon(pic5);
		} else if (e.getSource() == Canada) 
		{
			label.setIcon(pic1);
		} else if (e.getSource() == China) 
		{
			label.setIcon(pic2);
		} else if (e.getSource() == England) 
		{
			label.setIcon(pic3);
		} else if (e.getSource() == German) 
		{
			label.setIcon(pic4);
		} else if (e.getSource() == USA) 
		{
			label.setIcon(pic5);
		}else if (e.getSource() == exit)//離開
		{
			System.exit(0);
		}
	}
}
