package a12.s995002203;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.LineBorder;


public class A121 extends JApplet {
	private JMenuItem jmcanada, jmchina, jmgermen, jmengland, jmusa;
	private JButton jbtcanada = new JButton(new ImageIcon(getClass().getResource("/image/Canada_Icon.gif")));
	private JButton jbtchina = new JButton(new ImageIcon(getClass().getResource("/image/China_Icon.gif")));
	private JButton jbtengland = new JButton(new ImageIcon(getClass().getResource("/image/England_Icon.gif")));
	private JButton jbtgermen = new JButton(new ImageIcon(getClass().getResource("/image/German_Icon.gif")));
	private JButton jbtusa = new JButton(new ImageIcon(getClass().getResource("/image/USA_Icon.gif")));
	private JLabel imageLabel=new JLabel();
    private ImageIcon image1 = new ImageIcon("image/Canada.gif");
	private ImageIcon image2 = new ImageIcon("image/China.gif");
	private ImageIcon image3 = new ImageIcon("image/England.gif");
	private ImageIcon image4 = new ImageIcon("image/German.gif");
	private ImageIcon image5 = new ImageIcon("image/USA.gif");
	
	ActionListener acListener = new acListener();
	public A121(){
		JMenuBar jmb = new JMenuBar();
		// Set menu bar to the applet
	    setJMenuBar(jmb);

	    // Add menu "Operation" to menu bar
	    JMenu choose = new JMenu("Choose contry");
	    choose.setMnemonic('C');
	    jmb.add(choose);
	    choose.add(jmcanada= new JMenuItem("Canada", 'C'));
	    choose.add(jmchina= new JMenuItem("China", 'C'));
	    choose.add(jmengland= new JMenuItem("England", 'E'));
	    choose.add(jmgermen= new JMenuItem("Germen", 'G'));
	    choose.add(jmusa= new JMenuItem("U.S.A.", 'U'));
	    
	    jmcanada.addActionListener(acListener);
	    jmchina.addActionListener(acListener);
	    jmengland.addActionListener(acListener);
	    jmgermen.addActionListener(acListener);
	    jmusa.addActionListener(acListener);
	    
	    JToolBar jToolBar1 = new JToolBar("My Tool Bar");
	    jToolBar1.setFloatable(false);
	    jToolBar1.setBorder(BorderFactory.createLineBorder(Color.BLACK));
	    jToolBar1.add(jbtcanada);
	    jToolBar1.add(jbtchina);
	    jToolBar1.add(jbtengland);
	    jToolBar1.add(jbtgermen);
	    jToolBar1.add(jbtusa);

	    jbtcanada.setToolTipText("Canada");
	    jbtchina.setToolTipText("China");
	    jbtengland.setToolTipText("England");
	    jbtgermen.setToolTipText("Germen");
	    jbtusa.setToolTipText("USA");
	    
	    
	    
	    jbtcanada.setBorderPainted(false);
	    jbtchina.setBorderPainted(false);
	    jbtengland.setBorderPainted(false);
	    jbtgermen.setBorderPainted(false);
	    jbtusa.setBorderPainted(false);
	    
	    
	    jbtcanada.addActionListener(acListener);
	    jbtchina.addActionListener(acListener);
	    jbtengland.addActionListener(acListener);
	    jbtgermen.addActionListener(acListener);
	    jbtusa.addActionListener(acListener);
	    
	    add(jToolBar1, BorderLayout.SOUTH);
	}

	class acListener implements ActionListener {//使用者輸入speed之後要做的事
		public void actionPerformed(ActionEvent e) {
			if(e.getSource()==jmcanada||e.getSource()==jbtcanada){
				imageLabel.setIcon(image1);
				imageLabel.setHorizontalAlignment(JLabel.CENTER);
				add(imageLabel);
			}
			else if(e.getSource()==jmchina||e.getSource()==jbtchina){
				imageLabel.setIcon(image2);
				imageLabel.setHorizontalAlignment(JLabel.CENTER);
				add(imageLabel);
			}
			else if(e.getSource()==jmengland||e.getSource()==jbtengland){
				imageLabel.setIcon(image3);
				imageLabel.setHorizontalAlignment(JLabel.CENTER);
				add(imageLabel);
			}
			else if(e.getSource()==jmgermen||e.getSource()==jbtgermen){
				imageLabel.setIcon(image4);
				imageLabel.setHorizontalAlignment(JLabel.CENTER);
				add(imageLabel);
			}
			else if(e.getSource()==jmusa||e.getSource()==jbtusa){
				imageLabel.setIcon(image5);
				imageLabel.setHorizontalAlignment(JLabel.CENTER);
				add(imageLabel);
			}
		}
	}
}
