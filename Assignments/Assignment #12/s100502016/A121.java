package a12.s100502016;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import org.w3c.dom.views.AbstractView;

public class A121 extends JApplet {
	private JMenuBar jmb = new JMenuBar();
	private JMenu fileMenu = new JMenu("File");
	private JMenu iconMenu = new JMenu("Icon");

	private ImageIcon imgIconCanada = new ImageIcon("image/Canada_Icon.gif");
	private ImageIcon imgIconChina = new ImageIcon("image/China_Icon.gif");
	private ImageIcon imgIconEngland = new ImageIcon("image/England_Icon.gif");
	private ImageIcon imgIconGerman = new ImageIcon("image/German_Icon.gif");
	private ImageIcon imgIconUSA = new ImageIcon("image/USA_Icon.gif");
	private ImageIcon imgCanada = new ImageIcon("image/Canada.gif");
	private ImageIcon imgChina = new ImageIcon("image/China.gif");
	private ImageIcon imgEngland = new ImageIcon("image/England.gif");
	private ImageIcon imgGerman = new ImageIcon("image/German.gif");
	private ImageIcon imgUSA = new ImageIcon("image/USA.gif");

	private Action exitAction = new MyAction("Exit", imgIconCanada);
	private Action CanadaAction = new MyAction("Canada", imgIconCanada);
	private Action ChinaAction = new MyAction("China", imgIconChina);
	private Action EnglandAction = new MyAction("England", imgIconEngland);
	private Action GermanAction = new MyAction("German", imgIconGerman);
	private Action USAAction = new MyAction("USA", imgIconUSA);

	private JButton btnCanada = new JButton(CanadaAction);
	private JButton btnChina = new JButton(ChinaAction);
	private JButton btnEngland = new JButton(EnglandAction);
	private JButton btnGerman = new JButton(GermanAction);
	private JButton btnUSA = new JButton(USAAction);

	private JToolBar jtb = new JToolBar();

	private JLabel lbl = new JLabel(imgCanada);
	private JPanel pnl = new JPanel();

	public A121() {
		pnl.setLayout(new GridLayout(2, 1));
		jmb.add(fileMenu);
		jmb.add(iconMenu);
		fileMenu.add(exitAction);
		iconMenu.add(CanadaAction);
		iconMenu.add(ChinaAction);
		iconMenu.add(EnglandAction);
		iconMenu.add(GermanAction);
		iconMenu.add(USAAction);

		jtb.add(btnCanada);
		jtb.add(btnChina);
		jtb.add(btnEngland);
		jtb.add(btnGerman);
		jtb.add(btnUSA);

		pnl.add(jmb);
		pnl.add(jtb);
		setLayout(new BorderLayout());
		add(pnl, BorderLayout.NORTH);
		add(lbl, BorderLayout.CENTER);
	}

	private class MyAction extends AbstractAction {
		String name;

		MyAction(String name, Icon icon) {
			// TODO Auto-generated constructor stub
			super(name, icon);
			this.name = name;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if (name.equals("Canada")) {
				lbl.setIcon(imgCanada);
			} else if (name.equals("China")) {
				lbl.setIcon(imgChina);
			} else if (name.equals("England")) {
				lbl.setIcon(imgEngland);
			} else if (name.equals("German")) {
				lbl.setIcon(imgGerman);
			} else if (name.equals("USA")) {
				lbl.setIcon(imgUSA);
			} else if (name.equals("Exit")) {
				System.exit(0);
			}
		}
	}

}
