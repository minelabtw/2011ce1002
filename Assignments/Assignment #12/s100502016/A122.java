package a12.s100502016;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.Format;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.swing.JApplet;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.Timer;

public class A122 extends JApplet {

	private JTextField txt1 = new JTextField();
	private JTextField txt2 = new JTextField();

	private JComboBox jcbCountry = new JComboBox();
	private JComboBox jcbTimeZone = new JComboBox();
	private JComboBox jcbDateStyle = new JComboBox();
	private JComboBox jcbTimeStyle = new JComboBox();

	private Locale locale = Locale.getDefault();
	private Locale[] locales = { Locale.ENGLISH, Locale.JAPAN, Locale.CHINESE,
			Locale.FRANCE };
	private ResourceBundle res = ResourceBundle.getBundle("MyResource", locale);

	private TimeZone timeZone = TimeZone.getTimeZone("EST");
	private TimeZone[] timeZones;
	private String[] availableTimeZones = TimeZone.getAvailableIDs();

	private DateFormat dateInstance;
	private DateFormat timeInstance;

	private enum Style {
		Full, Long, Medium, Short
	};

	private int dateStyle;
	private int timeStyle;

	private JLabel lbl_0 = new JLabel(res.getString("Date"));
	private JLabel lbl_1 = new JLabel(res.getString("Time"));
	private JLabel lbl_2 = new JLabel(res.getString("Choose_locale"));
	private JLabel lbl_3 = new JLabel(res.getString("Time_Zone"));
	private JLabel lbl_4 = new JLabel(res.getString("Date_Style"));
	private JLabel lbl_5 = new JLabel(res.getString("Time_Style"));

	public void initializeAllCombox() {
		for (int i = 0; i < locales.length; i++) {
			jcbCountry.addItem(locales[i].getDisplayName());
		}
		timeZones = new TimeZone[availableTimeZones.length];
		for (int i = 0; i < availableTimeZones.length; i++) {
			jcbTimeZone.addItem(availableTimeZones[i]);
			timeZones[i] = TimeZone.getTimeZone(availableTimeZones[i]);
		}
		jcbDateStyle.addItem(Style.Full);
		jcbDateStyle.addItem(Style.Long);
		jcbDateStyle.addItem(Style.Medium);
		jcbDateStyle.addItem(Style.Short);
		jcbTimeStyle.addItem(Style.Full);
		jcbTimeStyle.addItem(Style.Long);
		jcbTimeStyle.addItem(Style.Medium);
		jcbTimeStyle.addItem(Style.Short);

		
		

	}

	public A122() {
		initializeAllCombox();
		setLayout(new GridLayout(3, 2, 5, 5));
		txt1.setEditable(false);
		txt2.setEditable(false);
		add(lbl_0);
		add(txt1);
		add(lbl_1);
		add(txt2);
		add(lbl_2);
		add(jcbCountry);
		add(lbl_3);
		add(jcbTimeZone);
		add(lbl_4);
		add(jcbDateStyle);
		add(lbl_5);
		add(jcbTimeStyle);
		jcbCountry.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				locale = locales[jcbCountry.getSelectedIndex()];
				updateString();
			}
		});
		jcbTimeZone.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				  timeZone = timeZones[jcbTimeZone.getSelectedIndex()];
			}
		});
		jcbDateStyle.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (jcbDateStyle.getSelectedIndex() == 0) {
					dateStyle = DateFormat.FULL;
				} else if (jcbDateStyle.getSelectedIndex() == 1) {
					dateStyle = DateFormat.LONG;
				} else if (jcbDateStyle.getSelectedIndex() == 2) {
					dateStyle = DateFormat.MEDIUM;
				} else if (jcbDateStyle.getSelectedIndex() == 3) {
					dateStyle = DateFormat.SHORT;
				}
			}
		});
		jcbTimeStyle.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (jcbTimeStyle.getSelectedIndex() == 0) {
					timeStyle = DateFormat.FULL;
				} else if (jcbTimeStyle.getSelectedIndex() == 1) {
					timeStyle = DateFormat.LONG;
				} else if (jcbTimeStyle.getSelectedIndex() == 2) {
					timeStyle = DateFormat.MEDIUM;
				} else if (jcbTimeStyle.getSelectedIndex() == 3) {
					timeStyle = DateFormat.SHORT;
				}
			}
		});
		timer.start();
		System.out.print(DateFormat.LONG);
	}

	private void updateString() {
		res = ResourceBundle.getBundle("MyResource", locale);
		lbl_0.setText(res.getString("Date"));
		lbl_1.setText(res.getString("Time"));
		lbl_2.setText(res.getString("Choose_locale"));
		lbl_3.setText(res.getString("Time_Zone"));
		lbl_4.setText(res.getString("Date_Style"));
		lbl_5.setText(res.getString("Time_Style"));
		repaint();
	}

	private Timer timer = new Timer(1000, new updateTime());

	class updateTime implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			Calendar calendar = new GregorianCalendar(timeZone, getLocale());

			dateInstance = DateFormat.getDateInstance(dateStyle);
			dateInstance.setTimeZone(timeZone);
			txt1.setText(dateInstance.format(calendar.getTime()));
			timeInstance = DateFormat.getTimeInstance(timeStyle);
			timeInstance.setTimeZone(timeZone);
			txt2.setText(timeInstance.format(calendar.getTime()));

		}

	}
}
