package a12.s992001033;

import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.util.*;

import javax.swing.*;

public class A122 extends JApplet
{
	public A122()
	{
		add(new DateJPanelWork());
	}
}
class DateJPanelWork extends JPanel implements ActionListener
{
	private Locale[] availableLocales = {Locale.ENGLISH,Locale.FRANCE,Locale.JAPAN,Locale.CHINESE};//country
	private String[] availableTimeZones = TimeZone.getAvailableIDs();//zone
	private String[] DateStyle ={"FULL","LONG","MEDIUM","SHORT"};//styls
	private String[] TimeStyle ={"FULL","LONG","MEDIUM","SHORT"};
	private ResourceBundle res = ResourceBundle.getBundle("MyResource");
	
	Locale  locale = availableLocales[0];//default locale
	
	int DFormat =DateFormat.FULL;//date format
	int TFormat =DateFormat.FULL;//time format
	
	JPanel datePanel = new JPanel();//panel
	JPanel localePanel = new JPanel();
	JPanel stylePanel = new JPanel();
	
	JLabel date = new JLabel(res.getString("Date"));//label
	JLabel time = new JLabel(res.getString("Time"));
	JLabel chooseLocale = new JLabel(res.getString("Choose_locale"));
	JLabel timeZone = new JLabel(res.getString("Time_Zone"));
	JLabel dateStyle = new JLabel(res.getString("Date_Style"));
	JLabel timeStyle = new JLabel(res.getString("Time_Style"));
	
	JTextField Date = new JTextField();//other component
	JTextField Time = new JTextField();
	JComboBox jcbLocales =new JComboBox();
	JComboBox jcbTimeZones =new JComboBox();
	JComboBox jcbDateStyle =new JComboBox();
	JComboBox jcbTimeStyle =new JComboBox();
	
	
	TimeZone zone = TimeZone.getTimeZone(availableTimeZones[0]);//zone
	DateFormat formatter1 =DateFormat.getDateInstance(DFormat, locale);//date format
	DateFormat formatter2 =DateFormat.getTimeInstance(TFormat, locale);//time format
	Calendar calender = new GregorianCalendar(zone,getLocale());//calendar
	
	
	DateJPanelWork()
	{
		
		
		change();//Initial
		setAvailableLocales();
		setAvailableZones();
		setjcbDateStyle();
		setTimeStyle();
		
		Date.setEditable(false);//can't change by keybord
		Time.setEditable(false);
		
		datePanel.setLayout(new GridLayout(1,4));//layout
		localePanel.setLayout(new GridLayout(1,4));
		stylePanel.setLayout(new GridLayout(1,4));
		setLayout(new GridLayout(3,1));
		
		datePanel.add(date);//add
		datePanel.add(Date);
		datePanel.add(time);
		datePanel.add(Time);
		localePanel.add(chooseLocale);
		localePanel.add(jcbLocales);
		localePanel.add(timeZone);
		localePanel.add(jcbTimeZones);
		stylePanel.add(dateStyle);
		stylePanel.add(jcbDateStyle);
		stylePanel.add(timeStyle);
		stylePanel.add(jcbTimeStyle);
		
		add(datePanel);
		add(localePanel);
		add(stylePanel);
		
		jcbLocales.addActionListener(new ActionListener()//JComboBox of locale
		{
			public void actionPerformed(ActionEvent e)
			{
				locale = availableLocales[jcbLocales.getSelectedIndex()];
				updateStrings();
				change();
			}
		});
		jcbTimeZones.addActionListener(new ActionListener()//JComboBox of time zone
		{
			public void actionPerformed(ActionEvent e)
			{
				zone = TimeZone.getTimeZone(availableTimeZones[jcbTimeZones.getSelectedIndex()]);
				formatter1.setTimeZone(zone);
				formatter2.setTimeZone(zone);
				change();
			}
		});
		jcbDateStyle.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if(jcbDateStyle.getSelectedIndex()==0)
					DFormat=DateFormat.FULL;
				else if(jcbDateStyle.getSelectedIndex()==1)
					DFormat=DateFormat.LONG;
				else if(jcbDateStyle.getSelectedIndex()==2)
					DFormat=DateFormat.MEDIUM;
				else if(jcbDateStyle.getSelectedIndex()==3)
					DFormat=DateFormat.SHORT;
				formatter1 =DateFormat.getDateInstance(DFormat, locale);
				change();
			}
		});
		jcbTimeStyle.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				if(jcbTimeStyle.getSelectedIndex()==0)
					TFormat=DateFormat.FULL;
				else if(jcbTimeStyle.getSelectedIndex()==1)
					TFormat=DateFormat.LONG;
				else if(jcbTimeStyle.getSelectedIndex()==2)
					TFormat=DateFormat.MEDIUM;
				else if(jcbTimeStyle.getSelectedIndex()==3)
					TFormat=DateFormat.SHORT;
				formatter2 =DateFormat.getTimeInstance(TFormat, locale);
				change();
			}
		});
		
	}
	private void setAvailableLocales()//set locale
	{
		for(int i = 0; i<availableLocales.length;i++)
		{
			jcbLocales.addItem(availableLocales[i].getDisplayName()+availableLocales[i].getDisplayCountry());
		}
	}
	private void setAvailableZones()//set zone
	{
		for(int i = 0; i<availableTimeZones.length;i++)
		{
			Arrays.sort(availableTimeZones);
			jcbTimeZones.addItem(availableTimeZones[i]);
		}
	}
	private void setjcbDateStyle()//set style
	{
		for(int i = 0; i<DateStyle.length;i++)
		{
			jcbDateStyle.addItem(DateStyle[i]);
		}
	}
	private void setTimeStyle()
	{
		for(int i = 0; i<TimeStyle.length;i++)
		{
			jcbTimeStyle.addItem(TimeStyle[i]);
		}
	}
	public void actionPerformed(ActionEvent e)
	{
		
	}
	private void updateStrings()//change  the text on the panel
	{
		res = ResourceBundle.getBundle("MyResource", locale);
		date.setText(res.getString("Date"));
		time.setText(res.getString("Time"));
		chooseLocale.setText(res.getString("Choose_locale"));
		timeZone.setText(res.getString("Time_Zone"));
		dateStyle.setText(res.getString("Date_Style"));
		timeStyle.setText(res.getString("Time_Style"));
	}
	private void change()//change format or locale or zone
	{
		formatter1 =DateFormat.getDateInstance(DFormat, locale);
		formatter2 =DateFormat.getTimeInstance(TFormat, locale);
		Date.setText(formatter1.format(calender.getTime()));
		Time.setText(formatter2.format(calender.getTime()));
	}
}