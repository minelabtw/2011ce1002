package a12.s992001033;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class A121 extends JApplet
{
	public A121()
	{
		JPanelWork p1=new JPanelWork();
		add(p1);
		setJMenuBar(p1.jmb);
	}
}
class JPanelWork extends JPanel implements ActionListener
{
	JMenu fileMenu = new JMenu("File");//menu
	JMenuItem exit = new JMenuItem("exit");
	JMenuItem CanadaM = new JMenuItem("Canada");//menu的button
	JMenuItem ChinaM = new JMenuItem("China");
	JMenuItem EnglandM = new JMenuItem("England");
	JMenuItem GermanM = new JMenuItem("German");
	JMenuItem USAM = new JMenuItem("USA");
	JButton Canada = new JButton("Canada");//toolbar的button
	JButton China = new JButton("China");
	JButton England = new JButton("England");
	JButton German = new JButton("German");
	JButton USA = new JButton("USA");
	JMenuBar jmb = new JMenuBar();
	JToolBar jToolBar1 = new JToolBar();
	JLabel ImageViewer = new JLabel();
	JPanelWork()
	{
		
		jToolBar1.setFloatable(true);
		//5個icon
		Canada.setIcon(new ImageIcon("image/Canada_Icon.gif"));
		China.setIcon(new ImageIcon("image/China_Icon.gif"));
		England.setIcon(new ImageIcon("image/England_Icon.gif"));
		German.setIcon(new ImageIcon("image/German_Icon.gif"));
		USA.setIcon(new ImageIcon("image/USA_Icon.gif"));
		
		CanadaM.setIcon(new ImageIcon("image/Canada_Icon.gif"));
		ChinaM.setIcon(new ImageIcon("image/China_Icon.gif"));
		EnglandM.setIcon(new ImageIcon("image/England_Icon.gif"));
		GermanM.setIcon(new ImageIcon("image/German_Icon.gif"));
		USAM.setIcon(new ImageIcon("image/USA_Icon.gif"));
		
		Canada.addActionListener(this);
		China.addActionListener(this);
		England.addActionListener(this);
		German.addActionListener(this);
		USA.addActionListener(this);
		//add進去
		jmb.add(fileMenu);
		
		fileMenu.add(CanadaM);
		fileMenu.add(ChinaM);
		fileMenu.add(EnglandM);
		fileMenu.add(GermanM);
		fileMenu.add(USAM);
		fileMenu.add(exit);
		
		jToolBar1.add(Canada);
		jToolBar1.add(China);
		jToolBar1.add(England);
		jToolBar1.add(German);
		jToolBar1.add(USA);
		jToolBar1.setLayout(new GridLayout(5,1));
		//調整排版
		setLayout(new BorderLayout());
		add(jToolBar1,BorderLayout.EAST);
		add(ImageViewer,BorderLayout.CENTER);
		//離開
		exit.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					System.exit(0);
				}
			}
		);//menu's action
		CanadaM.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					ImageViewer.setIcon(new ImageIcon("image/Canada.gif"));
				}
			}
		);
		ChinaM.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					ImageViewer.setIcon(new ImageIcon("image/China.gif"));
				}
			}
		);
		EnglandM.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					ImageViewer.setIcon(new ImageIcon("image/England.gif"));
				}
			}
		);
		GermanM.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					ImageViewer.setIcon(new ImageIcon("image/German.gif"));
				}
			}
		);
		USAM.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					ImageViewer.setIcon(new ImageIcon("image/USA.gif"));
				}
			}
		);
	}
	public void actionPerformed(ActionEvent e)//toolbar's button
	{
			if(e.getSource()==Canada)
				ImageViewer.setIcon(new ImageIcon("image/Canada.gif"));
			else if(e.getSource()==China)
				ImageViewer.setIcon(new ImageIcon("image/China.gif"));
			else if(e.getSource()==England)
				ImageViewer.setIcon(new ImageIcon("image/England.gif"));
			else if(e.getSource()==German)
				ImageViewer.setIcon(new ImageIcon("image/German.gif"));
			else if(e.getSource()==USA)
				ImageViewer.setIcon(new ImageIcon("image/USA.gif"));		
	}
}