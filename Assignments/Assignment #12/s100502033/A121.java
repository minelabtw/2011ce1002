package a12.s100502033;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
public class A121 extends JApplet
{
	
	
	private ImageIcon USAIcon = new ImageIcon(getClass().getResource("/image/USA_Icon.gif"));
    private ImageIcon USA = new ImageIcon(getClass().getResource("/image/USA.gif"));
	private ImageIcon CanadaIcon =  new ImageIcon(getClass().getResource("/image/Canada_Icon.gif"));
	private ImageIcon Canada =  new ImageIcon(getClass().getResource("/image/Canada.gif"));
	private ImageIcon EnglandIcon =  new ImageIcon(getClass().getResource("/image/England_Icon.gif"));
	private ImageIcon England =  new ImageIcon(getClass().getResource("/image/England.gif"));
	private ImageIcon ChinaIcon =  new ImageIcon(getClass().getResource("/image/China_Icon.gif"));
	private ImageIcon China =  new ImageIcon(getClass().getResource("/image/China.gif"));
	private ImageIcon GermanIcon =  new ImageIcon(getClass().getResource("/image/German_Icon.gif"));
	private ImageIcon German =  new ImageIcon(getClass().getResource("/image/German.gif"));
	private JLabel jlblImage = new JLabel(USAIcon, JLabel.CENTER);
	private JDesktopPane desktop = new JDesktopPane();//宣告一個很像桌面的視窗
	private JInternalFrame internalFrame = new JInternalFrame("", false, false, true, false);//一個有最小化，最大化，關閉的功能
	private JMenuItem Ca = new JMenuItem("Canada");
	private JMenuItem Ch = new JMenuItem("China");
	private JMenuItem Eng = new JMenuItem("England");
	private JMenuItem Ge = new JMenuItem("German");
	private JMenuItem US = new JMenuItem("USA");
    class MyAction extends AbstractAction  //用ACTION來運行
	{
	    String name;

	    MyAction(String name, Icon icon) 
	    {
	      super(name, icon);
	      this.name = name;
	    }
	    public void actionPerformed(ActionEvent event)
	    {
	    	if (name.equals("Canada"))
	    	{
	    		jlblImage.setIcon(Canada);//改變圖片
				internalFrame.setFrameIcon(CanadaIcon);//改變標題ICON
				internalFrame.add(jlblImage);
				internalFrame.setTitle("Canada");//改變標題名稱
	    	}
	    	else if(name.equals("China"))
	    	{
				jlblImage.setIcon(China);//改變圖片
				internalFrame.setFrameIcon(ChinaIcon);//改變標題ICON
				internalFrame.add(jlblImage);
				internalFrame.setTitle("China");//改變標題名稱
	    	}
	    	else if(name.equals("England"))
	    	{
				jlblImage.setIcon(England);//改變圖片
				internalFrame.setFrameIcon(EnglandIcon);//改變標題ICON
				internalFrame.add(jlblImage);
				internalFrame.setTitle("England");//改變標題名稱
	    	}
	    	else if(name.equals("German"))
	    	{
	    		
	    		jlblImage.setIcon(German);//改變圖片
				internalFrame.setFrameIcon(GermanIcon);//改變標題ICON
				internalFrame.add(jlblImage);
				internalFrame.setTitle("German");//改變標題名稱
	    	}
	    	else if(name.equals("USA"))
	    	{
				jlblImage.setIcon(USA);//改變圖片
				internalFrame.setFrameIcon(USAIcon);//改變標題ICON
				internalFrame.add(jlblImage);
				internalFrame.setTitle("USA");//改變標題名稱
	    	}
	    }
	}
	public A121()
	{
		Action Canada2 = new MyAction("Canada" , CanadaIcon);//89~108都是ADD入物件
		Action USA2 = new MyAction("USA" , USAIcon);
		Action China2 = new MyAction("China" , ChinaIcon);
		Action German2 = new MyAction("German" , GermanIcon);
		Action England2 = new MyAction("England" , EnglandIcon);
		desktop.add(internalFrame);
		this.setSize(new Dimension(400, 300));
		this.getContentPane().add(desktop, BorderLayout.CENTER);
		internalFrame.setLocation(100, 100);
		internalFrame.setSize(500, 500);
		internalFrame.setVisible(true);
		JMenuBar JB = new JMenuBar();    
		this.setJMenuBar(JB);
		JMenu fileMenu = new JMenu("Flags");
		JB.add(fileMenu);
	    JToolBar jToolBar1 = new JToolBar(JToolBar.VERTICAL);
	    jToolBar1.setFloatable(false);
	    jToolBar1.setBorder(BorderFactory.createLineBorder(Color.BLACK));
	    jToolBar1.add(Canada2);
	    jToolBar1.add(China2);
	    jToolBar1.add(German2);
	    jToolBar1.add(England2);
	    jToolBar1.add(USA2);
		fileMenu.add(Canada2);
		fileMenu.add(China2);
		fileMenu.add(England2);
		fileMenu.add(German2);
		fileMenu.add(USA2);
		add(jToolBar1, BorderLayout.EAST);
	}
}
