package a12.s995002026;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import java.awt.*;
import java.net.URL;

/*----------------------------------在JFrame裡面成功的執行了，但是改成JApplet就讀不到圖了--------------------------*/


public class A121 extends JApplet {
	FrameWork f = new FrameWork();

	public A121() {
		add(f);
	}
}

class FrameWork extends JApplet implements ActionListener {
	Paint paint = new Paint();

	JMenu country = new JMenu("Country"); // 菜單

	JMenuBar mbar = new JMenuBar(); // 菜單列

	JButton b1 = new JButton(new ImageIcon("bin\\image\\Canada_icon.gif")); // tool

	JButton b2 = new JButton(new ImageIcon("bin\\image\\China_icon.gif"));

	JButton b3 = new JButton(new ImageIcon("bin\\image\\England_icon.gif"));

	JButton b4 = new JButton(new ImageIcon("bin\\image\\German_icon.gif"));

	JButton b5 = new JButton(new ImageIcon("bin\\image\\USA_icon.gif"));

	JToolBar tbar = new JToolBar();

	JMenuItem canada = new JMenuItem("Canada"); // 菜單選項

	JMenuItem china = new JMenuItem("China");

	JMenuItem england = new JMenuItem("England");

	JMenuItem german = new JMenuItem("German");

	JMenuItem usa = new JMenuItem("USA");

	JPanel p = new JPanel();


	FrameWork() {
		canada.addActionListener(this);
		china.addActionListener(this);
		england.addActionListener(this);
		german.addActionListener(this);
		usa.addActionListener(this);
		b1.addActionListener(this);
		b2.addActionListener(this);
		b3.addActionListener(this);
		b4.addActionListener(this);
		b5.addActionListener(this);
		mbar.add(country);
		tbar.add(b1);
		tbar.add(b2);
		tbar.add(b3);
		tbar.add(b4);
		tbar.add(b5);
		country.add(canada);
		country.add(china);
		country.add(england);
		country.add(german);
		country.add(usa);
		add(mbar, BorderLayout.NORTH);
		p.add(tbar);
		add(paint);
		add(p, BorderLayout.SOUTH);
	}

	public void actionPerformed(ActionEvent e) {		//換圖指令
		if (e.getSource() == b1) {
			paint.draw1();
			repaint();
		}
		if (e.getSource() == b2) {
			paint.draw2();
			repaint();
		}
		if (e.getSource() == b3) {
			paint.draw3();
			repaint();
		}
		if (e.getSource() == b4) {
			paint.draw4();
			repaint();
		}
		if (e.getSource() == b5) {
			paint.draw5();
			repaint();
		}
		if (e.getSource() == canada) {
			paint.draw6();
			repaint();
		}
		if (e.getSource() == china) {
			paint.draw7();
			repaint();
		}
		if (e.getSource() == england) {
			paint.draw8();
			repaint();
		}
		if (e.getSource() == german) {
			paint.draw9();
			repaint();
		}
		if (e.getSource() == usa) {
			paint.draw10();
			repaint();
		}
	}
}