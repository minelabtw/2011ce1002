package a12.s100502015;

import java.awt.*;
import java.awt.event.*;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.swing.*;

public class A122 extends JApplet {
	private JTextField jtf1 = new JTextField();
	private JTextField jtf2 = new JTextField();
	private JComboBox jcbCountry = new JComboBox();
	private JComboBox jcbTimeZone = new JComboBox();
	private JComboBox jcbDateStyle = new JComboBox();
	private JComboBox jcbTimeStyle = new JComboBox();
	private Locale localelan = Locale.getDefault();
	private Locale[] localelans = { Locale.ENGLISH, Locale.JAPAN,
			Locale.CHINESE, Locale.FRANCE };

	private enum Style {
		Full, Long, Short
	};

	private ResourceBundle resbun = ResourceBundle.getBundle("MyResource",
			localelan);
	private TimeZone timeZone = TimeZone.getTimeZone("EST");
	private String[] availableTimeZones = TimeZone.getAvailableIDs();
	private TimeZone[] timeZones;
	private DateFormat dateInstance;
	private DateFormat timeInstance;
	private int dateStyle;
	private int timeStyle;
	private JLabel l0 = new JLabel(resbun.getString("Date"));
	private JLabel l1 = new JLabel(resbun.getString("Time"));
	private JLabel l2 = new JLabel(resbun.getString("Choose_locale"));
	private JLabel l3 = new JLabel(resbun.getString("Time_Zone"));
	private JLabel l4 = new JLabel(resbun.getString("Date_Style"));
	private JLabel l5 = new JLabel(resbun.getString("Time_Style"));

	public A122() {
		initializeAllCombox();
		setLayout(new GridLayout(3, 2, 5, 5));
		jtf1.setEditable(false);
		jtf2.setEditable(false);
		add(l0);
		add(jtf1);
		add(l1);
		add(jtf2);
		add(l2);
		add(jcbCountry);
		add(l3);
		add(jcbTimeZone);
		add(l4);
		add(jcbDateStyle);
		add(l5);
		add(jcbTimeStyle);
		//select country
		jcbCountry.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				localelan = localelans[jcbCountry.getSelectedIndex()];
				updateString();
			}
		});
		//select timezone
		jcbTimeZone.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				timeZone = timeZones[jcbTimeZone.getSelectedIndex()];
			}
		});
		//select datastyle
		jcbDateStyle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (jcbDateStyle.getSelectedIndex() == 0) {
					dateStyle = DateFormat.FULL;
				} else if (jcbDateStyle.getSelectedIndex() == 1) {
					dateStyle = DateFormat.LONG;
				} else if (jcbDateStyle.getSelectedIndex() == 2) {
					dateStyle = DateFormat.SHORT;
				}
			}
		});
		//select timestyle
		jcbTimeStyle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (jcbTimeStyle.getSelectedIndex() == 0) {
					timeStyle = DateFormat.FULL;
				} else if (jcbTimeStyle.getSelectedIndex() == 1) {
					timeStyle = DateFormat.LONG;
				} else if (jcbTimeStyle.getSelectedIndex() == 2) {
					timeStyle = DateFormat.SHORT;
				}
			}
		});
		timer.start();
	}

	private void initializeAllCombox() {
		
		for (int i = 0; i < localelans.length; i++) {
			jcbCountry.addItem(localelans[i].getDisplayName());
		}
		timeZones = new TimeZone[availableTimeZones.length];
		for (int i = 0; i < availableTimeZones.length; i++) {
			jcbTimeZone.addItem(availableTimeZones[i]);
			timeZones[i] = TimeZone.getTimeZone(availableTimeZones[i]);
		}
		jcbTimeStyle.addItem(Style.Full);
		jcbTimeStyle.addItem(Style.Long);
		jcbTimeStyle.addItem(Style.Short);
		jcbDateStyle.addItem(Style.Full);
		jcbDateStyle.addItem(Style.Long);
		jcbDateStyle.addItem(Style.Short);
	}

	private void updateString() {
		
		resbun = ResourceBundle.getBundle("MyResource", localelan);
		l0.setText(resbun.getString("Date"));
		l1.setText(resbun.getString("Time"));
		l2.setText(resbun.getString("Choose_locale"));
		l3.setText(resbun.getString("Time_Zone"));
		l4.setText(resbun.getString("Date_Style"));
		l5.setText(resbun.getString("Time_Style"));
		repaint();
	}

	private Timer timer = new Timer(1000, new updateTime());

	class updateTime implements ActionListener {
		//refresh time
		public void actionPerformed(ActionEvent arg0) {
			Calendar calendar = new GregorianCalendar(timeZone, getLocale());
			
			timeInstance = DateFormat.getTimeInstance(timeStyle);
			timeInstance.setTimeZone(timeZone);
			jtf2.setText(timeInstance.format(calendar.getTime()));
			dateInstance = DateFormat.getDateInstance(dateStyle);
			dateInstance.setTimeZone(timeZone);
			jtf1.setText(dateInstance.format(calendar.getTime()));

		}

	}
}
