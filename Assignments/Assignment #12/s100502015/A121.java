package a12.s100502015;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class A121 extends JApplet implements ActionListener {
	private ImageIcon i1 = new ImageIcon("image/Canada.gif");
	private ImageIcon i2 = new ImageIcon("image/China.gif");
	private ImageIcon i3 = new ImageIcon("image/England.gif");
	private ImageIcon i4 = new ImageIcon("image/German.gif");
	private ImageIcon i5 = new ImageIcon("image/USA.gif");
	private JButton jbt1 = new JButton(new ImageIcon(("image/Canada_Icon.gif")));
	private JButton jbt2 = new JButton(new ImageIcon(("image/China_Icon.gif")));
	private JButton jbt3 = new JButton(
			new ImageIcon(("image/England_Icon.gif")));
	private JButton jbt4 = new JButton(new ImageIcon(("image/German_Icon.gif")));
	private JButton jbt5 = new JButton(new ImageIcon(("image/USA_Icon.gif")));
	private JMenuBar menuBar = new JMenuBar();
	private JMenu m1 = new JMenu("File");
	private JMenuItem exit = new JMenuItem("exit");
	private JMenu m2 = new JMenu("Icon");
	private JMenuItem mCanada = new JMenuItem("Canada");
	private JMenuItem mChina = new JMenuItem("China");
	private JMenuItem mEngland = new JMenuItem("England");
	private JMenuItem mGerman = new JMenuItem("German");
	private JMenuItem mUSA = new JMenuItem("USA");
	private JToolBar jToolBar = new JToolBar("box");
	private JPanel p1 = new JPanel();
	private JLabel l1 = new JLabel();

	public A121() {
		jToolBar.add(jbt1);
		jToolBar.add(jbt2);
		jToolBar.add(jbt3);
		jToolBar.add(jbt4);
		jToolBar.add(jbt5);

		m1.add(exit);
		m2.add(mCanada);
		m2.add(mChina);
		m2.add(mEngland);
		m2.add(mGerman);
		m2.add(mUSA);
		menuBar.add(m1);
		menuBar.add(m2);

		p1.setLayout(new GridLayout(2, 1));
		jbt1.addActionListener(this);
		jbt2.addActionListener(this);
		jbt3.addActionListener(this);
		jbt4.addActionListener(this);
		jbt5.addActionListener(this);
		mCanada.addActionListener(this);
		mChina.addActionListener(this);
		mEngland.addActionListener(this);
		mGerman.addActionListener(this);
		mUSA.addActionListener(this);
		exit.addActionListener(this);
		setJMenuBar(menuBar);
		p1.add(menuBar);
		p1.add(jToolBar);
		add(p1, BorderLayout.NORTH);
		add(l1, BorderLayout.CENTER);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == jbt1) {
			l1.setIcon(i1);
		} else if (e.getSource() == jbt2) {
			l1.setIcon(i2);
		} else if (e.getSource() == jbt3) {
			l1.setIcon(i3);
		} else if (e.getSource() == jbt4) {
			l1.setIcon(i4);
		} else if (e.getSource() == jbt5) {
			l1.setIcon(i5);
		} else if (e.getSource() == exit) {
			System.exit(0);
		} else if (e.getSource() == mCanada) {
			l1.setIcon(i1);
		} else if (e.getSource() == mChina) {
			l1.setIcon(i2);
		} else if (e.getSource() == mEngland) {
			l1.setIcon(i3);
		} else if (e.getSource() == mGerman) {
			l1.setIcon(i4);
		} else if (e.getSource() == mUSA) {
			l1.setIcon(i5);
		}

	}
}
