package a12.s100502029;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.TimeZone;
import javax.swing.JApplet;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;

public class A122 extends JApplet {
	// create labels
	private JLabel jlblDate = new JLabel("Date", JLabel.CENTER);
	private JLabel jlblTime = new JLabel("Time", JLabel.CENTER);
	private JLabel jlblLocale = new JLabel("Choose Locale", JLabel.CENTER);
	private JLabel jlblTimeZone = new JLabel("Time Zone", JLabel.CENTER);
	private JLabel jlblDateStyle = new JLabel("Date Style", JLabel.CENTER);
	private JLabel jlblTimeStyle = new JLabel("Time Style", JLabel.CENTER);
	
	// combo box for selecting available locales, timezones, date styles, and time styles
	private JComboBox jcbLocales = new JComboBox();
	private JComboBox jcbTimeZones = new JComboBox();
	private JComboBox jcbDateStyles = new JComboBox();
	private JComboBox jcbTimeStyles = new JComboBox();
	
	// text field for displaying date and time
	private JTextField jtfDate = new JTextField();
	private JTextField jtfTime = new JTextField();
	
	// create four locales
	private Locale chinese = Locale.CHINESE;
	private Locale english = Locale.ENGLISH;
	private Locale japanese = Locale.JAPANESE;
	private Locale french = Locale.FRENCH;
	
	// four time zones
	private String taiwan = "Asia/Taipei"; // Etc/GMT-7
	private String usa = "US/Eastern"; // Etc/GMT+5
	private String japan = "Asia/Tokyo"; // Etc/GMT-8
	private String france = "Europe/Paris"; // Etc/GMT-1
	
	private int dateStyle = DateFormat.FULL;
	private int timeStyle = DateFormat.FULL;
	private TimeZone timeZone = TimeZone.getTimeZone(taiwan);
	private Timer timer = new Timer(1000, new TimerListener());
	private ResourceBundle res = ResourceBundle.getBundle("MyResource");
	
	public A122() {
		setLocale(english);
		
		setAvailableLocales(); // initialize jcbLocales with four locales
		setAvailableTimeZones(); // initialize jcbTimeZones with four time zones
		setAvailableDateStyles(); // initialize jcbDateStyles with four styles
		setAvailableTimeStyles(); // initialize jcbTimeStyles with four styles
		
		// set text field can not edit
		jtfDate.setEditable(false);
		jtfTime.setEditable(false);
		
		JPanel panel1 = new JPanel(new GridLayout(6, 1));
		panel1.add(jlblDate);
		panel1.add(jlblTime);
		panel1.add(jlblLocale);
		panel1.add(jlblTimeZone);
		panel1.add(jlblDateStyle);
		panel1.add(jlblTimeStyle);
		
		JPanel panel2 = new JPanel(new GridLayout(6, 1));
		panel2.add(jtfDate);
		panel2.add(jtfTime);
		panel2.add(jcbLocales);
		panel2.add(jcbTimeZones);
		panel2.add(jcbDateStyles);
		panel2.add(jcbTimeStyles);
		
		setLayout(new BorderLayout());
		add(panel1, BorderLayout.WEST);
		add(panel2, BorderLayout.CENTER);
		
		timer.start();
		
		// set user selected locale and change language
		jcbLocales.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (jcbLocales.getSelectedIndex() == 0) {
					updateStrings(english);
					setLocale(english);
				}
				else if (jcbLocales.getSelectedIndex() == 1) {
					updateStrings(chinese);
					setLocale(chinese);
				}
				else if (jcbLocales.getSelectedIndex() == 2) {
					updateStrings(japanese);
					setLocale(japanese);
				}
				else if (jcbLocales.getSelectedIndex() == 3) {
					updateStrings(french);
					setLocale(french);
				}
			}
		});
		
		// set user selected time zone
		jcbTimeZones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (jcbTimeZones.getSelectedIndex() == 0)
					timeZone = TimeZone.getTimeZone(taiwan);
				else if (jcbTimeZones.getSelectedIndex() == 1)
					timeZone = TimeZone.getTimeZone(usa);
				else if (jcbTimeZones.getSelectedIndex() == 2)
					timeZone = TimeZone.getTimeZone(japan);
				else if (jcbTimeZones.getSelectedIndex() == 3)
					timeZone = TimeZone.getTimeZone(france);
			}
		});
		
		// set user selected date style
		jcbDateStyles.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (jcbDateStyles.getSelectedIndex() == 0)
					dateStyle = DateFormat.FULL;
				else if (jcbDateStyles.getSelectedIndex() == 1)
					dateStyle = DateFormat.LONG;
				else if (jcbDateStyles.getSelectedIndex() == 2)
					dateStyle = DateFormat.MEDIUM;
				else if (jcbDateStyles.getSelectedIndex() == 3)
					dateStyle = DateFormat.SHORT;
			}
		});
		
		// set user selected time style
		jcbTimeStyles.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (jcbTimeStyles.getSelectedIndex() == 0)
					timeStyle = DateFormat.FULL;
				else if (jcbTimeStyles.getSelectedIndex() == 1)
					timeStyle = DateFormat.LONG;
				else if (jcbTimeStyles.getSelectedIndex() == 2)
					timeStyle = DateFormat.MEDIUM;
				else if (jcbTimeStyles.getSelectedIndex() == 3)
					timeStyle = DateFormat.SHORT;
			}
		});
	}
	
	private void setAvailableLocales() {
		jcbLocales.addItem(english.getDisplayName());
		jcbLocales.addItem(chinese.getDisplayName());
		jcbLocales.addItem(japanese.getDisplayName());
		jcbLocales.addItem(french.getDisplayName());
	}
	
	private void setAvailableTimeZones() {
		jcbTimeZones.addItem(taiwan);
		jcbTimeZones.addItem(usa);
		jcbTimeZones.addItem(japan);
		jcbTimeZones.addItem(france);
	}
	
	private void setAvailableDateStyles() {
		jcbDateStyles.addItem("Full");
		jcbDateStyles.addItem("Long");
		jcbDateStyles.addItem("Medium");
		jcbDateStyles.addItem("Short");
	}
	
	private void setAvailableTimeStyles() {
		jcbTimeStyles.addItem("Full");
		jcbTimeStyles.addItem("Long");
		jcbTimeStyles.addItem("Medium");
		jcbTimeStyles.addItem("Short");
	}
	
	// change display's language
	private void updateStrings(Locale locale) {
		res = ResourceBundle.getBundle("MyResource", locale);
		jlblDate.setText(res.getString("Date"));
		jlblTime.setText(res.getString("Time"));
		jlblLocale.setText(res.getString("Choose_locale"));
		jlblTimeZone.setText(res.getString("Time_Zone"));
		jlblDateStyle.setText(res.getString("Date_Style"));
		jlblTimeStyle.setText(res.getString("Time_Style"));
	}
	
	private class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			// display date and time on the text field
			Calendar calendar = new GregorianCalendar(timeZone, getLocale());
			DateFormat formatterDate = DateFormat.getDateInstance(dateStyle, getLocale());
			DateFormat formatterTime = DateFormat.getTimeInstance(timeStyle, getLocale());
			formatterDate.setTimeZone(timeZone);
			formatterTime.setTimeZone(timeZone);
			jtfDate.setText(formatterDate.format(calendar.getTime()));
			jtfTime.setText(formatterTime.format(calendar.getTime()));
		}
	}
}
