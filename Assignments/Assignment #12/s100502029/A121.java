package a12.s100502029;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class A121 extends JApplet {
	// create images and icon of five country
	private ImageIcon canada = new ImageIcon(getClass().getResource("image/Canada.gif"));
	private ImageIcon china = new ImageIcon(getClass().getResource("image/China.gif"));
	private ImageIcon england = new ImageIcon(getClass().getResource("image/England.gif"));
	private ImageIcon german = new ImageIcon(getClass().getResource("image/German.gif"));
	private ImageIcon usa = new ImageIcon(getClass().getResource("image/USA.gif"));
	private ImageIcon canadaIcon = new ImageIcon(getClass().getResource("image/Canada_Icon.gif"));
	private ImageIcon chinaIcon = new ImageIcon(getClass().getResource("image/China_Icon.gif"));
	private ImageIcon englandIcon = new ImageIcon(getClass().getResource("image/England_Icon.gif"));
	private ImageIcon germanIcon = new ImageIcon(getClass().getResource("image/German_Icon.gif"));
	private ImageIcon usaIcon = new ImageIcon(getClass().getResource("image/USA_Icon.gif"));
	
	private JLabel jlblPicture = new JLabel(); // create a label to put the image
	
	public A121() {
		// create actions
		Action canadaAction = new MyAction("Canada", canadaIcon);
		Action chinaAction = new MyAction("China", chinaIcon);
		Action englandAction = new MyAction("England", englandIcon);
		Action germanAction = new MyAction("Germany", germanIcon);
		Action usaAction = new MyAction("USA", usaIcon);
		
		JMenuBar jmb = new JMenuBar(); // create menu bar
		setJMenuBar(jmb); // set menu bar to the applet
		
		// add menu "Country" to menu bar
		JMenu countryMenu = new JMenu("Country");
		jmb.add(countryMenu);
		
		// add actions to the menu
		countryMenu.add(canadaAction);
		countryMenu.add(chinaAction);
		countryMenu.add(englandAction);
		countryMenu.add(germanAction);
		countryMenu.add(usaAction);
		
		JToolBar jToolBar1 = new JToolBar(JToolBar.VERTICAL); // create a toolbar
		
		// add actions to the toolbar
		jToolBar1.add(canadaAction);
		jToolBar1.add(chinaAction);
		jToolBar1.add(englandAction);
		jToolBar1.add(germanAction);
		jToolBar1.add(usaAction);
		
		// add toolbar to the east
		add(jToolBar1, BorderLayout.EAST);
		
		jlblPicture.setText(null);
		add(jlblPicture, BorderLayout.CENTER); // add label to the center
	}
	
	private class MyAction extends AbstractAction {
		String country;
		
		MyAction(String country, Icon icon) {
			super(country, icon);
			this.country = country;
		}
		
		// display the picture according to user��s choice
		public void actionPerformed(ActionEvent e) {
			if (country.equals("Canada"))
				jlblPicture.setIcon(canada);
			else if (country.equals("China"))
				jlblPicture.setIcon(china);
			else if (country.equals("England"))
				jlblPicture.setIcon(england);
			else if (country.equals("Germany"))
				jlblPicture.setIcon(german);
			else if (country.equals("USA"))
				jlblPicture.setIcon(usa);
		}
	}
}
