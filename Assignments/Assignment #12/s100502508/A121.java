package a12.s100502508;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JToolBar;


public class A121 extends JApplet
{
	//create imaga icons
	private ImageIcon BigCanadaFlag=new ImageIcon(getClass().getResource("image/Canada.gif"));
	private ImageIcon BigChinaFlag=new ImageIcon(getClass().getResource("image/China.gif"));
	private ImageIcon BigEnglandFlag=new ImageIcon(getClass().getResource("image/England.gif"));
	private ImageIcon BigGermanFlag=new ImageIcon(getClass().getResource("image/German.gif"));
	private ImageIcon BigUSAFlag=new ImageIcon(getClass().getResource("image/USA.gif"));
	private ImageIcon SmallCanadaFlag=new ImageIcon(getClass().getResource("image/Canada_Icon.gif"));
	private ImageIcon SmallChinaFlag=new ImageIcon(getClass().getResource("image/China_Icon.gif"));
	private ImageIcon SmallEnglandFlag=new ImageIcon(getClass().getResource("image/England_Icon.gif"));
	private ImageIcon SmallGermanFlag=new ImageIcon(getClass().getResource("image/German_Icon.gif"));
	private ImageIcon SmallUSAFlag=new ImageIcon(getClass().getResource("image/USA_Icon.gif"));
	
	JPanel panel=new JPanel();
	JLabel label=new JLabel(BigCanadaFlag);
	JMenuBar jmb=new JMenuBar();//create menu
	JMenu menu=new JMenu("Flag");
	JToolBar toolBar=new JToolBar("My toolbar",JToolBar.VERTICAL);//create toolbar
	
	public A121()
	{
		//create actions
		Action canadaAction=new MyAction("Canada", SmallCanadaFlag);
		Action chinaAction=new MyAction("China", SmallChinaFlag);
		Action englandAction=new MyAction("England", SmallEnglandFlag);
		Action germanAction=new MyAction("German", SmallGermanFlag);
		Action usaAction=new MyAction("USA", SmallUSAFlag);
		
		//ass actions to the menu
		menu.add(canadaAction);
		menu.add(chinaAction);
		menu.add(englandAction);
		menu.add(germanAction);
		menu.add(usaAction);
		jmb.add(menu);
		
		//add actions to the toolbar
		toolBar.add(canadaAction);
		toolBar.add(chinaAction);
		toolBar.add(englandAction);
		toolBar.add(germanAction);
		toolBar.add(usaAction);
		
		panel.add(label);
		add(jmb,BorderLayout.NORTH);
		add(panel,BorderLayout.CENTER);
		add(toolBar,BorderLayout.EAST);
	}
	
	private class MyAction extends AbstractAction
	{
		String name;

		public MyAction(String name,Icon icon)//constructor 
		{
			super(name,icon);
			this.name=name;
		}
		
		public void actionPerformed(ActionEvent e)//handle
		{
			if(name.equals("Canada"))
			{
				label.setIcon(BigCanadaFlag);//set icon
			}
			else if(name.equals("China"))
			{
				label.setIcon(BigChinaFlag);//set icon
			}
			else if(name.equals("England"))
			{
				label.setIcon(BigEnglandFlag);//set icon
			}
			else if(name.equals("German"))
			{
				label.setIcon(BigGermanFlag);//set icon
			}
			else if(name.equals("USA"))
			{
				label.setIcon(BigUSAFlag);//set icon
			}
		}
	}
	
	public static void main(String[] args)
	{
		JFrame frame=new JFrame();//create a frame
		A121 applet=new A121();//create an instance of the applet
		frame.add(applet);//add the applet to the frame
		
		frame.setSize(500, 300);//set the frame size
		frame.setLocationRelativeTo(null);//center a frame
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);//display the frame
	}
}

