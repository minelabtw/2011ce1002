package a12.s100502508;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.swing.JApplet;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class A122 extends JApplet
{
	private TimeZone timeZone = TimeZone.getTimeZone("EST");
	private Calendar calender = new GregorianCalendar(timeZone, getLocale());
	private DateFormat formatterdate=DateFormat.getDateInstance(DateFormat.FULL,getLocale());
	private DateFormat formattertime=DateFormat.getTimeInstance(DateFormat.FULL,getLocale());
	private Locale[] availablelocales=Locale.getAvailableLocales();//obtain all available locales
	private String[] availableTimeZones=TimeZone.getAvailableIDs();//obtain all time zone ids
	private ResourceBundle res=ResourceBundle.getBundle("MyResource");//get resourse
	private String[] datestyle={"FULL","LONG","MEDIUM","SHORT"};
	private String[] timestyle={"FULL","LONG","MEDIUM","SHORT"};
	
	JPanel panel1=new JPanel();//create panels
	JPanel panel2=new JPanel();
	JPanel panel3=new JPanel();
	JPanel panel4=new JPanel(new GridLayout(3, 0, 0, 0));
	
	JLabel labeldate=new JLabel(res.getString("Date"));//create labels
	JLabel labeltime=new JLabel(res.getString("Time"));
	JLabel labelchooselocale=new JLabel(res.getString("Choose_locale"));
	JLabel labeltimezone=new JLabel(res.getString("Time_Zone"));
	JLabel labeldatestyle=new JLabel(res.getString("Date_Style"));
	JLabel labeltimestyle=new JLabel(res.getString("Time_Style"));
	
	JTextField datefield=new JTextField(20);//create textfields
	JTextField timefield=new JTextField(20);
	
	JComboBox jcbLocales=new JComboBox();//create comboxes
	JComboBox jcbTimeZones=new JComboBox();
	JComboBox datestyleBox=new JComboBox(datestyle);
	JComboBox timestyleBox=new JComboBox(timestyle);
	
	public A122()
	{
		for(int a=0;a<availablelocales.length;a++)//add locale items to combobox
		{
			jcbLocales.addItem(availablelocales[a].getDisplayName()+" "+availablelocales[a].toString());
		}
		
		Arrays.sort(availableTimeZones);//sort time zones
		for(int a=0;a<availableTimeZones.length;a++)//add timezone items to combobox
		{
			jcbTimeZones.addItem(availableTimeZones[a]);
		}
		
		datefield.setText(formatterdate.format(calender.getTime()));//display current date
		timefield.setText(formattertime.format(calender.getTime()));//display current time
		
		panel1.add(labeldate);
		panel1.add(datefield);
		panel1.add(labeltime);
		panel1.add(timefield);
		
		panel2.add(labelchooselocale);
		panel2.add(jcbLocales);
		panel2.add(labeltimezone);
		panel2.add(jcbTimeZones);
		
		panel3.add(labeldatestyle);
		panel3.add(datestyleBox);
		panel3.add(labeltimestyle);
		panel3.add(timestyleBox);
		
		panel4.add(panel1);
		panel4.add(panel2);
		panel4.add(panel3);
		
		add(panel4);
		
		jcbLocales.addActionListener(new ActionListener()//new locale 
		{
			public void actionPerformed(ActionEvent e)
			{
				setLocale(availablelocales[jcbLocales.getSelectedIndex()]);
				changelocale();//call changelocale method
			}
		});
		
		jcbTimeZones.addActionListener(new ActionListener()//new time zone 
		{
			public void actionPerformed(ActionEvent e) 
			{
				setTimeZone(TimeZone.getTimeZone(availableTimeZones[jcbTimeZones.getSelectedIndex()]));
				changelocale();//call changelocale method
			}
		});
		
		datestyleBox.addActionListener(new ActionListener()//new datestyle 
		{
			public void actionPerformed(ActionEvent e)
			{
				changelocale();//call changelocale method
			}
		});
		
		timestyleBox.addActionListener(new ActionListener()//new timestyle 
		{
			public void actionPerformed(ActionEvent e) 
			{
				changelocale();//call changelocale method
			}
		});
	}
	
	public void setTimeZone(TimeZone timeZone)
	{
		this.timeZone=timeZone;
	}
	
	public void changelocale()//display new locale , timezone , datestyle , timezonestyle  
	{
		res = ResourceBundle.getBundle("MyResource",getLocale());
		labeldate.setText(res.getString("Date"));
		labeltime.setText(res.getString("Time"));
		labelchooselocale.setText(res.getString("Choose_locale"));
		labeltimezone.setText(res.getString("Time_Zone"));
		labeldatestyle.setText(res.getString("Date_Style"));
		labeltimestyle.setText(res.getString("Time_Style"));
		formatterdate=DateFormat.getDateInstance(datestyleBox.getSelectedIndex(),getLocale());
		formattertime=DateFormat.getTimeInstance(timestyleBox.getSelectedIndex(),getLocale());
		formatterdate.setTimeZone(timeZone);
		formattertime.setTimeZone(timeZone);
		datefield.setText(formatterdate.format(calender.getTime()));
		timefield.setText(formattertime.format(calender.getTime()));
	}
	
	public static void main(String[] args)
	{
		JFrame frame=new JFrame();//create a frame
		A122 applet=new A122();//create an instance of the applet
		frame.add(applet);//add the applet to the frame
		
		frame.setSize(800, 250);//set the frame size
		frame.setLocationRelativeTo(null);//center a frame
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);//display the frame
	}
}
