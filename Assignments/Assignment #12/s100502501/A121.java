package a12.s100502501;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class A121 extends JApplet implements ActionListener{
	private JMenuItem canada=new JMenuItem("Canada",new ImageIcon("image/Canada_Icon.gif")); //create menu bar
	private JMenuItem china=new JMenuItem("China",new ImageIcon("image/China_Icon.gif"));
	private JMenuItem usa=new JMenuItem("U.S.A",new ImageIcon("image/USA_Icon.gif"));
	private JMenuItem england=new JMenuItem("England",new ImageIcon("image/England_Icon.gif"));
	private JMenuItem germany=new JMenuItem("Germany",new ImageIcon("image/German_Icon.gif"));
	private JButton jbtcanada=new JButton(new ImageIcon("image/Canada_Icon.gif")); //create button
	private JButton jbtchina=new JButton(new ImageIcon("image/China_Icon.gif"));
	private JButton jbtusa=new JButton(new ImageIcon("image/USA_Icon.gif"));
	private JButton jbtengland=new JButton(new ImageIcon("image/England_Icon.gif"));
	private JButton jbtgermany=new JButton(new ImageIcon("image/German_Icon.gif"));
	private JPanel p1=new JPanel();
	private JLabel l1=new JLabel();
	public A121(){
		JMenuBar jmb=new JMenuBar();
		setJMenuBar(jmb); //set menu bar to applet
				
		JMenu Menu=new JMenu("Flag"); //add menu to menu bar
		Menu.add(canada); //add menu item to menu
		Menu.add(china);
		Menu.add(usa);
		Menu.add(england);
		Menu.add(germany);
		jmb.add(Menu);
		
		JToolBar jtool=new JToolBar(JToolBar.VERTICAL); //create tool bar
		jtool.setFloatable(false);
		jtool.add(jbtcanada); //add button to tool bar 
		jtool.add(jbtchina);
		jtool.add(jbtusa);
		jtool.add(jbtengland);
		jtool.add(jbtgermany);
		add(jtool,BorderLayout.EAST);
		p1.add(l1);
		
		canada.addActionListener(this); //active button event here
		china.addActionListener(this);
		usa.addActionListener(this);
		england.addActionListener(this);
		germany.addActionListener(this);
		jbtcanada.addActionListener(this);
		jbtchina.addActionListener(this);
		jbtusa.addActionListener(this);
		jbtengland.addActionListener(this);
		jbtgermany.addActionListener(this);
		add(p1);
	}
	public void actionPerformed(ActionEvent e) { //change image
		if(e.getSource()==canada||e.getSource()==jbtcanada){
			l1.setIcon(new ImageIcon("image/Canada.gif"));
		}
		if(e.getSource()==china||e.getSource()==jbtchina){
			l1.setIcon(new ImageIcon("image/China.gif"));
		}
		if(e.getSource()==usa||e.getSource()==jbtusa){
			l1.setIcon(new ImageIcon("image/USA.gif"));
		}
		if(e.getSource()==england||e.getSource()==jbtengland){
			l1.setIcon(new ImageIcon("image/England.gif"));
		}
		if(e.getSource()==germany||e.getSource()==jbtgermany){
			l1.setIcon(new ImageIcon("image/German.gif"));
		}
	}
}
