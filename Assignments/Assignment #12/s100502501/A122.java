package a12.s100502501;
import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.util.*;
import javax.swing.*;
import javax.swing.Timer;
public class A122 extends JApplet{
	private Timer timer=new Timer(1000,new TimerListener());
	private String[] loc={"English","�c�餤��","Japanese","French"}; //item of locale
	private String[] timezone={"GMT-4 (U.S.A)","GMT+2 (France)","GMT+8 (Taiwan)","GMT+9 (Japan)"};
	private String[] datestyle={"Full","Long","Medium","Short"}; //item of style
	private String[] timestyle={"Full","Long","Medium","Short"};
	private JComboBox jcblocale=new JComboBox(loc); //create combobox
	private JComboBox jcbtz=new JComboBox(timezone);
	private JComboBox jcbdates=new JComboBox(datestyle);
	private JComboBox jcbts=new JComboBox(timestyle);
	private JTextField jtfdate=new JTextField(20); //create textfield
	private JTextField jtftime=new JTextField(20);
	private ResourceBundle res1=ResourceBundle.getBundle("MyResource_en"); //get resource
	private JPanel p1=new JPanel();
	private JPanel p2=new JPanel();
	private JPanel p3=new JPanel();
	private JLabel ltime=new JLabel(res1.getString("Time"));
	private JLabel ldate=new JLabel(res1.getString("Date"));
	private JLabel llocale=new JLabel(res1.getString("Choose_locale"));
	private JLabel ltz=new JLabel(res1.getString("Time_Zone"));
	private JLabel lds=new JLabel(res1.getString("Date_Style"));
	private JLabel lts=new JLabel(res1.getString("Time_Style"));
	private TimeZone tz1=TimeZone.getTimeZone("GMT-4");
	private Locale locale=new Locale("en", "US"); //create locale
	private Calendar cld=new GregorianCalendar(tz1,getLocale()); //get current time
	private DateFormat formatd=DateFormat.getDateInstance(DateFormat.FULL,locale); //set format
	private DateFormat formatt=DateFormat.getTimeInstance(DateFormat.FULL,locale);
	public A122(){
		setLayout(new GridLayout(3,1));
		p1.add(ldate,BorderLayout.EAST);
		p1.add(jtfdate,BorderLayout.CENTER);
		p1.add(ltime,BorderLayout.EAST);
		p1.add(jtftime,BorderLayout.CENTER);
		p2.add(llocale);
		p2.add(jcblocale);
		p2.add(ltz);
		p2.add(jcbtz);
		p3.add(lds);
		p3.add(jcbdates);
		p3.add(lts);
		p3.add(jcbts);
		add(p1);
		add(p2);
		add(p3);
		timer.start();
		jcblocale.addItemListener(new ItemListener(){ //set locale
		    public void itemStateChanged(ItemEvent e) {
		    	if(jcblocale.getSelectedIndex()==0){
		    		locale=new Locale("en", "US"); //english
		    		formatd=DateFormat.getDateInstance(DateFormat.FULL,locale);
		    		formatt=DateFormat.getTimeInstance(DateFormat.FULL,locale);
		    		res1=ResourceBundle.getBundle("MyResource_en");
		    		ltime.setText(res1.getString("Time"));
		    		ldate.setText(res1.getString("Date"));
		    		llocale.setText(res1.getString("Choose_locale"));
		    		ltz.setText(res1.getString("Time_Zone"));
		    		lds.setText(res1.getString("Date_Style"));
		    		lts.setText(res1.getString("Time_Style"));		    		
		        }
		        if(jcblocale.getSelectedIndex()==1){
		        	locale=new Locale("zh","TW"); //chinese
		        	formatd=DateFormat.getDateInstance(DateFormat.FULL,locale);
		        	formatt=DateFormat.getTimeInstance(DateFormat.FULL,locale);
		        	res1=ResourceBundle.getBundle("MyResource_zh");
		        	ltime.setText(res1.getString("Time"));
		    		ldate.setText(res1.getString("Date"));
		    		llocale.setText(res1.getString("Choose_locale"));
		    		ltz.setText(res1.getString("Time_Zone"));
		    		lds.setText(res1.getString("Date_Style"));
		    		lts.setText(res1.getString("Time_Style"));
		        }
		        if(jcblocale.getSelectedIndex()==2){
		        	locale=new Locale("ja", "JP"); //japanese
		    		formatd=DateFormat.getDateInstance(DateFormat.FULL,locale);
		    		formatt=DateFormat.getTimeInstance(DateFormat.FULL,locale);
		        	res1=ResourceBundle.getBundle("MyResource_ja");
		        	ltime.setText(res1.getString("Time"));
		    		ldate.setText(res1.getString("Date"));
		    		llocale.setText(res1.getString("Choose_locale"));
		    		ltz.setText(res1.getString("Time_Zone"));
		    		lds.setText(res1.getString("Date_Style"));
		    		lts.setText(res1.getString("Time_Style"));		        	
		        }
		        if(jcblocale.getSelectedIndex()==3){
		        	locale=new Locale("fr", "FR"); //french
		    		formatd=DateFormat.getDateInstance(DateFormat.FULL,locale);
		    		formatt=DateFormat.getTimeInstance(DateFormat.FULL,locale);
		        	res1=ResourceBundle.getBundle("MyResource_fr");
		        	ltime.setText(res1.getString("Time"));
		    		ldate.setText(res1.getString("Date"));
		    		llocale.setText(res1.getString("Choose_locale"));
		    		ltz.setText(res1.getString("Time_Zone"));
		    		lds.setText(res1.getString("Date_Style"));
		    		lts.setText(res1.getString("Time_Style"));
		        }
		        repaint();
		    }
		});
		jcbtz.addItemListener(new ItemListener(){ //set timezone
		    public void itemStateChanged(ItemEvent e) {
		    	if(jcbtz.getSelectedIndex()==0){
	    			tz1=TimeZone.getTimeZone("GMT-4");
		    	}
		    	if(jcbtz.getSelectedIndex()==1){
	    			tz1=TimeZone.getTimeZone("GMT+2");
		    	}
		    	if(jcbtz.getSelectedIndex()==2){
	    			tz1=TimeZone.getTimeZone("GMT+8");
		    	}
		    	if(jcbtz.getSelectedIndex()==3){
	    			tz1=TimeZone.getTimeZone("GMT+9");
		    	}
		    	repaint();
		    }
		});
		jcbdates.addItemListener(new ItemListener(){ //set data format
		    public void itemStateChanged(ItemEvent e) {
		    	if(jcbdates.getSelectedIndex()==0){
		    		formatd=DateFormat.getDateInstance(DateFormat.FULL,locale);
		    	}
		    	if(jcbdates.getSelectedIndex()==1){
		    		formatd=DateFormat.getDateInstance(DateFormat.LONG,locale);
		    	}
		    	if(jcbdates.getSelectedIndex()==2){
		    		formatd=DateFormat.getDateInstance(DateFormat.MEDIUM,locale);
		    	}
		    	if(jcbdates.getSelectedIndex()==3){
		    		formatd=DateFormat.getDateInstance(DateFormat.SHORT,locale);
		    	}
		    	repaint();
		    }
		});
		jcbts.addItemListener(new ItemListener(){ //set time format
		    public void itemStateChanged(ItemEvent e) {
		    	if(jcbts.getSelectedIndex()==0){
		    		formatt=DateFormat.getTimeInstance(DateFormat.FULL,locale);
		    	}
		    	if(jcbts.getSelectedIndex()==1){
		    		formatt=DateFormat.getTimeInstance(DateFormat.LONG,locale);
		    	}
		    	if(jcbts.getSelectedIndex()==2){
		    		formatt=DateFormat.getTimeInstance(DateFormat.MEDIUM,locale);
		    	}
		    	if(jcbts.getSelectedIndex()==3){
		    		formatt=DateFormat.getTimeInstance(DateFormat.SHORT,locale);
		    	}
		    	repaint();
		    }
		});
	}
	private class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			cld=new GregorianCalendar(tz1,getLocale());
			formatd.setTimeZone(tz1);
			formatt.setTimeZone(tz1);
			jtfdate.setText(formatd.format(cld.getTime()));
			jtftime.setText(formatt.format(cld.getTime()));
			repaint();
		}
	}
}

