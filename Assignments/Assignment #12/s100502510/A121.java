package a12.s100502510;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class A121 extends JApplet implements ActionListener {
	JMenu menu = new JMenu("Menu");
	JToolBar toolbar = new JToolBar();
	ImageIcon canada = new ImageIcon("image/Canada.gif");
	ImageIcon canadaicon = new ImageIcon("image/Canada_Icon.gif");
	ImageIcon china = new ImageIcon("image/China.gif");
	ImageIcon chinaicon = new ImageIcon("image/China_Icon.gif");
	ImageIcon england = new ImageIcon("image/England.gif");
	ImageIcon englandicon = new ImageIcon("image/England_Icon.gif");
	ImageIcon german = new ImageIcon("image/German.gif");
	ImageIcon germanicon = new ImageIcon("image/German_Icon.gif");
	ImageIcon usa = new ImageIcon("image/USA.gif");
	ImageIcon usaicon = new ImageIcon("image/USA_Icon.gif");
	JMenuItem Canada, China, England, German, USA;
	JLabel picture = new JLabel();
	JButton setcanada = new JButton(canadaicon);
	JButton setchina = new JButton(chinaicon);
	JButton setengland = new JButton(englandicon);
	JButton setgerman = new JButton(germanicon);
	JButton setusa = new JButton(usaicon);

	

	public A121() {
		JMenuBar jmb = new JMenuBar();
		setJMenuBar(jmb);
		menu.setMnemonic('0');
		jmb.add(menu);
		menu.add(Canada = new JMenuItem("Canada", 'C'));
		menu.add(China = new JMenuItem("China", 'C'));
		menu.add(England = new JMenuItem("England", 'E'));
		menu.add(German = new JMenuItem("German", 'G'));
		menu.add(USA = new JMenuItem("USA", 'U'));
		Canada.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				picture.setIcon(canada);
			}
		});
		China.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				picture.setIcon(china);
			}
		});
		England.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				picture.setIcon(england);
			}
		});
		German.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				picture.setIcon(german);
			}
		});
		USA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				picture.setIcon(usa);
			}
		});
		toolbar.setLayout(new GridLayout(5, 1));
		toolbar.add(setcanada);
		toolbar.add(setchina);
		toolbar.add(setengland);
		toolbar.add(setgerman);
		toolbar.add(setusa);
		setcanada.addActionListener(this);
		setchina.addActionListener(this);
		setengland.addActionListener(this);
		setgerman.addActionListener(this);
		setusa.addActionListener(this);
		setLayout(new BorderLayout());
		add(picture, BorderLayout.CENTER);
		add(toolbar, BorderLayout.EAST);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == setcanada) {
			picture.setIcon(canada);
		} else if (e.getSource() == setchina) {
			picture.setIcon(china);
		} else if (e.getSource() == setengland) {
			picture.setIcon(england);
		} else if (e.getSource() == setgerman) {
			picture.setIcon(german);
		} else if (e.getSource() == setusa) {
			picture.setIcon(usa);
		}

	}

}
