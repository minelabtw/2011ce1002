package a12.s100502030;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.*;
import javax.swing.border.Border;

public class A121 extends JApplet {

	private int showflag = 0;
	private CardLayout cardLayout = new CardLayout(20, 10);
	private JPanel flagPanel = new JPanel(cardLayout);
	private JLabel flagLabel = new JLabel();

	public A121() {
		
		// add imageviewer panel
		flagPanel.add(new ImageViewer(new ImageIcon(getClass().getResource(
				"image/Canada.gif")).getImage()), "1");
		flagPanel.add(
				new ImageViewer(new ImageIcon(getClass().getResource(
						"image/China.gif")).getImage()), "2");
		flagPanel.add(
				new ImageViewer(new ImageIcon(getClass().getResource(
						"image/England.gif")).getImage()), "3");
		flagPanel.add(
				new ImageViewer(new ImageIcon(getClass().getResource(
						"image/German.gif")).getImage()), "4");
		flagPanel.add(
				new ImageViewer(new ImageIcon(getClass().getResource(
						"image/USA.gif")).getImage()), "5");
		
		// set action
		Action canadaAction = new MyAction("Canada", new ImageIcon(getClass()
				.getResource("image/Canada_Icon.gif")), "Show Canada flag",
				new Integer(KeyEvent.VK_C), KeyStroke.getKeyStroke(
						KeyEvent.VK_C, ActionEvent.CTRL_MASK));
		Action chinaAction = new MyAction("China", new ImageIcon(getClass()
				.getResource("image/China_Icon.gif")), "Show China flag",
				new Integer(KeyEvent.VK_H), KeyStroke.getKeyStroke(
						KeyEvent.VK_H, ActionEvent.CTRL_MASK));
		Action englandAction = new MyAction("England", new ImageIcon(getClass()
				.getResource("image/England_Icon.gif")), "Show England flag",
				new Integer(KeyEvent.VK_E), KeyStroke.getKeyStroke(
						KeyEvent.VK_E, ActionEvent.CTRL_MASK));
		Action germanAction = new MyAction("German", new ImageIcon(getClass()
				.getResource("image/German_Icon.gif")), "Show German flag",
				new Integer(KeyEvent.VK_G), KeyStroke.getKeyStroke(
						KeyEvent.VK_G, ActionEvent.CTRL_MASK));
		Action USAAction = new MyAction("USA", new ImageIcon(getClass()
				.getResource("image/USA_Icon.gif")), "Show USA flag",
				new Integer(KeyEvent.VK_U), KeyStroke.getKeyStroke(
						KeyEvent.VK_U, ActionEvent.CTRL_MASK));

		JMenuBar jmb = new JMenuBar();
		setJMenuBar(jmb);
		
		// set menu
		JMenu countryJMenu = new JMenu("Country");
		countryJMenu.setMnemonic('C');
		jmb.add(countryJMenu);
		
		countryJMenu.add(canadaAction);
		countryJMenu.add(chinaAction);
		countryJMenu.add(englandAction);
		countryJMenu.add(germanAction);
		countryJMenu.add(USAAction);
		
		//set toolbar
		JToolBar jtb = new JToolBar();
		jtb.setBorder(BorderFactory.createLineBorder(Color.BLUE));

		jtb.add(canadaAction);
		jtb.add(chinaAction);
		jtb.add(englandAction);
		jtb.add(germanAction);
		jtb.add(USAAction);

		add(jtb, BorderLayout.NORTH);
		add(flagPanel, BorderLayout.CENTER);
	}

	private class MyAction extends AbstractAction {
		String name;

		MyAction(String name, Icon icon) {
			super(name, icon);
			this.name = name;
		}

		MyAction(String name, Icon icon, String desc, Integer mnemonic,
				KeyStroke accelerator) {
			super(name, icon);
			putValue(Action.SHORT_DESCRIPTION, desc);
			putValue(Action.MNEMONIC_KEY, mnemonic);
			putValue(Action.ACCELERATOR_KEY, accelerator);
			this.name = name;
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			// show flag
			if (name.equals("Canada"))
				cardLayout.show(flagPanel, "1");
			else if (name.equals("China"))
				cardLayout.show(flagPanel, "2");
			else if (name.equals("England"))
				cardLayout.show(flagPanel, "3");
			else if (name.equals("German"))
				cardLayout.show(flagPanel, "4");
			else if (name.equals("USA"))
				cardLayout.show(flagPanel, "5");
		}
	}
}
