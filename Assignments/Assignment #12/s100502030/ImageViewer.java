package a12.s100502030;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.JPanel;
// image viewer panel
public class ImageViewer extends JPanel {

	private Image image;

	private boolean stretched = true;

	private int xCoordinate;

	private int yCoordinate;

	public ImageViewer() {

	}

	public ImageViewer(Image image) {
		this.image = image;
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		// draw image
		if (image != null) {
			if (isStretched()) {
				g.drawImage(image, xCoordinate, yCoordinate, getWidth(),
						getHeight(), this);
			}
		} else
			g.drawImage(image, xCoordinate, yCoordinate, this);
	}

	public boolean isStretched() {
		return stretched;
	}
	
	public void setStretched(boolean stretched){
		this.stretched = stretched;
	}// set stretched
	
	public int getXCoordinate(){
		return xCoordinate;
	}
	
	public void setXCoodrinate(int xCoordinate){
		this.xCoordinate = xCoordinate;
		repaint();
	}//set xcoordinate
	
	public int getYCoordinate(){
		return yCoordinate;
	}
	
	public void setYCoodrinate(int yCoordinate){
		this.yCoordinate = yCoordinate;
		repaint();
	}//set ycoordinate
}
