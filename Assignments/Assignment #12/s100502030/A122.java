package a12.s100502030;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.swing.JApplet;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.border.Border;

public class A122 extends JApplet {
	private Locale locale = Locale.getDefault();
	private Locale[] locales = { Locale.ENGLISH, Locale.FRENCH, Locale.JAPAN,
			Locale.CHINESE };

	private String[] availableTimeZones = TimeZone.getAvailableIDs();
	private String[] styles = { "Full", "Long", "Medium", "Short" };
	private JComboBox jcbLocales = new JComboBox();
	private JComboBox jcbTimeZones = new JComboBox();
	private JComboBox jcbDateStyles = new JComboBox();
	private JComboBox jcbTimeStyles = new JComboBox();
	private ResourceBundle res = ResourceBundle.getBundle("MyResource");

	private JLabel jlbDate = new JLabel(res.getString("Date"));
	private JLabel jlbTime = new JLabel(res.getString("Time"));
	private JLabel jlbChooseALocale = new JLabel(res.getString("Choose_locale"));
	private JLabel jlbTimeZone = new JLabel(res.getString("Time_Zone"));
	private JLabel jlbDateStyle = new JLabel(res.getString("Date_Style"));
	private JLabel jlbTimeStytle = new JLabel(res.getString("Time_Style"));

	private JTextField jtfDate = new JTextField();
	private JTextField jtfTime = new JTextField();

	private TimeZone timeZone = TimeZone.getTimeZone("CST");
	private TimeZone[] timeZones;

	private Timer timer = new Timer(1000, new TimerListener());
	private DateFormat format_date;
	private DateFormat format_time;
	private int dateStyle = DateFormat.FULL;
	private int timeStyle = DateFormat.FULL;

	public A122() {
		
		setLocale(locale);
		ResourceBundle.getBundle("MyResource");
		timer.start();
		
		//set comboBox
		setjcbLocales();
		setAvailableTimeZones();
		setJcbDateStyles();
		setJcbTimeStyles();
		
		JPanel p1 = new JPanel(new BorderLayout(5, 5));
		p1.add(jlbDate, BorderLayout.WEST);
		p1.add(jtfDate, BorderLayout.CENTER);

		JPanel p2 = new JPanel(new BorderLayout(5, 5));
		p2.add(jlbTime, BorderLayout.WEST);
		p2.add(jtfTime, BorderLayout.CENTER);

		JPanel p3 = new JPanel(new GridLayout(1, 2));
		p3.add(p1);
		p3.add(p2);

		JPanel p4 = new JPanel(new BorderLayout(5, 5));
		p4.add(jlbChooseALocale, BorderLayout.WEST);
		p4.add(jcbLocales, BorderLayout.CENTER);

		JPanel p5 = new JPanel(new BorderLayout(5, 5));
		p5.add(jlbTimeZone, BorderLayout.WEST);
		p5.add(jcbTimeZones, BorderLayout.CENTER);

		JPanel p6 = new JPanel(new GridLayout(1, 2));
		p6.add(p4);
		p6.add(p5);

		JPanel p7 = new JPanel(new BorderLayout(5, 5));
		p7.add(jlbDateStyle, BorderLayout.WEST);
		p7.add(jcbDateStyles, BorderLayout.CENTER);

		JPanel p8 = new JPanel(new BorderLayout(5, 5));
		p8.add(jlbTimeStytle, BorderLayout.WEST);
		p8.add(jcbTimeStyles, BorderLayout.CENTER);

		JPanel p9 = new JPanel(new GridLayout(1, 2));
		p9.add(p7);
		p9.add(p8);

		jtfDate.setEditable(false);
		jtfTime.setEditable(false);//set editable false

		jcbLocales.addActionListener(new ActionListener() {
			// choose local
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				locale = locales[jcbLocales.getSelectedIndex()];
				setLocale(locale);
				updateString();
				System.out.println(locales[jcbLocales.getSelectedIndex()].getDisplayName());
				
			}
		});

		jcbTimeZones.addActionListener(new ActionListener() {
			// choose time Zone
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				timeZone = timeZones[jcbTimeZones.getSelectedIndex()];
				
			}
		});

		jcbDateStyles.addActionListener(new ActionListener() {

			// choose date style
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (jcbDateStyles.getSelectedIndex() == 0)
					dateStyle = DateFormat.FULL;
				else if (jcbDateStyles.getSelectedIndex() == 1)
					dateStyle = DateFormat.LONG;
				else if (jcbDateStyles.getSelectedIndex() == 2)
					dateStyle = DateFormat.MEDIUM;
				else if (jcbDateStyles.getSelectedIndex() == 3)
					dateStyle = DateFormat.SHORT;
			}
		});

		jcbTimeStyles.addActionListener(new ActionListener() {
			// choose time style
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (jcbTimeStyles.getSelectedIndex() == 0)
					timeStyle = DateFormat.FULL;
				else if (jcbTimeStyles.getSelectedIndex() == 1)
					timeStyle = DateFormat.LONG;
				else if (jcbTimeStyles.getSelectedIndex() == 2)
					timeStyle = DateFormat.MEDIUM;
				else if (jcbTimeStyles.getSelectedIndex() == 3)
					timeStyle = DateFormat.SHORT;
			}
		});

		setLayout(new GridLayout(3, 1));
		add(p3);
		add(p6);
		add(p9);
	}
	
	//set LocalcomboBox
	public void setjcbLocales() {
		for (int i = 0; i < locales.length; i++) {
			jcbLocales.addItem(locales[i].getDisplayName());
		}
	}
	
	//set TimeZonecomboBox
	public void setAvailableTimeZones() {
		Arrays.sort(availableTimeZones);
		timeZones = new TimeZone[availableTimeZones.length];
		for (int i = 0; i < availableTimeZones.length; i++) {
			jcbTimeZones.addItem(availableTimeZones[i]);
			timeZones[i] = TimeZone.getTimeZone(availableTimeZones[i]);
		}
	}
	
	//set DateStylecomboBox
	public void setJcbDateStyles() {
		for (int i = 0; i < styles.length; i++) {
			jcbDateStyles.addItem(styles[i]);
		}
	}
	
	//set TimeStylecomboBox
	public void setJcbTimeStyles() {
		for (int i = 0; i < styles.length; i++) {
			jcbTimeStyles.addItem(styles[i]);
		}
	}

	public void updateString() {
		res = ResourceBundle.getBundle("MyResource", locale);
		jlbDate.setText(res.getString("Date"));
		jlbTime.setText(res.getString("Time"));
		jlbChooseALocale.setText(res.getString("Choose_locale"));
		jlbTimeZone.setText(res.getString("Time_Zone"));
		jlbDateStyle.setText(res.getString("Date_Style"));
		jlbTimeStytle.setText(res.getString("Time_Style"));
		
		// reoaint label
		repaint();
	}

	private class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			Calendar calendar = new GregorianCalendar(timeZone, getLocale());
			
			//Display digit time on textfield
			format_date = DateFormat.getDateInstance(dateStyle, getLocale());
			format_time = DateFormat.getTimeInstance(timeStyle, getLocale());
			format_date.setTimeZone(timeZone);
			format_time.setTimeZone(timeZone);

			jtfDate.setText(format_date.format(calendar.getTime()));
			jtfTime.setText(format_time.format(calendar.getTime()));
			
		}
	}

}