package a12.s975002502;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class A121 extends JApplet {
	// Create image icons(small)
	private ImageIcon canadaIcon = new ImageIcon(getClass().getResource("/image/Canada_Icon.gif"));
	private ImageIcon chinaIcon = new ImageIcon(getClass().getResource("/image/China_Icon.gif"));
	private ImageIcon englandIcon = new ImageIcon(getClass().getResource("/image/England_Icon.gif"));
	private ImageIcon germanIcon = new ImageIcon(getClass().getResource("/image/German_Icon.gif"));
	private ImageIcon usaIcon = new ImageIcon(getClass().getResource("/image/USA_Icon.gif"));
	
	// Create image icons(large)
	private ImageIcon canadaFlagIcon = new ImageIcon(getClass().getResource("/image/Canada.gif"));
	private ImageIcon chinaFlagIcon = new ImageIcon(getClass().getResource("/image/China.gif"));
	private ImageIcon englandFlagIcon = new ImageIcon(getClass().getResource("/image/England.gif"));
	private ImageIcon germanFlagIcon = new ImageIcon(getClass().getResource("/image/German.gif"));
	private ImageIcon usaFlagIcon = new ImageIcon(getClass().getResource("/image/USA.gif"));
	
	private JLabel jlblImage = new JLabel(canadaFlagIcon, JLabel.CENTER);
	private FlowLayout flowLayout = new FlowLayout();

	public A121() {
		// Create actions
		Action caAction = new MyAction("Canada", canadaIcon);
		Action chAction = new MyAction("China", chinaIcon);
		Action enAction = new MyAction("England", englandIcon);
		Action geAction = new MyAction("German", germanIcon);
		Action usAction = new MyAction("USA", usaIcon);

		// Create menus
		JMenuBar jMenuBar = new JMenuBar();
		JMenu jmenuFlag = new JMenu("Flags");
		setJMenuBar(jMenuBar);
    	jMenuBar.add(jmenuFlag);

    	// Add actions to the menu
    	jmenuFlag.add(caAction);
    	jmenuFlag.add(chAction);
    	jmenuFlag.add(enAction);
    	jmenuFlag.add(geAction);
    	jmenuFlag.add(usAction);

    	// Add actions to the toolbar
    	JToolBar jToolBar = new JToolBar(JToolBar.VERTICAL);
    	jToolBar.setBorder(BorderFactory.createLineBorder(Color.black));
    	jToolBar.add(caAction);
    	jToolBar.add(chAction);
    	jToolBar.add(enAction);
    	jToolBar.add(geAction);
    	jToolBar.add(usAction);

    	// Add tool bar to the east and panel to the center
    	getContentPane().add(jToolBar, BorderLayout.EAST);
    	getContentPane().add(jlblImage, BorderLayout.CENTER);
  	}

	private class MyAction extends AbstractAction {
		String name;

		MyAction(String name, Icon icon) {
			super(name, icon);
			this.name = name;
    	}

    	public void actionPerformed(ActionEvent e) {
    		if (name.equals("Canada"))
    			jlblImage.setIcon(canadaFlagIcon);
    		else if (name.equals("China"))
    			jlblImage.setIcon(chinaFlagIcon);
    		else if (name.equals("England"))
    			jlblImage.setIcon(englandFlagIcon);
    		else if (name.equals("German"))
    			jlblImage.setIcon(germanFlagIcon);
    		else if (name.equals("USA"))
    			jlblImage.setIcon(usaFlagIcon);
    	}
	}

  	public static void main(String[] args) {
  		A121 applet = new A121();
  		JFrame frame = new JFrame();
    	frame.setTitle("A121");
    	frame.getContentPane().add(applet, BorderLayout.CENTER);
    	applet.init();
    	applet.start();
    	frame.setSize(500,700);
    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	frame.setLocationRelativeTo(null); // Center the frame
    	frame.setVisible(true);
  	}
}
