/** This class inherits a subclass from interface Action. */
package a12.s100502514;

//import java.awt.event.*;

import javax.swing.*;

abstract class A121SelectAction extends AbstractAction {

	/** UID */
	private static final long serialVersionUID = 1L;
	
	//Icon names, URLs and so on
	static String[] labelNames = {
		"Canada",
		"China",
		"England",
		"German",
		"USA"
	};
	static String[] imageURLs = {
		"image/Canada.gif",
		"image/China.gif",
		"image/England.gif",
		"image/German.gif",
		"image/USA.gif"
	};
	static String[] iconURLs = {
		"image/Canada_Icon.gif",
		"image/China_Icon.gif",
		"image/England_Icon.gif",
		"image/German_Icon.gif",
		"image/USA_Icon.gif"
	};
	/*
	static ImageIcon[] icons = new ImageIcon[iconURLs.length];
	private void initImageIconObjects(){
		for(int i=0; i<iconURLs.length; i++){
			icons[i] = new ImageIcon(getClass().getResource(iconURLs[i]));
		}
	}
	*/
	
	ImageIcon action_icon;
	ImageIcon showing_image;
	
	//Constructors of the Action class
	public A121SelectAction(String name, ImageIcon icon, ImageIcon show_img){
		super(name, icon);
		action_icon = icon;
		showing_image = show_img;
	}
	public A121SelectAction(String name, ImageIcon icon){
		this(name, null, null);
	}
	public A121SelectAction(String name){
		this(name, null);
	}
	
	
	
	//Action events trigger this method [abstract].
	/*
	@Override
	public void actionPerformed(ActionEvent e) {
		
	}
	*/

}
