/**
 * CE1002-100502514 FlashTeens Chiang
 * [The Following Rules are from ce1002 website]
 * Assignment 12-1
 * 
 * Please write an Applet to meet the following requirements:

	class A121: inherits Applet
	Menu items:
	Toolbar items: use icon picture
	ImageViewer: display the picture according to user��s choice
	Use the Action interface to centralize the processing for the actions.
	https://dl.dropbox.com/u/21602009/image.zip
	
	For more informations, please see ce1002 website.
	
 */
package a12.s100502514;

import java.awt.*;
import java.awt.event.ActionEvent;

import javax.swing.*;
import javax.swing.border.TitledBorder;

public class A121 extends JApplet {

	/** UID */
	private static final long serialVersionUID = 1L;
	
	/** GUI Members */
	JMenuBar myMenubar;
	JMenu myMenu;
	JToolBar myToolbar;
	ImageViewer imgPanel;
	
	/** GUI Action Members */
	Action[] actions = new A121SelectAction[A121SelectAction.iconURLs.length];
	
	/** Empty Constructor */
	public A121(){
		/* initialize on init() instead of here. */
	}
	
	/** Initialization */
	@Override
	public void init(){
		//Layout initialization
		setLayout(new BorderLayout());
		
		//Add image viewer panel.
		add(imgPanel = new ImageViewer(null));
		
		//MenuBar Settings
		setJMenuBar(myMenubar = new JMenuBar());
		myMenu = new JMenu("Countries");
		for(int i=0; i<A121SelectAction.labelNames.length;i++){
			myMenu.add(new A121SelectAction(A121SelectAction.labelNames[i],
				new ImageIcon(getClass().getResource(A121SelectAction.iconURLs[i])),
				new ImageIcon(getClass().getResource(A121SelectAction.imageURLs[i]))){

					/** UID */
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent e) {
						imgPanel.setIcon(showing_image);
						repaint();
					}
				
			});
		}
		myMenubar.add(myMenu);
		
		//ToolBar Settings (under the image viewer)
		myToolbar = new JToolBar(JToolBar.HORIZONTAL);
		myToolbar.setBorder(new TitledBorder("Country Options"));
		for(int i=0; i<A121SelectAction.labelNames.length;i++){
			myToolbar.add(new A121SelectAction(A121SelectAction.labelNames[i],
				new ImageIcon(getClass().getResource(A121SelectAction.iconURLs[i])),
				new ImageIcon(getClass().getResource(A121SelectAction.imageURLs[i]))){

					/** UID */
					private static final long serialVersionUID = 1L;

					@Override
					public void actionPerformed(ActionEvent e) {
						imgPanel.setIcon(showing_image);
						repaint();
					}
				
			});
		}
		add(myToolbar, BorderLayout.SOUTH);
	}
	
}