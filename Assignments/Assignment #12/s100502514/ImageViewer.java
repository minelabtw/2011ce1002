package a12.s100502514;

import java.awt.*;

import javax.swing.*;

public class ImageViewer extends JPanel {

	/** UID */
	private static final long serialVersionUID = 1L;
	
	//Icon getter and setter (image files)
	private ImageIcon icon;
	public ImageIcon getIcon() {
		return icon;
	}
	public void setIcon(ImageIcon icon) {
		this.icon = icon;
	}
	
	//Constructor for an ImageViewer panel
	public ImageViewer(ImageIcon icon){
		setIcon(icon);
	}
	
	//Overriden painting method
	@Override
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		try{
			//Get the image reference
			Image img = icon.getImage();
			
			//Find the scaling ratio to centralize the image in an appropiate size.
			//Writing "1.0*..." is to calculate as double.
			double widthRatio = 1.0*getWidth()/img.getWidth(this);
			double heightRatio = 1.0*getHeight()/img.getHeight(this);
			double imageScaleRatio = Math.min(widthRatio, heightRatio);
			
			//Draw the image
			int w = (int)(img.getWidth(this)*imageScaleRatio);
			int h = (int)(img.getHeight(this)*imageScaleRatio);
			int x = (int)((getWidth()-w)/2);
			int y = (int)((getHeight()-h)/2);
			g.drawImage(img, x, y, w, h, this);
		}catch(NullPointerException err){
			//The image-not-found message
			Font msg_font = new Font("Arial Black", Font.PLAIN, getHeight()/8);
			String msg = "Image NOT found!!";
			g.setColor(Color.red);
			g.setFont(msg_font);
			g.drawString(msg,
					(getWidth()-g.getFontMetrics(msg_font).stringWidth(msg))/2,
					getHeight()/2);
		}
	}
	
}
