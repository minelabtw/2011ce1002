/**
 * CE1002-100502514 FlashTeens Chiang
 * [The Following Rules are from ce1002 website]
 * Assignment 12-1
 * 
 * Please write an Applet to meet the following requirements:
	
	class A122 : inherits Applet
	Use Text-filed display the current time and date as show in the subsequent figure.
	Let user select a locale, time zone, date style and time style from the combo boxes.
	In addition, there are four language files in the following link which are
		English, France, Japan, Chinese.
	You have to use this files to change the text in Jlabel.
	https://dl.dropbox.com/u/34779778/Language.zip
	
	For more informations, please see ce1002 website.
	
 */
package a12.s100502514;

import java.awt.*;
import java.awt.event.*;
import java.text.DateFormat;
import java.util.*;

import javax.swing.*;

public class A122 extends JApplet {

	/** UID */
	private static final long serialVersionUID = 1L;
	
	//Notice:	TA Language resource files (MyResource[_xx].properties)
	//			are already copied to the path in testing environment:
	//			bin/MyResource[_xx].properties
	//TODO There are some unsolvable bug described below!! (But not causing any major problems)
	//BUG 1:	If there is a not-found language then it selects to Chinese automatically.
	//BUG 2:	The default value of TimeZone displaying on the TextField is Trad. Chinese,
	//			while not-found situation will be Simp. Chinese form.
	
	/** ResourceBundle and Property Variable Definitions */
	ResourceBundle myResource = ResourceBundle.getBundle("MyResource");
	String myTimeZoneID = TimeZone.getDefault().getID();
	final String Choose_locale="Choose_locale";
	final String Time="Time";
	final String Date="Date";
	final String Time_Zone="Time_Zone";
	final String Date_Style="Date_Style";
	final String Time_Style="Time_Style";
	
	
	/** GUI Members */
	//Labels
	A122LanguageLabel jlbDateTime = new A122LanguageLabel(myResource, new String[]{Date, Time});
	A122LanguageLabel jlbChoose_locale = new A122LanguageLabel(myResource, Choose_locale);
	A122LanguageLabel jlbTime_Zone = new A122LanguageLabel(myResource, Time_Zone);
	A122LanguageLabel jlbDate_Style = new A122LanguageLabel(myResource, Date_Style);
	A122LanguageLabel jlbTime_Style = new A122LanguageLabel(myResource, Time_Style);
	//Text fields of time & date
	A122DateTimeTextField txtDateTime = new A122DateTimeTextField();
	//ComboBoxes
	LanguageComboBox lang_combo = new LanguageComboBox();
	TimeZoneComboBox zone_combo = new TimeZoneComboBox();
	StyleComboBox date_combo = new StyleComboBox(StyleTargetOption.DATE);
	StyleComboBox time_combo = new StyleComboBox(StyleTargetOption.TIME);
	
	//Collection of the objects to change properties
	A122_AbleToResetResource[] label_collection = {
			jlbDateTime, jlbChoose_locale, jlbTime_Zone, jlbDate_Style, jlbTime_Style,
			txtDateTime};
	
	/** Empty Constructor */
	public A122(){
		/* initialize on init() instead of here. */
	}
	
	/** Initialization */
	@Override
	public void init(){
		//Layout Setting
		setLayout(new GridLayout(5, 2));
		
		//Add Components
		add(jlbDateTime);
		add(txtDateTime);
		
		add(jlbChoose_locale);
		add(lang_combo);
		
		add(jlbTime_Zone);
		add(zone_combo);
		
		add(jlbDate_Style);
		add(date_combo);

		add(jlbTime_Style);
		add(time_combo);
	}
	
	/** Inner Language ComboBox class */
	class LanguageComboBox extends JComboBox<String> {
	
		/** UID */
		private static final long serialVersionUID = 1L;
		
		private Locale[] available = Locale.getAvailableLocales();
		
		public LanguageComboBox(){
			addItem(Locale.getDefault().getDisplayName()+" (Default)");
			for(int i=0; i<available.length; i++){
				addItem(available[i].getDisplayName(available[i]));
			}
			addItemListener(new ItemListener(){
				@Override
				public void itemStateChanged(ItemEvent e) {
					if(getSelectedIndex()==0){
						myResource = ResourceBundle.getBundle("MyResource");
					}else{
						myResource = ResourceBundle.getBundle("MyResource",
								available[getSelectedIndex()-1]);
					}
					for(int i=0; i<label_collection.length; i++){
						label_collection[i].resetResource(myResource);
						((JComponent)label_collection[i]).repaint();
					}
					
				}
			});
		}
		
	}
	
	/** Inner Time Zone ComboBox class */
	class TimeZoneComboBox extends JComboBox<String> {
	
		/** UID */
		private static final long serialVersionUID = 1L;
		
		private String[] available = TimeZone.getAvailableIDs();
		
		public TimeZoneComboBox(){
			addItem(TimeZone.getDefault().getID()+" (Default)");
			for(int i=0; i<available.length; i++){
				addItem(available[i]);
			}
			addItemListener(new ItemListener(){
				@Override
				public void itemStateChanged(ItemEvent e) {
					if(getSelectedIndex()==0/*default value*/){
						myTimeZoneID = TimeZone.getDefault().getID();
					}else{
						myTimeZoneID = available[getSelectedIndex()-1];
					}
					txtDateTime.setTimeZone(
								TimeZone.getTimeZone(myTimeZoneID));
					//txtDateTime.repaint(); //Already auto-repainted
				}
			});
		}
		
	}
	
	/** Inner Style ComboBox class */
	class StyleComboBox extends JComboBox<String> {
	
		/** UID */
		private static final long serialVersionUID = 1L;
		
		//Option labels
		private String[] styles = {"Full", "Long", "Medium", "Short"};
		
		//Style Target Option: Make the class able to be used in both Date and Time styles.
		//The enum class is defined below this class definition.
		private StyleTargetOption targetOption = StyleTargetOption.DATE;
		public StyleTargetOption getTargetOption(){
			return targetOption;
		}
		
		public StyleComboBox(final StyleTargetOption targetOption){
			this.targetOption = targetOption;
			for(int i=0; i<styles.length; i++){
				addItem(styles[i]);
			}
			addItemListener(new ItemListener(){
				@Override
				public void itemStateChanged(ItemEvent e) {
					int value_to_set = DateFormat.FULL;
					switch(getSelectedIndex()){
					case 0://full
						value_to_set = DateFormat.FULL;
						break;
					case 1://long
						value_to_set = DateFormat.LONG;
						break;
					case 2://medium
						value_to_set = DateFormat.MEDIUM;
						break;
					case 3://short
						value_to_set = DateFormat.SHORT;
						break;
					}
					
					switch(targetOption){
					case DATE:
						txtDateTime.setDateStyle(value_to_set);
						break;
					case TIME:
						txtDateTime.setTimeStyle(value_to_set);
						break;
					}
					//txtDateTime.repaint(); //Already auto-repainted
				}
			});
		}
	}
	
	/** Inner Enum for Style ComboBox class */
	enum StyleTargetOption{DATE, TIME};
}

