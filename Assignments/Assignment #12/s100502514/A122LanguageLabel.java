/** class A122LanguageLabel: Makes the text more flexible for different languages. */
package a12.s100502514;

import java.awt.*;
import java.util.*;

import javax.swing.*;

public class A122LanguageLabel extends JLabel implements A122_AbleToResetResource {

	/** UID */
	private static final long serialVersionUID = 1L;
	
	/** Variable Name Property */
	private String[] varnames;
	
	public String separator = new String(" / "); //Public visibility for convenience
	
	public void setVarName(ResourceBundle res, String[] varnames){
		//System.out.println(res.getLocale().getDisplayName());
		this.varnames = varnames;
		String val = "";
		if(varnames!=null){
			try{
				for(int i=0; i<varnames.length; i++){
					if(i>0 && separator!=null){
						val += separator;
					}
					val += res.getString(varnames[i]);
				}
				setToolTipText(null);
				setForeground(Color.black);
			}catch(MissingResourceException err){
				val = "#N/A";
				setToolTipText("Language Resource Not Found!!");
				setForeground(Color.red);
			}
		}
		setText(val);
	}
	
	/** Resource Reference Property */
	private ResourceBundle res;
	@Override
	public void resetResource(ResourceBundle res){
		this.res = res;
	}
	
	/** Constructor with ResourceBundle reference and the String of variable name */
	public A122LanguageLabel(ResourceBundle res, String[] varnames){
		setVarName(this.res = res, varnames);
	}
	public A122LanguageLabel(ResourceBundle res, String varname){
		this(res, new String[]{varname});
	}
	
	
	/** Painting method to Refresh the label while repainting */
	@Override
	protected void paintComponent(Graphics g){
		setVarName(res, varnames);
		super.paintComponent(g);
	}
}
