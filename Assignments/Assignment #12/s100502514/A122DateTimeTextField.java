/** class A122LanguageTextField: Makes the text more flexible for different languages. */
package a12.s100502514;

import java.awt.*;
import java.text.*;
import java.util.*;

import javax.swing.*;

public class A122DateTimeTextField extends JTextField implements A122_AbleToResetResource {

	/** UID */
	private static final long serialVersionUID = 1L;
	
	// Style&Locale Properties and getters/setters
	private int dateStyle, timeStyle;
	private TimeZone timezone;
	private Locale language;
	public int getDateStyle() {
		return dateStyle;
	}
	public void setDateStyle(int dateStyle) {
		this.dateStyle = dateStyle;
	}
	public int getTimeStyle() {
		return timeStyle;
	}
	public void setTimeStyle(int timeStyle) {
		this.timeStyle = timeStyle;
	}
	public TimeZone getTimeZone() {
		return timezone;
	}
	public void setTimeZone(TimeZone timezone) {
		if(timezone!=null){
			this.timezone = timezone;
		}else{
			//If value is null then set to system default value.
			this.timezone = TimeZone.getDefault();
		}
	}
	public Locale getLanguage() {
		return language;
	}
	public void setLanguage(Locale language) {
		if(language!=null){
			this.language = language;
		}else{
			//If value is null then set to system default value.
			this.language = Locale.getDefault();
		}
	}
	
	// Specified Constructors
	public A122DateTimeTextField(int dateStyle, int timeStyle, TimeZone timezone, Locale language){
		setDateStyle(dateStyle);
		setTimeStyle(timeStyle);
		setTimeZone(timezone);
		setLanguage(language);
		
		setEditable(false); //The text field cannot be changed by user input.
	}
	public A122DateTimeTextField(int dateStyle, int timeStyle, TimeZone timezone){
		this(dateStyle, timeStyle, timezone, null);
	}
	// Default Constructor
	public A122DateTimeTextField(){
		this(DateFormat.FULL, DateFormat.FULL, null);
	}
	
	/** Painting method to Refresh the label while repainting */
	@Override
	protected void paintComponent(Graphics g){
		//Change the text content for each time painting.
		DateFormat fmtDate = DateFormat.getDateInstance(dateStyle, language);
		fmtDate.setTimeZone(timezone);
		
		DateFormat fmtTime = DateFormat.getTimeInstance(timeStyle, language);
		fmtTime.setTimeZone(timezone);
		
		
		Date time = Calendar.getInstance(/*timezone*/).getTime();
		setText(fmtDate.format(time)+"\n"+fmtTime.format(time));
		super.paintComponent(g);
	}
	
	//Implementation from interface A122_AbleToResetResource.
	@Override
	public void resetResource(ResourceBundle res) {
		try{
			setLanguage(res.getLocale());
		}catch(NullPointerException err){
			setLanguage(null);//default value on error
		}
	}
	
}
