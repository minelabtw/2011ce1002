/** The interface for convenient call in A122 main class as polymorphism. */
package a12.s100502514;

import java.util.ResourceBundle;

public interface A122_AbleToResetResource {
	/** Reset language resource only. */
	public void resetResource(ResourceBundle res);
}
