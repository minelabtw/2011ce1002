package a12.s100502506;
import java.applet.Applet;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.swing.JApplet;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;
public class A122 extends JApplet
{
	private ResourceBundle myres = ResourceBundle.getBundle("MyResource");//load MyResource
	private JLabel disLable[]=new JLabel[6];
	private JTextField disText[]=new JTextField[2];
	private JComboBox[] selectComboBox=new JComboBox[4];
	private Locale locales[] =Calendar.getAvailableLocales();
	private Locale locale;
	private String Date_Style[]={"Full","Medium","Short"};
	private String Time_Style[]={"Full","Medium","Short"};;
	private TimeZone timeZone;
	private String[] timeZones =TimeZone.getAvailableIDs();
	private Timer timer = new Timer(100, new TimerListener());
	private DateFormat formatter1;
	private DateFormat formatter2;
	private JPanel[] panels=new JPanel[6];
	private Calendar calendar;
	public A122()//init
	{
		for(int i=0;i<6;i++)
		{
			disLable[i]=new JLabel();
			panels[i]=new JPanel();
		}
		for(int i=0;i<2;i++)
			disText[i]=new JTextField(10);
		for(int i=0;i<4;i++)
			selectComboBox[i]=new JComboBox();
			
		for (int i = 0; i < locales.length; i++)
			selectComboBox[0].addItem(locales[i].getDisplayName());
		for(int i=0;i<timeZones.length;i++)
			selectComboBox[1].addItem(timeZones[i]);
		for(int i=0;i<3;i++)
		{
			selectComboBox[2].addItem(Date_Style[i]);
			selectComboBox[3].addItem(Time_Style[i]);
		}
		//set Time Date
		locale = locales[selectComboBox[0].getSelectedIndex()];
		timeZone =TimeZone.getTimeZone(timeZones[selectComboBox[1].getSelectedIndex()]);
		calendar = new GregorianCalendar(timeZone,locale);
		formatter1 = DateFormat.getDateInstance(DateFormat.LONG, locale);
		formatter1.setTimeZone(timeZone);
		formatter2 = DateFormat.getTimeInstance(DateFormat.LONG, locale);
		formatter2.setTimeZone(timeZone);
		//set Text
		disText[0].setText(formatter1.format(calendar.getTime()));
		disText[1].setText(formatter2.format(calendar.getTime()));
		disLable[0].setText(myres.getString("Date"));
		disLable[1].setText(myres.getString("Time"));
		disLable[2].setText(myres.getString("Choose_locale"));
		disLable[3].setText(myres.getString("Time_Zone"));
		disLable[4].setText(myres.getString("Date_Style"));
		disLable[5].setText(myres.getString("Time_Style"));
		panels[0].add(disLable[0]);
		panels[0].add(disText[0]);
		panels[1].add(disLable[1]);
		panels[1].add(disText[1]);
		panels[2].add(disLable[2]);
		panels[2].add(selectComboBox[0]);
		panels[3].add(disLable[3]);
		panels[3].add(selectComboBox[1]);
		panels[4].add(disLable[4]);
		panels[4].add(selectComboBox[2]);
		panels[5].add(disLable[5]);
		panels[5].add(selectComboBox[3]);
		//Action listener
		selectComboBox[0].addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				 locale = locales[selectComboBox[0].getSelectedIndex()];
				 updateStrings();
			}
		});
		selectComboBox[1].addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				timeZone =TimeZone.getTimeZone(timeZones[selectComboBox[1].getSelectedIndex()]);
				updateStrings();
			}
		});
		selectComboBox[2].addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				updateDatestytle();
			}
		});
		selectComboBox[3].addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				updateTimestytle();
			}
		});
		//set layout
		for(int i=0;i<6;i++)
			panels[i].setLayout(new GridLayout(1,2,5,5));
		setLayout(new GridLayout(3,2,5,5));
		for(int i=0;i<6;i++)
			add(panels[i]);
		updateStrings() ;
		timer.start();
	}
	/** Update resource strings */
	private void updateStrings() // update String according to locale
	{
		myres = ResourceBundle.getBundle("MyResource", locale);
		disLable[0].setText(myres.getString("Date"));
		disLable[1].setText(myres.getString("Time"));
		disLable[2].setText(myres.getString("Choose_locale"));
		disLable[3].setText(myres.getString("Time_Zone"));
		disLable[4].setText(myres.getString("Date_Style"));
		disLable[5].setText(myres.getString("Time_Style"));
		// Make sure the new labels are displayed
		updateTimestytle();
		updateDatestytle();
		repaint();
	}
	public void updateTimestytle()// update String according to Time stytle
	{
		calendar = new GregorianCalendar(timeZone,locale);
		if(selectComboBox[2].getSelectedIndex()==0)
		{
			formatter1 = DateFormat.getDateInstance(DateFormat.LONG, locale);
		}
		else if(selectComboBox[2].getSelectedIndex()==1)
		{
			formatter1 = DateFormat.getDateInstance(DateFormat.MEDIUM, locale);
		}
		else if(selectComboBox[2].getSelectedIndex()==2)
		{
			formatter1 = DateFormat.getDateInstance(DateFormat.SHORT, locale);
		}
		formatter1.setTimeZone(timeZone);
		disText[0].setText(formatter1.format(calendar.getTime()));
	}
	public void updateDatestytle()// update String according to Date stytle
	{
		calendar = new GregorianCalendar(timeZone,locale);
		if(selectComboBox[3].getSelectedIndex()==0)
		{
			formatter2 = DateFormat.getTimeInstance(DateFormat.FULL, locale);
		}
		else if(selectComboBox[3].getSelectedIndex()==1)
		{
			formatter2 = DateFormat.getTimeInstance(DateFormat.MEDIUM, locale);	
		}
		else if(selectComboBox[3].getSelectedIndex()==2)
		{
			formatter2 = DateFormat.getTimeInstance(DateFormat.SHORT, locale);
		}
		formatter2.setTimeZone(timeZone);
		disText[1].setText(formatter2.format(calendar.getTime()));
		
	}
	public static void main(String argc[])
	{
		JFrame testFrame =new JFrame();
		A122 applet=new A122();
		testFrame.add(applet);
		testFrame.setTitle("A122");
		testFrame.setVisible(true);
		testFrame.setSize(800,600);
		testFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	class TimerListener implements ActionListener// Time listener to update
	{
		public void actionPerformed(ActionEvent e) 
		{
			updateTimestytle();
			updateDatestytle();
		}
		
	}
}
