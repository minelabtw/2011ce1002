package a12.s100502506;
import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JToolBar;

public class A121 extends JApplet
{
	private ImageIcon[] images=new ImageIcon[10];
	
	private JMenuBar muBar = new JMenuBar();
	private JMenu menu1 = new JMenu("Country");
	private JMenuItem usami = new JMenuItem();
	private JMenuItem gmmi = new JMenuItem();
	private JMenuItem ukmi = new JMenuItem();
	private JMenuItem cnmi = new JMenuItem();
	private JMenuItem cndmi = new JMenuItem();
	private JToolBar toolBar = new JToolBar(JToolBar.VERTICAL);
	private JButton usaButton = new JButton();
	private JButton gmButton = new JButton();
	private JButton ukButton = new JButton();
	private JButton cnButton = new JButton();
	private JButton cndButton = new JButton();
	private JLabel countryLabel =new JLabel();
	public A121()// initialize
	{
		for(int i=0;i<10;i++)
			images[i]=new ImageIcon();
		images[0] =new ImageIcon(getClass().getResource("image/USA_Icon.gif"));
		images[1] =new ImageIcon(getClass().getResource("image/German_Icon.gif"));
		images[2] =new ImageIcon(getClass().getResource("image/England_Icon.gif"));
		images[3] =new ImageIcon(getClass().getResource("image/China_Icon.gif"));
		images[4] =new ImageIcon(getClass().getResource("image/Canada_Icon.gif"));
		images[5] =new ImageIcon(getClass().getResource("image/USA.gif"));
		images[6] =new ImageIcon(getClass().getResource("image/German.gif"));
		images[7] =new ImageIcon(getClass().getResource("image/England.gif"));
		images[8] =new ImageIcon(getClass().getResource("image/China.gif"));
		images[9] =new ImageIcon(getClass().getResource("image/Canada.gif"));
		usami.setIcon(images[0]);
		gmmi.setIcon(images[1]);
		ukmi.setIcon(images[2]);
		cnmi.setIcon(images[3]);
		cndmi.setIcon(images[4]);
		usaButton.setIcon(images[0]);
		gmButton.setIcon(images[1]);
		ukButton.setIcon(images[2]);
		cnButton.setIcon(images[3]);
		cndButton.setIcon(images[4]);
		
		countryLabel.setIcon(images[5]);
		//add to menu bar
		muBar.add(menu1);
		//add to menu
		menu1.add(usami);
		menu1.add(gmmi);
		menu1.add(ukmi);
		menu1.add(cnmi);
		menu1.add(cndmi);
		//add to tool bar
		toolBar.add(usaButton);
		toolBar.add(gmButton);
		toolBar.add(ukButton);
		toolBar.add(cnButton);
		toolBar.add(cndButton);
		//add action listener
		usami.addActionListener(new action());
		gmmi.addActionListener(new action());
		ukmi.addActionListener(new action());
		cnmi.addActionListener(new action());
		cndmi.addActionListener(new action());
		usaButton.addActionListener(new action());
		gmButton.addActionListener(new action());
		ukButton.addActionListener(new action());
		cnButton.addActionListener(new action());
		cndButton.addActionListener(new action());
		//add to applet
		add(muBar,BorderLayout.NORTH);
		add(countryLabel,BorderLayout.CENTER);
		add(toolBar,BorderLayout.EAST);
	}
	class action implements ActionListener //Action listener about button and menu item
	{
		public void actionPerformed(ActionEvent e) 
		{
			if(e.getSource()==usami||e.getSource()==usaButton)
			{
				countryLabel.setIcon(images[5]);
			}
			if(e.getSource()==gmmi||e.getSource()==gmButton)
			{
				countryLabel.setIcon(images[6]);
			}
			if(e.getSource()==ukmi||e.getSource()==ukButton)
			{
				countryLabel.setIcon(images[7]);
			}
			if(e.getSource()==cnmi||e.getSource()==cnButton)
			{
				countryLabel.setIcon(images[8]);
			}
			if(e.getSource()==cndmi||e.getSource()==cndButton)
			{
				countryLabel.setIcon(images[9]);
			}
		}
	}
	public static void main(String argc[])// main
	{
		JFrame testFrame =new JFrame();
		A121 applet=new A121();
		testFrame.add(applet);
		testFrame.setTitle("A122");
		testFrame.setVisible(true);
		testFrame.setSize(800,600);
		testFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
}
