package a12.s100502023;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

import javax.swing.*;

public class A121 extends JApplet
{
	//menu
	private JMenuBar jmb = new JMenuBar();
	private JMenu jmu_choose = new JMenu("choose");
	private JMenuItem menu_Canada;
	private JMenuItem menu_China;
	private JMenuItem menu_England;
	private JMenuItem menu_German;
	private JMenuItem menu_USA;

	//button & toolbar
	private JToolBar jtool_bar = new JToolBar(JToolBar.VERTICAL);
	private JButton jbt_Canada; 
	private JButton jbt_China; 
	private JButton jbt_England; 
	private JButton jbt_German; 
	private JButton jbt_USA; 
	//imageIcon
	
	private ImageIcon Icon_Canada = new ImageIcon(getClass().getResource("image/Canada_Icon.gif"));
	private ImageIcon Icon_China = new ImageIcon(getClass().getResource("image/China_Icon.gif"));
	private ImageIcon Icon_England = new ImageIcon(getClass().getResource("image/England_Icon.gif"));
	private ImageIcon Icon_German = new ImageIcon(getClass().getResource("image/German_Icon.gif"));
	private ImageIcon Icon_USA = new ImageIcon(getClass().getResource("image/USA_Icon.gif"));
	//Action
	private Action Canada_Action = new PressAction("Canada",Icon_Canada,"Canada");
	private Action China_Action = new PressAction("China",Icon_China,"China");
	private Action England_Action = new PressAction("England",Icon_England,"England");
	private Action German_Action = new PressAction("German",Icon_German,"German");
	private Action USA_Action = new PressAction("USA",Icon_USA,"USA");
	//JLabel : show picture
	private JLabel jl_showPicture = new JLabel();
	public void init()
	{
		set_menu();
		set_tool();
		add_menu_JMenuBar();
		add_tool_JToolBar();
		add(jl_showPicture,BorderLayout.CENTER);
		
		
	}

	public static void main(String[] args) 
	{
		//none
	}
	
	public void set_menu()//set menu and add Action
	{
		menu_Canada= new JMenuItem(Canada_Action);
		menu_China= new JMenuItem(China_Action);
		menu_England= new JMenuItem(England_Action);
		menu_German= new JMenuItem(German_Action);
		menu_USA= new JMenuItem(USA_Action);
		
	}
	public void set_tool()//set tool and add Action
	{
		jbt_Canada = new JButton(Canada_Action); 
		jbt_China = new JButton(China_Action); 
		jbt_England = new JButton(England_Action); 
		jbt_German = new JButton(German_Action);  
		jbt_USA = new JButton(USA_Action);
		//(����ܦr)
		jbt_Canada.setText(null);
		jbt_China.setText(null);
		jbt_England.setText(null);
		jbt_German.setText(null);
		jbt_USA.setText(null);
		
	}
	public void add_menu_JMenuBar() //add menu
	{
		jmb.add(jmu_choose);
		jmu_choose.add(menu_Canada);
		jmu_choose.add(menu_China);
		jmu_choose.add(menu_England);
		jmu_choose.add(menu_German);
		jmu_choose.add(menu_USA);
		add(jmb,BorderLayout.NORTH);
	}
	
	public void add_tool_JToolBar() //add tool
	{
		jtool_bar.add(jbt_Canada);
		jtool_bar.add(jbt_China);
		jtool_bar.add(jbt_England);
		jtool_bar.add(jbt_German);
		jtool_bar.add(jbt_USA);
		add(jtool_bar,BorderLayout.EAST);
	}
	//Action
	private class PressAction extends AbstractAction
	{
		String access_country;
		//initialize
		PressAction(String country,Icon icon)
		{
			super(country,icon);
			access_country=country;
		}
		
		PressAction(String country,Icon icon,String desc)
		{
			super(country,icon);
			putValue(Action.SHORT_DESCRIPTION,desc);
			access_country=country;
		}
		//decide picture
		public void actionPerformed(ActionEvent e) 
		{
			if (access_country.equals("Canada"))
			{
				jl_showPicture.setIcon(new ImageIcon(getClass().getResource("image/Canada.gif")));
			}
			else if (access_country.equals("China"))
			{
				jl_showPicture.setIcon(new ImageIcon(getClass().getResource("image/China.gif")));
			}
			else if (access_country.equals("England"))
			{
				jl_showPicture.setIcon(new ImageIcon(getClass().getResource("image/England.gif")));
			}
			else if (access_country.equals("German"))
			{
				jl_showPicture.setIcon(new ImageIcon(getClass().getResource("image/German.gif")));
			}
			else if (access_country.equals("USA"))
			{
				jl_showPicture.setIcon(new ImageIcon(getClass().getResource("image/USA.gif")));
			}
			else
			{
				//none
			}
		}
		
	}
}
