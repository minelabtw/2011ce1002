package a12.s100502023;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.swing.*;
import javax.swing.text.Style;

public class A122 extends JApplet
{
	//resource 
	private ResourceBundle res = ResourceBundle.getBundle("MyResource",Locale.CHINESE);//預設語系:中文
	//jlabel
	private JLabel jl_Date = new JLabel(res.getString("Date"));
	private JLabel jl_Time = new JLabel(res.getString("Time"));
	private JLabel jl_Choose_locale = new JLabel(res.getString("Choose_locale"));
	private JLabel jl_Time_Zone = new JLabel(res.getString("Time_Zone"));
	private JLabel jl_Date_Style = new JLabel(res.getString("Date_Style"));
	private JLabel jl_Time_Style = new JLabel(res.getString("Time_Style"));
	//jtextField : show  date and time
	private JTextField jtf_Date = new JTextField();
	private JTextField jtf_Time = new JTextField();
	//JComboBox
	private JComboBox jcboLocale = new JComboBox();
	private JComboBox jcboTimeZone = new JComboBox();
	private JComboBox jcboDateStyle = new JComboBox();
	private JComboBox jcboTimeStyle = new JComboBox();
	
	private Locale locale = Locale.getDefault();
	
	private Locale [] availableLocales = new Locale[4];
	private String [] availableTimeZones = TimeZone.getAvailableIDs();
	private String [] availableStyle = new String[4];
	
	private int style_Date = DateFormat.FULL;//init:FULL;(FULL,LONG,MEDIUM,SHORT)
	private int style_Time = DateFormat.FULL;//init:FULL;(FULL,LONG,MEDIUM,SHORT)
	
	private JPanel p1 = new JPanel();//add date and time
	private JPanel p2 = new JPanel();//add choose locale and time zone
	private JPanel p3 = new JPanel();//add date style and time style
	
	//formatter
	private DateFormat formatter;
	//timezone
	private TimeZone timeZone;
	//timer and calendar
	private Timer timer = new Timer(1000,new TimerListener());
	private Calendar calendar;
	
	
	public void init()
	{
		timer.start();
		//set panel
		setPanel_p1();
		setPanel_p2();
		setPanel_p3();
		
		//set item
		setAvailableLocales();
		setAvailableStyle();
		//set comboBox
		initialize_Locale_ComboBox();
		initialize_TimeZone_ComboBox();
		initialize_DateStyle_ComboBox();
		initialize_TimeStyle_ComboBox();
		
		//Japplet
		setLayout(new GridLayout(3,1,10,10));
		add(p1);
		add(p2);
		add(p3);
		
		//ActionListener
		jcboLocale.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				locale=availableLocales[jcboLocale.getSelectedIndex()];
				update_Locale_String();
			}

		});
		
		jcboTimeZone.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{	
				timeZone=TimeZone.getTimeZone(availableTimeZones[jcboTimeZone.getSelectedIndex()]);
				formatter.setTimeZone(timeZone);	
			}
			
		});
		
		jcboDateStyle.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (jcboDateStyle.getSelectedIndex()==0)//full
				{
					style_Date=DateFormat.FULL;
				}
				else if (jcboDateStyle.getSelectedIndex()==1)//long
				{
					style_Date=DateFormat.LONG;
				}
				else if (jcboDateStyle.getSelectedIndex()==2)//MEDIUM
				{
					style_Date=DateFormat.MEDIUM;
				}
				else if (jcboDateStyle.getSelectedIndex()==3)//SHORT
				{
					style_Date=DateFormat.SHORT;
				}
			}
			
		});

		jcboTimeStyle.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				if (jcboTimeStyle.getSelectedIndex()==0)//full
				{
					style_Time=DateFormat.FULL;
				}
				else if (jcboTimeStyle.getSelectedIndex()==1)//long
				{
					style_Time=DateFormat.LONG;
				}
				else if (jcboTimeStyle.getSelectedIndex()==2)//MEDIUM
				{
					style_Time=DateFormat.MEDIUM;
				}
				else if (jcboTimeStyle.getSelectedIndex()==3)//SHORT
				{
					style_Time=DateFormat.SHORT;
				}
			}
			
		});
		
		
	}

	public static void main(String[] args) 
	{
		//none

	}
	

	public void setPanel_p1()
	{
		p1.setLayout(new GridLayout(1,4,10,10));
		p1.add(jl_Date);
		p1.add(jtf_Date);
		p1.add(jl_Time);
		p1.add(jtf_Time);
	}
	public void setPanel_p2()
	{
		p2.setLayout(new GridLayout(1,4,10,10));
		p2.add(jl_Choose_locale);
		p2.add(jcboLocale);
		p2.add(jl_Time_Zone);
		p2.add(jcboTimeZone);
	}
	public void setPanel_p3()
	{
		p3.setLayout(new GridLayout(1,4,10,10));
		p3.add(jl_Date_Style);
		p3.add(jcboDateStyle);
		p3.add(jl_Time_Style);
		p3.add(jcboTimeStyle);
	}
	public void setAvailableLocales()//item:Locales
	{
		availableLocales[0]=Locale.CHINESE;
		availableLocales[1]=Locale.ENGLISH;
		availableLocales[2]=Locale.FRANCE;
		availableLocales[3]=Locale.JAPAN;
	}
	
	public void setAvailableStyle()//item:style
	{
		availableStyle[0]="FULL";
		availableStyle[1]="LONG";
		availableStyle[2]="MEDIUM";
		availableStyle[3]="SHORT";	
	}

	
	public void initialize_Locale_ComboBox()
	{
		for (int i=0;i<4;i++)
		{
			jcboLocale.addItem(availableLocales[i].getDisplayName());
		}
	}
	
	
	private void initialize_TimeZone_ComboBox() 
	{
		for (int i=0;i<availableTimeZones.length;i++)
		{
			jcboTimeZone.addItem(availableTimeZones[i]);
		}
		
	}
	

	private void initialize_TimeStyle_ComboBox()
	{
		for (int i=0;i<availableStyle.length;i++)
		{
			jcboTimeStyle.addItem(availableStyle[i]);
		}
		
	}


	private void initialize_DateStyle_ComboBox() 
	{
		for (int i=0;i<availableStyle.length;i++)
		{
			jcboDateStyle.addItem(availableStyle[i]);
		}
	}
	
	private void update_Locale_String() 
	{
		res=ResourceBundle.getBundle("MyResource",locale);
		jl_Date.setText(res.getString("Date"));
		jl_Time.setText(res.getString("Time"));
		jl_Choose_locale.setText(res.getString("Choose_locale"));
		jl_Time_Zone.setText(res.getString("Time_Zone"));
		jl_Date_Style.setText(res.getString("Date_Style"));
		jl_Time_Style.setText(res.getString("Time_Style"));
	}
	
	private class TimerListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e)
		{
			//set Time
			timeZone=TimeZone.getTimeZone(availableTimeZones[jcboTimeZone.getSelectedIndex()]);
			calendar = new GregorianCalendar(timeZone,locale);
			
			formatter = DateFormat.getTimeInstance(style_Time, locale);
			
			formatter.setTimeZone(timeZone);
			jtf_Time.setText(formatter.format(calendar.getTime()));
			
			//set date
			formatter = DateFormat.getDateInstance(style_Date, locale);
			jtf_Date.setText(formatter.format(calendar.getTime()));
			
		}
		
	}

	
}
