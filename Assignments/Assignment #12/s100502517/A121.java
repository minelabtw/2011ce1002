package a12.s100502517;

import java.applet.Applet;
import javax.swing.*;

import java.awt.*;

public class A121 extends JApplet{
	
	private ImageIcon Canada = new ImageIcon("image/Canada.jpg");
	private JButton Canada_Icon = new JButton(new ImageIcon(getClass().getResource("image/Canada_Icon.jpg")));
	private ImageIcon China = new ImageIcon("image/China.jpg");
	private JButton China_Icon = new JButton(new ImageIcon(getClass().getResource("image/China_Icon.jpg")));
	private ImageIcon England = new ImageIcon("image/England_Icon.jpg");
	private JButton England_Icon = new JButton(new ImageIcon(getClass().getResource("image/England_Icon.jpg")));	
	private ImageIcon German = new ImageIcon("image/German_Icon.jpg");
	private JButton German_Icon = new JButton(new ImageIcon(getClass().getResource("image/German_Icon.jpg")));
	private ImageIcon USA = new ImageIcon("image/USA.jpg");
	private JButton USA_Icon = new JButton(new ImageIcon(getClass().getResource("image/USA_Icon.jpg")));
	
	private JMenuItem canada,china,england,german,usa,exit;
	
	public A121(){
		JMenuBar jmb = new JMenuBar();
		setJMenuBar(jmb);
		
		JMenu operationMenu = new JMenu("Country");
		operationMenu.setMnemonic('c');
		jmb.add(operationMenu);
		
		JMenu exitMenu = new JMenu("Exit");
		exitMenu.setMnemonic('E');
		jmb.add(exitMenu);
		
		operationMenu.add(canada = new JMenuItem("Canada"));
		operationMenu.add(china = new JMenuItem("China"));
		operationMenu.add(england = new JMenuItem("England"));
		operationMenu.add(german = new JMenuItem("German"));
		operationMenu.add(usa = new JMenuItem("USA"));
		exitMenu.add(exit = new JMenuItem("Exit"));
		
		JPanel picture = new JPanel(){
			protected void paintComponent(Graphics g){
				g.drawImage(Canada.getImage(), 0, 0, getWidth(), getHeight(), this);
				super.paintComponent(g);
			}
		};
		//使原本背景透明化
		picture.setBackground(new Color(0, 0, 0, 0));
		
		JToolBar jtb = new JToolBar("Countries");
		jtb.setFloatable(true);
		jtb.add(Canada_Icon);
		jtb.add(China_Icon);
		jtb.add(England_Icon);
		jtb.add(German_Icon);
		jtb.add(USA_Icon);
		
		setLayout(new BorderLayout());
		add(picture, BorderLayout.CENTER);
		add(jtb, BorderLayout.SOUTH);		
		
	}
	
	public void init(){
		
	}

}
