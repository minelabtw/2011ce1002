package a12.s100502027;

import java.awt.BorderLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JToolBar;

public class A121 extends JApplet {
	private JLabel ShowPic = new JLabel();
	private ImageIcon IICanada = new ImageIcon(getClass().getResource("image/Canada.gif"));
	private ImageIcon IIChina = new ImageIcon(getClass().getResource("image/China.gif"));
	private ImageIcon IIEngland = new ImageIcon(getClass().getResource("image/England.gif"));
	private ImageIcon IIGerman = new ImageIcon(getClass().getResource("image/German.gif"));
	private ImageIcon IIUSA = new ImageIcon(getClass().getResource("image/USA.gif"));
	public A121(){
		//catch the iconpic
		ImageIcon IIcCanada = new ImageIcon(getClass().getResource("image/Canada_Icon.gif"));
		ImageIcon IIcChina = new ImageIcon(getClass().getResource("image/China_Icon.gif"));
		ImageIcon IIcEngland = new ImageIcon(getClass().getResource("image/England_Icon.gif"));
		ImageIcon IIcGerman = new ImageIcon(getClass().getResource("image/German_Icon.gif"));
		ImageIcon IIcUSA = new ImageIcon(getClass().getResource("image/USA_Icon.gif"));
		//set the action's icon and name 
		Action CanadaAction = new MyAction("Canada",IIcCanada);
		Action ChinaAction = new MyAction("China",IIcChina);
		Action EnglandAction = new MyAction("England",IIcEngland);
		Action GermanAction = new MyAction("German",IIcGerman);
		Action USAAction = new MyAction("USA",IIcUSA);
		//set Menu
		JMenuBar JMBPic = new JMenuBar();
		setJMenuBar(JMBPic);
		JMenu JMPic = new JMenu("Picture");
		JMBPic.add(JMPic);
		//set the action to the menu
		JMPic.add(CanadaAction);
		JMPic.add(ChinaAction);
		JMPic.add(EnglandAction);
		JMPic.add(GermanAction);
		JMPic.add(USAAction);
		//set the action to the toolbar
		JToolBar JTBPic = new JToolBar(JToolBar.VERTICAL);
		JTBPic.add(CanadaAction);
		JTBPic.add(ChinaAction);
		JTBPic.add(EnglandAction);
		JTBPic.add(GermanAction);
		JTBPic.add(USAAction);
		//layout the label which show pic  and  toolbar
		setLayout(new BorderLayout());
		add(JTBPic,BorderLayout.EAST);
		add(ShowPic,BorderLayout.CENTER);
	}
	//set the Action  to use
	private class MyAction extends AbstractAction{
		String name;
		MyAction(String name,Icon icon){
			super(name,icon);
			this.name=name;
		}
		//use Action's name to choose pic to set in the label
		public void actionPerformed(ActionEvent e) {
			if(name.equals("Canada")){
				IICanada.setImage(IICanada.getImage().getScaledInstance(ShowPic.getWidth(),ShowPic.getHeight() , Image.SCALE_DEFAULT));
				ShowPic.setIcon(IICanada);
			}
			else if(name.equals("China")){
				IIChina.setImage(IIChina.getImage().getScaledInstance(ShowPic.getWidth(),ShowPic.getHeight() , Image.SCALE_DEFAULT));
				ShowPic.setIcon(IIChina);
			}
			else if(name.equals("England")){
				IIEngland.setImage(IIEngland.getImage().getScaledInstance(ShowPic.getWidth(),ShowPic.getHeight() , Image.SCALE_DEFAULT));
				ShowPic.setIcon(IIEngland);
			}
			else if(name.equals("German")){
				IIGerman.setImage(IIGerman.getImage().getScaledInstance(ShowPic.getWidth(),ShowPic.getHeight() , Image.SCALE_DEFAULT));
				ShowPic.setIcon(IIGerman);
			}
			else if(name.equals("USA")){
				IIUSA.setImage(IIUSA.getImage().getScaledInstance(ShowPic.getWidth(),ShowPic.getHeight() , Image.SCALE_DEFAULT));
				ShowPic.setIcon(IIUSA);
			}
		}
	}
}
