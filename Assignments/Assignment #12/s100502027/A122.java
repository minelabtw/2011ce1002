package a12.s100502027;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.swing.JApplet;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;

public class A122 extends JApplet{
	private TimeZone timeZone = TimeZone.getTimeZone("EST");
	private Timer timer = new Timer(1000,new TimerListener());
	private int dateStyle = DateFormat.FULL;
	private int timeStyle = DateFormat.FULL;
	private Locale aLocale =Locale.ENGLISH;
	private int[] StyleArray = {DateFormat.FULL,DateFormat.LONG,DateFormat.MEDIUM,DateFormat.SHORT};
	private String[] LanguageArray = {"英文","法文","日文","中文"};
	private Locale[] LocalesArray = {Locale.ENGLISH,Locale.FRENCH,Locale.JAPANESE,Locale.CHINESE};
	private String[] availableTimeZones = TimeZone.getAvailableIDs();
	private String[] DateFormatsArray = {"FULL","LONG","MEDIUM","SHORT"};
	private JComboBox jcbLocales = new JComboBox(LanguageArray);
	private JComboBox jcbTimeZones = new JComboBox();
	private JComboBox jcbDateStyle = new JComboBox(DateFormatsArray);
	private JComboBox jcbTimeStyle = new JComboBox(DateFormatsArray);
	private ResourceBundle res = ResourceBundle.getBundle("MyResource");
	private JLabel jlDate = new JLabel(res.getString("Date")) ;
	private JLabel jlTime = new JLabel(res.getString("Time")) ;
	private JLabel jlLocale = new JLabel(res.getString("Choose_locale")) ;
	private JLabel jlTimeZone = new JLabel(res.getString("Time_Zone")) ;
	private JLabel jlDateStyle = new JLabel(res.getString("Date_Style")) ;
	private JLabel jlTimeStyle = new JLabel(res.getString("Time_Style")) ;
	private JTextField jtfDate = new JTextField(10);
	private JTextField jtfTime = new JTextField(10);
	public A122(){
		setLayout(new GridLayout(3,1));
		setAvailabTimeZones();
		
		JPanel jpDate = new JPanel();
		jpDate.setLayout(new BorderLayout());
		jpDate.add(jlDate,BorderLayout.WEST);
		jpDate.add(jtfDate,BorderLayout.CENTER);
		
		JPanel jpTime = new JPanel();
		jpTime.setLayout(new BorderLayout());
		jpTime.add(jlTime,BorderLayout.WEST);
		jpTime.add(jtfTime,BorderLayout.CENTER);
		
		JPanel jpDateTime = new JPanel();
		jpDateTime.setLayout(new GridLayout(1,2));
		jpDateTime.add(jpDate);
		jpDateTime.add(jpTime);

		JPanel jpLocale = new JPanel();
		jpLocale.setLayout(new BorderLayout());
		jpLocale.add(jlLocale,BorderLayout.WEST);
		jpLocale.add(jcbLocales,BorderLayout.CENTER);
		
		JPanel jpTimeZone = new JPanel();
		jpTimeZone.setLayout(new BorderLayout());
		jpTimeZone.add(jlTimeZone,BorderLayout.WEST);
		jpTimeZone.add(jcbTimeZones,BorderLayout.CENTER);
		
		JPanel jplocalzone = new JPanel();
		jplocalzone.setLayout(new GridLayout(1,2));
		jplocalzone.add(jpLocale);
		jplocalzone.add(jpTimeZone);
		
		JPanel jpstyle = new JPanel();
		jpstyle.setLayout(new GridLayout(1,6));
		jpstyle.add(new JLabel(" "));
		jpstyle.add(jlDateStyle);
		jpstyle.add(jcbDateStyle);
		jpstyle.add(jlTimeStyle);
		jpstyle.add(jcbTimeStyle);
		jpstyle.add(new JLabel(" "));
		//set the combobox's actionlistener
		jcbLocales.addActionListener(new ActionListener(){
			//when choose a local , become the local and use it to resettime
			public void actionPerformed(ActionEvent e) {
				aLocale = LocalesArray[jcbLocales.getSelectedIndex()];
				updateStrings();
				coputeLoan();
			}
		});
		jcbTimeZones.addActionListener(new ActionListener(){
			//when choose a timezone , become the timezone and use it to resettime
			public void actionPerformed(ActionEvent e) {
				timeZone = TimeZone.getTimeZone(availableTimeZones[jcbTimeZones.getSelectedIndex()]);
				coputeLoan();
			}
		});
		jcbDateStyle.addActionListener(new ActionListener(){
			//when choose a datestyle , become the datestyle and use it to resettime
			public void actionPerformed(ActionEvent e) {
				dateStyle = StyleArray[jcbDateStyle.getSelectedIndex()];
				coputeLoan();
			}
		});
		jcbTimeStyle.addActionListener(new ActionListener(){
			//when choose a timestyle , become the timestyle and use it to resettime
			public void actionPerformed(ActionEvent e) {
				timeStyle = StyleArray[jcbTimeStyle.getSelectedIndex()];
				coputeLoan();
			}
		});
		
		coputeLoan();
		add(jpDateTime);
		add(jplocalzone);
		add(jpstyle);
		timer.start();
	}
	//when user become the local , reset the jlabel's text with new local
	private void updateStrings() {
		res = ResourceBundle.getBundle("MyResource",aLocale);
		jlDate.setText(res.getString("Date")) ;
		jlTime.setText(res.getString("Time")) ;
		jlLocale.setText(res.getString("Choose_locale")) ;
		jlTimeZone.setText(res.getString("Time_Zone")) ;
		jlDateStyle.setText(res.getString("Date_Style")) ;
		jlTimeStyle.setText(res.getString("Time_Style")) ;
	}
	//sort timezones and addit to combobox
	private void setAvailabTimeZones() {
		Arrays.sort(availableTimeZones);
		for(int i=0;i<availableTimeZones.length;i++){
			jcbTimeZones.addItem(availableTimeZones[i]);
		}
	}
	//renew the time with all sets 
	private void coputeLoan(){
		Calendar calendar = new GregorianCalendar(timeZone,getLocale());
		DateFormat forDatematter = DateFormat.getDateInstance(dateStyle, aLocale);
		DateFormat forTimematter = DateFormat.getTimeInstance(timeStyle, aLocale);
		forDatematter.setTimeZone(timeZone);
		forTimematter.setTimeZone(timeZone);
		jtfDate.setText(forDatematter.format(calendar.getTime()));
		jtfTime.setText(forTimematter.format(calendar.getTime()));
	}
	private class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			coputeLoan();
		}
	}
}
