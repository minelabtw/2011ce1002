package a9.s995002056;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener {

	JPanel p = new JPanel();
	JLabel screen = new JLabel();
	JButton clear = new JButton("clear");
	JButton start = new JButton("START");
	JButton stop = new JButton("STOP");
	Font f = new Font("Impact", 1, 15);
	int hour = 0;
	int minute = 0;
	int second = 0;
	boolean time = false;

	Clock c = new Clock();
	Timer t1 = new Timer(1000, new TimerListener());

	public FrameWork() {// add component
		start.addActionListener(this);
		stop.addActionListener(this);
		clear.addActionListener(this);

		add(this.c);

		p.setLayout(new GridLayout(1, 4, 1, 1));
		// 設定字形
		clear.setFont(f);
		start.setFont(f);
		stop.setFont(f);
		screen.setFont(f);

		screen.setText(hour + ":" + minute + ":" + second);

		p.add(screen);
		p.add(start);
		p.add(stop);
		p.add(clear);
		add(p, BorderLayout.SOUTH);

		t1.start();

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == start)
			time = true;
		if (e.getSource() == stop)
			time = false;
		if (e.getSource() == clear) {
			hour = 0;
			minute = 0;
			second = 0;
			screen.setText(hour + ":" + minute + ":" + second);
		}
	}

	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			setTime();
			repaint();
		}

	}

	public void setTime() {// 讓碼表進位
		if (time) {
			second++;
			if (second == 60) {
				second = 0;
				minute++;
			}
			if (minute == 60) {
				minute = 0;
				hour++;
			}
			if (hour == 99)
				hour = 0;
			screen.setText(hour + ":" + minute + ":" + second);
		}
	}
}
