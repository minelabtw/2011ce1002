package a9.s995002056;

import java.awt.*;
import java.util.*;
import javax.swing.*;

public class Clock extends JPanel {
	int hour, minute, second;
	String ct;

	Font f = new Font("Impact", 1, 15);

	Clock() {
	}

	public void setCurrentClock() {
		Calendar time = new GregorianCalendar();
		hour = time.get(11);
		minute = time.get(12);
		second = time.get(13);
		ct = hour + ":" + minute + ":" + second;
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		setCurrentClock();

		// 畫圓
		g.drawOval(getWidth() / 2 - 150, getHeight() / 2 - 150, 300, 300);

		g.setFont(f);
		g.drawString(ct, (int) (getWidth() / 2 - 20), 40);// 顯示currentTime
		g.drawString("12", getWidth() / 2 - 5, getHeight() / 2 - 130);// 設定時鐘數字
		g.drawString("3", getWidth() / 2 + 130, getHeight() / 2);
		g.drawString("6", getWidth() / 2 - 5, getHeight() / 2 + 140);
		g.drawString("9", getWidth() / 2 - 140, getHeight() / 2);

		// 畫指針
		// hour
		g.setColor(Color.blue);
		int x1 = (int) (getWidth() / 2);
		int y1 = (int) (getHeight() / 2);
		int x2 = (int) (getWidth() / 2 + 50 * Math.sin(hour * 30 * 2 * Math.PI
				/ 360));
		int y2 = (int) (getHeight() / 2 - 50 * Math.cos(hour * 30 * 2 * Math.PI
				/ 360));
		g.drawLine(x1, y1, x2, y2);

		// minute
		g.setColor(Color.red);
		x2 = (int) (getWidth() / 2 + 90 * Math.sin(minute * 6 * 2 * Math.PI
				/ 360));
		y2 = (int) (getHeight() / 2 - 90 * Math.cos(minute * 6 * 2 * Math.PI
				/ 360));
		g.drawLine(x1, y1, x2, y2);

		// second
		g.setColor(Color.yellow);
		x2 = (int) (getWidth() / 2 + 110 * Math.sin(second * 6 * 2 * Math.PI
				/ 360));
		y2 = (int) (getHeight() / 2 - 110 * Math.cos(second * 6 * 2 * Math.PI
				/ 360));
		g.drawLine(x1, y1, x2, y2);

	}
}
