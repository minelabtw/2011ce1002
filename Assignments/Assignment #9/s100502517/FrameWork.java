package a9.s100502517;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class FrameWork extends JFrame {
	Clock clock = new Clock();
	action a = new action();
	TimerListener timerlistener = new TimerListener();
	Timer timer = new Timer(1000, new TimerListener());	
	JButton transfer = new JButton("transfer");
	JButton start = new JButton("Start");
	JButton stop = new JButton("Stop");
	JButton clear = new JButton("Clear");
	boolean checktodo = true;
	String showtime;
	JLabel time = new JLabel(showtime.valueOf(clock.gethour())+":"+showtime.valueOf(clock.getminute())+":"+showtime.valueOf(clock.getsecond()));
	Font font = new Font("TimesRoman", Font.BOLD, 16);
	
	//時鐘的版面
	public FrameWork(){
		
			
		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(1,5));
		time.setFont(font);
		start.setFont(font);
		stop.setFont(font);
		clear.setFont(font);
		transfer.setFont(font);
		p1.add(time);
		p1.add(start);
		p1.add(stop);
		p1.add(clear);
		p1.add(transfer);
		
		setLayout(new BorderLayout());
		add(clock, BorderLayout.CENTER);
		add(p1, BorderLayout.SOUTH);
		
		transfer.addActionListener(a);
		start.addActionListener(a);
		stop.addActionListener(a);	
		clear.addActionListener(a);	
		
		timer.start();
	}
	
	//按下Button的反應
	class action implements ActionListener{
		public void actionPerformed(ActionEvent e){
			if(e.getSource() == start){
				timer.start();
			}
			if(e.getSource() == stop){
				if(!checktodo){
					timer.stop();				
				}
			}
			if(e.getSource() == clear){
				if(!checktodo){
					clock.clearClock();
				}				
			}
			if(e.getSource() == transfer){
				if(checktodo){
					clock.clearClock();
					timer.stop();
					checktodo = false;
					
				}
				else{					
					clock.setTime();
					timer.start();
					checktodo = true;
					
				}
			}
			
			time.setText((showtime.valueOf(clock.gethour())+":"+showtime.valueOf(clock.getminute())+":"+showtime.valueOf(clock.getsecond())));
			
			repaint();
		}
	}
	
	//timer 
	class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			
			if(checktodo)
				clock.setCurrentTime();
			else 
				clock.countTimer();//只有在時鐘模式才會自動跑
			time.setText((showtime.valueOf(clock.gethour())+":"+showtime.valueOf(clock.getminute())+":"+showtime.valueOf(clock.getsecond())));
						
			repaint();//更新
		}

		
		
		
	}

}
