package a9.s100502517;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.Timer;


import java.util.*;
public class Clock extends JPanel{
	private int hour;
	private int minute;
	private int second;	
		
	public Clock(){
		setCurrentTime();		
	}
	
	//轉換成時鐘
	public void setTime(){
		setCurrentTime();
	}
		
	
	public int gethour(){
		return hour;
	}
	
	public int getminute(){
		return minute;
	}
	
	public int getsecond(){
		return second;
	}
	
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		//時鐘
		int clockRadius = (int) (Math.min(getWidth(), getHeight())*0.8*0.5);
		int xCenter = getWidth()/2;
		int yCenter = getHeight()/2;
		
		g.setColor(Color.BLACK);
		g.drawOval(xCenter - clockRadius, yCenter - clockRadius , 2*clockRadius , 2*clockRadius);
		g.drawString("12", xCenter - 5, yCenter - clockRadius + 12);
		g.drawString("9", xCenter - clockRadius + 3, yCenter + 5);
		g.drawString("3", xCenter + clockRadius - 10, yCenter + 3);
		g.drawString("6", xCenter - 3, yCenter + clockRadius - 3);
		int sLength = (int)(clockRadius*0.8);
		int xSecond = (int)(xCenter + sLength*Math.sin(second*(2*Math.PI/60)));
		int ySecond = (int) (yCenter - sLength*Math.cos(second*(2*Math.PI/60)));
		g.setColor(Color.red);
		g.drawLine(xCenter, yCenter, xSecond, ySecond);
		
		int mLength = (int)(clockRadius*0.65);
		int xMinute = (int)(xCenter + mLength*Math.sin(minute*(2*Math.PI/60)));
		int yMinute = (int) (yCenter - mLength*Math.cos(minute*(2*Math.PI/60)));
		g.setColor(Color.blue);
		g.drawLine(xCenter, yCenter, xMinute, yMinute);
		
		int hLength = (int)(clockRadius*0.5);
		int xHour = (int)(xCenter + hLength*Math.sin((hour%12+minute/60)*(2*Math.PI/12)));
		int yHour = (int)(yCenter - hLength*Math.cos((hour%12+minute/60)*(2*Math.PI/12)));
		g.setColor(Color.green);
		g.drawLine(xCenter, yCenter, xHour, yHour);		
				
	}
	
	//系統時間
	public void setCurrentTime(){		
		Calendar calendar = new GregorianCalendar();
		this.hour = calendar.get(Calendar.HOUR_OF_DAY);
		this.minute = calendar.get(Calendar.MINUTE);
		this.second = calendar.get(Calendar.SECOND);
	}
	
	//歸零
	public void clearClock(){
		this.hour = 0;
		this.minute = 0;
		this.second = 0;
	}
	
	public void countTimer(){
		second += 1;
		if(second-60 == 0){
			second = 0;
			minute += 1;
		}
		if(minute-60 == 0){
			minute = 0;
			hour += 1;
		}
	}
	

}
