package a9.s100502003;

import java.awt.*;
import javax.swing.*;
import java.util.*;

public class Clock extends JPanel {
	private int hour=0, minute=0, second=0; // initial values are all 0
	
	public Clock() {
		setCurrentTime(); // once the system start, let the clock run
	}
	
	public int getSecond() {
		return this.second;
	}
	
	public int getMinute() {
		return this.minute;
	}
	
	public int getHour() {
		return this.hour;
	}
	
	public void setCurrentTime() { // get the time now
		Calendar calendar = new GregorianCalendar();
		this.hour = calendar.get(Calendar.HOUR_OF_DAY);
		this.minute = calendar.get(Calendar.MINUTE);
		this.second = calendar.get(Calendar.SECOND);
	}
	
	public void setCurrentClock() {
		this.second++; // each time the value of second add one
		if(this.second == 60) { // 60s=1min 
			this.second = 0;
			this.minute++;
		}
		if(this.minute == 60) { // 60min=1hr
			this.minute = 0;
			this.hour++;
		}
		if(this.hour == 12) // can't excess 12hr
			this.hour = 0;
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		int radius = (int)(Math.min(getWidth(), getHeight())*0.8*0.5); // radius of the clock's circle
		int xCenter = getWidth()/2;
		int yCenter = getHeight()/2;
		
		g.setColor(Color.BLACK);
		
		g.drawOval(xCenter-radius, yCenter-radius, 2*radius, 2*radius); // draw the circle
		// draw the numbers on the clock
		g.drawString("12", xCenter-5, yCenter-radius+15);
		g.drawString("9", xCenter-radius+5, yCenter);
		g.drawString("3", xCenter+radius-10, yCenter);
		g.drawString("6", xCenter, yCenter+radius-5);
		
		// 秒針
		int sLength = (int)(radius*0.8);
		int xSecond = (int)(xCenter+sLength*Math.sin(second*(2*Math.PI/60)));
		int ySecond = (int)(yCenter-sLength*Math.cos(second*(2*Math.PI/60)));
		g.setColor(Color.RED);
		g.drawLine(xCenter, yCenter, xSecond, ySecond);
		
		// 分針
		int mLength = (int)(radius*0.65);
		int xMinute = (int)(xCenter+mLength*Math.sin(minute*(2*Math.PI/60)));
		int yMinute = (int)(yCenter-mLength*Math.cos(minute*(2*Math.PI/60)));
		g.setColor(Color.GREEN);
		g.drawLine(xCenter, yCenter, xMinute, yMinute);
		
		// 時針
		int hLength = (int)(radius*0.5);
		int xHour = (int)(xCenter+hLength*Math.sin((hour%12+minute/60.0)*(2*Math.PI/12)));
		int yHour = (int)(yCenter-hLength*Math.cos((hour%12+minute/60.0)*(2*Math.PI/12)));;
		g.setColor(Color.BLUE);
		g.drawLine(xCenter, yCenter, xHour, yHour);
	}
	
	public void ClearClock() { // 重新計算
		this.hour = 0;
		this.minute = 0;
		this.second = 0;
	}
}
