package a9.s100502003;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FrameWork extends JFrame {
	private Clock clock = new Clock(); // declare an object of Clock
	// declare buttons
	private JButton start = new JButton("start");
	private JButton stop = new JButton("stop");
	private JButton clear = new JButton("clear");
	private JButton change = new JButton("change");
	
	public int times=0; // variable used to count the total times that "change" been pressed
	public JLabel Time = new JLabel(getTime());
	Timer timerNow = new Timer(1000, new NowTimeListener()); // timer that help to get the current time
	
	public FrameWork() {
		timerNow.start(); // start counting as soon as the system work(clock)
		JPanel p = new JPanel(new GridLayout(1,4,10,10)); // panel that concludes buttons
		ButtonListener listener = new ButtonListener();
		start.addActionListener(listener);
		stop.addActionListener(listener);
		clear.addActionListener(listener);
		change.addActionListener(listener);
		// add those buttons to the panel "p"
		p.add(start);
		p.add(stop);
		p.add(clear);
		p.add(change);
		
		JPanel pTop = new JPanel(new BorderLayout(5,5));
		pTop.add(new JLabel(" 現在是時鐘，按下change後即可變碼表"), BorderLayout.WEST); // leading messages
		pTop.add(Time, BorderLayout.EAST);
		
		setLayout(new BorderLayout(10,10));
		add(pTop, BorderLayout.NORTH); // informations
		add(clock, BorderLayout.CENTER); // clock
		add(p, BorderLayout.SOUTH);	// buttons
	}
	
	public String getTime() {
		String time = clock.getHour()+" : "+clock.getMinute()+" : "+clock.getSecond();
		return time;
	}
	
	class ButtonListener implements ActionListener {
		Timer timer = new Timer(1000, new TimerListener()); // timer that help the system of 馬錶
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == change) {
				if(times%2==0) { // 一開始times是0，時鐘在跑，按下change後times變奇數次，要讓時鐘變馬錶
					// both timer 的計數器都歸零
					timerNow.stop();
					timer.stop();
					clock.ClearClock(); // 指針歸零
					clock.repaint();
					Time.setText(getTime());
					times++;
				}
				else {
					clock.setCurrentTime(); // 得到目前時間
					clock.repaint();
					timerNow.start(); // 讓時間繼續跑
					Time.setText(getTime());
					times++;
				}	
			}
			else if(e.getSource() == start) {
				timer.start();
			}
			else if(e.getSource() == stop)
				timer.stop();
			else if(e.getSource() == clear) {
				if(times%2 != 0) { // 讓時鐘在跑時功能沒效(不讓馬錶功能影響到時鐘)
					timer.stop();
					clock.ClearClock();
					clock.repaint();
					Time.setText(getTime());
				}
			}	
		}
	}
	
	private class NowTimeListener implements ActionListener { // clock's timeListener
		public void actionPerformed(ActionEvent e) {
			clock.setCurrentTime(); // reset the time now
			Time.setText(getTime());
			clock.repaint();
		}
	}
	private class TimerListener implements ActionListener { // time counter's timeListener
		public void actionPerformed(ActionEvent e) {
			clock.setCurrentClock();
			Time.setText(getTime());
			clock.repaint();
		}
	}
}

