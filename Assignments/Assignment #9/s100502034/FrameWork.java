package a9.s100502034;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.*;

import javax.swing.*;
public class FrameWork extends JFrame {
	private int control = 0;//用來控制start按鈕的timer.start只執行一次
	private Timer timer;
	private JButton start = new JButton("start");
	private JButton stop = new JButton("stop");
	private JButton clear = new JButton("clear	");
	private JLabel time = new JLabel("00:00:00");
	private Clock clock = new Clock();
	public FrameWork() {
		time.setHorizontalAlignment(JLabel.CENTER);
		JPanel pp = new JPanel();//用來放label跟button的panel
		pp.setLayout(new GridLayout(1,4));
		pp.add(time);
		pp.add(start);
		pp.add(stop);
		pp.add(clear);
		add(clock, BorderLayout.CENTER);
		add(pp,BorderLayout.SOUTH);
		Listener listener = new Listener();
		start.addActionListener(listener);
		stop.addActionListener(listener);
		clear.addActionListener(listener);
	}
	class Listener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == start){
				if (control == 0){
					timer = new Timer(1000, new TimerListener());
					timer.start();
					control = 1;//再按start就不會有多一個時間在跑
				}
			}
			if(e.getSource() == stop){
				timer.stop();//stop
				control = 0;//代表沒有timer在跑了
			}
			if(e.getSource() == clear){
				clock.setHour(0);//數值重新初始化
				clock.setMinute(0);
				clock.setSecond(0);
				time.setText("00:00:00");
				timer.stop();
				control = 0;
				repaint();
			}
		}
	}
    class TimerListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
        	String h = "";
        	String m = "";
        	String s = "";
        	int hh = clock.getHour() %60;
        	int mm = clock.getMinute() %60;
        	int ss = clock.getSecond()%60;
        	if(hh <10){//如果單位數字就在前面加上一個"0"
        		h = "0" + hh;
        	}
        	else{
        		h = "" + hh;
        	}
        	if(mm<10){
        		m = "0" + mm;
        	}
        	else{
        		m = "" + mm;
        	}
        	if(ss<10){
        		s = "0" + ss;
        	}
        	else{
        		s = "" + ss;
        	}
        	time.setText(h + ":" + m +":"+ s);
        	repaint();
        }
        
    }
}
