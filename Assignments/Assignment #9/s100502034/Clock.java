package a9.s100502034;

import java.awt.Graphics;

import javax.swing.*;
import java.awt.*;
class Clock extends JPanel{
	private int hour = 0;
	private int minute = 0;
	private static int second = -1;
	public Clock(){
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
	    int clockRadius = (int)(Math.min(getWidth(), getHeight())*0.48);//把圖形稍稍平移了點
	    int xCenter = getWidth() / 2;
	    int yCenter = getHeight() / 2;
	    g.drawOval(xCenter - clockRadius, yCenter - clockRadius, 2 * clockRadius, 2 * clockRadius);
	    g.drawString("12", xCenter - 5, yCenter - clockRadius + 12);
	    g.drawString("9", xCenter - clockRadius + 3, yCenter + 5);
	    g.drawString("3", xCenter + clockRadius - 10, yCenter + 3);
	    g.drawString("6", xCenter - 3, yCenter + clockRadius - 3);
	    int sLength = (int)(clockRadius * 0.8);
	    int xSecond = (int)(xCenter + sLength * Math.sin(second * (2 * Math.PI / 60)));
	    int ySecond = (int)(yCenter - sLength * Math.cos(second * (2 * Math.PI / 60)));
	    g.setColor(Color.red);
	    g.drawLine(xCenter, yCenter, xSecond, ySecond);

	    int mLength = (int)(clockRadius * 0.65);
	    int xMinute = (int)(xCenter + mLength * Math.sin(minute * (2 * Math.PI / 60)));
	    int yMinute = (int)(yCenter - mLength * Math.cos(minute * (2 * Math.PI / 60)));
	    g.setColor(Color.blue);
	    g.drawLine(xCenter, yCenter, xMinute, yMinute);

	    int hLength = (int)(clockRadius * 0.5);
	    int xHour = (int)(xCenter + hLength * Math.sin((hour % 12 + minute / 60.0) * (2 * Math.PI / 12)));
	    int yHour = (int)(yCenter - hLength * Math.cos((hour % 12 + minute / 60.0) * (2 * Math.PI / 12)));
	    g.setColor(Color.green);
	    g.drawLine(xCenter, yCenter, xHour, yHour);
	    second++;
	    if(second % 60 == 0&& second>59){
	    	minute++;
	    }
	    if (minute % 60 == 0 &&minute>59 && second %60==0){
	    	hour++;
	    }
	}
	public int getHour() {//一堆set跟get 時分秒的method
		return hour;
	}
    public int getMinute() {  
        return minute;  
    }  
    public int getSecond() {  
        return second;  
    } 
    public void setHour(int h) {  
        hour = h;  
    }  
    public void setMinute(int m) {  
        minute = m;  
    }  
    public void setSecond(int s) {  
        second = s;  
    }


}
