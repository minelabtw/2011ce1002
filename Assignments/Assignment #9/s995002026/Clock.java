package a9.s995002026;

import javax.swing.*;

import java.awt.*;

import java.awt.event.ActionListener;

import java.util.Calendar;

import java.util.GregorianCalendar;

class Clock extends JPanel{
	
	private int hour=0,minute=0,second=0;	//碼表計時
	
	private int chour,cminute,csecond;
	
	private int hourhand,secondhand;
	
	
	public Clock(){
		hourhand=50;
		secondhand=100;
		
	}
	
	public void clearClock(){
		hour=0;
		minute=0;
		second=0;
	}
	
	public String setCurrentTime(){			//當前時間
		Calendar time=new GregorianCalendar();
		
		chour=time.get(Calendar.HOUR_OF_DAY);	
		
		cminute=time.get(Calendar.MINUTE);
		
		csecond=time.get(Calendar.SECOND);
		
		String currenttime=Integer.toString(chour)+":"+Integer.toString(cminute)+":"+Integer.toString(csecond);
		
		return currenttime;				//回傳當前時間
	}
	
	public void paintComponent(Graphics g){
		int x1,y1,x2,y2;
	
		super.paintComponent(g);
		
		g.setColor(Color.ORANGE);		//時鐘外型
		g.drawOval(getWidth()/2-150,getHeight()/2-150,300, 300);
		
		
		g.setColor(Color.blue);			//時針
		x1=getWidth()/2;
		y1=getHeight()/2;
		x2=(int)(getWidth()/2+hourhand*Math.sin(chour*2*Math.PI/60));
		y2=(int)(getHeight()/2-hourhand*Math.cos(chour*2*Math.PI/60));
		g.drawLine(x1,y1, x2,y2);
		
		g.setColor(Color.blue);			//秒針
		x2=(int)(getWidth()/2+secondhand*Math.sin(csecond*2*Math.PI/60));
		y2=(int)(getHeight()/2-secondhand*Math.cos(csecond*2*Math.PI/60));
		g.drawLine(x1,y1, x2,y2);
		
		//整點
		g.drawString("12", x1-6, y1-130);
		g.drawString("3", x1+130,y1);
		g.drawString("6", x1-5, y1+140);
		g.drawString("9", x1-140, y1);
		
		//現在時間
		g.drawString(setCurrentTime(), x1-20, y1-160);
		
		repaint();

	}
	
	public void settime(int a){			//碼表
		hour=a/3600;
		
		minute=a/60;
		
		second=a%60;
	}
	
	public int gethour(){
		return hour;
	}
	
	public int getminute(){
		return minute;
	}
	
	public int getsecond(){
		return second;
	}
	 
	
	
}


