package a9.s995002026;

import javax.swing.*;

import java.awt.*;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;

public class FrameWork extends JFrame implements ActionListener{
	
	public JPanel p2=new JPanel(new GridLayout(1,4));
	
	public JButton start=new JButton("Start");
	
	public JButton stop=new JButton("Stop");
	
	public JButton clear=new JButton("Clear");
	
	public JLabel label=new JLabel("");
	
	public int time;
	
	public boolean flag;		//判斷指令動作
	
	Clock clock=new Clock();
	
	Timer timer=new Timer(1000,new TimerListener());
	
	
	public FrameWork(){
		
		
		add(clock);
		
		function();
		
		add(p2,BorderLayout.SOUTH);
		
		start.addActionListener(this);
		
		stop.addActionListener(this);
		
		clear.addActionListener(this);

	}
	
	public void function(){
		p2.add(start);
		
		p2.add(stop);
		
		p2.add(clear);
		
		Font font=new Font("Serif",Font.BOLD,15);
		
		label.setFont(font);
		
		p2.add(label);
		
		
		
	}
	
	public void actionPerformed(ActionEvent e){
		if(e.getSource()==start){
			timer.start();			//開始計時
			flag=true;
		}
		if(e.getSource()==stop){
			timer.stop();			//停止計時
			flag=false;
		}
		if(e.getSource()==clear){
			time=0;
			clock.clearClock();		//重置
			timer.stop();
			flag=false;
		}
		String output="     "+Integer.toString(clock.gethour())+":"+Integer.toString(clock.getminute())+":"+Integer.toString(clock.getsecond());
		label.setText(output);		//碼表時間
	}
	
	class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			
			if(flag){
				time=time+1;
				clock.settime(time);
				
				String output="     "+Integer.toString(clock.gethour())+":"+Integer.toString(clock.getminute())+":"+Integer.toString(clock.getsecond());
				label.setText(output);	
					
			}
		}
	}
}

