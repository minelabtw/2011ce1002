package a9.s100502004;

import java.awt.event.*;
import javax.swing.*;
import javax.xml.stream.events.StartDocument;
import java.awt.*;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Clock extends JPanel{
	protected int hour = 0;
	protected int minute = 0;
	protected int second = 0;	
	
	public Clock(){
	}

	public void clearClockNumber(){//use this method to clean the clock
		hour = minute = second = 0;
	}
	
	public void setTimer(){//use this to change the number for second, minute, and hour
		second++;
		if(second==60){
			minute++;
			second = 0;
		}
		if(minute==60){
			hour++;
			minute = 0;
		}
	}
	
	
	public void setHour(int hour){
		this.hour = hour;
		repaint();
	}
	
	public void setMinute(int minute){
		this.minute = minute;
		repaint();
	}
	public void setSecond(int second){
		this.second = second;
		repaint();
	}
	
	public int getHour(){
		return hour;
	}
	
	public int getMinute(){
		return minute;
	}
	
	public int getsSecond(){
		return second;
	}
	
	protected void paintComponent(Graphics g){//use to graph the clock
		super.paintComponent(g);
		
		int clockRadius = (int)(Math.min(getWidth(), getHeight())*0.8*0.5);
		
		int xCenter = getWidth()/2;
		int yCenter = getHeight()/2;
		
		
		g.setColor(Color.BLACK);
		g.drawOval(xCenter-clockRadius, yCenter-clockRadius, clockRadius*2, clockRadius*2);
		g.drawString("12", xCenter-5, yCenter-clockRadius+12);
		g.drawString("9", xCenter-clockRadius+3, yCenter+5);
		g.drawString("3", xCenter+clockRadius-10, yCenter+3);
		g.drawString("6", xCenter-3, yCenter+clockRadius-3);
		
		int sLength = (int)(clockRadius*0.8);
		int xSecond = (int)(xCenter + sLength*Math.sin(second*(2*Math.PI/60)));
		int ySecond = (int)(yCenter-sLength*Math.cos(second*(2*Math.PI/60)));
		g.setColor(Color.red);
		g.drawLine(xCenter,yCenter,xSecond,ySecond);
		
		
		int mLengh = (int)(clockRadius*0.65);
		int xMinute = (int)(xCenter+mLengh*Math.sin(minute*(2*Math.PI/60)));
		int yMinuy = (int)(yCenter-mLengh*Math.cos(minute*(2*Math.PI/60)));
		g.setColor(Color.blue);
		g.drawLine(xCenter, yCenter, xMinute, yMinuy);
		
		
		int hLength = (int)(clockRadius*0.5);
		int xHour = (int)(xCenter+hLength*Math.sin((hour%12+minute/60)*(2*Math.PI/12)));
		int yHour = (int)(yCenter-hLength*Math.cos((hour%12+minute/60)*(2*Math.PI/12)));
		g.setColor(Color.green);
		g.drawLine(xCenter, yCenter, xHour, yHour);	
	}
	
	public void  setCurrentClock(){//use to show the current time
		Calendar calendar = new GregorianCalendar(); 
		
		this.hour = calendar.get(Calendar.HOUR_OF_DAY);
		this.minute = calendar.get(Calendar.MINUTE);
		this.second = calendar.get(Calendar.SECOND);		
	}
	
	public String setClockNumber(){//use to set the number besides button
		return hour + ":" + minute + ":" +second ;
	}
	
	public Dimension getPreferredSize(){
		return new Dimension(200,200);
	}	
}
	
	
	
	
	
	
	

