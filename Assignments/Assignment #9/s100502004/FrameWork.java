package a9.s100502004;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.xml.stream.events.StartDocument;


public class FrameWork extends JFrame implements ActionListener{
	private Timer timer;
	private Clock clock = new Clock();
	JButton start = new JButton("start");
	JButton stop = new JButton("stop/continue");
	JButton clear = new JButton("clear");
	JButton current = new JButton("show the current time");
	JLabel screen = new JLabel("");
	boolean StartOperation = false;//use to check what kind of information should it show
	
	
	public FrameWork(){
		clock.setCurrentClock();
		JPanel p1 = new JPanel();//use to combine the buttons
		screen.setText(clock.getHour() + ":" + clock.getMinute() + ":" + clock.getsSecond() );
		add(clock,BorderLayout.NORTH);
		p1.setLayout(new GridLayout(1,4));
		p1.add(screen);
		p1.add(start);
		p1.add(stop);
		p1.add(clear);
		p1.add(current);		
		add(p1,BorderLayout.SOUTH);	
		start.addActionListener(this);
		stop.addActionListener(this);
		clear.addActionListener(this);
		current.addActionListener(this);
		clock.setCurrentClock();		
		timer = new Timer(1000, new TimerListener());
		timer.start();
		
	}
	
	
	public void actionPerformed(ActionEvent e) {//set if you press the button what will happen
		if(e.getSource()==start){//to tell the buttons how to do 
			timer.start();
			screen.setText("0:0:0");
			StartOperation = true;
			clock.clearClockNumber();						
			screen.setText(clock.setClockNumber());			
		}
		
		if(e.getSource()==stop){
			if(timer.isRunning()){
				timer.stop();
			} 
			else{
				timer.start();
			}
		}
		
		if(e.getSource()==clear){
			screen.setText("0:0:0");	
			clock.clearClockNumber();
			timer.stop();
		}
		
		if(e.getSource()==current){
			timer.start();
			clock.setCurrentClock();
			StartOperation = false;
		}
		clock.repaint();
	}
	

	
	
	
	private class TimerListener implements ActionListener{//use to change the number besides the buttons
		public void actionPerformed(ActionEvent e){
			
			String h = "0";
			String m = "0";
			String s = "0";
			if(StartOperation==false){
				clock.setCurrentClock();			
				h = String.valueOf(clock.getHour());
				m = String.valueOf(clock.getMinute());
				s = String.valueOf(clock.getsSecond());
				screen.setText(h + ":" + m + ":" + s);
				clock.repaint();
			}
			
			if(StartOperation==true){		
				screen.setText(clock.getHour() + ":" + clock.getMinute() + ":" + clock.getsSecond());
				clock.setTimer();
				clock.repaint();
			}
		}		
	}
	
}
