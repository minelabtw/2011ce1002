package a9.s992008002;

import java.awt.*;
import javax.swing.*;
import java.util.*;

public class Clock extends JPanel {

	private int hour;
	private int minute;
	private int second;
	private int nowhour;
	private int nowminute;
	private int nowsecond;


/** Construct a default clock with the current time*/
{
	setCurrentTime();
	setCurrentTime2();
}


public Clock()
{
	this.hour = 0;//hour;
	this.minute = 0;//minute;
	this.second = 0;//second;
	
}

//清除時鐘
public void clearClock(){
	hour = 0;
	minute = 0;
	second = -1;
}

public void paintComponent() {
	repaint();
}

//Set current hour, minute and second  碼表版
public void setCurrentTime() {
	if(this.minute == 60)this.hour++ ;
	if(this.second == 60)this.minute++ ;
	this.second ++;
}

//Set current hour, minute and second  時鐘版
public void setCurrentTime2() {
	// Construct a calendar for the current date and time
	Calendar calendar = new GregorianCalendar();
	// Set current hour, minute and second
	this.nowhour = calendar.get(Calendar.HOUR_OF_DAY);
	this.nowminute = calendar.get(Calendar.MINUTE);
	this.nowsecond = calendar.get(Calendar.SECOND);

}

//碼表版set & get
/** Return hour */
public int getHour() {
	return hour;
}

/** Set a new hour */
public void setHour(int hour) {
	this.hour = hour;
	repaint();
}

/** Return minute */
public int getMinute() {
	return minute;
}

/** Set a new minute */
public void setMinute(int minute) {
	this.minute = minute;
	repaint();
}

/** Return second */
public int getSecond() {
	return second;
}

/** Set a new second */
public void setSecond(int second) {
	this.second = second;
	repaint();
}


//時鐘版 set & get
/** Return hour */
public int getNowHour() {
	return nowhour;
}

/** Set a new hour */
public void setNowHour(int nowhour) {
	this.nowhour = nowhour;
	
}

/** Return minute */
public int getNowMinute() {
	return nowminute;
}

/** Set a new minute */
public void setNowMinute(int nowminute) {
	this.nowminute = nowminute;
	repaint();
}

/** Return second */
public int getNowSecond() {
	return nowsecond;
}

/** Set a new second */
public void setNowSecond(int nowsecond) {
	this.nowsecond = nowsecond;
	
}


/** Draw the clock */
protected void paintComponent(Graphics g) {

	super.paintComponent(g);
// Initialize clock parameters
	int clockRadius = (int)(Math.min(getWidth(), getHeight()) * 0.8 * 0.5);
	int xCenter = getWidth() / 2;
	int yCenter = getHeight() / 2;
// Draw circle
	g.setColor(Color.BLACK);
	g.drawOval(xCenter - clockRadius, yCenter - clockRadius,2 * clockRadius, 2 * clockRadius);
	g.drawString("12", xCenter - 5, yCenter - clockRadius + 12);
	g.drawString("9", xCenter - clockRadius + 3, yCenter + 5);
	g.drawString("3", xCenter + clockRadius - 10, yCenter + 3);
	g.drawString("6", xCenter - 3, yCenter + clockRadius - 3);
// Draw second hand
	int sLength = (int)(clockRadius * 0.8);

	int xSecond = (int)(xCenter + sLength *Math.sin(second * (2 * Math.PI / 60)));
	int ySecond = (int)(yCenter - sLength *Math.cos(second * (2 * Math.PI / 60)));
	g.setColor(Color.red);
	g.drawLine(xCenter, yCenter, xSecond, ySecond);
// Draw minute hand
	int mLength = (int)(clockRadius * 0.65);
	int xMinute = (int)(xCenter + mLength *Math.sin(minute * (2 * Math.PI / 60)));
	int yMinute = (int)(yCenter - mLength *Math.cos(minute * (2 * Math.PI / 60)));
	g.setColor(Color.blue);
	g.drawLine(xCenter, yCenter, xMinute, yMinute);
// Draw hour hand
	int hLength = (int)(clockRadius * 0.5);
	int xHour = (int)(xCenter + hLength *Math.sin((hour % 12 + minute / 60.0) * (2 * Math.PI / 12)));
	int yHour = (int)(yCenter - hLength *Math.cos((hour % 12 + minute / 60.0) * (2 * Math.PI / 12)));
	g.setColor(Color.green);
	g.drawLine(xCenter, yCenter, xHour, yHour);
}


}
