package a9.s992008002;

import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;


public class FrameWork extends JFrame implements ActionListener{	

private Clock clock = new Clock();
private JButton clear = new JButton("CLEAR");
private JButton start = new JButton("START");
private JButton stop = new JButton("STOP");	
private JLabel screen =  new JLabel();
private JLabel timescreen = new JLabel();

//Create a timer with delay 1000 ms
Timer timer = new Timer(1000, new TimerListener());


public FrameWork() {
//設定"碼表讀秒" & "現在時間顯示" 兩個label
screen.setText("讀秒: "+String.valueOf(clock.getHour())+" : "+String.valueOf(clock.getMinute())+" : "+String.valueOf(clock.getSecond()));
timescreen.setText("時間: "+String.valueOf(clock.getNowHour())+" : "+String.valueOf(clock.getNowMinute())+" : "+String.valueOf(clock.getNowSecond()));

clear.addActionListener(new TimerListener());
start.addActionListener(new TimerListener());
stop.addActionListener(new TimerListener());

add(clock);//加上時鐘

//加上button
clock.add(screen);
clock.add(clear);
clock.add(start);
clock.add(stop);
clock.add(timescreen);

}

/** Handle the action event */
private class TimerListener implements ActionListener {

	
public void actionPerformed(ActionEvent e) {
// Set new time and repaint the clock to display current time

	//開始計數
	if (e.getSource() == start){
    	timer.start();
		}
	//停止並歸零
	else if (e.getSource() == clear){
	    timer.stop();
		clock.clearClock();
		clock.paintComponent();
	    } 
	//暫停
	else if (e.getSource() == stop){
	     timer.stop();
	    }
	//重設jlabel顯示
	screen.setText("讀秒: "+String.valueOf(clock.getHour())+" : "+String.valueOf(clock.getMinute())+" : "+String.valueOf(clock.getSecond()+1));
	timescreen.setText("時間: "+String.valueOf(clock.getNowHour())+" : "+String.valueOf(clock.getNowMinute())+" : "+String.valueOf(clock.getNowSecond()));

	//重設時間及讀秒
	clock.setCurrentTime();
	clock.setCurrentTime2();
	
	//重畫圖
	clock.paintComponent();

}
}

@Override
public void actionPerformed(ActionEvent e) {
	// TODO Auto-generated method stub
	
}



}

