package a9.s100502506;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.Timer;
//class FrameWork:
//include three button (start, stop, clear button) and one JLabel (screen)
//implement the event of the buttons
//implement the mileometer��s functionality with Timer (so you need to implement class TimerListner)
public class FrameWork extends JFrame
{
	private static int hour;
	private static int minute;
	private static int second;
	private static DrawClock test=new DrawClock();
	private Clock testClock=new Clock();
	private JLabel label1;
	private JButton button1;
	private JButton button2;
	private JButton button3;
	private JPanel panel1;
	private Timer timer =new Timer(1000, new TimerListener());
	private String hourString="00";
	private String minuteString="00";
	private String secondString="00";
	private String TimeString=String.format("%s:%s:%s",hourString,minuteString,secondString);
	public FrameWork()
	{
		actionListener listener=new actionListener();
		add(test,BorderLayout.CENTER);
		panel1=new JPanel();
		label1=new JLabel();
		label1.setText(TimeString);
		button1=new JButton("Start");
		button2=new JButton("Stop");
		button3=new JButton("Reset");
		panel1.add(label1);
		panel1.add(button1);
		panel1.add(button2);
		panel1.add(button3);
		panel1.setLayout(new GridLayout(1,4,0,0));
		add(panel1,BorderLayout.SOUTH);
		//action listener
		button1.addActionListener(listener);
		button2.addActionListener(listener);
		button3.addActionListener(listener);
	}
	class TimerListener implements ActionListener		//Time Listener
	{
		public void actionPerformed(ActionEvent e)
		{
			
			
			testClock.timeplus();
			hour=testClock.getHour();
			minute=testClock.getMinute();
			
			second=testClock.getSecond();
			secondString=String.format("0%d",second);
			minuteString=String.format("0%d",minute);
			hourString=String.format("0%d",hour);
			//set Jlabel Text
			if(second>=10)
			{
				secondString=String.format("%d",second);
			}
			if(minute>=10) 
			{
				minuteString=String.format("%d",minute);
			}
			if(hour>=10)
			{
				hourString=String.format("%d",hour);
			}
			TimeString=String.format("%s:%s:%s",hourString,minuteString,secondString);
			label1.setText(TimeString);
			repaint();
		}
		
	}
	class actionListener implements ActionListener		//Time Listener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(e.getSource()==button1)//start
			{
				timer.start();
				
			}
			else if(e.getSource()==button2)//stop
			{
				timer.stop();
			}
			else if(e.getSource()==button3)//reset
			{
				testClock.clearClock();
				hour=testClock.getHour();
				minute=testClock.getMinute();
				second=testClock.getSecond();
				TimeString="00:00:00";
				label1.setText(TimeString);
				timer.stop();
			}
			repaint();
		}
		
	}
	
	static class DrawClock extends JPanel//paint on Jpanel
	{
		public DrawClock() 
		{
			
			
		}
		protected void paintComponent(Graphics g)
		{
			
			super.paintComponent(g);
			int ClockRadius=(int)(Math.min(getHeight(), getWidth())*0.4);
			int xcenter=getWidth()/2;
			int ycenter=getHeight()/2;
			//draw clock
			g.drawOval(xcenter-ClockRadius,ycenter-ClockRadius,2*ClockRadius, 2*ClockRadius);
			g.drawString("12", xcenter,ycenter-ClockRadius+10);
			g.drawString("9", xcenter-ClockRadius+5, ycenter);
			g.drawString("3", xcenter+ClockRadius-10, ycenter);
			g.drawString("6", xcenter, ycenter+1*ClockRadius);
			//draw second
			g.setColor(Color.red);
			g.drawLine(xcenter, ycenter, xcenter+(int)((ClockRadius)*0.8*Math.sin(second*(2*Math.PI/60))),ycenter-(int)((ClockRadius)*0.8*Math.cos(second*(2*Math.PI/60))));
			//draw minute
			g.setColor(Color.gray);
			g.drawLine(xcenter, ycenter, xcenter+(int)((ClockRadius)*0.5*Math.sin(minute*(2*Math.PI/60))),ycenter-(int)((ClockRadius)*0.5*Math.cos(minute*(2*Math.PI/60))));
			//draw hour
			g.setColor(Color.blue);
			g.drawLine(xcenter, ycenter, xcenter+(int)((ClockRadius)*0.3*Math.sin(hour*(2*Math.PI/60))),ycenter-(int)((ClockRadius)*0.3*Math.cos(hour*(2*Math.PI/60))));

			
		}
		
	}


}
