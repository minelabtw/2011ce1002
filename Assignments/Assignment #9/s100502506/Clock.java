package a9.s100502506;

import java.awt.Graphics;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.Timer;


//class Clock: 
//data member: hour, minute, second, all of them are integers
//method clearClock: to reset the time
//method setCurrentClock: to set the current time
//method paintComponent: to draw the clock and second hand
//implement as a JPanel
public class Clock				
{
	private static int hour;
	private static int minute;
	private static int second;

	public Clock()
	{
	}
	public void clearClock()					//reset
	{
		second=0;
		minute=0;
		hour=0;
	}
	public void setSecond(int input)			//setsecond
	{
		second=input;
		
	}
	public void setMinute(int input)			//setminute
	{
		minute=input;
	}
	public void setHour(int input)				//sethour
	{
		hour=input;
	}
	public int getSecond()						//getsecond
	{
		return second;
	}
	public int getMinute()						//getminute
	{
		return minute;
	}
	public int getHour()						//gethour
	{
		return hour;
	}
	public void timeplus()						//second++
	{
		second++;
		if(second>=60) 							
		{
			second = 0;
			minute ++;
		}
		if(minute>=60) 
		{
			minute = 0;
			hour ++;
		}
		if(hour>=24) 
		{
			hour=0;
			second=0;
			minute=0;
		}
		
	}
}
