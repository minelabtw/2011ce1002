package a9.s100502511;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Framework extends JFrame implements ActionListener {
	JButton start = new JButton("Start");
	JButton stop = new JButton("Stop");
	JButton clear = new JButton("Clear");
	Clock clock = new Clock();
	JPanel button = new JPanel();
	JLabel screen = new JLabel();
	Timer timer = new Timer(1000, new TimerListener());
	boolean START = false;

	public Framework() {
		button.add(start, BorderLayout.WEST);
		button.add(stop, BorderLayout.CENTER);
		button.add(clear, BorderLayout.EAST);
		add(clock, BorderLayout.CENTER);
		add(button, BorderLayout.SOUTH);
		start.addActionListener(this);
		stop.addActionListener(this);
		clear.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == start) { // 按下start,時鐘開始跑
			timer.start();
			START = true;
		} else if (e.getSource() == stop) { // 按下stop,時鐘停止
			timer.stop();
		} else if (e.getSource() == clear) { // 按下clear,初始時鐘
			clock.clearClock();
			clock.repaint();
			timer.stop();
		}
	}

	public class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (START = true) { // 當START為true,時間開始計時
				clock.setCurrentTime();
			}
			clock.repaint();
		}
	}
}