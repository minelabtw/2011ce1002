package a9.s100502511;

import javax.swing.JFrame;

public class A91 {
	public static void main(String[] args) {
		Framework homework = new Framework();
		homework.setTitle("Clock");
		homework.setSize(400, 400);
		homework.setLocationRelativeTo(null);
		homework.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		homework.setVisible(true);
	}
}
