package a9.s100502505;

import java.awt.event.*;
import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;


public class Clock extends JPanel{
	private int hour;
	private int minute;
	private int second;
	
	public Clock()//一開始指針都在12的位置
	{
		hour = 0;
		minute = 0;
		second = 0;
	}
	
	public void paintComponent(Graphics g)//畫時鐘
	{
		super.paintComponent(g);
		
		int clockRadius = (int)(Math.min(getWidth(),getHeight())*0.8*0.5);
		int xCenter = getWidth()/2;
		int yCenter = getHeight()/2;
		
		g.setColor(Color.BLACK);
		g.drawOval(xCenter-clockRadius, yCenter-clockRadius, 2*clockRadius, 2*clockRadius);
		g.drawString("12", xCenter-5, yCenter-clockRadius+12);
		g.drawString("9", xCenter-clockRadius+3, yCenter+5);
		g.drawString("3", xCenter+clockRadius-10, yCenter+3);
		g.drawString("6",xCenter-3,yCenter+clockRadius-3);
		
		int sLength = (int)(clockRadius*0.8);
		int xSecond = (int)(xCenter+sLength*Math.sin(second*(2*Math.PI/60)));
		int ySecond = (int)(yCenter-sLength*Math.cos(second*(2*Math.PI/60)));
		g.setColor(Color.black);//秒針是黑色的
		g.drawLine(xCenter, yCenter, xSecond, ySecond);
		
		int mLength = (int)(clockRadius*0.65);
		int xMinute = (int)(xCenter+mLength*Math.sin(minute*(2*Math.PI/60)));
		int yMinute = (int)(yCenter-mLength*Math.cos(minute*(2*Math.PI/60)));
		g.setColor(Color.red);//分針是紅色的
		g.drawLine(xCenter, yCenter, xMinute, yMinute);
		
		int hLength = (int)(clockRadius*0.5);
		int xHour = (int)(xCenter+hLength*Math.sin((hour%12+minute/60)*(2*Math.PI/60)));
		int yHour = (int)(yCenter-hLength*Math.cos(minute*(2*Math.PI/60)));
		g.setColor(Color.yellow);//時針是黃色的
		g.drawLine(xCenter, yCenter, xHour, yHour);
		
	}
	public void clearClock()//回到12
	{
		second = 0;
		minute = 0;
		hour = 0;
		repaint();
	}
	public void setCurrentTime()//每秒動一次
	{
		second++;
		minute = second/60;
		hour = second/3600;
		repaint();
	}
	
	public Dimension getPreferredSize()//時鐘面積
	{
		return new Dimension(200,200);
	}
	
}

