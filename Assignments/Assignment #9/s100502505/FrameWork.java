package a9.s100502505;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener
{
	Clock clock = new Clock();//宣告物件
	JButton start = new JButton("Start");//宣告按鈕
	JButton stop = new JButton("Stop");
	JButton clear = new JButton("Clear");
		
	public FrameWork()
	{
		JPanel p = new JPanel();//排時鐘和按鈕的位置
		p.add(start);
		p.add(stop);
		p.add(clear);
		add(clock,BorderLayout.CENTER);
		add(p,BorderLayout.SOUTH);
		start.addActionListener(this);//讓按鈕有意義
		stop.addActionListener(this);
		clear.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent arg0) 
	{
		if(arg0.getSource() == start)//案start按鈕,就開始動
		{
			timer.start();
		}
		if(arg0.getSource() == stop)//案stop按鈕,就停止
		{
			timer.stop();
		}
		if(arg0.getSource() == clear)//案clear按鈕,就重新開始
		{
			clock.clearClock();
		}
	}
	
	Timer timer = new Timer(1000, new TimerListener());
	
	class TimerListener implements ActionListener 
	{
		public void actionPerformed(ActionEvent e) 
		{
			clock.setCurrentTime();//讓時鐘動			
		}

	}
}

