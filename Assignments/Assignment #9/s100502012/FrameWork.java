package a9.s100502012;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;




public class FrameWork extends JFrame implements ActionListener{
	Timer timer=new Timer(1000,new TimerListener());
	private Clock needle=new Clock();
	private JButton b1=new JButton("START");
	private JButton b2=new JButton("STOP");
	private JButton b3=new JButton("CLEAR");
	private String hrSTR="0"+needle.getHr()+":";
	private String minSTR="0"+needle.getMin()+":";
	private String secSTR="0"+needle.getSec();
	private int hr=0;
	private int min=0;
	private int sec=0;
	private String Text="00:00:00";
	private JLabel counter=new JLabel(Text);
	
	
	public FrameWork(){
		Font largeFont=new Font("Mesquite Std",Font.BOLD,25);//setting counter
        JPanel Panel=new JPanel();//counter&&buttons
        JPanel p1=new JPanel();//counter        
        JPanel p2=new JPanel();//buttons
        
        counter.setFont(largeFont);
        counter.setForeground(Color.red);
        p1.add(counter);

	    p2.setLayout(new GridLayout(1,3));
   	    p2.add(b1);
   	    b1.addActionListener(this);//setting function of button 
   	    p2.add(b2);
   	    b2.addActionListener(this);//setting function of button
   	    p2.add(b3);
   	    b3.addActionListener(this);//setting function of button
   	    
   	    Panel.setLayout(new GridLayout(2, 1));
   	    Panel.add(p1);
   	    Panel.add(p2);
   	    
   	    add(needle,BorderLayout.CENTER);//add clock to frame
		add(Panel,BorderLayout.SOUTH);//add Panel to frame
	}
	
	public void setTimeText(){
		if (hr<10){
    		hr=needle.getHr();//get current hour
    	    hrSTR="0"+hr+":";//store information as string
    	}
    	else{
    		hr=needle.getHr();
    		hrSTR=hr+":";
    	}
    	
    	if (min<10){
    		min=needle.getMin();//get current minute
    	    minSTR="0"+min+":";//store information as string 
    	}
    	else{
    		min=needle.getMin();
    		minSTR=min+":";
    	}
    	
    	if (sec<10){
    		sec=needle.getSec();//store information as string 
    	    secSTR="0"+sec;//store information as string 
    	}
    	else{
    		sec=needle.getSec();
    		secSTR=sec+"";
    	}
    	
    	counter.setText(hrSTR+minSTR+secSTR);// access all the information of current time to the counter displaying onto the frame
	}
	
	public void actionPerformed(ActionEvent e){//setting buttons functions
		
	    if (e.getSource()==b1){
	    	timer.start();//starting the clock
	    }
	    
	    if (e.getSource()==b2){
        	timer.stop();//stopping the clock
        }
        
        if (e.getSource()==b3){
        	needle.clear();//clearing the whole time that it has pass through
        	timer.stop();//and stop the clock(you need to click the button "START" again and restart the clock from time zero)
        }
	}
	
	class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			setTimeText();//that the information of current time could be renew per second
		    needle.repaint();//renew the graphic of clock per second
		}
	}
}
