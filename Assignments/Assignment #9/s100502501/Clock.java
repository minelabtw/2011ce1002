package a9.s100502501;
import java.awt.*;

import javax.swing.*;
public class Clock extends JPanel{
	protected int hour=0,minute=0,second=0;
	public Clock(){ //constructor
		setCurrentClock();
	}
	public Clock(int hr,int min,int sec){
		this.hour=hr;
		this.minute=min;
		this.second=sec;
	}
	public void clearClock(){ //reset the time
		hour=0;
		minute=0;
		second=0;
	}
	public void setCurrentClock(){ //calculate the time
		int sec1=second;
		second=second%60;
		minute+=sec1/60;
		if(minute%60==0&&minute!=0){
			minute=0;
			hour++;
		}
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int clockRadius=(int)(Math.min(getWidth(), getHeight())*0.75*0.5);
	    int xCenter=getWidth()/2;
		int yCenter=getHeight()/2;
		g.setColor(new Color(100,149,237)); // Draw circle
		g.drawOval(xCenter-clockRadius, yCenter-clockRadius,2*clockRadius,2*clockRadius);
		g.drawString("12",xCenter-7,yCenter-clockRadius+15);
		g.drawString("3",xCenter+clockRadius-13,yCenter+3);
		g.drawString("6",xCenter-3,yCenter+clockRadius-5);
		g.drawString("9",xCenter-clockRadius+5,yCenter+6);

		int sLength=(int)(clockRadius*0.8); // Draw second hand
		int xSecond=(int)(xCenter+sLength*Math.sin(second*(2*Math.PI/60)));
		int ySecond=(int)(yCenter-sLength*Math.cos(second*(2*Math.PI/60)));
		g.setColor(new Color(255,193,37));
		g.drawLine(xCenter, yCenter, xSecond, ySecond);
		

	}
	
}
