package a9.s100502501;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class FrameWork extends JFrame implements ActionListener{
	private Clock ck=new Clock();
	JButton start=new JButton("Start");  //create button
	JButton stop=new JButton("Stop");
	JButton clear=new JButton("Clear");
	JLabel jlb=new JLabel(""); //create label
	Timer timer=new Timer(1000,new TimerListener()); //create a timer
	public FrameWork(){
		JPanel p1=new JPanel(new GridLayout(1,2)); //create panel
		JPanel p2=new JPanel(new BorderLayout());    
		jlb.setText(ck.hour+":"+ck.minute+":"+ck.second);
		jlb.setFont(new Font("SansSerif",Font.BOLD,28)); //set font of the label
		jlb.setHorizontalAlignment(JLabel.CENTER); //set the text in the center of label
		jlb.setForeground(new Color(0,139,0)); //set text color
		p1.add(start); //add button to panel
		p1.add(stop);
		p1.add(clear);
		p2.add(jlb,BorderLayout.NORTH); //add label to panel
		p2.add(ck,BorderLayout.CENTER); //add clock to panel
		p2.add(p1,BorderLayout.SOUTH);
		add(p2); //add panel to label
		start.addActionListener(this); //active button event here
		stop.addActionListener(this);
		clear.addActionListener(this);
	}
	class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			ck.second++;
			ck.setCurrentClock();
			jlb.setText(ck.hour+":"+ck.minute+":"+ck.second); //show the time
			ck.repaint();
		}
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==start){
			timer.start(); //start timer
		}
		if(e.getSource()==stop){
			timer.stop(); //stop timer
			ck.setCurrentClock();
			ck.repaint();
		}
		if(e.getSource()==clear){
			timer.stop();
			ck.clearClock(); //clear the clock
			jlb.setText(ck.hour+":"+ck.minute+":"+ck.second);
			ck.repaint();
		}
	}
}
