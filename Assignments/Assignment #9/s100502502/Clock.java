//Clock.java
package a9.s100502502;
import java.awt.*;
import javax.swing.*;
public class Clock extends JPanel{
	protected int hour, minute, second;
	Clock(){//initialize
		hour = 0;
		minute = 0;
		second = 0;
	}
	public void clearClock(){//clear clock
		hour = 0;
		minute = 0;
		second = 0;
	}
	public void setCurrentClock(){//set current time
		int temp = second;
		second = second % 60;
		minute = minute + temp / 60;
		hour = hour + minute / 60;
	}
	protected void paintComponent(Graphics g){//paint clock
		super.paintComponent(g);
		int radius = (int)(Math.min(getWidth(), getHeight())* 0.8 * 0.5);
		int xCenter = getWidth()/2;//circle's center x
		int yCenter = getHeight()/2;//circle's center y
		g.setFont(new Font("", Font.BOLD, 13));//set string's font
		g.drawOval(xCenter-radius, yCenter-radius, 2*radius, 2*radius);//draw circle
		g.drawString("12", xCenter-5, yCenter-radius+12);
		g.drawString("9", xCenter-radius+3, yCenter+5);
		g.drawString("3", xCenter+radius-10, yCenter+3);
		g.drawString("6", xCenter-3, yCenter+radius-3);
		int sLength = (int)(radius*0.8);
		int xLength = (int)(xCenter+sLength*Math.sin(second*(2*Math.PI/60)));
		int yLength = (int)(yCenter-sLength*Math.cos(second*(2*Math.PI/60)));
		g.drawLine(xCenter, yCenter, xLength, yLength);//draw second hand
	}
}
