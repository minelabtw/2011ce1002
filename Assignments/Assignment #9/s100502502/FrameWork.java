//FrameWork.java
package a9.s100502502;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class FrameWork extends JFrame implements ActionListener{
	//create buttons, clock, Label, font and timer 
	JButton start = new JButton("Start");
	JButton stop = new JButton("Stop");
	JButton clear = new JButton("Clear");
	Clock clock = new Clock();
	JLabel L2 = new JLabel();
	JPanel P2 = new JPanel(new GridLayout(1, 4, 5, 1));
	JPanel P3 = new JPanel(new BorderLayout());
	Timer timer = new Timer(1000, new TimerListener());
	Font font = new Font("Serif", Font.BOLD, 20);
	FrameWork(){//initialize and add buttons actionListener
		clock.clearClock();
		setLayout(new BorderLayout());
		L2.setFont(font);
		L2.setText(clock.hour + ":" + clock.minute + ":" + clock.second);
		L2.setHorizontalAlignment(JLabel.CENTER);
		P2.add(L2);
		P2.add(start);
		P2.add(stop);
		P2.add(clear);
		add(clock, BorderLayout.CENTER);
		add(P2, BorderLayout.SOUTH);
		start.addActionListener(this);
		stop.addActionListener(this);
		clear.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e){//when click the buttons
		if(e.getSource() == start){//start 
			timer.start();//start running
		}
		else if(e.getSource() == stop){//stop
			timer.stop();//stop running
			clock.setCurrentClock();//set current time
			clock.repaint();//repaint clock
		}
		else if(e.getSource() == clear){//stop and clear
			timer.stop();//stop running
			clock.clearClock();//clear time
			clock.repaint();//repaint clock
			L2.setText(clock.hour + ":" + clock.minute + ":" + clock.second);//reset text
		}
		else{}
	}
	class TimerListener implements ActionListener{//when clock is running
		public void actionPerformed(ActionEvent e){
			clock.second++;
			clock.setCurrentClock();//set current time
			L2.setText(clock.hour + ":" + clock.minute + ":" + clock.second);//reset text
			clock.repaint();//repaint clock
		}
	}
}
