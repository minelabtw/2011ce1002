package a9.s100502512;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class Clock extends JPanel {
	private int hour;
	private int minute;
	private int second;

	public Clock() {
		this.hour = 0;//初始值
		this.minute = 0;
		this.second = 0;
	}

	protected void paintComponent(Graphics g) {//畫時鐘,參考課本
		super.paintComponent(g);
		int r = (int) (Math.min(getWidth(), getHeight()) * 0.8 * 0.5);
		int x = getWidth() / 2;
		int y = getHeight() / 2;
		g.setFont(new Font("Gungsuh", Font.BOLD, 15));
		g.setColor(Color.white);
		g.fillOval(x - r, y - r, 2 * r, 2 * r);
		g.setColor(Color.black);
		g.drawString("12", x - 5, y - r + 12);
		g.drawString("9", x - r + 3, y + 5);
		g.drawString("3", x + r - 10, y + 3);
		g.drawString("6", x - 3, y + r - 3);
		int sLength = (int) (r * 0.8);
		int xSecond = (int) (x + sLength
				* Math.sin(second * (2 * Math.PI / 60)));
		int ySecond = (int) (y - sLength
				* Math.cos(second * (2 * Math.PI / 60)));
		g.setColor(Color.red);
		g.drawLine(x, y, xSecond, ySecond);
		int mLength = (int) (r * 0.65);
		int xMinute = (int) (x + mLength
				* Math.sin((minute % 60.0 + (second % 60 / 60.0))
						* (2 * Math.PI / 60)));
		int yMinute = (int) (y - mLength
				* Math.cos((minute % 60.0 + (second % 60 / 60.0))
						* (2 * Math.PI / 60)));
		g.setColor(Color.blue);
		g.drawLine(x, y, xMinute, yMinute);
		int hLength = (int) (r * 0.5);
		int xHour = (int) (x + hLength
				* Math.sin((hour % 12 + (minute % 60.0 / 60.0))
						* (2 * Math.PI / 12)));
		int yHour = (int) (y - hLength
				* Math.cos((hour % 12 + (minute % 60.0 / 60.0))
						* (2 * Math.PI / 12)));
		g.setColor(Color.green);
		g.drawLine(x, y, xHour, yHour);
	}

	public void setCurrentTime() {//設定時鐘怎麼跑
		this.second++;
		this.minute = second / 60;
		this.hour = second / 3600;
		repaint();
	}

	public void clearClock() {//清除時間,回歸初始值
		this.second = 0;
		this.minute = 0;
		this.hour = 0;
		repaint();
	}

	public Dimension getPreferredSize() {//時鐘的大小
		return new Dimension(400, 400);
	}
}
