package a9.s100502512;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.*;

import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener {
	private Clock clock = new Clock();//運用clock的class
	JButton Start = new JButton("Start");//設置按鈕
	JButton Stop = new JButton("Stop");
	JButton Clear = new JButton("Clear");

	public FrameWork() {
		JPanel p1 = new JPanel();//排版
		p1.add(clock);
		p1.setBackground(Color.black);
		JPanel p2 = new JPanel();
		p2.setLayout(new BorderLayout());
		p2.add(Start, BorderLayout.WEST);
		Start.setFont(new Font("serif", Font.BOLD, 25));
		p2.add(Stop, BorderLayout.CENTER);
		Stop.setFont(new Font("serif", Font.BOLD, 25));
		p2.add(Clear, BorderLayout.EAST);
		Clear.setFont(new Font("serif", Font.BOLD, 25));
		JPanel p3 = new JPanel();
		p3.setLayout(new BorderLayout());
		p3.add(p1, BorderLayout.NORTH);
		p3.add(p2, BorderLayout.CENTER);
		add(p3);
		Start.addActionListener(this);
		Stop.addActionListener(this);
		Clear.addActionListener(this);
	}
	Timer timer = new Timer(1000, new TimerListener());//使用timelistener
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == Start){
			timer.start();
		}
		if(e.getSource() == Stop){
			timer.stop();
		}
		if(e.getSource() == Clear){
			clock.clearClock();
		}
	}
	 class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			clock.setCurrentTime();//時鐘如何跑				
		}

	}

	

}
