package a9.s100502503;

import javax.swing.*;

import java.awt.*;

public class clock extends JPanel
{
	protected int second;
	protected int minute;
	protected int hour;
	
	//the constructor
	public clock()
	{
		second = 0;
		minute = 0;
		hour = 0;
	}
	
	//the function to clear the time
	public void clear()
	{
		second = 0;
		minute = 0;
		hour = 0;
	}
	
	//show out the current time
	public  void currentTime()
	{
		minute += second / 60;
		hour += minute / 60;
		second %= 60;
	}
	
	//draw the clock
	protected void paintComponent(Graphics g)
	{
		super.paintComponents(g);
		
		int clockRadius = (int)(Math.min(getWidth(), getHeight()) * 0.8 * 0.5);
		int xCenter = getWidth() / 2;
		int yCenter = getHeight() / 2;
		
		g.setColor(Color.WHITE);
		g.fillOval(xCenter - clockRadius,yCenter - clockRadius, clockRadius * 2, clockRadius * 2);
		
		g.setColor(Color.BLACK);
		g.drawOval(xCenter - clockRadius,yCenter - clockRadius, clockRadius * 2, clockRadius * 2);
	
		
		g.drawString("12", xCenter - 5, yCenter - clockRadius + 12);
		g.drawString("9", xCenter - clockRadius + 3, yCenter + 5);
		g.drawString("6", xCenter, yCenter + clockRadius -12);
		g.drawString("3", xCenter + clockRadius - 10, yCenter - 3);
		
		int secondL = (int)(clockRadius * 0.8);
		int xSecond = (int)(xCenter + secondL * Math.sin(second * (2 * Math.PI / 60)));
		int ySecond = (int)(yCenter - secondL * Math.cos(second * (2 * Math.PI / 60)));
		g.setColor(Color.RED);
		
		g.drawLine(xCenter, yCenter, xSecond, ySecond);

	}

}
