package a9.s100502503;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrameWork extends JFrame implements ActionListener
{
	//set members
	protected JButton start = new JButton("start");
	protected JButton stop = new JButton("stop");
	protected JButton clear = new JButton("clear");
	protected JPanel p2 = new JPanel(new GridLayout(1, 4, 5, 5));
	protected JLabel showTime = new JLabel("00:00:00");
	protected clock c = new clock();
	protected Timer timer = new Timer(1000, new TimerListener());
	
	//the constructor to set the panel and label
	public FrameWork()
	{
		p2.add(showTime);
		p2.add(start);
		p2.add(stop);
		p2.add(clear);
	
		add(c, BorderLayout.CENTER);
		add(p2, BorderLayout.SOUTH);
	
		start.addActionListener(this);
		stop.addActionListener(this);
		clear.addActionListener(this);
	}
	
	//set the action when user click the buttons
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == start)
			timer.start();
		
		else if(e.getSource() == stop)
		{
			timer.stop();
			c.currentTime();
			c.repaint();
		}
		
		else if(e.getSource() == clear)
		{
			timer.stop();
			c.clear();
			c.repaint();
			showTime.setText(c.hour + ":" + c.minute + ":" + c.second);
		}
		
		else;
	}
	
	//control the time pass
	class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			c.second++;
			c.currentTime();
			showTime.setText(c.hour + ":" + c.minute + ":" + c.second);
			c.repaint();
		}
	}
}