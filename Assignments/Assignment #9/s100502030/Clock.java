package a9.s100502030;

import java.awt.*;
import javax.swing.*;
import java.util.*;

public class Clock extends JPanel {
	private int s;
	private int hour;
	private int minute;
	private int second;

	public Clock() {
		hour = 0;
		minute = 0;
		second = 0;
	}

	public void clearClock() {
		s = 0;
	}
	
	public void timeMove() {
		s += 1;
	}

	public void setCurrentClock() {
		hour = s / 60 / 60 % 24;
		minute = s / 60 % 60;
		second = s % 60;
	}// set new time

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		int r = (int) (Math.min(getWidth(), getHeight()) * 0.8*0.5);
		int xCender = getWidth() / 2;
		int yCender = getHeight() / 2;

		g.setColor(Color.black);
		g.drawOval(xCender - r, yCender - r, 2 * r, 2 * r);


		int sLength = (int) (r * 0.8);
		int xScend = (int)(xCender+sLength*Math.sin(second*Math.PI/30*6));
		int yScend = (int)(yCender-sLength*Math.cos(second*Math.PI/30*6));
		g.setColor(Color.BLUE);
		g.drawLine(xCender, yCender, xScend, yScend);
	}// draw clock
	
	public int getHour() {
		return hour;
	}
	
	public int getMinute() {
		return minute;
	}
	
	public int getSecond() {
		return second;
	}
}
