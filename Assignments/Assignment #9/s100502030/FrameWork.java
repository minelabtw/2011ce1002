package a9.s100502030;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class FrameWork extends JFrame {
	private Clock clock = new Clock();
	private JButton start = new JButton("start");
	private JButton stop = new JButton("stop");
	private JButton clear = new JButton("clear");
	private JLabel screen = new JLabel("  00:00:00");
	private Timer t = new Timer(1000, new TimerListener());

	public FrameWork() {

		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(1, 4));
		p1.add(screen);
		p1.add(start);
		p1.add(stop);
		p1.add(clear);

		start.addActionListener(new ButtonListener());
		stop.addActionListener(new ButtonListener());
		clear.addActionListener(new ButtonListener());
		// add button event

		add(clock, BorderLayout.CENTER);
		add(p1, BorderLayout.SOUTH);
	}

	public String getTime() {
		String h = clock.getHour() + "";
		String m = clock.getMinute() + "";
		String s = clock.getSecond() + "";
		if (clock.getHour() < 10)
			h = "0" + h;
		if (clock.getMinute() < 10)
			m = "0" + m;
		if (clock.getSecond() < 10)
			s = "0" + s;

		return "  "+h + ":" + m + ":" + s;

	}// get time to show on screen

	private class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {// time event
			clock.timeMove();
			clock.setCurrentClock();
			screen.setText(getTime());
			clock.repaint();
		}
	}

	class ButtonListener implements ActionListener {// button event
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == start) {
				t.start();//time start
			} else if (e.getSource() == stop) {
				t.stop();// time stop
			} else if (e.getSource() == clear) {
				clock.clearClock();// clear time
				clock.setCurrentClock();
				screen.setText(getTime());
				t.stop();
				clock.repaint();
			}
		}
	}

}
