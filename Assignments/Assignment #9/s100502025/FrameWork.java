package a9.s100502025;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener{
	private JPanel p1 = new JPanel();  //存放按鈕的板子
	private JButton jbtStart = new JButton("start");  //按鈕start
	private JButton jbtStop = new JButton("stop");  //按鈕stop
	private JButton jbtClear = new JButton("clear");  //按鈕clear
	private JButton jbtCurrentTime = new JButton("current time");  //按鈕urrent time
	private Clock clock = new Clock();  
	private Timer timer1 = new Timer(1000 , new TimerListener1());  //每秒更新一次的timer
	private Timer timer2 = new Timer(1000 , new TimerListener2());  //每秒更新一次的timer
	
	public FrameWork() {		
		p1.setLayout(new GridLayout(1,4));  //將按鈕存入p1板子
		p1.add(jbtStart);
		p1.add(jbtStop);
		p1.add(jbtClear);
		p1.add(jbtCurrentTime);
				
		setLayout(new BorderLayout());  //將時鐘還有按鈕放進視窗
		add(clock, BorderLayout.CENTER);
		add(p1 , BorderLayout.SOUTH);
		
		jbtStart.addActionListener(this);  //讓各個按鈕有動作
		jbtStop.addActionListener(this);
		jbtClear.addActionListener(this);
		jbtCurrentTime.addActionListener(this);
		
	}
	
	class TimerListener1 implements ActionListener {  //碼表
		public void actionPerformed(ActionEvent e) {
			clock.setClock();
			clock.repaint();
		}
	}
	
	class TimerListener2 implements ActionListener {  //現在時間
		public void actionPerformed(ActionEvent e) {	
			clock.setCurrentClock();
			clock.repaint();
		}
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == jbtStart) {  //當按下start		
			timer1.start();  //開始計時
		}
		if(e.getSource() == jbtStop) {  //當按下stop
			timer1.stop();  //停止
		}
		if(e.getSource() == jbtClear) {  //當按下clear
			timer1.stop();  //先設定不再繼續動作
			clock.clearClock();  //在清除
			clock.repaint();
		}
		if(e.getSource() == jbtCurrentTime) {  //當按下current time
			timer2.start();  //現在的時刻
		}
	}
}
