package a9.s100502025;

import java.awt.*;
import javax.swing.*;
import java.util.*;

public class Clock extends JPanel {  //時鐘相關的設定
	private int hour;  //宣告一個變數hour代表時
	private int minute;  //宣告一個變數minute代表分
	private int second;  //宣告一個變數second代表秒
	
	public Clock() {
		
		hour = 0;  //設定初始值為0
		minute = 0;
		second = 0;
	}
	
	public void setClock() { 
		if( second < 59 ) {  //如果比59秒小的話
			second = second + 1;  //繼續+1秒
		}
		else {  //如果是60秒
			second = 0;  //將秒數回歸0秒
			if( minute < 59 ) {  //如果比59分小的話
				minute = minute + 1;  //繼續+1分
			}
			else {  //如果是60分
				minute = 0;  //將分回歸0分
				if( hour < 11 ) {  //如果比11時小的話
					hour = hour + 1;  //繼續+1時
				}
				else {  //如果是12時
					hour = 0;  //將時回歸0時
				}
			}			
		}			
	}
	
	public void clearClock() {
		hour = 0;  //把所有變數都變為0
		minute = 0;
		second = 0;
	}
	
	public void setCurrentClock() {  //系統中現在的時間
		Calendar calendar = new GregorianCalendar();
		
		this.hour = calendar.get(Calendar.HOUR_OF_DAY);
		this.minute = calendar.get(Calendar.MINUTE);
		this.second = calendar.get(Calendar.SECOND);
	}
	
	protected void paintComponent(Graphics g) {  //畫clock的圖
		super.paintComponent(g);
		int clockRadius = (int)( Math.min( getWidth() * 0.8 * 0.5 , getHeight() * 0.8 * 0.5 ) );
		int xCenter = getWidth() / 2;
		int yCenter = getHeight() / 2 - 20;
		g.setColor(Color.BLACK);
		g.drawOval(xCenter - clockRadius, yCenter - clockRadius , 2 * clockRadius , 2 * clockRadius );
		
		g.drawString("12", xCenter - 5 , yCenter - clockRadius + 12 );
		g.drawString("9", xCenter - clockRadius + 3 , yCenter + 5 );
		g.drawString("3", xCenter + clockRadius -10 , yCenter + 3 );
		g.drawString("6", xCenter - 3 , yCenter + clockRadius -3 );
		
		int sLength = (int)( clockRadius * 0.8 );
		int xSecond = (int)( xCenter + sLength * Math.sin( second * ( 2 * Math.PI / 60 ) ) );
		int ySecond = (int)( yCenter - sLength * Math.cos( second * ( 2 * Math.PI / 60 ) ) );
		g.setColor(Color.RED);
		g.drawLine( xCenter , yCenter , xSecond , ySecond );
		
		int mLength = (int)( clockRadius * 0.65 );
		int xMinute = (int)( xCenter + mLength * Math.sin( minute * ( 2 * Math.PI / 60 ) ) );
		int yMinute = (int)( yCenter - mLength * Math.cos( minute * ( 2 * Math.PI / 60 ) ) );
		g.setColor(Color.BLUE);
		g.drawLine( xCenter , yCenter , xMinute , yMinute );
		
		int hLength = (int)( clockRadius * 0.5 );
		int xHour = (int)( xCenter + hLength * Math.sin( ( hour % 12 + minute / 60.0 ) * ( 2 * Math.PI / 12 ) ) );
		int yHour = (int)( yCenter - hLength * Math.cos( ( hour % 12 + minute / 60.0 ) * ( 2 * Math.PI / 12 ) ) );
		g.setColor(Color.GREEN);
		g.drawLine( xCenter , yCenter , xHour , yHour );
		
	}
}
