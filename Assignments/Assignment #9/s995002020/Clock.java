package a9.s995002020;
	import java.awt.*;
	import java.awt.event.*;
	import javax.swing.*;
	public class Clock extends JFrame{
		private int deg = 0, counter = 0;
		private int ms = 0, s = 0, m = 0, h = 0, d = 0;
		private int mms = 0, ss = 0, mm = 0, hh = 0, dd = 0;
		private java.awt.image.BufferedImage buffer = null;
		JButton roll1Button, roll2Button, roll3Button, roll4Button;//four buttons
		static final String roll1Buttons = "roll1Buttons";
	    static final String roll2Buttons = "roll2Buttons";
		static final String roll3Buttons = "roll3Buttons";
		static final String roll4Buttons = "roll4Buttons";	
		public Clock(){//constructor
			super("小碼錶");
			Container container = getContentPane();
			container.setLayout( new FlowLayout() );
			
			roll1Button = new JButton( " 開始 " );
			//roll1Button.addActionListener( this );
			roll1Button.setActionCommand("roll1Buttons");
			roll1Button.setEnabled(true);
			container.add( roll1Button );
			
			roll2Button = new JButton( " 暫停 " );
			//roll2Button.addActionListener( this );
			roll2Button.setActionCommand("roll2Buttons");
			roll2Button.setEnabled(true);
			container.add( roll2Button );
			
			roll3Button = new JButton( "繼續" );
			//roll3Button.addActionListener( this );
			roll3Button.setActionCommand("roll3Buttons");
			roll3Button.setEnabled(true);
			container.add( roll3Button );
			
			roll4Button = new JButton( "歸零" );
			//roll4Button.addActionListener( this );
			roll4Button.setActionCommand("roll4Buttons");
			roll4Button.setEnabled(true);
			container.add( roll4Button );
			setSize(400, 400);
			buffer = new java.awt.image.BufferedImage(400, 400, java.awt.image.BufferedImage.TYPE_3BYTE_BGR);
			show();  	
		}
		/*private int rotaXms(int deg){
			int tmp =(int) (90*Math.cos(deg*Math.PI/50-Math.PI/2));
			return tmp;
		}
		private int rotaYms(int deg){
			int tmp = (int)(90*Math.sin(deg*Math.PI/50-Math.PI/2));
			return tmp;
		}*/
		private int rotaXs(int deg){
			int tmp =(int) (80*Math.cos(deg*Math.PI/30-Math.PI/2));
			return tmp;//3000
		}
		private int rotaYs(int deg){
			int tmp = (int)(80*Math.sin(deg*Math.PI/30-Math.PI/2));
			return tmp;
		}
		private int rotaXm(int deg){
			int tmp =(int)(70*Math.cos(deg*Math.PI/1800-Math.PI/2));
			return tmp;//180000
		}
		private int rotaYm(int deg){
			int tmp = (int)(70*Math.sin(deg*Math.PI/1800-Math.PI/2));
			return tmp;
		}
		private int rotaXh(int deg){
			int tmp =(int)(60*Math.cos(deg*Math.PI/21600-Math.PI/2));
			return tmp;//2160000
		}
		private int rotaYh(int deg){
			int tmp = (int)(60*Math.sin(deg*Math.PI/21600-Math.PI/2));
			return tmp;
		}
		public void paint(Graphics g){
			Graphics gb = buffer.getGraphics();//draw the second meter , minute meter ,and hour meter
			super.paint(gb);
			gb.setColor(Color.black);
			gb.setFont( new Font("Serif", Font.BOLD, 12) );
			gb.drawString( ""+ dd + d + "天"+ hh + h +" : " + mm + m + " : " + ss + s /*+ " : "+ mms + ms*/, 150, 350);
			gb.setColor(Color.white);
			gb.fillArc(100, 100, 200, 200, 0, 360);
			gb.setColor(Color.black);
			//gb.drawLine(200, 200, 200+rotaXms(deg), 200+rotaYms(deg));
			gb.setColor(Color.blue);
			gb.drawLine(200, 200, 200+rotaXs(deg), 200+rotaYs(deg));
			gb.setColor(Color.red);
			gb.drawLine(200, 200, 200+rotaXm(deg), 200+rotaYm(deg));
			gb.setColor(Color.green);
			gb.drawLine(200, 200, 200+rotaXh(deg), 200+rotaYh(deg));
			g.drawImage((java.awt.Image)buffer, 0, 0, 400, 400, Color.gray, this); 
		}
		public static void main(String[] arg){
			Clock a = new Clock();
			a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			//public void actionPerformed( ActionEvent actionEvent ){  
				//String s = actionEvent.getActionCommand();
				//if(s == "roll1Buttons"){
				while(true){
					try{
						Thread.sleep(1000);
					}
					catch(Exception e){
						System.err.println(e);
						System.exit(0);
					}
					a.deg = (a.deg+1)%43200;
					a.counter = (a.counter+1)%864000;
					//a.ms = (a.counter)%10;
					//a.mms = ((a.counter)/10)%10;
					a.s = (a.counter)%10;
					a.ss = ((a.counter)/10)%6;
					a.m = ((a.counter)/60)%10;
					a.mm = ((a.counter)/600)%6;
					a.h = ((a.counter)/3600)%10;
					a.hh = ((a.counter)/36000)%2;
					a.d = ((a.counter)/864000)%10;
					a.dd = ((a.counter)/864000)%10;
					a.repaint();
				}
				/*roll1Button.setEnabled(false);
				roll2Button.setEnabled(true);
				roll3Button.setEnabled(true);
				roll4Button.setEnabled(false);
			}*/
			
		}
	}

