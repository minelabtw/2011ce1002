package a9.s100502507;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.lang.Math;
import java.util.*;

public class Clock extends JPanel {
	Clock() {//Initializes variables
		hour = minute = second = 0;
	}
	
	public void clearClock() {//Sets time to 0
		hour = minute = second = 0;
	}
	
	public void setCurrentClock() {//Sets time to the system time
		Calendar c = new GregorianCalendar();
		this.hour = c.get(Calendar.HOUR_OF_DAY);
		this.minute = c.get(Calendar.MINUTE);
		this.second = c.get(Calendar.SECOND);
	}
	
	public void timekeeperType() {//Timekeeper mode
		second += 1;
		if(second>=60) {
			second = 0;
			minute += 1;
		}
		if(minute>=60) {
			minute = 0;
			hour += 1;
		}
		if(hour>=12) {
			hour = minute = second = 0;
		}
	}
	
	public int getHour() {//Return hour
		return hour;
	}
	
	public int getMinute() {//Return minute
		return minute;
	}
	
	public int getSecond() {//Return second
		return second;
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		int r = (int)(Math.min(getWidth(), getHeight()) * 0.8 * 0.5);
		int xCenter = getWidth()/2;
		int yCenter = getHeight()/2;
		
		g.drawOval(xCenter - r, yCenter - r, 2 * r, 2 * r);//Draws clock
		g.drawString("12", xCenter - 5, yCenter - r + 12);
		g.drawString("3", xCenter + r - 10, yCenter + 3);
		g.drawString("6", xCenter - 3, yCenter + r - 3);
		g.drawString("9", xCenter - r + 3, yCenter + 5);
		
		int r2 = (int)(r * 0.8);//Draws needles
		g.setColor(Color.RED);
		g.drawLine(xCenter, yCenter, (int)(xCenter + r2 * Math.sin(second * (2 * Math.PI / 60))), (int)(yCenter - r2 * Math.cos(second * (2 * Math.PI / 60))));//Second
		r2 = (int)(r * 0.65);
		g.setColor(Color.BLUE);
		g.drawLine(xCenter, yCenter, (int)(xCenter + r2 * Math.sin(minute * (2 * Math.PI / 60))), (int)(yCenter - r2 * Math.cos(minute * (2 * Math.PI / 60))));//Minute
		r2 = (int)(r * 0.5);
		g.setColor(Color.BLACK);
		g.drawLine(xCenter, yCenter, (int)(xCenter + r2 * Math.sin((hour % 12 + minute / 60.0) * (2 * Math.PI / 12))), (int)(yCenter - r2 * Math.cos((hour % 12 + minute / 60.0) * (2 * Math.PI / 12))));//Hour
	}
	
	private int hour, minute, second;
}
