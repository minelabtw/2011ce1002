package a9.s100502507;

import javax.swing.JFrame;

public class A9 {
	public static void main(String[] args) {
		FrameWork f = new FrameWork();
		f.setTitle("A9");
		f.setSize(550, 300);
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
		f.setResizable(false);
	}
}