package a9.s100502507;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener {
	FrameWork() {
		setLayout(new BorderLayout());
		JPanel p = new JPanel(new GridLayout(1, 5));//To display buttons and time
		clock = new Clock();//Initializes variables
		time = "00:00:00";
		screen = new JLabel(time);
		start = new JButton("Current time");
		stop = new JButton("Stop");
		clear = new JButton("Clear");
		timekeeper = new JButton("Timekeeper");
		t = new Timer(1000, this);
		
		start.addActionListener(this);
		stop.addActionListener(this);
		clear.addActionListener(this);
		timekeeper.addActionListener(this);
		
		p.add(screen);
		p.add(timekeeper);
		p.add(start);
		p.add(stop);
		p.add(clear);
		add(clock, BorderLayout.CENTER);
		add(p, BorderLayout.SOUTH);
		
		clockType = true;//First, set the clock to display the system time
	}
	
	public void actionPerformed(ActionEvent e) {//Catch the events
		if(e.getSource()==start) {//Button start clicked
			clockType = true;
			t.start();
		}
		else if(e.getSource()==stop) {//Button stop clicked
			t.stop();
		}
		else if(e.getSource()==clear) {//Button clear clicked
			time = "00:00:00";
			screen.setText(time);
			clock.clearClock();
			clock.repaint();
			t.stop();
		}
		else if(e.getSource()==timekeeper) {//Button timekeeper clicked 
			if(clockType==true) {
				clock.clearClock();
				time = "00:00:00";
			}
			clockType = false;
			clock.repaint();
			screen.setText(time);
			t.start();
		}
		else if(e.getSource()==t) {//It's time to change the flag
			if(clockType) {//Check the type of clock and executes appropriately
				clock.setCurrentClock();
			}
			else {
				clock.timekeeperType();
			}
			
			String hour, minute, second;//Three variable to hold the data of clock
			
			if(clock.getHour()<10) {//These judgement let the output can be 00:00:01 and 00:10:11, and so on
				hour = "0" + String.valueOf(clock.getHour());
			}
			else {
				hour = String.valueOf(clock.getHour());
			}
			if(clock.getMinute()<10) {
				minute = "0" + String.valueOf(clock.getMinute());
			}
			else {
				minute = String.valueOf(clock.getMinute());
			}
			if(clock.getSecond()<10) {
				second = "0" + String.valueOf(clock.getSecond());
			}
			else {
				second = String.valueOf(clock.getSecond());
			}
			
			time = String.valueOf(hour + ":" + minute + ":" + second);
			screen.setText(time);
			clock.repaint();
		}
		else {
			
		}
	}
	
	private String time;
	private Timer t;
	private Clock clock;
	private JLabel screen;
	private JButton start;
	private JButton stop;
	private JButton clear;
	private JButton timekeeper;
	private boolean clockType;
}
