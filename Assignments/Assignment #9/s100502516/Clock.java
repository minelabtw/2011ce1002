package a9.s100502516;

import javax.swing.*;
import java.awt.*;
import java.util.*;

public class Clock extends JPanel {
	private int hour = 0;
	private int minute = 0;
	private int second = 0;	

	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		int xCenter = getWidth()/ 2;
		int yCenter = getHeight() / 2;
		int radius = (int) (Math.min(getWidth(), getHeight()) * 0.4);
				
		g.setColor(Color.BLACK);//draw clock
		g.drawOval(xCenter - radius, yCenter - radius, radius * 2, radius * 2);
		g.drawString("12", xCenter - 5, (int) (yCenter - radius + 12));
		g.drawString("3", xCenter + radius - 10, yCenter + 3);	
		g.drawString("6", xCenter - 3, yCenter + radius - 3);
		g.drawString("9", xCenter - radius + 3, yCenter + 3);
		
		int sLength = (int) (radius * 0.8);
		int xSecond = (int) (xCenter + sLength * Math.sin(second * 2 * Math.PI / 60));
		int ySecond = (int) (yCenter - sLength * Math.cos(second * 2 * Math.PI / 60));
		
		g.setColor(Color.RED);//draw second hand
		g.drawLine(xCenter, yCenter, xSecond, ySecond);
		
		int mLength = (int) (radius * 0.8);
		int xMinute = (int) (xCenter + mLength * Math.sin(minute * 2 * Math.PI / 60));
		int yMinute = (int) (yCenter - mLength * Math.cos(minute * 2 * Math.PI / 60));
		
		g.setColor(Color.BLUE);//draw minute hand
		g.drawLine(xCenter, yCenter, xMinute, yMinute);
		
		int hLength = (int) (radius * 0.65);
		int xHour = (int) (xCenter + hLength * Math.sin(hour * 2 * Math.PI / 12));
		int yHour = (int) (yCenter - hLength * Math.cos(hour * 2 * Math.PI / 12));
		
		g.setColor(Color.GREEN);//draw hour hand
		g.drawLine(xCenter, yCenter, xHour, yHour);
	}	
	public void setCurrentTime()//add the time
	{
		Calendar calendar = new GregorianCalendar();
		
		hour = calendar.get(Calendar.HOUR_OF_DAY);
		minute = calendar.get(Calendar.MINUTE);
		second = calendar.get(Calendar.SECOND);
	}
	public void timing()
	{
		second++;
		
		if(second == 60)
		{
			second = 0;
			minute++;
		}
		
		if(minute == 60)
		{
			minute = 0;
			hour++;
		}
		
		if(hour == 12)
			hour = 0;
	}
	public void clearClock()//reset
	{
		second = 0;
		minute = 0;
		hour = 0;
	}
	public int getSecond()
	{
		return second;
	}
	public int getMinute()
	{
		return minute;
	}
	public int getHour()
	{
		return hour;
	}
}
