package a9.s100502516;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class FrameWork extends JFrame {
	private JButton jbtStart = new JButton("Start");//to timing
	private JButton jbtStop = new JButton("Stop");
	private JButton jbtClear = new JButton("Clear");
	private JButton jbtAccelerate = new JButton("Accelerate");//speed up
	private JButton jbtCurrentTime = new JButton("CurrentTime");
	private JLabel jlelTime = new JLabel("00:00:00");
	private Timer timer = new Timer(1000, new TimerListener());
	private Clock clock = new Clock();
	private boolean accelerateFlag = false;//check is it accelerating
	private boolean changeFlag = false;//check is it change to timing
	
	public FrameWork()
	{		
		jbtStart.addActionListener(new ButtonListener());
		jbtStop.addActionListener(new ButtonListener());
		jbtClear.addActionListener(new ButtonListener());		
		jbtAccelerate.addActionListener(new ButtonListener());
		jbtCurrentTime.addActionListener(new ButtonListener());
		
		JPanel p = new JPanel(new GridLayout(1,6));
		p.add(jlelTime);
		p.add(jbtCurrentTime);
		p.add(jbtStart);
		p.add(jbtStop);
		p.add(jbtClear);
		p.add(jbtAccelerate);
		
		setLayout(new BorderLayout());
		
		add(clock, BorderLayout.CENTER);
		add(p, BorderLayout.SOUTH);
		
		clock.setCurrentTime();
		updateTimeLabel(jlelTime, clock);
		timer.start();
	}
	public void updateTimeLabel(JLabel jlelTime, Clock clock)//update label
	{
		String temp = "";
		
		if(String.valueOf(clock.getHour()).length() == 1)			
			temp += "0" + String.valueOf(clock.getHour()) + ":";
		else
			temp += String.valueOf(clock.getHour()) + ":";
		
		if(String.valueOf(clock.getMinute()).length() == 1)
			temp += "0" + String.valueOf(clock.getMinute()) + ":";
		else
			temp += String.valueOf(clock.getMinute()) + ":";
		
		if(String.valueOf(clock.getSecond()).length() == 1)
			temp += "0" + String.valueOf(clock.getSecond());
		else
			temp += String.valueOf(clock.getSecond());
		
		jlelTime.setText(temp);
	}
	
	class ButtonListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(e.getSource() == jbtCurrentTime)
			{
				changeFlag = false;
				clock.setCurrentTime();//to update the screen immediately
				updateTimeLabel(jlelTime, clock);
				clock.repaint();
				timer.start();
			}
			else if(e.getSource() == jbtStart)
			{
				if(!changeFlag)
					clock.clearClock();
				
				changeFlag = true;				
				clock.repaint();//to update the screen immediately
				updateTimeLabel(jlelTime, clock);
				timer.start();
			}
			else if(e.getSource() == jbtStop)
				timer.stop();
			else if(e.getSource() == jbtClear)
			{
				clock.clearClock();
				updateTimeLabel(jlelTime, clock);
				clock.repaint();//to update the screen immediately
			}
			else if(e.getSource() == jbtAccelerate)
			{
				if(!accelerateFlag)//speed up
				{
					accelerateFlag = true;
					timer.setDelay(1);
					jbtAccelerate.setText("Revert");
				}
				else if(accelerateFlag)//speed down
				{
					accelerateFlag = false;
					timer.setDelay(1000);
					jbtAccelerate.setText("Accelerate");
				}					
			}
		}
	}
	
	class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(!changeFlag)
				clock.setCurrentTime();
			else if(changeFlag)
				clock.timing();
			
			updateTimeLabel(jlelTime, clock);
			
			clock.repaint();
		}
	}
}