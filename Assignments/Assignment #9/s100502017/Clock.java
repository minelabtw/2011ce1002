package a9.s100502017;
import javax.swing.*;
import java.awt.*;

public class Clock extends JPanel{
	protected int hour,minute,second;
	public Clock(){//constructor to initial the data
		clearClock();
	}
	public void clearClock(){//to reset data 
		hour=0;
		minute=0;
		second=0;
	}
	public void setCurrentClock(int h,int m,int s){//set time
		hour=h;
		minute=m;
		second=s;
	}
	public void paintComponent(Graphics g){//draw the clock and second hand 
		super.paintComponent(g);
		double w=getWidth();
		double h=getHeight();
		double s_d=second;
		g.drawString("12",(int)(w*(0.49)),(int)(h*(0.15)));
		g.drawString("3",(int)(w*(0.85)),(int)(h*(0.51)));
		g.drawString("6",(int)(w*(0.49)),(int)(h*(0.86)));
		g.drawString("9",(int)(w*(0.14)),(int)(h*(0.51)));
		g.drawOval((int)(w*(0.1)),(int)(h*(0.1)),(int)(w*(0.8)),(int)(h*(0.8)));
		g.setColor(Color.RED);
		g.drawLine((int)(w*(0.5)),(int)(h*(0.5)),(int)(w*(0.5)+w*(0.35)*Math.sin(2*(s_d*Math.PI/60))),(int)(h*(0.5)+(-1)*h*(0.35)*Math.cos(2*(s_d*Math.PI/60))));
	}
}
