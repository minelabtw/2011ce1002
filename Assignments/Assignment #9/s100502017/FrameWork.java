package a9.s100502017;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrameWork extends JFrame implements ActionListener{
	JButton start=new JButton("start");
	JButton stop=new JButton("stop");
	JButton clear=new JButton("clear");
	JLabel screen=new JLabel();
	JPanel p=new JPanel();
	Clock clock=new Clock();
	Timer t=new Timer(1000,new TimerListener());//use Timer class and let program run by time
	public FrameWork(){//constructor to layout panel
		p.setLayout(new GridLayout(1,4));
		setscreen();
		p.add(screen);
		p.add(start);
		p.add(stop);
		p.add(clear);
		add(clock,BorderLayout.CENTER);
		add(p,BorderLayout.SOUTH);
		start.addActionListener(this);
		stop.addActionListener(this);
		clear.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e){//button event is here
		if(e.getSource()==start){
			t.start();
		}
		else if(e.getSource()==stop){
			t.stop();
		}
		else if(e.getSource()==clear){//reset all data
			t.stop();
			clock.clearClock();
			repaint();
			setscreen();
		}
	}
	public void setscreen(){//set screen's time
		screen.setText(String.valueOf(clock.hour)+":"+String.valueOf(clock.minute)+":"+String.valueOf(clock.second));
	}
	class TimerListener implements ActionListener{//time event is here
		public void actionPerformed(ActionEvent e){
			int s=clock.second;
			int m=clock.minute;
			int h=clock.hour;
			s++;
			if(s>59){//time's rule
				s=0;
				m++;
				if(m>59){
					m=0;
					h++;
				}
			}
			clock.setCurrentClock(h,m,s);
			repaint();
			setscreen();
		}
	}
}
	
