package a9.s100502017;

import javax.swing.JFrame;

public class A91 {
	public static void main(String [] args){
		FrameWork f = new FrameWork();
		f.setTitle("Asignment 10"); //Frame title
		f.setSize(600,625);   //Frame size
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}