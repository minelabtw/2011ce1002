package a9.s100502545;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.*;

public class Clock extends JPanel
{
	private int hour = 0;
	private int minute = 0;
	private int second = 0;
	
	public Clock()
	{
		
	}
	
	public void clearClock()
	{//讓 時:分:秒變回0
		hour=0;
		minute=0;
		second=0;
		
	}
	
	public void setCurrentClock()
	{
		second++;
		//判斷當 到60秒時 ,分鐘要+1/到60分鐘,小時要加1
		if(second==60)
		{
			minute++;
			second=0;
		}
		else if (minute==60)
		{
			hour++;
			minute=0;
		}
	}

	public int getHour()
	{
		return hour;
	}
	
	
	public int getMinute()
	{
		return minute;
	}
	
	
	public int getSecond()
	{
		return second;
	}
	
	//Draw the clock
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		//initialize clock parameters
		int clockRadius = (int)(Math.min(getWidth(), getHeight()*0.8*0.5));
		
		int xCenter = getWidth() / 2;
		int yCenter = getHeight() / 2;
		
		//Draw circle
		g.setColor(Color.BLACK);
		g.setFont(new Font("Arial",Font.BOLD,24));
		g.drawOval(xCenter - clockRadius, yCenter - clockRadius,2 * clockRadius,2 * clockRadius);
		g.drawString("12",xCenter - 11, yCenter - clockRadius + 20);
		g.drawString("9",xCenter - clockRadius + 3,yCenter + 5);
		g.drawString("3",xCenter + clockRadius-12,yCenter + 6);
		g.drawString("6", xCenter - 3, yCenter + clockRadius - 3);
		//draw second hand
		int sLength = (int)(clockRadius * 0.8);
		int xSecond = (int)(xCenter + sLength * Math.sin(second*(2*Math.PI/60)));
		int ySecond = (int)(yCenter - sLength * Math.cos(second*(2*Math.PI/60)));
		g.setColor(Color.red);
		g.drawLine(xCenter, yCenter, xSecond, ySecond);
		
		
	
	}

	
	

	
}
