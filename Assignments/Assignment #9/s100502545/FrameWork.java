package a9.s100502545;


import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class FrameWork extends JFrame implements ActionListener
{
	
	private JButton jbtStart = new JButton("Start");
	private JButton jbtStop = new JButton("Stop");
	private JButton jbtClear = new JButton("Clear");
	private JLabel  Screen = new JLabel("0:0:0");
	private JPanel p2 = new JPanel();
	private Clock p1 = new Clock();
	private Timer timer ;
	
	public FrameWork()
	{
		setLayout(new BorderLayout());
		add(p1,BorderLayout.CENTER);
		p2.add(jbtStart);
		p2.add(jbtStop);
		p2.add(jbtClear);
		p2.add(Screen);
		jbtStart.addActionListener(this);
		jbtStop.addActionListener(this);
		jbtClear.addActionListener(this);
		add(p2,BorderLayout.SOUTH);
		timer = new Timer(100, new TimerListener());
		
	}
	
	//處理按鈕事件 停止.開始,清除
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()==jbtStop)
		{
			timer.stop();
		}
		else if(e.getSource()==jbtStart)
		{
			timer.start();
		}
		else if(e.getSource()==jbtClear)
		{
			timer.stop();
			p1.clearClock();
			repaint();
			Screen.setText("0:0:0");
			
		}
	}
	//處理顯示 時:分:秒 ==>0:0:0
	class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			p1.setCurrentClock();
			p1.repaint();
			Screen.setText(String.valueOf(p1.getHour())+":"+p1.getMinute()+":"+p1.getSecond());
		}
	}
}
