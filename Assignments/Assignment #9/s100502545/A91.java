package a9.s100502545;

import javax.swing.*;

public class A91 extends JFrame
{
	public static void main(String[] args)
	{
		FrameWork f = new FrameWork();
		f.setTitle("#A9"); 
		f.setSize(500, 500);   
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
