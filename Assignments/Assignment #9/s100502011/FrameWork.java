package a9.s100502011;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class FrameWork extends JFrame implements ActionListener{
	private JButton start = new JButton("start");
	private JButton stop = new JButton("stop"); 
	private JButton clear = new JButton("clear");
	private JPanel timep = new JPanel();
	private JPanel function = new JPanel();
	private JPanel total = new JPanel();
	protected JLabel screen = new JLabel();
	Clock clock = new Clock();  // object
	Timer timer = new Timer(1000,new TimerListener());
	Font font = new Font("Sarif",Font.BOLD,50);
 	public FrameWork(){ // constructor
 		
 		//add to panel
		timep.setLayout(new GridLayout(1,1)); 
		timep.add(screen);
		function.setLayout(new GridLayout(1,3));
		function.add(start);
		function.add(stop);
		function.add(clear);
		start.addActionListener(this);
		start.setFont(font);
		stop.addActionListener(this);
		stop.setFont(font);
		clear.addActionListener(this);
		clear.setFont(font);
		total.setLayout(new GridLayout(3,1));
		total.add(clock);
		total.add(timep);
		total.add(function);
		
		//add to frame
		add(total);
	}
 	
 	public void actionPerformed(ActionEvent e){ // button's action
		if(e.getSource()==start){ // when click start button
			timer.start();
		}
		if(e.getSource()==stop){ // when click stop button
			timer.stop();
		}
		if(e.getSource()==clear){ // when click clear button
			clock.clearClock();
			repaint(); // repaint the picture
			screen.setText("                     00:00:0"+(clock.second)); // show time
		}
 	} 
 	
	class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			repaint();
			screen.setFont(font); // set font
			if(clock.second<=9){
				screen.setText("                     00:0"+clock.minute+":0"+(clock.second));
			}
			else{
				screen.setText("                     00:0"+clock.minute+":"+(clock.second));
			}
			if(clock.second>=60){ // second change to minute
				clock.minute++;
				clock.second -=60;
				screen.setText("                     00:0"+clock.minute+":0"+(clock.second));
			}
			if(clock.minute>=60){ // minute change to hour
				clock.hour++;
				clock.minute -=60;
				screen.setText("                     00:0"+clock.minute+":0"+(clock.second));
			}
		}
	}
}
