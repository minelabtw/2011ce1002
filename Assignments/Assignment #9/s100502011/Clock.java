package a9.s100502011;

import javax.swing.*;
import java.awt.*;
import java.util.*;
public class Clock extends JPanel{

	private int secondx; // second's x position
	private int secondy; // second's y position
	private int count =0; //counter
	protected int second=0;	
	protected int hour=0;
	protected int minute=0;
	public Clock(){ // constructor
		
	}
	
	protected void paintComponent(Graphics g){ // paint circle
		super.paintComponent(g);	
		int clockRadius = 100; // radius
		int centerx = getWidth()/2; // center of picture
		int centery = getHeight()/2; 
		int handlength =80; // length of second'hand
		
		// draw clock
		g.drawOval(centerx-clockRadius,centery-clockRadius,2*clockRadius,2*clockRadius);
		g.drawString("12",centerx-5,centery-clockRadius+12);
		g.drawString("9",centerx-clockRadius+3,centery+5);
		g.drawString("3",centerx+clockRadius-10,centery+3);
		g.drawString("6",centerx-3,centery+clockRadius-3);
		secondx = (int) (centerx + handlength*Math.sin(second*(Math.PI/30)));
		secondy = (int) (centery- handlength*Math.cos(second*Math.PI/30));
		g.drawLine(centerx, centery, secondx,secondy);
		
		count++; // run time
		if(count>2){ // start to count second
			second++;
		}
	} //end function
	
	public void setCurrentTime(){ // to  set current time
		Calendar calendar = new GregorianCalendar();
		hour=calendar.get(Calendar.HOUR_OF_DAY);
		minute =calendar.get(Calendar.MINUTE);
		second = calendar.get(Calendar.SECOND);
	} // end function
	
	public void clearClock(){ // to clear the time and picture
		hour=0;
		second=0;
		minute=0;
		count=2;
	} // end function
}
