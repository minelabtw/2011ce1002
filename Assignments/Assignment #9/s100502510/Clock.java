package a9.s100502510;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JPanel;

public class Clock extends JPanel {
	int hour = 0;
	int minute = 0;
	int second = 0;
	Font number = new Font("serif", Font.BOLD, 50);

	public void clearClock() {
		hour = 0;
		minute = 0;
		second = 0;
	}

	public void setCurrentClock() {
		second++;
		if (second == 60) {// 60秒時秒數應歸零,分鐘數加一
			minute += 1;
			second = 0;
		} else if (minute == 60) {// 同上的概念
			hour += 1;
			minute = 0;
		}

	}

	public int gethour() {
		return hour;
	}

	public int getsecond() {
		return second;
	}

	public int getminute() {
		return minute;
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		int r = (int) (Math.min(getWidth(), getHeight()) * 0.8 * 0.5);// 時鐘的半徑
		int xCenter = getWidth() / 2;
		int yCenter = getHeight() / 2;
		this.setFont(number);

		g.setColor(Color.black);// 畫時鐘的框以及數字
		g.drawOval(xCenter - r, yCenter - r, 2 * r, 2 * r);
		g.drawString("12", xCenter - 20, yCenter - r + 40);
		g.drawString("9", xCenter - r + 3, yCenter + 15);
		g.drawString("3", xCenter + r - 25, yCenter + 15);
		g.drawString("6", xCenter - 10, yCenter + r - 3);

		int secondlength = (int) (r * 0.8);// 畫秒針
		int xSecond = (int) (xCenter + secondlength
				* Math.sin(second * (2 * Math.PI / 60)));
		int ySecond = (int) (yCenter - secondlength
				* Math.cos(second * (2 * Math.PI / 60)));
		g.setColor(Color.green);
		g.drawLine(xCenter, yCenter, xSecond, ySecond);

		int minutelength = (int) (r * 0.65);// 畫分針
		int xMinute = (int) (xCenter + minutelength
				* Math.sin(minute * (2 * Math.PI / 60)));
		int yMinute = (int) (yCenter - minutelength
				* Math.cos(minute * (2 * Math.PI / 60)));
		g.setColor(Color.blue);
		g.drawLine(xCenter, yCenter, xMinute, yMinute);

		int hourlength = (int) (r * 0.5);// 畫時針
		int xHour = (int) (xCenter + hourlength
				* Math.sin((hour % 12 + minute / 60.0) * (2 * Math.PI / 12)));
		int yHour = (int) (yCenter - hourlength
				* Math.cos((hour % 12 + minute / 60.0) * (2 * Math.PI / 12)));
		g.setColor(Color.black);
		g.drawLine(xCenter, yCenter, xHour, yHour);
	}
}
