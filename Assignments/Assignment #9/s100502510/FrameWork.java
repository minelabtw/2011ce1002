package a9.s100502510;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener {
	JButton start = new JButton("Start");
	JButton stop = new JButton("Stop");
	JButton clear = new JButton("Clear");
	Font button = new Font("serif", Font.BOLD, 50);
	Font Display = new Font("serif", Font.BOLD, 75);
	JPanel buttons = new JPanel();
	Clock clock = new Clock();
	String display = clock.gethour() + "   :   " + clock.getminute()
			+ "   :   " + clock.getsecond();// 用來顯示目前的時間
	JLabel screen = new JLabel(display);
	Timer timer = new Timer(1000, new TimerListener());
	int flag = 1;// 使時鐘跑的驅動

	

	public FrameWork() {
		screen.setFont(Display);// 排版
		start.setFont(button);
		stop.setFont(button);
		clear.setFont(button);
		buttons.setLayout(new BorderLayout());
		buttons.add(start, BorderLayout.WEST);
		buttons.add(stop, BorderLayout.CENTER);
		buttons.add(clear, BorderLayout.EAST);
		this.setLayout(new BorderLayout());
		this.add(screen, BorderLayout.NORTH);
		this.add(clock, BorderLayout.CENTER);
		this.add(buttons, BorderLayout.SOUTH);
		start.addActionListener(this);
		stop.addActionListener(this);
		clear.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == start) {
			timer.start();
			flag = 1;
		} else if (e.getSource() == stop) {
			timer.stop();
		} else if (e.getSource() == clear) {
			clock.clearClock();// 歸零
			display = "";// 使顯示出的時間歸零
			display = display + clock.gethour() + "   :   " + clock.getminute()
					+ "   :   " + clock.getsecond();
			screen.setText(display);
			repaint();
		}
	}

	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			if (flag == 1) {// 使時間一直跑
				clock.setCurrentClock();
			}
			display = "";
			display = display + clock.gethour() + "   :   " + clock.getminute()
					+ "   :   " + clock.getsecond();
			screen.setText(display);
			repaint();
		}
	}
}
