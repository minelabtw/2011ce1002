package a9.s100502519;
import javax.swing.*;
import java.awt.*;

public class Clock extends JPanel{
	
	private int second;
	private int minute;
	private int hour;
	private String s = null;			//string化的second
	private String m = null;			//string化的minute
	private String h = null;			//string化的hour
	
	public Clock(){
		setCurrentClock();			//目前時間狀態
	}
	
	public Clock(int hour,int minute,int second){
		this.hour = hour;
		this.minute = minute;
		this.second = second;
	}
	
	public int getHour(){
		return hour;
	}
	
	public void setHour(int hour){
		this.hour = hour;
		repaint();
	}
	
	public int getMinute(){
		return minute;
	}
	
	public void setMinute(int minute){
		this.minute = minute;
		repaint();
	}
	
	public int getSecond(){
		return second;
	}
	
	public void setSecond(int second){
		this.second = second;
		repaint();
	}
	
	public void clearClock(){
		this.second = 0;			//歸零
		this.minute = 0;
		this.hour = 0;
		setCurrentClock();			//馬上再重set
	}
	
	public void setCurrentClock(){			/*set目前的時間狀態*/
		setSecond(second);
		setMinute(minute);
		setHour(hour);
		
		s =  Integer.toString(getSecond());			//string化的second
		m =  Integer.toString(getMinute());			//string化的minute
		h =  Integer.toString(getHour());			//string化的hour
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		
		int xCenter = getWidth()/2;			//圓心X座標
		int yCenter = getHeight()/2;			//元新Y座標
		int clockRadius = (int)(Math.min(getWidth(), getHeight())*0.8*0.5);			//圓半徑
		
		/*底圓*/
		Color color1 = new Color(87,44,0);
		g.setColor(color1);
		g.fillOval(xCenter-clockRadius-5, yCenter-clockRadius-5, clockRadius*2+10, clockRadius*2+10);
		
		/*前圓*/
		Color color2 = new Color(206,206,206);
		g.setColor(color2);
		g.fillOval(xCenter-clockRadius, yCenter-clockRadius, clockRadius*2, clockRadius*2);
		
		/*秒針*/
		int sLength = (int)(clockRadius*0.8);
		int xSecond = (int)(xCenter+sLength*Math.sin(second*(2*Math.PI/60)));
		int ySecond = (int)(yCenter-sLength*Math.cos(second*(2*Math.PI/60)));
		Color color3 = new Color(185,216,10);
		g.setColor(color3);
		g.drawLine(xCenter, yCenter, xSecond, ySecond);
		
		/*數字*/
		Color color4 = new Color(0,6,62);
		g.setColor(color4);
		g.drawString("0s",xCenter-5,yCenter-clockRadius+12);
		g.drawString("45s",xCenter-clockRadius+3,yCenter+5);
		g.drawString("30s",xCenter-3,yCenter+clockRadius-3);
		g.drawString("15s",xCenter+clockRadius-22,yCenter+3);
		g.drawString("60", xCenter-5, yCenter-clockRadius-5);
		
		/*screen*/
		JLabel screen = new JLabel();
		screen = FrameWork.getScreen();
		screen.setText(h + ":" + m + ":" + s);
		
		/*進位判斷*/
		if(second < 59){
			second++;
		}
		else if(second == 59){
			second=0;
			
			if(minute < 59){
				minute++;
			}
			else if(minute == 59){
				minute=0;
				hour++;
			}
		}
	}
}
