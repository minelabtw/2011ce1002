package a9.s100502519;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrameWork extends JFrame implements ActionListener{
	
	private JButton button_start = new JButton("Start");			//一些按鍵
	private JButton button_stop = new JButton("Stop");
	private JButton button_clear = new JButton("Clear");
	private static JLabel label_screen = new JLabel();
	private Clock clock = new Clock();
	private JPanel buttonArea = new JPanel();
	private Color color1 = new Color(255,179,255);
	private Color color2 = new Color(39,168,36);
	
	Timer timer = new Timer(1000,new TimerListener());			//建一個timer
	
	private class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			clock.setCurrentClock();			//目前的time
			clock.repaint();			//重畫
		}
	}
	
	public static JLabel getScreen(){
		return label_screen;			//給Clock用
	}
	
	public FrameWork(){
		setLayout(new BorderLayout(1,1));			//frame的排版法
		buttonArea.setLayout(new GridLayout(1, 4, 5, 5));			//按鍵區塊的排版法
		
		button_start.setBackground(color1);			//設按鈕背景色
		button_stop.setBackground(color1);
		button_clear.setBackground(color1);
		button_start.setForeground(color2);			//設按鈕前景色
		button_stop.setForeground(color2);
		button_clear.setForeground(color2);
		buttonArea.add(button_start);			//加按鈕到按鍵區塊
		buttonArea.add(button_stop);
		buttonArea.add(button_clear);
		buttonArea.add(label_screen);
		
		add(clock,BorderLayout.CENTER);			//加時鐘到frame
		add(buttonArea,BorderLayout.SOUTH);			//加按鍵區塊到frame
		
		button_start.addActionListener(this);			//加listener給按鍵們
		button_stop.addActionListener(this);
		button_clear.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == button_start){	
			timer.start();			//時間轉動
		}
		
		if(e.getSource() == button_stop){	
			timer.stop();			//時間停止
		}
		
		if(e.getSource() == button_clear){	
			clock.clearClock();			//call清除
			timer.stop();			//時間停止
			repaint();			//馬上重畫
		}
	}
}
