package a9.s100502002;
import java.awt.*;
import javax.swing.*;
import java.util.*;
public class Clock extends JPanel
{
	private int second;
	private int hour;
	private int minute;
	public Clock()//initialize clock
	{
		second = 0;
		hour = 0;
		minute = 0;
	}
	public void clearClock()//turn time to 0
	{
		second = 0;
		hour = 0;
		minute = 0;
		repaint();
	}
	public void setCurrentClock()//second = second +1 after 1 second
	{
		second = second +1;
		if(second>=60)
			minute = (int)(second/60);
		if(minute>=60)
			hour = (int)(minute/60);
		repaint();
		
	}
	public int getsecond()//to get second
	{
		if(second<60)
			return second;
		else
			return second%60;
	}
	
	public int getminute()//to get minute
	{
		if(second>=60)
			return (int)(second/60);
		else
			return 0;
	}
	
	public int gethour()//to get hour
	{
		if(minute>=60)
			return (int)(minute/60);
		else
			return 0;
	}
	
	protected void paintComponent(Graphics g)//畫圖
	{
		super.paintComponents(g);
		
		int radius = (int)(Math.min(getWidth(), getHeight())*0.8*0.5);
		int x = getWidth()/2;
		int y = getHeight()/2;
		
		g.setColor(Color.black);
		g.drawOval(x-radius, y-radius, 2*radius, 2*radius);
		g.drawString("12", x-5, y-radius+12);
		g.drawString("9", x-radius+3, y+5);
		g.drawString("3", x+radius-10, y+5);
		g.drawString("6", x-3, y+radius-12);//畫鐘面
		
		int sl = (int)(radius*0.8);
		int sx = (int)(x+sl*Math.sin(second*(2*Math.PI/60)));
		int sy = (int)(y-sl*Math.cos(second*(2*Math.PI/60)));
		g.setColor(Color.red);
		g.drawLine(x, y, sx, sy);//秒針
		
		int ml = (int)(radius*0.65);
		int mx = (int)(x+ml*Math.sin(minute*(2*Math.PI/60)));
		int my = (int)(y-ml*Math.cos(minute*(2*Math.PI/60)));
		g.setColor(Color.blue);
		g.drawLine(x, y, mx, my);//分針
		
		int hl = (int)(radius*0.5);
		int hx = (int)(x+hl*Math.sin((hour%12+minute/60.0)*(2*Math.PI/12)));
		int hy = (int)(y-hl*Math.cos((hour%12+minute/60.0)*(2*Math.PI/12)));
		g.setColor(Color.green);
		g.drawLine(x, y, hx, hy);//時針
	}
	
}
