package a9.s100502002;
import java.awt.BorderLayout;
import java.awt.event.*;
import javax.swing.*;

public class FrameWork extends JFrame 
{
	Timer timer = new Timer(1000,new T());//create timer
	private Clock c = new Clock();
	private JButton stop = new JButton("stop");
	private JButton clear = new JButton("clear");
	private JButton start = new JButton("start");
	private JLabel l = new JLabel("");
	private JPanel p3 = new JPanel();//create every element
	public FrameWork()
	{
		
	    
		p3.add(l);
		p3.add(start);
		p3.add(clear);
		p3.add(stop);
		p3.add(l);
		
		add(c,BorderLayout.CENTER);
		add(p3,BorderLayout.SOUTH);//add something to screen
		
		
		buttonListener li = new buttonListener();
		stop.addActionListener(li);
		clear.addActionListener(li);
		start.addActionListener(li);//actionlistener
		
		
	}
	
	public class T implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			c.setCurrentClock();
			l.setText(c.gethour()+":"+c.getminute()+":"+c.getsecond());
			repaint();
			
		}//show current time
	}
	public class buttonListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			
			
			if(e.getSource() == start)
			{
				
				timer.start();//start timer
				
			}
			if(e.getSource() == stop)
			{
			
				timer.stop();//stop timer
			}
			if(e.getSource() == clear)
			{
				c.clearClock();
				repaint();
				l.setText("0:0:0");//clear and turn time to 0
			}
	
			
			
		}
	}
}

