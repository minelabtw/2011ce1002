package a9.s100502544;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class clock extends JPanel{
	private int hour=0;
	private int min=0;
	private int second=0;//three private int hour min second
	public clock(){
		
	}
	public int gethour(){
		return hour;
	}
	public int getmin(){
		return min;
	}
	public int getsecond(){
		return second;
	}
	public void clearClock(){
		hour=0;//清除記錄  重設為0
		min=0;
		second=0;
	}
	public void setCurrentClock(){
		second++;//秒數+1
		
		if(second==60){//60秒轉一分  在從0開始
			min++;
			second=0;
		}
		else if(min==60){//60分為1小時  在從0開始
			hour++;
			min=0;
		}
		
	}
	public void paintComponent(Graphics g){//畫圖
		
		super.paintComponent(g);
		int clockRadius=(int)(Math.min(getWidth(), getHeight())*0.8*0.5);
		int xCenter=getWidth()/2;
		int yCenter=getHeight()/2;
		//畫圓
		g.drawOval(xCenter-clockRadius, yCenter-clockRadius, 2*clockRadius, 2*clockRadius);
		g.drawString("12", xCenter-5, yCenter-clockRadius+12);
		g.drawString("9", xCenter-clockRadius+3, yCenter+5);
		g.drawString("3", xCenter+clockRadius-10, yCenter+3);
		g.drawString("6", xCenter-3, yCenter+clockRadius-3);
		int sLength=(int)(clockRadius*0.8);
		int xsecond=(int)(xCenter+sLength*Math.sin(second*(2*Math.PI/60)));
		int ysecond=(int)(yCenter-sLength*Math.cos(second*(2*Math.PI/60)));
		g.drawLine(xCenter, yCenter, xsecond, ysecond);//畫秒針
		
	}
}
