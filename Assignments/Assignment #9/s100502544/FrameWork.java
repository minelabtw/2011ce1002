package a9.s100502544;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

public class FrameWork extends JFrame implements ActionListener{
	private JButton start=new JButton("start");//宣告三個button
	private JButton stop=new JButton("stop");
	private JButton clear=new JButton("clear");
	private JLabel screen=new JLabel("00:00:00");//宣告一個JLabel
	private clock p1=new clock();//宣告class clock panel=p1
	private JPanel p2=new JPanel();
	private Timer timer;//宣告timer constructor
	Border lineborder=new LineBorder(Color.BLACK,3);//宣告粗框
	public FrameWork(){
		setLayout(new BorderLayout());
		add(p1,BorderLayout.CENTER);
		p1.setBorder(lineborder);
		p2.setBorder(lineborder);
		p2.add(screen);
		p2.add(start);
		start.addActionListener(this);
		p2.add(stop);
		stop.addActionListener(this);
		p2.add(clear);
		clear.addActionListener(this);
		add(p2,BorderLayout.SOUTH);
		timer=new Timer(1000, new TimerListener());
		//設定一秒走一格
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == start){
			timer.start();//按到start button 開始走
		}
		else if(e.getSource()==stop){
			timer.stop();//按到stop button 停
		}
		else if(e.getSource()==clear){
			//如果按到clear button 把p2 panel的 Jlable screen秒數重設00:00:00
			screen.setText("00:00:00");
			timer.stop();//先停 再歸0
			p1.clearClock();
			p1.repaint();//重畫
		}
	}
	class TimerListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			p1.repaint();//每一秒重畫一次
			p1.setCurrentClock();//
			screen.setText(String.valueOf(p1.gethour())+":"+String.valueOf(p1.getmin())+":"+String.valueOf(p1.getsecond()));
			//把hour min second 轉為字串  再顯示在 screen上
		}
		
	}
}
