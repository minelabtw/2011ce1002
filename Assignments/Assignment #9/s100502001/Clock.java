package a9.s100502001;
import java.awt.*;

import javax.swing.*;
import java.util.*;
public class Clock extends JPanel{
	private int hour=0; //store the current time 
	private int minute=0;
	protected int second=0;

	public Clock(){ 
		
	}
	public void clearClock(){ //delete the current time of count
		this.hour=0;
		this.minute=0;
		this.second=0;
	}
	public void setCurrentClock(){ //reset the clock
		//construct a calendar for the current date and time
		Calendar calendar=new GregorianCalendar();
		this.hour=calendar.get(Calendar.HOUR_OF_DAY);
		this.minute=calendar.get(Calendar.MINUTE);
		this.second=calendar.get(Calendar.SECOND);
	}

	public void setHour(int H){
		this.hour=H;
	}
	public void setMinute(int M){
		this.minute=M;
	}
	public void setSecond(int S){
		this.second=S;
	}
	public int getHour(){
		return this.hour;
	}
	public int getMinute(){
		return this.minute;
	}
	public int getSecond(){
		return this.second;
	}
	protected void paintComponent(Graphics g){ //draw the clock
		super.paintComponent(g);
		int ClockRadius=(int)(Math.min(getWidth(), getHeight())*0.8*0.5); //取時鐘的半徑
		int xCenter=getWidth()/2;
		int yCenter=getHeight()/2;
		g.setColor(Color.darkGray); //時鐘外框的顏色
		g.drawOval(xCenter-ClockRadius,yCenter-ClockRadius , ClockRadius*2, ClockRadius*2);
		g.setColor(Color.orange);
		g.drawString("12", xCenter, yCenter-ClockRadius);
		g.drawString("3", xCenter+ClockRadius, yCenter);
		g.drawString("6", xCenter, yCenter+ClockRadius);
		g.drawString("9", xCenter-ClockRadius, yCenter);
		g.setColor(Color.yellow); //時鐘中心點
		g.fillOval(xCenter-(int)(getHeight()*0.03)/2, yCenter-(int)(getHeight()*0.03)/2, (int)(getHeight()*0.03), (int)(getHeight()*0.03));
		int LSecond=(int)(ClockRadius*0.85);//秒針
		int xSecond=(int)(xCenter+LSecond*Math.sin((Math.PI*2/60)*this.second)); //秒針每次所在的X位置
		int ySecond=(int)(yCenter-LSecond*Math.cos((Math.PI*2/60)*this.second)); //秒針每次所在的Y位置
		
		g.setColor(Color.red);
		g.drawLine(xCenter, yCenter, xSecond, ySecond);
		g.drawLine(0, (int)(getHeight()/2), (int)(getWidth()*0.1), (int)(getHeight()/2));
		g.drawString("秒針", (int)(getWidth()*0.1), (int)(getHeight()/2));
		
		int LMinute=(int)(ClockRadius*0.6);//分針
		int xMinute=(int)(xCenter+LMinute*Math.sin((Math.PI*2/60)*this.minute));
		int yMinute=(int)(yCenter-LMinute*Math.cos((Math.PI*2/60)*this.minute));
		g.setColor(Color.green);
		g.drawLine(xCenter, yCenter, xMinute, yMinute);
		g.drawLine(0, (int)((getHeight()/2)*1.2), (int)(getWidth()*0.07), (int)((getHeight()/2)*1.2));
		g.drawString("分針", (int)(getWidth()*0.07), (int)((getHeight()/2)*1.2));
		
		int LHour=(int)(ClockRadius*0.4); //時針
		int xHour=(int)(xCenter+LHour*Math.sin((Math.PI*2/60)*this.hour));
		int yHour=(int)(yCenter-LHour*Math.cos((Math.PI*2/60)*this.hour));
		g.setColor(Color.blue);
		g.drawLine(xCenter, yCenter, xHour, yHour);
		g.drawLine(0, (int)((getHeight()/2)*1.4), (int)(getWidth()*0.04), (int)((getHeight()/2)*1.4));
		g.drawString("時針", (int)(getWidth()*0.04), (int)((getHeight()/2)*1.4));
	}
}
