package a9.s100502001;
import java.awt.*;

import javax.swing.*;
import java.awt.event.*;
public class FrameWork extends JFrame{
	protected Clock clock=new Clock();
	protected JButton start;
	protected JButton stop;
	protected JButton clear;
	protected JButton current_time;
	protected JLabel show_time;
	protected int sec=0; //計時器的秒數
	protected Timer mileometer= new Timer(1000, new Mileometer());//每次頻率相關，現在時間
	protected Timer cur_time= new Timer(1000, new TimerListner());//計時器秒針的頻率
	public FrameWork() {
		show_time=new JLabel("哈哈");
		show_time.setFont(new Font("Serif",Font.BOLD,30));
		JPanel choice=new JPanel();
		choice.setLayout(new GridLayout(1,4));
		setLayout(new BorderLayout()); //選項放置在最下方
		start=new JButton("Start");
		stop=new JButton("Stop");
		clear=new JButton("Clear");
		current_time=new JButton("current_time");
		choice.add(start);
		choice.add(stop);
		choice.add(clear);
		choice.add(current_time);
		add(choice,BorderLayout.SOUTH);
		add(show_time,BorderLayout.NORTH);
		add(clock);
		ItemOfChoose choose=new ItemOfChoose(); //當按下這幾個button時，觸發事件
		start.addActionListener(choose);
		stop.addActionListener(choose);
		clear.addActionListener(choose);
		current_time.addActionListener(choose);
	}
	class ItemOfChoose implements ActionListener{ //選擇4種按鍵的情況	
		public void actionPerformed(ActionEvent e){
			if(e.getSource()==start){
				cur_time.stop();
				mileometer.start();		
				show_time.setText("Wait for a moment!!");
			}
			else if(e.getSource()==stop){
				mileometer.stop();//把計時器給(Timer)給stop
				cur_time.stop();//把目前時間(Timer)給stop
				show_time.setText("計時器總計 : "+Integer.toString(clock.getSecond())+" 秒 " );
				
			}
			else if(e.getSource()==clear){
				clock.clearClock();
				sec=0; //將碼表秒數歸零
				clock.repaint();
			}
			else if(e.getSource()==current_time){
				sec=0;//將碼表秒數歸零
				mileometer.stop();
				cur_time.start();
			}

		}
	}
	class TimerListner implements ActionListener{//顯示當下時間
		public void actionPerformed(ActionEvent e){
			clock.setCurrentClock();//隨著timer的頻率而改變每次時間值
			String time=Integer.toString(clock.getHour())+" 時 "+
						Integer.toString(clock.getMinute())+" 分 "+
						Integer.toString(clock.getSecond())+" 秒 ";
			show_time.setText("The current time   "+time);		
			clock.repaint(); //因應每1秒圖就重新被repaint一次
		}
	}
	class Mileometer implements ActionListener{ //計時器實作
		public void actionPerformed(ActionEvent e){	
			clock.setHour(0); //時針與分針指向12的位置
			clock.setMinute(0);
			clock.setSecond(sec++); //秒數每次+1
			show_time.setText("Count time "+clock.getSecond()+" 秒");
			clock.repaint(); //因應每1秒圖就重新被repaint一次
		}
	}
}