package a9.s100502010;

import java.awt.Graphics;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JPanel;

public class Clock extends JPanel
{
	public int hour;
	public int minute;
	public int second;
	public double arc=0;
	public int count=0;
	
	public void clearClock()//reset time	
	{
		hour=0;
		minute=0;
		second=0;
		arc=0;
		count=0;
		repaint();
	}
	public void setCurrentClock()//set the current time
	{
		Calendar calendar=new GregorianCalendar();
		hour=calendar.get(Calendar.HOUR_OF_DAY);
		minute=calendar.get(Calendar.MINUTE);
		second=calendar.get(Calendar.SECOND);
	}
	protected void paintComponent(Graphics g)//draw the clock and second hand
	{
		if(count==0)
		{
			count++;
		}
		else if(count!=0)
		{
			arc=arc+(2*Math.PI)/60;
		}
		int xcenter=135;
		int ycenter=95;
		int handlong=60;
		int x=xcenter;
		int y=ycenter-75;
		super.paintComponent(g);
		
		g.drawOval(60,20,150,150);
		g.drawLine(xcenter, ycenter, (int)(xcenter+handlong*Math.sin(arc)),(int)(ycenter-handlong*Math.cos(arc)));
		g.drawString("12", xcenter-5, ycenter-65);
		g.drawString("3", xcenter+70, ycenter);
		g.drawString("6", xcenter, ycenter+75);
		g.drawString("9", xcenter-70, ycenter);
	}
}
