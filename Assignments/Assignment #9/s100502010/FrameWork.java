package a9.s100502010;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.*;
import javax.swing.*;


public class FrameWork extends JFrame implements ActionListener
{
	private JButton start=new JButton("start"); //add buttons
	private JButton stop=new JButton("stop");
	private JButton clear=new JButton("clear");
	Timer timer=new Timer(1000, new TimerListener());   //timer
	Clock clock=new Clock();
	private JLabel now=new JLabel();  //jlabel used to print current time
	
	public FrameWork()
	{
		JPanel p1=new JPanel();
		JPanel p2=new JPanel();
		p2.setLayout(new GridLayout(1,3,10,10));
		p2.add(start);
		p2.add(stop);
		p2.add(clear);
		p1.add(now);
		setLayout(new GridLayout(3,1,10,10));
		add(clock);
		add(p1);
		add(p2);
		start.addActionListener(this);
		stop.addActionListener(this);
		clear.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource()==start) //button start
		{
			timer.start();
		}
		else if(e.getSource()==stop)  //button stop
		{
			timer.stop();
		}
		else if(e.getSource()==clear)  //button clear
		{
			clock.clearClock();
		}
	}
	class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)//
		{
			clock.setCurrentClock();
			now.setText(clock.hour+" hour "+clock.minute+" minute "+clock.second+" second ");
			repaint();
		}
	}
}
