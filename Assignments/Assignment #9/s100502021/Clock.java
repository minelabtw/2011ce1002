package a9.s100502021;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.*;
import java.util.*;
import javax.swing.Timer;
import javax.swing.*;
public class Clock extends JPanel{
	protected int hour; //�ŧi
	protected int min;
	protected int sec;
	int xsec;
	int ysec;
	int sec2=0;
	public Clock(){
		
	}
	
	public void clearClock(){
		sec=0;
		sec2=0;
	}
	public void setCurrentClock(){
		sec+=1;
		sec2++; 
	}
	public int gethour(){
		return sec2/3600;
	}
	public int getmin(){
		return sec2/60;
	}
	public int getsec(){
		return sec2%60;
	}
	public void paintComponent(Graphics g){ //���X����
		super.paintComponent(g);
		int r=(int)(Math.min(getWidth(),getHeight())*0.8*0.5);
		int x=getWidth()/2;
		int y=getHeight()/2;
		g.setColor(Color.BLACK);
		g.drawOval(x-r, y-r,2*r,2*r);
		g.drawString("12", x-5, y-r+12);
		g.drawString("9", x-r+3, y+5);
		g.drawString("3", x+r-10, y+3);
		g.drawString("6", x-3, y+r-3);
		int sLength=(int)(r*0.8);
		xsec=(int)(x+sLength*Math.sin(sec*(2*Math.PI/60)));
		ysec=(int)(y-sLength*Math.cos(sec*(2*Math.PI/60)));
		g.setColor(Color.red);
		g.drawLine(x, y, xsec, ysec);
	}
	public Dimension getPreferredSize(){
		return new Dimension(400,400);
	}
}
