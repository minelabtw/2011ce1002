package a9.s100502021;
import java.awt.BorderLayout;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.Timer;
public class FrameWork extends JFrame implements ActionListener{	
	private Clock clock=new Clock();
	private JPanel panel1 = new JPanel(); //新增底層
	private JPanel panel2 = new JPanel();
	private JLabel time=new JLabel(Integer.toString(clock.gethour())+":"+Integer.toString(clock.getmin())+":"+Integer.toString(clock.getsec())); //設定時間顯示
	private JButton bstart = new JButton("start"); //設定按鈕
	private JButton bstop = new JButton("stop");
	private JButton breset = new JButton("reset");	
	Timer timer=new Timer(1000,new TimerListener());	
	public FrameWork(){
		panel2.add(time);
		panel2.add(bstart);
		panel2.add(bstop);
		panel2.add(breset);
		panel1.add(clock);
		add(panel1,BorderLayout.NORTH);
		add(panel2,BorderLayout.SOUTH);
		bstart.addActionListener(this);
		bstop.addActionListener(this);
		breset.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==breset){	
			clock.clearClock();
			time.setText(Integer.toString(clock.gethour())+":"+Integer.toString(clock.getmin())+":"+Integer.toString(clock.getsec()));
			repaint();
			timer.stop();
		}
		else if(e.getSource()==bstart){		
			timer.start();
		}
		else if(e.getSource()==bstop){
			timer.stop();
		}
	}
	private class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			clock.setCurrentClock();
			time.setText(Integer.toString(clock.gethour())+":"+Integer.toString(clock.getmin())+":"+Integer.toString(clock.getsec()));
			repaint();
		}
	}	
}
