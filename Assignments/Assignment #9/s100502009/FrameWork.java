package a9.s100502009;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

public class FrameWork extends JFrame
{
	Clock clock=new Clock();
	JButton start=new JButton("start");//set three buttons
	JButton stop=new JButton("stop");
	JButton clear=new JButton("clear");
	Timer timer = new Timer(1000,new TimerListener()); 
	JLabel messagePanel=new JLabel(clock.hour+":"+clock.minute+":"+clock.second);
	FrameWork()
	{						
		JPanel p2=new JPanel(new GridLayout(1,4,20,20));
		p2.add(messagePanel);
		p2.add(start);
		p2.add(stop);
		p2.add(clear);
		add(clock,BorderLayout.CENTER);
		add(p2,BorderLayout.SOUTH);
		ButtonListener listener=new ButtonListener();
		start.addActionListener(listener );
		stop.addActionListener(listener);
		clear.addActionListener(listener);
	}
	class ButtonListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(e.getSource()==start)
			{
				timer.start();//start to run  when the button is pressed
			}
			else if(e.getSource()==stop)
			{
				timer.stop();//stop when the button is pressed
			}
			else if(e.getSource()==clear)
			{
				clock.clearClock();//clear the clock when the button is pressed
				messagePanel.setText(clock.hour+":"+clock.minute+":"+clock.second);
				repaint();
			}
		}
	}
	
	class TimerListener implements ActionListener//repaint the clock
	{
		public void actionPerformed(ActionEvent e)
		{			
			clock.setCurrentClock();//run the clock
			messagePanel.setText(clock.hour+":"+clock.minute+":"+clock.second);
			repaint();
		
		}
	}
}