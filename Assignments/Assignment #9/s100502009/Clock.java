package a9.s100502009;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

public class Clock extends JPanel
{
	int hour=0;
	int minute=0;
	int second=0;
	
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		int radius=150;
		int xCenter=getWidth()/2;
		int yCenter=getHeight()/3;
		
		g.setColor(Color.BLUE);//draw the clock
		g.drawOval(xCenter-radius, yCenter-radius, 2*radius, 2*radius);
		g.setColor(Color.BLACK);
		g.drawString("12", xCenter-6, yCenter-radius+12);
		g.drawString("9", xCenter-radius+3, yCenter+3);
		g.drawString("3", xCenter+radius-10, yCenter+3);
		g.drawString("6", xCenter-3, yCenter+radius-3);
		
		int sLength=(int)(radius*0.8);//draw the second hand
		int xSecond=(int)(xCenter+sLength*Math.sin(second*(2*Math.PI/60)));
		int ySecond=(int)(yCenter-sLength*Math.cos(second*(2*Math.PI/60)));
		g.setColor(Color.RED);
		g.drawLine(xCenter, yCenter, xSecond, ySecond);

	}
	
	public void setCurrentClock()
	{		
		this.second++;//run the clock
		if(this.second==60)
		{
			this.minute++;
			this.second=0;
		}
		if(this.minute==60)
		{
			this.hour++;
			this.minute=0;
		}
	}
	
	public void clearClock()//method to clear the clock
	{
		this.hour=0;
		this.minute=0;
		this.second=0;
	}
}