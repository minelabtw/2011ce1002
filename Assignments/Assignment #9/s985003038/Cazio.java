package a9.s985003038;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JPanel;
import javax.swing.Timer;

public class Cazio extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private Calendar calendar = new GregorianCalendar();
	private Timer timer;									// create a timer to control both the clock and the stopwatch
	private Clock clock;									// create a clock, the bigger clock in the middle, to show the current time
	private Clock stopwatch;								// create another clock, the smaller clock at the left hand side, to implement the requirement of this assignment
	private boolean startFlag;								// a flag to control the stopwatch, set the flag to be ON to start the stopwatch, and OFF to stop it. We need this flag since we only use one timer to control both the clock and the stopwatch
	private int today;										// the date of today, using to show at the right hand side of the framework 
	
	public Cazio(){											// constructor
		clock = new Clock();								// create the clock
		stopwatch = new Clock(0, 0, 0);						// create the stopwatch
		timer = new Timer(1000, this);						// set the timer to send a event after each seconds
		timer.start();										// start the timer
		today = calendar.get(Calendar.DATE);				// set the date of today
	}
	
	public void actionPerformed(ActionEvent e){				// when the timer send a event
		clock.tick();										// make the clock tick
		if(startFlag) stopwatch.tick();						// if the stopwatch is started, make the stopwatch tick also
		if(clock.getHour() == 0 && clock.getMinute() == 0 && clock.getSecond() == 0)
			today = calendar.get(Calendar.DATE);			// if the clock is 24:00:00, the next day is come, so we update the date
		repaint();											// update the graphic of the framework
    }
	
	public Timer getTimer(){								// get the timer
		return timer;
	}
	
	public Clock getClock(){								// get the clock
		return clock;
	}
	
	public Clock getStopwatch(){							// get the stopwatch
		return stopwatch;
	}
	
	public boolean getStopwatchFlag(){						// get the stopwatch flag
		return startFlag;
	}
	
	public void startStopwatch(){							// start the stopwatch, that is, setting ON the flag
		startFlag = true;
	}
	
	public void stopStopwatch(){							// stop the stopwatch
		startFlag = false;
	}
	
	public void resetStopwatch(){							// reset the stopwatch
		startFlag = false;									// to do so, first we clear the flag
		stopwatch.clearClock();								// and also, reset the time of the stopwatch
		repaint();											// finally, we need to repaint immediately so we can see the change
	}
	
	public void adjustTime(){								// adjust the time, if the time of the clock is not correct
		clock.setCurrentClock();
	}
															// input an Clock object and draw it out on the screen
	private void drawClock(Graphics g, Clock clock, int clockRadius, int xCenter, int yCenter){
		g.setColor(Color.black);							// draw the outer circle and write the marks on the edge
		g.drawOval(xCenter - clockRadius, yCenter - clockRadius, 2 * clockRadius, 2 * clockRadius);
		g.drawString("12", xCenter - 5, yCenter - clockRadius + 12);
		g.drawString("9", xCenter - clockRadius + 3, yCenter + 5);
		g.drawString("3", xCenter + clockRadius - 10, yCenter + 3);
		g.drawString("6", xCenter - 3, yCenter + clockRadius - 3);

		int sLength = (int)(clockRadius * 0.8);				// draw the second hand
		int xSecond = (int)(xCenter + sLength * Math.sin(clock.getSecond() * (2 * Math.PI / 60)));
		int ySecond = (int)(yCenter - sLength * Math.cos(clock.getSecond() * (2 * Math.PI / 60)));
		g.setColor(Color.red);
		g.drawLine(xCenter, yCenter, xSecond, ySecond);

		int mLength = (int)(clockRadius * 0.65);			// draw the minute hand
		int xMinute = (int)(xCenter + mLength * Math.sin(clock.getMinute() * (2 * Math.PI / 60)));
		int yMinute = (int)(yCenter - mLength * Math.cos(clock.getMinute() * (2 * Math.PI / 60)));
		g.setColor(Color.blue);
		g.drawLine(xCenter, yCenter, xMinute, yMinute);

		int hLength = (int)(clockRadius * 0.5);				// draw the hour hand
		int xHour = (int)(xCenter + hLength * Math.sin((clock.getHour() % 12 + clock.getMinute() / 60.0) * (2 * Math.PI / 12)));
		int yHour = (int)(yCenter - hLength * Math.cos((clock.getHour() % 12 + clock.getMinute() / 60.0) * (2 * Math.PI / 12)));
		g.setColor(Color.green);
		g.drawLine(xCenter, yCenter, xHour, yHour);
	}
	
	protected void paintComponent(Graphics g){
		super.paintComponent(g);							// draw the clocks and write the date
		g.drawRect((int)(getWidth() / 1.5), (int)(getHeight() / 2 - getHeight() * 0.05 / 2), (int)(getWidth() * 0.08), (int)(getHeight() * 0.05));
		g.drawString(String.valueOf(today), (int)(getWidth() * 0.7), (int)(getHeight() / 2 + 3));
		drawClock(g, clock, (int)(Math.min(getWidth(), getHeight()) * 0.8 * 0.5), getWidth() / 2, getHeight() / 2);
		drawClock(g, stopwatch, (int)(Math.min(getWidth(), getHeight()) * 0.25 * 0.5), getWidth() / 3, getHeight() / 2);
		g.setColor(Color.GRAY);
		g.setFont(new Font("Arial", Font.BOLD, 20));
		g.drawString("Cazio", (int)(getWidth() / 2 - 25), (int)(getHeight() * 0.3));
	}
}
