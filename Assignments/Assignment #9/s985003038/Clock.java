package a9.s985003038;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Clock {
	private int hour;
	private int minute;
	private int second;

	public Clock(){											// constructor
		setCurrentClock();									// set the current time
	}

	public Clock(int hour, int minute, int second){			// constructor
		setTime(hour, minute, second);						// set the specified time
	}

	public String getTime(){								// get the time as a string
		DecimalFormat format = new DecimalFormat("00");
		return format.format(getHour()) + ":" + format.format(getMinute()) + ":" + format.format(getSecond());
	}
	
	public void setTime(int hour, int minute, int second){	// method to set the specified time
		this.hour = hour;
		this.minute = minute;
		this.second = second;
	}
	
	public int getHour(){									// get the hour
		return hour;
	}

	public void setHour(int hour){							// set the hour
		this.hour = hour;
	}

	public int getMinute(){									// get the minute
		return minute;
	}

	public void setMinute(int minute){						// set the minute
		this.minute = minute;
	}

	public int getSecond(){									// get the second
		return second;
	}

	public void setSecond(int second){						// set the second
		this.second = second;
	}

	public void setCurrentClock(){							// method to set the current time
		Calendar calendar = new GregorianCalendar();
		hour = calendar.get(Calendar.HOUR_OF_DAY);
		minute = calendar.get(Calendar.MINUTE);
		second = calendar.get(Calendar.SECOND);
	}
	
	public void clearClock(){								// reset the clock to be 00:00:00
		hour = 0;
		minute = 0;
		second = 0;
	}
	
	public void tick(){										// method to implement the clock tick
		second++;
		if(second >= 60){
			second = 0;
			minute++;
			if(minute >= 60){
				minute = 0;
				hour++;
				if(hour >= 24)
					hour = 0;
			}
		}
	}
}