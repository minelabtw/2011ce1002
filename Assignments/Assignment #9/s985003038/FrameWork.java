package a9.s985003038;

import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;

import javax.swing.*;
import javax.swing.border.*;

public class FrameWork extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	private JLabel stopwatchLabel;								// a label to show the time on the stopwatch
	private JButton start, stop, reset, adjust;					// four button to control the clocks
	private Cazio myCazio = new Cazio();						// create the object which include the clocks
	private Timer timer;										// create a timer to do the synchronization

	public FrameWork(){
		setTitle("Cazio");										// set the frame properties
		setSize(600, 600);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout(1,2));
		add(myCazio, BorderLayout.CENTER);						// add the clock panel on the frame
		
		JPanel controlPanel = new JPanel();
		controlPanel.setLayout(new GridLayout(1,4));
		
		stopwatchLabel = new JLabel("00:00:00");				// add the label on the frame
		stopwatchLabel.setOpaque(true);
		stopwatchLabel.setForeground(Color.blue);
		stopwatchLabel.setFont(new Font("Arial", Font.BOLD, 15));
		stopwatchLabel.setHorizontalAlignment(JLabel.CENTER);
		stopwatchLabel.setBorder(BorderFactory.createCompoundBorder(new LineBorder(Color.black, 1), (Border)BorderFactory.createEmptyBorder(10,20,10,20)));
		controlPanel.add(stopwatchLabel);
		
		start = new JButton("Start");							// add the buttons on the frame
		stop = new JButton("Stop");
		reset = new JButton("Reset");
		adjust = new JButton("Adjust");
		controlPanel.add(start);
		controlPanel.add(stop);
		controlPanel.add(reset);
		controlPanel.add(adjust);
		add(controlPanel, BorderLayout.SOUTH);
		
		start.addActionListener(this);							// add event handler to the buttons
		stop.addActionListener(this);
		reset.addActionListener(this);
		adjust.addActionListener(this);
		
		timer = myCazio.getTimer();								// get the timer from the panel
		timer.addActionListener(this);							// add timer ticking event handler to the timer
	}
	
	public void actionPerformed(ActionEvent e){					// event handler of the buttons and timer
		if(e.getSource() == timer){								// if it is a timer ticking event
			if(myCazio.getStopwatchFlag()){						// check if the stopwatch is started
				DecimalFormat format = new DecimalFormat("00");	// show the time on the label if stopwatch is started
				stopwatchLabel.setText(format.format(myCazio.getStopwatch().getHour()) + ":" + format.format(myCazio.getStopwatch().getMinute()) + ":" + format.format(myCazio.getStopwatch().getSecond() + 1));
			}
		} else if(e.getSource() == start){						// if start button is clicked, call the method to start the stopwatch
			myCazio.startStopwatch();
		} else if(e.getSource() == stop){						// if stop button is clicked, call the method to stop the stopwatch
			myCazio.stopStopwatch();
		} else if(e.getSource() == reset){						// if reset button is clicked, call the method to reset the stopwatch
			myCazio.resetStopwatch();							// and also reset the label on the frame
			stopwatchLabel.setText(myCazio.getStopwatch().getTime());
		} else if(e.getSource() == adjust){						// if adjust button is clicked, call the method to adjust the time of the clock
			myCazio.adjustTime();
		}
	}
}
