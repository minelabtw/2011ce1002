package a9.s100502020;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
public class FrameWork extends JFrame implements ActionListener{
	private Clock clock = new Clock();
	JButton clear = new JButton("clear"); 
	JButton start = new JButton("start");
	JButton stop = new JButton("stop");
	
	public FrameWork()//arrange the position
	{
		JPanel p1 = new JPanel();
		p1.add(clock);
		JPanel p2 = new JPanel();
		p2.setLayout(new BorderLayout());
		p2.add(clear , BorderLayout.WEST);
		start.setFont(new Font("Timesroman", Font.BOLD, 25));
		p2.add(start, BorderLayout.CENTER);
		stop.setFont(new Font("Timesroman", Font.BOLD, 25));
		p2.add(stop, BorderLayout.EAST);
		clear.setFont(new Font("Timesroman", Font.BOLD, 25));
		p2.add(p1, BorderLayout.NORTH);
		add(p2);
		clear.addActionListener(this);
		start.addActionListener(this);
		stop.addActionListener(this);
	}
	Timer T = new Timer(1000, new TimerListener());//use timerlistener
	public void actionPerformed(ActionEvent e) {//let the button react something
		if(e.getSource() == start){
			T.start();
		}
		if(e.getSource() == stop){
			T.stop();
		}
		if(e.getSource() == clear){
			clock.clearClock();
		}
	}
	 class TimerListener implements ActionListener 
	 {
		public void actionPerformed(ActionEvent e) 
		{
			clock.setCurrentTime();//�����p��]				
		}
	 }
}
