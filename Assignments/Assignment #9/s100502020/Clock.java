package a9.s100502020;
import java.awt.*;
import javax.swing.*;
import java.util.*;
public class Clock extends JPanel{
	private int hour;
	private int minute;
	private int second;
	
	public Clock()
	{
		hour = 0 ;
		minute = 0;
		second = 0;
	}
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		int x = getWidth()/2;
		int y = getHeight()/2;
		int radius = (int)(Math.min(getWidth(), getHeight()) * 0.8 * 0.5);
		
		
		
		g.setColor(Color.WHITE);
		g.fillOval(x - radius , y - radius , 2*radius , 2*radius);
		g.setColor(Color.black);
		g.drawString("12", x - 5 , y - radius + 12);//draw 12 3 6 9
		g.drawString("3", x + radius - 10 , y + 3);
		g.drawString("6", x - 3 , y + radius - 3);
		g.drawString("9", x - radius + 3 , y + 5);
		
		
		
		int sLength = (int)(radius*0.8);//set ���w
		int xSecond = (int)(x+sLength*Math.sin(second*(2*Math.PI/60)));
		int ySecond = (int)(y-sLength*Math.cos(second*(2*Math.PI/60)));
		g.setColor(Color.red);
		g.drawLine(x, y, xSecond, ySecond);
		
		
		int mLength = (int)(radius*0.65);//set ���w
		int xMinute = (int)(x+mLength*Math.sin((minute%60.0+(second%60/60.0))*(2*Math.PI/60)));
		int yMinute = (int)(y-mLength*Math.cos((minute%60.0+(second%60/60.0))*(2*Math.PI/60)));
		g.setColor(Color.blue);
		g.drawLine(x,y,xMinute,yMinute);
		
		
		int hLength = (int)(radius*0.5);//set �ɰw
		int xHour = (int)(x+hLength*Math.sin((hour%12+(minute%60.0/60.0))*(2*Math.PI/12)));
		int yHour = (int)(y-hLength*Math.cos((hour%12+(minute%60.0/60.0))*(2*Math.PI/12)));
		g.setColor(Color.green);
		g.drawLine(x,y,xHour,yHour);
				
	}
	
	public void setCurrentTime()//add
	{
		this.second++;
		this.minute = second/60;
		this.hour = second/3600;
		repaint();		
	}
	
	public void clearClock()//clear the clock
	{
		this.second = 0;
		this.minute = 0;
		this.hour = 0;
		repaint();		
	}
	
	public Dimension getPreferredSize() 
	{//size of the clock
		return new Dimension(700, 500);
	}
}
