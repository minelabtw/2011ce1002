package a9.s992001033;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FrameWork extends JFrame
{
	//各個元件
	JButton start = new JButton("start");
	JButton stop = new JButton("stop");
	JButton clear = new JButton("clear");
	JLabel screen = new JLabel(" ");
	Clock clock = new Clock();
	String display = " ";
	Timer timer = new Timer(1000, new TimerListener());
	int pmValue = 0;
	FrameWork()
	{
		add(clock);
		//下方panel
		JPanel below = new JPanel(new GridLayout(1,4));
		//按鍵的listener
		start.addActionListener(new ButtonListener());
		stop.addActionListener(new ButtonListener());
		clear.addActionListener(new ButtonListener());
		//加入元件
		below.add(screen);
		below.add(start);
		below.add(stop);
		below.add(clear);
		add(below,new BorderLayout().SOUTH);
		
		timer.start();
	}
	public class TimerListener implements ActionListener
	{
		//讓時間開始跑
		public void actionPerformed(ActionEvent e)
		{
			boolean pm = false;
			if(clock.getSecond()!=59)
				clock.setSecond(clock.getSecond()+1);
			else
			{
				clock.setSecond(0);
				if(clock.getMinute()!=59)
					clock.setMinute(clock.getMinute()+1);
				else
				{
					clock.setMinute(0);
					if(clock.getHour()!=11)
						clock.setHour(clock.getHour()+1);
					else
					{
						clock.setHour(0);
						//am、pm的問題
						pm = !pm;
						if(pm == true)
							pmValue = 1;
						else
							pmValue = 0;
					}
				}
			}
			//顯示在左下角的文字
			display = (clock.getHour()+pmValue*12)+":"+clock.getMinute()+":"+clock.getSecond();
			screen.setText(display);
			clock.repaint();
		}
	}
	//button的listener
	public class ButtonListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(e.getSource()== start)
				timer.start();
			else if(e.getSource()== stop)
				timer.stop();
			else if(e.getSource()== clear)
			{
				clock.clearClock();
				display = (clock.getHour()+pmValue*12)+":"+clock.getMinute()+":"+clock.getSecond();
				screen.setText(display);
			}
		}
	}
	
}
