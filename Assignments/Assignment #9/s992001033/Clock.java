package a9.s992001033;

import javax.swing.*;
import java.awt.*;
import java.util.*;
public class Clock extends JPanel
{
	//initial variable
	private int hour = 0;
	private int minute = 0;
	private int second = 0;
	//設為0
	public void clearClock()
	{
		hour = 0;
		minute = 0;
		second = 0;
	}
	//設為現在時間
	public void setCurrentClock()
	{
		Calendar calendar = new GregorianCalendar();
		
		this.hour = calendar.get(Calendar.HOUR_OF_DAY);
		this.minute = calendar.get(calendar.MINUTE);
		this.second = calendar.get(calendar.SECOND);
	}
	//畫圖形
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		//為了不超出panel所以*0.8
		int clockRadius =(int) (Math.min(getWidth(),getHeight())*0.8*0.5);
		int xCenter = getWidth()/2;
		int yCenter = getHeight()/2;
		//時鐘各數字的位置
		g.drawOval(xCenter-clockRadius, yCenter-clockRadius, 2*clockRadius, 2*clockRadius);
		g.drawString("12",xCenter-5, yCenter -clockRadius + 12);
		g.drawString("9",xCenter -clockRadius + 3, yCenter + 5);
		g.drawString("3",xCenter +clockRadius -10, yCenter + 3);
		g.drawString("6",xCenter - 3, yCenter + clockRadius -3);
		//設座標
		int sLength = (int) (clockRadius*0.8);
		int xSecond = (int) (xCenter + sLength *Math.sin(second*(2*Math.PI/60)));
		int ySecond = (int) (yCenter - sLength *Math.cos(second*(2*Math.PI/60)));
		
		g.drawLine(xCenter,yCenter,xSecond,ySecond);
		
		int mLength = (int) (clockRadius*0.65);
		int xMinute = (int) (xCenter + mLength *Math.sin(minute*(2*Math.PI/60)));
		int yMinute = (int) (yCenter - mLength *Math.cos(minute*(2*Math.PI/60)));
		
		g.drawLine(xCenter,yCenter,xMinute,yMinute);
		
		int hLength = (int) (clockRadius*0.5);
		int xHour = (int) (xCenter + hLength *Math.sin((hour+minute/60)*(2*Math.PI/60)));
		int yHour = (int) (yCenter - hLength *Math.cos((hour+minute/60)*(2*Math.PI/60)));
		
		g.drawLine(xCenter,yCenter,xHour,yHour);
	}
	public int getHour()
	{
		return hour;
	}
	public int getMinute()
	{
		return minute;
	}
	public int getSecond()
	{
		return second;
	}
	public void setSecond(int second)
	{
		this.second = second;
	}
	public void setMinute(int minute)
	{
		this.minute = minute;
	}
	public void setHour(int hour)
	{
		this.hour = hour;
	}
}
