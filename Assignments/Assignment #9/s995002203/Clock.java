package a9.s995002203;
import javax.swing.*;
import java.awt.*;
import java.util.*;

public class Clock extends JPanel{
	private int hour, minute, second;
	public Clock(){
		setCurrentClock(false);
	}
	
	public void clearClock(){
		hour=0;
		minute=0;
		second=0;
	}
	public void setCurrentClock(boolean flag){
		if(!flag){
			Calendar calendar=new GregorianCalendar();
			hour=calendar.get(Calendar.HOUR_OF_DAY);
			minute=calendar.get(Calendar.MINUTE);
			second=calendar.get(Calendar.SECOND);
		}
		else{
			clearClock();
			repaint();
		}
	}

	public int getHr(){
		return hour;
	}
	public int getMin(){
		return minute;
	}
	public int getSec(){
		return second;
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		int radius=(int)(Math.min(getWidth(), getHeight())*0.8*0.5);
		int x=getWidth()/2;
		int y=getHeight()/2;
		
		g.setColor(Color.BLACK);
		g.drawOval(x-radius, y-radius, 2*radius, 2*radius);
		g.drawString("12", x-5, y-radius+12);//設定時鐘外觀
		g.drawString("9", x-radius+3, y+5);
		g.drawString("3", x+radius-10, y+3);
		g.drawString("6", x-3, y+radius-3);
		
		int secLength=(int)(radius*0.8);//設定指針長度
		int minLength=(int)(radius*0.65);
		int hrLength=(int)(radius*0.5);
		
		int xSec=(int)(x+secLength*Math.sin(second*(2*Math.PI/60)));
		int ySec=(int)(y-secLength*Math.cos(second*(2*Math.PI/60)));
		g.setColor(Color.RED);
		g.drawLine(x, y, xSec, ySec);//畫出秒針
		
		int xMin=(int)(x+minLength*Math.sin(minute*(2*Math.PI/60)));
		int yMin=(int)(y-minLength*Math.cos(minute*(2*Math.PI/60)));
		g.setColor(Color.BLUE);
		g.drawLine(x, y, xMin, yMin);//畫出分針
		
		int xHr=(int)(x+hrLength*Math.sin((hour%12 + minute/60.0)*(2*Math.PI/12)));
		int yHr=(int)(y-hrLength*Math.cos((hour%12 + minute/60.0)*(2*Math.PI/12)));
		g.setColor(Color.GREEN);
		g.drawLine(x, y, xHr, yHr);//畫出時針
	
	}
}
