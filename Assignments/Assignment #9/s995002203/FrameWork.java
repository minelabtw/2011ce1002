package a9.s995002203;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.*;
import javax.swing.*;

public class FrameWork extends JFrame  {
	private Clock clk=new Clock();
	private JButton start=new JButton("START");
	private JButton stop=new JButton("STOP");
	private JButton reset=new JButton("RESET");
	private JPanel p1=new JPanel(new GridLayout(1, 3, 2, 2));//放button的panel
	private JPanel p2=new JPanel(new BorderLayout());//放時鐘跟按鈕的panel
	private Timer timer=new Timer(1000,new TimerListener());
	private boolean flag=false;//判斷現在的時間要歸零嗎?
	
	public FrameWork(){
		p1.add(start);
		p1.add(stop);
		p1.add(reset);
		p2.add(p1, BorderLayout.NORTH);
		p2.add(clk,BorderLayout.CENTER);
		add(p2);
		//Timer timer=new Timer(1000,new TimerListener());
		start.addActionListener(new buttonListener());
		stop.addActionListener(new buttonListener());
		reset.addActionListener(new buttonListener());
		timer.start();
	}
	
	private class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			clk.setCurrentClock(flag);
			clk.repaint();
		}
	}
	
	private class buttonListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			if(e.getSource()==start){
				flag=false;
				timer.start();
			}
			else if(e.getSource()==stop){
				timer.stop();
			}
			else if(e.getSource()==reset){
				flag=true;//時間要歸零則flag-true
				timer.start();
			}
		}
	}

}
