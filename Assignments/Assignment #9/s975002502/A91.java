package a9.s975002502;
import javax.swing.JFrame;


public class A91 {
	/** Main method */
	public static void main(String[] args) {
		FrameWork f = new FrameWork();
		f.setTitle("Asignment #9");
		f.setSize(200, 200);
		f.setLocationRelativeTo(null); // Center the frame
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
