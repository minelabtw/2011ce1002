package a9.s975002502;
import java.awt.GridLayout;
import java.awt.event.*;
import javax.swing.*;

public class FrameWork extends JFrame {
	private Clock clock = new Clock();
	// Create a timer with delay 1000 ms
	private Timer timer = new Timer(1000, new TimerListener());
	// create JLabel for screen
	private JLabel screen = new JLabel();
	private String screenText = "0:0:0";
	
	public FrameWork() {
		
		JPanel Pclock = new JPanel();
		Pclock.setLayout(new GridLayout(1, 2, 5, 5));
		
		JPanel Pbottom = new JPanel();
		Pbottom.setLayout(new GridLayout(2, 1, 5, 5));
		
		// Create JPanel btnPanel for the buttons and set GridLayout
		JPanel btnPanel = new JPanel();
		btnPanel.setLayout(new GridLayout(3, 1, 5, 5));
		
		// Create the operation buttons
		JButton start = new JButton("Start");
		start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				timer.start();
			}
		});
		
		JButton stop = new JButton("Stop");
		stop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				timer.stop();
			}
		});
		
		JButton clear = new JButton("Clear");
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clock.clearClock();
				timer.stop();
				repaint();
			}
		});
		
		// add buttons to the panel
		btnPanel.add(start);
		btnPanel.add(stop);
		btnPanel.add(clear);
		
		screen.setText(screenText);
		
		// add screen and buttons to bottom Panel
		Pbottom.add(screen);
		Pbottom.add(btnPanel);
		
		// add clock and bottom to Pclock
		Pclock.add(clock);
		Pclock.add(Pbottom);
		
		
		add(Pclock);
	}
	
	public void setScreenText() {
		this.screenText = clock.getHour()+":"+clock.getMinute()+":"+clock.getSecond();
		screen.setText(screenText);
	}
	
	private class TimerListener implements ActionListener {
		/** Handle the action event */
		public void actionPerformed(ActionEvent e) {
			// Set new time and repaint the clock to display current time
			clock.setCurrentClock();
			setScreenText();
			repaint();
		}
	}
}
