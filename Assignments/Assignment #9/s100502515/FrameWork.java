package a9.s100502515;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class FrameWork extends JFrame implements ActionListener {
	private JLabel theScreen = new JLabel(" ");
	private JPanel theWindow = new JPanel();
	private JPanel control = new JPanel();
	private JButton start = new JButton("Start");
	private JButton stop = new JButton("Stop");
	private JButton reset = new JButton("Reset");
	private JButton time = new JButton("Now time");
	private JButton stopWatch = new JButton("Stop Watch");
	private Clock clock = new Clock();
	private int toStart = -1;
	private int toStop = -1;
	private int toReset = -1;
	private int toTime = 1;
	private int tostopWatch = -1;

	FrameWork() {
		setLayout(new GridLayout(2, 1));
		control.setLayout(new GridLayout(5,1));
		control.add(start);
		control.add(stop);
		control.add(reset);
		control.add(time);
		control.add(stopWatch);
		theWindow.setLayout(new BorderLayout());
		theWindow.add(clock, BorderLayout.NORTH);
		theWindow.add(theScreen, BorderLayout.SOUTH);
		this.add(theWindow);
		this.add(control);
		
		start.addActionListener(this);
		stop.addActionListener(this);
		reset.addActionListener(this);
		time.addActionListener(this);
		time.addActionListener(this);
		
	}
	
	public void actionPerformed(ActionEvent e) {
			if (toStart == 1) {
				
			} else if (toStop == 1) {
				
			} else if (toReset == 1) {
				
			} else if (toTime == 1) {
				
			} else if (tostopWatch == 1) {
				
			}	
	}
}
