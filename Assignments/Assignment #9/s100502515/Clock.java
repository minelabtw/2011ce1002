package a9.s100502515;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JPanel;

public class Clock extends JPanel {
	private int hour;
	private int minute;
	private int second;
	
	Clock() {
		setTime();
	}
	
	public void setTime() {
		Calendar calendar = new GregorianCalendar();
		hour = calendar.get(Calendar.HOUR_OF_DAY);
		minute = calendar.get(Calendar.MINUTE);
		second = calendar.get(Calendar.SECOND);
	}
	
	public void setHour(int inHour) {
		hour =  inHour;
		if(hour >= 24)
			hour = 0;
	}
	
	public void setMinute(int inMinute) {
		minute = inMinute;
		if(minute >= 60) {
			hour++;
		}
	}
	
	public void setSecond(int inSecond) {
		second = inSecond;
		if(second >= 60) {
			minute++;
		}
	}
	
	public int getHour() {
		return hour;
	}
	
	public int getMinute() {
		return minute;
	}
	
	public  int getSecond() {
		return second;
	}
	
	protected void paintComponent(Graphics graphics) {
		super.paintComponents(graphics);
		int xOfCenter = getWidth() / 2;
		int yOfCenter = getHeight() / 2;
		Color black = new Color(0, 0, 0);
		Color red = new Color(255, 0, 0);
		Color green = new Color(0, 255, 0);
		Color blue = new Color(0, 0, 255);
		int radius = (int) (getWidth() * 0.2);
		

		int hourLength = (int) (radius * 0.3);
		int minuteLength = (int) (radius * 0.8);
		int secondLength = (int) (radius * 0.9);

		graphics.setColor(black);
		graphics.drawOval(xOfCenter - radius, yOfCenter - radius, 2 * radius, 2 * radius);
		graphics.drawString("12", xOfCenter - 5, yOfCenter - radius + 10);
		graphics.drawString("3", xOfCenter - 8 + radius, yOfCenter);
		graphics.drawString("6", xOfCenter, yOfCenter + radius);
		graphics.drawString("9", xOfCenter - radius, yOfCenter);
		graphics.setColor(red);
		graphics.drawLine(xOfCenter, yOfCenter, Hour_x, Hour_y);
		graphics.setColor(green);
		graphics.drawLine(xOfCenter, yOfCenter, Minute_x, Minute_y);
		graphics.setColor(blue);
		graphics.drawLine(xOfCenter, yOfCenter, Second_x, Second_y);
		
		int Hour_x = (int) (xOfCenter + hourLength
				* Math.sin(hour * (2 * Math.PI / 60)));
		int Hour_y = (int) (xOfCenter + hourLength
				* Math.cos(hour * (2 * Math.PI / 60)));
		int Minute_x = (int) (xOfCenter + minuteLength
				* Math.sin(minute * (2 * Math.PI / 60)));
		int Minute_y = (int) (yOfCenter - minuteLength
				* Math.cos(minute * (2 * Math.PI / 60)));
		int Second_x = (int) (xOfCenter + secondLength
				* Math.sin(second * (2 * Math.PI / 60)));
		int Second_y = (int) (yOfCenter - secondLength
				* Math.cos(second * (2 * Math.PI / 60)));

		
	}
}
