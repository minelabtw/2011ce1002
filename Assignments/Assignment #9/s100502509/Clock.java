package a9.s100502509;
import java.awt.*;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.*;

public class Clock extends JPanel{
	
	protected int hour;
	protected int min;
	protected int sec;
	public Clock(){
		
	}
	
	
	
	public void setCurrentTime(){
		Calendar calendar = new GregorianCalendar();
		this.hour = calendar.get(Calendar.HOUR_OF_DAY);
		this.min = calendar.get(Calendar.MINUTE);
		this.sec = calendar.get(Calendar.SECOND);
	}
	
	public void clearClock() {
		hour = min = sec = 0;
	}
	
	public void setCountTime(){//Function to count time
		sec += 1;
		if(sec>=60) {
			sec = 0;
			min += 1;
		}
		if(min>=60) {
			min = 0;
			hour += 1;
		}
		if(hour>=12) {
			hour = min = sec = 0;
		}
	}
	
	public void setHour(){
		this.hour=hour;

	}
	
	public int getHour(){
		return hour;
	}
	
	public void setMin(){
		this.min=min;

	}
	
	
	public int getMin(){
		return min;
	}
	
	public void setSec(){
		this.sec=sec;

	}
	
	public int getSec(){
		return sec;
	}
	
	
	
	protected void paintComponent(Graphics g){//Function to graphic clock!!
		super.paintComponent(g);
		int clockRadius = (int)(Math.min(getWidth(), getHeight())*0.8*0.5);
		int xCenter = getWidth()/2;
		int yCenter = getHeight()/2;
		g.setColor(Color.BLACK);
		
		g.drawOval(xCenter-clockRadius, yCenter-clockRadius, 2*clockRadius, 2*clockRadius);
		g.drawString("12",xCenter-5 , yCenter-clockRadius+12);
		g.drawString("9",xCenter-clockRadius+3 , yCenter+5);
		g.drawString("3",xCenter+clockRadius-10 , yCenter+3);
		g.drawString("6",xCenter-3 , yCenter+clockRadius-3);
		
		int sLength = (int)(clockRadius*0.8);
		int xSecond = (int)(xCenter+sLength*Math.sin(sec*(2*Math.PI/60)));
		int ySecond = (int)(yCenter-sLength*Math.cos(sec*(2*Math.PI/60)));
		g.setColor(Color.red);
		g.drawLine(xCenter, yCenter, xSecond, ySecond);
		
		int mLength = (int)(clockRadius*0.65);
		int xMinute = (int)(xCenter+mLength*Math.sin(min*2*Math.PI/60));
		int yMinute = (int)(yCenter-mLength*Math.cos(min*2*Math.PI/60));
		g.setColor(Color.blue);
		g.drawLine(xCenter, yCenter, xMinute, yMinute);
		
		int hLength = (int)(clockRadius*0.5);
		int xHour = (int)(xCenter+hLength*Math.sin((hour%12+min/60.0)*(2*Math.PI/12)));
		int yHour = (int)(yCenter-hLength*Math.cos((hour%12+min/60.0)*(2*Math.PI/12)));
		g.setColor(Color.green);
		g.drawLine(xCenter, yCenter, xHour, yHour);
		
		
	}

	
	

}
