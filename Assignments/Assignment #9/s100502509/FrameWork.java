package a9.s100502509;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
public class FrameWork extends JFrame implements ActionListener{
	
	JButton current = new JButton("Time");//declare Button
	JButton start = new JButton("Start");
	JButton stop = new JButton("Stop");
	JButton clear = new JButton("Clear");
	JLabel screen = new JLabel("00:00:00");
	Clock clock = new Clock();
	String countTime = "00:00:00";
	boolean clockType= true;
	Timer t = new Timer(1000, new TimerListener());
	public FrameWork(){
		
		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(1,5,5,5));
		p1.add(screen);
		p1.add(current);
		current.addActionListener(this);
		p1.add(start);
		start.addActionListener(this);
		p1.add(stop);
		stop.addActionListener(this);
		p1.add(clear);
		clear.addActionListener(this);
		add(p1,BorderLayout.SOUTH);
		add(clock,BorderLayout.CENTER);
		if(clockType==true){// Whether it is the function of counting time or showing current time
			clock.setCurrentTime();
			t.start();
		}
		
	}

	public void actionPerformed(ActionEvent e) {
		
		
		if(e.getSource()==start){//use count time
			clockType = false;
			clock.clearClock();
			t.start();
		
			
		}
		
		else if(e.getSource()==stop){
			t.stop();
		}
		
		else if(e.getSource()==clear){
			countTime = "0:0:0";
			screen.setText(countTime);
			clock.clearClock();
			clock.repaint();
			t.stop();
			
		}
		else {//use current time
			clockType = true;
			clock.setCurrentTime();
			t.start();
			
		}
	}
	
	class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(clockType){
				clock.setCurrentTime();
				clock.repaint();
			}
			
			else
			{
				clock.setCountTime();				
				countTime = String.valueOf(clock.getHour()) + ":" + String.valueOf(clock.getMin()) + ":" + String.valueOf(clock.getSec());
				screen.setText(countTime);
				clock.repaint();
			}
		}
	}

		
}
	
