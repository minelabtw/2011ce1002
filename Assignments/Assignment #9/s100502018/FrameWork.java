package a9.s100502018;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener {
	private JButton start = new JButton("Start");
	private JButton stop = new JButton("Stop");
	private JButton clear = new JButton("Clear");
	private JPanel changable = new JPanel();
	private JPanel buttons = new JPanel();
	private JPanel total = new JPanel();
	protected JLabel display = new JLabel();
	Clock myClock = new Clock();
	Timer timer = new Timer(1000, new TimerListener());
	Font buttonfont = new Font("TimesRoman", Font.BOLD, 30);
	Font displayfont = new Font("TimesRoman", Font.BOLD, 50);

	public FrameWork() { // set the GUI
		buttons.setLayout(new GridLayout(1, 3));
		buttons.add(start);
		buttons.add(stop);
		buttons.add(clear);
		start.setFont(buttonfont);
		stop.setFont(buttonfont);
		clear.setFont(buttonfont);
		start.addActionListener(this);
		stop.addActionListener(this);
		clear.addActionListener(this);
		changable.setLayout(new GridLayout(2, 1));
		changable.add(display);
		display.setText("          00:00:00");
		display.setFont(displayfont);
		changable.add(buttons);
		total.setLayout(new GridLayout(2, 1));
		total.add(myClock);
		total.add(changable);
		add(total);
	}

	public void actionPerformed(ActionEvent e) { // buttons' effect
		if (e.getSource() == start) {
			timer.start();
		}
		if (e.getSource() == stop) {
			timer.stop();
		}
		if (e.getSource() == clear) {
			timer.stop();
			myClock.clearClock();
			repaint();
			display.setText("          00:00:00");
		}
	}

	class TimerListener implements ActionListener { // timer's effect
		public void actionPerformed(ActionEvent e) {
			int hour = myClock.second / 3600;
			int minute = (myClock.second - hour * 3600) / 60;
			int second = myClock.second - hour * 3600 - minute * 60;
			String result;
			repaint();
			display.setFont(displayfont);
			if (hour < 10)
				result = "          0" + hour;
			else
				result = "          " + hour;

			if (minute < 10)
				result += ":0" + minute;
			else
				result += ":" + minute;

			if (second < 10)
				result += ":0" + second;
			else
				result += ":" + second;

			display.setText(result);
		}
	}
}