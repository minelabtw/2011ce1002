package a9.s100502018;

import javax.swing.*;
import java.awt.*;

public class A91 {
	public static void main(String[] args) {
		FrameWork myFrameWork = new FrameWork();
		myFrameWork.setTitle("Assignment #9");
		myFrameWork.setSize(500, 500);
		myFrameWork.setLocationRelativeTo(null);
		myFrameWork.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		myFrameWork.setVisible(true);
	}
}
