package a9.s100502018;

import javax.swing.*;
import java.awt.*;
import java.util.*;

public class Clock extends JPanel {
	private int secondx;
	private int secondy;
	protected int second = 0;
	protected int hour = 0;
	protected int minute = 0;

	public Clock() {
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int clockRadius = (int) (Math.min(getWidth(), getHeight()) * 0.3);
		int clockX = getWidth() / 2;
		int clockY = getHeight() / 2;
		int secondlength = (int) (clockRadius * 0.65);

		g.drawOval(clockX - clockRadius, clockY - clockRadius, 2 * clockRadius, // draw
																				// the
																				// clock
				2 * clockRadius);
		g.drawString("12", clockX - 6, clockY - (int) (clockRadius * 0.75));
		g.drawString("3", clockX + (int) (clockRadius * 0.75), clockY + 5);
		g.drawString("6", clockX - 3, clockY + (int) (clockRadius * 0.95));
		g.drawString("9", clockX - (int) (clockRadius * 0.85), clockY + 5);
		secondx = (int) (clockX + secondlength
				* Math.sin(second * Math.PI / 30));
		secondy = (int) (clockY - secondlength
				* Math.cos(second * Math.PI / 30));
		g.drawLine(clockX, clockY, secondx, secondy);
		second++;
	}

	public void setCurrentTime() {
		Calendar myCalendar = new GregorianCalendar();
		hour = myCalendar.get(Calendar.HOUR_OF_DAY);
		minute = myCalendar.get(Calendar.MINUTE);
		second = myCalendar.get(Calendar.SECOND);
	}

	public void clearClock() {
		hour = 0;
		second = 0;
		minute = 0;
	}
}
