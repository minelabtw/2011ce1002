package a9.s982003034;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

public class A9 extends JFrame implements ActionListener {

	Clock a9clock;
	Timer time;
	int s, m, h;
	
	JPanel button_panel;
	JLabel screen;
	JButton start;
	JButton stop;
	JButton reset;
	
	public static void main(String[] args) {
		A9 a9 = new A9 ();
	}
	
	public A9 () {

		a9clock = new Clock();
		time = new Timer (1000, this);
		s = 0;
		m = 0;
		h = 0;
		
		button_panel = new JPanel ();
		screen = new JLabel (h + " : " + m + " : " + s);
		
		start = new JButton ("Start");
		start.addActionListener(new ActionListener () {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				time.start();
				start.setEnabled(false);
				stop.setEnabled(true);
				reset.setEnabled(true);
			}
			
		});
		
		stop = new JButton ("Stop");
		stop.addActionListener(new ActionListener () {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				time.stop();
				start.setEnabled(true);
				stop.setEnabled(false);
				reset.setEnabled(true);
			}
			
		});
		stop.setEnabled(false);
		
		reset = new JButton ("Reset");
		reset.addActionListener(new ActionListener () {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				time.stop();
				resetClock();
				start.setEnabled(true);
				stop.setEnabled(false);
				reset.setEnabled(false);
			}
			
		});
		reset.setEnabled(false);
		
		button_panel.setLayout(new GridLayout (1,4));
		button_panel.add(screen);
		button_panel.add(start);
		button_panel.add(stop);
		button_panel.add(reset);
		
		this.setLayout (new BorderLayout ());
		this.add(a9clock, BorderLayout.CENTER);
		this.add(button_panel, BorderLayout.SOUTH);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(500, 500);
		this.setVisible (true);
		
	}
	
	protected void resetClock() {
		s = 0;
		m = 0;
		h = 0;
		a9clock.clearClock();
		setscreen();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		s++;
		m = s/60;
		h = m/60;
		
		setscreen();
		a9clock.setCurrentClock(s, m, h);
		a9clock.repaint();
		if (h == 12) resetClock();
	}

	private void setscreen() {
		screen.setText(h + " : " + m%60 + " : " + s%60);
	}
	

}
