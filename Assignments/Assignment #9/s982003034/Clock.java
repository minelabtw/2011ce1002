package a9.s982003034;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class Clock extends JPanel {

	// clock time
	int sec;
	int min;
	int hr;
	
	// clock position
	int midx;
	int midy;
	int smaller; // which is smaller between width and height
	
	// clock dimension
	int shand;
	int mhand;
	int hhand;
	
	public Clock () {
		clearClock();
		this.setBackground(Color.black);
	}
	
	public void clearClock () {
		sec = 0;
		min = 0;
		hr = 0;
		repaint();
	}

	// the setCurrentClock
	// the input will not be checked
	public void setCurrentClock (int s, int m, int h) {
		sec = s;
		min = m;
		hr = h;
	}
	
	public void paintComponent (Graphics g) {
		
		super.paintComponent(g);
		
		// calculate position
		if (this.getWidth() < this.getHeight()) smaller = this.getWidth();
		else smaller = this.getHeight();
		midx = smaller/2;
		midy = smaller/2;
		
		// calculate dimension
		shand = (int) (midx * 0.9);
		mhand = (int) (midx * 0.75);
		hhand = (int) (midx * 0.6);
		
		// clock base
		// draw the clock according to who is smaller
		g.setColor(Color.white);
		g.drawOval(0, 0, smaller, smaller);
		
		// second hand
		// 1 sec = 6 deg
		// the angle of time must be negative
		// because angle in Cartesian coordinate goes counter-clockwise
		// while a clock goes clockwise (of course)
		g.setColor(Color.green);
		g.drawLine(midx, midy, (int) (getx(sec*6.0)*shand+midx), (int) (gety(sec*6.0)*shand+midy) );

		// minute hand
		// 1 min = 6 deg
		g.setColor(Color.blue);
		g.drawLine(midx, midy, (int) (getx(min*6.0)*mhand+midx), (int) (gety(min*6.0)*mhand+midy) );
		
		// hour hand
		// 1 hour = 30 deg
		g.setColor(Color.red);
		g.drawLine(midx, midy, (int) (getx(hr*30.0)*hhand+midx), (int) (gety(hr*30.0)*hhand+midy) );
		
	}
	
	private double getx (double deg) {
		return Math.cos((deg-90.0)/180.0*Math.PI);
	}
	
	private double gety (double deg) {
		return Math.sin((deg-90.0)/180.0*Math.PI);
	}
	
}
