package a9.s100502028;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class FrameWork extends JFrame implements ActionListener {
	
	private Clock clock = new Clock(); // Create a clock
	private Timer timer = new Timer(10, new TimerListener()); // Create a timer with delay 10 ms
	private JButton jbtStart = new JButton("Start");
	private JButton jbtStop = new JButton("Stop");
	private JButton jbtClear = new JButton("Clear");
	private JLabel jlblScreen = new JLabel("00:00:00:00");
	protected String hourString;
	protected String minuteString;
	protected String secondString;
	protected String millisecondString;
	
	private Font font1 = new Font("Algerian", Font.BOLD, 30);
	private Font font2 = new Font("TimeNewRoman", Font.BOLD, 16);
	private Border lineBorder1 = new LineBorder(Color.BLUE, 2);
	private Color color1 = new Color(88, 222, 108);
	private Color color2 = new Color(255, 255, 128);
	
	public FrameWork() {
		
		// Create panel p1 for the buttons
		JPanel p1 = new JPanel(new GridLayout(1, 3, 5, 5));
		p1.setBackground(Color.PINK);
		jbtStart.setFont(font2);
		jbtStop.setFont(font2);
		jbtClear.setFont(font2);
		jbtStart.setBackground(color2);
		jbtStop.setBackground(color2);
		jbtClear.setBackground(color2);
		p1.add(jbtStart);
		p1.add(jbtStop);
		p1.add(jbtClear);
		
		// Create panel p2 for time screen
		JPanel p2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		jlblScreen.setFont(font1);
		jlblScreen.setForeground(Color.RED);
		p2.setBackground(Color.PINK);
		p2.add(jlblScreen);
		
		// Create panel p3 for p1 and p2
		JPanel p3 = new JPanel(new GridLayout(1, 2, 5, 5));
		p3.setBackground(Color.PINK);
		p3.setBorder(lineBorder1);
		p3.add(p2, BorderLayout.WEST);
		p3.add(p1, BorderLayout.CENTER);
		
		// Add clock and p3 to frame
		clock.setBackground(color1);
		add(clock, BorderLayout.CENTER);
		add(p3, BorderLayout.SOUTH);
		
		// Active button event here
		jbtStart.addActionListener(this);
		jbtStop.addActionListener(this);
		jbtClear.addActionListener(this);
	}
	
	// Method to let the button works
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == jbtStart) {
			timer.start(); // Start timer
		}
		else if (e.getSource() == jbtStop) {
			timer.stop(); // Stop timer
		}
		else if (e.getSource() == jbtClear) {
			timer.stop();
			clock.clearClock(); // Reset the clock
			typesetTime();
			jlblScreen.setText(hourString + ":" + minuteString + ":" + secondString + ":" + millisecondString);
			repaint();
		}
	} // End method actionPerformed
	
	// Method to typeset the form of the time 
	public void typesetTime() {
		if (clock.millisecond < 10) {
			millisecondString = "0" + clock.millisecond;
			}
		else
			millisecondString = "" + clock.millisecond;
		
		if (clock.second < 10) {
			secondString = "0" + clock.second;
		}
		else
			secondString = "" + clock.second;
		
		if (clock.minute < 10) {
			minuteString = "0" + clock.minute;
		}
		else
			minuteString = "" + clock.minute;
		
		if(clock.hour < 10) {
			hourString = "0" + clock.hour;
		}
		else
			hourString = "" + clock.hour;
	} // End method typesetTime
	
	// Method to let the timer work
	private class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) { // Handle the action event 
			typesetTime();
			clock.setCurrentClock(); // Set the new time
			jlblScreen.setText(hourString + ":" + minuteString + ":" + secondString + ":" + millisecondString);
			repaint(); // Repaint the clock to display current time
		}	
	}
}
