package a9.s100502028;

import java.awt.*;
import javax.swing.*;

public class Clock extends JPanel {
	protected int hour;
	protected int minute;
	protected int second;
	protected int millisecond;
	
	private Color color1 = new Color(128, 168, 230);
	
	public Clock() {
		
	}
	
	// Method to reset the time
	public void clearClock() {
		hour = 0;
		minute = 0;
		second = 0;
		millisecond = 0;
	}
	
	// Method to set the time 
	public void setCurrentClock() {
		if (millisecond < 99) {
			millisecond = millisecond + 1;
		}
		else {
			if (second < 59) {
				second = second + 1;
			}
			else {
				if (minute < 59) {
					minute = minute + 1;
				}
				else {
					if (hour < 23)
						hour = hour + 1;
					else {
						hour = 0;
					}
					minute = 0;
				}
				second = 0; 
			}
			millisecond = 0;
		}
	} // End method setCurrentClock
	
	// Draw the clock 
	public void paintComponent(Graphics g) { // Override paintComponent
		super.paintComponent(g); // Draw things in the superclass
		
		// Initial clock parameters
		int clockRadius = (int)(Math.min(getWidth(), getHeight()) * 0.8 * 0.5);
		int xCenter = getWidth() / 2;
		int yCenter = getHeight() / 2;
		
		// Draw circle and the lines of graduation
		g.setColor(Color.BLACK);
		g.drawOval(xCenter - clockRadius - 1, yCenter - clockRadius - 1, 2 * clockRadius + 2, 2 * clockRadius + 2);
		g.setColor(color1);
		g.fillOval(xCenter - clockRadius, yCenter - clockRadius, 2 * clockRadius, 2 * clockRadius);
		g.setColor(Color.BLACK);
		g.drawLine(xCenter, yCenter - clockRadius, xCenter, yCenter - clockRadius + 15);
		g.drawLine(xCenter + clockRadius, yCenter, xCenter + clockRadius - 15, yCenter);
		g.drawLine(xCenter, yCenter + clockRadius, xCenter, yCenter + clockRadius - 15);
		g.drawLine(xCenter - clockRadius, yCenter, xCenter - clockRadius + 15, yCenter);
		
		// Draw second hand
		int sLength = (int)(clockRadius * 0.8);
		int xSecond = (int)(xCenter + sLength * Math.sin(second * (2 * Math.PI / 60)));
		int ySecond = (int)(yCenter - sLength * Math.cos(second * (2 * Math.PI / 60)));
		g.setColor(Color.RED);
		g.drawLine(xCenter, yCenter, xSecond, ySecond);
		
	} // End method paintComponent
}
