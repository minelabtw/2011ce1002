package a9.s100502028;

import javax.swing.*;

public class A91 {
	public static void main(String[] args) {
		FrameWork frame = new FrameWork(); // Create a frame
		frame.setTitle("Assignment #9"); // Set the frame name
		frame.setSize(600, 600); // Set the frame size
		frame.setLocationRelativeTo(null); // Center a frame
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true); // Display the frame
	}
}
