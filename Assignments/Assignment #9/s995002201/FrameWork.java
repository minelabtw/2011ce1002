package a9.s995002201;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.Timer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class FrameWork extends JFrame implements ActionListener
{
	//private JPanel panel_screen = new JPanel();
	private JPanel panel_buttons = new JPanel();
	private JLabel label_screen = new JLabel();
	private JButton button_0 = new JButton("start");
	private JButton button_1 = new JButton("stop");
	private JButton button_2 = new JButton("clear");
	
	//Clock k = new Clock();
	StillClock c = new StillClock();
	protected int second;
	
	public FrameWork()
	{
		//setLayout(new GridLayout(2,2,2,2));
		//panel_buttons.setLayout(new GridLayout(3,5,1,1));
		//panel_screen.setLayout(new GridLayout(1,1,1,1));
		//panel_screen.setSize(300, 100);
		//panel_screen.add(label_screen);
		panel_buttons.add(button_0);
		panel_buttons.add(button_1);
		panel_buttons.add(button_2);
		add(label_screen);
		add(panel_buttons);
		//k.add();
		button_0.addActionListener(this);
		button_1.addActionListener(this);
		button_2.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e)
	{
		c.setCurrentClock();
		c.repaint();
		if(e.getSource()==button_0)//start
		{
			c.setCurrentClock();
			System.out.println("start");
		}
		else if(e.getSource()==button_1)//stop
		{
			c.stopSecond(this.second);
			System.out.println("stop");
		}
		else if(e.getSource()==button_2)//clear
		{
			c.clearClock();
			System.out.println("clear");
		}
	}
}