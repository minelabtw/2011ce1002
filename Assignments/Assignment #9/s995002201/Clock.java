package a9.s995002201;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.event.*;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.*;

public class Clock extends JFrame
{
    StillClock c = new StillClock();
    
	public Clock() 
	{
		add();
	}
	public void add()
	{
		add(c);
		Timer timer = new Timer(1000, new TimerListener());
		timer.start();
	}
	private class TimerListener implements ActionListener 
	{
		/* Handle the action event */
		public void actionPerformed(ActionEvent e)
		{
			// Set new time and repaint the clock to display current time
			c.setCurrentClock();
			c.repaint();
		}
	}
}
class StillClock extends JPanel 
{
	//data member: hour, minute, second, all of them are integers
	private int hour;
	private int minute;
	private int second;
	/* Construct a default clock with the current time*/
	{
		setCurrentClock();
	}
	
	//method clearClock: to reset the time
	public void clearClock()
	{
		clearSecond();
	}
	
    //method setCurrentClock: to set the current time
	public void setCurrentClock() 
	{
		getSecond();
	}
	//method paintComponent: to draw the clock and second hand
	protected void paintComponent(Graphics g) 
	{
		super.paintComponent(g);
		// Initialize clock parameters
		int clockRadius = (int)(Math.min(getWidth(), getHeight()) * 0.8 * 0.5);
		int xCenter = getWidth() / 2;
		int yCenter = getHeight() / 2;
		// Draw circle
		g.setColor(Color.BLACK);
		g.drawOval(xCenter - clockRadius, yCenter - clockRadius,2 * clockRadius, 2 * clockRadius);
		g.drawString("12", xCenter - 5, yCenter - clockRadius + 12);
		g.drawString("9", xCenter - clockRadius + 3, yCenter + 5);
		g.drawString("3", xCenter + clockRadius - 10, yCenter + 3);
		g.drawString("6", xCenter - 3, yCenter + clockRadius - 3);
		// Draw second hand
		int sLength = (int)(clockRadius * 0.8);
		int xSecond = (int)(xCenter + sLength *
				Math.sin(second * (2 * Math.PI / 60)));
		int ySecond = (int)(yCenter - sLength *
				Math.cos(second * (2 * Math.PI / 60)));
		g.setColor(Color.red);
		g.drawLine(xCenter, yCenter, xSecond, ySecond);
	}
	
	/* Set a new second */
	public void setSecond(int second) 
	{
		this.second = second;
		repaint();
	}
	/* Return second */
	public int getSecond() 	
	{
		second++;
		return second;
	}
	//讓碼表歸零
	public int clearSecond()
	{
		second = 0;
		return second;
	}
	//讓碼表暫停
	public int stopSecond(int second)
	{
		this.second = second;
		this.second += 0;
		return second;
	}
	//畫出碼表形狀
	public Dimension getPreferredSize() 
	{
		return new Dimension(400, 400);
	}
}