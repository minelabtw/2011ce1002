package e10.s100502521;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
public class E10_Frame extends JFrame
{
	private static final long serialVersionUID = 1L;
	private JPanel panel1;
	public E10_Frame()
	{
		super("car");
		setSize(350,350);//�]�w�j�p
		panel1=new paint();
		add(panel1);
		//paintComponent(getGraphics());
	}
	class paint extends JPanel 
	{	
		private static final long serialVersionUID = 1L;
		private int originX=0,originY=100,width;
		private boolean goright=true;
		public paint()
		{
			Timer timer = new Timer(10,new TimerListener());
			timer.start();
			width=getWidth();
		}
		public void paintComponent(Graphics g)
		{
			width=getWidth();
			super.paintComponent(g);			
			g.setColor(Color.RED);//car up
			g.drawRoundRect(originX+20, originY+60, 160, 60, 50, 20);
			g.fillRoundRect(originX+20, originY+60, 160, 60, 50, 20);
			g.setColor(Color.BLUE);
			int carX[]=new int[4];//car down
			int carY[]=new int[4];
			carX[0]=originX+10;
			carX[1]=originX+0;
			carX[2]=originX+200;
			carX[3]=originX+190;
			carY[0]=originY+90;
			carY[1]=originY+120;
			carY[2]=originY+120;
			carY[3]=originY+90;
			g.drawPolygon(carX, carY, 4);
			g.fillPolygon(carX, carY, 4);
		}
		class TimerListener implements ActionListener
		{
			public void actionPerformed(ActionEvent arg0)
			{
				if(originX+200>width)
				{
					goright=false;
				}
				if(originX<0)
				{
					goright=true;
				}
				if(goright)
				{
					originX++;
				}
				else
				{
					originX--;
				}
				repaint();	
			}
		}
	}
}
