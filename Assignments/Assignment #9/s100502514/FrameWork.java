/**
 * class FrameWork in Assignment 9-1:
	 *	include three button (start, stop, clear button) and one JLabel (screen)
	 *	implement the event of the buttons
	 *	implement the mileometer��s functionality with Timer (so you need to implement class TimerListner)
 * NOTICE: The Screen is shown on the clock in this program, instead of JLabel, as Extra.
 */
package a9.s100502514;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener {

	/** UID */
	private static final long serialVersionUID = 1L;
	
	/** Component Definitions */
	Clock myClock = new Clock();
	JPanel buttonPanel = new JPanel(new GridLayout(2, 3));
	
	JButton btnHour = new JButton("+Hours"); //at most 99 hours
	JButton btnMinute = new JButton("+Minutes");
	JButton btnSecond = new JButton("+Seconds");
	JButton btnMode = new JButton("Switch to Timer");
	JButton btnStartStop = new JButton("Start/Stop");
	JButton btnClear = new JButton("Clear");
	
	/** Declares a boolean to determine if the timer is counting */
	boolean is_counting = false; //if timer starts, this will be true, otherwise false
	
	/** Two Timer declarations */
	Timer myTimer = new Timer(1000, new ActionListener(){

		@Override
		public void actionPerformed(ActionEvent evt) {
			try{
				if(myClock.isTimerMode()){
					is_counting = true;
					if(myClock.isCountingDown()){
						myClock.countDown();
					}else{
						myClock.countUp();
					}
				}else{
					is_counting = false; //Make sure not related to counting-up/down in Clock Mode.
					//Current Time will be repainted via paintComponent().
				}
			}catch(ClockModeException ex){
				ex.printStackTrace();
			}
			//Repaint the framework
			repaint();
		}
		
	});
	Timer myTimerForClock = new Timer(500, new ActionListener(){

		@Override
		public void actionPerformed(ActionEvent e) {
			repaint();
		}
		
	});
	
	public FrameWork(){
		/** Set Window Size and Closing Operation */
		setTitle("Clock Timer Test by FlashTeens");
		setSize(450, 500);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		/** Add Components for main framework layout */
		setLayout(new BorderLayout());
		add(myClock, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
		
		/** Add Components for the button panel */
		buttonPanel.add(btnHour);
		buttonPanel.add(btnMinute);
		buttonPanel.add(btnSecond);
		buttonPanel.add(btnMode);
		buttonPanel.add(btnStartStop);
		buttonPanel.add(btnClear);
		
		/** Add button listeners */
		btnHour.addActionListener(this);
		btnMinute.addActionListener(this);
		btnSecond.addActionListener(this);
		btnMode.addActionListener(this);
		btnStartStop.addActionListener(this);
		btnClear.addActionListener(this);
		
		/** Start the timer for default CLOCK MODE */
		myTimer.start();
		myTimerForClock.start();//This(2nd) Timer will never stop
		
	}

	/** Implements BUTTON Actions */
	@Override
	public void actionPerformed(ActionEvent e) {
		try{
			if(myClock.isTimerMode() && myClock.isCountingDown() && myClock.isTimeZero()){
				myClock.resetClock();
				myTimer.stop();
				is_counting = false;
			}else if(e.getSource()==btnHour){
				if(!is_counting)myClock.AddOneHour();
			}else if(e.getSource()==btnMinute){
				if(!is_counting)myClock.AddOneMinute();
			}else if(e.getSource()==btnSecond){
				if(!is_counting)myClock.AddOneSecond();
			}else if(e.getSource()==btnMode){
				if(myClock.isClockMode()){
					myClock.switchMode();//Switch to Timer
					myTimer.stop();
				}else if(myClock.isTimeZero()){//Only if time is zero can user switch back to clock mode.
					myClock.switchMode();//Switch to Clock
					myTimer.start();
				}
				is_counting = false;
			}else if(e.getSource()==btnStartStop){
				if(myClock.isClockMode())throw new ClockModeException();
				if(is_counting){
					myTimer.stop();
					//System.out.print("stop ");
				}else{
					myTimer.start();
					//System.out.print("start ");
				}
				is_counting = !is_counting;
				//System.out.println(is_counting);
			}else if(e.getSource()==btnClear){
				if(!is_counting)myClock.resetClock();
			}
			if(myClock.isTimerMode()){
				if(!is_counting && myClock.isTimeZero()){
					btnMode.setText("Switch to Clock");
					btnMode.setToolTipText(null);
					btnMode.setEnabled(true);
				}else{
					btnMode.setText("Cannot Switch");
					btnMode.setToolTipText("You cannot switch to Clock until the timer is zero.");
					btnMode.setEnabled(false);
				}
			}else{
				btnMode.setText("Switch to Timer");
			}
		}catch(ClockModeException ex){
			JOptionPane.showMessageDialog(null, "Please switch to Timer Mode!!",
					"Assignment 9-1", JOptionPane.ERROR_MESSAGE);
		}
		repaint();
	}
	
}
