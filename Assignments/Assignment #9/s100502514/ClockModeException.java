/** Represents an Exception when trying to use a timer-mode function in CLOCK Mode. */

package a9.s100502514;

public class ClockModeException extends Exception {

	/** UID */
	private static final long serialVersionUID = 1L;
	
	public ClockModeException(){
		super("Timer functions cannot be used in Clock Mode.");
	}
	public ClockModeException(String message){
		super(message);
	}
	
}
