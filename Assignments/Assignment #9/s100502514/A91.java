/**
 * CE1002-100502514 FlashTeens Chiang
 * [The Following Rules are from ce1002 website]
 * Assignment 9-1
 * 
 * Please write a program and meet the following requirements:
	
	class Clock:
	 *	data member: hour, minute, second, all of them are integers
	 *	method clearClock: to reset the time
	 *	method setCurrentClock: to set the current time
	 *	method paintComponent: to draw the clock and second hand
	 *	implement as a JPanel
	class FrameWork:
	 *	include three button (start, stop, clear button) and one JLabel (screen)
	 *	implement the event of the buttons
	 *	implement the mileometer��s functionality with Timer (so you need to implement class TimerListner)
	
 * For more informations, please see on ce1002 website.
	
 */
package a9.s100502514;

public class A91 {
	public static void main(String[] args){
		FrameWork myWindow = new FrameWork();
		myWindow.setVisible(true);
	}
}
