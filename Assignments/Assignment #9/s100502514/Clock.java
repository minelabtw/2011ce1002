/**
 * class Clock:
	 *	data member: hour, minute, second, all of them are integers
	 *	method clearClock: to reset the time
	 *	method setCurrentClock: to set the current time
	 *	method paintComponent: to draw the clock and second hand
	 *	implement as a JPanel
 * EXTRA: Both Count-up and Count-down functionalities.
 */
package a9.s100502514;

import java.awt.*;
import java.util.Date;

import javax.swing.*;

public class Clock extends JPanel {
	
	/** UID */
	private static final long serialVersionUID = 1L;
	
	/** Private Data Members */
	private int hour, minute, second;
	
	//Determines whether the timer text color will be changed in (almost) time-up state or not.
	private boolean is_counting_down = false;
	
	//Determines clock mode or timer mode.
	private boolean is_timer_mode = false;
	
	/** Specified Constructor with TIMER MODE */
	public Clock(int h, int m, int s){
		is_timer_mode = true;
		switchMode(is_timer_mode);
		hour = h;
		minute = m;
		second = s;
	}
	/** Default Constructor with CLOCK MODE */
	public Clock(){
		is_timer_mode = false;
		switchMode(is_timer_mode);
	}
	
	/** Getter methods for attributes */
	public int getHours(){ return hour; }
	public int getMinutes(){ return minute; }
	public int getSeconds(){ return second; }
	public boolean isCountingDown(){ return is_counting_down; }
	public boolean isClockMode(){ return !is_timer_mode; }
	public boolean isTimerMode(){ return is_timer_mode; }
	
	/** Reset the clock */
	public void resetClock() throws ClockModeException{
		if(isClockMode())throw new ClockModeException();
		hour = minute = second = 0;
		is_counting_down = false;
	}
	
	/** Transform the private data values into total seconds. */
	public int getTotalSeconds(){
		return hour*3600+minute*60+second;
	}
	
	/** Determine whether the time is zero. */
	public boolean isTimeZero() throws ClockModeException{
		if(isTimerMode())return getTotalSeconds()<=0;
		else throw new ClockModeException();
	}
	
	/** When "+Hours" button pressed */
	public void AddOneHour() throws ClockModeException{
		if(isClockMode())throw new ClockModeException();
		hour = (hour+1)%100;
		is_counting_down = !isTimeZero();
	}
	/** When "+Minutes" button pressed */
	public void AddOneMinute() throws ClockModeException{
		if(isClockMode())throw new ClockModeException();
		minute = (minute+1)%60;
		is_counting_down = !isTimeZero();
	}
	/** When "+Seconds" button pressed */
	public void AddOneSecond() throws ClockModeException{
		if(isClockMode())throw new ClockModeException();
		second = (second+1)%60;
		is_counting_down = !isTimeZero();
	}
	
	/** Timer Counting Function
	 *  The parameter integer may be positive for counting-up; or negative for counting-down.
	 *  If the parameter is set zero, neither counting up nor down will be done,
		but make sure that the timer clock is no longer counting down.
	 *  Returns true for keep counting;
	 *  Returns false to indicate that time is up when counting down.
	 */
	public boolean countClock(int seconds) throws ClockModeException{
		/* Determine if seconds<0, where the variable is_counting_down decides the timer text color. */
		is_counting_down = seconds<0;
		
		/* Add some seconds into total time. */
		int total_time = getTotalSeconds();
		total_time += seconds;
		
		/* Check if the time is up */
		if(is_counting_down && isTimeZero()){
			return false;
		}
		
		/* Set the time if still time-countable. */
		hour = total_time/3600;
		minute = (total_time/60)%60;
		second = total_time%60;
		return true;
	}
	
	/** Count-up 1 seconds; the short syntax form for countClock(1);
	 *  notice that nothing will be returned because it would always be true. */
	public void countUp() throws ClockModeException{
		countClock(1);
	}
	/** Count-down 1 seconds; the short syntax form for countClock(-1);
	 *  when time is up, it will return false, otherwise true. */
	public boolean countDown() throws ClockModeException{
		return countClock(-1);
	}
	
	/** Recommended use: countClock(int) instead of assignment requirement: setCurrentClock(int).
	 *  In fact 2 methods are equivalant in TIMER MODE;
	 *  still, updateClock() will be run automatically in paintComponent(). */
	@Deprecated
	public boolean setCurrentClock(int seconds) throws ClockModeException{
		if(is_timer_mode)return countClock(seconds);
		else{
			updateClock();
			return false;
		}
	}
	
	/** Switch to Clock or Timer Mode */
	public void switchMode(){
		switchMode(!is_timer_mode);
	}
	public void switchMode(boolean mode){
		is_timer_mode = mode;
		if(is_timer_mode){
			hour = minute = second = 0;
			setBackground(Color.black);
		}else{
			updateClock();
			setBackground(Color.blue);
		}
	}
	
	/** Update the current time in CLOCK MODE */
	@SuppressWarnings("deprecation")
	private void updateClock(){
		Date now = new Date();
		hour = now.getHours();
		minute = now.getMinutes();
		second = now.getSeconds();
		//System.out.println(hour+" "+minute+" "+second);
	}
	
	@SuppressWarnings("deprecation")
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		try{
			int centerX = getWidth()/2, centerY = getHeight()/2;
			int radius = Math.min(centerX, centerY)*8/9;
			int outer_radius = radius + 3;
			
			if(!is_timer_mode)updateClock(); //Update to current time in CLOCK MODE
			
			/* Draw the skin of the clock */
			g.setColor(Color.orange); //Bright Orange Ring
			g.fillOval(centerX-outer_radius, centerY-outer_radius, outer_radius*2, outer_radius*2);
			g.setColor(new Color(64, 32, 0)); //Vandyke Brown Inside
			g.fillOval(centerX-radius, centerY-radius, radius*2, radius*2);
			
			/* Determine the color of numeric strings */
			if(!is_timer_mode){
				g.setColor(new Color(153, 0, 204)); //Purple Text if CLOCK mode
			}else if(is_counting_down){
				int total_time = getTotalSeconds();
				//The timer text color is determined by the rest of time while counting down.
				if(total_time<=0)g.setColor(new Color(255, 255, 0)); //Yellow Text if time is up
				else if(total_time<=10)g.setColor(new Color(255, 51, 0)); //Bright Red Text if <= 10 sec
				else if(total_time<=60)g.setColor(new Color(204, 102, 0)); //Golden Red Text if <= 1 min
				else g.setColor(new Color(153, 102, 0)); //Caramel Brown Text if > 1 min
			}else{
				g.setColor(new Color(51, 102, 0)); //Dark Green Text
			}
			
			/* Determine the content of numeric strings */
			String message = "";
			if(is_counting_down && isTimeZero()){
				message = "Time's Up!";
			}else if(getTotalSeconds()<0){
				message = "--:--:--";
			}else{
				message += hour+":";
				
				if(minute<10)message+="0";
				message += minute+":";
				
				if(second<10)message+="0";
				message += second;
			}
			
			/* Draw the numeric strings with central alignment */
			g.setFont(new Font("Arial Black",Font.PLAIN,radius/3));
			g.drawString(message,
					centerX-g.getFontMetrics().stringWidth(message)/2,
					centerY+g.getFontMetrics().getHeight()/4);
			
			/* Set the length for 3 hands on the timer */
			double length_s = 0.95*radius, length_m = 0.8*radius, length_h = 0.6*radius;
			double[] lengths = {length_s, length_m, length_h};
			
			/* Calculate the rotations for 3 hands on the CLOCK */
			Date now = new Date();
			double rotation_s = toRadian(now.getSeconds()*6);
			double rotation_m = toRadian(now.getMinutes()*6)+rotation_s/60;
			double rotation_h = toRadian(now.getHours()*30)+rotation_m/12;
			double[] rotations = {rotation_s,rotation_m,rotation_h};
			
			/* Determines the colors for 3 hands on the CLOCK */
			Color[] rgbs = {
					new Color(0, 255, 153, 102) //Bright Green with 60% transparency for SECOND hand
				,	new Color(153, 255, 0, 102) //Yellow Green with 60% transparency for MINUTE hand
				,	new Color(255, 204, 0, 102) //Golden Yellow with 60% transparency for HOUR hand
			};
			
			
			/* Draw the 3 hands on the CLOCK */
			for(int i=2;i>=0;i--){
				g.setColor(rgbs[i]); //Set the hand colors particularly
				int thickness = i*i*i+2;
				g.fillPolygon(
					new int[]{centerX,
							(int)(centerX+Math.sin(rotations[i]-0.01*thickness)*lengths[i]/2),
							(int)(centerX+Math.sin(rotations[i])*lengths[i]),
							(int)(centerX+Math.sin(rotations[i]+0.01*thickness)*lengths[i]/2)
							},
					new int[]{centerY,
							(int)(centerY-Math.cos(rotations[i]-0.01*thickness)*lengths[i]/2),
							(int)(centerY-Math.cos(rotations[i])*lengths[i]),
							(int)(centerY-Math.cos(rotations[i]+0.01*thickness)*lengths[i]/2)
							},
					4);
			}
		}catch(ClockModeException ex){
			ex.printStackTrace();
		}
	}
	
	/* Transfer degree to radian */
	private double toRadian(double degree){
		return degree*Math.PI/180;
	}
}
