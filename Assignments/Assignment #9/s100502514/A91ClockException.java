/** Represents an Exception related to the clock in Assignment 9-1. */

package a9.s100502514;

public class A91ClockException extends Exception {

	/** UID */
	private static final long serialVersionUID = 1L;
	
	protected A91ClockException(){
		this("A91 Clock Exception Occurred.");
	}
	protected A91ClockException(String message){
		super(message);
	}
}
