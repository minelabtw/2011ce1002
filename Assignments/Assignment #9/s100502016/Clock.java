package a9.s100502016;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.JPanel;

public class Clock extends JPanel {
	private int hour;
	private int minute;
	private int second;

	public void setHour(int hour) {
		this.hour = hour;
		if (this.hour >= 24)
			this.hour = 0;

	}

	public void setMinute(int minute) {
		this.minute = minute;
		if (this.minute >= 60) {
			this.hour += 1;
			this.minute = 0;
		}
	}

	public void setSecond(int second) {
		this.second = second;
		if (this.second >= 60) {
			this.minute += 1;
			this.second = 0;
		}
	}

	public int getHour() {
		return hour;
	}

	public int getMinute() {
		return minute;
	}

	public int getSecond() {
		return second;
	}

	Clock() {
		setCurrentClock();
	}

	public void clearClock() {
		hour = 0;
		minute = 0;
		second = 0;
	}

	public void setCurrentClock() {
		Calendar calendar = new GregorianCalendar();
		setHour(calendar.get(Calendar.HOUR_OF_DAY));
		setMinute(calendar.get(Calendar.MINUTE));
		setSecond(calendar.get(Calendar.SECOND));
	}

	protected void paintComponent(Graphics g) {
		super.paintComponents(g);
		Color black = new Color(0, 0, 0);
		Color red = new Color(255, 0, 0);
		Color green = new Color(0, 255, 0);
		Color blue = new Color(0, 0, 255);
		int radius = (int) (Math.min(getWidth(), getHeight() * 0.8 * 0.5));
		int Center_x = getWidth() / 2;
		int Center_y = getHeight() / 2;

		int HourHandLength = (int) (radius * 0.5);
		int MinuteHandLength = (int) (radius * 0.65);
		int SecondHandLength = (int) (radius * 0.8);

		int Hour_x = (int) (Center_x + HourHandLength
				* (Math.sin(((hour % 12) + minute / 60) * Math.PI / 6)));
		int Hour_y = (int) (Center_y - HourHandLength
				* (Math.cos(((hour % 12) + minute / 60) * Math.PI / 6)));
		int Minute_x = (int) (Center_x + MinuteHandLength
				* Math.sin(minute * (2 * Math.PI / 60)));
		int Minute_y = (int) (Center_y - MinuteHandLength
				* Math.cos(minute * (2 * Math.PI / 60)));
		int Second_x = (int) (Center_x + SecondHandLength
				* Math.sin(second * (2 * Math.PI / 60)));
		int Second_y = (int) (Center_y - SecondHandLength
				* Math.cos(second * (2 * Math.PI / 60)));

		g.setColor(black);
		g.drawOval(Center_x - radius, Center_y - radius, 2 * radius, 2 * radius);
		g.drawString("12", Center_x - 5, Center_y - radius + 10);
		g.drawString("3", Center_x - 8 + radius, Center_y);
		g.drawString("6", Center_x, Center_y + radius);
		g.drawString("9", Center_x - radius, Center_y);
		g.setColor(red);
		g.drawLine(Center_x, Center_y, Hour_x, Hour_y);
		g.setColor(green);
		g.drawLine(Center_x, Center_y, Minute_x, Minute_y);
		g.setColor(blue);
		g.drawLine(Center_x, Center_y, Second_x, Second_y);
	}
}
