package a9.s100502016;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener {
	private JButton StartButton = new JButton("Start");
	private JButton StopButton = new JButton("Stop");
	private JButton ClearButton = new JButton("Clear");
	private JButton TimeButton = new JButton("Current Time");
	private JButton WatchButton = new JButton("StopWatch");
	private JLabel Screen = new JLabel(" ");
	private JPanel AssemblePanel = new JPanel();
	private JPanel ControlPanel = new JPanel();
	private Clock clock = new Clock();
	boolean isStart = false;
	boolean isStop = false;
	boolean isClear = false;
	boolean isTime = true;
	boolean isWatch = false;

	FrameWork() {
		setLayout(new GridLayout(2, 1));
		ControlPanel.add(StartButton);
		ControlPanel.add(StopButton);
		ControlPanel.add(ClearButton);
		ControlPanel.add(TimeButton);
		ControlPanel.add(WatchButton);
		AssemblePanel.setLayout(new BorderLayout());
		AssemblePanel.add(Screen, BorderLayout.NORTH);
		AssemblePanel.add(ControlPanel, BorderLayout.CENTER);

		this.add(clock, BorderLayout.NORTH);
		this.add(AssemblePanel);

		StartButton.addActionListener(this);
		StopButton.addActionListener(this);
		ClearButton.addActionListener(this);
		TimeButton.addActionListener(this);
		WatchButton.addActionListener(this);
		Timer timer = new Timer(1000, new TimerListener());
		timer.start();
		Screen.setText(Integer.toString(clock.getHour()) + ":"
				+ Integer.toString(clock.getMinute()) + ":"
				+ Integer.toString(clock.getSecond()));
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == StartButton) {
			isStart = true;
			isStop = false;
			isClear = false;
			isTime = false;
			isWatch = false;
		} else if (e.getSource() == StopButton) {
			isStart = false;
			isStop = true;
			isClear = false;
			isTime = false;
			isWatch = false;
		} else if (e.getSource() == ClearButton) {
			isStart = false;
			isStop = false;
			isClear = true;
			isTime = false;
			isWatch = false;
		} else if (e.getSource() == TimeButton) {
			isStart = false;
			isStop = false;
			isClear = false;
			isTime = true;
			isWatch = false;
		} else if (e.getSource() == WatchButton) {
			isStart = false;
			isStop = false;
			isClear = false;
			isTime = false;
			isWatch = true;
		}

	}

	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (isStart == true) {
				clock.setSecond(clock.getSecond() + 1);
				DisplayScreen();
			} else if (isStop == true) {
				DisplayScreen();
			} else if (isClear == true) {
				clock.clearClock();
				DisplayScreen();
			} else if (isTime == true) {
				clock.setCurrentClock();
				DisplayScreen();
			} else if (isWatch == true) {
				clock.clearClock();
				DisplayScreen();
			}
			repaint();

		}
	}
	private void DisplayScreen(){
		Screen.setText(Integer.toString(clock.getHour()) + ":"
				+ Integer.toString(clock.getMinute()) + ":"
				+ Integer.toString(clock.getSecond()));
	}

}
