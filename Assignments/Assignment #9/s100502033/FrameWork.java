package a9.s100502033;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;;
public class FrameWork	extends JFrame
{
	private static Clock clock = new Clock(); 
	private static JPanel p1 = new JPanel();
	private static JButton start = new JButton("START");
	private static JButton stop = new JButton("STOP");
	private static JButton clear = new JButton("CLEAR");
	private static JLabel label = new JLabel("");
	private static Timer timer;
	public FrameWork()
	{
		p1.add(label , BorderLayout.NORTH);
		p1.add(clear , BorderLayout.EAST);
		p1.add(stop , BorderLayout.SOUTH);
		p1.add(start , BorderLayout.WEST);
		add(p1 , BorderLayout.SOUTH);
		add(clock , BorderLayout.CENTER);
		Frame frame = new Frame();
		start.addActionListener(frame);
		stop.addActionListener(frame);
		clear.addActionListener(frame);
		label.setForeground(Color.red);
		clock.clearClock(true);
		label.setText(clock.getHour() + ":" + clock.getMinute() + ":" + clock.getSecond() + '\n');
	}
	class Frame implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			if(event.getSource() == start)//按下START按鈕後
			{
				timer = new Timer(1000 , new TimerListner()); //每一秒執行一次
				timer.start();//開始
				
			}
			else if(event.getSource() == stop)//按下stop按鈕後
			{
				timer.stop();//停止
			}
			else if(event.getSource() == clear)//按下clear按鈕後
			{
				timer.stop();//停止
				clock.clearClock(true);//呼叫CLASS
				label.setText(clock.getHour() + ":" + clock.getMinute() + ":" + clock.getSecond() + '\n');
				repaint(); //重新整埋
			}
		}
	}
	class TimerListner implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			clock.setCurrentClock();
			label.setText(clock.getHour() + ":" + clock.getMinute() + ":" + clock.getSecond() + '\n');
			repaint(); //重新整埋
		}
	}
}
