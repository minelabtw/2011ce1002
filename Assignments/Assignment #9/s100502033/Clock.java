package a9.s100502033;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;
public class Clock extends JPanel
{
	private static int hour;
	private static int minute;
	private static int second;
	public Clock()
	{

	}
	public static void clearClock(boolean string)
	{
		if (string == true)//時間歸零
		{
			hour = 0;
			minute = 0;
			second = 0;
		}
	}
	public static void setCurrentClock()
	{ 
		second++;//秒針海秒加1
		if(second == 60)
		{
			minute++;//當秒針值等於60，分針加1
			second = 0;//秒針歸零
			if(minute == 60)//當分針值等於60，時針加1
			{
				hour++;
				minute = 0;//分針歸零
			}
		}
	}
	public void paintComponent(Graphics g)//畫圓
	{
        int xCenter = getWidth() / 2;  //取得中點
        int yCenter = getHeight() / 2;  
        int radius = (int) (Math.min(this.getWidth(), this.getHeight()) * 0.8 * 0.5);  //取得半徑
        g.drawOval(xCenter - radius, yCenter - radius, radius * 2, radius * 2); //畫圓
        g.setColor(Color.ORANGE);//設定頻色
        g.drawString("12", xCenter - 6, yCenter - radius + 12);  //標示時間
        g.drawString("3", xCenter + radius - 12, yCenter + 4);  
        g.drawString("6", xCenter - 4, yCenter + radius - 8);  
        g.drawString("9", xCenter - radius + 4, yCenter + 6);
        g.setColor(Color.blue);
        g.drawLine(xCenter, yCenter, (int) (xCenter + radius * 0.8 * Math.sin(second * 2 * Math.PI / 60)), (int) (yCenter - radius * 0.8 * Math.cos(second * 2 * Math.PI / 60)));  //秒針
        g.setColor(Color.yellow);
        g.drawLine(xCenter, yCenter, (int) (xCenter + radius * 0.6 * Math.sin(minute * 2 * Math.PI / 60)), (int) (yCenter - radius * 0.6 * Math.cos(minute * 2 * Math.PI / 60)));  //分針
        g.setColor(Color.green);
        g.drawLine(xCenter, yCenter, (int) (xCenter + radius * 0.4 * Math.sin((hour + minute / 60.0) * 2 * Math.PI / 12)), (int) (yCenter - radius * 0.4   * Math.cos((hour + minute / 60.0) * 2 * Math.PI / 12))); //時針 
	}
    public int getHour() {  
        return hour;  
    }  
    public int getMinute() {  
        return minute;  
    }  
    public int getSecond() {  
        return second;  
    }  
}
