package a9.s100502014;
import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
public class Clock extends JPanel {
	private int hr, min, sec, tempSec, totalSec;
	private int cx=270, cy=200, sx, sy;
	private Timer timer = new Timer(100, new TimerListener());
	private boolean hasStart = false; //whether timer start
	public Clock() {
		clearClock();
	}
	public void paintComponent(Graphics g) { //to draw the clock and second hand
		super.paintComponent(g);
		g.setColor(Color.BLUE);
		g.drawOval(120, 50, 300, 300);
		g.setColor(Color.RED);
		g.drawLine(sx, sy, cx, cy);
		g.setColor(Color.BLACK);
		g.setFont(new Font("SansSerif", Font.BOLD, 50));
		g.drawString(""+(totalSec+tempSec)+" sec.", 200, 450);
	}
	public void clearClock() { // to reset the time
		totalSec = 0;
		tempSec = 0;
		sx = 270;
		sy = 75;
		repaint();
		timer.stop();
		hasStart = false; //timer clears, so it doesn't start
	}
	public void setCurrentClock() { // to set the current time
		Calendar c = new GregorianCalendar();
		hr = c.get(Calendar.HOUR_OF_DAY);
		min = c.get(Calendar.MINUTE);
		sec = c.get(Calendar.SECOND);
	}
	public void start() {
		setCurrentClock();
		timer.start();
		hasStart = true; //timer starts
	}
	public void stop() {
		timer.stop();
		totalSec += tempSec;
		hasStart = false; //timer stops, so it doesn't start
	}
	public boolean getHasStart() {
		return hasStart;
	}
	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Calendar c = new GregorianCalendar();
			tempSec = (c.get(Calendar.HOUR_OF_DAY)-hr)*3600+(c.get(Calendar.MINUTE)-min)*60+(c.get(Calendar.SECOND)-sec);
			sx = cx - (int)(125*Math.sin((double)(totalSec+tempSec)*Math.PI/(-30.0)));
			sy = cy - (int)(125*Math.cos((double)(totalSec+tempSec)*Math.PI/(-30.0)));
			repaint();
		}
	}
}
