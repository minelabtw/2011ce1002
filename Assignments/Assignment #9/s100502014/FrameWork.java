package a9.s100502014;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class FrameWork extends JFrame {
	private Clock clock = new Clock();
	private Button start = new Button("Start");
	private Button stop = new Button("Stop");
	private Button clear = new Button("Clear");
	public FrameWork() {
		add(clock);
		
		//button panel
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(1,3));
		panel.add(start);
		panel.add(stop);
		panel.add(clear);
		
		add(panel, BorderLayout.SOUTH);
		start.addActionListener(new ButtonListener());
		stop.addActionListener(new ButtonListener());
		clear.addActionListener(new ButtonListener());
	}
	class ButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == start) {
				if(!clock.getHasStart()) //if timer doesn't start
					clock.start();
			}
			else if(e.getSource() == stop) {
				clock.stop();
			}
			else if(e.getSource() == clear) {
				clock.clearClock();
			}
		}
	}
}
