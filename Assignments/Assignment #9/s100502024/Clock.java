package a9.s100502024;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
public class Clock extends JPanel
{
	protected int hour;
	protected int minute;
	protected int second;
	public Clock()
	{
		hour = 0; // ﹍て0
		minute = 0;
		second = 0;
	}
	public void startClock()
	{
		if(second < 59) // 璝计 < 59  玥尿糤计
		{
			second++;
		}
		else // ぃ琌玥计耴0,だ皐+1
		{
			second = 0; 
			minute++;
		}
		if(minute > 59)
		{
			minute = 0;
			hour++;
		}
	}
	public void clearClock() // hour,minute,second场砞
	{
		hour = 0;
		minute = 0;
		second = 0;
	}
	public void setCurrentClock() // 眔╰参丁
	{
		Calendar calendar = new GregorianCalendar();
		this.hour = calendar.get(Calendar.HOUR_OF_DAY);
		this.minute = calendar.get(Calendar.MINUTE);
		this.second = calendar.get(Calendar.SECOND);
	}
	public void paintComponent(Graphics g) // 礶瓜
	{
		super.paintComponent(g);
		int clockRadius = (int)(Math.min(getWidth(),getHeight())*0.8*0.5);
		int xCenter = getWidth()/2;
		int yCenter = getHeight()/2;
		g.setColor(Color.BLACK);
		g.drawOval(xCenter-clockRadius,yCenter-clockRadius,2*clockRadius,2*clockRadius);
		g.drawString("12",xCenter-5,yCenter-clockRadius+12);
		g.drawString("9",xCenter-clockRadius+3,yCenter+5);
		g.drawString("3",xCenter+clockRadius-10,yCenter+3);
		g.drawString("6",xCenter-3,yCenter+clockRadius-3);
		
		int sLength = (int)(clockRadius*0.8);
		int xSecond = (int)(xCenter+sLength*Math.sin(second*(2*Math.PI/60)));
		int ySecond = (int)(yCenter-sLength*Math.cos(second*(2*Math.PI/60)));
		g.setColor(Color.red);
		g.drawLine(xCenter,yCenter,xSecond,ySecond);
		
		int mLength = (int)(clockRadius*0.65);
		int xMinute = (int)(xCenter+mLength*Math.sin(minute*(2*Math.PI/60)));
		int yMinute = (int)(yCenter-mLength*Math.cos(minute*(2*Math.PI/60)));
		g.setColor(Color.blue);
		g.drawLine(xCenter,yCenter,xMinute,yMinute);
		
		int hLength = (int)(clockRadius*0.5);
		int xHour = (int)(xCenter+hLength*Math.sin((hour%12+minute/60)*(2*Math.PI/12)));
		int yHour = (int)(yCenter-hLength*Math.cos((hour%12+minute/60)*(2*Math.PI/12)));
		g.setColor(Color.green);
		g.drawLine(xCenter,yCenter,xHour,yHour);
	}
}
