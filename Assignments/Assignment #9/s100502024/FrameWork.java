package a9.s100502024;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Calendar;
public class FrameWork extends JFrame implements ActionListener
{
	private JPanel p1 = new JPanel();
	private JButton start = new JButton("Start");
	private JButton stop = new JButton("Stop");
	private JButton clear = new JButton("Clear");
	private JButton set = new JButton("CurrentTime");
	private JLabel screen = new JLabel();
	private Font font = new Font("SansSerif",Font.BOLD,19);
	private Clock clock = new Clock();
	private Timer timer_cur = new Timer(1000,new TimerListenersetTime()); // 每1秒 call 一次TimerListenersetTime
	private Timer timerstart = new Timer(1000,new TimerListenerClock()); // 每1秒 call 一次TimerListenerClock
	public FrameWork()
	{
		p1.setLayout(new GridLayout(1,5));
		p1.add(screen);
		p1.add(start);
		p1.add(stop);
		p1.add(clear);
		p1.add(set);
		
		setLayout(new BorderLayout());
		add(clock,BorderLayout.CENTER); // 排版
		add(p1,BorderLayout.SOUTH);
		start.addActionListener(this);
		stop.addActionListener(this);
		clear.addActionListener(this);
		set.addActionListener(this);
	}
	class TimerListenersetTime implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			clock.setCurrentClock();
			screen.setFont(font);
			screen.setText("   "+Integer.toString(clock.hour)+":"+Integer.toString(clock.minute)+":"+Integer.toString(clock.second)); // 顯示系統現在時間
			clock.repaint();
		}
	}
	class TimerListenerClock implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			clock.startClock();
			screen.setFont(font);
			screen.setText("      "+Integer.toString(clock.hour)+":"+Integer.toString(clock.minute)+":"+Integer.toString(clock.second)); // 顯示目前碼表時間
			clock.repaint();
		}
	}
	public void actionPerformed(ActionEvent e) // 當按下Button時要做的事情
	{
		if(e.getSource() == start)
		{
			if(timer_cur.isRunning() == true)
			{
				timer_cur.stop(); // 暫停顯示系統時間
				clock.clearClock(); // 歸0
				timerstart.start(); // 開始跑碼表時間
			}
			else
			{
				timerstart.start();
			}
		}
		if(e.getSource() == stop)
		{
			timerstart.stop(); // 暫停碼表時間
		}
		if(e.getSource() == clear)
		{
			timer_cur.stop();
			clock.clearClock();
			timerstart.stop();
			screen.setFont(font);
			screen.setText("      "+Integer.toString(clock.hour)+":"+Integer.toString(clock.minute)+":"+Integer.toString(clock.second)); // 顯示歸0
			clock.repaint();
		}
		if(e.getSource() == set)
		{
			timerstart.stop(); // 暫停碼表時間
			timer_cur.start(); // 開始顯示系統時間
		}
	}
}
