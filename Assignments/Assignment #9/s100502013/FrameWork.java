package a9.s100502013;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;


public class FrameWork extends JFrame implements ActionListener{
	private JLabel screen = new JLabel();
	private JPanel total = new JPanel(new GridLayout(2,1));
	private Clock myclock = new Clock();
	private JPanel scrbut = new JPanel(new GridLayout(2,1));
	private JPanel buttonset = new JPanel(new GridLayout(1,3));
	private JButton start = new JButton("Start");
	private JButton stop = new JButton("Stop");
	private JButton clear = new JButton("Clear");
	private Font butfont = new Font("TimesRoman",Font.BOLD,20);
	private Font timefont = new Font("TimesRoman",Font.BOLD,32);
	private Timer timer = new Timer(1000,new TimerListener());
	private String timestring = "00:00:00";
	
	FrameWork(){ //constructor: set panels ,buttons ,and something
		scrbut.setBackground(Color.WHITE);
		myclock.setBackground(Color.WHITE);
		start.setFont(butfont);
		stop.setFont(butfont);
		clear.setFont(butfont);
		screen.setHorizontalAlignment(JTextField.CENTER);
		screen.setFont(timefont);
		buttonset.add(start);
		buttonset.add(stop);
		buttonset.add(clear);
		scrbut.add(screen);
		screen.setText(timestring);
		scrbut.add(buttonset);
		total.add(myclock);
		total.add(scrbut);
		add(total);
		start.addActionListener(this);
		stop.addActionListener(this);
		clear.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e){
		if(e.getSource()==start){ //push start: timer start
			timer.start();
		}
		else if(e.getSource()==stop){ //push stop: timer stop
			timer.stop();
		}
		else if(e.getSource()==clear){ //push clear: timer stop and reset the clock to 00:00:00
			timer.stop();
			myclock.clearClock();
			screen.setText("00:00:00");
			myclock.repaint();
		}
	}
	
	class TimerListener implements ActionListener{ //timer listener
		public void actionPerformed(ActionEvent e){
			if(myclock.getsecond()<60){ //set time in currect form
				myclock.setCurrentClock(myclock.getsecond()+1,myclock.getminute(),myclock.gethour());
				if(myclock.getsecond()==60){
					myclock.setCurrentClock(0,myclock.getminute()+1,myclock.gethour());
					if(myclock.getminute()==60){
						myclock.setCurrentClock(0,0,myclock.gethour()+1);
					}
				}
			}
			timestring = ""; 
			if(myclock.gethour()<10){
				timestring += "0";
			}
			timestring += myclock.gethour() + ":";
			if(myclock.getminute()<10){
				timestring += "0";
			}
			timestring += myclock.getminute() + ":";
			if(myclock.getsecond()<10){
				timestring += "0";
			}
			timestring += myclock.getsecond();
			screen.setText(timestring); //set the screen label output xx:xx:xx
			myclock.repaint();
		}
	}
}
