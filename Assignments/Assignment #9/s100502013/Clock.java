package a9.s100502013;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.util.*;

public class Clock extends JPanel{
	private int hour = 0;
	private int minute = 0;
	private int second = 0;
	
	Clock(){
		
	}
	
	public void clearClock(){
		setCurrentClock(0,0,0); //set the time to 00:00:00
	}
	
	public void setCurrentClock(int x,int y,int z){ //set the time
		second = x;
		minute = y;
		hour = z;
	}
	
	public int gethour(){
		return hour;
	}
	
	public int getminute(){
		return minute;
	}
	
	public int getsecond(){
		return second;
	}
	
	public void paintComponent(Graphics g){ //paint the clock
		super.paintComponent(g);
		int radius = (int)(Math.min(getWidth(),getHeight()/2)*0.7);
		int xcent = (int)(getWidth()/2);
		int ycent = (int)(getHeight()/2);
		int seclen = (int)(radius*0.8);
		int xsec = (int)(xcent + seclen * Math.sin(second * (2 * Math.PI / 60)));
		int ysec = (int)(ycent - seclen * Math.cos(second * (2 * Math.PI / 60)));
		
		g.drawOval(xcent-radius, ycent-radius, 2*radius, 2*radius); //the circle
		g.setColor(Color.RED);
		g.drawLine(xcent, ycent, xsec, ysec); //second hand
		g.setColor(Color.BLACK);
		g.drawString("12", xcent-6, ycent-radius);
		g.drawString("3", xcent+radius, ycent+3);
		g.drawString("6", xcent-3, ycent+radius+12);
		g.drawString("9", xcent-radius-6, ycent+3);
	}
}
