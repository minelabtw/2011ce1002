package a9.s100502026;

import java.awt.*;
import java.util.*;
import java.util.Timer;

import javax.swing.*;
import java.awt.event.*;

public class Clock extends JPanel{
	
	private int hour;
	private int minute;
	private int second;
	
	Clock()
	{
		clearClock();
	}
	
	public int getHour()
	{
		return hour;
	}
	
	public int getMinute()
	{
		return minute;
	}
	
	public int getSecond()
	{
		return second;
	}
	
	public void setHour(int hour)
	{
		this.hour=hour;
	}
	
	public void setMinute(int minute)
	{
		this.minute=minute;
	}
	
	public void setSecond(int second)
	{
		this.second=second;
	}
	
	public void clearClock()
	{
		hour=0;
		minute=0;
		second=0;
	}
	
	public void setCurrentTime()
	{
		Calendar calendar=new GregorianCalendar();
		
		this.hour=calendar.get(Calendar.HOUR_OF_DAY);
		this.minute=calendar.get(Calendar.MINUTE);
		this.second=calendar.get(Calendar.SECOND);
	}

	protected void paintComponent(Graphics g)
	{
		int clockRadius=(int)Math.min(getWidth(), getHeight());
		int xCenter=getWidth()/2;
		int yCenter=getHeight()/2;
		int sLength=(int)(clockRadius*0.8);
		int xSecond=(int)(xCenter+sLength*Math.sin(second*2*Math.PI/60));
		int ySecond=(int)(yCenter-sLength*Math.cos(second*2*Math.PI/60));
		
		super.paintComponent(g);
		
		second=second+1;
		
		g.drawOval(xCenter-clockRadius,yCenter-clockRadius,clockRadius*2,clockRadius*2);		
		g.drawLine(xCenter,yCenter,xSecond,ySecond);
	}
}
