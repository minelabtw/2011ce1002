package a9.s100502026;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrameWork extends JFrame {

	//Declare
	private JPanel graphicPanel=new JPanel();
	private JPanel controlPanel=new JPanel();
	private JButton jbtStart=new JButton("Start");
	private JButton jbtStop=new JButton("Stop");
	private JButton jbtClear=new JButton("Clear");
	private Clock clock=new Clock();
	private ButtonListener listener=new ButtonListener();
	private Timer timer=new Timer(1000,new TimerListener());
	
	FrameWork()
	{
		setLayout(new BorderLayout(5,5));
		
		jbtStop.addActionListener(listener);
		jbtStart.addActionListener(listener);
		jbtClear.addActionListener(listener);
		
		graphicPanel.setSize(500, 200);
		
		controlPanel.add(jbtClear);
		controlPanel.add(jbtStart);
		controlPanel.add(jbtStop);
		//graphicPanel.add(new Clock());
		
		//graphicPanel.setSize(500,400);
		
		add(new Clock(),BorderLayout.NORTH);
		add(controlPanel,BorderLayout.SOUTH);
	}
	
	class ButtonListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(e.getSource()==jbtStart)
			{				
				timer.start();
				clock.repaint();
			}
			else if(e.getSource()==jbtStop)
			{
				int tempSecond=clock.getSecond();
				
				timer.stop();
				
				JOptionPane.showMessageDialog(null, "It's been "+tempSecond+" seconds.");
			}
			else if(e.getSource()==jbtClear)
			{
				clock.clearClock();
			}
		}
	}
	
	class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			repaint();
		}
	}
}
