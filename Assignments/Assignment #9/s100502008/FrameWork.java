package a9.s100502008;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener {
	JButton btstart = new JButton("Start");
	JButton btstop = new JButton("Stop");
	JButton btclear = new JButton("Clear");
	Clock clock = new Clock();
	JLabel time = new JLabel();
	int count=0;
	Timer timer = new Timer(1000, new TimerListener());
	Timer inittimer = new Timer(1000,new initTimerListener());
	public FrameWork() {//draw button
		
		JPanel p = new JPanel(new GridLayout(1, 3));
		p.add(btstart);
		p.add(btstop);
		p.add(btclear);
		JPanel p0 = new JPanel();
		p0.add(time);
		time.setText("Time Now : "+clock.hour+" : "+clock.min+" : "+clock.sec);
		JPanel p1 = new JPanel(new GridLayout(2, 1));
		p1.add(p0);
		p1.add(p);
		JPanel p2 = new JPanel(new BorderLayout());
		p2.add(clock, BorderLayout.CENTER);
		p2.add(p1, BorderLayout.SOUTH);
		add(p2);
		btstart.addActionListener(this);
		btstop.addActionListener(this);
		btclear.addActionListener(this);
		inittimer.start();
	}
	
	class initTimerListener implements ActionListener {//current time
		public void actionPerformed(ActionEvent e) {
			clock.setCurrentClock();
			time.setText("Time Now : "+clock.hour+" : "+clock.min+" : "+clock.sec);
		}
	}
	
	class TimerListener implements ActionListener {//timer
		public void actionPerformed(ActionEvent e) {
			time.setText(clock.hour+" hour "+clock.min+" min "+clock.sec+" sec");
			repaint();
		}
	}
	
	public void actionPerformed(ActionEvent e) {//button event
		if (e.getSource() == btstart) {
			if(count==0)
			{
				inittimer.stop();
				clock.clearClock();
				repaint();
				count++;
			}
			timer.start();	
		}
		if (e.getSource() == btstop) {
			timer.stop();
		}
		if (e.getSource() == btclear) {
			clock.clearClock();
			repaint();
			time.setText(clock.hour+" hour "+clock.min+" min "+clock.sec+" sec");
		}
	}
}
