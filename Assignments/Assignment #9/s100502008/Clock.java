package a9.s100502008;

import java.awt.Graphics;
import java.util.*;
import javax.swing.JPanel;

public class Clock extends JPanel{
	int hour=0,min=0,sec=0;
	double arc =-1*Math.PI / 30;;
	int count=0;
	public Clock()//constructor
	{
		setCurrentClock();
	} 
	protected void paintComponent(Graphics g) {//paint
		double x1, y1;
		super.paintComponent(g);
		int x = getWidth();
		int y = getHeight();
		x1 = x / 2 + 80 * Math.sin(arc);
		y1 = 120 - 80 * Math.cos(arc);
		if(count==0)
		{
			count++;
		}
		else//time
		{
			if(min==59&&sec==59)
			{
				sec=0;
				min=0;
				hour++;
			}	
			if(sec==59)
			{
				sec=0;
				min++;
			}
			else
			{
				sec++;
			}
		}
		g.drawOval(x / 2 - 100, 20, 200, 200);//clock
		g.drawString("12", x / 2 - 8, 40);
		g.drawString("3", x / 2 + 90, 120);
		g.drawString("6", x / 2 - 2, 215);
		g.drawString("9", x / 2 - 94, 120);
		g.drawLine(x / 2, 120, (int) x1, (int) y1);//second hand
		arc += Math.PI / 30;
	}
	
	void clearClock()//clear
	{
		hour=0;
		min=0;
		sec=0;
		arc=0;
	}
	void setCurrentClock()//current time
	{
		Calendar calendar = new GregorianCalendar();
		hour=calendar.get(Calendar.HOUR_OF_DAY);
		min=calendar.get(Calendar.MINUTE);
		sec=calendar.get(Calendar.SECOND);
	}
	
}
