package a9.s100502513;

import java.awt.*;
import java.util.*;
import javax.swing.*;

public class Clock extends JPanel {
	private int hour, minute, second;

	public Clock() { // 預設開始時間為零
		this.hour = 0;
		this.minute = 0;
		this.second = 0;
	}

	public void clearClock() { // 清除時間
		this.hour = 0;
		this.minute = 0;
		this.second = 0;
		repaint();
	}

	public void setCurrentClock() { // 設定目前時間
		this.second++; // 每秒second+1
		this.minute = this.second / 60;
		this.hour = this.second / 3600;
		repaint();
	}

	public int getsecond() {  //get秒
		return (this.second%60);
	}

	public int getminute() {  //get分
		return (this.minute%60);
	}

	public int gethour() {  //get時
		return this.hour;
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		int r = (int) (Math.min(getWidth(), getHeight() * 0.3)); // 時鐘半徑
		int x = getWidth() / 2; // 時鐘中心
		int y = getHeight() / 2;

		g.setColor(Color.BLACK); // 時鐘顏色
		g.setFont(new Font("Cambria", Font.BOLD + Font.ITALIC, 50)); // 數字字體
		g.fillOval(x - r, y - r, r * 2, r * 2); // 時鐘大小
		g.setColor(Color.WHITE); // 數字顏色
		g.drawString("12", x - 25, y - r + 50); // 數字
		g.drawString("1", x + r / 2 - 20, y - r * 2 / 3 + 10);
		g.drawString("2", x + r * 2 / 3 + 5, y - r / 2 + 30);
		g.drawString("3", x + r - 40, y + 20);
		g.drawString("4", x + r * 2 / 3 + 5, y + r / 2 + 10);
		g.drawString("5", x + r / 2 - 30, y + r * 2 / 3 + 35);
		g.drawString("6", x - 15, y + r - 10);
		g.drawString("7", x - r * 2 / 3 + 30, y + r * 2 / 3 + 30);
		g.drawString("8", x - r / 2 - 60, y + r / 2);
		g.drawString("9", x - r + 10, y + 15);
		g.drawString("10", x - r * 2 / 3 - 30, y - r / 2 + 35);
		g.drawString("11", x - r / 2 - 10, y - r * 2 / 3 + 10);

		int slength = (int) (r * 0.8); // 秒針
		int sx = (int) (x + slength
				* Math.sin(this.second * (2 * Math.PI / 60)));
		int sy = (int) (y - slength
				* Math.cos(this.second * (2 * Math.PI / 60)));
		g.setColor(Color.WHITE);
		g.drawLine(x, y, sx, sy);

		int mlength = (int) (r * 0.6); // 分針
		int mx = (int) (x + mlength
				* Math.sin((this.minute % 60 + (this.second % 60) / 60.0)
						* (2 * Math.PI / 60)));
		int my = (int) (y - mlength
				* Math.cos((this.minute % 60 + (this.second % 60) / 60.0)
						* (2 * Math.PI / 60)));
		g.setColor(Color.WHITE);
		g.drawLine(x, y, mx, my);

		int hlength = (int) (r * 0.5); // 時針
		int hx = (int) (x + hlength
				* Math.sin((this.hour % 12 + (this.minute % 60) / 60.0)
						* (2 * Math.PI / 12)));
		int hy = (int) (y - hlength
				* Math.cos((this.hour % 12 + (this.minute % 60) / 60.0)
						* (2 * Math.PI / 12)));
		g.setColor(Color.WHITE);
		g.drawLine(x, y, hx, hy);

	}
	
	
}
