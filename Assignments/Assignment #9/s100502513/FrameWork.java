package a9.s100502513;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener {
	private JPanel screen;
	private JPanel p2 = new JPanel();
	private JPanel p3 = new JPanel();
	private JButton start;
	private JButton stop;
	private JButton clear;
	private JLabel[] ltime = new JLabel[5];
	private String[] stime = new String[3];
	private Clock c = new Clock();
	private Timer timer = new Timer(1000, new TimeListener());;

	public FrameWork() {
		screen = c; // 時鐘
		screen.setBackground(Color.WHITE);

		p2.setLayout(new GridLayout(1, 3));
		start = new JButton("START"); // 開始按鈕
		start.setBackground(Color.BLACK);
		start.setFont(new Font("Cambria", Font.BOLD + Font.ITALIC, 40));
		start.setForeground(Color.WHITE);
		start.setBorder(new LineBorder(Color.WHITE, 3));
		p2.add(start);
		stop = new JButton("STOP"); // 停止按鈕
		stop.setBackground(Color.BLACK);
		stop.setFont(new Font("Cambria", Font.BOLD + Font.ITALIC, 40));
		stop.setForeground(Color.WHITE);
		stop.setBorder(new LineBorder(Color.WHITE, 3));
		p2.add(stop);
		clear = new JButton("CLEAR"); // 清除按鈕
		clear.setBackground(Color.BLACK);
		clear.setFont(new Font("Cambria", Font.BOLD + Font.ITALIC, 40));
		clear.setForeground(Color.WHITE);
		clear.setBorder(new LineBorder(Color.WHITE, 3));
		p2.add(clear);

		stime[0] = "     00";
		stime[1] = "   00";
		stime[2] = "00";

		p3.setLayout(new GridLayout(1, 5)); // 上方數位時鐘
		p3.add(ltime[0] = new JLabel(stime[0]));
		p3.add(ltime[1] = new JLabel("    :"));
		p3.add(ltime[2] = new JLabel(stime[1]));
		p3.add(ltime[3] = new JLabel("    :"));
		p3.add(ltime[4] = new JLabel(stime[2]));
		p3.setBackground(Color.BLACK);
		p3.setBorder(new LineBorder(Color.WHITE, 3));
		for (int i = 0; i < 5; i++) {
			ltime[i].setFont(new Font("Cambria", Font.BOLD + Font.ITALIC, 40));
			ltime[i].setForeground(Color.WHITE);
		}

		setLayout(new BorderLayout());

		add(screen, BorderLayout.CENTER);
		add(p3, BorderLayout.NORTH);
		add(p2, BorderLayout.SOUTH);

		start.addActionListener(this); // 讓按鈕作用
		stop.addActionListener(this);
		clear.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == start) {
			timer.start(); // 時鐘開始
		} else if (e.getSource() == stop) {
			timer.stop(); // 時鐘停止
		} else if (e.getSource() == clear) {
			c.clearClock(); // 歸零
			ltime[0].setText(stime[0] = "     " + c.gethour());
			ltime[2].setText(stime[1] = "   " + c.getminute());
			ltime[4].setText(stime[2] = "" + c.getsecond());
			timer.stop();
		}

	}

	class TimeListener implements ActionListener { // 時間動的時候的動作
		public void actionPerformed(ActionEvent e) {
			c.setCurrentClock();
			ltime[0].setText(stime[0] = "     " + c.gethour());
			ltime[2].setText(stime[1] = "   " + c.getminute());
			ltime[4].setText(stime[2] = "" + c.getsecond());
		}

	}

}
