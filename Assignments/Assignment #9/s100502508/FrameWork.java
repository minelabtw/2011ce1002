package a9.s100502508;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

public class FrameWork extends JFrame implements ActionListener
{
	private JButton button1=new JButton("Start");//create buttons
	private JButton button2=new JButton("Stop");
	private JButton button3=new JButton("Clear");
	
	Timer timer=new Timer(1000,new TimerListener());//create timer
	JLabel time;//create label
	Clock clock=new Clock();//create a Clock object
	Font font=new Font("SansSerif",Font.BOLD,20);//create a font
	
	public FrameWork()
	{
		time=new JLabel("0'0'0");//initial label
		time.setFont(font);//set font
		JPanel p1=new JPanel(new GridLayout(1, 4, 0, 0));//create a panel
		
		p1.add(time);//add label to panel
		p1.add(button1);//add buttons into panel
		p1.add(button2);
		p1.add(button3);
		
		add(clock,BorderLayout.CENTER);
		add(p1,BorderLayout.SOUTH);
		
		button1.addActionListener(this);//active buttons event here
		button2.addActionListener(this);
		button3.addActionListener(this);
		
	}
	
	
	class TimerListener implements ActionListener//Handle ActionEvent
	{
		public void actionPerformed(ActionEvent e)//event handler
		{
			int second=1;
			second+=clock.getsecond();
			clock.setCurrentClock(second);
			time.setText(clock.gethour()+"'"+clock.getminute()+"'"+clock.getsecond());
			time.setFont(font);
			repaint();//repaint panel
		}
	}
	
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource()==button1)//press the start button
		{
			timer.start();//start timer
		}
		else if(e.getSource()==button2)//press the stop button
		{
			timer.stop();//stop timer
		}
		else //press the clean button
		{
			clock.clearClock();//call clearnClock method
			timer.stop();//stop timer
			time.setText(clock.gethour()+"'"+clock.getminute()+"'"+clock.getsecond());//call gethour, getminute and getsecond method
			time.setFont(font);
			repaint();//repaint panel
		}
	}
}
