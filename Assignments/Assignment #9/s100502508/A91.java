package a9.s100502508;

import javax.swing.JFrame;

public class A91 
{
	public static void main(String[] args)
	{
		FrameWork f=new FrameWork();//create a frame
		f.setTitle("Clock");//set the frame title
		f.setSize(500, 500);//set the frame size
		f.setLocationRelativeTo(null);//center a frame
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);//display the frame
	}//end main
}
