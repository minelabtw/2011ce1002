package a9.s100502508;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.JPanel;

public class Clock extends JPanel//new panel class
{
	private int hour;
	private int minute;
	private int second;
	
	public Clock()//initialize the hour, minute and second 
	{
		hour=0;
		minute=0;
		second=0;
	}
	public void clearClock()//make the hour, minute and second initialize
	{
		hour=0;
		minute=0;
		second=0;
	}
	public void setCurrentClock(int second)
	{
		if(second==60)//60 secomds equal 1 minute
		{
			minute++;
			second=0;
			
		}
		this.second=second;
		if(minute==60)//60 minutes equal 1 hour
		{
			hour++;
			minute=0;
		}
	}
	protected void paintComponent(Graphics g)//override paintComponent
	{
		super.paintComponent(g);//draw things in the superclass
		g.setColor(Color.white);//set white color
		g.fillOval(0, 0, getWidth(), getHeight());//draw a circle
		g.setColor(Color.black);//set black color
		g.drawLine(getWidth()/2, getHeight()/2, (int)((getWidth()/2+(getWidth()/3*Math.sin(second*Math.PI/30)))), (int)((getHeight()/2-(getWidth()/3*Math.cos(second*Math.PI/30)))));//draw the second hand
		g.drawString("12", (getWidth()/2)-1, 10);
		g.drawString("3", 5, getHeight()/2);
		g.drawString("6", getWidth()/2, getHeight()-5);
		g.drawString("9", getWidth()-10, getHeight()/2);
		g.fillOval((getWidth()/2)-4, (getHeight()/2)-4, 8, 8);
		g.drawOval(0, 0, getWidth(), getHeight());//draw the outline
	}
	public int gethour()//get the hour
	{
		return hour;
	}
	public int getminute()//get the minute
	{
		return minute;
	}
	public int getsecond()//get the second
	{
		return second;
	}
}
