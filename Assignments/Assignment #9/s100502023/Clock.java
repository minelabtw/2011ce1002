package a9.s100502023;

import java.awt.*;
import java.util.*;

import javax.swing.*;

public class Clock extends JPanel
{
	protected int hour=0;
	protected int minute=0;
	protected int second=0;
	
	
	
	public void clearClock()  //clear
	{
		hour=0;
		minute=0;
		second=0;
	}
	public void setCurrentClock()  //current time
	{
		Calendar calendar = new GregorianCalendar();
		this.hour=calendar.get(Calendar.HOUR_OF_DAY);
		this.minute=calendar.get(Calendar.MINUTE);
		this.second=calendar.get(Calendar.SECOND);
		
	}
	public void paintComponent(Graphics g)  //paint
	{
		super.paintComponent(g);
		
		//時間設定
		if(second>59)
		{
			second=0;
			minute+=1;
			
		}
		if(minute>60)
		{
			minute=0;
			hour+=1;
			
		}
		if(hour>24)
		{
			hour=1;
		}
		
		int clockRadius = (int)(Math.min(getWidth(),getHeight())*0.8*0.5);
		int xCenter = getWidth()/2;
		int yCenter = getHeight()/2;
		//draw
		//鐘面:3,6,9,12
		g.setColor(Color.BLACK);
		g.drawOval(xCenter-clockRadius, yCenter-clockRadius, 2*clockRadius, 2*clockRadius);
		g.drawString("12",xCenter-5,yCenter-clockRadius+12);
		g.drawString("3",xCenter+clockRadius-10,yCenter+3);
		g.drawString("6",xCenter-3,yCenter+clockRadius-3);
		g.drawString("9",xCenter-clockRadius+3,yCenter+5);
		
		//秒針
		int sLength = (int)(clockRadius*0.8);
		int xSecond = (int)(xCenter+sLength*Math.sin(second*(2*Math.PI/60)));
		int ySecond = (int)(yCenter-sLength*Math.cos(second*(2*Math.PI/60)));
		g.setColor(Color.red);
		g.drawLine(xCenter, yCenter, xSecond, ySecond);
		
		//分針
		int mLength = (int)(clockRadius*0.65);
		int xMinute = (int)(xCenter+mLength*Math.sin(minute*(2*Math.PI/60)));
		int yMinute = (int)(yCenter-mLength*Math.cos(minute*(2*Math.PI/60)));
		g.setColor(Color.blue);
		g.drawLine(xCenter, yCenter, xMinute, yMinute);
		
		//時針
		int hLength = (int)(clockRadius*0.5);
		int xHour = (int)(xCenter+hLength*Math.sin(hour*(2*Math.PI/12)));
		int yHour = (int)(yCenter-hLength*Math.cos(hour*(2*Math.PI/12)));
		g.setColor(Color.green);
		g.drawLine(xCenter, yCenter, xHour, yHour);
	}
}
