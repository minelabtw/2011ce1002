package a9.s100502023;

import java.awt.*;
import java.awt.event.*;


import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener
{
	//buttons
	private JButton start = new JButton("start");
	private JButton stop = new JButton("stop");
	private JButton clear = new JButton("clear");
	private JButton b_currentTime = new JButton("showCurrentTime");
	//label
	private JLabel originTime = new JLabel("起始時間:");//"起始時間"
	private JLabel message_originTime = new JLabel();//顯示現在時間
	private JLabel count = new JLabel("計時:");//"計時"
	private JLabel message_count = new JLabel();//顯示計時
	//panel
	private JPanel p1 = new JPanel();//add clock
	private JPanel p2 = new JPanel();//add currenTime,count
	private JPanel p3 = new JPanel();//add buttons
	private JPanel p4 = new JPanel();//add p1,p2
	//clock
	private Clock clock = new Clock();
	
	//起始時間
	private int origin_hour=0;
	private int origin_minute=0;
	private int origin_second=0;

	//timer listener
	TimerListener listener = new TimerListener();
	//timer
	Timer timer = new Timer(1000,listener);
	//判斷是否按壓b_currentTime button
	boolean is_press_b_currentTime=false;
	
	public FrameWork()
	{
		//set panel
		p1.setLayout(new GridLayout(1,1,10,10));
		p1.add(clock);
		
		p2.setLayout(new GridLayout(4,1,10,10));
		p2.add(originTime);
		p2.add(message_originTime);
		p2.add(count);
		p2.add(message_count);
		
		p4.setLayout(new GridLayout(1,2,10,10));
		p4.add(p1);
		p4.add(p2);
		
		p3.setLayout(new GridLayout(1,4,10,10));
		p3.add(start);
		p3.add(stop);
		p3.add(clear);
		p3.add(b_currentTime);
		
		//ser frame
		setLayout(new BorderLayout(10,10));
		
		add(p4,BorderLayout.CENTER);
		add(p3,BorderLayout.SOUTH);
		
		
		//add listener to buttons
		start.addActionListener(this);
		stop.addActionListener(this);
		clear.addActionListener(this);
		b_currentTime.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()==start)  //按下start button
		{
			if(is_press_b_currentTime==true)
			{
				clock.hour=0;
				clock.minute=0;
				clock.second=0;
			}
			is_press_b_currentTime=false;
			timer.start();
		}
		else if(e.getSource()==stop)  //按下stop button
		{
			is_press_b_currentTime=false;
			timer.stop();
		}
		else if(e.getSource()==clear)  //按下clear button
		{
			timer.stop();
			clock.clearClock();
			clock.repaint();
			message_originTime.setText("");
			message_count.setText("");
			origin_hour=0;
			origin_minute=0;
			origin_second=0;
			is_press_b_currentTime=false;
			
		}
		else if(e.getSource()==b_currentTime)  //按下showCurrentTime button
		{
			is_press_b_currentTime=true;
			timer.start();
			clock.setCurrentClock();
			clock.repaint();
			origin_hour=clock.hour;
			origin_minute=clock.minute;
			origin_second=clock.second;
			message_originTime.setText(origin_hour+" : " + origin_minute + " : " + origin_second);
			message_count.setText("");
			
		}
	}
	class TimerListener implements ActionListener  //listener
	{
		public void actionPerformed(ActionEvent e)
		{
			
			if(is_press_b_currentTime==true)  //當按壓is_press_b_currentTime時
			{
				clock.second+=1;
				clock.repaint();
				message_originTime.setText(clock.hour+" : " + clock.minute + " : " + clock.second);
			}
			else
			{
				clock.second+=1;
				clock.repaint();
				message_count.setText(clock.hour+" : " + clock.minute + " : " + clock.second);
			}
		}
	}
	
}
