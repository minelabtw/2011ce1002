package a9.s100502015;

import java.awt.*;
import javax.swing.*;

import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener {
	private boolean bstart = false;
	private boolean bstop = false;
	private boolean bclear = false;
	private boolean btime = false;
	private Clock c1 = new Clock();
	private JPanel p1 = new JPanel();
	private JPanel p2 = new JPanel();
	private JPanel p3 = new JPanel();
	private JLabel l1 = new JLabel(String.valueOf(c1.hour) + ":"
			+ String.valueOf(c1.minute) + ":" + String.valueOf(c1.second));
	private JButton start = new JButton("start");
	private JButton stop = new JButton("stop");
	private JButton clear = new JButton("clear");
	private JButton time = new JButton("current time");

	public FrameWork() {
		p3.setLayout(new GridLayout(1, 4));
		p3.add(start);// button
		p3.add(stop);
		p3.add(clear);
		p3.add(time);
		p2.setLayout(new GridLayout(2, 1));
		p2.add(l1);// screen
		p2.add(p3);
		p1.setLayout(new GridLayout(1, 2));
		p1.add(c1);// clock
		p1.add(p2);
		add(p1);
		start.addActionListener(this);
		stop.addActionListener(this);
		clear.addActionListener(this);
		time.addActionListener(this);
		Timer timer = new Timer(1000, new TimerListener());
		timer.start();
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == start)// when push button
		{
			if (bstop == true) {
				bstart = true;
				bstop = false;
				btime = false;
				bclear = false;
			} else {
				bstart = true;
				bstop = false;
				btime = false;
				bclear = false;
				c1.clearClock();
			}
		} else if (e.getSource() == stop) {
			bstop = true;
			btime = false;
			bstart = false;
			bclear = false;
		} else if (e.getSource() == clear) {
			bclear = true;
			bstop = false;
			bstart = false;
			btime = false;
		} else if (e.getSource() == time) {
			btime = true;
			bstop = false;
			bstart = false;
			bclear = false;
		}

	}

	class TimerListener implements ActionListener// button function
	{
		public void actionPerformed(ActionEvent e) {
			if (bstart == true)// count time from 0:0:0
			{

				if (c1.second < 60)// just like your watch
				{
					c1.second += 1;
				} else {
					c1.second = 0;
				}
				if (c1.minute < 60) {
					if (c1.second == 0) {
						c1.minute += 1;
					}
				} else {
					c1.minute = 0;
				}
				if (c1.hour < 24) {
					if (c1.minute == 59 && c1.second == 0) {
						c1.hour += 1;
					}
				} else {
					c1.hour = 0;
				}
				c1.repaint();
				l1.setText(String.valueOf(c1.hour) + ":"
						+ String.valueOf(c1.minute) + ":"
						+ String.valueOf(c1.second));

			} else if (bstop == true)// stop temporaily
			{

				c1.repaint();
				l1.setText(String.valueOf(c1.hour) + ":"
						+ String.valueOf(c1.minute) + ":"
						+ String.valueOf(c1.second));
			} else if (bclear == true)// clear time to 0:0:0
			{
				c1.clearClock();
				c1.repaint();
				l1.setText(String.valueOf(c1.hour) + ":"
						+ String.valueOf(c1.minute) + ":"
						+ String.valueOf(c1.second));
			} else if (btime == true)// return to current time
			{

				c1.setCurrentTime();
				c1.repaint();
				l1.setText(String.valueOf(c1.hour) + ":"
						+ String.valueOf(c1.minute) + ":"
						+ String.valueOf(c1.second));

			} else {
				c1.setCurrentTime();// begin
				c1.repaint();
				l1.setText(String.valueOf(c1.hour) + ":"
						+ String.valueOf(c1.minute) + ":"
						+ String.valueOf(c1.second));
			}
		}

	}
}
