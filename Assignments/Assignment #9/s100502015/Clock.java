package a9.s100502015;

import java.awt.*;
import javax.swing.*;
import java.util.*;

public class Clock extends JPanel {
	public int hour = 0;
	public int minute = 0;
	public int second = 0;

	Clock() {
		setCurrentTime();

	}

	public void clearClock() {
		hour = 0;
		minute = 0;
		second = 0;
	}

	public void setCurrentTime()// your computer's time
	{
		Calendar calendar = new GregorianCalendar();
		hour = calendar.get(Calendar.HOUR_OF_DAY);
		minute = calendar.get(Calendar.MINUTE);
		second = calendar.get(Calendar.SECOND);
	}

	public void paintComponent(Graphics g) {
		int x = getWidth();
		int y = getHeight();
		int xhour = (int) (x / 2 + 20 * (Math.sin(((hour % 12) + minute / 60)
				* Math.PI / 6)));// hour
		int yhour = (int) (y / 2 - 20 * (Math.cos(((hour % 12) + minute / 60)
				* Math.PI / 6)));
		int xminute = (int) (x / 2 + 35 * (Math.sin((minute % 60) * Math.PI
				/ 30)));// minute
		int yminute = (int) (y / 2 - 35 * (Math.cos((minute % 60) * Math.PI
				/ 30)));
		int xsecond = (int) (x / 2 + 45 * (Math.sin((second % 60) * Math.PI
				/ 30)));// second
		int ysecond = (int) (y / 2 - 45 * (Math.cos((second % 60) * Math.PI
				/ 30)));
		super.paintComponent(g);
		g.drawOval(x / 4, y / 4, x / 2, y / 2);// draw a clock
		g.drawLine(x / 2, y / 2, xhour, yhour);
		g.drawLine(x / 2, y / 2, xminute, yminute);
		g.drawLine(x / 2, y / 2, xsecond, ysecond);
	}

}
