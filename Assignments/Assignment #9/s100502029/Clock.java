package a9.s100502029;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

public class Clock extends JPanel {
	// data member to store hour, minute , and second
	protected int hour;
	protected int minute;
	protected int second;
	
	// clear the current clock to zero
	public void clearClock() {
		hour = 0;
		minute = 0;
		second = 0;
	}
	
	// set current clock
	public void setCurrentClock() {
		if (second < 59)
			second += 1;
		else {
			if (minute < 59)
				minute += 1;
			else {
				if (hour < 59)
					hour += 1;
				else
					hour = 0;
				
				minute = 0;
			}
			second = 0;
		}
	}
	
	// draw a clock
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		// set clock's background color
		setBackground(new Color(100, 200, 0));
		
		// determine the clock's radius and center point
		int clockRadius = (int)(Math.min(getWidth(), getHeight()) * 0.8 * 0.5);
		int xCenter = getWidth() / 2;
		int yCenter = getHeight() / 2;
		
		// set clock's side color to blue
		g.setColor(Color.BLUE);
		// draw a circle to represent a clock
		g.fillOval(xCenter - clockRadius, yCenter - clockRadius, 2 * clockRadius, 2 * clockRadius);
		
		// draw the short line to represent 1 to 12
		int x1, y1, x2, y2;
		double rate;
		for (int i = 0; i < 12; i++) {
			if (i % 3 == 0)
				rate = 0.81;
			else
				rate = 0.85;
			
			x1 = (int)(xCenter + (clockRadius * 0.95) * Math.sin(i * (2 * Math.PI / 12)));
			y1 = (int)(yCenter - (clockRadius * 0.95) * Math.cos(i * (2 * Math.PI / 12)));
			x2 = (int)(xCenter + (clockRadius * rate) * Math.sin(i * (2 * Math.PI / 12)));
			y2 = (int)(yCenter - (clockRadius * rate) * Math.cos(i * (2 * Math.PI / 12)));
			g.setColor(Color.BLACK);
			g.drawLine(x1, y1, x2, y2);
			if (i % 6 == 0) {
				g.drawLine(x1 - 1, y1, x2 - 1, y2);
				g.drawLine(x1 + 1, y1, x2 + 1, y2);
			}
			if (i % 6 == 3) {
				g.drawLine(x1, y1 - 1, x2, y2 - 1);
				g.drawLine(x1, y1 + 1, x2, y2 + 1);
			}
		}
		
		// set second hand's color to red
		g.setColor(Color.RED);
		// draw small point at center
		g.fillOval(xCenter - 3, yCenter - 3, 7, 7);
		
		// draw second hand
		int secondHand = (int)(clockRadius * 0.8);
		int xSecond = (int)(xCenter + secondHand * Math.sin(second * (2 * Math.PI / 60)));
		int ySecond = (int)(yCenter - secondHand * Math.cos(second * (2 * Math.PI / 60)));
		g.drawLine(xCenter, yCenter, xSecond, ySecond);
		
		// draw second hand's arrow
		int arrow = (int)(clockRadius * 0.65);
		int x1arrow = (int)(xCenter + arrow * Math.sin((second - 1) * (2 * Math.PI / 60)));
		int y1arrow = (int)(yCenter - arrow * Math.cos((second - 1) * (2 * Math.PI / 60)));
		int x2arrow = (int)(xCenter + arrow * Math.sin((second + 1) * (2 * Math.PI / 60)));
		int y2arrow = (int)(yCenter - arrow * Math.cos((second + 1) * (2 * Math.PI / 60)));
		Polygon polygon = new Polygon();
		polygon.addPoint(xSecond, ySecond);
		polygon.addPoint(x1arrow, y1arrow);
		polygon.addPoint(x2arrow, y2arrow);
		g.fillPolygon(polygon);
	}
}
