package a9.s100502029;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener {
	private Clock clock = new Clock(); // create a clock
	private JLabel jlblScreen = new JLabel("00:00:00", JLabel.CENTER); // create a screen
	private JButton jbtStart = new JButton("Start"); // create a button to start
	private JButton jbtStop = new JButton("Stop"); // create a button to stop
	private JButton jbtClear = new JButton("Clear"); // create a button to clear
	private Timer timer = new Timer(1000, new TimerListener()); // create a timer
	
	public FrameWork() {
		// create a panel to set screen and three buttons
		JPanel jplButtons = new JPanel();
		
		// decorate panel and buttons color
		jplButtons.setBackground(Color.YELLOW);
		jbtStart.setBackground(Color.GRAY);
		jbtStart.setForeground(Color.CYAN);
		jbtStop.setBackground(Color.GRAY);
		jbtStop.setForeground(Color.CYAN);
		jbtClear.setBackground(Color.GRAY);
		jbtClear.setForeground(Color.CYAN);
		
		// put screen and buttons on the panel
		jplButtons.setLayout(new GridLayout(1, 4));
		jplButtons.add(jlblScreen);
		jplButtons.add(jbtStart);
		jplButtons.add(jbtStop);
		jplButtons.add(jbtClear);
		
		// combine panel and clock to the frame
		setLayout(new BorderLayout());
		add(jplButtons, BorderLayout.SOUTH);
		add(clock, BorderLayout.CENTER);
		
		// add action listener
		jbtStart.addActionListener(this);
		jbtStop.addActionListener(this);
		jbtClear.addActionListener(this);
	}
	
	// set each action
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == jbtStart)
			timer.start(); // start or continue to run the clock
		if (e.getSource() == jbtStop)
			timer.stop(); // stop the clock
		if (e.getSource() == jbtClear) {
			timer.stop();
			clock.clearClock(); // clear the clock
			jlblScreen.setText("00:00:00"); // initialize the screen
			clock.repaint();
		}
	}
	
	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			clock.setCurrentClock(); // set current time
			// display the result on the screen
			jlblScreen.setText((clock.hour >= 10 ? (clock.hour + "") : ("0" + clock.hour)) + ":" 
							+ (clock.minute >= 10 ? (clock.minute + "") : ("0" + clock.minute)) + ":" 
							+(clock.second >= 10 ? (clock.second + "") : ("0" + clock.second)));
			clock.repaint();
		}
	}
}
