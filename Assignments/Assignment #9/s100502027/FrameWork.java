package a9.s100502027;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class FrameWork extends JFrame implements ActionListener {
	Clock clock = new Clock();
	Timer timer = new Timer (1000, new TimerListener());
	private JButton JBstart = new JButton("Start");
	private JButton JBclear = new JButton("Clear");
	private JButton JBstop = new JButton("Stop");
	private JLabel JLscreen = new JLabel("00:00:00");
	public FrameWork() {
		Border linesg = new LineBorder(Color.GRAY,3);
		setLayout(new BorderLayout(5,5));
		clock.setBackground(Color.ORANGE);
		add(clock ,BorderLayout.CENTER);
		
		JPanel imfo = new JPanel();
		imfo.setLayout(new GridLayout(1,4,5,5));
		JBstart.addActionListener(this);
		JBstart.setBorder(linesg);
		JBstop.addActionListener(this);
		JBstop.setBorder(linesg);
		JBclear.addActionListener(this);
		JBclear.setBorder(linesg);
		JLscreen.setHorizontalAlignment(JLabel.CENTER);
		JLscreen.setBorder(linesg);
		imfo.add(JLscreen);
		imfo.add(JBstart);
		imfo.add(JBstop);
		imfo.add(JBclear);
		add(imfo , BorderLayout.SOUTH);
	}
	public void actionPerformed(ActionEvent e){   //use to respond three button event
		if(e.getSource()==JBstart){
			timer.start();
		}
		else if(e.getSource()==JBstop){
			timer.stop();
		}
		else if(e.getSource()==JBclear){
			timer.stop();
			clock.clearClock();
			clock.repaint();
			handletime();
		}
	}
	public void handletime(){   //A method to make the String to display the time 
		int h=clock.gethour();
		int m=clock.getminute();
		int s=clock.getsecond();
		String timeshow = "";
		if(h<10){
			timeshow += "0" + h + ":" ;
		}
		else{
			timeshow += h + ":" ;
		}
		if(m<10){
			timeshow += "0" + m + ":" ;
		}
		else{
			timeshow += m + ":" ;
		}
		if(s<10){
			timeshow += "0" + s  ;
		}
		else{
			timeshow += s  ;
		}
		
		JLscreen.setText(timeshow);
	}
	
	private class TimerListener implements ActionListener {   //to follow timer
		public void actionPerformed (ActionEvent e){
			clock.setCurrentClock();
			clock.repaint();
			handletime();
		}
	}
}