package a9.s100502027;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

import javax.swing.JPanel;

public class Clock extends JPanel{
	private int hour=0;
	private int minute=0;
	private int second=0 ;
	public Clock() {
		
	}
	public int gethour(){
		return hour ;
	}
	public int getminute(){
		return minute ;
	}
	public int getsecond(){
		return second ;
	}
	public void clearClock(){   // clear => three elements all =0
		hour=0;
		minute=0;
		second=0;
	}
	
	public void setCurrentClock(){  // time - > second++
		if(second<59){
			second+=1;
		}
		else{
			second=0;
			if(minute<59){
				minute+=1;
			}
			else{
				minute=0;
				hour+=1;
			}
		}
	}
	
	protected void paintComponent(Graphics g){   // draw a picture to practice use draw & fill method . and set the (x,y) to the pic
		super.paintComponent(g);
		int Xc = getWidth()/2;
		int Yc = getHeight()/2;
		int r ;
		if (Xc <= Yc){
			r = (int)(Xc / 2 *1.7) ;
		}
		else{
			r = (int)(Yc / 2 *1.7);
		}
		int r1 = r;
		int r2 = (int)(r*0.95);
		int r3 = (int)(r*0.8);
		int r4 = (int)(r*0.65);
		int r5 = (int)(r*0.25);
		int r6 = (int)(r*0.22);
		int r7 = (int)(r*0.05);
		int Xsecond = Xc + (int)(r3*Math.sin(second*2*Math.PI/60));
		int Ysecond = Yc - (int)(r3*Math.cos(second*2*Math.PI/60));
		g.setColor(Color.BLACK);
		g.fillOval(Xc-r1, Yc-r1, r1*2, r1*2);
		g.setColor(Color.WHITE);
		g.drawOval(Xc-r2, Yc-r2, r2*2, r2*2);
		g.drawOval(Xc-r3, Yc-r3, r3*2, r3*2);
		for(int t=0;t<6;t++){
			g.drawArc(Xc-r4,Yc-r4, r4*2 , r4*2,t*60-21,42);
		}
		Polygon sixstar1 = new Polygon();
		Polygon sixstar2 = new Polygon();
		for(int t=0;t<6;t++){
			if(t%2==1){
				sixstar1.addPoint(Xc-(int)(Math.sin(Math.PI*t/3)*r3),Yc-(int)(Math.cos(Math.PI*t/3)*r3));
			}
			else{
				sixstar2.addPoint(Xc-(int)(Math.sin(Math.PI*t/3)*r3),Yc-(int)(Math.cos(Math.PI*t/3)*r3));
			}
		}
		g.drawPolygon(sixstar1);
		g.drawPolygon(sixstar2);
		g.fillOval(Xc-r5, Yc-r5, r5*2, r5*2);
		g.setColor(Color.BLACK);
		g.fillOval(Xc-r6, Yc-r6, r6*2, r6*2);
		g.setColor(Color.WHITE);
		g.fillOval(Xc-r7, Yc-r7, r7*2, r7*2);
		
		g.setColor(new Color(128,64,0));
		g.drawLine(Xc, Yc, Xsecond, Ysecond);
	}
}