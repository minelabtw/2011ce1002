package a1.s995002026;
import java.util.Scanner;
public class A12 {
	public void a12 (){
		Scanner input=new Scanner(System.in);
		
		System.out.println("input the temperature in Celsius");
		
		double c=input.nextDouble();
		
		toFahrenheit(c);
		
		System.out.println("input the temperature in Fahrenheit");
		
		double f=input.nextDouble();
		
		toCelsius(f);
	}
	public void toFahrenheit(double t){
		t=9/5*t+32;
		System.out.println("the Fahrenheit is "+t);
	}
	public void toCelsius(double t){
		t=(t-32)*5/9;
		System.out.println("the Celsius is "+t);
	}
}
