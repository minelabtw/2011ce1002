package a1.s100502024;

import java.util.Scanner;
public class A12 
{
	public static double toFahrenheit (double number1) // 攝氏轉華氏function
	{
		double Fahrenheit = number1*9/5+32; // 計算
		return Fahrenheit;
	}
	public static double toCelsius (double number2) // 攝氏轉華氏function
	{
		double Celsius = (number2-32)*5/9; // 計算
		return Celsius;
	}
	public static void main (String[] args)
	{
		boolean loop = true;
		while (loop == true)
		{
			System.out.print("請選擇其中一項功能 :"+"\t");
			System.out.print("(1) 。C → 。F"+"\t"); // 功能選項
			System.out.print("(2) 。F → 。C"+"\t");
			System.out.print("(0) Exit");
			Scanner input = new Scanner (System.in);
			int choose = input.nextInt();
			switch (choose)
			{
				case 1:
					System.out.print("請輸入溫度 : ");
					double temperature1 = input.nextDouble(); // 讀入溫度
					System.out.print("等同於華式"+toFahrenheit(temperature1)+"。F"+"\n"); // 將溫度傳入function計算
					break;
				case 2:
					System.out.print("請輸入溫度 : ");
					double temperature2 = input.nextDouble(); // 讀入溫度
					System.out.print("等同於攝式"+toCelsius(temperature2)+"。F"+"\n"); // 將溫度傳入function計算
					break;
				case 0:
					loop = false;
					System.out.print("Thanks for your use!");
					break;
			}
		}
	}
}
