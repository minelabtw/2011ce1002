package a1.s100502024;

import java.util.Scanner;
public class A11 
{
	public static void main (String[] args)
	{
		boolean select = true; // 使用boolean讓之後能跳出while迴圈
		System.out.print ("Please enter your name:");
		Scanner input = new Scanner (System.in);
		String name = input.next(); // 讀入名字
		System.out.print ("Please enter your school number:");
		int schoolnumber = input.nextInt(); // 讀入學號
		System.out.println("Please enter a number: ");
		double total = input.nextDouble(); // 讀入第一個數字
		while (select == true) // 進入while迴圈
		{
			System.out.println("Please select a function:"); 
			System.out.println("(1) Add (2) Minus (3) Multiple (4) Divide (5) Show Information (0) Exit"); // 使用者選項
			int choose = input.nextInt();
			switch (choose)
			{
				case 1:
					System.out.print("Please enter another number: ");
					double number = input.nextDouble();
					total = total+number; // 計算
					System.out.println("The answer is "+ total); // 印出目前總值
					break;
				case 2:
					System.out.print("Please enter another number: ");
					number = input.nextDouble();
					total = total-number;
					System.out.println("The answer is "+ total); // 印出目前總值
					break;
				case 3:
					System.out.print("Please enter another number: ");
					number = input.nextDouble();
					total = total*number;
					System.out.println("The answer is "+ total); // 印出目前總值
					break;
				case 4:
					System.out.print("Please enter another number: "); 
					number = input.nextDouble();
					total = total/number;
					System.out.println("The answer is "+ total); // 印出目前總值
					break;
				case 5:
					System.out.println("Your name is "+name+"."); // 顯示名字
					System.out.println("Your school number is "+schoolnumber+"."); // 顯示學號
					break;
				case 0:
					System.out.println("Thanks for your use,"+name+"."); 
					select = false; // 跳出while迴圈
					break;
			}
		}
	}
}
