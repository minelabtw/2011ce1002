package A1.s100502545;

import java.util.Scanner;

public class A12
{
	public static void main (String[] args)
	{
		Scanner input = new Scanner(System.in);
		double tem;
		double result;
		int choose;
		
		while(true)
		{
			//three choose methods
			System.out.println("Please choose : ");
			
			System.out.println("1.To Fahrenheit");
			System.out.println("2.To Celcius");
			System.out.println("3.Exit");
			
			choose = input.nextInt();
			
			
			switch(choose)
			{//user choose a method to change
				case 1:					
					System.out.println("Please input temperature:");
					tem= input.nextDouble();
					result = ToFah(tem);
					System.out.println("Celcius To Fahrenheit result :"+result);
					break;
					
				case 2: 	
					
					System.out.println("Please input temperature:");
					tem= input.nextDouble();
					result = ToCel(tem);
					System.out.println("Fahrenheit To Celcius result :"+result);
					break;
					
				case 3:
					System.out.println("Bye");
		    		System.exit(0);
					break;
					
				default:
					break;
			}
		}
		
	}
	
	public static double ToFah(double input)
	{
		//to Fahrenheit method function
		double result = (input*9)/5+32;
		return result;
	}
	
	public static double ToCel(double input)
	{
		//to celcius method function
		double result = ((input - 32)*5)/9;
		return result ;
	}
}
