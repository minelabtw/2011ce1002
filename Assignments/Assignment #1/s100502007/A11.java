package a1.s100502007;

import java.util.Scanner;

public class A11 {
	public static double Add(double first,double second){//add method
		double add = first + second;
		return add;
	}
	public static double Minus(double first,double second){//minus method
		double minus = first - second;
		return minus;
	}
	public static double Multiple(double first,double second){//multiple method
		double multiple = first * second;
		return multiple;
	}
	public static double Divide(double first,double second){//divide method
		double divide = first / second;
		return divide;
	}
	public static void main (String[] args){
		Scanner input = new Scanner(System.in);
			System.out.print("Please enter your name.\n");
			String name = input.next();//輸入名字
			System.out.print("Please enter your student number.\n");
			int number = input.nextInt();//輸入學號
		for( ; ; ){
			System.out.print("Welcome to use this method.\n"+"1.add.\n"+"2.minus.\n"+"3.multiple\n"+"4.divide\n"+"5.show information\n"+"6.exit\n");//介紹功能3
			int choose = input.nextInt();;//輸入選擇
			switch(choose){
				case 1:
					double first1 = input.nextDouble();
					double second1 = input.nextDouble();
					System.out.print("結果 = "+Add(first1,second1));
					break;
				case 2:
					double first2 = input.nextDouble();
					double second2 = input.nextDouble();
					System.out.print("結果 = "+Minus(first2,second2));
					break;
				case 3:
					double first3 = input.nextDouble();
					double second3 = input.nextDouble();
					System.out.print("結果 = "+Multiple(first3,second3));
					break;
				case 4:
					double first4 = input.nextDouble();
					double second4 = input.nextDouble();
					System.out.print("結果 = "+Divide(first4,second4));
					break;
				case 5:
					System.out.print(name+" "+number+" \n");
					break;
				case 6:
					break;
			}
			if(choose == 6){//跳出迴圈
				break;
			}
			}
		}
	
}
