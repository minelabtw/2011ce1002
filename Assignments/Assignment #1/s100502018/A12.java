package a1.s100502018;

import java.util.Scanner;

public class A12 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int choice = 0;
		double degree = 0;
		System.out.println("Welcome to the poor temperature tramsformer!");
		do {
			System.out
					.print("1. Change input from Celsius to Fahrenheit\n2. Change input form Fahrenheit to Celsius\n3. Leave the program\nPlease choose a service: ");
			choice = input.nextInt(); //input the choice
			switch (choice) {
			case 1:
				System.out.print("Please input the degree: ");
				degree = input.nextDouble(); //input the degree to transfer
				System.out.println("The result is " + toFahrenheit(degree));
				break;
			case 2:
				System.out.print("Please input the degree: ");
				degree = input.nextDouble(); //input the degree to transfer
				System.out.println("The result is " + toCelsius(degree));
				break;
			case 3:
				System.out.print("Good bye~");
			default:
				break;
			}
		} while (choice != 3);
	}

	public static double toFahrenheit(double x) { //method to change Celsius into Fahrenheit
		return x * 9 / 5 + 32;
	}

	public static double toCelsius(double x) { //method to change Fahrenheit into Celsius
		return (x - 32) * 5 / 9;
	}
}
