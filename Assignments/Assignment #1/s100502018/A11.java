package a1.s100502018;

import java.util.Scanner;

public class A11 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Welcome to the poor calculator!\nPlease input your school number: ");
		String schoolnumber = input.nextLine(); // input the school number of
												// user
		System.out.print("Please input your name: ");
		String name = input.nextLine(); // input the name of user
		System.out.println("Hello, " + schoolnumber + " " + name);
		int choice = 0;
		do {
			System.out
					.print("\n1. Addition\n2. Subtraction\n3. Multiplication\n4. Division\n5. Show the information of user\n6. Leave the calculator\nPlease choose a service: ");
			choice = input.nextInt(); // input the choice
			double number1 = 0;
			double number2 = 0;
			switch (choice) { // tell the choice
			case 1:
				System.out.print("Please input number as and b (a + b): ");
				number1 = input.nextDouble();
				number2 = input.nextDouble();
				System.out.println("The result is " + add(number1, number2));
				break;
			case 2:
				System.out.print("Please input number a and b (a - b): ");
				number1 = input.nextDouble();
				number2 = input.nextDouble();
				System.out.println("The result is " + minus(number1, number2));
				break;
			case 3:
				System.out.print("Please input number a and b (a * b): ");
				number1 = input.nextDouble();
				number2 = input.nextDouble();
				System.out.println("The result is " + multiple(number1, number2));
				break;
			case 4:
				System.out.print("Please input number a and b (a / b): ");
				number1 = input.nextDouble();
				number2 = input.nextDouble();
				System.out.println("The result is " + divide(number1, number2));
				break;
			case 5:
				showinformation(schoolnumber, name);
				break;
			case 6:
				System.out.print("Thanks for your using. Good bye~ " + name);
				break;
			default:
				break;
			}
		} while (choice != 6);
	}

	public static double add(double x, double y) { // method to addition
		return x + y;
	}

	public static double minus(double x, double y) { // method to subtraction
		return x - y;
	}

	public static double multiple(double x, double y) { // method to
														// multiplication
		return x * y;
	}

	public static double divide(double x, double y) { // method to division
		return x / y;
	}

	public static void showinformation(String x, String y) {
		System.out.println("School number: " + x + " User: " + y);
	}
}