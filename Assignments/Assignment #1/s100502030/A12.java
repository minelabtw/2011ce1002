package a1.s100502030;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class A12 {

	public static double toFahrenheit(double celsius) {
		return celsius * 9 / 5 + 32;
	}// 將攝氏轉華氏的功能

	public static double toCelsius(double fahrenheit) {
		return (fahrenheit - 32) * 5 / 9;
	}// 將華氏轉攝氏的功能

	public static void main(String[] args) {
		while (true) {
			String choiceStr = JOptionPane.showInputDialog(null,
					"1.toFahrenheit\n2.toCelsius\n3.exit", "Unit Conversion",
					JOptionPane.QUESTION_MESSAGE);// 輸入選擇
			int choice = Integer.parseInt(choiceStr);

			if (choice == 1) {
				String celStr = JOptionPane.showInputDialog(null,
						"Please input a value", "toFahrenheit",
						JOptionPane.QUESTION_MESSAGE);// 輸入攝氏溫度
				double cel = Double.parseDouble(celStr);
				JOptionPane.showMessageDialog(null, toFahrenheit(cel),
						"result", JOptionPane.INFORMATION_MESSAGE);// 顯示轉化結果
			}
			if (choice == 2) {
				String fahStr = JOptionPane.showInputDialog(null,
						"Please input a value", "toCelsius",
						JOptionPane.QUESTION_MESSAGE);
				double fah = Double.parseDouble(fahStr);// 輸入華氏溫度
				JOptionPane.showMessageDialog(null, toCelsius(fah), "result",
						JOptionPane.INFORMATION_MESSAGE);// 顯示轉化結果
			}
			if (choice == 3)
				break;
		}
		JOptionPane.showMessageDialog(null, "Thank for your use!", "goodbye!",
				JOptionPane.INFORMATION_MESSAGE);// 離開訊息
	}
}
