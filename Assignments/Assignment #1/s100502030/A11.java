package a1.s100502030;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class A11 {
	public static void main(String[] args) {

		double answer, number1, number2;// 宣告計算的數字和計算結果
		String number1Str, number2Str;// 宣告計算數字的字串型別

		String name = JOptionPane
				.showInputDialog("Welcome to use this calculator\n"
						+ "Please input your name");// 用小視窗輸入姓名
		String studentNumber = JOptionPane
				.showInputDialog("Please input your student number");// 用小視窗輸入學號

		while (true) {
			String choiceString = JOptionPane.showInputDialog(null, "1.add\n"
					+ "2.minus\n" + "3.multiple\n" + "4.divide\n"
					+ "5.show user information\n" + "6.exit", "Calculator",
					JOptionPane.QUESTION_MESSAGE);// 用小視窗顯示可執行的動作
			int choice = Integer.parseInt(choiceString);

			switch (choice) {
				case 1:
					number1Str = JOptionPane
							.showInputDialog("Please input a value");// 輸入要計算的數字
					number1 = Double.parseDouble(number1Str);// 轉回double型別
					number2Str = JOptionPane
							.showInputDialog("Please input a value");
					number2 = Double.parseDouble(number2Str);
					answer = number1 + number2;//計算結果
					JOptionPane.showMessageDialog(null, number1 + " + " + number2
							+ " = " + answer, "Result",
							JOptionPane.INFORMATION_MESSAGE);// 顯示計算結果
					break;
				case 2:
					number1Str = JOptionPane
							.showInputDialog("Please input a value");
					number1 = Double.parseDouble(number1Str);
					number2Str = JOptionPane
							.showInputDialog("Please input a value");
					number2 = Double.parseDouble(number2Str);
					answer = number1 - number2;
					JOptionPane.showMessageDialog(null, number1 + " - " + number2
							+ " = " + answer, "Result",
							JOptionPane.INFORMATION_MESSAGE);
					break;
				case 3:
					number1Str = JOptionPane
							.showInputDialog("Please input a value");
					number1 = Double.parseDouble(number1Str);
					number2Str = JOptionPane
							.showInputDialog("Please input a value");
					number2 = Double.parseDouble(number2Str);
					answer = number1 * number2;
					JOptionPane.showMessageDialog(null, number1 + " * " + number2
							+ " = " + answer, "Result",
							JOptionPane.INFORMATION_MESSAGE);
					break;
				case 4:
					number1Str = JOptionPane
							.showInputDialog("Please input a value");
					number1 = Double.parseDouble(number1Str);
					number2Str = JOptionPane
							.showInputDialog("Please input a value");
					number2 = Double.parseDouble(number2Str);
					answer = number1 / number2;
					JOptionPane.showMessageDialog(null, number1 + " / " + number2
							+ " = " + answer, "Result",
							JOptionPane.INFORMATION_MESSAGE);
					break;
				case 5:
					JOptionPane.showMessageDialog(null, "name: " + name
							+ "\nstudent number: " + studentNumber,
							"user information", JOptionPane.INFORMATION_MESSAGE);// 顯示使用者資料
					break;
				case 6:
					JOptionPane.showMessageDialog(null, "goodbye! " + name);// 離開訊息
					System.exit(0);// 離開
					break;
				default:
					break;
			}
		}
	}
}
