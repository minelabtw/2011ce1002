package a1.s100502012;

import java.util.Scanner;

public class A11 
{
 public static void main(String[] args)
 {
  Scanner input = new Scanner(System.in);
  
  System.out.println("Please enter your name and student number");
  System.out.println("\nname: ");
  String name = input.next();
  System.out.println("\nID: ");
  int number = input.nextInt();
  
  double number1;
  double number2;
  
  int flag = 0;
  while(flag == 0)
     { 
	  System.out.println("\n1.add / 2.subtract / 3.multiply /  4.divide / 5.show information / 6.EXIT\nPlease choose a function:");
	  int order = input.nextInt();
      switch (order)
         {
          case 1:
    	      System.out.println("Please enter two numbers for adding them");
    	      number1 = input.nextDouble();
    	      number2 = input.nextDouble();
    	      double add = number1 + number2;
    	      System.out.println(number1+" + "+number2+" = "+add);
    	      break;
    	  
          case 2:
    	      System.out.println("Please enter two numbers for subtracting");
    	      number1 = input.nextDouble();
    	      number2 = input.nextDouble();
    	      double subtract = number1 - number2;
    	      System.out.println(number1+" - "+number2+" = "+subtract);
    	      break;
    	  
          case 3:
        	  System.out.println("Please enter two numbers for multiplying them");
        	  number1 = input.nextDouble();
        	  number2 = input.nextDouble();
        	  double multiply = number1 * number2;
        	  System.out.println(number1+" x "+number2+" = "+multiply);
        	  break;
    	  
          case 4:
        	  System.out.println("Please enter two numbers for dividing");
        	  number1 = input.nextDouble();
        	  number2 = 0;
        	  while (number2 == 0)
        	     { 
        		  number2 = input.nextDouble();
        		  if (number2 == 0);
        		  	 System.out.println("ERROR! Denominator can not be zero!");
        	     } 
        	  double divide = number1 / number2;
        	  System.out.println(number1+" / "+number2+" = "+divide);
        	  break;
    	  
          case 5:
        	  System.out.println("Student's Name: "+name+"\n"+"ID Number: "+number); 
        	  break;
    	  
          case 6:
        	  flag = order;
        	  System.out.println("\nSee you ~ Have a nice day , "+name);
        	  break;
        }  
     }
 }
}

