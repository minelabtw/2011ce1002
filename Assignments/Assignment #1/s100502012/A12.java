package a1.s100502012;

import java.util.Scanner;

public class A12 
{
 public static double CtoF(double C)
 {
  double result1 = ((C+40)*1.8)-40;
  return result1;
 }
 
 public static double FtoC(double F)
 {
  double result2 = ((F+40)/1.8)-40;
  return result2;
 }
 
 public static void main(String[] args)
 {
  Scanner input = new Scanner(System.in);
	  
  System.out.println("\nEntering a degree of temperature by Celsius:\n");
  double Celsius = input.nextDouble();
  System.out.println("\n"+Celsius+" Celsius = "+CtoF(Celsius)+" Fahrenheit");
  
  System.out.println("\nEntering a degree of temperature by Fahrenheit:\n");
  double Fahrenheit = input.nextDouble();
  System.out.println("\n"+Fahrenheit+" Fahrenheit = "+FtoC(Fahrenheit)+" Celsius");
  
 }
}