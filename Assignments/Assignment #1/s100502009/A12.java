package a1.s100502009;

import java.util.Scanner;

public class A12 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int exit=0;//set a variable to exit
		while(exit!=1)
		{
			System.out.println("Please choose a service:"+"1.c->f  2.f->c  3.exit");
			int choose=input.nextInt();	//user's choice	
			switch(choose)
			{
				case 1:
					System.out.println("Please enter the Celsius temperature: ");
					double c=input.nextDouble();//input the Celsius temperature
					System.out.println("The result is: "+toFahrenheit(c));
					break;
				case 2:
					System.out.println("Please enter the Fahrenheit temperature: ");
					double f=input.nextDouble();//input the Fahrenheit temperature
					System.out.println("The result is: "+toCelsius(f));
					break;
				case 3:
					exit=1;
					System.out.println("BYE BYE");
					break;
				default:
					System.out.println("Errors: invalid status");
					break;
			}
		}
	}
	
	public static double toFahrenheit(double cel)//method to transform the celsius to fahrenheit
	{
		double fah=(cel*9/5)+32;
		return fah;
	}
	
	public static double toCelsius(double fah)//method to transform the celsius to fahrenheit
	{
		double cel=(fah-32)*5/9;
		return cel;
	}
}
