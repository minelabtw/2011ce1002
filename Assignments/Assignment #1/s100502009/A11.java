package a1.s100502009;

import java.util.Scanner;

public class A11 {
	public static void main(String[] args){
		int exit=0;//set a variable to exit
		Scanner input = new Scanner(System.in);
		System.out.println("Please enter your name and your school number: ");
		String name=input.next();//input the user's name
		String schoolNum=input.next();//input the user's school number
		System.out.println("Please enter the first number: ");		
		double sum=input.nextDouble();
		double num2;
		while(exit!=1)//loop to operate again
		{		
			System.out.println("1.add(+)\n"+"2.minus(-)\n"+"3.multiple(*)\n"+"4.divide(/)\n"+"5.answer(=)\n"+"6.show information\n"+"7.exit");
			int choose=input.nextInt();//user's choice
			switch(choose)
			{
				case 1:
					num2=input.nextDouble();
					sum+=num2;//add the number
					break;
				case 2:
					num2=input.nextDouble();
					sum-=num2;//minus the number
					break;
				case 3:
					num2=input.nextDouble();
					sum*=num2;//multiple the number
					break;
				case 4:
					num2=input.nextDouble();
					sum/=num2;//divide the number
					break;
				case 5:
					System.out.println("The total is "+sum);
					break;
				case 6:
					System.out.println("Your name is "+name+" and your school number is "+schoolNum);
					break;
				case 7:
					System.out.println("Thanks for using~ "+name+" "+schoolNum);
					exit=1;
					break;
				default:
					System.out.println("Error: invalid status");
					break;
					
			}
			
		}
		
	}

}
