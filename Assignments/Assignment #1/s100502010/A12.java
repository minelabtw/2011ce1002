package a1.s100502010;

import java.util.Scanner;

public class A12 
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		System.out.println("enter the temperature:");
		double num1=input.nextDouble(); //讀取原始度數
		toCelsius(num1);
		toFahrenheit(num1);
		
	}
	public static void toCelsius(double temp) //method用於計算成攝氏
	{
		temp=(1.8*temp)+32;
		System.out.println("Celsius: " + temp);
	}	

	public static void toFahrenheit(double temp) //method用於計算成華氏
	{
		temp=(temp-32)*5/9;
		System.out.println("Fahrenheit: " + temp);
	}
}