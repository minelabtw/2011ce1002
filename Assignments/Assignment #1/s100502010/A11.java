package a1.s100502010;

import java.util.Scanner;

public class A11
{
	public static void main(String[] args)
	{
		int exit=0;
		double answer=0;
		Scanner input = new Scanner(System.in);
		System.out.print("please enter your name: ");//詢問使用者姓名
		String name=input.nextLine();
		System.out.print("please enter your student number: ");//詢問使用者學號
		int number=input.nextInt();
		while(exit<10)
		{
		
			System.out.print("choose the function you want to use: \n");//功能清單
			System.out.print("1.add \n");
			System.out.print("2.minus \n");
			System.out.print("3.multiple \n");
			System.out.print("4.divide \n");
			System.out.print("5.show imformation \n");
			System.out.print("6.ext \n");
			int choose = input.nextInt();//讀取使用者育使用之功能
			
			switch(choose)
			{
				case 1://加法功能
					System.out.print("enter the first number: \n");
					double num1=input.nextDouble();
					System.out.print("enter the second number: \n");
					double num2=input.nextDouble();
					
					answer=num1+num2;
					System.out.println(answer);
					break;
				case 2://減法功能
					System.out.print("enter the first number: \n");
					double num3=input.nextDouble();
					System.out.print("enter the second number: \n");
					double num4=input.nextDouble();
					
					answer=num3-num4;
					System.out.println(answer);
					break;
				case 3://乘法功能
					System.out.print("enter the first number: \n");
					double num5=input.nextDouble();
					System.out.print("enter the second number: \n");
					double num6=input.nextDouble();
					
					answer=num5*num6;
					System.out.println(answer);
					break;
				case 4://除法功能
					System.out.print("enter the first number: \n");
					double num7=input.nextDouble();
					System.out.print("enter the second number: \n");
					double num8=input.nextDouble();
					
					answer=num7/num8;
					System.out.println(answer);
					break;
				case 5://顯示使用者之姓名及學號
					System.out.println("your name is " + name);
					System.out.println("your student number is " + number);
					break;
				case 6://跳出程式
					System.out.println("wish you a good day!!");
					exit=15;
					break;
			}
		}
	}
}

