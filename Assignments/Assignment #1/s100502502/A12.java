//A12.java
package a1.s100502502;
import java.util.Scanner;
public class A12 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		boolean on = true; //initialize it
		while(on){
			System.out.println("1.Celsius to Fahrenheit.  2.Fahrenheit to Celsius.  3.exit.");//print list
			System.out.print("Choose number : ");
			int choose = input.nextInt();//save input
			switch(choose){
				case 1://Celsius to Fahrenheit
					System.out.print("Enetr the value you want to change : ");
					//calculate and show the result
					System.out.println("Fahrenheit temperature = " + toFahrenheit(input.nextDouble()));
					break;
				case 2: //Fahrenheit to Celsius
					System.out.print("Enetr the value you want to change : ");
					//calculate and show the result
					System.out.println("Celisus temperature = " + toCelisus(input.nextDouble()));
					break;
				case 3://exit
					System.out.println("Byebye....");//show leave message
					on = false;//stop the loop
					break;
				default:
					break;
			}
		}
	}
	public static double toFahrenheit(double T){//change Celsius to Fahrenheit
		double F = 0;
		F = 1.8 * T + 32;
		return F;
	}
	public static double toCelisus(double T){//change Fahrenheit to Celsius
		double C = 0;
		C = (T - 32) / 1.8;
		return C;
	}
}
