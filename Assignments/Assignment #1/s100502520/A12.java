package A1.s100502520;

import java.util.Scanner;

public class A12 {
	public double result;
	public static void main(String[] args){
		
		Scanner input = new Scanner(System.in);  //create a scanner to input from keyboard
		int judge = 1;
		while(judge == 1){
			System.out.println("choose a work");
			System.out.println("1. Centigrade become Fahrenheit");
			System.out.println("2. Fahrenheit become Centigrade");
			System.out.println("3. exit");
			
			int choose = input.nextInt();
			
			A12 use = new A12();  //use A12 create a object
			switch(choose){
				case 1:  //Centigrade become Fahrenheit
					System.out.println("input a Centigrade temperature: ");
					double temp = input.nextDouble();
					use.toFahrenheit(temp);
					System.out.print("the Fahrenheit temperature is: ");
					System.out.println(use.result);
					break;
				case 2:  //Fahrenheit become Centigrade
					System.out.println("input a Fahrenheit temperature: ");
					temp = input.nextDouble();
					use.toCentigrade(temp);
					System.out.print("the Centigrade temperature is: ");
					System.out.println(use.result);
					break;
				case 3:  //exit
					System.out.println("bye bye");
					judge = 2;
					break;
				
			}
		}
	}
	
	public void toFahrenheit(double temp){  //the method let Centigrade become Fahrenheit
		result = (temp*1.8) + 32;
	}
	
	public void toCentigrade(double temp){  //the method let Fahrenheit become Centigrade
		result = (temp-32)*5/9;
	}

}
