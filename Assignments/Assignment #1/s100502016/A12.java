package a1.s100502016;
import java.util.Scanner;

public class A12 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		boolean exitflag = false;

		while (!exitflag) {
			System.out.println("1.change Celsius to Fahrenheit\n2.change Celsius to Fahrenheit");
			int cho = input.nextInt();
			switch (cho) {
			case 1:
				System.out.println("Input a number: ");
				double n = input.nextInt();
				System.out.println("The result is: " + toFahrenheit(n));
				break;

			case 2:
				System.out.println("Input a number: ");
				n = input.nextInt();
				System.out.println("The result is: " + toCelisus(n));
				break;

			case 3:
				exitflag = true;
			}

		}
	}

	public static double toFahrenheit(double n) {
		return n * 9 / 5 + 32;
	}

	public static double toCelisus(double n) {
		return (n - 32) * 5 / 9;
	}

}
