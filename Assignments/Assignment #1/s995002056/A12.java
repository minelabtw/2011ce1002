package a1.s995002056;
import java.util.Scanner;

public class A12 {
	
	public static double toFarenheit(double C){		//攝氏轉換華氏
		double F;
		F = C * (9 / 5 ) + 32 ; 
		return F;
	}
	
	public static double toCelsius(double F){		//華氏轉換攝氏
		double C;
		C = ((F - 32) * 5) / 9 ;
		return C;
	}
	
	public static void main(String [] argv){        //主程式
		
		Scanner scanner = new Scanner(System.in);
		
		boolean control = true ;
		
		while(control){
			
			System.out.println("請選擇您要的功能");
			System.out.println("1.攝氏轉換華氏");
			System.out.println("2.華氏轉換攝氏");
			System.out.println("3.結束程式");
			
			int choice = scanner.nextInt();
			double C , F ;
			
			switch(choice){
			
				case(1):
						System.out.println("您選的功能為攝氏轉換華氏");
						System.out.print("請輸入攝氏溫度:");
						C = scanner.nextDouble();
						System.out.println("華氏溫度為: " + toFarenheit(C));
						break;
						
				case(2):
						System.out.println("您選的功能為華氏轉換攝氏");
						System.out.print("請輸入華氏溫度:");
						F = scanner.nextDouble();
						System.out.println("攝氏溫度為: " + toCelsius(F));
						break;
						
				case(3):
						System.out.println("謝謝您的使用。");
						control = false;
						break;
						
			}
		}
	}
}
