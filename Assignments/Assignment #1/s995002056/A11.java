package a1.s995002056;
import java.util.Scanner;

public class A11 {

	public static void main(String [] argv){            //主程式
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("請輸入姓名:");
		String Name = scanner.next();
		
		boolean control = true ;
		
		while(control){
			                                                    //介面
			System.out.println("計算機");
			System.out.println("請選擇您需要的功能");
			System.out.println("1.加");
			System.out.println("2.減");
			System.out.println("3.乘");
			System.out.println("4.除");
			System.out.println("5.顯示使用者姓名");
			System.out.println("6.離開");
			System.out.print("     ");
			System.out.print("請選擇:");
			
			int choice = scanner.nextInt();
			
			double i , j ;
			
			switch(choice){                                    //switch開始
			
				case(1):                                       //加法
						System.out.print("請輸入被加數");
						i = scanner.nextDouble();
						System.out.print("請輸入加數");
						j = scanner.nextDouble();
						System.out.println(i + j);
						break;
						
				case(2):									   //減法	                 
						System.out.print("請輸入被減數");
						i = scanner.nextDouble();
						System.out.print("請輸入減數");
						j = scanner.nextDouble();
						System.out.println(i - j);
						break;
						
				case(3):									   //乘法
						System.out.print("請輸入被乘數");
						i = scanner.nextDouble();
						System.out.print("請輸入乘數");
						j = scanner.nextDouble();
						System.out.println(i * j);
						break;
						
				case(4):									   //除法
						System.out.print("請輸入被除數");
						i = scanner.nextDouble();
						System.out.print("請輸入除數");
						j = scanner.nextDouble();
						System.out.println(i / j);
						break;
				
				case(5):									   //顯示使用者姓名
						System.out.println(Name);
						break;
						
				case(6):									   //離開
						control = false;
						System.out.print("謝謝您的使用。");
						break;						           //結束
			}			
		}					
	}
}
