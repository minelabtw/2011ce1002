package a1.s975002502;

import java.io.*;
import java.lang.Math;

public class A12 {
	public static void main(String[] args) throws IOException {
		BufferedReader buf;
		String temstr;
		double tem;
		double result;
		int option;
		
		buf=new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Welcome !! ");
		System.out.print("Please input a number: ");
		temstr=buf.readLine();
		tem=Double.parseDouble(temstr);
		System.out.println("The input number is "+tem);
		System.out.println("Select the service: 1.toFahrenheit 2.toCelsius");
		option=Integer.parseInt(buf.readLine());
		if((option!=1)&&(option!=2)){
			System.out.println("Wrong input!!!");
			System.out.println("Select the service: 1.toFahrenheit 2.toCelsius");
		}
		switch(option){
		case 1:
			result=toFahrenheit(tem);
			result=(double)(Math.round(result*100))/100;
			System.out.println("Celsius "+tem+"'C == Fahrenheit "+result+"'F");
			break;
		case 2:
			result=toCelsius(tem);
			result=(double)(Math.round(result*100))/100;
			System.out.println("Fahrenheit "+tem+"'F == Celsius "+result+"'C");
			break;
		}
	}
	
	public static double toFahrenheit(double tem){
		double result;
		result=(double)9/5*tem+32;
		return result;
	}
	
	public static double toCelsius(double tem){
		double result;
		result=(double)(tem-32)*5/9;
		return result;
	}

}
