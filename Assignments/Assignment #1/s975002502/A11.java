package a1.s975002502;

import java.util.Scanner;
import java.io.*;

public class A11 {
	public static void main(String[] args) throws IOException {
		
		//Variables
		String name, stuID;
		BufferedReader buf;
		int temp='e';			//Receive User Input(The ascii code of a character)
		int num=0;				//The inputs of numbers
		int op=0;				//The input of operator
		byte cal=0;				//cal=0 : not calculated  cal=1 : calculated
		int Result=0;
		
		System.out.println("Welcome to the Small Calculator!!");
		
		//*****Let user inputs his user name and student number*****
		
		//Method 1 Use BufferedReader
		Scanner scanner = new Scanner(System.in);		
		System.out.print("Please input your name: ");
		name=scanner.next();
	    
	    //Method 2 Use Scanner	    
	    buf=new BufferedReader(new InputStreamReader(System.in));
	    System.out.print("Please input you student ID number: ");
	    stuID=buf.readLine();
	    
	    //*****Implement a easy ˇ§Calculatorˇ¨!!*****
	    
	    System.out.println("");
	    System.out.println("Current Output: "+num);
	    System.out.println("");
	    
	    temp=Instr(buf);
	    
	    while(temp!='e'){
	    	switch(temp){
	    		case '0':	case '1': 	case '2':	case '3':	case '4':
	    		case '5':	case '6':	case '7':	case '8':	case '9':
	    			num=num*10+temp-48;
	    			
	    			System.out.println("");
	    			System.out.println("Current Output: "+num);
	    			System.out.println("");
	    			break;
	    		case 'a':
	    			System.out.println("");
	    			System.out.println("Current Output: +");
	    			System.out.println("");
	    			
	    			if(cal==0){
	    				op=1;
	    				Result=num;
	    				cal=1;
	    				num=0;
	    			}
	    			else if(cal==1){
	    				Result=Operating(num, Result, op);
	    				
	    				System.out.println("");
	    				System.out.println("Current Output: "+Result);
	    				System.out.println("");
	    				num=0;
	    				op=1;
	    			}
	    			break;
	    		case 's':
	    			System.out.println("");
	    			System.out.println("Current Output: -");
	    			System.out.println("");
	    			
	    			if(cal==0){
	    				op=2;
	    				Result=num;
	    				cal=1;
	    				num=0;
	    			}
	    			else if(cal==1){
	    				Result=Operating(num, Result, op);
	    				
	    				System.out.println("");
	    				System.out.println("Current Output: "+Result);
	    				System.out.println("");
	    				num=0;
	    				op=2;
	    			}
	    			break;
	    		case 'd':
	    			System.out.println("");
	    			System.out.println("Current Output: *");
	    			System.out.println("");
	    			
	    			if(cal==0){
	    				op=3;
	    				Result=num;
	    				cal=1;
	    				num=0;
	    			}
	    			else if(cal==1){
	    				Result=Operating(num, Result, op);
	    				
	    				System.out.println("");
	    				System.out.println("Current Output: "+Result);
	    				System.out.println("");
	    				num=0;
	    				op=3;
	    			}
	    			break;
	    		case 'f':
	    			System.out.println("");
	    			System.out.println("Current Output: /");
	    			System.out.println("");

	    			if(cal==0){
	    				op=4;
	    				Result=num;
	    				cal=1;
	    				num=0;
	    			}
	    			else if(cal==1){
	    				Result=Operating(num, Result, op);
	    				
	    				System.out.println("");
    					System.out.println("Current Output: "+Result);
    					System.out.println("");
	    				num=0;
	    				op=4;
	    			}
	    			break;
	    		case 'w':
	    			Result=Operating(num, Result, op);
	    			
	    			System.out.println("");
	    			System.out.println("Total Result= "+Result);
	    			System.out.println("");
					num=0;
					op=0;
					cal=0;
	    			break;
	    			
	    	    default :
	    	    	System.out.println("");
	    	    	System.out.println("WRONG input!!!");
	    	    	break;
	    	}
	    	
	    	temp=Instr(buf);
   		}
	    System.out.println("");
	    System.out.println("BYEBYE~~"+stuID+"_"+name);
	    System.out.println("");
	}

	//Read the user input
	public static int Instr(BufferedReader buf)throws IOException{
		
		String strtemp;			//Receive User Input(String)
		int temp;
		
		//a list for user to choose
	    System.out.println("***********The Function code***********");
	    System.out.println("a : add                 s : minus");
	    System.out.println("d : multiple            f : divide");
	    System.out.println("w : show information    e : exit");
	    System.out.println("***************************************");
	    System.out.println("");
	    
	    System.out.print("Please choose a number form 0-9 or enter a Funtion code: ");
	    strtemp=buf.readLine();
	    while(strtemp.length()!=1){
	    	System.out.println("");
	    	System.out.println("You chose too MUCH!!");
	    	System.out.println("");
	    	System.out.print("Please choose a number form 0-9 or enter a Funtion code: ");
		    strtemp=buf.readLine();
	    }
	    
	    temp=(int)strtemp.charAt(0);
	    
	    return temp;
	}
	
	//Calculating
	public static int Operating(int num, int Result, int op)
	{	
		switch(op){
		case 1:
			Result+=num;
			break;
		case 2:
			Result-=num;
			break;
		case 3:
			Result*=num;
			break;
		case 4:
			try {Result/=num;}
			catch(ArithmeticException ae)
			{
				Result=0;
				System.out.println("WRONG CALCULATION!!!!");
			}
			break;
		default:
			break;
		}
		return Result;
	}

}

