package a1.s100502002;
import java.util.Scanner;
public class A11 {
	public static void main(String[] arg)
	{
		Scanner input = new Scanner(System.in);
		
		System.out.println("Please enter your name");
		String name = input.nextLine();//輸入名子
		System.out.println("Please enter your student number");
		int sn = input.nextInt();//輸入學號
		
		for(;;)//製造無窮回圈
		{
			
			System.out.println("\nPlease enter the number you want to calculate");//輸出輸入數字的訊息
			int number = input.nextInt();//輸入一個數字
			System.out.println("Please choose which function do you want 1.add 2.minus 3. multiple 4.divide 5.show user information");
			int choose = input.nextInt();//輸入選項
			System.out.println("Please enter the number");
			int number2 = input.nextInt();//輸入第二個數字
			switch(choose){
			case 1://加
				System.out.println("the answer is: ");
				System.out.println(number+number2);
				break;
			case 2://減
				System.out.println("the answer is: ");
				System.out.println (number-number2);
				break;
			case 3://乘
				System.out.println("the answer is: ");
				System.out.println(number*number2);
				break;
			case 4://除
				System.out.println("the answer is: ");
				System.out.println(number/number2);
				break;
			case 5://輸出使用者資料
				System.out.print("Hello!!"+name+"!!");
				System.out.print("your student number is "+sn);
				break;
			default://錯誤的訊息
				System.out.println("error no this function");
				break;
				
				}
			System.out.println("If you want leave please key in 0");//輸出離開方式
			System.out.println("If you don't want to leave please key in any other number");
			int exit = input.nextInt();//離開表示
			if(exit==0)
				break;//離開
		}
		System.out.println(name +" thank you for your use");//輸出最後訊息
		
		
	}
	
}
