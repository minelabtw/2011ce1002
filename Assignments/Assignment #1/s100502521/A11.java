package a1.s100502521;
import java.util.Scanner;//輸入要import的  空白會區分
import java.io.*;//BufferedReadeer要import的  可以有空白
public class A11 
{
	public static void main(String[] args) throws IOException//使用BufferedReader物件的readLine()方法必須處理IOException例外 （exception）  加上throws IOException先不處理例外
	{
		int choice=-1;//作為選項的變數
		double total=0.0,temp;//total目前加總的數字 
		Scanner input = new Scanner(System.in);//先創建一個Scanner物件
		BufferedReader inputLine=new BufferedReader(new InputStreamReader(System.in));//先創建BufferedReader物件
		System.out.println("請輸入姓名:(可以包含空白) ");//印一行 print line
		String name = inputLine.readLine();//用BufferedReader去抓取一整行
		System.out.println("請輸入學號: ");
		String ID = input.next();//用Scanner 去抓接下來空白前的字串
		while(choice!=0)	//0時離開
		{
			System.out.print("<0>離開程式\n<1>加\n<2>減\n<3>乘\n<4>除\n<5>歸零\n<6>使用者資料\n");//直接印 像C++ cout
			System.out.println("目前值為 "+total);
			choice=input.nextInt();
			switch(choice)
			{
			case 1://+
				System.out.print("要加多少呢?");
				temp=input.nextDouble();	
				total=total+temp;
				break;
			case 2://-
				System.out.print("要減多少呢?");
				temp=input.nextDouble();
				total=total-temp;
				break;
			case 3://*
				System.out.print("要乘多少呢?");
				temp=input.nextDouble();
				total=total*temp;
				break;
			case 4://除
				System.out.print("要除多少呢?");
				temp=input.nextDouble();
				total=total/temp;
				break;
			case 5:
				total=0.0;
				break;
			case 6://show information
				System.out.printf("使用者姓名是%s，使用者學號為%s\n",name,ID);//printf像C一樣的用法
				break;
			case 0://Exit
				break;
			default:
				System.out.println("請輸入正確選項!!\n");
				break;
			}
		}
		System.out.printf("%s 掰掰~",name);
	}
}
