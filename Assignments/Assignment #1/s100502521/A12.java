package a1.s100502521;
import java.util.Scanner;//同A11 這裡都數字就用Scanner好了
public class A12 
{
	public static void main(String[] args)//Main函式(方法)  進入點
	{
		int choice=-1;
		double inputNumber;
		Scanner input = new Scanner(System.in);
		System.out.println("歡迎來到攝氏華氏溫度轉換程式!!");
		while(choice!=0)//0時離開
		{
			System.out.print("<0>離開\n<1>攝氏轉華氏\n<2>華氏轉攝氏\n請輸入選項: ");
			choice=input.nextInt();//nextInt 取得整數
			switch(choice)
			{
			case 1:
				System.out.print("請輸入攝氏度數: ");
				inputNumber=input.nextDouble();//取得 double
				System.out.print("華氏結果="+toFahrenheit(inputNumber)+"\n");
				break;
			case 2:
				System.out.print("請輸入華氏度數: ");
				inputNumber=input.nextDouble();
				System.out.print("華氏結果="+toCelsius(inputNumber)+"\n");
				break;
			case 0:
				break;
			default:
				break;
			}
		}
		System.out.println("ByeBye!!");
	}
	public static double toFahrenheit(double c)//方法  將攝氏轉華氏
	{
		return c*9/5+32;
	}
	public static double toCelsius(double f)//方法  將華氏轉攝氏
	{
		return (f-32)*5/9;
	}
}
