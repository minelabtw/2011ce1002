package a1.s100502020;

import java.util.Scanner;//import scanner for input

public class A12 {
	public static double toFahrenheit(double num)	{//method calculating C to F
		double ans;
		ans = ((9.0 / 5.0) * num) + 32;
		return ans;
	}
	public static double toCelsius(double num){//method calculating F to C
		double ans;
		ans = (num - 32) * 5.0 / 9.0;
		return ans;
	}
	public static void main(String[] args){//main
		Scanner input = new Scanner(System.in);
		double temp;
		System.out.print("1.From Celsius to Fahrenheit\n2.From Fahrenheit to Celsius\n");
		int choice;
		choice = input.nextInt();//java's cin
		switch(choice){
		case 1:
			System.out.print("Input temp u wish to change: ");
			temp = input.nextDouble();
			System.out.print("The changed temp: "+ toFahrenheit(temp) + " F\n");//output
			break;
		case 2:
			System.out.print("Input temp u wish to change: ");
			temp = input.nextDouble();
			System.out.print("The changed temp: "+ toCelsius(temp) + " C\n");//output
			break;
		
		}
		
	}

}
