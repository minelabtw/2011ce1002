package a1.s100502033;
import java.util.Scanner;
import javax.swing.JOptionPane;
import java.io.*;
public class A11 
{
	public static void main (String args[])
	{
		Scanner input = new Scanner(System.in);
		System.out.println("write down your name");
		int number , number1;
		Double number2 , number3 , number4 ;
		String name ;
		name = input.next();
		System.out.println("\nwrite down your student number");
		number = input.nextInt();
		
		for (int i = 0 ; i < 3 ; i--) //無限迴圈
		{
			System.out.println("Select a function:1.add 2.minus 3.multiple 4.divide 5.show user information 6.exit");
			number1 = input.nextInt();
			switch(number1) //跟據NUMBER1來選擇功能
			{
				case 1: //NUMBER1 等於1時 ，運行加法
					System.out.println("first number:");
					number2 = input.nextDouble();
					System.out.println("second number:");
					number3 = input.nextDouble();
					number4 = number2 + number3 ;
					System.out.printf(number2  + " + "  + number3  +  " = " + number4 + "\n");
					break;
					
				case 2: //NUMBER1 等於2時，運行減法
					System.out.println("first number:");
					number2 = input.nextDouble();
					System.out.println("second number:");
					number3 = input.nextDouble();
					number4 = number2 - number3 ;
					System.out.printf(number2  + " - "  + number3  +  " = " + number4 + "\n");
					break;
					
				case 3: //NUMBER1 等於3時，運行乘法
					System.out.println("first number:");
					number2 = input.nextDouble();
					System.out.println("second number:");
					number3 = input.nextDouble();
					number4 = number2 * number3 ;
					System.out.printf(number2  + " * "  + number3  +  " = " + number4 + "\n");
					break;
				
				case 4: //NUMBER1 等於4時，運行除法
					System.out.println("first number:");
					number2 = input.nextDouble();
					System.out.println("second number:");
					number3 = input.nextDouble();
					number4 = number2 / number3 ;
					System.out.printf( number2  + " / "  + number3  +  " = " + number4 + "\n");
					break;
					
				case 5: //NUMBER1 等於5時，顯示使用者資料
					System.out.printf("user name:" + name + "\n" + "user student number:" + number + "\n");
					break;
					
				case 6: //NUMBER1 等於6時，強制結束
					JOptionPane.showMessageDialog(null,"GOODBYE " + name); //彈出視窗
					System.exit(0); //強制結束
					break;
			}
		}
		
	}
}
