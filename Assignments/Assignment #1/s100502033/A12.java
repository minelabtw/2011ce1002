package a1.s100502033;
import java.util.Scanner;
import javax.swing.JOptionPane;
import java.io.*;
public class A12 
{

	public static void main (String args[]) 
	{
		Scanner input = new Scanner(System.in);
		double Temperature;
		int number;
		for(int i =0 ; i < 3 ; i-- ) //無限迴圈
		{
			System.out.println("請選擇一種功能:1.攝氏轉華氏 2.華氏轉攝氏 3.離開");
			number = input.nextInt();
			switch(number) //根據NUMBER選擇功能
			{
				case 1:
					System.out.println("請輸入一個溫度:");
					Temperature = input.nextDouble();
					System.out.println( toFahrenheit(Temperature )); //呼叫toFahrenheit
					break;
				
				case 2:
					System.out.println("請輸入一個溫度:");
					Temperature = input.nextDouble();
					System.out.println( toCelsius(Temperature )); //呼叫 toCelsius
					break;
				
				case 3:
					JOptionPane.showMessageDialog(null, "Thank you");//彈出視窗
					System.exit(1);
			}
		 }
	}
	public static double toFahrenheit(double number) //宣告一個METHOD
	{
		System.out.println("攝氏溫度為:\n" + number + "\n" + "華氏溫度為:");
		return (number * 9) / 5 + 32; //傳回華氏溫度的值
	}
	public static double toCelsius(double number) //宣告一個METHOD
	{
		System.out.println("華氏溫度為:\n" + number + "\n" + "攝氏溫度為:");
		return (number - 32) * 5 / 9; //傳回攝氏溫度的值
	}
}
