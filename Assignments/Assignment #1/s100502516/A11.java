package A1.s100502516;

import java.util.Scanner;

public class A11 {
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);//create a Scanner object
		
		System.out.print("Enter your name: ");
		String name  = input.nextLine();		
		System.out.print("Enter your student number: ");
		String stuNumber = input.nextLine();
	
		double num = 0;		//Calculator number
		boolean exit = false;		//represent the end of while
		
		while(!exit) {		
			System.out.print("\nCalculator!!\n" +	"1. Sum\n" + "2. Minus\n" +
					"3. Multiple\n" + "4. Divide\n" + "5. Show user information\n" + 
					"6. Exit\n\n"  + "Number now: " + num + 
					"\n\nChoose your selection: ");
			int choose = input.nextInt();		//user's selection
			
			while(choose < 1 || choose > 6) {
				System.out.print("Invalid enter, please enter again: ");
				choose = input.nextInt();
			}
			
			switch(choose) {
				case 1:
					System.out.print("Enter number: ");
					num += input.nextDouble();
					break;
				case 2:
					System.out.print("Enter number: ");
					num -= input.nextDouble();
					break;
				case 3:
					System.out.print("Enter number: ");
					num *= input.nextDouble();
					break;
				case 4:
					System.out.print("Enter number: ");
					num /= input.nextDouble();
					break;
				case 5:
					System.out.println("\nUser information: \n\nName: " + name + 
							"\nStudent number: " + stuNumber + "\n");
					break;
				case 6:
					exit = true;
					break;
			}
		}
		
		System.out.println("\n�p�D bye bye");
		
	}
}
