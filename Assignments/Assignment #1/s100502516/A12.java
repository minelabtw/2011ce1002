package A1.s100502516;

import java.util.Scanner;

public class A12 {
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		boolean exit = false;		//represent the end of while
		double temper;		//temperature
		
		while(!exit) {		
			System.out.print("\n1. toFahrenheit\n" + "2. toCelsius\n" + 
					"3. exit\n\n" + "Choose your selection: ");
			
			int choose = input.nextInt();		//user's selection
			
			while(choose < 1 || choose > 3) {
				System.out.print("Invalid enter, please try again: ");
				choose = input.nextInt();
			}
			
			switch(choose) {
				case 1:					
					System.out.print("Enter the temperature: ");
					temper = input.nextDouble();
					if(temper < -273)
						System.out.println("Invalid temperature.");
					else
						System.out.println("\nFahrenheit temperature is " + toFahrenheit(temper));
					break;
				case 2:
					System.out.print("Enter the temperature: ");					
					temper = input.nextDouble();
					if(temper < -169)
						System.out.println("Invalid temperature.");
					else
						System.out.println("\nCelsius temperature is " + toCelsius(temper));
					break;
				case 3:
					exit = true;
					break;
			}
		}
		
		System.out.print("�p�D bye bye");
	}
	

	public static double toFahrenheit(double celsius) {		//Celsius to Fahrenheit
		return celsius * 9 / 5 + 32;
	}
	
	public static double toCelsius(double Fahrenheit) {		//Fahrenheit to Celsius
		return (Fahrenheit - 32) * 5 / 9;
	}
}
