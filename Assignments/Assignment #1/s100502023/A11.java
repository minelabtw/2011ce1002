package a1.s100502023;

import java.util.Scanner;

public class A11 
{
	public static void main(String[] args)
	{
		Scanner input =new Scanner(System.in);
		
		System.out.print("Please input your name :");
		String userName=input.nextLine();
		
		System.out.print("Please input your student number :");
		int studentNumber=input.nextInt();
		
		boolean done=false;  //用於while迴圈，判斷是否結束操握
		
		double sum=0;  //起始總額為0
		int counter=0;  //起始總操作次赴為0
		
		while (!done)
		{
			counter+=1;  //計算共操作幾次
			
			System.out.println("----------------------------");
			System.out.printf("                    Sum:%.3f\n",sum);  //只顯示到小數點後第3位
			System.out.println("choose  function");
			System.out.println("(1)add");
			System.out.println("(2)minus");
			System.out.println("(3)multiple");
			System.out.println("(4)divide");
			System.out.println("(5)show information");
			System.out.println("(0)exit");
			
			int choice=input.nextInt();
			
			switch (choice)
			{
				case 1:
					sum=methodofAdd(sum);  //add
					break;
				case 2:
					sum=methodofMinus(sum);  //minus
					break;
				case 3:
					sum=methodofMultiple(sum);  //multiple
					break;
				case 4:
					sum=methodofDivide(sum);  //divide
					break;
				case 5:
					methodofShowInformation(sum,userName,studentNumber,counter);  //show information
					break;
				case 0:
					methodofShowInformation(sum,userName,studentNumber,counter);  ////show information and exit
					System.out.println("Good Bye");
					done=true;
					break;
				default :
					System.out.println("沒有這個選項，請重新開始");
					done=true;
					break;
				
			}
			
		}
		
	}
	
	public static double methodofAdd(double sum)
	{
		System.out.print("Please input the number:");  //number 為使用者輸入的數字
		Scanner input =new Scanner(System.in);
		double number=input.nextDouble();  
		
		while (number<0)
		{
			System.out.println("請輸入一個正數 ，請重新輸入");
			System.out.print("Please input the number:");
			number=input.nextDouble();			
		}
		
		sum+=number;
		
		return sum;
		
	}
	
	public static double methodofMinus(double sum)
	{
		System.out.print("Please input the number:");  //number 為使用者輸入的數字
		Scanner input =new Scanner(System.in);
		double number=input.nextDouble();
		
		while (number<0)
		{
			System.out.println("請輸入一個正數 ，請重新輸入");
			System.out.print("Please input the number:");
			number=input.nextDouble();			
		}
				
		sum-=number;
		
		return sum;
		
	}
	
	public static double methodofMultiple(double sum)
	{
		System.out.print("Please input the number:");  //number 為使用者輸入的數字
		Scanner input =new Scanner(System.in);
		double number=input.nextDouble();
								
		sum*=number;
		
		return sum;
		
	}
	
	public static double methodofDivide(double sum)  
	{
		System.out.print("Please input the number:");  //number 為使用者輸入的數字
		Scanner input =new Scanner(System.in);
		double number=input.nextDouble();
		
		while (number==0)  //除數不可為0
		{
			System.out.println("除數不可為0，請重新輸入");
			System.out.print("Please input the number:");
			number=input.nextDouble();
		}
		
		sum/=number;
		
		return sum;
		
	}

	public static void methodofShowInformation(double sum,String userName,int studentNumber,int counter)
	{
		System.out.println("--------------------");
		System.out.println("Name:" + userName);
		System.out.println("StudentNumber:" + studentNumber);
		System.out.println("Sum:" + sum);
		System.out.println("總操作次數:" + counter);
		
	}
	
}
