package a1.s100502023;

import java.util.Scanner;

public class A12 
{
	public static void main(String[] args)
	{
		Scanner input =new Scanner(System.in);
		
		System.out.println("toFahrenheit:changes user input from Celsius to Fahrenheit.");
		System.out.println("Please input the temperature :  (Celsius)");
		double tem=input.nextDouble();  //tem 為使用者欲轉換的溫度
		
		toFahrenheit(tem);
		
		System.out.println("toCelisus: changes user input from Fahrenheit to Celsius.");
		System.out.println("Please input the temperature :  (Fahrenheit)");
		tem=input.nextDouble();
		
		toCelisus(tem);
	}
	
	public static void toFahrenheit(double tem)
	{
		double temperature;
		
		temperature=tem*9/5+32;
		
		System.out.println("Fahrenheit : "+temperature);
	}
	
	public static void toCelisus(double tem)
	{
		double temperature;
		
		temperature=(tem-32)*5/9;
		
		System.out.println("Celsius : "+temperature);
	}

}
