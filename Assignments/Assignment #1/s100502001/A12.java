package a1.s1005202001;
import java.util.Scanner;
public class A12 {
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);
		System.out.print("Enter the Celsius: ");
		double c=input.nextDouble();
		System.out.println("The Celsius: "+c+"\nAnd equal to Fahrenheit:"+toFahrenheit(c));
		System.out.print("\nEnter the Fahrenheit: ");
		double f=input.nextDouble();
		System.out.println("The Fahrenheit: "+f+"\nAnd equal to Fahrenheit:"+toCelsius(f));
		
	}
	public static double toFahrenheit(double C){
		return C*9/5+32;
	}
	public static double toCelsius(double F){
		return (F-32)*5/9;
	}

}
