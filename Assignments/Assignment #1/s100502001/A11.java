package a1.s100502001;
import java.util.Scanner;
public class A11 {
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);
		System.out.print("Your name: ");
		String name=input.next();//input user name
		System.out.print("Your school numbers: ");
		int num=input.nextInt();//input school number
		System.out.print("Number1: ");
		double num1=input.nextDouble();//the first number to calculate
		double sum=num1;//the operating result
		boolean judge=true;
		while(judge){
			System.out.println("1:add\n2:minus\n3:multiple\n4:divide\n5:show user information & Operating result\n6:Refresh or Exit");
			int choice=input.nextInt();
			switch(choice){
				case 1:
					double add_num=input.nextDouble();
					sum+=add_num;//to plus
					break;
				case 2:
					double sub_num=input.nextDouble();
					sum-=sub_num;//to subtract
					break;
				case 3:
					double mul_num=input.nextDouble();
					sum*=mul_num;//to multiple
					break;
				case 4:
					double div_num=input.nextDouble();
					if(div_num!=0)//to divide
						sum/=div_num;
					else
						System.out.println("Error!!= =");//denominator is not equal to zero
					break;
				case 5:
					System.out.println("Name: "+name+"\nSchool number: "+num+"\nThe result: "+sum);//show message & operating result
					sum=0;
					break;
				case 6:
					System.out.println("1:Refresh\n2:Exit");
					int cont=input.nextInt();
					if(cont==2){ //finish the input
						judge=false;
						System.out.print("Thank you~~bye bye^^");
					}
					else if(cont==1){ //refresh the calculator
						System.out.print("Number1: ");
						num1=input.nextDouble();
					}
					else
						System.out.println("There is not the choice.");
					break;
				default:
					System.out.println("There is no the choice.>.<");
					break;
					
			}
			
		}
	}

}
