package a1.s100502027;

import java.util.Scanner;  //get the 'input' function

public class A12 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);  //set the 'input' function
		int choice1 = 0;
		double originT = 0 ;
		while(choice1<1){  //use 'while' to repeat 
			System.out.println();
			System.out.println("Have two funtions to use.");
			System.out.println("1:�XC-> �XF");
			System.out.println("2:�XF-> �XC");
			System.out.println("or  3: exit");
			System.out.println("Which one do you want to do ? :");
			int choice2 = input.nextInt();
			switch(choice2){   //use 'switch' to choose function
				case 1:
					System.out.println("Please enter the Celsius that you want to change :");
					originT = input.nextDouble();
					System.out.println("The Fahrenheit is :" + toFahrenheit(originT));
					break;
				case 2:
					System.out.println("Please enter the Fahrenheit that you want to change :");
					originT = input.nextDouble();
					System.out.println("The Celsius is :" + toCelsius(originT));
					break;
				case 3:
					choice1 = 1 ;  //change the choice1 to end 'while'
					System.out.println("Exit...  Good-bye!");
					break;
				default :   //To warning the error  
					System.out.println("Error ! Please enter the correct number to use.");
					break;
			}
		}
	}
	public static double toFahrenheit(double t){
		double f=0;   // �XC->�XF 
		f= t*9/5 +32;
		return f ;
	}
	public static double toCelsius(double t){
		double c=0;   //�XF-> �XC
		c= (t-32)*5 /9;
		return c;
	}

}
