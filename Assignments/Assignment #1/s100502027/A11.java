package a1.s100502027;

import java.util.Scanner; //get the 'input' function

public class A11 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);  //set the 'input' function
		System.out.print("Please enter your name : ");
		String username = input.next();
		System.out.println("Please enter your student number : ");
		int studentnumber = input.nextInt();
		int choice1 = 0;
		double result = 0;
		while(choice1<1){   //use 'while' to repeat 
			System.out.println();
			System.out.println("1:add");
			System.out.println("2:minus");
			System.out.println("3:multiple");
			System.out.println("4:divide");
			System.out.println("5:show user information");
			System.out.println("6:exit");
			System.out.println("Please enter number to choose:");
			int choice2 = input.nextInt();
			switch(choice2){   //use 'switch' to choose function
				case 1 :
				case 2 :
				case 3 :
				case 4 :
					System.out.println("Please enter the first number :");
					double numberA = input.nextDouble();
					System.out.println("Please enter the second number :");
					double numberB = input.nextDouble();
					switch(choice2){   
						case 1 :
							result = numberA + numberB ;
							System.out.println("The count result is : " + result);
							break;
						case 2 :
							result = numberA - numberB ;
							System.out.println("The count result is : " + result);
							break;
						case 3 :
							result = numberA * numberB ;
							System.out.println("The count result is : " + result);
							break;
						case 4 :
							while(numberB==0){   //use 'while' to make sure 
								System.out.println("Error!Second number = 0");
								System.out.println("Please enter correct number:");
								numberB = input.nextDouble();
							}
							result = numberA / numberB ;
							System.out.println("The count result is : " + result);
							break;
					}
					break;
				case 5 :
					System.out.println("User name :" + username);
					System.out.println("Student number :" + studentnumber);
					break ;
				case 6 :
					choice1 = 1;  //change the choice1 to end 'while'
					break ;
				default :
					System.out.println("ERROR!"); //To warning the error  
					System.out.println("Please enter the correct function");
					break ;
			}
		}
		System.out.println("Program is closing. Good-bye!");
	}

}
