//A11: simple calculator.

package A1.s100502503;

import java.util.Scanner;

public class A11 {
	public static double ans = 0;
	public static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args)
	{	
		System.out.println("Plese enter your name and student number.");
		
		String name = input.next();
		int studentNumber = input.nextInt();
		
		//the flag is the sign to determine whether keep go on or not.  
		boolean flag = true;
		
		while(flag)
		{
			System.out.print("\n\n1: plus " + "2: minus " + "3: multiple " + "4: divide " + "5: show the imformation + 0: exit");
			System.out.print("\nPlease choose the function you want to use:");
			
			int chosen = input.nextInt();
			
			//the program has 5 function, the user choose the function, then key in the number to calculate,
			//it will show out the answer immediately.
			switch(chosen)
			{
				case 1:
					System.out.print("Please enter the number you want to add: ");
					double plusNumber = input.nextDouble();
					System.out.print("The answer is: " + plus(plusNumber));
					break;
					
				case 2: 
					System.out.print("Please enter the number you want to minus: ");
					double minusNumber = input.nextDouble();
					System.out.print("The answer is: " + minus(minusNumber) );
					break;
					
				case 3:
					System.out.print("Please enter the number you want to mutiple: ");
					double mutipleNumber = input.nextDouble();
					System.out.print("The answer is: " + mutiple(mutipleNumber));
					break;
					
				case 4:
					System.out.print("Please enter the number you want to divide: ");
					double divideNumber = input.nextDouble();
					System.out.print("The answer is: " + divide(divideNumber));
					break;
				
				case 5:
					System.out.print("Name: " + name + "\nstudent number: " + studentNumber);
					break;
				
				case 0:
					System.out.println("Goodbye " + name + studentNumber);
					flag = false;
					break;
				//when the flag become false, the program will stop.
			}
		}
	}
	
	//the function of these four method is calculating the number of the user key in.
	public static double plus(double number)
	{
		ans = ans + number;
		return ans;
	}
	
	public static double minus(double number)
	{
		ans = ans - number;
		return ans;
	}

	public static double mutiple(double number)
	{
		ans = ans * number;
		return ans;
	}
	
	public static double divide(double number)
	{
		ans = ans / number;
		return ans;
	}
}
