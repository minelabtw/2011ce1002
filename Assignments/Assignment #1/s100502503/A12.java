//A12: calculate the degrees.

package A1.s100502503;

import java.util.Scanner;

public class A12
{
	public static double ans = 0; 
	public static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		System.out.println("Plese choose the statement." + "\n1: Fahrenheit turns to Celisus." + "\n2: Celius turns to Fahrenheit.");
		
		int chosen = input.nextInt();
		//choose which statement to determine F to C or exchanged.
		switch(chosen)
		{
			case 1:
				System.out.println("Plese enter Fahrenheit.");
				double inputFahrenheit = input.nextDouble();
				System.out.print("Celius = " + toCelsius(inputFahrenheit));
				break;
				
			case 2:
				System.out.println("Please enter Celisius");
				double inputCelisius = input.nextDouble();
				System.out.print("Fahrenheit = " + toFahrenheit(inputCelisius));
				break;
		}
		
		
	}
	
	//these two method are the function to calculate the degrees.
	public static double toCelsius(double number)
	{
		ans = (number - 32) * (5/9);
		return ans;
	}
	
	public static double toFahrenheit(double number)
	{
		ans = number * (5/9) + 32;
		return ans;
	}
}
