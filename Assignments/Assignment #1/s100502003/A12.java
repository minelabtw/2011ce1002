package a1.s100502003;

import java.util.Scanner;

public class A12 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create a scanner object
		
		System.out.print("Please enter a temperature: "); // let the user to enter a temperature
		double temp = input.nextDouble();
		// call methods and change the temperature
		System.out.println("If the temperature is Celsius, after changing into Fahrenheit, it will be: " + toFahrenheit(temp)); 
		System.out.println("If the temperature is Fahrenheit, after changing into Celsius, it will be: " + toCelsius(temp));
	}
	
	public static double toFahrenheit(double cel_temp) { // method that changes user input from Celsius to Fahrenheit
		return cel_temp*9/5+32;
	}
	
	public static double toCelsius(double fah_temp) { // method that changes user input from Fahrenheit to Celsius
		return (fah_temp-32)*5/9;
	}
}
