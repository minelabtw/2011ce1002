package a1.s100502003;

import java.util.Scanner;

public class A11 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // create a scanner object
		
		// let the user enter his name & student ID
		System.out.print("Please enter your name: ");
		String name = input.nextLine();
		System.out.print("Please enter your student ID: ");
		String studentID = input.nextLine();
		
		// print informations that let the user know how to use the system
		System.out.println("Hello, " + name + "! It's a calculator.");
		System.out.println("There will have 5 functions for you to choose.");
		System.out.println("1 is for adding the numbers you entered.");
		System.out.println("2 is for substrating the numbers you entered.");
		System.out.println("3 is for multipling the numbers you entered.");	
		System.out.println("4 is for dividing the numbers you entered.");
		System.out.println("5 is for leaving the system.");
		
		System.out.print("Now, please enter an initial number: ");
		double num1 = input.nextDouble(); // enter first number
		System.out.print("Please enter a second number to have an operation with your former number: ");
		double num2 = input.nextDouble(); // enter second number
		System.out.print("Which function do you want to do with the numbers you entered (choose 1~5): ");
		int choice = input.nextInt(); // enter the user's choice
		
		int function = choice; // store the user's choice into another variable
		choice = 0; // let the choice be any number that is not in the scope of 1~5 (here set it as 0)
		double number_new, result_temp, result; // declare other variables that is going to used later 
		result_temp = num1; // let the first number entered by the user be a temporarily result
		number_new = num2; // let the second number entered by the user be a new number been inputed into the system
		
		while(choice != 5) { // if the user doesn't choose to exit, let the loop continue
			switch(function) {
				case 1: // add
					result = result_temp + number_new; // the operation is between the former solution(temporarily result) and the new number
					System.out.println("The result is: " + result);
					result_temp = result; // let the result just get become the temporarily result
					break;
				case 2: // minus
					result = result_temp - number_new;
					System.out.println("The result is: " + result);
					result_temp = result;
					break;
				case 3: // multiple
					result = result_temp * number_new;
					System.out.println("The result is: " + result);
					result_temp = result;
					break;
				case 4: // divide
					result = result_temp / number_new;
					System.out.println("The result is: " + result);
					result_temp = result;
					break;
				case 5: // exit
					System.out.println(name + ", whose ID is " + studentID + ", thank's for your using!");
					System.out.println("See you next time~");
					choice = function; // let choice become 5 and break the loop
					break;
				default: // other situations
					System.out.println("Sorry! The number you entered is not meaningful!");
					System.out.println("Please try again!!");
					break;
			}
			if(function != 5) { // if the user doesn't choose to leave, continue the system
				System.out.print("Which function do you want to use later (choose 1~5): ");
				function = input.nextInt();
			}
			if(function ==1 || function ==2 || function ==3 || function == 4 ) { // as soon as the user choose 1~4, let him enter a new number
			System.out.print("Please enter a new number: ");
			number_new = input.nextDouble();
			}
		}
	}
}
