package a1.s100502506;
import java.util.Scanner;

public class A11 
{
	private double result;
	//Methods
	public void add(double x,double y)//加法
	{
		result=x+y;
	}
	public void minus(double x,double y)//減法
	{
		result=x-y;
	}
	public void multiple(double x,double y)//乘法
	{
		result=x*y;
	}
	public void divide(double x,double y)//除法
	{
		result=x/y;
	}
	public double showresult()//回傳結果值
	{
		return result;
	}
	public void setresult(double x)//設置結果
	{
		result=x;
	}
	public void showinformation(String user,String snumber)//顯示
	{
		System.out.printf("hello %s-%s, welcome to use this Calculator\n",user,snumber);
	}
	public void exit()//跳出
	{
		return;
	}
	//主程式
	public static void main(String argc[])
	{
		String userName,studentNumber;
		double number1,number2;
		int question1,question2,question3=0;
		A11 mycalculator =new A11();
		Scanner input = new Scanner(System.in);
		
		//輸入名字與學號
		System.out.println("Please enter your name.");
		userName=input.next();
		System.out.println("Please enter your student number.");
		studentNumber=input.next();
		//計算機
		do//是否再次使用計算機
		{
			System.out.println("Calculate");
			System.out.println("Please enter a number");//輸入第一個數
			number1=input.nextDouble();
			do//是否做連續運算
			{
				//計算機多個功能
				System.out.println("Calculator has five fuction as follows");
				System.out.println("1.Add");
				System.out.println("2.Minus");
				System.out.println("3.Multiple");
				System.out.println("4.Divide");
				System.out.println("5.Show information");
				System.out.println("6.Exit");
				System.out.println("Which fuction do you want to use?");
				question1=input.nextInt();
				
				switch(question1)
				{
					case 1://加法
						System.out.printf("%.2f + ? = \n", number1);
						System.out.println("Please enter the other number");
						number2=input.nextDouble();
						mycalculator.add(number1, number2);
						System.out.printf("%.2f + %.2f = %.2f\n",number1,number2,mycalculator.showresult());
						number1=mycalculator.showresult();//將結果傳至number1以便連續運算
						break;
					case 2://減法
						System.out.printf("%.2f - ? = \n", number1);
						System.out.println("Please enter the other number");
						number2=input.nextDouble();
						mycalculator.minus(number1, number2);
						System.out.printf("%.2f - %.2f = %.2f\n",number1,number2,mycalculator.showresult());
						number1=mycalculator.showresult();//將結果傳至number1以便連續運算
						break;
					case 3://乘法
						System.out.printf("%.2f * ? = \n", number1);
						System.out.println("Please enter the other number");
						number2=input.nextDouble();
						mycalculator.multiple(number1, number2);
						System.out.printf("%.2f * %.2f = %.2f\n",number1,number2,mycalculator.showresult());
						number1=mycalculator.showresult();//將結果傳至number1以便連續運算
						break;
					case 4://除法
						System.out.printf("%.2f / ? = \n", number1);
						System.out.println("Please enter the other number");
						number2=input.nextDouble();
						mycalculator.divide(number1, number2);
						System.out.printf("%.2f / %.2f = %.2f\n",number1,number2,mycalculator.showresult());
						number1=mycalculator.showresult();//將結果傳至number1以便連續運算
						break;
					case 5://顯示訊息
						mycalculator.showinformation(userName, studentNumber);
						break;
					case 6://離開計算機
						mycalculator.exit();
						break;
					default://輸入1~6以外數值
						System.out.println("Please enter 1~6");
						break;
				}
				System.out.println("Whether you want to continue to calculate? yes:1 no:2");
				question2=input.nextInt();
			}while(question2==1);
			System.out.println("Whether you want to clear numbers and use calculator again or not? yes:1 no:2");
			question3=input.nextInt();
			
		}while(question3==1);
		
		
		System.out.printf("Goodbye %s-%s, have a nice holiday!", userName,studentNumber);
		
	}
}
