package a1.s100502506;
import java.util.Scanner;

public class A12 
{
	
	private double result;
	//Methods
	public void toFahrenheit(double C)//攝氏轉華氏
	{
		result=9*(C/5)+32;
	}
	
	public void toCelsius(double F)//華氏轉攝氏
	{
		result=5*(F-32)/9;
	}
	public double show()//顯示結果
	{
		return result;
	}
	public void exit()//離開
	{
		return;
	}
	
	//主程式
	public static void main(String argc[])
	{
		Scanner input = new Scanner(System.in);
		int question1,question2;
		double number1;
		A12 Transformer =new A12();
		do//是否重複使用溫度轉換
		{
			System.out.println("Which fuction do you want to use?");
			System.out.println("1.ToFahrenheit");
			System.out.println("2.ToCelsius");
			question1=input.nextInt();
			switch(question1)
			{
				case 1://攝氏轉華氏
					System.out.println("Enter the number you want to transform");
					number1=input.nextDouble();
					Transformer.toFahrenheit(number1);
					System.out.printf("%.2f celsius to fahrenheit is %.2f\n",number1,Transformer.show());
					break;
				case 2://華氏轉攝氏
					System.out.println("Enter the number you want to transform");
					number1=input.nextDouble();
					Transformer.toCelsius(number1);
					System.out.printf("%.2f fahrenheit to celsius is %.2f\n",number1,Transformer.show());
					break;
				case 3://離開溫度轉換
					Transformer.exit();
					break;
				default://輸入1~3以外的數值
					System.out.println("Please enter 1~3");
					break;
			}
			System.out.println("Whether you want to do again or not? yes:1 no:2");
			question2=input.nextInt();
			
		}while(question2==1);
		System.out.println("Thanks for using");
	}

}
