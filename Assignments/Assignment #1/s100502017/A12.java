package a1.s100502017;

import java.util.Scanner;//for input

public class A12 {
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);//for input
		int choise;
		double tem;
		boolean key=true;// declare a boolean to control while loop
		while(key){//use key to determine whether the loop continue or not
			System.out.println("1.Change Celsius to Fahrenheit");
			System.out.println("2.Change Fahrenheit to Celsius");
			System.out.println("3.exit");
			System.out.println("choise functionality: ");
			choise=input.nextInt();
			switch(choise){//use switch statement to determine the choice
			case 1://Celsius to Fahrenheit
				System.out.println("Please enter the temperature");
				tem=input.nextDouble();
				tem=toFahrenheit(tem);
				System.out.println("The resault is :"+tem+" F");
				break;
			case 2://Fahrenheit to Celsius
				System.out.println("Please enter the temperature");
				tem=input.nextDouble();
				tem=toCelsius(tem);
				System.out.println("The resault is :"+tem+" C");
				break;
			case 3://exit
				System.out.println("Thank for using!");
				key=false;//let key=false and end the loop
				break;
			default:
				System.out.println("The number is wrong");
				break;
			}
		}
	}//end of main method
	public static double toFahrenheit(double cel){//a method changes user input from Celsius to Fahrenheit
		double fah=cel*1.8+32;
		return fah;
	}
	public static double toCelsius(double fah){//a method changes user input from Fahrenheit to Celsius
		double cel=(fah-32)*5/9;
		return cel;
	}
}
