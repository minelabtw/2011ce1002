package a1.s100502017;

import java.util.Scanner;//for input

public class A11 {
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);//for input
		System.out.println("Please enter your name: ");
		String username=input.next();//declare and read a string
		System.out.println("Please enter your student number: ");
		String number=input.next();//declare and read a string
		boolean key=true;// declare a boolean to control while loop
		int choise;
		double num1,num2,ans;
		while(key){//use key to determine whether the loop continue or not
			System.out.println("1.add");
			System.out.println("2.minus");
			System.out.println("3.multiple");
			System.out.println("4.divide");
			System.out.println("5.exit");
			System.out.println("choise functionality: ");
			choise=input.nextInt();
			switch(choise){//use switch statement to determine the choice
				case 1://add
					System.out.println("Enter two number(a+b): ");
					num1=input.nextDouble();
					num2=input.nextDouble();
					ans=num1+num2;
					System.out.println("The anser is "+ans);
					break;
				case 2://minus
					System.out.println("Enter two number(a-b): ");
					num1=input.nextDouble();
					num2=input.nextDouble();
					ans=num1-num2;
					System.out.println("The anser is "+ans);
					break;
				case 3://multiple
					System.out.println("Enter two number(a*b): ");
					num1=input.nextDouble();
					num2=input.nextDouble();
					ans=num1*num2;
					System.out.println("The anser is "+ans);
					break;
				case 4://divide
					System.out.println("Enter two number(a/b): ");
					num1=input.nextDouble();
					num2=input.nextDouble();
					ans=num1/num2;
					System.out.println("The anser is "+ans);
					break;
				case 5:
					System.out.println("Goodbye "+username+" !");
					key=false;//let key=false and end the loop
					break;
				default:
					System.out.println("The number is wrong");
					break;
			}
		}
	}
}
