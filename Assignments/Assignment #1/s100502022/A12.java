package a1.s100502022;
import java.util.Scanner;

public class A12 {
	public static double toFahrenheit(double temp){
		double Fah=temp*9/5+32;
		return Fah;
	}
	public static double toCelsius(double temp){
		double Cel=(temp-32)*9/5;
		return Cel;
	}
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);
		System.out.println("Plz enter ur CelsiusTemp= \n");
		double CelTemp=input.nextDouble();
		System.out.println("Plz enter ur FahrenheitTemp= \n");
		double FahrTemp=input.nextDouble();
		double F=toFahrenheit(CelTemp);
		double C=toCelsius(FahrTemp);
		System.out.printf("CelsiusTemp %e to FahrenheitTemp =%e\n",CelTemp,F);
		System.out.printf("FahrenheitTemp %e to CelsiusTemp =%e",FahrTemp,C);
	}
}
