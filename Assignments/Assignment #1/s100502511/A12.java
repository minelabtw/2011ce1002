package a1.s100502511;

import java.util.Scanner; //使用Scanner類別

public class A12 {
	public double toFahrenheit(double number) {
		double Celisus = 0;
		Celisus = ((number - 32) / 9) * 5;
		return Celisus;
	}

	public double toCelisus(double number) {
		double Fahrenheit = 0;
		Fahrenheit = (number * 9) / 5 + 32;
		return Fahrenheit;
	}

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); //取得使用者輸入

		A12 a12 = new A12();

		System.out.println("Please input the number you want: ");
		double num = input.nextDouble();

		System.out.print("1. toFahrenheit: ");
		System.out.println(a12.toFahrenheit(num) + " F");
		System.out.print("2. toCelisus: ");
		System.out.println(a12.toCelisus(num) + " C");

	}

}
