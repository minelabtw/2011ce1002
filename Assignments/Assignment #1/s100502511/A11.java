package a1.s100502511;

import java.util.Scanner; //使用Scanner類別

public class A11 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); //取得使用者輸入

		System.out.println("Please input user's name: ");
		String username = input.next();

		System.out.println("Please input user's studentnumber: ");
		int studentnumber = input.nextInt();

		for (;;) {
			System.out.println("Input first integer: ");
			double num1 = input.nextDouble();
			System.out.println("Input Second integer: ");
			double num2 = input.nextDouble();

			System.out.println("1. Add");
			System.out.println("2. Minus");
			System.out.println("3. Multiple");
			System.out.println("4. Divide");
			System.out.println("5. Show information");
			System.out.println("6. Exit");

			System.out.println("Choose function: ");
			int functionnumber = input.nextInt();

			double answer = 0;

			switch (functionnumber) {
			case 1:
				answer = num1 + num2;
				System.out.printf("Answer is %f \n", answer);
				break;
			case 2:
				answer = num1 - num2;
				System.out.printf("Answer is %f \n", answer);
				break;
			case 3:
				answer = num1 * num2;
				System.out.printf("Answer is %f \n", answer);
				break;
			case 4:
				answer = num1 / num2;
				System.out.printf("Answer is %f \n", answer);
				;
				break;
			case 5:
				System.out.println("user: " + username);
				System.out.println("user's studentnumber: " + studentnumber);
				break;
			case 6:
				System.out.printf("Bye-bye %s", username);
				System.exit(0);
				break;
			}
		}

	}

}
