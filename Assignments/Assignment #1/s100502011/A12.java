package a1.s100502011;

import java.util.Scanner;

public class A12 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		for(;;){
			System.out.println("1.Change Celsius to Fahrenheit");
			System.out.println("2.Change Fahrenheit to Celsius");
			System.out.println("3.Exit!!");
			System.out.print("Enter an temperature : ");
			double temp =input.nextDouble();
			System.out.print("Choose one funtion : ");
			int chos = input.nextInt();
			switch(chos){
				case 1:
					System.out.println(toFahrenheit(temp));
					break;
				case 2:
					System.out.println(toCelsius(temp));
					break;
				case 3:
					System.exit(0);
					break;
				default:
					System.out.println("This number is invalid!");
					break;
			}
		}
		
	}
	public static double toFahrenheit(double temp){
		return temp*9/5+32;
	}
	public static double toCelsius(double temp){
		return (temp-32)*5/9;
	}
}
