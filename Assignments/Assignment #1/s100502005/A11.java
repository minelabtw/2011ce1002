package a1.s100502005;

import java.util.Scanner;

public class A11 
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter your name:");
		String name = input.next();
		System.out.print("Enter your student number:");		
		int number  = input.nextInt();
		boolean jump = true; 
		while(jump)
		{
			System.out.println("1.add");
			System.out.println("2.minus");
			System.out.println("3.multiple");
			System.out.println("4.divide");
			System.out.println("5.show user information");
			System.out.println("6.exit");
		
			int choice = input.nextInt();
		
			switch(choice)
			{
			 case 1:
				System.out.print("Enter the first number:");
				int a1 = input.nextInt();
				System.out.print("Enter the second number:");
				int b1 = input.nextInt();
				System.out.println( a1+b1 );
				break;
		
			 case 2:
				System.out.print("Enter the first number:");
				int a2 = input.nextInt();
				System.out.print("Enter the second number:");
				int b2 = input.nextInt();
				System.out.println( a2-b2 );
				break;
			
			 case 3:
				System.out.print("Enter the first number:");
				int a3 = input.nextInt();
				System.out.print("Enter the second number:");
				int b3 = input.nextInt();
				System.out.println( a3*b3 );
				break;
			
			 case 4:
				System.out.print("Enter the first number:");
				int a4 = input.nextInt();
				System.out.print("Enter the second number:");
				int b4 = input.nextInt();
				System.out.println( a4/b4 );
				break;
			
			 case 5:
				System.out.println(name);
				System.out.println(number);
				break;
				
			 case 6:
				System.out.print("Bye Bye ");
				System.out.print(name);
				System.out.print(" ~");
				jump = false;
				break;
			 default:
				break;
			}
		}
	}

}
