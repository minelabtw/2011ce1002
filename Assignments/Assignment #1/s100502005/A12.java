package a1.s100502005;

import java.util.Scanner;

public class A12
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		boolean jump = true;
		
		while(jump)
		{
			System.out.println("Chose a method:");
			System.out.println("1.changes user input from Celsius to Fahrenheit");
			System.out.println("2.changes user input from Fahrenheit to Celsius");
			System.out.println("3.exit");
		
			int choice = input.nextInt();
		
			switch(choice)
			{
			 case 1:
				 System.out.print("Enter a number:");
				 double Ce = input.nextDouble();
				 toFahrenheit(Ce);
				 System.out.println("The answer that changes user input from Celsius to Fahrenheit is " + toFahrenheit(Ce));
				 break;
			 case 2:
				 System.out.print("Enter a number:");
				 double Fa = input.nextDouble();
				 toCelsius(Fa);
				 System.out.println("The answer that changes user input from Celsius to Fahrenheit is " + toCelsius(Fa));
				 break;
			 case 3:
				 System.out.print("Bye Bye~");
				 jump = false;
				 break;
			 default:
					break;
			
		
			}
		}
	}
	
	public static double toFahrenheit(double Ce)
	{
		double answer;
		answer = Ce*9/5+32;
		return answer;	
	}
	
	public static double toCelsius(double Fa)
	{
		double answer;
		answer =  (Fa-32)*5/9;
		return answer;
	}
}
