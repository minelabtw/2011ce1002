import java.util.Scanner;

public class A12 {
	public static void main(String args[])
	{
		Scanner input = new Scanner(System.in);
		double temperature = 0;
		System.out.println("請輸入溫度:");
		temperature = input.nextDouble();
		System.out.printf("轉換成攝氏:%f\n", toCelisus(temperature));
		System.out.printf("轉換成華氏:%f\n", toFahrenheit(temperature));
	}
	
	public static double toFahrenheit(double temperature1)
	{
		temperature1 = temperature1*9/5+32;
		return temperature1;
	}
	
	public static double toCelisus(double temperature2)
	{
		temperature2 = (temperature2-32)*5/9;
		return temperature2;
	}
}

