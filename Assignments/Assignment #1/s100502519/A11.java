package a1.s100502519;

import java.util.Scanner;
public class A11 {
	public static void main (String[]args){
		Scanner input = new Scanner (System.in);
		
		String name;			//宣告變數
		int id;
		int choice1;
		int choice2;
		double num = 0;
		double sum = 0;
		int exit1 = 0;
		int exit2 = 0;
		
		
		System.out.print("What's your name ? ");
		name = input.nextLine();			//input使用者姓名
		System.out.print("What's your student ID ? ");
		id = input.nextInt();			//input使用者學號
			
		while(exit1==0){			//while loop 使可以重複使用
			
			System.out.println("\n---------Main Menu---------");
			System.out.println("What do you want to do ? " +
								"\n1.use Calculator " +
								"\n2.show user imformation \n3.exit");			//1.用計算機  2.顯示資料  3.結束
			System.out.println("---------------------------");
			choice1 = input.nextInt();			//input選擇
		
			switch(choice1){			//switch使用者的選擇 
				
				case 1:			//計算
					System.out.print("Enter number : ");
					sum = input.nextDouble();			//設總和為第一個數
					
					while(exit2==0){			//while loop 使能夠不停計算
						
						System.out.println("\n1.add " +
											"\n2.minus " +
											"\n3.mutiple " +
											"\n4.divide " +
											"\n5.show current answer " +
											"\n6.reset clear " +
											"\n7.back to main menu");			//1.+ 2.- 3.* 4./ 5.目前答案  6.歸零  7.回到主選單
						choice2 = input.nextInt();			//input 使用者的選擇
						
						switch(choice2){			//switch 選擇
							
							case 1:
								System.out.print("Enter number : ");			//要求要加的數
								num = input.nextDouble();
								sum = sum+num;			//總和加輸入的數
								break;
							case 2:
								System.out.print("Enter number : ");			//要求要減的數
								num = input.nextDouble();
								sum = sum-num;			//總和減輸入的數
								break;
							case 3:
								System.out.print("Enter number : ");			//要求要乘的數
								num = input.nextDouble();
								sum = sum*num;			//總和乘輸入的數
								break;
							case 4:
								System.out.print("Enter number : ");			//要求要除的數
								num = input.nextDouble();
								sum = sum/num;			//總和除輸入的數
								break;
							case 5:
								System.out.println("The answer is : " + sum);			//顯示目前答案
								break;
							case 6:
								sum = 0;			//使總和歸零
								System.out.print("Enter number : ");			//要求第一個數
								num = input.nextDouble();
								sum = num;			//設第一個數為總和
								break;
							case 7:
								exit2 = 1;			//使能夠跳出迴圈
								break;
							default:
								System.out.println("error!! you should choose 1~7 !!");			//顯示錯誤訊息
								break;
						}
					}
					break;
					
				case 2:
					System.out.println("\n" + name + "  " + id + "\n");			//顯示名字和學號
					break;
					
				case 3:
					exit1 = 1;			//使能夠跳出迴圈
					break;
					
				default:
					System.out.println("error!! you should choose 1~3 !!");			//顯示錯誤訊息
					break;
			}
		}
				
		System.out.println("Bye Bye ~  " + name);			//結語
		
	}
}
