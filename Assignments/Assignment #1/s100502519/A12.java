package a1.s100502519;

import java.util.Scanner;
public class A12 {
	public static void main (String[]args){
		Scanner input = new Scanner (System.in);
		
		int choice;			//淵告變數
		double cs1;
		double fh1;
		int exit = 0;
		
		while(exit == 0){			//使可多次使用
			
			System.out.println("What do you want to ?");
			System.out.println("1. toFahrenheit " +
								"\n2. toCelsius \n3. exit");			//1.C轉F 2.F轉C 3.結束
			choice = input.nextInt();			//input使用者的選擇
			
			switch(choice){			//switch選擇
				
				case 1:
					System.out.println("Enter Celsius : ");			//要求輸入原始攝氏
					cs1 = input.nextDouble();
					double tF = toFahrenheit(cs1);			//define C轉F 的  method
					System.out.println("\n" + cs1 + " C" + " = " + tF + " F\n");			//顯示換算結果
					break;
				case 2:
					System.out.println("Enter Fahrenheit : ");			//要求輸入原始華氏
					fh1 = input.nextDouble();
					double tC = toCelsius(fh1);			//define F轉C 的  method
					System.out.println("\n" + fh1 + " F" + " = " + tC + " C\n");			//顯示換算結果
					break;
				case 3:
					System.out.println("Bye~~~~~");
					exit = 1;			//使跳出迴圈
					break;
				default:
					System.out.println("error!! you should choose 1~3 !!!\n");			//顯示錯誤訊息
					break;
			}
			
		}
	}
	
	
	
	
	
	public static double toFahrenheit(double cs2){			/*C轉F的method*/
		double fh = 9.0/5.0*cs2+32.0;			//公式
		return fh;			//return回去
	}
	
	public static double toCelsius(double fh2){			/*F轉C的method*/
		double cs = (fh2-32.0)*5.0/9.0;			//公式
		return cs;			//return回去
	}
}
