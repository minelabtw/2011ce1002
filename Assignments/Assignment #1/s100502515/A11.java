package a1.s100502515;
import java.util.*;

public class A11 
{
	public static void main( String[] args )
	{
		Scanner input = new Scanner( System.in );//import.
		
		System.out.print( "Enter your name: " );
		String userName = input.nextLine();//enter user name.
		
		System.out.print( "Enter your student ID: " );
		int studentID = input.nextInt();//Enter student ID.
		
		System.out.print( "Enter Y to use the calculator, and N to leave: " );
		char choice = (char)input.next().charAt(0);//Choose if you want to use the calculator.
		
		if ( choice == 'N' )
		{
			System.out.println( "Thank you " +  studentID + " " + userName + "!!" );
			return;//If you don't want to use it.
		}
		else if ( choice == 'Y' )//If you do.
		{
			for( ;; )//Calculator done here.
			{
				System.out.print( "Enter the initial number:" );
				double answer = input.nextDouble();
			
				for( ;; )
				{
					System.out.print( "Enter +, -, *, /, =, E �� exit: " );
					char operation = (char)input.next().charAt(0);
					
					switch( operation )
					{
						case '+':
							System.out.print( "Enter the secondary number:" );
							double numberAdd = input.nextDouble();
							answer += numberAdd;
							break;
						
						case '-':
							System.out.print( "Enter the secondary number:" );
							double numberMinus = input.nextDouble();
							answer -= numberMinus;
							break;
							
						case '*':
							System.out.print( "Enter the secondary number:" );
							double numberTimes = input.nextDouble();
							answer *= numberTimes;
							break;
							
						case '/':
							System.out.print( "Enter the secondary number:" );
							double numberDivide = input.nextDouble();
							answer /= numberDivide;
							break;
						
						case '=':
							System.out.println( "The answer is: " + answer );
							break;
						
						case 'E':
							System.out.println( "Thank you " +  studentID + " " + userName + "!!" );
							return;
					}
					
				}
				
			}
			
			
			
		}
			
			
		
	}
}
