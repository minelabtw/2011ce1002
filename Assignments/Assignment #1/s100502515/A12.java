package a1.s100502515;
import java.util.*; 

public class A12 
{
	static double toFahrenheit( double temperature )
	{
		return temperature * 9 / 5 + 32;
	}//Convert C to F
	
	static double toCelcius( double temperature )
	{
		return ( temperature - 32 ) * 5 / 9; 
	}//Convert F to C
	
	public static void main( String[] args )
	{
		Scanner input = new Scanner( System.in );
	
		System.out.print( "Enter the temperature: " );
		double temper = input.nextDouble();
	
		System.out.print( "Select F to convert C to F, C to convert F to C: " );
		char choice = (char)input.next().charAt(0);
	
		switch( choice )
		{
			case 'F':
				System.out.println( "It is: " + toFahrenheit( temper ) + " oF" );
				break;
			
			case 'C':
				System.out.println( "It is: " + toCelcius( temper ) + " oC" );
				break;
		}
	}
	
}
