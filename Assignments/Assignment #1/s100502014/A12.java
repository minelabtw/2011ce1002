package a1.s100502014;
import javax.swing.*;

public class A12 {
	public static void main(String[] args) {
		int flag = 0;
		double degree = 0;
		while(flag != -1) {
			//intput option
			String optionString =
					JOptionPane.showInputDialog(null,
					"1. Celsius to Fahrenheit\n" +
					"2. Fahrenheit to Celsius\n" +
					"3. Exit\n" +
					"Please enter your option :",
					"Option!!!",
					JOptionPane.QUESTION_MESSAGE);
			int option = Integer.parseInt(optionString);  //change string to int
			if(option == 1 || option == 2) {  //if not exit
				//input degree
				String degreeString =
					JOptionPane.showInputDialog(null,
						"Please enter the degree :",
						"Degree!!!",
						JOptionPane.QUESTION_MESSAGE);
				degree = Double.parseDouble(degreeString);
			}
			switch(option) {
				//toFahrenheit
				case 1:	String outputF = "Your anwser is " + toFahrenheit(degree) + " " + '\u00B0' + "F";
						JOptionPane.showMessageDialog(null,
							outputF,
							"Answer!!!",
							JOptionPane.INFORMATION_MESSAGE);
						break;
				//toCelsius
				case 2:	String outputC = "Your anwser is " + toCelsius(degree) + " " + '\u00B0' + "C";
						JOptionPane.showMessageDialog(null,
							outputC,
							"Answer!!!",
							JOptionPane.INFORMATION_MESSAGE);
						break;
				//exit
				case 3: flag = -1;
						break;
				default:System.out.println("Errors: invalid status");
						System.exit(0);
			}
		}
	}
	public static double toFahrenheit(double degree) {
		return degree*9/5+32;
	}
	public static double toCelsius(double degree) {
		return (degree-32)*5/9;
	}
}
