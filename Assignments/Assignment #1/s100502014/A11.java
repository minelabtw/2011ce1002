package a1.s100502014;
import javax.swing.*;

public class A11 {
	public static void main(String[] args) {
		//input name
		String name =
			JOptionPane.showInputDialog(null,
			"Please enter your name :",
			"Name!!!",
			JOptionPane.QUESTION_MESSAGE);
		//input student number
		String studentNumber =
			JOptionPane.showInputDialog(null,
			"Please enter your student number :",
			"Student Number!!!",
			JOptionPane.QUESTION_MESSAGE);
		int option = 6, flag = 0;
		double number = 0, temp = 0;
		while(flag != -1) {
			if(flag == 0) {    //input the first number
				String numberString =
					JOptionPane.showInputDialog(null,
					"Please enter one number :",
					"Number!!!",
					JOptionPane.QUESTION_MESSAGE);
				number = Double.parseDouble(numberString);  //change string to double
				flag = 1;  //first number has inputed
			}
			//input option
			String optionString =
				JOptionPane.showInputDialog(null,
				"1. Add\n" +
				"2. Minus\n" +
				"3. Multiple\n" +
				"4. Divide\n" +
				"5. Show information\n" +
				"6. Show the answer\n" +
				"7. Clear\n" +
				"8. Exit\n" +
				"Please enter your option :",
				"Option!!!",
				JOptionPane.QUESTION_MESSAGE);
			option = Integer.parseInt(optionString);  //change string to integer
			switch(option) {
				//add
				case 1:	String addString =
							JOptionPane.showInputDialog(null,
							"Please enter one number :",
							"Add!!!",
							JOptionPane.QUESTION_MESSAGE);
						temp = Double.parseDouble(addString);
						number += temp;
						break;
				//minus
				case 2:	String minusString =
							JOptionPane.showInputDialog(null,
							"Please enter one number :",
							"Minus!!!",
							JOptionPane.QUESTION_MESSAGE);
						temp = Double.parseDouble(minusString);
						number -= temp;
						break;
				//multiple
				case 3:	String multipleString =
							JOptionPane.showInputDialog(null,
							"Please enter one number :",
							"Multiple!!!",
							JOptionPane.QUESTION_MESSAGE);
						temp = Double.parseDouble(multipleString);
						number *= temp;
						break;
				//divide
				case 4:	String divideString =
							JOptionPane.showInputDialog(null,
							"Please enter one number :",
							"Divide!!!",
							JOptionPane.QUESTION_MESSAGE);
						temp = Double.parseDouble(divideString);
						number /= temp;
						break;
				//show information
				case 5: String output = "Your name is " + name + " .\n" +
										"Your student number is " + studentNumber + " .";
						JOptionPane.showMessageDialog(null,
							output,
							"Show information!!!",
							JOptionPane.INFORMATION_MESSAGE);
						break;
				//show the answer
				case 6: String output2 = "Your answer is " + number;
						JOptionPane.showMessageDialog(null,
							output2,
							"Show the anwer!!!",
							JOptionPane.INFORMATION_MESSAGE);
						break;
				//clear
				case 7: flag = 0;
						break;
				//exit
				case 8: flag = -1;
						break;
				default:System.out.println("Errors: invalid status");
						System.exit(0);
			}
		}
		String output3 = "Thank you for using!!\n" +
						 "Goodbye " + name + " (" + studentNumber + ") ~";
		//the ending message
		JOptionPane.showMessageDialog(null,
			output3,
			"Goodbye!!!",
			JOptionPane.INFORMATION_MESSAGE);
	}
}
