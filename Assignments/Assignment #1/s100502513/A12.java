package a1.s100502513;

import java.util.Scanner;
public class A12 {
   public static void main(String[] args){
	  
	  Scanner input = new Scanner(System.in);
	  System.out.println("請選擇溫度轉換方式\n1.攝氏轉為華氏\n2.華氏轉為攝氏\n3.離開程式 ");  //設置清單供使用者選擇
	  for(;;){   //用無線迴圈重複輸入功能
		 System.out.println("\n請輸入數字選擇: ");  
		 int choice = input.nextInt();
		 switch(choice){
		    case 1:
		    	System.out.println("請輸入攝氏溫度: ");
		    	double cdegree = input.nextDouble();  //輸入攝氏溫度
		    	System.out.println("轉換成華氏溫度為: "+ toFahrenheit(cdegree));   //換算成華氏溫度
		    	break;
		    case 2:
		    	System.out.println("請輸入華氏溫度: ");
		    	double fdegree = input.nextDouble();   //輸入華氏溫度
		        System.out.println("轉換成攝氏溫度為: "+ toCelsius(fdegree));    //換算成攝氏溫度
		        break;
		    case 3:
		    	System.out.println("離開 掰掰");
		    	System.exit(0);   //離開程式
		    default:  //輸入其他無效數字，重新輸入
		    	System.out.println("無此選擇，請重新輸入");
		    	break;
		 }
	  }
   }
   
   public static double toFahrenheit(double celsius){
	  double fahrenheit = celsius*9/5+32;   //公式
      return fahrenheit;    //把計算過後的值回傳
   }
   
   public static double  toCelsius(double fahrenheit){
		  double celsius = (fahrenheit-32)*5/9;   //公式
	      return celsius;  //把計算過後的值回傳
	   }
}
