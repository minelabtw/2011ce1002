package a1.s100502513;

import java.util.Scanner;

public class A11 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("請輸入姓名: ");   
		String name = input.next();   //輸入姓名存到name
		System.out.println("請輸入學號: ");
		int snumber = input.nextInt();   //輸入學號存到snumber	

		System.out.println("請輸入兩個數字: ");
		double numb1 = input.nextDouble();   //輸入數字兩個數字做計算
		double numb2 = input.nextDouble();

		System.out.println("使用下列功能\n0.離開\n1.加法\n2.減法\n3.乘法\n4.除法\n5.顯示使用者資料");
		for (;;) {   //用無線迴圈來重複輸入功能
			System.out.println("請選擇你要的功能: ");
			int choice = input.nextInt();  //輸入數字選擇功能
			switch (choice) {
				case 0:
					System.out.println("掰掰 " + name + " 歡迎下次光臨!!");
					System.exit(0);  //離開程式
				case 1:
					System.out.println(numb1 + " + " + numb2 + " = "+ (numb1 + numb2));  //加法
					break;  
				case 2:
					System.out.println(numb1 + " - " + numb2 + " = "+ (numb1 - numb2));  //減法
					break;   
				case 3:
					System.out.println(numb1 + " * " + numb2 + " = "+ (numb1 * numb2));  //乘法
					break;   
				case 4:
					System.out.println(numb1 + " / " + numb2 + " = "+ (numb1 / numb2));  //除法
					break;
				case 5:
					System.out.println("姓名為: " + name + "\n學號為: " + snumber);   //顯示輸入的姓名及學號
					break;
				default:  
					System.out.println("無此功能，請重新輸入");  //輸入其他無效數字，重新輸入
					break;
			}
		}
	}
}