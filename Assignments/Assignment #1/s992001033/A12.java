package a1.s992001033;
import java.util.Scanner;

public class A12
{
	public static void main(String args[])
	{
		Scanner input = new Scanner(System.in);
		System.out.println("1.Celsius to Fahrenheit");
		System.out.println("2.Fahrenheit to Celsius");
		System.out.println("3.exit");
		int switchnumber=0;
		while(switchnumber!=3)//不是3就繼續
		{
			System.out.print("Enter your switch: ");
			switchnumber = input.nextInt();
			double number = 0;
			if(switchnumber>0 && switchnumber<3)//是1或2就需要輸入數字
			{
				System.out.print("Enter the number: ");
				number = input.nextInt();
			}
			switch(switchnumber)//選擇使用哪一個method
			{
				case 1:
						System.out.println("The Fahrenheit is"+toFahrenheit(number));
						break;
				case 2:
						System.out.println("The Celisus is"+toCelisus(number));
						break;
				case 3:
						break;
				default://需要輸入正確的選擇
						System.out.println("Enter the rightswitch.");
						break;
			}
			System.out.println();
			if(switchnumber==3)//3就離開
				break;
		}
	}
	public static double toFahrenheit(double number)//攝氏轉華氏的method
	{
		return (number*9)/5+32;
	}
	public static double toCelisus(double number)//華氏轉攝氏的method
	{
		return (number-32)*5/9;
	}
}