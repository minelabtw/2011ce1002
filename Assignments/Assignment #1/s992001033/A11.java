package a1.s992001033;
import java.util.Scanner;

public class A11
{
	public static void main(String args[])
		{
			Scanner input = new Scanner(System.in);//用來檢查輸入
			System.out.print("Enter your user name: ");
			String username = input.nextLine();
			System.out.print("Enter your student number: ");
			String studentnumber = input.nextLine();
			System.out.println("You can use below functions.");
			int switchnumber=0;
			double number1=0;
			double number2=0;
			while(switchnumber!=6)//不是離開就重複輸入
			{
				System.out.println("1.add");
				System.out.println("2.minus");
				System.out.println("3.multiple");
				System.out.println("4.devide");
				System.out.println("5.show user information");
				System.out.println("6.exit");
				System.out.print("Enter your switch: ");//選擇哪一個功能
				switchnumber = input.nextInt();
				if(switchnumber>0 && switchnumber<5)//1到4需要輸入數字
				{
					System.out.print("Enter number1: ");//第一個數字
					number1 = input.nextInt();
					System.out.print("Enter number2: ");//第二個數字
					number2 = input.nextInt();
				}
				switch(switchnumber)
				{

					case 1:
							System.out.println("The result is "+(number1+number2));
							break;
					case 2:
							System.out.println("The result is "+(number1-number2));
							break;
					case 3:
							System.out.println("The result is "+(number1*number2));
							break;
					case 4:
							while(number2==0)//除數不能為0
							{	
								if(number2==0)
								System.out.println("number2 can't be 0");
								System.out.print("Enter number2: ");
								number2 = input.nextInt();
							}
							System.out.println("The result is "+(number1/number2));
							break;
					case 5:
							System.out.println("User name is "+username);
							System.out.println("Student number is "+studentnumber);
							break;
					case 6:
							break;
					default:
							System.out.println("Enter the rightswitch.");
							break;
				}
				System.out.println();
				if(switchnumber==6)
					break;
			}
			System.out.println("Thank you for using!");
		}
}
