package a1s100502507;

import java.util.Scanner;

public class A12 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		System.out.print("Enter 1 for toFahrenheit method\n" + "2 for toCelsius method\n" + "0 to exit: ");
		int choice = input.nextInt();
		double temp;
		while(choice != 0){
			switch(choice){
				case 1:
					System.out.print("Enter the temperature of Celsius: ");
					temp = input.nextDouble();
					System.out.println("The result is: " + toFahrenheit(temp) );
					break;
				case 2:
					System.out.print("Enter the temperature of Fahrenheit: ");
					temp = input.nextDouble();
					System.out.println("The result is: " + toCelsius(temp) );
					break;
				case 0:
					break;
				case ' ':
				case '\n':
				case '\t':
					break;
				default:
					System.out.println("Incorrect input!");
					break;
			}
			System.out.print("Enter 1 for toFahrenheit method\n" + "2 for toCelsius method\n" + "0 to exit: ");
			choice = input.nextInt();
		}
	}
	
	public static double toFahrenheit(double c){
		double result;
		result = c*9/5+32;
		return result;
	}
	
	public static double toCelsius(double f){
		double result;
		result = (f-32)*5/9;
		return result;
	}
}