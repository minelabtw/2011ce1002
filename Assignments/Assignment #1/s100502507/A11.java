package a1s100502507;

import java.util.Scanner;

public class A11 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		System.out.print("Enter your name: ");
		String name = input.nextLine();
		System.out.print("Enter your ID number: ");
		String number = input.nextLine();
		System.out.print("1 for add\n" + "2 for minus\n" + "3 for multiply\n" + "4 for divide\n" + "5 for show information\n" + "0 for exit: ");
		int choice = input.nextInt();
		double num01;
		double num02;
		while(choice != 0){
			System.out.print("Enter first number: ");
			num01 = input.nextDouble();
			System.out.print("Enter second number: ");
			num02 = input.nextDouble();
			switch(choice){
				case 1:
					System.out.println("The result is: " + (num01+num02));
					break;
				case 2:
					System.out.println("The result is: " + (num01-num02));
					break;
				case 3:
					System.out.println("The result is: " + (num01*num02));
					break;
				case 4:
					System.out.println("The result is: " + (num01/num02));
					break;
                                case 5:
					System.out.println("Your name is " + name);
					System.out.println("Your ID is " + number);
					break;
				case 0:
					break;
				case 
				case ' ':
				case '\n':
				case '\t':
					break;
				default:
					System.out.println("Incorrect input!");
					break;
			}
			System.out.print("1 for add\n" + "2 for minus\n" + "3 for multiply\n" + "4 for divide\n" + "0 for exit: ");
			choice = input.nextInt();
		}
		
		System.out.println("Thank you " + name);
	}
}
