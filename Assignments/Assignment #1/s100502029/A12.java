package a1.s100502029;
import java.util.Scanner;

public class A12 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // Create a Scanner
		
		while (true) {
			// show functionality list
			System.out.println("1.toFahrenheit: changes user input from Celsius to Fahrenheit.\n" +
					"2.toCelisus: changes user input from Fahrenheit to Celsius.");
			// read user's choice
			System.out.print("Please choose a function: ");
			int choice = input.nextInt();
			
			switch (choice) {
				case 1: // changes user input from Celsius to Fahrenheit
					System.out.print("Enter celsius temperature: ");
					double celsius = input.nextDouble();
					System.out.println("Fahrenheit temperature: " + toFahrenheit(celsius) + "\n");
					break;
				case 2: // changes user input from Fahrenheit to Celsius
					System.out.print("Enter fahrenheit temperature: ");
					double fahrenheit = input.nextDouble();
					System.out.println("Celsius temperature: " + toCelsius(fahrenheit) + "\n");
					break;
				default: // show error message
					System.out.println("Errors: invalid input");
					break;
			}
		}
	}
	
	 // changes temperature from Celsius to Fahrenheit
	public static double toFahrenheit(double cel) {
		double fah;
		fah = cel * 9 / 5 + 32;
		return fah;
	}
	
	 // changes temperature from Fahrenheit to Celsius
	public static double toCelsius(double fahren) {
		double celsi;
		celsi = (fahren - 32 ) * 5 / 9;
		return celsi;
	}
}
