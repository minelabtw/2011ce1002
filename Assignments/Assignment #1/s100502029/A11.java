package a1.s100502029;
import javax.swing.JOptionPane;


public class A11 {
	public static void main(String[] args) {
		// display welcome message
		JOptionPane.showMessageDialog(null, "Welcome to use this calculator!", "Calculator", JOptionPane.INFORMATION_MESSAGE);
		// prompt user's name
		String userName = JOptionPane.showInputDialog(null, "Please enter your name:", "Calculator", JOptionPane.QUESTION_MESSAGE);
		// prompt user's student ID
		String userNumber = JOptionPane.showInputDialog(null, "Please enter your student ID:", "Calculator", JOptionPane.QUESTION_MESSAGE);
		
		// functionality list
		String list =
				"1.add\n" +
				"2.minus\n" +
				"3.multiple\n" +
				"4.divide\n" +
				"5.show information\n" +
				"6.set result to 0\n" +
				"0.exit";
		
		// start calculator
		while (true) {
			double result = 0; // initialize the result to zero
			String value_1st = JOptionPane.showInputDialog("Enter a value:"); // read and store the number
			double number_1st = Double.parseDouble(value_1st); // change number from string type to double type
			result = number_1st;
			
			boolean flag = true; // if flag become false, that indicates first while loop will run again and it is means result will be set to zero
			while (flag) {
				String choose = JOptionPane.showInputDialog("result: " + result + "\n\n" + list); // show current result and functionality list, then read user's choice
				int choice = Integer.parseInt(choose); // change user's choice from string type to integer type
				
				switch (choice) { // determine user choose what function
					case 1: // function 1~4 is calculate function
					case 2:
					case 3:
					case 4:
						String value = JOptionPane.showInputDialog("Enter a value:");
						double number = Double.parseDouble(value);
						switch (choice) { // determine add or minus or multiple or divide
							case 1:
								result += number;
								break;
							case 2:
								result -= number;
								break;
							case 3:
								result *= number;
								break;
							case 4:
								// determine validity of user's input
								if (number != 0)
									result /= number;
								else {
									JOptionPane.showMessageDialog(null, "Errors: Zero cannot be a dividend"); // show error message
									System.exit(0); // exit the program
								}
								break;
							default:
								break;
						}
						break;
					case 5: // display user's information
						JOptionPane.showMessageDialog(null,  "Student name: " + userName + "\nStudent ID: " + userNumber, "User Information", JOptionPane.INFORMATION_MESSAGE);
						break;
					case 6: // let result initialize to 0
						flag = false;
						break;
					case 0: // show thanks message and then exit program
						JOptionPane.showMessageDialog(null, userName + ", thanks for your use.");
						System.exit(0);
					default: // show error message and then exit program
						JOptionPane.showMessageDialog(null, "Errors: invalid input");
						System.exit(0);
				}
			}
		}
	}
}
