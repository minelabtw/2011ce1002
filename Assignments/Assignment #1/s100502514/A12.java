/**
 * CE1002-100502514 FlashTeens Chiang
 * [The Following Rules are from ce1002 website]
 * Assignment 1-2

	Please write a program (by Java and Eclipse) and meet the following requirements:

	1.Please implement two method: toFahrenheit and toCelsius, and use each method in your program once.
	
	* toFahrenheit: changes user input from Celsius to Fahrenheit.
	* toCelisus: changes user input from Fahrenheit to Celsius.
 */
package a1.s100502514;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

public class A12 extends MyBaseWindow{
	
	/** Constructor to Initialize Window */
	public A12(){
		/**Initializing Window (Default, not to be shown)*/
		initialize();
	}
	
	/** Function toCelsius */
	public double toCelsius(double f){
		//Return the answer to 3 digits after decimal point
		return (double)(Math.round(((f-32)*5/9)*1000))/1000;
	}
	
	/** Function toFahrenheit */
	public double toFahrenheit(double c){
		//Return the answer to 3 digits after decimal point
		return (double)(Math.round((c*9/5+32)*1000))/1000;
	}
	
	/** Main Function will call the Constructor */
	public static void main(String args[]){
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				A12 myWindow = new A12();
				boolean keep_running = true;
				while(keep_running){
					String button_labels[] = {"toFahrenheit()","toCelsius()"};
					String step2_messages[] = {"Celsius","Fahrenheit"};
					try{
						int choice = JOptionPane.showOptionDialog(myWindow,
										"Please choose which function to use:",
										"Assignment 1-2 by 100502514",
										JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null,
										button_labels, null);
						double num = Double.parseDouble(
								JOptionPane.showInputDialog("Please input the "
										+step2_messages[choice]+" degree number:"));
						String message="";
						if(choice==0){
							//toFahrenheit() is Chosen
							/** Note: character '\u02da' is for "degree sign". */
							message=num+"\u02daC = "+myWindow.toFahrenheit(num)+"\u02daF";
						}else if(choice==1){
							//toCelsius() is Chosen
							message=num+"\u02daF = "+myWindow.toCelsius(num)+"\u02daC";
						}
						JOptionPane.showMessageDialog(myWindow, message);
					}catch(NumberFormatException err){
						//Run here if user inputs a not-a-number value.
						JOptionPane.showMessageDialog(myWindow, "Invalid Value!!");
					}catch(Exception err){
						//Run here if user pressed "Cancel".
						keep_running = false;
					}
				}
				//Exit Program
				System.exit(0);
			}
		});
	}
	
}
