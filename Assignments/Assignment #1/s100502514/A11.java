/**
 * CE1002-100502514 FlashTeens Chiang
 * [The Following Rules are from ce1002 website]
 * Assignment 1-1

	Please write a program (by Java and Eclipse) and meet the following requirements:
	
	1.Let user inputs his user name and student number.
	2.Implement a easy ˇ§Calculatorˇ¨!!
	
	* Calculator includes five functionality: add, minus, multiple, divide and show user information.
	* Please make a list for user choice, and infinite user input.
	* Use switch statement to determine the choice.
	* If user chooses to exit, leave a message to the user
 */
package a1.s100502514;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class A11 extends MyBaseWindow{
	
	/** Declares Frames in the Window */
	public String UserName;
	public String StudentNumber;
	public JPanel MessageGroup;
	public JPanel ButtonGroup;
	public MyBigTextField InputA = new MyBigTextField("Number A");
	public MyBigTextField InputB = new MyBigTextField("Number B");
	public MyBigTextField answer_txt = new MyBigTextField("<answer>");
	public MyBigLabel MyOperator = new MyBigLabel("?");
	
	/** Constructor to Initialize Window */
	public A11(){
		/**Initializing Window*/
		initialize("My Java Calculator by 100502514 on Feb 24 2012", 700, 150);
		
		/**Set Main Layout*/
		//Upper frame is for Messages
		//Central frame is for Inputs
		//Lower frame is for Buttons
		GridLayout myLayout = new GridLayout(3,1);
		myLayout.setVgap(3);
		setLayout(myLayout);
		
		/**Add Messages*/
		add(new MyBigLabel("My Java Calculator by 100502514"));
		
		/**Add Inputs*/
		MessageGroup = new JPanel();
		MessageGroup.setLayout(new GridLayout(1,5));
		//Add 1st Input Text
		InputA.addKeyListener(new KeyListener(){
			public void keyPressed(KeyEvent keyEvent){
				if(keyEvent.getKeyCode()==KeyEvent.VK_ENTER){
					//Focus to the next input
					InputB.requestFocusInWindow();
				}else{
					//Reset the answer when text changes
					answer_txt.setText("<answer>");
				}
			}
			public void keyReleased(KeyEvent keyEvent){
				//nothing, but required for the compiler
			}
			public void keyTyped(KeyEvent keyEvent){
				//nothing, but required for the compiler
			}
		});
		MessageGroup.add(InputA);
		//Add operator label
		MessageGroup.add(MyOperator);
		//Add 2nd Input Text
		InputB.addKeyListener(new KeyListener(){
			public void keyPressed(KeyEvent keyEvent){
				if(keyEvent.getKeyCode()==KeyEvent.VK_ENTER){
					//Calculate the answer
					calculate(InputA.getText(), MyOperator.getText(), InputB.getText());
				}else{
					//Reset the answer when text changes
					answer_txt.setText("<answer>");
				}
			}
			public void keyReleased(KeyEvent keyEvent){
				//nothing, but required for the compiler
			}
			public void keyTyped(KeyEvent keyEvent){
				//nothing, but required for the compiler
			}
		});
		MessageGroup.add(InputB);
		MessageGroup.add(new MyBigLabel("="));
		//Add Result text message
		answer_txt.setEditable(false);
		answer_txt.setForeground(new Color(255,102,0));
		answer_txt.setHorizontalAlignment(JLabel.CENTER);
		MessageGroup.add(answer_txt);
		//Add the Central Panel at last
		add(MessageGroup);
		
		/**Add Buttons*/
		ButtonGroup = new JPanel();
		String ButtonLabels[]={"+","-","*","/","Mod","Enter","Info"};
		ButtonGroup.setLayout(new GridLayout(1,ButtonLabels.length));
		for(int i=0;i<ButtonLabels.length;i++){
			//Defining with keyword "final" allows the call of onClick(myBtn.getLabel())
			final JButton myBtn = new JButton(ButtonLabels[i]);
			myBtn.setFont(new Font("Arial Black",0,20));
			
			//Change the Enter Button to Yellow and the Info Button to Green
			//And Set Button Listeners
			if(ButtonLabels[i]=="Enter"){
				myBtn.setBackground(new Color(255,255,0));
			}else if(ButtonLabels[i]=="Info"){
				myBtn.setBackground(new Color(153,204,102));
			}
			myBtn.addMouseListener(new MouseListener(){
				//The following line is generated in Eclipse while debugging
				@SuppressWarnings("deprecation")
				public void mousePressed(MouseEvent e){
					onClick(myBtn.getLabel());
				}
				public void mouseReleased(MouseEvent e){
					//nothing, but required for the compiler
				}
				public void mouseClicked(MouseEvent e){
					//nothing, but required for the compiler
				}
				public void mouseEntered(MouseEvent e){
					//nothing, but required for the compiler
				}
				public void mouseExited(MouseEvent e){
					//nothing, but required for the compiler
				}
			});
			
			ButtonGroup.add(myBtn);
		}
		//Add the Lower Panel at last
		add(ButtonGroup);
		
		addWindowListener(new WindowListener(){
			public void windowActivated(WindowEvent arg0) {
				//nothing
			}
			public void windowClosed(WindowEvent arg0) {
				//nothing
			}
			public void windowClosing(WindowEvent arg0) {
				goodbye();
			}
			public void windowDeactivated(WindowEvent arg0) {
				//nothing
			}
			public void windowDeiconified(WindowEvent arg0) {
				//nothing
			}
			public void windowIconified(WindowEvent arg0) {
				//nothing
			}
			public void windowOpened(WindowEvent arg0) {
				//nothing
			}
		});
		
	}
	
	/** Define Button Events */
	public void onClick(String sign){
		switch(sign){
		case "+":
		case "-":
		case "*":
		case "/":
		case "Mod":
			MyOperator.setText(sign);
			break;
		case "Enter":
			calculate(InputA.getText(), MyOperator.getText(), InputB.getText());
			break;
		case "Info":
			//TO BE CONTINUED
			JOptionPane.showMessageDialog(this, "My Java Calculator\nDesigned by FlashTeens 2012.2/24\n" +
					"Current User: "+UserName+"\nUser Student Number: "+StudentNumber);
			break;
		}
	}
	
	/** Define the Method to Calculate */
	public void calculate(String num1, String oper, String num2){
		double x=0,y=0;
		try{
			x=Double.parseDouble(num1);
			y=Double.parseDouble(num2);
		}catch(Exception ex){
			//When either num1 or num2 is invalid, an error alert will be shown.
			JOptionPane.showMessageDialog(this, "Invalid Values!!");
			answer_txt.setText("ERROR");
			return;
		}
		double ans=0;
		/** ASSIGNMENT REQUIREMENT: Switch Statement */
		//Condition for the operator sign
		switch(oper){
		case "+":
			ans=x+y;
			break;
		case "-":
			ans=x-y;
			break;
		case "*":
			ans=x*y;
			break;
		case "/":
			if(y==0){
				//Error Message for x/0
				JOptionPane.showMessageDialog(this, "Cannot Divide by Zero!!");
				answer_txt.setText("ERROR");
				return;
			}
			ans=x/y;
			break;
		case "Mod":
			if(y==0){
				//Error Message for x mod 0
				JOptionPane.showMessageDialog(this, "Cannot Divide by Zero!!");
				answer_txt.setText("ERROR");
				return;
			}
			ans=x%y;
			break;
		default:
			JOptionPane.showMessageDialog(this, "The Operator is NOT SET YET!!");
			answer_txt.setText("ERROR");
			return;
		}
		//Print the answer to 3 digits after decimal point
		answer_txt.setText(""+(double)(Math.round(ans*1000))/1000);
	}
	
	/** Showing goodbye message */
	public void goodbye(){
		JOptionPane.showMessageDialog(this,"Goodbye "+UserName+" !! :)");
	}
	
	/** Main Function will call the Constructor */
	public static void main(String args[]){
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				A11 myWindow = new A11();
				try{
					//Program will require user's name and student number
					myWindow.UserName = JOptionPane.showInputDialog("Please input your name:");
					if(myWindow.UserName.isEmpty())myWindow.UserName="Guest";
					myWindow.StudentNumber = JOptionPane.showInputDialog("Please input your student number:");
					if(myWindow.StudentNumber.isEmpty())myWindow.StudentNumber="not-set-yet";
					//Show the calculator window
					myWindow.setVisible(true);
				}catch(Exception cancel_error){
					//Run this scope if user pressed "Cancel" button
					JOptionPane.showMessageDialog(myWindow, "Goodbye Anonymous !! :(");
					System.exit(0);
				}
			}
		});
	}
	
}
