package a1.s100502517;

import java.util.Scanner;

public class A12 {	
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		
		System.out.println("Welcome ~!!");
		
		int run = 0;
		while(run==0){
		
			System.out.println("1. F to C  2. C to F  3. exit");
			System.out.print("Choice : ");
			int choose = input.nextInt();
		
			double temperature;
		
			switch(choose){
				case 1:
					System.out.print("Enter the temperature: ");
					temperature = input.nextDouble();
					if(temperature <= -459.4)// absolute zero
						System.out.println("Impossible~!!!");
					else
						System.out.println("You will get "+ toCelsius(temperature));
					break;
				
				case 2:
					System.out.print("Enter the temperature: ");
					temperature = input.nextDouble();
					if(temperature <= -273)// absolute zero
						System.out.println("Impossible~!!!");
					else
						System.out.println("You will get "+ toFahrenheit(temperature));
					break;
			
				case 3:
					run = 1;
					break;
				
				
				default:
					System.out.println("Wrong ~!!!");
					break;
			}
		}
		System.out.println("Bye~Bye~Bye~Bye~Bye~!!!");
		
		
		
	}
	
	public static double toFahrenheit (double Celsius){ //C to F
		double sum;
		sum = Celsius * 9 / 5 + 32 ;
		return sum;
	}

	public static double toCelsius( double Fahrenheit){// F to C
		double sum;
		sum = (Fahrenheit-32) * 5 / 9;
		return sum;
	}
}
