package a1.s100502510;

import java.util.Scanner;

public class A11 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		System.out.println("Please input your student number and name:");
		double studentnumber = input.nextDouble();
		String name = input.next();
		while (1 == 1) {//無限迴圈
			System.out.println("Please choose your choice:(1.add 2.minus 3.multiple 4.divide 5.show information):");
			int choice = input.nextInt();

			switch (choice) {
			case 1:
				System.out.println("Please input the first number:");
				double numberone = input.nextDouble();
				System.out.println("Please input the second number:");
				double numbertwo = input.nextDouble();
				double answer = numberone + numbertwo;
				System.out.printf("The answer is: %.3f" + "\n", answer);//使答案不要顯示太多小數點,且不會跟後面那行連在一起
				System.out.println("Would you like to continue using the calculator?If don't,please press 0.If you want to continue using,press 1:");
				int choicetwo = input.nextInt();
				if (choicetwo == 0) {
					System.out.println("Thanks for using!!");//結語
					System.exit(0);
				}
				break;//結束程式
			case 2:
				System.out.println("Please input the first number:");
				double numberthree = input.nextDouble();
				System.out.println("Please input the second number:");
				double numberfour = input.nextDouble();
				double answertwo = numberthree - numberfour;
				System.out.printf("The answer is: %.3f" + "\n", answertwo);
				System.out.println("Would you like to continue using the calculator?If don't,please press 0.If you want to continue using,press 1:");
				int choicethree = input.nextInt();
				if (choicethree == 0) {
					System.out.println("Thanks for using!!");
					System.exit(0);
				}
				break;
			case 3:
				System.out.println("Please input the first number:");
				double numberfive = input.nextDouble();
				System.out.println("Please input the second number:");
				double numbersix = input.nextDouble();
				double answerthree = numberfive * numbersix;
				System.out.printf("The answer is: %.3f" + "\n", answerthree);
				System.out.println("Would you like to continue using the calculator?If don't,please press 0.If you want to continue using,press 1:");
				int choicefour = input.nextInt();
				if (choicefour == 0) {
					System.out.println("Thanks for using!!");
					System.exit(0);
				}
				break;
			case 4:
				System.out.println("Please input the first number:");
				double numberseven = input.nextDouble();
				System.out.println("Please input the second number:");
				double numbereight = input.nextDouble();
				double answerfour = numberseven / numbereight;
				System.out.printf("The answer is: %.3f" + "\n", answerfour);
				System.out.println("Would you like to continue using the calculator?If don't,please press 0.If you want to continue using,press 1:");
				int choicefive = input.nextInt();
				if (choicefive == 0) {
					System.out.println("Thanks for using!!");
					System.exit(0);
				}
				break;
			case 5:
				System.out.printf("Your name is: %s" + "\n", name);
				System.out.printf("Your studnet number is: %.0f" + "\n",studentnumber);
				System.out.println("Would you like to continue using the calculator?If don't,please press 0.If you want to continue using,press 1:");
				int choicesix = input.nextInt();
				if (choicesix == 0) {
					System.out.println("Thanks for using!!");
					System.exit(0);
				}
				break;

			}
		}

	}
}
