package a1.s100502510;

import java.util.Scanner;

public class A12 {
	public static double toFahrenheit(double number) {
		double answer;
		answer = ((9.0 / 5.0) * number) + 32;//+.0使計算結果正確
		return answer;
	}

	public static double toCelsius(double number) {
		double answer;
		answer = (number - 32) * 5.0 / 9.0;
		return answer;
	}

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Please choose your choice(1. from Celsius to Fahrenheit 2. from Fahrenheit to Celsius):");
		int choice = input.nextInt();//選擇使用的記算方式
		switch (choice) {
		case 1:
			System.out.println("Please input the number:");
			double number = input.nextDouble();
			System.out.println("the answer is:");
			System.out.print(toFahrenheit(number));
			break;

		case 2:
			System.out.println("Please input the number:");
			double number2 = input.nextDouble();
			System.out.println("the answer is:");
			System.out.print(toCelsius(number2));
			break;
		}
	}
}
