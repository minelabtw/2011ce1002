package a1.s100502508;
import java.util.Scanner;//Scanner is in the java.util package
public class A12 
{
	public static void main(String[] args)//main method
	{
		Scanner input=new Scanner(System.in);//creat a Scanner object
		int flag=1;
		while(flag!=0)//repeat to choose
		{
			System.out.print("Enter 1 to transfer C to F\nEnter 2 to transfer F to C ");
			int choice=input.nextInt();
			double temperature;
			switch(choice)//categorize
			{
				case 1://transfer Celsius to Fahrenheit
					System.out.print("Please enter a temperature ");
					temperature=input.nextDouble();
					System.out.println("\n"+temperature+"C --> "+toFahrenheit(temperature)+"F ");//call toFahrenheit method
					break;
				case 2://transfer Fahrenheit to Celsius
					System.out.print("Please enter a temperature ");
					temperature=input.nextDouble();
					System.out.println("\n"+temperature+"F --> "+toCelsius(temperature)+"C ");//call toCelsius method
					break;
				default:
					System.out.println("\nPlease enter 1 or 2 ");
					break;
			}//end switch
			System.out.print("\nEnter 1 to repeat\nEnter 0 to exit ");
			flag=input.nextInt();
		}//end while
	}//end main
	public static double toFahrenheit(double Cnumber)//transfer Celsius to Fahrenheit
	{
		Cnumber=(Cnumber*9/5)+32;
		return Cnumber;
	}//end toFahrenheit method
	public static double toCelsius(double Fnumber)//transfer Fahrenheit to Celsius
	{
		Fnumber=(Fnumber-32)*5/9;
		return Fnumber;
	}//end toCelsius method
}
