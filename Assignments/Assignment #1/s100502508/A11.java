package a1.s100502508;
import java.util.Scanner;//Scanner is in the java.util package
public class A11 
{
	public static void main(String[] args)//main method
	{
		Scanner input=new Scanner(System.in);//creat a Scanner object
		System.out.print("Please enter your name. ");
		String name=input.next();
		System.out.print("Please enter your ID number. ");
		String IDnumber=input.next();
		int flag=1;
		double number1;
		double number2;
		double result;
		while(flag!=0)//repeat to enter
		{
			System.out.print("\nEnter 1 for adding\nEnter 2 for minusing\nEnter 3 for multipling\nEnter 4 for dividing\nEnter 5 for showing infornation\nEnter 6 for exiting ");
			int choice=input.nextInt();
			switch(choice)//categorize
			{
				case 1://adding
					System.out.print("Enter two numbers. ");
					number1=input.nextDouble();
					number2=input.nextDouble();
					result=number1+number2;
					System.out.println(number1+" + "+number2+" = "+result);
					flag=1;
					break;
				case 2://minusing
					System.out.print("Enter two numbers. ");
					number1=input.nextDouble();
					number2=input.nextDouble();
					result=number1-number2;
					System.out.println(number1+" - "+number2+" = "+result);
					flag=1;
					break;
				case 3://multipling
					System.out.print("Enter two numbers. ");
					number1=input.nextDouble();
					number2=input.nextDouble();
					result=number1*number2;
					System.out.println(number1+" * "+number2+" = "+result);
					flag=1;
					break;
				case 4://dividing
					System.out.print("Enter two numbers. ");
					number1=input.nextDouble();
					number2=input.nextDouble();
					result=number1/number2;
					if(number2!=0)//denominator is not equal to zero
					{	
						System.out.println(number1+" / "+number2+" = "+result);
					}
					else//denominator is equal to zero
					{
						System.out.println("\nWrong!! Denuminator can't equal zero. ");
					}
					flag=1;
					break;
				case 5://showing 
					System.out.println("\nName: "+name+" \nIDnumber: "+IDnumber);
					flag=1;
					break;
				case 6://exiting
					flag=0;
					break;
				default:
					System.out.println("Please enter 1 to 6. ");
					flag=1;
					break;
			}//end switch
		}//end while
		System.out.println("\nGood bye,"+name+". Welcome to use me again.");
	}//end main
}
