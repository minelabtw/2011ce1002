package a1.s100502505;

import java.util.Scanner;

public class A11 {
	public static void main(String arg[])
	{
		String name,studentnum;//宣告變數
		int num;
		double num1,num2;
		Scanner input=new Scanner(System.in);
		System.out.print("請輸入你的名字和學號:");
		name = input.next();//輸入進去
		studentnum = input.next();//輸入進去
		for(;;)//重複輸入
		{
			System.out.print("1.加 2.減 3.乘 4.除 5.秀出學號和姓名 6.離開 請問你要選擇哪一個:");
			num = input.nextInt();
			A11 cal = new A11();//建立物件
			switch(num)
			{
				case 1: System.out.print("請輸入兩個數字:");//加
						num1 = input.nextDouble();
						num2 = input.nextDouble();
						System.out.println(cal.add(num1,num2));
						break;
				case 2: System.out.print("請輸入兩個數字:");//減
						num1 = input.nextDouble();
						num2 = input.nextDouble();
						System.out.println(cal.minus(num1,num2));
						break;
				case 3: System.out.print("請輸入兩個數字:");//乘
						num1 = input.nextDouble();
						num2 = input.nextDouble();
						System.out.println(cal.multiple(num1,num2));
						break;
				case 4: System.out.print("請輸入兩個數字:");//除
						num1 = input.nextDouble();
						num2 = input.nextDouble();
						System.out.println(cal.divide(num1,num2));
						break;
				case 5: System.out.print("你的名字是:");//秀出名字和學號
						System.out.println(name);
						System.out.print("你的學號是:");
						System.out.println(studentnum);
						break;
				case 6: System.out.println("謝謝王胖!");//離開
						System.exit(0);
						break;
				default: break;
			}
		}		
	}

	public double add(double num1, double num2)//加 
	{
		double result;
		result = num1 + num2;
		return result;
	}

	public double minus(double num1, double num2)//減
	{
		double result;
		result = num1 - num2;
		return result;
	}

	public double multiple(double num1, double num2)//乘 
	{
		double result;
		result = num1 * num2;
		return result;
	}

	public double divide(double num1, double num2)//除
	{
		double result;
		result = num1 / num2;
		return result;
	}
}//完成!
