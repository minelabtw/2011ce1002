package a1.s100502505;

import java.util.Scanner;

public class A12 {
	public static void main(String arg[])
	{
		int num;//宣告變數
		double num1;
		Scanner input = new Scanner(System.in);
		System.out.print("請問你要 (1)攝氏轉華氏    or (2)華氏轉攝氏 :");
		num = input.nextInt();//輸入進去
		A12 cf = new A12();//建立物件
		switch(num)
		{
			case 1: System.out.print("請輸入一個數字:");//攝氏轉華氏
					num1 = input.nextDouble();
					System.out.println(cf.toFahrenheit(num1));
					break;
			case 2: System.out.print("請輸入一個數字:");//華氏轉攝氏
					num1 = input.nextDouble();
					System.out.println(cf.toCelsius(num1));
					break;
		}
	}
	
	public double toFahrenheit(double num1)//攝氏轉華氏
	{
		double result;
		result = num1 * 9/5 + 32;
		return result;
	}
	
	public double toCelsius(double num1)//華氏轉攝氏
	{
		double result;
		result = (num1 - 32) * 5/9;
		return result;
	}
}//完成!

