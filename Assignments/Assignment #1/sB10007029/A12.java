package A1.sB10007029;

import java.util.Scanner;

public class A12 {

	public static void main(String[] args) {
		Scanner Input = new Scanner(System.in);
		
		double Number=0;
		int MenuChoose=0;
		boolean whilecontorler = true;
		
		while (whilecontorler) {
			System.out.println("1.toFahrenheit , 2.totoCelsius , 3.Exit");
			MenuChoose = Input.nextInt();
			if (MenuChoose < 3){
				System.out.println("Enter a Temperature:");
				Number = Input.nextDouble();
			}
			switch (MenuChoose) {
				case 1:
					System.out.println("Anser:"+toFahrenheit(Number));
					break;
				case 2:
					System.out.println("Anser:"+toCelsius(Number));
					break;
				case 3:
					whilecontorler = false;
					break;
				default:
					System.out.println("Choose not on menu!!");
					break;
			}
		}
		System.exit(0);
	}
	
	public static double toFahrenheit(double Celsius) {	
		return (((Celsius*9.0)/5.0)+32.0);
	}
	public static double toCelsius(double Fahrenheit) {	
		return (((Fahrenheit-32.0)*5.0)/9.0);
	}
}
