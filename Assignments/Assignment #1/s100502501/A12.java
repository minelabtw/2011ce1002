package a1.s100502501;

import java.util.Scanner;

public class A12 {
	public static double toFahrenheit(double a){  //function to calculate Celsius to Fahrenheit
		double fah=(a*9/5)+32;
		return fah;
	}
	public static double toCelsius(double b){ //function to calculate Fahrenheit to Celsius
		double cel=(b-32)*5/9;
		return cel;
	}
	public static void main(String[] args){
		Scanner input = new Scanner(System.in); //add a scanner named "input"
		while(true){
			System.out.println("\n1.Celsius(�J)to Fahrenheit(�K)\n"+"2.Fahrenheit(�K)to Celsius(�J)\n"+"3.Exit");
			System.out.print("Please choose one : "); //output the option
			int choice = input.nextInt(); //store the choice
			if(choice==3) //exit
				  break;
			else
			  switch(choice){
			  	case 1:
			  		System.out.print("Enter the Celsius temperature(�J): ");
			  		double ct = input.nextDouble();
			  		double t1= toFahrenheit(ct); //use function to convert Celsius to Fahrenheit
			  		System.out.println("The Fahrenheit temperature(�K) is : +"+t1); //output the result
			  		break;
			  	case 2:
			  		System.out.print("Enter the Fahrenheit temperature(�K):");
			  		double ft = input.nextDouble();
			  		double t2=toCelsius(ft); //use function to convert Fahrenheit to Celsius
			  		System.out.println("The Celsius temperature(�J) is : "+t2);
			  		break;
			  }
		}
		System.out.print("Goodbye!");	
	  }
}
