package a1.s100502501;

import java.util.Scanner;

public class A11 {
	public static void main(String[] args){
		  Scanner user = new Scanner(System.in); //add a scanner named "user"
		  System.out.print("Please input your name : ");
		  String name = user.next(); //store user name
		  System.out.print("Please input your student number : ");
		  int IDnum = user.nextInt(); //store student number
		  double sum = 0;
		  
		  while(true){
			  //output the option
			  System.out.println("\n1. add  2.minus  3.multiple  4.divide  5.show information  6.reset to 0  7.exit\n"+"Please choose one : ");
			  int choice = user.nextInt(); //store the choice
			  if(choice==7) //exit
				  break;
			  else if(choice==5){ //show user information
				  System.out.println("User name : "+ name);
				  System.out.println("Student number : "+ IDnum);
			      continue;
			  }
			  else if(choice==6){  //reset the calculator to 0
				  sum=0;
	  			  System.out.print("Sum = "+sum+"\n");
	  			  continue;
			  }
			  else
				  System.out.print("Enter the number : ");
			  	  double number = user.nextDouble(); //store the number that user want to calculate
				  switch(choice){
		  			case 1: //add
		  				sum += number; 
		  				System.out.print("Sum = "+sum+"\n");
		  				break;
		  			case 2: //minus
		  				sum -= number; 
		  				System.out.print("Sum = "+sum+"\n");
		  				break;
		  			case 3: //multiple
		  				sum = sum*number;
		  				System.out.print("Sum = "+sum+"\n");
		  				break;
		  			case 4: //divide
		  				sum = sum/number; 
		  				System.out.print("Sum = "+sum+"\n");
		  				break;  		
		  			default:
		  				System.out.print("Error!");
		  				break;		  
		  }  
		}
		System.out.print("Goodbye!!!!!");
	  }
}
