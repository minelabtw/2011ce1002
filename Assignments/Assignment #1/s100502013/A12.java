package a1.s100502013;
import java.util.Scanner;

public class A12 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int choose = 1; //use it to choose function
		double temper; //save the temperature input
		System.out.println("Welcome! This simple program helps you calculate temperature.");
		System.out.println("Enter 1: change Celsius to Fahrenheit"); //list
		System.out.println("Enter 2: change Fahrenheit to Celsius");
		System.out.println("Enter 0: exit");
		while(choose!=0){
			System.out.println("Please enter which one you want to use: ");
			choose = input.nextInt();
			switch(choose){
			case 1: System.out.println("Please enter the temperature of Celsius: "); //C to F
					temper = input.nextDouble();
					System.out.println("Celsius " + temper + " = Fahrenheit " + toFahrenheit(temper));
					break;
					
			case 2: System.out.println("Please enter the temperature of Fahrenheit: "); //F to C
					temper = input.nextDouble();
					System.out.println("Fahrenheit " + temper + " = Celsius " + toCelsius(temper));
					break;
					
			case 0: System.out.println("Goodbye!"); //exit
					break;
					
			default: System.out.println("This is invalid."); //wrong input
					 break;
			}
		}
	}
	
	public static double toFahrenheit(double temp){ //the function to change Celsius to Fahrenheit
		double fah;
		fah = (temp * 9 / 5) + 32;
		return fah;
	}
	
	public static double toCelsius(double temp){ //the function to change Fahrenheit to Celsius
		double cel;
		cel = (temp - 32) * 5 / 9;
		return cel;
	}
}
