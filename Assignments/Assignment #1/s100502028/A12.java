package a1.s100502028;
import java.util.Scanner; // Using Scanner in the java.util package
public class A12 {
	// Method to change Celsius to Fahrenheit 
	public static double toFahrenheit(double celisus){
		return celisus * 9 / 5 + 32;
	}
	
	// Method to change Fahrenheit to Celsius
	public static double toCelisus(double fahrenheit){
		return (fahrenheit - 32) * 5 / 9;
	}
	
	// Main method
	public static void main(String[] args){
		Scanner input = new Scanner(System.in); // Create an object of the Scanner type
		
		// While loop to do the program repeatedly
		while(true){
			double temperature;
			
			// Prompt the user for the choice
			System.out.print("Input 1 to change Celsius to Fahrenheit\nInput 2 to change Fahrenheit to Celsius: ");
			int choose = input.nextInt();
			
			// Switch statement for choice of function
			switch(choose){ 
				case 1: // Case for change Celsius to Fahrenheit
					System.out.print("\nEnter the temperature you in Celsius: "); // Prompt the user for the temperature
					temperature = input.nextDouble();
					System.out.println("You input Celsius" + temperature + " and Fahrenheit is: " + toFahrenheit(temperature) + "\n");
					break;
				case 2: // Case for change Fahrenheit to Celsius
					System.out.print("\nEnter the temperature you in Fahrenheit: ");
					temperature = input.nextDouble();
					System.out.println("You input Fahrenheit " + temperature + " and Celsius is: " + toCelisus(temperature) + "\n");
					break;
				default: // Catch all other characters
					System.out.println("Errors!!!Invalid input of choose!!!");
					System.exit(0);	
			} // End switch 
		} // End while loop
	} // End main method
} //End class
