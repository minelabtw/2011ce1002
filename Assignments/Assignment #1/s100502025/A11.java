package A1.s100502025;

import java.util.Scanner;

public class A11 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);  //輸入
		
		System.out.print("Input yuor name:");  //顯示"輸入名字"
		String name = input.nextLine();  //宣告name
		
		System.out.print("Input your student number:");		
		int number = input.nextInt();
		
		System.out.print("輸入初始數字:");  //先有一個初始值
		double initailnumber = input.nextDouble();
		
		int abc = 1;  //先令abc初始值為為1，讓while能執行
		
		while(abc == 1 ){  //執行while
			System.out.print("你想做哪種運算?(1.加 2.減 3.乘 4.除 5.顯示資料 6.離開):");  //選擇運算方式
			int choice = input.nextInt();  
			switch(choice){	
				case 1 :  //做加的運算
					System.out.print("輸入要加的數字:");
					double number1 = input.nextDouble();
					initailnumber = initailnumber + number1; 
					System.out.print("結果為:" + initailnumber + " \n");  //運算後的結果
					break;
				case 2 :  //做減的運算
					System.out.print("輸入要減數字:");
					double number2 = input.nextDouble();
					initailnumber = initailnumber - number2;	
					System.out.print("結果為:" + initailnumber + " \n");  //運算後的結果
					break;
				case 3 :  //做乘的運算
					System.out.print("輸入要乘的數字:");
					double number3 = input.nextDouble();
					initailnumber = initailnumber * number3;			
					System.out.print("結果為:" + initailnumber + " \n");  //運算後的結果
					break;
				case 4:  //做除的運算
					System.out.print("輸入要除的數字:");
					double number4 = input.nextDouble();
					initailnumber = initailnumber / number4;
					System.out.print("結果為:" + initailnumber + " \n");  //運算後的結果
					break;
				case 5:  //顯示名字，學號
					System.out.print("你的名字:" + name + "\n" + "你的學號:" +  number + "\n");
					break;
				case 6:  //離開
					abc = 2;  //令abc為2，跳出while
					break;
				default:  //輸入不正確的數字，再輸入一次
					System.out.print("選擇錯誤，請再輸入一次\n");  
					break;
			}
		}
		System.out.print( "新學期快樂~");  //結束前顯示的留言
	}
}
