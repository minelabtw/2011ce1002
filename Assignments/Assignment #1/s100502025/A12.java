package A1.s100502025;

import java.util.Scanner;

public class A12 {
	public static void main(String[] args){
		
		Scanner input = new Scanner(System.in);  //輸入
		int abc = 1;  //先令abc為1
		
		while(abc == 1){  //執行while
			System.out.print("選擇你要運算的方式(1.華氏→攝氏  2.攝氏→華氏 3.離開):");  //選擇運算方式
			int choice = input.nextInt();
			if(choice == 1){  //當選擇第一種運算方式
				System.out.print("輸入華氏溫度:");
				double Fahrenheit = input.nextDouble();
				double Celisus = toCelisus(Fahrenheit);  //將數值傳入轉換的函式中，再回傳出來
				System.out.print("華氏 " + Fahrenheit + " 度為攝氏 " + Celisus + " 度\n");  //顯示結果
			}
			if(choice == 2){  //當選擇第二種運算方式
				System.out.print("輸入攝氏溫度:");
				double Celisus = input.nextDouble();
				double Fahrenheit = toFahrenheit(Celisus);  //將數值傳入轉換的函式中，再回傳出來
				System.out.print("攝氏 " + Celisus + " 度為華氏 " + Fahrenheit + " 度\n");  //顯示結果
			}
			if(choice == 3){  //選擇離開
				abc = 2;  //令abc為2，跳出while
			}
		}		
	}

	public static double toCelisus(double fahrenheitt){  //華氏轉攝氏的方法
		double celisuss = ( fahrenheitt - 32 ) * 5 / 9;
		return celisuss;  //回傳
	}
	
	public static double toFahrenheit(double celisusss){  //攝氏轉華氏的方法
		double fahrenheittt = celisusss * 9 / 5 +32;
		return fahrenheittt;  //回傳
	}
}
