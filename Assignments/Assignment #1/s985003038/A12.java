	// ========================================================================================= //
	//		Author: Pluto Chan, Ka Hou															 //
	//		E-mail: jesic01@jesic-tech.com														 //
	//		Date: 2012/2/23																		 //
	//		Last-Mod: 2012/2/25																	 //
	//		Description: Assignment I for course CE1002 - Introduction to Computer Science II	 //
	//					 A simple program that can convert temperature from both degree celsius	 //
	//					 and degree fahrenheit into the other									 //
	// ========================================================================================= //

package a1.s985003038;

import java.text.DecimalFormat;
import java.util.Scanner;

public class A12 {
	public static void main(String[] argv){
		String action;													// temporary save what action do you choose
        Scanner input = new Scanner(System.in);							// a scanner for receiving your input 
        DecimalFormat doubleFormat = new DecimalFormat("#.0");			// display the result with one decimal place
        
        while(true){													// here is a infinite loop so you can keep taking new actions until you choose to exit the program
        	System.out.println("======================================================");
            System.out.println("If you want to convert a temperature in degree celsius to fahrenheit, input 1");
            System.out.println("If you want to convert a temperature in degree fahrenheit to celsius, input 2");
            System.out.println("If you want to exit the program, input 3");
            System.out.println("======================================================");
            System.out.print("What action do you want to take: ");
            action = input.nextLine();									// get what you have input, that is, what action you have chosen
            if(action.equals("1")){										// if you chose action 1, we convert degree celsius into fahrenheit
            	System.out.print("Please input a temperature in degree celsius: ");
            	System.out.println("The corresponding temperature in fahrenheit form is: " + doubleFormat.format(celsiusToFahrenheit(Double.valueOf(input.nextLine()))) + "(�XF)");
            															// here, we send the input, that is, the temperature in celsius as a parameter to the function, and return a temperature in fahrenheit.
            															// and we also convert the reture value, in double, into string with format of one decimal place, so we can print it out using the system function 
            } else if(action.equals("2")){								// if you chose action 2, we convert degree fahrenheit into celsius
            	System.out.print("Please input a temperature in degree fahrenheit: ");
            	System.out.println("The corresponding temperature in celsius form is: " + doubleFormat.format(fahrenheitToCelsius(Double.valueOf(input.nextLine()))) + "(�XC)");
            															// similar to action 1, we convert the temperature from fahrenheit into celsius, and print the result in string form out of the screen
            } else if(action.equals("3")){								// if you chose action 3, we exit the program
            	System.out.print("Bye");
            	break;													// we break out of the loop, so the program will be terminated 
            } else {													// if you input the other opinions, the program will do nothing
            	System.out.println("Error: no such action");
            }	
        }
        
        return;
	}
	
	// ========================================================================================= //
	//		Name: celsiusToFahrenheit															 //
	//		Input: a temperature in double														 //
	//		Output: a temperature in double														 //
	//		Description: converting a temperature in celsius into fahrenheit					 //
	// ========================================================================================= //
	public static double celsiusToFahrenheit(double temperature){		// the method for converting a temperature in celsius into fahrenheit
		return temperature * 9 / 5 + 32; 
	}

	// ========================================================================================= //
	//		Name: fahrenheitToCelsius															 //
	//		Input: a temperature in double														 //
	//		Output: a temperature in double														 //
	//		Description: converting a temperature in fahrenheit into celsius					 //
	// ========================================================================================= //
	public static double fahrenheitToCelsius(double temperature){		// the method for converting a temperature in fahrenheit into celsius
		return (temperature - 32) * 5 / 9;
	}
}
