package a1.s995002203;

import java.util.Scanner;

public class A12 
{
	public static double toFahrenheit(double input)
	{
		double tmp;
		tmp=input*9/5+32;
		return tmp;
	}
	
	public static double toCelsius(double input)
	{
		double tmp;
		tmp=(input-32)*5/9	;
		return tmp;
	} 
	
	public static void main(String[] args)
	{
		double degree=0.0;
		int flag=0;
		Scanner cin=new Scanner(System.in);
		while(flag==0)
		{
			System.out.print("1.celsius to fahrenheit\n2.fahrenheit to celsius\n3.exit\nyou choice:  ");
			switch(cin.nextInt())
			{
				case 1:
					System.out.print("celsius degree: ");
					degree=cin.nextDouble();
					System.out.println("fahrenheit degree is: "+ toFahrenheit(degree));
					break;
				case 2:
					System.out.print("fahrenheit degree: ");
					degree=cin.nextDouble();
					System.out.println("celsius degree is: "+ toCelsius(degree));
					break;
				case 3:
					System.out.println("bye bye");
					System.exit(1);
				default:
					System.out.println("wrong input");
					break;
			}
		}
	}
}
