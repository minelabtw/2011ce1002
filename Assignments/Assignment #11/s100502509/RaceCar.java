package a11.s100502509;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Polygon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class RaceCar extends JPanel{
	private int x = 0;
	private int Velocity;
	Color color;
	Timer timer = new Timer(1000,new TimerListener());
	public RaceCar(){
		color = new Color(0, 0, 0);
		Velocity=0;
		timer.start();
	}
	
	public void setSpeed(int delay) {//set speed
		timer.setDelay(delay);//use delay
		if(delay == 0) {
			Velocity = 0;
		}
		else {
			Velocity = 10;
		}
	}
	
	public void setColor(int x){//Function to set the car color
		if(x==1){
			color = new Color(255,0,0);//Red 
		}
		
		else if (x==2){
			color = new Color(0,255,0);//Green
		}
		
		else{
			color = new Color(0,0,255);//Blue
		}
		
	}

	protected void paintComponent(Graphics g){//graphic the car
			super.paintComponent(g);
			Polygon polygon = new Polygon();
			int y =  2;
			x += Velocity;  //���eX�Z��+5
			if(x>getWidth()){
				x=0;
			}
			int[] px = { x + 20, x + 30, x + 40, x + 10 };
			int[] py = { y, y, y + 10, y + 10 };
			
			for (int i = 0; i < 4; i++)
				polygon.addPoint(px[i], py[i]);
			g.fillPolygon(polygon);
			g.setColor(color);
			g.fillRect(x, y + 10, 50, 10);
			g.fillOval(x + 10, y + 20, 10, 10);
			g.fillOval(x + 30, y + 20, 10, 10);
		}
		
		class TimerListener implements ActionListener{

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				repaint();
			}
			
		}
	}
