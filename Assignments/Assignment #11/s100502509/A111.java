package a11.s100502509;

import java.applet.Applet;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class A111 extends Applet{
	RaceCar r1 = new RaceCar();
	RaceCar r2 = new RaceCar();
	RaceCar r3 = new RaceCar();
	private JRadioButton jrbRed1, jrbGreen1, jrbBlue1;//Create RadioButton 
	private JRadioButton jrbRed2, jrbGreen2, jrbBlue2;
	private JRadioButton jrbRed3, jrbGreen3, jrbBlue3;

	JPanel contenent1 = new JPanel();//Create Panel to put into the Radio Button and TextField
	JPanel contenent2 = new JPanel();
	JPanel contenent3 = new JPanel();
	ButtonGroup group1 = new ButtonGroup();
	ButtonGroup group2 = new ButtonGroup();
	ButtonGroup group3 = new ButtonGroup();
	JTextField field1 = new JTextField("");//Create TextFiedl to input Speed
	JTextField field2 = new JTextField("");
	JTextField field3 = new JTextField("");
	JPanel rdbPanel1 = new JPanel();//Create Panel to put Radio Button
	JPanel rdbPanel2 = new JPanel();
	JPanel rdbPanel3 = new JPanel();
	public A111(){
		
	setLayout(new GridLayout(6,1));	
	contenent1.setLayout(new GridLayout(1,3));
	rdbPanel1.add(jrbRed1 = new JRadioButton("Red"));
	rdbPanel1.add(jrbGreen1 = new JRadioButton("Green"));
	rdbPanel1.add(jrbBlue1 = new JRadioButton("Blue"));
	group1.add(jrbRed1);
	group1.add(jrbGreen1);
	group1.add(jrbBlue1);
	contenent1.add(rdbPanel1);
	contenent1.add(new JLabel ("Speed"));
	contenent1.add(field1);
	
	contenent2.setLayout(new GridLayout(1,3));
	rdbPanel2.add(jrbRed2 = new JRadioButton("Red"));
	rdbPanel2.add(jrbGreen2 = new JRadioButton("Green"));
	rdbPanel2.add(jrbBlue2 = new JRadioButton("Blue"));
	group2.add(jrbRed2);
	group2.add(jrbGreen2);
	group2.add(jrbBlue2);
	//pCar2.add(r2);
	contenent2.add(rdbPanel2);
	contenent2.add(new JLabel ("Speed"));
	contenent2.add(field2);
	
	contenent3.setLayout(new GridLayout(1,3));
	rdbPanel3.add(jrbRed3 = new JRadioButton("Red"));
	rdbPanel3.add(jrbGreen3 = new JRadioButton("Green"));
	rdbPanel3.add(jrbBlue3 = new JRadioButton("Blue"));
	group3.add(jrbRed3);
	group3.add(jrbGreen3);
	group3.add(jrbBlue3);
	//pCar3.add(r3);
	contenent3.add(rdbPanel3);
	contenent3.add(new JLabel ("Speed"));
	contenent3.add(field3);
	
	add(contenent1);
	add(r1);
	add(contenent2);
	add(r2);
	add(contenent3);
	add(r3);
	jrbRed1.addActionListener(new ActionListener(){//car1 use Red Button
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			r1.setColor(1);
		}
	});
	
	jrbRed2.addActionListener(new ActionListener(){//car2 use Red Button
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			r2.setColor(1);
		}
		
	});
	
	jrbRed3.addActionListener(new ActionListener(){//car3 use Red Button
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			r3.setColor(1);
		}
		
	});
	
	jrbGreen1.addActionListener(new ActionListener(){//car1 use Green Button
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			r1.setColor(2);
		}
		
	});
	
	jrbGreen2.addActionListener(new ActionListener(){//car2 use Green Button
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			r2.setColor(2);
		}
		
	});
	
	jrbGreen3.addActionListener(new ActionListener(){//car3 use Green Button
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			r3.setColor(2);
		}
		
	});
	
	jrbBlue1.addActionListener(new ActionListener(){//car1 use Blue Button
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			r1.setColor(3);
		}
		
	});
	
	jrbBlue2.addActionListener(new ActionListener(){//car2 use Blue Button
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			r2.setColor(3);
		}
		
	});
	
	jrbBlue3.addActionListener(new ActionListener(){//car3 use Blue Button
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			r3.setColor(3);
		}
		
	});
	field1.addActionListener(new ActionListener(){//car1 use TextField

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			r1.setSpeed(100-Integer.parseInt(field1.getText()));//set the speed
		}
	});
	
	field2.addActionListener(new ActionListener(){//car2 use TextField

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			r2.setSpeed(100-Integer.parseInt(field2.getText()));//set the speed
		}
	});
	
	field3.addActionListener(new ActionListener(){//car3 use TextField

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			r3.setSpeed(100-Integer.parseInt(field3.getText()));//set the speed
		}
	});
	
	}
}
