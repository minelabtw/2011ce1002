package a11.s100502016;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollBar;
import javax.swing.JTextField;

public class A111 extends JApplet {
	
	class controlPanel extends JPanel {
		private TextField txtVar = new TextField();
		private JScrollBar jscbVelocity = new JScrollBar(JScrollBar.HORIZONTAL);
		private RaceCar c1 = new RaceCar();
		private JPanel assemble = new JPanel();
		private JPanel txtPanel = new JPanel();
		private JPanel radioGroup = new JPanel();
		private TextField txtVelocity = new TextField("Min=0,Max=100");
		private JLabel lbl = new JLabel("Velocity");
		private JButton btn = new JButton("Change Velicity");
		private JRadioButton jrbRed = new JRadioButton("Red");
		private JRadioButton jrbGreen = new JRadioButton("Green");
		private JRadioButton jrbBlue = new JRadioButton("Blue");

		controlPanel() {

			jrbRed.addActionListener(new radioListener());
			jrbGreen.addActionListener(new radioListener());
			jrbBlue.addActionListener(new radioListener());
			btn.addActionListener(new radioListener());

			radioGroup.setLayout(new GridLayout(3, 1, 0, 2));
			radioGroup.add(jrbRed);
			radioGroup.add(jrbGreen);
			radioGroup.add(jrbBlue);
			jrbRed.setSelected(true);

			
			

			jscbVelocity.addAdjustmentListener(new BarListener());

			assemble.setLayout(new GridLayout(1, 5, 0, 5));
			assemble.add(radioGroup);
			assemble.add(lbl);
			assemble.add(txtVelocity);
			assemble.add(btn);
			assemble.add(jscbVelocity);

			setLayout(new GridLayout(2, 1, 0, 5));
			add(c1);
			add(assemble);
		}

		class radioListener implements ActionListener {
			public void actionPerformed(ActionEvent e) {
				if (e.getSource() == jrbRed) {
					c1.setColor(new Color(255, 0, 0), new Color(255, 100, 100),
							new Color(200, 50, 50));
					jrbRed.setSelected(true);
					jrbGreen.setSelected(false);
					jrbBlue.setSelected(false);
				} else if (e.getSource() == jrbGreen) {
					c1.setColor(new Color(0, 255, 0), new Color(100, 255, 100),
							new Color(50, 200, 50));
					jrbRed.setSelected(false);
					jrbGreen.setSelected(true);
					jrbBlue.setSelected(false);
				} else if (e.getSource() == jrbBlue) {
					c1.setColor(new Color(0, 0, 255), new Color(100, 100, 255),
							new Color(50, 50, 200));
					jrbRed.setSelected(false);
					jrbGreen.setSelected(false);
					jrbBlue.setSelected(true);
				} else if (e.getSource() == btn) {
					c1.setSpeed(Double.parseDouble(txtVelocity.getText()));
					jscbVelocity.setValue(Integer.parseInt(txtVelocity.getText()));
				}
			}
		}

		class BarListener implements AdjustmentListener {
			public void adjustmentValueChanged(AdjustmentEvent e) {
				double value = jscbVelocity.getValue();
				c1.setSpeed(value);

			}
		}

	}
	controlPanel p1 = new controlPanel();
	controlPanel p2 = new controlPanel();
	controlPanel p3 = new controlPanel();

	public A111() {
		setLayout(new GridLayout(3, 1));
		add(p1);
		add(p2);
		add(p3);
	}
}
