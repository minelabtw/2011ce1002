package a11.s100502016;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

import javax.swing.*;

public class RaceCar extends JPanel {
	private int delay;
	private double speed;
	Color clrHead = new Color(255, 0, 0);
	Color clrBody = new Color(255, 100, 100);
	Color clrFeet = new Color(200, 50, 50);

	private int ref_x = getWidth() / 4;
	private int ref_y = getHeight() + 20;
	int dir = 0;

	RaceCar() {
		delay = 10;
		speed = 5;
		Timer timer = new Timer(delay, new TimerListener());
		timer.start();
	}

	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			repaint();
		}
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}
	

	public void setColor(Color clrHead, Color clrBody, Color clrFeet) {
		this.clrHead = clrHead;
		this.clrBody = clrBody;
		this.clrFeet = clrFeet;
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int head_x[] = { ref_x + 40, ref_x + 60, ref_x + 80, ref_x + 20 };
		int head_y[] = { ref_y + 0, ref_y + 0, ref_y + 20, ref_y + 20 };
		int body_x[] = { ref_x + 0, ref_x + 100, ref_x + 100, ref_x + 0 };
		int body_y[] = { ref_y + 20, ref_y + 20, ref_y + 40, ref_y + 40 };
		g.setColor(clrHead);
		g.fillPolygon(head_x, head_y, head_x.length);
		g.setColor(clrBody);
		g.fillPolygon(body_x, body_y, body_x.length);
		g.setColor(clrFeet);
		g.fillOval(ref_x + 25, ref_y + 35, 20, 20);
		g.fillOval(ref_x + 55, ref_y + 35, 20, 20);

		if (dir == 0) {
			ref_x += speed;
		} else if (dir == 1) {
			ref_x -= speed;
		}
		if (ref_x >= getWidth() - 100)
			dir = 1;
		else if (ref_x <= 0)
			dir = 0;
	}
}
