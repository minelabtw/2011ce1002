package a11.s995002204;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class RaceCar extends JPanel 
{
	//declare variable
	private int cycle = 10;
	private int styleC = 1;
	private int color = 0;
	private int x = 0;
	private int s = 10;
	private int delay = 400;
	private Timer timer;
	private int second=0;
	
	public RaceCar()
	{
		timer = new Timer(delay, new TimerListener());
		timer.start();
	}
	
	public void paintComponent(Graphics g) 
	{
	 	super.paintComponent(g);
		if(color ==0)
		{
			g.setColor(Color.black);
		}
		else if (color == 1)
		{
			g.setColor(Color.red);
		}
		else
		{
			g.setColor(Color.green);
		}
		if(styleC <= 1)
		{
			g.fillRoundRect(x,0,100,50,0,0);
			g.setColor(Color.red);
			g.fillOval(x+20,40,20,20);
			g.fillOval(x+60,40,20,20);
		}
		else if(styleC <= 2)
		{
			if(cycle >=40)
			{
				cycle = 10;
			}
			g.fillOval(x+10,0,cycle,cycle);
			cycle+=10;
		}
		else if (styleC <=3)
		{
			int clockRadius = (int)(Math.min(getWidth(), getHeight())*0.48);

			int yCenter = getHeight() / 2;
		    g.drawOval(x - clockRadius, yCenter - clockRadius, 2 * clockRadius, 2 * clockRadius);
		    g.drawString("12", x - 5, yCenter - clockRadius + 12);
		    g.drawString("9", x - clockRadius + 3, yCenter + 5);
			g.drawString("3", x + clockRadius - 10, yCenter + 3);
			g.drawString("6", x- 3, yCenter + clockRadius - 3);
			int sLength = (int)(clockRadius * 0.8);
			int xSecond = (int)(x + sLength * Math.sin(second * (2 * Math.PI / 60)));
			int ySecond = (int)(yCenter - sLength * Math.cos(second * (2 * Math.PI / 60)));
			g.setColor(Color.red);
			g.drawLine(x, yCenter, xSecond, ySecond);
			second++;
		}
		
		//determine the object is out of the frame of not
		if(x>=getWidth())
		{
			x = -120;
		}
		x+=s;
	}
	
	class TimerListener implements ActionListener 
	{
		public void actionPerformed(ActionEvent e) 
		{
			repaint();
		}
	}
	
	
	//set color
	public void setColor(int tcolor)
	{
		color = tcolor;	
	}
	
	//set delay
	public void setDalay(int time)
	{
		timer.setDelay(time);
	}
	
	// set style
	public void setStyle(int d){
		styleC = d;
		System.out.println(d);
	}
}

