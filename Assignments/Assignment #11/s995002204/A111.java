package a11.s995002204;

import java.applet.Applet;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;
import javax.swing.*;

public class A111  extends JApplet
{
	//declare object
	private JRadioButton[] sC = new JRadioButton[9];
	private JLabel[] speed = new JLabel[3];
	private JTextField[] sEnter = new JTextField[3];
	private RaceCar[] c = new RaceCar[3];
	private JButton[] style = new JButton[3];
	
	//constructor
	public A111()
	{
		Timer timer = new Timer(100,new TimerListener());
		timer.start();
		Listener listener = new Listener();
		JPanel[] p = new JPanel[3];
		JPanel[] pRadio = new JPanel[3];
		for(int i =0;i<p.length;i++){
			p[i] = new JPanel();
			p[i].setLayout(new GridLayout(2,1));
			pRadio[i] = new JPanel();
		}
		for(int i =0;i<sEnter.length;i++){
			sEnter[i]= new JTextField("400");
		}
		for(int i = 0;i<sC.length;i+=3){
			sC[i] = new JRadioButton("red");
			sC[i].addActionListener(listener);
			sC[i+1] = new JRadioButton("green");
			sC[i+1].addActionListener(listener);
			sC[i+2] = new JRadioButton("black");
			sC[i+2].addActionListener(listener);
			sC[i+2].setSelected(true);
		}
		for(int i = 0;i<pRadio.length;i++){
			for(int y = 0;y<3;y++){
				pRadio[i].add(sC[y+i*3]);
			}
			speed[i] = new JLabel("speed enter(number inverse");
			style[i] = new JButton("style change");
			style[i].addActionListener(listener);
			pRadio[i].add(speed[i]);
			pRadio[i].add(sEnter[i]);
			pRadio[i].add(style[i]);
		}
		
		for(int i =0;i<c.length;i++){
			c[i] = new RaceCar();
		}
		for(int i =0;i<p.length;i++){
			p[i].add(pRadio[i]);
			p[i].add(c[i]);
		}
		setLayout(new GridLayout(3,1));
		for(int i = 0 ;i<p.length;i++){
			add(p[i]);
		}
	}
	class Listener implements ActionListener 
	{
		PrintWriter[] output = new PrintWriter[6];
		public void actionPerformed(ActionEvent e)
		{
			if(e.getSource() == sC[0])
			{
				c[0].setColor(1);
				sC[1].setSelected(false);
				sC[2].setSelected(false);
			}
			else if(e.getSource() == sC[1])
			{
				c[0].setColor(2);
				sC[0].setSelected(false);
				sC[2].setSelected(false);
			}
			else if(e.getSource() == sC[2])
			{
				c[0].setColor(0);
				sC[0].setSelected(false);
				sC[1].setSelected(false);
			}
			if(e.getSource() == sC[3])
			{
				c[1].setColor(1);
				sC[4].setSelected(false);
				sC[5].setSelected(false);
			}
			else if(e.getSource() == sC[4])
			{
				c[1].setColor(2);
				sC[3].setSelected(false);
				sC[5].setSelected(false);
			}
			else if(e.getSource() == sC[5])
			{
				c[1].setColor(0);
				sC[3].setSelected(false);
				sC[4].setSelected(false);
			}
			if(e.getSource() == sC[6])
			{
				c[2].setColor(1);
				sC[7].setSelected(false);
				sC[8].setSelected(false);
			}
			else if(e.getSource() == sC[7])
			{
				c[2].setColor(2);
				sC[6].setSelected(false);
				sC[8].setSelected(false);
			}
			else if(e.getSource() == sC[8])
			{
				c[2].setColor(0);
				sC[7].setSelected(false);
			}
			for(int i =0;i<style.length;i++)
			{
				if(e.getSource() == style[i])
				{
					c[i].setStyle((int)(Math.random()*3)+1);
				}
			}
			
		}
	}
	
	class TimerListener implements ActionListener 
	{
		public void actionPerformed(ActionEvent e) 
		{
			for(int i =0;i<c.length;i++)
			{
				c[i].setDalay(Integer.valueOf(sEnter[i].getText()));
			}
		}
	}
	
	public static void main(String[] args) 
	{
		A111 applet = new A111();
		JFrame frame = new JFrame();
		frame.setLayout(new FlowLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("A111");
		frame.setSize(500, 500);
		frame.setVisible(true);
	}
}
