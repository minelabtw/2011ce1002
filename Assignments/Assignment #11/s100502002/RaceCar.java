package a11.s100502002;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class RaceCar extends JPanel
{
	private int bodyx = 100;//the initial position of the car
	private int bodyy;
	
	private int k;
	
	Timer timer;
	
	
	public RaceCar()
	{
	}
	public void start()//car to start
	{
		timer.start();
	}

	public void sety(int y)
	{
		bodyy = y;
	}
	protected void paintComponent(Graphics g)//draw a car
	{
		
		super.paintComponent(g);
		
		if(bodyy==0)
		{
			bodyy=getHeight();
		}
		g.drawLine(0, bodyy+30, getWidth(), bodyy+30);
		g.setColor(Color.BLACK);
		g.fillOval(bodyx, bodyy+15, 10, 10);
		g.fillOval(bodyx+40, bodyy+15, 10, 10);
		g.setColor(Color.yellow);
		int headx [] = {bodyx+10,bodyx,bodyx+50,bodyx+40};
		int heady [] = {bodyy-10,bodyy,bodyy,bodyy-10};
		g.fillPolygon(headx, heady, 4);
		g.setColor(Color.BLUE);
		g.fillRect(bodyx, bodyy, 50, 20);
		switch(k)//set color
		{
		case 1:
			g.setColor(Color.red);
			g.fillRect(bodyx, bodyy, 50, 20);
			break;
		case 2:
			g.setColor(Color.YELLOW);
			g.fillRect(bodyx, bodyy,50, 20);
			break;
		case 3:
			g.setColor(Color.BLUE);
			g.fillRect(bodyx, bodyy, 50, 20);
			break;
		case 4:
			g.setColor(Color.BLACK);
			g.fillRect(bodyx, bodyy, 50, 20);
			break;
		case 5:
			g.setColor(Color.PINK);
			g.fillRect(bodyx, bodyy, 50, 20);
			break;
		case 6:
			g.setColor(Color.ORANGE);
			g.fillRect(bodyx, bodyy, 50, 20);
			break;
		case 7:
			g.setColor(Color.WHITE);
			g.fillRect(bodyx, bodyy, 50, 20);
			break;
		case 8:
			g.setColor(Color.GREEN);
			g.fillRect(bodyx, bodyy, 50, 20);
			break;
		case 9:
			g.setColor(Color.CYAN);
			g.fillRect(bodyx, bodyy, 50, 20);
			break;
		default:
			g.fillRect(bodyx, bodyy, 50, 20);
			break;
		
			
			
		}
		bodyx = bodyx +10;//move function
		if(bodyx>getWidth())
			bodyx=0;
		
	}
	
	public void setcolor(String string)//a value to control color
	{
		if(string == "red")
			k=1;
		else if(string == "yellow")
			k=2;
		else if(string == "blue")
			k=3;
		else if(string == "black")
			k=4;
		else if(string == "pink")
			k=5;
		else if(string == "orange")
			k=6;
		else if(string == "white")
			k=7;
		else if(string == "green")
			k=8;
		else if(string == "purple")
			k=9;
		else
			k=10;
	}
	public void setspeed(int speed)//set delay
	{
		timer.setDelay(speed);
		
	}
	public Timer settimer(Timer time)//timer is in the other class
	{
		timer = time;
		return timer;
	}
	
	
	
	public class TimerListener implements ActionListener//when time goes by what will the program do
	{
		public void actionPerformed(ActionEvent e)
		{
			repaint();
			
		}
		
	}

}
