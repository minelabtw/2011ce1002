package a11.s100502002;
import javax.swing.*;
import java.awt.event.*;

import javax.swing.event.*;
import java.awt.*;
public class A111 extends JApplet
{
	private RaceCar car1 = new RaceCar();
	private RaceCar car2 = new RaceCar();
	private RaceCar car3 = new RaceCar();
	Timer timer1 = new Timer(200, new TimerListener(car1));
	Timer timer2 = new Timer(200, new TimerListener(car2));
	Timer timer3 = new Timer(200, new TimerListener(car3));
	private ButtonGroup group1 = new ButtonGroup();
	private ButtonGroup group2 = new ButtonGroup();
	private ButtonGroup group3 = new ButtonGroup();
	private JRadioButton yellow = new JRadioButton("yellow");
	private JRadioButton blue = new JRadioButton("Blue");
	private JRadioButton orange = new JRadioButton("Orange");
	private JRadioButton black = new JRadioButton("black");
	private JRadioButton white = new JRadioButton("white");
	private JRadioButton pink = new JRadioButton("pink");
	private JRadioButton green = new JRadioButton("green");
	private JRadioButton cyan = new JRadioButton("cyan");
	private JRadioButton red = new JRadioButton("Red");
	private JPanel p_car =new JPanel();
	private JPanel p_button = new JPanel();
	private JPanel p_speed = new JPanel();

	private JLabel c1 = new JLabel("car1");
	private JLabel c2 = new JLabel("car2");
	private JLabel c3 = new JLabel("car3");
	private int speed1;
	private int speed2;
	private int speed3;
	
	private JTextField f1= new JTextField("0",4);
	private JTextField f2= new JTextField("0",4);
	private JTextField f3= new JTextField("0",4);//there are all element on the interface
	
	public boolean check (String s)//check the string from the field all can switch to integer
	{
		int counter=0;
		for(int i=0;i<s.length();i++)
		{
			char temp = s.charAt(i);
			if(temp=='0'||temp=='1'||temp=='2'||temp=='3'||temp=='4'||temp=='5'||temp=='6'||temp=='7'||temp=='8'||temp=='9')
			{
				counter++;
			}
				
			
		}
		if(counter==s.length())
			return true;
		else
			return false;
	}
	public void init()
	{
		
		car1.settimer(timer1);
		car2.settimer(timer2);
		car3.settimer(timer3);
		
		 f1.getDocument().addDocumentListener(new DocumentListener()
		 {
			public void changedUpdate(DocumentEvent e)
			{
			}
			public void insertUpdate(DocumentEvent e) 
			{
				String text = f1.getText();
				if(check(text))
				{
					speed1 = Integer.parseInt(text);
					car1.setspeed(speed1);
				}
				else{}
			}
			public void removeUpdate(DocumentEvent e) 
			{
			}
		});
		f2.getDocument().addDocumentListener(new DocumentListener()
		{
			public void changedUpdate(DocumentEvent e)
			{
				
			}
			public void insertUpdate(DocumentEvent e) 
			{
				String text = f2.getText();
				if(check(text))
				{
					speed2 = Integer.parseInt(text);
					car2.setspeed(speed2);
				}
				else{}
			}
			public void removeUpdate(DocumentEvent e) 
			{
				
			}
		});
		f3.getDocument().addDocumentListener(new DocumentListener()
		{
			public void changedUpdate(DocumentEvent e)
			{
				
			}
			public void insertUpdate(DocumentEvent e) 
			{
				String text = f3.getText();
				if(check(text))
				{
					speed3 = Integer.parseInt(text);
					car3.setspeed(speed3);
					System.out.println(text);
				}
				else{}
			}
			public void removeUpdate(DocumentEvent e) 
			{
				
			}
		});
		
		
		
		car1.start();
		car2.start();
		car3.start();
	
		
		car1.sety((1/2)*getHeight());
		car2.sety((1/2)*getHeight());
		car3.sety((1/2)*getHeight());
		
		ButtonListener li = new ButtonListener();
		yellow.addActionListener(li);
		blue.addActionListener(li);
		orange.addActionListener(li);
		black.addActionListener(li);
		white.addActionListener(li);
		pink.addActionListener(li);
		green.addActionListener(li);
		red.addActionListener(li);
		cyan.addActionListener(li);
		
		p_car.setLayout(new GridLayout(3,1,1,1));
		p_button.setLayout(new GridLayout(9, 1, 1, 1));
		
		group1.add(yellow);
		group1.add(blue);
		group1.add(orange);
		group2.add(black);
		group2.add(white);
		group2.add(pink);
		group3.add(green);
		group3.add(red);
		group3.add(cyan);
		
		
		p_car.add(car1);
		p_car.add(car2);
		p_car.add(car3);
		
		p_speed.add(c1);
		p_speed.add(f1);
		p_speed.add(c2);
		p_speed.add(f2);
		p_speed.add(c3);
		p_speed.add(f3);
		
		p_button.add(yellow);
		p_button.add(blue);
		p_button.add(orange);
		p_button.add(black);
		p_button.add(white);
		p_button.add(pink);
		p_button.add(green);
		p_button.add(red);
		p_button.add(cyan);//add all element to screen
		
		
		
		add(p_speed,BorderLayout.NORTH);
		add(p_button,BorderLayout.WEST);
		add(p_car,BorderLayout.CENTER);//border layout
		
		
	
	}
	public class ButtonListener implements ActionListener//what will happen when button is be clicked
	{
		
		public void actionPerformed(ActionEvent e) 
		{
			if(e.getSource()==yellow)
			{
				car1.setcolor("yellow");
			}
			else if(e.getSource() == blue)
			{
				car1.setcolor("blue");
			}
			else if(e.getSource() == orange)
			{
				car1.setcolor("orange");
			}
			else if(e.getSource() == black)
			{
				car2.setcolor("black");
			}
			else if(e.getSource() == white)
			{
				car2.setcolor("white");
			}
			else if(e.getSource() == pink)
			{
				car2.setcolor("pink");
			}
			else if(e.getSource() == green)
			{
				car3.setcolor("green");
			}
			else if(e.getSource() == cyan)
			{
				car3.setcolor("cyan");
			}
			else if(e.getSource() == red)
			{
				car3.setcolor("red");
			}
				
		}
	}
	class TimerListener implements ActionListener//time listener
	{
		RaceCar car;
		TimerListener(RaceCar Car)
		{
			car = Car;
		}
		public void actionPerformed(ActionEvent e)
		{
			car.repaint();
		}
	}
	
}
