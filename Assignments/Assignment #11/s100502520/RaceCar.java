package a11.s100502520;

import java.awt.Color;
import java.awt.Graphics;

import javax.security.auth.x500.X500Principal;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class RaceCar extends JPanel{
	private int x = 0;
	private Color color = Color.blue;
	
	//設定車位置
	public void setx(){
		x++;
		if(x == getWidth()+40){
			x = 0;
		}
	}
	
	//設定顏色
	public void setcolor(int i){
		if(i == 1){
			color = Color.GREEN;
		}
		else if(i == 2){
			color = Color.BLUE;
		}
		else if(i == 3){
			color = Color.RED;
		}
	}
	
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		int width = getWidth();
		int height = getHeight();
		
		//畫出車子
		g.setColor(color);
		g.fillRect(x, (int)(0.5*height), 40, 20);
		g.setColor(Color.BLACK);
		g.fillOval(x+5, (int)(0.5*height+19), 10, 10);
		g.fillOval(x+25, (int)(0.5*height+19), 10, 10);
	}
}
