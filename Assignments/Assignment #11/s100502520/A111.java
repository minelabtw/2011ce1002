package a11.s100502520;

import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.Timer;

public class A111 extends JApplet implements ActionListener{
	
	Timer timer1 = new Timer(500, new TimerListener());
	Timer timer2 = new Timer(500, new TimerListener());
	Timer timer3 = new Timer(500, new TimerListener());
	RaceCar car1 = new RaceCar();
	RaceCar car2 = new RaceCar();
	RaceCar car3 = new RaceCar();
	private JButton fast1 = new JButton("fast");
	private JButton slow1 = new JButton("slow");
	private JButton fast2 = new JButton("fast");
	private JButton slow2 = new JButton("slow");
	private JButton fast3 = new JButton("fast");
	private JButton slow3 = new JButton("slow");
	private JButton color11 = new JButton("green");
	private JButton color21 = new JButton("blue");
	private JButton color31 = new JButton("red");
	private JButton color12 = new JButton("green");
	private JButton color22= new JButton("blue");
	private JButton color32 = new JButton("red");
	private JButton color13 = new JButton("green");
	private JButton color23 = new JButton("blue");
	private JButton color33 = new JButton("red");
	private JPanel panel1 = new JPanel();
	private JPanel panel2 = new JPanel();
	private JPanel panel3 = new JPanel();
	private int v1 = 1000;
	private int v2 = 1000;
	private int v3 = 1000;
	
	public A111(){
		//第一輛車的功能鍵
		panel1.setLayout( new GridLayout(5, 1, 1 , 1));
		panel1.add(fast1);
		panel1.add(slow1);
		panel1.add(color11);
		panel1.add(color21);
		panel1.add(color31);
		//第二輛車的功能鍵
		panel2.setLayout( new GridLayout(5, 1, 1 , 1));
		panel2.add(fast2);
		panel2.add(slow2);
		panel2.add(color12);
		panel2.add(color22);
		panel2.add(color32);
		//第三輛車的功能鍵
		panel3.setLayout( new GridLayout(5, 1, 1 , 1));
		panel3.add(fast3);
		panel3.add(slow3);
		panel3.add(color13);
		panel3.add(color23);
		panel3.add(color33);
		
		fast1.addActionListener(this);
		slow1.addActionListener(this);
		color11.addActionListener(this);
		color12.addActionListener(this);
		color13.addActionListener(this);
		
		fast2.addActionListener(this);
		slow2.addActionListener(this);
		color21.addActionListener(this);
		color22.addActionListener(this);
		color23.addActionListener(this);
		
		fast3.addActionListener(this);
		slow3.addActionListener(this);
		color31.addActionListener(this);
		color32.addActionListener(this);
		color33.addActionListener(this);
		
		setLayout( new GridLayout(3, 2, 1 , 1));
		add(car1);
		add(panel1);
		add(car2);
		add(panel2);
		add(car3);
		add(panel3);
	}
	
	private class TimerListener implements ActionListener{ 
		public void actionPerformed(ActionEvent e){
			if(e.getSource() ==timer1){
				car1.setx();
				car1.repaint();
			}
			if(e.getSource() ==timer2){
				car2.setx();
				car2.repaint();
			}
			if(e.getSource() ==timer3){
				car3.setx();
				car3.repaint();
			}
			
			
		}
		
	}
	
	//按下各按鈕情況
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == fast1){
			v1 = v1-100;
			timer1.start();
			timer1.setDelay(v1);
			timer1.restart();
			car1.repaint();
		}
		
		else if(e.getSource() == slow1){
			v1 = v1+100;
			timer1.setDelay(v1);
			timer1.restart();
			car1.repaint();
			
		}
		
		if(e.getSource() == fast2){
			v2 = v2-100;
			timer2.start();
			timer2.setDelay(v1);
			timer2.restart();
			car2.repaint();
		}
		
		else if(e.getSource() == slow2){
			v2 = v2+100;
			timer2.setDelay(v1);
			timer2.restart();
			car2.repaint();
		}
		
		if(e.getSource() == fast3){
			v3 = v3-100;
			timer3.start();
			timer3.setDelay(v1);
			timer3.restart();
			car3.repaint();
		}
		
		else if(e.getSource() == slow3){
			v3 = v3+100;
			timer3.setDelay(v1);
			timer3.restart();
			car3.repaint();
		}
		if(e.getSource() == color11){
			car1.setcolor(1);
			car1.repaint();
		}
		else if(e.getSource() == color21){
			car1.setcolor(2);
			car1.repaint();
		}
		else if(e.getSource() == color31){
			car1.setcolor(3);
			car1.repaint();
		}
		
		if(e.getSource() == color12){
			car2.setcolor(1);
			car2.repaint();
		}
		else if(e.getSource() == color22){
			car2.setcolor(2);
			car2.repaint();
		}
		else if(e.getSource() == color32){
			car2.setcolor(3);
			car2.repaint();
		}
		
		if(e.getSource() == color13){
			car3.setcolor(1);
			car3.repaint();
		}
		else if(e.getSource() == color23){
			car3.setcolor(2);
			car3.repaint();
		}
		else if(e.getSource() == color33){
			car3.setcolor(3);
			car3.repaint();
		}	
	}
}
