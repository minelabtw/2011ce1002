package a11.s100502505;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

public class A111 extends JApplet{
	private JRadioButton red1;
	private JRadioButton red2;
	private JRadioButton red3;
	private JRadioButton blue1;
	private JRadioButton blue2;
	private JRadioButton blue3;
	private JRadioButton green1;
	private JRadioButton green2;
	private JRadioButton green3;
	private JTextField speed1;
	private JTextField speed2;
	private JTextField speed3;
	private JButton go1;
	private JButton go2;
	private JButton go3;
	private RaceCar car1;
	private RaceCar car2;
	private RaceCar car3;
	
	public A111() 
	{
		setLayout(new GridLayout(3, 2));//排版
		
		JPanel p1 = new JPanel();//宣告
		JPanel p2 = new JPanel();
		JPanel p3 = new JPanel();
		red1 = new JRadioButton("Red");
		red2 = new JRadioButton("Red");
		red3 = new JRadioButton("Red");
		blue1 = new JRadioButton("Blue");
		blue2 = new JRadioButton("Blue");
		blue3 = new JRadioButton("Blue");
		green1 = new JRadioButton("Green");
		green2 = new JRadioButton("Green");
		green3 = new JRadioButton("Green");
		car1 = new RaceCar();
		car2 = new RaceCar();
		car3 = new RaceCar();
		speed1 = new JTextField("0");
		speed2 = new JTextField("0");
		speed3 = new JTextField("0");
		go1 = new JButton("Go");
		go2 = new JButton("Go");
		go3 = new JButton("Go");
		ButtonGroup group1 = new ButtonGroup();
		ButtonGroup group2 = new ButtonGroup();
		ButtonGroup group3 = new ButtonGroup();
		group1.add(red1);
		group1.add(blue1);
		group1.add(green1);
		group2.add(red2);
		group2.add(blue2);
		group2.add(green2);
		group3.add(red3);
		group3.add(blue3);
		group3.add(green3);
		JrbRed jrbRed = new JrbRed();
		JrbBlue jrbBlue = new JrbBlue();
		JrbGreen jrbGreen = new JrbGreen();
		red1.addActionListener(jrbRed);
		red2.addActionListener(jrbRed);
		red3.addActionListener(jrbRed);
		blue1.addActionListener(jrbBlue);
		blue2.addActionListener(jrbBlue);
		blue3.addActionListener(jrbBlue);
		green1.addActionListener(jrbGreen);
		green2.addActionListener(jrbGreen);
		green3.addActionListener(jrbGreen);
		JbtConfirm jbtConfirm = new JbtConfirm();
		go1.addActionListener(jbtConfirm);
		go2.addActionListener(jbtConfirm);
		go3.addActionListener(jbtConfirm);
		p1.setLayout(new GridLayout(1, 5));
		p2.setLayout(new GridLayout(1, 5));
		p3.setLayout(new GridLayout(1, 5));
		p1.add(red1);
		p1.add(blue1);
		p1.add(green1);
		p1.add(speed1);
		p1.add(go1);
		p2.add(red2);
		p2.add(blue2);
		p2.add(green2);
		p2.add(speed2);
		p2.add(go2);
		p3.add(red3);
		p3.add(blue3);
		p3.add(green3);
		p3.add(speed3);
		p3.add(go3);
		add(p1);
		add(car1);
		add(p2);
		add(car2);
		add(p3);
		add(car3);
	}
	class JrbRed implements ActionListener 
	{
		public void actionPerformed(ActionEvent e)//設定按鈕的意義 
		{
			if(e.getSource() == red1)
			{
				car1.setColor(Color.RED);
			}
			if(e.getSource() == red2) 
			{
				car2.setColor(Color.RED);
			}
			if(e.getSource() == red3) 
			{
				car3.setColor(Color.RED);
			}
		}
	}
	class JrbBlue implements ActionListener 
	{
		public void actionPerformed(ActionEvent e)//設定按鈕的意義
		{
			if(e.getSource() == blue1) 
			{
				car1.setColor(Color.BLUE);
			}
			if(e.getSource() == blue2) 
			{
				car2.setColor(Color.BLUE);
			}
			if(e.getSource() == blue3) 
			{
				car3.setColor(Color.BLUE);
			}
		}
	}
	class JrbGreen implements ActionListener
	{
		public void actionPerformed(ActionEvent e)//設定按鈕的意義 
		{
			if(e.getSource() == green1) 
			{
				car1.setColor(Color.GREEN);
			}
			if(e.getSource() == green2)
			{
				car2.setColor(Color.GREEN);
			}
			if(e.getSource() == green3) 
			{
				car3.setColor(Color.GREEN);
			}
		}
	}
	class JbtConfirm implements ActionListener
	{
		public void actionPerformed(ActionEvent e)//設定按鈕的意義
		{
			if(e.getSource() == go1)
			{
				if(Integer.parseInt(speed1.getText()) <= 100)
				{
					car1.setSpeed(101 - Integer.parseInt(speed1.getText()));
				}
			}
			if(e.getSource() == go2) 
			{
				if(Integer.parseInt(speed1.getText()) <= 100)
				{
					car2.setSpeed(101 - Integer.parseInt(speed2.getText()));
				}
			}
			if(e.getSource() == go3)
			{
				if(Integer.parseInt(speed1.getText()) <= 100)
				{
					car3.setSpeed(101 - Integer.parseInt(speed3.getText()));
				}
			}
		}
	}
}//完成!
