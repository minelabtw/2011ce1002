package a11.s100502505;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

public class RaceCar extends JPanel{
	private int x;//宣告
	private int y;
	private int v;
	private Color color;
	private Timer timer;
	
	public RaceCar() 
	{
		color = new Color(0, 0, 0);//黑色
		x=0;
		y=30;
		v=0;
		timer = new Timer(0, new TimerListener());
		timer.start();
	}
	public void setSpeed(int delay)//設定速度 
	{
		timer.setDelay(delay);
		if(delay == 0) 
		{
			v=0;
		}
		else 
		{
			v=10;
		}
	}
	public void setColor(Color c)//設定顏色 
	{
		color = new Color(c.getRGB());
	}
	private class TimerListener implements ActionListener 
	{
		public void actionPerformed(ActionEvent e)
		{
			repaint();
		}
	}
	protected void paintComponent(Graphics g)//畫車子
	{
		super.paintComponent(g);
		int[] x1 = new int[7];
		int[] y1 = new int[7];
		x1[0] = x + 20;
		x1[1] = x1[6] = x + 30;
		y1[0] = y1[1] = y - 30;
		x1[2] = x1[5] = x + 10;
		x1[3] =  x + 40;
		y1[2] =  y1[3] = y - 20;
		x1[4] = x;
		y1[4] = y - 20;
		y1[5] = y1[6] = y - 10;
		Color holdColor = new Color(color.getRGB());
		g.setColor(color);
		g.fillOval(x1[5], y1[5], 10, 10);
		g.fillOval(x1[6], y1[6], 10, 10);
		color.brighter();
		g.setColor(color);
		g.fillRect(x1[4], y1[4], 50, 10);
		color.brighter();
		g.setColor(color);
		Polygon top = new Polygon();
		top.addPoint(x1[0], y1[0]);
		top.addPoint(x1[1], y1[1]);
		top.addPoint(x1[3], y1[3]);
		top.addPoint(x1[2], y1[2]);
		g.fillPolygon(top);
		if(x > getWidth()) 
		{
			x = 0 - 50;
		}
		x += v;
		color = new Color(holdColor.getRGB());
	}
}//完成!
