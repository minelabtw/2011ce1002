package a11.s100502027;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class RaceCar extends JPanel{
	Timer timer = new Timer(100,new TimerListener());
	private int ShapeCode = 0 ; 
	private int s = 0 ;
	private Color CarC = new Color(0,0,0);
	public RaceCar(){   // At start  , to start the timer to let car move
		timer.start();
	}
	public void setGoal(){  //when method be called , seat ++ to letcar move
		if(s<getWidth()){
			s+=1;
		}
		else{
			s=0;
		}
	}
	public void setSpeed(int speed){     // get the input to cange the speed
		timer.stop();
		timer.setDelay(speed);
		timer.start();
	}
	public void setColor(Color c){    //when click color button  ,change the color which mthod used
		CarC = c ;
	}
	public void setShapeCode(int code){   // when chose the shape , change the code to change shape
		ShapeCode = code ;
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		int XL=getWidth();
		int YL=getHeight();
		g.setColor(CarC);
		
		if(ShapeCode==0){   //basic car
			int px1[]={20+s,40+s,60+s,80+s};
			int py1[]={YL-35,YL-55,YL-55,YL-35};
			int px2[]={0+s,0+s,100+s,100+s};
			int py2[]={YL-15,YL-35,YL-35,YL-15};
			g.fillPolygon(px1, py1, 4);
			g.fillPolygon(px2, py2, 4);
			g.fillOval(20+s, YL-20, 20, 20);
			g.fillOval(60+s, YL-20, 20, 20);
		}
		else if(ShapeCode==1){// long long car
			int px1[]={0+s,0+s,60+s,100+s,120+s,200+s,220+s};
			int py1[]={YL-15,YL-35,YL-55,YL-55,YL-35,YL-35,YL-15};
			g.fillPolygon(px1,py1,7);
			g.fillOval(40+s, YL-20, 20, 20);
			g.fillOval(180+s, YL-20, 20, 20);
		}
		else if(ShapeCode==2){  // thin  arrow
			int px1[]={0+s,  20+s, 0+s,  50+s, 70+s, 150+s,130+s,220+s,130+s,150+s,70+s, 50+s};
			int py1[]={YL-60,YL-70,YL-80,YL-80,YL-75,YL-75,YL-80,YL-70,YL-60,YL-65,YL-65,YL-60};
			g.fillPolygon(px1,py1,12);
		}
		else if(ShapeCode==3){  //hard shape  
			int px1[]={0+s,30+s,15+s,40+s,90+s,160+s,300+s,300+s,250+s,250+s,220+s,220+s,180+s,180+s,160+s,40+s,35+s};
			int py1[]={YL-150,YL-100,YL-85,YL-100,YL-100,YL-60,YL-60,YL-80,YL-120,YL-130,YL-130,YL-140,YL-140,YL-135,YL-120,YL-125,YL-150};
			g.fillPolygon(px1,py1,17);
			g.drawLine(200+s,YL-135,200+s,YL-150);
			g.drawLine(200+s+(int)(40*Math.cos(s/50)),YL-150,200+s-(int)(40*Math.cos(s/50)),YL-150);
			g.fillOval(170+s, YL-62, 10, 10);
			g.fillOval(240+s, YL-62, 10, 10);
		}
		else{
			
		}
	}
	private class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			setGoal();
			repaint();
			
		}
	}
}
