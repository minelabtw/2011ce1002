package a11.s100502027;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class A111 extends JApplet implements ActionListener {
	private JLabel JLcar1 = new JLabel("Speed1");
	private JLabel JLcar2 = new JLabel("Speed2");
	private JLabel JLcar3 = new JLabel("Speed3");
	private JTextField JTspeed1 = new JTextField(5);
	private JTextField JTspeed2 = new JTextField(5);
	private JTextField JTspeed3 = new JTextField(5);
	private JButton JBfresh = new JButton("Fresh");
	private JRadioButton JRBblack1 = new JRadioButton("Black");
	private JRadioButton JRBblack2 = new JRadioButton("Black");
	private JRadioButton JRBblack3 = new JRadioButton("Black");
	private JRadioButton JRBred1 = new JRadioButton("Red");
	private JRadioButton JRBred2 = new JRadioButton("Red");
	private JRadioButton JRBred3 = new JRadioButton("Red");
	private JRadioButton JRBblue1 = new JRadioButton("Blue");
	private JRadioButton JRBblue2 = new JRadioButton("Blue");
	private JRadioButton JRBblue3 = new JRadioButton("Blue");
	private JRadioButton JRByellow1 = new JRadioButton("Yellow");
	private JRadioButton JRByellow2 = new JRadioButton("Yellow");
	private JRadioButton JRByellow3 = new JRadioButton("Yellow");
	private JRadioButton JRBgreen1 = new JRadioButton("Green");
	private JRadioButton JRBgreen2 = new JRadioButton("Green");
	private JRadioButton JRBgreen3 = new JRadioButton("Green");
	private RaceCar Car1 = new RaceCar();
	private RaceCar Car2 = new RaceCar();
	private RaceCar Car3 = new RaceCar();
	private String[] CarShapes = {"Car","Limousine","Arrow","helicopter"};
	private JComboBox JCshape1 = new JComboBox(CarShapes);
	private JComboBox JCshape2 = new JComboBox(CarShapes);
	private JComboBox JCshape3 = new JComboBox(CarShapes);
	public A111(){
		this.setSize(800,640);
		setLayout(new BorderLayout(5,5));
		Border linesg = new LineBorder(Color.GRAY,1);
		
		JPanel TopPanel = new JPanel();    //The Panel have JLabel .JTextField.JButton on the top
		TopPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		TopPanel.add(JLcar1);
		TopPanel.add(JTspeed1);
		TopPanel.add(JLcar2);
		TopPanel.add(JTspeed2);
		TopPanel.add(JLcar3);
		TopPanel.add(JTspeed3);
		TopPanel.add(JBfresh);
		
		JPanel JRBPanel1 = new JPanel();   //First color group panel
		JRBPanel1.setLayout(new GridLayout(5,1));
		JRBPanel1.add(JRBblack1);
		JRBPanel1.add(JRBred1);
		JRBPanel1.add(JRBblue1);
		JRBPanel1.add(JRByellow1);
		JRBPanel1.add(JRBgreen1);
		JRBPanel1.setBorder(linesg);
		
		JPanel JRBPanel2 = new JPanel();  //Second color group panel
		JRBPanel2.setLayout(new GridLayout(5,1));
		JRBPanel2.add(JRBblack2);
		JRBPanel2.add(JRBred2);
		JRBPanel2.add(JRBblue2);
		JRBPanel2.add(JRByellow2);
		JRBPanel2.add(JRBgreen2);
		JRBPanel2.setBorder(linesg);
		
		JPanel JRBPanel3 = new JPanel();  //Threest color group panel
		JRBPanel3.setLayout(new GridLayout(5,1));
		JRBPanel3.add(JRBblack3);
		JRBPanel3.add(JRBred3);
		JRBPanel3.add(JRBblue3);
		JRBPanel3.add(JRByellow3);
		JRBPanel3.add(JRBgreen3);
		JRBPanel3.setBorder(linesg);
		
		JPanel JRBandCar1 = new JPanel();    // add JRB and car to a Panel
		JRBandCar1.setLayout(new BorderLayout());
		JRBandCar1.add(JRBPanel1,BorderLayout.WEST);
		JRBandCar1.add(Car1,BorderLayout.CENTER);
		JRBandCar1.add(JCshape1,BorderLayout.EAST);
		JRBandCar1.setBorder(linesg);
		
		JPanel JRBandCar2 = new JPanel();   // add JRB and car to a Panel
		JRBandCar2.setLayout(new BorderLayout());
		JRBandCar2.add(JRBPanel2,BorderLayout.WEST);
		JRBandCar2.add(Car2,BorderLayout.CENTER);
		JRBandCar2.add(JCshape2,BorderLayout.EAST);
		JRBandCar2.setBorder(linesg);
		
		JPanel JRBandCar3 = new JPanel();    // add JRB and car to a Panel
		JRBandCar3.setLayout(new BorderLayout());
		JRBandCar3.add(JRBPanel3,BorderLayout.WEST);
		JRBandCar3.add(Car3,BorderLayout.CENTER);
		JRBandCar3.add(JCshape3,BorderLayout.EAST);
		JRBandCar3.setBorder(linesg);
		
		JPanel AllPanel = new JPanel();
		AllPanel.setLayout(new GridLayout(3,1));
		AllPanel.add(JRBandCar1);
		AllPanel.add(JRBandCar2);
		AllPanel.add(JRBandCar3);
		AllPanel.setBorder(linesg);
		
		
		ButtonGroup ColorGroup1 = new ButtonGroup();  //let the five button into group
		ColorGroup1.add(JRBblack1);
		ColorGroup1.add(JRBred1);
		ColorGroup1.add(JRBblue1);
		ColorGroup1.add(JRByellow1);
		ColorGroup1.add(JRBgreen1);
		
		ButtonGroup ColorGroup2 = new ButtonGroup();  //let the five button into group
		ColorGroup2.add(JRBblack2);
		ColorGroup2.add(JRBred2);
		ColorGroup2.add(JRBblue2);
		ColorGroup2.add(JRByellow2);
		ColorGroup2.add(JRBgreen2);
		
		ButtonGroup ColorGroup3 = new ButtonGroup();   //let the five button into group
		ColorGroup3.add(JRBblack3);
		ColorGroup3.add(JRBred3);
		ColorGroup3.add(JRBblue3);
		ColorGroup3.add(JRByellow3);
		ColorGroup3.add(JRBgreen3);
		
		JBfresh.addActionListener(this);
		JRBblack1.addActionListener(this);
		JRBblack2.addActionListener(this);
		JRBblack3.addActionListener(this);
		JRBred1.addActionListener(this);
		JRBred2.addActionListener(this);
		JRBred3.addActionListener(this);
		JRBblue1.addActionListener(this);
		JRBblue2.addActionListener(this);
		JRBblue3.addActionListener(this);
		JRByellow1.addActionListener(this);
		JRByellow2.addActionListener(this);
		JRByellow3.addActionListener(this);
		JRBgreen1.addActionListener(this);
		JRBgreen2.addActionListener(this);
		JRBgreen3.addActionListener(this);
		JCshape1.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e){
				Car1.setShapeCode(JCshape1.getSelectedIndex());
			}
		});
		JCshape2.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e){
				Car2.setShapeCode(JCshape2.getSelectedIndex());
			}
		});
		JCshape3.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e){
				Car3.setShapeCode(JCshape3.getSelectedIndex());
			}
		});
		
		add(TopPanel,BorderLayout.NORTH);
		add(AllPanel,BorderLayout.CENTER);
	}
	public void actionPerformed(ActionEvent e){
		if(e.getSource()==JBfresh){
			if(JTspeed1.getText()!=""){
				Car1.setSpeed(Integer.parseInt(JTspeed1.getText()));
			}
			if(JTspeed2.getText()!=""){
				Car2.setSpeed(Integer.parseInt(JTspeed2.getText()));
			}
			if(JTspeed3.getText()!=""){
				Car3.setSpeed(Integer.parseInt(JTspeed3.getText()));
			}
		}
		if(e.getSource()==JRBblack1){
			Car1.setColor(Color.BLACK);
		}
		if(e.getSource()==JRBblack2){
			Car2.setColor(Color.BLACK);
		}
		if(e.getSource()==JRBblack3){
			Car3.setColor(Color.BLACK);
		}
		if(e.getSource()==JRBred1){
			Car1.setColor(Color.RED);
		}
		if(e.getSource()==JRBred2){
			Car2.setColor(Color.RED);
		}
		if(e.getSource()==JRBred3){
			Car3.setColor(Color.RED);
		}
		if(e.getSource()==JRBblue1){
			Car1.setColor(Color.BLUE);
		}
		if(e.getSource()==JRBblue2){
			Car2.setColor(Color.BLUE);
		}
		if(e.getSource()==JRBblue3){
			Car3.setColor(Color.BLUE);
		}
		if(e.getSource()==JRByellow1){
			Car1.setColor(Color.YELLOW);
		}
		if(e.getSource()==JRByellow2){
			Car2.setColor(Color.YELLOW);
		}
		if(e.getSource()==JRByellow3){
			Car3.setColor(Color.YELLOW);
		}
		if(e.getSource()==JRBgreen1){
			Car1.setColor(Color.GREEN);
		}
		if(e.getSource()==JRBgreen2){
			Car2.setColor(Color.GREEN);
		}
		if(e.getSource()==JRBgreen3){
			Car3.setColor(Color.GREEN);
		}
	}
}
