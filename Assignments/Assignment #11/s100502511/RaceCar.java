package a11.s100502511;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class RaceCar extends JPanel {
	Color color = Color.BLACK;
	Timer timer = new Timer(50, new TimerListener());

	public RaceCar() {
		timer.start();
	}

	int xCenter = getWidth() / 2;

	protected void paintComponent(Graphics g) {  // 畫出車子的圖案
		super.paintComponent(g);
		g.setColor(color);
		int yCenter = getHeight() / 2;
		int radius = (int) (Math.min(getWidth(), getHeight()) * 0.04);
		g.drawArc(xCenter - 10, yCenter + 20, radius, radius, 0, 360);
		g.fillArc(xCenter - 10, yCenter + 20, radius, radius, 0, 360);
		g.drawArc(xCenter + 20, yCenter + 20, radius, radius, 0, 360);
		g.fillArc(xCenter + 20, yCenter + 20, radius, radius, 0, 360);
		g.drawLine(xCenter - 20, yCenter, xCenter + 40, yCenter);
		g.drawLine(xCenter - 20, yCenter + 20, xCenter + 40, yCenter + 20);
		g.drawLine(xCenter - 20, yCenter, xCenter - 20, yCenter + 20);
		g.drawLine(xCenter + 40, yCenter, xCenter + 40, yCenter + 20);
		g.drawLine(xCenter - 10, yCenter, xCenter, yCenter - 20);
		g.drawLine(xCenter + 30, yCenter, xCenter + 20, yCenter - 20);
		g.drawLine(xCenter, yCenter - 20, xCenter + 20, yCenter - 20);

		if (xCenter + 20 > getWidth()) {
			xCenter = 10;
		} else
			xCenter += 10;
	}

	public void setColor(int number) {  // 設定顏色
		if (number == 1) {
			color = Color.RED;
		} else if (number == 2) {
			color = Color.GREEN;
		} else if (number == 3) {
			color = Color.BLUE;
		}
	}

	public void setspeed(int number) {  // 設定速度
		if (number == 0) {
			timer.stop();
			timer = new Timer(50, new TimerListener());
			timer.start();
		}
		if (number == 1) {
			timer.stop();
			timer = new Timer(100, new TimerListener());
			timer.start();
		}
		if (number == 2) {
			timer.stop();
			timer = new Timer(1000, new TimerListener());
			timer.start();
		}
	}

	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			repaint();
		}

	}

}
