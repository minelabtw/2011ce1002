package a11.s100502511;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class A111 extends JApplet {
	RaceCar CarA = new RaceCar();
	RaceCar CarB = new RaceCar();
	RaceCar CarC = new RaceCar();
	JRadioButton jrbRed, jrbGreen, jrbBlue; // 建立顏色的JRadorButton
	JLabel L1 = new JLabel("CarA");
	JLabel L2 = new JLabel("CarB");
	JLabel L3 = new JLabel("CarC");
	Font wordFont = new Font("TimesRoman", Font.BOLD, 20);
	JPanel jpRadioButtonCAR1 = new JPanel(new GridLayout(4, 1));
	JPanel jpRadioButtonCAR2 = new JPanel(new GridLayout(4, 1));
	JPanel jpRadioButtonCAR3 = new JPanel(new GridLayout(4, 1));
	JPanel speedbox1 = new JPanel(new GridLayout(1, 2));
	JPanel speedbox2 = new JPanel(new GridLayout(1, 2));
	JPanel speedbox3 = new JPanel(new GridLayout(1, 2));
	String[] speednum1 = { "50", "100", "1000" }; // 建立速度的JComboBox
	JComboBox jcb1 = new JComboBox(speednum1);
	String[] speednum2 = { "50", "100", "1000" };
	JComboBox jcb2 = new JComboBox(speednum2);
	String[] speednum3 = { "50", "100", "1000" };
	JComboBox jcb3 = new JComboBox(speednum3);
	JPanel CAR1 = new JPanel(new GridLayout(1, 4));
	JPanel CAR2 = new JPanel(new GridLayout(1, 4));
	JPanel CAR3 = new JPanel(new GridLayout(1, 4));
	JPanel All = new JPanel(new GridLayout(3, 1));

	public A111() {
		L1.setFont(wordFont);
		jpRadioButtonCAR1.add(jrbRed = new JRadioButton("Red"));
		jpRadioButtonCAR1.add(jrbGreen = new JRadioButton("Green"));
		jpRadioButtonCAR1.add(jrbBlue = new JRadioButton("Blue"));
		jrbRed.addActionListener(new ActionListener() { // 設定顏色
			public void actionPerformed(ActionEvent e) {
				CarA.setColor(1);
			}
		});
		jrbGreen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CarA.setColor(2);
			}
		});
		jrbBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CarA.setColor(3);
			}
		});
		ButtonGroup groupone = new ButtonGroup();
		groupone.add(jrbRed);
		groupone.add(jrbGreen);
		groupone.add(jrbBlue);
		jcb1.setForeground(Color.red);
		jcb1.setBackground(Color.white);
		jcb1.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				CarA.setspeed(jcb1.getSelectedIndex());
			}
		});
		speedbox1.add(new JLabel("speed"));
		speedbox1.add(jcb1);

		L2.setFont(wordFont);
		jpRadioButtonCAR2.add(jrbRed = new JRadioButton("Red"));
		jpRadioButtonCAR2.add(jrbGreen = new JRadioButton("Green"));
		jpRadioButtonCAR2.add(jrbBlue = new JRadioButton("Blue"));
		jrbRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CarB.setColor(1);
			}
		});
		jrbGreen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CarB.setColor(2);
			}
		});
		jrbBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CarB.setColor(3);
			}
		});
		ButtonGroup grouptwo = new ButtonGroup();
		grouptwo.add(jrbRed);
		grouptwo.add(jrbGreen);
		grouptwo.add(jrbBlue);
		jcb2.setForeground(Color.red);
		jcb2.setBackground(Color.white);
		jcb2.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				CarB.setspeed(jcb2.getSelectedIndex());
			}
		});
		speedbox2.add(new JLabel("speed"));
		speedbox2.add(jcb2);

		L3.setFont(wordFont);
		jpRadioButtonCAR3.add(jrbRed = new JRadioButton("Red"));
		jpRadioButtonCAR3.add(jrbGreen = new JRadioButton("Green"));
		jpRadioButtonCAR3.add(jrbBlue = new JRadioButton("Blue"));
		jrbRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CarC.setColor(1);
			}
		});
		jrbGreen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CarC.setColor(2);
			}
		});
		jrbBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CarC.setColor(3);
			}
		});
		ButtonGroup groupthree = new ButtonGroup();
		groupthree.add(jrbRed);
		groupthree.add(jrbGreen);
		groupthree.add(jrbBlue);
		jcb3.setForeground(Color.red);
		jcb3.setBackground(Color.white);
		jcb3.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				CarC.setspeed(jcb3.getSelectedIndex());
			}
		});
		speedbox3.add(new JLabel("speed"));
		speedbox3.add(jcb3);

		CAR1.add(L1);
		CAR2.add(L2);
		CAR3.add(L3);

		CAR1.add(jpRadioButtonCAR1, BorderLayout.WEST);
		CAR2.add(jpRadioButtonCAR2, BorderLayout.WEST);
		CAR3.add(jpRadioButtonCAR3, BorderLayout.WEST);

		CAR1.add(CarA, BorderLayout.CENTER);
		CAR2.add(CarB, BorderLayout.CENTER);
		CAR3.add(CarC, BorderLayout.CENTER);

		CAR1.add(speedbox1, BorderLayout.EAST);
		CAR2.add(speedbox2, BorderLayout.EAST);
		CAR3.add(speedbox3, BorderLayout.EAST);

		All.add(CAR1);
		All.add(CAR2);
		All.add(CAR3);
		add(All);
	}

}
