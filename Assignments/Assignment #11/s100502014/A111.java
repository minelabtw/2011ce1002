package a11.s100502014;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class A111 extends JApplet {
	public A111() {
		RaceCarPanel r1 = new RaceCarPanel();
		RaceCarPanel r2 = new RaceCarPanel();
		RaceCarPanel r3 = new RaceCarPanel();
		setLayout(new GridLayout(3,1));
		add(r1);
		add(r2);
		add(r3);
	}
}