package a11.s100502014;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class RaceCar extends JPanel {
	private int speed, type;
	private Timer timer;
	private int t;
	private Color color;
	
	public RaceCar() {
		t=0;
		speed = 50;
		timer = new Timer(speed, new TimeListener());
		type = 0;
		color = Color.BLACK;
		timer.start();
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(color);
		
		//car
		if(type==0) {
			g.drawRect(t+20,100,60,40);
			g.fillRect(t,140,100,30);
			g.drawOval(t+10,170,30,30);
			g.drawOval(t+60,170,30,30);
		}
		
		//ufo
		else if(type==1) {
			g.drawOval(t+17,(int)(Math.sin(t/20)*10)+117, 36, 36);
			g.fillOval(t,(int)(Math.sin(t/20)*10)+135, 70, 35);
		}
		
		//star
		else {
			Polygon polygon = new Polygon();
			int[] xTemp = new int[5];
			int[] yTemp = new int[5];
			double angle = 2*Math.PI/5;
			
			//first point about origin
			xTemp[0] = (int)(Math.cos(t/1.02)*50);
			yTemp[0] = (int)(Math.sin((-1)*t/1.02)*50);
			
			//other point about origin
			for(int i=1;i<5;i++) {
				xTemp[i] = (int)(xTemp[0]*Math.cos(i*angle)-yTemp[0]*Math.sin(i*angle));
				yTemp[i] = (int)(xTemp[0]*Math.sin(i*angle)+yTemp[0]*Math.cos(i*angle));
			}
			
			//translate to center
			for(int i=0;i<5;i++) {
				xTemp[i] += t;
				yTemp[i] += 100;
			}
			polygon.addPoint(xTemp[0], yTemp[0]);
			polygon.addPoint(xTemp[2], yTemp[2]);
			polygon.addPoint(xTemp[4], yTemp[4]);
			polygon.addPoint(xTemp[1], yTemp[1]);
			polygon.addPoint(xTemp[3], yTemp[3]);
			g.fillPolygon(polygon);
		}
		g.setColor(Color.BLACK);
		g.drawLine(0,200,600,200);
	}
	class TimeListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			t+=5;
			t%=600;
			repaint();
		}
	}
	public void faster() {
		timer.setDelay(speed>10?speed-=10:10);
	}
	public void slower() {
		timer.setDelay(speed<50?speed+=10:50);
	}
	public void setColor(Color c) {
		color = c;
	}
	public void setType(int x) {
		type = x;
	}
}
