package a11.s100502014;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class RaceCarPanel extends JPanel {
	private RaceCar raceCar = new RaceCar();
	private JButton minus = new JButton("-");
	private JButton plus = new JButton("+");
	public JPanel buttonPanel = new JPanel();
	public JRadioButton rRed = new JRadioButton("Red");
	public JRadioButton rBlue = new JRadioButton("Blue");
	public JRadioButton rGreen = new JRadioButton("Green");
	private ButtonGroup bg = new ButtonGroup();
	private String[] str = {"Car","UFO","Star"};
	private JComboBox cb = new JComboBox(str);
	
	public RaceCarPanel() {
		setLayout(new GridLayout(1,2));
		
		add(raceCar);
		
		buttonPanel.setLayout(new GridLayout(1,5));
		
		//button
		buttonPanel.add(minus);
		buttonPanel.add(plus);
		
		//radio button
		bg.add(rRed);
		bg.add(rBlue);
		bg.add(rGreen);
		buttonPanel.add(rRed);
		buttonPanel.add(rBlue);
		buttonPanel.add(rGreen);
		
		//combo box
		buttonPanel.add(cb);
		
		add(buttonPanel);
		
		//listener
		minus.addActionListener(new ButtonListener());
		plus.addActionListener(new ButtonListener());
		rRed.addItemListener(new RadioButtonListener());
		rBlue.addItemListener(new RadioButtonListener());
		rGreen.addItemListener(new RadioButtonListener());
		cb.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				raceCar.setType(cb.getSelectedIndex());
			}
		});
	}
	class ButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if(e.getSource()==minus)
				raceCar.slower();
			if(e.getSource()==plus)
				raceCar.faster();
		}
	}
	class RadioButtonListener implements ItemListener {
		public void itemStateChanged(ItemEvent e)
		{ 
			if(e.getStateChange()==e.SELECTED)	{ 
				if(e.getSource()==rRed)
					raceCar.setColor(Color.RED);
				if(e.getSource()==rBlue)
					raceCar.setColor(Color.BLUE);
				if(e.getSource()==rGreen)
					raceCar.setColor(Color.GREEN);
			}
		}
	}
}
