package a11.s100502513;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class RaceCar extends JPanel {
	private int x = 0; // 設置車的初始X座標
	private Color c = Color.RED; // 車子初始顏色:紅
	private Timer timer = new Timer(50, new TimerListener());

	public RaceCar() {
		timer.start(); 
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		Polygon polygon = new Polygon();
		int y = getHeight() - 30;
		int[] px = { x + 20, x + 30, x + 40, x + 10 };
		int[] py = { y, y, y + 10, y + 10 };

		for (int i = 0; i < 4; i++)
			polygon.addPoint(px[i], py[i]);
		g.setColor(c); // 車子顏色
		g.fillPolygon(polygon); // 車頂
		g.fillRect(x, y + 10, 50, 10); // 車身
		g.setColor(Color.BLACK); // 輪胎顏色
		g.fillOval(x + 10, y + 17, 10, 10); // 輪胎
		g.fillOval(x + 30, y + 17, 10, 10);
		g.drawLine(0, getHeight() - 5, getWidth(), getHeight() - 5);
		if (x > getWidth())
			x = -40;
		x += 5; // 重畫X距離+5

	}

	public void setcarcolor(String s) { // 設定車子顏色
		switch (s) {
		case "Red":
			c = Color.RED;
			repaint();
			break;
		case "Blue":
			c = Color.BLUE;
			repaint();
			break;
		case "Yellow":
			c = Color.YELLOW;
			repaint();
			break;
		}
	}

	public void setspeed(String s) { // 設定車子速度(DELAY)
		timer.setDelay((int) Double.parseDouble(s));
	}

	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			repaint(); // 重畫
		}
	}

}
