package a11.s100502513;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class A111 extends JApplet {
	
	public A111() {
	    car c1 = new car();
		car c2 = new car();
		car c3 = new car();
		setLayout(new GridLayout(3, 1));
		add(c1);
		add(c2);
		add(c3);		
	}

	public class car extends JApplet{
		private JPanel radioPanel = new JPanel(); // 顏色欄位
		private JRadioButton redButton; // 顏色按鈕
		private JRadioButton blueButton;
		private JRadioButton yellowButton;
		private ButtonGroup group = new ButtonGroup();
		private RaceCar car = new RaceCar(); // 車子
		private JPanel speedp = new JPanel(); // 改速度欄位
		private JTextField speed = new JTextField();
		private JLabel speedla = new JLabel("Speed(delay)");
		public car() {
			radioPanel.setLayout(new GridLayout(1, 3)); // 顏色按鈕欄位
			radioPanel.add(redButton = new JRadioButton("Red"));
			radioPanel.add(blueButton = new JRadioButton("Blue"));
			radioPanel.add(yellowButton = new JRadioButton("Yellow"));
			group.add(redButton);
			group.add(blueButton);
			group.add(yellowButton);
			speedla.setFont(new Font("Cambria", Font.BOLD + Font.ITALIC, 40));
			speedp.setLayout(new BorderLayout()); // 改變速度欄位
			speedp.add(speedla, BorderLayout.NORTH);
			speedp.add(speed, BorderLayout.CENTER);

			add(radioPanel, BorderLayout.WEST); // 顏色+車+速度
			add(car, BorderLayout.CENTER);
			add(speedp, BorderLayout.EAST);

			redButton.addActionListener(new ActionListener() {  //紅色按鈕作用
				public void actionPerformed(ActionEvent e) {
					car.setcarcolor("Red");   //改車子的顏色為紅色
				}
			});
			blueButton.addActionListener(new ActionListener() {  //藍色按鈕作用
				public void actionPerformed(ActionEvent e) {
					car.setcarcolor("Blue");   //改車子的顏色為藍色
				}
			});
			yellowButton.addActionListener(new ActionListener() {  //黃色按鈕作用
				public void actionPerformed(ActionEvent e) {
					car.setcarcolor("Yellow");   //改車子的顏色為黃色
				}
			});
			speed.addActionListener(new ActionListener() {  //速度
				public void actionPerformed(ActionEvent e) {
					car.setspeed(speed.getText());  //改變車子速度
				}
			});
		}
	}
}
