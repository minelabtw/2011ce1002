package a11.s100502030;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class RaceCar extends JPanel {
	private int carXi = 0;// 初始X值
	private int carYi;
	private double an = 0;
	private int dx;//x移動速度
	private int dy;//y移動速度
	private int carStyle = 0;
	private Timer t = new Timer(50, new TimerListener());

	public RaceCar() {
		t.start();
	}

	private Color c = Color.RED;

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		switch (carStyle) {
			case 0:// draw car
				carYi = getHeight() - 30;
	
				Polygon p1 = new Polygon();
				Polygon p2 = new Polygon();
	
				p1.addPoint(carXi + 10, carYi);
				p1.addPoint(carXi + 20, carYi - 10);
				p1.addPoint(carXi + 30, carYi - 10);
				p1.addPoint(carXi + 40, carYi);
	
				p2.addPoint(carXi, carYi);
				p2.addPoint(carXi + 50, carYi);
				p2.addPoint(carXi + 50, carYi + 10);
				p2.addPoint(carXi, carYi + 10);
	
				g.setColor(c);
				g.fillPolygon(p1);
				g.fillPolygon(p2);
				g.setColor(Color.BLACK);
				g.fillOval(carXi + 10, carYi + 10, 10, 10);
				g.fillOval(carXi + 30, carYi + 10, 10, 10);
	
				if (carXi <= 0)
					dx = 2;
				if (carXi >= getWidth() - 50)
					dx = -2;
				carXi += dx;
				break;
			case 1:// draw ball
				g.setColor(c);
				g.fillOval(carXi, carYi, 30, 30);
				if (carXi <= 0)
					dx = 2;
				if (carXi >= getWidth() - 30)
					dx = -2;
				if (carYi <= 0)
					dy = 10;
				if (carYi >= getHeight() - 30)
					dy = -10;
	
				carXi += dx;
				carYi += dy;
				break;
			case 2:// dwaw UFO
				g.setColor(c);
				Polygon p = new Polygon();
				p.addPoint(carXi + 10, carYi + 10);
				p.addPoint(carXi + 30, carYi + 10);
				p.addPoint(carXi + 40, carYi + 20);
				p.addPoint(carXi, carYi + 20);
				g.fillPolygon(p);
				g.fillOval(carXi + 10, carYi, 20, 20);
				g.setColor(Color.YELLOW);
				g.fillOval(carXi + 2, carYi + 22, 10, 10);
				g.fillOval(carXi + 15, carYi + 22, 10, 10);
				g.fillOval(carXi + 28, carYi + 22, 10, 10);
				if (carXi <= 0)
					dx = 2;
				if (carXi >= getWidth() - 30)
					dx = -2;
				carXi += dx;
				an = an + Math.PI / 12;
				carYi = (int) (getHeight() / 2 + 15 * Math.sin(an));
				break;
			case 3:// draw dart
				int xCender = carXi + 15;
				int yCender = carYi;
	
				g.setColor(c);
				Polygon p3 = new Polygon();
				p3.addPoint(xCender + (int) (15 * Math.sin(-Math.PI / 2 + an)),
						yCender - (int) (15 * Math.cos(-Math.PI / 2 + an)));
				p3.addPoint(
						xCender
								+ (int) (3 * Math.sqrt(2) * Math.sin(-Math.PI / 4
										+ an)),
						yCender
								- (int) (3 * Math.sqrt(2) * Math.cos(-Math.PI / 4
										+ an)));
				p3.addPoint(xCender + (int) (15 * Math.sin(an)), yCender
						- (int) (15 * Math.cos(an)));
				p3.addPoint(
						xCender
								+ (int) (3 * Math.sqrt(2) * Math.sin(an + Math.PI
										/ 4)),
						yCender
								- (int) (3 * Math.sqrt(2) * Math.cos(an + Math.PI
										/ 4)));
				p3.addPoint(xCender + (int) (15 * Math.sin(an + Math.PI / 2)),
						yCender - (int) (15 * Math.cos(an + Math.PI / 2)));
				p3.addPoint(
						xCender
								+ (int) (3 * Math.sqrt(2) * Math.sin(an + Math.PI
										* 3 / 4)),
						yCender
								- (int) (3 * Math.sqrt(2) * Math.cos(an + Math.PI
										* 3 / 4)));
				p3.addPoint(xCender + (int) (15 * Math.sin(an + Math.PI)), yCender
						- (int) (15 * Math.cos(an + Math.PI)));
				p3.addPoint(
						xCender
								+ (int) (3 * Math.sqrt(2) * Math.sin(an + Math.PI
										* 5 / 4)),
						yCender
								- (int) (3 * Math.sqrt(2) * Math.cos(an + Math.PI
										* 5 / 4)));
				g.fillPolygon(p3);
				g.setColor(Color.BLACK);
				g.fillOval(carXi + 12, carYi - 15 + 12, 6, 6);
				if (carXi <= 0)
					dx = 2;
				if (carXi >= getWidth() - 30)
					dx = -2;
				carXi += dx;
				an += Math.PI / 6;// 旋轉速度
				break;
		}

	}

	public void setStyle(int s) {
		carStyle = s;
	}// 改變圖案

	public void setSpeed(int s) {
		t.setDelay(s);
	}// set delay

	public void setCarColor(Color c) {
		this.c = c;
	}// set object color

	private class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {// time event
			repaint();
		}
	}
}
