package a11.s100502030;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class CarWork extends JPanel {
	private JRadioButton rb_r = new JRadioButton("Red");
	private JRadioButton rb_g = new JRadioButton("Green");
	private JRadioButton rb_b = new JRadioButton("Blue");
	protected RaceCar carPanel = new RaceCar();
	private JButton setStyle = new JButton("Car");
	private int style = 0;

	public CarWork(int s) {
		JPanel rbPanel = new JPanel();
		rbPanel.setLayout(new GridLayout(3, 1));
		rb_r.setForeground(Color.RED);
		rb_g.setForeground(Color.GREEN);
		rb_b.setForeground(Color.BLUE);
		rbPanel.add(rb_r);
		rbPanel.add(rb_g);
		rbPanel.add(rb_b);
		
		//creat a ratio-button group to group three buttons
		ButtonGroup group = new ButtonGroup();
		group.add(rb_r);
		group.add(rb_g);
		group.add(rb_b);
		
		style = s;
		setLayout(new BorderLayout(5, 5));
		add(rbPanel, BorderLayout.WEST);
		add(carPanel, BorderLayout.CENTER);
		add(setStyle, BorderLayout.EAST);
		
		// set object color
		rb_r.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				carPanel.setCarColor(Color.RED);
			}
		});
		rb_g.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				carPanel.setCarColor(Color.GREEN);
			}
		});
		rb_b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				carPanel.setCarColor(Color.BLUE);
			}
		});
		
		setStyle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (setStyle.getText() == "Car") {
					setButton(style);
					carPanel.setStyle(style);
				} else{
					carPanel.setStyle(0);
					setButton(0);
				}
			}
		});
	}

	public void setButton(int x) {
		if (x == 1) {
			setStyle.setText("Ball");
		}
		else if(x==2){
			setStyle.setText("UFO");
		}
		else if(x==3){
			setStyle.setText("dart");
		}
		else
			setStyle.setText("Car");
	}// 設置要變成哪個圖案
}
