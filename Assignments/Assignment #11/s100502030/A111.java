package a11.s100502030;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class A111 extends JApplet{
	private JTextField jTF1 = new JTextField(4);
	private JTextField jTF2 = new JTextField(4);
	private JTextField jTF3 = new JTextField(4);
	private CarWork c1 = new CarWork(1);
	private CarWork c2 = new CarWork(2);
	private CarWork c3 = new CarWork(3);
	
	public A111(){
		// set textfield panel
		JPanel jpTF= new JPanel(new GridLayout(1,3));
		JPanel jpTF1= new JPanel(new BorderLayout(5,0));
		JPanel jpTF2= new JPanel(new BorderLayout(5,0));
		JPanel jpTF3= new JPanel(new BorderLayout(5,0));
		JLabel car1 = new JLabel("car1");
		JLabel car2 = new JLabel("car2");
		JLabel car3 = new JLabel("car3");

		// set carwork panel
		JPanel showcar = new JPanel(new GridLayout(3,1));
		jpTF1.add(car1,BorderLayout.WEST);
		jpTF1.add(jTF1,BorderLayout.CENTER);
		jpTF2.add(car2,BorderLayout.WEST);
		jpTF2.add(jTF2,BorderLayout.CENTER);
		jpTF3.add(car3,BorderLayout.WEST);
		jpTF3.add(jTF3,BorderLayout.CENTER);
		jpTF.add(jpTF1);
		jpTF.add(jpTF2);
		jpTF.add(jpTF3);
		
		showcar.add(c1);
		showcar.add(c2);
		showcar.add(c3);
		
		// text field event to change delay
		jTF1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				c1.carPanel.setSpeed(Integer.parseInt(jTF1.getText()));
			}
		});
		
		jTF2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				c2.carPanel.setSpeed(Integer.parseInt(jTF2.getText()));
			}
		});
		
		jTF3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				c3.carPanel.setSpeed(Integer.parseInt(jTF3.getText()));
			}
		});
		add(jpTF,BorderLayout.NORTH);
		add(showcar,BorderLayout.CENTER);
	}
}
