package a11.s975002502;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class RaceCar extends JPanel {
	// Define constant
	public static final int BLACK = 0;
	public static final int BLUE = 1;
	public static final int GREEN = 2;
	public static final int GRAY = 3;
	public static final int ORANGE = 4;
	public static final int PINK = 5;
	public static final int RED = 6;
	public static final int YELLOW = 7;
	
	public static final int CAR = 0;
	public static final int JUMPINGBALL = 1;
	
	private int delay = 1000;
	private Color carcolor = Color.green;
	private Color wheelcolor = Color.darkGray;
	private int type = CAR;
	
	// Create a timer with delay 1000ms
	private Timer timer = new Timer(delay, new TimerListener());
	
	// Current car position
	private int x = 0;
	private int y = 0;
	
	// Ball radius
	private int radius = 15;
	
	// Increment on car's x-coordinate
	private int dx = 4;
	// Increment on car's y-coordinate
	private int dy = 4;
	
	public RaceCar() {
		timer.start();
	}
	
	private class TimerListener implements ActionListener {
		/** Handle the action event */
		public void actionPerformed(ActionEvent e) {
			repaint();
		}
	}
	
	// Paint car
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		switch(type){
			case CAR:
				int Height = getHeight();
				int Width = getWidth();
				int top_height = 30;
				int wheel_radius = 10;
				int car_length = 180;
			
				// draw the top of car
				Polygon car_top = new Polygon();
				car_top.addPoint(70+x, (Height/2) - top_height);
				car_top.addPoint(110+x, (Height/2) - top_height);
				car_top.addPoint(130+x, Height/2);
				car_top.addPoint(50+x, Height/2);
				g.fillPolygon(car_top);
	
				// draw the center of car
				g.setColor(carcolor);
				Polygon car_center = new Polygon();
				car_center.addPoint(0+x, Height/2);
				car_center.addPoint(180+x, Height/2);
				car_center.addPoint(180+x, (Height/2) + 30);
				car_center.addPoint(0+x, (Height/2) + 30);
				g.fillPolygon(car_center);
	
				// draw the wheels of car
				g.setColor(wheelcolor);
				g.fillOval(35+x, (Height/2) + 30, 2 * wheel_radius, 2 * wheel_radius);
				g.fillOval(125+x, (Height/2) + 30, 2 * wheel_radius, 2 * wheel_radius);
					
				if(x > Width) {
					x = -180;
				}
				x += dx;
				break;
			case JUMPINGBALL:
				g.setColor(carcolor);
				// Check boundaries
				if(x < radius)
					//dx = Math.abs(dx);
				if(x > getWidth()-radius) 
						dx = -Math.abs(dx);
				if(y < radius) 
					dy = Math.abs(dy);
				if(y > getHeight()-radius) 
					dy = -Math.abs(dy);
				
				// Adjust ball position
				x += dx;
				y += dy;
				g.fillOval(x-radius, y-radius, radius*2, radius*2);
				break;
		}
	}
	
	public void suspend() {
		// Suspend timer
		timer.stop();
	}
	
	public void resume() {
		// Resume timer
		timer.start();
	}
	
	public void setDelay(int delay) {
		this.delay = delay;
		timer.setDelay(delay);
	}
	
	public void setColor(int color) {
		switch(color){
			case BLACK:
				this.carcolor = Color.black;
				this.wheelcolor = Color.red;
				break;
			case BLUE:
				this.carcolor = Color.blue;
				this.wheelcolor = Color.yellow;
				break;
			case GREEN:
				this.carcolor = Color.green;
				this.wheelcolor = Color.darkGray;
				break;				
			case GRAY:
				this.carcolor = Color.gray;
				this.wheelcolor = Color.white;
				break;				
			case ORANGE:
				this.carcolor = Color.orange;
				this.wheelcolor = Color.blue;
				break;
			case PINK:
				this.carcolor = Color.pink;
				this.wheelcolor = Color.magenta;
				break;
			case RED:
				this.carcolor = Color.red;
				this.wheelcolor = Color.black;
				break;
			case YELLOW:
				this.carcolor = Color.yellow;
				this.wheelcolor = Color.lightGray;
				break;
		}
		repaint();
	}

	public void setType(int type) {
		this.type = type;
		repaint();
	}
}
