package a11.s975002502;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class A111 extends JApplet {
	private CarControl car1 = new CarControl();
	private CarControl car2 = new CarControl();
	private CarControl car3 = new CarControl();
	
	public A111() {
		JPanel p = new JPanel(new GridLayout(3,1,5,5));
		p.add(car1);
		p.add(car2);
		p.add(car3);
		
		add(p);
	}
	
	// An inner class for a car control
	class CarControl extends JPanel {
		private RaceCar racecar = new RaceCar();
		private JButton jbtSuspend = new JButton("Suspend");
		private JButton jbtResume = new JButton("Resume");
		private JScrollBar jsbDelay = new JScrollBar();
		private JLabel jlType = new JLabel("Car Type");
		private JLabel jlColor = new JLabel("Color Style");
		
		// Declare an array of Strings for car type options and color options
		private String[] typeTitles = {"Car", "JumpingBall"};
		private String[] colorTitles = {"Black", "Blue", "Green", "Gray", 
										"Orange", "Pink", "RED", "Yellow"};
		
		// Create a combo box for selecting car type and car's color
		private JComboBox jcbType = new JComboBox(typeTitles);
		private JComboBox jcbColor = new JComboBox(colorTitles); 

		
		public CarControl() {
			// Set the first color style for car
			setColorDisplay(0);
			
			// Group buttons in a panel
			JPanel buttonPanel = new JPanel();
			buttonPanel.add(jbtSuspend);
			buttonPanel.add(jbtResume);
			
			JPanel westPanel = new JPanel(new GridLayout(4,1,0,0));
			westPanel.add(jlType);
			westPanel.add(jcbType);
			westPanel.add(jlColor);
			westPanel.add(jcbColor);
			
			// Add ball and buttons to the panel
			racecar.setBorder(new javax.swing.border.LineBorder(Color.red));
			jsbDelay.setOrientation(JScrollBar.HORIZONTAL);
			racecar.setDelay(jsbDelay.getMaximum());
			setLayout(new BorderLayout());
			add(jsbDelay, BorderLayout.NORTH);
			add(racecar, BorderLayout.CENTER);
			add(buttonPanel, BorderLayout.SOUTH);
			add(westPanel, BorderLayout.WEST);
		
			// Register listeners
			jbtSuspend.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					racecar.suspend();
				}
			});
			
			jbtResume.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					racecar.resume();
				}
			});
			
			jsbDelay.addAdjustmentListener(new AdjustmentListener() {
				public void adjustmentValueChanged(AdjustmentEvent e) {
					racecar.setDelay(jsbDelay.getMaximum() - e.getValue());
				}
			});
			
			jcbType.addItemListener(new ItemListener() {
				/** Handle item selection */
				public void itemStateChanged(ItemEvent e) {
					setTypeDisplay(jcbType.getSelectedIndex());
				}
			});
			
			jcbColor.addItemListener(new ItemListener() {
				/** Handle item selection */
				public void itemStateChanged(ItemEvent e) {
					setColorDisplay(jcbColor.getSelectedIndex());
				}
			});
			
		}
		
		// Set display car type 
		public void setTypeDisplay(int index) {
			racecar.setType(index);
		}
		
		// Set display color on the car 
		public void setColorDisplay(int index) {
			racecar.setColor(index);
		}
	}
}
