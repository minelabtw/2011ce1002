package a11.s100502503;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

public class RaceCar extends JPanel
{
	protected int xOri = 0;
	protected int yOri = 0;
	protected boolean flag = true;
	
	protected String[] colorString = {"Black", "Red", "Yellow"};
	protected JComboBox colorBx = new JComboBox(colorString);
	public int speedControl = 100; //The variable to control the speed
	
	public Color c;
	
	public Timer timer = new Timer(speedControl, new TimerListener());	
		
	public RaceCar()
	{
		timer.start();
	}
	
	//The variables are used to change the situation of the car
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		 int x[] = {xOri+20, xOri+30, xOri+10, xOri+40, xOri, xOri+50, xOri, xOri+50};
		 int y[] = {yOri+40, yOri+40, yOri+50, yOri+50, yOri+50, yOri+50, yOri+60, yOri+60};
	
		//Draw the shape of the car and fill it
		g.drawLine(x[0], y[0], x[1], y[1]);
		g.drawLine(x[0], y[0], x[2], y[2]);
		g.drawLine(x[1], y[1], x[3], y[3]);
		g.drawLine(x[4], y[4], x[5], y[5]);
		g.drawLine(x[4], y[4], x[6], y[6]);
		g.drawLine(x[5], y[5], x[7], y[7]);
		g.drawLine(x[6], y[6], x[7], y[7]);
		g.drawOval(xOri+10, yOri+60, 10, 10);
		g.drawOval(xOri+30, yOri+60, 10, 10);
		
		g.setColor(c);
		g.fillOval(xOri+30, yOri+60, 10, 10);
		g.fillOval(xOri+10, yOri+60, 10, 10);
		g.fillRect(x[4], y[4], 50, 10);
		
		//To check if the car is going to crash the boundary, and change its way
		if(xOri+50 > getWidth())
			flag = false;
		if(xOri < 0)
			flag = true;
		
		if(flag == true)
			xOri++;
		else
			xOri--;
	}
	
	//Because the text field can only get strings, so turn the string into integer first
	public void setSpeed(String input)
	{
		int i = Integer.parseInt(input); 
		speedControl = i;
	}
	
	//the method is used to set the car's color
	public void setColor(Color input)
	{
		c = input;
	}
	
	//to repaint the car so that it seems like moving
	class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			repaint();
		}
	}	
}
