package a11.s100502503;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class A111  extends JApplet
{
	public RaceCar r1 = new RaceCar();
	public RaceCar r2 = new RaceCar();
	public RaceCar r3 = new RaceCar();

	public JTextField speed1 = new JTextField("100");
	public JTextField speed2 = new JTextField("100");
	public JTextField speed3 = new JTextField("100");
	
	private JRadioButton jrbRed1, jrbBlue1, jrbGreen1;
	private JRadioButton jrbRed2, jrbBlue2, jrbGreen2;
	private JRadioButton jrbRed3, jrbBlue3, jrbGreen3;

	public A111()
	{
		//The panels
		JPanel p = new JPanel(new GridLayout(3, 1, 5, 5));
		JPanel pTex = new JPanel(new GridLayout(1, 5, 1, 1));
		JLabel Message = new JLabel("Please enter the speed");
		
		//the radio button's group
		JPanel pRadioButton = new JPanel(new GridLayout(1, 3, 1, 1));
		JPanel pRadioButton1 = new JPanel();
		JPanel pRadioButton2 = new JPanel();
		JPanel pRadioButton3 = new JPanel();
		
		//the button to sure the speed that user input
		JButton ok = new JButton("OK");

		//r1's radio buttons
		pRadioButton1.add(jrbRed1 = new JRadioButton("Red"));
		pRadioButton1.add(jrbBlue1 = new JRadioButton("Blue"));
		pRadioButton1.add(jrbGreen1 = new JRadioButton("Green"));
		
		pRadioButton2.add(jrbRed2 = new JRadioButton("Red"));
		pRadioButton2.add(jrbBlue2 = new JRadioButton("Blue"));
		pRadioButton2.add(jrbGreen2 = new JRadioButton("Green"));
		
		pRadioButton3.add(jrbRed3 = new JRadioButton("Red"));
		pRadioButton3.add(jrbBlue3 = new JRadioButton("Blue"));
		pRadioButton3.add(jrbGreen3 = new JRadioButton("Green"));
		
		//Group the radio buttons
		ButtonGroup group1 = new ButtonGroup();
		group1.add(jrbBlue1);
		group1.add(jrbGreen1);
		group1.add(jrbRed1);
	
		ButtonGroup group2 = new ButtonGroup();
		group2.add(jrbBlue1);
		group2.add(jrbGreen1);
		group2.add(jrbRed1);
		
		ButtonGroup group3 = new ButtonGroup();
		group3.add(jrbBlue1);
		group3.add(jrbGreen1);
		group3.add(jrbRed1);
		
		//the action listener to set the color of the car
		jrbRed1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				r1.setColor(Color.RED);
			}
		});
		
		jrbRed2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				r2.setColor(Color.RED);
			}
		});
		
		jrbRed3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				r3.setColor(Color.RED);
			}
		});
		
		jrbGreen1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				r1.setColor(Color.GREEN);
			}
		});
		
		jrbGreen2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				r2.setColor(Color.GREEN);
			}
		});
		
		jrbGreen3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				r3.setColor(Color.GREEN);
			}
		});
		
		jrbBlue1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				r1.setColor(Color.BLUE);
			}
		});
		
		jrbBlue2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				r2.setColor(Color.BLUE);
			}
		});
		
		jrbBlue3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				r3.setColor(Color.BLUE);
			}
		});

		//set the text field panel
		pTex.add(Message);
		pTex.add(ok);
		pTex.add(speed1);
		pTex.add(speed2);
		pTex.add(speed3);

		//when user press the ok button, the variable to control the speed will change
		ok.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				r1.setSpeed(speed1.getText());
			}
		});
		
		ok.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				r2.setSpeed(speed2.getText());
			}
		});
		
		ok.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				r3.setSpeed(speed3.getText());
			}
		});
		
		p.add(r1, BorderLayout.NORTH);
		p.add(r2, BorderLayout.CENTER);
		p.add(r3, BorderLayout.SOUTH);
		
		pRadioButton.add(pRadioButton1);
		pRadioButton.add(pRadioButton2);
		pRadioButton.add(pRadioButton3);

		add(pRadioButton, BorderLayout.SOUTH);
		add(pTex, BorderLayout.NORTH);
		add(p);
	}
	
	
}