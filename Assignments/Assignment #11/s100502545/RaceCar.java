package a11.s100502545;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class RaceCar extends JPanel 
{
	int speed = 500;
	int x = 0;
	int y = 10;
	Color color = new Color(0, 0, 0);
	Timer timer = new Timer(speed, new TimerListener());

	public RaceCar()
	{
		timer.start();
	}

	public void paint(Graphics g) //畫出一輛car
	{
		super.paint(g);
		int[] one = { x, y, 24, 10 };
		int[] two = { x + 4, y + 10, 4, 4 };
		int[] three = { x + 16, y + 10, 4, 4 };
		int[] four = { x + 20 / 3, y - 5, 10, 5 };
		g.setColor(color);
		g.drawRect(one[0], one[1], one[2], one[3]);
		g.drawOval(two[0], two[1], two[2], two[3]);
		g.drawOval(three[0], three[1], three[2], three[3]);
		g.drawRect(four[0], four[1], four[2], four[3]);
		x += 5;
	}

	//設定速度
	public void setspeed(int s) 
	{
		timer.stop();
		speed = s;
		timer = new Timer(speed, new TimerListener());
		timer.start();
	}

	public void setcolor(int colors) // 設定判斷使用者所選擇的顏色
	{
		if (colors == 0) 
		{
			color = new Color(230, 27, 4);
		} 
		else if (colors == 1) 
		{
			color = new Color(0, 0, 0);
		} 
		else if (colors == 2) 
		{
			color = new Color(13, 226, 7);
		}
	}

	class TimerListener implements ActionListener 
	{
		public void actionPerformed(ActionEvent arg0) 
		{
			repaint();
		}

	
	}
}
