package a11.s100502545;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;

import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener 
{
	private String[] flagTitles = {"Red","Black","Green"};
	private JComboBox jcbo1 = new JComboBox(flagTitles);
	private JComboBox jcbo2 = new JComboBox(flagTitles);
	private JComboBox jcbo3 = new JComboBox(flagTitles);
	private JTextField Speed1 = new JTextField(10);
	private JTextField Speed2 = new JTextField(10);
	private JTextField Speed3 = new JTextField(10);
	private JLabel Car1 = new JLabel("My Car1");
	private JLabel Car2 = new JLabel("My Car2");
	private JLabel Car3 = new JLabel("My Car3");
	private JPanel Speedone = new JPanel();
	private JPanel Speedtwo = new JPanel();
	private JPanel Speedthree = new JPanel();
	private JPanel control1 = new JPanel();
	private JPanel control2 = new JPanel();
	private JPanel control3 = new JPanel();
	private RaceCar car1 = new RaceCar();
	private RaceCar car2 = new RaceCar();
	private RaceCar car3 = new RaceCar();
	private JButton change1 = new JButton("Change");
	private JButton change2 = new JButton("Change");
	private JButton change3 = new JButton("Change");
		
		
	
	public FrameWork() 
	{
		//設定版面配置
		this.setLayout(new GridLayout(6, 1));
			
		Speedone.setLayout(new BorderLayout());
		Speedone.add(new JLabel("Speed"), BorderLayout.WEST);
		Speedone.add(Speed1, BorderLayout.CENTER);
		Speedone.add(change1, BorderLayout.EAST);
		control1.setLayout(new GridLayout(1, 4));
		control1.add(Car1);
		control1.add(jcbo1);
		control1.add(Speedone);
		change1.addActionListener(this);
			
		Speedtwo.setLayout(new BorderLayout());
		Speedtwo.add(new JLabel("Speed"), BorderLayout.WEST);
		Speedtwo.add(Speed2, BorderLayout.CENTER);
		Speedtwo.add(change2, BorderLayout.EAST);
		control2.setLayout(new GridLayout(1, 4));
		control2.add(Car2);
		control2.add(jcbo2);
		control2.add(Speedtwo);
		change2.addActionListener(this);
			
		control3.setLayout(new GridLayout(1, 6));
		Speedthree.setLayout(new BorderLayout());
		Speedthree.add(new JLabel("Speed"), BorderLayout.WEST);
		Speedthree.add(Speed3, BorderLayout.CENTER);
		Speedthree.add(change3, BorderLayout.EAST);
		control3.add(Car3);
		control3.add(jcbo3);
		control3.add(Speedthree);
		change3.addActionListener(this);
			
		this.add(control1);
		this.add(control2);
		this.add(control3);
		this.add(car1);
		this.add(car2);
		this.add(car3);
		
		//三個ComboBox(jcbo1,jcbo2,jcbo3)按鈕處理
		jcbo1.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				setDisplay1(jcbo1.getSelectedIndex());
			}
		});
		jcbo2.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				setDisplay2(jcbo2.getSelectedIndex());
			}
		});
		jcbo3.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e)
			{
					setDisplay3(jcbo3.getSelectedIndex());
			}
		});
	}
	//分別顯示使用者所選擇的顏色	
	public void setDisplay1(int index)
	{
		if(index == '0')
		{
			car1.setcolor(index);
		}	
		else if(index == '1')
		{
			car1.setcolor(index);
		}	
		else
		{
			car1.setcolor(index);
		}
	}
		
	public void setDisplay2(int index)
	{
		if(index == '0')
		{
			car2.setcolor(index);
		}
		
		else if(index == '1')
		{
			car2.setcolor(index);
		}
		
		else
		{
			car2.setcolor(index);
		}
	}
		
	public void setDisplay3(int index)
	{
		if(index == '0')
		{
			car3.setcolor(index);
		}
			
		else if(index == '1')
		{
			car3.setcolor(index);
		}
			
		else
		{
			car3.setcolor(index);
		}
	}

	//使用者所改變的速度狀況處理
	public void actionPerformed(ActionEvent e) 
	{
		if (e.getSource() == change1) 
		{
			int speed = Integer.parseInt(Speed1.getText());
			car1.setspeed(speed);
		} 
		else if (e.getSource() == change2) 
		{
			int speed = Integer.parseInt(Speed2.getText());
			car2.setspeed(speed);
		} 
		else if (e.getSource() == change3) 
		{
			int speed = Integer.parseInt(Speed3.getText());
			car3.setspeed(speed);
		}

	}
	
}
