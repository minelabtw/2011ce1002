package A11.s100502512;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class SetCar extends JPanel {
	private JRadioButton cl1, cl2, cl3;
	private JScrollBar V = new JScrollBar(JScrollBar.HORIZONTAL);
	private int delay = 100;
	private ButtonGroup group = new ButtonGroup();
	private RaceCar c1 = new RaceCar();
	public SetCar() {
		JPanel p1 = new JPanel();//按鈕排版
		p1.setLayout(new GridLayout(3, 1));
		p1.add(cl1 = new JRadioButton("C1"));
		p1.add(cl2 = new JRadioButton("C2"));
		p1.add(cl3 = new JRadioButton("C3"));
		group.add(cl1);
		group.add(cl2);
		group.add(cl3);
		p1.add(V);
		JPanel p2 = new JPanel();
		p2.setLayout(new GridLayout(2, 1));
		p2.add(c1);
		p2.add(p1);
		add(p2);
		// 改變速度
		V.addAdjustmentListener(new AdjustmentListener() {
			public void adjustmentValueChanged(AdjustmentEvent e) {
				int Value = V.getValue();
				int Max = V.getMaximum();
				c1.dx = Value;
			}
		});
		// 改變顏色
		cl1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				c1.color1 = new Color(255, 100, 0);
			}
		});
		cl2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				c1.color1 = new Color(100, 0, 255);
			}
		});
		cl3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				c1.color1 = new Color(0, 255, 100);
			}
		});
		class TimerListener implements ActionListener {
			public void actionPerformed(ActionEvent e) {
				repaint();
			}
		}
		Timer timer = new Timer(delay, new TimerListener());
		timer.start();
	}

}
