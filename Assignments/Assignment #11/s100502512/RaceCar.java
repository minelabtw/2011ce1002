package A11.s100502512;

import javax.swing.*;
import java.awt.*;
import java.awt.Graphics;



public class RaceCar extends JPanel {
	private int x = 0;
	private int y = 50;
	protected Color color1 = new Color(0, 0, 0);
	private Color color2 = new Color(255, 255, 225);
	protected int dx = 5;
	private boolean B ;
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (x > getWidth() - 50)// 如果 x > 總長度 ,就會往回跑
		{
			B = false;
		} else if (x < 0) {
			B  = true;
		}
		if (B == true) {
			x += dx;
		} else {
			x -= dx;
		}
		g.setColor(color1);
		g.fillRect(x, y, 50, 10);
		g.setColor(color2);
		int triangley[] = { 50, 30, 30, 50 };
		int trianglex[] = { 10 + x, 20 + x, 30 + x, 40 + x };
		g.fillPolygon(trianglex, triangley, 4); // 畫車子
		g.setColor(color2);
		g.fillOval(x, y + 10, 10, 10);// /畫輪子畫
		g.fillOval(x + 40, y + 10, 10, 10);

	}

}
