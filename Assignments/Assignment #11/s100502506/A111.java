//Class A111: inherit Applet
//Create three cars with class RaceCar
//Three components (you can use button, textfield or combo box) to control the speed of the three cars
//An Radio Button component to set the color of this car
//Three components (you can use button, textfield or combo box) to control the style of the three cars (optional)
//ActionListeners to handle events of the three cars
//Write a html file to access your applet
//You have to save your html file at workspace/project/bin, in order to access the class file
//Remember to write your package name to the applet code
package a11.s100502506;
import java.applet.Applet;
import javax.swing.*;
import java.awt.GridLayout;
import java.awt.*;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class A111 extends Applet    //applet
{
	
	public A111()//init to applet
	{
		JPanel panel=new JPanel();
		panel.setLayout(new GridLayout(3,1,5,5));
		RaceCar car1=new RaceCar();
		RaceCar car2=new RaceCar();
		RaceCar car3=new RaceCar();
		panel.add(car1);
		panel.add(car2);
		panel.add(car3);
		add(panel);
	}
	public static void main(String argc[])
	{
		JFrame finalFrame=new JFrame();
		
		A111 test=new A111();//create an instance of the applet
		
		finalFrame.add(test);//add the applet to the frame
		finalFrame.setSize(600,600);
		finalFrame.setVisible(true);
		finalFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
}
