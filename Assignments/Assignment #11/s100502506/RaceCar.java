//Please write an Applet to meet the following requirements:
//Class RaceCar: JPanel Component
//A timer to control the speed of car
//A set method to set the speed of this car (the delay of the timer)
//A set method to set the color of this car
//A set method to set the style of this car(optional)
//An ActionListener to control drawing the JPanel
package a11.s100502506;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollBar;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.plaf.basic.BasicBorders.RadioButtonBorder;




public class RaceCar extends JPanel 
{
	private int speed=100;
	private String speedString=String.format("%d", speed);
	private int x=500;
	private Timer timer =new Timer(speed, new TimerListener());
	private String[] Shape={"Car","UFO","Bus","PIG"};
	private JCheckBox[] CBoxes=new JCheckBox[7];
	
	private JPanel RBpanel=new JPanel();
	private JPanel CBpanel1=new JPanel();
	private JPanel selectPanel=new JPanel();
	
	private JLabel speedlabel=new JLabel("Speed�G");
	private JTextField speedtext=new JTextField(7);
	private JButton start =new JButton("Start");
	private JButton stop =new JButton("Stop");
	private JComboBox cbChange =new JComboBox(Shape);
	private JPanel contrJPanel=new JPanel();
	private JPanel speedPanel=new JPanel();
	private int Type =cbChange.getSelectedIndex();
	private JScrollBar hori=new JScrollBar(JScrollBar.HORIZONTAL);
	RaceCar() 
	{
		//set scrollbar
		hori.setMinimum(1);
		hori.setMaximum(1000);
		hori.setValue(speed);
		speedtext.setText(speedString);
		speedPanel.add(speedlabel);
		speedPanel.add(speedtext);
		
		selectPanel.setLayout(new GridLayout(5,1,5,5));
		selectPanel.add(start);
		selectPanel.add(stop);
		selectPanel.add(speedPanel);
		selectPanel.add(hori);
		selectPanel.add(cbChange);
		
		RBpanel.setLayout(new GridLayout(7,1,5,5));
		
		for(int i=0;i<7;i++)
		{
			CBoxes[i]=new JCheckBox();
			RBpanel.add(CBoxes[i]);
			
		}
		
		CBoxes[0].setText("Red");
		CBoxes[1].setText("Orange");
		CBoxes[2].setText("Yellow");
		CBoxes[3].setText("Green");
		CBoxes[4].setText("Blue");
		CBoxes[5].setText("Indigo");
		CBoxes[6].setText("Purple");
		
		
		drawRaceCar car=new drawRaceCar();
		
		contrJPanel.setLayout(new GridLayout(1,2,5,5));//setlayout
		contrJPanel.add(RBpanel);	
		contrJPanel.add(selectPanel);
		
		
		setLayout(new GridLayout());
		add(contrJPanel,BorderLayout.WEST);				//add to RaceCar JPanel
		add(car,BorderLayout.CENTER);					//add to RaceCar JPanel
		
		start.addActionListener(new ButtonListener());	//add buttonlistener
		stop.addActionListener(new ButtonListener());   //add buttonlistener
		
		
		
		
	}
	class TimerListener implements ActionListener		//Time Listener
	{
		public void actionPerformed(ActionEvent e)
		{
			//delay to reset
			Type =cbChange.getSelectedIndex();
			x=x+50;
			speedString=String.format("%d",hori.getValue());
			speedtext.setText(speedString);
			speed=Integer.parseInt(speedtext.getText());
			
			timer.setDelay(speed);
			repaint();
			
		}
		
	}
	
	class ButtonListener implements ActionListener		//Button Listener
	{
		public void actionPerformed(ActionEvent e)
		{

			if(e.getSource()==start)
			{
				timer.start();
			}
			if(e.getSource()==stop)
			{
				timer.stop();
			}
		}
		
	}
	class drawRaceCar extends JPanel					//paintcomponent
	{
		
		public drawRaceCar() 
		{
			
			
		}
		public void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			
		
			int y=getHeight()/2;
			int side=20;
			int rectx1=x;
			int rectx2=x+side;
			int rectx3=x+2*side;
			int rectx4=x+3*side;
			int recty1=y-1*side;
			int recty2=y-2*side;
			int recty3=y-2*side;
			int recty4=y-1*side;
			
			int rectxs[]={rectx1,rectx2,rectx3,rectx4};
			int rectys[]={recty1,recty2,recty3,recty4};
			
			
			
			if(Type==0)												//car
			{
				g.setColor(Color.WHITE);
				g.fillOval(x, (int)(y-side*0.5), side, side);
				g.setColor(Color.WHITE);
				g.fillOval(x+2*side, (int)(y-side*0.5), side, side);
				g.setColor(Color.WHITE);
				g.fillRect(x-side, y-side, side*5,side);
				g.setColor(Color.WHITE);
				g.fillPolygon(rectxs,rectys, 4);
				
				if(CBoxes[0].isSelected())
				{
					g.setColor(Color.RED);
					g.fillOval(x, (int)(y-side*0.5), side, side);
				}
				if(CBoxes[1].isSelected())
				{
					g.setColor(Color.ORANGE);
					g.fillOval(x+2*side, (int)(y-side*0.5), side, side);
				}
				if(CBoxes[2].isSelected())
				{
					g.setColor(Color.YELLOW);
					g.fillRect(x-side, y-side, side*5,side);
				}
				if(CBoxes[3].isSelected())
				{
					g.setColor(Color.GREEN);
					g.fillPolygon(rectxs,rectys, 4);
				}
				if(CBoxes[4].isSelected())
				{
					g.setColor(Color.blue);
					g.fillOval(x, (int)(y-side*0.5), side, side);
				}
				if(CBoxes[5].isSelected())
				{
					g.setColor(Color.cyan);
					g.fillOval(x+2*side, (int)(y-side*0.5), side, side);
				}
				if(CBoxes[6].isSelected())
				{
					g.setColor(Color.pink);
					g.fillRect(x-side, y-side, side*5,side);
				}
			}
			else if(Type==1)											//UFO
			{
				g.setColor(Color.WHITE);
				g.fillOval(x, (int)(y-side*1.5), side*3,side);
				g.setColor(Color.WHITE);
				g.fillOval(x-side, y-side, side*5,side);
				
				
				
				if(CBoxes[0].isSelected())
				{
					g.setColor(Color.RED);
					g.fillOval(x, (int)(y-side*1.5), side*3,side);
				}
				if(CBoxes[1].isSelected())
				{
					g.setColor(Color.ORANGE);
					g.fillOval(x-side, y-side, side*5,side);
				}
				if(CBoxes[2].isSelected())
				{
					g.setColor(Color.YELLOW);
					g.fillOval(x, (int)(y-side*1.5), side*3,side);
				}
				if(CBoxes[3].isSelected())
				{
					g.setColor(Color.GREEN);
					g.fillOval(x-side, y-side, side*5,side);
				}
				if(CBoxes[4].isSelected())
				{
					g.setColor(Color.blue);
					g.fillOval(x, (int)(y-side*1.5), side*3,side);
				}
				if(CBoxes[5].isSelected())
				{
					g.setColor(Color.cyan);
					g.fillOval(x-side, y-side, side*5,side);
				}
				if(CBoxes[6].isSelected())
				{
					g.setColor(Color.pink);
					g.fillOval(x, (int)(y-side*1.5), side*3,side);
				}
			}
			else if(Type==2)											//BUS
			{
				g.setColor(Color.WHITE);
				g.fillOval(x, (int)(y-side*0.5), side, side);
				g.setColor(Color.WHITE);
				g.fillOval(x+2*side,(int)(y-side*0.5), side, side);
				g.setColor(Color.WHITE);
				g.fillRect(x-side, (int)(y-side*1), side*5,side);
				g.setColor(Color.WHITE);
				g.fillRect(x-side, (int)(y-side*2), side*5,side);
				g.setColor(Color.white);
				g.fillRect((int)(x+side*3), (int)(y-side*2), side,side);
				g.fillRect((int)(x+side*2), (int)(y-side*1.8), (int)(side*0.5), (int)(side*0.5));
				g.fillRect((int)(x+side*1), (int)(y-side*1.8), (int)(side*0.5), (int)(side*0.5));
				g.fillRect((int)(x), (int)(y-side*1.8), (int)(side*0.5), (int)(side*0.5));
				g.fillRect((int)(x-side*1), (int)(y-side*1.8), (int)(side*0.5), (int)(side*0.5));

				if(CBoxes[0].isSelected())
				{
					g.setColor(Color.RED);
					g.fillOval(x, (int)(y-side*0.5), side, side);
				}
				if(CBoxes[1].isSelected())
				{
					g.setColor(Color.ORANGE);
					g.fillOval(x+2*side,(int)(y-side*0.5), side, side);
				}
				if(CBoxes[2].isSelected())
				{
					g.setColor(Color.YELLOW);
					g.fillRect(x-side, (int)(y-side*1), side*5,side);
				}
				if(CBoxes[3].isSelected())
				{
					g.setColor(Color.GREEN);
					g.fillRect(x-side, (int)(y-side*2), side*5,side);
				}
				if(CBoxes[4].isSelected())
				{
					g.setColor(Color.blue);
					g.fillRect((int)(x+side*3), (int)(y-side*2), side,side);
					g.fillRect((int)(x+side*2), (int)(y-side*1.8), (int)(side*0.5), (int)(side*0.5));
					g.fillRect((int)(x+side*1), (int)(y-side*1.8), (int)(side*0.5), (int)(side*0.5));
					g.fillRect((int)(x), (int)(y-side*1.8), (int)(side*0.5), (int)(side*0.5));
					g.fillRect((int)(x-side*1), (int)(y-side*1.8), (int)(side*0.5), (int)(side*0.5));
				}
				if(CBoxes[5].isSelected())
				{
					g.setColor(Color.cyan);
					g.fillRect((int)(x+side*3), (int)(y-side*2), side,side);
					g.fillRect((int)(x+side*2), (int)(y-side*1.8), (int)(side*0.5), (int)(side*0.5));
					g.fillRect((int)(x+side*1), (int)(y-side*1.8), (int)(side*0.5), (int)(side*0.5));
					g.fillRect((int)(x), (int)(y-side*1.8), (int)(side*0.5), (int)(side*0.5));
					g.fillRect((int)(x-side*1), (int)(y-side*1.8), (int)(side*0.5), (int)(side*0.5));
				}
				if(CBoxes[6].isSelected())
				{
					g.setColor(Color.pink);
					g.fillRect((int)(x+side*3), (int)(y-side*2), side,side);
					g.fillRect((int)(x+side*2), (int)(y-side*1.8), (int)(side*0.5), (int)(side*0.5));
					g.fillRect((int)(x+side*1), (int)(y-side*1.8), (int)(side*0.5), (int)(side*0.5));
					g.fillRect((int)(x), (int)(y-side*1.8), (int)(side*0.5), (int)(side*0.5));
					g.fillRect((int)(x-side*1), (int)(y-side*1.8), (int)(side*0.5), (int)(side*0.5));
				}
			}
			else if(Type==3) 									//PIG
			{
				g.setColor(Color.BLACK);
				
				g.fillOval(55+x, 10, 270,250);
				g.fillOval(75+x, 10, 270,250);
				g.fillArc(0+x, -70, 150,150,270,80);
				g.fillArc(250+x, -70, 150,150,270,-80);
				
				g.setColor(Color.WHITE);
				g.fillOval(90+x,95,40,40);
				g.fillOval(270+x,95,40,40);
				g.fillOval(160+x,125, 80, 50);
			
				g.setColor(Color.BLACK);
				g.fillOval(170+x, 135, 22, 33);
				g.fillOval(205+x, 135, 22, 33);
				
				if(CBoxes[0].isSelected())
				{
					g.setColor(Color.RED);
					g.fillOval(55+x, 10, 270,250);
					g.fillOval(75+x, 10, 270,250);
				}
				if(CBoxes[1].isSelected())
				{
					g.setColor(Color.ORANGE);
					g.fillArc(0+x, -70, 150,150,270,80);
					g.fillArc(250+x, -70, 150,150,270,-80);
				}
				if(CBoxes[2].isSelected())
				{
					g.setColor(Color.YELLOW);
					g.fillOval(90+x,95,40,40);
					g.fillOval(270+x,95,40,40);
					g.fillOval(160+x,125, 80, 50);
				}
				if(CBoxes[3].isSelected())
				{
					g.setColor(Color.GREEN);
					g.fillOval(170+x, 135, 22, 33);
					g.fillOval(205+x, 135, 22, 33);
				}
				if(CBoxes[4].isSelected())
				{
					g.setColor(Color.blue);
					g.fillOval(55+x, 10, 270,250);
					g.fillOval(75+x, 10, 270,250);
				}
				if(CBoxes[5].isSelected())
				{
					g.setColor(Color.cyan);
					g.fillArc(0+x, -70, 150,150,270,80);
					g.fillArc(250+x, -70, 150,150,270,-80);
				}
				if(CBoxes[6].isSelected())
				{
					g.setColor(Color.pink);
					g.fillOval(90+x,95,40,40);
					g.fillOval(270+x,95,40,40);
					g.fillOval(160+x,125, 80, 50);
				}
				
				
			}
			//reset to zero
			if(x>=getWidth()+50)
				x=-50;
			//reset	
			Type =cbChange.getSelectedIndex();
			speedString=String.format("%d",hori.getValue());
			speedtext.setText(speedString);
			speed=Integer.parseInt(speedtext.getText());
			
			timer.setDelay(speed);
			repaint();
		}
		
		
	}

}
