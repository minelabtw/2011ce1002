package a11.s992001033;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class RaceCar extends JPanel
{
	Color c;
	private int x1 = this.getX();
	private int y1 = this.getY()+20;
	private int px[] = new int[4];
	private int py[] = new int[4];
	private int tx[] = new int[2];
	private int ty[] = new int[2];
	private Timer timer = new Timer(1000,new TimerListener());
	RaceCar()
	{
		timer.start();
	}
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		px[0] = x1+10;
		py[0] = y1;
		px[1] = px[0]+10;
		py[1] = py[0]-10;
		px[2] = px[1]+10;
		py[2] = py[1];
		px[3] = px[0]+30;
		py[3] = py[0];
		tx[0] = px[0]-5;
		ty[0] = py[0]+10;
		tx[1] = px[3]-5;
		ty[1] = py[3]+10;
		
		g.setColor(c);
		g.fillRect(x1, y1, 50, 10);
		g.fillPolygon(px, py, 4);
		g.fillOval(tx[0], ty[0], 10, 10);
		g.fillOval(tx[1], ty[1], 10, 10);
		
		x1 += 10;
		if(x1>this.getX()+this.getWidth()-50)
			x1 =this.getX();			
	}
	private class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			repaint();
		}
	}
	public void setSpeed(double speed)  
	{
		timer.setDelay((int) (1000*(1/speed)));
	}
	public void setColor(Color color)
	{
		c = color;
	}
}
