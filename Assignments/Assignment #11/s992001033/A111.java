package a11.s992001033;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class A111 extends JApplet
{
	public A111()
	{
		setLayout(new GridLayout(3,1));
		add(new controlPanel());
		add(new controlPanel());
		add(new controlPanel());
	}
}
class controlPanel extends JPanel implements ActionListener
{
	JPanel control = new JPanel(new BorderLayout());
	JPanel all = new JPanel(new GridLayout(1,2));
	JScrollBar speed = new JScrollBar(JScrollBar.HORIZONTAL);
	JPanel radio = new JPanel(new GridLayout(1,3));
	JRadioButton colorR =new JRadioButton("Red");
	JRadioButton colorG =new JRadioButton("Green");
	JRadioButton colorB =new JRadioButton("Blue");
	RaceCar car = new RaceCar();
	ButtonGroup group = new ButtonGroup();
	
	controlPanel()
	{
		speed.setMinimum(1);
		speed.setMaximum(100+speed.getMinimum());
		speed.addAdjustmentListener
		(new AdjustmentListener()
			{
				public void adjustmentValueChanged(AdjustmentEvent e)
				{
					car.setSpeed(speed.getValue());
				}
			}
		);
		group.add(colorR);
		group.add(colorG);
		group.add(colorB);
		radio.add(colorR);
		radio.add(colorG);
		radio.add(colorB);
		colorR.addActionListener(this);
		colorG.addActionListener(this);
		colorB.addActionListener(this);
		
		control.add(speed,BorderLayout.PAGE_START);
		control.add(radio,BorderLayout.CENTER);
		
		all.add(car);
		all.add(control);
		
		setLayout(new BorderLayout());
		add(all,BorderLayout.CENTER);
	}
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource() == colorR)
			car.setColor(Color.red);
		else if(e.getSource() == colorG)
			car.setColor(Color.GREEN);
		else if(e.getSource() == colorB)
			car.setColor(Color.BLUE);
		
		
	}
}