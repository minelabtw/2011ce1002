package a11.s100502021;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class RaceCar extends JPanel{
	protected int speed = 1000;
	int x=0,y=-50,s=0,w=0;
	Color co;
	Timer timer=new Timer(speed,new TimerListener());
	public RaceCar(){
					
		timer.start();	
	}
	public void setspeed(String s){ //儲存速度
		speed=Integer.parseInt(s);
		timer.setDelay(speed);
	}
	public void setcolor(Color c){ //儲存顏色
		co=c;
	}
	protected void paintComponent(Graphics g){		
		super.paintComponent(g);
		if(x>=(getWidth()-150)){
			s=-1;
		}
		if(x==0){
			s=0;
		}
		
		if(s==-1){
			x-=5;
		}
		if(s==0){
			x+=5;
		}
	
		//y++;
		int x1[]={x+50,x+100,x+150,x}; //畫車子
		int y1[]={y+50,y+50,y+100,y+100};
		g.setColor(co);
		g.fillPolygon(x1,y1,4);
		//g.drawLine(x1[1],y1[1],x1[2],y1[2]);
	//	g.drawLine(x1[2],y1[2],x1[3],y1[3]);
	//	g.drawLine(x1[3],y1[3],x1[0],y1[0]);
		int x2[]={x+150,x+150,x,x};
		int y2[]={y+100,y+125,y+125,y+100};
		g.fillPolygon(x2,y2,4);
		//g.drawLine(x1[2],y1[2],x1[2],y2[0]);
	//	g.drawLine(x1[2],y2[0],x1[3],y2[0]);
	//	g.drawLine(x1[3],y2[0],x1[3],y1[3]);
		g.fillOval(x1[0]-2, y2[0]+25, 25 , 25);
		g.fillOval(x1[1]-2, y2[0]+25, 25 , 25);	
		
	}
	class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			repaint();
		}
	}
	
}
