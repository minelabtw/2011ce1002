package a11.s985003038;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

public class RaceCar extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	private Timer timer = new Timer(0, this);
	private Color[] colors = {Color.red, Color.green, Color.blue};
	private int color, style;
	
	private int[] carX;											// data for the car style
	private int[] carY;
	private int[] carWinX;
	private int[] carWinY;
	private int carWheelX;
	private int carWheelY;
	
	private int flyPositionX;									// data for the fly style
	private int flyPositionY;
	
	private int ufoPositionX;									// data for the UFO style
	private int ufoPositionY;
	private boolean ufoDirectionX;
	private boolean ufoDirectionY;
	
	public RaceCar(){											// construction
		setSize(500, 200);										// default panel size
		setSpeed(500);											// default speed - 500
		setColor(0);											// default color - red
		setStyle(0);											// default style - car
		timer.start();											// start the timer
	}
	
	public void setColor(int color){							// method to set the color
		if(style == 1) JOptionPane.showMessageDialog(null, "Fly can only in black color", "Assignment XI", JOptionPane.INFORMATION_MESSAGE);

		if(color >= 0 && color <= 5) this.color = color;
		else JOptionPane.showMessageDialog(null, "No such color", "Assignment XI", JOptionPane.ERROR_MESSAGE);
		repaint();												// show an error message if the input is out of range
	}
	
	public void setSpeed(int speed){							// method to set the speed
		if(speed <= 0) timer.stop();							// if the speed is 0, stop the timer
		else if(!timer.isRunning()) timer.start();				// otherwise, check if we need to restart the timer
		timer.setDelay((int)(speed * 50000 / Math.pow(speed, 2)));
	}															// calculate the relationship between speed and timer delay
	
	public void setStyle(int style){							// method to set the style
		if(style == 0){											// set the default position for the car
			carX = new int[]{-280, -250, -200, -150, -100, -80, -50, -300};
			carY = new int[]{100, 100, 50, 50, 100, 100, 150, 150};
			carWinX = new int[]{-240, -200, -150, -110};
			carWinY = new int[]{100, 60, 60, 100};
			carWheelX = -250;
			carWheelY = 130;
		} else if(style == 1){									// set the default position for the fly
			flyPositionX = getWidth() / 2;
			flyPositionY = 75;
		} else if(style == 2){									// set the default position for the UFO
			ufoPositionX = getWidth() / 2;
			ufoPositionY = 75;
		}
		
		if(style >= 0 && style <= 2) this.style = style;
		else JOptionPane.showMessageDialog(null, "No such style", "Assignment XI", JOptionPane.ERROR_MESSAGE);
		repaint();												// show an error message if the input is out of range
	}
	
	private void move(){										// method to implement the movements
		if(style == 0){											// for the car
			if(getWidth() > 0 && carX[7] > getWidth()){			// if the car is moved out of the screen
				for(int i = 0; i < carX.length; i++)			// reset its position to the left of the screen
					carX[i] -= getWidth() + 300;
	    		for(int i = 0; i < carWinX.length; i++)
	    			carWinX[i] -= getWidth() + 300;
	    		carWheelX -= getWidth() + 300;
			} else {											// otherwise
	    		for(int i = 0; i < carX.length; i++)			// update the position of the car
	    			carX[i] += 5;
	    		for(int i = 0; i < carWinX.length; i++)			// update the position of the window
	    			carWinX[i] += 5;
	    		carWheelX += 5;
			}
		} else if(style == 1){									// for the fly
			flyPositionX += (int)(Math.random() * 10 - 5);		// let it fly randomly
			flyPositionY += (int)(Math.random() * 10 - 5);
		} else if(style == 2){									// for the UFO, calculate its new position
			ufoDirectionX = (int)(Math.random() * 30) == 0 ? !ufoDirectionX : ufoDirectionX;
			ufoDirectionY = (int)(Math.random() * 30) == 0 ? !ufoDirectionY : ufoDirectionY;
			ufoPositionX += (int)(Math.random() * 10) * (ufoDirectionX ? 1 : -1);
			ufoPositionY += (int)(Math.random() * 10) * (ufoDirectionY ? 1 : -1);
			if(ufoPositionX < -200) ufoPositionX = getWidth() + 30;
			else if(ufoPositionX > getWidth() + 30) ufoPositionX = -200;
			if(ufoPositionY < -30) ufoPositionY = getHeight() + 30;
			else if(ufoPositionY > getHeight() + 30) ufoPositionY = -30;
		}														// check if the UFO is out the screen, replace it to the right position
		
		repaint();
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == timer)								// for each tick of the timer
			move();												// call the movement method
	}
	
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		if(style == 0){
			g.setColor(colors[color]);
			g.fillPolygon(carX, carY, carX.length);				// draw the car
			g.setColor(Color.black);
			g.fillPolygon(carWinX, carWinY, carWinX.length);	// draw the window
			g.setColor(Color.gray);
			g.fillOval(carWheelX, carWheelY, 40, 40);			// draw the left wheel
			g.fillOval(carWheelX + 100 + 20, carWheelY, 40, 40);// draw the right wheel
			g.setColor(Color.green);
		} else if(style == 1){
			g.fillOval(flyPositionX, flyPositionY, 20, 20);		// draw the body
			g.setFont(new Font("Arial", Font.PLAIN, 15));		// set the font and write a string beside it
			g.drawString("Fly flies in random direction", flyPositionX + 30, flyPositionY + 30);
		} else if(style == 2){
			g.setColor(colors[color]);
			g.setFont(new Font("Arial", Font.PLAIN, 15));		// draw the invisible UFO
			g.drawString("This is an invisible UFO", ufoPositionX, ufoPositionY);
		}
	}
}
