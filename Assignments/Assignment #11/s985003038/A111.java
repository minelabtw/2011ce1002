package a11.s985003038;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.LineBorder;

public class A111 extends JApplet implements ActionListener {
	private static final long serialVersionUID = 1L;
	private RaceCar car1, car2, car3;							// define three cars, and three sets of buttons to control the cars
	private JButton minus1, add1, style11, style12, style13;
	private JButton minus2, add2, style21, style22, style23;
	private JButton minus3, add3, style31, style32, style33;
	private JLabel speed1, speed2, speed3;
	private JRadioButton jrbRed1, jrbGreen1, jrbBlue1;
	private JRadioButton jrbRed2, jrbGreen2, jrbBlue2;
	private JRadioButton jrbRed3, jrbGreen3, jrbBlue3;
	
	public A111(){
		car1 = new RaceCar();									// create the cars
		car2 = new RaceCar();
		car3 = new RaceCar();
		
		this.setLayout(new GridLayout(3,1));
		
		JPanel panel1 = new JPanel();							// design the panel for car1
		panel1.setLayout(new BorderLayout(1,3));
		JPanel left1 = new JPanel();
		left1.setLayout(new GridLayout(2,3));
		minus1 = new JButton("Slower");
		add1 = new JButton("Faster");
		style11 = new JButton("Car");
		style12 = new JButton("Fly");
		style13 = new JButton("UFO");
		speed1 = new JLabel("500");
		speed1.setHorizontalAlignment(JLabel.CENTER);
		speed1.setBorder(new LineBorder(Color.black, 1));
		left1.add(minus1);
		left1.add(speed1);
		left1.add(add1);
		left1.add(style11);
		left1.add(style12);
		left1.add(style13);
		minus1.addActionListener(this);
		add1.addActionListener(this);
		style11.addActionListener(this);
		style12.addActionListener(this);
		style13.addActionListener(this);
		JPanel right1 = new JPanel();
		right1.setLayout(new GridLayout(3,1));
		jrbRed1 = new JRadioButton("Red");
		jrbGreen1 = new JRadioButton("Green");
		jrbBlue1 = new JRadioButton("Blue");
		ButtonGroup group1 = new ButtonGroup();
	    group1.add(jrbRed1);
	    group1.add(jrbGreen1);
	    group1.add(jrbBlue1);
		right1.add(jrbRed1);
		right1.add(jrbGreen1);
		right1.add(jrbBlue1);
		jrbRed1.addActionListener(this);
		jrbGreen1.addActionListener(this);
		jrbBlue1.addActionListener(this);
		panel1.add(left1, BorderLayout.WEST);
		panel1.add(car1, BorderLayout.CENTER);
		panel1.add(right1, BorderLayout.EAST);
		
		JPanel panel2 = new JPanel();							// design the panel for car2
		panel2.setLayout(new BorderLayout(1,3));
		JPanel left2 = new JPanel();
		left2.setLayout(new GridLayout(2,3));
		minus2 = new JButton("Slower");
		add2 = new JButton("Faster");
		style21 = new JButton("Car");
		style22 = new JButton("Fly");
		style23 = new JButton("UFO");
		speed2 = new JLabel("500");
		speed2.setHorizontalAlignment(JLabel.CENTER);
		speed2.setBorder(new LineBorder(Color.black, 1));
		left2.add(minus2);
		left2.add(speed2);
		left2.add(add2);
		left2.add(style21);
		left2.add(style22);
		left2.add(style23);
		minus2.addActionListener(this);
		add2.addActionListener(this);
		style21.addActionListener(this);
		style22.addActionListener(this);
		style23.addActionListener(this);
		JPanel right2 = new JPanel();
		right2.setLayout(new GridLayout(3,1));
		jrbRed2 = new JRadioButton("Red");
		jrbGreen2 = new JRadioButton("Green");
		jrbBlue2 = new JRadioButton("Blue");
		ButtonGroup group = new ButtonGroup();
	    group.add(jrbRed2);
	    group.add(jrbGreen2);
	    group.add(jrbBlue2);
		right2.add(jrbRed2);
		right2.add(jrbGreen2);
		right2.add(jrbBlue2);
		jrbRed2.addActionListener(this);
		jrbGreen2.addActionListener(this);
		jrbBlue2.addActionListener(this);
		panel2.add(left2, BorderLayout.WEST);
		panel2.add(car2, BorderLayout.CENTER);
		panel2.add(right2, BorderLayout.EAST);
		
		JPanel panel3 = new JPanel();							// design the panel for car3
		panel3.setLayout(new BorderLayout(1,3));
		JPanel left3 = new JPanel();
		left3.setLayout(new GridLayout(2,3));
		minus3 = new JButton("Slower");
		add3 = new JButton("Faster");
		style31 = new JButton("Car");
		style32 = new JButton("Fly");
		style33 = new JButton("UFO");
		speed3 = new JLabel("500");
		speed3.setHorizontalAlignment(JLabel.CENTER);
		speed3.setBorder(new LineBorder(Color.black, 1));
		left3.add(minus3);
		left3.add(speed3);
		left3.add(add3);
		left3.add(style31);
		left3.add(style32);
		left3.add(style33);
		minus3.addActionListener(this);
		add3.addActionListener(this);
		style31.addActionListener(this);
		style32.addActionListener(this);
		style33.addActionListener(this);
		JPanel right3 = new JPanel();
		right3.setLayout(new GridLayout(3,1));
		jrbRed3 = new JRadioButton("Red");
		jrbGreen3 = new JRadioButton("Green");
		jrbBlue3 = new JRadioButton("Blue");
		ButtonGroup group3 = new ButtonGroup();
	    group3.add(jrbRed3);
	    group3.add(jrbGreen3);
	    group3.add(jrbBlue3);
		right3.add(jrbRed3);
		right3.add(jrbGreen3);
		right3.add(jrbBlue3);
		jrbRed3.addActionListener(this);
		jrbGreen3.addActionListener(this);
		jrbBlue3.addActionListener(this);
		panel3.add(left3, BorderLayout.WEST);
		panel3.add(car3, BorderLayout.CENTER);
		panel3.add(right3, BorderLayout.EAST);
		
		panel1.setBorder(new LineBorder(Color.black, 1));
		panel2.setBorder(new LineBorder(Color.black, 1));
		panel3.setBorder(new LineBorder(Color.black, 1));
		
		this.add(panel1);										// add the three panel into the applet
		this.add(panel2);
		this.add(panel3);
		
		jrbRed1.setSelected(true);								// default selection of the radio buttons
		jrbGreen2.setSelected(true);
		car2.setColor(1);
		jrbBlue3.setSelected(true);
		car3.setColor(2);
	}

	public void actionPerformed(ActionEvent e) {				// implement the event handlings
		if(e.getSource() == minus1){
			int speed = Integer.valueOf(speed1.getText()) - 100;
			speed1.setText(String.valueOf(speed < 0 ? 0 : speed));
			car1.setSpeed(Integer.valueOf(speed1.getText()));
		} else if(e.getSource() == minus2){
			int speed = Integer.valueOf(speed2.getText()) - 100;
			speed2.setText(String.valueOf(speed < 0 ? 0 : speed));
			car2.setSpeed(Integer.valueOf(speed2.getText()));
		} else if(e.getSource() == minus3){
			int speed = Integer.valueOf(speed3.getText()) - 100;
			speed3.setText(String.valueOf(speed < 0 ? 0 : speed));
			car3.setSpeed(Integer.valueOf(speed3.getText()));
		} else if(e.getSource() == add1){
			speed1.setText(String.valueOf(Integer.valueOf(speed1.getText()) + 100));
			car1.setSpeed(Integer.valueOf(speed1.getText()));
		} else if(e.getSource() == add2){
			speed2.setText(String.valueOf(Integer.valueOf(speed2.getText()) + 100));
			car2.setSpeed(Integer.valueOf(speed2.getText()));
		} else if(e.getSource() == add3){
			speed3.setText(String.valueOf(Integer.valueOf(speed3.getText()) + 100));
			car3.setSpeed(Integer.valueOf(speed3.getText()));
		} else if(e.getSource() == style11){
			car1.setStyle(0);
		} else if(e.getSource() == style12){
			car1.setStyle(1);
		} else if(e.getSource() == style13){
			car1.setStyle(2);
		} else if(e.getSource() == style21){
			car2.setStyle(0);
		} else if(e.getSource() == style22){
			car2.setStyle(1);
		} else if(e.getSource() == style23){
			car2.setStyle(2);
		} else if(e.getSource() == style31){
			car3.setStyle(0);
		} else if(e.getSource() == style32){
			car3.setStyle(1);
		} else if(e.getSource() == style33){
			car3.setStyle(2);
		} else if(e.getSource() == jrbRed1){
			car1.setColor(0);
		} else if(e.getSource() == jrbGreen1){
			car1.setColor(1);
		} else if(e.getSource() == jrbBlue1){
			car1.setColor(2);
		} else if(e.getSource() == jrbRed2){
			car2.setColor(0);
		} else if(e.getSource() == jrbGreen2){
			car2.setColor(1);
		} else if(e.getSource() == jrbBlue2){
			car2.setColor(2);
		} else if(e.getSource() == jrbRed3){
			car3.setColor(0);
		} else if(e.getSource() == jrbGreen3){
			car3.setColor(1);
		} else if(e.getSource() == jrbBlue3){
			car3.setColor(2);
		}
	}
}
