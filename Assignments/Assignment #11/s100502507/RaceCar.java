package a11.s100502507;

import java.awt.event.*;
import java.awt.*;

import javax.swing.*;

public class RaceCar extends JPanel {
	RaceCar() {//Initializing variables
		timer = new Timer(0, new TimerListener());
		color = new Color(0, 0, 0);
		xCoordinate = 0;
		yCoordinate = 30;
		velocity = 0;
		timer.start();
	}
	
	public void setSpeed(int delay) {//Change the "delay" of timer
		timer.setDelay(delay);
		if(delay == 0) {
			velocity = 0;
		}
		else {
			velocity = 10;
		}
	}
	
	public void setColor(Color c) {//Set the color of this car
		color = new Color(c.getRGB());
	}
	
	protected void paintComponent(Graphics g) {//Draw this car
		super.paintComponent(g);
		
		int[] x = new int[7];
		int[] y = new int[7];
		
		x[0] = xCoordinate + 20;
		x[1] = x[6] = xCoordinate + 30;
		y[0] = y[1] = yCoordinate - 30;
		x[2] = x[5] = xCoordinate + 10;
		x[3] =  xCoordinate + 40;
		y[2] =  y[3] = yCoordinate - 20;
		x[4] = xCoordinate;
		y[4] = yCoordinate - 20;
		y[5] = y[6] = yCoordinate - 10;
		
		Color holdColor = new Color(color.getRGB());
		
		g.setColor(color);
		g.fillOval(x[5], y[5], 10, 10);
		g.fillOval(x[6], y[6], 10, 10);
		
		color.brighter();
		g.setColor(color);
		g.fillRect(x[4], y[4], 50, 10);
		
		color.brighter();
		g.setColor(color);
		Polygon top = new Polygon();
		top.addPoint(x[0], y[0]);
		top.addPoint(x[1], y[1]);
		top.addPoint(x[3], y[3]);
		top.addPoint(x[2], y[2]);
		g.fillPolygon(top);
		
		if(xCoordinate > getWidth()) {//If this car is out of window, set this car to the left of the window
			xCoordinate = 0 - 50;
		}
		
		xCoordinate += velocity;
		
		color = new Color(holdColor.getRGB());
	}
	
	private class TimerListener implements ActionListener {//Redraw this car when time is up
		public void actionPerformed(ActionEvent e) {
			repaint();
		}
	}
	
	private int velocity;
	private int xCoordinate;
	private int yCoordinate;
	private Color color;
	private Timer timer;
}
