package a11.s100502507;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

public class A111 extends JApplet {
	public A111() {
		setLayout(new GridLayout(3, 2));
		
		JPanel functions01 = new JPanel();//Some panel which contain the functions to change the car
		JPanel functions02 = new JPanel();
		JPanel functions03 = new JPanel();
		
		car01 = new RaceCar();//3 cars
		car02 = new RaceCar();
		car03 = new RaceCar();
		
		red01 = new JRadioButton("Red");//Radio buttons(Change the color of cars)
		green01 = new JRadioButton("Green");
		blue01 = new JRadioButton("Blue");
		red02 = new JRadioButton("Red");
		green02 = new JRadioButton("Green");
		blue02 = new JRadioButton("Blue");
		red03 = new JRadioButton("Red");
		green03 = new JRadioButton("Green");
		blue03 = new JRadioButton("Blue");
		
		speed01 = new JTextField("0");//Fields to let user change the FPS of cars
		speed02 = new JTextField("0");
		speed03 = new JTextField("0");
		
		confirmSpeed01 = new JButton("Confirm");//Once these button are clicked, the value of the corresponding text field will be passed to the car
		confirmSpeed02 = new JButton("Confirm");
		confirmSpeed03 = new JButton("Confirm");
		
		ButtonGroup group01 = new ButtonGroup();//Avoid 2 or 3 radio buttons are selected at the same time
		ButtonGroup group02 = new ButtonGroup();
		ButtonGroup group03 = new ButtonGroup();
		
		group01.add(red01);//"Group" the radio buttons
		group01.add(blue01);
		group01.add(green01);
		group02.add(red02);
		group02.add(blue02);
		group02.add(green02);
		group03.add(red03);
		group03.add(blue03);
		group03.add(green03);
		
		JrbRed jrbRed = new JrbRed();//Action listeners which are used by radio buttons to change the color of cars
		JrbBlue jrbBlue = new JrbBlue();
		JrbGreen jrbGreen = new JrbGreen();
		
		red01.addActionListener(jrbRed);
		red02.addActionListener(jrbRed);
		red03.addActionListener(jrbRed);
		blue01.addActionListener(jrbBlue);
		blue02.addActionListener(jrbBlue);
		blue03.addActionListener(jrbBlue);
		green01.addActionListener(jrbGreen);
		green02.addActionListener(jrbGreen);
		green03.addActionListener(jrbGreen);
		
		JbtConfirm jbtConfirm = new JbtConfirm();//Action listeners which are used by buttons to change the FPS of cars
		confirmSpeed01.addActionListener(jbtConfirm);
		confirmSpeed02.addActionListener(jbtConfirm);
		confirmSpeed03.addActionListener(jbtConfirm);
		
		functions01.setLayout(new GridLayout(1, 5));
		functions02.setLayout(new GridLayout(1, 5));
		functions03.setLayout(new GridLayout(1, 5));
		
		functions01.add(red01);
		functions01.add(blue01);
		functions01.add(green01);
		functions01.add(speed01);
		functions01.add(confirmSpeed01);
		functions02.add(red02);
		functions02.add(blue02);
		functions02.add(green02);
		functions02.add(speed02);
		functions02.add(confirmSpeed02);
		functions03.add(red03);
		functions03.add(blue03);
		functions03.add(green03);
		functions03.add(speed03);
		functions03.add(confirmSpeed03);
		
		add(functions01);
		add(car01);
		add(functions02);
		add(car02);
		add(functions03);
		add(car03);
	}
	
	private class JrbRed implements ActionListener {//Changes color(red)
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == red01) {
				car01.setColor(Color.RED);
			}
			if(e.getSource() == red02) {
				car02.setColor(Color.RED);
			}
			if(e.getSource() == red03) {
				car03.setColor(Color.RED);
			}
		}
	}
	
	private class JrbBlue implements ActionListener {//Changes color(blue)
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == blue01) {
				car01.setColor(Color.BLUE);
			}
			if(e.getSource() == blue02) {
				car02.setColor(Color.BLUE);
			}
			if(e.getSource() == blue03) {
				car03.setColor(Color.BLUE);
			}
		}
	}
	
	private class JrbGreen implements ActionListener {//Changes color(green)
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == green01) {
				car01.setColor(Color.GREEN);
			}
			if(e.getSource() == green02) {
				car02.setColor(Color.GREEN);
			}
			if(e.getSource() == green03) {
				car03.setColor(Color.GREEN);
			}
		}
	}
	
	private class JbtConfirm implements ActionListener {//Passes new FPS to timer
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == confirmSpeed01) {
				if(Integer.parseInt(speed01.getText()) <= 100) {
					car01.setSpeed(101 - Integer.parseInt(speed01.getText()));
				}
			}
			if(e.getSource() == confirmSpeed02) {
				if(Integer.parseInt(speed01.getText()) <= 100) {
					car02.setSpeed(101 - Integer.parseInt(speed02.getText()));
				}
			}
			if(e.getSource() == confirmSpeed03) {
				if(Integer.parseInt(speed01.getText()) <= 100) {
					car03.setSpeed(101 - Integer.parseInt(speed03.getText()));
				}
			}
		}
	}
	
	private JTextField speed01;
	private JTextField speed02;
	private JTextField speed03;
	private JRadioButton red01;
	private JRadioButton green01;
	private JRadioButton blue01;
	private JRadioButton red02;
	private JRadioButton green02;
	private JRadioButton blue02;
	private JRadioButton red03;
	private JRadioButton green03;
	private JRadioButton blue03;
	private JButton confirmSpeed01;
	private JButton confirmSpeed02;
	private JButton confirmSpeed03;
	private RaceCar car01;
	private RaceCar car02;
	private RaceCar car03;
}
