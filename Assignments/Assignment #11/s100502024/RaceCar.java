package a11.s100502024;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class RaceCar extends JPanel
{
	protected int delay = 100; // 設定初始值為100ms
	private int x = 0;
	private int y = 100;
	private int dx = 2;
	private Color color = new Color(0,0,0);
	private boolean direction = true;
	private Timer timer = new Timer(delay,new TimerListener());
	public RaceCar()
	{
		timer.start();
	}
	public void setspeed(int delay) // 設定速度
	{
		timer.setDelay(delay); // 設定TImer時間
	}
	public void setcolor(Color c)// 設定顏色
	{
		color = c;
	}
	private class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			repaint();
		}
	}
	protected void paintComponent(Graphics g) // 畫車子
	{
		super.paintComponent(g);
		g.fillOval(x,y+20,20,20);
		g.fillOval(x+40,y+20,20,20);
		g.setColor(color);
		g.fillRect(x,y,60,20);
		if(direction == true) // 向右走設為true
		{
			if(x < getWidth()-60)
			{
				x+=dx;
			}
			else
			{
				direction = false;
			}
		}
		if(direction == false) // 向左走設為false
		{
			if(x > 0)
			{
				x-=dx;
			}
			else
			{
				direction = true;
			}
		}
	}
}




