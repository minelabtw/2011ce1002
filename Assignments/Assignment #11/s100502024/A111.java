package a11.s100502024;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
public class A111 extends JApplet implements ActionListener
{
	private RaceCar Car1 = new RaceCar(); // 新增object
	private RaceCar Car2 = new RaceCar();
	private RaceCar Car3 = new RaceCar();
	private JPanel p1_1 = new JPanel(); // p*_1為整合Button的Panel
	private JPanel p1_2 = new JPanel(); // p*_2為整合RadioButton的Panel
	private JPanel p1_3 = new JPanel(); // p*_3為整合p*_1 & p*_2的Panel
	private JPanel p2_1 = new JPanel();
	private JPanel p2_2 = new JPanel();
	private JPanel p2_3 = new JPanel();
	private JPanel p3_1 = new JPanel();
	private JPanel p3_2 = new JPanel();
	private JPanel p3_3 = new JPanel();
	private JButton add_1 = new JButton("Speed Up"); // Button
	private JButton minus_1 = new JButton("Speed Down");
	private JButton add_2 = new JButton("Speed Up");
	private JButton minus_2 = new JButton("Speed Down");
	private JButton add_3 = new JButton("Speed Up");
	private JButton minus_3 = new JButton("Speed Down");
	private JRadioButton[] jrbRGB = new JRadioButton[9]; 
	ButtonGroup group_1 = new ButtonGroup();
	ButtonGroup group_2 = new ButtonGroup();
	ButtonGroup group_3 = new ButtonGroup();
	public A111()
	{
		p1_1.setLayout(new GridLayout(1,2));
		p2_1.setLayout(new GridLayout(1,2));
		p3_1.setLayout(new GridLayout(1,2));
		p1_1.add(add_1); // 新增Button到Panel上
		p1_1.add(minus_1);
		p2_1.add(add_2);
		p2_1.add(minus_2);
		p3_1.add(add_3);
		p3_1.add(minus_3);
		
		p1_2.setLayout(new GridLayout(3,1));
		p2_2.setLayout(new GridLayout(3,1));
		p3_2.setLayout(new GridLayout(3,1));
		p1_2.add(jrbRGB[0] = new JRadioButton("Red")); // 新增RadioButton到Panel上
		p1_2.add(jrbRGB[1] = new JRadioButton("Green"));
		p1_2.add(jrbRGB[2] = new JRadioButton("Blue"));
		group_1.add(jrbRGB[0]);
		group_1.add(jrbRGB[1]);
		group_1.add(jrbRGB[2]);
		
		p2_2.add(jrbRGB[3] = new JRadioButton("Red")); // 新增RadioButton到Panel上
		p2_2.add(jrbRGB[4] = new JRadioButton("Green"));
		p2_2.add(jrbRGB[5] = new JRadioButton("Blue"));
		group_2.add(jrbRGB[3]);
		group_2.add(jrbRGB[4]);
		group_2.add(jrbRGB[5]);
		
		p3_2.add(jrbRGB[6] = new JRadioButton("Red")); // 新增RadioButton到Panel上
		p3_2.add(jrbRGB[7] = new JRadioButton("Green"));
		p3_2.add(jrbRGB[8] = new JRadioButton("Blue"));
		group_3.add(jrbRGB[6]);
		group_3.add(jrbRGB[7]);
		group_3.add(jrbRGB[8]);
		
		p1_3.setLayout(new BorderLayout());
		p2_3.setLayout(new BorderLayout());
		p3_3.setLayout(new BorderLayout());  // 排版
		p1_3.add(Car1);
		p1_3.add(p1_1,BorderLayout.SOUTH); 
		p1_3.add(p1_2,BorderLayout.EAST);
		p2_3.add(Car2);
		p2_3.add(p2_1,BorderLayout.SOUTH);
		p2_3.add(p2_2,BorderLayout.EAST);
		p3_3.add(Car3);
		p3_3.add(p3_1,BorderLayout.SOUTH);
		p3_3.add(p3_2,BorderLayout.EAST);
		setLayout(new GridLayout(3,1));
		add(p1_3);
		add(p2_3);
		add(p3_3);
		add_1.addActionListener(this);  // ActionListeners handle
		minus_1.addActionListener(this);
		add_2.addActionListener(this);
		minus_2.addActionListener(this);
		add_3.addActionListener(this);
		minus_3.addActionListener(this);
		for(int i=0;i<9;i++)
		{
			jrbRGB[i].addActionListener(this);
		}
	}
	public void actionPerformed(ActionEvent e)  // 當按下Button時會做的事
	{
		if(e.getSource() == add_1)
		{
			Car1.delay-=5;
			Car1.setspeed(Car1.delay);
		}
		if(e.getSource() == minus_1)
		{
			Car1.delay+=5;
			Car1.setspeed(Car1.delay);
		}
		if(e.getSource() == add_2)
		{
			Car2.delay-=5;
			Car2.setspeed(Car2.delay);
		}
		if(e.getSource() == minus_2)
		{
			Car2.delay+=5;
			Car2.setspeed(Car2.delay);
		}
		if(e.getSource() == add_3)
		{
			Car3.delay-=5;
			Car3.setspeed(Car3.delay);
		}
		if(e.getSource() == minus_3)
		{
			Car3.delay+=5;
			Car3.setspeed(Car3.delay);
		}
		if(e.getSource() == jrbRGB[0])
		{
			Car1.setcolor(Color.RED);
			repaint();
		}
		if(e.getSource() == jrbRGB[1])
		{
			Car1.setcolor(Color.GREEN);
			repaint();
		}
		if(e.getSource() == jrbRGB[2])
		{
			Car1.setcolor(Color.BLUE);
			repaint();
		}
		if(e.getSource() == jrbRGB[3])
		{
			Car2.setcolor(Color.RED);
		}
		if(e.getSource() == jrbRGB[4])
		{
			Car2.setcolor(Color.GREEN);
		}
		if(e.getSource() == jrbRGB[5])
		{
			Car2.setcolor(Color.BLUE);
		}
		if(e.getSource() == jrbRGB[6])
		{
			Car3.setcolor(Color.RED);
		}
		if(e.getSource() == jrbRGB[7])
		{
			Car3.setcolor(Color.GREEN);
		}
		if(e.getSource() == jrbRGB[8])
		{
			Car3.setcolor(Color.BLUE);
		}
	}
}
