package a11.s100502008;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.LineBorder;

public class A111 extends JApplet implements ActionListener{
	JTextField t1=new JTextField();
	JTextField t2=new JTextField();
	JTextField t3=new JTextField();
	RaceCar car1 = new RaceCar();
	RaceCar car2 = new RaceCar();
	RaceCar car3 = new RaceCar();
	int v1,v2,v3;
	JRadioButton green1=new JRadioButton("Green");
	JRadioButton green2=new JRadioButton("Green");
	JRadioButton green3=new JRadioButton("Green");
	JRadioButton blue1=new JRadioButton("Blue");
	JRadioButton blue2=new JRadioButton("Blue");
	JRadioButton blue3=new JRadioButton("Blue");
	JRadioButton red1=new JRadioButton("Red");
	JRadioButton red2=new JRadioButton("Red");
	JRadioButton red3=new JRadioButton("Red");
	public A111()
	{
		JPanel p1=new JPanel(new GridLayout(3,1));
		car1.setBorder(new LineBorder(Color.BLACK,2));
		p1.add(car1);
		car2.setBorder(new LineBorder(Color.BLACK,2));
		p1.add(car2);
		car3.setBorder(new LineBorder(Color.BLACK,2));
		p1.add(car3);
		JPanel p2=new JPanel(new GridLayout(1,6));
		p2.add(new JLabel("Car 1 Delay:"));
		p2.add(t1);
		p2.add(new JLabel("Car 2 Delay:"));
		p2.add(t2);
		p2.add(new JLabel("Car 3 Delay:"));
		p2.add(t3);
		JPanel p3=new JPanel(new GridLayout(4,1));
		p3.add(new JLabel("Car 1 Color: "));
		p3.add(red1);
		p3.add(green1);
		p3.add(blue1);
		red1.setMnemonic('R');
		green1.setMnemonic('G');
		blue1.setMnemonic('B');
		red1.addActionListener(this);
		green1.addActionListener(this);
		blue1.addActionListener(this);
		JPanel p4=new JPanel(new GridLayout(4,1));
		p4.add(new JLabel("Car 2 Color: "));
		p4.add(red2);
		p4.add(green2);
		p4.add(blue2);
		red2.setMnemonic('R');
		green2.setMnemonic('G');
		blue2.setMnemonic('B');
		red2.addActionListener(this);
		green2.addActionListener(this);
		blue2.addActionListener(this);
		JPanel p5=new JPanel(new GridLayout(4,1));
		p5.add(new JLabel("Car 3 Color: "));
		p5.add(red3);
		p5.add(green3);
		p5.add(blue3);
		red3.addActionListener(this);
		green3.addActionListener(this);
		blue3.addActionListener(this);
		red3.setMnemonic('R');
		green3.setMnemonic('G');
		blue3.setMnemonic('B');
		JPanel p6=new JPanel(new GridLayout(3,1));
		p6.add(p3);
		p6.add(p4);
		p6.add(p5);
		red1.setSelected(true);
		red2.setSelected(true);
		red3.setSelected(true);
		JPanel p=new JPanel(new BorderLayout());
		p.add(p1,BorderLayout.CENTER);
		p.add(p2,BorderLayout.NORTH);
		p.add(p6,BorderLayout.WEST);
		add(p);
		t1.addActionListener(this);
		t2.addActionListener(this);
		t3.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==t1)
		{
			v1=Integer.parseInt(t1.getText());
			car1.setDelay(v1);
			repaint();
		}
		else if(e.getSource()==t2)
		{
			v2=Integer.parseInt(t2.getText());
			car2.setDelay(v2);
			repaint();
		}
		else if(e.getSource()==t3)
		{
			v3=Integer.parseInt(t3.getText());
			car3.setDelay(v3);
			repaint();
		}
		if(e.getSource()==red1)
		{
			car1.setColor(Color.RED);
			repaint();
			green1.setSelected(false);
			blue1.setSelected(false);
		}
		else if(e.getSource()==green1)
		{
			car1.setColor(Color.GREEN);
			repaint();
			red1.setSelected(false);
			blue1.setSelected(false);
		}
		else if(e.getSource()==blue1)
		{
			car1.setColor(Color.BLUE);
			repaint();
			red1.setSelected(false);
			green1.setSelected(false);
		}
		if(e.getSource()==red2)
		{
			car2.setColor(Color.RED);
			repaint();
			green2.setSelected(false);
			blue2.setSelected(false);
		}
		else if(e.getSource()==green2)
		{
			car2.setColor(Color.GREEN);
			repaint();
			red2.setSelected(false);
			blue2.setSelected(false);
		}
		else if(e.getSource()==blue2)
		{
			car2.setColor(Color.BLUE);
			repaint();
			red2.setSelected(false);
			green2.setSelected(false);
		}
		if(e.getSource()==red3)
		{
			car3.setColor(Color.RED);
			repaint();
			green3.setSelected(false);
			blue3.setSelected(false);
		}
		else if(e.getSource()==green3)
		{
			car3.setColor(Color.GREEN);
			repaint();
			red3.setSelected(false);
			blue3.setSelected(false);
		}
		else if(e.getSource()==blue3)
		{
			car3.setColor(Color.BLUE);
			repaint();
			red3.setSelected(false);
			green3.setSelected(false);
		}
	}
}
