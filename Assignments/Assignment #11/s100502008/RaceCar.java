package a11.s100502008;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class RaceCar extends JPanel{
	int delay;
	Color carColor;
	Timer timer= new Timer(delay,new TimerListener());
	int x=0;
	
	public RaceCar()
	{
		setDelay(100);
		setColor(Color.RED);
		timer.start();
	}
	void setDelay(int num)
	{
		delay=num;
		timer.setDelay(delay);
	}
	void setColor(Color c)
	{
		carColor=c;
	}
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		int y = getHeight() ;
		Polygon polygon = new Polygon();
		super.paintComponent(g);		
		if (x > getWidth()) {
			x = 0;
		}
		x += 3;
		g.setColor(Color.BLACK);
		g.fillOval(x + 10, y - 10, 10, 10);
		g.fillOval(x + 30, y - 10, 10, 10);
		g.setColor(carColor);
		g.fillRect(x, y - 20, 50, 10);
		polygon.addPoint(x + 10, y - 20);
		polygon.addPoint(x + 20, y - 30);
		polygon.addPoint(x + 30, y - 30);
		polygon.addPoint(x + 40, y - 20);
		g.fillPolygon(polygon);
	}
	class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			repaint();
		}
	}	
}
