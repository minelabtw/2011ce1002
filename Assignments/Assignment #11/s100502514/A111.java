/**
 * CE1002-100502514 FlashTeens Chiang
 * [The Following Rules are from ce1002 website]
 * Assignment 11-1
 * 
 * Please write an APPLET to meet the following requirements:
	
	Class RaceCar: JPanel Component
		A timer to control the speed of car
		A set method to set the speed of this car (the delay of the timer)
		A set method to set the color of this car
		A set method to set the style of this car(optional)
		An ActionListener to control drawing the JPanel
		
	Class A111: inherit Applet
		Create three cars with class RaceCar
		Three components (you can use button, textfield or combo box)
			to control the speed of the three cars
		A Radio Button component to set the color of this car
		Three components (you can use button, textfield or combo box)
			to control the style of the three cars (optional)
		ActionListeners to handle events of the three cars
	
	Write a html file to access your applet
		You have to save your html file at workspace/project/bin,
			in order to access the class file
	
 */
package a11.s100502514;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class A111 extends JApplet {

	/** UID */
	private static final long serialVersionUID = 1L;
	
	/** GUI members */
	RaceCar[] cars = new RaceCar[3];
	JPanel carGroup = new JPanel(new GridLayout(3, 1, 5, 5));
	JSlider[] controls_left = new JSlider[cars.length];
	JSlider[] controls_right = new JSlider[cars.length];
	boolean[] left_dragging = new boolean[cars.length];
	boolean[] right_dragging = new boolean[cars.length];
	
	Color[] color_options = {
			new Color(255, 0, 0, 153)/*red*/,
			new Color(255, 224, 0, 153)/*gold*/,
			new Color(0, 255, 0, 153)/*green*/,
			new Color(0, 0, 255, 153)/*blue*/,
			new Color(255, 102, 204, 153)/*pink*/
	};
	String[] color_labels = {"Red Devil", "Golden Age", "Freedom Express",
			"Cool Sapphire", "Pinky Gigantica"};
	JRadioButton[][] radioButtons = new JRadioButton[cars.length][color_labels.length];
	ButtonGroup[] radioBtnGroups = new ButtonGroup[cars.length];
	int defaultRadioSelection = 2;
	
	/** Timer Declaration */
	Timer sliderDetector;
	
	public A111(){
		/** init() will initialize this applet. */
	}
	public void init(){
		setLayout(new BorderLayout());
		for(int i=0; i<cars.length; i++){
			JPanel panelBoard = new JPanel(new BorderLayout());
			
			//Add car driver, which contains the scenery and acceleration bars.
			panelBoard.add(setAndGetNewCarDriver(i), BorderLayout.CENTER);
			
			//Add Radio buttons
			JPanel radioPanel = new JPanel(new GridLayout(color_labels.length, 1));
			radioBtnGroups[i] = new ButtonGroup();
			for(int j=0; j<color_labels.length; j++){
				//make a radio-button instance
				radioButtons[i][j] = new JRadioButton(color_labels[j]);
				//set the colors
				Color label_color = new Color(
						color_options[j].getRed(), color_options[j].getGreen(),
						color_options[j].getBlue(), 255
				);
				radioButtons[i][j].setForeground(label_color);
				radioButtons[i][j].setBackground(Color.black);
				//Add objects and listeners
				radioBtnGroups[i].add(radioButtons[i][j]);
				radioPanel.add(radioButtons[i][j]);
				radioButtons[i][j].addItemListener(radioListener);
			}
			radioButtons[i][defaultRadioSelection].setSelected(true);
			
			
			panelBoard.add(radioPanel, BorderLayout.EAST);
			
			carGroup.add(panelBoard);
			
			cars[i].setAccel(0);
		}
		add(carGroup);
		
		/** Implementing Timer Here */
		sliderDetector = new Timer(40, new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				for(int i=0; i<cars.length; i++){
					
					if(cars[i].isCrashed()){
						//Do nothing if crashed.
						controls_left[i].setEnabled(false);
						controls_right[i].setEnabled(false);
						continue;
					}
					
					if(cars[i].isStopped()){
						controls_left[i].setEnabled(true);
						controls_right[i].setEnabled(true);
					}
					
					JSlider control;
					int direction;
					if(!controls_right[i].isEnabled()){
						control = controls_left[i];
						direction = -1;
					}else if(!controls_left[i].isEnabled()){
						control = controls_right[i];
						direction = 1;
					}else{
						continue;
					}
					
					//Let the sliders gradually turning back to zero after dragging.
					if(!left_dragging[i] && !right_dragging[i]){
						double currentVal = control.getValue();
						double middleVal = 0;
						
						control.setValue(
							(int)(currentVal+Math.floor((middleVal-currentVal)/4)));
						
						// Turn the unused slider to zero
						if(cars[i].isGoingRight())controls_left[i].setValue(0); 
						else controls_right[i].setValue(0);
						
					}else{
						
						/** If found the direction of speed changes, then updates the data. */
						if(left_dragging[i] && cars[i].isGoingRight()){
							cars[i].goLeft();
						}else if(right_dragging[i] && cars[i].isGoingLeft()){
							cars[i].goRight();
						}
					}
					
					double halfRange = (control.getMaximum()-control.getMinimum())/2;
					cars[i].setAccel((double)(control.getValue()/halfRange)*0.002*direction);
					
				}
				
				repaint(); //Repaint after every event.
				
			}
		});
		sliderDetector.start();
	}
	
	private JPanel setAndGetNewCarDriver(int i){// i is between 0 and (cars.length-1)
		JPanel driver = new JPanel(new BorderLayout()); // inside panelBoard
		
		//Add display
		driver.add(cars[i] = new RaceCar(), BorderLayout.CENTER);
		
		//Add sliders
		driver.add(controls_left[i] = new JSlider(JSlider.VERTICAL), BorderLayout.WEST);
		driver.add(controls_right[i] = new JSlider(JSlider.VERTICAL), BorderLayout.EAST);
		controls_left[i].setMaximum(600);
		controls_left[i].setMinimum(-1000);
		controls_right[i].setMaximum(600);
		controls_right[i].setMinimum(-1000);
		
		controls_left[i].addMouseListener(sliderListener);
		controls_right[i].addMouseListener(sliderListener);
		
		left_dragging[i] = right_dragging[i] = false;
		return driver;
	}
	
	MouseAdapter sliderListener = new MouseAdapter(){
		@Override
		public void mousePressed(MouseEvent e) {
			for(int i=0; i<cars.length; i++){
				if(cars[i].isCrashed()){
					//Do nothing if crashed.
					continue;
				}
				if(e.getSource() == controls_left[i] &&
						(cars[i].isGoingLeft() || cars[i].isStopped())){
					cars[i].goLeft();
					controls_left[i].setEnabled(true);
					controls_right[i].setEnabled(false);

					controls_right[i].setValue(0); // Turn the unused slider to zero
					
					left_dragging[i] = true;
					break;
				}else if(e.getSource() == controls_right[i] &&
						(cars[i].isGoingRight() || cars[i].isStopped())){
					cars[i].goRight();
					controls_left[i].setEnabled(false);
					controls_right[i].setEnabled(true);
					
					controls_left[i].setValue(0); // Turn the unused slider to zero
					
					right_dragging[i] = true;
					break;
				}
			}
			
			((JComponent)(e.getSource())).repaint(); //Repaint after every event.
			
		}
		@Override
		public void mouseReleased(MouseEvent e) {
			for(int i=0; i<cars.length; i++){
				if(e.getSource() == controls_left[i]){
					left_dragging[i] = false;
					break;
				}else if(e.getSource() == controls_right[i]){
					right_dragging[i] = false;
					break;
				}
			}
			
			((JComponent)(e.getSource())).repaint(); //Repaint after every event.
			
		}
	};
	
	ItemListener radioListener = new ItemListener(){
		@Override
		public void itemStateChanged(ItemEvent e) {
			for(int i=0; i<cars.length; i++){
				for(int j=0; j<color_labels.length; j++){
					if(e.getSource() == radioButtons[i][j]){
						cars[i].setColor(color_options[j]);
						return;
					}
				}
			}
			
			((JComponent)(e.getSource())).repaint(); //Repaint after every event.
			
		}
	};
	
	public static void main(String[] args){
		JFrame win = new JFrame();
		A111 app = new A111();
		app.init();
		win.add(app);
		win.setSize(800,800);
		win.setLocationRelativeTo(null);
		win.setTitle("Race Car Test 2012.05.19");
		win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		win.setVisible(true);
	}
}
