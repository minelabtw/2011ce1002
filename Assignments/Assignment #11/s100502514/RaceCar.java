package a11.s100502514;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.Timer;

/** ALTER: The scene outside the car window. */
public class RaceCar extends JPanel {

	/** UID */
	private static final long serialVersionUID = 1L;
	
	/** Settable Properties */
	private boolean isGoingRight = true;   //Left or Right
	public boolean isGoingRight(){
		return isGoingRight;
	}
	public boolean isGoingLeft(){
		return !isGoingRight;
	}
	public void goLeft(){
		isGoingRight = false;
		
		//Reset the station timer if just moved from rest.
		if(currentSpeed==0)resetStationTimer();
		
		//Just to make the number non-zero, but very close to it.
		//In order not to be regarded as the "stopped" state.
		currentSpeed -= 1e-10;
		
		//stopped = false;
	}
	public void goRight(){
		isGoingRight = true;
		
		//Reset the station timer if just moved from rest.
		if(currentSpeed==0)resetStationTimer();
		
		//Just to make the number non-zero, but very close to it.
		//In order not to be regarded as the "stopped" state.
		currentSpeed += 1e-10; 
		//stopped = false;
	}
	public boolean isStopped(){
		return /*stopped &&*/ currentSpeed == 0;
	}
	public double getSpeedInKPH(){
		return Math.abs(currentSpeed*25*3.6);
	}
	/*public boolean isVerySlow(){ //If the speed < 0.5kph then return true.
		return getSpeedInKPH() < 0.5;
	}*/
	private double acceleration = 0;
	public double getAccel(){
		return acceleration;
	}
	public void setAccel(double accel){
		acceleration = accel;
	}
	public boolean isBoosting(){
		return (Math.abs(acceleration)>0.0002)&&(isGoingRight == (acceleration>0));
	}
	public boolean isBraking(){
		return (Math.abs(acceleration)>0.0002)&&(isGoingRight != (acceleration>0));
	}
	public void setColor(Color newColor){
		arrow.setColor(newColor);
	}
	
	/** Private Members */
	private double currentPosition = 50;   //In meter
	private double currentSpeed = 0;       //For each 90kph
	private double sceneryLoopUnit = 2000; //In meter
	private double maxDistantFromRoad = 70;//In meter
	private double cameraElavation = 1.2;  //In meter
	//private boolean stopped = true;
	private boolean crashed = false;
	public boolean isCrashed(){
		return crashed;
	}
	
	private ArrowAnimation arrow = new ArrowAnimation(); //Run in paintComponent()
	
	private long lastStopTime_UTC; //In ms
	public void resetStationTimer(){
		lastStopTime_UTC = new GregorianCalendar().getTimeInMillis();
	}
	public long getStationTimer(){
		return new GregorianCalendar().getTimeInMillis() - lastStopTime_UTC;
	}
	
	private double cameraSize = 200;
	//Tested and got the proper number above.
	
	private LinkedList<Person> objs = new LinkedList<Person>();
	
	//People images will be set in the Constructor.
	ImageIcon[] persons;
	
	//Set the image URL strings first.
	String[] URLs = {
			"images/woman1.png",
			"images/woman2.png",
			"images/man1.png",
			"images/man2.png"
	};
	
	//Set the scale ratio for a person.
	double scaleRatio;
	
	//Timer delay: 100 in Applet, or 40 in Frame
	int init_delay = 100, delay = 100;
	long currentUTC = new GregorianCalendar().getTimeInMillis();
	
	public RaceCar(){
		//Set People images
		persons = new ImageIcon[URLs.length];
		try{
			for(int i=0; i<URLs.length; i++){
				persons[i] = new ImageIcon(this.getClass().getResource(URLs[i]));
			}
		}catch(NullPointerException err){
			//If run in JFrame and throw an exception, then here will be run.
			for(int i=0; i<URLs.length; i++){
				persons[i] = new ImageIcon("src/a11/s100502514/"+URLs[i]);
			}
			delay = 40;
		}
		
		//Tested and got the proper number below.
		scaleRatio = (double)persons[0].getIconHeight()/(double)persons[0].getIconWidth();
		
		//Set Sky background
		setBackground(new Color(102, 153, 255));
		
		
		//Add People
		for(int i=0;i<2500;i++){
			objs.add(new Person(i));
		}
		
		//Add Timer
		Timer timer = new Timer(init_delay, new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				//Refresh delay first
				long newUTC = new GregorianCalendar().getTimeInMillis();
				delay = (int)(newUTC-currentUTC);
				currentUTC = new GregorianCalendar().getTimeInMillis();
				
				//boolean crashed = false; //Defined as a private member.
				
				if(crashed){ /** The car will lose control if crashed. */
					currentSpeed *= 0.995;
				}else if(/*!stopped ||*/ ((currentSpeed>0)==(isGoingRight))){
					currentSpeed += acceleration*(delay/40.0); //To make sure braking is fine.
				}else{
					//If the speed meets zero, then stop the car.
					currentSpeed = acceleration = 0;
					//stopped = true;
				}
				
				currentPosition+=currentSpeed*(delay/40.0);
				
				if(!(isGoingRight && currentSpeed>0) && !(!isGoingRight && currentSpeed<0)){
					currentSpeed = acceleration = 0;
				}
				
				
				//If the car goes out of boundaries and it will be crashed.
				if(!isGoingRight && currentPosition < -4){
					currentPosition = -4;
					//currentSpeed = acceleration = 0;
					currentSpeed *= -1;
					/*stopped = */crashed = true;
					goRight();
				}else if(isGoingRight && currentPosition > sceneryLoopUnit+4){
					currentPosition = sceneryLoopUnit+4;
					//currentSpeed = acceleration = 0;
					currentSpeed *= -1;
					/*stopped = */crashed = true;
					goLeft();
				}
				repaint();
				
			}
		});
		timer.start();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		
		//Draw Ground
		g.setColor(new Color(51, 204, 153)); //green
		g.fillRect(0, getHeight()/2, getWidth(), getHeight()/2);
		

		//Draw stations
		draw(g, new Station(0));
		draw(g, new Station(480, 40, 3));
		draw(g, new Station(960, 80));
		draw(g, new Station(1480, 40, 3));
		draw(g, new Station(sceneryLoopUnit-Station.defaultWidth));
		
		//Draw People
		Object[] objs = this.objs.toArray();
		Arrays.sort(objs, new Comparator(){
			@Override
			public int compare(Object arg0, Object arg1) {
				double z0 = Math.abs(((Person)(arg0)).z);
				double z1 = Math.abs(((Person)(arg1)).z);
				if(z0>z1)return -1;
				else if(z0<z1)return 1;
				else return 0;
			}
		});
		for(int i=0; i<objs.length; i++){
			draw(g, (Person)(objs[i]));
		}
		
		//Draw Top-left Messages
		g.setColor(Color.orange);
		g.setFont(new Font("Arial", 0, 20));
		g.drawString("Speed="+Math.rint(getSpeedInKPH()*10)/10+
				"kph", 10, 20);
		
		double display_distance = isGoingRight?
				(sceneryLoopUnit-currentPosition):(currentPosition);
		if(display_distance>=0){
			g.setColor(Color.yellow);
		}else{
			display_distance *= -1;
			g.setColor(Color.red);
		}
		g.drawString("Position="+Math.rint(display_distance*10)/10+"m",
				10, 40);
		
		//Draw Top-right Messages (Spending Time)
		g.setColor(Color.white);
		Font top_right_font = new Font("微軟正黑體", 0, 20);
		g.setFont(top_right_font);
		long station_timer = getStationTimer();
		String time_str = "花了"+station_timer/60000+"分"+
				(100+station_timer/1000%60+"").substring(1)+"秒"+
				(1000+station_timer%1000+"").substring(1);
		// The use like (1000+n+"").substring(1) is to make sure that three digits display.
		if(crashed){
			time_str = "緊急撤離!!";
			g.setColor(Color.red);
		}else if(isStopped()){
			time_str = "已停車";
		}
		g.drawString(time_str,
				getWidth()-(10+g.getFontMetrics(top_right_font).stringWidth(time_str)), 20);
		
		//Draw Arrow Messages
		draw(g, arrow);
		
		//Draw an alert if crashed
		if(crashed){
			g.setColor(new Color(255, 0, 0, 102));
			g.fillRect(0, 0, getWidth(), getHeight());
			g.setColor(Color.yellow);
			Font crash_msg_font = new Font("微軟正黑體", Font.BOLD, 30);
			g.setFont(crash_msg_font);
			String crash_msg = "請換一台車吧...";
			g.drawString(crash_msg,
					(getWidth()-g.getFontMetrics(crash_msg_font).stringWidth(crash_msg))/2,
					getHeight()/2
			);
		}
	}
	
	/** The following method is used in paintComponents() */
	private void draw(Graphics g, Person obj){
		int centerX = getWidth()/2, centerY = getHeight()/2;
		Person sb = obj;
		double x_3d = sb.x-currentPosition, y_3d = -cameraElavation, z_3d = sb.z;
		ImageIcon ico = persons[sb.image];
		Image img = ico.getImage();
		
		//2D-transformed position and size
		double scale = (double)ico.getIconHeight()/(double)ico.getIconWidth();
		double px = x_3d/z_3d*cameraSize, py = -(y_3d*scaleRatio)/z_3d*cameraSize;
		double ph = sb.height*cameraSize
				*scaleRatio/z_3d; //Private member scaleRatio
		double pw = sb.height*cameraSize
				*(scaleRatio/scale)/z_3d;
		
		g.drawImage(img, (int)(px+centerX), (int)(py+centerY-(ph*0.95)),
				(int)(pw), (int)(ph), this);
		/*
		g.setColor(Color.pink);
		Font msgFont = new Font("Arial", 0, (int)(ph/20));
		g.setFont(msgFont);
		String num_msg = "#"+sb.id;
		g.drawString(num_msg,
				(int)(px+centerX+(pw*0.55)-g.getFontMetrics(msgFont).stringWidth(num_msg)/2),
				(int)(py+centerY-(ph*0.5)));
		*/
	}
	private void draw(Graphics g, Station obj){
		int centerX = getWidth()/2, centerY = getHeight()/2;
		Station st = obj;
		for(int h=0; h<=1; h++){ //h=1 for ceiling
			double x_3d = (st.x-currentPosition)/scaleRatio,
					y_3d = -cameraElavation+h*st.ceiling, z_3d = 0.3/(cameraSize/200);
			int[] px_arr = new int[4], py_arr = new int[4];
			for(double m=0; m*scaleRatio<st.width; m++){
				for(int i=0; i<4; i++){
					px_arr[i] = (int)(((x_3d+m+(i/2)*1)
							/(z_3d+((i+1)/2%2)*st.depth))*cameraSize+centerX);
					py_arr[i] = (int)((-(y_3d)
							/(z_3d+((i+1)/2%2)*st.depth))*cameraSize+centerY);
				}
				g.setColor(new Color((int)(153+30.0*(m%3)/3), (int)(153+30.0*(m%4)/4),
						(int)(170+30.0*(m%5)/5)));
				g.fillPolygon(px_arr, py_arr, 4);
			}
		}
	}
	private void draw(Graphics g, ArrowAnimation a){
		a.refreshDirection(this); //Refresh the display via reading the direction
		g.setColor(a.getColor());
		
		int cycleMovement = (int)(60*(getStationTimer()%a.period)/a.period-30);
		int arrow_pos = getWidth()/2;
		switch(a.getDirection()){
		case 1: //Rightward
			arrow_pos += cycleMovement;
			g.fillRect(arrow_pos, 30, 40, 20);
			g.fillPolygon(
					new int[]{arrow_pos+40, arrow_pos+40, arrow_pos+60},
					new int[]{20, 60, 40}, 3);
			break;
		case -1: //Leftward
			arrow_pos -= cycleMovement-40;
			g.fillRect(arrow_pos, 30, 40, 20);
			g.fillPolygon(
					new int[]{arrow_pos, arrow_pos, arrow_pos-20},
					new int[]{20, 60, 40}, 3);
			break;
		default: //Stopped: a square sign
			g.fillRect(arrow_pos+10, 20, 40, 40);
		}
		
		//Message text on the arrow
		String accel_msg = "";
		if(crashed){
			accel_msg = "故障";
		}else if(a.getDirection()==0){
			accel_msg = "停車";
		}else if(isBoosting()){
			accel_msg = "加速";
		}else if(isBraking()){
			accel_msg = "煞車";
		}else{
			accel_msg = "空檔";
		}
		Font accel_msg_font = new Font("微軟正黑體", Font.BOLD, 16);
		g.setColor(new Color(255, 255, 255));
		g.setFont(accel_msg_font);
		g.drawString(accel_msg,
				arrow_pos + ((a.getDirection()==0) ? 30 : a.getDirection()*10+20)
					-g.getFontMetrics(accel_msg_font).stringWidth(accel_msg)/2,
				45);
	}
	
	/** The following inner classes are just for the scenery outside the car window. */
	class Person{
		int image, id; //which image for the person
		double x, z, height; //In meter
		public Person(double x, double z, double height, int id){
			this.x = x;
			this.z = z;
			this.height = height;
			this.image = (int)(Math.random()*persons.length);
			this.id = id;
			if(sceneryLoopUnit/2-Math.abs(x-sceneryLoopUnit/2)<150){
				this.z-=3.5;
			}
		}
		public Person(int id){
			this(Math.random()*(sceneryLoopUnit),
					Math.random()*(maxDistantFromRoad-5)+5,
					Math.pow((Math.random()-0.45)/0.55, 9)*0.9+1.65, id);
		}
	}
	class Station{
		double x, width, depth, ceiling;
		static final double defaultWidth = 150;
		public Station(double x, double width, double depth, double ceiling){
			this.x = x;
			this.width = width;
			this.depth = depth;
			this.ceiling = ceiling;
		}
		public Station(double x, double width, double depth){
			this(x, width, depth, 2.7);
		}
		public Station(double x, double width){
			this(x, width, 5, 2.7);
		}
		public Station(double x){
			this(x, defaultWidth, 5, 2.7);
		}
	}
	
	class ArrowAnimation{ // A lightning arrow that shows above the window
		
		int period; // period in ms
		
		private Color color; // Color object declaration
		
		public ArrowAnimation(int period, Color color){
			this.period = period;
			this.color = color;
		}
		public ArrowAnimation(int period){
			this(period, new Color(0, 255, 0, 153)); //lime green with 60% alpha
		}
		public ArrowAnimation(){
			this(2000);
		}
		
		public void setColor(Color _new){ // set a new color by class RaceCar
			color = _new;
		}
		public Color getColor(){ // get the arrow color
			return color;
		}
		
		private int direction; // 1 rightward or -1 leftward, or 0 stopped
		public int getDirection(){
			return direction;
		}
		
		public void refreshDirection(RaceCar ref){
			if(ref.isStopped()){
				direction = 0;
			}else if(ref.isGoingRight()){
				direction = 1;
			}else{
				direction = -1;
			}
		}
	}
}
