package a11.s100502501;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.*;
public class A111 extends JApplet implements ActionListener{
	private String[] speed={"20 (slow)","40","60","80","100 (fast)","stop"};
	RaceCar rc1=new RaceCar(); //create 3 RaceCar
	RaceCar rc2=new RaceCar();
	RaceCar rc3=new RaceCar();
	JPanel p1=new JPanel(new GridLayout(3,1)); //create panel
	JPanel p2=new JPanel(new GridLayout(3,1));
	JPanel p3=new JPanel(new GridLayout(3,1));
	JPanel p4=new JPanel(new GridLayout(3,1));
	JPanel p5=new JPanel(new GridLayout(3,1));
	JPanel p6=new JPanel(new BorderLayout());
	JPanel p7=new JPanel(new GridLayout(1,2));
	JPanel pcombo=new JPanel(new GridLayout(3,1)); 
	JPanel plabel=new JPanel(new GridLayout(3,1));
	JLabel l1=new JLabel("Speed1");
	JLabel l2=new JLabel("Speed2");
	JLabel l3=new JLabel("Speed3");
	JRadioButton jrb1=new JRadioButton("Red"); //create radio button
	JRadioButton jrb2=new JRadioButton("Blue");
	JRadioButton jrb3=new JRadioButton("Cyan");
	JRadioButton jrb4=new JRadioButton("Black");
	JRadioButton jrb5=new JRadioButton("Green");
	JRadioButton jrb6=new JRadioButton("Pink");
	JRadioButton jrb7=new JRadioButton("Yellow");
	JRadioButton jrb8=new JRadioButton("Purple");
	JRadioButton jrb9=new JRadioButton("Orange");
	private JComboBox jcbo=new JComboBox(speed); //create combo box
	private JComboBox jcbo1=new JComboBox(speed);
	private JComboBox jcbo2=new JComboBox(speed);
	public A111(){
		ButtonGroup group=new ButtonGroup(); //create button group
		ButtonGroup group1=new ButtonGroup(); //the button can be chosen one in a button group  
		ButtonGroup group2=new ButtonGroup();
		group.add(jrb1); //add radio button to button group 
		group.add(jrb2);
		group.add(jrb3);
		group1.add(jrb4);
		group1.add(jrb5);
		group1.add(jrb6);
		group2.add(jrb7);
		group2.add(jrb8);
		group2.add(jrb9);
		p1.add(jrb1);
		p1.add(jrb2);
		p1.add(jrb3);
		p2.add(jrb4);
		p2.add(jrb5);
		p2.add(jrb6);
		p3.add(jrb7);
		p3.add(jrb8);
		p3.add(jrb9);
		p4.add(p1);
		p4.add(p2);
		p4.add(p3);
		p5.add(rc1);
		p5.add(rc2);
		p5.add(rc3);
		pcombo.add(jcbo);
		pcombo.add(jcbo1);
		pcombo.add(jcbo2);
		plabel.add(l1);
		plabel.add(l2);
		plabel.add(l3);
		p7.add(plabel);
		p7.add(pcombo);
		p6.add(p4,BorderLayout.WEST);
		p6.add(p5,BorderLayout.CENTER);
		p6.add(p7,BorderLayout.EAST);
		add(p6); //add panel to frame
		
		
		jrb1.addActionListener(this); //active button event here
		jrb2.addActionListener(this);
		jrb3.addActionListener(this);
		jrb4.addActionListener(this);
		jrb5.addActionListener(this);
		jrb6.addActionListener(this);
		jrb7.addActionListener(this);
		jrb8.addActionListener(this);
		jrb9.addActionListener(this);
		
		jcbo.addItemListener(new ItemListener(){
		    public void itemStateChanged(ItemEvent e) { //set the velocity of car1
		    	if(jcbo.getSelectedIndex()==0){
		        	rc1.setV(20);
		        }
		        if(jcbo.getSelectedIndex()==1){
		        	rc1.setV(40);
		        }
		        if(jcbo.getSelectedIndex()==2){
		        	rc1.setV(60);
		        }
		        if(jcbo.getSelectedIndex()==3){
		        	rc1.setV(80);
		        }
		        if(jcbo.getSelectedIndex()==4){
		        	rc1.setV(100);
		        }
		        if(jcbo.getSelectedIndex()==5){
		        	rc1.setV(0);
		        }
		    }
		});
		jcbo1.addItemListener(new ItemListener(){
		    public void itemStateChanged(ItemEvent e) { //set the velocity of car2
		    	if(jcbo1.getSelectedIndex()==0){
			       	rc2.setV(20);
			    }
		    	if(jcbo1.getSelectedIndex()==1){
			       	rc2.setV(40);
		    	}
			    if(jcbo1.getSelectedIndex()==2){
			       	rc2.setV(60);
			    }
			    if(jcbo1.getSelectedIndex()==3){
		        	rc2.setV(80);
			    }
			    if(jcbo1.getSelectedIndex()==4){
			       	rc2.setV(100);
			    }
			    if(jcbo1.getSelectedIndex()==5){
			        rc2.setV(0);
			    }
		    }
		});
		jcbo2.addItemListener(new ItemListener(){
		    public void itemStateChanged(ItemEvent e) { //set the velocity of car3
		    	if(jcbo2.getSelectedIndex()==0){
			        rc3.setV(20);
			    }
			    if(jcbo2.getSelectedIndex()==1){
			        rc3.setV(40);
			    }
			    if(jcbo2.getSelectedIndex()==2){
			    	rc3.setV(60);
			    }
			    if(jcbo2.getSelectedIndex()==3){
			        rc3.setV(80);
			    }
			    if(jcbo2.getSelectedIndex()==4){
			        rc3.setV(100);
			    }
			    if(jcbo2.getSelectedIndex()==5){
			        rc3.setV(0);
			    }
		    }
		});

	}
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==jrb1){ //set the color of car1
			rc1.setColor(Color.RED);
		}
		if(e.getSource()==jrb2){
			rc1.setColor(Color.BLUE);
		}
		if(e.getSource()==jrb3){
			rc1.setColor(Color.CYAN);
		}
		if(e.getSource()==jrb4){ //set the color of car2
			rc2.setColor(Color.BLACK);
		}
		if(e.getSource()==jrb5){
			rc2.setColor(new Color(30,145,70));
		}
		if(e.getSource()==jrb6){
			rc2.setColor(new Color(255,130,220));
		}
		if(e.getSource()==jrb7){ //set the color of car3
			rc3.setColor(new Color(255,255,128));
		}
		if(e.getSource()==jrb8){
			rc3.setColor(new Color(105,0,210));
		}
		if(e.getSource()==jrb9){
			rc3.setColor(new Color(255,128,0));
		}
		
	}
}
