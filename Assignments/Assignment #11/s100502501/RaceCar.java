package a11.s100502501;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
public class RaceCar extends JPanel{
	private Color c1=Color.gray;
	private int v1=20;
	private int x;
	private int y;
	public RaceCar(){
		Timer timer=new Timer(100,new TimerListener()); //create a timer
		timer.start();
	}
	public void setColor(Color c){
		c1=c;
	}
	public void setV(int v){
		v1=v;
	}	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g); //draw things in superclass
		y=getHeight()/2; //set car at half height
	
		Polygon plg=new Polygon();
		plg.addPoint(x+20,y-30); //draw the car
		plg.addPoint(x+30,y-30);
		plg.addPoint(x+40,y-20);
		plg.addPoint(x+50,y-20);
		plg.addPoint(x+50,y-10);
		plg.addPoint(x,y-10);
		plg.addPoint(x,y-20);
		plg.addPoint(x+10,y-20);
		g.setColor(Color.black);
		g.fillArc(x+10,y-10,10,10,0,360); //draw the wheel
		g.fillArc(x+30,y-10,10,10,0,360);
		g.setColor(c1);
		g.fillPolygon(plg);
		
		x+=v1;
		if(x>=getWidth()-50){
			x=0;
		}
	}
	class TimerListener implements ActionListener {
	      public void actionPerformed(ActionEvent e) { //handle actionevent
	        repaint();
	      }
	}
}
