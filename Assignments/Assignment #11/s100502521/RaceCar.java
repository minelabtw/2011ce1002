package a11.s100502521;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class RaceCar extends JPanel
{
	private Timer time;
	private Color carColor=Color.BLUE;
	private int carX=0,carY=0,width;
	public int style=0;
	public RaceCar()
	{
		time=new Timer(100,new TimerListener());
	}
	public void setSpead(int v)
	{
		time.setDelay(v);
	}
	public void setColor(Color p)
	{
		carColor=p;
		repaint();
	}
	public void setStyle(int i)
	{
		style=i;
		repaint();
	}
	public void run(boolean open)
	{
		if(open)
		{
			time.start();
		}
		else
		{
			time.stop();
		}
		repaint();
	}
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		switch(style)
		{
		case 0:
			g.setColor(Color.ORANGE);
			g.drawOval(carX+3,carY+120, 20, 20);
			g.fillOval(carX+3,carY+120, 20, 20);
			g.drawOval(carX+37,carY+120, 20, 20);
			g.fillOval(carX+37,carY+120, 20, 20);
			g.setColor(carColor);
			g.drawRect(carX+0,carY+100,60, 30);
			g.fillRect(carX+0,carY+100,60, 30);
			break;
		case 1:
			g.setColor(Color.ORANGE);
			g.drawOval(carX+10,carY+130, 20, 20);
			g.fillOval(carX+10,carY+130, 20, 20);
			g.setColor(carColor);
			int []x=new int[]{carX+20,carX+40,carX};
			int []y=new int[]{carY+130,carY+140,carY+140};
			g.drawPolygon(x, y, 3);
			g.fillPolygon(x, y, 3);
			break;
		case 2:
			g.setColor(carColor);
			g.drawOval(carX+0,carY+100,60, 60);
			g.fillOval(carX+0,carY+100,60, 60);
			break;
		default:
			break;
		}
		width=getWidth();
		g.setColor(Color.BLACK);
		g.drawRect(0,0,getWidth()-1, getHeight());
	}
	class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			carX++;
			if(carX>=width)
			{
				carX=-50;
			}
			repaint();
		}
	}
}
