package a11.s100502521;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.Timer;

public class A111 extends JApplet implements ActionListener,ItemListener
{
	private JPanel []controlPanel=new JPanel[3];
	private JPanel []controlSpeed=new JPanel[3];
	private JPanel []controlColor=new JPanel[3];
	private JPanel []controlStyle=new JPanel[3];
	private JButton []speed=new JButton[3];
	private TextField []t_speed=new TextField[3];
	private JRadioButton []color=new JRadioButton[9];
	private JComboBox []style=new JComboBox[3];
	private String []names=new String[]{"Car","UFO","ball"};
	private JLabel []text=new JLabel[3];
	private JLabel []text2=new JLabel[3];
	private JLabel []text3=new JLabel[3];
	private ButtonGroup []radiogroup=new ButtonGroup[3]; 
	private RaceCar []car=new RaceCar[3];
	public A111()
	{
		setLayout(new GridLayout(3,2));
		for(int i=0;i<3;i++)
		{
			controlPanel[i]=new JPanel();
			controlPanel[i].setLayout(new GridLayout(3,1));
			controlSpeed[i]=new JPanel();
			controlSpeed[i].setLayout(new GridLayout(1,5));
			speed[i]=new JButton("確定");
			speed[i].addActionListener(this);
			t_speed[i]=new TextField("");
			text[i]=new JLabel("第"+(i+1)+"台車       ");
			text3[i]=new JLabel("請輸入間格時間:");
			text2[i]=new JLabel("ms");
			controlSpeed[i].add(text[i]);
			controlSpeed[i].add(text3[i]);
			controlSpeed[i].add(t_speed[i]);
			controlSpeed[i].add(text2[i]);
			controlSpeed[i].add(speed[i]);
			controlColor[i]=new JPanel();
			controlColor[i].setLayout(new GridLayout(1,3));
			controlStyle[i]=new JPanel();
			controlStyle[i].setLayout(new GridLayout(1,1));
			style[i]=new JComboBox(names);
			style[i].addItemListener(this);
			controlStyle[i].add(style[i]);
			radiogroup[i]=new ButtonGroup();
			car[i]=new RaceCar();
		}
		for(int i=0;i<9;i++)
		{
			switch(i%3)
			{
			case 0:
				color[i]=new JRadioButton("BLUE");
				break;
			case 1:
				color[i]=new JRadioButton("Green");
				break;
			case 2:
				color[i]=new JRadioButton("Red");
				break;
			}
			color[i].addItemListener(this);
			radiogroup[i/3].add(color[i]);
			controlColor[i/3].add(color[i]);
		}
		for(int i=0;i<3;i++)
		{
			controlPanel[i].add(controlSpeed[i]);
			controlPanel[i].add(controlColor[i]);
			controlPanel[i].add(controlStyle[i]);
		}
		for(int i=0;i<3;i++)
		{
			add(car[i]);
			add(controlPanel[i]);
		}
	}
	public void actionPerformed(ActionEvent e) 
	{
		for(int i=0;i<3;i++)
		{
			if(e.getSource()==speed[i])
			{
				if(Integer.parseInt(t_speed[i].getText())<=0)
				{
					car[i].run(false);
				}
				else
				{
					car[i].setSpead(Integer.parseInt(t_speed[i].getText()));
					car[i].run(true);
				}
			}
		}
	}
	public void itemStateChanged(ItemEvent e) 
	{
		for(int i=0;i<3;i++)
		{
			if(e.getSource()==style[i])
			{
				car[i].setStyle(style[i].getSelectedIndex());
			}
		}
		if (e.getStateChange() == ItemEvent.SELECTED) 
		{
			for(int i=0;i<9;i++)
			{
				if(e.getItem()==color[i])
				{
					Color setting=Color.BLACK;
					switch(i%3)
					{
					case 0:
						setting=Color.blue;
						break;
					case 1:
						setting=Color.green;
						break;
					case 2:
						setting=Color.RED;
						break;
					}
					car[i/3].setColor(setting);
				}
			}
		}
	}
}
