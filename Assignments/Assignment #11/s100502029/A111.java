package a11.s100502029;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

public class A111 extends JApplet {
	private static RaceCar[] car = new RaceCar[3]; // create three cars
	
	// create three text fields to let user can input speed
	private JTextField jtfCarSpeed0 = new JTextField(8);
	private JTextField jtfCarSpeed1 = new JTextField(8);
	private JTextField jtfCarSpeed2 = new JTextField(8);
	
	// create three buttons to change the style
	private JButton jbtChangeStyle0 = new JButton("Truck ");
	private JButton jbtChangeStyle1 = new JButton("to UFO");
	private JButton jbtChangeStyle2 = new JButton("Plane ");
	
	// create three flag to 判斷車子是否變形了, true represent car is not changed, false is changed
	private Boolean flag0 = true;
	private Boolean flag1 = true;
	private Boolean flag2 = true;

	public A111() {
		Border lineBorder = new LineBorder(new Color(49, 187, 253), 2);
		Font font1 = new Font("TimesRoman", Font.BOLD, 13); // jlabel's font
		Font font2 = new Font("TimesRoman", Font.BOLD, 17);// jbutton's font
		
		// create three button group
		ButtonGroup group0 = new ButtonGroup();
		ButtonGroup group1 = new ButtonGroup();
		ButtonGroup group2 = new ButtonGroup();
		
		// create three jlabel to display the message
		JLabel jlblCarSpeed0 = new JLabel("1st Car Speed:");
		JLabel jlblCarSpeed1 = new JLabel("2nd Car Speed:");
		JLabel jlblCarSpeed2 = new JLabel("3rd Car Speed:");
		
		JLabel jlblTitle = new JLabel("Change Style");
		
		// create three sets of RGB buttons
		JRadioButton[] jrbRed = new JRadioButton[3];
		JRadioButton[] jrbGreen = new JRadioButton[3];
		JRadioButton[] jrbBlue = new JRadioButton[3];
		
		// create three jpanel to put each RGB buttons
		JPanel jpRadioButtons0 = new JPanel(new GridLayout(3, 1));
		JPanel jpRadioButtons1 = new JPanel(new GridLayout(3, 1));
		JPanel jpRadioButtons2 = new JPanel(new GridLayout(3, 1));
		
		// create the jpanel to put three sets of RGB buttons
		JPanel jpAllColorButtons = new JPanel(new GridLayout(3, 1));
		
		// create the jpanel to put three car
		JPanel jpThreeCars = new JPanel(new GridLayout(3, 1));
		
		// create the panel to put three change buttons
		JPanel jpThreeChangeButtons = new JPanel(new GridLayout(3, 1));
		
		// create three panel to put each set speed jlabel and jtextfield
		JPanel jpEachSetSpeed0 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JPanel jpEachSetSpeed1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		JPanel jpEachSetSpeed2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		
		// create the jpanel to put three sets of set speed
		JPanel jpAllSetSpeed = new JPanel(new GridLayout(1, 3));
		
		// create the panel to put all set speed panel and a jlabel
		JPanel jpAllSetSpeedAndTransformer = new JPanel(new BorderLayout());

		// set "change style" label's border, font, and set change buttons background color, font
		jlblTitle.setBorder(lineBorder);
		jbtChangeStyle0.setBackground(new Color(49, 187, 253));
		jbtChangeStyle1.setBackground(new Color(49, 187, 253));
		jbtChangeStyle2.setBackground(new Color(49, 187, 253));
		jlblTitle.setFont(font1);
		jbtChangeStyle0.setFont(font2);
		jbtChangeStyle1.setFont(font2);
		jbtChangeStyle2.setFont(font2);
		
		// initialize three car and three set of RGB　buttons
		for (int i = 0; i < 3; i++) {
			car[i] = new RaceCar();
			jrbRed[i] = new JRadioButton("Red");
			jrbGreen[i] = new JRadioButton("Green");
			jrbBlue[i] = new JRadioButton("Blue");
		}
		
		// add RGB buttons to jpRadioButtons
		jpRadioButtons0.add(jrbRed[0]);
		jpRadioButtons0.add(jrbGreen[0]);
		jpRadioButtons0.add(jrbBlue[0]);
		jpRadioButtons0.setBorder(lineBorder);
		jpRadioButtons1.add(jrbRed[1]);
		jpRadioButtons1.add(jrbGreen[1]);
		jpRadioButtons1.add(jrbBlue[1]);
		jpRadioButtons1.setBorder(lineBorder);
		jpRadioButtons2.add(jrbRed[2]);
		jpRadioButtons2.add(jrbGreen[2]);
		jpRadioButtons2.add(jrbBlue[2]);
		jpRadioButtons2.setBorder(lineBorder);
		
		// add RGB buttons to button group
		group0.add(jrbRed[0]);
		group0.add(jrbGreen[0]);
		group0.add(jrbBlue[0]);
		group1.add(jrbRed[1]);
		group1.add(jrbGreen[1]);
		group1.add(jrbBlue[1]);
		group2.add(jrbRed[2]);
		group2.add(jrbGreen[2]);
		group2.add(jrbBlue[2]);
		
		// initialize the selected
		jrbRed[0].setSelected(true);
		car[0].setColor(Color.RED);
		jrbGreen[1].setSelected(true);
		car[1].setColor(Color.GREEN);
		jrbBlue[2].setSelected(true);
		car[2].setColor(Color.BLUE);

		// add jpRadioButtons to jpAllColorButtons
		jpAllColorButtons.add(jpRadioButtons0);
		jpAllColorButtons.add(jpRadioButtons1);
		jpAllColorButtons.add(jpRadioButtons2);
		
		// add each jlabel and jtextfield to correspoding jpEachSetSpeed
		jpEachSetSpeed0.add(jlblCarSpeed0);
		jpEachSetSpeed0.add(jtfCarSpeed0);
		jpEachSetSpeed0.setBorder(lineBorder);
		jpEachSetSpeed1.add(jlblCarSpeed1);
		jpEachSetSpeed1.add(jtfCarSpeed1);
		jpEachSetSpeed1.setBorder(lineBorder);
		jpEachSetSpeed2.add(jlblCarSpeed2);
		jpEachSetSpeed2.add(jtfCarSpeed2);
		jpEachSetSpeed2.setBorder(lineBorder);
		
		// add jpEachSetSpeed to jpAllSetSpeed
		jpAllSetSpeed.add(jpEachSetSpeed0);
		jpAllSetSpeed.add(jpEachSetSpeed1);
		jpAllSetSpeed.add(jpEachSetSpeed2);
		
		jpAllSetSpeedAndTransformer.add(jpAllSetSpeed, BorderLayout.CENTER);
		jpAllSetSpeedAndTransformer.add(jlblTitle, BorderLayout.EAST);
		
		// add cars to jpThreeCar
		for (int i = 0; i < 3; i++) {
			jpThreeCars.add(car[i]);
		}
		
		// add change buttons to jpThreeChangeButtons
		jpThreeChangeButtons.add(jbtChangeStyle0);
		jpThreeChangeButtons.add(jbtChangeStyle1);
		jpThreeChangeButtons.add(jbtChangeStyle2);
		
		// combine all thing to japplet
		add(jpThreeCars, BorderLayout.CENTER);
		add(jpAllColorButtons, BorderLayout.WEST);
		add(jpThreeChangeButtons, BorderLayout.EAST);
		add(jpAllSetSpeedAndTransformer, BorderLayout.NORTH);
		
		// set actionlistener of RGB buttons
		jrbRed[0].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car[0].setColor(Color.RED); // set car's color
			}
		});
		jrbGreen[0].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car[0].setColor(Color.GREEN);
			}
		});
		jrbBlue[0].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car[0].setColor(Color.BLUE);
			}
		});
		jrbRed[1].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car[1].setColor(Color.RED);
			}
		});
		jrbGreen[1].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car[1].setColor(Color.GREEN);
			}
		});
		jrbBlue[1].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car[1].setColor(Color.BLUE);
			}
		});
		jrbRed[2].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car[2].setColor(Color.RED);
			}
		});
		jrbGreen[2].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car[2].setColor(Color.GREEN);
			}
		});
		jrbBlue[2].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car[2].setColor(Color.BLUE);
			}
		});
		
		// set actionlistener to "set speed text field"
		jtfCarSpeed0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String text = jtfCarSpeed0.getText();
				int speed = Integer.parseInt(text);
				car[0].setSpeed(speed);
				jtfCarSpeed0.setText(""); // clear the text field
			}
		});
		jtfCarSpeed1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String text = jtfCarSpeed1.getText();
				int speed = Integer.parseInt(text);
				car[1].setSpeed(speed);
				jtfCarSpeed1.setText("");
			}
		});
		jtfCarSpeed2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String text = jtfCarSpeed2.getText();
				int speed = Integer.parseInt(text);
				car[2].setSpeed(speed);
				jtfCarSpeed2.setText("");
			}
		});
		
		// set "change style buttons" actionlitener
		jbtChangeStyle0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// check it if changed or not
				if (flag0 == true) {
					car[0].setStyle(1); // change to style 1
					jbtChangeStyle0.setText("to CAR"); // change the message on the button
					flag0 = false;
				}
				else {
					car[0].setStyle(0);
					jbtChangeStyle0.setText("Truck ");
					flag0 = true;
				}
			}
		});
		jbtChangeStyle1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (flag1 == true) {
					car[1].setStyle(2);
					jbtChangeStyle1.setText("to CAR");
					flag1 = false;
				}
				else {
					car[1].setStyle(0);
					jbtChangeStyle1.setText("to UFO");
					flag1 = true;
				}
			}
		});
		jbtChangeStyle2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (flag2 == true) {
					car[2].setStyle(3);
					jbtChangeStyle2.setText("to CAR");
					flag2 = false;
				}
				else {
					car[2].setStyle(0);
					jbtChangeStyle2.setText("Plane ");
					flag2 = true;
				}
			}
		});
	}
}
