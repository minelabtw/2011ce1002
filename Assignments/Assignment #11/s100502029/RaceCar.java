package a11.s100502029;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class RaceCar extends JPanel {
	private int x;
	private int y;
	private Color color; // car's color
	private int style = 0; // car's style, 0 is car, 1 is truck, 2 is UFO, 3 is plane
	private boolean flag = true; // if flag's value changed, means car's move direction changed
	private Timer timer; // create a timer
	
	public RaceCar() {
		timer = new Timer(50, new TimerListener()); // initialize the timer
		timer.start(); // start the timer
	}
	
	// use set delay to change car's speed
	public void setSpeed(int delay) {
		timer.setDelay(delay);
	}
	
	// set car's color
	public void setColor(Color color) {
		this.color = color;
	}
	
	// set car's style
	public void setStyle(int style) {
		this.style = style;
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(color); // set car's color
		
		// draw a car
		if (style == 0) {
			y = getHeight();
			Polygon polygon1 = new Polygon();
			polygon1.addPoint(x + 10, y - 20);
			polygon1.addPoint(x + 20, y - 30);
			polygon1.addPoint(x + 30, y - 30);
			polygon1.addPoint(x + 40, y - 20);
			g.fillPolygon(polygon1);
			
			Polygon polygon2 = new Polygon();
			polygon2.addPoint(x, y - 10);
			polygon2.addPoint(x, y - 20);
			polygon2.addPoint(x + 50, y - 20);
			polygon2.addPoint(x + 50, y - 10);
			g.fillPolygon(polygon2);
			
			g.fillArc(x + 10, y - 10, 10, 10, 0, 360);
			g.fillArc(x + 30, y - 10, 10, 10, 0, 360);
			
			// right direction
			if (flag == true) {
				x += 2;
				if (x > getWidth() - 50)
					flag = false;
			}
			
			// left direction
			if (flag == false) {
				x -= 2;
				if (x < 0)
					flag = true;
			}
		}
		
		// draw a truck
		if (style == 1) {
			y = getHeight();
			
			// draw a right direction truck
			if (flag == true) {
				Polygon polygon1 = new Polygon();
				polygon1.addPoint(x, y - 20);
				polygon1.addPoint(x, y - 35);
				polygon1.addPoint(x + 40, y - 35);
				polygon1.addPoint(x + 40, y - 55);
				polygon1.addPoint(x + 60, y - 55);
				polygon1.addPoint(x + 70, y - 35);
				polygon1.addPoint(x + 70, y - 20);
				g.fillPolygon(polygon1);
				
				g.fillArc(x + 5, y - 20, 20, 20, 0, 360);
				g.fillArc(x + 40, y - 20, 20, 20, 0, 360);
				
				g.setColor(Color.WHITE);
				g.fillRect(x + 45, y - 50, 5, 15);
				g.fillArc(x + 40, y - 50, 20, 30, 0, 90);
				
				x += 2;
				if (x > getWidth() - 70)
					flag = false;
			}
			
			// draw a left direction truck
			if (flag == false) {
				Polygon polygon1 = new Polygon();
				polygon1.addPoint(x + 70, y - 20);
				polygon1.addPoint(x + 70, y - 35);
				polygon1.addPoint(x + 30, y - 35);
				polygon1.addPoint(x + 30, y - 55);
				polygon1.addPoint(x + 10, y - 55);
				polygon1.addPoint(x, y - 35);
				polygon1.addPoint(x, y - 20);
				g.fillPolygon(polygon1);
				
				g.fillArc(x + 10, y - 20, 20, 20, 0, 360);
				g.fillArc(x + 45, y - 20, 20, 20, 0, 360);
				
				g.setColor(Color.WHITE);
				g.fillRect(x + 20, y - 50, 5, 15);
				g.fillArc(x + 10, y - 50, 20, 30, 90, 90);
				
				x -= 2;
				if (x < 0)
					flag = true;
			}
		}
		
		// draw a UFO
		if (style == 2) {
			y = getHeight();
			Polygon polygon1 = new Polygon();
			polygon1.addPoint(x, y - 70);
			polygon1.addPoint(x + 10, y - 80);
			polygon1.addPoint(x + 60, y - 80);
			polygon1.addPoint(x + 70, y - 70);
			g.fillPolygon(polygon1);
			
			g.fillArc(x + 14, y - 95, 42, 30, 0, 180);
			
			g.setColor(Color.BLACK);
			g.drawLine(x + 10, y - 80, x + 60, y - 80);
			g.drawLine(x + 35, y - 67, x + 35, y - 57);
			g.drawLine(x + 15, y - 67, x + 7, y - 57);
			g.drawLine(x + 55, y - 67, x + 63, y - 57);
			g.drawPolygon(polygon1);
			g.drawArc(x + 14, y - 95, 42, 30, 0, 180);
			
			if (flag == true) {
				x += 2;
				if (x > getWidth() - 50)
					flag = false;
			}
			
			if (flag == false) {
				x -= 2;
				if (x < 0)
					flag = true;
			}
		}
		
		// draw a plane
		if (style == 3) {
			Polygon polygon1 = new Polygon();
			polygon1.addPoint(x, y - 85);
			polygon1.addPoint(x, y - 120);
			polygon1.addPoint(x + 10, y - 100);
			polygon1.addPoint(x + 65, y - 100);
			polygon1.addPoint(x + 65, y - 85);
			g.fillPolygon(polygon1);
			
			Polygon polygon2 = new Polygon();
			polygon2.addPoint(x + 30, y - 100);
			polygon2.addPoint(x + 40, y - 130);
			polygon2.addPoint(x + 45, y - 130);
			polygon2.addPoint(x + 45, y - 100);
			g.fillPolygon(polygon2);
			
			Polygon polygon3 = new Polygon();
			polygon3.addPoint(x + 30, y - 85);
			polygon3.addPoint(x + 45, y - 85);
			polygon3.addPoint(x + 45, y - 50);
			polygon3.addPoint(x + 40, y - 50);
			g.fillPolygon(polygon3);
			
			g.fillArc(x + 50, y - 100, 30, 30, 0, 90);
			
			g.setColor(Color.WHITE);
			g.fillArc(x + 50, y - 97, 20, 16, 0, 90);
			g.setColor(Color.BLACK);
			g.drawArc(x + 50, y - 97, 20, 16, 0, 90);
			g.drawLine(x + 60, y - 97, x + 60, y - 89);
			g.drawLine(x + 60, y - 89, x + 70, y - 89);
			
			for (int i = 0; i < 10; i++) {
				g.setColor(Color.WHITE);
				g.fillRect(x + 55 - 5 * i, y - 97, 3, 8);
				g.setColor(Color.BLACK);
				g.drawRect(x + 55 - 5 * i, y - 97, 3, 8);
			}
			
			x += 2;
			
			// if plane go through the right wall, then the plane continue to fly from left wall
			if (x >= getWidth())
				x = -80;
		}
		
		// draw panel's border
		g.setColor(new Color(49, 187, 253));
		g.drawLine(0, 0, getWidth() - 1, 0);
		g.drawLine(getWidth() - 1, 0, getWidth() - 1, getHeight() - 1);
		g.drawLine(getWidth() - 1, getHeight() - 1, 0, getHeight() - 1);
		g.drawLine(0, getHeight() - 1, 0, 0);
	}
	
	// set timer actionlistener
	public class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			repaint(); // repaint the car
		}
	}
}
