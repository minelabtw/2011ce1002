package a11.s100502018;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class RaceCar extends JPanel {
	private int delay;
	private Color carColor;
	Timer timer = new Timer(delay, new TimerListener());
	private int xCoordinate = 0;
	private int direction = 0;

	public RaceCar() { //initial the car's delay and color
		setDelay(100);
		setColor(Color.RED);
		timer.start();
	}

	public void setDelay(int d) { //set the delay
		delay = d;
		timer.setDelay(d);
	}

	public void setColor(Color c) { //set the color
		carColor = c;
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int yCoordinate = getHeight() / 2;
		if (direction % 2 == 0) { //car will turn if it touch the wall
			if (xCoordinate + 50 < getWidth()) {
				xCoordinate += 5;
			} else {
				xCoordinate -= 5;
				direction++;
			}
		} else {
			if (xCoordinate != 0) {
				xCoordinate -= 5;
			} else {
				xCoordinate += 5;
				direction++;
			}
		}
		Polygon carbody = new Polygon();
		g.setColor(carColor);
		carbody.addPoint(xCoordinate, yCoordinate);
		carbody.addPoint(xCoordinate + 50, yCoordinate);
		carbody.addPoint(xCoordinate + 50, yCoordinate - 10);
		carbody.addPoint(xCoordinate + 40, yCoordinate - 10);
		carbody.addPoint(xCoordinate + 30, yCoordinate - 20);
		carbody.addPoint(xCoordinate + 20, yCoordinate - 20);
		carbody.addPoint(xCoordinate + 10, yCoordinate - 10);
		carbody.addPoint(xCoordinate, yCoordinate - 10);
		g.fillPolygon(carbody);
		g.setColor(Color.BLACK);
		g.fillOval(xCoordinate + 10, yCoordinate, 10, 10);
		g.fillOval(xCoordinate + 30, yCoordinate, 10, 10);
	}

	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			repaint();
		}
	}
}