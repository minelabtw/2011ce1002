package a11.s100502018;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

public class A111 extends JApplet implements ActionListener {
	JTextField speed1 = new JTextField();
	JTextField speed2 = new JTextField();
	JTextField speed3 = new JTextField();
	JRadioButton red1 = new JRadioButton("Red");
	JRadioButton red2 = new JRadioButton("Red");
	JRadioButton red3 = new JRadioButton("Red");
	JRadioButton green1 = new JRadioButton("Green");
	JRadioButton green2 = new JRadioButton("Green");
	JRadioButton green3 = new JRadioButton("Green");
	JRadioButton blue1 = new JRadioButton("Blue");
	JRadioButton blue2 = new JRadioButton("Blue");
	JRadioButton blue3 = new JRadioButton("Blue");
	RaceCar myCar1 = new RaceCar();
	RaceCar myCar2 = new RaceCar();
	RaceCar myCar3 = new RaceCar();

	public A111() {
		JPanel p1 = new JPanel(new GridLayout(1, 6));
		p1.add(new JLabel("Car 1's delay: "));
		p1.add(speed1);
		speed1.addActionListener(this);
		p1.add(new JLabel("Car 2's delay: "));
		p1.add(speed2);
		speed2.addActionListener(this);
		p1.add(new JLabel("Car 3's delay: "));
		p1.add(speed3);
		speed3.addActionListener(this);
		JPanel p21 = new JPanel(new GridLayout(4, 1));
		p21.add(new JLabel("Car 1's Color:"));
		p21.add(red1);
		p21.add(green1);
		p21.add(blue1);
		red1.addActionListener(this);
		green1.addActionListener(this);
		blue1.addActionListener(this);
		red1.setSelected(true);
		JPanel p22 = new JPanel(new GridLayout(4, 1));
		p22.add(new JLabel("Car 2's Color:"));
		p22.add(red2);
		p22.add(green2);
		p22.add(blue2);
		red2.addActionListener(this);
		green2.addActionListener(this);
		blue2.addActionListener(this);
		red2.setSelected(true);
		JPanel p23 = new JPanel(new GridLayout(4, 1));
		p23.add(new JLabel("Car 3's Color:"));
		p23.add(red3);
		p23.add(green3);
		p23.add(blue3);
		red3.addActionListener(this);
		green3.addActionListener(this);
		blue3.addActionListener(this);
		red3.setSelected(true);
		JPanel p2 = new JPanel(new GridLayout(3, 1));
		p2.add(p21);
		p2.add(p22);
		p2.add(p23);
		JPanel p3 = new JPanel(new GridLayout(3, 1));
		p3.add(myCar1);
		p3.add(myCar2);
		p3.add(myCar3);
		JPanel total = new JPanel(new BorderLayout());
		total.add(p1, BorderLayout.NORTH);
		total.add(p2, BorderLayout.WEST);
		total.add(p3, BorderLayout.CENTER);
		add(total);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == speed1) {
			myCar1.setDelay(Integer.parseInt(speed1.getText()));
			repaint();
		} else if (e.getSource() == speed2) {
			myCar2.setDelay(Integer.parseInt(speed2.getText()));
			repaint();
		} else if (e.getSource() == speed3) {
			myCar3.setDelay(Integer.parseInt(speed3.getText()));
			repaint();
		} else if (e.getSource() == red1) {
			myCar1.setColor(Color.RED);
			green1.setSelected(false);
			blue1.setSelected(false);
			repaint();
		} else if (e.getSource() == red2) {
			myCar2.setColor(Color.RED);
			green2.setSelected(false);
			blue2.setSelected(false);
			repaint();
		} else if (e.getSource() == red3) {
			myCar3.setColor(Color.RED);
			green3.setSelected(false);
			blue3.setSelected(false);
			repaint();
		} else if (e.getSource() == green1) {
			myCar1.setColor(Color.GREEN);
			red1.setSelected(false);
			blue1.setSelected(false);
			repaint();
		} else if (e.getSource() == green2) {
			myCar2.setColor(Color.GREEN);
			red2.setSelected(false);
			blue2.setSelected(false);
			repaint();
		} else if (e.getSource() == green3) {
			myCar3.setColor(Color.GREEN);
			red3.setSelected(false);
			blue3.setSelected(false);
			repaint();
		} else if (e.getSource() == blue1) {
			myCar1.setColor(Color.BLUE);
			red1.setSelected(false);
			green1.setSelected(false);
			repaint();
		} else if (e.getSource() == blue2) {
			myCar2.setColor(Color.BLUE);
			red2.setSelected(false);
			green2.setSelected(false);
			repaint();
		} else if (e.getSource() == blue3) {
			myCar3.setColor(Color.BLUE);
			red3.setSelected(false);
			green3.setSelected(false);
			repaint();
		}
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame("Applet is in the frame");
		A111 applet = new A111();

		frame.add(applet);
		frame.setSize(500, 500);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}