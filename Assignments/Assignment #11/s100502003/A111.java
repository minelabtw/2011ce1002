package a11.s100502003;

import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class A111 extends Applet {
	private JTextField jtfCar1 = new JTextField(5);
	private JTextField jtfCar2 = new JTextField(5);
	private JTextField jtfCar3 = new JTextField(5);
	private RaceCar car1 = new RaceCar();
	private RaceCar car2 = new RaceCar();
	private RaceCar car3 = new RaceCar();
	
	public A111() {
		JPanel jpTextField = new JPanel(); // panel of deciding each car's speed
		jpTextField.setLayout(new GridLayout(1,6,5,5));
		jpTextField.add(new JLabel("Car1 Speed:"));
		jpTextField.add(jtfCar1);
		jpTextField.add(new JLabel("Car2 Speed:"));
		jpTextField.add(jtfCar2);
		jpTextField.add(new JLabel("Car3 Speed:"));
		jpTextField.add(jtfCar3);
		
		jtfCar1.setHorizontalAlignment(JTextField.LEFT);
		jtfCar2.setHorizontalAlignment(JTextField.LEFT);
		jtfCar3.setHorizontalAlignment(JTextField.LEFT);
		
		jtfCar1.addActionListener(new ActionListener() { // change car1 with choice
			public void actionPerformed(ActionEvent e) {
				// get the text and set the delay of speed
				car1.setdelay(Integer.parseInt(jtfCar1.getText())); 
				jtfCar1.requestFocusInWindow();
			}
		});
		jtfCar2.addActionListener(new ActionListener() { // change car2 with choice
			public void actionPerformed(ActionEvent e) {
				car2.setdelay(Integer.parseInt(jtfCar2.getText())); 
				jtfCar2.requestFocusInWindow();
			}
		});
		jtfCar3.addActionListener(new ActionListener() { // change car3 with choice
			public void actionPerformed(ActionEvent e) {
				car3.setdelay(Integer.parseInt(jtfCar3.getText()));
				jtfCar3.requestFocusInWindow();
			}
		});
		
		DrawColor draw1 = new DrawColor();
		DrawColor draw2 = new DrawColor();
		DrawColor draw3 = new DrawColor();
		
		JPanel panel1 = new JPanel(); // for car1
		panel1.setLayout(new BorderLayout(5,5));
		panel1.add(draw1.radioButtons(), BorderLayout.WEST); // the color options
		panel1.add((Component) draw1.setCar(car1), BorderLayout.CENTER); // running car
		
		JPanel panel2 = new JPanel(); // for car2
		panel2.setLayout(new BorderLayout(5,5));
		panel2.add(draw2.radioButtons(), BorderLayout.WEST);
		panel2.add((Component) draw2.setCar(car2), BorderLayout.CENTER);
		
		JPanel panel3 = new JPanel(); // for car3
		panel3.setLayout(new BorderLayout(5,5));
		panel3.add(draw3.radioButtons(), BorderLayout.WEST);
		panel3.add((Component) draw3.setCar(car3), BorderLayout.CENTER);
	
		JPanel cars = new JPanel();
		cars.setLayout(new GridLayout(3,1));
		cars.add(panel1);
		cars.add(panel2);
		cars.add(panel3);
		
		setLayout(new BorderLayout(5,5));
		add(jpTextField, BorderLayout.NORTH);
		add(cars, BorderLayout.CENTER);
	}
	
	public static void main() {
		JFrame frame = new JFrame("A111");
		A111 applet = new A111();
		frame.add(applet, BorderLayout.CENTER); // add applet to the frame
		
		frame.setSize(400,600);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}

class DrawColor {
	private JRadioButton jrbRed, jrbYellow, jrbBlue; // declare RadioButtons
	RaceCar car = new RaceCar();
	JPanel jpRadioButtons = new JPanel();
	public DrawColor() {
		jpRadioButtons.setLayout(new GridLayout(3,1));
		jpRadioButtons.add(jrbRed = new JRadioButton("Red"));
		jpRadioButtons.add(jrbYellow = new JRadioButton("Yellow"));
		jpRadioButtons.add(jrbBlue = new JRadioButton("Blue"));
		
		ButtonGroup group = new ButtonGroup(); // group the three RadioButtons
		group.add(jrbRed);
		group.add(jrbYellow);
		group.add(jrbBlue);
		
		jrbRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car.setcolor(Color.RED); // set the car to color red
			}
		});
		
		jrbYellow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car.setcolor(Color.YELLOW); // set the car to color yellow
			}
		});
		
		jrbBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car.setcolor(Color.BLUE); // set the car to color blue
			}
		});
		
		jrbBlue.setSelected(true); // default the choice to blue
	}
	
	public JPanel radioButtons() {
		return jpRadioButtons; 
	}
	
	public Object setCar(Object CAR) {
		this.car = (RaceCar) CAR;
		car.setcolor(Color.BLUE); // set the origin color to blue
		return car;
	}
}
