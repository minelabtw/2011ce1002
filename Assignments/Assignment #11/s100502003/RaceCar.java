package a11.s100502003;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class RaceCar extends JPanel{
	private int delay=1000; // default value is 1000(run one time for each second)
	private Color color;
	int xCoordinate = 0;
	Timer timer = new Timer(delay, new TimerListener());
	
	public RaceCar() {
		timer.start();
	}
	
	public void setdelay(int Delay) {
		this.delay = Delay;
		timer.setDelay(Delay); // delay the timer
	}
	
	public void setcolor(Color COLOR) {
		this.color = COLOR; // set color
	}
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		if(xCoordinate+70 > getWidth()) { // across the border
			xCoordinate = 0; // start again from the original point
		}
		else {
			xCoordinate += 5;
		}
		// draw car
		int x1[]={xCoordinate+20, xCoordinate+30, xCoordinate+40, xCoordinate+10};
		int y1[]={70, 70, 80, 80};
		g.setColor(Color.GREEN);
		g.fillPolygon(x1, y1, x1.length);
		g.setColor(color); // set the color of the car's body
		g.fillRect(xCoordinate, 80, 50, 10);
		g.setColor(Color.BLACK);
		g.fillOval(xCoordinate+10, 90, 10, 10);
		g.fillOval(xCoordinate+30, 90, 10, 10);
		g.drawLine(0, 100, getWidth()-20, 100);
	}
	
	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			repaint();
		}
	}
}
