package a11.s100502544;

import java.applet.Applet;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;



public class A111 extends Applet implements ActionListener{
	RaceCar raceCarA =new RaceCar();//第一車JPanel class
	RaceCar raceCarD =new RaceCar();//第二車JPanel class
	RaceCar raceCarZ =new RaceCar();//第三車JPanel class
	JPanel p1=new JPanel();//  放  設定車顏色和設定速度
	
	JLabel AcarColorLable=new JLabel("First car color");
	JRadioButton AcarCjrb=new JRadioButton("CYAN");
	JRadioButton AcarWjrb=new JRadioButton("While");
	JRadioButton AcarOjrb=new JRadioButton("Orange");
	JRadioButton AcarRjrb=new JRadioButton("Rad");
	//用JRadioButton調整Acar的顏色
	
	
	
	JLabel DcarColorLable=new JLabel("Second car color");
	JRadioButton DcarYjrb=new JRadioButton("Yellow");
	JRadioButton DcarGjrb=new JRadioButton("Gray");
	JRadioButton DcarPjrb=new JRadioButton("Pink");
	JRadioButton DcarRjrb=new JRadioButton("Rad");
	//用JRadioButton調整Dcar的顏色
	
	JLabel ZcarColorLable=new JLabel("Third car color");
	JRadioButton ZcarBjrb=new JRadioButton("Black");
	JRadioButton ZcarWjrb=new JRadioButton("While");
	JRadioButton ZcarPjrb=new JRadioButton("Pink");
	JRadioButton ZcarRjrb=new JRadioButton("Rad");
	//用JRadioButton調整Zcar的顏色
	
	JLabel AcarJLabel=new JLabel("First car speed");
	JButton AspeedF=new JButton("高鐵");
	JButton AspeedS=new JButton("台鐵");
	JButton AspeedT=new JButton("腿鐵");
	//Acar speed
	
	JLabel DcarJLabel=new JLabel("Second car speed");
	JButton DspeedF=new JButton("高鐵");
	JButton DspeedS=new JButton("台鐵");
	JButton DspeedT=new JButton("腿鐵");
	//Dcar speed
	
	JLabel ZcarJLabel=new JLabel("Third car speed");
	JButton ZspeedF=new JButton("高鐵");
	JButton ZspeedS=new JButton("台鐵");
	JButton ZspeedT=new JButton("腿鐵");
	//Zcar speed
	
//	JButton f=new JButton("hrgrygg");
	
	public A111(){
		ButtonGroup Agroup=new ButtonGroup();
		//group Acar button
		Agroup.add(AcarCjrb);
		Agroup.add(AcarWjrb);
		Agroup.add(AcarOjrb);
		Agroup.add(AcarRjrb);
		
		
		//group Dcar button
		ButtonGroup Dgroup=new ButtonGroup();
		Dgroup.add(DcarYjrb);
		Dgroup.add(DcarGjrb);
		Dgroup.add(DcarPjrb);
		Dgroup.add(DcarRjrb);
		
		//group Zcar button
		ButtonGroup Zgroup=new ButtonGroup();
		Zgroup.add(ZcarBjrb);
		Zgroup.add(ZcarWjrb);
		Zgroup.add(ZcarPjrb);
		Zgroup.add(ZcarRjrb);
		
		
		setLayout(new GridLayout(4,3));//4*3
		add(p1);
		add(raceCarA);
		add(raceCarD);
		add(raceCarZ);
		//panel
		
		//p1 panel 排版
		p1.add(AcarJLabel);
		p1.add(AspeedF);//Acar 速度
		AspeedF.addActionListener(this);
		p1.add(AspeedS);
		AspeedS.addActionListener(this);
		p1.add(AspeedT);
		AspeedT.addActionListener(this);
		
		
		p1.add(DcarJLabel);
		//Dcar 速度
		p1.add(DspeedF);//Acar 速度
		DspeedF.addActionListener(this);
		p1.add(DspeedS);
		DspeedS.addActionListener(this);
		p1.add(DspeedT);
		DspeedT.addActionListener(this);
		
		
		p1.add(ZcarJLabel);
		//Zcar 速度
		p1.add(ZspeedF);//Acar 速度
		ZspeedF.addActionListener(this);
		p1.add(ZspeedS);
		ZspeedS.addActionListener(this);
		p1.add(ZspeedT);
		ZspeedT.addActionListener(this);
		
		
		p1.add(AcarColorLable);
		p1.add(AcarCjrb);
		AcarCjrb.addActionListener(this);
		p1.add(AcarWjrb);
		AcarWjrb.addActionListener(this);
		p1.add(AcarOjrb);
		AcarOjrb.addActionListener(this);
		p1.add(AcarRjrb);
		AcarRjrb.addActionListener(this);
		//Acar JRadioButton control
		
		
//		p1.add(f);
//		f.addActionListener(this);
		
		
		p1.add(DcarColorLable);
		p1.add(DcarYjrb);
		DcarYjrb.addActionListener(this);
		p1.add(DcarGjrb);
		DcarGjrb.addActionListener(this);
		p1.add(DcarPjrb);
		DcarPjrb.addActionListener(this);
		p1.add(DcarRjrb);
		DcarRjrb.addActionListener(this);
		//Dcar JRadioButton control
		
		p1.add(ZcarColorLable);
		p1.add(ZcarBjrb);
		ZcarBjrb.addActionListener(this);
		p1.add(ZcarWjrb);
		ZcarWjrb.addActionListener(this);
		p1.add(ZcarPjrb);
		ZcarPjrb.addActionListener(this);
		p1.add(ZcarRjrb);
		ZcarRjrb.addActionListener(this);
		//Dcar JRadioButton control
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==AcarCjrb){
			raceCarA.Color(Color.CYAN);
		}
		else if(e.getSource()==AcarWjrb){
			raceCarA.Color(Color.WHITE);
		}
		else if(e.getSource()==AcarOjrb){
			raceCarA.Color(Color.ORANGE);
		}
		else if(e.getSource()==AcarRjrb){
			raceCarA.Color(Color.RED);
		}
		//Acar color
		
		
		else if(e.getSource()==DcarYjrb){
			raceCarD.Color(Color.YELLOW);
		}
		else if(e.getSource()==DcarGjrb){
			raceCarD.Color(Color.GRAY);
		}
		else if(e.getSource()==DcarPjrb){
			raceCarD.Color(Color.PINK);
		}
		else if(e.getSource()==DcarRjrb){
			raceCarD.Color(Color.RED);
		}
		//Dcar color
		
		
		else if(e.getSource()==ZcarBjrb){
			raceCarZ.Color(Color.BLACK);
		}
		else if(e.getSource()==ZcarWjrb){
			raceCarZ.Color(Color.WHITE);
		}
		else if(e.getSource()==ZcarPjrb){
			raceCarZ.Color(Color.PINK);
		}
		else if(e.getSource()==ZcarRjrb){
			raceCarZ.Color(Color.RED);
		}
		//Zcar color
		
		
		else if(e.getSource()==AspeedF){
			raceCarA.carspeed(1);
		}
		else if(e.getSource()==AspeedS){
			raceCarA.carspeed(100);
		}
		else if(e.getSource()==AspeedT){
			raceCarA.carspeed(1000);
		}
		//Acar speed
		
		
		else if(e.getSource()==DspeedF){
			raceCarD.carspeed(1);
		}
		else if(e.getSource()==DspeedS){
			raceCarD.carspeed(100);
		}
		else if(e.getSource()==DspeedT){
			raceCarD.carspeed(1000);
		}
		//Dcar speed
		
		else if(e.getSource()==ZspeedF){
			raceCarZ.carspeed(1);
		}
		else if(e.getSource()==ZspeedS){
			raceCarZ.carspeed(100);
		}
		else if(e.getSource()==ZspeedT){
			raceCarZ.carspeed(1000);
		}
		//Zcar speed
		
		
//		else if(e.getSource()==f){
//		raceCarA.carspeed(1);
//		}
		
//		else if(e.getSource()==AspeedtextField.getText()){
//			raceCarA.carspeed(Integer.valueOf(AspeedtextField.getText()));
//		}
	}
	
}
