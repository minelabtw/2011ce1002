package a11.s100502013;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.*;

public class A111 extends JApplet implements ActionListener{
	private JPanel total = new JPanel();
	private JPanel carpanel = new JPanel(new GridLayout(3,1));
	private RaceCar car1 = new RaceCar();
	private RaceCar car2 = new RaceCar();
	private RaceCar car3 = new RaceCar();
	private JLabel carpanel1 = new JLabel();
	private JLabel carpanel2 = new JLabel();
	private JLabel carpanel3 = new JLabel();
	private Border carsline = new LineBorder(Color.BLACK,2);
	private JLabel info1 = new JLabel("Car 1");
	private JLabel info2 = new JLabel("Car 2");
	private JLabel info3 = new JLabel("Car 3");
	private JLabel info4 = new JLabel("#  initial is 20  #");
	private JTextField cspeed1 = new JTextField();
	private JTextField cspeed2 = new JTextField();
	private JTextField cspeed3 = new JTextField();
	private JButton setbutton = new JButton("Set Speed");
	private JPanel speedset = new JPanel(new GridLayout(2,4));
	private JPanel ccolor1 = new JPanel(new GridLayout(3,1));
	private JPanel ccolor2 = new JPanel(new GridLayout(3,1));
	private JPanel ccolor3 = new JPanel(new GridLayout(3,1));
	private JRadioButton cred1 = new JRadioButton("Red");
	private JRadioButton cgreen1 = new JRadioButton("Green");
	private JRadioButton cblue1 = new JRadioButton("Blue");
	private JRadioButton cred2 = new JRadioButton("Red");
	private JRadioButton cgreen2 = new JRadioButton("Green");
	private JRadioButton cblue2 = new JRadioButton("Blue");
	private JRadioButton cred3 = new JRadioButton("Red");
	private JRadioButton cgreen3 = new JRadioButton("Green");
	private JRadioButton cblue3 = new JRadioButton("Blue");
	private ButtonGroup cc1 = new ButtonGroup();
	private ButtonGroup cc2 = new ButtonGroup();
	private ButtonGroup cc3 = new ButtonGroup();
	private JPanel colorset = new JPanel(new GridLayout(3,1));
	
	public A111(){ //set things in order
		total.setLayout(new BorderLayout(1,1));
		car1.setBackground(Color.WHITE);
		car2.setBackground(Color.WHITE);
		car3.setBackground(Color.WHITE);
		carpanel.setBackground(Color.WHITE);
		carpanel1.setLayout(new BorderLayout(3,3));
		carpanel2.setLayout(new BorderLayout(3,3));
		carpanel3.setLayout(new BorderLayout(3,3));
		carpanel1.setBorder(carsline);
		carpanel2.setBorder(carsline);
		carpanel3.setBorder(carsline);
		carpanel1.add(car1,BorderLayout.CENTER);
		carpanel2.add(car2,BorderLayout.CENTER);
		carpanel3.add(car3,BorderLayout.CENTER);
		speedset.add(info1);
		speedset.add(info2);
		speedset.add(info3);
		speedset.add(info4);
		speedset.add(cspeed1);
		speedset.add(cspeed2);
		speedset.add(cspeed3);
		speedset.add(setbutton);
		setbutton.addActionListener(new ButtonListener());
		ccolor1.add(cred1);
		ccolor1.add(cgreen1);
		ccolor1.add(cblue1);
		ccolor2.add(cred2);
		ccolor2.add(cgreen2);
		ccolor2.add(cblue2);
		ccolor3.add(cred3);
		ccolor3.add(cgreen3);
		ccolor3.add(cblue3);
		cred1.addActionListener(this);
		cgreen1.addActionListener(this);
		cblue1.addActionListener(this);
		cred2.addActionListener(this);
		cgreen2.addActionListener(this);
		cblue2.addActionListener(this);
		cred3.addActionListener(this);
		cgreen3.addActionListener(this);
		cblue3.addActionListener(this);
		cc1.add(cred1);
		cc1.add(cgreen1);
		cc1.add(cblue1);
		cc2.add(cred2);
		cc2.add(cgreen2);
		cc2.add(cblue2);
		cc3.add(cred3);
		cc3.add(cgreen3);
		cc3.add(cblue3);
		cred1.setSelected(true);
		cred2.setSelected(true);
		cred3.setSelected(true);
		colorset.add(ccolor1);
		colorset.add(ccolor2);
		colorset.add(ccolor3);
		carpanel.add(carpanel1);
		carpanel.add(carpanel2);
		carpanel.add(carpanel3);
		total.add(speedset,BorderLayout.NORTH);
		total.add(carpanel,BorderLayout.CENTER);
		total.add(colorset,BorderLayout.WEST);
		add(total);
	}

	private class ButtonListener implements ActionListener { //check the textfield and set delay
		public void actionPerformed(ActionEvent e){
			if(cspeed1.getText().trim().equals("")){
				car1.setdelay(car1.getdelay());
			}
			else{
				double cars1 = Double.parseDouble(cspeed1.getText());
				car1.setdelay(cars1);
			}
			if(cspeed2.getText().trim().equals("")){
				car2.setdelay(car2.getdelay());
			}
			else{
				double cars2 = Double.parseDouble(cspeed2.getText());
				car2.setdelay(cars2);
			}
			if(cspeed3.getText().trim().equals("")){
				car3.setdelay(car3.getdelay());
			}
			else{
				double cars3 = Double.parseDouble(cspeed3.getText());
				car3.setdelay(cars3);
			}
		}
	}

	public void actionPerformed(ActionEvent e) { //change the car color and repaint
		if(e.getSource()==cred1){
			car1.setCarColor(Color.RED);
			car1.repaint();
		}
		else if(e.getSource()==cgreen1){
			car1.setCarColor(Color.GREEN);
			car1.repaint();
		}
		else if(e.getSource()==cblue1){
			car1.setCarColor(Color.BLUE);
			car1.repaint();
		}
		else if(e.getSource()==cred2){
			car2.setCarColor(Color.RED);
			car2.repaint();
		}
		else if(e.getSource()==cgreen2){
			car2.setCarColor(Color.GREEN);
			car2.repaint();
		}
		else if(e.getSource()==cblue2){
			car2.setCarColor(Color.BLUE);
			car2.repaint();
		}
		else if(e.getSource()==cred3){
			car3.setCarColor(Color.RED);
			car3.repaint();
		}
		else if(e.getSource()==cgreen3){
			car3.setCarColor(Color.GREEN);
			car3.repaint();
		}
		else if(e.getSource()==cblue3){
			car3.setCarColor(Color.BLUE);
			car3.repaint();
		}
	}
}
