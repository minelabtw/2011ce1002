package a11.s100502013;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class RaceCar extends JPanel{
	private double speeddelay = 50;
	private Color carcolor = Color.RED;
	private String style = null;
	private int brx = 0;
	private int brdx = 3;
	private int lrx = 12;
	private int lrdx = 3;
	private int lwx = 12;
	private int lwdx = 3;
	private int rwx = 36;
	private int rwdx = 3;
	private int fwx = 24;
	private int fwdx = 3;
	private Timer timer = new Timer((int)speeddelay,new TimerListener());
	
	public RaceCar(){ //constructor:start the Timer
		timer.start();
	}
	
	public void setdelay(double x){ //set the Timer delay
		if(x==0){
			timer.stop();
			speeddelay = 0;
		}
		else{
			speeddelay = 1000/x; //a bigger "x" get a smaller delay
			timer.setDelay((int)speeddelay);
			timer.start();
		}
	}
	
	public double getdelay(){ //get the delay
		return speeddelay;
	}
	
	public void setCarColor(Color x){ //set the racecar's color
		carcolor = x;
	}
	
	public void setLocation(){ //set the car's location
		if(brx>getWidth()-60 || brx<0){
			brdx = -1*brdx;
			brx += brdx;
		}
		else{
			brx += brdx;
		}
		if(lrx>getWidth()-48 || lrx<12){
			lrdx = -1*lrdx;
			lrx += lrdx;
		}
		else{
			lrx += lrdx;
		}
		if(lwx>getWidth()-48 || lwx<12){
			lwdx = -1*lwdx;
			lwx += lwdx;
		}
		else{
			lwx += lwdx;
		}
		if(rwx>getWidth()-24 || rwx<36){
			rwdx = -1*rwdx;
			rwx += rwdx;
		}
		else{
			rwx += rwdx;
		}
		if(fwx>getWidth()-36 || fwx<24){
			fwdx = -1*fwdx;
			fwx += fwdx;
		}
		else{
			fwx += fwdx;
		}
	}
	
	public void paintComponent(Graphics g){ //paint graphics
		super.paintComponent(g);
		g.setColor(carcolor);
		g.fillRect(brx,getHeight()-72,60,12);
		g.fillRect(lrx,getHeight()-84,24,12);
		g.fillOval(lwx,getHeight()-62,12,12);
		g.fillOval(rwx,getHeight()-62,12,12);
		g.fillArc(fwx,getHeight()-84,24,24,0,90);
	}
	
	class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			setLocation();
			repaint();
		}
	}
}
