package a11.s100502009;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class RaceCar extends JPanel{
	private int delay=100;	
	private Timer timer=new Timer(delay,new TimerListener());
	private int x=0;
	private int dx=1;
	private int touch=0;
	private Color color1=Color.BLUE;
	private Color color2=Color.RED;
	private Color color3=Color.GREEN;
	
	public RaceCar()
	{
		timer.start();
	}
	
	protected void paintComponent(Graphics g)//draw cars
	{	
		int y=getHeight();//set the value of y
		super.paintComponent(g);		
		Polygon polygon =new Polygon();//draw the car
		polygon.addPoint(x+20,y-30);
		polygon.addPoint(x+30,y-30);
		polygon.addPoint(x+40,y-20);
		polygon.addPoint(x+10,y-20);
		g.setColor(color1);
		g.fillPolygon(polygon);
		g.setColor(color2);
		g.fillRect(x,y-20, 50, 10);
		g.setColor(color3);
		g.fillOval(x+10, y-10, 10, 10);		
		g.fillOval(x+30, y-10, 10, 10);	
		if(x+50==getWidth())//let the car reverse when it touches the edge
			touch++;
		else if(x==0)
			touch++;
		if(x+50<=getWidth() && (touch%2)==0)
		{			
			x+=dx;
		}		
		else if(x>0 && (touch%2)==1)
		{	
			x-=dx;
		}		
	}
	
	private class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			repaint();
		}
	}
	
	public void setDelay(int delay)//set the speed of the car
	{
		this.delay=delay;
		timer.setDelay(delay);
	}
	
	public void setColor(Color color)//set the color of the car
	{
		this.color2=color;
	}

}
