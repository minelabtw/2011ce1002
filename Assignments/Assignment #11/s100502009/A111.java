package a11.s100502009;
import javax.swing.*;
import javax.swing.border.LineBorder;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class A111 extends JApplet{
	private RaceCar car1=new RaceCar();//create cars
	private RaceCar car2=new RaceCar();
	private RaceCar car3=new RaceCar();
	private String[] speed={"1","10","100","1000"};//set the speed
	private JComboBox jcbo1=new JComboBox(speed);
	private JComboBox jcbo2=new JComboBox(speed);
	private JComboBox jcbo3=new JComboBox(speed);
	private JRadioButton jrbRed,jrbGreen,jrbBlue,jrbBlack,jrbMagenta,jrbYellow,jrbGray,jrbOrange,jrbPink;//colors
	
	public A111()
	{
		JPanel jp1=new JPanel();
		JPanel jp2=new JPanel();
		JPanel jp3=new JPanel();
		JPanel jp4=new JPanel(new GridLayout(3,1));
		JPanel jp5=new JPanel(new GridLayout(2,3));
		JPanel jp6=new JPanel(new GridLayout(3,1));
		JLabel jlb1=new JLabel("Car1");
		JLabel jlb2=new JLabel("Car2");
		JLabel jlb3=new JLabel("Car3");
		jp1.setLayout(new GridLayout(3,1));
		jp2.setLayout(new GridLayout(3,1));
		jp3.setLayout(new GridLayout(3,1));
		jp1.add(jrbRed=new JRadioButton("Red"));//create a panel for choosing colors
		jp1.add(jrbGreen=new JRadioButton("Green"));
		jp1.add(jrbBlue=new JRadioButton("Blue"));
		jp2.add(jrbBlack=new JRadioButton("Black"));
		jp2.add(jrbMagenta=new JRadioButton("Magenta"));
		jp2.add(jrbYellow=new JRadioButton("Yellow"));
		jp3.add(jrbGray=new JRadioButton("Gray"));
		jp3.add(jrbOrange=new JRadioButton("Orange"));
		jp3.add(jrbPink=new JRadioButton("Pink"));
		jp4.add(jp1);
		jp4.add(jp2);
		jp4.add(jp3);
		jp5.add(jlb1);
		jp5.add(jlb2);
		jp5.add(jlb3);
		jp5.add(jcbo1);
		jp5.add(jcbo2);
		jp5.add(jcbo3);
		car1.setBorder(new LineBorder(Color.BLACK));
		car2.setBorder(new LineBorder(Color.BLACK));
		car3.setBorder(new LineBorder(Color.BLACK));
		jp6.add(car1);
		jp6.add(car2);
		jp6.add(car3);		
		add(jp4,BorderLayout.WEST);
		add(jp5,BorderLayout.NORTH);
		add(jp6,BorderLayout.CENTER);
		ButtonGroup group1=new ButtonGroup();//let user choose only one color
		group1.add(jrbRed);
		group1.add(jrbGreen);
		group1.add(jrbBlue);
		ButtonGroup group2=new ButtonGroup();
		group2.add(jrbBlack);
		group2.add(jrbMagenta);
		group2.add(jrbYellow);
		ButtonGroup group3=new ButtonGroup();
		group3.add(jrbGray);
		group3.add(jrbOrange);
		group3.add(jrbPink);
		
		jrbRed.addActionListener(new ActionListener()//set a new color of the car
		{
			public void actionPerformed(ActionEvent e)
			{
				car1.setColor(Color.RED);
			}
		});
		jrbGreen.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				car1.setColor(Color.GREEN);
			}
		});
		jrbBlue.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				car1.setColor(Color.BLUE);
			}
		});
		jrbBlack.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				car2.setColor(Color.BLACK);
			}
		});
		jrbMagenta.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				car2.setColor(Color.MAGENTA);
			}
		});
		jrbYellow.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				car2.setColor(Color.YELLOW);
			}
		});
		jrbGray.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				car3.setColor(Color.GRAY);
			}
		});
		jrbOrange.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				car3.setColor(Color.ORANGE);
			}
		});
		jrbPink.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				car3.setColor(Color.PINK);
			}
		});
		jcbo1.addItemListener(new ItemListener(){//choose which speed for the car running
			public void itemStateChanged(ItemEvent e)
			{
				int delay=Integer.parseInt(speed[jcbo1.getSelectedIndex()]);
				car1.setDelay(delay);
			}
		});
		jcbo2.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e)
			{
				int delay=Integer.parseInt(speed[jcbo2.getSelectedIndex()]);
				car2.setDelay(delay);
			}
		});
		jcbo3.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e)
			{
				int delay=Integer.parseInt(speed[jcbo3.getSelectedIndex()]);
				car3.setDelay(delay);
			}
		});

	}
}
