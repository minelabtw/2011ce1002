package a11.s100502012;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JApplet;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class A111 extends JApplet{
	public A111(){
		add(new FrameWork());//add whole panel to the frame of JApplet
	}
}

class FrameWork extends JPanel implements ActionListener{
	//pSpeed={pSpeed1,pSpeed2,pSpeed3}
	//pColor={pColor1,pColor2,pColor3}
	//pCar={car1,car2,car3}
	//mainPanel={pColor,pSpeed}
	//frame={mainPanel,pSpeed}
	RaceCar car1=new RaceCar();
	RaceCar car2=new RaceCar();
	RaceCar car3=new RaceCar();
	private JLabel VCar1=new JLabel("  NO.1 Vehicle Velocity");
	private JLabel VCar2=new JLabel("  NO.2 Vehicle Velocity");
	private JLabel VCar3=new JLabel("  NO.3 Vehicle Velocity");
	private JTextField velocity1=new JTextField(5);
	private JTextField velocity2=new JTextField(5);
	private JTextField velocity3=new JTextField(5);
	private JPanel pSpeed1=new JPanel();
	private JPanel pSpeed2=new JPanel();
	private JPanel pSpeed3=new JPanel();
	private JPanel pSpeed=new JPanel();
	private JLabel Red1=new JLabel("Red");
	private JLabel Green1=new JLabel("Green");
	private JLabel Blue1=new JLabel("Blue");
	private JLabel Red2=new JLabel("Red");
	private JLabel Green2=new JLabel("Green");
	private JLabel Blue2=new JLabel("Blue");
	private JLabel Red3=new JLabel("Red");
	private JLabel Green3=new JLabel("Green");
	private JLabel Blue3=new JLabel("Blue");
	private JRadioButton Rbt11=new JRadioButton();
	private JRadioButton Rbt12=new JRadioButton();
	private JRadioButton Rbt13=new JRadioButton();
	private JRadioButton Rbt21=new JRadioButton();
	private JRadioButton Rbt22=new JRadioButton();
	private JRadioButton Rbt23=new JRadioButton();
	private JRadioButton Rbt31=new JRadioButton();
	private JRadioButton Rbt32=new JRadioButton();
	private JRadioButton Rbt33=new JRadioButton();
	private ButtonGroup group1=new ButtonGroup();
	private ButtonGroup group2=new ButtonGroup();
	private ButtonGroup group3=new ButtonGroup();
	private JPanel pColor1=new JPanel();
	private JPanel pColor2=new JPanel();
	private JPanel pColor3=new JPanel();
	private JPanel pColor=new JPanel();
	private JPanel pCar=new JPanel();
	private JPanel mainPanel=new JPanel();
	private Border lineBorder=new LineBorder(new Color(100, 0, 253), 2);
	
    public FrameWork(){
        pSpeed1.setBorder(lineBorder);
        pSpeed1.setLayout(new GridLayout(1,2));
        pSpeed1.add(VCar1);
        pSpeed1.add(velocity1);
        
        pSpeed2.setBorder(lineBorder);
        pSpeed2.setLayout(new GridLayout(1,2));
        pSpeed2.add(VCar2);
        pSpeed2.add(velocity2);
        
        pSpeed3.setBorder(lineBorder);
        pSpeed3.setLayout(new GridLayout(1,2));
        pSpeed3.add(VCar3);
        pSpeed3.add(velocity3);
        
        pSpeed.setBorder(lineBorder);
        pSpeed.setLayout(new GridLayout(1,3));
        pSpeed.add(pSpeed1);
        pSpeed.add(pSpeed2);
        pSpeed.add(pSpeed3);
        
        
        pColor1.setBorder(lineBorder);
        pColor1.setLayout(new GridLayout(3,2));
        group1.add(Rbt11);
        group1.add(Rbt12);
        group1.add(Rbt13);
        pColor1.add(Rbt11);
        Rbt11.addActionListener(this);
        pColor1.add(Red1);
        pColor1.add(Rbt12);
        Rbt12.addActionListener(this);
        pColor1.add(Green1);
        pColor1.add(Rbt13);
        Rbt13.addActionListener(this);
        pColor1.add(Blue1);
    
        pColor2.setBorder(lineBorder);
        pColor2.setLayout(new GridLayout(3,2));
        group2.add(Rbt21);
        group2.add(Rbt22);
        group2.add(Rbt23);
        pColor2.add(Rbt21);
        Rbt21.addActionListener(this);
        pColor2.add(Red2);
        pColor2.add(Rbt22);
        Rbt22.addActionListener(this);
        pColor2.add(Green2);
        pColor2.add(Rbt23);
        Rbt23.addActionListener(this);
        pColor2.add(Blue2);
    
        pColor3.setBorder(lineBorder);
        pColor3.setLayout(new GridLayout(3,2));
        group3.add(Rbt31);
        group3.add(Rbt32);
        group3.add(Rbt33);
        pColor3.add(Rbt31);
        Rbt31.addActionListener(this);
        pColor3.add(Red3);
        pColor3.add(Rbt32);
        Rbt32.addActionListener(this);
        pColor3.add(Green3);
        pColor3.add(Rbt33);
        Rbt33.addActionListener(this);
        pColor3.add(Blue3);
    
        pColor.setBorder(lineBorder);
        pColor.setLayout(new GridLayout(3,1));
        pColor.add(pColor1);
        pColor.add(pColor2);
        pColor.add(pColor3);
    
        
        pCar.setBorder(lineBorder);
        pCar.setLayout(new GridLayout(3,1));
        car1.setBorder(lineBorder);
        car2.setBorder(lineBorder);
        car3.setBorder(lineBorder);
        pCar.add(car1);
        pCar.add(car2);
        pCar.add(car3);
    
        mainPanel.setBorder(lineBorder);
        mainPanel.setLayout(new BorderLayout(5,5));
        mainPanel.add(pColor,BorderLayout.WEST);
        mainPanel.add(pCar,BorderLayout.CENTER);
        
        setLayout(new GridLayout(2,1));
        add(pSpeed,BorderLayout.NORTH);
        add(mainPanel,BorderLayout.CENTER);
        
        velocity1.addActionListener(new ActionListener(){//read and modify the velocity by senting it to method "setVelocity()" 
        	public void actionPerformed(ActionEvent e){
        		String text=velocity1.getText();
            	double vcar1=Double.parseDouble(text);//convert the type from String to double
            	car1.setVelocity(vcar1);
            	velocity1.setText("");
        	} 	
        });
        
        velocity2.addActionListener(new ActionListener(){//read and modify the velocity by senting it to method "setVelocity()" 
        	public void actionPerformed(ActionEvent e){
        		String text=velocity2.getText();
            	double vcar2=Double.parseDouble(text);//convert the type from String to double
            	car2.setVelocity(vcar2);
            	velocity2.setText("");
        	} 	
        });
        
        velocity3.addActionListener(new ActionListener(){//read and modify the velocity by senting it to method "setVelocity()" 
        	public void actionPerformed(ActionEvent e){
        		String text=velocity3.getText();
            	double vcar3=Double.parseDouble(text);//convert the type from String to double
            	car3.setVelocity(vcar3);
            	velocity3.setText("");
        	} 	
        });  
    }
    
    public void actionPerformed(ActionEvent e){//modify the color of cars
    	if (e.getSource()==Rbt11){
    		car1.setColor(Color.RED);//sent the choosed color to the method "setColor()"
    		car1.repaint();
    	}
    	
        if (e.getSource()==Rbt12){
        	car1.setColor(Color.GREEN);//sent the choosed color to the method "setColor()"
        	car1.repaint();
    	}
        
        if (e.getSource()==Rbt13){
        	car1.setColor(Color.BLUE);//sent the choosed color to the method "setColor()"
        	car1.repaint();
    	}
        
        if (e.getSource()==Rbt21){
        	car2.setColor(Color.RED);//sent the choosed color to the method "setColor()"
        	car2.repaint();
        }
        
        if (e.getSource()==Rbt22){
        	car2.setColor(Color.GREEN);//sent the choosed color to the method "setColor()"
        	car2.repaint();
        }
        
        if (e.getSource()==Rbt23){
        	car2.setColor(Color.BLUE);//sent the choosed color to the method "setColor()"
        	car2.repaint();
        }
        
        if (e.getSource()==Rbt31){
        	car3.setColor(Color.RED);//sent the choosed color to the method "setColor()"
        	car3.repaint();
        }
        
        if (e.getSource()==Rbt32){
        	car3.setColor(Color.GREEN);//sent the choosed color to the method "setColor()"
        	car3.repaint();
        }
        
        if (e.getSource()==Rbt33){
        	car3.setColor(Color.BLUE);//sent the choosed color to the method "setColor()"
        	car3.repaint();
        }
    }
}
