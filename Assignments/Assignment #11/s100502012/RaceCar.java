package a11.s100502012;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import javax.swing.Timer;

public class RaceCar extends JPanel{//refer to E10
	private int velocity=1;//direction of the cars
	private int x11=10;
	private int x21=0;
	private int x31=5;
	private int x32=35;
	private int speed=10;
	Timer timer=new Timer(speed,new TimerListener()); 
	Color color;//color of cars
	
	public RaceCar(){
        timer.start();//start the animation
	}
	
	public void setColor(Color iro){//get the color of the car
	    color=iro;
	}
	
	public void setVelocity(double velocity){//get the speed of the car
	    speed=(int)(1000/velocity);
	    timer.setDelay(speed);
	}
	
	protected void paintComponent(Graphics g){//graphic of the car 
		super.paintComponent(g);
		g.setColor(color);
		
		int y11=getHeight()/2;
		int y21=getHeight()/2;
		int y31=(getHeight()/2)+10;
		int y32=(getHeight()/2)+10;
		
		Polygon rec1=new Polygon();
		rec1.addPoint(x11,y11);
		rec1.addPoint(x11+10,y11-5);
		rec1.addPoint(x11+20,y11-5);
		rec1.addPoint(x11+30,y11);
		
		Polygon rec2=new Polygon();
		rec2.addPoint(x21,y21);
		rec2.addPoint(x21+50,y21);
		rec2.addPoint(x21+50,y21+10);
		rec2.addPoint(x21,y21+10);
		
		g.fillPolygon(rec1);
		g.fillPolygon(rec2);
		g.fillOval(x31, y31, 10, 10);
		g.fillOval(x32, y32, 10, 10);
		
		if (velocity>0){
		    x11+=1;
			x21+=1;
		    x31+=1;
		    x32+=1;
		}
		
		if (velocity<0){
		    x11-=1;
			x21-=1;
		    x31-=1;
		    x32-=1;
		}
		
		if (x21>getWidth()-50){
			velocity=-1;
		}
		
		if (x21<0){
			velocity=1;
		}
	}	
	
	class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			repaint();
		}
	}
}

