package a11.s100502011;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
public class RaceCar extends JPanel{		
	private Border line = new LineBorder(Color.BLACK,2); // set border
	private Color color=(Color.BLACK); // default color
	int speed = 1000; // default speed
	int centerx = 0; // center of car
	int centery = 0; // center of car
	int dir=0; // dierction
	public Timer timer = new Timer(1000,new TimerListener());
	public RaceCar(){ // constructor
		setLayout(new GridLayout(1,1,5,5));
		add(new DrawCar(),BorderLayout.CENTER);
		setBorder(line);
		timer.start();
	}
	
	public void setColor(Color c){ // set the car's color
		color=c;
	}
	
	public void setSpeed(int x){
		timer.setDelay(x); // set the delay of time
	}
	
	class TimerListener implements ActionListener{ // timer action
		public void actionPerformed(ActionEvent e){
			repaint(); // repaint picture
		}
	}

	public class DrawCar extends JPanel{
		protected void paintComponent(Graphics g){
			super.paintComponent(g);
			
			if(centerx==0&&centery==0){
				centerx = getWidth()/2-335;
				centery = getHeight()/2;
			}
			
			//draw car
			g.setColor(color);
			g.drawOval(centerx-15, centery+5, 10, 10);
			g.drawOval(centerx+5, centery+5, 10, 10);
			g.fillOval(centerx-15, centery+5, 10, 10);
			g.fillOval(centerx+5, centery+5, 10, 10);	
			int roof_x[]={centerx+15,centerx+5,centerx-5,centerx-15};
			int roof_y[]={centery-5,centery-15,centery-15,centery-5};
			g.drawPolygon(roof_x,roof_y, roof_x.length);
			g.fillPolygon(roof_x,roof_y,roof_x.length);
			int body_x[]={centerx-25,centerx-25,centerx+25,centerx+25};
			int body_y[]={centery-5,centery+5,centery+5,centery-5};
			g.drawPolygon(body_x,body_y, body_x.length);
			g.fillPolygon(body_x,body_y, body_x.length);
			
			//change direction
			if(centerx+30>=getWidth())
				dir=1;
			if(centerx-30<=0)
				dir=0;
			if(dir==0)
				centerx+=1;
			if(dir==1)
				centerx-=1;
		}
	}
}