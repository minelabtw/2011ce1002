package a11.s100502011;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.*;

public class A111 extends JApplet implements ActionListener{
	private JPanel jpcolor1 = new JPanel();
	private JPanel jpcolor2 = new JPanel();
	private JPanel jpcolor3 = new JPanel();
	private JPanel jpcar1 = new JPanel();
	private JPanel jpcar2 = new JPanel();
	private JPanel jpcar3 = new JPanel();
	private JPanel jptotal = new JPanel();
	private JPanel jptf1 = new JPanel();
	private JPanel jptf2 = new JPanel();
	private JPanel jptf3 = new JPanel();
	private JPanel jpSpeedTotal = new JPanel();
	private JRadioButton jrbOrange1 = new JRadioButton("Orange");
	private JRadioButton jrbGreen1=new JRadioButton("Green");
	private JRadioButton jrbPink1=new JRadioButton("Pink");
	private JRadioButton jrbOrange2 = new JRadioButton("Orange");
	private JRadioButton jrbGreen2=new JRadioButton("Green");
	private JRadioButton jrbPink2=new JRadioButton("Pink");
	private JRadioButton jrbOrange3 = new JRadioButton("Orange");
	private JRadioButton jrbGreen3=new JRadioButton("Green");
	private JRadioButton jrbPink3=new JRadioButton("Pink");
	private JTextField jtfspeed1=new JTextField(6); // to enter the speed of car1
	private JTextField jtfspeed2=new JTextField(6); // to enter the speed of car2
	private JTextField jtfspeed3=new JTextField(6); // to enter the speed of car3
	private Border line = new LineBorder(Color.BLACK,2); // set border
	private JLabel lab1 = new JLabel("Car1");
	private JLabel lab2 = new JLabel("Car2");
	private JLabel lab3 = new JLabel("Car3");
	private RaceCar car1 = new RaceCar(); // car1
	private	RaceCar car2 = new RaceCar(); // car2
	private	RaceCar car3 = new RaceCar(); // car3
	
	public A111(){ // constructor
		
		// to group car1's button
		ButtonGroup group1 = new ButtonGroup();
		group1.add(jrbOrange1);
		group1.add(jrbGreen1);
		group1.add(jrbPink1);
		
		// to group car2's button
		ButtonGroup group2 = new ButtonGroup();
		group2.add(jrbOrange2);
		group2.add(jrbGreen2);
		group2.add(jrbPink2);
		
		// to group car3's button
		ButtonGroup group3 = new ButtonGroup();
		group3.add(jrbOrange3);
		group3.add(jrbGreen3);
		group3.add(jrbPink3);
		
		// to set car1's panel
		jpcar1.setLayout(new BorderLayout(5,5));
		jpcolor1.setLayout(new GridLayout(3,1));
		jrbOrange1.setForeground(Color.ORANGE);
		jrbGreen1.setForeground(Color.GREEN);
		jrbPink1.setForeground(Color.PINK);
		jpcolor1.add(jrbOrange1);
		jpcolor1.add(jrbGreen1);
		jpcolor1.add(jrbPink1);
		jpcolor1.setBorder(line);
		jpcar1.add(jpcolor1,BorderLayout.WEST);
		jpcar1.add(car1,BorderLayout.CENTER);
		
		// to set car2's panel
		jpcar2.setLayout(new BorderLayout(5,5));
		jpcolor2.setLayout(new GridLayout(3,1));
		jrbOrange2.setForeground(Color.ORANGE);
		jrbGreen2.setForeground(Color.GREEN);
		jrbPink2.setForeground(Color.PINK);
		jpcolor2.add(jrbOrange2);
		jpcolor2.add(jrbGreen2);
		jpcolor2.add(jrbPink2);
		jpcolor2.setBorder(line);
		jpcar2.add(jpcolor2,BorderLayout.WEST);
		jpcar2.add(car2,BorderLayout.CENTER);
		
		// to set car3's panel
		jpcar3.setLayout(new BorderLayout(5,5));
		jpcolor3.setLayout(new GridLayout(3,1));
		jrbOrange3.setForeground(Color.ORANGE);
		jrbGreen3.setForeground(Color.GREEN);
		jrbPink3.setForeground(Color.PINK);
		jpcolor3.add(jrbOrange3);
		jpcolor3.add(jrbGreen3);
		jpcolor3.add(jrbPink3);
		jpcolor3.setBorder(line);
		jpcar3.add(jpcolor3,BorderLayout.WEST);
		jpcar3.add(car3,BorderLayout.CENTER);
		
		// to set the speed of three cars' panel
		jptotal.setLayout(new GridLayout(3,1));
		jptotal.add(jpcar1,BorderLayout.NORTH);
		jptotal.add(jpcar2,BorderLayout.CENTER);
		jptotal.add(jpcar3,BorderLayout.SOUTH);
		jptf1.add(lab1,BorderLayout.WEST);
		jptf1.add(jtfspeed1,BorderLayout.CENTER);
		jptf2.add(lab2,BorderLayout.WEST);
		jptf2.add(jtfspeed2,BorderLayout.CENTER);
		jptf3.add(lab3,BorderLayout.WEST);
		jptf3.add(jtfspeed3,BorderLayout.CENTER);
		jpSpeedTotal.setLayout(new GridLayout(1,3));
		jpSpeedTotal.add(jptf1);
		jpSpeedTotal.add(jptf2);
		jpSpeedTotal.add(jptf3);
		
		// to make button button active
		jtfspeed1.addActionListener(this);
		jtfspeed2.addActionListener(this);
		jtfspeed3.addActionListener(this);
		jrbOrange1.addActionListener(this);
		jrbGreen1.addActionListener(this);
		jrbPink1.addActionListener(this);
		jrbOrange2.addActionListener(this);
		jrbGreen2.addActionListener(this);
		jrbPink2.addActionListener(this);
		jrbOrange3.addActionListener(this);
		jrbGreen3.addActionListener(this);
		jrbPink3.addActionListener(this);
		
		// add to applet
		setLayout(new BorderLayout(5,5));
		add(jpSpeedTotal,BorderLayout.NORTH);
		add(jptotal,BorderLayout.CENTER);
	}
	
	public void actionPerformed(ActionEvent e){ // button action
		if(e.getSource()==jrbOrange1){
			car1.setColor(Color.ORANGE);
		}
		if(e.getSource()==jrbGreen1){
			car1.setColor(Color.GREEN);
		}
		if(e.getSource()==jrbPink1){
			car1.setColor(Color.PINK);
		}
		if(e.getSource()==jrbOrange2){
			car2.setColor(Color.ORANGE);
		}
		if(e.getSource()==jrbGreen2){
			car2.setColor(Color.GREEN);
		}
		if(e.getSource()==jrbPink2){
			car2.setColor(Color.PINK);
		}
		if(e.getSource()==jrbOrange3){
			car3.setColor(Color.ORANGE);
		}
		if(e.getSource()==jrbGreen3){
			car3.setColor(Color.GREEN);
		}
		if(e.getSource()==jrbPink3){
			car3.setColor(Color.PINK);
		}
		if(e.getSource()==jtfspeed1){
			if(jtfspeed1.getText()!=""){
				int Speed1=Integer.parseInt(jtfspeed1.getText());
				car1.setSpeed(1000/Speed1);
			}
		}
		if(e.getSource()==jtfspeed2){
			if(jtfspeed2.getText()!=""){
				int Speed2=Integer.parseInt(jtfspeed2.getText());
				car2.setSpeed(1000/Speed2);
			}
		}
		if(e.getSource()==jtfspeed3){
			if(jtfspeed3.getText()!=""){
				int Speed3=Integer.parseInt(jtfspeed3.getText());
				car3.setSpeed(1000/Speed3);
			}
		}
	}
}
