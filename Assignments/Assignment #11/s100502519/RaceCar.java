package a11.s100502519;
import javax.swing.*;
import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.event.*;

public class RaceCar extends JPanel{
	private int x0 = getX();
	private int y0 = getHeight()+100;
	int speed = 5;

	Color c;
	Timer timer = new Timer(40,new TimerListener());
	
	/*-----constructor-----*/
	public RaceCar(){
		timer.start();
	}
	
	/*TimerListener*/
	class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			repaint();
		}
	}
	
	/*-----setSpeed-----*/
	public void setSpeed(int i){
		switch(i){
			case 1:
				speed = 1;
				break;
			case 2:
				speed = 2;
				break;
			case 3:
				speed = 2;
				break;
			case 4:
				speed = 4;
				break;
			case 5:
				speed = 5;
				break;
			
		}
	}
	
	/*-----setColor-----*/
	public void setColor(int i){
		switch(i){
			case 1:
				c = new Color(255,0,0);
				break;
			case 2:
				c = new Color(0,255,0);
				break;
			case 3:
				c = new Color(0,0,255);
				break;
		}
	}
	
	/*-----setStyle-----*/
	public void setStyle(){
		
	}
	
	protected void paintComponent(Graphics g){
		super.paintComponents(g);
		
		Polygon body = new Polygon();
		Polygon roof = new Polygon();
		body.addPoint(x0, y0-10);
		body.addPoint(x0, y0-20);
		roof.addPoint(x0+10, y0-20);
		roof.addPoint(x0+20, y0-30);
		roof.addPoint(x0+30, y0-30);
		roof.addPoint(x0+40, y0-20);
		body.addPoint(x0+50, y0-20);
		body.addPoint(x0+50, y0-10);
		
		g.setColor(c);
		g.drawPolygon(body);
		g.drawPolygon(roof);
		g.drawOval(x0+10, y0-10, 10, 10);
		g.drawOval(x0+30, y0-10, 10, 10);
		
		x0=x0+speed;
		
		if(x0+50>=getWidth()||x0<=getX()){
			speed=speed*(-1);
		}
	}
	
}
