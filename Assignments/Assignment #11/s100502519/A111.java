package a11.s100502519;
import javax.swing.*;

import org.omg.CORBA.PUBLIC_MEMBER;

import java.awt.*;
import java.awt.event.*;

public class A111 extends JApplet{
	
	/*-----main-----*/
	/*public static void main(String[] args){
		A111 frame = new A111();
		frame.setTitle("RaceCar");
		frame.setSize(1200,600);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		//frame.setResizable(false);
	}*/

	/*-----FrameWork-----*/
	public A111(){
		
		/*new*/
		JPanel panel_car1_block = new JPanel();
		JPanel panel_car2_block = new JPanel();
		JPanel panel_car3_block = new JPanel();
		final RaceCar car1 = new RaceCar();
		final RaceCar car2 = new RaceCar();
		final RaceCar car3 = new RaceCar();
		JPanel panel_control1 = new JPanel();
		JPanel panel_control2 = new JPanel();
		JPanel panel_control3 = new JPanel();
		String[] string_speedLevel = {"Level 1" , "Level 2" , "Level 3" , "Level 4" , "Level 5"};
		final JComboBox comboBox_speed1 = new JComboBox(string_speedLevel);
		final JComboBox comboBox_speed2 = new JComboBox(string_speedLevel);
		final JComboBox comboBox_speed3 = new JComboBox(string_speedLevel);
		JPanel panel_radioButton1 = new JPanel();
		JPanel panel_radioButton2 = new JPanel();
		JPanel panel_radioButton3 = new JPanel();
		ButtonGroup group1 = new ButtonGroup();
		ButtonGroup group2 = new ButtonGroup();
		ButtonGroup group3 = new ButtonGroup();
		JRadioButton radioButton_red1 = new JRadioButton("Red");
		JRadioButton radioButton_blue1 = new JRadioButton("Blue");
		JRadioButton radioButton_green1 = new JRadioButton("Green");
		JRadioButton radioButton_red2 = new JRadioButton("Red");
		JRadioButton radioButton_blue2 = new JRadioButton("Blue");
		JRadioButton radioButton_green2 = new JRadioButton("Green");
		JRadioButton radioButton_red3 = new JRadioButton("Red");
		JRadioButton radioButton_blue3 = new JRadioButton("Blue");
		JRadioButton radioButton_green3 = new JRadioButton("Green");
		String[] string_style = {"Car" , "UFO" , "Fly"};
		JComboBox comboBox_style1 = new JComboBox(string_style);
		JComboBox comboBox_style2 = new JComboBox(string_style);
		JComboBox comboBox_style3 = new JComboBox(string_style);
		
		/*layout*/
		setLayout(new GridLayout(3,1,0,0));
		panel_car1_block.setLayout(new BorderLayout(0,0));
		panel_car2_block.setLayout(new BorderLayout(0,0));
		panel_car3_block.setLayout(new BorderLayout(0,0));
		panel_control1.setLayout(new GridLayout(1,3,0,0));
		panel_control2.setLayout(new GridLayout(1,3,0,0));
		panel_control3.setLayout(new GridLayout(1,3,0,0));
		
		/*add*/
		add(panel_car1_block);
		add(panel_car2_block);
		add(panel_car3_block);
		panel_car1_block.add(car1,BorderLayout.CENTER);
		panel_car1_block.add(panel_control1,BorderLayout.EAST);
		panel_car2_block.add(car2,BorderLayout.CENTER);
		panel_car2_block.add(panel_control2,BorderLayout.EAST);
		panel_car3_block.add(car3,BorderLayout.CENTER);
		panel_car3_block.add(panel_control3,BorderLayout.EAST);
		panel_radioButton1.add(radioButton_red1);
		panel_radioButton1.add(radioButton_green1);
		panel_radioButton1.add(radioButton_blue1);
		panel_radioButton2.add(radioButton_red2);
		panel_radioButton2.add(radioButton_green2);
		panel_radioButton2.add(radioButton_blue2);
		panel_radioButton3.add(radioButton_red3);
		panel_radioButton3.add(radioButton_green3);
		panel_radioButton3.add(radioButton_blue3);
		group1.add(radioButton_red1);
		group1.add(radioButton_green1);
		group1.add(radioButton_blue1);
		group2.add(radioButton_red2);
		group2.add(radioButton_green2);
		group2.add(radioButton_blue2);
		group3.add(radioButton_red3);
		group3.add(radioButton_green3);
		group3.add(radioButton_blue3);
		panel_control1.add(comboBox_speed1);
		panel_control1.add(panel_radioButton1);
		panel_control1.add(comboBox_style1);
		panel_control2.add(comboBox_speed2);
		panel_control2.add(panel_radioButton2);
		panel_control2.add(comboBox_style2);
		panel_control3.add(comboBox_speed3);
		panel_control3.add(panel_radioButton3);
		panel_control3.add(comboBox_style3);
		
		/*combox_speed*/
		comboBox_speed1.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e){
				car1.setSpeed(comboBox_speed1.getSelectedIndex());
			}
		});
		
		comboBox_speed2.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e){
				car2.setSpeed(comboBox_speed2.getSelectedIndex());
			}
		});
		
		comboBox_speed3.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e){
				car3.setSpeed(comboBox_speed3.getSelectedIndex());
			}
		});
		
		/*radioButton_color*/
		radioButton_red1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				car1.setColor(1);
			}
		});
		
		radioButton_green1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				car1.setColor(2);
			}
		});
		
		radioButton_blue1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				car1.setColor(3);
			}
		});
		
		radioButton_red2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				car2.setColor(1);
			}
		});
		
		radioButton_green2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				car2.setColor(2);
			}
		});
		
		radioButton_blue2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				car2.setColor(3);
			}
		});
		
		radioButton_red3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				car3.setColor(1);
			}
		});
		
		radioButton_green3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				car3.setColor(2);
			}
		});
		
		radioButton_blue3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				car3.setColor(3);
			}
		});

	}
}
