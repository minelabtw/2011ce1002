package a11.s100502517;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class RaceCar extends JPanel{
	private int xCoordinate = 0;
	Color color = new Color(255,0,0);
	private boolean change = true;
	Timer timer = new Timer(50, new TimerListener());
	
	//一開始就讓車車跑	
	public RaceCar(){
		
		timer.start();
	}
	
	//改變車體顏色
	public void setColor(int change){
		switch(change){
			case 1:
				color = new Color(255,0,0);
				break;
			case 2:
				color = new Color(0,255,0);
				break;
			case 3:
				color = new Color(0,0,255);
				break;
		}
		
	}
	
	//改變速度
	public void setSpeed(int speed){
		timer.setDelay(speed);
	}
	
	//車 車
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		int width = getWidth();//視窗寬
		int height = getHeight();//視窗高
		
		//判 斷 是 否 撞 牆
		if(xCoordinate > getWidth()-50){
			//xCoordinate = getWidth()-50;
			change = false;
		}
		if(xCoordinate < 0){
			xCoordinate = 0;
			change = true;
		}
		if(change){
			xCoordinate += 5;
		}
		else{
			xCoordinate -= 5;
		}
		int x[] = {xCoordinate+5,xCoordinate+10,xCoordinate+40,xCoordinate+45};
		int y[] = {(height/2)-10, (height/2)-20, (height/2)-20,(height/2)-10};
		
		g.setColor(color);
		g.fillRect(xCoordinate,(height/2)-10,50,20);
		g.setColor(Color.BLACK);
		g.fillOval(xCoordinate+7, (height/2)+10 , 12 , 12);
		g.fillOval(xCoordinate+31, (height/2)+10 , 12 , 12);
		g.setColor(Color.WHITE);
		g.fillPolygon(x, y, x.length);
	}
	
	//讓車車移動
	class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			repaint();
		}
	}

}
