package a11.s100502517;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class A111 extends JApplet{
	
	private JTextField firstcarspeed = new JTextField();
	private JTextField secondcarspeed = new JTextField();
	private JTextField thirdcarspeed = new JTextField();
	private RaceCar car1 = new RaceCar();
	private RaceCar car2 = new RaceCar();
	private RaceCar car3 = new RaceCar();
	
	private JRadioButton Red, Green, Blue;
	private JRadioButton Red2, Green2, Blue2;
	private JRadioButton Red3, Green3, Blue3;
	
	private ButtonGroup group = new ButtonGroup();
	private ButtonGroup group2 = new ButtonGroup();
	private ButtonGroup group3 = new ButtonGroup();
	
	
	//設計版面
	public A111(){
		
		//p0是改變速度的排版
		JPanel p0 = new JPanel(new GridLayout(1,6));
		p0.add(new JLabel("No.1 speed"));
		p0.add(firstcarspeed);
		p0.add(new JLabel("No.2 speed"));
		p0.add(secondcarspeed);
		p0.add(new JLabel("No.3 speed"));
		p0.add(thirdcarspeed);
		
		//p1~p3是no.1~no.3車體顏色的排版
		JPanel p1 = new JPanel(new GridLayout(3,1));
		p1.add(Red = new JRadioButton("Red"));
		p1.add(Green = new JRadioButton("Green"));
		p1.add(Blue = new JRadioButton("Blue"));
		group.add(Red);
		group.add(Green);
		group.add(Blue);
		
		JPanel p2 = new JPanel(new GridLayout(3,1));
		p2.add(Red2 = new JRadioButton("Red"));
		p2.add(Green2 = new JRadioButton("Green"));
		p2.add(Blue2 = new JRadioButton("Blue"));
		group2.add(Red2);
		group2.add(Green2);
		group2.add(Blue2);
		
		JPanel p3 = new JPanel(new GridLayout(3,1));
		p3.add(Red3 = new JRadioButton("Red"));
		p3.add(Green3 = new JRadioButton("Green"));
		p3.add(Blue3 = new JRadioButton("Blue"));
		group3.add(Red3);
		group3.add(Green3);
		group3.add(Blue3);
		
		//p4~p6是呼叫RaceCar和車體顏色結合的排版
		JPanel p4 = new JPanel(new BorderLayout(1,2));
		p4.add(p1,BorderLayout.WEST);
		p4.add(car1,BorderLayout.CENTER);
		
		JPanel p5 = new JPanel(new BorderLayout(1,2));
		p5.add(p2,BorderLayout.WEST);
		p5.add(car2,BorderLayout.CENTER);
		
		JPanel p6 = new JPanel(new BorderLayout(1,2));
		p6.add(p3,BorderLayout.WEST);
		p6.add(car3,BorderLayout.CENTER);
		
		//三車結合
		JPanel p7 = new JPanel(new GridLayout(3,1));
		p7.add(p4);
		p7.add(p5);
		p7.add(p6);
		
		//總結合
		setLayout(new BorderLayout());
		add(p0, BorderLayout.NORTH);
		add(p7);
		
		Red.addActionListener(new ChangecolorListener());
		Green.addActionListener(new ChangecolorListener());
		Blue.addActionListener(new ChangecolorListener());
		Red2.addActionListener(new ChangecolorListener());
		Green2.addActionListener(new ChangecolorListener());
		Blue2.addActionListener(new ChangecolorListener());
		Red3.addActionListener(new ChangecolorListener());
		Green3.addActionListener(new ChangecolorListener());
		Blue3.addActionListener(new ChangecolorListener());
		
		firstcarspeed.addActionListener(new TextListener());
		secondcarspeed.addActionListener(new TextListener());
		thirdcarspeed.addActionListener(new TextListener());
		}
	
		//改變顏色的ActionListener
		class ChangecolorListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				if(e.getSource() == Red){
					car1.setColor(1);
				}
				if(e.getSource() == Green){
					car1.setColor(2);
				}
				if(e.getSource() == Blue){
					car1.setColor(3);
				}
				if(e.getSource() == Red2){
					car2.setColor(1);
				}
				if(e.getSource() == Green2){
					car2.setColor(2);
				}
				if(e.getSource() == Blue2){
					car2.setColor(3);
				}
				if(e.getSource() == Red3){
					car3.setColor(1);
				}
				if(e.getSource() == Green3){
					car3.setColor(2);
				}
				if(e.getSource() == Blue3){
					car3.setColor(3);
				}			
				
			}		
		}
		
		//改變速度的ActionListener
		class TextListener implements ActionListener{
			public void actionPerformed(ActionEvent e){
				int speed1 = Integer.parseInt(firstcarspeed.getText());
				
				if(speed1 >= 1000){
					speed1 = 1000;
				}
				else if(speed1 <= 1){
					speed1 = 1;				
				}
				
				car1.setSpeed(speed1);
				
				int speed2 = Integer.parseInt(secondcarspeed.getText());
				
				if(speed2 >= 1000){
					speed2 = 1000;
				}
				else if(speed2 <= 1){
					speed2 = 1;				
				}
				
				car2.setSpeed(speed2);
				
				int speed3 = Integer.parseInt(thirdcarspeed.getText());
				
				if(speed3 >= 1000){
					speed3 = 1000;
				}
				else if(speed3 <= 1){
					speed3 = 1;
				}
				
				car3.setSpeed(speed3);
				}
			}
		}
	
	



