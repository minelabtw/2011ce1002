package a11.s100502004;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class A111 extends JApplet{//to create the frame and buttons 
	
	public String[] speedType = {"Slow" , "Middle" , "Fast"};
	RaceCar c1 = new RaceCar();//create three cars
	RaceCar c2 = new RaceCar();
	RaceCar c3 = new RaceCar();
	JLabel spLabel1 = new JLabel("speed:");
	JLabel spLabel2 = new JLabel("speed:");
	JLabel spLabel3 = new JLabel("speed:");

	JLabel cLabel1 = new JLabel("change color:");
	JLabel cLabel2 = new JLabel("change color:");
	JLabel cLabel3 = new JLabel("change color:");
	JRadioButton color1G = new JRadioButton("Green");
	JRadioButton color2G = new JRadioButton("Green");
	JRadioButton color3G = new JRadioButton("Green");
	JRadioButton color1R = new JRadioButton("Red:");
	JRadioButton color2R = new JRadioButton("Red:");
	JRadioButton color3R = new JRadioButton("Red:");
	JRadioButton color1B = new JRadioButton("Blue");
	JRadioButton color2B = new JRadioButton("Blue");
	JRadioButton color3B = new JRadioButton("Blue");
	JButton changeSpeed = new JButton("Set Speed");
	JButton style1 = new JButton("Style");
	JButton style2 = new JButton("Style");
	JButton style3 = new JButton("Style");
	JPanel p1R = new JPanel();
	JPanel p1RU = new JPanel();
	JPanel p1RD = new JPanel();
	JPanel p1T = new JPanel();
	JPanel p2R = new JPanel();
	JPanel p2RU = new JPanel();
	JPanel p2RD = new JPanel();
	JPanel p2T = new JPanel();
	JPanel p3R = new JPanel();
	JPanel p3RU = new JPanel();
	JPanel p3RD = new JPanel();
	JPanel p3T = new JPanel();
	JComboBox s1 = new JComboBox(speedType);
	JComboBox s2 = new JComboBox(speedType);
	JComboBox s3 = new JComboBox(speedType);
	ButtonGroup g1 = new ButtonGroup();
	ButtonGroup g2 = new ButtonGroup();
	ButtonGroup g3 = new ButtonGroup();
	
	public A111(){
		g1.add(color1B);//set that you can just press one radio button in one time
		g1.add(color1G);
		g1.add(color1R);
		g2.add(color2B);
		g2.add(color2G);
		g2.add(color2R);
		g3.add(color3B);
		g3.add(color3G);
		g3.add(color3R);
		
		
		
		
		color1G.addActionListener(new  ActionListener() {//add action listeners
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				c1.setColor(Color.green);
			}
		});
		
		color1R.addActionListener(new  ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				c1.setColor(Color.RED);
			}
		});		
		
		color1B.addActionListener(new  ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				c1.setColor(Color.BLUE);
			}
		});
		
		color2G.addActionListener(new  ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				c2.setColor(Color.green);
			}
		});
		
		color2R.addActionListener(new  ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				c2.setColor(Color.RED);
			}
		});
		
		color2B.addActionListener(new  ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				c2.setColor(Color.BLUE);
			}
		});
		
		color3G.addActionListener(new  ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				c3.setColor(Color.green);
			}
		});
		
		color3R.addActionListener(new  ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				c3.setColor(Color.RED);
			}
		});
		
		color3B.addActionListener(new  ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				c3.setColor(Color.BLUE);
			}
		});
		
		s1.addItemListener(new ItemListener() {//action listener
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				c1.setSpeed(s1.getSelectedIndex());				
			}	
		});
		
		s2.addItemListener(new ItemListener() {//action listener
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				c2.setSpeed(s2.getSelectedIndex());				
			}	
		});

		s3.addItemListener(new ItemListener() {//action listener
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				c3.setSpeed(s3.getSelectedIndex());				
			}	
		});
		//�ƪ�
		
		p1RU.setLayout(new GridLayout(1,4));
		p1RU.add(cLabel1);			
		p1RU.add(color1G);	
		p1RU.add(color1R);	
		p1RU.add(color1B);		
		p1RD.setLayout(new GridLayout(1,2));
		p1RD.add(spLabel1);
		p1RD.add(s1);
		p1R.setLayout(new GridLayout(2,1));
		p1R.add(p1RU);
		p1R.add(p1RD);
		p1T.setLayout(new GridLayout(1,2));
		p1T.add(c1);
		p1T.add(p1R);	
		p2RU.setLayout(new GridLayout(1,4));
		p2RU.add(cLabel2);			
		p2RU.add(color2G);		
		p2RU.add(color2R);		
		p2RU.add(color2B);
		p2RD.setLayout(new GridLayout(1,2));
		p2RD.add(spLabel2);
		p2RD.add(s2);
		p2R.setLayout(new GridLayout(2,1));
		p2R.add(p2RU);
		p2R.add(p2RD);
		p2T.setLayout(new GridLayout(1,2));
		p2T.add(c2);
		p2T.add(p2R);	
		p3RU.setLayout(new GridLayout(1,3));
		p3RU.add(cLabel3);	
		p3RU.add(color3G);		
		p3RU.add(color3R);			
		p3RU.add(color3B);
		p3RD.setLayout(new GridLayout(1,2));
		p3RD.add(spLabel3);
		p3RD.add(s3);
		p3R.setLayout(new GridLayout(2,1));
		p3R.add(p3RU);
		p3R.add(p3RD);
		p3T.setLayout(new GridLayout(1,2));
		p3T.add(c3);
		p3T.add(p3R);		
		setLayout(new GridLayout(3,1));		
		add(p1T);
		add(p2T);
		add(p3T);
	}
	
	
}
