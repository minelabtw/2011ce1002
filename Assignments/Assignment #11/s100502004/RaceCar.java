package a11.s100502004;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.Timer;

public class RaceCar extends JPanel{//to draw the car
	int xCoordinate = 200;
	boolean change = false;
	int speed = 1000;
	int speedArray [] ={10,100,1000};
	Color color = new Color(255,0,0);
	Timer timer = new Timer(speed, new TimerListener());
	
	public RaceCar() {		
		
		timer.start();
		
	}
	
	public void setSpeed(int s){
		
		if(s == 2){
			timer.setDelay(10);
		}
		if(s == 1){
			timer.setDelay(100);
		}
		if(s == 0){
			timer.setDelay(1000);
		}
		
	}
	
	public void setColor(Color c){
		color = c;
	}
	
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		if(xCoordinate+50 > getWidth()) { // 超過右邊的邊界
			change = true; // 換方向
		}
		if(change) {
			xCoordinate -= 5; // 每次減5，即向左
			if(xCoordinate == 0) // 跑到最左邊時又往返
				change = false;
		}
		else {
			xCoordinate += 5; // 預設為向右跑
		}
		// draw car
		int x1[]={xCoordinate+20, xCoordinate+30, xCoordinate+40, xCoordinate+10};
		int y1[]={70, 70, 80, 80};
		g.setColor(color);
		g.fillPolygon(x1, y1, x1.length);
		g.fillRect(xCoordinate, 80, 50, 10);
		g.fillOval(xCoordinate+10, 90, 10, 10);
		g.fillOval(xCoordinate+30, 90, 10, 10);
		
	}
	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			repaint();
		}
	}
}
