package a11.s982003034;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class RaceCar extends JPanel implements ActionListener {
	
	private Timer speed;
	private Color color;
	
	private Car car;
	
	private int dx; // car's x change
	private int dy; // car's y change
	private int dir; // direction
	
	public RaceCar () {
		
		speed = new Timer (1000,this);
		color = Color.red;
		car = new Car ();
		dx = 0;
		dy = this.getHeight()/2 + 25;
		dir = 1;
		car.update_pos(dx, dy);
		speed.start();
		
	}
	
	public void set_speed (int v) {
		speed.setDelay(v);
	}
	
	public void inc_speed () {
		if (speed.getDelay() > 100) speed.setDelay(speed.getDelay()-200);
	}
	
	public void dec_speed () {
		if (speed.getDelay() < 2000) speed.setDelay(speed.getDelay()+200);
	}
	
	public void set_color (Color new_color) {
		color = new_color;
	}
	
	@Override
	public void paintComponent (Graphics g) {
		
		super.paintComponent(g);
		// draw body
		g.setColor(color);
		g.fillPolygon(car.bodyx, car.bodyy, car.bodyc);
		
		// draw wheels
		g.setColor(Color.black);
		// draw front wheels
		g.fillOval(car.wheelf[0], car.wheelf[1], 5, 5);
		// draw rear wheels
		g.fillOval(car.wheelr[0], car.wheelr[1], 5, 5);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (dx > this.getWidth()-20) dir = -1;
		if (dx < 5) dir = 1;
		dx= dir*10;
		car.update_pos(dx, 0);
		this.repaint();		
	}
	
}
