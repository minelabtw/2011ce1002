package a11.s982003034;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class RaceLane extends JPanel implements ActionListener {
	
	RaceCar rc;
	JPanel rc_control;
	JButton rc_incspeed;
	JButton rc_decspeed;
	JRadioButton rc_red;
	JRadioButton rc_green;
	JRadioButton rc_blue;
	ButtonGroup rc_group;
	
	public RaceLane () {
		
		rc = new RaceCar();
		
		rc_control = new JPanel();
		rc_control.setLayout(new GridLayout (5,1));
		
		rc_incspeed = new JButton("Increase Speed");
		rc_incspeed.addActionListener(this);
		rc_control.add(rc_incspeed);
		
		rc_decspeed = new JButton("Decrease Speed");
		rc_decspeed.addActionListener(this);
		rc_control.add(rc_decspeed);
		
		rc_red = new JRadioButton("Red");
		rc_red.setActionCommand("red");
		rc_red.addActionListener(this);
		rc_control.add(rc_red);
		rc_red.setSelected(true);
		
		rc_green = new JRadioButton("Green");
		rc_green.setActionCommand("green");
		rc_green.addActionListener(this);
		rc_control.add(rc_green);
		
		rc_blue = new JRadioButton("Blue");
		rc_blue.setActionCommand("blue");
		rc_blue.addActionListener(this);
		rc_control.add(rc_blue);
		
		rc_group = new ButtonGroup();
		rc_group.add(rc_red);
		rc_group.add(rc_green);
		rc_group.add(rc_blue);
		
		this.setLayout(new BorderLayout());
		this.add(rc_control, BorderLayout.WEST);
		this.add(rc, BorderLayout.CENTER);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == rc_incspeed) rc.inc_speed();
		if (e.getSource() == rc_decspeed) rc.dec_speed();
		if (e.getSource() == rc_red) rc.set_color(Color.red);
		if (e.getSource() == rc_green) rc.set_color(Color.green);
		if (e.getSource() == rc_blue) rc.set_color(Color.blue);
	}
	
}
