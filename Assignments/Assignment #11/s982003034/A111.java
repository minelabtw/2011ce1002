package a11.s982003034;

import java.applet.Applet;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class A111 extends Applet {

	public void init () {
		
		RaceLane rl1 = new RaceLane();
		RaceLane rl2 = new RaceLane();
		RaceLane rl3 = new RaceLane();
		
		this.setLayout(new GridLayout (3,1));
		this.add(rl1);
		this.add(rl2);
		this.add(rl3);
		this.setVisible(true);
		
	}
	
}
