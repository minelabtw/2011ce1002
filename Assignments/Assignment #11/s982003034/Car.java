package a11.s982003034;

public class Car {

	public int[] bodyx; // body's x
	public int[] bodyy; // body's y
	public int bodyc; // body's vertex count
	
	public int[] wheelf; // front wheel
	public int[] wheelr; // rear wheel
	public int wheelc; // wheel's count
	
	public Car () {
		
		bodyx = new int[8];
		bodyx[0] = 0;
		bodyx[1] = 5;
		bodyx[2] = 10;
		bodyx[3] = 15;
		bodyx[4] = 20;
		bodyx[5] = 25;
		bodyx[6] = 25;
		bodyx[7] = 0;
		bodyy = new int[8];
		bodyy[0] = 5;
		bodyy[1] = 5;
		bodyy[2] = 0;
		bodyy[3] = 0;
		bodyy[4] = 5;
		bodyy[5] = 5;
		bodyy[6] = 10;
		bodyy[7] = 10;
		bodyc = 8;
		
		wheelf = new int[2];
		wheelf[0] = 3;
		wheelf[1] = 10;
		wheelr = new int[2];
		wheelr[0] = 12;
		wheelr[1] = 10;
		
	}
	
	// function to update car's position
	public void update_pos (int dx, int dy) {
		for (int i = 0; i < bodyc; i++){
			bodyx[i] += dx;
			bodyy[i] += dy;
		}
		wheelf[0] += dx;
		wheelf[1] += dy;
		wheelr[0] += dx;
		wheelr[1] += dy;
	}
}
