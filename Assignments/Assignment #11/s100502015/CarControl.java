package a11.s100502015;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class CarControl extends JPanel {		
	private JRadioButton jrbred , jrbgreen ,jrbblue;
	private JScrollBar jsbhor = new JScrollBar(JScrollBar.HORIZONTAL);
	private int delay = 50;
	private JPanel p1 = new JPanel();
	private JPanel p2 = new JPanel(); 
	private JPanel p3 = new JPanel();
	private Car c1 = new Car(); 
	public CarControl()
	{					
		p3.setLayout(new GridLayout(3,1));			
		p3.add(jrbred = new JRadioButton("Red"));
		p3.add(jrbblue = new JRadioButton("Blue"));
		p3.add(jrbgreen = new JRadioButton("Green"));
		ButtonGroup group = new ButtonGroup();
		group.add(jrbred);
		group.add(jrbblue);
		group.add(jrbgreen);
		p2.setLayout(new GridLayout(2,1));
		p2.add(jsbhor);
		p2.add(p3);
		p1.setLayout(new GridLayout(2,1));			
		p1.add(c1);
		p1.add(p2);			
		add(p1);
		//change speed scroll
		jsbhor.addAdjustmentListener(new AdjustmentListener()
		{
			public void adjustmentValueChanged(AdjustmentEvent e)
			{
				int value = jsbhor.getValue();
				int max = jsbhor.getMaximum();
				c1.dx = value;
			}
		});
		//change color button
		jrbred.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				c1.color = new Color(255,0,0);
			}
		});

		jrbblue.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				c1.color = new Color(0,255,0);
			}
		});

		jrbgreen.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				c1.color = new Color(0,0,255);
			}
		});	
		Timer timer = new Timer(delay,new TimerListener());
		timer.start();
	}			
	class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			repaint();
		}
	}
}

