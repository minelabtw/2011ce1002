package a11.s100502015;
import javax.swing.*;
import java.awt.*;
import java.awt.Graphics;
public class Car extends JPanel{
	protected int xcor = 0;
	protected int ycor = 50;
	protected Color color = new Color(0,255,255);	
	protected Color color2 = new Color(255,255,100);	
	protected int dx = 2;	
	private boolean back = true; 	
	private int triangley[] = {50,40,40,50};	
	protected void paintComponent(Graphics g) 
	{
		super.paintComponent(g);			
		if (xcor > getWidth()-50)//if x >width then it will back
		{
			back = false;
		}
		else if(xcor < 0)
		{
			back = true;
		}
		if(back==true)
		{
			xcor+=dx;
		}
		else if(back==false)
		{
			xcor-=dx;
		}
		int trianglex[] = {10+xcor,20+xcor,30+xcor,40+xcor};
		g.setColor(color2);
		g.fillRect(xcor, ycor, 50, 10);
		
		g.setColor(color);
		g.fillPolygon(trianglex, triangley,4);	//draw 	���
		g.setColor(color2);
		g.fillOval(xcor+10, ycor+10, 10, 10);//draw circle		
		g.fillOval(xcor+30, ycor+10, 10, 10);
		
	}
	
	
}
