package a11.s995002026;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;

import javax.swing.*;

public class A111 extends JApplet{
	FrameWork f=new FrameWork();
	public A111(){
	add(f);
	}
}
class FrameWork extends JApplet implements ActionListener{
	String[] choice1={"car1","car2","car3"};
	String[] choice2={"style1","style2","style3"};
	String[] choice3={"green","red","blue"};
	JButton speed=new JButton("speed");
	
	
	Panel p1=new Panel(new GridLayout(1,4));	//選項列
	Panel p3=new Panel(new GridLayout(3,1));  //車道
	JComboBox car=new JComboBox(choice1);
	JComboBox style=new JComboBox(choice2);
	JComboBox color=new JComboBox(choice3);
	Racecar[] r=new Racecar[3];
	

	public FrameWork(){
		r[0]=new Racecar();
		r[1]=new Racecar();
		r[2]=new Racecar();
		color.addActionListener(this);
		speed.addActionListener(this);
		style.addActionListener(this);
		p3.add(r[0]);
		p3.add(r[1]);
		p3.add(r[2]);
		p1.add(car);
		p1.add(color);
		p1.add(speed);
		p1.add(style);
		add(p3);
		add(p1,BorderLayout.SOUTH);
	}
	
	public void actionPerformed(ActionEvent e){
			if(e.getSource()==color){		//設定車的顏色
				r[car.getSelectedIndex()].setColor(color.getSelectedIndex());
			}
			if(e.getSource()==speed){		//設定車的速度
				String s=JOptionPane.showInputDialog(null, "速度");
				r[car.getSelectedIndex()].setSpeed(Integer.parseInt(s));
			}
			if(e.getSource()==style){		//設定車的形狀
				r[car.getSelectedIndex()].setStyle(style.getSelectedIndex());
			}
	}
	
	
}

