package a11.s995002026;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Racecar extends JPanel{
	private int flag=0;		//到最右邊
	private int color;		//判斷顏色
	private int style;		//判斷造型
	private int time;		//改變速度
	private int i=0;		//移動
	Timer timer;			//計時器
	public Racecar(){
	}
	
	private class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){		//判斷有沒有到最右邊
			if(flag==1)
				i=i-1;
			else
				i=i+1;
			repaint();
		}
	}
	
	
	public void setColor(int input){		//設定顏色
		color=input;
	}
	
	public void setSpeed(int s){			//設定速度
		time=s;
		timer=new Timer(s,new TimerListener());
		timer.start();
	}
	
	public void setStyle(int input){		//選擇車型
		style=input;
	}
	
	public void paintComponent(Graphics g){		//三種車型
		super.paintComponent(g);
		if(color==0)
			g.setColor(Color.green);
		if(color==1)
			g.setColor(Color.red);
		if(color==2)
			g.setColor(Color.blue);
		
		if(style==0){
			int[] x={0+i,50+i,40+i,10+i};
			int[] y={getHeight()/2-40,getHeight()/2-40,getHeight()/2-20,getHeight()/2-20};
			g.fillPolygon(x,y,4);
			g.fillOval(5+i, getHeight()/2-20, 20, 20);
			g.fillOval(25+i, getHeight()/2-20, 20, 20);
			if(i+50>=getWidth())
				flag=1;
			if(i==0)
				flag=0;
			repaint();
		}
		
		if(style==1){
			int[] x={i+25,i+50,i+25};
			int[] y={getHeight()/2-70,getHeight()/2-55,getHeight()/2-40};
			g.fillRect(i, getHeight()/2-10, 50, 10);
			g.fillRect(i+20, getHeight()/2-70, 5, 60);
			g.fillPolygon(x,y,3);
			if(i+50>=getWidth())
				flag=1;
			if(i==0)
				flag=0;
			repaint();
		}
		
		if(style==2){
			int[] x={40+i,40+i,120+i};
			int[] y={getHeight()/2-80,getHeight()/2,getHeight()/2-40};
			g.fillPolygon(x,y,3);
			int[] x2={40+i,40+i,10+i,20+i,0+i,20+i,10+i};
			int[] y2={getHeight()/2-60,getHeight()/2-20,getHeight()/2-10,getHeight()/2-25,getHeight()/2-40,getHeight()/2-55,getHeight()/2-70};
			g.fillPolygon(x2,y2,7);
			if(i+120>=getWidth())
				flag=1;
			if(i==0)
				flag=0;
			repaint();
		}
	}
}
