package a11.s100502028;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class RaceCar extends JPanel {

	private int x = 0, y = 0, s = 0;
	private int dx = 5, dy = 5, radius = 10;
	private Timer timer = new Timer(1000, new TimerListener());
	private Color color = new Color(0, 0, 0);
	private int style;
	
	// Constructor
	public RaceCar() {
		timer.start(); // Start the timer
	}
	
	// A set method to set the speed of this car 
	public void setSpeed(int speed) {
		timer.setDelay(speed);
	}
	
	// A set method to set the color of this car
	public void setColor(Color c) {
		color = c;
	}
	
	// A set method to set the style of this car
	public void setStyle(int s) {
		style = s;
	}
	
	// Draw the car with different style
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		switch(style) {
			case 0: // case for car
				y = getHeight();
				int x1[] = { x + 10, x + 20, x + 30, x + 40 };
				int y1[] = { y - 20, y - 30, y - 30, y - 20 };
				int x2[] = { x, x + 50, x + 50, x };
				int y2[] = { y - 20, y - 20, y - 10, y - 10 };

				g.setColor(color);
				g.fillPolygon(x1, y1, 4);
				g.fillPolygon(x2, y2, 4);
				g.setColor(Color.BLACK);
				g.fillOval(x + 10, y - 10, 10, 10);
				g.fillOval(x + 30, y - 10, 10, 10);
				
				if (x > getWidth() - 40)
					s = 1;
				if (x == 0)
					s = -1;
				if (s == 1)
					x -= 10;
				if (s == -1)
					x += 10;
				break;
			case 1: // case for ball
				if(x < radius) dx = Math.abs(dx);
				if(x > getWidth() - radius) dx = -Math.abs(dx);
				if(y < radius + 10) dy = Math.abs(dy);
				if(y > getHeight() - radius) dy = -Math.abs(dy);
				g.setColor(color);
				g.fillOval(x - radius, y - radius * 2, radius * 2, radius * 2);
				x += dx;
				y += dy;
				break;
			case 2: // case for UFO
				y = getHeight() / 2;
				
				int x21[] = {x, x + 89, x + 89, x};
				int y21[] = {y - 20, y - 20, y - 10, y - 10};
				
				g.setColor(Color.BLACK);
				g.fillPolygon(x21, y21, 4);
				g.fillArc(x + 3 * radius, y - 2 * radius + 5, radius * 3, radius , 180, 180);
				g.setColor(color);
				g.fillArc(x + 2 * radius, y - radius * 4, radius * 5, radius * 4, 0, 180);
				g.fillArc(x + radius, y - 2 * radius + 5, radius , radius , 180, 180);
				g.fillArc(x + 7 * radius, y - 2 * radius + 5, radius , radius , 180, 180);
				if(x > getWidth()) {
					x = -30;
				}
				x += 10;
				break;
			default:
				break;
		}
	}
	
	// Listener class
	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) { // Handle the action event
			repaint(); // Repaint the picture
		}
	}
}
