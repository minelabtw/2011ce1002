package a11.s100502028;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;

public class A111 extends JApplet implements ActionListener{
	
	// Create three objects of RaceCar 
	private RaceCar car1 = new RaceCar();
	private RaceCar car2 = new RaceCar();
	private RaceCar car3 = new RaceCar();
	
	private JTextField jtfSpeedInputForCar1 = new JTextField(10);
	private JTextField jtfSpeedInputForCar2 = new JTextField(10);
	private JTextField jtfSpeedInputForCar3 = new JTextField(10);
	
	private JRadioButton jrbRed1 = new JRadioButton("Red");
	private JRadioButton jrbGreen1 = new JRadioButton("Green");
	private JRadioButton jrbBlue1 = new JRadioButton("Blue");
	private JRadioButton jrbRed2 = new JRadioButton("Red");
	private JRadioButton jrbGreen2 = new JRadioButton("Green");
	private JRadioButton jrbBlue2 = new JRadioButton("Blue");
	private JRadioButton jrbRed3 = new JRadioButton("Red");
	private JRadioButton jrbGreen3 = new JRadioButton("Green");
	private JRadioButton jrbBlue3 = new JRadioButton("Blue");
	
	// Declare an array of Strings for car styles
	private String[] style = {"car", "ball", "UFO"};
	private JComboBox jcbChangeStyleForCar1 = new JComboBox(style);
	private JComboBox jcbChangeStyleForCar2 = new JComboBox(style);
	private JComboBox jcbChangeStyleForCar3 = new JComboBox(style);
	
	private Border lineBorder1 = new LineBorder(Color.BLACK, 3);
	
	private Color colorRed = new Color(255, 0, 0);
	private Color colorGreen = new Color(0, 255, 0);
	private Color colorBlue = new Color(0, 0, 255);
	
	public A111() {
		
		// Panel for the speed of car1
		JPanel jpTextFieldForCar1 = new JPanel();
		jpTextFieldForCar1.setLayout(new BorderLayout(5, 0));
		jpTextFieldForCar1.add(new JLabel("Car1 Speed"), BorderLayout.WEST);
		jpTextFieldForCar1.add(jtfSpeedInputForCar1, BorderLayout.CENTER);
		
		// Panel for the speed of car2
		JPanel jpTextFieldForCar2 = new JPanel();
		jpTextFieldForCar2.setLayout(new BorderLayout(5, 0));
		jpTextFieldForCar2.add(new JLabel("Car2 Speed"), BorderLayout.WEST);
		jpTextFieldForCar2.add(jtfSpeedInputForCar2, BorderLayout.CENTER);
		
		// Panel for the speed of car3
		JPanel jpTextFieldForCar3 = new JPanel();
		jpTextFieldForCar3.setLayout(new BorderLayout(5, 0));
		jpTextFieldForCar3.add(new JLabel("Car3 Speed"), BorderLayout.WEST);
		jpTextFieldForCar3.add(jtfSpeedInputForCar3, BorderLayout.CENTER);
		
		JPanel p1 = new JPanel(new GridLayout(1, 3, 10, 10));
		p1.add(jpTextFieldForCar1);
		p1.add(jpTextFieldForCar2);
		p1.add(jpTextFieldForCar3);
		
		// Panel for the radio buttons of car1
		JPanel jpRadioButtonsForCar1 = new JPanel();
		jpRadioButtonsForCar1.setLayout(new GridLayout(3, 1));
		jrbRed1.setForeground(colorRed);
		jrbGreen1.setForeground(colorGreen);
		jrbBlue1.setForeground(colorBlue);
		jpRadioButtonsForCar1.add(jrbRed1);
		jpRadioButtonsForCar1.add(jrbGreen1);
		jpRadioButtonsForCar1.add(jrbBlue1);
		ButtonGroup group1 = new ButtonGroup();
		group1.add(jrbRed1);
		group1.add(jrbGreen1);
		group1.add(jrbBlue1);
		
		// Panel for the radio buttons of car2
		JPanel jpRadioButtonsForCar2 = new JPanel();
		jpRadioButtonsForCar2.setLayout(new GridLayout(3, 1));
		jrbRed2.setForeground(colorRed);
		jrbGreen2.setForeground(colorGreen);
		jrbBlue2.setForeground(colorBlue);
		jpRadioButtonsForCar2.add(jrbRed2);
		jpRadioButtonsForCar2.add(jrbGreen2);
		jpRadioButtonsForCar2.add(jrbBlue2);
		ButtonGroup group2 = new ButtonGroup();
		group2.add(jrbRed2);
		group2.add(jrbGreen2);
		group2.add(jrbBlue2);
		
		// Panel for the radio buttons of car3
		JPanel jpRadioButtonsForCar3 = new JPanel();
		jpRadioButtonsForCar3.setLayout(new GridLayout(3, 1));
		jrbRed3.setForeground(colorRed);
		jrbGreen3.setForeground(colorGreen);
		jrbBlue3.setForeground(colorBlue);
		jpRadioButtonsForCar3.add(jrbRed3);
		jpRadioButtonsForCar3.add(jrbGreen3);
		jpRadioButtonsForCar3.add(jrbBlue3);
		ButtonGroup group3 = new ButtonGroup();
		group3.add(jrbRed3);
		group3.add(jrbGreen3);
		group3.add(jrbBlue3);
		
		// Panel for radio buttons panel, car1 panel, and combo box for car1
		JPanel p2 = new JPanel(new BorderLayout(5, 5));
		p2.add(jpRadioButtonsForCar1, BorderLayout.WEST);
		car1.setBorder(lineBorder1);
		jpRadioButtonsForCar1.setBorder(lineBorder1);
		p2.add(car1, BorderLayout.CENTER);
		p2.add(jcbChangeStyleForCar1, BorderLayout.EAST);
		
		// Panel for radio buttons panel, car2 panel, and combo box for car2
		JPanel p3 = new JPanel(new BorderLayout(5, 5));
		p3.add(jpRadioButtonsForCar2, BorderLayout.WEST);
		car2.setBorder(lineBorder1);
		jpRadioButtonsForCar2.setBorder(lineBorder1);
		p3.add(car2, BorderLayout.CENTER);
		p3.add(jcbChangeStyleForCar2, BorderLayout.EAST);
		
		// Panel for radio buttons panel, car3 panel, and combo box for car3
		JPanel p4 = new JPanel(new BorderLayout(5, 5));
		p4.add(jpRadioButtonsForCar3, BorderLayout.WEST);
		car3.setBorder(lineBorder1);
		jpRadioButtonsForCar3.setBorder(lineBorder1);
		p4.add(car3, BorderLayout.CENTER);
		p4.add(jcbChangeStyleForCar3, BorderLayout.EAST);
		
		JPanel p5 = new JPanel(new GridLayout(3, 1, 5, 5));
		p5.add(p2);
		p5.add(p3);
		p5.add(p4);
		
		// Add all panels to the frame
		setLayout(new BorderLayout(5, 5));
		add(p1, BorderLayout.NORTH);
		add(p5, BorderLayout.CENTER);
		
		// Active button event here
		jrbRed1.addActionListener(this);
		jrbGreen1.addActionListener(this);
		jrbBlue1.addActionListener(this);
		jrbRed2.addActionListener(this);
		jrbGreen2.addActionListener(this);
		jrbBlue2.addActionListener(this);
		jrbRed3.addActionListener(this);
		jrbGreen3.addActionListener(this);
		jrbBlue3.addActionListener(this);
		jtfSpeedInputForCar1.addActionListener(this);
		jtfSpeedInputForCar2.addActionListener(this);
		jtfSpeedInputForCar3.addActionListener(this);
		
		// Create and register anonymous inner-class listener for combo box
		jcbChangeStyleForCar1.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				car1.setStyle(jcbChangeStyleForCar1.getSelectedIndex());
			}
		});
		jcbChangeStyleForCar2.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				car2.setStyle(jcbChangeStyleForCar2.getSelectedIndex());
			}
		});
		jcbChangeStyleForCar3.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				car3.setStyle(jcbChangeStyleForCar3.getSelectedIndex());
			}
		});
	}
	
	// Method to let the buttons work
	public void actionPerformed(ActionEvent e) {
		// If statement for radio buttons of car1
		if(e.getSource() == jrbRed1) {
			car1.setColor(colorRed);
		}
		else if(e.getSource() == jrbGreen1) {
			car1.setColor(colorGreen);
		}
		else if(e.getSource() == jrbBlue1) {
			car1.setColor(colorBlue);
		}
		
		// If statement for radio buttons of car2
		if(e.getSource() == jrbRed2) {
			car2.setColor(colorRed);
		}
		else if(e.getSource() == jrbGreen2) {
			car2.setColor(colorGreen);
		}
		else if(e.getSource() == jrbBlue2) {
			car2.setColor(colorBlue);
		}
		
		// If statement for radio buttons of car3
		if(e.getSource() == jrbRed3) {
			car3.setColor(colorRed);
		}
		else if(e.getSource() == jrbGreen3) {
			car3.setColor(colorGreen);
		}
		else if(e.getSource() == jrbBlue3) {
			car3.setColor(colorBlue);
		}
		
		// If statement for text field of car1
		if(e.getSource() == jtfSpeedInputForCar1) {
			car1.setSpeed(Integer.parseInt(jtfSpeedInputForCar1.getText()));
		}
		
		// If statement for text field of car2
		if(e.getSource() == jtfSpeedInputForCar2) {
			car2.setSpeed(Integer.parseInt(jtfSpeedInputForCar2.getText()));
		}
		
		// If statement for text field of car3
		if(e.getSource() == jtfSpeedInputForCar3) {
			car3.setSpeed(Integer.parseInt(jtfSpeedInputForCar3.getText()));
		}
	}
}
