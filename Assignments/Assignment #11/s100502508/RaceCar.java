package a11.s100502508;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.Timer;

public class RaceCar extends JPanel
{
	private int x=0;
	private int y;
	private int velocity=5;
	private int Delay=100;
	private Color color;
	
	private Timer timer=new Timer(Delay,new TimerListener());//create a timer
	
	public RaceCar(Color OriginColor) 
	{
		setColor(OriginColor);
		timer.start();//start timer
	}
	
	protected void paintComponent(Graphics g)//override paintComponent
	{
		super.paintComponent(g);//draw things in the superclass
		
		y=getHeight()/2;
		
		int X1[]={x+20,x+30,x+40,x+10};
		int Y1[]={y-30,y-30,y-20,y-20};
		int X2[]={x,x+50,x+50,x};
		int Y2[]={y-20,y-20,y-10,y-10};
		
		g.setColor(Color.red);//set color
		g.fillPolygon(X1,Y1,X1.length);
		g.setColor(color);
		g.fillPolygon(X2,Y2,X2.length);
		g.setColor(Color.green);
		g.fillOval(x+10, y-10, 10, 10);//fill a circle
		g.setColor(Color.yellow);
		g.fillOval(x+30, y-10, 10, 10);
	}
	class TimerListener implements ActionListener//Handle ActionEvent
	{
		public void actionPerformed(ActionEvent e)//event handler
		{
			if(velocity>0)
			{
				if(x+50>getWidth())//picture is beyond the right wall
				{
					x=getWidth()-50;
					velocity=velocity*-1;
				}
				else
				{
					x=x+velocity;//move the picture
				}
			}
			else
			{
				if(x<0)//picture is beyond the left wall
				{
					x=0;
					velocity=velocity*-1;
				}
				else
				{
					x=x+velocity;
				}
			}
			
			repaint();//repaint panel
		}
	}
	
	public int getdelay()//get the value of delay
	{
		return Delay;
	}
	
	public void setdelay(int delay)//set the timer's delay
	{
		Delay=delay;
		timer.setDelay(Delay);
	}
	
	public void setColor(Color NewColor)//set car's color
	{
		color=NewColor;
	}
}
