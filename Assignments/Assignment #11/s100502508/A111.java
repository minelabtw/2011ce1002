package a11.s100502508;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

import javax.swing.*;

public class A111 extends JApplet
{
	private JRadioButton jrb1=new JRadioButton("Blue",true);//declare radio buttons 
	private JRadioButton jrb2=new JRadioButton("Black",false);
	private JRadioButton jrb3=new JRadioButton("Orange",false);
	private JRadioButton jrb4=new JRadioButton("Cyan",true);
	private JRadioButton jrb5=new JRadioButton("Dark Gray",false);
	private JRadioButton jrb6=new JRadioButton("Magenta",false);
	private JRadioButton jrb7=new JRadioButton("Pink",true);
	private JRadioButton jrb8=new JRadioButton("Light Gray",false);
	private JRadioButton jrb9=new JRadioButton("White",false);
	
	RaceCar raceCar1=new RaceCar(Color.blue);//create RaceCar's object and initial the color
	RaceCar raceCar2=new RaceCar(Color.cyan);
	RaceCar raceCar3=new RaceCar(Color.PINK);
	
	JPanel panel1=new JPanel(new GridLayout(1,2,0,0));//create panels
	JPanel panel2=new JPanel();
	JPanel panel3=new JPanel(new GridLayout(3,1,0,0));
	JPanel panel4=new JPanel(new GridLayout(1,2,0,0));
	JPanel panel5=new JPanel();
	JPanel panel6=new JPanel(new GridLayout(3,1,0,0));
	JPanel panel7=new JPanel(new GridLayout(1,2,0,0));
	JPanel panel8=new JPanel();
	JPanel panel9=new JPanel(new GridLayout(3,1,0,0));
	JPanel panel10=new JPanel(new GridLayout(3,1,0,0));
	
	JLabel label1=new JLabel("Change speed :",JLabel.CENTER);//create labels and some strings on it and center them
	JLabel label2=new JLabel("Change color :");//create labels and some strings on it
	JLabel label3=new JLabel("Change speed :",JLabel.CENTER);
	JLabel label4=new JLabel("Change color :");
	JLabel label5=new JLabel("Change speed :",JLabel.CENTER);
	JLabel label6=new JLabel("Change color :");
	
	ButtonGroup group1=new ButtonGroup();//create a radio-button group
	ButtonGroup group2=new ButtonGroup();
	ButtonGroup group3=new ButtonGroup();
	
	JScrollBar jsbDelay1=new JScrollBar();//create scroll bars
	JScrollBar jsbDelay2=new JScrollBar();
	JScrollBar jsbDelay3=new JScrollBar();
	
	
	public A111() 
	{
		jsbDelay1.setOrientation(JScrollBar.HORIZONTAL);//create horizontal scroll bars
		jsbDelay2.setOrientation(JScrollBar.HORIZONTAL);
		jsbDelay3.setOrientation(JScrollBar.HORIZONTAL);
		
		panel1.add(label1);//add label to panel
		panel1.add(jsbDelay1);//add scroll bar to panel
		
		panel2.add(label2);//add label to panel
		panel2.add(jrb1);//add radio buttons to panel
		panel2.add(jrb2);
		panel2.add(jrb3);
		
		group1.add(jrb1);//group three buttons
		group1.add(jrb2);
		group1.add(jrb3);
		
		panel3.add(panel1);
		panel3.add(raceCar1);
		panel3.add(panel2);
		
		panel4.add(label3);//add label to panel
		panel4.add(jsbDelay2);//add scroll bar to panel
		
		panel5.add(label4);//add label to panel
		panel5.add(jrb4);//add radio buttons to panel
		panel5.add(jrb5);
		panel5.add(jrb6);
		
		group2.add(jrb4);//group three buttons
		group2.add(jrb5);
		group2.add(jrb6);
		
		panel6.add(panel4);
		panel6.add(raceCar2);
		panel6.add(panel5);
		
		panel7.add(label5);//add label to panel
		panel7.add(jsbDelay3);//add scroll bar to panel
		
		panel8.add(label6);//add label to panel
		panel8.add(jrb7);//add radio buttons to panel
		panel8.add(jrb8);
		panel8.add(jrb9);
		
		group3.add(jrb7);//group three buttons
		group3.add(jrb8);
		group3.add(jrb9);
		
		panel9.add(panel7);
		panel9.add(raceCar3);
		panel9.add(panel8);
		
		panel10.add(panel3);
		panel10.add(panel6);
		panel10.add(panel9);
		
		add(panel10);
		
		jsbDelay1.addAdjustmentListener(new AdjustmentListener()//register listener for the scroll bars 
		{
			public void adjustmentValueChanged(AdjustmentEvent e) 
			{
				raceCar1.setdelay(jsbDelay1.getMaximum()-e.getValue());
			}
		});
		
		jsbDelay2.addAdjustmentListener(new AdjustmentListener() 
		{
			public void adjustmentValueChanged(AdjustmentEvent e) 
			{
				raceCar2.setdelay(jsbDelay2.getMaximum()-e.getValue());
			}
		});
		
		jsbDelay3.addAdjustmentListener(new AdjustmentListener() 
		{
			public void adjustmentValueChanged(AdjustmentEvent e) 
			{
				raceCar3.setdelay(jsbDelay3.getMaximum()-e.getValue());
			}
		});
		
		jrb1.addActionListener(new ActionListener()//register listeners for radio buttons 
		{
			public void actionPerformed(ActionEvent e) 
			{
				raceCar1.setColor(Color.blue);
			}
		});
		
		jrb2.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				raceCar1.setColor(Color.black);
			}
		});
		
		jrb3.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				raceCar1.setColor(Color.orange);
			}
		});
		jrb4.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				raceCar2.setColor(Color.cyan);
			}
		});
		jrb5.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				raceCar2.setColor(Color.DARK_GRAY);
			}
		});
		jrb6.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				raceCar2.setColor(Color.magenta);
			}
		});
		jrb7.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				raceCar3.setColor(Color.PINK);
			}
		});
		jrb8.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				raceCar3.setColor(Color.LIGHT_GRAY);
			}
		});
		jrb9.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				raceCar3.setColor(Color.WHITE);
			}
		});
	}
	
	public static void main(String[] args)
	{
		JFrame frame=new JFrame();//create a frame
		A111 applet=new A111();//create an instance of the applet
		frame.add(applet);//add the applet to the frame
		
		frame.setSize(500, 700);//set the frame size
		frame.setLocationRelativeTo(null);//center a frame
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);//display the frame
	}
}