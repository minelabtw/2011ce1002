package a11.s100502001;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class A111 extends JApplet{
	RaceCar car1=new RaceCar();
	RaceCar car2=new RaceCar();
	RaceCar car3=new RaceCar();
	private JComboBox jcboSpeedcar1; //選擇車子數度
	private JComboBox jcboSpeedcar2;
	private JComboBox jcboSpeedcar3;
	private JRadioButton[] jrb1={new JRadioButton("Red"),new JRadioButton("Blue"),new JRadioButton("Green")} ;
	private JRadioButton[] jrb2={new JRadioButton("yellow"),new JRadioButton("pink"),new JRadioButton("cyan")} ;
	private JRadioButton[] jrb3={new JRadioButton("magenta"),new JRadioButton("orange"),new JRadioButton("LIGHT_GRAY")};
	private String[] speed1={"停下來吧!!","100","200","300"};
	private String[] speed2={"呼呼!!!好累!!take a rest","400","500","600"};
	private String[] speed3={"呵呵!!喝水多休息","700","800","900"};
	private Color[] color1={Color.red,Color.blue,Color.green};
	private Color[] color2={Color.yellow,Color.pink,Color.cyan};
	private Color[] color3={Color.magenta,Color.orange,Color.LIGHT_GRAY};
	public A111(){
		jcboSpeedcar1=new JComboBox(speed1); //選車子速度
		jcboSpeedcar2=new JComboBox(speed2);
		jcboSpeedcar3=new JComboBox(speed3);
		jcboSpeedcar1.setBackground(new Color((int)(Math.random()*256),(int)(Math.random()*256),(int)(Math.random()*256)));
		jcboSpeedcar2.setBackground(new Color((int)(Math.random()*256),(int)(Math.random()*256),(int)(Math.random()*256)));
		jcboSpeedcar3.setBackground(new Color((int)(Math.random()*256),(int)(Math.random()*256),(int)(Math.random()*256)));
		ButtonGroup group1=new ButtonGroup(); //一次只能選一個顏色
		for(int i=0;i<jrb1.length;i++)
			group1.add(jrb1[i]);
		ButtonGroup group2=new ButtonGroup(); //一次只能選一個顏色
		for(int i=0;i<jrb1.length;i++)
			group2.add(jrb2[i]);
		ButtonGroup group3=new ButtonGroup(); //一次只能選一個顏色
		for(int i=0;i<jrb1.length;i++)
			group3.add(jrb3[i]);
		
		JPanel jp1=new JPanel();
		jp1.setLayout(new BorderLayout());
		
		jp1.setLayout(new GridLayout(4,1));
		jp1.add(jcboSpeedcar1,BorderLayout.EAST);
		for(int i=0;i<jrb1.length;i++){
			jrb1[i].setForeground(new Color((int)(Math.random()*256),(int)(Math.random()*256),(int)(Math.random()*256)));
			jp1.add(jrb1[i]);
		}
		JPanel jp2=new JPanel();
		jp2.setLayout(new BorderLayout());
		
		jp2.setLayout(new GridLayout(4,1));
		jp2.add(jcboSpeedcar2,BorderLayout.EAST);
		for(int i=0;i<jrb1.length;i++){
			jrb2[i].setForeground(new Color((int)(Math.random()*256),(int)(Math.random()*256),(int)(Math.random()*256)));
			jp2.add(jrb2[i]);
		}

		JPanel jp3=new JPanel();
		jp3.setLayout(new BorderLayout());
		
		jp3.setLayout(new GridLayout(4,1));
		jp3.add(jcboSpeedcar3,BorderLayout.EAST);
		for(int i=0;i<jrb1.length;i++){
			jrb3[i].setForeground(new Color((int)(Math.random()*256),(int)(Math.random()*256),(int)(Math.random()*256)));
			jp3.add(jrb3[i]);
		}
		setLayout(new BorderLayout());
		setLayout(new GridLayout(3,2));
		add(jp1,BorderLayout.WEST);
		add(car1);
		add(jp2,BorderLayout.WEST);
		add(car2);
		add(jp3,BorderLayout.WEST);
		add(car3);
		jcboSpeedcar1.addItemListener(new ItemListener() { //選擇car1速度
			public void itemStateChanged(ItemEvent e) {
				int index=jcboSpeedcar1.getSelectedIndex();
				if(index==0)
					car1.stop();
				else
					car1.setSpeed(Integer.parseInt(speed1[index]));
			}
		});
		jcboSpeedcar2.addItemListener(new ItemListener() {//選擇car2速度
			public void itemStateChanged(ItemEvent e) {
				int index=jcboSpeedcar2.getSelectedIndex();
				if(index==0)
					car2.stop();
				else
					car2.setSpeed(Integer.parseInt(speed2[index]));
			}
		});
		jcboSpeedcar3.addItemListener(new ItemListener() { //選擇car3速度
			public void itemStateChanged(ItemEvent e) {
				int index=jcboSpeedcar3.getSelectedIndex();
				if(index==0)
					car3.stop();
				else 
					car3.setSpeed(Integer.parseInt(speed3[index]));
			}
		});
		
		jrb1[0].addActionListener(new ActionListener() { //選擇car1顏色
			public void actionPerformed(ActionEvent e){
				car1.setColor(color1[0]);
			}
		});
		jrb1[1].addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e){
				
				car1.setColor(color1[1]);
			}
		});
		jrb1[2].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				car1.setColor(color1[2]);
			}
		});
		
		jrb2[0].addActionListener(new ActionListener() { //選擇car2顏色
			public void actionPerformed(ActionEvent e){
				car2.setColor(color2[0]);
			}
		});
		jrb2[1].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				car2.setColor(color2[1]);
			}
		});
		jrb2[2].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				car2.setColor(color2[2]);
			}
		});
		
			
		jrb3[0].addActionListener(new ActionListener() { //選擇car3顏色
			public void actionPerformed(ActionEvent e) {
				car3.setColor(color3[0]);
			}
		});
		jrb3[1].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car3.setColor(color3[1]);
			}
		});
		jrb3[2].addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car3.setColor(color3[2]);
			}
		});
		
		
	}
	
	/*public static void main(String[] args){
		A111 f=new A111();
		f.setSize(600, 600);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}*/
	
	
	
}
