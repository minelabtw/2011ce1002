package a11.s100502001;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
public class RaceCar extends JPanel{
	private Color color;
	private int Xcoordinate=0;
	private int Ycoordinate=0;
	private boolean forward=true;
	private Timer timer;
	public RaceCar(){
		timer=new Timer(0,new Speed());
	}
	public void setSpeed(int s){
		timer.setDelay(s);
		timer.restart();
	}
	public void setColor(Color c){
		color=c;
		repaint();
	}
	public void setStyle(){
		
	}
	public void stop(){
		timer.stop();
	}
	protected void paintComponent(Graphics g){ //設計車子的移動
		super.paintComponent(g);
		Ycoordinate=(int)(getHeight()*0.8);
		
		if(!forward){ //若car不繼續往前(向右)
			if(Xcoordinate<0){ //若到了最左邊
				Xcoordinate=0; //則準備向右移動
				forward=true;
			}
			Xcoordinate-=5;//則往回走
		}
		else { //往右移動
			Xcoordinate+=5;
			if(Xcoordinate>getWidth()-50){ //若到了最左邊
				Xcoordinate=getWidth()-50; 
				forward=false;
			}
		}
		//設計車子
		int[] X={Xcoordinate+10,Xcoordinate+20,Xcoordinate+30,Xcoordinate+40};
		int[] Y={Ycoordinate+10,Ycoordinate,Ycoordinate,Ycoordinate+10};
		g.setColor(color);
		g.fillPolygon(X,Y,X.length);
		g.fillRect(Xcoordinate,Ycoordinate+10 ,50, 10);
		g.fillOval(Xcoordinate+10,Ycoordinate+20 , 10, 10);
		g.fillOval(Xcoordinate+30, Ycoordinate+20, 10, 10);
		
	}
	class Speed implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			repaint();
		}
	}
	

}
