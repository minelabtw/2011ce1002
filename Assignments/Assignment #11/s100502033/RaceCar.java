package a11.s100502033;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;
public class RaceCar extends JPanel
{
	private int x = 0;//初始的X座標啦
	private Color r;
	private String string1 = "speed:slow";
	private String string2 = "speed:normal";
	private String string3 = "speed:fast";
	private String string4 = "speed:fastest";
	private int speed1 ;
	private int color1 ;
	private int style1 ;
	private int speed2 ;
	private int color2 ;
	private int style2 ;
	private int speed3 ;
	private int color3 ;
	private int style3 ;
	private Timer timer;
	public RaceCar(){
		timer = new Timer(50, new TimerListener());//速度預設為50
		timer.start();
	}
	public void speed(int number)
	{
		speed1 = number;
	}
	public void color(int number2)
	{
		if(number2 == 0)
		{
			Color c = new Color(255 , 0 , 0);//紅色
			r = c;
		}
		else if(number2 == 1)
		{
			Color c = new Color(255 , 0 , 255);//紫色
			r = c;
		}
		else if(number2 == 2)
		{
			Color c = new Color(0 , 0 , 255);//藍色
			r = c;
		}
	}
	public void style(int number1)
	{
		style1 = number1;
	}

	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
	    if(speed1 == 0)
	    {
	    	g.drawString(string1, 0 ,20);
	    	timer.setDelay(50);//變更速度
	    }
	    else if(speed1 == 1)
	    {
	    	g.drawString(string2, 0 ,20);
	    	timer.setDelay(25);//變更速度
	    }
	    else if(speed1 == 2)
	    {
	    	g.drawString(string3, 0 ,20);
	    	timer.setDelay(12);//變更速度
	    }
	    else if(speed1 == 3)
	    {
	    	g.drawString(string4, 0 ,20);
	    	timer.setDelay(1);//變更速度
	    }
	    g.setColor(r);
	    if(style1 == 0)//變更風格
	    {
		    g.fillRoundRect(x,50,100,50,0,0);
		    g.fillRoundRect(x+100, 50, 10, 50, 0, 0);
	    }
	    else if(style1 == 1)//變更風格
	    {
		    g.fillRoundRect(x,50,100,50,10000,10000);
		    g.fillRoundRect(x+100, 50, 10, 50, 0, 0);
	    }
	    else if(style1 == 2)//變更風格
	    {
		    g.fillRoundRect(x,50,100,50,0,0);
		    g.fillRoundRect(x+100, 50, 10, 50, 10000, 10000);
	    }
	    else if(style1 == 3)//變更風格
	    {
		    g.fillRoundRect(x,50,100,50,10000,10000);
		    g.fillRoundRect(x+100, 50, 10, 50, 10000, 10000);
	    }
	    g.fillOval(x+20,90,20,20);//繪畫車輪
        g.fillOval(x+60,90,20,20);//繪畫車輪
        x++;
        if(x > getWidth())//到達盡頭時由起點再來
        {
        	x = 0;
        }
	}
	class TimerListener implements ActionListener 
	{
		public void actionPerformed(ActionEvent e) 
	    {
	      repaint();
	    }
	}
}
