package a11.s100502033;
import java.applet.*;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.*;
import javax.swing.*;
public class A111 extends JApplet
{
	private int number = 0 ;
	private int number2 = 0 ;
	private JButton sp = new JButton("speed1");
	private JButton sl = new JButton("style1");
	private JButton sp1 = new JButton("speed2");
	private JButton sl1 = new JButton("style2");
	private JButton sp2 = new JButton("speed3");
	private JButton sl2 = new JButton("style3");
	private JPanel p1 = new JPanel();
	private JPanel p2 = new JPanel();
	private JPanel p3 = new JPanel();
	private JPanel p4 = new JPanel();
	private RaceCar car = new RaceCar();
	private RaceCar car1 = new RaceCar();
	private RaceCar car2 = new RaceCar();
	private JRadioButton Color1 = new JRadioButton("red1");
	private JRadioButton Color2 = new JRadioButton("Purple1");
	private JRadioButton Color3 = new JRadioButton("blue1");
	private JRadioButton Color4 = new JRadioButton("red2");
	private JRadioButton Color5 = new JRadioButton("Purple2");
	private JRadioButton Color6 = new JRadioButton("blue2");
	private JRadioButton Color7 = new JRadioButton("red3");
	private JRadioButton Color8 = new JRadioButton("Purple3");
	private JRadioButton Color9 = new JRadioButton("blue3");
	public A111()
	{
		p1.setLayout(new GridLayout(1,5));
		p2.setLayout(new GridLayout(1,5));
		p3.setLayout(new GridLayout(1,5));
		p4.setLayout(new GridLayout(3,1));
		p1.add(Color1 , BorderLayout.EAST);
		p1.add(Color2 , BorderLayout.WEST);
		p1.add(Color3 , BorderLayout.CENTER);
		p1.add(sp , BorderLayout.NORTH);
		p1.add(sl , BorderLayout.SOUTH);
		p2.add(Color4 , BorderLayout.EAST);
		p2.add(Color5 , BorderLayout.WEST);
		p2.add(Color6 , BorderLayout.CENTER);
		p2.add(sp1 , BorderLayout.NORTH);
		p2.add(sl1 , BorderLayout.SOUTH);
		p3.add(Color7 , BorderLayout.EAST);
		p3.add(Color8 , BorderLayout.WEST);
		p3.add(Color9 , BorderLayout.CENTER);
		p3.add(sp2 , BorderLayout.NORTH);
		p3.add(sl2 , BorderLayout.SOUTH);
		p4.add(p1 , BorderLayout.NORTH);
		p4.add(p2 , BorderLayout.CENTER);
		p4.add(p3 , BorderLayout.SOUTH);
		add(p4 , BorderLayout.SOUTH);
		Framework framework = new Framework();
		sp.addActionListener(framework);
		Color1.addActionListener(framework);
		Color2.addActionListener(framework);
		Color3.addActionListener(framework);
		sl.addActionListener(framework);
		sp1.addActionListener(framework);
		Color4.addActionListener(framework);
		Color5.addActionListener(framework);
		Color6.addActionListener(framework);
		sl1.addActionListener(framework);
		sp2.addActionListener(framework);
		Color7.addActionListener(framework);
		Color8.addActionListener(framework);
		Color9.addActionListener(framework);
		sl2.addActionListener(framework);
		setLayout(new GridLayout(4,1));//分開四個界面
		add(car);
		add(car1);
		add(car2);
	}
	class Framework implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			if(event.getSource() == sp)
			{	
				if(number == 3)//變更速度
				{
					number = -1 ;
				}
				number++;
				car.speed(number);
				add(car);
				repaint();
			}
			else if(event.getSource() == Color1)
			{
				car.color(0);//變更頻色
				add(car);
				repaint();
				Color2.setSelected(false);//只能選擇一個功能
				Color3.setSelected(false);//只能選擇一個功能
			}
			else if(event.getSource() == Color2)
			{
				car.color(1);//變更頻色
				add(car);
				repaint();
				Color1.setSelected(false);//只能選擇一個功能
				Color3.setSelected(false);//只能選擇一個功能
			}
			else if(event.getSource() == Color3)
			{
				car.color(2);//變更頻色
				add(car);
				repaint();
			Color1.setSelected(false);//只能選擇一個功能
			Color2.setSelected(false);//只能選擇一個功能
			}
			else if(event.getSource() == sl)
			{
				if(number2 == 3)//變更風格
				{
					number2 = -1 ;
				}
				number2++;
				car.style(number2);
				add(car);
				repaint();
			}
			else if(event.getSource() == sp1)
			{	
				if(number == 3)//變更速度
				{
					number = -1 ;
				}
				number++;
				car1.speed(number);
				add(car1);
				repaint();
			}
			else if(event.getSource() == Color4)
			{
				car1.color(0);//變更頻色
				add(car1);
				repaint();
				Color5.setSelected(false);//只能選擇一個功能
				Color6.setSelected(false);//只能選擇一個功能
			}
			else if(event.getSource() == Color5)
			{
				car1.color(1);//變更頻色
				add(car1);
				repaint();
				Color4.setSelected(false);//只能選擇一個功能
				Color6.setSelected(false);//只能選擇一個功能
			}
			else if(event.getSource() == Color6)
			{
				car1.color(2);//變更頻色
				add(car1);
				repaint();
				Color4.setSelected(false);//只能選擇一個功能
				Color5.setSelected(false);//只能選擇一個功能
			}
			else if(event.getSource() == sl1)
			{
				if(number2 == 3)//變更風格
				{
					number2 = -1 ;
				}
				number2++;
				car1.style(number2);
				add(car1);
				repaint();
			}
			else if(event.getSource() == sp2)
			{	
				if(number == 3)//變更速度
				{
					number = -1 ;
				}
				number++;
				car2.speed(number);
				add(car2);
				repaint();
			}
			else if(event.getSource() == Color7)
			{
				car2.color(0);//變更頻色
				add(car2);
				repaint();
				Color8.setSelected(false);//只能選擇一個功能
				Color9.setSelected(false);//只能選擇一個功能
			}
			else if(event.getSource() == Color8)
			{
				car2.color(1);//變更頻色
				add(car2);
				repaint();
				Color7.setSelected(false);//只能選擇一個功能
				Color9.setSelected(false);//只能選擇一個功能
			}
			else if(event.getSource() == Color9)
			{
				car2.color(2);//變更頻色
				add(car2);
				repaint();
				Color7.setSelected(false);//只能選擇一個功能
				Color8.setSelected(false);//只能選擇一個功能
			}
			else if(event.getSource() == sl2)
			{
				if(number2 == 3)//變更風格
				{
					number2 = -1 ;
				}
				number2++;
				car2.style(number2);
				add(car2);
				repaint();
			}
		}
	}
	public static void main(String args[])
	{
		A111 applet = new A111();
		JFrame f = new JFrame();
		f.setTitle("CAR"); 
		f.setSize(1000, 750); //Frame size
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
