package a11.s100502023;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.*;


import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class A111 extends Applet
{	//car
	private RaceCar car1 = new RaceCar();
	private RaceCar car2 = new RaceCar();
	private RaceCar car3 = new RaceCar();
	//combobox item
	private String []  kindOfSpeed = {"fast","medium","slow"};
	//combobox
	private JComboBox control_car1_speed = new JComboBox(kindOfSpeed);
	private JComboBox control_car2_speed = new JComboBox(kindOfSpeed);
	private JComboBox control_car3_speed = new JComboBox(kindOfSpeed);
	
	//radiobutton
	private JRadioButton jrb_car1_red = new JRadioButton("Red");
	private JRadioButton jrb_car1_blue = new JRadioButton("Blue");
	private JRadioButton jrb_car1_black = new JRadioButton("Blcak");
	
	private JRadioButton jrb_car2_red = new JRadioButton("Red");
	private JRadioButton jrb_car2_blue = new JRadioButton("Blue");
	private JRadioButton jrb_car2_black = new JRadioButton("Blcak");
	
	private JRadioButton jrb_car3_red = new JRadioButton("Red");
	private JRadioButton jrb_car3_blue = new JRadioButton("Blue");
	private JRadioButton jrb_car3_black = new JRadioButton("Blcak");
	//buttongroup
	private ButtonGroup group1 = new ButtonGroup();
	private ButtonGroup group2 = new ButtonGroup();
	private ButtonGroup group3 = new ButtonGroup();
	//panel for RadioButton
	private JPanel jp_car1_RadioButton = new JPanel();
	private JPanel jp_car2_RadioButton = new JPanel();
	private JPanel jp_car3_RadioButton = new JPanel();
	//panel for ComboBox and RadioButton
	private JPanel jp_car1_ComboBox_RadioButton = new JPanel();
	private JPanel jp_car2_ComboBox_RadioButton = new JPanel();
	private JPanel jp_car3_ComboBox_RadioButton = new JPanel();
	//panel (all)
	private JPanel jp_car1_all = new JPanel();
	private JPanel jp_car2_all = new JPanel();
	private JPanel jp_car3_all = new JPanel();
	//itemListener
	ComboBox_ItemListener car_itemListener = new ComboBox_ItemListener();
	
	public void init()
	{	
		//car1
		jp_car1_RadioButton.setLayout(new GridLayout(1,3,10,10));
		jp_car1_RadioButton.add(jrb_car1_red);
		jp_car1_RadioButton.add(jrb_car1_blue);
		jp_car1_RadioButton.add(jrb_car1_black);
		
		jp_car1_ComboBox_RadioButton.setLayout(new GridLayout(2,1,10,10));
		jp_car1_ComboBox_RadioButton.add(control_car1_speed);//combobox
		jp_car1_ComboBox_RadioButton.add(jp_car1_RadioButton);
		
		jp_car1_all.setLayout(new GridLayout(1,2,10,10));
		jp_car1_all.add(car1);
		jp_car1_all.add(jp_car1_ComboBox_RadioButton);
		//car2
		jp_car2_RadioButton.setLayout(new GridLayout(1,3,10,10));
		jp_car2_RadioButton.add(jrb_car2_red);
		jp_car2_RadioButton.add(jrb_car2_blue);
		jp_car2_RadioButton.add(jrb_car2_black);
		
		jp_car2_ComboBox_RadioButton.setLayout(new GridLayout(2,1,10,10));
		jp_car2_ComboBox_RadioButton.add(control_car2_speed);//combobox
		jp_car2_ComboBox_RadioButton.add(jp_car2_RadioButton);
		
		jp_car2_all.setLayout(new GridLayout(1,2,10,10));
		jp_car2_all.add(car2);
		jp_car2_all.add(jp_car2_ComboBox_RadioButton);
		//car3
		jp_car3_RadioButton.setLayout(new GridLayout(1,3,10,10));
		jp_car3_RadioButton.add(jrb_car3_red);
		jp_car3_RadioButton.add(jrb_car3_blue);
		jp_car3_RadioButton.add(jrb_car3_black);
		
		jp_car3_ComboBox_RadioButton.setLayout(new GridLayout(2,1,10,10));
		jp_car3_ComboBox_RadioButton.add(control_car3_speed);//combobox
		jp_car3_ComboBox_RadioButton.add(jp_car3_RadioButton);
		
		jp_car3_all.setLayout(new GridLayout(1,2,10,10));
		jp_car3_all.add(car3);
		jp_car3_all.add(jp_car3_ComboBox_RadioButton);
		
		setLayout(new GridLayout(3,1,10,10));
		
		add(jp_car1_all);
		add(jp_car2_all);
		add(jp_car3_all);
		
		//set init
		jrb_car1_red.setSelected(true);
		jrb_car2_red.setSelected(true);
		jrb_car3_red.setSelected(true);
		
		control_car1_speed.setSelectedIndex(1);
		control_car2_speed.setSelectedIndex(1);
		control_car3_speed.setSelectedIndex(1);
		
		group1.add(jrb_car1_red);
		group1.add(jrb_car1_blue);
		group1.add(jrb_car1_black);
		
		group2.add(jrb_car2_red);
		group2.add(jrb_car2_blue);
		group2.add(jrb_car2_black);
		
		group3.add(jrb_car3_red);
		group3.add(jrb_car3_blue);
		group3.add(jrb_car3_black);
		//listener
		control_car1_speed.addItemListener(car_itemListener);
		control_car2_speed.addItemListener(car_itemListener);
		control_car3_speed.addItemListener(car_itemListener);
		addActionListener_JRadioButton();
		
	}
	
	public void setControl_speed(int index,JComboBox control_car_speed)
	{//set speed
		if(control_car_speed==control_car1_speed)
		{
			if(index==0)
			{
				car1.setTheSpeed(10);
			}
			else if(index==1)
			{
				car1.setTheSpeed(500);
			}
			else if(index==2)
			{
				car1.setTheSpeed(1500);
			}
		}
		else if(control_car_speed==control_car2_speed)
		{
			if(index==0)
			{
				car2.setTheSpeed(10);
			}
			else if(index==1)
			{
				car2.setTheSpeed(500);
			}
			else if(index==2)
			{
				car2.setTheSpeed(1500);
			}
		}
		else if(control_car_speed==control_car3_speed)
		{
			if(index==0)
			{
				car3.setTheSpeed(10);
			}
			else if(index==1)
			{
				car3.setTheSpeed(500);
			}
			else if(index==2)
			{
				car3.setTheSpeed(1500);
			}
		}
	}
	
	class ComboBox_ItemListener implements ItemListener
	{//call setControl_speed method
		public void itemStateChanged(ItemEvent e)
		{
			setControl_speed(control_car1_speed.getSelectedIndex(),control_car1_speed);
			setControl_speed(control_car2_speed.getSelectedIndex(),control_car2_speed);
			setControl_speed(control_car3_speed.getSelectedIndex(),control_car3_speed);
		}
	}
	public void addActionListener_JRadioButton()
	{	//actionListener
		//car1
		jrb_car1_red.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				car1.setTheColor(0);
			}
		});
		
		jrb_car1_blue.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				car1.setTheColor(1);
			}
		});
		jrb_car1_black.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				car1.setTheColor(2);
			}
		});
		//car2
		jrb_car2_red.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				car2.setTheColor(0);
			}
		});
		
		jrb_car2_blue.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				car2.setTheColor(1);
			}
		});
		jrb_car2_black.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				car2.setTheColor(2);
			}
		});
		//car3
		jrb_car3_red.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				car3.setTheColor(0);
			}
		});
		
		jrb_car3_blue.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				car3.setTheColor(1);
			}
		});
		jrb_car3_black.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				car3.setTheColor(2);
			}
		});
	}
	 
	public static void main(String[] args) 
	{	//set Frame (application)
		JFrame frame = new JFrame("DisplayRaceCar");
		A111 applet = new A111();
		
		frame.add(applet,BorderLayout.CENTER);
		
		applet.init();
		applet.start();
		
		frame.setSize(600,600);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);

	}

}
