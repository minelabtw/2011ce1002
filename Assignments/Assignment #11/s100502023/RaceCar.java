package a11.s100502023;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class RaceCar extends JPanel 
{
	private int xCoordinate;
	//�P�_���
	private boolean is_back=false;
	//delay
	int delay=500;
	Timer timer = new Timer(delay,new TimerListener());
	//color
	Color car_color_red = new Color(255,0,0);
	Color car_color_blue = new Color(0,0,255);
	Color car_color_black = new Color(0,0,0);
	
	Color car_color = car_color_red;//initial color of car
	
	public RaceCar()
	{
		timer.start();
	}
	
	public void  setTheSpeed(int delay)
	{//set speed
		timer.setDelay(delay);
	}
	
	public void setTheColor(int color_index)
	{	//set color
		//color_index=0=>Red;color_index=1=>Blue;color_index=2=>Blcak
		if(color_index==0)
		{
			car_color = car_color_red;
		}
		else if(color_index==1)
		{
			car_color = car_color_blue;
		}
		else if(color_index==2)
		{
			car_color = car_color_black;
		}
	}

	protected void paintComponent(Graphics g)
	{	//paint
		super.paintComponent(g);
		
		if (xCoordinate>getWidth()-50)
		{
			is_back=true;
		}
		else if (xCoordinate<0)
		{
			is_back=false;
		}
		
		if (is_back==true)
		{
			xCoordinate-=10;
		}
		else
		{
			xCoordinate+=10;
		}
		
		Polygon polygon = new Polygon();
		
		int x=getWidth();
		int y=getHeight();
		
		/*
		int x_origin=0;
		int y_origin=0;
		*/
		//���
		polygon.addPoint(xCoordinate+20,y-30);
		polygon.addPoint(xCoordinate+10,y-20);
		polygon.addPoint(xCoordinate+40,y-20);
		polygon.addPoint(xCoordinate+30,y-30);
		
		g.setColor(car_color);
		g.fillPolygon(polygon);
		//�����
		g.fillRect(xCoordinate,y-20,50, 10);
		//��
		g.fillOval(xCoordinate+10, y-10, 10, 10);
		g.fillOval(xCoordinate+30, y-10, 10, 10);
	}
	
	class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			repaint();
		}
	}
}
