package a11.s100502026;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class RaceCar extends JPanel{

	private Timer timer;
	private int[] carBodyX = { 0 , 0 , 50 , 50 , 40 , 30 , 20 , 10 };
	private int[] carBodyY = { 20 , 30 , 30 , 20 , 20 , 10 , 10 , 20 };
	private Polygon polygon = new Polygon();
	private Color color;

	RaceCar()
	{

	}

	public void setColor( Color color )
	{
		this.color = color;
	}

	public void setSpeed( int speed )
	{
		timer.setDelay( speed );
	}

	protected void paintComponent( Graphics g )
	{
		super.paintComponent( g );

		g.drawPolygon( carBodyX , carBodyY , carBodyX.length );
	}
}
