package a11.s100502026;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class A111 {

	private static RaceCar car0 = new RaceCar();
	private static RaceCar car1 = new RaceCar();
	private static RaceCar car2 = new RaceCar();
	
	public static void main( String[] args )
	{
		
		
		
		JTextField jtxt0 = new JTextField( "Speed" );
		int speed0;
		
		JTextField jtxt1 = new JTextField( "Speed" );
		int speed1;
		
		JTextField jtxt2 = new JTextField( "Speed" );
		int speed2;
		
		JRadioButton jrbRed0 = new JRadioButton( "Red" );
		JRadioButton jrbGreen0 = new JRadioButton( "Green" );
		JRadioButton jrbBlue0 = new JRadioButton( "Blue" );
		
		ButtonGroup group0 = new ButtonGroup();
		group0.add( jrbBlue0 );
		group0.add( jrbGreen0 );
		group0.add( jrbRed0 );
		
		jrbRed0.addActionListener
		(
			new ActionListener()
			{
				public void actionPerformed( ActionEvent e )
				{
					car0.setColor( Color.RED );
				}
			}
		);
		
		jrbGreen0.addActionListener
		(
				new ActionListener()
				{
					public void actionPerformed( ActionEvent e )
					{
						car0.setColor( Color.GREEN );
					}
				}
		);
		
		jrbBlue0.addActionListener
		(
				new ActionListener()
				{
					public void actionPerformed( ActionEvent e )
					{
						car0.setColor( Color.BLUE );
					}
				}
		);
		
		JRadioButton jrbRed1 = new JRadioButton( "Red" );
		JRadioButton jrbGreen1 = new JRadioButton( "Green" );
		JRadioButton jrbBlue1 = new JRadioButton( "Blue" );
		
		ButtonGroup group1 = new ButtonGroup();
		group1.add( jrbBlue1 );
		group1.add( jrbGreen1 );
		group1.add( jrbRed1 );
		
		jrbRed1.addActionListener
		(
			new ActionListener()
			{
				public void actionPerformed( ActionEvent e )
				{
					car1.setColor( Color.RED );
				}
			}
		);
		
		jrbGreen1.addActionListener
		(
				new ActionListener()
				{
					public void actionPerformed( ActionEvent e )
					{
						car1.setColor( Color.GREEN );
					}
				}
		);
		
		jrbBlue1.addActionListener
		(
				new ActionListener()
				{
					public void actionPerformed( ActionEvent e )
					{
						car1.setColor( Color.BLUE );
					}
				}
		);
		
		JRadioButton jrbRed2 = new JRadioButton( "Red" );
		JRadioButton jrbGreen2 = new JRadioButton( "Green" );
		JRadioButton jrbBlue2 = new JRadioButton( "Blue" );
		
		ButtonGroup group2 = new ButtonGroup();
		group2.add( jrbBlue2 );
		group2.add( jrbGreen2 );
		group2.add( jrbRed2 );
		
		jrbRed2.addActionListener
		(
			new ActionListener()
			{
				public void actionPerformed( ActionEvent e )
				{
					car2.setColor( Color.RED );
				}
			}
		);
		
		jrbGreen2.addActionListener
		(
				new ActionListener()
				{
					public void actionPerformed( ActionEvent e )
					{
						car2.setColor( Color.GREEN );
					}
				}
		);
		
		jrbBlue2.addActionListener
		(
				new ActionListener()
				{
					public void actionPerformed( ActionEvent e )
					{
						car2.setColor( Color.BLUE );
					}
				}
		);
		

		
		JFrame frame = new JFrame();

		frame.setLayout( new BorderLayout() );
		frame.setTitle( "3 Cars" );
		frame.setLocationRelativeTo( null );
		frame.setDefaultCloseOperation( frame.EXIT_ON_CLOSE );
		frame.setSize( 500 , 500 );
		frame.setVisible( true );

		frame.add( car0 , BorderLayout.NORTH );
		frame.add( car1 , BorderLayout.CENTER );
		frame.add( car2 , BorderLayout.SOUTH );
		
		speed0 = Integer.parseInt( jtxt0.getText() );
		car0.setSpeed( speed0 );
		
		speed1 = Integer.parseInt( jtxt1.getText() );
		car1.setSpeed( speed1 );

		speed2 = Integer.parseInt( jtxt2.getText() );
		car2.setSpeed( speed2 );
	}
}
