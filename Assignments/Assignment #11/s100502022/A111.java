package a11.s100502022;

import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class A111 extends Applet{
	private JTextField jtf1=new JTextField(10);//input of setting speed
	private JTextField jtf2=new JTextField(10);
	private JTextField jtf3=new JTextField(10);
	
	//set panel
	private JPanel jp1=new JPanel();
	private JPanel jp2=new JPanel();
	private JPanel jp3=new JPanel();
	private JPanel jp11=new JPanel();
	private JPanel jp22=new JPanel();
	private JPanel jp33=new JPanel();
	private JPanel jpr1=new JPanel();
	private JPanel jpr2=new JPanel();
	private JPanel jpr3=new JPanel();
	private JRadioButton jr11=new JRadioButton("Red");
	private JRadioButton jr12=new JRadioButton("Blue");
	private JRadioButton jr13=new JRadioButton("Blake");
	private JRadioButton jr21=new JRadioButton("Red");
	private JRadioButton jr22=new JRadioButton("Blue");
	private JRadioButton jr23=new JRadioButton("Blake");
	private JRadioButton jr31=new JRadioButton("Red");
	private JRadioButton jr32=new JRadioButton("Blue");
	private JRadioButton jr33=new JRadioButton("Blake");
	protected RaceCar r1=new RaceCar();
	protected RaceCar r2=new RaceCar();
	protected RaceCar r3=new RaceCar();
	public A111(){
		//set the location of panels
		setLayout(new GridLayout(3,1));
	
		
		jp1.setLayout(new GridLayout(3,1));
		jp2.setLayout(new GridLayout(3,1));
		jp3.setLayout(new GridLayout(3,1));
		jp1.add(jtf1,BorderLayout.EAST);
		jp2.add(jtf2,BorderLayout.EAST);
		jp3.add(jtf3,BorderLayout.EAST);
		jp11.add(jp1);
		jp22.add(jp2);
		jp33.add(jp3);
		
		jp1.add(jr11);
		jp1.add(jr12);
		jp1.add(jr13);
		jpr1.add(r1);
		jp1.add(jpr1,BorderLayout.EAST);
		jp2.add(jr21);
		jp2.add(jr22);
		jp2.add(jr23);
		jpr2.add(r2);
		jp2.add(jpr2,BorderLayout.EAST);
		jp3.add(jr31);
		jp3.add(jr32);
		jp3.add(jr33);
		jpr3.add(r3);
		jp3.add(jpr3,BorderLayout.EAST);
		add(jp11);
		add(jp22);
		add(jp33);
		//let the button effect
		jr11.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				r1.setColor(Color.RED);
			}
		});
		jr12.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				r1.setColor(Color.BLUE);
			}
		});
		jr13.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				r1.setColor(Color.BLACK);
			}
		});
		jr21.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				r2.setColor(Color.RED);
			}
		});
		jr22.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				r2.setColor(Color.BLUE);
			}
		});
		jr23.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				r2.setColor(Color.BLACK);
			}
		});
		jr31.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				r3.setColor(Color.RED);
			}
		});
		jr32.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				r3.setColor(Color.BLUE);
			}
		});
		jr33.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				r3.setColor(Color.BLACK);
			}
		});
		jtf1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				r1.setSpeed(jtf1.getText());
			}
		});
		jtf2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				r2.setSpeed(jtf2.getText());
			}
		});
		jtf3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				r3.setSpeed(jtf3.getText());
			}
		});
		
	
		
	}
	
	
	
}
