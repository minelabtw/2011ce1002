package a11.s100502022;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;



public class RaceCar extends JPanel{
	private int x = 0; // set origin value of x
	private boolean flag = false;
	Color co;
	int speed;
	public RaceCar() { //time
		Timer timer = new Timer(speed, new TimerListener());
		timer.start();

		class car extends JPanel { 
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				Polygon polygon = new Polygon();
				int y = getHeight() / 2;

				if (x + 50 >= getWidth()) {  //judge when the car return
					flag = true;
				} else if (x <= 0) {
					flag = false;
				}
				if (flag == true) {  //return
					x -= 5;
				} else
					x += 5; // distance add5

				int[] px = { x + 20, x + 30, x + 40, x + 10 };
				int[] py = { y, y, y + 10, y + 10 };

				for (int i = 0; i < 4; i++)//paint the car
					polygon.addPoint(px[i], py[i]);
				g.fillPolygon(polygon);
				g.setColor(co);
				g.fillRect(x, y + 10, 50, 10);
				g.fillOval(x + 10, y + 20, 10, 10);
				g.fillOval(x + 30, y + 20, 10, 10);

			}
		}
		add(new car());
	}

	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			repaint(); // redraw
		}
	}
	public void setSpeed(String s){
		speed = Integer.parseInt(s);
	}
	public void setColor(Color c){
		co=c;
	}
}
