package a11.s100502510;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.*;

public class A111 extends JApplet implements ActionListener {
	JTextField speed1 = new JTextField(10);
	JTextField speed2 = new JTextField(10);
	JTextField speed3 = new JTextField(10);
	JLabel one = new JLabel("Car1");
	JLabel two = new JLabel("Car2");
	JLabel three = new JLabel("Car3");
	JPanel speedone = new JPanel();
	JPanel speedtwo = new JPanel();
	JPanel speedthree = new JPanel();
	JPanel control1 = new JPanel();
	JPanel control2 = new JPanel();
	JPanel control3 = new JPanel();
	RaceCar car1 = new RaceCar();
	RaceCar car2 = new RaceCar();
	RaceCar car3 = new RaceCar();
	JButton change1 = new JButton("Change");
	JButton change2 = new JButton("Change");
	JButton change3 = new JButton("Change");
	JRadioButton red, green, black, red2, green2, black2, red3, green3, black3;

	public A111() {
		this.setLayout(new GridLayout(6, 1));
		speedone.setLayout(new BorderLayout());
		speedone.add(new JLabel("Speed"), BorderLayout.WEST);
		speedone.add(speed1, BorderLayout.CENTER);
		speedone.add(change1, BorderLayout.EAST);
		control1.setLayout(new GridLayout(1, 6));
		control1.add(one);
		control1.add(red = new JRadioButton("Red"));
		control1.add(green = new JRadioButton("Green"));
		control1.add(black = new JRadioButton("Black"));
		ButtonGroup group1 = new ButtonGroup();
		group1.add(red);
		group1.add(green);
		group1.add(black);
		red.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car1.setcolor("Red");
			}
		});
		green.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car1.setcolor("Green");
			}
		});
		black.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car1.setcolor("Black");
			}
		});
		control1.add(speedone);
		change1.addActionListener(this);
		speedtwo.setLayout(new BorderLayout());
		speedtwo.add(new JLabel("Speed"), BorderLayout.WEST);
		speedtwo.add(speed2, BorderLayout.CENTER);
		speedtwo.add(change2, BorderLayout.EAST);
		control2.setLayout(new GridLayout(1, 6));
		control2.add(two);
		control2.add(red2 = new JRadioButton("Red"));
		control2.add(green2 = new JRadioButton("Green"));
		control2.add(black2 = new JRadioButton("Black"));
		ButtonGroup group2 = new ButtonGroup();
		group2.add(red2);
		group2.add(green2);
		group2.add(black2);
		red2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car2.setcolor("Red");
			}
		});
		green2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car2.setcolor("Green");
			}
		});
		black2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car2.setcolor("Black");
			}
		});
		control2.add(speedtwo);
		change2.addActionListener(this);
		control3.setLayout(new GridLayout(1, 6));
		speedthree.setLayout(new BorderLayout());
		speedthree.add(new JLabel("Speed"), BorderLayout.WEST);
		speedthree.add(speed3, BorderLayout.CENTER);
		speedthree.add(change3, BorderLayout.EAST);
		control3.add(three);
		control3.add(red3 = new JRadioButton("Red"));
		control3.add(green3 = new JRadioButton("Green"));
		control3.add(black3 = new JRadioButton("Black"));
		ButtonGroup group3 = new ButtonGroup();
		group3.add(red3);
		group3.add(green3);
		group3.add(black3);
		red3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car3.setcolor("Red");
			}
		});
		green3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car3.setcolor("Green");
			}
		});
		black3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car3.setcolor("Black");
			}
		});
		control3.add(speedthree);
		change3.addActionListener(this);
		this.add(control1);
		this.add(control2);
		this.add(control3);
		this.add(car1);
		this.add(car2);
		this.add(car3);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == change1) {
			int speed = Integer.parseInt(speed1.getText());
			car1.setspeed(speed);
		} else if (e.getSource() == change2) {
			int speed = Integer.parseInt(speed2.getText());
			car2.setspeed(speed);
		} else if (e.getSource() == change3) {
			int speed = Integer.parseInt(speed3.getText());
			car3.setspeed(speed);
		}

	}
}
