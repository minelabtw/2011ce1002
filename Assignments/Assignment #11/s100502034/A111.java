package a11.s100502034;
import java.applet.Applet;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.PrintWriter;

import javax.swing.*;

public class A111  extends JApplet{
	private JRadioButton[] sC = new JRadioButton[9];//九個RadioButton
	private JLabel[] speed = new JLabel[3];//text前面總要有句話告訴別人這邊輸入速度吧
	private JTextField[] sEnter = new JTextField[3];//速度輸入的text
	private RaceCar[] c = new RaceCar[3];//車車
	private JButton[] style = new JButton[3];//有不同的型態....
	public A111() {
		Timer timer = new Timer(100,new TimerListener());//每百份之一秒就根據text的數值改變速度
		timer.start();
		Listener listener = new Listener();
		JPanel[] p = new JPanel[3];//每一車的版面(含車&選擇的東西
		JPanel[] pRadio = new JPanel[3];//除了車車 其他都在這
		for(int i =0;i<p.length;i++){
			p[i] = new JPanel();
			p[i].setLayout(new GridLayout(2,1));
			pRadio[i] = new JPanel();
			//pRadio[i].setLayout(new GridLayout(1,5));
		}
		for(int i =0;i<sEnter.length;i++){
			sEnter[i]= new JTextField("400");
		}
		for(int i = 0;i<sC.length;i+=3){
			sC[i] = new JRadioButton("red");
			sC[i].addActionListener(listener);
			sC[i+1] = new JRadioButton("green");
			sC[i+1].addActionListener(listener);
			sC[i+2] = new JRadioButton("black");
			sC[i+2].addActionListener(listener);
			sC[i+2].setSelected(true);//初始是給黑色
		}
		for(int i = 0;i<pRadio.length;i++){
			for(int y = 0;y<3;y++){
				pRadio[i].add(sC[y+i*3]);
			}
			speed[i] = new JLabel("speed enter(number inverse");
			style[i] = new JButton("style change");
			style[i].addActionListener(listener);
			pRadio[i].add(speed[i]);
			pRadio[i].add(sEnter[i]);
			pRadio[i].add(style[i]);
		}
		
		for(int i =0;i<c.length;i++){
			c[i] = new RaceCar();
		}
		for(int i =0;i<p.length;i++){
			p[i].add(pRadio[i]);
			p[i].add(c[i]);
		}
		setLayout(new GridLayout(3,1));
		for(int i = 0 ;i<p.length;i++){
			add(p[i]);
		}
	}
	class Listener implements ActionListener {
		PrintWriter[] output = new PrintWriter[6];
		public void actionPerformed(ActionEvent e) {
			if(e.getSource() == sC[0]){//這邊有點暴力哦0.o 是檢查哪個Radio按了
				c[0].setColor(1);
				sC[1].setSelected(false);//其他的button當然就要變false啦
				sC[2].setSelected(false);
			}
			else if(e.getSource() == sC[1]){
				c[0].setColor(2);
				sC[0].setSelected(false);
				sC[2].setSelected(false);
			}
			else if(e.getSource() == sC[2]){
				c[0].setColor(0);
				sC[0].setSelected(false);
				sC[1].setSelected(false);
			}
			if(e.getSource() == sC[3]){
				c[1].setColor(1);
				sC[4].setSelected(false);
				sC[5].setSelected(false);
			}
			else if(e.getSource() == sC[4]){
				c[1].setColor(2);
				sC[3].setSelected(false);
				sC[5].setSelected(false);
			}
			else if(e.getSource() == sC[5]){
				c[1].setColor(0);
				sC[3].setSelected(false);
				sC[4].setSelected(false);
			}
			if(e.getSource() == sC[6]){
				c[2].setColor(1);
				sC[7].setSelected(false);
				sC[8].setSelected(false);
			}
			else if(e.getSource() == sC[7]){
				c[2].setColor(2);
				sC[6].setSelected(false);
				sC[8].setSelected(false);
			}
			else if(e.getSource() == sC[8]){
				c[2].setColor(0);
				sC[7].setSelected(false);
			}
			for(int i =0;i<style.length;i++){
				if(e.getSource() == style[i]){
					c[i].setStyle((int)(Math.random()*3)+1);//用random選style(所以~~ 有時候可能會按了button也沒有改變
				}
			}
			
		}
	}
	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			for(int i =0;i<c.length;i++){
				c[i].setDalay(Integer.valueOf(sEnter[i].getText()));//把text的東西掉進去setDelay
			}
		}
	}
	public static void main(String[] args) {
		A111 applet = new A111();
		JFrame frame = new JFrame();
		frame.setLayout(new FlowLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("A111");
		frame.setSize(500, 500);
		frame.setVisible(true);
	}
}
