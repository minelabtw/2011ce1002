package a11.s100502034;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
class RaceCar extends JPanel {
	private int cycle = 10;//其中一個style是圈 這是它的直徑
	private int styleC = 1;//初始style為車
	private int color = 0;//初始顏色為黑色
	private int x = 0;//初始的X座標啦
	private int s = 10;//每一次移動的距離
	private int delay = 400;
	private Timer timer;
	private int second=0;
	public RaceCar(){
		timer = new Timer(delay, new TimerListener());
		timer.start();
	}
	public void paintComponent(Graphics g) {
	 	super.paintComponent(g);
		if(color ==0){//顏色變化區
			g.setColor(Color.black);
		}
		else if (color == 1){
			g.setColor(Color.red);
		}
		else{
			g.setColor(Color.green);
		}
		if(styleC <= 1){//style變化區
			g.fillRoundRect(x,0,100,50,0,0);
			g.setColor(Color.red);
			g.fillOval(x+20,40,20,20);
			g.fillOval(x+60,40,20,20);
		}
		else if(styleC <= 2){
			if(cycle >=40){
				cycle = 10;
			}
			g.fillOval(x+10,0,cycle,cycle);
			cycle+=10;
		}
		else if (styleC <=3){//這是clock copy過來的東西(x座標當然用自己的x啦
			 int clockRadius = (int)(Math.min(getWidth(), getHeight())*0.48);//把圖形稍稍平移了點

			    int yCenter = getHeight() / 2;
			    g.drawOval(x - clockRadius, yCenter - clockRadius, 2 * clockRadius, 2 * clockRadius);
			    g.drawString("12", x - 5, yCenter - clockRadius + 12);
			    g.drawString("9", x - clockRadius + 3, yCenter + 5);
			    g.drawString("3", x + clockRadius - 10, yCenter + 3);
			    g.drawString("6", x- 3, yCenter + clockRadius - 3);
			    int sLength = (int)(clockRadius * 0.8);
			    int xSecond = (int)(x + sLength * Math.sin(second * (2 * Math.PI / 60)));
			    int ySecond = (int)(yCenter - sLength * Math.cos(second * (2 * Math.PI / 60)));
			    g.setColor(Color.red);
			    g.drawLine(x, yCenter, xSecond, ySecond);
			    second++;
		}
		
		if(x>=getWidth()){
			x = -120;//超過視窗就重新跑
		}
		x+=s;
	}
	class TimerListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			repaint();
		}
	}
	public void setColor(int c){//三個set motheds
		color = c;	
	}
	public void setDalay(int d){
		timer.setDelay(d);
	}
	public void setStyle(int d){
		styleC = d;
		System.out.println(d);
	}
}
