package a11.s100502010;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class RaceCar extends JPanel{
	int del=0;
	int x=0;
	Timer timer= new Timer(del,new TimerListener());
	Color Color;
	public RaceCar()//Constructor
	{
		delay(300);
		color(Color.GREEN);
		timer.start();
	}
	public void color(Color colorin)//change color
	{
		Color=colorin;
	}
	public void delay(int delayin)//set delay
	{
		del=delayin;
		timer.setDelay(del);
	}
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		int y=getHeight();
		super.paintComponent(g);		
		if (x>getWidth()) 
		{
			x=0;
		}
		x=x+5;
		g.fillOval(x+5,y-15,15,15);//draw car
		g.fillOval(x+30,y-15,15,15);
		g.setColor(Color.BLACK);
		Polygon poly = new Polygon();
		poly.addPoint(x+10,y-20);
		poly.addPoint(x+10,y-30);
		poly.addPoint(x+40,y-30);
		poly.addPoint(x+40,y-20);
		g.setColor(Color);
		g.fillRect(x,y-20,50,10);
		g.fillPolygon(poly);
	}
	class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e) {
			repaint();
		}
	}	
}