package a11.s100502010;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.LineBorder;

public class A111 extends JApplet implements ActionListener
{
	RaceCar c1=new RaceCar();
	RaceCar c2=new RaceCar();
	RaceCar c3=new RaceCar();
	JLabel j1=new JLabel("rate of car1");
	JLabel j2=new JLabel("rate of car2");
	JLabel j3=new JLabel("rate of car3");
	int rate1,rate2,rate3;
	JTextField t1=new JTextField();
	JTextField t2=new JTextField();
	JTextField t3=new JTextField();
	JRadioButton cg1=new JRadioButton("Green");
	JRadioButton cg2=new JRadioButton("Green");
	JRadioButton cg3=new JRadioButton("Green");
	JRadioButton cb1=new JRadioButton("Blue");
	JRadioButton cb2=new JRadioButton("Blue");
	JRadioButton cb3=new JRadioButton("Blue");
	JRadioButton cr1=new JRadioButton("Red");
	JRadioButton cr2=new JRadioButton("Red");
	JRadioButton cr3=new JRadioButton("Red");
	public A111()
	{
		JPanel p1=new JPanel();                                     //panel for cars
		c1.setBorder(new LineBorder(Color.BLACK,2));
		c2.setBorder(new LineBorder(Color.BLACK,2));
		c3.setBorder(new LineBorder(Color.BLACK,2));
		p1.setLayout(new GridLayout(3,1,20,20));
		p1.add(c1);
		p1.add(c2);
		p1.add(c3);
		
		JPanel p2=new JPanel();                                      //panel for textfield and label
		p2.setLayout(new GridLayout(1,6,20,20));
		p2.add(j1);
		p2.add(t1);
		p2.add(j2);
		p2.add(t2);
		p2.add(j3);
		p2.add(t3);
		
		JPanel p3=new JPanel();                                      //panel for the radiobottom to choose color
		p3.setLayout(new GridLayout(4,1,20,20));
		p3.add(new JLabel("Car1 Color: "));
		p3.add(cr1);
		p3.add(cg1);
		p3.add(cb1);
		cr1.addActionListener(this);
		cg1.addActionListener(this);
		cb1.addActionListener(this);
		
		JPanel p4=new JPanel();                                      //panel for the radiobottom to choose color
		p4.setLayout(new GridLayout(4,1,20,20));
		p4.add(new JLabel("Car2 Color: "));
		p4.add(cr2);
		p4.add(cg2);
		p4.add(cb2);
		cr2.addActionListener(this);
		cg2.addActionListener(this);
		cb2.addActionListener(this);
		
		
		JPanel p5=new JPanel();                                      //panel for the radiobottom to choose color
		p5.setLayout(new GridLayout(4,1,20,20));
		p5.add(new JLabel("Car3 Color: "));
		p5.add(cr3);
		p5.add(cg3);
		p5.add(cb3);
		cr3.addActionListener(this);
		cg3.addActionListener(this);
		cb3.addActionListener(this);
		
		cg1.setSelected(true);
		cg2.setSelected(true);
		cg3.setSelected(true);
		
		JPanel p6=new JPanel();                                       //panel6=[anel3+panel4+panel5
		p6.setLayout(new GridLayout(3,1,20,20));
		p6.add(p3);
		p6.add(p4);
		p6.add(p5);
		
		JPanel panel=new JPanel();                                    //final panel
		
		panel.setLayout(new BorderLayout());
		panel.add(p2,BorderLayout.NORTH);
		panel.add(p6,BorderLayout.WEST);
		panel.add(p1,BorderLayout.CENTER);
		add(panel);
	}
	
	public void actionPerformed(ActionEvent e) //action
	{
		if(e.getSource()==t1)
		{
			rate1=Integer.parseInt(t1.getText());
			c1.delay(rate1);
			repaint();
		}
		else if(e.getSource()==t2)
		{
			rate2=Integer.parseInt(t1.getText());
			c2.delay(rate2);
			repaint();	
		}
		else if(e.getSource()==t3)
		{
			rate3=Integer.parseInt(t1.getText());
			c3.delay(rate3);
			repaint();	
		}
		else if(e.getSource()==cg1)
		{
			c1.color(Color.GREEN);
			repaint();
			cr1.setSelected(false);
			cb1.setSelected(false);
		}
		else if(e.getSource()==cg2)
		{
			c2.color(Color.GREEN);
			repaint();
			cr2.setSelected(false);
			cb2.setSelected(false);
		}
		else if(e.getSource()==cg3)
		{
			c3.color(Color.GREEN);
			repaint();
			cr3.setSelected(false);
			cb3.setSelected(false);
		}
		else if(e.getSource()==cb1)
		{
			c1.color(Color.BLUE);
			repaint();
			cr1.setSelected(false);
			cg1.setSelected(false);
		}
		
		else if(e.getSource()==cb2)
		{
			c2.color(Color.BLUE);
			repaint();
			cr2.setSelected(false);
			cg2.setSelected(false);
		}
		else if(e.getSource()==cb3)
		{
			c3.color(Color.BLUE);
			repaint();
			cr3.setSelected(false);
			cg3.setSelected(false);
		}
		else if(e.getSource()==cr1)
		{
			c1.color(Color.RED);
			repaint();
			cb1.setSelected(false);
			cg1.setSelected(false);
		}
		else if(e.getSource()==cr2)
		{
			c2.color(Color.RED);
			repaint();
			cb2.setSelected(false);
			cg2.setSelected(false);
		}
		else if(e.getSource()==cr3)
		{
			c3.color(Color.RED);
			repaint();
			cb3.setSelected(false);
			cg3.setSelected(false);
		}
	}
}
