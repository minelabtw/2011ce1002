package a11.s100502025;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class A111 extends JApplet {
	
	private JButton jbtsu1 = new JButton("Speed Up");  //宣告一些按鈕還有面板
	private JButton jbtsd1 = new JButton("Speed Down"); 
	private JButton jbtsu2 = new JButton("Speed Up");  
	private JButton jbtsd2 = new JButton("Speed Down");
	private JButton jbtsu3 = new JButton("Speed Up"); 
	private JButton jbtsd3 = new JButton("Speed Down"); 
	private JPanel p1 = new JPanel(); 
	private JPanel p2 = new JPanel(); 
	private JPanel p3 = new JPanel();  
	private JPanel p4 = new JPanel(); 
	private JPanel p5 = new JPanel();
	private JPanel p6 = new JPanel();
	private JPanel p7 = new JPanel();
	private JPanel p8 = new JPanel();
	private JPanel p9 = new JPanel();
	private JRadioButton jrbRed1 = new JRadioButton("Red");
	private JRadioButton jrbBlue1 = new JRadioButton("Blue");
	private JRadioButton jrbGreen1 = new JRadioButton("Green");
	private JRadioButton jrbRed2 = new JRadioButton("Red");
	private JRadioButton jrbBlue2 = new JRadioButton("Blue");
	private JRadioButton jrbGreen2 = new JRadioButton("Green");
	private JRadioButton jrbRed3 = new JRadioButton("Red");
	private JRadioButton jrbBlue3 = new JRadioButton("Blue");
	private JRadioButton jrbGreen3 = new JRadioButton("Green");
	private RaceCar car1 = new RaceCar();  //第一台車
	private RaceCar car2 = new RaceCar();  //第二台車
	private RaceCar car3 = new RaceCar();  //第三台車	
	private int delay = 1000;  
	
	public A111() {	
		jrbRed1.setMnemonic('R');  //讓各個按鈕的第一個字有底線
		jrbBlue1.setMnemonic('B');
		jrbGreen1.setMnemonic('G');
		jrbRed2.setMnemonic('R');
		jrbBlue2.setMnemonic('B');
		jrbGreen2.setMnemonic('G');
		jrbRed3.setMnemonic('R');
		jrbBlue3.setMnemonic('B');
		jrbGreen3.setMnemonic('G');
		
		ButtonGroup group1 = new ButtonGroup();  //三種不同按鈕設定成一個群組
		group1.add(jrbRed1);
		group1.add(jrbBlue1);
		group1.add(jrbGreen1);	
		
		ButtonGroup group2 = new ButtonGroup();
		group2.add(jrbRed1);
		group2.add(jrbBlue1);
		group2.add(jrbGreen1);	
		
		ButtonGroup group3 = new ButtonGroup();
		group3.add(jrbRed1);
		group3.add(jrbBlue1);
		group3.add(jrbGreen1);	
		
		p1.setLayout(new GridLayout(3,1));  //排版
		p1.add(jrbRed1);
		p1.add(jrbBlue1);
		p1.add(jrbGreen1);
		
		p2.setLayout(new GridLayout(3,1));
		p2.add(jrbRed2);
		p2.add(jrbBlue2);
		p2.add(jrbGreen2);
		
		p3.setLayout(new GridLayout(3,1));
		p3.add(jrbRed3);
		p3.add(jrbBlue3);
		p3.add(jrbGreen3);
		
		p4.setLayout(new GridLayout(1,2));
		p4.add(jbtsu1);
		p4.add(jbtsd1);
		
		p5.setLayout(new GridLayout(1,2));
		p5.add(jbtsu2);
		p5.add(jbtsd2);
		
		p6.setLayout(new GridLayout(1,2));
		p6.add(jbtsu3);
		p6.add(jbtsd3);
		
		p7.setLayout(new BorderLayout());  //第一台車的排版
		p7.add(car1,BorderLayout.CENTER);
		p7.add(p1,BorderLayout.EAST);
		p7.add(p4,BorderLayout.SOUTH);
		
		p8.setLayout(new BorderLayout());  //第二台車的排版
		p8.add(car2,BorderLayout.CENTER);
		p8.add(p2,BorderLayout.EAST);
		p8.add(p5,BorderLayout.SOUTH);
		
		p9.setLayout(new BorderLayout());  //第二台車的排版
		p9.add(car3,BorderLayout.CENTER);
		p9.add(p3,BorderLayout.EAST);
		p9.add(p6,BorderLayout.SOUTH);
		
		setLayout(new GridLayout(3,1));
		add(p7);
		add(p8);
		add(p9);
		
		
		jbtsu1.addActionListener(new ActionListener() {  //讓各個按鈕有功能
			public void actionPerformed(ActionEvent e) {
				if(delay >= 60) {
					delay = delay - 50;
				}
				else {
					delay = 50;
				}
				car1.setSpeed(delay);
			}
		});
		
		jbtsd1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				delay = delay + 50;
				car1.setSpeed(delay);
			}
		});
		
		jbtsu2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(delay >= 60) {
					delay = delay - 50;
				}
				else {
					delay = 50;
				}
				car2.setSpeed(delay);
			}
		});
		
		jbtsd2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				delay = delay + 50;
				car2.setSpeed(delay);
			}
		});
		
		jbtsu3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(delay >= 60) {
					delay = delay - 50;
				}
				else {
					delay = 50;
				}
				car3.setSpeed(delay);
			}
		});
		
		jbtsd3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				delay = delay + 50;
				car3.setSpeed(delay);
			}
		});
		
		jrbRed1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car1.setcolor(Color.RED);
			}
		});
		
		jrbBlue1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car1.setcolor(Color.BLUE);
			}
		});
		
		jrbGreen1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car1.setcolor(Color.GREEN);
			}
		});
		
		jrbRed2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car2.setcolor(Color.RED);
			}
		});
		
		jrbBlue2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car2.setcolor(Color.BLUE);
			}
		});
		
		jrbGreen2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car2.setcolor(Color.GREEN);
			}
		});
		
		jrbRed3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car3.setcolor(Color.RED);
			}
		});
		
		jrbBlue3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car3.setcolor(Color.BLUE);
			}
		});
		
		jrbGreen3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				car3.setcolor(Color.GREEN);
			}
		});
	}	
}
