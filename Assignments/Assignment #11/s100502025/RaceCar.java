package a11.s100502025;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;



public class RaceCar extends JPanel {  //車子的圖案
	private int delay = 1000;  //先設定1秒動一次
	private Color xyz = new Color(0,0,0);  //先設定車子為黑色
	private int xcoor = 0;  //x軸的初始位置
	private int ycoor = 150;  //y軸的初始位置
	
	private Timer timer = new Timer(delay,new TimerListener());  
	
	public RaceCar() {
		timer.start();  //車子開始動
	}
	
	public void setcolor(Color color) {
		xyz = color;  //設定車子的顏色
	}
	
	public void setSpeed(int speed) {
		timer.setDelay(speed);  //設定車子的速度
	}
	
	protected void paintComponent(Graphics g) {  //畫車子的圖案
		super.paintComponent(g);
		g.setColor(xyz);  //車子顏色，初始值為黑色
		g.fillRect(xcoor, ycoor, 80, 30);  //車體
		g.setColor(Color.BLACK);  //輪子也為黑色
		g.fillOval(xcoor + 10, 180, 15, 15);  //輪子為黑色
		g.fillOval(xcoor + 50, 180, 15, 15);  //輪子為黑色
		g.drawLine(0, 195, getWidth(), 195);  //讓車子跑的線
		if(xcoor < getWidth() - 80) {  //判斷有沒有撞到最右邊了
			xcoor = xcoor + 10; 
		}
		else {  //撞到了就回到起點
			xcoor = -70;
		}
	}
	
	class TimerListener implements ActionListener {  //Timer
		public void actionPerformed(ActionEvent e) {
			repaint();
		}
	}
	
	
	
	
}
