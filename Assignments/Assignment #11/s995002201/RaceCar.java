package a11.s995002201;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.management.timer.Timer;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

/*
A timer to control the speed of car
A set method to set the speed of this car (the delay of the timer)
A set method to set the color of this car
A set method to set the style of this car(optional)
An ActionListener to control drawing the JPanel
 * */
public class RaceCar extends JFrame implements ActionListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//Timer timer = new Timer(1000, new TimerListener());
	private JPanel buttonPanel1 = new JPanel();
	private JPanel buttonPanel2 = new JPanel();
	private JPanel buttonPanel3 = new JPanel();
	private JLabel label_screen = new JLabel();
	private JLabel screen1 = new JLabel("o", JLabel.CENTER);
	private JLabel screen2 = new JLabel("o", JLabel.CENTER);
	private JLabel screen3 = new JLabel("o", JLabel.CENTER);
	private JRadioButton speed0;
	private JRadioButton speed50;
	private JRadioButton speed100;
	private JRadioButton red;
	private JRadioButton yellow;
	private JRadioButton blue;
	
	Border lineBorder = new LineBorder(Color.BLACK, 3);
	
	public RaceCar()
	{
		super();
		setLayout(new GridLayout(50,10,5,1));
		buttonPanel1.setBorder(lineBorder);
		buttonPanel1.setSize(300, 300);
		buttonPanel1.setBackground(new Color(255,255,255));
		buttonPanel1.add(label_screen);
		buttonPanel1.add(screen1);
		setLayout(new GridLayout(50,5,5,1));
		buttonPanel2.setBorder(lineBorder);
		buttonPanel2.setSize(300, 100);
		buttonPanel2.setBackground(new Color(255,255,255));
		buttonPanel2.add(label_screen);
		buttonPanel2.add(screen2);
		setLayout(new GridLayout(3,5,5,1));
		buttonPanel3.setBorder(lineBorder);
		buttonPanel3.setSize(300, 300);
		buttonPanel3.setBackground(new Color(255,255,255));
		buttonPanel3.add(label_screen);
		buttonPanel3.add(screen3);
		speed0 = new JRadioButton("speed = 0",false);
		speed50 = new JRadioButton("speed = 50",false);
		speed100 = new JRadioButton("speed = 100",false);
		red = new JRadioButton("red",false);
		yellow = new JRadioButton("yellow",false);
		blue = new JRadioButton("blue",false);
		
		add(speed0);
		add(speed50);
		add(speed100);
		add(red);
		add(yellow);
		add(blue);
		add(buttonPanel1);
		add(buttonPanel2);
		add(buttonPanel3);
	}
	//public void paint(Graphics g)
	//{
	  //  g.drawRect (10, 10, 10, 10);  
	//}
	private class TimerListener implements ActionListener
	{ 
		// while the timer action every time
		public void actionPerformed(ActionEvent e)
		{
			
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		// TODO Auto-generated method stub
		//if(e.getStateChange() == speed0)
		//{
			//System.out.println("00000");
		//}
	}
}
