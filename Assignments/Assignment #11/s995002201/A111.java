package a11.s995002201;

import java.applet.Applet;

import javax.swing.JFrame;

/*
Create three cars with class RaceCar
Three components (you can use button, textfield or combo box) to control the speed of the three cars
An Radio Button component to set the color of this car
Three components (you can use button, textfield or combo box) to control the style of the three cars (optional)
ActionListeners to handle events of the three cars
 * */
public class A111 extends Applet
{
	public static void main( String[] args )
	{
		RaceCar frame = new RaceCar();
		frame.setTitle("A111");
		frame.setSize(600,600);
		//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
