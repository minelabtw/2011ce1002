package a11.s995002203;

import java.awt.*;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.*;
//import java.awt.event.ItemListener;

import javax.swing.*;

public class A111 extends JApplet implements ItemListener{
	private JPanel choicePanel =new JPanel(new GridLayout(2, 3, 5, 5));//contain 使用者的選擇
	private JPanel carPanel =new JPanel(new GridLayout(3, 1,0,10));//contain 三輛車
	private JPanel infoPanel =new JPanel(new GridLayout(3, 1, 0, 1));//contain 每輛車的資訊
	
	private String[] carColor = {"Blue", "Red", "Green", "yellow"};//combobox的選項
	private JComboBox jcbo1 = new JComboBox(carColor);
	private JComboBox jcbo2 = new JComboBox(carColor);
	private JComboBox jcbo3 = new JComboBox(carColor);
	private JTextField jtfSpeed1 = new JTextField("1");
	private JTextField jtfSpeed2 = new JTextField("1");
	private JTextField jtfSpeed3 = new JTextField("1");
	private JTextArea carInfo1=new JTextArea("speed: 1");
	private JTextArea carInfo2=new JTextArea("speed: 1");
	private JTextArea carInfo3=new JTextArea("speed: 1");
	private RaceCar car1 =new RaceCar();
	private RaceCar car2 =new RaceCar();
	private RaceCar car3 =new RaceCar();
	ActionListener acListener = new acListener();
	
	public A111(){
		Font font=new Font("Arial", Font.BOLD, 25);
		jcbo1.addItemListener( this);
		jcbo2.addItemListener( this);
		jcbo3.addItemListener( this);
		jtfSpeed1.addActionListener(acListener);
		jtfSpeed2.addActionListener(acListener);
		jtfSpeed3.addActionListener(acListener);
		
		jtfSpeed1.setHorizontalAlignment(JTextField.RIGHT);
		jtfSpeed2.setHorizontalAlignment(JTextField.RIGHT);
		jtfSpeed3.setHorizontalAlignment(JTextField.RIGHT);
		
		choicePanel.add(jcbo1);
		choicePanel.add(jcbo2);
		choicePanel.add(jcbo3);
		choicePanel.add(jtfSpeed1);
		choicePanel.add(jtfSpeed2);
		choicePanel.add(jtfSpeed3);

		
		setLayout(new BorderLayout());
		add(choicePanel, BorderLayout.NORTH);

		carInfo1.setFont(font);
		carInfo2.setFont(font);
		carInfo3.setFont(font);
		carInfo1.setBackground(Color.pink);
		carInfo2.setBackground(Color.pink);
		carInfo3.setBackground(Color.pink);
		
		//carInfo3.setFont(font);
		carPanel.add(car1);
		carPanel.add(car2);
		carPanel.add(car3);
		infoPanel.add(carInfo1);
		infoPanel.add(carInfo2);
		infoPanel.add(carInfo3);
		add(carPanel,BorderLayout.CENTER);
		add(infoPanel,BorderLayout.EAST);
		
	}
	
	public void itemStateChanged(ItemEvent e) {//combobox狀態改變時要做的事
		if(e.getSource()==jcbo1)
			car1.setColor(jcbo1.getSelectedIndex());
		else if(e.getSource()==jcbo2)
			car2.setColor(jcbo2.getSelectedIndex());
		else if(e.getSource()==jcbo3)
			car3.setColor(jcbo3.getSelectedIndex());
		
	}
	
	class acListener implements ActionListener {//使用者輸入speed之後要做的事
		public void actionPerformed(ActionEvent e) {
			if(e.getSource()==jtfSpeed1){
				car1.setSpeed(Integer.parseInt(jtfSpeed1.getText()));
				jtfSpeed1.requestFocusInWindow();
				carInfo1.setText("speed: "+jtfSpeed1.getText());
				jtfSpeed1.setText("");
				
				
			}
			else if(e.getSource()==jtfSpeed2){
				car2.setSpeed(Integer.parseInt(jtfSpeed2.getText()));
				jtfSpeed2.requestFocusInWindow();
				carInfo2.setText("speed: "+jtfSpeed2.getText());
				jtfSpeed2.setText("");
			}
			else if(e.getSource()==jtfSpeed3){
				car3.setSpeed(Integer.parseInt(jtfSpeed3.getText()));
				jtfSpeed3.requestFocusInWindow();
				carInfo3.setText("speed: "+jtfSpeed3.getText());
				jtfSpeed3.setText("");
			}
     	 }
	}
	public static void main(String[] args){
		A111 f=new A111();

		/*f.setTitle("car race");
		f.setSize(1000, 1000);
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);*/
	}
	
}
