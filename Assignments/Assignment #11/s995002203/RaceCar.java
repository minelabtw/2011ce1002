package a11.s995002203;
import java.awt.*;
import java.awt.event.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;
public class RaceCar extends JPanel {
	Timer timer = new Timer(1000, new TimerListener());
	public int clk=0;
	public int nt=1000;
	public int speed=200;
	final int nomalSpeed=200;//原始速度
	public Color carColor=Color.blue;;
	
	public RaceCar(){
		timer.start();
	}
	
	public void paintComponent(Graphics g) {
	    super.paintComponent(g);
	    int hh=getHeight()/2;
	    if((40+clk)==getWidth())//如果車子走到底則把重頭開始走
	    	clk=0;
	    int[] x = {0+clk, 0+clk, 10+clk, 10+clk, 30+clk, 30+clk, 40+clk, 40+clk};
	    int[] y = {hh, hh-10, hh-10, hh-20, hh-20, hh-10, hh-10, hh};
	    
	    g.setColor(carColor);
	    g.fillPolygon(x, y, x.length);//畫車子
	    g.setColor(Color.black);
	    g.fillOval(0+clk,getHeight()/2,10,10);//畫輪胎
	    g.fillOval(30+clk,getHeight()/2,10,10);
	    

	}
	
	public void setSpeed(int num){
		speed= nomalSpeed/num;
	}
	
	public void setColor(int num){//接收combobox的資料 看使用者選擇了哪種顏色
		if(num==0)
			carColor=Color.blue;
		else if(num==1)
			carColor=Color.red;
		else if(num==2)
			carColor=Color.green;
		else if(num==3)
			carColor=Color.yellow;
		repaint();
	}
	
	  private class TimerListener implements ActionListener {
		    /** Handle the action event */
		    public void actionPerformed(ActionEvent e) {
		      // Set new time and repaint the clock to display current time
		    	
		    	clk++;
		    	if(speed==0)//最大的速度是delay(0)的時候
		    		timer.setDelay(0);
		    	else
		    		 timer.setDelay(speed);
		    	repaint();
		    }
		  }
}
