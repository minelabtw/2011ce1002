package a11.s100502017;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class RaceCar extends JPanel{
	protected int X=0;
	protected boolean b=true;
	Timer t;
	Color c=new Color(123,45,88);
	public RaceCar(){
		this.setSize(333,500);
	}
	public void setSpeed(int delay){//set delay
		t=new Timer(delay,new TimerListener());
		t.start();
	}
	public void setcolor(Color col){//change cars color
		c=col;
		repaint();
	}
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		if(X<0)//control arrow
			b=true;
		else if(X>getWidth()-40)
			b=false;
		else
			b=b;
		if(b==true)
			X+=3;
		else
			X-=3;
		Polygon pol=new Polygon();
		pol.addPoint(X+10,getHeight()-20);
		pol.addPoint(X+20,getHeight()-30);
		pol.addPoint(X+30,getHeight()-30);
		pol.addPoint(X+40,getHeight()-20);
		g.setColor(new Color(0,0,0));
		g.fillOval(X+10, getHeight()-10,10,10);
		g.fillOval(X+30, getHeight()-10,10,10);
		g.setColor(c);
		g.fillRect(X,getHeight()-20,50,11);
		g.setColor(new Color(155,100,20));
		g.fillPolygon(pol);
	}
	public Dimension getPreferredSize(){//override getPreferredSize
		return new Dimension(500,60);
	}
	class TimerListener implements ActionListener{
		public void actionPerformed(ActionEvent e){//time event is here
			repaint();
		}
	}
}
