package a11.s100502017;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
public class A111 extends JApplet implements ActionListener{
	RaceCar car1=new RaceCar();//create car panel
	RaceCar car2=new RaceCar();
	RaceCar car3=new RaceCar();
	JPanel head=new JPanel();
	JPanel body=new JPanel();
	JPanel pan1=new JPanel();
	JPanel pan2=new JPanel();
	JPanel pan3=new JPanel();
	JPanel rbpan1=new JPanel();
	JPanel rbpan2=new JPanel();
	JPanel rbpan3=new JPanel();
	JTextField d1=new JTextField(5);
	JTextField d2=new JTextField(5);
	JTextField d3=new JTextField(5);
	ButtonGroup g1=new ButtonGroup();
	ButtonGroup g2=new ButtonGroup();
	ButtonGroup g3=new ButtonGroup();
	JRadioButton red1,blue1,green1,red2,blue2,green2,red3,blue3,green3;
	public A111(){	//constructor for Applet
		this.setSize(2500,1600);
		setLayout(new BorderLayout());
		head.setLayout(new FlowLayout());
		head.add(new JLabel("car1 delay"));
		head.add(d1);
		head.add(new JLabel("car2 delay"));
		head.add(d2);
		head.add(new JLabel("car3 delay"));
		head.add(d3);
				
		rbpan1.setLayout(new GridLayout(3,1));
		rbpan1.add(red1=new JRadioButton("red"));
		rbpan1.add(blue1=new JRadioButton("blue"));
		rbpan1.add(green1=new JRadioButton("green"));
		
		rbpan2.setLayout(new GridLayout(3,1));
		rbpan2.add(red2=new JRadioButton("red"));
		rbpan2.add(blue2=new JRadioButton("blue"));
		rbpan2.add(green2=new JRadioButton("green"));
		
		rbpan3.setLayout(new GridLayout(3,1));
		rbpan3.add(red3=new JRadioButton("red"));
		rbpan3.add(blue3=new JRadioButton("blue"));
		rbpan3.add(green3=new JRadioButton("green"));
		
		pan1.add(car1,BorderLayout.CENTER);
		pan1.add(rbpan1,BorderLayout.EAST);
		
		pan2.add(car2,BorderLayout.CENTER);
		pan2.add(rbpan2,BorderLayout.EAST);
		
		pan3.add(car3,BorderLayout.CENTER);
		pan3.add(rbpan3,BorderLayout.EAST);
		body.setLayout(new GridLayout(3,1));
		body.add(pan1);
		body.add(pan2);
		body.add(pan3);
		add(head,BorderLayout.NORTH);
		add(body,BorderLayout.CENTER);
		g1.add(red1);//make group
		g1.add(blue1);
		g1.add(green1);
		g2.add(red2);
		g2.add(blue2);
		g2.add(green2);
		g3.add(red3);
		g3.add(blue3);
		g3.add(green3);
		d1.addActionListener(this);//add event
		d2.addActionListener(this);
		d3.addActionListener(this);
		red1.addActionListener(this);
		blue1.addActionListener(this);
		green1.addActionListener(this);
		red2.addActionListener(this);
		blue2.addActionListener(this);
		green2.addActionListener(this);
		red3.addActionListener(this);
		blue3.addActionListener(this);
		green3.addActionListener(this);
	}
	public static void main(String [] args){
		JFrame f=new JFrame("A11");
		A111 applet =new A111();
		f.add(applet);
		f.setSize(2500,1600);
		f.pack();//set good size
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
	public void actionPerformed(ActionEvent e){//component event is here
		if(e.getSource()==d1){
			car1.setSpeed(Integer.valueOf(d1.getText()));
		}
		else if(e.getSource()==d2){
			car2.setSpeed(Integer.valueOf(d2.getText()));
		}
		else if(e.getSource()==d3){
			car3.setSpeed(Integer.valueOf(d3.getText()));
		}
		if(e.getSource()==red1){
			car1.setcolor(Color.RED);
		}
		else if(e.getSource()==blue1){
			car1.setcolor(Color.BLUE);
		}
		else if(e.getSource()==green1){
			car1.setcolor(Color.GREEN);
		}
		else if(e.getSource()==red2){
			car2.setcolor(Color.RED);
		}
		else if(e.getSource()==blue2){
			car2.setcolor(Color.BLUE);
		}
		else if(e.getSource()==green2){
			car2.setcolor(Color.GREEN);
		}
		else if(e.getSource()==red3){
			car3.setcolor(Color.RED);
		}
		else if(e.getSource()==blue3){
			car3.setcolor(Color.BLUE);
		}
		else if(e.getSource()==green3){
			car3.setcolor(Color.GREEN);
		}
	}
}
