//RaceCar.java
package a11.s100502502;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;;
public class RaceCar extends JPanel{
	int X = 0;
	int Y = getWidth();
	private Color CenterPair = Color.BLUE;//create car's pair color
	private Timer timer;
	public void setTimer(Timer time){//set car's timer
		timer = time;
	}
	public void setSpeed(int delay){//set car's speed
		timer.setDelay(delay);
	}
	public void startCar(){
		timer.start();
	}
	public void setColor(int c){//set car's color
		if(c == 1){
			CenterPair = Color.RED;
		}
		else if(c == 2){
			CenterPair = Color.GREEN;
		}
		else if(c == 3){
			CenterPair = Color.BLUE;
		}
		else if(c == 4){
			CenterPair = Color.ORANGE;
		}
		else if(c == 5){
			CenterPair = Color.PINK;
		}
		else if(c == 6){
			CenterPair = Color.YELLOW;
		}
	}
	public Color getColor(){//get car's pair color
		return CenterPair;
	}
	protected void paintComponent(Graphics g){//paint car
		super.paintComponent(g);
		if(X == 0 && Y == 0){
			X = 0;
			Y = getHeight();
		}
		g.setColor(Color.BLACK);
		g.drawLine(0, getHeight()-5, getWidth(), getHeight()-5);
		if(X == getWidth()){
			X = -10;
		}
		X = X + 1;
		g.setColor(Color.BLACK);
		g.fillOval(X+10, Y-16, 10, 10);
		g.fillOval(X+30, Y-16, 10, 10);
		g.setColor(getColor());
		g.fillRect(X, Y-25, 50, 10);
		g.setColor(Color.gray);
		int[] x = {X+10, X+20, X+30, X+40};
		int[] y = {Y-25, Y-35, Y-35, Y-25};
		g.fillPolygon(x, y, 4);
	}
}
