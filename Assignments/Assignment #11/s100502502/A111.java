//A111.java
package a11.s100502502;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
public class A111 extends JApplet{
	//Create cars, timers,Label , TextField, Panel, buttons
	RaceCar car1 = new RaceCar();
	RaceCar car2 = new RaceCar();
	RaceCar car3 = new RaceCar();
	Timer timer1 = new Timer(20, new TimerListener(car1));
	Timer timer2 = new Timer(20, new TimerListener(car2));
	Timer timer3 = new Timer(20, new TimerListener(car3));
	JPanel Pspeed = new JPanel();
	JLabel L1 = new JLabel("Car1");
	JLabel L2 = new JLabel("Car2");
	JLabel L3 = new JLabel("Car3");
	JTextField f1 = new JTextField(4);
	JTextField f2 = new JTextField(4);
	JTextField f3 = new JTextField(4);
	JPanel CA = new JPanel();
	JPanel RadioA = new JPanel();
	ButtonGroup group1 = new ButtonGroup();
	ButtonGroup group2 = new ButtonGroup();
	ButtonGroup group3 = new ButtonGroup();
	JRadioButton Red = new JRadioButton("Red");
	JRadioButton Blue = new JRadioButton("Blue");
	JRadioButton Green = new JRadioButton("Green");
	JRadioButton Pink = new JRadioButton("Pink");
	JRadioButton Orange = new JRadioButton("Orange");
	JRadioButton Yellow = new JRadioButton("Yellow");
	JRadioButton Pink2 = new JRadioButton("Pink");
	JRadioButton Blue2 = new JRadioButton("Blue");
	JRadioButton Orange2 = new JRadioButton("Orange"); 
	JButton Set = new JButton("SetSpeed");
	public void init(){
		//set car's timer
		car1.setTimer(timer1);
		car2.setTimer(timer2);
		car3.setTimer(timer3);
		setSize(800, 600);//set frame's size
		//set layout
		CA.setLayout(new GridLayout(3, 1, 1, 1));
		RadioA.setLayout(new GridLayout(9, 1, 1, 1));
		//add elements
		Pspeed.add(L1);
		Pspeed.add(f1);
		Pspeed.add(L2);
		Pspeed.add(f2);
		Pspeed.add(L3);
		Pspeed.add(f3);
		Pspeed.add(Set);
		RadioA.add(Red);
		RadioA.add(Green);
		RadioA.add(Blue);
		RadioA.add(Orange);
		RadioA.add(Pink);
		RadioA.add(Yellow);
		RadioA.add(Pink2);
		RadioA.add(Blue2);
		RadioA.add(Orange2);
		group1.add(Red);
		group1.add(Green);
		group1.add(Blue);
		group2.add(Orange);
		group2.add(Pink);
		group2.add(Yellow);
		group3.add(Pink2);
		group3.add(Blue2);
		group3.add(Orange2);
		CA.add(car1);
		CA.add(car2);
		CA.add(car3);
		add(Pspeed, BorderLayout.NORTH);
		add(RadioA, BorderLayout.WEST);
		add(CA, BorderLayout.CENTER);
		//start cars
		car1.startCar();
		car2.startCar();
		car3.startCar();
		//add button's action listener
		Red.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				car1.setColor(1);
			}
		});
		Green.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				car1.setColor(2);
			}
		});
		Blue.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				car1.setColor(3);
			}
		});
		Orange.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				car2.setColor(4);
			}
		});
		Pink.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				car2.setColor(5);
			}
		});
		Yellow.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				car2.setColor(6);
			}
		});
		Pink2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				car3.setColor(5);
			}
		});
		Blue2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				car3.setColor(3);
			}
		});
		Orange2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				car3.setColor(4);
			}
		});
		Set.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(!f1.getText().isEmpty()){
					int speed = Integer.parseInt(f1.getText());
					car1.setSpeed(speed);
				}
				else{}
				if(!f2.getText().isEmpty()){
					int speed = Integer.parseInt(f2.getText());
					car2.setSpeed(speed);
				}
				else{}
				if(!f3.getText().isEmpty()){
					int speed = Integer.parseInt(f3.getText());
					car3.setSpeed(speed);
				}
				else{}
			}
		});
	}
	class TimerListener implements ActionListener{//when time run
		RaceCar car;
		TimerListener(RaceCar Car){
			car = Car;
		}
		public void actionPerformed(ActionEvent e){
			car.repaint();
		}
	}
}
