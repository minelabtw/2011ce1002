package a11.s100502020;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;
public class A111 extends JApplet implements ActionListener{
	private JPanel JPcolor1 = new JPanel();//panels 
	private JPanel JPcolor2 = new JPanel();
	private JPanel JPcolor3 = new JPanel();
	private JPanel JPcar1 = new JPanel();
	private JPanel JPcar2 = new JPanel();
	private JPanel JPcar3 = new JPanel();	
	private JPanel JPCarframe = new JPanel();
	private JPanel JPtext1 = new JPanel();
	private JPanel JPtext2 = new JPanel();
	private JPanel JPtext3 = new JPanel();
	private JPanel JPSpeedframe = new JPanel();
	private JRadioButton JRBBlue1 = new JRadioButton("Blue");
	private JRadioButton JRBGrey1=new JRadioButton("Grey");
	private JRadioButton JRBYellow1=new JRadioButton("Yellow");
	private JRadioButton JRBBlue2 = new JRadioButton("Blue");
	private JRadioButton JRBGrey2=new JRadioButton("Grey");
	private JRadioButton JRBYellow2=new JRadioButton("Yellow");
	private JRadioButton JRBBlue3 = new JRadioButton("Blue");
	private JRadioButton JRBGrey3=new JRadioButton("Grey");
	private JRadioButton JRBYellow3=new JRadioButton("Yellow");
	private JTextField TFspeed1=new JTextField(10);
	private JTextField TFspeed2=new JTextField(10);
	private JTextField TFspeed3=new JTextField(10);
	private JLabel label1 = new JLabel("Car1");
	private JLabel label2 = new JLabel("Car2");
	private JLabel label3 = new JLabel("Car3");
	private RaceCar car1 = new RaceCar();
	private	RaceCar car2 = new RaceCar();
	private	RaceCar car3 = new RaceCar();
	
	
	
	public A111()
	{
		ButtonGroup group1 = new ButtonGroup();//for button independent
		ButtonGroup group2 = new ButtonGroup();
		ButtonGroup group3 = new ButtonGroup();
		group1.add(JRBBlue1);
		group1.add(JRBGrey1);
		group1.add(JRBYellow1);
		group2.add(JRBBlue2);
		group2.add(JRBGrey2);
		group2.add(JRBYellow2);
		group3.add(JRBBlue3);
		group3.add(JRBGrey3);
		group3.add(JRBYellow3);
		
		JPcar1.setLayout(new BorderLayout(5,5));//set car1 panel and color
		JPcolor1.setLayout(new GridLayout(3,1));
		JPcolor1.add(JRBBlue1);
		JPcolor1.add(JRBGrey1);
		JPcolor1.add(JRBYellow1);
		JPcar1.add(JPcolor1,BorderLayout.WEST);
		JPcar1.add(car1,BorderLayout.CENTER);
		
		JPcar2.setLayout(new BorderLayout(5,5));//set car2 panel and color
		JPcolor2.setLayout(new GridLayout(3,1));
		JPcolor2.add(JRBBlue2);
		JPcolor2.add(JRBGrey2);
		JPcolor2.add(JRBYellow2);
		JPcar2.add(JPcolor2,BorderLayout.WEST);
		JPcar2.add(car2,BorderLayout.CENTER);
		
		JPcar3.setLayout(new BorderLayout(5,5));//set car3 panel and color
		JPcolor3.setLayout(new GridLayout(3,1));
		JPcolor3.add(JRBBlue3);
		JPcolor3.add(JRBGrey3);
		JPcolor3.add(JRBYellow3);
		JPcar3.add(JPcolor3,BorderLayout.WEST);
		JPcar3.add(car3,BorderLayout.CENTER);
		
			
		JPtext1.add(label1,BorderLayout.WEST);//the top panel with speed change
		JPtext1.add(TFspeed1,BorderLayout.CENTER);
		JPtext2.add(label2,BorderLayout.WEST);
		JPtext2.add(TFspeed2,BorderLayout.CENTER);
		JPtext3.add(label3,BorderLayout.WEST);
		JPtext3.add(TFspeed3,BorderLayout.CENTER);
		JPSpeedframe.setLayout(new GridLayout(1,3));
		JPSpeedframe.add(JPtext1);
		JPSpeedframe.add(JPtext2);
		JPSpeedframe.add(JPtext3);
		
		
		
		
		TFspeed1.addActionListener(this);
		TFspeed2.addActionListener(this);
		TFspeed3.addActionListener(this);
		JRBBlue1.addActionListener(this);
		JRBBlue2.addActionListener(this);
		JRBBlue3.addActionListener(this);
		JRBGrey1.addActionListener(this);
		JRBGrey2.addActionListener(this);
		JRBGrey3.addActionListener(this);
		JRBYellow1.addActionListener(this);
		JRBYellow2.addActionListener(this);
		JRBYellow3.addActionListener(this);
		setLayout(new GridLayout(4,1));//set the frame into 4 piece
		add(JPSpeedframe);//in order add in
		add(JPcar1);
		add(JPcar2);
		add(JPcar3);
	}
	
	public void actionPerformed(ActionEvent e) //action listener
	{
		if(e.getSource()==JRBBlue1)
		{
			car1.setColor(Color.BLUE);
		}
		else if(e.getSource()==JRBGrey1)
		{
			car1.setColor(Color.gray);
		}
		else if(e.getSource()==JRBYellow1)
		{
			car1.setColor(Color.YELLOW);
		}
		else if(e.getSource()==JRBBlue2)
		{
			car2.setColor(Color.BLUE);
		}
		else if(e.getSource()==JRBGrey2)
		{
			car2.setColor(Color.GRAY);
		}
		else if(e.getSource()==JRBYellow2)
		{
			car2.setColor(Color.YELLOW);
		}
		else if(e.getSource()==JRBBlue3)
		{
			car3.setColor(Color.BLUE);
		}
		else if(e.getSource()==JRBGrey3)
		{
			car3.setColor(Color.GRAY);
		}
		else if(e.getSource()==JRBYellow3)
		{
			car3.setColor(Color.YELLOW);
		}
		else if(e.getSource()==TFspeed1)
		{
			if(TFspeed1.getText()!="")
			{
				int Speed1=Integer.parseInt(TFspeed1.getText());//change to int type
				car1.setSpeed(1000/Speed1);
			}
		}
		else if(e.getSource()==TFspeed2)
		{
			if(TFspeed2.getText()!="")
			{
				int Speed2=Integer.parseInt(TFspeed2.getText());
				car2.setSpeed(1000/Speed2);
			}
		}
		else if(e.getSource()==TFspeed3)
		{
			if(TFspeed3.getText()!="")
			{
				int Speed3=Integer.parseInt(TFspeed3.getText());
				car3.setSpeed(1000/Speed3);
			}
		}
		
	}
}
