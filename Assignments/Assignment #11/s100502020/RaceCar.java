package a11.s100502020;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class RaceCar extends JPanel{//initial
	private Color color = (Color.BLACK);
	int speed = 1000;
	int centerx = 0;
	int centery = 0;
	int dir=0;
	
	public Timer timer = new Timer(1000,new TimerListener());
	public RaceCar()//timer start and set car
	{
		setLayout(new GridLayout(1,1,5,5));
		add(new DrawCar(),BorderLayout.CENTER);
		timer.start();
	}	
	public void setSpeed(int X)
	{
		timer.setDelay(X);
	}	
	public void setColor(Color c)
	{
		color=c;
	}
	class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)//if time change then repaint the car
		{
			repaint();
		}
	}
	public class DrawCar extends JPanel{//draw car
		protected void paintComponent(Graphics g){
			super.paintComponent(g);
			if(centerx==0&&centery==0)
			{
				centerx = getWidth()/2;
				centery = getHeight()/2;
			}
			
			
			g.setColor(color);
			
			
			
			g.drawOval(centerx-15, centery+5, 15, 15);
			g.drawOval(centerx+5, centery+5, 15, 15);
			g.fillOval(centerx-15, centery+5, 15, 15);
			g.fillOval(centerx+5, centery+5, 15, 15);
			
			
			
			int capx[]={centerx+15,centerx+5,centerx-5,centerx-25};
			int capy[]={centery-5,centery-15,centery-15,centery-5};
			g.drawPolygon(capx,capy,capx.length);
			g.fillPolygon(capx,capy,capx.length);
			
			
			
			int bodyx[]={centerx-25,centerx-25,centerx+25,centerx+25};
			int bodyy[]={centery-5,centery+5,centery+5,centery-5};			
			g.drawPolygon(bodyx,bodyy,bodyx.length);
			g.fillPolygon(bodyx,bodyy,bodyx.length);
			
			
			
			
			if(centerx+30>=getWidth())
				dir=1;
			if(centerx-30<=0)
				dir=0;
			if(dir==0)
				centerx+=10;
			if(dir==1)
				centerx-=10;
		}
	}
}
