package a11.s100502516;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class RaceCar extends JPanel {	//draw car
	private int x = 0;
	private int y = 0;
	private int speed = 1;
	private Timer timer = new Timer(10, new TimerListener());
	private Color body = Color.RED;
	private int style = 0;	//shape flag
	
	public RaceCar(int pWidth, int pHeight)
	{			
		this.x = pWidth / 2;
		this.y = pHeight / 3;
		
		timer.start();
	}
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);	
		
		if(style == 0)	//chose the style
			DrawCar(g);
		else if(style == 1)
			DrawUFO(g);
		else if(style == 2)
			DrawFly(g);
	}
	public void DrawCar(Graphics g)
	{
		int[] x1 = {x + 10, x + 20, x + 30, x + 40};
		int[] y1 = {y + 10, y, y, y +10};
		g.setColor(body);
		g.fillPolygon(x1, y1, 4);
		
		int[] x2 = {x, x + 50, x + 50, x};
		int[] y2 = {y + 10, y + 10, y + 20, y + 20};
		g.setColor(Color.GREEN);
		g.fillPolygon(x2,  y2, 4);
		
		int radius = 5;
		g.setColor(Color.BLACK);
		g.fillOval(x + 15 - radius, y + 25 - radius, radius * 2, radius * 2);
		g.fillOval(x + 35 - radius, y + 25 - radius, radius * 2, radius * 2);
	}
	public void DrawUFO(Graphics g)
	{
		g.setColor(body);
		g.fillOval(x, y, 40, 20);
		g.setColor(Color.ORANGE);
		g.fillOval(x + 20, y + 10, 5, 5);
	}
	public void DrawFly(Graphics g)
	{
		g.setColor(body);
		g.fillOval(x, y, 10, 10);
		g.setColor(Color.BLACK);
		g.drawOval(x - 3, y, 6, 6);
		g.drawOval(x + 7, y, 6, 6);
	}
	public void setDelay(int delay)
	{
		timer.setDelay(delay);
	}
	public void setColor(Color color)
	{
		body = color;
	}
	public void setStyle(int style)
	{
		this.style = style;
	}

	private class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			x += speed;
			
			if(x >= getWidth())
				x = 0;
			
			repaint();			
		}
	}
}
