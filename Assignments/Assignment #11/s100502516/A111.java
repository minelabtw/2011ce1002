package a11.s100502516;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class A111 extends JApplet {			
	public void init()
	{
		int nums = 3;
		ControlPanel[] cars = new ControlPanel[nums];
		setLayout(new GridLayout(nums, 1));
		
		int pWidth = Integer.parseInt(getParameter("PWIDTH"));	//get display size
		int pHeight = Integer.parseInt(getParameter("PHEIGHT"));
		
		for(int i = 0; i < nums; i++)
		{
			cars[i] = new ControlPanel(pWidth, pHeight);
			add(cars[i]);
		}
	}		
}

class ControlPanel extends JPanel	//separate car panel
{
	private RaceCar car;
	private JRadioButton jrbRed, jrbBlue, jrbYellow;
	private JTextField jtfSpeed = new JTextField();
	private String[] styleTitle = {"Car", "UFO", "Fly"};
	private JComboBox jcbo = new JComboBox(styleTitle);

	public ControlPanel(int pWidth, int pHeight)
	{
		car = new RaceCar(pWidth, pHeight);
		
		JPanel panelJrb = new JPanel(new GridLayout(1, 3));
		panelJrb.add(jrbRed = new JRadioButton("Red"));
		panelJrb.add(jrbBlue = new JRadioButton("Blue"));
		panelJrb.add(jrbYellow = new JRadioButton("Yellow"));
		
		ButtonGroup group = new ButtonGroup();
		group.add(jrbRed);
		group.add(jrbBlue);
		group.add(jrbYellow);
		
		jrbRed.addActionListener(new ButtonListener());
		jrbBlue.addActionListener(new ButtonListener());
		jrbYellow.addActionListener(new ButtonListener());
		
		JPanel panelSpeed = new JPanel(new BorderLayout(5, 0));
		panelSpeed.add(new JLabel("Speed(1~1000)"), BorderLayout.WEST);
		panelSpeed.add(jtfSpeed, BorderLayout.CENTER);
		jtfSpeed.addActionListener(new TextListener());
		
		JPanel p1 = new JPanel(new BorderLayout());
		p1.add(panelSpeed, BorderLayout.NORTH);
		p1.add(panelJrb, BorderLayout.CENTER);
		
		JPanel p2 = new JPanel(new BorderLayout());
		p2.add(jcbo, BorderLayout.NORTH);
		p2.add(car, BorderLayout.CENTER);
		jcbo.addItemListener(new ComboBoxListener());
		
		setLayout(new BorderLayout());	
		add(p1, BorderLayout.WEST);
		add(p2, BorderLayout.CENTER);		
	}
	
	private class ButtonListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if(e.getSource() == jrbRed)
				car.setColor(Color.RED);
			else if(e.getSource() == jrbBlue)
				car.setColor(Color.BLUE);
			else if(e.getSource() == jrbYellow)
				car.setColor(Color.YELLOW);
		}
	}
	private class TextListener implements ActionListener	//set speed
	{
		public void actionPerformed(ActionEvent e)
		{
			int temp = Integer.parseInt(jtfSpeed.getText());
			
			if(temp >= 1000)
				temp = 1000;
			else if(temp <= 1)
				temp = 1;
			
			car.setDelay(temp);	//remember to pass key enter
		}
	}
	private class ComboBoxListener implements ItemListener
	{		
		public void itemStateChanged(ItemEvent e) 
		{			
			car.setStyle(jcbo.getSelectedIndex());
		}		
	}
}
