package a6.s100502020;

public class Person {
	protected String Name;
	protected String Gender;
	protected String Mess = "This guys is a Person!";
	
	Person(String name, String gender)//constructor
	{
		Name = name;
		Gender = gender;
	}

	public String getName()
	{
		return Name;
	}

	public String getGender()
	{
		return Gender;
	}
	
	public String toString()//get message
	{
		return Mess;
	}
	
}
