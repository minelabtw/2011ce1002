package a6.s100502020;

public class Employee extends Person{//class extends Person
	protected int Salary;
	protected int Rank;
	
	Employee(String name,String gender,int salary,int rank)//contructor with inherit
	{
		super(name,gender);
		Salary = salary;
		Rank = rank;
	}
	public int getSalary()
	{
		return Salary;
	}
	public String getRank()//get rank
	{
		String mess = "";
		if(Rank == 1)
		{
			mess = "Professor";
		}
		else if(Rank == 2)
		{
			mess = "Associated Professor";
		}
		else if(Rank == 3)
		{
			mess = "Assistant Professor";
		}
		else if(Rank == 4)
		{
			mess = "Lecturer";
		}
		return mess;//return string
	}	
	public String toString(boolean examine)//check by boolean
	{
		String mess = "";
		if(examine == true)
		{
			mess = "This guy is a Employee!!";
		}else if(examine == false) 
		{
			mess = "Though this guy is not a Student, " + Mess;
		}
		return mess;
	}	
	public String toString()
		{
			String mess = "This guy is a Employee!";
			return mess;		
		}
}
