package a6.s100502521;

public class Student extends Person //繼承Person
{
	private String classroom;//班級
	public Student(String in_name, String in_gender,String in_classroom)//初始化
	{
		super(in_name, in_gender);
		setClassRoom(in_classroom);
	}
	public void setClassRoom(String input)//設定班級
	 {
		classroom=input;
	 }
	 public String getClassRoom()//取得班級
	 {
		 return classroom;
	 }
	 public String toString()//回傳字串
	 {
		 return "This guy is a Student!!";
	 }
	 public String toString(boolean check)//回傳字串 是學生 不是教師
	 {
		 if(check)
		 {
			 return toString();
		 }
		 else
		 {
			 return "Though this guy is not an Employee, "+super.toString();
		 }
	 }
}
