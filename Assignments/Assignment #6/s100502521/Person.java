package a6.s100502521;

public class Person 
{
	 private String name;//名稱
	 private String gender;//性別
	 public Person(String in_name,String in_gender)//初始化
	 {
		 setName(in_name);
		 setGender(in_gender);
	 }
	 public void setName(String input)//設定名稱
	 {
		 name=input;
	 }
	 public void setGender(String input)//設定性別
	 {
		 gender=input;
	 }
	 public String getName()//取得名稱
	 {
		 return name;
	 }
	 public String getGender()//取得性別
	 {
		 return gender;
	 }
	 public String toString()//回傳字串
	 {
		 return "This guy is a person!!";
	 }
	 
}
