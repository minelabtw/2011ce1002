package a6.s100502521;

public class Employee extends Person//繼承Person
{	
	private int salary;//薪水
	private int rank; //等級
	public Employee(String in_name, String in_gender,int in_salary,int in_rank) //初始化
	{
		super(in_name, in_gender);
		setSalary(in_salary);
		setRank(in_rank);
	}
	 public void setSalary(int input)//設定薪水
	 {
		 salary=input;
	 }
	 public void setRank(int input)//設定等級
	 {
		 rank=input;
	 }
	 public int getSalary()//取得薪水
	 {
		 return salary;
	 }
	 public int getRank()//取得等級
	 {
		 return rank;
	 }
	 public String toString()//回傳字串
	 {
		 return "This guy is a Employee!!";
	 }
	 public String toString(boolean check)//還傳字串 是教師  不是學生
	 {
		 if(check)
		 {
			 return toString();
		 }
		 else
		 {
			 this.getClass().getName();
			 return "Though this guy is not a Student, "+super.toString();
		 }
	 }
}
