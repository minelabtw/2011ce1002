package a6.s100502007;

public class Employee extends Person{
	int salary,rank;
	Employee(String Name,String Gender,int Salary,int Rank){
	name=Name;
	gender=Gender;
	salary=Salary;
	rank=Rank;
	}
	public int getSalary(){
		return salary;
	}
	public int getRank(){
		return rank;
	}
	public String toString(){
		return " This guy is a Employee!!";
	}
	public String toString(boolean check){
		if(check==true){
			return toString();
		}
		else{
			return " Though this guy is not a Student,"+super.toString();
		}
	}
}
