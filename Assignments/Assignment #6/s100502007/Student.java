package a6.s100502007;

public class Student extends Person{
	String classroom;
	Student(String Name,String Gender,String Classroom){
		name=Name;
		gender=Gender;
		classroom=Classroom;
	}
	public String getClassRoom(){
		return classroom;
	}
	public String toString(){
		return " This guy is a Student!!";
	}
	public String toString(boolean check){
		if(check==true){
			return toString();
		}
		else{
			return " Though this guy is not a Employee,"+super.toString();
		}
	}
}
