package a6.s995002201;

public class Employee extends Person{
//繼承person
	protected String name;
	protected String gender;
	protected int salary;
	protected int rank = 0;
	protected boolean tf = false;
	protected String rank1;
	public Employee(String nam, String gen, int sal, int ran)
	{
		super(nam,gen,sal,ran);
		name = nam;
		gender = gen;
		salary = sal;
		rank = ran;
	}
	//get方法
	public String getName() 
	{
		return name;
	}
	public String getGender() 
	{
		return gender;
	}
	public int getSalary() 
	{
		return salary;
	}
	//getrank方法辨識等級 並對應顯示
	public String getRank() 
	{
		if(rank==1)
		rank1 = "Professor";
		if(rank==2)
		rank1 = "Associated Professor";
		if(rank==3)
		rank1 = "Assistant Professor";
		if(rank==4)
		rank1 = "Lecturer";
		return rank1;
	}
	public String toString()
	{
		return String.format("This guy is a Employee!!");
	}
	public String toString(boolean b) 
	{
		tf = b;
		if(tf==true)
			return String.format("This guy is a Employee!!");
		if(tf==false)
			return String.format("Though this guy is not a Student, This guy is a Person!!");
		return null;
	}
}
/*some data member salary, rank with get method.
Rank 1 to 3 represents professor, 
associated professor and assistant professor.
toString( ): a override method to show a message 
to say object with this class represents a employee.
toString( boolean check): a overload method
 with a boolean argument to check whether the object 
 is a teacher or not.
  Please use toString( ) method to answer*/