package a6.s995002201;

public class Student extends Person{
	//繼承person

	protected String name;
	protected String gender;
	protected String classroom;
	protected boolean tf = false;//protected定義變數
	public Student(String na, String ge, String cl) 
	{
		super(na,ge,cl);
		name = na;
		gender = ge;
		classroom = cl;
	}
	//get方法
	public String getName() 
	{
		return name;
	}
	public String getGender() 
	{
		return gender;
	}
	public String getClassRoom() 
	{
		return classroom;
	}
	//toString 顯示文字
	public String toString()
	{
		return String.format("This guy is a Student!!");
	}
	public String toString(boolean b) 
	{
		tf = b;
		if(tf==true)
			return String.format("This guy is a Student!!");
		if(tf==false)
			return String.format("Though this guy is not a Employee, This guy is a Person!!");
		return null;
	}
}
/*some data member classroom with get method.
toString( ): a override method to show a message
 to say object with this class represents a student.
toString(boolean check): a overload method
 with a boolean argument to check whether the object 
 is a student or not. 
 Please use toString( ) method to answer.*/