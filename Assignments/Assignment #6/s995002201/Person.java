package a6.s995002201;

public class Person 
{
	protected String name;
	protected String gender;
	protected int salary;
	protected int rank;
	protected String classroom;
	public Person(String n, String g, int s, int r) 
	{
		name = n;
		gender = g;
		salary = s;
		rank = r;
	}
	public Person(String nam, String gen, String cla)
	{
		name = nam;
		gender = gen;
		classroom = cla;
	}
	public String getName()
	{
		return name;
	}
	public String getGender()
	{
		return gender;
	}
	public String toString()
	{
		return String.format(name,gender);
	}
}
/*some data member name, gender with get method
toString(): a method to show message to say object with this class represents a person.*/