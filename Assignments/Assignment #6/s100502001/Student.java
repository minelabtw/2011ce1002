package a6.s100502001;

public class Student extends Person{ //inherit the class Person
	private  String classroom; 
	public Student(String name_stu,String gender_stu,String classNum){
		super(name_stu,gender_stu); //initialize the Person's constructor
		this.classroom=classNum; 
	}
	public String getClassRoom(){ 
		return classroom;
	}
	public String toString(){ //override the supperclass Person's toString()
		return "This is a student.";
	}
	public String toString(boolean check_stu){ //overload
		if(check_stu) //judge whether the class belongs student or not 
			return toString();
		else
			return "Though this is not a employee, "+super.toString();
	}
}
