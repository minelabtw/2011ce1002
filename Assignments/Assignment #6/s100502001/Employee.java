package a6.s100502001;

public class Employee extends Person{ ////inherit the class Person
	private String[] rank;
	private  int salary;
	private int numOfrank;
	public Employee(String name_tea, String gender_tea, int sal, int ran){
		super(name_tea,gender_tea); //initialize the Person's constructor
		rank=new String[4];
		setRank();
		this.salary=sal;
		if(ran>=1&&ran<=3)
			this.numOfrank=ran;
		else
			this.numOfrank=4;
	}
	public void setRank(){ //set rank of teacher
		rank[0]="professor";
		rank[1]="associated professor";
		rank[2]="assistant professor";
		rank[3]=".....><...Wrong choice";
	}
	public int getSalary(){
		return salary;
	}
	public String getRank(){
		return rank[numOfrank-1];
	}
	public String toString(){ //override the supperclass Person's toString()
		return "This is a employee.";
	}
	public String toString(boolean check_tea){ //overload
		if(check_tea)//judge whether the class belongs teacher or not 
			return toString();
		else
			return "Though this is not a student, "+super.toString();
	}
}
