package a6.s100502001;

public class Person {
	protected String name;
	protected String gender;
	public Person(String n,String g){ //store the user name and gender
		name=n;
		gender=g;
	}
	public String getName(){
		return name;
	}
	public String getGender(){
		return gender;
	}
	
	public String toString(){
		return "This is a person.";
	}
}
