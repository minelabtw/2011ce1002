package a6.s100502002;

public class Employee extends Person
{
	protected int salary;
	protected int rank;//設變數
	
	public Employee(String name_input,String gender_input,int salary_input,int rank_input)
	{
		super();
		name = name_input;
		gender = gender_input;
		salary = salary_input;
		rank = rank_input;//初始化變數
	}
	public int getSalary()
	{
		return salary;
	}
	public String getRank()
	{
		/*1 for Professor, 2 for Associated Professor, 3 for Assitant Professor, 4 for Lecturer*/
		if(rank==1)
			return "This guy is a professor";
		else if(rank==2)
			return "This guy is a associated professor";
		else if(rank==3)
			return "This guy is a assitant professor";
		else if(rank==4)
			return "This guy is a lecturer";
		else
			return "no this rank";
	}
	public String toString()//overwrite原本繼承的method
	{
		return "This guy is a Employee!!";
	}
	public String toString(boolean check)//判斷是不是&輸出結果
	{
		if(check)
			return this.toString();
		else if(!check)
		    return "Though this guy is not a student but , " + super.toString();
		else
			return "I don't no what you mean";
		
	}

}
