package a6.s100502002;

public class Student extends Person
{
	protected String classroom;
	public Student(String name_input,String gender_input,String classroom_input)
	{
		super();
		name = name_input;
		gender = gender_input;
		classroom = classroom_input;//初始化變數	
	}
	public String getClassRoom()//取出字串
	{
		return classroom;
	}
	public String toString()//輸出這個CLASS的結果
	{
		return "This guy is a student";
	}
	public String toString(boolean check)//判斷是不是並輸出結果
	{
		if(check)
			return this.toString();
		else if(!check)
			return "Though this guy is not a Employee,but "+super.toString();
		else
			return "I don't know what you mean";
	}
	
}
