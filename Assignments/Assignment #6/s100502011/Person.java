package a6.s100502011;

public class Person {
	protected String name; // user's name
	protected String gender; // user's gender
	
	public Person(String usergender,String username){ // constructor
		gender = usergender;
		name = username;
	}
	
	public String getName(){ // get name
		return name;
	}
	
	public String getGender(){ // get gender
		return gender;
	}

	public String toString(){ // the category
		return "Person";
	}

}
