package a6.s100502011;

public class Student extends Person{
	private String classroom; // user's classroom
	
	public Student(String username,String usergender,String userclassroom){ //constructor
		super(usergender,username); // superclass's constructor
		classroom = userclassroom;
	}
	
	public String toString(){ // category
		return "Student";
	}
	
	public String getClassRoom(){ // get classroom
		return classroom;
	}
	
	public String toString(boolean check){ // category
		if(check) // true
			return "This guy is a "+toString()+"!!";
		else //false
			return "Though this guy is not an Employee, This guy is a"+super.toString()+"!!";
	}

}
