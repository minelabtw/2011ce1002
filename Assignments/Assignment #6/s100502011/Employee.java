package a6.s100502011;

public class Employee extends Person{
	private int salary; // user's salary
	private int rank; // user's rank

	public int getSalary(){ // get salary
		return salary;
	}
	
	public String getRank(){ // get rank (number to category)
		if(rank ==1) // professor
			return "Professor";
		else if(rank ==2) // associated professor
			return "Associated Professor";
		else if(rank ==3) // assistant professor
			return "Assitant Professor";
		else  //lecturer
			return "Lecturer";
	}
	
	public Employee(String username,String usergender,int usersalary,int userrank){ // constructor
		super(usergender,username); // superclass's constructor
		salary = usersalary;
		rank = userrank;
	}
	
	public String toString(){ // category
		return "Employee";
	}
	
	public String toString(boolean check){ // get category
		if(check) // true
			return "This guy is a "+toString()+"!!";
		else // false
			return "Though this guy is not an Student, This guy is a"+super.toString()+"!!\n";
	}

}
