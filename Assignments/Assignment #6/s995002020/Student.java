package a6.s995002020;

public class Student extends Person{
	protected String classroom;//data member
	public Student(String name,String gender,String classroom){//Constructor
		this.name=name;
		this.gender=gender;
		this.classroom=classroom;
	}
	public String toString(){
		return "This guy is a Student!!";
	}
	public String toString(boolean check){
		if(check)
			return "This guy is a Student!!";
		else
			return "Though this guy is not a Employee, This guy is a Person!!";
	}
	public String getClassRoom(){return classroom;}
}