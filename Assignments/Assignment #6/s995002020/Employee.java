package a6.s995002020;

public class Employee extends Person{
	protected int salary;//data member
	protected String rank;
	public Employee(String name,String gender,int salary,int ra){//Constructor
		this.name=name;
		this.gender=gender;
		this.salary=salary;
		switch(ra){
			case 1:
				rank="professor";//professor
				break;
			case 2:
				rank="associated professor";//associated professor
				break;
			case 3:
				rank="assistant professor";//assistant professor
				break;
			default:
				rank="Lecturer";
				break;
		}
	}
	public String toString(){
		return "This guy is an Employee!!";
	}
	public String toString(boolean check){
		if(check)
			return "This guy is an Employee!!";
		else
			return "Though this guy is not a Student, This guy is a Person!!";
	}
	public int getSalary(){return salary;}
	public String getRank(){return rank;}
}