package a6.s995002203;

public class Person {
	private String name, gender;

	Person(String name, String gender){//constructor
		this.name=name;
		this.gender=gender;
	}
	public String getName(){
		return name;
	}
	public String getGender(){
		return gender;
	}
	public String toString(){
		return "This guy is a Person!!";
	}
}
