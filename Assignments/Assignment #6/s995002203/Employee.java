package a6.s995002203;

public class Employee extends Person{//繼承Person
	private int salary, rank;
	Employee(String name,String gender,int salary,int rank){
		super(name,gender);//呼叫Person的constructor
		this.salary=salary;
		this.rank=rank;
	}
	public int getSalary(){
		return salary;
	}
	public String getRank(){//由rank判斷是教授或學生
		if(rank==1)
			return "professor";
		else if(rank==2)
			return "associated professor";
		else
			return  "assistant professor";
	}
	public String toString(){
		return "This guy is a Employee!!";
	}
	public String toString(boolean check){
		if(check)
			return toString();
		else
			return "Though this guy is not a Student, "+super.toString();//呼叫Person的toString()
	}
}
