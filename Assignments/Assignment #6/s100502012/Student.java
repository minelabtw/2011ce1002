package a6.s100502012;

public class Student extends Person{
	private String classroom;
	
	public Student(String d ,String e ,String f){
		name=d;
		gender=e;
		classroom=f;
	}
	
	public String getClassRoom(){
		return classroom;
	}
	
	public String toString(){
		return "This guy is a Student!!";
	}
	
	public String toString(boolean flag){
		if (flag==true)
			return "This guy is a Student!!";
		else
			return "Though this guy is not a Employee, This guy is a Person!!";
	}


}
