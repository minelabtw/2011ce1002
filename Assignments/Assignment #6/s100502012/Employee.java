package a6.s100502012;

public class Employee extends Person{
	private int salary;
	private int rank;
	
	public Employee (String a ,String b,int c,int d){
		name=a;
		gender=b;
		salary=c;
		rank=d;
	}
    	
	public int getSalary(){
		return salary;
	}
	
	public String getRank(){
		
		if (rank==1)
		    return "Professor";
		if (rank==2)
			return "Associated Professor";
		if (rank==3)
			return "Assitant Professor";
		else 
			return "Lecturer";	
	}
	
	public String toString(){
		return "This guy is a Teacher!!";
	}
	
	public String toString(boolean flag){
		if (flag==true)
			return "This guy is a Employee!!";
		else
			return "Though this guy is not a Student, This guy is a Person!!";
	}
}
