package a6.s100502022;

public class Employee extends Person{
	private int salary;
	private int rank;
	private String category;

	public Employee(String name, String gender,int salary,int rank) {
		super(name, gender);
		this.salary=salary;
		this.rank=rank;
		// TODO Auto-generated constructor stub
	}
	public int getSalary(){
		return salary;
	}
	
	public int getRank(){
		return rank;
	}
	
	public String getCategory(){
		switch(rank){
		case 1:
			return String.format("Professor");
		case 2:
			return String.format("Associated Professor");
		case 3:
			return String.format("Assitant Professor");
		case 4:
			return String.format("Lecturer");
		default:
			return null;
	}
		
	}
	
	public String toString(){
		return String.format(getCategory());
	}
	
	public String toString(boolean answer){
		if(answer==true){
			return String.format("This guy is a Employee!!");
		}
		else if (answer==false){
			return String.format("Though this guy is not a Student, This guy is a Person!!\n");
		}
		else 
			return null;
	}

}
