package a6.s100502022;

public class Person {
	protected String name;
	protected String gender;
	
	public Person(String name,String gender){
		this.name=name;
		this.gender=gender;
	}
	
	public String getName(){
		return name;
	}
	
	public String getGender(){
		return gender;
	}
	
	public String toString(){
		return String.format("NAME: "+name+" GENDER: "+gender);
	}

}
