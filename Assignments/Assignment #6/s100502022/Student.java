package a6.s100502022;

public class Student extends Person{
	private String classroom;
	
	public Student(String name, String gender,String classroom) {
		super(name, gender);
		// TODO Auto-generated constructor stub
		this.classroom=classroom;
	}
	
	public String getClassRoom(){
		return classroom;
	}
	
	public String toString(){
		return String.format("This guy is a Student!!");
	}
	
	public String toString(boolean ans){
		if(ans==true){
			return String.format("This guy is a Student!!");
		}
		else if (ans==false){
			return String.format("Though this guy is not a Employee, This guy is a Person!!\n");
		}
		else 
			return null;
	}
	
	
	

}
