package a6.s100502520;

public class Student extends Person {
	public String classroom;
	
	public Student(String username,String usergender,String input){
		super(username,usergender);
		classroom = input;
	}
	
	public String getClassRoom(){
		return classroom;
	}
	
	public String toString(){
		return ("This guy is a Student!!"); 
	}
	
	public String toString(boolean check){
		if(check){
			return toString();
		}
		else{
			return ("Though this guy is not a Studen" + super.toString());
		}
	}
}
