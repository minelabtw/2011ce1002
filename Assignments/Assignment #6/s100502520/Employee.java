package a6.s100502520;

public class Employee extends Person {
	public int salary;
	public int rank;
	public Employee(String username,String usergender,int usersalary,int userrank){
		super(username, usergender);
		salary = usersalary;
		rank = userrank;
	}
	
	public int getSalary(){
		return salary;
	}
	
	public int getRank(){
		return rank;
	}
	
	public  String toString(){
		return ("This guy is a Employee!!");
	}
	
	public String toString( boolean check){
		if(check){
			return toString();
		}
		else{
			return ("Though this guy is not a Employee, "+super.toString());
		}
	}
}
