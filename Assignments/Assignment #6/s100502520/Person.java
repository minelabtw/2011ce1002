package a6.s100502520;

public class Person {
	public String name;
	public String gender;
	
	public Person(String input1,String input2){
		name = input1;
		gender = input2;
	}
	
	public String getName(){
		return name;
	}
	
	public String getGender(){
		return gender;
	}
	
	public String toString(){
		return ("This guy is a Person!!");
	}
}
