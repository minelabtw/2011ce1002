package a6.s100502018;

public class Student extends Person {
	private String classroom;

	Student(String x, String y, String z) { // constructor to set the data
		super(x, y);
		this.classroom = z;
	}

	public String getClassroom() { // return the classroom
		return classroom;
	}

	public String toString() { // an override method to the Student message
		return "This guy is a Student!!!";
	}

	public String toString(boolean x) { // to tell the identity
		if (x == true) {
			return this.toString();
		} else {
			return "Though this guy is not a Employee." + super.toString();
		}
	}
}
