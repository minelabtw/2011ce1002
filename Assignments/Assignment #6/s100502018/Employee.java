package a6.s100502018;

public class Employee extends Person {
	private int salary, rank;

	Employee(String w, String x, int y, int z) { // constructor to set the data
		super(w, x);
		this.salary = y;
		this.rank = z;
	}

	public int getSalary() { // return the salary
		return this.salary;
	}

	public String getRank() { // return the rank
		switch (this.rank) {
		case 1:
			return "Professor";
		case 2:
			return "Associated Professor";
		case 3:
			return "Assitant Professor";
		case 4:
			return "Lecturer";
		default:
			return "";
		}
	}

	public String toString() { // an override method to show the Employee
								// message
		return "This guy is a Employee!!!";
	}

	public String toString(boolean x) { // to tell the identity
		if (x == true) {
			return this.toString();
		} else {
			return "Though this guy is not a Student. " + super.toString();
		}
	}
}
