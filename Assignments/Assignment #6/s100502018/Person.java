package a6.s100502018;

public class Person {
	private String name, gender;

	Person(String x, String y) { // constructor to set the data
		this.name = x;
		this.gender = y;
	}

	public String getName() { // return name
		return this.name;
	}

	public String getGender() { // return gender
		return this.gender;
	}

	public String toString() { // an override(Superclass Object) method to show
								// the Person message
		return "This guy is a Person!!!";
	}
}
