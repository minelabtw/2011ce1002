package a6.s100502018;

import java.util.Scanner;

public class A61 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int choice = 0;
		while (choice != 3) { // run until the choice of user is equal to 3
			System.out
					.print("Please the person you want to create 1.teacher 2.student 3.Exit: ");
			choice = input.nextInt();
			switch (choice) {
			case 1:
				System.out
						.println("Please input the teacher's name, gender, salary");
				System.out
						.println(", and rank(1 for Professor, 2 for Associated Professor, 3 for Assitant Professor, 4 for Lecturer");
				Employee myEmployee = new Employee(input.next(), input.next(),
						input.nextInt(), input.nextInt());
				System.out.println("The teacher's name is "
						+ myEmployee.getName());
				System.out.println("The teacher's gender is "
						+ myEmployee.getGender());
				System.out.println("The teacher's salary is "
						+ myEmployee.getSalary());
				System.out.println("The teacher's rank is "
						+ myEmployee.getRank());
				System.out.println("The category of this guy is: "
						+ myEmployee.toString());
				System.out.println("Is this guy a Employee? "
						+ myEmployee.toString(true));
				System.out.println("Is this guy a Student? "
						+ myEmployee.toString(false));
				break;
			case 2:
				System.out
						.println("Please input the student's name, gender, and classroom number");
				Student myStudent = new Student(input.next(), input.next(),
						input.next());
				System.out
						.println("The student name is " + myStudent.getName());
				System.out.println("The student's gender is "
						+ myStudent.getGender());
				System.out.println("The student's classroom is at "
						+ myStudent.getClassroom());
				System.out.println("The category of this guy is: "
						+ myStudent.toString());
				System.out.println("Is this guy a Student? "
						+ myStudent.toString(true));
				System.out.println("Is this guy a Employee? "
						+ myStudent.toString(false));
				break;
			case 3:
				System.out.println("See you next week~~");
				break;
			default:
				break;
			}
		}
	}

}
