package a6.s100502004;

public class Person {
	String name;
	String gender;
	
	public Person(String newname,String newgender){//a constructor
		name = newname;
		gender = newgender;
	}
	
	public String getName(){
		return name;	
	}
	
	public String getGender(){
		return gender;	
	}
	
	public String toString(){// create the method toString
		return  "This guy is a Person!!";
	}
	
	
}
