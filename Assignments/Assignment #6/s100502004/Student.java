package a6.s100502004;

public class Student extends Person{
	String classroom;
	
	public Student(String newname,String newgender,String classroom1 ){
		super(newname,newgender);	
		classroom=classroom1;
	}
	
	public String getClassRoom(){
		return classroom;
	}
	
	public String toString(){// override the method toString
		return  "This guy is an Student!!";
	}
	
	public String toString(boolean check){//to check the person is Student or not
		if(check){		
			return  "This guy is a Student!!";
		}
		else{
			return  "Though this guy is not a Employee. This guy is a Person!!!!";
		}
	}

	
	
}
