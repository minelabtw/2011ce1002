package a6.s100502004;

public class Employee extends Person{//Employee class extends Person
	int salary;
	int rank;
	
	public Employee(String newname,String newgender, int salary1 , int rank1){//a constructor
		super(newname,newgender);
		salary = salary1;
		rank = rank1;
	}	
	
	public String getRank(){//to check what is the person
		if(rank==1){
			return "Professor";
		}
		else if(rank==2){
			return " Associated Professor ";
		}
		else if(rank==3){
			return "Assistant Professor";
		}
		else{
			return "We don't have this rank.";
		}
	}
	
	public int getSalary(){// override the method toString
		return salary;
	}
	
	public String toString(){
		return  "This guy is an Employee!!";
	}
	
	public String toString(boolean check){// override and create the method toString
		if(check){		
			return  "This guy is a Employee!!";
		}
		else{
			return  "Though this guy is not a Student. This guy is a Person!!";
		}
	}
	
	
	
}
