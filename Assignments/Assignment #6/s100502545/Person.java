package a6.s100502545;

public class Person
{
	public String Name1,Gender1;
	
	//Constructor person 
	public Person(String A,String B)
	{
		Name1 = A;
		Gender1 = B;
	}
	
	//Return user input get name
	public String getName()
	{
		return Name1;
	}
	//return user input get gender
	public String getGender()
	{
		return Gender1;
	}
	
	//Show message
	public String toString()
	{
		return "This guy is a Person!!";
	}
}
