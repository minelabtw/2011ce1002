package a6.s100502545;

public class Employee extends Person 
{
	public int Salary;
	public int Rank;
	
	
	
	public Employee(String a,String b,int c,int d)
	{
		//String a,b extend name1 and gender1 in person.java
		super(a,b);
		
		Salary = c;
		Rank = d;
	
	}
	//return return user input get Salary 
	public int getSalary()
	{
		return Salary;
	}
	
	//return user choose rank
	public String getRank()
	{
		if (Rank == 1)
		{
			return "Professor";
		}
		else if (Rank == 2)
		{
			return "Associated Professor";
		}
		else if (Rank == 3)
		{
			return "Assitant Professor";
		}
		else 
			return "Lecturer";
		
	} 
	
	//Show message
	public String toString()
	{
		return " This guy is a Employee!!";
	}
	//Show message
	public String toString(boolean check)
	{
		if (check == true)
		{
			return this.toString();
		}
		else 
			return "Though this guy is not a Student,"+super.toString();
	}
	
}
