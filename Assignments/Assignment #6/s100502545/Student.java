package a6.s100502545;

public class Student extends Person
{
	public String ClassRoom;
	
	
	public Student(String i,String j,String k)
	{
		//String i,j extend name1 and gender1 in person.java
		super(i,j);
		ClassRoom = k;
	}
	
	//return user input classroom 
	public String getClassRoom()
	{
		return ClassRoom;
	}
	
	//Show message
	public String toString()
	{
		return "This guy is a Student!!";
	}
	
	//Show message
	public String toString(boolean check)
	{
		if (check == true)
		{
			return this.toString();
		}
		else
			return "Though this guy is not a Employee,"+super.toString();
	}
}
