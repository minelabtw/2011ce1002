package a6.s100502511;

public class Person {
	private String name;
	private String gender;

	Person() {

	}

	public Person(String newname, String newgender) {
		name = newname; // 設定name為輸入的newname
		gender = newgender; // 設定gender為輸入的newgender
	}

	public String getName() { // 回傳name
		return name;
	}

	public String getGender() { // 回傳gender
		return gender;
	}

	public String toString() {
		return super.toString();
	}
}
