package a6.s100502511;

public class Student extends Person {
	private String ClassRoom;

	Student() {

	}

	public Student(String name, String gender, String newClassRoom) {
		super(name, gender); // 呼叫Person的name和gender
		this.ClassRoom = newClassRoom; // 設定ClassRoom為輸入的newClassRoom
	}

	public String getClassRoom() { // 回傳ClassRoom
		return ClassRoom;
	}

	public String toString() { // 輸出此字串
		return "This guy is a Student!!";
	}

	public String toString(boolean check) {
		String word = "";
		if (check == true) {
			word = "This guy is a Student!!";
			return word;
		} else if (check == false) {
			word = "Though this guy is not a Employee, This guy is a Person!! ";
			return word;
		} else
			return word;
	}
}
