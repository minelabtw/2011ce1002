package a6.s100502511;

public class Employee extends Person {
	private double salary;
	private double rank;

	Employee() {

	}

	public Employee(String name, String gender, double newsalary, double newrank) {
		super(name, gender); // 呼叫Person的name和gender
		this.salary = newsalary; // 設定salary為輸入的newsalary
		this.rank = newrank; // 設定rank為輸入的newrank
	}

	public double getSalary() { // 回傳salary
		return salary;
	}

	public void getRank() { // 回傳rank名稱
		if (this.rank == 1) {
			System.out.println("Professor");
		} else if (this.rank == 2) {
			System.out.println("Associated Professor");
		} else if (this.rank == 3) {
			System.out.println("Professor");
		} else if (this.rank == 4) {
			System.out.println("Lecturer");
		}
	}

	public String toString() { // 輸出此字串
		return "This guy is a Employee!!";
	}

	public String toString(boolean check) {
		String word = "";
		if (check == true) {
			word = "This guy is a Employee!!";
			return word;
		} else if (check == false) {
			word = "Though this guy is not a Student, This guy is a Person!! ";
			return word;
		} else
			return word;
	}
}
