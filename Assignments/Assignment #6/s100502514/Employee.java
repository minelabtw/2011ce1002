/**
 * class Employee extends Person
 * Required implements and superclass methods omitted here.
 * This header comment only lists the extra points:
 * +getStatus():String overrides the superclass method, in order to change the messages in this class.
 * +getRankNumber():int gets Rank number in Integer 1 to 4; invalid numbers will become -1.
 * +getSalaryInDollar():String returns salary in dollar format.
 */
package a6.s100502514;

public class Employee extends Person {
	private int salary;
	private int rank;
	
	public Employee(String name, String gender, int salary, int rank){
		super(name, gender);
		this.salary = salary;
		this.rank = rank;
	}
	
	public String getStatus(){
		return "Employee";
	}
	
	public int getSalary(){
		return salary;
	}
	public String getSalaryInDollar(){
		return "$"+salary;
	}
	
	public int getRankNumber(){
		if(rank>4||rank<1)return -1;
		return rank;
	}
	public String getRank(){
		switch(rank){
		case 1:
			return "Professor";
		case 2:
			return "Associated Professor";
		case 3:
			return "Assitant Professor";
		case 4:
			return "Lecturer";
		default:
			return "not exist";
		}
	}
	
	public String toString(){
		return super.toString()+" (Actually my teacher)";
	}
	public String toString(boolean check){
		if(check){
			return toString();
		}else{
			return "Though this "+getHowToCallThis()+" is not a Student, "+super.toString();
		}
	}
}
