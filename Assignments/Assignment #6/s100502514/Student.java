/**
 * class Student extends Person
 * Required implements and superclass methods omitted here.
 * This header comment only lists the extra points:
 * +getStatus():String overrides the superclass method, in order to change the messages in this class.
 */
package a6.s100502514;

public class Student extends Person {
	private String room;
	
	public Student(String name, String gender, String room){
		super(name, gender);
		this.room=room;
	}
	
	public String getStatus(){
		return "Student";
	}
	
	public String getClassRoom(){
		return room;
	}
	
	public String toString(){
		return super.toString()+" (Actually my classmate or myself)";
	}
	public String toString(boolean check){
		if(check){
			return toString();
		}else{
			return "Though this "+getHowToCallThis()+" is not an Employee, "+super.toString();
		}
	}
}
