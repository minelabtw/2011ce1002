/**
 * class Person
 * Required implements omitted here; this header comment only lists the extra points:
 * +Person() as the default constructor. (calls +Person("undefined","unknown"), though not used in A61)
 * +getStatus():String is one of the methods that simplify the message generating code.
 * +getHowToCallThis():String determines to return I, you, he or she as subject of a sentence
								, or just a word.
 * +toString(WhoIsSaying, TellWhat):String is the extra version of toString()
 	, where the 2 parameters are enums, defined on the last part in this java file.
 	(toString() just calls toString(WhoIsSaying.HE_OR_SHE, TellWhat.TELL_STATUS))
 * -isFirstLetterVowel(String):boolean returns true if first charater is in a, e, i, o, u.
 */

package a6.s100502514;

public class Person {
	private String name;
	private String gender;
	
	public Person(String name, String gender){
		this.name=name;
		this.gender=gender.toLowerCase().trim(); //lower the case and strip all empty characters
	}
	public Person(){
		this("undefined","unknown"); //The person's name&gender are set unknown.
	}
	
	/** Get the class name as the status name */
	public String getStatus(){
		/** The Polymorphism way crashes the assignment requirements. */
		/*String[] arr = super.toString().split("@")[0].split(".");
		return arr[arr.length-1];*/
		return "Person";
	}
	public String getName(){
		return name;
	}
	public String getGender(){
		return gender;
	}
	public String getHowToCallThis(){
		if(gender.equals("male")||gender.equals("man")||gender.equals("boy")||gender.equals("guy")){
			return "guy";
		}else if(gender.equals("female")||gender.equals("woman")||
				gender.equals("girl")||gender.equals("lady")){
			return "lady";
		}else{
			return "person";
		}
	}
	
	public String toString(WhoIsSaying who, TellWhat what){
		String msg="";
		/** Determines who said the sentence */
		switch(who){
		case I:
			msg+="I am ";
			break;
		case YOU:
			msg+="You are ";
			break;
		case HE_OR_SHE:
			msg+="This "+getHowToCallThis()+" is ";
			break;
		}
		if(what==TellWhat.TELL_STATUS){
			/** Telling the status */
			String status = getStatus();
			/** Determines "a" or "an" */
			if(isFirstLetterVowel(status)){
				msg+="an ";
			}else{
				msg+="a ";
			}
			msg+=status+"!!";
		}else{
			/** Telling the name */
			msg+=name+"!!";
		}
		return msg;
	}
	public String toString(){
		return toString(WhoIsSaying.HE_OR_SHE, TellWhat.TELL_STATUS);
	}
	public static enum WhoIsSaying {I, YOU, HE_OR_SHE, SHORT_WORD};
	public static enum TellWhat {TELL_NAME, TELL_STATUS};
	
	private static boolean isFirstLetterVowel(String word){
		char first_letter = word.charAt(0);
		/** Determines "a" or "an" */
		switch(Character.isUpperCase(first_letter) ? first_letter+'a'-'A' : first_letter){
		case 'a':
		case 'e':
		case 'i':
		case 'o':
		case 'u':
			return true;
		default:
			return false;
		}
	}
}
