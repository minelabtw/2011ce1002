package a6.s100502030;

public class Student extends Person {
	private String classroom;

	public Student(String student_name, String student_gender, String classroom) {
		name = student_name;
		gender = student_gender;
		this.classroom = classroom;
	}

	public String toString() {
		return "This guy is a Student!!";
	}// show message the guy is a student

	public String toString(boolean check) {
		if (check)
			return toString();
		else
			return "This guy is not a Student. " + super.toString();
	}// show message the guy is a student or not

	public String getClassRoom() {
		return classroom;
	}

}
