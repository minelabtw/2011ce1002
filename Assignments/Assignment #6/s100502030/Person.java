package a6.s100502030;

public class Person {
	protected String name;
	protected String gender;

	public Person() {

	}

	public void setName(String name) {
		this.name = name;
	}

	public void setGander(String gender) {
		this.gender = gender;
	}

	public String toString() {
		return "This guy is a Person!!";
	}// show message the guy is a person

	public String getName() {
		return name;
	}

	public String getGender() {
		return gender;
	}
}
