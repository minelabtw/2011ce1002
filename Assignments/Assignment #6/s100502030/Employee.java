package a6.s100502030;

public class Employee extends Person {
	private int salary;
	private int rank;

	public Employee(String employee_name, String employee_gender,
			int employee_salary, int employee_rank) {
		name = employee_name;
		gender = employee_gender;
		salary = employee_salary;
		rank = employee_rank;
	}

	public String toString() {
		return "This guy is a Employee!!";
	}// show message the guy is a employee

	public String toString(boolean check) {
		if (check)
			return toString();
		else
			return "This guy is not a Employee. " + super.toString();
	}// show message the guy is a employee or not

	public int getSalary() {
		return salary;
	}

	public String getRank() {
		if (rank == 1)
			return "Professor";
		else if (rank == 2)
			return "Associated professor";
		else if (rank == 3)
			return "Assistant professor";
		else
			return "Lecturer";
	}
}
