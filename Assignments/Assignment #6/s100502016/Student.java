package a6.s100502016;

public class Student extends Person {
	private String classroom;

	Student(String name, String gender, String classroom) {
		this.name = name;
		this.gender = gender;
		this.classroom = classroom;
	}

	public String toString() {

		return "This guy is a Student!!";
	}

	protected String toString(boolean check) {
		if (check == true)
			return this.toString();
		else
			return "Though this guy is not a Employee. " + super.toString();
	}

	public String getClassRoom() {
		return classroom;
	}
}
