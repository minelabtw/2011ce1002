package a6.s100502016;

public class Employee extends Person {
	private int salary;
	private int rank;

	Employee(String name, String gender, int salary, int rank) {
		this.name = name;
		this.gender = gender;
		this.salary = salary;
		this.rank = rank;
	}

	public String toString() {
		return "This guy is a Employee!!";
	}

	protected String toString(boolean check) {
		if (check == true)
			return this.toString();
		else
			return "Though this guy is not a Student. " + super.toString();
	}

	public String getRank() {
		if (rank == 1)
			return "Professor";
		else if (rank == 2)
			return "Associated Professor";
		else if (rank == 3)
			return "Assitant Professor";
		else
			return "Lecturer";
	}

	public int getSalary() {
		return salary;
	}

}
