package a6.s100502016;

public class Person {
	protected String name;
	protected String gender;

	public String toString() {
		return "This guy is a Person";
	}

	public String getName() {
		return name;
	}

	public String getGender() {
		return gender;
	}

}
