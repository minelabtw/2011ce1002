//Employee.java
package a6.s100502502;
public class Employee extends Person{
	protected int salary;
	protected int rank;
	Employee(String Name, String Gender, int Salary, int Rank){//save user's input
		name = Name;
		gender = Gender;
		salary = Salary;
		rank = Rank;
	}
	public int getSalary(){//return salary
		return salary;
	}
	public String getRank(){//return rank
		if(rank == 1){
			return "professor";
		}
		else if(rank == 2){
			return "associated professor";
		}
		else if(rank == 3){
			return "assistant professor";
		}
		else{
			return "";
		}
	}
	public String toString(){//show a message
		return "This guy is a Employee!!";
	}
	public String toString(boolean check){//check whether the guy is an employee or not 
		if(check == true){
			return this.toString();
		}
		else if(check == false){
			return "Though this guy is not a Student, " + super.toString();
		}
		else{
			return "";
		}
	}
}
