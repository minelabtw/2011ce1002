//Student java
package a6.s100502502;
public class Student extends Person{
	protected String classroom;
	Student(String Name, String Gender, String Classroom){//save user's input
		name = Name;
		gender = Gender;
		classroom = Classroom;
	}
	public String getClassRoom(){//get user's classroom name
		return classroom;
	}
	public String toString(){//show a message
		return "This guy is a Student!!";
	}
	public String toString(boolean check){//check whether the guy is student or not
		if(check == true){
			return this.toString();
		}
		else if(check == false){
			return "Though this guy is not a Employee, " + super.toString();
		}
		else{
			return "";
		}
	}
}
