package a6.s100502034;

public class Student extends Person{
	public String studentClass;
	public Student(String personName, String personGender, String studentClassRoom) {
		super(personName, personGender);
		studentClass = studentClassRoom;//class
	}
	public String getClassRoom(){//get method <~ class
		return studentClass;
	}
	public String toString(){//overrid
		return "This guy is a Student!!";
	}
	
	public String toString(boolean check){//overload
		if (check == true){
			return this.toString();//在這邊特意加了一個this 不加也沒事啦 只是心理上想跟下面來個對比
		}
		else{
			return "Though this guy is not a Employee, " + super.toString();
		}
	}
}
