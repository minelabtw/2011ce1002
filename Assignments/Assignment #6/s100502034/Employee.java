package a6.s100502034;

public class Employee extends Person{
	public int salary;
	public int rank;
	public Employee(String personName, String personGender, int personSalary, int personRank) {
		super(personName, personGender);//就是super姓名跟性別 (話說java自動搞好這句還蠻不錯
		salary = personSalary;//就薪水
		rank = personRank;//職位
	}
	public int getSalary(){//get method <~ salary
		return salary;
	}
	public String getRank(){//get method <~ rank
		switch(rank){//rank的數字是代表不同的職位 在這邊來做個判斷
			case 1:
				return "Professor";
		case 2:
				return "Associated Professor";
			case 3:
				return "Assitant Professor";
			case 4:
				return "Lecturer";
			default://超過正常值的時候總要給個訊息的
				return "!!!! . Sorry, maybe you are enter the wrong number of the rank";
		}
		
	}
	public String toString(){
		return "This guy is a Employee!!";//overrid
	}
	public String toString( boolean check){//overload
		if (check == true){
			return this.toString();//在這邊特意加了一個this 不加也沒事啦 只是心理上想跟下面來個對比
		}
		else{
			return "Though this guy is not a Student" + super.toString();
		}
	}
}
