package a6.s100502034;

public class Person {
	public  String name;//就是名字
	public  String gender;//就是性別
	public Person(String personName, String personGender){
		name = personName;//就是老師跟學生都要用到的名字跟性別
		gender = personGender;
	}
	public String getName(){//get method<~ name
		return name;
	}
	public String getGender(){//get method<~ gender
		return gender;
	}
	public String toString()
	{
		return " This guy is a Person!!\n";//之後給其他兩個class寫super來call的東西
	}
}
