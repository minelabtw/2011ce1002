package a6.s100502024;

public class Person 
{
	private String name,gender; // 設立 private data
	Person (String name,String gender)
	{
		this.name = name;  // 設定名字
		this.gender = gender;  // 設定性別
	}
	public String getName()
	{
		return name;  // 回傳名字
	}
	public String getGender()
	{
		return gender;  // 回傳性別
	}
	public String toString()
	{
		return "This guy is a Person!!";
	}
}
