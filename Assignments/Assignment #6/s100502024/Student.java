package a6.s100502024;

public class Student extends Person  // Student 繼承 Person
{
	private String classroom;  // 設立 private data
	Student(String name,String gender,String classroom)
	{
		super(name,gender);  // 繼承 superclass 裡的 arguments
		this.classroom = classroom;
	}
	public String getClassroom()
	{
		return classroom;
	}
	public String toString()  // overrides Person class 裡的 toString method
	{
		return "This guy is a Student!!";
	}
	public String toString(boolean check)
	{
		if (check == true)  // 判斷
		{
			return "This guy is an Student!!";
		}
		else
		{
			return "Though this guy is not an Employee, This guy is a Person!!";
		}
	}
}
