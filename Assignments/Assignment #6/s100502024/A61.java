package a6.s100502024;
import java.util.Scanner;
public class A61
{
	public static void main (String[] args)
	{
		Scanner input = new Scanner(System.in);
		boolean flag = false;
		while(flag == false)
		{
			System.out.println("Please select the person you want to create: (1)teacher (2)student (3)exit");  // 顯示選項
			int choice = input.nextInt();
			switch(choice)
			{
				case 1:
					System.out.println("Please input the teacher's name,gender,salary");  // 提示輸入
					System.out.println(",and rank(1)for Professor (2)for Associated Professor (3)for Assitant Professor (4)for Lecturer");
					Employee Teacher = new Employee(input.next(),input.next(),input.nextInt(),input.nextInt());
					System.out.println("The teacher's name is " + Teacher.getName());   // call method
					System.out.println("The teacher's gender is " + Teacher.getGender()); 
					System.out.println("The teacher's salary is " + Teacher.getSalary());
					System.out.println("The teacher's rank is " + Teacher.getRank());
					System.out.println("The category of this guy is: " + Teacher.toString());
					System.out.println("Is this guy an Employee? " + Teacher.toString(true));  // 判斷是否為 employee
					System.out.println("Is this guy a Student? " + Teacher.toString(false)+"\n");  // 判斷是否為student
					break;
				case 2:
					System.out.println("Please input the student's name, gender, and classroom number");
					Student student = new Student(input.next(),input.next(),input.next());
					System.out.println("The student name is " + student.getName());
					System.out.println("The student's gender is " + student.getGender());
					System.out.println("The student's classroom is at " + student.getClassroom());
					System.out.println("The category of this guy is: " + student.toString());
					System.out.println("Is this guy a Student? " + student.toString(true));   // 判斷是否為student
					System.out.println("Is this guy an Employee? " + student.toString(false)+"\n"); // 判斷是否為employee 
					break;
				case 3:
					flag = true;  // 跳出迴圈
					break;
			}
		}
	}
}
