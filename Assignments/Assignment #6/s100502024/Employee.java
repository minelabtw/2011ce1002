package a6.s100502024;

public class Employee extends Person  // Employee 繼承 Person
{
	private int salary,rank;  // 設立 private data
	Employee(String name,String gender,int salary,int rank)
	{
		super(name,gender);  // 繼承superclass 的 arguments
		this.salary = salary;
		this.rank = rank;
	}
	public int getSalary()
	{
		return salary;
	}
	public String getRank() 
	{
		if(rank == 1)  // 判別rank
		{
			return "Professor";
		}
		else if(rank == 2)
		{
			return "Associated Professor";
		}
		else if (rank == 3)
		{
			return "Assitant Professor";
		}
		else
		{
			return "Lecturer";
		}
	}
	public String toString()  // overrides Person class 裡的 toString method
	{
		return "This guy is an Employee!!";
	}
	public String toString(boolean check)
	{
		if (check == true)  // 判別
		{
			return "This guy is an Employee!!";
		}
		else
		{
			return "Though this guy is not a Student, This guy is a Person!!";
		}
	}
}
