package a6.s100502014;

public class Employee extends Person {
	private int salary, rank;
	public int getSalary() {
		return salary;
	}
	//Rank 1 to 3 represents professor, associated professor and assistant professor.
	public String getRank() {
		if(rank==1)
			return "Professor";
		else if(rank==2)
			return "Associated professor";
		else
			return "Assistant professor";
	}
	//a override method to show a message to say object with this class represents a employee.
	public String toString( ) {
		return "This guy is a Employee!!";
	}
	// a overload method with a boolean argument to check whether the object is a teacher or not
	public String toString(boolean check) {
		if(check)
			return toString();
		else
			return "Though this guy is not a Student, " + super.toString();
	}
	public Employee(String name, String gender, int salary, int rank) {
		super(name, gender);
		this.salary = salary;
		this.rank = rank;
	}
}
