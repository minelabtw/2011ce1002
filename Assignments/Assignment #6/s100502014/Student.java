package a6.s100502014;

public class Student extends Person {
	private String classroom;
	public String getClassRoom() { 
		return classroom;
	}
	//a override method to show a message to say object with this class represents a student.
	public String toString() {
		return "This guy is a Student!!";
	}
	// a overload method with a boolean argument to check whether the object is a student or not
	public String toString(boolean check) {
		if(check)
			return toString();
		else
			return "Though this guy is not a Employee, " + super.toString();
	}
	public Student(String name, String gender, String classroom) {
		super(name, gender);
		this.classroom = classroom;
	}
}
