package a6.s100502014;

public class Person {
	private String name, gender;
	public String getName() {
		return name;
	}
	public String getGender() {
		return gender;
	}
	//a method to show message to say object with this class represents a person.
	public String toString() {
		return "This guy is a Person!!";				
	}
	public Person(String name, String gender) {
		this.name = name;
		this.gender = gender;
	}
}
