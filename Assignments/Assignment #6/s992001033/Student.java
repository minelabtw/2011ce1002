package a6.s992001033;

public class Student extends Person
{
	protected String classroom = "";
	public Student(String name,String gender,String classroom)//constructor
	{
		this.name = name;
		this.gender = gender;
		this.classroom = classroom;
	}
	public String getClassRoom()//get method
	{
		return classroom;
	}
	public String toString()
	{
		 return "This guy is a Student!!";
	}
	public String toString(boolean check)
	{
		if(check)
			return toString();
		else
			return "Though this guy is not a Employee, "+new Person().toString();//�ΰΦW����
	}
}
