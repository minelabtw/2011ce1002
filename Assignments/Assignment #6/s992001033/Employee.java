package a6.s992001033;

public class Employee extends Person
{
	protected int salary = 0;
	protected int rank = 0;
	public Employee(String name,String gender,int salary,int rank)//constructor
	{
		this.name = name;
		this.gender = gender;
		this.salary = salary;
		this.rank = rank;
	}
	public int getSalary()
	{
		return salary;
	}
	public String getRank()//根據rank值不同，輸出不同
	{
		if(rank==1)
			return "Professor";
		else if(rank==2)
			return "associated professor";
		else 
			return "assistant professor";		
	}
	public String toString()
	{
		 return "This guy is a Employee!!";
	}
	public String toString(boolean check)
	{
		if(check)
			return toString();
		else
			return "Though this guy is not a Student, "+new Person().toString();//用匿名物件
	}
}
