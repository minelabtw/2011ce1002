package a6.s100502501;

public class Student extends Person{ //inherits class Person
	private String classroom;
	
	public Student(String name1,String gender1,String class1){
		name=name1;
		gender=gender1;
		classroom=class1;
	}
	public String getClassRoom(){ //get the classroom
		return classroom;
	}
	public String toString(){ //(override method)show a message to say object with this class represents a student
		return "This guy is a Student!!";
	}
	public String toString(boolean check){ //(overload method)show a message to say object with this class represents a student
		String check1="";
		if(check==true)
			check1=toString();
		else
			check1="Though this guy is not an Employee,"+super.toString()+"\n";;
		return check1;
	}
}
