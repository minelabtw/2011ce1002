package a6.s100502501;

public class Person {
	protected String name;
	protected String gender;
	
	public Person(){
	}
	public String getName(){ //get the name
		return name;
	}
	public String getGender(){ //get the gender
		return gender;
	}
	public String toString(){ //show message to say object with this class represents a person
		return "This guy is a Person!!";
	}
}
