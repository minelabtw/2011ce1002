package a6.s100502501;

public class Employee extends Person{ //inherits class Person
	private int salary,rank;
	
	public Employee(String name1,String gender1,int salary1,int rank1){
		name=name1;
		gender=gender1;
		salary=salary1;
		rank=rank1;
	}
	public int getSalary(){ //get the salary
		return salary;
	}
	public String getRank(){
		String rank2="";
		switch(rank){
			case 1:
				rank2="Professor";
				break;
			case 2:
				rank2="Associated Professor";
				break;
			case 3:
				rank2="Assitant Professor";
				break;
			case 4:
				rank2="Lecturer";
				break;
		}
		return rank2;
	}
	public String toString(){ //(override method)show a message to say object with this class represents a employee
		return "This guy is an Employee!!";
	}
	public String toString(boolean check){//(overload method)check if the object is a teacher
		String check1="";
		if(check==true)
			check1=toString();
		else
			check1="Though this guy is not a Student,"+super.toString()+"\n";
		return check1;
	}
}
