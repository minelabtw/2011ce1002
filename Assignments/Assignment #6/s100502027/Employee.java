package a6.s100502027;

public class Employee extends Person {
	int salary;
	int rank;
	Employee(String Iname,String Igender,int Isalary,int Irank){
		name = Iname ;
		gender = Igender ;
		salary = Isalary ;
		rank = Irank ;
	}
	public int getSalary(){
		return salary;
	}
	public String getRank(){ //Respond the rank to return the represents
		if(rank==1){
			return "Professor";
		}
		else if (rank==2){
			return "Associated Professor";
		}
		else if (rank==3){
			return "Assitant Professor";
		}
		else if (rank==4){
			return "Lecturer";
		}
		else{
			return "error";
		}
	}
	public String toString(){
		return "This guy is a Employee!!";
	}
	public String toString(boolean check){    
		if(check==true){
			return "This guy is a Employee!!" ;
		}
		else{
			return "Though this guy is not a Student, " + super.toString() ;  // output the false message and the super.message
		}
	}
}
