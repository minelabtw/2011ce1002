package a6.s100502027;

public class Student extends Person{
	String classroom;
	Student(String Iname,String Igender,String Iclassroom){
		name = Iname;
		gender = Igender;
		classroom = Iclassroom ;
	}
	public String getClassRoom(){
		return classroom;
	}
	public String toString(){
		return "This guy is a Student!!";
	}
	public String toString(boolean check){
		if(check==true){
			return "This guy is a Student!!";
		}
		else{
			return "Though this guy is not a Employee, " + super.toString();   // output the false message and the super.message
		}
	}
}
