package a6.s100502003;

public class Person {
	protected String name;
	protected String gender;
	
	public Person(String NAME, String GENDER) { // initialize
		name = NAME;
		gender = GENDER;
	}
	
	public String getName() {
		return name;
	}
	
	public String getGender() {
		return gender;
	}
	
	public String toString() { // Show information
		return "This guy is a person.";
	}
}
