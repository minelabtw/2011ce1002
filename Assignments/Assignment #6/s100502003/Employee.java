package a6.s100502003;

public class Employee extends Person{
	protected int salary;
	protected int rank;
	
	public Employee(String Name, String Gender, int Salary, int Rank) { // initialize
		super(Name, Gender); // define the super constructor
		name = Name;
		gender = Gender;
		salary = Salary;
		rank = Rank;
	}
	
	public int getSalary() {
		return salary;
	}
	
	public String getRank() {
		if (rank == 1)
			return "Professor";
		else if (rank == 2)
			return "Associated Professor";
		else if (rank == 3)
			return "Assitant Professor";
		else if (rank == 4)
			return "Lecturer";
		else
			return "Not defined!";
	}
	
	public String toString() { // override
		return "This guy is an Employee!!";
	}
	
	public String toString(boolean check) { // overload
		if(check) {
			return "This guy is an Employee!!";
		}
		else {
			return "Though this guy is not a Student, this guy is a Person!!";
		}
	}
}
