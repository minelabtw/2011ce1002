package a6.s100502003;

public class Student extends Person{
	private String classroom;
	
	public Student(String Name, String Gender, String Classroom) {
		super(Name, Gender); // define the super constructor
		name = Name;
		gender = Gender;
		classroom = Classroom;
	}
	
	public String getClassroom() {
		return classroom;
	}
	
	public String toString() { // override
		return "This guy is a Student!!";
	}
	
	public String toString(boolean check) { // overload
		if(check) {
			return "This guy is a Student!!";
		}
		else {
			return "Though this guy is not an Employee, this guy is a Person!!";
		}
	}
}
