package a6.s100502544;

public class Student extends Person{//子class
	
	public Student(String name, String gender, String aclassRoom) {//接收傳進的三個質
		super(name, gender);//引用父class的內容
		// TODO Auto-generated constructor stub
		setclassRoom(aclassRoom);//收到的 aclassroom給setclassroomy作初始化
	}


	String classRoom;
	public void setclassRoom(String ClassRoom){
		classRoom=ClassRoom;
	}
	public String getclassRoom(){
		return classRoom;
	}
	
	
	public String toString(){
		return "This guy is a Student!!";
	}
	public String toString(boolean check){
		if(check==true)
			return toString();//==true到toString method回傳字串
		else//回傳另一個字串
			return "Though this guy is not a Employee, This guy is a Person!!";
	}
}
