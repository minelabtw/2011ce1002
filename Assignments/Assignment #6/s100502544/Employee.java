package a6.s100502544;

public class Employee extends Person{//子class
	private int salary;
	private int rank;
	public Employee(String name, String gender,int asalary,int arank) {//接收四個資料 由main傳來
		super(name, gender);//引用父class的資料
		// TODO Auto-generated constructor stub
		setsalary(asalary);//把傳進的質給setsalary作初始化
		setrank(arank);//把傳進的質給setrank作初始化
	}
	
	
	public void setsalary(int Salary){
		salary=Salary;
	}
	public int getsalary(){
		return salary;
	}
	
	public void setrank(int Rank){
		rank=Rank;
	}
	public String getrank(){//設一個回傳String 的資料給main
		if(rank==1){
			return "Professor";
		}
		else if(rank==2){
			return "Associated Professor";
		}
		else if(rank==3){
			return "Assitant Professor";
		}
		else
			return "Lecturer";
	}//四種不同的資料  給1 2 3 4 分別有不同的回傳字串
	public String toString(){
		return "This guy is a Employee!!";
	}//一個METHOD可以讓她回傳字串
	public String toString(boolean check){//overload的method  傳進boolean的質
		if(check==true)//判斷傳進的質  true 或  false
		return toString();
		else
			return"Though this guy is not a Student, This guy is a Person!!";
	}
	
}

