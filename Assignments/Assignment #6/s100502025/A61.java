package a6.s100502025;

import java.util.Scanner;

public class A61 {
	public static void main(String[] args){		
		
		Scanner input = new Scanner(System.in);		
		
		boolean exitFlag = false;  //判斷是否繼續執行while迴圈
		
		while(exitFlag == false){			
			System.out.println("Please the person you want to create 1.teacher 2.student 3.Exit: ");  //選擇是老師還是學生，或是選擇離開		
			int userChoice = input.nextInt();  // user choice for creating object			
			switch(userChoice){				
				case 1:  // new a Employee object, and initial it					
					System.out.print("Please input the teacher's name, gender, salary");  //輸入老師名字、性別、薪水			
					System.out.println(", and rank(1 for Professor, 2 for Associated Professor, 3 for Assitant Professor, 4 for Lecturer)");  //這老師是教授、副教授、助理教授還是講師					
					Employee teacher = new Employee( input.next(), input.next(), input.nextInt(), input.nextInt() );  // use methods to answer the questions					
					System.out.println("The teacher's name is " + teacher.getName());  //輸出老師名字
					System.out.println("The teacher's gender is " + teacher.getGender());  //輸出老師性別		
					System.out.println("The teacher's salary is " + teacher.getSalary());  //輸出老師薪水						
					System.out.println("The teacher's rank is " + teacher.getRank());  //輸出老師級別					
					System.out.println("The category of this guy is: " + teacher.toString());					
					System.out.println("Is this guy a Employee? " + teacher.toString(true));  //他是員工					
					System.out.println("Is this guy a Student? " + teacher.toString(false));  //他不是學生					
					break;				
				case 2:  // new a Student object, and initial it					
					System.out.println("Please input the student's name, gender, and classroom number");  //輸入學生名字、性別以及教室				
					Student student = new Student( input.next(), input.next(), input.next() );  // use methods to answer the questions	
					System.out.println("The student name is " + student.getName());  //輸出名字
					System.out.println("The student's gender is " + student.getGender());  //輸出性別			
					System.out.println("The student's classroom is at " + student.getClassRoom());  //輸出教室				
					System.out.println("The category of this guy is: " + student.toString());  //輸出他的分類	
					System.out.println("Is this guy a Student? " + student.toString(true));  //他是學生
					System.out.println("Is this guy a Employee? " + student.toString(false));  //他不是員工		
					break;				
				case 3:  //離開			
					exitFlag = true;  //不執行while迴圈			
					System.out.println("See you next week~~");					
					break;				
				default:					
					break;			
			}		
		}	
	}		
}
