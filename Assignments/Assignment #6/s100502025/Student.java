package a6.s100502025;

public class Student extends Person {  //繼承class Person
	
	private String classroom;

	Student(String Name, String Gender , String Classroom) {
		super(Name, Gender);  //繼承class Person的constructor
		setClassRoom(Classroom);	
	}
	
	public void setClassRoom(String Classroom) {    //將班級存入private中
		classroom = Classroom;
	}
	
	public String getClassRoom() {
		return classroom;
	}
	
	public String toString() {  //回傳他是學生
		return "This guy is a Student!!" ;
	}
	
	public String toString(boolean check) {  //他是否為學生
		if(check == true) {
			return this.toString();
		}
		else {
			return "Though this guy is not an Employee , " + super.toString();
		}
	}
}
