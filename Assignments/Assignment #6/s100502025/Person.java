package a6.s100502025;

public class Person {
	
	protected String name , gender;  //將名稱，性別設成private
	
	Person(String Name , String Gender) {  //constructor
		setName(Name);  
		setGender(Gender);
	}
	
	public void setName(String Name) {  //將名稱存入private中
		name = Name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setGender(String Gender) {  //將性別存入private中
		gender = Gender;
	}
	
	public String getGender() {
		return gender;
	}
	
	public String toString() {
		return "This guy is a Person!!" ;
	}
}
