package a6.s100502025;

public class Employee extends Person {  //繼承class Person  
	
	private int salary , rank;  //將薪水，等級設成private

	Employee(String Name, String Gender,int Salary , int Rank) {  //constructor
		super(Name, Gender);  //繼承class Person的constructor
		setSalary(Salary);
		setRank(Rank);
	}
	
	public void setSalary(int Salary) {    //將薪水存入private中
		salary = Salary;
	}
	
	public int getSalary() {
		return salary;
	}
	
	public void setRank(int Rank) {  //將等級存入private中
		rank = Rank;
	}
	
	public String getRank() {
		if(rank == 1) {
			return "Professor";
		}
		else if(rank == 2) {
			return "Associated Professor";
		}
		else if(rank == 3) {
			return "Assitant Professor";
		}
		else {
			return "Lecturer";
		}
	}
	
	public String toString() {  //回傳他的稱呼	
			return "This guy is an Employee!!";	
	}
	
	public String toString(boolean check) {  //他是否為員工
		if(check == true) {
			return this.toString() ;
		}
		else {
			return "Though this guy is not a Student , " + super.toString();
		}
	}
}
