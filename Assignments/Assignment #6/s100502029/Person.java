package a6.s100502029;

public class Person {
	private String name;
	private String gender;
	Person() {
		
	}
	
	// set name
	public void setName(String name) {
		this.name = name;
	}
	
	// get name
	public String getName() {
		return name;
	}
	
	// set gender
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	// get gender
	public String getGender() {
		return gender;
	}
	
	// display message that the guy is a person
	public String toString() {
		return "This guy is a Person!!";
	}
}
