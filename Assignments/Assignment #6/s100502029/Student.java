package a6.s100502029;

public class Student extends Person {
	private String classroom;
	
	// constructoe to initialize variables
	Student(String name, String gender, String classroom) {
		this.classroom = classroom;
		setName(name);
		setGender(gender);
	}
	
	// get classromm number
	public String getClassRoom() {
		return classroom;
	}
	
	// display message that guy is student
	public String toString() {
		return "This guy is a Student!!";
	}
	
	// check whether the object is a student or not
	public String toString(boolean check) {
		if (check)
			return toString();
		else
			return "Though this guy is not an employee, " + super.toString();
	}
}
