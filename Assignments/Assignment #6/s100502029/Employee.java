package a6.s100502029;

public class Employee extends Person {
	private int salary;
	private int rank;
	
	// constructor to initialize variables
	Employee(String name, String gender, int salary, int rank) {
		this.salary = salary;
		this.rank = rank;
		setName(name);
		setGender(gender);
	}
	
	// get salary
	public int getSalary() {
		return salary;
	}
	
	// display the rank of teacher
	public String getRank() {
		if (rank == 1)
			return "Professor";
		else if (rank == 2)
			return "Associated Professor";
		else if (rank == 3)
			return "Assitant Professor";
		else
			return "Lecturer";
	}
	
	// display message that guy is an employee
	public String toString() {
		return "This guy is an employee!!";
	}
	
	// check whether the object is a teacher or not
	public String toString(boolean check) {
		if (check)
			return toString();
		else
			return "Though this guy is not a Student, " + super.toString();
	}
}
