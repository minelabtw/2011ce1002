package a6.s100502510;

public class Employee extends Person {
	protected int salary;
	protected int rank;

	public Employee(String Name, String Gender, int Salary, int Rank) {// 繼承Person加上Salary跟Rank
		super(Name, Gender);
		salary = Salary;
		rank = Rank;
	}

	public int getSalary() {
		return salary;
	}

	public String getRank() {// 判斷rank所代表的職位
		String rank2 = "";// 初始化才能return
		if (rank == 1) {
			rank2 = "Professor";
		} else if (rank == 2) {
			rank2 = "Associated Professor";
		} else if (rank == 3) {
			rank2 = "Associated Professor";
		} else if (rank == 4) {
			rank2 = "Lecturer";
		}
		return rank2;
	}

	public String toString() {// override
		String message2 = "This guy is a Employee!!";
		return message2;

	}

	public String toString(boolean judgement) {// overload
		String message3 = "";
		if (judgement == true) {
			message3 = "this guy is a Employee!!";

		} else if (judgement == false) {
			message3 = "Though this guy is not a Student, " + super.toString();// 使用person裡的message

		}
		return message3;
	}

}
