package a6.s100502510;

public class Student extends Person {
	String classroom;

	public Student(String Name, String Gender, String classroomnumber) {// „[¤Jclassroomnumber
		super(Name, Gender);
		classroom = classroomnumber;

	}

	public String getClassRoom() {
		return classroom;
	}

	public String toString() {// override
		String message3 = "This guy is a Student!!";
		return message3;
	}

	public String toString(boolean judgement) {// overload
		String message4 = "";
		if (judgement == true) {
			message4 = "this guy is a Student!!";

		} else if (judgement == false) {
			message4 = "Though this guy is not a Employee, " + super.toString();

		}
		return message4;
	}

}
