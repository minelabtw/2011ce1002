package a6.s100502510;

public class Person {
	protected String name;
	protected String gender;
	protected String message = "This guy is a Person!!";;// 使用到Person的toString()所顯示出的message

	public Person(String Name, String Gender) {
		name = Name;
		gender = Gender;
	}

	public String getName() {
		return name;
	}

	public String getGender() {
		return gender;
	}

	public String toString() {
		return message;
	}
}
