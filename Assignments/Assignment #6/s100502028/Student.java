package a6.s100502028;

public class Student extends Person {
	private String classroom;
	
	// Constructor to take user's input as argument and initialize
	public Student(String nameOfInput, String genderOfInput, String classroomOfInput){
		name = nameOfInput;
		gender = genderOfInput;
		classroom = classroomOfInput;
	}
	
	// Get method to return classroom name
	public String getClassRoom(){ 
		return classroom;
	}
	
	// An override method to show a message to say object with this class represents a student
	public String toString(){ 
		return "This guy is a Student!!";
	}
	
	// A overload method with a boolean argument to check whether the object is a student or not
	public String toString(boolean check){
		if(check == true)
			return "This guy is a Student!!";
		else
			return "Though this guy is not an Employee, " + super.toString();
	}
} // End class Student
