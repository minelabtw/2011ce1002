package a6.s100502028;

public class Employee extends Person {
	private int salary,rank;
	
	// Constructor to take user's input as argument and initialize
	public Employee(String nameOfInput, String genderOfInput, int salaryOfInput, int rankOfInput){
		name = nameOfInput;
		gender = genderOfInput;
		salary = salaryOfInput;
		rank = rankOfInput;
	}
	
	// Get method to return the salary
	public int getSalary(){
		return salary;
	}
	
	// Method to get the rank by taking user's input as argument
	public String getRank(){
		if(rank == 1)
			return "Professor";
		else if(rank == 2)
			return "Associated Professor";
		else if(rank == 3)
			return "Assitant Professor";
		else
			return "Lecturer";
	}
	
	// A override method to show a message to say object with this class represents a employee
	public String toString(){ 
		return "This guy is an Employee!!";
	}
	
	// A overload method with a boolean argument to check whether the object is a teacher or not
	public String toString(boolean check){
		if(check == true)
			return "This guy is an Employee!!";
		else
			return "Though this guy is not a Student, " + super.toString();
	}
} // End class Employee
