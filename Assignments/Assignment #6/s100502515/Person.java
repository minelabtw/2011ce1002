package a6.s100502515;

public class Person {

	protected String name;
	protected String gender;
	protected int salary;
	
	public void setName(String nAME) {
		name = nAME;
	}
	
	public void setGender(String gENDER) {
		gender = gENDER;
	}
	
	public void setSalary(int sALARY) {
		salary = sALARY;
	}
	
	public String getName() {
		return name;
	}
	
	public String getGender() {
		return gender;
	}
	
	public int getSalary() {
		return salary;
	}
	
	public String toString() {
		return "This guy is a person!!";
	}	
	
}
