package a6.s100502515;

public class Student extends Person {
	private String classroomnumber;
	
	Student(String nAME, String gENDER, String classRoomNumber) {
		name = nAME;
		gender = gENDER;
		classroomnumber = classRoomNumber;
	}
	
	public String getClassRoom() {
		return classroomnumber;
	}
	
	public String toString() {
		return "This guy is a Student!!";
	}
	
	public String toString(boolean check) {
		if(check == true)
			return "This guy is a Student!!";
		else
			return "Though this guy is not an Employee, This guy is a Person!!";
	}
}
