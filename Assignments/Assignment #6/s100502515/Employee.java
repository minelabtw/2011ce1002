package a6.s100502515;

public class Employee extends Person {
	private int rank;
	
	Employee(String nAME,String gENDER,int sALARY,int rANK) {
		name = nAME;
		gender = gENDER;
		salary = sALARY;
		rank = rANK;
	}
	
	public void setRank() {
		
	}
	
	public String getRank() {
		if(rank == 1)
			return "Professor";
		else if(rank == 2)
			return "Associated Professor";
		else if(rank == 3)
			return "Assitant Professor";
		else if(rank == 4)
			return "Lecturer";
		else
			return "Error";
	}
	
	public String toString() {
		return "This guy is an employee!!";
	}
	
	public String toString(boolean check) {
		if(check == true)
			return "This guy is an Employee!!";
		else
			return "Though this guy is not a Student, This guy is a Person!!";
	}
}
