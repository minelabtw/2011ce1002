package a6.s100502009;

public class Person {
	protected String name,gender;
	
	Person(String n,String g)//constructor to take the name and gender in
	{
		setNameGender(n,g);
	}
	
	public void setNameGender(String na,String ge)//to initialize the name and gender
	{
		name=na;
		gender=ge;
	}
	
	public String getName()//to show the name
	{
		return name;
	}
	
	public String getGender()//to show the gender
	{
		return gender;
	}
	
	public String toString()//to say about the person
	{
		return "This guy is a person!!";
	}

}
