package a6.s100502009;

public class Employee extends Person{
	protected int salary,rank;	

	public Employee(String name,String gender,int s,int r)//constructor to take the user's input in
	{
		super(name,gender);
		setSalaryRank(s,r);
	}
	
	public void setSalaryRank(int sal,int ran)//to initialize the salary and the rank
	{
		salary=sal;
		rank=ran;
	}
	
	public int getSalary()//to show the salary
	{
		return salary;
	}
	
	public String getRank()//to show the rank
	{
		if(rank==1)
		{
			return "Professor";
		}
		else if(rank==2)
		{
			return "Associated Professor";
		}
		else
			return "Assitant Professor";
	}
	
	public String toString()//to talk about the person
	{
		return "This guy is a Employee!!";
	}
	
	public String toString(boolean check)//to check if the person is a student or not
	{
		String represent="";
		if(check==true)
		{
			represent+=toString();
		}
		else
		{
			represent+="Though this guy is not a Student, "+super.toString()+"\n";
		}
		return represent;		
	}

}
