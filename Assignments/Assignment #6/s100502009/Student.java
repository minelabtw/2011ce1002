package a6.s100502009;

public class Student extends Person{
	String classroom;
	
	Student(String name,String gender,String cr)//to take the user's input in
	{
		super(name,gender);
		setClassRoom(cr);
	}
	
	public void setClassRoom(String room)//to initialize the classroom
	{
		classroom=room;
	}
	
	public String getClassRoom()//to show the classroom
	{
		return classroom;
	}
	
	public String toString()//to talk about the person
	{
		return "This guy is a Student!!";
	}
	
	public String toString(boolean check)//to check if the person is an employee or not 
	{
		String represent="";
		if(check==true)
		{
			represent+=toString();
		}
		else
		{
			represent+="Though this guy is not an Employee, "+super.toString()+"\n";
		}
		return represent;
	}

}
