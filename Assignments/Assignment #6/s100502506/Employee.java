package a6.s100502506;

public class Employee extends Person													//Inherit
{
	private int salary;
	private int rank;
	public Employee(String inputName, String inputGender,int inputSalary,int inputRank) //constructor 
	{
		super(inputName, inputGender);													//initialize Inherit constructor
		salary=inputSalary;
		rank=inputRank;
	}
	public int getSalary()																//return salary
	{
		return salary;
	}
	public String getRank()																//return rank
	{
		String[] status={"Professor","Associated Professor","Assitant Professor","Lecturer"};
		return status[rank-1];
	}
	public String toString()															//return message
	{
		String messageString="This guy is a Employee!!";
		return messageString;
	}
	public String toString(boolean check)												//return message with input boolean
	{
		if (check) 																		//true
		{
			String messageString="This guy is a Employee!!";
			return messageString;
		}
		else 																			//false
		{
			String messageString="Though this guy is not a Student, This guy is a Person!!";
			return messageString;
		}
	}

}
