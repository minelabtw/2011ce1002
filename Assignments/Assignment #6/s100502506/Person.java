package a6.s100502506;
public class Person 
{
	private String name;
	private String gender;
	
	public Person(String inputName,String inputGender)	//constructor    initialize
	{
		name=inputName;
		gender=inputGender;
	}
	public String getName()								//return name
	{
		return name;
	}
	public String getGender()							//return gender
	{
		return gender;
	}
}
