package a6.s100502506;

public class Student extends Person												//Inherit
{
	private String classroom;
	public Student(String inputName, String inputGender,String inputClass) 		//constructor
	{
		super(inputName, inputGender);											//initialize inherit constructor
		classroom=inputClass;
	}
	public String getClassRoom()												//return classroom
	{
		return classroom;
	}
	public String toString() 													//return message
	{
		String messageString="This guy is a Student!!";
		return messageString;
	}
	public String toString(boolean check)										//return message with input
	{
		if(check)																//true
		{
			String messageString="This guy is a Student!!";
			return messageString;
		}
		else																	//false
		{
			String messageString="Though this guy is not a Employee, This guy is a Person!!";
			return messageString;
		}
		
	}
}
