package a6.s100502505;

public class Employee extends Person {
	Employee(String name, String gender, int salary, int rank)//繼承
	{
		super(name, gender);
		Salary = salary;
		Rank = rank;
	}

	public int getSalary()//getSalary
	{
		return Salary;
	}
	
	public String getRank()//getRank,不同等級有不同的職位
	{
		String message = "";
		if(Rank == 1)
		{
			message = "Professor";
		}else if(Rank == 2)
		{
			message = "Associated Professor";
		}else if(Rank == 3)
		{
			message = "Assistant Professor";
		}else if(Rank == 4)
		{
			message = "Lecturer";
		}
		return message;
	}
	
	public String toString()//Message
	{
		String message = "This guy is a Employee!!";
		return message;
	}
	
	public String toString(boolean check)//判斷是否為Employee
	{
		String message = "";
		if (check==true) 
		{
			message = "This guy is a Employee!!";
		}else if(check==false) 
		{
			message = "Though this guy is not a Student, " + Message;
		}
		return message;
	}
	
	protected int Salary;
	protected int Rank;
}
