package a6.s100502013;

public class Person {
	protected String name;
	protected String gender;
	
	Person(){ //default constructor
		
	}
	
	Person(String a,String b){ //constructor: initialize
		name = a;
		gender = b;
	}
	
	public String getName(){
		return name;
	}
	
	public String getGender(){
		return gender;
	}
	
	public String toString(){ //output: is Person
		return "This guy is a Person!!";
	}
}
