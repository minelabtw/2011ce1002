package a6.s100502013;

public class Employee extends Person{
	private int salary;
	private int rank;
	
	Employee(){ //default constructor
		
	}
	
	Employee(String a,String b,int c,int d){ //constructor: initialize
		name = a;
		gender = b;
		salary = c;
		rank = d;
	}
	
	public int getSalary(){
		return salary;
	}
	
	public String getRank(){ //according to the rank to return different job rank
		if(rank==1)
			return "Professor";
		else if(rank==2)
			return "Associated Professor";
		else if(rank==3)
			return "Assitant Professor";
		else
			return "Lecturer Shih";
	}
	
	public String toString(){ //override:  is Employee
		return "This guy is a Employee!!";
	}
	
	public String toString(boolean check){ //overload: check
		if(check==true)
			return toString();
		else
			return "Though this guy is not a Student, " + super.toString();
	}
}
