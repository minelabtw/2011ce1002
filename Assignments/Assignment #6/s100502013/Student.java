package a6.s100502013;

public class Student extends Person{
	private String classroom;
	
	Student(){ //default constructor
		
	}
	
	Student(String a,String b,String c){ //constructor: initialize
		name = a;
		gender = b;
		classroom = c;
	}
	
	public String getClassRoom(){
		return classroom;
	}
	
	public String toString(){ //override:  is Student
		return "This guy is a Student!!";
	}
	
	public String toString(boolean check){ //overload: check
		if(check==true)
			return toString();
		else
			return "Though this guy is not a Employee, " + super.toString();
	}
}
