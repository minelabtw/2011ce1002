package a6.s995002056;

public class Person {
	String name;
	String gender;
	
	Person(String n,String g){
		name = n;
		gender = g;
	}
	
	public String getName(){
		return name;
	}
	
	public String getGender(){
		return gender;
	}
	public String toString(){
		return "This guy is a Person!!";
	}
}
