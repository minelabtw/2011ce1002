package a6.s995002056;

public class Student extends Person{
	String cla;
	public Student(String n , String g , String c){
		super(n,g);							//繼承 將名字與性別輸入到person
		cla = c;
	}
	public String getClassRoom(){
		return cla;
	}
	public String toString(boolean a){
		if(a)
			return "This guy is a Student!!";
		else
			return "Though this guy is not a Employee," + toString();
	}
	
	public String toString(){				//override
		return "This guy is a Student!!";
	}
}
