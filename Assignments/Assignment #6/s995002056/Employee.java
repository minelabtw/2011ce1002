package a6.s995002056;

public class Employee extends Person {
	int salary;
	int ch;
	
	public Employee(String n,String g,int s , int c){
		super(n,g);					//繼承 將名字與性別輸入到person
		this.salary = s;
		this.ch = c;
	}
	public int getSalary(){
		return salary;
	}
	
	public String getRank(){
		if(ch == 1)					//1 for Professor, 2 for Associated Professor, 3 for Assitant Professor, 4 for Lecturer
			return "Professor";
		else if (ch == 2)
			return "Associated Professor";
		else if (ch == 3)
			return "Assitant Professor";
		else if (ch == 4)
			return "Lecturer";
		else
			return "error";
	}
	public String toString(boolean a){
		if(a)
			return "This guy is a Employee!!";
		else
			return "Though this guy is not a Student," + toString();
	}
	public String toString(){		//override
			return "This guy is a Employee!!";
	}
}
