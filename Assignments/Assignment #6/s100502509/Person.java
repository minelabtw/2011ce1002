package a6.s100502509;

public class Person {
	protected String name;
	protected String gender;

	Person(String name, String gender) {
		this.name = name;
		this.gender = gender;

	}

	public String getName() {//function to get name
		return name;
	}

	public String getGender() {//function to get gender
		return gender;
	}
	
	public String toSTring(){//Function to show Message
		return "This guy is a person!!";
		
	}



}
