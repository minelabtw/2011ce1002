package a6.s100502509;

public class Student extends Person{//class Student extends Person
	String userClassroomNumber;
	
	public Student(String name, String gender, String room){
		super(name,gender);
		
		userClassroomNumber=room;
	}
	
	public String getClassRoom()
	{
		return userClassroomNumber;
	}
	
	public String toString(){//Function to show Message
		return "This guy is a Student!!";
	}
	
	public String toString( boolean check ){//Function to check 
		if(check==true){
			return this.toString();
		}
		else
			return "Though this guy is not a Employee" + super.toSTring();
			
		
	}
	

}
