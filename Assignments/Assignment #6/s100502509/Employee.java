package a6.s100502509;

public class Employee extends Person{//class Employee extends Person
	
	int userChoose,userSalary;
	public Employee(String name, String gender, int salary, int choose){
		super(name,gender);
		userChoose=choose;
		userSalary=salary;
	}
	
	
	public String getRank(){//function to get Rank
		
		if(userChoose==1){
			return "Professor";
		}
		
		else if(userChoose==2){
			return "Associated Professor";
		}
		
		else if(userChoose==3){
			return "Assitant Professor";
		}
			
					
		else{
			return "Lecturer";
		}
	}
	
	public int getSalary(){//function to get Salary
			return userSalary;
		}
	
	public String toString(){//Function to show Message
		return "This guy is a Employee!!";
	}
	
	public String toString( boolean check ){//Function to check 
		if(check==true){
			return this.toString();
		}
		else
			return "Though this guy is not a Student" + super.toSTring();
			
		
	}

}
