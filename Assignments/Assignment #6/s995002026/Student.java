package a61.s995002026;

public class Student extends Person{
	private String classroom;
	
	Student(String n,String g,String c){				//constructor Student
		super(n,g);							//constructor Person
		classroom=c;
	}
	
	public String getClassRoom(){						//get classroom
		return classroom;
	}
	
	public String toString(){									//overwrite toString
		return "This guy is a Student!!";
	}
	
	public String toString(boolean check){
		if(check==true)
			return "This guy is a Student!!";
		
		else
			return "Though this guy is not a Employee"+super.toString();				//super.toString()  call toSting() which in person
	}
}
