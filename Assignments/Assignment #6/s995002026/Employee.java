package a61.s995002026;

public class Employee extends Person {
	private int salary;
	
	private int rank;
	Employee(String n,String g,int s,int r){			//constructor Employee
		super(n,g);						//constructor Person
		
		salary=s;
		
		rank=r;
	}
	
	public int getSalary(){					//get salary
		return salary;
	}
	
	public String  getRank(){							//get rank
		if(rank==1)
			return "Professor";
		
		else if(rank==2)
			return "Associated Professor";
		
		else if(rank==3)
			return "Assitant Professor";
		
		else if(rank==4)
			return "Lecturer";
		
		else
			return "wrong choice";
	}
	
	public String toString(){								//overwrite toString
		return "This guy is a Employee!!";
	}
	
	public String toString(boolean check){
		if(check==true)
			return "This guy is a Employee!!";
		
		else
			return "Though this guy is not a Student"+super.toString();				//super.toString()  call toSting() which in person
	}
}
