package e7.s995002026;

public class Megabank {
	int id;

	double balance;

	double annuallnterestRate;

	Megabank(double rate){
	id=995002026;
	balance=50360.00;
	
	annuallnterestRate=rate;
	}
	
	protected void setInterestRate(double rate){
		annuallnterestRate=rate;
	}


	public double getInterestRate(){
		return annuallnterestRate; // return the rate
	}
	public double getBalance(){
	
		return balance; //return balance
	}

	int getId(){ //return ID
		return id;
	}


	public double calculateInterest(int input){
	
		double interest;
	
	
		int numberofinstallments=input;
	
		interest= getBalance() * annuallnterestRate * numberofinstallments; // calculate the Interest money
	
		return interest;
	}

	public double calculateResult(int input){
	
		double result;
	
	
		int numberofinstallments=input;
	
		result=getBalance()*(1+annuallnterestRate * numberofinstallments);

	
		return result;					//calculate the result money
	}

}
