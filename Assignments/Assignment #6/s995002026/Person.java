package a61.s995002026;

public class Person {
	private String name,gender;
	Person(String n,String g){						//constructor person
		name  = n;
		
		gender = g;
	}
	
	public String getName(){							//get name
		return name;
	}
	
	public String getGender(){						//get gender
		return gender;
	}
	
	public String toString(){
		return ",This guy is a person!!\n";
	}
}
