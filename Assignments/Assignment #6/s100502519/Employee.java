package a6.s100502519;

public class Employee extends Person{

	 private int salary;			//private data
	 private int rank;
	 
	 public Employee(String input_name,String input_gender,int input_salary,int input_rank){
		setName(input_name);			//person的setName
		setGender(input_gender);			//person的setGender
		 salary = input_salary;
		 rank = input_rank;
	 }

	 public int getSalary(){
		return salary; 			//回傳salary
	 }
	 
	 public String getRank(){
		 switch(rank){			/*選擇rank並依據輸入回傳*/
		 	case 1:
		 		return "professor";
		 	case 2:
		 		return "Associated Professor";
		 	case 3:
		 		return "Assitant Professor";
		 	case 4:
		 		return "Lecturer";
		 	default:
		 		return "Error you should only choose 1~4"; 
		 }
	 }
	 
	 public String toString(){
		 return "This guy is a Employee!!";			//override
	 }
	 
	 public String toString(boolean input_TorF){			/*overload*/
		 if(input_TorF == true){
			 return "This guy is a Employee!!";
		 }
		 else{
			 return "Though this guy is not a Student," + super.toString();			//用Person的toString
		 }
	 }

}
