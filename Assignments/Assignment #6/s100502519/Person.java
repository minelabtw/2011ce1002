package a6.s100502519;

public class Person {

	 private String name;			//private data
	 private String gender;
	 
	 public void setName(String input_name){
		 name = input_name;			//接收input並存入
	 }
	 
	 public void setGender(String input_gender){
		 gender = input_gender;			//接收input並存入
	 }
	 
	 public String getName(){
		 return name;			//回傳name
	 }
	 
	 public String getGender(){
		 return gender;			//回傳gender
	 }
	 
	 public String toString(){
		return "This guy is a Person!!";			//回傳string
		 
	 }
	 
}
