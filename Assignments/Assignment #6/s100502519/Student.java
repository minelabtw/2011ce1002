package a6.s100502519;

public class Student extends Person{

	String classroom;			//private data;
	
	public Student(String input_name,String input_gender,String input_classroom){
		setName(input_name);			//person��setName
		setGender(input_gender);			//person��setGender
		classroom = input_classroom;
	 }
	
	public String getClassRoom(){
		return classroom;			//�^��classroom
	}
	
	 public String toString(){			//override
		 return "This guy is a Student!!";
	 }

	 public String toString(boolean input_TorF){			/*overload*/
		 if(input_TorF == true){
			 return "This guy is a Student!!";
		 }
		 else{
			 return "Though this guy is not a Employee," + super.toString();
		 }
	 }
	 
}
