package a6.s100502513;

public class Student extends Person{
	protected String classroom;
	
	Student(){ //dafault
	}
	
	Student(String name, String gender, String classroom){  //輸入名字、性別、教室
		super(name, gender);  //繼承PERSON的建構元
		this.classroom = classroom;
	}

	public String getClassRoom(){  //回傳教室
		return classroom;
	}
	
	public String toString(){  //回傳一段話
		return "This guy is a Student!!";
	}
	
	public String toString(boolean check){  //檢查是否為Student
		if(check == true)
			return this.toString();
		else  //不是Student
			return "Though this guy is not a Employee," + super.toString();
	}
}
