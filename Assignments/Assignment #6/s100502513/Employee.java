package a6.s100502513;

public class Employee extends Person{
	protected int salary;
	protected int rank;

	Employee(){  //dafault
	}
	
	Employee(String name, String gender, int salary, int rank){  //輸入姓名、性別、薪資、等級
		super(name,gender);  //繼承PERSON的建構元
		this.salary = salary;
		this.rank = rank;
	}
	
	public int getSalary(){  //回傳薪資
		return salary;
	}
	
	public String getRank(){  //回傳等級
		if(rank == 1)
			return "Professor";
		else if(rank == 2)
			return "Associated Professor";
		else if(rank == 3)
			return "Assitant Professor";
		else if(rank == 4)
			return "Lecturer";
		else 
			return "Wrong!";
	}
	
	public String toString(){  //回傳一段話
		return "This guy is a Employee!!";
	}
	
	public String toString(boolean check){  //檢查是否為EMPLOYEE
		if(check == true)
			return this.toString();
		else  //不是EMPLOYEE
			return "Though this guy is not a Student," + super.toString();
	}
}
