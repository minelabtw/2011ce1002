package a6.s100502513;

public class Person {
	protected String name;  
	protected String gender;
	
	Person(){  //dafault
	}
	
	Person(String name, String gender){  //輸入名字與性別
		this.name = name;
		this.gender = gender;
	}
	
	public String getName(){  //回傳名字
		return name;
	}
	
	public String getGender(){  //回傳性別
		return gender;
	}
	
	public String toString(){  //回傳一段話
		return " This guy is a Person!!";
	}
}


