package a6.s100502503;

//extends Person class
public class Employee extends Person{
	
	protected int Salary, Rank;
	
	//constructor
	public Employee(String name, String gender, int salary, int rank)
	{
		super(name, gender);
		setSalary(salary);
		setRank(rank);
	}
	
	public void setSalary(int salary)//set salary
	{
		Salary = salary;
	}
	
	public int getSalary()//get salary
	{
		return Salary;
	}
	
	public void setRank(int rank)//set rank
	{
		Rank = rank;
	}
	public String getRank()//check which rank did user input, and show the number standard
	{
		if(Rank == 1)
			return "Professor";
		else if(Rank == 2)
			return "Associated Professor";
		else if(Rank == 3)
			return "for Assitant Professor";
		else
			return "???";
		
	}
	
	public String toString()//show out the message that overrides Person 
	{
		return "This guy is a Employee!!";
	}
	
	public String toString(boolean flag)//show out the message that overloads Person
	{
		if(flag == true)
			return "This guy is a Employee!!";
		else
			return"Though this guy is not a Student, This guy is a Person!!";
	}
}
