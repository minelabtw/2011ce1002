package a6.s100502503;

//extends form Person
public class Student extends Person {
	
	protected String ClassRoomNumber;

	//constructor
	public Student(String name, String gender, String classroom)
	{
		super(name, gender);
		setClassRoom(classroom);
	}
	
	//set classroom number
	public void setClassRoom(String classroom)
	{
		ClassRoomNumber = classroom;
	}
	
	//get classroom number
	public String getClassRoom()
	{
		return ClassRoomNumber;
	}
	
	//show out the message that overrides Person 
	public String toString()
	{
		return "This guy is a Employee!!";
	}
	
	//show out the message that overloads Person
	public String toString(boolean flag)
	{
		if(flag == true)
			return "This guy is a Student!!";
		else
			return"Though this guy is not a Employee, This guy is a Person!!";
	}
}
