package a6.s100502503;

public class Person {

	protected String Name, Gender;//The data member that only can be used in the same package
	
	public Person(String name, String gender)//constructor
	{
		setName(name);
		setGender(gender);
	}
	
	public void setName(String name)//set the name
	{
		Name = name;
	}
	
	public String getName()//get the name
	{
		return Name;
	}
	
	public void setGender(String gender)//set the gender
	{
		Gender = gender;
	}
	
	public String getGender()//get the gender
	{
		return Gender;
	}
	
	public String toString()//the function to show out the message
	{
		return "The guy is a person!";                                                          
	}
}
