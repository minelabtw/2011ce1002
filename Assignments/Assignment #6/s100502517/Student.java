package a6.s100502517;

public class Student extends Person{
	private String Classroomnumber;
	
	Student(String name, String gender, String classroom){//Constructor
		super(name, gender);
		setClassroom(classroom);
	}
	
	public void setClassroom(String classroominput){//store into the private data
		Classroomnumber = classroominput;
	}
	
	public String getClassRoom(){
		return Classroomnumber;
	}
	
	public String toString(){//override
		return "This guy is a Student~!!!";
	}
	
	public String toString(boolean check){//overload
		if(check){
			return "This guy is a Student~!!!";
		}
		else{
			return "Though this guy is not a Employee, "+super.toString();
		}
	}
	
}
