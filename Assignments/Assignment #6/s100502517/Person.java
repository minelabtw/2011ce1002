package a6.s100502517;

public class Person {
	protected String name;
	protected String gender;
	
	Person(String namein, String genderin){//Constructor
		setName(namein);
		setGender(genderin);
	}
	
	public void setName(String nameinput){//store into private data
		name = nameinput;
	}
	
	public String getName(){
		return name;
	}
	
	public void setGender(String genderinput){//store into private data
		gender = genderinput;
	}
	
	public String getGender(){
		return gender;
	}
	
	public String toString(){
		return "This guy is a person~!!!";
	}
}
