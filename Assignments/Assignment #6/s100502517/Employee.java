package a6.s100502517;

public class Employee extends Person {
	private int salary;
	private int rank;
	
	Employee(String name, String gender, int salaryin, int rankin){//Constructor
		super(name, gender);
		setSalary(salaryin);
		setRank(rankin);
	}
	
	public void setSalary(int salaryinput){//store into the private data
		salary = salaryinput;
	}
	
	public int getSalary(){
		return salary; 
	}
	
	public void setRank(int rankinput){//store into the private data
		rank = rankinput;
	}
	
	public String getRank(){//state
		switch(rank){
			case 1:
				return "Professor";
			case 2:
				return "Associated Professor";
			case 3:
				return "Assitant Professor";
			case 4:
				return "Lecturer";
			default:
				return "Wrong number~!!!";			
		}
	}
	
	public String toString(){//override
		return "This guy is a Employee~!!!";
	}
	
	public String toString(boolean check){//overload
		if(check){
			return "This guy is a Employee~!!!";
		}
		else{
			return "Though this guy is not a Student, "+super.toString();
		}
	}
	

}
