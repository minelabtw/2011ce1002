package a6.s985003038;

public class Employee extends Person {
	private int salary;
	private int rank;
	
	public Employee(String newName, String newGender, int newSalary, int newRank){
		super(newName, newGender);							// parent's constructor
		salary = newSalary;
		rank = newRank;
	}
	
	public void setSalary(int newSalary){					// set salary method
		if(salary > 0)
			salary = newSalary;								// salary should be greater than 0
		else
			salary = 0;										// if wrong input detected, use default salary 
	}
	
	public void setRank(int newRank){						// set rank method
		if(rank > 0 && rank < 4)
			rank = newRank;									// rank range in 1 to 3
		else
			rank = 3;										// if wrong input detected, use default rank
	}
	
	public int getSalary(){									// get salary method
		return salary;
	}
	
	public String getRank(){								// get rank method
		if(rank == 1)
			return "Professor";								// rank 1 for professor
		else if(rank == 2)
			return "Associated Professor";					// rank 2 for associated professor
		else
			return "Assitant Professor";					// rank 3 for assitant professor
	}
	
	public String toString(){								// a override method to show a message to say object with this class represents a employee
		return "This guy is a Employee!!";
	}
	
	public String toString(boolean check){					// a overload method with a boolean argument to check whether the object is a teacher or not
		if(check == true)
			return "This guy is a Employee!!";				// if input is true, show employee message
		else
			return "Though this guy is not a Student, " + super.toString();
	}														// otherwise, show person message
}
