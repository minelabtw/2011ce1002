package a6.s985003038;

public class Person {
	private String name;
	private String gender;
	
	Person(String newName, String newGender){						// constructor
		name = newName;
		gender = newGender;
	}
	
	public void setName(String newName){							// set name method
		name = newName;
	}
	
	public void setGender(String newGender){						// set gender method
		gender = newGender;
	}
	
	public String getName(){										// get name method
		return name;
	}
	
	public String getGender(){										// get gender method
		return gender;
	}
	
	public String toString(){										// a method to show message to say object with this class represents a person
		return "This guy is a Person!!";
	}
}
