package a6.s985003038;

public class Student extends Person {
	private String classroom;
	
	public Student(String newName, String newGender, String newClassroom){
		super(newName, newGender);									// parent's constructor
		classroom = newClassroom;
	}
	
	public void setClassRoom(String newClassroom){					// set classroom method
		classroom = newClassroom;
	}
	
	public String getClassRoom(){									// get classroom method
		return classroom;
	}
	
	public String toString(){										// a override method to show a message to say object with this class represents a student
		return "This guy is a Student!!";
	}
	
	public String toString(boolean check){							// a overload method with a boolean argument to check whether the object is a student or not
		if(check == true)
			return "This guy is a Student!!";						// if input is true, show employee message
		else
			return "Though this guy is not a Employee, " + super.toString();
	}																// otherwise, show person message
}
