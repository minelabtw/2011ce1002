package a6.s100502508;

public class Person 
{
	private String Name;
	private String Gender;
	
	public Person(String name,String gender)//initialize name and gender
	{
		Name=name;
		Gender=gender;
	}
	
	public String getName()//get the name
	{
		return Name;
	}
	
	public String getGender()//get the gender
	{
		return Gender;
	}

	public String toString()//show message to say object with this class represents a person.
	{
		return "This guy is a Person!!";
	}
}
