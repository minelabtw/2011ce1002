package a6.s100502508;

public class Student extends Person
{
	private String ClassRoom;
	
	public Student(String name,String gender,String classroom)//initialize name,gender and classroom
	{
		super(name, gender);////call last class of constructor 
		ClassRoom=classroom;
	}
	
	public String getClassRoom()//get the classroom
	{
		return ClassRoom;
	}
	
	public String toString()// an override method to show a message to say object with this class represents a student
	{
		return "This guy is a Student!!";
	}
	
	public String toString(boolean flag)//an overload method with a boolean argument to check whether the object is a student or no
	{
		if(flag==true)
			return "This guy is a Student!!";
		else 
			return "Though this guy is not a Employee, "+super.toString();//call last class of toString method
	}
}
