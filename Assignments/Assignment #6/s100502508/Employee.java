package a6.s100502508;

public class Employee extends Person
{
	private int Salary;
	private int Rank;
	
	public Employee(String name,String gender,int salary,int rank)//initialize name,gender,salary and rank
	{
		super(name,gender);//call last class of constructor 
		Salary=salary;
		Rank=rank;
	}
	
	public int getSalary()//get the salary
	{
		return Salary;
	}
	
	public String getRank()//Rank 1 to 3 represents professor, associated professor and assistant professor
	{
		if(Rank==1)
			return "Professor";
		else if(Rank==2)
			return "Associated Professor";
		else if(Rank==3)
			return "Assitant Professor";
		else 
			return "Lecturer";
	}
	
	public String toString()//an override method to show a message to say object with this class represents an employee
	{
		return "This guy is a Employee!!";
	}
	
	public String toString(boolean flag)//an overload method with a boolean argument to check whether the object is a teacher or not
	{
		if(flag==true)
			return "This guy is a Employee!!";
		else 
			return "Though this guy is not a Student, "+super.toString();//call last class of toString method
	}
}
