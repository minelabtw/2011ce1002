package a6.s100502516;

public class Employee extends Person {
	private int salary;
	private int rank;	
	
	public Employee(String name, String gender, int salary, int rank)
	{
		super(name, gender);
		this.salary = salary;
		this.rank = rank;
	}
	
	public int getSalary()
	{
		return salary;
	}
	
	public String getRank()//to correspond the profession
	{
		switch(rank)
		{
			case 1:
				return "Professor";				
			case 2:
				return "Associated Professor";				
			case 3:
				return "Assitant Professor";				
			case 4:
				return "Lecturer";				
			default:
				return "No this rank!!";				
		}
	}
	
	public String toString()//show the message when this guy is a teacher
	{
		return "This guy is an Employee!!";
	}
	
	public String toString(boolean check)//if it's not, tell user this guy is a person
	{
		if(check)
			return toString();
		else 
			return "Though this guy is not a Student, " + super.toString();
	}
}
