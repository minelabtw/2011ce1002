package a6.s100502516;

public class Student extends Person {
	private String classroom;
	
	public Student(String name, String gender, String classroom)
	{
		super(name, gender);
		this.classroom = classroom;
	}
	
	public String getClassRoom()
	{
		return classroom;
	}
	
	public String toString()//show the message when this guy is a student
	{
		return "This guy is a Student!!";
	}
	
	public String toString(boolean check)//if it's not, tell user this guy is a person
	{
		if(check)
			return toString();
		else
			return "Though this guy is not an Employee, " + super.toString();
	}
}
