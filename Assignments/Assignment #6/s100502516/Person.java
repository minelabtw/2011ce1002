package a6.s100502516;

public class Person {
	protected String name;
	protected String gender;
	
	public Person(String name, String gender)	
	{
		this.name = name;
		this.gender = gender;
	}	

	public String getName()
	{
		return name;
	}
	
	public String getGender()
	{
		return gender;
	}
	
	public String toString()//show the message when this guy is a person
	{
		return "This guy is a Person!!";
	}
}
