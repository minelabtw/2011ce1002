package a6.s975002502;

public class Employee extends Person {
	protected int salary;
	protected int rank;
	
	public Employee() {}
	
	/** constructor with arguments to initial data member */
	public Employee( String name, String gender, int salary, int rank ){
		this.name = name;
		this.gender = gender;
		this.salary = salary;
		this.rank = rank;
	}
	
	/** getSalary(): Return the Employee's salary */
	public int getSalary() {
	    return salary;
	}
	
	/** getRank(): Return the Employee's rank */
	public int getRank() {
	    return rank;
	}
	
	/** toString( ): an override method to show a message to say object with this class represents an employee */
	public String toString() {
		return "This guy is a Employee!!";
	}
	
	/** toString( boolean check): an overload method with a boolean argument to check whether the object is a teacher or not */
	public String toString(boolean check) {
		if(check == true) {
			return this.toString();
		}
		else {
			return "Though this guy is not a Student, " + super.toString();
		}
	}
}
