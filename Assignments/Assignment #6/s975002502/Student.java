package a6.s975002502;

public class Student extends Person {
	protected String classroom;
	
	public Student() {}
	
	/** constructor with arguments to initial data member */
	public Student( String name, String gender, String classroom ){
		this.name = name;
		this.gender = gender;
		this.classroom = classroom;
	}
	
	/** getClassRoom(): Return the Student's classroom */
	public String getClassRoom() {
	    return classroom;
	}
	
	/** toString( ): an override method to show a message to say object with this class represents a student */
	public String toString() {
		return "This guy is a Student!!";
	}
	
	/** toString(boolean check): an overload method with a boolean argument to check whether the object is a student or not */
	public String toString(boolean check) {
		if(check == true) {
			return this.toString();
		}
		else {
			return "Though this guy is not a Employee, " + super.toString();
		}
	}
}
