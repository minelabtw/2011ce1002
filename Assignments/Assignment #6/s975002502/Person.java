package a6.s975002502;

public class Person {
	protected String name;
	protected String gender;
	
	public Person() {}
	
	/** getName(): Return the person's name */
	public String getName() {
	    return name;
	}
	
	/** getGender(): Return the the person's gender */
	public String getGender() {
	    return gender;
	}
	
	/** toString(): a method to show message to say object with this class represents a person */
	public String toString() {
		return "This guy is a Person!!";
	}
}
