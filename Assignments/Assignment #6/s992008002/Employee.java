package a6.s992008002;

public class Employee extends Person{

	private int salary;
	private int rank;
	
	public Employee(String Name,String Gender,int Salary,int Rank){
		
		super(Name,Gender);//�~��Person���غc��
		salary = Salary;
		rank = Rank;
		
	}
	
	
	public int getSalary(){
		return salary;
	}
	
	//�P�_Rank
	//1 for Professor, 2 for Associated Professor, 3 for Assitant Professor, 4 for Lecturer
	public String getRank(){
		if (rank == 1) return ("Professor");
		else if (rank == 2) return ("Associated Professor");
		else if (rank == 3) return ("Assitant Professor");
		else return ("Lecturer");
		
	}
	

	public String toString()
	{
		return String.format("This guy is a Employee!!");
	}
	
	//overload toString ���L�P�_
	public String toString(boolean check){
		if(check == true) return ("This guy is a Employee!!");
		else return ("Though this guy is not a Student, This guy is a Person!!"); 
	}
	
	
	
}
