package a6.s992008002;

public class Person {
	
	private String name; 
	private String gender;


	//建構式
	public Person(String Name, String Gender){
		name = Name;
		gender = Gender;		
	}
	


	//回傳名字
	public String getName(){
		return name;
	}
	
	//回傳性別
	public String getGender(){
		return gender;
	}
	
	

	//回傳敘述
	public String toString()
	{
		return String.format("This guy is a Person!!");
	}
	
	
	
	

}
