package a6.s100502015;

public class Student extends Person {
	private String classroom;

	public Student(String name, String gender, String classroom) {//constructor
		super(name, gender);
		this.classroom = classroom;
	}

	public String getClassRoom() {
		return classroom;
	}

	public void setClassRoom(String classroom) {
		this.classroom = classroom;
	}

	public String toString() {
		return "this guy is a student";
	}

	public String toString(boolean check) {//result
		if (check) {
			return "This guy is a Student!!";
		} else {
			return "Though this guy is not a Employee " + super.toString();
		}
	}
}
