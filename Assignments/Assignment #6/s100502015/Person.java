package a6.s100502015;

public class Person {//constructor
	private String name = "";
	private String gender = "";

	public Person() {
		this("tony", "male");
	}

	public Person(String name, String gender) {
		this.name = name;
		this.gender = gender;
	}

	public String getName() {
		return name;
	}

	public String getGender() {
		return gender;
	}

	public void setName(String input) {
		name = input;
	}

	public void setGender(String input) {
		gender = input;
	}
	public String toString(){//result
		return "this guy is a person";
	}

}
