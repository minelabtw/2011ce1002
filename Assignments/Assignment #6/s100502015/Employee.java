package a6.s100502015;

public class Employee extends Person {//inherit person 
	private int rank, salary;

	public Employee(String emname, String emgender, int salary, int rank) {//constructor
		super(emname, emgender);
		this.rank = rank;
		this.salary = salary;
	}
	public String getRank() {//employee rank
		if (rank == 1) {
			return "professor";
		} 
		else if (rank == 2) {
			return "associated professor";
		}
		else {
			return "assistant professor";
		}
	}

	public int getSalary() {
		return salary;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public String toString() {
		return "this guy is a employee!!";
	}

	public String toString(boolean check) {//result 
		if(check){
			return "this guy is a employee!!";
		}
		else{
			return "this guy is not a student " + super.toString(); 
		}

	}
}
