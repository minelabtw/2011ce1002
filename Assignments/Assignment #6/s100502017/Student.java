package a6.s100502017;

public class Student extends Person{
	private String ClassRoom;
	public Student(String name,String gender,String classRoom){//a constructor to get the data
		Name=name;
		Gender=gender;
		ClassRoom=classRoom;
	}
	public String getClassRoom(){
		return ClassRoom;
	}
	public String toString(){//an override method to show a message 
		return "This guy is a Student!!";
	}
	public String toString(boolean check){//an overload method with a boolean argument to check whether the object is a student or not
		if(check==true)
			return this.toString();
		else
			return "Though this guy is not a Employee!!, "+super.toString();
	}
}
