package a6.s100502017;

public class Employee extends Person{
	private int Salary,Rank;
	public Employee(String name,String gender,int salary,int rank){//a constructor to get the data
		Name=name;
		Gender=gender;
		Salary=salary;
		Rank=rank;
	}
	public int getSalary(){
		return Salary;
	}
	public String getRank(){//use if to determined what professor is
		if(Rank==1)
			return "professor";
		else if(Rank==2)
			return "associated professor";
		else
			return "assistant professor";
	}
	public String toString(){//an override method to show a message 
		return "This guy is a Employee!!";
	}
	public String toString(boolean check){//an overload method with a boolean argument to check whether the object is a teacher or not. 
		if(check==true)
			return this.toString();
		else
			return "Though this guy is not a Student!!, "+super.toString();
	}
}
