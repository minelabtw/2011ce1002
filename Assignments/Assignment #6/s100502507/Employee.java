package a6.s100502507;

public class Employee extends Person {//In addition to name and gender, employee includes salary and rank
	Employee( String Name, String Gender, int Salary, int Rank ) {
		super(Name, Gender);
		salary = Salary;
		if(Rank==1) {//Transform the input number to the corresponding profession
			rank = "Professor";
		}
		else if(Rank==2) {
			rank = "Associated Professor";
		}
		else if(Rank==3) {
			rank = "Assistant Professor";
		}
		else {
			rank = "Lecturer";
		}
	}
	
	public String toString() {//Show the information of itself
		return "This guy is a Employee!!";
	}
	
	public String toString(boolean type) {//Show the information of itself
		if(type) {
			return "This guy is a Employee!!";
		}
		else {
			return "Though this guy is not a Student, This guy is a Person!!";
		}
	}
	
	public int getSalary() {//Return salary
		return salary;
	}
	
	public String getRank() {//Return profession
		return rank;
	}
	
	private int salary;
	private String rank;
}
