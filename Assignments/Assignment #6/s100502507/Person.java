package a6.s100502507;
public class Person {
	Person() {}
	Person(String Name, String Gender) {
		name = Name;
		gender = Gender;
	}
	public String toString() {
		return "Though this guy is not a Student, This guy is a Person!!";
	}
	public String getGender() {
		return gender;
	}
	public String getName() {
		return name;
	}
	private String gender;
	private String name;
}