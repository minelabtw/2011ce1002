package a6.s100502507;

public class Student extends Person { //In addition to name and gender, student includes another information - classroom
	Student( String Name, String Gender, String Classroom ) {
		super(Name, Gender);
		classroom = Classroom;
	}
	
	public String toString() {//Show the information of itself
		return "This guy is a Student!!";
	}
	
	public String toString(boolean type) {//Show the information of itself
		if(type) {
			return "This guy is a Student!!";
		}
		else {
			return "Though this guy is not a Employee, This guy is a Person!!";
		}
	}
	
	public String getClassRoom() {//Return classroom
		return classroom;
	}
	
	private String classroom;
}
