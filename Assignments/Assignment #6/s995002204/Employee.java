package a6.s995002204;

public class Employee extends Person{
	
	private int salary;
	private int rank;
	
	//constructor
	Employee(String input_name,String input_gender,int input_salary,int input_rank)
	{
		name = input_name;
		gender = input_gender;
		salary = input_salary;
		rank = input_rank;
	}
	
	//get variable salary
	public int getSalary()
	{
		return salary;
	}
	
	//get variable rank
	public String getRank()
	{
		if(rank==1)
			return "Professor";
		else if(rank==2)
			return "Associated Professor";
		else if(rank==3)
			return "Assitant Professor";
		else
			return "Lecturer";
	}
	
	//show a message to say object with this class represents an employee.
	public String toString()
	{
		return "This guy is a Employee!!";
	}
	
	//check whether the object is a teacher or not
	public String toString(boolean input)
	{
		if(input==true)
			return this.toString();
		else
			return "Though this guy is not a Student, "+super.toString();
	}
}
