package a6.s995002204;

public class Person {
	
	protected String name;
	protected String gender;
	
	//constructor
	Person()
	{
		
	}
	
	//get variable name
	public String getName()
	{
		return name;
	}
	
	//get variable gender
	public String getGender()
	{
		return gender;
	}
	
	//a method to show message to say object with this class represents a person
	public String toString()
	{
		return "This guy is a Person!!";
	}
}
