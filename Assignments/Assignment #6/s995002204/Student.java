package a6.s995002204;

public class Student extends Person{
	
	String classroom;
	
	//constructor
	Student(String input_name,String input_gender,String input_classroom)
	{
		name = input_name;
		gender = input_gender;
		classroom = input_classroom;
	}
	
	//get variable classroom
	public String getClassRoom()
	{
		return classroom;
	}
	
	// show a message to say object with this class represents a student.
	public String toString()
	{
		return "This guy is a Student!!";
	}
	
	//check whether the object is a student or not.
	public String toString(boolean input)
	{
		if(input==true)
			return this.toString();
		else
			return "Though this guy is not a Employee, "+super.toString();
	}
}
