package a6.s100502033;

public class Employee extends Person
{
	private static int salary;
	private static int rank;
	public Employee(String name1 , String gender1 , int salary1 , int rank1)
	{
		super(name1 , gender1);//將name1,gender1傳回給PERSON
		salary = salary1;
		rank = rank1;
	}
	public static String getName()
	{
		return name;//讀取PERSON的name
	}
	public static String getGender()
	{
		return gender;//讀取PERSON的gender
	}
	public static int getSalary()
	{
		return salary;
	}
	public static String getRank() 
	{
		if(rank == 1)
		{
			return "Professor";
		}
		else if(rank == 2)
		{
			return "Associated Professor";
		}
		else if(rank == 3)
		{
			return "Assitant Professor";
		}
		else if(rank == 4)
		{
			return "Lecturer";
		}
		else
		{
			return "ERROR";
		}
	}
	public String toString()
	{
		return "This guy is a Employee!!";
	}
	public String toString(boolean check)
	{
		if ( check == true)
		{
			return toString();
		}
		else if (check == false)
		{
			return String.format("Though this guy is not a Student , %s\n" , super.toString());//讀取PERSON的toString
		}
		else
		{
			return "ERROR";
		}
	}
	
}
