package a6.s100502033;

public class Student extends Person
{
	private static String classnumber;
	public Student(String name1 , String gender1 , String classnumber1)
	{
		super(name1 , gender1);//將name1,gender1傳回給PERSON
		classnumber = classnumber1;
	}
	public static String getName()
	{
		return name;//讀取PERSON的name
	}
	public static String getGender()
	{
		return gender;//讀取PERSON的gender
	}
	public static String getClassRoom()
	{
		return classnumber;
	}
	public String toString()
	{
		return "This guy is a Student!!";
	}
	public String toString(boolean check)
	{
		if ( check == true)
		{
			return toString();
		}
		else if (check == false)
		{
			return String.format("Though this guy is not a Employee , %s \n" , super.toString());//讀取PERSON的toString
		}
		else
		{
			return "ERROR";
		}
			
	}
}
