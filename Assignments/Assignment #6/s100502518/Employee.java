package a6.s100502518;

public class Employee extends Person{//�~��
	private int salary;
	private int rank;
	
	public Employee(String n,String g,int s,int r)
	{
		salary=s;
		rank=r;
		setGender(g);
		setName(n);
	}
	
	public int getSalary()
	{
		return salary;
	}
	
	public String getRank() 
	{
		if(rank==1)
		{
			return ("Professor");
		}
		
		else if(rank==2)
		{
			return ("Associated Professor");
		}
		
		else if(rank==3)
		{
			return ("Assitant Professor");
		}
		
		else
		{
			return ("Lecturer");
		}
	}
	
	public String toString()//override Person toString 
	{
		return ("This guy is a Employee!!");
	}
	
	public String toString(boolean a)//another
	{
		if(a==true)
		{
			return ("This guy is a Employee!!");
		}
		else
		{
			return ("Though this guy is not a Student, "+super.toString());//��super�I�s�~�Ӫ�toString
		}
	}
}
