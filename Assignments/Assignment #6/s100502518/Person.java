package a6.s100502518;

public class Person {
	private String name;
	private String gender;
	
	public void setName(String a) 
	{
		name=a;
	}
	
	public void setGender(String a)
	{
		gender=a;
	}
	
	public String getName() 
	{
		return name;
	}

	public String getGender()
	{
		return gender;
	}
	
	public String toString()//override object toString
	{
		return ("This guy is a Person!!");
	}
}
