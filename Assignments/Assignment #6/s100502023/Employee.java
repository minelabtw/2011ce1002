package a6.s100502023;

public class Employee extends Person 
{
	private int salary;
	private int rank;  //1 for Professor, 2 for Associated Professor, 3 for Assitant Professor, 4 for Lecturer
	
	public Employee(String input_name, String input_gender,int input_salary,int input_rank) //constructor
	{
		super(input_name, input_gender); // superclass constructor
		salary=input_salary;
		rank=input_rank;
	}
	
	public int getSalary()  //get Salary
	{
		return salary;
	}
	
	public String getRank()  //get Rank
	{
		//1 for Professor, 2 for Associated Professor, 3 for Assitant Professor, 4 for Lecturer
		String outputofRank="";
		
		switch(rank)
		{
			case 1:
				outputofRank="Professor";
				break;

			case 2:
				outputofRank="Associated Professor";
				break;
			
			case 3:
				outputofRank="Assitant Professor";
				break;
			
			case 4:
				outputofRank="Lecturer";
				break;		
		}
		
		return outputofRank;
	}
	
	public String toString( )  //override
	{
		return "This guy is a Employee!!";
	}
	
	public String toString(boolean check)  //overload
	{
		String outputofCheck;
		//whether the object is a teacher or not
		if (check==true)
		{
			outputofCheck=toString( );
		}
		else
		{
			//false
			outputofCheck="Though this guy is not a Student," + super.toString();	
		}
		
		return outputofCheck;
	}
	
}
