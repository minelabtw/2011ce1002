package a6.s100502023;

public class Student extends Person
{
	private String  classRoom;
	
	public Student(String input_name, String input_gender,String input_classRoom)  //constructor
	{
		super(input_name, input_gender);  // superclass constructor
		classRoom=input_classRoom;
	}

	
	
	public String getClassRoom()  //get ClassRoom
	{
		return classRoom;
	}
	
	public String toString( )  //override
	{
		return "This guy is a Student!!";
	}
	
	public String toString(boolean check)  //overload
	{
		String outputofCheck;
		//whether the object is a student or not
		if (check==true)
		{
			outputofCheck=toString( );
		}
		else
		{
			//false
			outputofCheck="Though this guy is not a Employee," + super.toString();
			
		}
		
		return outputofCheck;
	}
}
