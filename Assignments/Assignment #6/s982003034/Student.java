package a6.s982003034;

public class Student extends Person {

	private String classroom;
	
	public Student(String name, String gender, String classroom) {
		super (name, gender);
		this.classroom = classroom;
	}

	public String getClassRoom() {
		String temp = classroom;
		return temp;
	}
	
	public String toString() {
		return "This " + getPronoun() + " is a student";
	}

	public String toString (boolean check) {
		if (check) return this.toString();
		else return "Though this " + getPronoun() + " is not a teacher," + super.toString();
	}

}
