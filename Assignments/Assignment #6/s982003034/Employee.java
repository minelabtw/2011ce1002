package a6.s982003034;

public class Employee extends Person{
	
	private int salary;
	private int rank;
	
	public Employee(String name, String gender, int salary, int rank) {
		super (name, gender);
		this.salary = salary;
		this.rank = rank;
	}

	public String getSalary () {
		String temp = "$" + salary;
		return temp;
	}
	
	public String getRank () {
		String temp = "";
		switch (rank) {
		case 1: temp = "Professor"; break;
		case 2: temp = "Associated Professor"; break;
		case 3: temp = "Assistant Professor"; break;
		}
		return temp;
	}
	
	public String toString() {
		return "This " + getPronoun() + " is an employee";
	}
	
	public String toString (boolean check) {
		if (check) return this.toString();
		else return "Though this " + getPronoun() + " is not a student," + super.toString();
	}
	
}
