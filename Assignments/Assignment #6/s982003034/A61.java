package a6.s982003034;

import java.util.Scanner;

public class A61 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);

		boolean exitFlag = false;

		while(exitFlag == false){
			System.out.println("Please the person you want to create 1.teacher 2.student 3.Exit: ");
			int userChoice = input.nextInt();

			// user choice for creating object
			switch(userChoice){
			case 1:
				// new a Employee object, and initial it
				System.out.println("Please input the teacher's name, gender, salary");
				System.out.println(", and rank(1 for Professor, 2 for Associated Professor, 3 for Assitant Professor, 4 for Lecturer");
				Employee teacher = new Employee( input.next(), input.next(), input.nextInt(), input.nextInt() );

				// use methods to answer the questions
				System.out.println("The teacher's name is " + teacher.getName());
				System.out.println("The teacher's gender is " + teacher.getGender());
				System.out.println("The teacher's salary is " + teacher.getSalary());
				System.out.println("The teacher's rank is " + teacher.getRank());
				System.out.println("The category of this guy is: " + teacher.toString());
				System.out.println("Is this guy a Employee? " + teacher.toString(true));
				System.out.println("Is this guy a Student? " + teacher.toString(false));

				break;

			case 2:
				// new a Student object, and initial it
				System.out.println("Please input the student's name, gender, and classroom number");
				Student student = new Student( input.next(), input.next(), input.next() );

				// use methods to answer the questions
				System.out.println("The student name is " + student.getName());
				System.out.println("The student's gender is " + student.getGender());
				System.out.println("The student's classroom is at " + student.getClassRoom());
				System.out.println("The category of this guy is: " + student.toString());
				System.out.println("Is this guy a Student? " + student.toString(true));
				System.out.println("Is this guy a Employee? " + student.toString(false));

				break;

			case 3:
				exitFlag = true;
				System.out.println("See you next week~~");
				break;

			default:
				break;
			}
		}
	}
}

