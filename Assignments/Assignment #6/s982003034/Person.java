package a6.s982003034;

public class Person {
	
	protected String name;
	protected String gender;
	
	public Person (String name, String gender) {
		this.name = name;
		this.gender = gender;
	}
	public String getName () {
		String temp = name; // to avoid returning name's address
		return temp;
	}
	
	public String getGender () {
		String temp = gender; // to avoid returning gender's address
		return temp;
	}
	
	public String getPronoun () {
		String pronoun;
		if (gender.compareToIgnoreCase("male") == 0) pronoun = "guy";
		else if (gender.compareToIgnoreCase("female") == 0) pronoun = "lady";
		else pronoun = "thing";
		return pronoun;
	}
	
	public String toString() {
		return "This " + getPronoun() + " is a person";
	}
	
}
