package a6.s100502031;

public class Employee extends Person{
	private int salary;
	private int rank;
	
	Employee(String Name,String Genter,int salary,int rank){
		this.Name=Name;
		this.Genter=Genter;
		this.salary=salary;
		this.rank=rank;
	}
	public String toString(){
		return "This guy is a Employee";
	}
	public setSalary(){
		return salary;
	}
	public setRank(){
		return rank;
	}
	protected String toString(boolean flag){
		if(flag==true){
			return "This guy is a Employee";
		}
		else
			return "Though this guy is not a Student, This guy is a Person !!!";
	}
	
}
