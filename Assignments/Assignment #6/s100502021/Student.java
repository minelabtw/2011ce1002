package a61.s100502021;

public class Student extends Person {
	protected String classroom;
	public Student(String a,String b,String c){ //constructor
		name=a;
		gender=b;
		classroom=c;
	}
	public String getClassRoom(){
		return classroom;
	}
	public String toString(){
		 return "This guy is a Student!!";
	 }
	public String toString(boolean check){
		 if(check==true){
			 return "This guy is a Student!!";
		 }
		 else
			 return  "Though this guy is not a Employee, "+super.toString();
	 }
}
