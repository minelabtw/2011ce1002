package a61.s100502021;

public class Employee extends Person {
	protected int salary;
	protected int rank;
	 public Employee(String a,String b,int c,int d) { //constructor
		name=a;
		gender=b;		 
		salary=c;
	 	rank=d;
	 } 
	 public int getSalary(){
		 return salary;
	 }
	 public String getRank(){
		 switch(rank){
		 case 1:
			 return "Professor";
			 
		 case 2:
			 return "Associated Professor";
			 
		 case 3:
			 return "Assitant Professor";
		 
		 case 4:
			 return "Lecturer";
		
		 }
		 return "";
		
	 }
	 public String toString(){
		 return "This guy is a Employee!!";
	 }
	 public String toString(boolean check){
		 if(check==true){
			 return "This guy is a Employee!!";
		 }
		 else
			 return  "Though this guy is not a Student, "+super.toString();
	 }
}
