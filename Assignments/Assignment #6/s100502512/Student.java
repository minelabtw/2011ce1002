package a6.s100502512;

public class Student extends Person {// 繼承Person
	protected String classroomnumber;

	public Student(String A, String B, String C) {// 多回傳calssroomnumber
		super(A, B);
		classroomnumber = C;
	}

	public String getClassRoom() {// 得到classroomnumber
		return classroomnumber;
	}

	public String toString() {// 顯示訊息
		return "This guy is a student!!";
	}

	public String toString(boolean check) {// true/false回傳不同訊息
		if (check == true) {
			return "This guy is a student!! ";
		} else {
			return "Though this guy is not an employee, This guy is a Person!!";
		}
	}

}
