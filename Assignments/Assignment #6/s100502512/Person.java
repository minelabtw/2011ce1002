package a6.s100502512;

public class Person {
	protected String name;// 用protected宣告,繼承的class就可以繼續使用 name,gender
	protected String gender;

	public Person(String A, String B) {// 回傳name,gender
		name = A;
		gender = B;
	}

	public String getName() {// 取得name
		return name;
	}

	public String getGender() {// 取得gender
		return gender;
	}

	public String toString() {// 顯式訊息
		return "The guy is a person";
	}

}
