package a6.s100502512;

public class Employee extends Person {// 繼承Person
	protected int salary;
	protected int rank;

	public Employee(String A, String B, int S, int R) {// 多回傳salary,rank
		super(A, B);
		salary = S;
		rank = R;
	}

	public int getSalary() {// 取得salary
		return salary;
	}

	public void getRank() {// 取得rank,輸入1,2,3,4分別顯示不同的階級
		if (rank == 1) {
			System.out.println("Professor");
		} else if (rank == 2) {
			System.out.println("associated professor");
		} else if (rank == 3) {
			System.out.println("Assitant Professor");
		} else if (rank == 4) {
			System.out.println("Lecturer");
		}
	}

	public String toString() {// 顯示訊息
		return "This guy is an employee!! ";
	}

	public String toString(boolean check) {// true/false回傳不同訊息
		if (check == true) {
			return "This guy is an employee!!";
		} else {
			return "Though this guy is not a student, This guy is a Person!!";
		}
	}
}
