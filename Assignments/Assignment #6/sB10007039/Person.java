package a6.sB10007039;

public class Person {
	
	private String Name;
	private String Gender;
	
	public Person(String iName,String iGrnder) {
		Name = iName;
		Gender = iGrnder;
	}
	
	public String getName() {
		return Name;
	}
	
	public String getGender() {
		return Gender;
	}
	
	public String toString() {
		return "This guy is a Person!!";
	}

}
