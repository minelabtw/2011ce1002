package a6.sB10007039;

public class Employee extends Person {
	private int Salary;
	private int Rank;
	
	public Employee(String iName, String iGrnder,int iSalary,int iRank) {
		super(iName, iGrnder);
		Salary = iSalary;
		Rank = iRank;
		
	}

	public int getSalary() {
		return Salary;
	}
	
	public String getRank() {
		String Output = null;
		switch (Rank) {
		case 1:
			Output = "Professor";
			break;
		case 2:
			Output = "Associated Professor";
			break;
		case 3:
			Output = "Assistant Professor";
			break;
		default:
			Output = "Lecturer";
			break;
		}
		return Output;
	}
	
	public String toString() {
		return "This guy is a Employee!!";
	}
	public String toString(boolean Type) {
		if (Type) {
			return toString();
		} else {
			return "Though this guy is not a Student, " + super.toString();
		}
	}
}
