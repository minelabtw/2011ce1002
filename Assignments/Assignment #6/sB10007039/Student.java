package a6.sB10007039;

public class Student extends Person{
	private String ClassRoom;
	
	public Student(String iName, String iGrnder,String iClassroom) {
		super(iName, iGrnder);
		ClassRoom = iClassroom;
	}
	public String getClassRoom() {
		return ClassRoom;
	}
	
	public String toString() {
		return "This guy is a Student!!";
	}
	
	public String toString(boolean Type) {
		if (Type) {
			return toString();
		} else {
			return "Though this guy is not a Employee, " + super.toString();
		}
	}
}
