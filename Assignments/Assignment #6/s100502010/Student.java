package a6.s100502010;

public class Student extends Person
{
	String classroom,answer;
	public Student(String nameo, String gendero,String classroomo) 
	{
		super(nameo, gendero);
		
		classroom=classroomo;
	}
	public String getClassRoom()//ruturn the classroom
	{
		return classroom;
	}
	public String toString(boolean check)//judge the person
	{
		if(check==true)
		{
			answer="This guy is a Student!!"; 
		}
		else if(check==false)
		{
			answer="Though this guy is not a Employee, This guy is a " + super.toString()+"!!";
		}
		return answer;
	}
	public String toString()//judge the person
	{
		String answer="This guy is a Student!!";
		return answer;
	}
}
