package a6.s100502010;

public class Employee extends Person
{
	String answer;
	String gender;
	int salary;
	int rank;
	
	public Employee(String nameo,String gendero,int salaryo,int ranko)
	{
		super (nameo,gendero);
		gender=gendero;
		salary=salaryo;
		rank=ranko;
	}
	public int getSalary()//return the salary
	{
		return salary;
	}
	public String getRank()//judge the rank of the person
	{
		String answer="";
		switch(rank)
		{
		case 1:
			answer="Professor";
			break;
		case 2:
			answer="Associated Professor";
			break;
		case 3:
			answer="Assitant Professor";
			break;
		default:
			break;
		}
		return answer;
	}
	public String toString(boolean check)//judge the person
	{
		if(check==true)
		{
			answer="This guy is a "+ toString() + "!!";
		}
		else if(check==false)
		{
			answer="Though this guy is not a Student, This guy is a " + super.toString() + "!!";
		}
		return answer;
	}
	public String toString()//judge the person
	{
		String answer="Employee";
		return answer;
	}

}
