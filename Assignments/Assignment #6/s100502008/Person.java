package a6.s100502008;

public class Person {
	protected String name, gender;

	public Person()// constructor
	{
		this("Trundle", "Male");
	}

	public Person(String username, String usergender)// constructor
	{
		name = username;
		gender = usergender;
	}

	public String getName() // get name
	{
		return name;
	}

	public String getGender() // get gender
	{
		return gender;
	}

	public String toString()// print string
	{
		return "This guy is a Person!!";
	}

}
