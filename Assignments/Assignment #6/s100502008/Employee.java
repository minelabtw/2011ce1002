package a6.s100502008;

public class Employee extends Person {
	protected int salary,rank;
	public Employee(String name,String gender,int usersalary,int userrank)//constructor
	{
		super(name,gender);
		salary=usersalary;
		rank=userrank;
	}
	public String toString()//print string
	{
		return "This guy is a Employee!!";
	}
	public String toString(boolean check)//print string
	{
		if(check)//is employee
		{
			return "This guy is a Employee!!";
		}
		else//not student
		{
			return "Though this guy is not a Student, "+super.toString();
		}
	}
	public int getSalary()//get salary
	{
		return salary;
	}
	public String getRank()//get rank
	{
		if(rank==1)
		{
			return "Professor";
		}
		else if(rank==2)
		{
			return "Associated Professor";
		}
		else
		{
			return "Assitant Professor";
		}
	}
}
