package a6.s100502008;

public class Student extends Person {
	protected String classroom;

	public Student(String name, String gender, String userclassroom)// constructor
	{
		super(name, gender);
		classroom = userclassroom;
	}

	public String toString()// print string
	{
		return "This guy is a Student!!";
	}

	public String toString(boolean check)// print string
	{
		if (check)// is student
		{
			return "This guy is a Student!!";
		} else// not employee
		{
			return "Though this guy is not a Employee, " + super.toString();
		}
	}

	public String getClassRoom()// get classroom
	{
		return classroom;
	}
}
