package a3.s100502544;

public class LinearEquation {
	 
	LinearEquation(double A,double B,double E,double C,double D,double F){//把收到的質assign給a b c d e f
		a=A;
	    b=B;
		c=C;
		d=D;
		e=E;
		f=F;
		
	}
	private static double a,b,c,d,e,f;//private值不可被改變
	double X=0,Y=0;
	boolean isSolvable(){//判斷分母是不是等於零
		if(a*d-b*c==0){
			return false;
		}
		return true;
	}
	void setxvalue(){//算出X值的涵式
			X=((e*d)-(b*f))/((a*d)-(b*c));
    }
    void setyvalue(){//算出Y值的涵式
    		Y=((a*f)-(e*c))/((a*d)-(b*c));
	}
    double getX(){//傳回X值到main
    	setxvalue();
    	return X;
    }
    double getY(){//傳回Y值
    	setyvalue();
    	return Y;
    }
}
