package a3.s100502544;
import java.util.Scanner;
import java.text.NumberFormat;
public class A31 {
	public static double[][] multipleMatrix( double[][] a, double[][] b){//二維的涵式 接收兩個二維陣列
		double[][] tot=new double[3][3];//用來儲存和回傳相乘之後的3*3陣列
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				for(int k=0;k<3;k++){
					tot[i][j]=tot[i][j]+a[i][k]*b[k][j];//單個陣列元素
			    }
			}	
		}
		return tot;
	}
	
	public static void main(String[] args){
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits( 1 );//用來捨去小去點1位之後的質
		double[][] matrix1=new double[3][3];
		double[][] matrix2=new double[3][3];
		double[][] matrix3=new double[3][3];//用來儲存回傳之後的陣列
		java.util.Scanner input=new Scanner(System.in);
		System.out.print("Enter matrix1:");
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				matrix1[i][j]=input.nextDouble();
			}
			
		}
		System.out.print("Enter matrix2:");
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				matrix2[i][j]=input.nextDouble();
			}
		}//輸入兩個二維陣列
		System.out.println("The matrices are multiplied as follows:");
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				System.out.print(matrix1[i][j]+" ");
				
			}
			
			if(i==1){
				System.out.print(" "+"*");
			}
			System.out.print("\t");
			for(int j=0;j<3;j++){
				System.out.print(matrix2[i][j]+" ");
				
			}
			if(i==1){
				System.out.print(" "+"=");
			}
			System.out.print("\t");
			matrix3=multipleMatrix(matrix1,matrix2);//接收回傳的陣列
			for(int j=0;j<3;j++){
				System.out.print(nf.format(matrix3[i][j])+" ");
			}
			System.out.println();
		}
	}
}
