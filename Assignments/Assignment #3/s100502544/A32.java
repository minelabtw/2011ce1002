package a3.s100502544;
import java.util.Scanner;

public class A32 {
	public static void main(String[] args){
		
		Scanner input=new Scanner(System.in);
		System.out.println("input:");
		double a=input.nextDouble();//輸入a b c d e f的質
		double b=input.nextDouble();
		double c=input.nextDouble();
		double d=input.nextDouble();
		double e=input.nextDouble();
		double f=input.nextDouble();
		LinearEquation linearEquation=new LinearEquation(a,b,e,c,d,f);//傳到class
		System.out.println("output");
		if(linearEquation.isSolvable()==true){
			System.out.println("x is "+linearEquation.getX()+" and y is "+linearEquation.getY());
		}
		else{
			System.out.println("The equation has no solution");
		}
	}

}
