package a3.sB10007039;

import java.util.Scanner;
import java.math.BigDecimal;

public class A31{

	
	public static void main(String[] args) {
		
		Scanner Inputer = new Scanner(System.in);
		
		
		
		double[][] matrix1 = new double[3][3];
		double[][] matrix2 = new double[3][3];
		double[][] multiple = new double[3][3];
		
		System.out.print("Enter matrix1:");
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				matrix1[i][j] = Inputer.nextDouble();
			}
		}
		System.out.print("Enter matrix2:");
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				matrix2[i][j] = Inputer.nextDouble();
			}
		}
		
		multiple = multipleMatrix(matrix1, matrix2);
		
		System.out.println("the matrices are multiplied as follows:");
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print(matrix1[i][j]);
				System.out.print("\t");
			}
			if (i==1) {
				System.out.print("*\t");
			}else {
				System.out.print("\t");
			}
			for (int j = 0; j < 3; j++) {
				System.out.print(matrix2[i][j]);
				System.out.print("\t");
			}
			if (i==1) {
				System.out.print("=\t");
			}else {
				System.out.print("\t");
			}
			for (int j = 0; j < 3; j++) {
				BigDecimal bd_value1 = new BigDecimal(multiple[i][j]);
				System.out.print(bd_value1.setScale(1, BigDecimal.ROUND_HALF_UP));
				System.out.print("\t");
			}
			System.out.print("\n");
		}
		
	}
	
	public static double[][] multipleMatrix( double[][] a, double[][] b){
		double[][] multiple = new double[3][3];
		
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < 3; k++) {
					multiple[i][j] += a[i][k]*b[k][j];
				}
			}
		}
		
		
		return multiple;
	}
	
}
