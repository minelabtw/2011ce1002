package a3.s100502031;

public class LinearEquation {
	
	//data field 
	private double a;
	private double b;
	private double c;
	private double d;
	private double e;
	private double f;
	String answer;
	
	//constructor
	public  LinearEquation(double Anumber,double Bnumber,double Cnumber,double Dnumber,double Enumber,double Fnumber){ 
		//asign the data field
		a=Anumber;
		b=Bnumber;
		c=Cnumber;
		d=Dnumber;
		e=Enumber;
		f=Fnumber;
		answer=isSolvable();
		
		 System.out.print(answer);
	}
	
	//the method to get the data field
	public double getA(){  
		return a;
	}
	public double getB(){  
		return b;
	}
	public double getC(){ 
		return c;
	}
	public double getD(){  
		return d;
	}
	public double getE(){  
		return e;
	}
	public double getF(){  
		return f;
	}
	
	//method to identify wether the answer is exist or not
	public String  isSolvable(){
		//if it exost,then get the answer
		if(a*d-b*c!=0){
			return "x is " + getX() + " and y is "+ getY();
		}
		//if not,return error
		else{
			return "The equation has no solution";
		}
		
	}
	//caculate x
	public double getX(){
		double x;
		x=(e*d-b*f)/(a*d-b*c);
		return x;
	}
	//caculate y
	public double getY(){
		double y;
		y=(a*f-e*c)/(a*d-b*c);
		return y;
	}

}

