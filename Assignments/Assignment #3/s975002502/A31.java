package a3.s975002502;

import java.util.Scanner;

public class A31 {
	/**to multiply two matrixes*/
	public static double[][] multipleMatrix(double[][] a, double[][] b){
		double[][] ans = new double[3][3];
		for (int i = 0; i < 3; i++){
			for (int j = 0; j < 3; j++){
				for (int k = 0; k < 3; k++){
					ans[i][j] += a[i][k]*b[k][j];
				}
			}
		}
		
		return ans;
	}
	
	/**to build a matrix form user's inputs*/
	public static double[][] constructMatrix(){
		double[][] matrix = new double[3][3];
		Scanner input = new Scanner(System.in);	
		
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				matrix[i][j] = input.nextDouble();
			}
		}
		return matrix;
	}
	
	/**show the result*/
	public static void printResult(double[][] a, double[][] b, double[][] ans){
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				System.out.print(a[i][j]+"\t");
			}
			if(i != 1){
				System.out.print("\t");
			}
			if(i ==1){
				System.out.print("*\t");
			}
			for(int j = 0; j < 3; j++){
				System.out.print(b[i][j]+"\t");
			}
			if(i != 1){
				System.out.print("\t");
			}
			if(i ==1){
				System.out.print("=\t");
			}
			for(int j = 0; j < 3; j++){
				System.out.printf("%2.1f\t",ans[i][j]);
			}
			System.out.println("");
		}
	}
	
	public static void main(String[] args){
		double[][] a = new double[3][3];
		double[][] b = new double[3][3];
		double[][] ans = new double[3][3];
		
		System.out.println("input");
		System.out.println("");
		
		//enter matrix1(a) value
		System.out.print("Enter matrix1: ");
		a = constructMatrix();		//get input and build matrix1 a
		
		//enter matrix2(b) value
		System.out.print("Enter matrix2: ");
		b = constructMatrix();		//get input and build matrix2 b
		
		System.out.println("");
		System.out.println("output");
		System.out.println("");
		
		//calculate and show the result
		System.out.println("the matrices are multiplied as follows: ");
		ans = multipleMatrix(a, b);			//calculate [matrix a] * [matrix b]
		printResult(a, b, ans);				//show the result
		
	}

}
