package a3.s975002502;
import java.util.Scanner;

public class A32 {
	public static void main(String [] argv){
		Scanner input = new Scanner(System.in); // new a Scanner object

		// input a, b, c, d, e and f of LinearEquation 
		System.out.println("input");
		Double a = input.nextDouble();
		Double b = input.nextDouble();
		Double c = input.nextDouble();
		Double d = input.nextDouble();
		Double e = input.nextDouble();
		Double f = input.nextDouble();

		// new a LinearEquation  object and initialize a, b, c, d, e and f
		LinearEquation  linearEquation  = new LinearEquation (a, b, c, d, e, f); 

		// show the solution for the equation
		boolean solvable = linearEquation.isSolvable();
		System.out.println("output");
		if(solvable == true){
			System.out.println("x is " + linearEquation.getX() +" and y is " + linearEquation.getY()); 
		}
		else{
			System.out.println("The equation has no solution");
		}
		
	}

}
