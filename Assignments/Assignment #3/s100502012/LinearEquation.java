package a3.s100502012;

public class LinearEquation {
public LinearEquation(double a,double b,double c,double d,double e,double f){// a constructor that accept six double
   A=a;
   B=b;
   C=c;
   D=d;
   E=e;
   F=f;
}
double A;
double B;
double C;
double D;
double E;
double F;
double getX(){// answer of x
   double X=(E*D-B*F)/(A*D-B*C);
   return X;
}
double getY(){//answer of y
   double Y=(A*F-E*C)/(A*D-B*C);
   return Y;
}
int isSolvable(){//judge whether the functions have the answer
   int flag=0;
   if ((A*D-B*C)==0){
      flag=0;	   	  
   }
   else{
	  flag=1;
   }
   return flag;
}
}