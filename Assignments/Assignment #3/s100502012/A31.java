package a3.s100502012;

import java.util.Scanner;

public class A31 {
public static double[][] subtractMatrices(double[][] matrix1,double[][] matrix2){
double[][] multi=new double[3][3];

for(int X=0 ; X<3 ; X++){// The algorithm of the multiple of the arrays.
   for(int Y=0 ; Y<3 ; Y++){
	  for(int Z=0 ; Z<3 ; Z++){
	     multi[X][Y]+=(int)(matrix1[X][Z]*matrix2[Z][Y]*10)/10.0;
	  }   
   }
}   
return multi;
}

public static void main(String[] ags){
Scanner input=new Scanner(System.in);	

double[][] matrix1=new double[3][3];
double[][] matrix2=new double[3][3];

System.out.println("Enter matrix1: ");//store array
for(int X=0 ; X<3 ; X++){
   for(int Y=0 ; Y<3 ; Y++){
	  matrix1[X][Y]=input.nextDouble(); 
   }
}

System.out.println("\nEnter matrix2: ");//store array
for(int X=0 ; X<3 ; X++){
   for(int Y=0 ; Y<3 ; Y++){
	  matrix2[X][Y]=input.nextDouble(); 
   }
}

double mu[][]=subtractMatrices(matrix1,matrix2);// sent back the value of the calculating of the arrays

System.out.println("\n\n\n\nThe matrices are multipled as follow: ");
String list="";
for(int X=0 ; X<3 ; X++){// arange the string
   for(int Y=0 ; Y<3 ; Y++){
	  list+=matrix1[X][Y];
	  if (Y==2){
	     if (X==1){
			list+="  *  "; 
	     }
	     else{
		    list+="\t";
	     }
	  }
	  else{
	     list+="\t"; 
	  }
   }
			   
   for(int Y=0 ; Y<3 ; Y++){
	  list+=matrix2[X][Y];
	  if (Y==2){
		 if (X==1){
		    list+="  =  "; 
		 }
		 else{
			list+="\t";
		 }
	  }
      else{
		 list+="\t"; 
	  }
   }
			   
   for(int Y=0 ; Y<3 ; Y++){
	  list+=mu[X][Y];
	  if (Y==2){
		 list+="\n";
	  }
	  else{
		 list+="\t"; 
	  }
   } 
}

System.out.println("\n"+list);
}
}
