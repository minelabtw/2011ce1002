//A31.java
package a3.s100502502;
import java.util.Scanner;
public class A31 {
	public static void main(String[] args){
		double[][] array1 = new double[3][3];//initialize array
		double[][] array2 = new double[3][3];
		double[][] array3 = new double[3][3];
		Scanner input = new Scanner(System.in);
		System.out.print("Enter matrix1: ");
		for(int i = 0; i < 3; i++){//save user input1
			for(int j = 0; j<3; j++){
				array1[i][j] = input.nextDouble();
			}
		}
		System.out.print("Enter matrix2: ");
		for(int i = 0; i < 3; i++){//save user input2
			for(int j = 0; j<3; j++){
				array2[i][j] = input.nextDouble();
			}
		}
		array3 = multipleMatrix(array1, array2);//calculate
		//show answer
		System.out.println(array1[0][0]+" "+array1[0][1]+" "+array1[0][2]+"\t"+array2[0][0]+" "+array2[0][1]+" "+array2[0][2]+"\t"+array3[0][0]+" "+array3[0][1]+" "+array3[0][2]);
		System.out.println(array1[1][0]+" "+array1[1][1]+" "+array1[1][2]+"  *\t"+array2[1][0]+" "+array2[1][1]+" "+array2[1][2]+"  =\t"+array3[1][0]+" "+array3[1][1]+" "+array3[1][2]);
		System.out.println(array1[2][0]+" "+array1[2][1]+" "+array1[2][2]+"\t"+array2[2][0]+" "+array2[2][1]+" "+array2[2][2]+"\t"+array3[2][0]+" "+array3[2][1]+" "+array3[2][2]);
	}
	public static double[][] multipleMatrix(double[][] a, double[][] b){//calculate
		double[][] c = new double[3][3];
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				c[i][j] = a[i][0] * b[0][j] + a[i][1] * b[1][j] + a[i][2] * b[2][j];
			}
		}
		return c;//return array
	}
}
