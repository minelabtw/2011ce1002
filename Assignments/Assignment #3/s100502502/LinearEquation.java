//class LinearEquation.java
package a3.s100502502;
public class LinearEquation {
	//initialize
	LinearEquation(float num1, float num2, float num3, float num4, float num5, float num6){
		a = num1;
		b = num2;
		c = num3;
		d = num4;
		e = num5;
		f = num6;
	}
	boolean isSolvable(){//whether the equation has answer
		if(a*d - b*c == 0)
			return false;
		return true;
	}
	float getX(){//calculate x's solution
		return (e*d-b*f)/(a*d-b*c);
	}
	float getY(){//calculate y's solution
		return (a*f-e*c)/(a*d-b*c);
	}
	private float a, b, c, d, e, f;
}
