//A32.java
package a3.s100502502;
import java.util.Scanner;
public class A32 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		float[] array = new float[6];//initialize
		System.out.println("input :");
		for(int i = 0; i < 6; i++){//save user input
			array[i] = input.nextFloat();
		}
		LinearEquation equation = new LinearEquation(array[0], array[1], array[2], array[3], array[4],array[5]);
		System.out.println("output :");
		if(equation.isSolvable() == true)//if the equation has solution, show the answer
			System.out.println("x is " + equation.getX() + " and y is " + equation.getY());
		else//if not, show the message
			System.out.println("The equation has no solution.");
	}
}
