package a3.s100502507;

import javax.swing.JOptionPane;

public class A32 {
	public static void main(String[] args){//Function main begins execution
		String inputA = JOptionPane.showInputDialog(null, "Input\n" + "coefficient a", "Input", JOptionPane.INFORMATION_MESSAGE);//Input coefficients and constants
		double a = Double.parseDouble(inputA);
		String inputB = JOptionPane.showInputDialog(null, "Input\n" + "coefficient b", "Input", JOptionPane.INFORMATION_MESSAGE);
		double b = Double.parseDouble(inputB);
		String inputC = JOptionPane.showInputDialog(null, "Input\n" + "constant c", "Input", JOptionPane.INFORMATION_MESSAGE);
		double c = Double.parseDouble(inputC);
		String inputD = JOptionPane.showInputDialog(null, "Input\n" + "coefficient d", "Input", JOptionPane.INFORMATION_MESSAGE);
		double d = Double.parseDouble(inputD);
		String inputE = JOptionPane.showInputDialog(null, "Input\n" + "coefficient e", "Input", JOptionPane.INFORMATION_MESSAGE);
		double e = Double.parseDouble(inputE);
		String inputF = JOptionPane.showInputDialog(null, "Input\n" + "constant f", "Input", JOptionPane.INFORMATION_MESSAGE);
		double f = Double.parseDouble(inputF);
		
		LinearEquation start = new LinearEquation(a, b, c, d, e, f);//Creating object from LinearEquation
		if(start.isSolvable()){//Checking solvable or not
			JOptionPane.showMessageDialog(null, "Output\n" + "x is " + start.getX() + " and y is " + start.getY(), "Output", JOptionPane.INFORMATION_MESSAGE);//Output result
		}
		else{//Checking failed
			JOptionPane.showMessageDialog(null, "The equation has no solution", "Output", JOptionPane.INFORMATION_MESSAGE);
		}
	}//End function main
}
