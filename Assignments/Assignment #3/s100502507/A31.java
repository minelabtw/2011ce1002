package a3.s100502507;

import javax.swing.JOptionPane;

public class A31 {
	public static void main(String[] args) {//Function main begins execution
		double[][] matrix1 = new double[3][3];//Two arrays will receiving input from user 
		double[][] matrix2 = new double[3][3];
		double[][] multipled = new double[3][3];//The final result
		
		for(int i=0; i<3; i++) {//Let user input matrix1
			for(int j=0; j<3; j++) {
				String input = JOptionPane.showInputDialog(null, "Enter matrix1[" + i + "][" + j + "]", "Input", JOptionPane.INFORMATION_MESSAGE);
				matrix1[i][j] = Double.parseDouble(input);
			}
		}
		for(int i=0; i<3; i++) {//Let user input matrix2
			for(int j=0; j<3; j++) {
				String input = JOptionPane.showInputDialog(null, "Enter matrix2[" + i + "][" + j + "]", "Input", JOptionPane.INFORMATION_MESSAGE);
				matrix2[i][j] = Double.parseDouble(input);
			}
		}
		
		multipled = multipleMatrix(matrix1, matrix2);//Calling function multipleMatrix to produce the final result
		
		for(int i=0; i<3; i++){//Output the result
			for(int j=0; j<3; j++){
				System.out.print(matrix1[i][j] + "\t");
			}
			if(i==1){
				System.out.print(" *\t");
			}
			else{
				System.out.print("\t");
			}
			for(int j=0; j<3; j++){
				System.out.print(matrix2[i][j] + "\t");
			}
			if(i==1){
				System.out.print(" =\t");
			}
			else{
				System.out.print("\t");
			}
			for(int j=0; j<3; j++){
				System.out.printf(" %.1f ",multipled[i][j] );
				System.out.print("\t");
			}
			System.out.println();
		}
	}
	public static double[][] multipleMatrix( double[][] a, double[][] b) {//A function which multiplies two arrays
		double[][] c = new double[3][3];//An array to hold the current result
		for(int i=0; i<3; i++) {//Initializing
			for(int j=0; j<3; j++) {
				c[i][j] = 0;
			}
		}
		for(int i=0; i<3; i++) {//i for row
			for(int j=0; j<3; j++) {//j for column
				/*Row of a and column of b always remain unchanged while multiplying. Column of a and row of b increase from 1 to 3.
				 *So I let the next for loop begin with the initial condition of k=0, and the finishing condition of k=3.
				 *Last, let k be the column of a and the row of b
				 */
				for(int k=0; k<3; k++) {
					c[i][j] = c[i][j] + (a[i][k]*b[k][j]);
				}
			}
		}
		return c;
	}
}//End function main