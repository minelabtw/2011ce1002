package a3.s100502507;

public class LinearEquation {//Constructor which receives 6 argument to revise private data
	LinearEquation(double A, double B, double C, double D, double E, double F ){
		a = A;
		b = B;
		c = C;
		d = D;
		e = E;
		f = F;
	}
	
	public boolean isSolvable(){//Checking these two equations are solvable or not
		if((a*d - b*c)!=0){
			return true;
		}
		else{
			return false;
		}
	}
	
	public double getX(){//Returning the result of X
		return (e*d - b*f)/(a*d - b*c);
	}
	
	public double getY(){//Returning the result of Y
		return (a*f - e*c)/(a*d - b*c);
	}
	
	private double a, b, c, d, e, f = 0;//6 private argument, including both coefficients and constants
}
