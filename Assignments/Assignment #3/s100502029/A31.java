package a3.s100502029;
import java.util.Scanner;

public class A31 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		// declare two two-dimensional arrays that size is 3x3
		double[][] matrix1 = new double[3][3];
		double[][] matrix2 = new double[3][3];
		
		// prompt user to input matrix1, store user's input in two-dimensional array matrix1
		System.out.print("Enter matrix1: ");
		for (int i=0; i<matrix1.length; i++) {
			for (int j=0; j<matrix1[0].length; j++) {
				matrix1[i][j] = input.nextDouble();
			}
		}
		
		// prompt user to input matrix2, store user's input in two-dimensional array matrix2
		System.out.print("Enter matrix2: ");
		for (int i=0; i<matrix2.length; i++) {
			for (int j=0; j<matrix2[0].length; j++) {
				matrix2[i][j] = input.nextDouble();
			}
		}
		
		// declare an array named matrix3, store the result of matrix1 mutiple matrix2
		double[][] matrix3 = multipleMatrix(matrix1, matrix2);
		
		// display the result, 把結果四捨五入至小數點第一位
		System.out.println("The matrices are multiplied as follows :");
		for (int i=0; i<matrix3.length; i++) { // display each row
			for (int j=0; j<matrix1[0].length; j++) // show matrix1
				System.out.printf("%4.1f\t", matrix1[i][j]);
			if (i != 1)
				System.out.print(" \t");
			else
				System.out.print("*\t");			
			for (int j=0; j<matrix2[0].length; j++) // show matrix2
				System.out.printf("%4.1f\t", matrix2[i][j]);
			if (i != 1)
				System.out.print(" \t");
			else
				System.out.print("=\t");			
			for (int j=0; j<matrix3[0].length; j++) // show matrix3
				System.out.printf("%4.1f\t", matrix3[i][j]);
			System.out.println(); // next line
		}
	}
	
	// 將陣列a與陣列b相乘，把所得結果陣列c回傳回去
	public static double[][] multipleMatrix(double[][] a, double[][] b) {
		double[][] c = new double[3][3];
		for (int i=0; i<c.length; i++) {
			for (int j=0; j<c[0].length; j++) {
				c[i][j] = (a[i][0] * b[0][j] + a[i][1] * b[1][j] + a[i][2] * b[2][j]);
			}
		}
		return c;
	}
}
