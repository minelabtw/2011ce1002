package a3.s100502029;

public class LinearEquation {
	private double a, b, c, d, e, f; // declare six private variables
	
	// constructor that initial six private variables
	LinearEquation(double new_a, double new_b, double new_c, double new_d, double new_e, double new_f) {
		a = new_a;
		b = new_b;
		c = new_c;
		d = new_d;
		e = new_e;
		f = new_f;
	}
	
	double get_a() { // get 'a' number
		return a;
	}
	double get_b() { // get 'b' number
		return b;
	}
	double get_c() { // get 'c' number
		return c;
	}
	double get_d() { // get 'd' number
		return d;
	}
	double get_e() { // get 'e' number
		return e;
	}
	double get_f() { // get 'f' number
		return f;
	}
	
	// determine whether the solution exists or not
	boolean isSolvable() {
		if ((a * d - b * c) != 0)
			return true;
		else
			return false;
	}
	
	// calculate x and return the result
	double getX() {
		return (e * d - b * f) / (a * d - b * c);
	}
	
	// calculate y and return the result
	double getY() {
		return (a * f - e * c) / (a * d - b * c);
	}
}
