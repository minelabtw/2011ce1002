package a3.s100502029;

public class A32 {
	public static void main(String[] args) {
		java.util.Scanner input = new java.util.Scanner(System.in);
		char character = '\u0061';
		double[] number = new double[6];
		System.out.println("Please input:");
		for (int i=0; i<number.length; i++) {
			System.out.print(character++ + " = ");
			number[i] = input.nextDouble();
		}
		LinearEquation object = new LinearEquation(number[0], number[1], number[2], number[3], number[4], number[5]);
		if (object.isSolvable() == true)
			System.out.println("x is " + object.getX() + " and y is " + object.getY());
		else
			System.out.println("The equation has no solution");
	}
}