package a3.s100502510;

public class LinearEquation {
	private double A;
	private double B;
	private double C;
	private double D;
	private double E;
	private double F;

	LinearEquation(double one, double two, double three, double four,
			double five, double six) {// 將輸入的六個數帶入LinearEquation裡
		A = one;
		B = two;
		C = three;
		D = four;
		E = five;
		F = six;

	}

	public boolean isSolvable() {// a*d-b*c不能等於零,因為分母不可等於零
		if (A * D - B * C == 0) {
			return false;
		} else {
			return true;
		}

	}

	public double getx() {
		double answer = (E * D - B * F) / (A * D - B * C);
		return answer;// x的解答
	}

	public double gety() {
		return (A * F - E * C) / (A * D - B * C);// y的解答
	}
}
