package a3.s100502510;

import java.util.Scanner;

public class A32 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double a = 0;// 定義六個輸入的數
		double b = 0;
		double c = 0;
		double d = 0;
		double e = 0;
		double f = 0;
		System.out.println("input");
		a = input.nextDouble();
		b = input.nextDouble();
		c = input.nextDouble();
		d = input.nextDouble();
		e = input.nextDouble();
		f = input.nextDouble();

		LinearEquation linearEquation = new LinearEquation(a, b, c, d, e, f);// 將LinearEquation帶入

		System.out.println("output");
		if (linearEquation.isSolvable()) {// 設定輸出的條件
			System.out.print("x is " + linearEquation.getx() + " and y is "
					+ linearEquation.gety());
		} else {
			System.out.print("The equation has no solution");
		}
	};

}
