package a3.s100502510;

import java.util.Scanner;

public class A31 {
	public static double[][] multipleMatrices(double[][] a, double[][] b) {// 用來儲存這個計算陣列的公式

		double[][] answer = new double[3][3];
		for (int x = 0; x < 3; x++) {

			for (int y = 0; y < 3; y++) {
				answer[x][y] = a[x][0] * b[0][y] + a[x][1] * b[1][y] + a[x][2]
						* b[2][y];

			}
		}

		return answer;// 最後的答案

	}

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double[][] arraya = new double[3][3];
		double[][] arrayb = new double[3][3];
		double[][] arrayc = new double[3][3];// 用來儲存最後的陣列
		System.out.print("Enter matrix1:");
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				arraya[i][j] = input.nextDouble();
			}
		}
		System.out.print("Enter matrix2:");
		for (int a = 0; a < 3; a++) {
			for (int b = 0; b < 3; b++) {
				arrayb[a][b] = input.nextDouble();
			}
		}
		arrayc = multipleMatrices(arraya, arrayb);
		System.out.println("The matrices are multiplied as follows:");
		for (int y = 0; y < 3; y++) {// 漫長的排版
			System.out.print(arraya[0][y] + "\t");
		}
		System.out.print("\t");
		for (int y = 0; y < 3; y++) {
			System.out.print(arrayb[0][y] + "\t");
		}
		System.out.print("\t");
		for (int y = 0; y < 3; y++) {
			System.out.printf("%.1f\t", arrayc[0][y]);// 使顯示出來的小數點不要過多
		}
		System.out.print("\n");
		for (int y = 0; y < 3; y++) {
			System.out.print(arraya[1][y] + "\t");
		}
		System.out.print("*" + "\t");
		for (int y = 0; y < 3; y++) {
			System.out.print(arrayb[1][y] + "\t");
		}
		System.out.print("=" + "\t");
		for (int y = 0; y < 3; y++) {
			System.out.printf("%.1f\t", arrayc[1][y]);
		}
		System.out.print("\n");
		for (int y = 0; y < 3; y++) {
			System.out.print(arraya[2][y] + "\t");
		}
		System.out.print("\t");
		for (int y = 0; y < 3; y++) {
			System.out.print(arrayb[2][y] + "\t");
		}
		System.out.print("\t");
		for (int y = 0; y < 3; y++) {
			System.out.printf("%.1f\t", arrayc[2][y]);

		}
		System.out.print("\n");
	}

}
