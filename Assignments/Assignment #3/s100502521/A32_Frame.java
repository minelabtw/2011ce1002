package a3.s100502521;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.Scanner;
public class A32_Frame extends JFrame implements ActionListener//類別繼承類別，類別實做介面
{
	private static final long serialVersionUID = 1L;//eclipse加的
	private JLabel[] label;//label標題
	private JTextField[] textField;//textbox
	private JButton[] button;//按鈕
	public A32_Frame() //初始化各個物件
	{
		super("二元一次方程式求解");
		setLayout(new FlowLayout(FlowLayout.LEFT));
		label=new JLabel[9];
		label[0]= new JLabel("X +");
		label[1]= new JLabel("Y = ");
		label[2]= new JLabel("			                                                                                                   											");
		label[3]= new JLabel("X +");
		label[4]= new JLabel("Y = ");
		label[5]= new JLabel("                                                                                                                           ");
		label[6]= new JLabel("X=                                                                                                                                                                       ");
		label[7]= new JLabel("Y=                                                                                                                                                                       ");
		label[8]= new JLabel("X,Y的解是:                                                                                                                                                                       ");
		
		textField=new JTextField[6];
		for(int i=0;i<6;i++)
		{
			textField[i]= new JTextField(5);
		}
		button=new JButton[3];
		button[0] = new JButton("計算!!");
		button[1] = new JButton("在小黑窗輸入輸出");
		button[2] = new JButton("清空");
		button[0].addActionListener(this);// 加入按鈕
		button[1].addActionListener(this);
		button[2].addActionListener(this);
		for(int i=0;i<6;i++)
		{
			add(textField[i]);
			add(label[i]);
		}
		add(label[8]);
		add(label[6]);
		add(label[7]);
		add(button[0]);
		add(button[1]);
		add(button[2]);
	}
	 
	@Override
	public void actionPerformed(ActionEvent e)//毒入3個值 判斷跟畫圖
	{
		
		if (e.getSource() == button[0])
		{
			double[] temp=new double[6];
			for(int i=0;i<6;i++)
			{
				temp[i]=Double.parseDouble(textField[i].getText());
			}
			LinearEquation object1=new LinearEquation(temp[0],temp[1],temp[3],temp[4],temp[2],temp[5]);
			if( object1.isSolvable()==true)
			{
				label[6].setText("X="+object1.getX()+"                                                                                                                                                                   ");
				label[7].setText("Y="+object1.getY()+"                                                                                                                                                                   ");
			}
			else
			{
				label[8].setText("X,Y的解是:    !!The equation has no solution!!                                                                                                                                                                      ");
			}
			
		}
		else if (e.getSource() == button[1])
		{
			this.setVisible(false);
			Scanner input = new Scanner(System.in);
			double[] temp=new double[6];
			System.out.print("ax+by=e,cx+dy=f，請輸入a,b,c,d,e,f:");
			for(int i=0;i<6;i++)
			{
				temp[i]=input.nextDouble();
			}
			LinearEquation object2=new LinearEquation(temp[0],temp[1],temp[2],temp[3],temp[4],temp[5]);
			if( object2.isSolvable()==true)
			{
				System.out.printf("x is %.1f and y is %.1f", object2.getX(),object2.getY());
			}
			else
			{
				System.out.println("The equation has no solution");
			}
			this.setVisible(true);
		}
		else if (e.getSource() == button[2])
		{
			label[6].setText("X=                                                                                                                                                                       ");
			label[7].setText("Y=                                                                                                                                                                       ");
			label[8].setText("X,Y的解是:                                                                                                                                                                       ");
			for(int i=0;i<6;i++)
			{
				textField[i].setText("");
			}
		}
	}
}
