package a3.s100502521;

public class LinearEquation 
{
	 private double a,b,c,d,e,f;
	 LinearEquation(double input1,double input2,double input3,double input4,double input5,double input6)
	 {
		 a=input1;
		 b=input2;
		 c=input3;
		 d=input4;
		 e=input5;
		 f=input6;
	 }
	 public boolean isSolvable()//檢查是否有解
	 {
		 if( (a*d-b*c) != 0)
		 {
			 return true;
		 }
		 else
		 {
			 return false;
		 }
	 }
	 public double getX()//回傳X解
	 {
		 return (e*d-b*f)/(a*d-b*c);
	 }
	 public double getY()//回傳Y解
	 {
		 return (a*f-e*c)/(a*d-b*c);
	 }
}
