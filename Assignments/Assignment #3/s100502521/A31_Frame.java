package a3.s100502521;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.Scanner;
import java.text.NumberFormat;//顯示小數2未用的
public class A31_Frame extends JFrame implements ActionListener//類別繼承類別，類別實做介面
{
	private static final long serialVersionUID = 1L;//eclipse加的
	private JLabel[] label;//label標題
	private JTextField[][][] textField;//textbox
	private JButton[] button;//按鈕
	private NumberFormat nf = NumberFormat.getInstance();
    
	public A31_Frame() //初始化各個物件
	{
		super("行列式相乘");
		setLayout(new FlowLayout(FlowLayout.LEFT));
		nf.setMaximumFractionDigits( 2 );
		label=new JLabel[12];
		label[0]= new JLabel("輸入第一個3*3行列式:                                        ");
		label[1]= new JLabel("輸入第二個3*3行列式:                                              ");
		label[2]= new JLabel("the matrices are multiplied as follows:                                       ");
		label[3]= new JLabel("                  ");
		label[4]= new JLabel("                  ");
		label[5]= new JLabel("                  ");
		label[6]= new JLabel("                  ");
		label[7]= new JLabel("                  ");
		label[8]= new JLabel("                  ");
		label[9]= new JLabel("                                                                                                                                                                        ");
		label[10]= new JLabel("                                                                                                                                                                       ");
		label[11]= new JLabel("                                                                                                                                                                       ");
		textField=new JTextField[3][3][2];
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				for(int k=0;k<2;k++)
				{
					textField[i][j][k] = new JTextField(5);
				}
			}
		}
		button=new JButton[3];
		button[0] = new JButton("計算!!");
		button[1] = new JButton("在小黑窗輸入輸出");
		button[2] = new JButton("清空");
		button[0].addActionListener(this);// 加入按鈕
		button[1].addActionListener(this);
		button[2].addActionListener(this);
		add(label[0]);
		add(label[1]);
		add(label[2]);
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				add(textField[i][j][0]);
			}
			add(label[i+3]);
			for(int j=0;j<3;j++)
			{
				add(textField[i][j][1]);
			}
			add(label[i+6]);
		}
		add(label[2]);
		add(label[9]);
		add(label[10]);
		add(label[11]);
		add(button[0]);
		add(button[1]);
		add(button[2]);
	}
	 
	@Override
	public void actionPerformed(ActionEvent e)//毒入3個值 判斷跟畫圖
	{
		
		if (e.getSource() == button[0])
		{
			PrintFrame();
		}
		else if (e.getSource() == button[1])
		{
			this.setVisible(false);
			PrintCmd();
			this.setVisible(true);
		}
		else if (e.getSource() == button[2])
		{
			for(int i=0;i<3;i++)
			{
				for(int j=0;j<3;j++)
				{
					textField[i][j][0].setText("");
					textField[i][j][1].setText("");
				}
			}
			label[9].setText("                                                                                                                                                                        ");
			label[10].setText("                                                                                                                                                                        ");
			label[11].setText("                                                                                                                                                                        ");
		}
	}
	public void PrintFrame()//視窗話顯示答案
	{
		double[][] array1=new double[3][3];
		double[][] array2=new double[3][3];
		double[][] array3=new double[3][3];
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				String temp;
				temp = textField[i][j][0].getText();
				array1[i][j]=Double.parseDouble(temp);
				temp = textField[i][j][1].getText();
				array2[i][j]=Double.parseDouble(temp);
			}
		}
		array3=multipleMatrix(array1,array2);
		for(int i=0;i<3;i++)
		{
			String temp="";
			for(int j=0;j<3;j++)
			{
				temp=temp+"  "+array1[i][j];
			}
			if(i==1)
			{
				temp=temp+"  *";
			}
			else
			{
				temp=temp+"    ";
			}
			for(int j=0;j<3;j++)
			{
				temp=temp+"  "+array2[i][j];
			}
			if(i==1)
			{
				temp=temp+"  =";
			}
			else
			{
				temp=temp+"    ";
			}
			for(int j=0;j<3;j++)
			{
				temp=temp+"  "+nf.format(array3[i][j]);
			}
			temp=temp+"                                                                                         ";
			label[i+9].setText(temp);
		}
	}
	public void PrintCmd()//在小黑窗顯示答案
	{
		Scanner input = new Scanner(System.in);//先創建一個Scanner物件
		double[][] array1=new double[3][3];
		double[][] array2=new double[3][3];
		double[][] array3=new double[3][3];
		System.out.print("Enter matrix1:");
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				array1[i][j]=input.nextDouble();
			}
		}
		System.out.print("Enter matrix2:");
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				array2[i][j]=input.nextDouble();
			}
		}
		array3=multipleMatrix(array1,array2);
		System.out.println("the matrices are multiplied as follows:");
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				System.out.print(array1[i][j]+" ");
			}
			if(i==1)
			{
				System.out.print(" *");
			}
			System.out.print("\t");
			for(int j=0;j<3;j++)
			{
				System.out.print(array2[i][j]+" ");
			}
			if(i==1)
			{
				System.out.print(" =");
			}
			System.out.print("\t");
			for(int j=0;j<3;j++)
			{
				System.out.print(array3[i][j]+" ");
			}
			System.out.print("\n");
		}
		
	}
	public static double[][] multipleMatrix( double[][] a, double[][] b)//行列式相乘
	{
		double[][] c=new double[3][3];
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				for(int k=0;k<3;k++)
				{
					c[i][j]+=a[i][k]*b[k][j];//1的橫排 * 2 的直排  
				}
			}
		}
		return c;
	}
}
