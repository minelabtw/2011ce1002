package a3.s100502021;
import java.util.Scanner;
public class A31 {
	public static void run(double[][] array){ //store method
		Scanner input=new Scanner(System.in);
		for(int i=0;i<3;i++){
			for(int x=0;x<3;x++){
				array[i][x]=input.nextDouble();	
			}
		}
	}
	public static void multipleMatrix(double[][] array1,double[][] array2,double[][] Equal){ //multiple matrix
		Equal[0][0]=array1[0][0]*array2[0][0]+array1[0][1]*array2[1][0]+array1[0][2]*array2[2][0];
		Equal[0][1]=array1[0][0]*array2[0][1]+array1[0][1]*array2[1][1]+array1[0][2]*array2[2][1];
		Equal[0][2]=array1[0][0]*array2[0][2]+array1[0][1]*array2[1][2]+array1[0][2]*array2[2][2];
		
		Equal[1][0]=array1[1][0]*array2[0][0]+array1[1][1]*array2[1][0]+array1[1][2]*array2[2][0];
		Equal[1][1]=array1[1][0]*array2[0][1]+array1[1][1]*array2[1][1]+array1[1][2]*array2[2][1];
		Equal[1][2]=array1[1][0]*array2[0][2]+array1[1][1]*array2[1][2]+array1[1][2]*array2[2][2];
		
		Equal[2][0]=array1[2][0]*array2[0][0]+array1[2][1]*array2[1][0]+array1[2][2]*array2[2][0];
		Equal[2][1]=array1[2][0]*array2[0][1]+array1[2][1]*array2[1][1]+array1[2][2]*array2[2][1];
		Equal[2][2]=array1[2][0]*array2[0][2]+array1[2][1]*array2[1][2]+array1[2][2]*array2[2][2];
	}
	public static void prin(double[][] array1,double[][] array2,double[][] array3){ //print method
		for(int i=0;i<3;i++){
			for(int x=0;x<3;x++){
				System.out.print(array1[i][x]+" ");	
			}
			if(i==1){System.out.print(" * ");}
			System.out.print("\t");
						
			for(int x=0;x<3;x++){
				System.out.print(array2[i][x]+" ");	
			}
			if(i==1){System.out.print(" = ");}
			System.out.print("\t");			
			
			for(int x=0;x<3;x++){
				System.out.print(array3[i][x]+" ");
			}			
			System.out.print("\n");
		}
	}
	public static void main(String args[]){
		Scanner input=new Scanner(System.in);

		double fir[][]=new double[3][3];
		double sec[][]=new double[3][3];
		double Equal[][]=new double[3][3];
		System.out.print("Enter matrix1: ");
		run(fir);
		System.out.print("Enter matrix2: ");
		run(sec);
		multipleMatrix(fir,sec,Equal);
		System.out.println("the matrices are multiplied as follows : ");
		prin(fir,sec,Equal); //print the answer
	}
}