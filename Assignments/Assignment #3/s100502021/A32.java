package a3.s100502021;
import java.util.Scanner;
public class A32 {
	public static void main(String [] argv){
		Scanner input = new Scanner(System.in); 
		System.out.println("enter number from a to f : ");
		Double a=input.nextDouble();
		Double b=input.nextDouble();
		Double c=input.nextDouble();
		Double d=input.nextDouble();
		Double e=input.nextDouble();
		Double f=input.nextDouble();
		LinearEquation LinearEquation=new LinearEquation(a,b,c,d,e,f);
		
		if(LinearEquation.isSolvable()==true){ 
			System.out.print("answer : ");
			System.out.println("x = "+LinearEquation.getX());
			System.out.println("y = "+LinearEquation.getY());
		}
	}
}