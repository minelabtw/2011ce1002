package a3.s100502015;

public class LinearEquation 
{
	private static double a = 0;
	private static double b = 0;
	private static double c = 0;
	private static double d = 0;
	private static double e = 0;
	private static double f = 0;
	
	LinearEquation(double num1,double num2,double num3,double num4,double num5,double num6)
	{
		a=num1;
		b=num2;
		c=num3;
		d=num4;
		e=num5;
		f=num6;
	}
	public static boolean isSolvable()// Judgment 
	{
		if((a*d)-(b*c)!=0)
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	public static double getx()//calculate method
	{
		return (e*d - b*f)/(a*d - b*c);
	}
	public static double gety()
	{
		return (a*f - e*c)/(a*d - b*c);
	}
}
