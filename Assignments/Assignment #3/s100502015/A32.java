package a3.s100502015;
import a3.s100502015.LinearEquation;
import java.util.Scanner;
public class A32 
{
	public static void main(String[]args)
	{
		double [] store = new double[6];
		Scanner input = new Scanner(System.in);
		System.out.print("input:\n");
		for(int i=0;i<6;i++)
		{		
			store[i] = input.nextDouble(); 
		}
		LinearEquation test = new  LinearEquation(store[0],store[1],store[2],store[3],store[4],store[5]);
		if(test.isSolvable()==true)
		{
			System.out.print("output\n"+"x is :"+test.getx()+" y is :"+test.gety());
		}
		else
		{
			System.out.print("The equation has no solution");
		}
	}

}
