package a3.s100502015;
import java.util.Scanner;

public class A31 
{
	public static void main(String[] args)
	{
		java.text.DecimalFormat decimal = new java.text.DecimalFormat("###,##0.0");
		Scanner input = new Scanner(System.in);
		double [][] square = new double[3][3];
		double [][] square2 = new double[3][3];	
		double [][] answer = new double[3][3];
		
		System.out.print("Enter Matrix1:");
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{	
				square[i][j] = input.nextDouble();
			}
		}
		System.out.print("Enter Matrix:2");
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{	
				square2[i][j] = input.nextDouble();
			}
		}
		answer =  mutipleMatrix(square,square2);
		for(int i=0;i<3;i++)
		{			
			for(int j=0;j<3;j++)
			{
				System.out.print(square[i][j]+" ");	
			}
			
			if(i==1)
			{					
				System.out.print(" * ");					
			}
			System.out.print("\t");				
			for(int j=0;j<3;j++)
			{
				System.out.print(square2[i][j]+" ");	
			}	
								
			if(i==1)
			{
				System.out.print(" = ");
			}
			System.out.print("\t");
			for(int j=0;j<3;j++)
			{
				System.out.print(decimal.format(answer[i][j])+" ");	
			}				
			System.out.print("\n");
		}
		
	}
		
	
	public static double[][] mutipleMatrix(double[][]a,double[][]b)
	{
		double[][] answer = new double[3][3];
		
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)				
			{
				for(int k=0;k<3;k++)
				{									
					answer[i][j] += a[i][k]*b[k][j];					
				}
					
			}
		}
		return answer;
	}
}

