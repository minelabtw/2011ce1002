package A3.s995002020;
import java.util.Scanner;
public class A31 {
	public static void main(String []argv){
		double a[][] = new double [3][3];  //create a new matrix
		double b[][] = new double [3][3];  //create a new matrix
		double c[][] = new double [3][3];  //create a new matrix
		Scanner scanner = new Scanner(System.in);  //enter input 
		
		System.out.print("請輸入矩陣1的元素(用Space鍵隔開各數值)\n");  //input matrix1
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
			a[i][j]= scanner.nextDouble();  //read matrix1
		    }
	    }
		System.out.print("請輸入矩陣2的元素(用Space鍵隔開各數值)\n");   // input matrix2
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
			b[i][j]= scanner.nextDouble();	//read matrix2
			}
		}
		c=mulmatrix(a,b); // c = matrix1 * matrix2 
		System.out.print(c[0][0]+"\t"+c[0][1]+"\t"+c[0][2]+"\n"); //output c  
		System.out.print(c[1][0]+"\t"+c[1][1]+"\t"+c[1][2]+"\n");
		System.out.print(c[2][0]+"\t"+c[2][1]+"\t"+c[2][2]+"\n");
	}
		public static double[][] mulmatrix(double[][]a,double[][]b) //multiply method
		{
		  double result[][] = new double[3][3];
		   for(int i=0;i<3;i++)
			{
			for(int j=0;j<3;j++)
			{
		   for(int k=0;k<3;k++)    
		   {
			   result[i][j] = result[i][j] + a[i][k]*b[k][j];
		   }
			}
		}
		   return result;
		}
			
	
}
