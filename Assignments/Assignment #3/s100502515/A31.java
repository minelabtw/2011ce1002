package a3.s100502515;

import java.util.*;

public class A31 {
	public static void main(String[] args) {
		double[][] matrixone = new double[3][3];
		double[][] matrixtwo = new double[3][3];
		double[][] matrixthree = new double[3][3];// set two input matrices and
													// one result matrix
		Scanner input = new Scanner(System.in);
		java.text.DecimalFormat decimal = new java.text.DecimalFormat(
				"###,##0.0");

		System.out.println("Please input 9 elements to the first matrix:");
		for (int k = 0; k < 3; k++) {
			for (int l = 0; l < 3; l++) {
				matrixone[k][l] = input.nextDouble();
			}
		}
		System.out.println("Please input 9 elements to the second matrix:");
		for (int m = 0; m < 3; m++) {
			for (int n = 0; n < 3; n++) {
				matrixtwo[m][n] = input.nextDouble();
			}
		}

		matrixthree = multiple(matrixone, matrixtwo);

		for (int o = 0; o < 3; o++) {
			for (int p = 0; p < 3; p++) {
				System.out.print(decimal.format(matrixone[o][p]) + " ");
			}

			if (o == 1)
				System.out.print(" * ");

			System.out.print("\t");

			for (int q = 0; q < 3; q++) {
				System.out.print(decimal.format(matrixtwo[o][q]) + " ");
			}

			if (o == 1)
				System.out.print(" = ");

			System.out.print("\t");

			for (int r = 0; r < 3; r++) {
				System.out.print(decimal.format(matrixthree[o][r]) + " ");
			}

			System.out.print("\n");
		}

	}// Matrices' multiple is shown here

	public static double[][] multiple(double[][] one, double[][] two) {
		double[][] multiplematrix = new double[3][3];

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				multiplematrix[i][j] = one[i][0] * two[0][j] + one[i][1]
						* two[1][j] + one[i][2] * two[2][j];
			}
		}

		return multiplematrix;
	}// Matrices' multiple is done here
}
