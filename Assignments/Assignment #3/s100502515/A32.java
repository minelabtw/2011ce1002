package a3.s100502515;

import java.util.*;

public class A32 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double[] factors = new double[6];

		System.out.println("Input the factors:\nL1:");
		factors[0] = input.nextDouble();
		System.out.print("x+ ");
		factors[1] = input.nextDouble();
		System.out.print("y= ");
		factors[4] = input.nextDouble();
		System.out.println();
		System.out.print("L2:");
		factors[2] = input.nextDouble();
		System.out.print("x+ ");
		factors[3] = input.nextDouble();
		System.out.print("y= ");
		factors[5] = input.nextDouble();
		System.out.println();//Numbers inputting.

		LinearEquation linearEquation = new LinearEquation(factors[0],
				factors[1], factors[2], factors[3], factors[4], factors[5]);
		
		if (linearEquation.isSolvable() == false) {
			System.out.println("the answer x = " + linearEquation.getX() + " and y = "
					+ linearEquation.getY());
		} else {
			System.out.println("The two equations has no solution.");//The answer is shown
		}
	}
}
