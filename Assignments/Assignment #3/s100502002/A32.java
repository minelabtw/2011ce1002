package a3.s100502002;
import java.util.Scanner;
public class A32 {
	public static void main(String[] arg)
	{
		Scanner input = new Scanner(System.in);
		System.out.println("please enter 6 numbers for tne functions' a b c d e & f");
		System.out.println("the two functions will be ax+by=e and cx+dy=f");//show the message at beginning
		double A = input.nextDouble();
		double B = input.nextDouble();
		double C = input.nextDouble();
		double D = input.nextDouble();
		double E = input.nextDouble();
		double F = input.nextDouble();//enter 6 numbers to built two functions
		LinearEquation linear = new LinearEquation(A,B,C,D,E,F);
		System.out.println("your function 1 is:"+linear.geta()+" x+"+linear.getb()+"y = "+linear.gete());
		System.out.println("your function 2 is:"+linear.getc()+" x+ "+linear.getd()+"y = "+linear.getf());//I don't want to let the six methods useless
		boolean canornot = linear.isSolvable();//to know if the equation have solution
		if (canornot)
		{
			System.out.println("x = "+linear.getX());
			System.out.println("y = "+linear.getY());//show x and y answer
		}
			
		else
			System.out.println("The equation has no solution");//the message with no solution
	}

}
