package a3.s100502002;
import java.util.Scanner;
public class A31 {
	public static void main(String[] args)
	{
		java.util.Scanner input = new Scanner(System.in);
		double array1[][] = new double[3][3];
		double array2[][] = new double[3][3];
		double array3[][] = new double[3][3];//宣告三個陣列
		System.out.println("please enter 9 numbers for array1: ");
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				array1[i][j]=input.nextDouble();
			}
		}//輸入第一個陣列值
		System.out.println("please enter 9 numbers for array2: ");
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++) 
			{
				array2[i][j]=input.nextDouble();
			}
		}//輸入第二個陣列值
		array3 = multipleMatrix(array1,array2);//用第三個陣列存一二陣列相乘結果
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				System.out.print(array1[i][j]+" ");
			}
			if(i==1)
				System.out.print("\t*\t");
			else
				System.out.print("\t\t");
			for(int j=0;j<3;j++)
			{
				System.out.print(array2[i][j]+" ");
			}
			if(i==1)
				System.out.print("\t=\t");
			else
				System.out.print("\t\t");
			for(int j=0;j<3;j++)
			{
				System.out.print(array3[i][j]+" ");
			}
			
			System.out.print("\n");
			
		}
		
	}//印出過程與結果
	public static double[][] multipleMatrix( double[][] a, double[][] b)
	{
		double result[][]= new double [3][3];
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				result[i][j] = a[i][0]*b[0][j]+a[i][1]*b[1][j]+a[i][2]*b[2][j];
					
			}//矩陣的奇怪相乘
		}
		return result;
	}

}
