package a3.s100502002;

public class LinearEquation 
{
	private static double a;
	private static double b;
	private static double c;
	private static double d;
	private static double e;
	private static double f;//宣告變數
	LinearEquation(double n1,double n2,double n3,double n4,double n5,double n6)
	{
		a=n1;
		b=n2;
		c=n3;
		d=n4;
		e=n5;
		f=n6;
		
	}//設定初始值
	public static double geta()
	{
		return a;
	}
	public static double getb()
	{
		return b;
	}
	public static double getc()
	{
		return c;
	}
	public static double getd()
	{
		return d;
	}
	public static double gete()
	{
		return e;
	}
	public static double getf()
	{
		return f;
	}//六個沒用到的method
	public static boolean isSolvable()
	{
		boolean k = true;
		double temp = a*d - b*c;
		if(temp==0)
			k = false;
		return k;
	}//判定是否有解
	public static double getX()
	{
		double above = e*d - b*f;
		double below = a*d - b*c;
		double xans = above / below;
		return xans;
	}//計算x
	public static double getY()
	{
		double above = a*f - e*c;
		double below = a*d - b*c;
		double yans = above / below;
		return yans;
	}//計算y
}
