package a3.s100502010;
import java.util.Scanner;
import java.math.*;;
public class A31 
{
	public static void main(String[] args)
	{
		double array1[][]=new double[3][3];//DECLARE THREE ARRAYS
		double array2[][]=new double[3][3];
		double array3[][]=new double[3][3];
		Scanner input=new Scanner(System.in);
		
		System.out.print("Enter matrix1: ");//GET THE NUMBERS OF ARRAY1
		for(int counter1=0;counter1<=2;counter1++)
		{
			for(int counter2=0;counter2<=2;counter2++)
			{
				array1[counter1][counter2]=input.nextDouble();
			}
		}
		System.out.print("Enter matrix2: ");//GET THE NUMBERS OF ARRAY2
		for(int counter1=0;counter1<=2;counter1++)
		{
			for(int counter2=0;counter2<=2;counter2++)
			{
				array2[counter1][counter2]=input.nextDouble();
			}
		}
		System.out.println("The matrices are subtracted as follows: ");
		
		array3=subtractMatrices(array1,array2);//CALL METHOD AND PRINT THE ANSWER
		System.out.println((Math.round(array1[0][0]*10))/10.0+" "+(Math.round(array1[0][1]*10))/10.0+" "+(Math.round(array1[0][2]*10))/10.0+" \t"+(Math.round(array2[0][0]*10))/10.0+" "+(Math.round(array2[0][1]*10))/10.0+" "+(Math.round(array2[0][2]*10))/10.0+"\t"+(Math.round(array3[0][0]*10))/10.0+" "+(Math.round(array3[0][1]*10))/10.0+" "+(Math.round(array3[0][2]*10))/10.0);                             
		System.out.println((Math.round(array1[1][0]*10))/10.0+" "+(Math.round(array1[1][1]*10))/10.0+" "+(Math.round(array1[1][2]*10))/10.0+" *\t"+(Math.round(array2[1][0]*10))/10.0+" "+(Math.round(array2[1][1]*10))/10.0+" "+(Math.round(array2[1][2]*10))/10.0+" =\t"+(Math.round(array3[1][0]*10))/10.0+" "+(Math.round(array3[1][1]*10))/10.0+" "+(Math.round(array3[1][2]*10))/10.0);
		System.out.println((Math.round(array1[2][0]*10))/10.0+" "+(Math.round(array1[2][1]*10))/10.0+" "+(Math.round(array1[2][2]*10))/10.0+" \t"+(Math.round(array2[2][0]*10))/10.0+" "+(Math.round(array2[2][1]*10))/10.0+" "+(Math.round(array2[2][2]*10))/10.0+"\t"+(Math.round(array3[2][0]*10))/10.0+" "+(Math.round(array3[2][1]*10))/10.0+" "+(Math.round(array3[2][2]*10))/10.0);
	}
	public static double[][] subtractMatrices(double[][] a,double[][] b)//MMETHOD USED TO CALCULATE
	{
		double answer[][]=new double[3][3];
		for(int counter1=0;counter1<=2;counter1++)
		{
			for(int counter2=0;counter2<=2;counter2++)
			{
				answer[counter1][counter2]=a[counter1][0]*b[0][counter2]+a[counter1][1]*b[1][counter2]+a[counter1][2]*b[2][counter2];
			}
		}
		return answer;
	}
}