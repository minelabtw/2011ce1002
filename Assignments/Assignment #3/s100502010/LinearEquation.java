package a3.s100502010;
public class LinearEquation 
{
	private double a;
	private double b;
	private double c;
	private double d;	
	private double e;
	private double f;
	LinearEquation()
	{
		a=1;
		b=2;
		c=3;
		d=4;
		e=5;
		f=6;
	}
	public LinearEquation(double g,double h,double i,double j,double k,double l)
	{
		a=g;//give numbers to the variables
		b=h;
		c=i;
		d=j;
		e=k;
		f=l;
	}
	public void isSolvable()//check if
	{
		if(a*d-b*c==0)//if the equation has no solution
		{
			System.out.println("output");
			System.out.println("The equation has no solution");
		}
		else//if there is a solution print the answer
		{
			double answerx=getX();
			double answery=getY();
			System.out.print("x = "+answerx+" "+"y = "+answery);
		}
	}
	public double getX()//calculate x
	{
		double x=(e*d-b*f)/(a*d-b*c);
		return x;
	}
	public double getY()//calculate y
	{
		double y=(a*f-e*c)/(a*d-b*c);
		return y;
	}
}