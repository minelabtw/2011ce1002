package a3.s100502033;
import java.util.Scanner;
import javax.swing.JOptionPane;
public class A31{

	public static void main(String[] argv)
	{
		double [][] array = new double[3][3];
		double [][] array1= new double[3][3];
		System.out.print("Enter matrix1: ");
		for ( int i = 0 ; i < 3 ; i++)
		{
			for ( int j = 0 ; j < 3 ; j++)
			{
				String number = JOptionPane.showInputDialog("請輸入A組一個數值");
				Double number1 = Double.parseDouble(number);//將STRING轉換成DOUBLE
				array[i][j] = number1;//將輸入的數值傳回陣列
				System.out.print(array[i][j] + "   ");
				
			}
		}
		System.out.println("");
		System.out.print("Enter matrix2: ");
		for ( int k = 0 ; k < 3 ; k++)
		{
			for (int z = 0 ; z < 3 ; z++)
			{
				String number2 = JOptionPane.showInputDialog("請輸入B組一個數值");
				Double number3 = Double.parseDouble(number2);//將STRING轉換成DOUBLE
				array1[k][z] = number3;//將輸入的數值傳回陣列
				System.out.print(array1[k][z] + "   ");
			}
		}
		System.out.println("");
		System.out.println("The matrices are subtracted as follows: ");
		
		subtractMatrices(array , array1);
	}


	public static double [][] subtractMatrices(double[][] a , double[][] b)
	{
		double [][] c = new double[3][3];
		for (int i = 0 ; i < 3 ; i ++)
		{
			for (int k = 0 ; k < 3 ; k++)
			{
				if(k == 2 & i == 1)
				{
					System.out.print(a[i][k] + "  *\t" );//為了格式才做
				}
				else
				{
					System.out.print(a[i][k] + "\t" );//為了格式才做
				}
			}
			for (int z = 0 ; z < 3 ; z++)
			{
				if(z == 2 & i == 1)
				{
					System.out.print(b[i][z] + "  =\t" );//為了格式才做
				}
				else
				{
					System.out.print(b[i][z] + "\t" );//為了格式才做
				}
			}
			for (int j = 0 ; j < 3 ; j++)
			{
				c[i][j] = a[i][j] * b[i][j] ;
				System.out.print(c[i][j] + "\t");
			}
			System.out.println("");
		}
		return c; //因為一定要RETURN，所以隨便回傳一個值
	}




}