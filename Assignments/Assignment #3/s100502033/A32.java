package a3.s100502033;
import javax.swing.JOptionPane;
public class A32 
{
	public static void main(String[] argv)
	{
		for ( int j = 0 ; j < 2 ; j--)
		{
			double a = 0;
			double b = 0;
			double c = 0;
			double d = 0;
			double e = 0;
			double f = 0;
			String name;
			for (int i = 0 ; i < 6 ; i++)//這個FOR是為了訊息的標題才做的和將使用者傳入的值傳回a b c d e f 
			{
				if ( i == 0)
				{
					name = "a";
					String message = String.format("請輸入%s的值", name);
					String number = JOptionPane.showInputDialog(message);//使用者傳入a的值
					Double number1 = Double.parseDouble(number);
					a = number1;
				}
				else if(i == 1)
				{
					name = "b";
					String message = String.format("請輸入%s的值", name);
					String number = JOptionPane.showInputDialog(message);//使用者傳入b的值
					Double number1 = Double.parseDouble(number);
					b = number1;
				}
				else if(i == 2)
				{
					name = "c";
					String message = String.format("請輸入%s的值", name);
					String number = JOptionPane.showInputDialog(message);//使用者傳入c的值
					Double number1 = Double.parseDouble(number);
					c = number1;
				}
				else if(i == 3)
				{
					name = "d";
					String message = String.format("請輸入%s的值", name);
					String number = JOptionPane.showInputDialog(message);//使用者傳入d的值
					Double number1 = Double.parseDouble(number);
					d = number1;
				}
				else if(i == 4)
				{
					name = "e";
					String message = String.format("請輸入%s的值", name);
					String number = JOptionPane.showInputDialog(message);//使用者傳入e的值
					Double number1 = Double.parseDouble(number);
					e = number1;
				}
				else if(i == 5)
				{
					name = "f";
					String message = String.format("請輸入%s的值", name);
					String number = JOptionPane.showInputDialog(message);//使用者傳入f的值
					Double number1 = Double.parseDouble(number);
					f = number1;
				}
			}
			System.out.printf("input\n" + a + " " + b + " " + c + " " + d + " " + e + " " + f + "\n");
			LinearEquation linearEquation = new LinearEquation(a , b , c , d , e , f);//顯示初台值
			linearEquation.isSolvable();//判段分母是否等於0
			System.out.printf("\noutput\n" + "x is " + linearEquation.getX());
			System.out.println(" and y is " + linearEquation.getY());
			String number2 = JOptionPane.showInputDialog("如果要離開請輸入數字「0」，繼續請輸入「1」");//讓使用者選擇繼續或離開
			Double number3 = Double.parseDouble(number2);
			if(number3 == 0)
			{
				JOptionPane.showInputDialog("多謝你的使用");
				System.exit(0);
			}
			else if(number3 == 1)
			{
				continue;
			}
		}
	}
}
