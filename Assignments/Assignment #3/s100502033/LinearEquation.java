package a3.s100502033;
import javax.swing.JOptionPane;
public class LinearEquation 
{

	private static double a = 0;
	private static double b = 0;
	private static double c = 0;
	private static double d = 0;
	private static double e = 0;
	private static double f = 0;
	public LinearEquation(double a1 , double b1 , double c1 , double d1 , double e1 , double f1)
	{
		a = a1;
		b = b1;
		c = c1;
		d = d1;
		e = e1;
		f = f1;
		//end constructor
	}
	public static void setVariable(double a1 , double b1 , double c1 , double d1 , double e1 , double f1)
	{
		a = a1;
		b = b1;
		c = c1;
		d = d1;
		e = e1;
		f = f1;
	}
	public static double getX()
	{
		return ((e * d)-(b * f))/((a * d)-(b * c)); //公式
	}
	public static double getY()
	{
		return ((a * f)-(e * c))/((a * d)-(b * c));//公式
	}
	public static void isSolvable()
	{
		if(a * d - b * c == 0)//判段分母是否等於0
		{
			JOptionPane.showMessageDialog(null, "The equation has no solution.");
			System.exit(0);
		}
	}
}
