package a3.s100502030;

import java.util.Scanner;

public class A32 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("input");
		// 輸入a~f之值
		double a = input.nextDouble();
		double b = input.nextDouble();
		double c = input.nextDouble();
		double d = input.nextDouble();
		double e = input.nextDouble();
		double f = input.nextDouble();
		LinearEquation linearEquation = new LinearEquation(a, b, c, d, e, f);
		if (linearEquation.isSolvable())//判斷是否有唯一解
		{
			System.out.println("x is " + linearEquation.getX() + " and y is "
					+ linearEquation.getY());
		}
		else 
		{
			if (e == f)// 判斷是無解或無線多解
				System.out.println("The equation has limitless solution.");
			else
				System.out.println("The equation has no solution.");
		}
	}
}
