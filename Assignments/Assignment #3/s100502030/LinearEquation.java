package a3.s100502030;

public class LinearEquation {
	private double a,b,c,d,e,f;
	
	LinearEquation(double seta,double setb,double setc,double setd,double sete,double setf)
	{
		a=seta;
		b=setb;
		c=setc;
		d=setd;
		e=sete;
		f=setf;
	}
	
	double geta()
	{
		return a;
	}
	
	double getb()
	{
		return b;
	}
	
	double getc()
	{
		return c;
	}
	
	double getd()
	{
		return d;
	}
	
	double gete()
	{
		return e;
	}
	
	double getf()
	{
		return f;
	}
	
	boolean isSolvable()
	{
		if((a*d-b*c)==0)
			return false;
		else
			return true;
	}
	
	double getX()
	{
		return (e*d - b*f)/(a*d - b*c);
	}
	
	double getY()
	{
		return (a*f - e*c)/(a*d - b*c);
	}
}
