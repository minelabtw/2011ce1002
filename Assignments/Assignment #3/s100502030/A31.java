package a3.s100502030;

import java.util.Scanner;

public class A31 {
	public static double[][] multipleMatrix(double[][] a, double[][] b) {
		double[][] arrayMultiple = new double[3][3];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				arrayMultiple[i][j] = a[i][0] * b[0][j] + a[i][1] * b[1][j]
						+ a[i][2] * b[2][j];
			}
		}
		return arrayMultiple;
	}// 將兩矩陣相乘

	public static void main(String[] args) {
		double[][] array1 = new double[3][3];
		double[][] array2 = new double[3][3];
		double[][] array3 = new double[3][3];
		Scanner input = new Scanner(System.in);
		System.out.println("input\n");
		System.out.println("Enter matrix1: ");
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				array1[i][j] = input.nextDouble();
			}
		}// 輸入數值到array1
		System.out.println("Enter matrix2: ");
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				array2[i][j] = input.nextDouble();
			}
		}// 輸入數值到array2
		array3 = multipleMatrix(array1, array2);
		System.out.println("\noutput\n");
		System.out.println("the matrices are multiplied as follows: ");
		for (int i = 0; i < 3; i++) {
			String pattern1 = "\t";
			String pattern2 = "\t";
			if (i == 1) {
				pattern1 = "+\t";
				pattern2 = "=\t";
			}// 第2列出現  * , =  
			for (int j = 0; j < 3; j++) {
				System.out.print(array1[i][j] + "\t");
			}// 輸出array1第i列
			System.out.print(pattern1);
			for (int j = 0; j < 3; j++) {
				System.out.print(array2[i][j] + "\t");
			}// 輸出array2第i列
			System.out.print(pattern2);
			for (int j = 0; j < 3; j++) {
				System.out.printf("%5.2f",array3[i][j]);
				System.out.print("\t");
			}// 輸出array3第i列
			System.out.print("\n");
		}
	}
}
