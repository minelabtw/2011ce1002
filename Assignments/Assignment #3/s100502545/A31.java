package a3.s100502545;

import java.util.Scanner;


public class A31 {

	public static void main (String[] args)
	{
		Scanner input = new Scanner(System.in);
		//先宣告三個陣列 3x3
		double arrayA [][] = new double [3][3];
		double arrayB [][] = new double [3][3];
		double arrayC [][] = new double [3][3];
		
		System.out.print("Enter nine numbers:");
		//使用者在arrayA輸入九個數字 
		for(int i=0 ;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				arrayA[i][j]= input.nextDouble();
			}
		}
		
		System.out.print("Enter nine numbers:");
		////使用者在arrayB輸入九個數字 
		for(int i=0 ;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				arrayB[i][j]= input.nextDouble();
			}
		}
		//在function multipleMatrix 計算相乘 回傳至arrayC
		arrayC = multipleMatrix(arrayA,arrayB);
		//輸出結果
		System.out.println(arrayA[0][0]+"\t"+arrayA[0][1]+"\t"+arrayA[0][2]+"\t"+arrayB[0][0]+"\t"+arrayB[0][1]+"\t"+arrayB[0][2]+"\t"+arrayC[0][0]+"\t"+arrayC[0][1]+"\t"+arrayC[0][2]);
		System.out.println(arrayA[1][0]+"\t"+arrayA[1][1]+"\t"+arrayA[1][2]+"  *  "+arrayB[1][0]+"\t"+arrayB[1][1]+"\t"+arrayB[1][2]+"  =  "+arrayC[1][0]+"\t"+arrayC[1][1]+"\t"+arrayC[1][2]);
		System.out.println(arrayA[2][0]+"\t"+arrayA[2][1]+"\t"+arrayA[2][2]+"\t"+arrayB[2][0]+"\t"+arrayB[2][1]+"\t"+arrayB[2][2]+"\t"+arrayC[2][0]+"\t"+arrayC[2][1]+"\t"+arrayC[2][2]);
		
		
	}
	
	public static double[][] multipleMatrix( double[][]	A, double[][] B)
	{//計算arrayA,arrayB的相乘結果
		double answer[][] = new double[3][3];
			
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				for(int k=0;k<3;k++)
				{
					answer[i][j] += A[i][k]*B[k][j] ;
					answer[i][j]=Math.round(answer[i][j]*10.0)/10.0;
					
				}
			}
				
		}
		
		return answer;
		
	}
	
}

