package a3.s100502545;

import java.util.Scanner;

public class A32
{
	public static void main (String[] args )
	{
		Scanner input = new Scanner(System.in);
		System.out.print("Enter six numbers:");
		
		//宣告使用者輸入的六個double變數
		double a = input.nextDouble();
		double b = input.nextDouble();
		double c = input.nextDouble();
		double d = input.nextDouble();
		double e = input.nextDouble();
		double f = input.nextDouble();
		
		
		LinearEquation Linear=new LinearEquation(a,b,c,d,e,f);
		
		//如果符合條件印出答案
		if(Linear.issolvable()=="true")
		{
			System.out.print("x is "+Linear.getX (a, b, c, d, e, f) +" and ");
			System.out.println("y is:"+Linear.getY (a, b, c, d, e, f));
		}
		//條件不符
		else
			System.out.println(Linear.issolvable());
	}
}

