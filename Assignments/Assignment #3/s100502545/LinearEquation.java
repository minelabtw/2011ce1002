package a3.s100502545;

public class LinearEquation
{ 		
	//Private double  a, b, c, d, e, f.
	private static double a;
	private	static double b;
	private static double c;
	private	static double d;
	private	static double e;
	private	static double f;
	
	//constructor
	LinearEquation(double h,double i,double j,double k,double l,double m)
	{
	   a=h;
	   b=i;
	   c=j;
	   d=k;
	   e=l;
	   f=m;
		
	}
	

	//getX() 計算方法
	public static double getX (double h,double i,double j,double k,double l,double m)
	{
		return (e*d-b*f)/(a*d-b*c);
	}
	 //getY()計算方法
	public static double getY (double h,double i,double j,double k,double l,double m)
	{
		return (a*f-e*c)/(a*d-b*c);
	}
	//issolvable()判斷不等於0符合條件,等於0就不符合條件
	public static String issolvable()
	{
		if((a*d-b*c)!=0)
		{
			return "true";
		}
		else 
			return "The equation has no solution!";
	}
}
