package a3.s100502511;

import java.util.Scanner;

public class A31 {
	public static double[][] multipleMatrix(double[][] a, double[][] b) { // 兩陣列相乘
		double c[][] = new double[3][3];
		for (int row = 0; row < 3; row++) {
			for (int column = 0; column < 3; column++) {
				c[row][column] = a[row][0] * b[0][column] + a[row][1]
						* b[1][column] + a[row][2] * b[2][column];
			}
		}
		return c;
	}

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double matrix1[][] = new double[3][3];
		double matrix2[][] = new double[3][3];
		System.out.println("input\n");
		System.out.print("Enter matrix1: ");
		for (int row = 0; row < matrix1.length; row++) { // 輸入陣列1的值
			for (int column = 0; column < matrix1[row].length; column++) {
				matrix1[row][column] = input.nextDouble();
			}
		}
		System.out.print("Enter matrix2: ");
		for (int row = 0; row < matrix2.length; row++) { // 輸入陣列2的值
			for (int column = 0; column < matrix2[row].length; column++) {
				matrix2[row][column] = input.nextDouble();
			}
		}
		System.out.print("\n");
		double[][] matrix3 = multipleMatrix(matrix1, matrix2); // 相乘

		System.out.println("output\n");
		System.out.println("the matrices are multiplied as follows: ");

		for (int row = 0; row < 3; row++) {
			for (int column1 = 0; column1 < 3; column1++) {
				System.out.print(matrix1[row][column1] + " ");
			}
			if (row == 2) {
				System.out.print("\t");
			}
			if (row == 0) {
				System.out.print("\t");
			}
			System.out.print("\t");
			if (row == 1) {
				System.out.print("*\t");
			}
			for (int column2 = 0; column2 < 3; column2++) {
				System.out.print(matrix2[row][column2] + " ");
			}
			if (row == 0) {
				System.out.print("\t\t");
			}
			if (row == 1) {
				System.out.print("\t=\t");
			}
			if (row == 2) {
				System.out.print("\t\t");
			}
			for (int column3 = 0; column3 < 3; column3++) {
				System.out.printf("%.1f ", matrix3[row][column3]); // 把小數點設為1位
			}
			System.out.println();
		}
	}
}
