package a3.s100502511;

public class LinearEquation {
	private double a, b, c, d, e, f;

	LinearEquation(double A, double B, double C, double D, double E, double F) { // 將輸入的值帶入參數中
		a = A;
		b = B;
		c = C;
		d = D;
		e = E;
		f = F;
	}

	boolean isSolvable() { // 判斷有解or無解
		if ((a * d) - (b * c) == 0) {
			return false;
		} else {
			return true;
		}
	}

	double getX() { // 解X
		double Xanswer = (e * d - b * f) / (a * d - b * c);
		return Xanswer;
	}

	double getY() { // 解Y
		double Yanswer = (a * f - e * c) / (a * d - b * c);
		return Yanswer;
	}

}
