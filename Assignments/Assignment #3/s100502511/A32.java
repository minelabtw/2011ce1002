package a3.s100502511;

import java.util.Scanner;

public class A32 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("input");
		double num1 = input.nextDouble();
		double num2 = input.nextDouble();
		double num3 = input.nextDouble();
		double num4 = input.nextDouble();
		double num5 = input.nextDouble();
		double num6 = input.nextDouble();
		LinearEquation answer = new LinearEquation(num1, num2, num3, num4, // 將輸入的數帶入class
																			// LinearEquation
				num5, num6);
		System.out.println("output");
		if (answer.isSolvable() == true) { // 等於true 有解
			System.out.println("x is " + answer.getX() + " and y is "
					+ answer.getY());
		} else { // 等於false 無解
			System.out.print("The equation has no solution");
		}
	}
}
