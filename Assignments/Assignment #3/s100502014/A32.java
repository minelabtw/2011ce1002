package a3.s100502014;
import java.util.Scanner;

public class A32 {
	public static void main(String[] args) {
		double a;
		double b;
		double c;
		double d;
		double e;
		double f;
		Scanner input = new Scanner(System.in);
		
		//input six numbers
		System.out.print("Input: ");
		a = input.nextDouble();
		b = input.nextDouble();
		c = input.nextDouble();
		d = input.nextDouble();
		e = input.nextDouble();
		f = input.nextDouble();
		
		//create object
		LinearEquation le = new LinearEquation(a,b,c,d,e,f);
		
		//show linear equation
		System.out.println("Your linear equation is:\n" +
				le.getA() + "x + " + le.getB() + "y = " + le.getE() + "\n" +
				le.getC() + "x + " + le.getD() + "y = " + le.getF() + "\n\n" +
				"And your anwer is:");
		
		//show answer
		switch(le.isSolvable()) {
			//solvable solution
			case 1:
				System.out.println("x is " + le.getX() + "and y is " + le.getY());
				break;
				
			//multiple solutions
			case 0:
				System.out.println("The equation has multiple solutions.");
				break;
				
			//no solution
			case -1:
				System.out.println("The equation has no solution.");
				break;
			default:
				System.out.println("Error!!!");
				break;
		}
	}
}
