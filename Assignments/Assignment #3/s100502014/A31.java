package a3.s100502014;
import java.util.Scanner;

public class A31 {
	public static void main(String[] args) {
		double[][] a = new double[3][3];
		double[][] b = new double[3][3];
		Scanner input = new Scanner(System.in);
		
		//enter matrix1
		System.out.print("Enter matrix1: ");
		for(int i=0;i<3;i++) {
			for(int j=0;j<3;j++) {
				a[i][j] = input.nextDouble();
			}
		}
		
		//enter matrix2
		System.out.print("Enter matrix2: ");
		for(int i=0;i<3;i++) {
			for(int j=0;j<3;j++) {
				b[i][j] = input.nextDouble();
			}
		}
		
		//show matrices
		System.out.println("the matrices are multiplied as follows:");
		for(int i=0;i<3;i++) {
			//matrix1
			for(int j=0;j<3;j++) {
				System.out.print(a[i][j]+"\t");
			}
			if(i!=1)
				System.out.print("\t");
			else
				System.out.print("*\t");
			
			//matrix2
			for(int j=0;j<3;j++) {
				System.out.print(b[i][j]+"\t");
			}
			if(i!=1)
				System.out.print("\t");
			else
				System.out.print("=\t");

			//the answer
			for(int j=0;j<3;j++) {
				System.out.print(multipleMatrix(a,b)[i][j]+"\t");
			}
			System.out.print("\n");
		}
	}
	
	//multiple matrix
	public static double[][] multipleMatrix(double[][] a, double[][] b) {
		double[][] c = new double[3][3];
		for(int i=0;i<3;i++) {
			for(int j=0;j<3;j++) {
				for(int k=0;k<3;k++) {
					c[i][j] += ( a[i][k] * b[k][j] );	//left row multiple right column
				}
			}
		}
		return c;
	}
}
