package a3.s100502004;

import java.util.Scanner;

public class A31 {
	public static double[][] multipleMatrix(double[][] a, double[][] b) {// 計算正列質
		double[][] ans = new double[3][3];
		ans[0][0] = a[0][0] * b[0][0] + a[0][1] * b[1][0] + a[0][2] * b[2][0];
		ans[0][1] = a[0][0] * b[0][1] + a[0][1] * b[1][1] + a[0][2] * b[2][1];
		ans[0][2] = a[0][0] * b[0][2] + a[0][1] * b[1][2] + a[0][2] * b[2][2];
		ans[1][0] = a[1][0] * b[0][0] + a[1][1] * b[1][0] + a[1][2] * b[2][0];
		ans[1][1] = a[1][0] * b[0][1] + a[1][1] * b[1][1] + a[1][2] * b[2][1];
		ans[1][2] = a[1][0] * b[0][2] + a[1][1] * b[1][2] + a[1][2] * b[2][2];
		ans[2][0] = a[2][0] * b[0][0] + a[2][1] * b[1][0] + a[2][2] * b[2][0];
		ans[2][1] = a[2][0] * b[0][1] + a[2][1] * b[1][1] + a[2][2] * b[2][1];
		ans[2][2] = a[2][0] * b[0][2] + a[2][1] * b[1][2] + a[2][2] * b[2][2];
		return ans;
	}

	public static void main(String[] args) {
		double[][] a = new double[3][3];// 宣告正列及個數
		double[][] b = new double[3][3];
		double[][] finans = new double[3][3];
		Scanner input = new Scanner(System.in);
		System.out.println("Enter matrix1: ");// 輸入正烈一
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				a[i][j] = input.nextDouble();
			}
		}

		System.out.println("Enter matrix2: ");// 輸入正列2
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				b[i][j] = input.nextDouble();
			}
		}

		System.out.println("The matrices are multiplied as follow: ");
		finans = multipleMatrix(a, b);// 計算
		for (int i = 0; i < 3; i++) {// 以下為顯示結果
			System.out.printf("%.1f ", a[0][i]);
		}
		System.out.print("\t");
		for (int i = 0; i < 3; i++) {
			System.out.printf("%.1f ", b[0][i]);
		}
		System.out.print("\t");
		for (int i = 0; i < 3; i++) {
			System.out.printf("%.1f ", finans[0][i]);
		}

		System.out.print("\n");
		for (int i = 0; i < 3; i++) {
			System.out.printf("%.1f ", a[1][i]);
		}
		System.out.print(" *  ");
		for (int i = 0; i < 3; i++) {
			System.out.printf("%.1f ", b[1][i]);
		}
		System.out.print(" =  ");
		for (int i = 0; i < 3; i++) {
			System.out.printf("%.1f ", finans[1][i]);
		}
		System.out.print("\n");
		for (int i = 0; i < 3; i++) {
			System.out.printf("%.1f ", a[2][i]);
		}
		System.out.print("\t");
		for (int i = 0; i < 3; i++) {
			System.out.printf("%.1f ", b[2][i]);
		}
		System.out.print("\t");
		for (int i = 0; i < 3; i++) {
			System.out.printf("%.1f ", finans[2][i]);
		}

	}

}
