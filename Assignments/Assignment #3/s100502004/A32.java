package a3.s100502004;

import java.util.Scanner;

public class A32 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double[] x = new double[4];
		double[] y = new double[2];
		double ans;
		boolean check;
		System.out.println("input");// 輸入數字a,b,c,d
		for (int i = 0; i < 4; i++) {
			x[i] = input.nextDouble();
		}
		for (int i = 0; i < 2; i++) {// 輸入數字e,f
			y[i] = input.nextDouble();
		}

		LinearEquation lin = new LinearEquation(x[0], x[1], x[2], x[3], y[0],y[1]);// 設定初始值
		ans = lin.calculate(x[0], x[1], x[2], x[3]);// 計算分母
		lin.setX();// 設定x
		lin.setY();// 設定y
		check = lin.isSolvable(ans);// 確認是否可以解題
		if (check) {// 輸出答案及結果
			System.out.println("x is " + lin.getX() + " and y is " + lin.getY());
		} else {
			System.out.println("The equation has no solution.");
		}

	}

}
