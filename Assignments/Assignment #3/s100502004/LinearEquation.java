package a3.s100502004;

public class LinearEquation {
	private double a;//設定一些private數
	private double b;
	private double c;
	private double d;
	private double e;
	private double f;
	private double x;
	private double y;
	double under;//確認分母
	LinearEquation(double a,double b,double c,double d,double e,double f){//輸入變數
		this.a=a;
		this.b=b;
		this.c=c;
		this.d=d;
		this.e=e;
		this.f=f;
	}
	
	public double getA(){//用來取得變數
		return a;
	}
	public double getB(){
		return b;
	}
	public double getC(){
		return c;
	}
	public double getD(){
		return d;
	}
	public double getE(){
		return e;
	}
	public double getF(){
		return f;
	}
	public double calculate(double a,double b,double c,double d){//用來確認分母的數字
		under=a*d-b*c;		
		return under;
	}
	
	public void setX(){//計算x的值
		x=(e*d-b*f)/(a*d-b*c);		
	}
	public void setY(){//計算y的值
		y=(a*f-e*c)/(a*d-b*c);			
	}
	
	
	
	public boolean isSolvable(double ans){//用來確認是否可以解題
		if(ans!=0){
			return true;
		}
		else{	
			return  false;
		}		
	}
	
	public double getX(){//取得x值
		return x;
	}
	
	public double getY(){//取得y值
		return y;
	}
	
	
	
	
	
}
