package a3.s100502027;

import java.util.Scanner;

public class A31{
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);
		double[][] matrice1=new double[3][3];
		double[][] matrice2=new double[3][3];
		double[][] matrice3=new double[3][3];
		System.out.println("input");
		System.out.println("");
		System.out.println("Enter matrix1: ");
		for(int r=0;r<3;r++){
			for(int c=0;c<3;c++){
				matrice1[r][c]=input.nextDouble();
			}
		}
		System.out.println("Enter matrix2: ");
		for(int r=0;r<3;r++){
			for(int c=0;c<3;c++){
				matrice2[r][c]=input.nextDouble();
			}
		}
		System.out.println("");
		matrice3=multipleMatrix(matrice1,matrice2);
		System.out.println("output");
		System.out.println("");
		System.out.println("the matrices are multiplied as follows:");
		for(int r=0;r<3;r++){ //a lot of [for] to set the output format
			for(int c=0;c<3;c++){
				System.out.print(matrice1[r][c]+"\t");
			}
			if(r==1){ //due to the [*],just control the only row to output
				System.out.print("*\t");
			}
			else{
				System.out.print("\t");
			}
			for(int c=0;c<3;c++){
				System.out.print(matrice2[r][c]+"\t");
			}
			if(r==1){  //due to the [=],just control the only row to output
				System.out.print("=\t");
			}
			else{
				System.out.print("\t");
			}
			for(int c=0;c<3;c++){
				System.out.printf("%4.1f\t",matrice3[r][c]);
			}
			System.out.println("");
		}
		
	}
	public static double[][] multipleMatrix( double[][] a, double[][] b){
		double[][] countresult=new double[3][3];
		for(int r=0;r<3;r++){
			for(int c=0;c<3;c++){
				countresult[r][c]=0;
				for(int t=0;t<3;t++){
					countresult[r][c]+=a[r][t]*b[t][c]; ////use the formula and [if] to input the result to the array
				}
			}
		}
		return countresult;
	}
}