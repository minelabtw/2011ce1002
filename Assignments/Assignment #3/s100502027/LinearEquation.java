package a3.s100502027;

public class LinearEquation {
	public LinearEquation(double A,double B,double C,double D,double E,double F){//construct the class 
		a=A;
		b=B;
		c=C;
		d=D;
		e=E;
		f=F;
	}
	private double a,b,c,d,e,f; //private data field 
	double geta(){
		return a;
	}
	double getb(){
		return b;
	}
	double getc(){
		return c;
	}
	double getd(){
		return d;
	}
	double gete(){
		return e;
	}
	double getf(){
		return f;
	}
	boolean  isSolvable(){  //count the ad-bc ,then return the result
		if(a*d-b*c!=0){
			return true;
		}
		else{
			return false;
		}
	}
	double getX(){  //use the formula to count x
		double x = (e*d-b*f)/(a*d-b*c);
		return x;
	}
	double getY(){  //use the formula to count y
		double y = (a*f-e*c)/(a*d-b*c);
		return y;
	}
}
