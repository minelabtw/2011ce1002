package a3.s100502027;
import java.util.Scanner;
public class A32 {
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);
		System.out.println("input");
		double a= input.nextDouble();
		double b= input.nextDouble();
		double c= input.nextDouble();
		double d= input.nextDouble();
		double e= input.nextDouble();
		double f= input.nextDouble();
		LinearEquation test =new LinearEquation(a,b,c,d,e,f); //declared the class
		System.out.println("output");
		System.out.println( test.geta() + "x + " + test.getb() + "y = " + test.getc()); //set the format
		System.out.println( test.getd() + "x + " + test.gete() + "y = " + test.getf());
		System.out.println("");
		if(test.isSolvable()){
			System.out.println("x is "+test.getX()+" and y is "+test.getY()); //Respond the true or false to output the result
		}
		else{
			System.out.println("The equation has no solution");
		}
	}
}