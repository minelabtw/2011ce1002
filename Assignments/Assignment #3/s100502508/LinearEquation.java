package a3.s100502508;

public class LinearEquation 
{
	private static double a;// declare the attributes of this class
	private static double b;
	private static double c;
	private static double d;
	private static double e;
	private static double f;
	LinearEquation(double ReceiveA,double ReceiveB,double ReceiveC,double ReceiveD,double ReceiveE,double ReceiveF)//initialize the values
	{
		a=ReceiveA;
		b=ReceiveB;
		c=ReceiveC;
		d=ReceiveD;
		e=ReceiveE;
		f=ReceiveF;
	}//end LinearEquation constructor
	boolean isSolvable()//judge if a*d is equal to b*c
	{
		if(a*d!=b*c)
		{
			return true;
		}
		else
		{
			return false;
		}
	}//end isSolvable
	double getX()//get value of X
	{
		return (e*d-b*f)/(a*d-b*c);
	}//end getX
	double getY()//get value of Y
	{
		return (a*f-e*c)/(a*d-b*c);
	}//end getY
}
