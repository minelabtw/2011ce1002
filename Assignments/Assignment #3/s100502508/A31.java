package a3.s100502508;

import java.util.Scanner;
public class A31 
{
	public static void main(String[] args)
	{
		double[][] array1=new double[3][3];//declare three arrays
		double[][] array2=new double[3][3];
		double[][] result=new double[3][3];
		Scanner input=new Scanner(System.in);//new a Scanner object
		System.out.println("input");
		System.out.print("\nEnter matrix1: ");
		for(int a=0;a<3;a++)//make nine numbers save in the array1
		{
			for(int b=0;b<3;b++)
			{
				array1[a][b]=input.nextDouble();
			}//end for
		}//end for
		System.out.print("Enter matrix2: ");
		for(int c=0;c<3;c++)//make nine numbers save in the array2
		{
			for(int d=0;d<3;d++)
			{
				array2[c][d]=input.nextDouble();
			}//end for
		}//end for
		System.out.println("\n\noutput\n\n");
		System.out.println("the matrices are multiplied as follows: ");
		for(int g=0;g<3;g++)//display the result
		{
			for(int h=0;h<3;h++)
			{
				System.out.print(array1[g][h]+"\t");
			}//end for
			if(g==1)
			{
				System.out.print("*\t");
			}
			else
			{
				System.out.print("\t");
			}
			for(int i=0;i<3;i++)
			{
				System.out.print(array2[g][i]+"\t");
			}//end for
			if(g==1)
			{
				System.out.print("=");
			}
			else
			{
				System.out.print(" ");
			}
			result=multipleMatrix(array1,array2);//call subtractMatrices method
			for(int j=0;j<3;j++)
			{
				System.out.printf("\t%.1f",result[g][j]);
			}//end for
			System.out.print("\n");
		}//end for
	}//end main
	public static double[][] multipleMatrix(double[][] a,double[][] b)//array1*array2
	{
		double[][] c=new double[3][3];
		for(int f=0;f<3;f++)
		{
			int e=0;
			c[e][e]=c[e][e]+a[e][f]*b[f][e];//calculate every value of matrix
			c[e][e+1]=c[e][e+1]+a[e][f]*b[f][e+1];
			c[e][e+2]=c[e][e+2]+a[e][f]*b[f][e+2];
			c[e+1][e]=c[e+1][e]+a[e+1][f]*b[f][e];
			c[e+1][e+1]=c[e+1][e+1]+a[e+1][f]*b[f][e+1];
			c[e+1][e+2]=c[e+1][e+2]+a[e+1][f]*b[f][e+2];
			c[e+2][e]=c[e+2][e]+a[e+2][f]*b[f][e];
			c[e+2][e+1]=c[e+2][e+1]+a[e+2][f]*b[f][e+1];
			c[e+2][e+2]=c[e+2][e+2]+a[e+2][f]*b[f][e+2];
			e++;
		}//end for
		return c;
	}//end multipleMatrix method
}
