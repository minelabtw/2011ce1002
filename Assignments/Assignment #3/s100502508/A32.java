package a3.s100502508;

import java.util.Scanner;
public class A32 
{
	public static void main(String[] args)
	{
		Scanner input=new Scanner(System.in);//new a Scanner object
		System.out.println("input");
		double A=input.nextDouble();//enter six numbers
		double B=input.nextDouble();
		double C=input.nextDouble();
		double D=input.nextDouble();
		double E=input.nextDouble();
		double F=input.nextDouble();
		LinearEquation linearEquation=new LinearEquation(A,B,C,D,E,F);// new a LinearEquation object and initialize a,b,c,d,e and f
		System.out.println("output");
		if(linearEquation.isSolvable())//if isSolvable() is true
		{
			System.out.print("X is "+linearEquation.getX()+" and Y is "+linearEquation.getY());
		}
		else//if isSolvable() is false
		{
			System.out.print("The equation has no solution");
		}
	}//end main
}
