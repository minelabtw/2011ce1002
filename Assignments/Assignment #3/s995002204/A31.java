package a3.s995002204;

import java.util.Scanner;

public class A31 {
	public static void main(String args[])
	{
		Scanner input = new Scanner(System.in);	
		double a[][] = new double[3][3];		//declare array
		double b[][] = new double[3][3];
		double c[][] = new double[3][3];
		
		System.out.println("Enter matrix 1:");	//request user input matrix
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				a[i][j] = input.nextDouble();
			}
		}
		
		System.out.println("Enter matrix 2:");  //request user input matrix
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				b[i][j] = input.nextDouble();
			}
		}
		
		System.out.println("The matrices are multiplied as follows:");		//show the answer
		c = multipleMatrix(a,b);
		for (int i=0;i<3;i++)
		{
			for (int j=0;j<3;j++)
			{
				System.out.printf("\t%4.1f", a[i][j]);
			}
			for (int j=0;j<3;j++)
			{
				if(i==1&&j==0)
					System.out.printf("\t*%4.1f ", b[i][j]);
				else
					System.out.printf("\t %4.1f ", b[i][j]);
			}
			for (int j=0;j<3;j++)
			{
				if(i==1&&j==0)
					System.out.printf("\t=%5.1f ", c[i][j]);
				else
					System.out.printf("\t %5.1f ", c[i][j]);
			}
			System.out.println();
		}
		
	}
	
	public static double[][] multipleMatrix(double[][] a,double[][] b)	//a function to calculate matrix multiple 
	{
		double c[][] = new double [3][3];
		for(int i=0;i<3;i++)
		{
			c[0][i] = a[0][0]*b[0][i]+a[0][1]*b[1][i]+a[0][2]*b[2][i];
			c[1][i] = a[1][0]*b[0][i]+a[1][1]*b[1][i]+a[1][2]*b[2][i];
			c[2][i] = a[2][0]*b[0][i]+a[2][1]*b[1][i]+a[2][2]*b[2][i];
		}
		return c;
	}
}
