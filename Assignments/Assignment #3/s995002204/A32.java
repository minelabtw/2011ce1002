package a3.s995002204;

import java.util.Scanner;

public class A32 {
	public static void main(String args[]){	
		
		//declare variable
		Scanner input = new Scanner(System.in);
		double a;								
		double b;
		double c;
		double d;
		double e;
		double f;
		
		//obtain variable
		System.out.println("input:");		
		a = input.nextDouble();			
		b = input.nextDouble();
		c = input.nextDouble();
		d = input.nextDouble();
		e = input.nextDouble();
		f = input.nextDouble();
		LinearEquation LinearEquation = new LinearEquation(a,b,c,d,e,f);  
		
		//Output
		System.out.println("output:");
		if(LinearEquation.isSolvable())
			System.out.println("x is "+LinearEquation.getX()+" and y is "+LinearEquation.getY());
		else
			System.out.println("The equation has no solution");
	}
	
	
}
