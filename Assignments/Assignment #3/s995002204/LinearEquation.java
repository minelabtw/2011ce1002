package a3.s995002204;

public class LinearEquation {
	private static double a;	//declare variable
	private static double b;
	private static double c;
	private static double d;
	private static double e;
	private static double f;
	
	LinearEquation(double a,double b,double c,double d,double e,double f)	//Constructor with initialization
	{
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
		this.e = e;
		this.f = f;
	}
	
	// a method to get variable a
	public static double geta()	
	{
		return a;
	}
	
	// a method to get variable b
	public static double getb() 
	{
		return b;
	}
	
	// a method to get variable c
	public static double getc() 
	{
		return c;
	}
	
	// a method to get variable d
	public static double getd() 
	{
		return d;
	}
	
	// a method to get variable e
	public static double gete() 
	{
		return e;
	}
	
	// a method to get variable f
	public static double getf() 
	{
		return f;
	}
	
	// a function to determinate the linear equation is solvable or not
	public static boolean isSolvable()	
	{
		if(a*d-b*c==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	// a method to get X;
	public static double getX()		
	{
		double x=0;
		if(isSolvable())
			x = (e*d-b*f)/(a*d-b*c);
		return x;
		
	}
	
	// a method to get Y;
	public static double getY()		
	{
		double y=0;
		if(isSolvable())
			y = (a*f-e*c)/(a*d-b*c);
		return y;
	}
}
