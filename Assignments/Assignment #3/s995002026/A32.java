package a3.s995002026;

import java.util.Scanner;

public class A32 {
	
	public static void main(String[] argv){
		Scanner input=new Scanner(System.in);

		double A,B,C,D,E,F;					//紀錄abcdef的內容
		
		System.out.println("ax+by=c"+"\n"+"ex+dy=f");
	
		System.out.println("please enter the value of a,b,c,d,e,f");
	
		A=input.nextDouble();
		
		B=input.nextDouble();
		
		C=input.nextDouble();
		
		D=input.nextDouble();
		
		E=input.nextDouble();
		
		F=input.nextDouble();
		
		LinearEquation  le=new LinearEquation(A,B,C,D,E,F);
		
		if(le.isSolvable()==false)												//判斷有無解
			System.out.println("The equation has no solution. ");
		
		else{																					//有解
			System.out.println("x	=	"+le.getX());
			
			System.out.println("y	=	"+le.getY());
		}
	}
	
}
