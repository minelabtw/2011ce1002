package a3.s995002026;

import java.text.DecimalFormat;

import 	java.util.Scanner;

public class A31 {
	public static void main(String[] argv){
		Scanner  input=new Scanner(System.in);
		
		double[][]  m1=new double[3][3];			//紀錄陣列1
		
		double[][] m2=new double[3][3];			//紀錄陣列2
		
		//------------------輸入陣列的值------------------//
		System.out.println("Enter matrix1 : ");
		
		for(int i=0;i<3;i++){	
			for(int j=0;j<3;j++){
				m1[i][j]=input.nextDouble();
			}
		}
		
		System.out.println("Enter matrix2 : ");
		
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				m2[i][j]=input.nextDouble();
			}
		}
		//------------------輸入陣列的值------------------//
		
		System.out.println("the matrices are multiplied as follows:");
		show(m1,m2,multipleMatrix(m1,m2));			//印出結果
		
		
	}
	
	
	//回傳結果
	public static double[][] multipleMatrix( double[][] a, double[][] b){		//a紀錄m1內容,b紀錄m2內容
		double[][] m3=new double[3][3];		//記錄結果
		
		double sum0;												//暫存 m3[i][j]的值
		for(int k=0;k<3;k++){
			for(int i=0;i<3;i++){								//讓陣列第一列的[0][0]，[0][1]，[0][2]都可以被記錄
				sum0=0;
			
				for(int j=0;j<3;j++){
					sum0=sum0+a[k][j]*b[j][i];
				}
				DecimalFormat sum1 = new DecimalFormat("##.00");
				
				m3[k][i]=Double.parseDouble(sum1.format(sum0));
			}
		}
		return m3;
	}
	
	//印出結果
	public static void show(double[][] a,double[][] b,double[][] c){		
		
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				System.out.print(a[i][j]+" \t");		//印出各行m1陣列的內容
				
				if(j==2 && i==1)											//判斷是否加入"-"
					System.out.print("*\t");
				
				else							
					System.out.print("\t");
			}
				
			for(int j=0;j<3;j++){
					System.out.print(b[i][j]+" \t");		//印出各行m2陣列的內容
					
					if(j==2 && i==1)											//判斷是否印出"="
						System.out.print("=\t");			
					
					else
						System.out.print("\t");
				}
			
			for(int j=0;j<3;j++){								//印出答案
				System.out.print(c[i][j]+"\t\t");
			}
			
			System.out.print("\n");					//印完一行就換行
		}
	}
}