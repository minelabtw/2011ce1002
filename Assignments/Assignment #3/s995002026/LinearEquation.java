package a3.s995002026;

public class LinearEquation {
	private static double a,b,c,d,e,f;
	
	
	//constructor
	LinearEquation(double aa,double bb,double cc,double dd,double ee,double ff){
		a=aa;
		
		b=bb;
		
		c=cc;
		
		d=dd;
		
		e=ee;
		
		f=ff;
	}
	/*↓↓↓↓↓↓↓↓↓↓↓回傳abcdef的內容↓↓↓↓↓↓↓↓↓↓↓↓*/
	public static double getA(){
		return a;
	}
	
	public static double getB(){
		return b;
	}
	
	public static double getC(){
		return c;
	}
	
	public static double getD(){
		return d;
	}
	
	public static double getE(){
		return e;
	}
	
	public static double getF(){
		return f;
	}
	/*↑↑↑↑↑↑↑↑↑↑↑回傳abcdef的內容↑↑↑↑↑↑↑↑↑↑↑*/
	
	public  boolean isSolvable(){			//判斷有無解
		if(a*d-b*c==0)											//無解
			return false;
		
		else																//有解
			return true;
	}
	
	public  double getX(){						//回傳X
		
		double x;
		
		x=(e*d - b*f)/(a*d-b*c);
		
		return x;
	}
	
	
	public  double getY(){						//回傳Y
		
		double y;
		
		y=(a*f - e*c)/(a*d-b*c);
		
		return y;
	}
}
