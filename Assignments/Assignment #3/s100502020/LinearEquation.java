package a3.s100502020;

public class LinearEquation {
	private static double a;//private members
	private static double b;
	private static double c;
	private static double d;
	private static double e;
	private static double f;	
	public static double getX(double a,double b,double c,double d,double e,double f)//calculate
	{
		return (e*d-b*f)/(a*d-b*c);
	}
	public static double getY(double a,double b,double c,double d,double e,double f)//calculate
	{
		return (a*f-e*c)/(a*d-b*c);
	}
	public static String issolvable()//see if a*d-b*c = 0 or not
	{
		if((a*d-b*c)!=0)
		{
			return "true";
		}
		else
			return "false";
	}
	LinearEquation(double A,double B,double C,double D,double E,double F)//constructor
	{
		a=A;
		b=B;
		c=C;
		d=D;
		e=E;
		f=F;		
	}
}
