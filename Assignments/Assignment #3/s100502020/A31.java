package a3.s100502020;
import java.util.Scanner;
public class A31 {
	public static double[][] multipleMatrix( double[][] x, double[][] y)//doing the math of matrix
	{
		double[][] ans = new double[3][3];
		for(int a=0;a<3;a++)//double for loop for calculating two dim array
		{
			for(int b=0;b<3;b++)
			{
				ans[a][b] = x[a][0] * y[0][b] + x[a][1] * y[1][b] + x[a][2]
						* y[2][b];
			}
		}
		return ans;
	}
	public static void main(String[] args)
	{
		double [][]array1 = new double[3][3];//create new array
		double [][]array2 = new double[3][3];
		double [][]array3 = new double[3][3];
		Scanner input = new Scanner(System.in);
		System.out.print("Enter matrix1: ");
		for(int a=0; a<3 ; a++)//store the numbers in array
		{
			for(int b=0;b<3;b++)
			{
				array1[a][b] = input.nextDouble();
			}
		}
		System.out.print("Enter matrix2: ");
		for(int a=0; a<3 ; a++)//store the numbers in array
		{
			for(int b=0;b<3;b++)
			{
				array2[a][b] = input.nextDouble();
			}
		}
		array3 = multipleMatrix( array1, array2);//calculate
		System.out.println("The matrices are multiplied as follows: ");
		for(int a=0;a<3;a++)//show the results.......
		{
			System.out.print(array1[0][a]+" ");
		}
		System.out.print("\t");
		for(int a=0;a<3;a++)
		{
			System.out.print(array2[0][a]+" ");
		}
		System.out.print("\t");
		for(int a=0;a<3;a++)
		{
			System.out.printf("%.1f\t", array3[0][a]);
		}
		System.out.print("\n");
		for(int a=0;a<3;a++)
		{
			System.out.print(array1[1][a]+" ");
		}
		System.out.print(" *  ");
		for(int a=0;a<3;a++)
		{
			System.out.print(array2[1][a]+" ");
		}
		System.out.print(" =  ");
		for(int a=0;a<3;a++)
		{
			System.out.printf("%.1f\t", array3[1][a]);
		}
		System.out.print("\n");
		for(int a=0;a<3;a++)
		{
			System.out.print(array1[2][a]+" ");
		}
		System.out.print("\t");
		for(int a=0;a<3;a++)
		{
			System.out.print(array2[2][a]+" ");
		}
		System.out.print("\t");
		for(int a=0;a<3;a++)
		{
			System.out.printf("%.1f\t", array3[2][a]);
		}
		System.out.print("\n");
		
		
	}

}
