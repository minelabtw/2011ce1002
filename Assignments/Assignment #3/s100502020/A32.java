package a3.s100502020;
import java.util.Scanner;
public class A32 {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		System.out.print("Input: ");
		String check;		
		double a = input.nextDouble();
		double b = input.nextDouble();
		double c = input.nextDouble();
		double d = input.nextDouble();
		double e = input.nextDouble();
		double f = input.nextDouble();
		LinearEquation LE=new LinearEquation(a,b,c,d,e,f);//include LinearEquation in
		check = LE.issolvable();//the return value stores in check variable
		if (check == "true")
		{
			System.out.print("X is "+LE.getX(a, b, c, d, e, f)+" AND "+"Y is "+LE.getY(a, b, c, d, e, f));
		}
		else if(check == "false"){
			System.out.println("The equation has no solution!");
		}
	}
}
