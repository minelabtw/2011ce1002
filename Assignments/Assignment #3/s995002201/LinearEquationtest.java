package a3.s995002201;

import java.util.Scanner;

public class LinearEquationtest 
{
	public static void main ( String[] args )
	{
		Scanner input = new Scanner(System.in);
		LinearEquation linear = new LinearEquation();
		double a,b,c,d,e,f;
		a = input.nextDouble();//linear.geta();
		b = input.nextDouble();//linear.getb();
		c = input.nextDouble();//linear.getc();
		d = input.nextDouble();//linear.getd();
		e = input.nextDouble();//linear.gete();
		f = input.nextDouble();//linear.getf();
		
		if(linear.isSolvable()==true)
		{
			linear.fin();
		}	
		else if(linear.isSolvable()==false)
		{
			System.out.println("The equation has no solution.");
		}
	}
}
