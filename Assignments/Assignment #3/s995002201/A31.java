package a3.s995002201;

import java.text.NumberFormat;
import java.util.Scanner;

public class A31 
{
	public static void main ( String[] args )
	{
		Scanner input = new Scanner( System.in );
		double[][] mat1 = new double[3][3];
		double[][] mat2 = new double[3][3];
		double[][] ans = new double[3][3];//宣告陣列
		
		System.out.println("Enter matrix1: ");
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				mat1[i][j] = input.nextDouble();
			}
		}//輸入陣列一
		
		System.out.println("Enter matrix2: ");
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				mat2[i][j] = input.nextDouble();
			}
		}//輸入陣列二
		
		System.out.println("the matrices are multiplied as follows: ");
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				System.out.print(mat1[i][j]+"\t");//印出陣列一
			}
			if(i==1)
			{
				System.out.print("*");//印出*號
			}
			for(int j=0;j<3;j++)
			{
				System.out.print("\t"+mat2[i][j]+"   ");//印出陣列二
			}
			if(i==1)
			{
				System.out.print("=");//印出=號
			}
			NumberFormat nf = NumberFormat.getInstance();
		    nf.setMaximumFractionDigits( 2 );//設定取小數後2位
			for(int j=0;j<3;j++)
			{
				ans = multipleMatrix(mat1,mat2);
				System.out.print("\t"+nf.format(ans[i][j])+" ");//印出答案
			}
			System.out.println();
		}
	}
	public static double[][] multipleMatrix( double[][] a, double[][] b)//矩陣相乘方法
	{
		double[][] ans = new double[3][3];
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				for(int k=0;k<3;k++)
				{
					ans[i][j] +=  a[i][k] * b[k][j];
				}
			}
		}
		return ans;	
	}
}
