package a3.s995002201;

public class LinearEquation 
{
	private double a,b,c,d,e,f;
	
	public double geta()
	{
		return a;
	}
	public double getb()
	{
		return b;
	}
	public double getc()
	{
		return c;
	}
	public double getd()
	{
		return d;
	}
	public double gete()
	{
		return e;
	}
	public double getf()
	{
		return f;
	}
	public boolean isSolvable()
	{
		if(((geta()*getd())-(getb()*getc()))==0)
		{
			return false;
		}
		else
		return true;
	}
	public double getX()
	{
		double ansx;
		ansx = ((gete()*getd())-(getb()*getf()))/((geta()*getd())-(getb()*getc()));
		return ansx;
	}
	public double getY()
	{
		double ansy;
		ansy = ((geta()*getf())-(gete()*getc()))/((geta()*getd())-(getb()*getc()));
		return ansy;
	}
	public void word()
	{
		System.out.println("x is "+getX()+" and y is "+getY());
	}
	public void fin()
	{
		word();
	}
}
