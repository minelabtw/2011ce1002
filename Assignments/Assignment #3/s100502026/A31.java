package a3.s100502026;

import java.util.Scanner;

public class A31
{
	private static int counter1 ;
	private static int counter2 ;
	private static Scanner input = new Scanner( System.in ) ; 
	
	//Method for multiple 2matrix
	public static double[][] multipleMatrix( double[][] a , double[][] b)
	{
		double[][] array = new double[3][3] ;
		
		counter1 = 0 ;
		counter2 = 0 ;
		while( counter1 < 3 )
		{
			counter2 = 0 ;
			
			while( counter2 < 3 )
			{
				int i = 0 ;
				int j = 0 ;
				while(i<3)
					{
						array[ counter1 ][ counter2 ] = a[ counter1 ][ j ] * b[ j ][ counter2 ] + array[ counter1 ][ counter2 ] ;
						i = i + 1 ;
						j = j + 1 ;
					}
				counter2 = counter2 + 1 ;
			}
			counter1 = counter1 + 1 ;
		}
		return array ;
	}
	
	//Method for enter matrix
	public static double[][] EnterMatrix(double[][] array)
	{
		counter1 = 0 ;
		counter2 = 0 ;
		
		System.out.print( "Enter matrix: " ) ;
		while( counter1 < array.length )
		{
			counter2 = 0 ;
			
			while( counter2 < array[counter1].length )
			{
				array[counter1][counter2] = input.nextDouble() ;
				counter2 = counter2 + 1 ;
			}
			
			counter1 = counter1 + 1 ;
		}		
		return array ;
	}
	
	public static void show( double[][] array , int n )
	{
		System.out.print( array[n][0] + " " + array[n][1] + " " + array[n][2] ) ;
	}
	
	public static void main(String[] args)
	{
		//Declare
		double[][] matrix1 = new double[3][3] ;
		double[][] matrix2 = new double[3][3] ;
		
		//Enter 2 matrix
		EnterMatrix( matrix1 ) ;
		EnterMatrix( matrix2 ) ;
		
		//Multiple 2 matrix and show result
		show( matrix1 , 0 ) ;
		System.out.print( "\t" ) ;
		show( matrix2 , 0 ) ;
		System.out.print( "\t" );
		show( multipleMatrix( matrix1 , matrix2 ) , 0 ) ;
		System.out.print( "\n" ) ;

		show( matrix1 , 1 ) ;
		System.out.print( "  *  " ) ;
		show( matrix2 , 1 ) ;
		System.out.print( "  =  " ) ;
		show( multipleMatrix( matrix1 , matrix2 ) , 1 ) ;
		System.out.print( "\n" ) ;

		show( matrix1 , 2 ) ;
		System.out.print( "\t" ) ;
		show( matrix2 , 2 ) ;
		System.out.print( "\t" ) ;
		show( multipleMatrix( matrix1 , matrix2 ) , 2 ) ;

	}

}
