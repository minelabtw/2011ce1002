package a3.s100502026;

import java.util.Scanner;

public class A32 {
	
	public static void main(String[] args)
	{
		//Declare
		Scanner input = new Scanner(System.in);
		
		//Enter from a to f
		System.out.print("Input:");
		double a1=input.nextDouble();
		double b1=input.nextDouble();
		double c1=input.nextDouble();
		double d1=input.nextDouble();
		double e1=input.nextDouble();
		double f1=input.nextDouble();
		
		LinearEquation equation=new LinearEquation(a1,b1,c1,d1,e1,f1);
		
		//Show solution
		if( equation.isSolvable() != 0)
		{
			System.out.println("Output\n");
			System.out.println("x is " + equation.GetX() + "and y is " + equation.GetY() );
		}
		else
		{
			System.out.println( "Output\n" );
			System.out.println( "The equation has no solution" );
		}
	}
}
