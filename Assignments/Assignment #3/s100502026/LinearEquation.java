package a3.s100502026;

public class LinearEquation {
	
	private static double a;
	private static double b;
	private static double c;
	private static double d;	
	private static double e;
	private static double f;
	private static double x;
	private static double y;
	
	//Initialize a to f
	LinearEquation( double a1 , double b1 , double c1 , double d1 , double e1 , double f1)
	{
		a=a1;
		b=b1;
		c=c1;
		d=d1;
		e=e1;
		f=f1;
		if ( isSolvable()!=0 )
		{
			x=(e*d-b*f)/(a*d-b*c);
			y=(a*f-e*c)/(a*d-b*c);
		}		
	}
	
	//Solve x
	public static double GetX()
	{
		return x=(e*d-b*f)/(a*d-b*c);
	}
	
	//Solve y
	public static double GetY()
	{
		return y=(a*f-e*c)/(a*d-b*c);
	}
	
	//Check a*d-b*c
	public static double isSolvable()
	{
		if(a*d-b*c!=0)
			return 1;
		else
			return 0;			
	}	
}
