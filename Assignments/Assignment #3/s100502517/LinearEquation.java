package a3.s100502517;

public class LinearEquation {
	private double a;
	private double b;
	private double c;
	private double d;
	private double e;
	private double f;
	
	public LinearEquation(double num1, double num2, double num3, double num4, double num5, double num6){
		a = num1;
		b = num2;
		c = num3;
		d = num4;
		e = num5;
		f = num6;
	}
	public boolean isSolvable(){//determine the functions ax+by = e and cx+dy = f are solvable
		if(a*d-b*c != 0)
			return true;
		else
			return false;		
	}	
	public double getX(){
		return ((e*d-b*f)/(a*d-b*c));		
	}
	public double getY(){
		return ((a*f-e*c)/(a*d-b*c));		
	}
	

}
