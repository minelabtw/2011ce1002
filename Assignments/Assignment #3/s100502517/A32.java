package a3.s100502517;

import java.util.Scanner;

public class A32 {
	public static void main(String[] args){
		
		Scanner input = new Scanner(System.in);
		
		double a1,b1,c1,d1,e1,f1;
		String check;
		boolean keepgoing = true;
		
		while(keepgoing == true){		
		
			System.out.print("input: ");//input six numbers 
			a1 = input.nextDouble();
			b1 = input.nextDouble();
			c1 = input.nextDouble();
			d1 = input.nextDouble();
			e1 = input.nextDouble();
			f1 = input.nextDouble();		
		
			LinearEquation result = new LinearEquation(a1,b1,c1,d1,e1,f1);//throw them into the class LinearEquation
		
			if(result.isSolvable() == true){
				System.out.println("x is "+result.getX()+" and y is "+ result.getY());
			}
			else{
				System.out.println("The equation has no solution~!!");
			}
			
			System.out.println("Continue(y/n): ");//y is yes and n is no~
			check = input.next();
			
			if(check.equals("y")){
				keepgoing = true;				
			}
			else if(check.equals("n")){
				keepgoing = false;
			}
			else{
				System.out.println("Error~!!Bye~!!");
				keepgoing = false;
			}			
		}
	}
}
