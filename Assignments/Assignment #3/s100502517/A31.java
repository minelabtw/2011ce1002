package a3.s100502517;

import java.util.Scanner;

public class A31 {

	public static double[][] multipleMatrix(double[][] a, double[][] b){//throw two arrays and multiply them
		
		double[][] sum = new double[3][3];
		
		for(int c = 0 ; c<3 ; c++){
			for(int d = 0 ; d<3 ; d++ ){
				sum[c][d] = a[c][d] * b[c][d]; 
			}				
		}
		
		return sum;
	}
	
	public static void main(String[] args){
		
		Scanner input = new Scanner(System.in);
		
		double[][] array1 = new double[3][3];
		double[][] array2 = new double[3][3];
		
		System.out.print("Enter matrix1: ");
		for(int a = 0 ; a<3 ; a++){
			for(int b = 0 ; b<3 ; b++ ){				
				array1[a][b] = input.nextDouble();//input all the numbers into the array1 simultaneously
			}				
		}
		
		System.out.print("Enter matrix2: ");
		for(int a = 0 ; a<3 ; a++){
			for(int b = 0 ; b<3 ; b++ ){
				
				array2[a][b] = input.nextDouble();//input all the number into the array2 simultaneously
			}				
		}
		
		System.out.println("the matrices are multiplied as follows: ");
		
		for(int show = 0; show < 1; show++){//
				for(int show3 = 0; show3 < 3; show3++){
					System.out.print(array1[show][show3]+" ");
				}
				
				System.out.print("   ");
				
				for(int show4 = 0; show4 < 3; show4++){
					System.out.print(array2[show][show4]+" ");
				}
				System.out.print("   ");
				for(int show5 = 0; show5 < 3; show5++){
					System.out.print(multipleMatrix(array1,array2)[show][show5]+" ");
				}
				System.out.print("\n");//the first line is over				
			
				for(int show3 = 0; show3 < 3; show3++){
					System.out.print(array1[show+1][show3]+" ");
				}
				System.out.print(" * ");
				for(int show4 = 0; show4 < 3; show4++){
					System.out.print(array2[show+1][show4]+" ");
				}
				System.out.print(" = ");
				for(int show5 = 0; show5 < 3; show5++){
					System.out.print(multipleMatrix(array1,array2)[show+1][show5]+" ");
				}
				System.out.print("\n");//the second line is over				
			
				for(int show3 = 0; show3 < 3; show3++){
					System.out.print(array1[show+2][show3]+" ");
				}
				System.out.print("   ");
				for(int show4 = 0; show4 < 3; show4++){
					System.out.print(array2[show+2][show4]+" ");
				}
				System.out.print("   ");
				for(int show5 = 0; show5 < 3; show5++){
					System.out.print(multipleMatrix(array1,array2)[show+2][show5]+" ");
				}
				System.out.print("\n");				
			}		
		}		
	}
