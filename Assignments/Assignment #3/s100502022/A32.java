package A3.s100502022;
import java.util.Scanner;
public class A32 {
	public static void main(String[] args){
		Scanner input =new Scanner(System.in);
		System.out.print("input :");
		//initiallize the varibles and input value in
		double a=input.nextDouble();
		double b=input.nextDouble();
		double c=input.nextDouble();
		double d=input.nextDouble();
		double e=input.nextDouble();
		double f=input.nextDouble();
		//initialize the class and trans the value 
		LinearEquation lin=new LinearEquation(a,b,c,d,e,f);
		//if funtion is solvable print the answer and sentence
		if ((a*d-b*c)!=0){
			System.out.println(lin.geta(a)+"x+"+lin.getb(b)+"y="+lin.geta(c)+"\n"+lin.getd(d)+"x+"+lin.gete(e)+"y="+lin.getf(f));
			System.out.println(lin.issolvable());
			System.out.print("THe X is:"+lin.getX(a, b, c, d, e, f)+" AND ");
			System.out.println("THe Y is:"+lin.getY(a, b, c, d, e, f));
		}
		//else if function cant nobe solvable
		else if((a*d-b*c)==0){
			System.out.println(lin.geta(a)+"x+"+lin.getb(b)+"y="+lin.geta(c)+"\n"+lin.getd(d)+"x+"+lin.gete(e)+"y="+lin.getf(f));
			System.out.println(lin.issolvable());
			System.out.println("The equation has no solution!!");
		}
		
	}

}
