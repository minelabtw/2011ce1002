package A3.s100502022;
import java.util.Scanner;

public class A31 {
	public static void main(String [] argv){
		// declare three matrices
		double a[][] = new double[3][3];
		double b[][] = new double[3][3];
		double c[][] = new double[3][3];
		// new a Scanner object
		Scanner scanner = new Scanner(System.in); 

		// input matrix 1
		System.out.print("Please input the matrix 1: ");
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				a[i][j] = scanner.nextDouble();
			}
		}

		// input matrix 2
		System.out.print("Please input the matrix 2: ");
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				b[i][j] = scanner.nextDouble();
			}
		}

		// call method subMat to calculate subtraction
		c = subMat(a,b);

		System.out.println(a[0][0]+", "+a[0][1]+", "+a[0][2]+"   "+b[0][0]+", "+b[0][1]+", "+b[0][2]+"   "+c[0][0]+", "+c[0][1]+", "+c[0][2]);
		System.out.println(a[1][0]+", "+a[1][1]+", "+a[1][2]+" * "+b[1][0]+", "+b[1][1]+", "+b[1][2]+" = "+c[1][0]+", "+c[1][1]+", "+c[1][2]);
		System.out.println(a[2][0]+", "+a[2][1]+", "+a[2][2]+"   "+b[2][0]+", "+b[2][1]+", "+b[2][2]+"   "+c[2][0]+", "+c[2][1]+", "+c[2][2]);
	}

	public static double[][] subMat(double[][]a,double[][]b){
		double result[][] = new double[3][3];
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				for (int k=0;k<3;k++){
					//according to calculation
					result[i][j] = result[i][j]+a[i][k] * b[k][j];
				}			
			}
		}
		return result;
	}
}