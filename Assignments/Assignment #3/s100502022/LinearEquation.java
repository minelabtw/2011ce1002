package A3.s100502022;

public class LinearEquation {
	// declare the attributes of this class
	private static double a;
	private static double b;
	private static double c;
	private static double d;
	private static double e;
	private static double f;
	//to call and check the value
	public static double geta (double a){
		return a;
	}
	public static double getb (double b){
		return b;
	}
	public static double getc (double c){
		return c;
	}
	public static double getd (double d){
		return d;
	}
	public static double gete (double e){
		return e;
	}
	public static double getf (double f){
		return f;
	}
	//getX to calculate the unknown X
	public static double getX(double a,double b,double c,double d,double e,double f){
		return (e*d-b*f)/(a*d-b*c);
	}
    //getY to calculate the unknown Y
	public static double getY(double a,double b,double c,double d,double e,double f){
		return (a*f-e*c)/(a*d-b*c);
	}
	//return the solvable question
	public static String issolvable(){
		if((a*d-b*c)!=0){
			return "true";
		}
		else return "false";
	}
	// constructor with initialization
	LinearEquation(double x,double y,double z,double j,double k,double l){
		a=x;
		b=y;
		c=z;
		d=j;
		e=k;
		f=l;
		
	}
}
