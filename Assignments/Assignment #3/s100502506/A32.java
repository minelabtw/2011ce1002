package a3.s100502506;

import java.util.Scanner;
public class A32 
{
	public static void main(String argc[])
	{
		
		double[] inputArray=new double[6];//create six double variable
		Scanner inputScanner=new Scanner(System.in);
		System.out.print("input\n");
		//input
		//input six variable
		for(int i=0;i<6;i++)
		{
			inputArray[i]=inputScanner.nextDouble();
		}
		
		//create an object and pass variable to constructor
		LinearEquation test=new LinearEquation(inputArray[0],inputArray[1], inputArray[2], inputArray[3], inputArray[4], inputArray[5]);
		
		//output
		System.out.print("output\n");
		//if the result is solvable return the result
		if(test.isSolvable()==true)
			System.out.printf("x is %.1f and y is %.1f",test.getX(),test.getY());
		//if the result isn't solvable return the message
		else 
		{
			//if the result isn't solvable but has infinite number of solutions
			if(test.isInfiniteNumberOfSolutions()==true)
				System.out.println("The equation has infinite number of solutions");
			//if the result isn't solvable
			else 
				System.out.println("The equation has no solution");
			
		}
	}
	
}
