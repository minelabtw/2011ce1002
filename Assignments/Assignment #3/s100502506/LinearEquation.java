package a3.s100502506;

public class LinearEquation
{
	//private data
	private double a;
	private double b;
	private double c;
	private double d;
	private double e;
	private double f;
	//constructor
	LinearEquation(double x1,double x2,double x3,double x4,double x5,double x6)
	{
		
		a=x1;
		b=x2;
		c=x3;
		d=x4;
		e=x5;
		f=x6;
	}
	//get private data
	public double getA()
	{
		return a;
	}
	public double getB()
	{
		return b;
	}
	public double getC()
	{
		return c;
	}
	public double getD()
	{
		return d;
	}
	public double getE()
	{
		return e;
	}
	public double getF()
	{
		return f;
	}
	
	//get x
	public double getX()
	{
		return (e*d-b*f)/(a*d-b*c);
	}
	//get y
	public double getY()
	{
		return (a*f-e*c)/(a*d-b*c);
	}
	//check if is Solvable
	public boolean isSolvable()
	{
		//isn't solvable return false
		if((a*d-b*c)==0)
		{
			return false;
		}
		//isn't solvable return true
		else
		{
			return true;
		}
	}
	//check if has infinite number of solutions
	public boolean isInfiniteNumberOfSolutions()
	{
		
		if((e*d-b*f)==0&&(a*f-e*c)==0)
		{
			return true;
		}
		else 
		{
			return false;
		}
		
	}
}
