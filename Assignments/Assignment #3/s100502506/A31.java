package a3.s100502506;

import java.util.Scanner;
public class A31 
{
	public static double[][] multipleMatrix( double[][] a, double[][] b)//method return double array
	{																	//pass two double array
		double[][] mul_Matrix=new double[3][3];
		
		
		for(int i=0;i<3;i++)											//multiple of matrix 
		{
			for(int j=0;j<3;j++)
			{
				for(int p=0;p<3;p++)
					mul_Matrix[i][j]+=a[i][p]*b[p][j];
			}
		}
		return mul_Matrix;
	}
	
	public static void main(String argc[])
	{
		double[][] inputarray1=new double[3][3];
		double[][] inputarray2=new double[3][3];
		double[][] inputarray3=new double[3][3];
		
		Scanner input =new Scanner(System.in);
		//input
		System.out.println("input");
		System.out.println();
		System.out.print("Enter matrix1: ");
		for(int i=0;i<3;i++)											//input array1
		{
			for(int j=0;j<3;j++)
			{
				inputarray1[i][j]=input.nextDouble();
			}
		}
		
		System.out.print("Enter matrix2: ");
		for(int i=0;i<3;i++)											//input array2
		{
			for(int j=0;j<3;j++)
			{
				inputarray2[i][j]=input.nextDouble();
			}
		}
		inputarray3=multipleMatrix(inputarray1,inputarray2);			//copy multipleMatrix result to inputarray3
		//output
		System.out.println();
		System.out.println("output");
		System.out.println();
		System.out.println("the matrices are subtracted as follows: ");
		
		for(int i=0;i<3;i++)
		{
			//output array1
			for(int j=0;j<3;j++)
			{
				System.out.printf("%.1f\t",inputarray1[i][j]);
					
			}
			//array1 between array2
			if(i==1)//at 2 line
				System.out.print("*\t");
			else
				System.out.print("\t");
			//output array2
			for(int j=0;j<3;j++)
			{
				System.out.printf("%.1f\t",inputarray2[i][j]);
						
			}
			
			//array2 between array3
			if(i==1)//at 2 line
				System.out.print("=\t");
			else
				System.out.print("\t");
			//output array3
			for(int j=0;j<3;j++)
			{
				System.out.printf("%.1f\t",inputarray3[i][j]);
				if(j==2)
					System.out.println();
						
			}
				
		}
	
	}
}

		
		
		
		
		
		
		
		
		
	
