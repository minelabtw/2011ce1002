/**
 * This class is used by Assignment 3-1 for Matrix input TextFields (GUI)
 */

package a3.s100502514;

import java.awt.*;
import java.awt.event.*;
import java.text.NumberFormat;

import javax.swing.*;

public class A31_MatrixPane extends JPanel {
	
	//Private variables
	private JPanel matrixContent;
	private int width, height;
	
	
	/** Default Constructor */
	public A31_MatrixPane(){
		/** Default Constructor will make a 3x3 matrix */
		this(3, 3);
	}
	
	/** Specified Constructor with height and width */
	public A31_MatrixPane(int i, int j){
		/** Initialize the layout */
		setLayout(new BorderLayout(10,10));
		/** Set width and height */
		width=j;
		height=i;
		/** Add the grid at the center */
		matrixContent = new JPanel();
		add(matrixContent,BorderLayout.CENTER);
		matrixContent.setLayout(new GridLayout(i,j,4,4));
		//matrixContent.setSize(30*j, 16*i);
		for(int m=0; m<i*j; m++){
			/** Add text fields for each cell, formatted that is number-only inputtable. */
			JFormattedTextField txt = new JFormattedTextField(NumberFormat.getNumberInstance());
			//txt.setSize(30, getHeight());
			matrixContent.add(txt);
			/** Add Events for each text fields if User presses Spacebar or Enter key. */
			txt.addKeyListener(new KeyListener(){
				public void keyPressed(KeyEvent keyEvent){
					if(keyEvent.getKeyCode()==KeyEvent.VK_ENTER || keyEvent.getKeyCode()==KeyEvent.VK_SPACE){
						
						for(int k=0;k<matrixContent.getComponentCount();k++){
							//Find current event target and focus to next text field
							if(keyEvent.getSource()==matrixContent.getComponent(k)){
								if(k==matrixContent.getComponentCount()-1){
									//If current input is the last one
									//cancel text focus
									matrixContent.requestFocus();
								}else{
									//Otherwise
									//Strip all spaces in the current input
									//	String stripped_str = ((JTextField)keyEvent.getSource()).getText().replace(" ", "");
									//	((JTextField)keyEvent.getSource()).setText(stripped_str);
									//focus to next input
									((JTextField)(matrixContent.getComponent(k+1))).setText("");
									matrixContent.getComponent(k+1).requestFocusInWindow();
								}
							}
						}
						
					}
				}
				public void keyReleased(KeyEvent keyEvent){
					//nothing, but required for the compiler
				}
				public void keyTyped(KeyEvent keyEvent){
					//nothing, but required for the compiler
				}
			});
		}
		
		/** Add the paramtheses on both sides */
		MyBigLabel leftParam = new MyBigLabel("[");
		leftParam.setFont(new Font("Arial Narrow",0,140));
		add(leftParam,BorderLayout.WEST);
		MyBigLabel rightParam = new MyBigLabel("]");
		rightParam.setFont(new Font("Arial Narrow",0,140));
		add(rightParam,BorderLayout.EAST);
	}
	
	/** Get the double array for calculation */
	public double[][] getDoubleData(){
		double[][] M = new double[height][width];
		try{
			for(int k=0;k<matrixContent.getComponentCount();k++){
				int i = k/width, j = k%width;
				M[i][j] = Double.parseDouble(((JTextField)(matrixContent.getComponent(k))).getText().replace(",", ""));
			}
		}catch(Exception err){
			/** Return Null if the text fields are invalid */
			return null;
		}
		return M;
	}
}
