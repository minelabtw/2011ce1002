/**
 * CE1002-100502514 FlashTeens Chiang
 * [The Following Rules are from ce1002 website]
 * Assignment 3-1
	
	1. Write a method to multiply two matrices.
	2. Write a test program that prompts the user to enter two 3X3 matrices and displays their product.
	   Please use ��\t�� to compose the output.
	3. (Some GUI skills as Extra Points)
	
	See ce1002 website for more informations.
	
 */
package a3.s100502514;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class A31 extends MyBaseWindow {
	
	/** Declare Matrix Panes */
	JPanel inputArea = new JPanel();
	A31_MatrixPane Matrix1 = new A31_MatrixPane();
	A31_MatrixPane Matrix2 = new A31_MatrixPane();
	A31_MatrixAnswerPane MatrixAnswer = new A31_MatrixAnswerPane(null,"<Answer>");
	JButton calculate_btn;
	
	/** Window-initializing Constructor */
	public A31(){
		/** Initializing Window */
		initialize("Matrix Multiplication Calculator by 100502514 on Mar 9 2012", 800, 250);
		
		/** Set Main Layout */
		add(new MyBigLabel("Matrix Mulplication"),BorderLayout.NORTH);
		//JPanel inputArea = new JPanel();
		inputArea.setLayout(new GridLayout(1,3));
		add(inputArea,BorderLayout.CENTER);
		calculate_btn = new JButton("Calculate");
		calculate_btn.setLabel("Go Multiplying!!");
		calculate_btn.setFont(new Font("Arial Black", 0, 20));
		add(calculate_btn,BorderLayout.SOUTH);
		
		/** Add Elements in the input area */
		JPanel MatrixGroup1 = new JPanel(new BorderLayout(3,3));
		MatrixGroup1.add(Matrix1,BorderLayout.CENTER);
		MatrixGroup1.add(new MyBigLabel("x"),BorderLayout.EAST);
		inputArea.add(MatrixGroup1);
		JPanel MatrixGroup2 = new JPanel(new BorderLayout(3,3));
		MatrixGroup2.add(Matrix2,BorderLayout.CENTER);
		MatrixGroup2.add(new MyBigLabel("="),BorderLayout.EAST);
		inputArea.add(MatrixGroup2);
		inputArea.add(MatrixAnswer);
		
		/** Add Calculate Button Event */
		calculate_btn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				double[][] result = null;
				try{
					double[][] data1 = Matrix1.getDoubleData(), data2 = Matrix2.getDoubleData();
					result = multiplyMatrix(data1, data2);
					A31_ConsolePrinter.printConsole(new double[][][]{data1, data2, result});
				}catch(A31_ConsolePrinterException err){
					System.err.println("\nOnly 3x3 Matrix is printable on Console!!");
				}catch(Exception err){
					JOptionPane.showMessageDialog(null, "Invalid Values!!");
				}
				inputArea.remove(MatrixAnswer);
				MatrixAnswer = new A31_MatrixAnswerPane(result);
				inputArea.add(MatrixAnswer);
				setVisible(true);
			}
		});
		
	}
	
	/** The method to calculate matrix1*matrix2 */
	/** Throws an error if the matrices are un-multiplicable. */
	public static double[][] multiplyMatrix(double[][] m1, double[][] m2) throws A31_CalculationException{
		/** Declare the matrix for answer */
		/** This declaration can be used in any muplicable matrices. */
		double m[][] = new double[m1.length][m2[0].length];
		
		for(int i=0;i<m.length;i++){
			for(int j=0;j<m[i].length;j++){
				try{
					
					/** Check if two matrices are multiplicable. */
					/** Throws an error if invalid. */
					if(m1[i].length!=m2.length)throw new Exception();
					
					//Initialize element for each run
					m[i][j]=0;
					
					/** Calculate the element in the matrix for answer */
					for(int k=0;k<m2.length;k++){
						m[i][j]+=m1[i][k]*m2[k][j];
					}
				}catch(Exception err){
					/** Throws A31_CalculationException if an error occurred. */
					throw new A31_CalculationException();
				}
			}
		}
		
		return m;
	}
	
	/** Main Function will call the Constructor */
	public static void main(String args[]){
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				A31 myWindow = new A31();
				//Show the application window
				myWindow.setVisible(true);
			}
		});
	}
}
