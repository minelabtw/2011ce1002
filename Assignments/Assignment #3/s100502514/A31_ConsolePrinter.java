/**
 * This class is used by Assignment 3-1 for printing in Console
 * The Console-printing function can only be used if matrices are 3x3.
 * An exception will be thrown if any of the matrices is not 3x3.
 * Even if this assignment does not resize the matrix, but useful in the future.
 */

package a3.s100502514;

public class A31_ConsolePrinter {
	
	//Private Function to print a row in one matrix for console
	private static void print_array(double[] line){
		for(int i=0;i<line.length;i++){
			System.out.print(Math.rint(line[i]*1000)/1000+"\t");
		}
	}
	
	//Determine by variables "line" and "which" in printConsole() if "*" or "=" needs to be printed
	private static void printSign(int line, int which){
		if(line==1){
			if(which==0)System.out.print("  *");
			else if(which==1)System.out.print("  =");
		}
		System.out.print("\t");
	}
	
	/** Print the Calculation Result to Console by passing array {matrix1, matrix2, answer} */
	/** This will be called as A31_ConsolePrinter.printConsole(double[][][]) in class A31 */
	public static void printConsole(double[][][] matrix_group) throws A31_ConsolePrinterException{
		try{
			for(int line=0;line<3;line++){
				for(int which=0;which<3;which++){
					if(matrix_group[which][line].length!=3 || matrix_group[which].length!=3){
						//Throws an error if any of matrices is not 3x3
						throw new Exception();
					}
					//Call Private Printing Methods
					print_array(matrix_group[which][line]);
					printSign(line,which);
				}
				System.out.println();
			}
			//Print out the separator of 2 different messages in the Console.
			System.out.println();
		}catch(Exception err){
			throw new A31_ConsolePrinterException();
		}
	}
}
