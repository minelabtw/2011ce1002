/**
 * CE1002-100502514 FlashTeens Chiang
 * This is the base class for the Basic Window Form.
 * Defining 3 ways for initialization.
 */
package a3.s100502514;

import javax.swing.*;

public class MyBaseWindow extends JFrame {
	
	//Using Default Title and Size to Initialize.
	public void initialize(){
		initialize("Assignment 100502514", 300, 200);
	}
	
	//Using Default Size but Different Title to Initialize.
	public void initialize(String title){
		initialize(title, 300, 200);
	}
	
	//Using Different Title and Size to Initialize.
	public void initialize(String title, int width, int height){
		setTitle(title);
		setSize(width, height);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
	}
}
