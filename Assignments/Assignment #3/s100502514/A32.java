/**
 * CE1002-100502514 FlashTeens Chiang
 * [The Following Rules are from ce1002 website]
 * Assignment 3-2
	
	1. Design a class named "LinearEquation" for a 2X2 system of linear equations:
		
		ax+by=e, cx+dy=f gets x=(ed-bf)/(ad-bc), y=(af-ec)/(ad-bc)
		
	2. The class contains:
	  *	Private data fields a, b, c, d, e, and f.
	  *	A constructor with the arguments for a, b, c, d, e, and f.
	  *	A method named isSolvable() that returns true if ad �V bc is not 0.
	  *	Method getX() and getY() that returns the solution for the equation.
	
	3. Implement the class.
		Write a test program that prompts the user to enter a~f and displays the result.
		If ad- bc is 0, report that ��The equation has no solution.��
			
	See ce1002 website for more informations.
	
 */
package a3.s100502514;

import java.awt.*;
import java.awt.event.*;
import java.text.NumberFormat;

import javax.swing.*;

public class A32 extends MyBaseWindow {
	
	/** Declare components */
	JButton calculate_btn = new JButton("Solve Equation");
	JButton reset_btn = new JButton("Reset Input");
	JFormattedTextField[] text_group = new JFormattedTextField[6];
	
	/** Window-initializing Constructor */
	public A32(){
		/** Initializing Window */
		initialize("2-variable Kramer Equation Calculator by 100502514 on Mar 9 2012", 300, 150);
		
		/** Set the main layout */
		setLayout(new BorderLayout());
		
		/** Add Button for Calculation */
		JPanel flow_shell = new JPanel(new FlowLayout(FlowLayout.CENTER));
		flow_shell.add(calculate_btn);
		flow_shell.add(reset_btn);
		add(flow_shell, BorderLayout.SOUTH);
		
		/** Initialize Text fields and their Events */
		for(int i=0;i<text_group.length;i++){
			text_group[i] = new JFormattedTextField(NumberFormat.getNumberInstance());
			text_group[i].setFont(new Font("Courier New",0,14));
			text_group[i].addKeyListener(new KeyListener(){
				public void keyPressed(KeyEvent keyEvent){
					if(keyEvent.getKeyCode()==KeyEvent.VK_ENTER || keyEvent.getKeyCode()==KeyEvent.VK_SPACE){
						
						for(int k=0;k<text_group.length;k++){
							//Find current event target and focus to next text field
							if(keyEvent.getSource()==text_group[k]){
								if(k==text_group.length-1){
									//If current input is the last one
									//cancel text focus and change it to button
									calculate_btn.requestFocus();
								}else{
									//Otherwise
									//Strip all spaces in the current input
									//	String stripped_str = ((JTextField)keyEvent.getSource()).getText().replace(" ", "");
									//	((JTextField)keyEvent.getSource()).setText(stripped_str);
									//focus to next input
									((JTextField)(text_group[k+1])).setText("");
									text_group[k+1].requestFocusInWindow();
								}
							}
						}
						
					}
				}
				public void keyReleased(KeyEvent keyEvent){
					//nothing, but required for the compiler
				}
				public void keyTyped(KeyEvent keyEvent){
					//nothing, but required for the compiler
				}
			});
		}
		
		/** Initialize the equation pane for input */
		JPanel equa_pane = new JPanel(new GridLayout(2,5,5,10));
		for(int line=0;line<2;line++){
			equa_pane.add(text_group[line*3+0]);
			equa_pane.add(new MyBigLabel("x  +"));
			equa_pane.add(text_group[line*3+1]);
			equa_pane.add(new MyBigLabel("y  ="));
			equa_pane.add(text_group[line*3+2]);
		}
		add(equa_pane, BorderLayout.CENTER);
		
		/** Add Calculating Button Event */
		calculate_btn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				try{
					/** Copy input texts to an array of doubles */
					double[] equa = new double[6];
					for(int k=0;k<equa.length;k++){
						equa[k]=Double.parseDouble(text_group[k].getText().replace(",", ""));
					}
					/** Calculate and show the the result using class LinearEquation */
					LinearEquation solution = new LinearEquation(equa);
					if(solution.isSolvable()){
						JOptionPane.showMessageDialog(null,
								"There is a solution:\n" +
								"(x, y) = ("+solution.getX()+", "+solution.getY()+")");
					}else{
						switch(solution.getKramerStatus()){
						case PARALELL:
							JOptionPane.showMessageDialog(null,
									"There is no any solutions:\n" +
									"Two lines paralells !!");
							break;
						case OVERLAP:
							JOptionPane.showMessageDialog(null,
									"There are infinite solutions:\n" +
									"Two lines are totally the same one !!");
							break;
						case ONE_ZERO_EQUATION:
							JOptionPane.showMessageDialog(null,
									"There are infinite solutions:\n" +
									"One of the equations is always true !!");
							break;
						case BOTH_ZERO_EQUATION:
							JOptionPane.showMessageDialog(null,
									"There are infinite solutions:\n" +
									"Both equations are always true !!");
							break;
						case ONE_FALSE_EQUATION:
							JOptionPane.showMessageDialog(null,
									"There is no any solutions:\n" +
									"One of the equations is always false !!");
							break;
						case BOTH_FALSE_EQUATION:
							JOptionPane.showMessageDialog(null,
									"There is no any solutions:\n" +
									"Both equations are always false !!");
							break;
						}
					}
				}catch(Exception anyError){
					JOptionPane.showMessageDialog(null,
							"Invalid Input:\n" +
							"Something may be empty or invalid,\n" +
							"please check your input !!");
				}
			}
		});
		
		/** Add Reset Button Event */
		reset_btn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent arg0) {
				for(int k=0;k<text_group.length;k++){
					text_group[k].setText("");
				}
			}
		});
		
	}
	
	/** Main Function will call the Constructor */
	public static void main(String args[]){
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				A32 myWindow = new A32();
				//Show the application window
				myWindow.setVisible(true);
			}
		});
	}
	
}
