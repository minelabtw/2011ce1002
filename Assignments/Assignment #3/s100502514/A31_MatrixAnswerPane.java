/**
 * This class is used by Assignment 3-1 for Matrix input TextFields (GUI)
 */

package a3.s100502514;

import java.awt.*;

import javax.swing.*;

public class A31_MatrixAnswerPane extends JPanel {
	
	//Private variables
	private JPanel matrixContent;
	private int width, height;
	private String message_on_error;
	
	
	/** Default Constructor */
	public A31_MatrixAnswerPane(){
		/** Default Constructor will make a 3x3 matrix */
		this(null);
	}
	
	/** Specified Constructor with height and width */
	public A31_MatrixAnswerPane(double[][] array){
		this(array,"Math Error");
	}
	
	/** Specified Constructor with height and width,
		and also the message that shows in case of null-array-error. */
	public A31_MatrixAnswerPane(double[][] array, String error_msg){
		message_on_error=error_msg;
		if(array==null){
			/** Show the "error" message if array is null. */
			width=height=0;
			setLayout(new BorderLayout());
			add(new MyBigLabel(message_on_error),BorderLayout.CENTER);
			return;
		}
		/** Initialize the layout */
		setLayout(new BorderLayout(10,10));
		/** Set the height */
		height=array.length;
		/** Set the width that will be the maximun length for every array[indexes]. */
		width=0;
		for(int i=0; i<array.length; i++){
			if(width<array[i].length){
				width=array[i].length;
			}
		}
		/** Add the grid at the center */
		matrixContent = new JPanel();
		add(matrixContent,BorderLayout.CENTER);
		matrixContent.setLayout(new GridLayout(height, width, 4, 4));
		/** Add Labels of Numbers */
		for(int i=0; i<array.length; i++){
			for(int j=0; j<array[i].length; j++){
				JLabel txt;
				/** Add Labels that can round off to 3rd digit after decimal point */
				/** But, if the value is infinite, then "Infinity" will be shown. */
				if(Double.isInfinite(array[i][j])) txt = new JLabel(""+(array[i][j]));
				else txt = new JLabel(""+Math.rint(array[i][j]*1000)/1000);
				matrixContent.add(txt);
			}
			/** Add some labels "?" to fill out a line if necessary.
				(Theorically, this loop may not be executed) */
			for(int m=array[i].length; m<width; m++){
				JLabel txt = new JLabel("?");
				matrixContent.add(txt);
			}
		}
		
		/** Add the paramtheses on both sides */
		MyBigLabel leftParam = new MyBigLabel("[");
		leftParam.setFont(new Font("Arial Narrow",0,140));
		add(leftParam,BorderLayout.WEST);
		MyBigLabel rightParam = new MyBigLabel("]");
		rightParam.setFont(new Font("Arial Narrow",0,140));
		add(rightParam,BorderLayout.EAST);
	}
	
	/** Get the double array for calculation */
	/** Copied from class A31_MatrixPane, which may not be used in Assignment 3-1,
		but reusable when other codes need.(in the future) */
	public double[][] getDoubleData(){
		double[][] M = new double[height][width];
		try{
			for(int k=0;k<matrixContent.getComponentCount();k++){
				int i = k/width, j = k%width;
				M[i][j] = Double.parseDouble(((JLabel)(matrixContent.getComponent(k))).getText().replace(",", ""));
			}
		}catch(Exception err){
			/** Return Null if the text fields are invalid */
			/** Null-array-error messages may return null when using this method */
			return null;
		}
		return M;
	}
}
