/**
 * CE1002-100502514 FlashTeens Chiang
 * [The Following Rules are from ce1002 website]
 * Assignment 3-2 Implement LinearEquation
	
	1. This class defines a 2X2 system of linear equations:
		
		ax+by=e, cx+dy=f gets x=(ed-bf)/(ad-bc), y=(af-ec)/(ad-bc)
		
	2. The class contains for assignment requirements:
	  *	Private data fields a, b, c, d, e, and f.
	  *	A constructor with the arguments for a, b, c, d, e, and f.
	  *	A method named isSolvable() that returns true if ad �V bc is not 0.
	  *	Method getX() and getY() that returns the solution for the equation.
	
	3. The class also contains some methods for extra points:
	  *	enum 7 kinds of the states in KramerStatus and return one of them.
			
	See ce1002 website for more informations.
	
 */

package a3.s100502514;

public class LinearEquation {
	/** Assignment Requirement: 6 Private member variables */
	private double a, b, c, d, e, f;
	
	/** Assignment Requirement: Declare a constructor for initialization of private members a~f */
	public LinearEquation(double[] nums){
		try{
			/** Assign the array elements to private variables. */
			a=nums[0];
			b=nums[1];
			e=nums[2];
			c=nums[3];
			d=nums[4];
			f=nums[5];
		}catch(Exception err){
			/** Set a suite of invalid values if nums.length<6 */
			a=b=c=d=e=f=0;
		}
	}
	
	/** Private method that returns ad-bc */
	private double getDelta(){
		return a*d-b*c;
	}
	
	/** Get the X solution in the equation */
	public double getX(){
		return (e*d-b*f)/getDelta();
	}
	
	/** Get the Y solution in the equation */
	public double getY(){
		return (a*f-e*c)/getDelta();
	}
	
	/** Determine if this equation is solvable by condition ad-bc!=0 */
	public boolean isSolvable(){
		return getDelta()!=0;
	}
	
	/** List all the Kramer status that is possible to happen. */
	public static enum KramerStatus{SOLVABLE, PARALELL, OVERLAP,
		ONE_ZERO_EQUATION, BOTH_ZERO_EQUATION, ONE_FALSE_EQUATION, BOTH_FALSE_EQUATION};
	/** The following method is an ADVANCED version from isSolvable():
	 * 	Returns a KramerStatus constant for the status of solution. */
	public KramerStatus getKramerStatus(){
		if(!isSolvable()){
			if(a==0&&b==0&&e!=0&&c==0&&d==0&&f!=0)return KramerStatus.BOTH_FALSE_EQUATION;
			else if((a==0&&b==0&&e!=0)||(c==0&&d==0&&f!=0))return KramerStatus.ONE_FALSE_EQUATION;
			else if(a==0&&b==0&&e==0&&c==0&&d==0&&f==0)return KramerStatus.BOTH_ZERO_EQUATION;
			else if((a==0&&b==0&&e==0)||(c==0&&d==0&&f==0))return KramerStatus.ONE_ZERO_EQUATION;
			else if(a*f==c*e&&b*f==d*e)return KramerStatus.OVERLAP;
			else return KramerStatus.PARALELL;
		}
		return KramerStatus.SOLVABLE;
	}
	
}
