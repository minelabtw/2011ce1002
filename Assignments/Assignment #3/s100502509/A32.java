package a3.s100502509;

import java.util.Scanner;


public class A32 
{
	public static void main(String [] args)
	{
		//LinearEquation liner = new LinearEquation();
		System.out.println("Please enter a, b ,c, d, e, and f:  ");
		Scanner input = new Scanner(System.in);
		System.out.println("input");
		double a = input.nextDouble();//declare six double numbers
		double b = input.nextDouble();
		double c = input.nextDouble();
		double d = input.nextDouble();
		double e = input.nextDouble();
		double f = input.nextDouble();
		System.out.println("output");
		LinearEquation linearEquation = new LinearEquation(a,b,c,d,e,f);//Declare object linearEquation
		linearEquation.getmethod();//call function getmethod
		//System.out.println("x is "  + linearEquation.getX() + "y is" + linearEquation.getY());
		if(LinearEquation.isSolvable() == true)
		{
			System.out.printf("x is " + linearEquation.getX() + " y is " + linearEquation.getY());//print answer
		}
				
		else
		{
			System.out.println("There is no solution!!");
		}
	}
}
