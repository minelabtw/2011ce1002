package a3.s100502509;
import java.util.Scanner;
import java.text.*; 
public class A31 
{
	public static void main(String [] args)
	{
		DecimalFormat df = new DecimalFormat( "#.0"); //取到小數第一位
		double [] [] array1 = new double [3] [3];//Declare array
		double [] [] array2 = new double [3] [3];
		double [][] result = new double [3] [3]; 
		Scanner input = new Scanner(System.in);
		System.out.println("Enter matrix1: ");
		for(int i=0; i<3; i++)
		{
			for(int j=0; j<3; j++)
			{
				 array1 [i] [j] = input.nextDouble();//store input inside the array1
			}
		}
		System.out.println("Enter matrix2: ");
		for(int k=0; k<3; k++)
		{
			for(int l=0; l<3; l++)
			{
				array2 [k] [l] = input.nextDouble();//store input inside the array2
			}
		 }
		
		result =  multipleMatrix(array1,array2);//Pass array1 and array2 to the function multipleMatrix
		System.out.println("the matrices are multiplied as follows: ");
		
		for(int y=0; y<3; y++)
		{
			for(int x=0; x<3; x++)
			{
				if(array1[y][x]<0)
				{
					System.out.print(df.format(array1[y][x]));//output array1
				}
				else
				{
					System.out.print(df.format(array1[y][x])+" "); 
				}
			}
			 
			if(y==1)
			{
				System.out.print("*\t"); 
			}
			else
			{
				System.out.print("\t"); 
			}
			 
			for(int a=0; a<3; a++)
			{
				if(array2[y][a]<0)
				{
					System.out.print(df.format(array2[y][a])); //output array2
				}
				else
				{
					System.out.print(df.format(array2[y][a])+" "); 
				}
			}
			if(y==1)
			{
				System.out.print("=\t"); 
			}
			else
			{
				System.out.print("\t"); 
			}
		 
			for(int b=0; b<3; b++)
			{
				if(result[y][b]<0)
				{
					System.out.print( df.format(result[y][b])); 
				}
				 
				else
				{
					System.out.print( df.format(result[y][b])+" " ); 
				}
			}
			System.out.print( "\n" ); 
		}
	}
	
	
	public static double [] []  multipleMatrix(double [] [] a, double [] [] b )//function to multiple matrix
	{
		double [] [] array3 = new double [3] [3];
		int e=0,k=0;
		for(int m=0; m<3; m++)
		{
			e=0;
			for(int n=0; n<3; n++)
			{
				
				
				array3 [m] [n] = a [m] [0] * b [0] [e] + a [m] [1] * b [1] [e] + a [m] [2] * b [2] [e];//multiple matrix
				e++;
			}
		
			k++;
			
		}
			
		return array3;
	}
}
