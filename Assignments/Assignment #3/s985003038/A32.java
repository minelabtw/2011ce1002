	// ========================================================================================= //
	//		Author: Pluto Chan, Ka Hou															 //
	//		E-mail: jesic01@jesic-tech.com														 //
	//		Date: 2012/3/8																		 //
	//		Last Edited: 2012/3/10																 //
	//		Description: Assignment III for course CE1002 - Introduction to Computer Science II	 //
	//					 Testing program for the LinearEquation class, here, we check the 		 //
	//					 readability and writability of the coefficients, the method to check	 //
	//					 the solvability of the equation, and the method to find the solutions	 //
	// ========================================================================================= //

package a3.s985003038;

import java.util.Scanner;

public class A32 {
	public static void main(String[] argv){
		Scanner input = new Scanner(System.in);
		while(true){
			System.out.println("Input");									// create a linear equation object, and assign the six coefficients to it
			LinearEquation equation = new LinearEquation(input.nextDouble(), input.nextDouble(), input.nextDouble(), input.nextDouble(), input.nextDouble(), input.nextDouble());
			System.out.println("Check your input");							// output the six coefficients from the object to check if the writing and reading method is correct
			System.out.println(equation.getA() + " " + equation.getB() + " " + equation.getC() + " " + equation.getD() + " " + equation.getE() + " " + equation.getF());
			System.out.println("Output");
			if(equation.isSolvable())										// check if the equation is solvable, if yes, find the solutions
				System.out.println("x is " + equation.getX() + " and y is " + equation.getY());
			else															// otherwise, show out the unsolvable result
				System.out.println("The equation has no solution");
			System.out.println();
		}
	}
}