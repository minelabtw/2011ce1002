	// ========================================================================================= //
	//		Author: Pluto Chan, Ka Hou															 //
	//		E-mail: jesic01@jesic-tech.com														 //
	//		Date: 2012/3/8																		 //
	//		Last Edited: 2012/3/10																 //
	//		Description: Assignment III for course CE1002 - Introduction to Computer Science II	 //
	//					 Class for a matrix, here is the description to all the methods			 //
	//					 - You can create a matrix object with specified name, columns and rows, //
	//					   default matrix will be given a size of 3 by 3 with no name			 //
	//					 - You can read the value of column, row, and the name of the matrix by	 //
	//					   the methods called getColumn, getRow	and getName						 //
	//					 - You can assigned all the elements into the matrix by the method		 //
	//					   called assignElements												 //
	//					 - You can read the value of an element by the method called getElement	 //
	//					 - You can print out all the elements by the method called printMatrix	 //
	//					 - You can calculate the determinant of the matrix by the method		 //
	//					   called getDeterminant												 //
	//					 - You can calculate the adjacency matrix by the method adjacencyMatrix	 //
	//					 - You can calculate the inverse matrix by the method inverseMatrix		 //
	//					 - You can add two matrices by the static method called addMatrices		 //
	//					 - You can subtract two matrices by the static method subtractMatrices	 //
	//					 - You can multiple two matrices by the static method multipleMatrices	 //
	// ========================================================================================= //

package a3.s985003038;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Matrix {
	private static int matricesCounter = 0;								// static counter counting the number of matrices
	private int index;													// the index of the matrix
	private int column;													// the number of columns of the matrix
	private int row;													// the number of rows of the matrix
	private double[][] elements;										// elements array of the matrix
	private String name;												// the name of the matrix
	
	public Matrix(){													// constructor of matrix with no parameters
		column = 3;														// default matrix column to be 3
		row = 3;														// default matrix row to be 3
		elements = new double[row][column];								// create elements array in double
		matricesCounter++;												// increasing matrices counter
		index = matricesCounter;										// update index of the matrix
	}
	
	public Matrix(String matrixName){									// constructor of matrix with only name parameter
		name = matrixName;												// update the name of the matrix
		column = 3;														// default matrix column to be 3
		row = 3;														// default matrix row to be 3
		elements = new double[row][column];								// create elements array in double
		matricesCounter++;												// increasing matrices counter
		index = matricesCounter;										// update index of the matrix
	}
	
	public Matrix(int x, int y){										// constructor of matrix with only size parameters
		column = x;														// define the column of the matrix
		row = y;														// define the row of the matrix
		elements = new double[row][column];								// create elements array in double
		matricesCounter++;												// increasing matrices counter
		index = matricesCounter;										// update index of the matrix
	}
	
	public Matrix(int x, int y, String matrixName){						// constructor of matrix with name, and size parameters
		name = matrixName;												// update the name of the matrix
		column = x;														// define the column of the matrix
		row = y;														// define the row of the matrix
		elements = new double[row][column];								// create elements array in double
		matricesCounter++;												// increasing matrices counter
		index = matricesCounter;										// update index of the matrix
	}
	
	protected void finalize(){											// destructor of matrix
		matricesCounter--;
	}
	
	// ========================================================================================= //
	//		Name: getColumn																		 //
	//		Input: none																			 //
	//		Output: column of the matrix														 //
	//		Description: get the column of the matrix											 //
	// ========================================================================================= //
	public int getColumn(){
		return column;
	}
	
	// ========================================================================================= //
	//		Name: getRow																		 //
	//		Input: none																			 //
	//		Output: row of the matrix															 //
	//		Description: get the row of the matrix												 //
	// ========================================================================================= //
	public int getRow(){
		return row;
	}
	
	// ========================================================================================= //
	//		Name: getName																		 //
	//		Input: none																			 //
	//		Output: name of the matrix in string												 //
	//		Description: get the name of the matrix												 //
	// ========================================================================================= //
	public String getName(){
		return (name == null ? "Matrix " + index : name);
	}
	
	// ========================================================================================= //
	//		Name: assignElements																 //
	//		Input: a scanner for system.in														 //
	//		Output: none																		 //
	//		Description: assign all the elements in the matrix									 //
	// ========================================================================================= //
	public void assignElements(){
		Scanner input = new Scanner(System.in);
		System.out.print("Enter " + getName() + ": ");
		for(int i = 0; i < row; i++)									// if the matrix has a name, print out the name of the matrix, otherwise, print out the index
			for(int j = 0; j < column; j++)
				elements[i][j] = input.nextDouble();					// input each elements in the matrix
	}
	
	// ========================================================================================= //
	//		Name: getElement																	 //
	//		Input: 2-dimensional coordinate of the matrix										 //
	//		Output: the value of the element at the input coordinate							 //
	//		Description: get the value of an element in the matrix								 //
	// ========================================================================================= //
	public double getElement(int x, int y){
		DecimalFormat format = new DecimalFormat("#.0");				// round off to 1 decimal place
		if(x < column && y < row)
			return Double.valueOf(format.format(elements[y][x]));		// return the value of the element
		else {															// if the element is out of the matrix
			System.out.println("Error: Getting element out of the matrix");
			return -1;													// show error message and return a non-meaning result
		}
	}
	
	// ========================================================================================= //
	//		Name: printMatrices																	 //
	//		Input: a 2-dimensional array matrix in double										 //
	//		Output: none																		 //
	//		Description: print out all the elements in the matrix								 //
	// ========================================================================================= //
	public void printMatrix(){
		DecimalFormat format = new DecimalFormat("#.0");				// round off to 1 decimal place
		System.out.println("Elements in " + getName() + ": ");
		for(int i = 0; i < row; i++){									// if the matrix has a name, print out the name of the matrix, otherwise, print out the index
			for(int j = 0; j < column; j++)
				System.out.print(format.format(elements[i][j]) + "\t");	// print out all the elements in a rows 
			System.out.print("\n");										// go to next line when finish printing out a row
		}
	}
	
	// ========================================================================================= //
	//		Name: getDeterminant																 //
	//		Input: none																			 //
	//		Output: determinant of the matrix in double											 //
	//		Description: calculate the determinant of the matrix								 //
	// ========================================================================================= //
	public double getDeterminant(){
		if(row != column){												// there is no determinant if the matrix isn't a square matrix
			System.out.println("Error: Only square matrix has a determinant");
			return -1;													// show the error message and return a non-meaning result
		} else {														// otherwise, pass the elements array to the subDeterminant method to calculate the determinant
			return subDeterminant(elements);
		}
	}
	
	// ========================================================================================= //
	//		Name: subDeterminant																 //
	//		Input: elements array in double														 //
	//		Output: determinant of the input sub matrix in double								 //
	//		Description: calculate the determinant of the input matrix by decreasing the matrix	 //
	//					 and calculate the determinant of the sub matrix recursively			 //
	// ========================================================================================= //
	private double subDeterminant(double[][] elements){
		if(elements.length == 1 && elements[0].length == 1){			// if the matrix has been decreased to only one element left
			return elements[0][0];										// return the element directly
		} else {														// otherwise, decrease the level of the matrix
			double result = 0;
			for(int i = 0; i < elements[0].length; i++){				// decrease the matrix for each column, and summation the result of the determinant of the sub matrix
				result += (i % 2 == 1 ? -1 : 1) * elements[0][i] * subDeterminant(subMatrix(elements, i, 0));
			}
			return result;												// return the result of the determinant of that sub matrix
		}
	}
	
	// ========================================================================================= //
	//		Name: subMatrix																		 //
	//		Input: elements array in double, the cross position of the column and row			 //
	//		Output: elements array of the sub matrix in double									 //
	//		Description: create a sub matrix of the input matrix								 //
	// ========================================================================================= //
	private double[][] subMatrix(double[][] elements, int x, int y){
		double[][] subElements = new double[elements.length-1][elements[0].length-1];
		int subRow = 0;													// we create a sub matrix by decreasing the column and row by 1
		for(int i = 0; i < elements.length; i++){
			if(i == y) continue;										// copy all the elements in the input matrix to the sub matrix except the elements on the y-row
			int subColumn = 0;
			for(int j = 0; j < elements[0].length; j++){				// copy all the elements in the input matrix to the sub matrix except the elements on the x-column
				if(j == x) continue;
				subElements[subRow][subColumn] = elements[i][j];
				subColumn++;
			}
			subRow++;
		}
		return subElements;
	}
	
	// ========================================================================================= //
	//		Name: adjacencyMatrix																 //
	//		Input: none																			 //
	//		Output: adjacency matrix						 									 //
	//		Description: calculate the adjacency matrix											 //
	// ========================================================================================= //
	public Matrix adjacencyMatrix(){
		if(row != column){												// there is no adjacency matrix if the matrix isn't a square matrix
			System.out.println("Error: Only square matrix has an adjacency matrix");
			return null;												// show the error message and return a null result
		} else {
			Matrix result = new Matrix(column, row, "Adjacency Matrix of " + getName());// create a resulting matrix to keep the result 
			result.elements = adjacencyElements();						// get the elements array and keep it into the resulting matrix 
			return result;
		}
	}
	
	// ========================================================================================= //
	//		Name: adjacencyElements																 //
	//		Input: none																			 //
	//		Output: elements array of adjacency matrix in double								 //
	//		Description: calculate all the values of elements in the adjacency matrix			 //
	// ========================================================================================= //
	private double[][] adjacencyElements(){
		double[][] adjElement = new double[row][column];				// create a double array to keep the elements
		for(int i = 0; i < row; i++){
			for(int j = 0; j < column; j++){
				adjElement[j][i] = ((i + j) % 2 == 1 ? -1 : 1) * subDeterminant(subMatrix(elements, j, i));
			}															// find all the value the of the elements, that is, the determinant of the sub matrix
		}
		return adjElement;												// return the elements array
	}
	
	// ========================================================================================= //
	//		Name: inverseMatrix																	 //
	//		Input: none																			 //
	//		Output: inverse matrix							 									 //
	//		Description: calculate the inverse matrix											 //
	// ========================================================================================= //
	public Matrix inverseMatrix(){
		if(row != column){												// there is no inverse matrix if the matrix isn't a square matrix
			System.out.println("Error: Only square matrix has an inverse matrix");
			return null;												// show the error message and return a null result
		} else {														// otherwise, pass the elements array to the subDeterminant method to calculate the determinant
			double det = getDeterminant();
			if(det == 0){												// if the determinant of the matrix is 0, there is no inverse matrix
				System.out.println("The matrix is singular, which means there is no inverse matrix");
				return null;											// return the message and a null result
			} else {													// create a resulting matrix to keep the result
				Matrix result = new Matrix(column, row, "Inverse Matrix of " + getName());
				double[][] adjElement = adjacencyElements();			// find the adjacency matrix
				for(int i = 0; i < adjElement.length; i++)
					for(int j = 0; j < adjElement[0].length; j++)
						adjElement[i][j] /= det;						// divide all the elements in the adjacency matrix by the determinant, so we can have the inverse matrix
				result.elements = adjElement;							// save all the elements into the inverse matrix
				return result;											// and return the inverse matrix
			}
		}
	}
	
	// ========================================================================================= //
	//		Name: addMatrices																	 //
	//		Input: two matrix																	 //
	//		Output: one resulting matrix														 //
	//		Description: add operation on two matrices											 //
	// ========================================================================================= //
	public static Matrix addMatrices(Matrix a, Matrix b){
		if(a.column != b.column || a.row != b.row){						// if the sizes do not match
			System.out.println("Error: Two matrices with different size cannot be added");
			return null;												// show the error message and return a null result
		} else {
			Matrix c = new Matrix(a.column, a.row, "Resulting Matrix of " + a.getName() + " + " + b.getName());
			for(int i = 0; i < a.row; i++)								// create a resulting matrix to keep the result
				for(int j = 0; j < a.column; j++)
					c.elements[i][j] = a.elements[i][j] + b.elements[i][j];
			return c;													// add each elements in the input matrices
		}
	}
	
	// ========================================================================================= //
	//		Name: subtractMatrices																 //
	//		Input: two matrix																	 //
	//		Output: one resulting matrix														 //
	//		Description: add operation on two matrices											 //
	// ========================================================================================= //
	public static Matrix subtractMatrices(Matrix a, Matrix b){
		if(a.column != b.column || a.row != b.row){						// if the sizes do not match
			System.out.println("Error: Two matrices with different size cannot be subtracted");
			return null;												// show the error message and return a null result
		} else {
			Matrix c = new Matrix(a.column, a.row, "Resulting Matrix of " + a.getName() + " + " + b.getName());
			for(int i = 0; i < a.row; i++)								// create a resulting matrix to keep the result
				for(int j = 0; j < a.column; j++)
					c.elements[i][j] = a.elements[i][j] - b.elements[i][j];
			return c;													// subtract each elements in the input matrices
		}
	}
	
	// ========================================================================================= //
	//		Name: multipleMatrices																 //
	//		Input: two matrix																	 //
	//		Output: one resulting matrix														 //
	//		Description: add operation on two matrices											 //
	// ========================================================================================= //
	public static Matrix multipleMatrices(Matrix a, Matrix b){
		if(a.column != b.row){											// if the sizes do not match
			System.out.println("Error: Two matrices with size cannot be multipled");
			return null;												// show the error message and return a null result
		} else {
			Matrix c = new Matrix(b.column, a.row, "Resulting Matrix of " + a.getName() + " + " + b.getName());
			for(int i = 0; i < a.row; i++)								// for each resulting elements, is equal to each elements in that row multiple by each elements in that column
				for(int j = 0; j < b.column; j++)						// therefore, we need three FOR loop, the first two loop is stepping over all the elements in the resulting matrix
					for(int k = 0; k < a.column; k++)					// and the last loop is multiplying all elements in the column of A and row of B
						c.elements[i][j] += a.elements[i][k] * b.elements[k][j];
																		// finally, the summation of the multiple will be the result of that element
			return c;													// return the multiplication result
		}
	}
}