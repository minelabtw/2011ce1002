	// ========================================================================================= //
	//		Author: Pluto Chan, Ka Hou															 //
	//		E-mail: jesic01@jesic-tech.com														 //
	//		Date: 2012/3/8																		 //
	//		Last Edited: 2012/3/10																 //
	//		Description: Assignment III for course CE1002 - Introduction to Computer Science II	 //
	//					 Testing program for the Matrix class, here, we test to create matrices, //
	//					 assigning value to elements, print out the whole matrix, taking multiple//
	//					 operation, and get out the only one element from the matrix			 //
	// ========================================================================================= //

package a3.s985003038;

public class A31 {	
	public static void main(String[] argv){
		Matrix a = new Matrix();													// create a 2-dimensional array matrices with size 3 by 3
		Matrix b = new Matrix();													// create another 2-dimensional array matrices with size 3 by 3

		while(true){
			a.assignElements();														// assign all the elements in the matrix
			b.assignElements();														// assign all the elements in the matrix
			
			System.out.println();
			a.printMatrix();														// print out all the elements in the matrix
			
			System.out.println();
			b.printMatrix();														// print out all the elements in the matrix
			
			Matrix result = Matrix.multipleMatrices(a, b);							// do multiple operation to the two matrices
			
			if(result != null){														// if the multiple operation is correctly execute
				System.out.println("\nThe matrices are multiplied as follows: ");	// show the result using the format the TA asked for
				int y = Math.max(a.getRow(), b.getRow());
				for(int i = 0; i < y; i++){
					if(i < a.getRow())
						for(int j = 0; j < a.getColumn(); j++)
							System.out.print(a.getElement(j, i) + "\t");			// get out all elements at each rows in matrix A
					else
						for(int j = 0; j < a.getColumn(); j++)
							System.out.print("\t");
					
					if(i != y / 2) System.out.print("\t");							// print the multiple operator at the middle row
					else System.out.print("*\t");									// and also print out a space at the other rows
					
					if(i < b.getRow())
						for(int j = 0; j < b.getColumn(); j++)
							System.out.print(b.getElement(j, i) + "\t");			// get out all elements at each rows in matrix B
					else
						for(int j = 0; j < b.getColumn(); j++)
							System.out.print("\t");
					
					if(i != y / 2) System.out.print("\t");							// print the equal operator at the middle row
					else System.out.print("=\t");									// and also print out a space at the other rows
					
					if(i < result.getRow())
						for(int j = 0; j < result.getColumn(); j++)
							System.out.print(result.getElement(j, i) + "\t");		// get out all elements at each rows in resulting matrix
					else
						for(int j = 0; j < b.getColumn(); j++)
							System.out.print("\t");
					
					System.out.println();											// goto next line after finish printing out a row
				}
			}
			System.out.println();
		}
	}
}