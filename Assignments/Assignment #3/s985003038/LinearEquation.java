	// ========================================================================================= //
	//		Author: Pluto Chan, Ka Hou															 //
	//		E-mail: jesic01@jesic-tech.com														 //
	//		Date: 2012/3/8																		 //
	//		Last Edited: 2012/3/10																 //
	//		Description: Assignment III for course CE1002 - Introduction to Computer Science II	 //
	//					 Class for a linear equation in form of ax + by = c, in which you can	 //
	//					 read and write the coefficients of the equation, check the solvability  //
	//					 of the equation, and also find the solutions to the equation			 //
	// ========================================================================================= //

package a3.s985003038;

public class LinearEquation {
	private double a, b, c, d, e, f;
	
	public LinearEquation(double newA, double newB, double newC, double newD, double newE, double newF){
		a = newA; b = newB; c = newC; d = newD; e = newE; f = newF;		// constructor, assign six coefficient
	}
	
	// ========================================================================================= //
	//		Name: getA, getB, getC, getD, getE, getF											 //
	//		Input: none																			 //
	//		Output: value of the corresponding coefficients in double							 //
	//		Description: return the value of the corresponding coefficients						 //
	// ========================================================================================= //
	public double getA(){ return a; }
	public double getB(){ return b; }
	public double getC(){ return c; }
	public double getD(){ return d; }
	public double getE(){ return e; }
	public double getF(){ return f; }
	
	// ========================================================================================= //
	//		Name: getX																			 //
	//		Input: none																			 //
	//		Output: value of X in double														 //
	//		Description: check if the equation is solvable, if yes, return the value of X		 //
	// ========================================================================================= //
	public double getX(){
		if(isSolvable()) return (e * d - b * f) / (a * d - b * c);		// if the equation is solvable, return X
		else {															// otherwise, show error message since we can get the value of X
			System.out.println("Error: The equation is unsolvable");
			return -1;
		}
	}

	// ========================================================================================= //
	//		Name: getY																			 //
	//		Input: none																			 //
	//		Output: value of Y in double														 //
	//		Description: check if the equation is solvable, if yes, return the value of Y		 //
	// ========================================================================================= //
	public double getY(){
		if(isSolvable()) return (a * f - e * c) / (a * d - b * c);		// if the equation is solvable, return Y
		else{															// otherwise, show error message since we can get the value of Y
			System.out.println("Error: The equation is unsolvable");
			return -1;
		}
	}
	
	// ========================================================================================= //
	//		Name: isSolvable																	 //
	//		Input: none																			 //
	//		Output: solvable flag in boolean													 //
	//		Description: check if the equation is solvable										 //
	// ========================================================================================= //
	public boolean isSolvable(){
		if(a * d - b * c != 0) return true;								// check whether the equation is solvable 
		else return false;
	}
}