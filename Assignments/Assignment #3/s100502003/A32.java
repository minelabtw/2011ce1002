package a3.s100502003;

import java.util.Scanner;

public class A32 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double[] numbers = new double[6];
		System.out.print("Please enter 6 numbers: ");
		for(int i=0; i<6; i++) {
			numbers[i] = input.nextDouble(); // store the numbers in an array
		}
		
		// initialize the values for the variables in the constructor
		LinearEquation le = new LinearEquation(numbers[0], numbers[1], numbers[2], numbers[3], numbers[4], numbers[5]);
		
		if(le.isSolvable() == true) { // situation that have result
			System.out.println("x is " + le.getX() + " and y is " + le.getY()); // show the result
		}
		else
			System.out.println("The equation has no solution."); // show the error information
	}
}
