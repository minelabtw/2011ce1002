package a3.s100502003;

public class LinearEquation {
	private static double a, b, c, d, e, f; // declare the variables with private state
	public static double[][] array1 = new double[2][2]; // a two-dimensional array to store first four numbers
	public static double[] array2 = new double[2]; // another array to store last two numbers
	// initialize the variables and store them into the arrays
	LinearEquation(double A, double B, double C, double D, double E, double F) { 
		a = A;
		b = B;
		c = C;
		d = D;
		e = E;
		f = F;
		array1[0][0] = a;
		array1[0][1] = b;
		array1[1][0] = c;
		array1[1][1] = d;
		array2[0] = e;
		array2[1] = f;
	}

	public boolean isSolvable() { // consider that whether the operations have result
		double denominator = array1[0][0]*array1[1][1] - array1[0][1]*array1[1][0];
		if((denominator) != 0)
			return true;
		else 
			return false;
	}
	
	public double getX() { // method that count the solution for x
		double denominator_x = array1[0][0]*array1[1][1] - array1[0][1]*array1[1][0]; // count denominator with the formula
		swap(array1, array2); // change the numbers
		double numerator_x = array1[0][0]*array1[1][1] - array1[0][1]*array1[1][0]; // count numerator with the formula
		swap(array2, array1); // change them back so that the following method can use the arrays with their initial values
		return numerator_x / denominator_x; // return the result
	}
	
	public double getY() { // method that count the solution for y
		double denominator_y = array1[0][0]*array1[1][1] - array1[0][1]*array1[1][0]; // denominator
		for(int i=0; i<2; i++) { // change d to f and change e to b
			// because of that the system will not use the arrays again, it is possible that we don't change them back
			array1[i][1] = array2[i];
		}
		double numerator_y = array1[0][0]*array1[1][1] - array1[0][1]*array1[1][0]; // numerator
		return numerator_y / denominator_y;
	}
	
	public static void swap(double[][] Array1, double[] Array2) { 
		for(int i=0; i<2; i++) { // exchange (a,e) & (c,f) 
			double temp = Array1[i][0];
			Array1[i][0] = Array2[i];
			Array2[i] = temp;
		}
	}
	
	public static void swap(double[] Array_2, double[][] Array_1) {
		for(int j=0; j<2; j++) { // change them back
			double temp = Array_2[j];
			Array_2[j] = Array_1[j][0];
			Array_1[j][0] = temp;
		}
	}
}
