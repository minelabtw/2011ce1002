package a3.s100502003;

import java.text.NumberFormat; // import it in order to decide the place of decimal point
import java.util.Scanner;

public class A31 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double[][] matrix1 = new double[3][3]; // declare the first array 
		System.out.print("Enter matrix1: ");
		for(int a=0; a<3; a++) {
			for(int b=0; b<3; b++) {
				matrix1[a][b] = input.nextDouble(); // input the numbers for the array
			}
		}
		double[][] matrix2 = new double[3][3]; // declare the second array
		System.out.print("Enter matrix2: ");
		for(int c=0; c<3; c++) {
			for(int d=0; d<3; d++) {
				matrix2[c][d] = input.nextDouble(); // input the numbers for the array
			}
		}
		
		double[][] solution = new double[3][3]; // array that used to store the result
		solution = multipleMatrix(matrix1, matrix2); // call the method and store into the array
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(1); // only leave one number after the decimal point 
		System.out.println("The matrices are multiplied as follows:");
		for(int e=0; e<3; e++) { // show the result
			if(e == 1) { // situation that include * & =
				System.out.print(nf.format(matrix1[e][0]) + " " + nf.format(matrix1[e][1]) + " " + nf.format(matrix1[e][2]) + " *\t");
				System.out.print(matrix2[e][0] + " " + matrix2[e][1] + " " + matrix2[e][2] + "  =\t");
				System.out.print(nf.format(solution[e][0]) + " " + nf.format(solution[e][1]) + " " + nf.format(solution[e][2]) + "\n");
			}
			else {
				System.out.print(nf.format(matrix1[e][0]) + " " + nf.format(matrix1[e][1]) + " " + nf.format(matrix1[e][2]) + "\t");
				System.out.print(matrix2[e][0] + " " + matrix2[e][1] + " " + matrix2[e][2] + "\t");
				System.out.print(nf.format(solution[e][0]) + " " + nf.format(solution[e][1]) + " " + nf.format(solution[e][2]) + "\n");
			}
		}
	}
	
	public static double[][] multipleMatrix( double[][] MATRIX1, double[][] MATRIX2) { // method that count the result
		double[][] result = new double[3][3];
		for(int i=0; i<3; i++) {
			for(int j=0; j<3; j++) {
				result[i][j] = MATRIX1[i][0]*MATRIX2[0][j] + MATRIX1[i][1]*MATRIX2[1][j] + MATRIX1[i][2]*MATRIX2[2][j]; // count it by a regular pattern
			}
		}
		return result;
	}
}
