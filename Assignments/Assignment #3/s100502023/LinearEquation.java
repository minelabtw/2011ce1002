package a3.s100502023;

public class LinearEquation
{
	//a,b,c,d,e,f為係數或常數
	private double a;
	private double b;
	private double c;
	private double d;
	private double e;
	private double f;
	
	//constructor
	LinearEquation(double a1,double b1,double c1,double d1,double e1 ,double f1)
	{
		a=a1;
		b=b1;
		c=c1;
		d=d1;
		e=e1;
		f=f1;
	}
	
	public boolean  isSolvable()  //判斷是否有解
	{
		if (a*d-b*c!=0)  //有解
		{
			return true;
		}
		else
		{
			return false;  //"a*d-b*c==0"，無解
		}
	}
	
	public double getX()  //求x的解的公式
	{
		double x;
		x=(e*d-b*f)/(a*d-b*c);
		
		return x;
	}
	
	public double getY()  //求y的解的公式
	{
		double y;
		y=(a*f-e*c)/(a*d-b*c);
		
		return y;
	}
	
	public double geta()  //回傳a的值
	{
		return a;
	}
	
	public double getb()  //回傳b的值
	{
		return b;
	}
	
	public double getc()  //回傳c的值
	{
		return c;
	}
	
	public double getd()  //回傳d的值
	{
		return d;
	}
	
	public double gete()  //回傳e的值
	{
		return e;
	}
	
	public double getf()  //回傳f的值
	{
		return f;
	}
}
