package a3.s100502023;

import java.util.Scanner;

public class A32 
{

	public static void main(String[] args)
	{
		System.out.print("input\n");
		Scanner input= new Scanner(System.in);
		
		double [] number= new double[6];//number[0]=a;number[1]=b;number[2]=c;number[3]=d;number[4]=e;number[5]=f;
		
		for (int i=0;i<6;i++)
		{
			number[i]=input.nextDouble();
		}
		
		//LinearEquation class
		LinearEquation equation= new LinearEquation(number[0],number[1],number[2],number[3],number[4],number[5]);
		
		boolean solvable=equation.isSolvable();  //solvable變數為判斷方程式是否有解
		
		if (solvable==true)//有解
		{
			System.out.print("output\n");
			System.out.print("x is " + equation.getX() + " and y is " + equation.getY());
		}
		else  //("solvable==false") 無解
		{
			System.out.print("output\n");
			System.out.print("The equation has no solution");
		}
	}

}
