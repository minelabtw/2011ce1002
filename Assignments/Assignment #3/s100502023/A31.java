package a3.s100502023;

import java.util.Scanner;

public class A31 {

	public static void main(String[] args) 
	{
		Scanner input= new Scanner(System.in);
		
		System.out.print("input:\n\n");
		System.out.print("Enter matrix1:");
		
		double[][] matrix1= new double[3][3];  //矩陣一
				
		for (int i=0;i<3;i++)
		{
			for (int j=0;j<3;j++)
			{
				matrix1[i][j]=input.nextDouble();
			}
		}
		
		System.out.print("Enter matrix2:");
		
		double[][] matrix2= new double[3][3];  //矩陣二
		
		for (int i=0;i<3;i++)
		{
			for (int j=0;j<3;j++)
			{
				matrix2[i][j]=input.nextDouble();
			}
		}
		
		double[][] multipleMatrix= new double[3][3];  //相乘矩陣
		
		multipleMatrix=multipleMatrix( matrix1,matrix2);  //multipleMatrix method 
		
		System.out.print("\noutput\n\n");
		System.out.print("the matrices are multiplied as follows:\n");
		
		display(matrix1,matrix2,multipleMatrix);  //display method	
		
	}
	
	public static double[][] multipleMatrix( double[][] a, double[][] b)
	{
		double[][] multipleMatrix= new double[3][3];
		
		for (int i=0;i<3;i++)
		{
			for (int j=0;j<3;j++)
			{
				for(int k=0;k<3;k++)
				{
					multipleMatrix[i][j]+=a[i][k]*b[k][j];  //矩陣相乘
				}
				
			}
			
		}
		
		return multipleMatrix;
	}
	
	public static void display(double [][] matrix1,double[][]matrix2,double[][] multipleMatrix)
	{
		//matrix1為矩陣一,matrix2為矩陣二,multipleMatrix為相乘後的矩陣
		for (int i=0;i<3;i++)
		{
			if (i==1)  //"i==1"時,輸出矩陣的第二列
			{
				for(int j=0;j<3;j++)  //第一個矩陣的第二列
				{
					System.out.printf("%.1f\t",matrix1[i][j]);  //只輸出到小數點後第一位
				}
			
				System.out.print("*\t");  //輸出乘
				
				for(int k=0;k<3;k++)
				{
					System.out.printf("%.1f\t",matrix2[i][k]);  //只輸出到小數點後第一位
				}
				
				System.out.print("=\t");  //輸出=
				
				for(int r=0;r<3;r++)
				{
					System.out.printf("%.1f\t",multipleMatrix[i][r]);  //只輸出到小數點後第一位
				}
				
				System.out.print("\n");
			}
			else  //第一列和第三列
			{
				for(int j=0;j<3;j++)
				{
					System.out.printf("%.1f\t",matrix1[i][j]);  //只輸出到小數點後第一位
				}
				
				System.out.print("\t");  //(排版)
				
				for(int k=0;k<3;k++)
				{
					System.out.printf("%.1f\t",matrix2[i][k]);  //只輸出到小數點後第一位
				}
				
				System.out.print("\t");  //(排版)
				
				for(int r=0;r<3;r++)
				{
					System.out.printf("%.1f\t",multipleMatrix[i][r]);  //只輸出到小數點後第一位
				}
				
				System.out.print("\n");
			}					
		}
	}
}
