package a3.s100502503;

import java.util.Scanner;


public class A31
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		double[][] a = new double[3][3];
		double[][] b = new double[3][3];
		
		System.out.print("Enter matrix1: ");  //enter the number 9 times and put them into array
		for(int i=0; i<3; i++)
		{
			for(int j=0; j<3; j++)
			{
				a[i][j] = input.nextDouble();
			}
		}
	
		System.out.print("Enter matrix2: ");
		for(int i=0; i<3; i++)
		{
			for(int j=0; j<3; j++)
			{
				b[i][j] = input.nextDouble();
			}
		}
		//show the answer
		System.out.println("the matrices are multiplied as follows: \n");
		double[][] c = multipleMatrix(a, b);
		
		for(int i=0; i<3; i++)
		{
			for(int j=0; j<3; j++)
			{
				System.out.printf("%3.1f ", a[i][j]);
			}
			
			//when finish array a, arrange the numbers by tab, and show out the math sign
			if(i==0)
				System.out.print("\t");
			else if(i==1)
				System.out.print("*\t");
			else
				System.out.print("\t");
			
			for(int j=0; j<3; j++)
			{
				System.out.printf("%3.1f ", b[i][j]);
			}
			
			if(i==0)
				System.out.print("\t");
			else if(i==1)
				System.out.print("=\t");
			else
				System.out.print("\t");
			
			for(int j=0; j<3; j++)
			{
				System.out.printf("%3.1f ", c[i][j]);
			}
			System.out.print("\n");
		}
	}
	
	//the function of calculate multiple matrix
	public static double[][] multipleMatrix(double[][] a, double[][] b)
	{
		double[][] c = new double[3][3];
		for(int i=0; i<3; i++)
		{
			//the first and second for loop is used to control array c, and the last one is plus them
			for(int j=0; j<3; j++)
			{
				for(int m=0; m<3; m++)
				{
					c[i][j] += a[i][m] * b[m][j];
				}
			}
		}
		
		return c;	
	}
}
	