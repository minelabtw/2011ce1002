package a3.s100502503;

public class LinearEquation 
{
	private static double a, b, c, d, e, f;
	
	//The constructor
	public LinearEquation(double A, double B, double C, double D, double E, double F)
	{
		set(A, B, C, D, E, F);
	}
	
	//returns true if ad �V bc is not 0.
	public static void set(double A, double B, double C, double D, double E, double F)
	{
		a = A;
		b = B;
		c = C;
		d = D;
		e = E;
		f = F;
	}
	
	public boolean isSolvable()
	{
		if((a*d) - (b*c) != 0)
			return true;
		else
			return false;
	}
	
	//calculate x
	public  double getX()
	{
		double X;
		X = ((e*d) - (b*f))/((a*d) - (b*c));
		return X;
	}
	
	//calculate y
	public double getY()
	{
		double y;
		y = ((a*f)-(e*c))/((a*d)-(b*c));
		return y;
	}
}
