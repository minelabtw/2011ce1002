package a3.s100502503;

import java.util.Scanner;

public class A32 
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		System.out.print("ax + by = e" + "\ncx + dy = f" + "\nPlease enter a ~ f");
		
		//Let user enter 6 numbers
		double A = input.nextDouble();
		double B = input.nextDouble();
		double C = input.nextDouble();
		double D = input.nextDouble();
		double E = input.nextDouble();
		double F = input.nextDouble();

		//call the constructor, if the answers exsisted, show up X and Y, if not, show out no solution.s
		LinearEquation equation = new LinearEquation(A, B, C, D, E, F);
		if(equation.isSolvable())
		{
			System.out.print("\nx = " + equation.getX());
			System.out.print("\ny = " + equation.getY());
		}
		else
			System.out.print("\nThe equation has no solution.");
		}
}
