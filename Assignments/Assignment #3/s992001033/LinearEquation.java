package a3.s992001033;

public class LinearEquation
{
	private static double a = 0;//private data member
	private static double b = 0;
	private static double c = 0;
	private static double d = 0;
	private static double e = 0;
	private static double f = 0;
	
	public LinearEquation(double inA,double inB,double inC,double inD,double inE,double inF)//constructor
	{
		a = inA;
		b = inB;
		c = inC;
		d = inD;
		e = inE;
		f = inF;
	}
	public static double getA()//6個get method
	{
		return a;
	}
	public static double getB()
	{
		return b;
	}
	public static double getC()
	{
		return c;
	}
	public static double getD()
	{
		return d;
	}
	public static double getE()
	{
		return e;
	}
	public static double getF()
	{
		return f;
	}
	public boolean isSolvable()//確定有解還是無解
	{
		if((a*d-b*c)!=0)
			return true;
		else
			return false;
	}
	public double getX()//求X的method
	{
		return (e*d-b*f)/(a*d-b*c);
	}
	public double getY()//求y的method
	{
		return (a*f-e*c)/(a*d-b*c);
	}
}
