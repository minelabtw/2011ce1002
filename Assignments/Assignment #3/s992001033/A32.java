package a3.s992001033;
import java.util.Scanner;

public class A32
{
	public static void main(String[] args)
	{
		double a = 0;
		double b = 0;
		double c = 0;
		double d = 0;
		double e = 0;
		double f = 0;
		Scanner input = new Scanner(System.in);//用來接收輸入訊息
		System.out.println("ax + by = e");
		System.out.println("cx + dy = f");
		System.out.print("Input a、b、c、d、e、f : ");
		a = input.nextDouble();
		b = input.nextDouble();
		c = input.nextDouble();
		d = input.nextDouble();
		e = input.nextDouble();
		f = input.nextDouble();
		LinearEquation test = new LinearEquation(a,b,c,d,e,f);//建立object
		if(test.isSolvable()!=false)
		{
			System.out.println("x is "+test.getX()+" and y is "+test.getY());//有解時輸出結果
		}
		else
		{
			System.out.println("The equation has no solution");//無解時輸出訊息
		}
	}
}
