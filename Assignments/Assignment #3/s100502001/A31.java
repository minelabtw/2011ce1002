package a3.s100502001;
import java.util.Scanner;
public class A31 {
	public static void main(String[] args){
		System.out.print("Enter matrix1: ");//output message to input num1-array
		double[][] num1=userinput();
		System.out.print("Enter matrix1: ");//output message to input num2-array
		double[][] num2=userinput();
		double[][] temp=multipleMatrix(num1,num2);//use array named temp to store the multipleMatrix result
		System.out.println("The matrices are multiplied as follows:");
		for(int i=0;i<num1.length;i++){ //show the operation process
			for(int j=0;j<num1.length;j++)
				System.out.print(num1[i][j]+" ");
			if(i==(int)(num1.length/2))
				System.out.print("*\t");
			else 
				System.out.print("\t");
			for(int j=0;j<num2.length;j++)
				System.out.print(num2[i][j]+" ");
			if(i==(int)(num1.length/2))
				System.out.print("=\t");
			else 
				System.out.print("\t");
			for(int j=0;j<num2.length;j++) // show the operating result
				System.out.printf("%3.2f ",temp[i][j]);
			System.out.println();
		}
			
	}
	public static double[][] userinput(){ //use a method to ask user input numbers
		Scanner input=new Scanner(System.in);
		double[][] num=new double[3][3];
		for(int i=0;i<num.length;i++){
			for(int j=0;j<num.length;j++)
				num[i][j]=input.nextDouble();
		}
		return num;
	}
	public static double[][] multipleMatrix( double[][] a, double[][] b){ //operate the multipleMatrix
		double[][] result=new double[a.length][a.length];//declare an array to store the multipleMatrix 
		int times=0;//each column of result
		while(times<a.length){
			for(int i=0;i<a.length;i++){ //the row of num1-array to multiple the column of num2-array
				double sum=0;//sum of each element result[][]
				for(int j=0;j<a.length;j++)
					sum+=a[i][j]*b[j][times];
				result[i][times]=sum;
			}
			times++;
		}
		return result;
	}
}
