package a3.s100502001;

class LinearEquation {
	private double a,b,c,d,e,f;//declare private varibles
	public LinearEquation(double A, double B, double C, double D, double E, double F){ //intialize the private varibles
		a=A;
		b=B;
		c=C;
		d=D;
		e=E;
		f=F;
	}
	public double get_a(){ //get the value of coefficient a
		return a;
	}
	public double get_b(){ //get the value of coefficient b
		return b;
	}
	public double get_c(){ //get the value of coefficient c
		return c;
	}
	public double get_d(){ ////get the value of coefficient d
		return d;
	}
	public double get_e(){ //get the value of coefficient e
		return e;
	}
	public double get_f(){ //get the value of coefficient f
		return f;
	}
	public boolean isSolvable(){ //�������o��0
		if((a*d-b*c)==0)
			return false;
		else 
			return true;
			
	}
	public double getX(){ //get the solution x
		return (e*d-b*f)/(a*d-b*c);
	}
	public double getY(){ //get the solution y
		return (a*f-e*c)/(a*d-b*c);
	}
}
