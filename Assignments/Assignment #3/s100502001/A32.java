package a3.s100502001;
import java.util.Scanner;
public class A32 {
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);
		System.out.println("ax+by=c\ncx+dy+f\nPlease enter a~f");//show the message to ask user to input the coefficient
		double[] coefficient=new double[6];//store the inputs
		boolean judge=true;
		while(judge){
			System.out.println("input:");
			for(int i=0;i<coefficient.length;i++)//input the six numbers
				coefficient[i]=input.nextDouble();
			LinearEquation getSolution=new LinearEquation(coefficient[0],coefficient[1],coefficient[2],coefficient[3],coefficient[4],coefficient[5]); //assign the value to constructor with arguments
			System.out.println("output:");
			if(getSolution.isSolvable())
				System.out.println("X is "+getSolution.getX()+" Y is "+getSolution.getY()); //show the solution
			else
				System.out.println("The equation has no solution!!"); //show the warning message
			System.out.print("1:Continue\n2:Quiting:  ");//continue or not
			int choice=input.nextInt();
			switch(choice){
				case 1:
					judge=true;//continue to calculate the solution
					break;
				case 2:
					judge=false;//close the operation
					System.out.println("Bye!!Bye!1^.<!!");
					break;
				default:
					System.out.println("There is not the choice. = =");
					break;
			}
		}
	}

}
