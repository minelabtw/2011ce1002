package a3.s100502017;

import java.util.Scanner;

public class A32 {
	public static void main (String[] args){
		Scanner input=new Scanner(System.in);
		double a,b,c,d,e,f;
		System.out.print("Please enter a,b,c,d,e,and f: ");
		a=input.nextDouble();//get user input
		b=input.nextDouble();
		c=input.nextDouble();
		d=input.nextDouble();
		e=input.nextDouble();
		f=input.nextDouble();
		LinearEquation LE=new LinearEquation(a,b,c,d,e,f);//declare a object and use construct assign value
		if(LE.isSolvable())//check weather the equation have result or not
			System.out.print("x is "+LE.getX()+" and y is "+LE.getY());
		else
			System.out.print("The equation has no solution");
	}
}