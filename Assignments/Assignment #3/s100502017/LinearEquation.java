package a3.s100502017;

public class LinearEquation {//a constructor to set a,b,c,d,e,f
	double a,b,c,d,e,f;
	LinearEquation(double A,double B,double C,double D,double E,double F){
		a=A;
		b=B;
		c=C;
		d=D;
		e=E;
		f=F;
	}
	boolean isSolvable(){//to check weather the equation have result or not
		if(a*d-b*c!=0)
			return true;
		else
			return false;	
	}
	double getX(){//to calculate x
		return (e*d-b*f)/(a*d-b*c);
	}
	double getY(){//to calculate y
		return (a*f-e*c)/(a*d-b*c);
	}
}
