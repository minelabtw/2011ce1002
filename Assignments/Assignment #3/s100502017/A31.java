package a3.s100502017;

import java.util.Scanner;

public class A31 {
	public static void main (String[] args){
		Scanner input=new Scanner(System.in);
		double[][] m1=new double[3][3];//declare 3 double array
		double[][] m2=new double[3][3];
		double[][] ans=new double[3][3];
		System.out.print("Enter matrix1: ");
		for(int i=0;i<3;i++){//let user to input matrix member
			for(int j=0;j<3;j++){
				m1[i][j]=input.nextDouble();
			}
		}
		System.out.print("Enter matrix2: ");
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				m2[i][j]=input.nextDouble();
			}
		}
		ans=multipleMatrix(m1,m2);//to call method and get result
		System.out.println("the matrices are multiplied as follows:");
		for(int i=0;i<3;i++){//output process and result
			for(int j=0;j<3;j++){
				System.out.print(m1[i][j]+"\t");
			}
			if(i==1)
				System.out.print("-\t\t");
			else
				System.out.print("\t\t");
			for(int j=0;j<3;j++){
				System.out.print(m2[i][j]+"\t");
			}
			if(i==1)
				System.out.print("=\t\t");
			else
				System.out.print("\t\t");
			for(int j=0;j<3;j++){
				System.out.print(ans[i][j]+"\t");
			}
			System.out.println("");//to change line
		}
	}
	public static double[][] multipleMatrix(double[][]a,double[][]b){//a method to multiply two matrices
		double[][] c=new double[3][3];
		for(int i=0;i<3;i++){//use two layer for loop to calculate answer
			for(int j=0;j<3;j++){
				c[i][j]=a[i][0]*b[0][j]+a[i][1]*b[1][j]+a[i][2]*b[2][j];
			}
		}
		return c;
	}
}
