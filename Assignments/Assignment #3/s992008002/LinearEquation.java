package a3.s992008002;

public class LinearEquation {

	private double a,b,c,d,e,f;
	
	
	//A constructor with the arguments for a, b, c, d, e, and f.
	public LinearEquation( double A,double B,double C,double D,double E, double F)
	{
		a = A;
		b = B;
		c = C;
		d = D;
		e = E;
		f = F;
	}
	
	
	//Six get method for a, b, c, d, e, and f.
	public void seta(double A){
		a = A;
	}
	
	public double geta(){
		return a;
	}
	
	public void setb(double B){
		b = B;
	}
	
	public double getb(){
		return b;
	}
	
	public void setc(double C){
		c = C;
	}
	
	public double getc(){
		return c;
	}
	
	public void setd(double D){
		d = D;
	}
	
	public double getd(){
		return d;
	}
	
	public void sete(double E){
		e = E;
	}
	
	public double gete(){
		return e;
	}
	
	public void setf(double F){
		f = F;
	}
	
	public double getf(){
		return f;
	}
	

    //A method that returns true if ad �V bc is not 0.
	public boolean isSolvable()
	{
		double QQ = a * d - b * c;
		if(QQ != 0)return true;
		else return false;
	}
	
	
	//returns the solution for the equation.
	public double getX() {
		double ans;
		ans = (e*d-b*f)/(a*d-b*c);
		return ans;
	}

	public double getY(){
		double ans;
		ans = (a*f-e*c)/(a*d-b*c);
		return ans;
	}
	
	
	
	
}
