package a3.s992008002;

import java.util.Scanner;

public class A31 {

	//矩陣相乘方法
		public static double[][] multiplyMatrix(double[][] a, double[][] b){
		
			double result[][] = new double[3][3];
			
			for(int i=0;i<3;i++){
		       	for(int j=0;j<3;j++){
		       		result[i][j] = a[i][0] * b[0][j] + a[i][1] * b[1][j] + a[i][2] * b[2][j] ;//使用迴圈一個一個乘
		       	}
		}

			return result;//回傳矩陣
		}
		
		
		public static void main(String args[]){
			
			double Taroka[][] = new double[3][3];
			double Carocha[][] = new double[3][3];
	        double result[][] = new double[3][3];
	     
	        
		    Scanner input = new Scanner(System.in);
			
		    System.out.print("Enter matrix1:");	 //輸入矩陣一
		    for(int i=0;i<3;i++){
		    	for(int j=0;j<3;j++)Taroka[i][j] = input.nextDouble();
		}

		
	        System.out.print("Enter matrix2:");	  //輸入矩陣二
	        for(int i=0;i<3;i++){
	       	for(int j=0;j<3;j++)Carocha[i][j] = input.nextDouble();
	}
		
	        
	        result = multiplyMatrix(Taroka, Carocha);//使用方法，並接收回傳值，java讓矩陣像物件
	        
	 
		
	       
		
		    
		    System.out.println("The matrices are added as follows:");
		    
		    //印出矩陣
		 
	        for(int i=0;i<3;i++){
	        	
	       	for(int j=0;j<3;j++)
	       		{
	       		System.out.printf("%.2f\t",Taroka[i][j]);
	       		}
	       	
	       	if(i==1)System.out.print("*");//印出乘號
	       	
	       	for(int j=0;j<3;j++)
	       	{
	       		System.out.print("\t"); //排版
	       	System.out.printf("%.2f\t",Carocha[i][j]);
	       	}
	       	
	       	if(i==1)System.out.print("=");//印出等於
	       	
	       	for(int j=0;j<3;j++)
	       	{
	       		System.out.print("\t");//排版
	       	System.out.printf("%.2f\t",result[i][j]);
	       	}
	       	
	       	System.out.println();//跳行
		}

		

	}
}
