package a3.s982003034;

import javax.swing.JOptionPane;

public class A31 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		double [][][] matrix = new double [2][3][3];
		double [][] result = new double [3][3];
		String temp;
		
		for (int i = 0; i < matrix.length; i++) {
		for (int j = 0; j < matrix[i].length; j++){
		for (int k = 0; k < matrix[i][j].length; k++){
			temp = JOptionPane.showInputDialog (
				null, "Please insert matrix" + i + " [" + j + "][" + k + "]"
				);
			matrix[i][j][k] = Double.parseDouble(temp);
				} // k
			} // j
		} // i
		
		result = multiplyMatrix (matrix[0], matrix[1]);
		temp = print2dmatrix (matrix[0], 'X', matrix[1], result);
		JOptionPane.showMessageDialog(null, "Result:\n" + temp);
		
		

	}
	
	public static double[][] multiplyMatrix (double [][] a, double [][] b){
		
		double[][] result;
		// check if the two matrix can be multiplied
		// (a's column = b's row)
		// we use the length of the first element of a row as the number of columns
		// so this function will not work (not guaranteed to work) when the matrix is a ragged array
		if (a[0].length == b.length) {
			// the result will have row = a's row && column = b's column
			result = new double [a.length][b[0].length];
			// i = result's row
			// j = result's column
			for (int i = 0; i < result.length; i++) {
				for (int j = 0; j < result[0].length; j++) {
					// k = a's column || b's row
					for (int k = 0; k < b.length; k++) {
						result [i][j] += a[i][k] * b[k][j];
					}// k
				} // j
			} // i
			
			return result;
			
		} // if
		
		else return null;
		
	} // multiplyMatrix
	
	// function to print out a 2d matrix
	// be forewarned that if the javax.swing.JOptionPane's font is not a constant-width font
	// the resulting text will not be in order 
	public static String print2dmatrix (
			double [][] op1, char operator, double [][] op2, double [][] res
			) {
		
		String output = "";
		
		// the height of the text area is the highest number of rows from all matrix
		int height = op1.length;
		if (op2.length > op1.length) height = op2.length;
		if (res.length > op1.length) height = res.length;
		if (op2.length > res.length) height = op2.length;
		int oper_pos = height / 2; // position of operator
		
		for (int i = 0; i < height; i ++) {
			
			// print op 1
			for (int a = 0; a < op1[0].length; a++) {
				// avoid overflow
				if (i < op1.length) output = output + "[" + op1[i][a] + "] ";
			}
			
			// print operator
			output += " ";
			if (i == oper_pos) output += operator;
			else output+= " ";
			output += " ";
			
			// print op2
			for (int b = 0; b < op1[0].length; b++) {
				// avoid overflow
				if (i < op2.length) output = output + "[" + op2[i][b] + "] ";
			}
			
			// print an equal symbol
			output += " ";
			if (i == oper_pos) output += "=";
			else output+= " ";
			output += " ";
			
			// print res
			for (int c = 0; c < op1[0].length; c++) {
				// avoid overflow
				if (i < res.length) output = output + "[" + res[i][c] + "]";
			}
			
			// print endline
			output += "\n";
			
		}
		
		return output;
	}

}
