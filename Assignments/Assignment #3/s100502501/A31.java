package a3.s100502501;
import java.util.Scanner;
public class A31 {
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);
		double[][] matrix1=new double[3][3];
		double[][] matrix2=new double[3][3];
		double[][] matrix3=new double[3][3];
		System.out.print("Please enter matrix 1 : ");
		for(int i=0;i<3;i++){ //loop to store element in array matrix1
			for(int j=0;j<3;j++){
				matrix1[i][j]=input.nextDouble();
			}
		}
		System.out.print("Please enter matrix 2 : ");
		for(int i=0;i<3;i++){ //loop to store element in array matrix2
			for(int j=0;j<3;j++){
				matrix2[i][j]=input.nextDouble();
			}
		}
		System.out.println();
		matrix3=multipleMatrix(matrix1,matrix2); //call method
		for(int m=0;m<3;m++){
			for(int n=0;n<3;n++){ //loop to show element in array matrix1
				System.out.print(matrix1[m][n]+"\t"); 
			}
			if(m==1)
				System.out.print("*");
			System.out.print("\t");
			for(int n=0;n<3;n++){ //loop to show element in array matrix2
				System.out.print(matrix2[m][n]+"\t"); 
			}
			if(m==1)
				System.out.print("=");
			System.out.print("\t");
			for(int n=0;n<3;n++){ //loop to show result
				System.out.printf("%.1f",matrix3[m][n]);
				System.out.print("\t");
			}
			System.out.println();
		}
	}
	
	public static double[][] multipleMatrix(double[][] a, double[][] b){ //method to multiply Matrix 1 by Matrix 2
		double[][] c=new double[3][3];
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				for(int k=0;k<3;k++){
					c[i][j]+=a[i][k]*b[k][j];
				}
			}
		}
		return c;
	}
}
