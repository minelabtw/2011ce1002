package a3.s100502501;
import java.util.Scanner;
public class A32 {
	public static void main(String[] args){
		Scanner user=new Scanner(System.in);
		System.out.println("ax + by = e\ncx + dy = f");
		System.out.print("Please enter a,b,c,d,e,f : ");
		double a=user.nextDouble(); //store numbers
		double b=user.nextDouble();
		double c=user.nextDouble();
		double d=user.nextDouble();
		double e=user.nextDouble();
		double f=user.nextDouble();
		LinearEquation linearequation=new LinearEquation(a,b,c,d,e,f); //new a LinearEquation object
		if(linearequation.isSolvable()){//use object to call method "isSolvable" and check whether equation has have solution
			System.out.print("x is "+linearequation.getX()+"\ny is "+linearequation.getY()); //show the result
		}
		else
			System.out.print("The equation has no solution.");
	}
}
