package a3.s100502501;
public class LinearEquation {
	private double a,b,c,d,e,f;
	public LinearEquation(double A,double B,double C,double D,double E,double F){ // constructor with initialization
		a=A;
		b=B;
		c=C;
		d=D;
		e=E;
		f=F;
	}			
	public boolean isSolvable(){ //method to judge whether x,y have solution
		if(a*d-b*c==0)
			return false;
		return true;
	}
	public double getX(){ //method to get x's solution
		return (e*d-b*f)/(a*d-b*c);
	}
	public double getY(){ //method to get y's solution
		return (a*f-c*e)/(a*d-b*c);
	}
}
