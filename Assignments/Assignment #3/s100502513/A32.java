package a3.s100502513;

import java.util.Scanner;

public class A32 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("input");
		double a = input.nextDouble(); // 宣告a~f讓使用者輸入
		double b = input.nextDouble();
		double c = input.nextDouble();
		double d = input.nextDouble();
		double e = input.nextDouble();
		double f = input.nextDouble();
		if (a == 0 && b == 0 && c == 0 && d == 0 && e == 0 && f == 0) {   //無限多解
			System.out.println("X and Y have limitless soulutions.");
		} else {
			LinearEquation linearequation = new LinearEquation(a, b, c, d, e, f); // 把參數帶入LinearEquation
			System.out.println("output");
			if (linearequation.isSolvable() == true) { // 如果方程式成立輸出X.Y
				System.out.print("x is " + linearequation.getX()
						+ " and  y is " + linearequation.getY());
			} else { // 不成立輸出錯誤訊息
				System.out.print("The equation has no solution.");
			}
		}
	}
}
