package a3.s100502513;

import java.util.Scanner;

public class A31 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double[][] a = new double[3][3]; // 兩個二維陣列3*3
		double[][] b = new double[3][3];

		System.out.print("input\n\nEnter maxtrix1: "); // 輸入陣列的值
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				a[i][j] = input.nextDouble();
			}
		}
		System.out.print("Enter maxtrix2: "); // 輸入陣列的值
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				b[i][j] = input.nextDouble();
			}
		}

		double[][] c = multipleMatrix(a, b); // 宣告C陣列接受RETURN的值
		System.out
				.println("\noutput\n\nThe matrices are subtracted as follows:");
		for (int x = 0; x < 3; x++) {
			for (int z = 0; z < 3; z++) {
				System.out.print(a[x][z] + "\t");
			}
			if (x != 1) { // 第一、三行不產生乘的符號
				System.out.print("\t");
			} else {
				System.out.print(" *\t");
			}
			for (int z = 0; z < 3; z++) {
				System.out.print(b[x][z] + "\t");
			}
			if (x != 1) {   // 第一、三行不產生等於的符號
				System.out.print("\t");
			} else {
				System.out.print(" =\t");
			}
			for (int z = 0; z < 3; z++) {
				System.out.printf("%.2f\t", c[x][z]);  
			}
			System.out.print("\n");
		}
	}

	public static double[][] multipleMatrix(double[][] a, double[][] b) {   //兩陣列(矩陣)相乘
		double[][] result = new double[3][3];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < 3; k++) {
					result[i][j] += a[i][k] * b[k][j];  
				}
			}
		}
		return result;
	}
}
