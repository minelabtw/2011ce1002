package a3.s100502513;

public class LinearEquation {
	private double a; // a~bclassㄏノ
	private double b;
	private double c;
	private double d;
	private double e;
	private double f;

	LinearEquation() { // ⊿块把计р计砞0
	}

	LinearEquation(double ina, double inb, double inc, double ind, double ine, double inf) { // 块把计
		a = ina;
		b = inb;
		c = inc;
		d = ind;
		e = ine;
		f = inf;
	}

	public boolean isSolvable() { // 耞よ祘Α琌Θミ
		if (a * d == b * c)
			return false;
		else
			return true;
	}

	public double getX() {
		return ((e * d - b * f) / (a * d - b * c)); // return x
	}

	public double getY() {
		return ((a * f - e * c) / (a * d - b * c)); // return y
	}
}
