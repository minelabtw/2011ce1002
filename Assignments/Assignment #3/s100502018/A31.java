package a3.s100502018;

import java.util.Scanner;
import java.lang.Math;

public class A31 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double[][] matrix1 = new double[3][3];
		double[][] matrix2 = new double[3][3];
		System.out.print("Enter matrix1: ");
		for (int i = 0; i < matrix1.length; i++) { // get the first matrix
													// inputed by user
			for (int j = 0; j < matrix1[i].length; j++) {
				matrix1[i][j] = input.nextDouble();
			}
		}
		System.out.print("Enter matrix2: ");
		for (int i = 0; i < matrix2.length; i++) { // get the second matrix
													// inputed by user
			for (int j = 0; j < matrix2[i].length; j++) {
				matrix2[i][j] = input.nextDouble();
			}
		}
		double[][] resultmatrix = multipleMatrix(matrix1, matrix2);
		for (int i = 0; i < resultmatrix.length; i++) { // show the result
														// ,compose a format by
														// \t
			for (int j = 0; j < resultmatrix[i].length; j++) {
				System.out.print(matrix1[i][j] + "\t");
			}
			if (i == 1) {
				System.out.print("*\t");
			} else {
				System.out.print("\t");
			}
			for (int k = 0; k < resultmatrix[i].length; k++) {
				System.out.print(matrix2[i][k] + "\t");
			}
			if (i == 1) {
				System.out.print("=\t");
			} else {
				System.out.print("\t");
			}
			for (int l = 0; l < resultmatrix[i].length; l++) {
				System.out.print(Math.round(resultmatrix[i][l] * 10) / 10.0
						+ "\t");
			}
			System.out.println("");
		}
	}

	public static double[][] multipleMatrix(double[][] a, double[][] b) {
		double[][] c = new double[3][3];
		for (int i = 0; i < 3; i++) { // use loop to calculate the matrix
			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < 3; k++) {
					c[i][j] += a[i][k] * b[k][j];
				}
			}
		}
		return c;
	}
}
