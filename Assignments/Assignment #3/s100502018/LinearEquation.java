package a3.s100502018;

public class LinearEquation {
	private static double X1 = 0, Y2 = 0, C1 = 0, X2 = 0, Y1 = 0, C2 = 0; // six
																			// data
																			// to
																			// save
																			// the
																			// coefficients

	LinearEquation(double a, double b, double c, double d, double e, double f) { // constructor
		X1 = a;
		Y1 = b;
		C1 = e;
		X2 = c;
		Y2 = d;
		C2 = f;
	}

	public static double getA1() { // show the A1
		return X1;
	}

	public static double getB1() { // show the B2
		return Y1;
	}

	public static double getC1() { // show the C2
		return C1;
	}

	public static double getA2() { // show the A2
		return X2;
	}

	public static double getB2() { // show the B2
		return Y2;
	}

	public static double getC2() { // show the C2
		return C2;
	}

	public static void isSolvable() { // solve the simultaneous equation
		System.out.println("x is " + getX() + " and y is " + getY());
	}

	private static double getX() { // a method to get x of the simultaneous
									// equation
		return (C1 * Y2 - Y1 * C2) / (X1 * Y2 - Y1 * X2);
	}

	private static double getY() { // a method to get y of the simultaneous
									// equation
		return (X1 * C2 - C1 * X2) / (X1 * Y2 - Y1 * X2);
	}
}
