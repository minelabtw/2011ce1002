package a3.s100502018;

import a3.s100502018.LinearEquation;
import java.util.Scanner;

public class A32 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("ax + by = e\ncx + dy = f\nEnter the a b c d e f: ");
		double[] factor = new double[6];
		for (int i = 0; i < factor.length; i++) { //get the coefficient inputed by user
			factor[i] = input.nextDouble();
		}
		LinearEquation linearequation = new LinearEquation(factor[0], //get the argument into the constructor
				factor[1], factor[2], factor[3], factor[4], factor[5]);
		if (factor[0] * factor[3] - factor[1] * factor[2] == 0) {
			System.out.print("The equation has no solution"); //show the no solution message
		} else {
			System.out.println("\n" + linearequation.getA1() + "x + " //show the format and answer
					+ linearequation.getB1() + "y = " + linearequation.getC1());
			System.out.println(linearequation.getA2() + "x + "
					+ linearequation.getB2() + "y = " + linearequation.getC2());
			linearequation.isSolvable();
		}
	}
}