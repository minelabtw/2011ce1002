package a3.s995002056;

public class Stock {

	private static double a ,b ,c ,d ,e ,f ;					//建立6個 data field
	
	Stock(double a , double b , double c , double d , double e , double f){
		this.a = a ;											//從主程式中輸入進六個數字
		this.b = b ;
		this.c = c ;
		this.d = d ;
		this.e = e ;
		this.f = f ;
	}
	
	public static boolean isSolvable(){							//判斷 ad - bc 是否為零
		if(a * d - b * c == 0)									//使用布林函數來回傳值
			return false;
			else
				return true;
	}
	
	public static double getX(){								//回傳 X
		return (e * d - b * f) / (a * d - b * c);
	}
	
	public static double getY(){								//回傳 Y
		return (a * f - e * c) / (a * d - b * c);
	}
}
