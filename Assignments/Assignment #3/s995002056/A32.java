package a3.s995002056;
import java.util.Scanner;

public class A32 {
	public static void main(String [] argc){
		
		Scanner scanner = new Scanner(System.in);

/*-----------------Input----------------------------------------------------------*/		
		
		System.out.println("Input");
		
		double a = scanner.nextDouble();			//user 輸入6個值
		double b = scanner.nextDouble();
		double c = scanner.nextDouble();
		double d = scanner.nextDouble();
		double e = scanner.nextDouble();
		double f = scanner.nextDouble();
		
		Stock stock = new Stock(a,b,c,d,e,f);		//將user輸入的6個值
													//輸入到class裡面
/*-----------------Output---------------------------------------------------------*/
		
		System.out.println("Output");
		
		if(stock.isSolvable() == false)				//傳出class裡判斷 ad-bc
			System.out.println("The quation has no solution.");
			else{
				System.out.print("X is " + stock.getX() + " and Y is " + stock.getY() );
			}	
	}
}
