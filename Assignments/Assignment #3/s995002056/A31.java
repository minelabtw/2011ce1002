package a3.s995002056;
import java.text.DecimalFormat;
import java.util.Scanner;

public class A31 {										//矩陣相乘
	
	public static void main(String [] argv){			//Start
		
		Scanner scanner = new Scanner(System.in);
		
		double [][] matrix1 = new double [3][3];
		double [][] matrix2 = new double [3][3];
		
//----------------input---------------------------------------------------------------------------------------------
		
		System.out.print("Input \n");
		System.out.print("Enter matrix1: ");
		
		for(int i = 0 ; i < 3 ; i++){					//user input matrix1
			for(int j = 0 ; j < 3 ; j++)
				matrix1[i][j] = scanner.nextDouble();
		}
		
		System.out.print("Enter matrix2: ");			//user input matrix2
		
		for(int i = 0 ; i < 3 ; i++){
			for(int j = 0 ; j < 3 ; j++)
				matrix2[i][j] = scanner.nextDouble();
		}
		
//-----------------output--------------------------------------------------------------------------------------------		
		
		System.out.println("\nOutput");
		System.out.println("the matrices are multiplied as follow");
		
		DecimalFormat df = new DecimalFormat("0.0");
		
		double [][] matrix3 = new double [3][3];
		
		matrix3 = multipleMatrix(matrix1 , matrix2);	//matrix3 = matrix1 * matrix2
		
		for(int i = 0 ; i < 3 ; i++){					//output matrix1 * matrix2 = matrix3
			for(int j = 0 ; j < 3 ; j++){				//output matrix1
				System.out.print(df.format(matrix1[i][j]) + "\t");
			}
			
			if(i == 1)									//在第二行新增 "+"
				System.out.print("*\t");
				else
					System.out.print("\t");				//非第二行則新增 \t
				
			for(int j = 0 ; j < 3 ; j++){				//output matrix2
				System.out.print(df.format(matrix2[i][j]) + "\t");
			}
			
			if(i == 1)									//在第二行新增 "="
				System.out.print("=\t");
				else
					System.out.print("\t");				//非第二行則新增 \t
			
			for(int j = 0 ; j < 3 ; j++){				//output matrix3
				System.out.print(df.format(matrix3[i][j]) + "\t");
			}
			
			System.out.print("\n");						//每一行最後換行
			
		}		
	}													//End

//-----------------method--------------------------------------------------------------------------------------------	
	
	public static double [][] multipleMatrix(double [][] a , double [][] b){
		
		double [][] matrix3 = new double [3][3];
		
		for(int j = 0 ; j < 3 ; j++){					//
			for(int i = 0 ; i < 3 ; i++){
				matrix3 [j][i]= a[j][0] * b[0][i] + a[j][1] * b[1][i] + a[j][2] * b[2][i];		
			}
		}
		return matrix3;
	}	
}
