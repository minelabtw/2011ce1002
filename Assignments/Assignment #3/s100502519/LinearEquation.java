package a3.s100502519;

public class LinearEquation {
	
	
	private double a,b,c,d,e,f;			//private data fields
	
	
	public LinearEquation( double newa , double newb , double newc , double newd , double newe , double newf ){			/*constructor 收6個陣列值*/
		
		a = newa;			//收到的值配到private data
		b = newb;
		c = newc;
		d = newd;
		e = newe;	
		f = newf;
		
	}
	
	
	boolean isSolvable(){			/*用來判斷有無解*/
		
		if( (a*d-b*c) != 0 ){
			
			return true;			//有解 傳true回去
		
		}
		else{
			
			return false;			//無解 傳false回去
		
		}
		
	}
	
	
	 double getX(){			/*計算x*/
		 
		 return (e*d-b*f)/(a*d-b*c);
	 
	 }
	 
	 
	 double getY(){			/*計算y*/
		 
		 return (a*f-e*c)/(a*d-b*c);
	 
	 }
	 
}
