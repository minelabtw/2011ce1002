package a3.s100502519;
import java.util.Scanner;

public class A31 {
	public static void main (String[]args){
		Scanner input = new Scanner (System.in);
		
		double [][] nums1 = new double [3][3];			//建兩個二微陣列
		double [][] nums2 = new double [3][3];
		
		System.out.print("input\n\n");
		System.out.print("Enter matrix1: ");
		
		for(int row=0;row<3;row++){
			for(int column=0;column<3;column++){
				
			nums1[row][column] = input.nextDouble();			//input 3*3 個數 到陣列1
			
			}
		}
		
		System.out.print("Enter matrix2: ");
		
		for(int row=0;row<3;row++){
			for(int column=0;column<3;column++){
				
			nums2[row][column] = input.nextDouble();			//input 3*3 個數到陣列2
			
			}
		}
		
		System.out.print("\noutput\n\n");
		System.out.println("the matrices are multiplied as follows:");
		
		double [][] nums4 = multipleMatrix ( nums1 , nums2 );			//建一個陣列收method傳回的值
		
		//排版 輸出結果
		System.out.println(nums1[0][0] + " " + nums1[0][1] + " " + nums1[0][2] + "\t" + " " + "\t" +
							nums2[0][0] + " " + nums2[0][1] + " " + nums2[0][2] + "\t" + " " + "\t" +
							nums4[0][0] + " " + nums4[0][1] + " " + nums4[0][2]);
		System.out.println(nums1[1][0] + " " + nums1[1][1] + " " + nums1[1][2] + "\t" + "*" + "\t" +
							nums2[1][0] + " " + nums2[1][1] + " " + nums2[1][2] + "\t" + "=" + "\t" +
							nums4[1][0] + " " + nums4[1][1] + " " + nums4[1][2]);
		System.out.println(nums1[2][0] + " " + nums1[2][1] + " " + nums1[2][2] + "\t" + " " + "\t" +
							nums2[2][0] + " " + nums2[2][1] + " " + nums2[2][2] + "\t" + " " + "\t" +
							nums4[2][0] + " " + nums4[2][1] + " " + nums4[2][2]);
	}
	
	
	public static double[][] multipleMatrix ( double[][] a , double[][] b ) {			/*收兩陣列*/
		
			double [][] nums3 = new double [3][3];			//結果陣列
			
			for (int row = 0; row < 3; row++) {
				for (int column = 0; column < 3 ; column++) {
					for (int n = 0; n < 3 ; n++) {
						
						nums3[row][column] = nums3[row][column] + a[row][n] * b[n][column];			//矩陣乘法規則
						/*
						 * a b c     j k l     s t u
						 * d e f  x  m n o  =  v w x 		← 規則 :  a*j + b*m + c*p = s
						 * g h i     p q r     y z z
						 * 
						 * */
					}
				}
			}
			
			return nums3;			//return 陣列3回去
	
	}
}
