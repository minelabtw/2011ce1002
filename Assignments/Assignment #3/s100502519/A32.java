package a3.s100502519;
import java.util.Scanner;

public class A32 {
	public static void main (String[]args){
		Scanner input = new Scanner (System.in);
		
		double [] nums = new double [6];			//建立一個陣列
		
		System.out.println("input");
		
		for(int term=0;term<6;term++){
			
			nums[term] = input.nextDouble();			//input 6個數到陣列
			
		}
		
		LinearEquation LE = new LinearEquation ( nums[0] , nums[1] , nums[2] , nums[3] , nums[4] , nums[5] );			//建個object 傳6個陣列值進去class constructor
		
		System.out.println("output");
		
		boolean havesolution = LE.isSolvable();			//用class 的 判斷用之function
		
		if(havesolution == true){			/*true表示有解*/
			
			System.out.println("x is " + LE.getX() + " and y is " + LE.getY());			//call x和y function 輸出答案
		
		}
		else{
			
			System.out.println("The equation has no solution");
		
		}
		
	}
}