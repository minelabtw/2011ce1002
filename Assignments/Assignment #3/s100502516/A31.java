package a3.s100502516;

import java.util.Scanner;

public class A31 {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		double[][] a = new double[3][3];//array1
		double[][] b = new double[3][3];//array2
		
		System.out.print("Enter matrix1: ");
		
		for(int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 3; j++)
				a[i][j] = input.nextDouble();
		}
		
		System.out.print("Enter matrix2: ");
		
		for(int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 3; j++)
				b[i][j] = input.nextDouble();
		}
		
		System.out.println("the matrices are multiplied as follows:");
		
		for(int h = 0; h < 3; h++)
		{
			for(int i = 0; i < 3; i++)
			{
				System.out.print(a[h][i] + " ");
				
			}
			
			if(h == 1)
				System.out.print(" *");
			
			System.out.print("\t");
			
			for(int i = 0; i < 3; i++)
			{
				System.out.print(b[h][i] + " ");
			}
			
			if(h == 1)
				System.out.print(" =");
			
			System.out.print("\t");
			
			for(int i = 0; i < 3; i++)
			{
				System.out.print(multipleMatrix(a, b)[h][i] + " ");
			}
			
			System.out.println();
		}
	}
	
	public static double[][] multipleMatrix( double[][] a, double[][] b)//multiple array
	{
		double[][] c = new double[3][3];
		
		for(int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 3; j++)
				c[i][j] = a[i][j] * b[i][j];
		}
		
		return c;
	}
}
