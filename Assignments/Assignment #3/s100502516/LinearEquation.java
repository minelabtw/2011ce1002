package a3.s100502516;

public class LinearEquation {
	private double a;
	private double b;
	private double c;
	private double d;
	private double e;
	private double f;	
	
	public LinearEquation(double n1, double n2, double n3, double n4, double n5, double n6)
	{
		a = n1;
		b = n2;
		c = n3;
		d = n4;
		e = n5;
		f = n6;
	}
	
	public boolean isSolvable()//determine solution whether true
	{
		if(a * d - b * c != 0)
			return true;
		else
			return false;
	}
	
	public double getX()
	{
		return (e * d - b * f) / (a * d - b * c);
	}
	
	public double getY()
	{
		return (a * f - e * c) / (a * d - b * c);
	}
}
