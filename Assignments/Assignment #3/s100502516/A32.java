package a3.s100502516;

import java.util.Scanner;

public class A32 {
	public static void main(String[] args)
	{
		boolean stop = false;//dtermine to exit fram while 
		int select;
		
		while(!stop)
		{
			Scanner input = new Scanner(System.in);
			
			System.out.println("input");
			
			LinearEquation test = new LinearEquation(input.nextDouble(), input.nextDouble(),input.nextDouble(), 
					input.nextDouble(), input.nextDouble(), input.nextDouble());
			
			System.out.println("output");
			
			if(test.isSolvable())//if answer is valid, output the answer
				System.out.println("x is " + test.getX() + "and y is " + test.getY());
			else
				System.out.println("The equation has no solution");
			
			System.out.print("\nSelection: 1.continuous 2.exit : ");
			select = input.nextInt();
			
			while(select <= 0 || select >=3)
			{
				System.out.print("Invalid enter, please try again: ");
				select = input.nextInt();
			}
			
			if(select == 1)
				System.out.println("\n");
			else
			{
				stop = true;
				System.out.println("Bye bye");
			}
		}
	}
}
