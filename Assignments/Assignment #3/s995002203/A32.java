package a3.s995002203;
import java.util.Scanner;

public class A32 {
	public static void main(String[] args) {
		double []num=new double[6];
		boolean flag=true;
		Scanner scanner = new Scanner(System.in);
		while(flag)
		{
			System.out.println("input");
			for(int i=0 ; i<6 ; i++){//user inputs six numbers
				num[i]=scanner.nextDouble();
			}
			LinearEquation l1=new LinearEquation(num[0],num[1],num[2],num[3],num[4],num[5]);//declare a LinearEquation object
			System.out.println("output");
			
			if(l1.isSolvable())//call member function "isSolvable()" to test the fraction is solvable or not
				System.out.print("x is "+l1.getX()+" and y is "+l1.getY()+"\n");//solvable
			else
				System.out.println("The equation has no solution");//unsolvable
			
			System.out.print("exit?(y/n): ");//exit?
			if(scanner.next().charAt(0) =='y')
				System.exit(0);
		}
	}
}
