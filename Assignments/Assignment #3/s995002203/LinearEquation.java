package a3.s995002203;

public class LinearEquation {
	LinearEquation(double in1,double in2,double in3,double in4,double in5,double in6){//constructor
		a=in1;
		b=in2;
		c=in3;
		d=in4;
		e=in5;
		f=in6;
	}
	
	public double getA(){return a;}//accessor function
	public double getB(){return b;}//accessor function
	public double getC(){return c;}//accessor function
	public double getD(){return d;}//accessor function
	public double getE(){return e;}//accessor function
	public double getF(){return f;}//accessor function
	
	public boolean isSolvable(){
		if(a*d==b*c)//means a*b-b*c=0 ,so the fraction is  Unsolvable
			return false;
		return true;
	}
	
	public double getX(){
		return (e*d-b*f)/(a*d-b*c);
	}
	public double getY(){
		return (a*f-e*c)/(a*d-b*c);
	}
	
	private double a, b, c, d, e, f;
}
