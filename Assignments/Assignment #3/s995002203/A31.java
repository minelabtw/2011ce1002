package a3.s995002203;
import java.util.Scanner;

public class A31 {
    public static void main(String[] args) {
    	double[][] matrixA=new double[3][3];//two dimension array
    	double[][] matrixB=new double[3][3];
    	double[][] matrixC=new double[3][3];

        Scanner scanner = new Scanner(System.in);
        System.out.println("input\n");
        System.out.println("Enter matrix1: ");
        
        for(int i=0;i<3;i++)//user input matrixA
        	for(int j=0;j<3;j++)
        		matrixA[i][j]=scanner.nextDouble();

        System.out.println("Enter matrix2: ");

        for(int i=0;i<3;i++)//user input matrixB
        	for(int j=0;j<3;j++)
        		matrixB[i][j]=scanner.nextDouble();

        System.out.println("output");//output result
    	matrixC=multipleMatrix(matrixA, matrixB);//call func.
    	for(int i=0;i<3;i++){//output every layer
        	for(int j=0;j<3;j++)
        		System.out.printf("%4.1f ",matrixA[i][j]);
    		
    		if(i==1)
    			System.out.print("*");
    		else
    			System.out.print(" ");

    		for(int k=0;k<3;k++)
        		System.out.printf("%4.1f ",matrixB[i][k]);
    		
    		if(i==1)
    			System.out.print("=");
    		else
    			System.out.print(" ");

    		for(int m=0;m<3;m++)
        		System.out.printf("%4.1f ",matrixC[i][m]);

    		System.out.println();
        }
    	
    }
    public static double[][] multipleMatrix(double[][]a, double[][]b)//MatrixA times MatrixB 
    {
    	double [][]c=new double[3][3];
        for(int i=0;i<3;i++)
        	for(int j=0;j<3;j++)
        		for(int k=0;k<3;k++)
        		c[i][j]+=a[i][k]*b[k][j];

        return c;
    }
}
