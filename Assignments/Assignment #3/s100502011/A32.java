package a3.s100502011;
import java.util.Scanner;

public class A32 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		System.out.print("Enter the first linear equation's(ax+by+c)'s a,b,c :");
		double a=input.nextDouble(); //input
		double b=input.nextDouble();//input
		double c=input.nextDouble();//input
		System.out.print("Enter the second linear equation's(dx+ey+f)'s d,e,f :");
		double d=input.nextDouble();//input
		double e=input.nextDouble();//input
		double f=input.nextDouble();//input
		double X=0; // answer
		double Y=0; // answer
		LinearEquation linearEquation=new LinearEquation(a,b,c,d,e,f);//return variable
		
		X=linearEquation.getX(); // the result of x
		Y=linearEquation.getY(); // the result of y
		linearEquation.isSolvable(); // judge the result exist or not
		
	}
}
