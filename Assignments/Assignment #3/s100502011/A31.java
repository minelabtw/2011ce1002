package a3.s100502011;
import java.util.Scanner;
import java.math.*;

public class A31 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		double[][] mat1 =new double[3][3]; //the first matrix
		double[][] mat2 =new double[3][3]; // the second matrix
		double[][] result =new double[3][3]; // the result matrix
		
		System.out.print("Enter matrix1: ");
		for(int a=0;a<3;a++){ //input to store in matrix1
			for(int b=0;b<3;b++){
				mat1[a][b]=input.nextDouble();
			}
		}
		System.out.print("Enter matrix2: ");
		for(int c=0;c<3;c++){ //input to store in matrix2
			for(int d=0;d<3;d++){
				mat2[c][d]=input.nextDouble();
			}
		}
		result =multipleMatrix(mat1,mat2); //use multiple method
		mat1=round(mat1); // 計算精準值
		mat2=round(mat2); // 計算精準值
		result=round(result); // 計算精準值
		System.out.println("the matrices are multiplied as follows: ");
		for(int e=0;e<3;e++){   // 排版
			for(int f=0;f<3;f++){ //matrix1
				System.out.print(mat1[e][f]);
				if(f!=2){
					System.out.print("\t");
				}
			}
			if(e==1)
				System.out.print("  *  ");
			else
				System.out.print("\t");
			for(int g=0;g<3;g++){ //matrix 2
				System.out.print(mat2[e][g]);
				if(g!=2){
					System.out.print("\t");
				}
			}
			if(e==1)
				System.out.print("  =  ");
			else
				System.out.print("\t");
			for(int h=0;h<3;h++){ //result
				System.out.print(result[e][h]);
				if(h!=2){
					System.out.print("\t");
				}
			}
			System.out.println();
		}
		
		
	}
	
	public static double[][] multipleMatrix( double[][] a, double[][] b){ //矩陣相乘
		double[][] ans = new double[3][3];
		for(int c=0;c<3;c++){ //multiple
			for(int d=0;d<3;d++){
				ans[c][d]=a[c][0]*b[0][d]+a[c][1]*b[1][d]+a[c][2]*b[2][d];
			}
				
		}
		return ans;
	}
	public static double[][]round(double[][]c){ //計算精準值
		double[][] trans = new double[3][3];
		for(int a=0;a<3;a++){
			for(int b=0;b<3;b++){
				trans[a][b]=Math.round(c[a][b]*10.0)/10.0;
			}
		}
		return trans;
	}
}
