package a3.s100502011;

public class LinearEquation {
	private static double a; //variable for equation
	private static double b; //variable for equation
	private static double c; //variable for equation
	private static double d; //variable for equation
	private static double e; //variable for equation
	private static double f; //variable for equation
	LinearEquation(double g,double h,double i,double j,double k,double l){ //constructer
		a=g;
		b=h;
		c=i;
		d=j;
		e=k;
		f=l;
	}
	public static void isSolvable(){ // dujge it can solve or not
		if((a*d-b*c)!=0)
			System.out.print("x is "+getX()+" and y is "+getY());
		else
			System.out.print("The equation has no solution");
	}
	public static double getX(){ // get answer of x
		return (e*d-b*f)/(a*d-b*c);
	}
	public static double getY(){  // get answer of y
		return (a*f-e*c)/(a*d-b*c);
	}
}
