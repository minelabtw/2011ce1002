package a3.s100502512;

import java.util.Scanner;

public class A31 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double[][] a = new double[3][3];
		double[][] b = new double[3][3];
		double[][] c = new double[3][3];
		System.out.println("Enter matrix1:");// 輸入第一個陣列
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				a[i][j] = input.nextDouble();
			}
		}
		System.out.println("Enter matrix2:");// 輸入第二個陣列
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				b[i][j] = input.nextDouble();
			}
		}
		System.out.println("the matrices are multiplied as follows:");
		c = multipleMatrix(a, b);// 用陣列c表示multipleMatrix所執行結果

		for (int i = 0; i < 3; i++) { // 排列陣列
			System.out.print(a[0][i] + "\t");
		}
		System.out.print("\t");
		for (int i = 0; i < 3; i++) {
			System.out.print(b[0][i] + "\t");
		}
		System.out.print("\t");
		for (int i = 0; i < 3; i++) {
			System.out.printf("%.2f\t", c[0][i]);// 顯示至小數點第二位
		}
		System.out.print("\n");
		for (int i = 0; i < 3; i++) {
			System.out.print(a[1][i] + "\t");
		}
		System.out.print(" * " + "\t");
		for (int i = 0; i < 3; i++) {
			System.out.print(b[1][i] + "\t");
		}
		System.out.print(" = " + "\t");
		for (int i = 0; i < 3; i++) {
			System.out.printf("%.2f\t", c[1][i]);
		}
		System.out.print("\n");
		for (int i = 0; i < 3; i++) {
			System.out.print(a[2][i] + "\t");
		}
		System.out.print("\t");
		for (int i = 0; i < 3; i++) {
			System.out.print(b[2][i] + "\t");
		}
		System.out.print("\t");
		for (int i = 0; i < 3; i++) {
			System.out.printf("%.2f\t", c[2][i]);
		}
		System.out.print("\n");
	}

	public static double[][] multipleMatrix(double[][] a, double[][] b) {// 陣列相乘計算
		double[][] result = new double[3][3];
		result[0][0] = a[0][0] * b[0][0] + a[0][1] * b[1][0] + a[0][2]
				* b[2][0];
		result[0][1] = a[0][0] * b[0][1] + a[0][1] * b[1][1] + a[0][2]
				* b[2][1];
		result[0][2] = a[0][0] * b[0][2] + a[0][1] * b[1][2] + a[0][2]
				* b[2][2];
		result[1][0] = a[1][0] * b[0][0] + a[1][1] * b[1][0] + a[1][2]
				* b[2][0];
		result[1][1] = a[1][0] * b[0][1] + a[1][1] * b[1][1] + a[1][2]
				* b[2][1];
		result[1][2] = a[1][0] * b[0][2] + a[1][1] * b[1][2] + a[1][2]
				* b[2][2];
		result[2][0] = a[2][0] * b[0][0] + a[2][1] * b[1][0] + a[2][2]
				* b[2][0];
		result[2][1] = a[2][0] * b[0][1] + a[2][1] * b[1][1] + a[2][2]
				* b[2][1];
		result[2][2] = a[2][0] * b[0][2] + a[2][1] * b[1][2] + a[2][2]
				* b[2][2];
		return result;

	}
}
