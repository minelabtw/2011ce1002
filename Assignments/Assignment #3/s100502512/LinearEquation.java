package a3.s100502512;

public class LinearEquation {
	private double a, b, c, d, e, f;

	LinearEquation(double newA, double newB, double newC, double newD,
			double newE, double newF) {// 題目所要的constructor
		a = newA;
		b = newB;
		c = newC;
		d = newD;
		e = newE;
		f = newF;
	}

	public boolean isSolvable() { // 判斷a*d-b*c是否為0
		if ((a * d - b * c) == 0) {
			return false;
		} else {
			return true;
		}
	}

	public double getX() { // x的計算公式
		double x = (e * d - b * f) / (a * d - b * c);
		return x;
	}

	public double getY() { // y的計算公式
		double y = (a * f - e * c) / (a * d - b * c);
		return y;
	}
}
