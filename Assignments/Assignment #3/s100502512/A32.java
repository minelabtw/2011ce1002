package a3.s100502512;

import java.util.Scanner;

public class A32 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Input:");
		double a, b, c, d, e, f;// 輸入六個數字
		a = input.nextDouble();
		b = input.nextDouble();
		c = input.nextDouble();
		d = input.nextDouble();
		e = input.nextDouble();
		f = input.nextDouble();
		System.out.println("Output:");
		LinearEquation Test = new LinearEquation(a, b, c, d, e, f);// 使用另一個class
		if (Test.isSolvable()) { // 如果isSolvabe()有執行則可顯示x,y結果
			System.out.println("The x is:" + Test.getX() + "and the y is:"
					+ Test.getY());
		} else { // 如果不執行則顯示訊息
			System.out.println("The equation has no solution");
		}
	}

}
