package a3.s100502028;

public class LinearEquation {
	private double a, b, c, d, e, f; // Declare private data field

	// Constructor to receive variable and set it to private data field
	public LinearEquation(double aInLinearEquation, double bInLinearEquation,
			double cInLinearEquation, double dInLinearEquation,
			double eInLinearEquation, double fInLinearEquation) {
		a = aInLinearEquation;
		b = bInLinearEquation;
		c = cInLinearEquation;
		d = dInLinearEquation;
		e = eInLinearEquation;
		f = fInLinearEquation;
	}
	
	// Public method to return private data
	public double geta(){
		return a;
	}
	public double getb(){
		return b;
	}
	public double getc(){
		return c;
	}
	public double getd(){
		return d;
	}
	public double gete(){
		return e;
	}
	public double getf(){
		return f;
	}
	
	// Public method to decide whether a*d-b*c=0 or not
	public boolean isSolvable() {
		if (a * d - b * c != 0)
			return true;
		else
			return false;
	}
	
	// Public method to get X value
	public double getX() {
		return (e * d - b * f) / (a * d - b * c);
	}
	
	// Public method to get Y value
	public double getY() {
		return (a * f - e * c) / (a * d - b * c);
	}
} // End class LinearEquation
