package a3.s100502028;
import java.util.Scanner; // Using Scanner in the java.util package
public class A31 {
	// Method to multiple two matrices
	public static double[][] multipleMatrices(double[][] a, double[][] b) {
		double[][] matrixResult = new double[3][3]; // Declare an array to store the result after calculation
		
		// For loops to calculate every element in two dimension array
		for (int row = 0; row < matrixResult.length; row++) {
			for (int column = 0; column < matrixResult[row].length; column++) {
				for (int index = 0; index < matrixResult[column].length; index++)
				matrixResult[row][column] += a[row][index] * b[index][column]; // Calculate and store result to the array
			}
		}
		return matrixResult;
	}
	
	// Main method 
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // Create an object of the Scanner type

		double[][] matrixA = new double[3][3];
		double[][] matrixB = new double[3][3];
		double[][] matrixC = new double[3][3];
		
		System.out.print("Enter matrix1: "); // Prompt the user for the first matrix
		for (int row = 0; row < matrixA.length; row++) { // For loop to store number in the two dimension array
			for (int column = 0; column < matrixA[row].length; column++) {
				matrixA[row][column] = input.nextDouble();
			}
		}
		
		System.out.print("Enter matrix2: "); // Prompt the user for the second matrix
		for (int row = 0; row < matrixB.length; row++) { 
			for (int column = 0; column < matrixB[row].length; column++) {
				matrixB[row][column] = input.nextDouble();
			}
		}
		
		// For loop to call the method multipleMatrices
		for (int row = 0; row < matrixC.length; row++) {
			for (int column = 0; column < matrixC[row].length; column++) {
				matrixC = multipleMatrices(matrixA, matrixB);
			}
		}
		
		System.out.print("The matrices are multipled as follows:\n");
		
		// For loop to show the result
		for (int row = 0; row < matrixA.length; row++) {
			for (int column = 0; column < matrixA[row].length; column++) {
				System.out.print(matrixA[row][column] + " ");
			}
			if (row == 1)
				System.out.print(" *\t");
			else
				System.out.print("\t");
			for (int column = 0; column < matrixB[row].length; column++) {
				System.out.print(matrixB[row][column] + " ");
			}
			if (row == 1)
				System.out.print(" =\t");
			else
				System.out.print("\t");
			for (int column = 0; column < matrixC[row].length; column++) {
				System.out.printf("%5.1f",matrixC[row][column]);
			}
			System.out.print("\n");
		}
	} // End main method
} // End class A31

