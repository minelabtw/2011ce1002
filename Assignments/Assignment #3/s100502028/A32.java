package a3.s100502028;
import java.util.Scanner; // Using Scanner in the java.util package
public class A32 {
	// Main method
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // Create an object of the Scanner type
		
		// Prompt the user for six variables
		System.out.print("Input a b c d e f: ");
		double aOfInput = input.nextDouble();
		double bOfInput = input.nextDouble();
		double cOfInput = input.nextDouble();
		double dOfInput = input.nextDouble();
		double eOfInput = input.nextDouble();
		double fOfInput = input.nextDouble();
		
		// Create an object of class LinearEquation with the six variables
		LinearEquation test = new LinearEquation(aOfInput,bOfInput,cOfInput,dOfInput,eOfInput,fOfInput);
		boolean bool = test.isSolvable(); // Call method isSolvable() in class LinearEquation
		
		/** If statement call method getX() and getY() in class LinearEquation if the bool is true
		 * or show a message if the bool is false */
		if (bool == true){
			System.out.println("\nx is " + test.getX() + " and y is " + test.getY());
		}
		else{
			System.out.println("\nThe equation has no solution!!!");
		}
	} // End main
} // End class A32
