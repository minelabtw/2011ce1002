package a3.s100502008;

public class LinearEquation {
	private double a,b,c,d,e,f;
	LinearEquation()//constructor
	{
		a=1;
		b=1;
		c=2;
		e=1;
		d=-1;
		f=0;
	}
	public LinearEquation(double num1,double num2,double num3,double num4,double num5,double num6)//set variables
	{
		a=num1;
		b=num2;
		c=num3;
		d=num4;
		e=num5;
		f=num6;
	}
	public boolean isSolvable()//check is solvable or not
	{
		if(a*d-b*c==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	public double getX()//return x
	{
		return ((e*d - b*f)/(a*d - b*c));
	}
	public double getY()//return y
	{
		return ((a*f - e*c)/(a*d - b*c));
	}
}
