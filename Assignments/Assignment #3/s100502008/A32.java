package a3.s100502008;
import java.util.Scanner;
import a3.s100502008.LinearEquation;

public class A32 {
	public static void main(String[] args)
	{
		Scanner input=new Scanner(System.in);
		double num1,num2,num3,num4,num5,num6;
		System.out.printf("input: ");
		num1=input.nextDouble();//get variables
		num2=input.nextDouble();
		num3=input.nextDouble();
		num4=input.nextDouble();
		num5=input.nextDouble();
		num6=input.nextDouble();
		LinearEquation myLinearEquation=new LinearEquation(num1,num2,num3,num4,num5,num6);//declare class and initialize data
		if(myLinearEquation.isSolvable())//solvable
		{
			System.out.println("The solution of the equation is X = "+myLinearEquation.getX()+" Y = "+myLinearEquation.getY()+".");
		}
		else//unsolvable
		{
			System.out.println("The equation has no solution.");
		}
	}
}
