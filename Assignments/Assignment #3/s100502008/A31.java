package a3.s100502008;
import java.util.Scanner;
import java.lang.Math;

public class A31 {
	public static void main(String[] args)
	{
		Scanner input=new Scanner(System.in);
		double[][]a=new double[3][3];//array1
		double[][]b=new double[3][3];//array2
		double[][]ans=new double[3][3];//array3
		System.out.printf("Enter matrix1: ");
		for(int i=0;i<3;i++)//input array1
		{
			for(int j=0;j<3;j++)
			{
				a[i][j]=input.nextDouble();
			}
		}
		System.out.printf("Enter matrix2: ");
		for(int i=0;i<3;i++)//input array2
		{
			for(int j=0;j<3;j++)
			{
				b[i][j]=input.nextDouble();
			}
		}
		ans=multipleMatrix(a,b);//compute
		for(int i=0;i<3;i++)//show result
		{
			for(int j=0;j<3;j++)
			{
				System.out.printf(a[i][j]+" ");
			}	
			if(i==1)
			{
				System.out.printf("x\t");
			}
			else
			{
				System.out.printf("\t");
			}
			for(int j=0;j<3;j++)
			{
				System.out.printf(b[i][j]+" ");
			}
			if(i==1)
			{
				System.out.printf("=\t");
			}
			else
			{
				System.out.printf("\t");
			}
			for(int j=0;j<3;j++)
			{
				System.out.printf(Math.round(ans[i][j]*10)/10.0+" ");
			}
			System.out.printf("\n");
		}
	}

	public static double[][] multipleMatrix(double[][]a, double[][]b)//multiple
	{
		double[][] ans=new double[3][3];
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				for(int k=0;k<3;k++)//add 3 times
				{
					ans[i][j]+=a[i][k]*b[k][j];//compute
				}
			}
		}
		return ans;
	}
}