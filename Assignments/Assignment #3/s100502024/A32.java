package a3.s100502024;
import java.util.Scanner;
public class A32 
{
	public static void main(String[] args)
	{
		double A,B,C,D,E,F;
		Scanner input = new Scanner(System.in);
		System.out.print("Please enter a ~ f:"); // 提示輸入係數
		A = input.nextDouble();
		B = input.nextDouble();
		C = input.nextDouble();
		D = input.nextDouble();
		E = input.nextDouble();
		F = input.nextDouble();
		LinearEquation linearequation = new LinearEquation(A,B,C,D,E,F); // 建立一個新的object並傳數值進去
		if (linearequation.isSolvable() == 0) // 若isSolvable函式回傳0則代表無解
		{
			System.out.println("The equation has no solution!"); // 顯示無解
		}
		else 
		{
			System.out.println("X is : "+linearequation.getX()); // 顯示X值
			System.out.println("Y is : "+linearequation.getY()); // 顯示Y值
		}
	}
}
