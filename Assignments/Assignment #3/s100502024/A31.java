package a3.s100502024;
import java.util.Scanner;
public class A31 
{
	public static double[][] multipleMatrix(double a[][],double b[][]) // 建立函式
	{
		double result[][] = new double[3][3];
		for (int i=0;i<3;i++)
		{
			for (int j=0;j<3;j++)
			{
				result[i][j] = a[i][0]*b[0][j]+a[i][1]*b[1][j]+a[i][2]*b[2][j]; // 陣列相乘
			}
		}
		return result; // 回傳陣列
	}
	public static void main (String[] args)
	{
		Scanner input = new Scanner(System.in);
		double matrix1[][] = new double[3][3];
		double matrix2[][] = new double[3][3];
		double matrix3[][] = new double[3][3];
		System.out.println("Please enter matrix1 :");
		int i,j;
		for (i=0;i<3;i++)
		{
			for (j=0;j<3;j++)
			{
				matrix1[i][j] = input.nextDouble(); // 將輸入的數值存入陣列1
			}
		}
		System.out.println("Please enter matrix2 :");
		for (i=0;i<3;i++)
		{
			for (j=0;j<3;j++)
			{
				matrix2[i][j] = input.nextDouble(); // 將輸入的數值存入陣列2
			}
		}
		matrix3 = multipleMatrix(matrix1,matrix2); // 傳矩陣1、2到function並把結果存入陣列3
		System.out.println("The matrices are multiplied as follows :");
		System.out.print(matrix1[0][0]+"\t"+matrix1[0][1]+"\t"+matrix1[0][2]+"\t"+matrix2[0][0]+"\t"+matrix2[0][1]+"\t"+matrix2[0][2]+"\t"); // 顯示結果
		System.out.printf("%.1f",matrix3[0][0]);
		System.out.print("\t"+matrix3[0][1]+"\t"+matrix3[0][2]+"\n");
		System.out.print(matrix1[1][0]+"\t"+matrix1[1][1]+"\t"+matrix1[1][2]+"  *  ");
		System.out.print(matrix2[1][0]+"\t"+matrix2[1][1]+"\t"+matrix2[1][2]+"  =  ");
		System.out.printf("%.1f",matrix3[1][0]);
		System.out.print("\t"+matrix3[1][1]+"\t"+matrix3[1][2]+"\n");
		System.out.print(matrix1[2][0]+"\t"+matrix1[2][1]+"\t"+matrix1[2][2]+"\t"+matrix2[2][0]+"\t"+matrix2[2][1]+"\t"+matrix2[2][2]+"\t"+matrix3[2][0]+"\t");
		System.out.printf("%.1f",matrix3[2][1]);
		System.out.print("\t"+matrix3[2][2]+"\n");
	}
}
