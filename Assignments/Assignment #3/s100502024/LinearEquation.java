package a3.s100502024;
public class LinearEquation 
{
	private static double a,b,c,d,e,f; // 宣告 private data
	LinearEquation(double A,double B,double C,double D,double E,double F) // constructor
	{
		a = A;
		b = B;
		c = C;
		d = D;
		e = E;
		f = F;
	}
	int isSolvable() // 判斷方程式是否有解
	{
		if (a*d-b*c!=0) 
		{
			return 1; // 有解回傳1
		}
		else
		{
			return 0; // 無解回傳0
		}	
	}
	double getX()
	{
		double x = (e*d-b*f)/(a*d-b*c); // 計算X
		return x; 
	}
	double getY()
	{
		double y = (a*f-e*c)/(a*d-b*c); // 計算Y
		return y;
	}
}
