package a3.s100502025;

import java.util.Scanner;

public class A32 {
	public static void main(String[] args){
		
		Scanner input = new Scanner(System.in);
		
		double[] matrice = new double[6];  //宣告新的陣列matrice，可存入6個數字
		
		System.out.print("Input:");
		
		for(int i = 0 ; i < 6 ; i++){
			matrice[i] = input.nextDouble();
		}
		
		LinearEquation linear = new LinearEquation(matrice[0] , matrice[1] , matrice[2] , matrice[3] , matrice[4] , matrice[5]);  //僵直傳入constructor
		
		int issolve = linear.isSolvable( matrice[0] , matrice[1] , matrice[2] , matrice[3] );  //判斷是否有解
		if(issolve == 1){
			System.out.print("The equation has no solution");  //無解
		}
		else{  //有解，繼續下面的函式
			double x = linear.getX(matrice[0] , matrice[1] , matrice[2] , matrice[3] , matrice[4] , matrice[5]);  //求x
			double y = linear.getY(matrice[0] , matrice[1] , matrice[2] , matrice[3] , matrice[4] , matrice[5]);  //求y
			System.out.print("x is " + x + " and y is " + y);
		}		
	}
}
