package a3.s100502025;

import java.util.Scanner;

public class A31 {
	public static void main(String[] args){
		
		Scanner input = new Scanner(System.in);
		
		double[][] matrice1 = new double[3][3];  //宣告新的陣列matrice1
		double[][] matrice2 = new double[3][3];  //宣告新的陣列matrice2
		double[][] matrice3 = new double[3][3];  //宣告新的陣列matrice3
		System.out.print("Enter matrix1:");
		for(int i = 0 ; i < 3 ; i++ ){
			for(int j = 0 ; j < 3 ; j++){
				matrice1[i][j] = input.nextDouble();  //輸入陣列matrice1的值
			}
		}
		System.out.print("Enter matrix2:");
		for(int i = 0 ; i < 3 ; i++ ){
			for(int j = 0 ; j < 3 ; j++){
				matrice2[i][j] = input.nextDouble();  //輸入陣列matrice2的值
			}
		}
		matrice3 = multipleMatrix(matrice1, matrice2);  //將multipleMatrix函式的值傳入陣列matrice3中
		
		
		for(int i = 0 ; i < 3 ; i++ ){
			System.out.print(matrice1[0][i] + " ");  //以下都是輸出&排版
		}
		System.out.print("\t");
		for(int i = 0 ; i < 3 ; i++ ){
			System.out.print(matrice2[0][i] + " ");
		}
		System.out.print("\t");
		for(int i = 0 ; i < 3 ; i++ ){
			System.out.print(matrice3[0][i] + " ");
		}
		System.out.print("\n");
		
		
		for(int i = 0 ; i < 3 ; i++ ){
			System.out.print(matrice1[1][i] + " ");
		}
		System.out.print(" *\t");
		for(int i = 0 ; i < 3 ; i++ ){
			System.out.print(matrice2[1][i] + " ");
		}
		System.out.print(" =\t");
		for(int i = 0 ; i < 3 ; i++ ){
			System.out.print(matrice3[1][i] + " ");
		}
		System.out.print("\n");
		
		
		for(int i = 0 ; i < 3 ; i++ ){
			System.out.print(matrice1[2][i] + " ");
		}
		System.out.print("\t");
		for(int i = 0 ; i < 3 ; i++ ){
			System.out.print(matrice2[2][i] + " ");
		}
		System.out.print("\t");
		for(int i = 0 ; i < 3 ; i++ ){
			System.out.print(matrice3[2][i] + " ");
		}
	}
	
	public static double[][] multipleMatrix( double[][] a, double[][] b){  //矩陣相乘
		double[][] c = new double[3][3]; 
		for(int i = 0 ; i < 3 ; i++ ){
			for(int j = 0 ; j < 3 ; j++ ){
				c[i][j] = 0;
				for(int k = 0 ; k < 3 ; k++ ){
					c[i][j] += a[i][k] * b[k][j];
				}
			}
			
		}
		return c;
	}


}
