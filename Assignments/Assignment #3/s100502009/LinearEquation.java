package a3.s100502009;

public class LinearEquation {
	private double a,b,c,d,e,f;
	LinearEquation(double m1,double m2,double m3,double m4,double m5,double m6)//initialize the private data
	{
		a=m1;
		b=m2;
		c=m3;
		d=m4;
		e=m5;
		f=m6;
	}
	public double getA()//method to display the value of a
	{
		return a;
	}
	public double getB()//method to display the value of b
	{
		return b;
	}
	public double getC()//method to display the value of c
	{
		return c;
	}
	public double getD()//method to display the value of d
	{
		return d;
	}
	public double getE()//method to display the value of e
	{
		return e;
	}
	public double getF()//method to display the value of f
	{
		return f;
	}
	public boolean isSolvable()//method to tell if the equation can be solved 
	{
		if((a*d-b*c) !=0)
			return true;
		else
			return false;
	}
	public double getX()//to display the value of x
	{
		return (e*d-b*f)/(a*d-b*c);
	}
	public double getY()//to display the value of y
	{
		return (a*f-e*c)/(a*d-b*c);
	}
}
