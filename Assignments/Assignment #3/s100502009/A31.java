package a3.s100502009;

import java.util.Scanner;

public class A31 {
	public static void main(String[] args)
	{
		double[][] matrix1=new double[3][3];
		double[][] matrix2=new double[3][3];
		Scanner input= new Scanner(System.in);
		
		System.out.println("Enter matrix1:");//store the numbers into the array
		for(int i=0;i<=2;i++)
		{
			for(int j=0;j<=2;j++)
			{
				matrix1[i][j]=input.nextDouble();
			}
		}
		
		System.out.println("Enter matrix2:");//store the numbers into the array
		for(int i=0;i<=2;i++)
		{
			for(int j=0;j<=2;j++)
			{
				matrix2[i][j]=input.nextDouble();
			}
		}
		System.out.println("The matrices are subtracted as follows:");
		double[][] total=multipleMatrices(matrix1,matrix2);//call method to multiple and store it into the array
		for(int k=0;k<=2;k++)
		{
			for(int m=0;m<=2;m++)
			{
				System.out.print(matrix1[k][m]+" ");
				if(m==2&&k==1)
					System.out.print("  * ");
				else if(m==2)
					System.out.print("\t");
			}
			for(int m=0;m<=2;m++)
			{
				System.out.print(matrix2[k][m]+" ");
				if(m==2&&k==1)
					System.out.print("  = ");
				else if(m==2)
					System.out.print("\t");
			}
			for(int m=0;m<=2;m++)
			{
				System.out.print(total[k][m]+" ");
				if(m==2)
					System.out.print("\n");
			}
		}
	}	
	public static double[][] multipleMatrices(double[][] a,double[][] b)//method to multiple
	{
		double[][] result=new double[3][3];
		for(int k=0;k<=2;k++)
		{
			int count=0;
			for(int i =0;i<=2;i++)
			{
				double product=0;
				for(int j=0;j<=2;j++)
				{
					product=product+(a[k][j]*b[j][i]);
				}
				result[k][count]=product;
				count++;				
			}
		}
		return result;
	}
}