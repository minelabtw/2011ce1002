package a3.s100502009;

import java.util.Scanner;

public class A32 {
	public static void main(String[] args)
	{
		Scanner input=new Scanner(System.in);
		System.out.println("Please enter 6 numbers: ");
		double a=input.nextDouble();
		double b=input.nextDouble();
		double c=input.nextDouble();
		double d=input.nextDouble();
		double e=input.nextDouble();
		double f=input.nextDouble();
		LinearEquation le=new LinearEquation(a,b,c,d,e,f);//set object to call the class
		System.out.println("a is: "+le.getA());//call method in the class
		System.out.println("b is: "+le.getB());//call method in the class
		System.out.println("c is: "+le.getC());//call method in the class
		System.out.println("d is: "+le.getD());//call method in the class
		System.out.println("e is: "+le.getE());//call method in the class
		System.out.println("f is: "+le.getF());//call method in the class
		if(le.isSolvable()==true)//call method to tell if the equation can be solved or not
		{
			System.out.print("x is "+le.getX()+" and "+"y is "+le.getY());
		}
		else
			System.out.print("The equation has no solution.");
	}
}
