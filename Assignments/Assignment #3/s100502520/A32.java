package a3.s100502520;

import java.util.Scanner;

public class A32 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double[] numb = new double[6];
		System.out.println("please input six number: ");
		for (int i = 0; i < 6; i++) {
			numb[i] = input.nextDouble();
		}
		//建立一個物件 useLinearEquation
		LinearEquation useLinearEquation = new LinearEquation(numb);
		//判斷是否有解
		if (useLinearEquation.isSolvable() == true) {
			System.out.println("x is " + useLinearEquation.getX() + " and y is "
					+ useLinearEquation.getY());
		}
		else {
			System.out.println("The equation has no solution.");
		}
	}
}
