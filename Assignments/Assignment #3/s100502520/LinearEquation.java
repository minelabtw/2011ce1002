package a3.s100502520;

public class LinearEquation {
	private double a;
	private double b;
	private double c;
	private double d;
	private double e;
	private double f;
	//使用建構子來初始化a、b、c、d、e、f
	public LinearEquation(double numb[]) {
		a = numb[0];
		b = numb[1];
		c = numb[2];
		d = numb[3];
		e = numb[4];
		f = numb[5];
	}
	//判斷是否有解
	public boolean isSolvable() {
		if (((a * d) - (b * c)) != 0) {
			return true;

		} else {
			return false;
		}
	}
	//計算x
	public double getX() {
		return (((e * d) - (b * f)) / ((a * d) - (b * c)));

	}
	//計算y
	public double getY() {
		return (((a * f) - (e * c)) / ((a * d) - (b * c)));

	}

}
