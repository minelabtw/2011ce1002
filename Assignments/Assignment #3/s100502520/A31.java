package a3.s100502520;

import java.util.Scanner;
import java.math.BigDecimal;

public class A31 {
	public static void main(String[] args) {
		double a[][] = new double[3][3];
		double b[][] = new double[3][3];
		double fin[][] = new double[3][3];
		Scanner input = new Scanner(System.in);
		// 輸入 matrix 1
		System.out.print("Enter matrix 1: ");
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				a[i][j] = input.nextDouble();
			}
		}
		// 輸入 matrix 1
		System.out.print("Enter matrix 2: ");
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				b[i][j] = input.nextDouble();
			}
		}
		// 呼叫 method subMat 計算   subtraction
		fin = multipleMatrix(a, b);
		// 將結果四捨五入
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				fin[i][j] = (new BigDecimal(fin[i][j]).setScale(2,
						BigDecimal.ROUND_HALF_UP)).doubleValue();
			}
		}
		System.out.println(a[0][0] + " " + a[0][1] + " " + a[0][2] + "\t"
				+ b[0][0] + " " + b[0][1] + " " + b[0][2] + "\t" + fin[0][0]
				+ " " + fin[0][1] + " " + fin[0][2]);
		System.out.println(a[1][0] + " " + a[1][1] + " " + a[1][2] + "  *\t"
				+ b[1][0] + " " + b[1][1] + " " + b[1][2] + "  =\t" + fin[1][0]
				+ " " + fin[1][1] + " " + fin[1][2]);
		System.out.println(a[2][0] + " " + a[2][1] + " " + a[2][2] + "\t"
				+ b[2][0] + " " + b[2][1] + " " + b[2][2] + "\t" + fin[2][0]
				+ " " + fin[2][1] + " " + fin[2][2]);

	}

	public static double[][] multipleMatrix(double[][] a, double[][] b) {
		double[][] result = new double[3][3];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < 3; k++) {
					result[i][j] += a[i][k] * b[k][j];
				}
			}
		}
		return result;
	}
}
