package a3.s100502016;
import java.util.Scanner;

public class A31 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double[][] a = new double[3][3];
		double[][] b = new double[3][3];
		double[][] c = new double[3][3];
		java.text.DecimalFormat decimal = new java.text.DecimalFormat("###,##0.0");
		System.out.print("Enter matrix1:");
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				a[i][j] = input.nextDouble();
			}
		}
		System.out.print("Enter matrix2:");
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				b[i][j] = input.nextDouble();
			}
		}
		c = multipleMatrix(a, b);
		System.out.println("the matrices are multiplied as follows:");
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print(decimal.format(a[i][j]) + " ");
			}
			if (i == 1) {
				System.out.print(" *");
			}
			System.out.print("\t");
			for (int j = 0; j < 3; j++) {
				System.out.print(decimal.format(b[i][j]) + " ");
			}
			if (i == 1) {
				System.out.print(" =");
			}
			System.out.print("\t");
			for (int j = 0; j < 3; j++) {
				System.out.print(decimal.format(c[i][j]) + " ");
			}
			System.out.println("");
		}

	}

	public static double[][] multipleMatrix(double[][] a, double[][] b) {
		double[][] c = new double[3][3];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < 3; k++) {
					c[i][j] += a[i][k] * b[k][j];
				}
			}
		}

		return c;

	}
}