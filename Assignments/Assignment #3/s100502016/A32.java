package a3.s100502016;

import java.util.Scanner;

public class A32 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double[] tempNum = new double[6];

		System.out.println("input");
		for (int i = 0; i < 6; i++) {
			tempNum[i] = input.nextDouble();
		}
		LinearEquation linearEquation = new LinearEquation(tempNum[0],
				tempNum[1], tempNum[2], tempNum[3], tempNum[4], tempNum[5]);

		if (linearEquation.isSolvable() == true) {
			System.out.println("x is " + linearEquation.getX() + " and y is "
					+ linearEquation.getY());
		} else {
			System.out.println("The equation has no solution");
		}
	}
}
