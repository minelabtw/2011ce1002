package a3.s100502518;

public class LinearEquation {
		private static double a;
		private static double b;
		private static double c;
		private static double d;
		private static double e;
		private static double f;
		
		LinearEquation(double[] num)//把陣列的值放進private
		{
			a=num[0];
			b=num[1];
			c=num[2];
			d=num[3];
			e=num[4];
			f=num[5];
		}
		
		public boolean isSolvable()
		{
			if((a*d-b*c)!=0)//不等於0的話return true
			{
				return true;
			}
			
			else
			{
				return false;
			}
		}
		
		public double getX()
		{
			return (e*d-b*f)/(a*d-b*c);
		}
		
		public double getY()
		{
			return (a*f-e*c)/(a*d-b*c);
		}
		
}
