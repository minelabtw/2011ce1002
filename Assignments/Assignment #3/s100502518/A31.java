package a3.s100502518;

import java.util.Scanner;
import java.math.BigDecimal;
public class A31 {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		double[][] a= new double[3][3];
		double[][] b= new double[3][3];
		
		System.out.print("Enter matrix1: ");
		for(int i=0;i<3;i++)//把值放進各自的陣列
		{
			for(int j=0;j<3;j++)
			{
				a[i][j]=input.nextDouble();
			}
		}
		
		System.out.print("Enter matrix2: ");
		for(int i=0;i<3;i++)//同上
		{
			for(int j=0;j<3;j++)
			{
				b[i][j]=input.nextDouble();
			}
		}
		
		System.out.println("The matrices are multiplied as follows:");
		
		for(int i=0;i<3;i++)//依序排版
		{
			for(int j=0;j<3;j++)
			{
				System.out.print(a[i][j]+" ");
			}
			
			if(i==1)
			{
				System.out.print(" *\t");
			}
			else
			{
				System.out.print("\t");
			}
			
			for(int j=0;j<3;j++)
			{
				System.out.print(b[i][j]+" ");
			}
			
			if(i==1)
			{
				System.out.print(" =\t");
			}
			else
			{
				System.out.print("\t");
			}
			
			for(int j=0;j<3;j++)
			{
				System.out.print(multipleMatrix(a, b)[i][j]+" ");
			}
			
			System.out.println();
		}
	}
	
	public static double[][] multipleMatrix(double[][] a,double[][] b)//矩陣的乘法公式
	{
		double[][] c = new double[3][3];
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				c[i][j]=a[i][0]*b[0][j]+a[i][1]*b[1][j]+a[i][2]*b[2][j];//找出規則並運用迴圈
				c[i][j]=(new   BigDecimal(c[i][j]).setScale(2,   BigDecimal.ROUND_HALF_UP)).doubleValue();//goole到的四捨五入方法
			}
		}
		return c;
	}
}
