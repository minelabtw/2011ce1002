package a3.s100502518;

import java.util.Scanner;
public class A32 {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		
		System.out.println("input");
		double[] num= new double[6];
		
		for(int a=0;a<6;a++)//輸入到陣列
		{
			num[a]=input.nextDouble();
		}
		
		LinearEquation linearEquation = new LinearEquation(num);//宣告object
		
		System.out.println("output");
		if(linearEquation.isSolvable()==true)//判斷要顯示什麼
		{
			System.out.println("x is "+linearEquation.getX()+" and y is "+linearEquation.getY());
		}
		else
		{
			System.out.println("The equation has no solution");
		}
	}

}
