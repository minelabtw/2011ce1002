package a3.s100502007;
import java.util.Scanner;
public class A31 {
	public static void main(String[] args){
		double[][] matrix1=new double[3][3];//create first matrix
		double[][] matrix2=new double[3][3];//create second matrix

		Scanner input=new Scanner(System.in);
		System.out.print("Enter matrix1 : ");
		for(int i=0;i<=2;i++){
			for(int j=0;j<=2;j++){
				matrix1[i][j] =input.nextDouble();//enter number in first matrix
			}
		}
		System.out.print("Enter matrix2 : ");
		for(int i=0;i<=2;i++){
			for(int j=0;j<=2;j++){
				matrix2[i][j] =input.nextDouble();//enter number in second matrix
			}
		}
		System.out.print("the matrices are multiplied as follows: \n");
		float[][] matrix3=multipleMatrix(matrix1,matrix2);//call method
		for(int i=0;i<=2;i++){
			if(i==0){//display row 0 of multipleMatrix
				System.out.println(matrix1[i][i]+"\t"+matrix1[i][i+1]+"\t"+matrix1[i][i+2]+"\t"+" "+"\t"+matrix2[i][i]+"\t"+matrix2[i][i+1]+"\t"+matrix2[i][i+2]+"\t"+" "+"\t"+matrix3[i][i]+"\t"+matrix3[i][i+1]+"\t"+matrix3[i][i+2]);				
		}
			if(i==1){//display row 1 of multipleMatrix
					System.out.println(matrix1[i][i-1]+"\t"+matrix1[i][i]+"\t"+matrix1[i][i+1]+"\t"+"*"+"\t"+matrix2[i][i-1]+"\t"+matrix2[i][i]+"\t"+matrix2[i][i+1]+"\t"+"="+"\t"+matrix3[i][i-1]+"\t"+matrix3[i][i]+"\t"+matrix3[i][i+1]);			
			}
			if(i==2){//display row 2 of multipleMatrix
				System.out.println(matrix1[i][i-2]+"\t"+matrix1[i][i-1]+"\t"+matrix1[i][i]+"\t"+" "+"\t"+matrix2[i][i-2]+"\t"+matrix2[i][i-1]+"\t"+matrix2[i][i]+"\t"+" "+"\t"+matrix3[i][i-2]+"\t"+matrix3[i][i-1]+"\t"+matrix3[i][i]);			
		}
		}
	}	
	public static float[][] multipleMatrix(double matrix1[][],double matrix2[][]){//the method of calculate multipleMatrix
		float[][] multiple=new float[3][3];
		for(int i=0;i<=2;i++){
			for(int j=0;j<=2;j++){
				if(i==0&&j==0){//when row=0 column=0
					multiple[i][j]=(float) (matrix1[i][j]*matrix2[i][j]+matrix1[i][j+1]*matrix2[i+1][j]+matrix1[i][j+2]*matrix2[i+2][j]);
				}
				if(i==0&&j==1){//when row=0 column=1
					multiple[i][j]=(float) (matrix1[i][j-1]*matrix2[i][j]+matrix1[i][j]*matrix2[i+1][j]+matrix1[i][j+1]*matrix2[i+2][j]);
				}
				if(i==0&&j==2){//when row=0 column=2
					multiple[i][j]=(float) (matrix1[i][j-2]*matrix2[i][j]+matrix1[i][j-1]*matrix2[i+1][j]+matrix1[i][j]*matrix2[i+2][j]);
				}
				if(i==1&&j==0){//when row=1 column=0
					multiple[i][j]=(float) (matrix1[i][j]*matrix2[i-1][j]+matrix1[i][j+1]*matrix2[i][j]+matrix1[i][j+2]*matrix2[i+1][j]);
				}
				if(i==1&&j==1){//when row=1 column=1
					multiple[i][j]=(float) (matrix1[i][j-1]*matrix2[i-1][j]+matrix1[i][j]*matrix2[i][j]+matrix1[i][j+1]*matrix2[i+1][j]);
				}
				if(i==1&&j==2){//when row=1 column=2
					multiple[i][j]=(float) (matrix1[i][j-2]*matrix2[i-1][j]+matrix1[i][j-1]*matrix2[i][j]+matrix1[i][j]*matrix2[i+1][j]);
				}
				if(i==2&&j==0){//when row=2 column=0
					multiple[i][j]=(float) (matrix1[i][j]*matrix2[i-2][j]+matrix1[i][j+1]*matrix2[i-1][j]+matrix1[i][j+2]*matrix2[i][j]);
				}
				if(i==2&&j==1){//when row=2 column=1
					multiple[i][j]=(float) (matrix1[i][j-1]*matrix2[i-2][j]+matrix1[i][j]*matrix2[i-1][j]+matrix1[i][j+1]*matrix2[i][j]);
				}
				if(i==2&&j==2){//when row=2 column=2
					multiple[i][j]=(float) (matrix1[i][j-2]*matrix2[i-2][j]+matrix1[i][j-1]*matrix2[i-1][j]+matrix1[i][j]*matrix2[i][j]);
				}
			}
		}
		return multiple;
	}
}

