package a3.s100502007;
import java.util.Scanner;
public class A32 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		System.out.println("input 6 number to calculate x and y : ");
	
		float a=input.nextFloat();//enter number in arguments
		float b=input.nextFloat();
		float c=input.nextFloat();
		float d=input.nextFloat();
		float e=input.nextFloat();
		float f=input.nextFloat();

		LinearEquation linearequation = new LinearEquation(a,b,c,d,e,f);//pass arguments into class
		if(linearequation.isSolvable()){//判斷ad-bc是否=0，若不=0，則顯示
			
			System.out.println("x is "+linearequation.getX()+" and y is "+linearequation.getY());//call method from class
		}
		else{
			System.out.println("The equation has no solution");//若=0，則顯示
			
		}
	}
}
