package a3.s100502505;

import java.util.Scanner;

public class A31 {
	public static double[][] subtractMatrices(double[][] a, double[][] b) {// 把兩個矩陣相乘
		double[][] c = new double[3][3];// 宣告陣列

		for (int i = 0; i < 3; i++) {// 矩陣乘法
			for (int j = 0; j < 3; j++) {
				c[i][j] = a[i][0] * b[0][j] + a[i][1] * b[1][j] + a[i][2]
						* b[2][j];
			}
		}
		return c;
	}

	public static void main(String arg[]) {
		System.out.print("Enter matrix1:");
		Scanner input = new Scanner(System.in);
		double[][] matrix1 = new double[3][3];// 宣告矩陣
		double[][] matrix2 = new double[3][3];// 宣告矩陣

		for (int i = 0; i < 3; i++) {// 把值放進矩陣
			for (int j = 0; j < 3; j++) {
				matrix1[i][j] = input.nextDouble();
			}
		}

		System.out.print("Enter matrix2:");

		for (int i = 0; i < 3; i++) {// 把值放進矩陣
			for (int j = 0; j < 3; j++) {
				matrix2[i][j] = input.nextDouble();
			}
		}

		System.out.println("The matrices are subtracted as follows:");

		for (int i = 0; i < 3; i++) {// 輸出數字 排版
			for (int j = 0; j < 3; j++) {
				System.out.print(matrix1[i][j] + "\t");
			}
			if (i == 1) {
				System.out.print("*\t");
			} else {
				System.out.print("\t");
			}
			for (int j = 0; j < 3; j++) {
				System.out.print(matrix2[i][j] + "\t");
			}
			if (i == 1) {
				System.out.print("=\t");
			} else {
				System.out.print("\t");
			}
			for (int j = 0; j < 3; j++) {
				System.out.printf("%.1f\t",
						subtractMatrices(matrix1, matrix2)[i][j]);//讓小數點只到第一位
			}
			System.out.println("");
		}
	}
}// 完成

