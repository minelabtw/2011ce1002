package a3.s100502505;

import java.util.Scanner;

public class A32 {
	public static void main(String arg[])
	{
		double a,b,c,d,e,f;//宣告數字
		System.out.println("input");
		
		Scanner input = new Scanner(System.in);
		a = input.nextDouble();//存入數字
		b = input.nextDouble();
		c = input.nextDouble();
		d = input.nextDouble();
		e = input.nextDouble();
		f = input.nextDouble();
		
		System.out.println("output");
		LinearEquation LE = new LinearEquation(a, b, c, d, e, f);//宣告物件,並把數字存到class裡
		
		if(LE.isSolvable())//如果a*d-b*c!=0,就顯示答案
		{
			System.out.print("x is " + LE.getX() + " and y is " + LE.getY());
		}else{//如果a*d-b*c==0,就輸出一段話
			System.out.print("The equation has no solution");
		}
	}
}
