package a3.s100502505;

public class LinearEquation {
	LinearEquation(double num1, double num2, double num3, double num4,double num5, double num6)
	{
		a = num1;//將數字存入class裡面
		b = num2;
		c = num3;
		d = num4;
		e = num5;
		f = num6;
	}
	
	public boolean isSolvable()//判斷a*d-b*c是否=0
	{
		if(a*d-b*c!=0) 
		{
			return true;
		}else{
			return false;
		}
	}
	
	public double getX()//算出X的答案
	{
		return (e*d-b*f) / (a*d-b*c);
		
	}

	public double getY()//算出Y的答案
	{
		return (a*f-e*c) / (a*d-b*c);
	}
	
private double a;
private double b;
private double c;
private double d;
private double e;
private double f;
}
