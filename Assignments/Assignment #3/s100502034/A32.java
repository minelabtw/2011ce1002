package a3.s100502034;

import java.util.Scanner;
import java.io.*;
import a3.s100502034.LinearEquation;
public class A32 {
	public static void main(String[] args){
		double[] modulus = new double[6];//用來放abcdef
		int loop = 0;
		while (loop == 0){//無限循環
			System.out.println("input");
			Scanner input = new Scanner(System.in);
			for ( int i =0 ; i < modulus.length;i++){//輸入abcdef
				modulus[i] = input.nextDouble();
			}
			System.out.println("output");
			LinearEquation equation = new LinearEquation(modulus[0],modulus[1],modulus[2],modulus[3],modulus[4],modulus[5]);//用LinearEquation宣告
			if (equation.isSolvable() == true)//判斷是否有solution
				System.out.println("x is " + equation.getX() + " and y is " + equation.getY());//LinearEquation裡的X Y return
			else
				System.out.println("The equation has no solution"); 
			System.out.println();
		}
	}
}