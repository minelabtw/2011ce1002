package a3.s100502034;

import java.util.Scanner;
import java.io.*;
import java.math.BigDecimal;//用來改小數點後一位數的東西
public class A31 {
	public static double[][] MultipleMatrix( double[][] a, double[][] b){//用來計算結果的method
		double[][] result = new double[a.length][b[0].length];
		for (int i = 0; i<a.length;i++){
			for ( int j = 0; j < b[0].length; j++){
				result[i][j] = 0;
				for ( int k = 0; k < a[0].length ; k++)
				{
					result[i][j] = result[i][j] + a[i][k]*b[k][j]; //就是...矩陣嘛  ((P.S.本來對矩陣就不熟了= =  花了一下時間去看一下
				}
			}
		}
		return result;//把結果陣列回傳
	}
	
	public static void main(String[] args){
		int arraySize1 = 0;//陣列的高度
		int arraySize2 = 0;//陣列的寬度
		int maxHeight = 0;//因為要協調兩個陣列的輸出 所以誕生了這個最大的高度
		Scanner input = new Scanner(System.in);
		
		System.out.println("please enter the height of the FIRST array");//第一個陣列的高度
		arraySize1 = input.nextInt();
		System.out.println("please enter the width of the FIRST array");//第一個陣列的寬度
		arraySize2 = input.nextInt();
		double [][] array1 = new double[arraySize1][arraySize2];//宣告第一個陣列 <~ java可以這樣宣告一個未知陣列的大小 很爽@V@
		System.out.println("please enter the height of the SECOND array");//第二個陣列的高度
		arraySize1 = input.nextInt();
		System.out.println("please enter the width of the SECOND array");//第二個陣列的寬度
		arraySize2 = input.nextInt();
		double [][] array2 = new double[arraySize1][arraySize2];//宣告第二個陣列
		System.out.println("input\n");
		
		System.out.print("Enter matrix1: ");//把第一個陣列的東西傳進去
		for ( int x = 0; x < array1.length; x++){
			for ( int y = 0;y < array1[0].length; y++){
				array1[x][y] = input.nextDouble();
			}
		}
		System.out.print("Enter matrix2: ");//把第二個陣列的東西傳進去
		for ( int x = 0; x < array2.length; x++){
			for ( int y = 0;y < array2[0].length; y++){
				array2[x][y] = input.nextDouble();
			}
		}
		
		System.out.println("the matrices are multiplied as follows:");
		double[][] outcome = MultipleMatrix(array1 , array2); // 宣告一個接收結果的陣列
		
		if (array1.length > array2.length){//計算陣列最大的高度
			maxHeight = array1.length;
		}
		else{
			maxHeight = array2.length;
		}
		
		if (array1[0].length == array2.length){//矩陣的條件 這兩個數要相等
			for ( int x = 0; x < maxHeight; x++){//用一個循環印每一行
				if (array1.length >= x + 1){ //要有足夠大少才會印出第一個陣列 (因為超過陣列範圍(就是沒宣告東西)就會ERROR)
					for (int y = 0 ; y < array1[0].length;y++){//印出第一個陣列的值
						System.out.print(array1[x][y] + "\t");
							if ( y == array1[0].length-1 && x == maxHeight/2)
								System.out.print("*"); //印出*號
					}
				}
				else{
					for (int i = 0 ; i < array1[0].length;i++)//如果陣列已經沒有東西的時候
						System.out.print("\t");//就給它一個\t
					if(x == maxHeight/2)
						System.out.print("*");
				}
				
				System.out.print("\t");
				if (array2.length >= x + 1){
					for (int y = 0 ; y < array2[0].length;y++){//要有足夠大少才會印出第二個陣列 
						System.out.print(array2[x][y] + "\t");
						if ( y == array2[0].length-1 && x == maxHeight/2)
								System.out.print("=");
						}
				}
				else
				{
					for (int i = 0 ; i < array2[0].length;i++)//如果陣列已經沒有東西的時候
						System.out.print("\t");//就給它一個\t
					if(x == maxHeight/2)
						System.out.print("=");
				}
				System.out.print("\t");
				if (outcome.length >= x + 1){//要有足夠大少才會印出結果陣列
					for (int y = 0 ; y < outcome[0].length;y++){
						BigDecimal point = new BigDecimal(outcome[x][y]);
						System.out.print(point.setScale(1, BigDecimal.ROUND_HALF_UP) + "\t");
					}
				}
				System.out.println();
			}
		}
	}
}//不熟矩陣 debug de 了超久