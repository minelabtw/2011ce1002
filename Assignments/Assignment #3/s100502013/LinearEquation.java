package a3.s100502013;

public class LinearEquation {
	LinearEquation(double n1,double n2,double n3,double n4,double n5,double n6){ //constructor: save the numbers
		a = n1;
		b = n2;
		c = n3;
		d = n4;
		e = n5;
		f = n6;
	}
	
	public boolean isSolvable(){ //test it whether it can be solve or not
		if(a*d - b*c!=0)
			return true;
		else
			return false;
	}
	
	public double getX(){ //get the answer x
		return (e*d - b*f)/(a*d - b*c);
	}
	
	public double getY(){ //get the answer y
		return (a*f - e*c)/(a*d - b*c);
	}
	
	private double a;
	private double b;
	private double c;
	private double d;
	private double e;
	private double f;
}
