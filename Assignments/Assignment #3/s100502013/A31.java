package a3.s100502013;
import java.util.Scanner;

public class A31 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		double matrix1[][] = new double[3][3];
		double matrix2[][] = new double[3][3];
		System.out.print("Enter matrix1: "); //the first matrix
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				matrix1[i][j] = input.nextDouble();
			}
		}
		System.out.print("Enter matrix2: "); //the second matrix
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				matrix2[i][j] = input.nextDouble();
			}
		}
		System.out.println("the matrices are multiplied as follows:");
		for(int i=0;i<3;i++){ //print out the whole equations
			for(int j=0;j<3;j++){ //print the first matrix
				System.out.print(matrix1[i][j] + "\t");
			}
			if(i==1)
				System.out.print("*\t");
			else
				System.out.print("\t");
			for(int j=0;j<3;j++){  //print the second matrix
				System.out.print(matrix2[i][j] + "\t");
			}
			if(i==1)
				System.out.print("=\t");
			else
				System.out.print("\t");
			for(int j=0;j<3;j++){  //print the result matrix
				System.out.print(multipleMatrix(matrix1,matrix2)[i][j] + "\t");
			}
			System.out.println();
		}
	}
	
	public static double[][] multipleMatrix(double[][] a , double[][] b){ //multiply two 2D-matrices
		double[][] result = new double[3][3];
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				result[i][j] = (a[i][0] * b[0][j]) + (a[i][1] * b[1][j]) + (a[i][2] * b[2][j]); 
			}
		}
		return result;
	}
}
