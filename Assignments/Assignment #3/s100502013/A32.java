package a3.s100502013;
import java.util.Scanner;

public class A32 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		System.out.print("ax + by = e\ncx + dy = f\n"); //the equations
		System.out.print("Enter the numbers a,b,c,d,e,f : "); //input the numbers
		double a = input.nextDouble();
		double b = input.nextDouble();
		double c = input.nextDouble();
		double d = input.nextDouble();
		double e = input.nextDouble();
		double f = input.nextDouble();
		LinearEquation mymathquestion = new LinearEquation(a,b,c,d,e,f); //create the Class
		if(mymathquestion.isSolvable()) //output the result
			System.out.println("x is " + mymathquestion.getX() + " and y is " + mymathquestion.getY());
		else
			System.out.println("The equation has no solution");
	}
}
