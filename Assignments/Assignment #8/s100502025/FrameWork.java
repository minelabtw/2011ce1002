package a8.s100502025;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

import javax.swing.border.*;

public class FrameWork extends JFrame implements ActionListener {
	
	JLabel jlb = new JLabel();  //這是計算機計算機的上半部
	private JButton[] jbt = new JButton[15];  //這是計算機的15個按鈕
	private String string = "";  //會顯示出來的字
	
	public FrameWork() {
		Border lineBorder = new LineBorder(Color.BLACK , 4);  //設定邊線，黑色的，粗為4
		Font largeFont1 = new Font("Serif" , Font.BOLD , 50);  //設定第一種字型，字體為Serif，粗體字，大小為50
		Font largeFont2 = new Font("Serif" , Font.BOLD , 35);  //設定第二種字型，字體為Serif，粗體字，大小為35
		
		jlb.setFont(largeFont1);  //將上面那塊設定第一種字型
		jlb.setBorder(lineBorder);  //將上面那塊設黑色邊線
				
		JPanel panel2 = new JPanel(new GridLayout(3,5));  //這是計算機的下半部，分成3*5個格子
		for(int i = 0 ; i <= 8 ; i++ ) {
			jbt[i] = new JButton("" + ( i + 1 ) );  
			jbt[i].setFont(largeFont2);  //將上面那塊設定第二種字型
			panel2.add(jbt[i]);  //一個按鈕一個按鈕加入
		}
		jbt[9] = new JButton("0");  //這些是剩下還沒設定的按鈕
		jbt[10] = new JButton("+");
		jbt[11] = new JButton("-");
		jbt[12] = new JButton("*");
		jbt[13] = new JButton("/");
		jbt[14] = new JButton("√");
		for(int i = 9 ; i <= 14 ; i++ ) {
			jbt[i].setFont(largeFont2);  //每個按鈕都設定成第二種字型
			panel2.add(jbt[i]);  //最後在加入下半部的板子中
		}
		
		setLayout(new GridLayout( 2 , 1 ));  //將兩個部份分別加入視窗
		add(jlb);
		add(panel2);	
		
		for(int i = 0 ; i <= 14 ; i++ ) {
			jbt[i].addActionListener(this);  
		}
	}
	
	public String string(String s) {  //處理文字的內容
		string = string + s;
		return string;
	}
	
	public void actionPerformed(ActionEvent e) {	
		if(e.getSource() == jbt[0]){  //假如按下1
			jlb.setText(string("1"));		
		}	
		if(e.getSource() == jbt[1]){  //假如按下2
			jlb.setText(string("2"));	
		}
		if(e.getSource() == jbt[2]){  //假如按下3
			jlb.setText(string("3"));	
		}
		if(e.getSource() == jbt[3]){  //假如按下4
			jlb.setText(string("4"));	
		}
		if(e.getSource() == jbt[4]){  //假如按下5
			jlb.setText(string("5"));	
		}
		if(e.getSource() == jbt[5]){  //假如按下6
			jlb.setText(string("6"));	
		}
		if(e.getSource() == jbt[6]){  //假如按下7
			jlb.setText(string("7"));	
		}
		if(e.getSource() == jbt[7]){  //假如按下8
			jlb.setText(string("8"));	
		}
		if(e.getSource() == jbt[8]){  //假如按下9
			jlb.setText(string("9"));	
		}
		if(e.getSource() == jbt[9]){  //假如按下0
			jlb.setText(string("0"));	
		}
		if(e.getSource() == jbt[10]){  //假如按下+
			jlb.setText(string("+"));	
		}
		if(e.getSource() == jbt[11]){  //假如按下-
			jlb.setText(string("-"));	
		}
		if(e.getSource() == jbt[12]){  //假如按下*
			jlb.setText(string("*"));	
		}
		if(e.getSource() == jbt[13]){  //假如按下/
			jlb.setText(string("/"));	
		}
		if(e.getSource() == jbt[14]){  //假如按下√
			jlb.setText(string("√"));	
		}
	}
}
