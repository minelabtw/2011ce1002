package a8.s100502507;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class FrameWork extends JFrame implements ActionListener {
	FrameWork() {
		strText01 = "0";
		JPanel base = new JPanel(new GridLayout(3, 1, 2, 2));//Background panel
		JPanel p1 = new JPanel(new GridLayout(1, 5, 2, 2));
		JPanel p2 = new JPanel(new GridLayout(1, 5, 2, 2));
		JPanel p3 = new JPanel(new GridLayout(1, 5, 2, 2));
		JPanel p4 = new JPanel(new GridLayout(1, 5, 2, 2));
		JPanel functions01 = new JPanel(new GridLayout(2, 1, 2, 2));//2 row (upper)
		JPanel functions02 = new JPanel(new GridLayout(2, 1, 2, 2));//2 row (below)
		JPanel outputField = new JPanel(new BorderLayout(2, 2));
		strText01 = "0";//String stores number
		strCondition = "";
		textField = new JLabel(strText01);//Output
		condition = new JLabel(strCondition);
		
		bt0 = new JButton("0");//Declare buttons
		bt1 = new JButton("1");
		bt2 = new JButton("2");
		bt3 = new JButton("3");
		bt4 = new JButton("4");
		bt5 = new JButton("5");
		bt6 = new JButton("6");
		bt7 = new JButton("7");
		bt8 = new JButton("8");
		bt9 = new JButton("9");
		btDot = new JButton(".");
		btAdd = new JButton("+");
		btDec = new JButton("-");
		btMul = new JButton("*");
		btDiv = new JButton("/");
		btResult = new JButton("=");
		btC = new JButton("C");
		
		bt0.addActionListener(this);//Add ActionListener to buttons
		bt1.addActionListener(this);
		bt2.addActionListener(this);
		bt3.addActionListener(this);
		bt4.addActionListener(this);
		bt5.addActionListener(this);
		bt6.addActionListener(this);
		bt7.addActionListener(this);
		bt8.addActionListener(this);
		bt9.addActionListener(this);
		btDot.addActionListener(this);
		btAdd.addActionListener(this);
		btDec.addActionListener(this);
		btMul.addActionListener(this);
		btDiv.addActionListener(this);
		btResult.addActionListener(this);
		btC.addActionListener(this);
		
		p1.add(bt7);//Add buttons to panels
		p1.add(bt8);
		p1.add(bt9);
		p1.add(btDiv);
		p2.add(bt4);
		p2.add(bt5);
		p2.add(bt6);
		p2.add(btMul);
		p3.add(bt1);
		p3.add(bt2);
		p3.add(bt3);
		p3.add(btDec);
		p4.add(bt0);
		p4.add(btDot);
		p4.add(btAdd);
		p4.add(btC);
		functions01.add(p1);
		functions01.add(p2);
		functions02.add(p3);
		functions02.add(p4);
		outputField.add(textField, BorderLayout.WEST);
		outputField.add(condition, BorderLayout.EAST);
		base.add(outputField);
		base.add(functions01);
		base.add(functions02);
		add(base);
	}
	
	public void actionPerformed(ActionEvent e) {//The events which will take place when user click on this
		if(e.getSource()==bt0 && strText01.length()<=20) {
			if(strText01=="0") {
				strText01 = "0";
				textField.setText(strText01);
			}
			else {
				strText01 += "0";
				textField.setText(strText01);
			}
		}
		else if(e.getSource()==bt1 && strText01.length()<=20) {
			if(strText01=="0") {
				strText01 = "1";
			}
			else {
				strText01 += "1";
			}
			textField.setText(strText01);
		}
		else if(e.getSource()==bt2 && strText01.length()<=20) {
			if(strText01=="0") {
				strText01 = "2";
			}
			else {
				strText01 += "2";
			}
			textField.setText(strText01);
		}
		else if(e.getSource()==bt3 && strText01.length()<=20) {
			if(strText01=="0") {
				strText01 = "3";
			}
			else {
				strText01 += "3";
			}
			textField.setText(strText01);
		}
		else if(e.getSource()==bt4 && strText01.length()<=20) {
			if(strText01=="0") {
				strText01 = "4";
			}
			else {
				strText01 += "4";
			}
			textField.setText(strText01);
		}
		else if(e.getSource()==bt5 && strText01.length()<=20) {
			if(strText01=="0") {
				strText01 = "5";
			}
			else {
				strText01 += "5";
			}
			textField.setText(strText01);
		}
		else if(e.getSource()==bt6 && strText01.length()<=20) {
			if(strText01=="0") {
				strText01 = "6";
			}
			else {
				strText01 += "6";
			}
			textField.setText(strText01);
		}
		else if(e.getSource()==bt7 && strText01.length()<=20) {
			if(strText01=="0") {
				strText01 = "7";
			}
			else {
				strText01 += "7";
			}
			textField.setText(strText01);
		}
		else if(e.getSource()==bt8 && strText01.length()<=20) {
			if(strText01=="0") {
				strText01 = "8";
			}
			else {
				strText01 += "8";
			}
			textField.setText(strText01);
		}
		else if(e.getSource()==bt9 && strText01.length()<=20) {
			if(strText01=="0") {
				strText01 = "9";
			}
			else {
				strText01 += "9";
			}
			textField.setText(strText01);
		}
		else if(e.getSource()==btDot && strText01.length()<=20) {
			strText01 += ".";
			textField.setText(strText01);
		}
		else if(e.getSource()==btAdd) {
			strText01 += "+";
			textField.setText(strText01);
		}
		else if(e.getSource()==btDec) {
			strText01 += "-";
			textField.setText(strText01);
		}
		else if(e.getSource()==btMul) {
			strText01 += "*";
			textField.setText(strText01);
		}
		else if(e.getSource()==btDiv) {
			strText01 += "/";
			textField.setText(strText01);
		}
		else if(e.getSource()==btResult) {
			strText01 += "=";
			textField.setText(strText01);
		}
		else if(e.getSource()==btC){
			strText01 = "0";
			strCondition = "";
			textField.setText(strText01);
			condition.setText(strCondition);
		}
		else {
			strCondition = "Full!";
			condition.setText(strCondition);
		}
	}
	private String strText01, strCondition;
	private JButton bt0, bt1, bt2, bt3, bt4, bt5, bt6, bt7, bt8, bt9, btDot, btAdd, btDec, btMul, btDiv, btResult, btC;
	private JLabel textField, condition;
}
