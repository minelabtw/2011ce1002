package a8.s100502014;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class FrameWork extends JFrame implements ActionListener {
	private JTextArea jta = new JTextArea();
	private JPanel jpta = new JPanel();
	private JPanel jpnl = new JPanel();
	private JButton[] jbtn = new JButton[23];
	private String pString = "";
	
	public FrameWork() {
		setLayout(new GridLayout(2,1));
		jpnl.setLayout(new GridLayout(5,5));
		
		//create number button
		for(int i=0;i<10;i++) {
			jbtn[i] = new JButton(String.valueOf(i));
			jbtn[i].setFont(new Font("SansSerif", Font.BOLD, 20));
			jpnl.add(jbtn[i]);
			jbtn[i].addActionListener(this);
		}
		
		//create others
		jbtn[10] = new JButton("+");
		jbtn[11] = new JButton("-");
		jbtn[12] = new JButton("��");
		jbtn[13] = new JButton("/");
		jbtn[14] = new JButton("��");
		jbtn[15] = new JButton("%");
		jbtn[16] = new JButton(".");
		jbtn[17] = new JButton("(");
		jbtn[18] = new JButton(")");
		jbtn[19] = new JButton("!");
		jbtn[20] = new JButton("^");
		jbtn[21] = new JButton("��");
		jbtn[22] = new JButton("C");
		for(int i=10;i<23;i++) {
			jbtn[i].setFont(new Font("SansSerif", Font.BOLD, 20));
			jpnl.add(jbtn[i]);
			jbtn[i].addActionListener(this);
		}
		
		jta.setRows(8);
		jta.setSize(950,10);
		jta.setFont(new Font("SansSerif", Font.BOLD, 20));
		jta.setEditable(false);
		jta.setLineWrap(true);
		jpta.add(jta);
		add(jpta);
		add(jpnl);		
	}
	public void actionPerformed(ActionEvent e) {
		//clean all
		if(e.getSource() == jbtn[22]) {
			pString = "";
			jta.setText(pString);
		}
		//others
		else {
			for(int i=0;i<22;i++) {
				if(e.getSource() == jbtn[i]) {
					pString += jbtn[i].getText();
					jta.setText(pString);
					break;
				}
				
			}
		}
	}
}
