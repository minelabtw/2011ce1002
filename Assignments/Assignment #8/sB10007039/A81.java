package a8.sB10007039;

import javax.swing.JFrame;

public class A81 {
	public static void main(String[] args) {//main for gui init and set close button to exit.
		FrameWork tempWork = new FrameWork();
		tempWork.setTitle("Asignment #8");
		tempWork.setSize(500, 300);
		tempWork.setLocationRelativeTo(null);
		tempWork.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		tempWork.setVisible(true);
	}
}
