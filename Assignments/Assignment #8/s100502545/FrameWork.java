package a8.s100502545;


import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public  class FrameWork extends JFrame implements ActionListener
{
	//宣告16個按鈕物件
	JButton num1 = new JButton("1");
	JButton num2 = new JButton("2");
	JButton num3 = new JButton("3");
	JButton num4 = new JButton("4");
	JButton num5 = new JButton("5");
	JButton num6 = new JButton("6");
	JButton num7 = new JButton("7");
	JButton num8 = new JButton("8");
	JButton num9 = new JButton("9");
	JButton num0 = new JButton("0");
	JButton Add  = new JButton("+");
	JButton Sub  = new JButton("-");
	JButton Mul  = new JButton("*");
	JButton Div  = new JButton("/");
	JButton Pit  = new JButton(".");
	JButton Equ  = new JButton("=");

	
	JLabel Show = new JLabel("");//宣告顯示user按取字串的label
	
	JPanel pa1 = new JPanel();//宣告兩個Panel
	JPanel pa2 = new JPanel();
	
	Font font = new Font("", Font.BOLD, 24);//設定字型大小
	
	public FrameWork()
	{//設定每個按鈕的字型.以及處理
		
		pa1.add(Show);
		pa1.setBackground(getBackground().WHITE);//背景色
		Show.setFont(font);
		
		pa2.setLayout(new GridLayout(4,4));//4X4
		pa2.add(num1);
		
		num1.setFont(font);
		num1.addActionListener(this);
		
		
		pa2.add(num2);
		num2.setFont(font);
		num2.addActionListener(this);
		
		pa2.add(num3);
		num3.setFont(font);
		num3.addActionListener(this);
		
		pa2.add(num4);
		num4.setFont(font);
		num4.addActionListener(this);
		
		pa2.add(num5);
		num5.setFont(font);
		num5.addActionListener(this);
		
		pa2.add(num6);
		num6.setFont(font);
		num6.addActionListener(this);
		
		pa2.add(num7);
		num7.setFont(font);
		num7.addActionListener(this);
		
		pa2.add(num8);
		num8.setFont(font);
		num8.addActionListener(this);
		
		pa2.add(num9);
		num9.setFont(font);
		num9.addActionListener(this);
		
		pa2.add(num0);
		num0.setFont(font);
		num0.addActionListener(this);
		
		pa2.add(Add);
		Add.setFont(font);
		Add.addActionListener(this);
		
		pa2.add(Sub);
		Sub.setFont(font);
		Sub.addActionListener(this);
		
		pa2.add(Mul);
		Mul.setFont(font);
		Mul.addActionListener(this);
		
		pa2.add(Div);
		Div.setFont(font);
		Div.addActionListener(this);
		
		pa2.add(Pit);
		Pit.setFont(font);
		Pit.addActionListener(this);
		
		pa2.add(Equ);
		Equ.setFont(font);
		Equ.addActionListener(this);
		
		add(pa2,BorderLayout.SOUTH);//版面設置
		add(pa1,BorderLayout.CENTER);
		
	}
	

	
	public void actionPerformed(ActionEvent e)//每個按鈕的處理,執行
	{
		if(e.getSource() == num1)
		{			
			Show.setText(Text()+"1");
		}
		
		if(e.getSource() == num2)
		{			
			Show.setText(Text()+"2");
		}
		
		if(e.getSource() == num3)
		{			
			Show.setText(Text()+"3");
		}
		
		if(e.getSource() == num4)
		{
			Show.setText(Text()+"4");
		}
		
		if(e.getSource() == num5)
		{
			Show.setText(Text()+"5");
		}
		
		if(e.getSource() == num6)
		{
			Show.setText(Text()+"6");
		}
		
		if(e.getSource() == num7)
		{
			Show.setText(Text()+"7");
		}
		
		if(e.getSource() == num8)
		{
			Show.setText(Text()+"8");
		}
		
		if(e.getSource() == num9)
		{	
			Show.setText(Text()+"9");
		}
		
		if(e.getSource() == num0)
		{
			Show.setText(Text()+"0");
		}
		
		if(e.getSource() == Add)
		{
			
			Show.setText(Text()+"+");
		}
		
		if(e.getSource() == Sub)
		{
			
			Show.setText(Text()+"-");
		}
		
		if(e.getSource() == Mul)
		{
			Show.setText(Text()+"*");
		}
		
		if(e.getSource() == Div)
		{
			Show.setText(Text()+"/");
		}
		
		if(e.getSource() == Pit)
		{
			Show.setText(Text()+".");
		}
		
		if(e.getSource() == Equ)
		{
			Show.setText(Text()+"=");
		}
	}
	
	
	public String Text()//得到Label上的字
	{
	   return Show.getText();
	}

}
