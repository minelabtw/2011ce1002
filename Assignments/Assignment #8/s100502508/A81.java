package a8.s100502508;

import javax.swing.*;

public class A81 extends JFrame
{	
	public static void main(String [] args)
	{
		FrameWork framework = new FrameWork();//create a frame
		framework.setTitle("Asignment #8");//set the frame title
		framework.setSize(1000, 500);//set the frame size
		framework.setLocationRelativeTo(null);//center a frame
		framework.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		framework.setVisible(true);//display the frame
	}//end main
}
