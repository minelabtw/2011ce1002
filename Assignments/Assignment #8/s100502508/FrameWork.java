package a8.s100502508;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener
{
	private JButton[] button=new JButton[17];
	private String word;
	
	JLabel label=new JLabel();//create label
	Font font1=new Font("SansSerif",Font.BOLD,30);//create fonts
	Font font2=new Font("SansSerif",Font.BOLD,80);
	
	public FrameWork()
	{
		word=new String();//initialize
		button[0]=new JButton("0");
		button[1]=new JButton("1");
		button[2]=new JButton("2");
		button[3]=new JButton("3");
		button[4]=new JButton("4");
		button[5]=new JButton("5");
		button[6]=new JButton("6");
		button[7]=new JButton("7");
		button[8]=new JButton("8");
		button[9]=new JButton("9");
		button[10]=new JButton("/");
		button[11]=new JButton("*");
		button[12]=new JButton("-");
		button[13]=new JButton("+");
		button[14]=new JButton(".");
		button[15]=new JButton("=");
		button[16]=new JButton("AC");
		
		JPanel p1=new JPanel(new GridLayout(2,1,5,5));//create panels and set GridLayout
		JPanel p2=new JPanel(new GridLayout(1,1,0,0));
		JPanel p3=new JPanel(new GridLayout(2,5,2,2));
		JPanel p4=new JPanel(new GridLayout(1,3,2,2));
		JPanel p5=new JPanel(new GridLayout(1,2,2,2));
		JPanel p6=new JPanel(new GridLayout(2,1,2,2));
		JPanel p7=new JPanel(new GridLayout(2,1,0,0));
		
		for(int a=0;a<17;a++)//set font
		{
			button[a].setFont(font1);
		}
		
		label.setFont(font2);
		p2.add(label);//add label to panel
		
		for(int a=0;a<17;a++)//add buttons to panel
		{
			if(a<10)
			{
				p3.add(button[a]);
			}
			else if(a>=10&&a<15)
			{
				p4.add(button[a]);
			}
			else
			{
				p5.add(button[a]);
			}
		}
		
		p6.add(p4,BorderLayout.NORTH);
		p6.add(p5,BorderLayout.CENTER);
		p7.add(p3,BorderLayout.NORTH);
		p7.add(p6,BorderLayout.CENTER);
		
		p1.add(p2,BorderLayout.NORTH);
		p1.add(p7,BorderLayout.CENTER);
		add(p1);
		
		for(int a=0;a<17;a++)//active button event here
		{
			button[a].addActionListener(this);
		}
	}

	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource()==button[0])
		{
			tostring("0");
			label.setText(word);
		}
		else if(e.getSource()==button[1])
		{
			tostring("1");
			label.setText(word);
		}
		else if(e.getSource()==button[2])
		{
			tostring("2");
			label.setText(word);
		}
		else if(e.getSource()==button[3])
		{
			tostring("3");
			label.setText(word);
		}
		else if(e.getSource()==button[4])
		{
			tostring("4");
			label.setText(word);
		}
		else if(e.getSource()==button[5])
		{
			tostring("5");
			label.setText(word);
		}
		else if(e.getSource()==button[6])
		{
			tostring("6");
			label.setText(word);
		}
		else if(e.getSource()==button[7])
		{
			tostring("7");
			label.setText(word);
		}
		else if(e.getSource()==button[8])
		{
			tostring("8");
			label.setText(word);
		}
		else if(e.getSource()==button[9])
		{
			tostring("9");
			label.setText(word);
		}
		else if(e.getSource()==button[10])
		{
			tostring("/");
			label.setText(word);
		}
		else if(e.getSource()==button[11])
		{
			tostring("*");
			label.setText(word);
		}
		else if(e.getSource()==button[12])
		{
			tostring("-");
			label.setText(word);
		}
		else if(e.getSource()==button[13])
		{
			tostring("+");
			label.setText(word);
		}
		else if(e.getSource()==button[14])
		{
			tostring(".");
			label.setText(word);
		}
		else if(e.getSource()==button[15])
		{
			tostring("=");
			label.setText(word);
		}
		else 
		{
			empty();
			label.setText(word);
		}
	}
	public void tostring(String receivestring)//change word 
	{
		word=word+receivestring;
	}
	public void empty()//initialize the word
	{
		word=new String();
	}
}
