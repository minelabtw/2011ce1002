package a8.s100502001;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
public class FrameWork extends JFrame implements ActionListener{
	private JButton button[];
	private JPanel push;
	private JTextField output;
	private String[] word;
	private String temp;
	public FrameWork(){
		button=new JButton[16]; //create the keyboard
		push=new JPanel(); //set the button
		output=new JTextField("PUSH THE BUTTON"); //create the screen
		word=new String[button.length];
		temp=""; //user所按的button所出現的字串
		setWord();
		setScreen();
		design();
	}
	public void setWord(){ //store the value of button
		for(int i=0;i<=9;i++)
			word[i]=Integer.toString(i);
		word[10]="+";
		word[11]="-";
		word[12]="*";
		word[13]="/";
		word[14]=".";
		word[15]="=";
	}
	public void design(){ //design the color and font
		Font font=new Font("TimesRoman",Font.BOLD,36);
		for(int i=0;i<button.length;i++){
			button[i].setFont(font);
			if(i<=9)
				button[i].setForeground(Color.BLUE);
			else
				button[i].setForeground(Color.RED);
			
		}
	}
	public void setScreen(){  //button與output的值
		output.setLayout(new BorderLayout(0,5)); 
		add(output,BorderLayout.NORTH);
		
		push.setLayout(new GridLayout(4,4));
		for(int i=0;i<=9;i++) //button[0]~button[9] 為數字1~9
			button[i]=new JButton(word[i]);
		button[10]=new JButton(word[10]);
		button[11]=new JButton(word[11]);
		button[12]=new JButton(word[12]);
		button[13]=new JButton(word[13]);
		button[14]=new JButton(word[14]);
		button[15]=new JButton(word[15]);
		for(int i=0;i<button.length;i++)
			push.add((JButton)button[i]);
		add(push);
		for(int i=0;i<button.length;i++)
			button[i].addActionListener(this);

	}
	public void actionPerformed(ActionEvent act){ 
		//public void actionPerformed(ActionEvent act) //是 ActionListener裡的abstract method
		for(int i=0;i<button.length;i++){
			if(act.getSource()==button[i]){ //若按下各個button之後
				setString(word[i]);
				output.setText(temp);
			}
		}
		
	}
	public void setString(String str){ // handle the string
			temp+=str; 
	}
}
