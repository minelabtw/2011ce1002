package a8.s100502520;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class FrameWork extends JFrame implements ActionListener  {
	private JFrame frame= new JFrame();
	private JButton[] buttons = new JButton[15];
	private JPanel p1 = new JPanel();
	private JPanel p2 = new JPanel();
	private Font f1 =  new Font("Boss", Font.BOLD, 30);
	public FrameWork(){
		frame.setSize(800, 600);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setbuttons();
		for(int i = 0; i<15; i++){
			buttons[i].addActionListener(this);
		}
		frame.setVisible(true);
	}
	//設置按鈕
	public void setbuttons(){
		for(int i = 0; i<10; i++){
			buttons[i] = new JButton(Integer.toString(i));
		}
		buttons[10] = new JButton("+");
		buttons[11] = new JButton("-");
		buttons[12] = new JButton("*");
		buttons[13] = new JButton("/");
		buttons[14] = new JButton("=");
		
		p1.setLayout(new GridLayout(3, 5));
		
		for(int j = 0; j<15; j++){
			p1.add(buttons[j]);
		}
		frame.add(p1, BorderLayout.SOUTH);
	}
	//按下按鍵所做的事
	public void actionPerformed(ActionEvent e){
		for(int i = 0; i < 10; i++){
			if(e.getSource() == buttons[i]){
				output(Integer.toString(i));
			}
		}
		if(e.getSource() == buttons[10]){
			output("+");
		}
		if(e.getSource() == buttons[11]){
			output("-");
		}
		if(e.getSource() == buttons[12]){
			output("*");
		}
		if(e.getSource() == buttons[13]){
			output("/");
		}
		if(e.getSource() == buttons[14]){
			output("=");
		}
	}
	//顯示輸入的自元
	public void output(String word){
		p2.setLayout(new FlowLayout());
		JLabel label = new JLabel(word);
		label.setFont(f1);
		p2.add(label);
		frame.add(p2, BorderLayout.NORTH);
		frame.setVisible(true);
	}
}
