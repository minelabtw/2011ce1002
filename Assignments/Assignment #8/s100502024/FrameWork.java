package a8.s100502024;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class FrameWork extends JFrame implements ActionListener
{
	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();
	JLabel jlb = new JLabel();
	JButton button[] = new JButton[15];
	Border border = new LineBorder(Color.BLACK,5);
	Font font = new Font("SansSerif",Font.BOLD,70);  // 設定字型
	String allstring = "";
	public FrameWork()
	{
		jlb.setBorder(border);
		p1.setLayout(new GridLayout(3,5));
		for(int i=0;i<=9;i++) // 依序增加Button內容及設定字型
		{
			button[i] = new JButton(""+i);
			button[i].setFont(font);
			p1.add(button[i]);
		}
		button[10] = new JButton("+");
		button[11] = new JButton("-");
		button[12] = new JButton("*");
		button[13] = new JButton("/");
		button[14] = new JButton("√");
		for(int i=10;i<=14;i++) // 依序設定字型並增加到p1上
		{
			button[i].setFont(font);
			p1.add(button[i]);
		}
		p2.setLayout(new GridLayout(2,1));
		p2.add(jlb);
		p2.add(p1);
		add(p2);
		for(int i=0;i<15;i++)
		{
			button[i].addActionListener(this);
		}
	}
	public String handlestring(String input)  // 處理string
	{
		allstring = allstring + input;
		return allstring;
	}
	public void actionPerformed(ActionEvent e)  // 按下Button後要做的事
	{
		if(e.getSource() == button[0])  
		{
			jlb.setText(handlestring("0")); // 按下"0"時增加"0"到method裡並顯示
			jlb.setFont(font);
		}
		if(e.getSource() == button[1])
		{
			jlb.setText(handlestring("1"));
			jlb.setFont(font);
		}
		if(e.getSource() == button[2])
		{
			jlb.setText(handlestring("2"));
			jlb.setFont(font);
		}
		if(e.getSource() == button[3])
		{
			jlb.setText(handlestring("3"));
			jlb.setFont(font);
		}
		if(e.getSource() == button[4])
		{
			jlb.setText(handlestring("4"));
			jlb.setFont(font);
		}
		if(e.getSource() == button[5])
		{
			jlb.setText(handlestring("5"));
			jlb.setFont(font);
		}
		if(e.getSource() == button[6])
		{
			jlb.setText(handlestring("6"));
			jlb.setFont(font);
		}
		if(e.getSource() == button[7])
		{
			jlb.setText(handlestring("7"));
			jlb.setFont(font);
		}
		if(e.getSource() == button[8])
		{
			jlb.setText(handlestring("8"));
			jlb.setFont(font);
		}
		if(e.getSource() == button[9])
		{
			jlb.setText(handlestring("9"));
			jlb.setFont(font);
		}
		if(e.getSource() == button[10])
		{
			jlb.setText(handlestring("+"));
			jlb.setFont(font);
		}
		if(e.getSource() == button[11])
		{
			jlb.setText(handlestring("-"));
			jlb.setFont(font);
		}
		if(e.getSource() == button[12])
		{
			jlb.setText(handlestring("*"));
			jlb.setFont(font);
		}
		if(e.getSource() == button[13])
		{
			jlb.setText(handlestring("/"));
			jlb.setFont(font);
		}
		if(e.getSource() == button[14])
		{
			jlb.setText(handlestring("√"));
			jlb.setFont(font);
		}
	}
}
