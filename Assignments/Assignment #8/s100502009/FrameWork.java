package a8.s100502009;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;
import javax.swing.border.*;
public class FrameWork extends JFrame implements ActionListener{
	JButton jbt1=new JButton("1");//create buttons
	JButton jbt2=new JButton("2");
	JButton jbt3=new JButton("3");
	JButton jbt4=new JButton("4");
	JButton jbt5=new JButton("5");
	JButton jbt6=new JButton("6");
	JButton jbt7=new JButton("7");
	JButton jbt8=new JButton("8");
	JButton jbt9=new JButton("9");
	JButton jbt0=new JButton("0");
	JButton jbtk=new JButton("+");
	JButton jbtl=new JButton("-");
	JButton jbtm=new JButton("*");
	JButton jbtn=new JButton("/");
	JButton jbtp=new JButton("��");
	JTextField jlb=new JTextField(output(""),43);//create a text field
	String answer="";
	public FrameWork()
	{
		JPanel p1=new JPanel();
		p1.setLayout(new GridLayout(5,3));
		p1.add(jbt1);
		p1.add(jbt2);
		p1.add(jbt3);
		p1.add(jbt4);
		p1.add(jbt5);
		p1.add(jbt6);
		p1.add(jbt7);
		p1.add(jbt8);
		p1.add(jbt9);
		p1.add(jbt0);
		p1.add(jbtk);
		p1.add(jbtl);
		p1.add(jbtm);
		p1.add(jbtn);
		p1.add(jbtp);
		jbt1.addActionListener(this);
		jbt2.addActionListener(this);
		jbt3.addActionListener(this);
		jbt4.addActionListener(this);
		jbt5.addActionListener(this);
		jbt6.addActionListener(this);
		jbt7.addActionListener(this);
		jbt8.addActionListener(this);
		jbt9.addActionListener(this);
		jbt0.addActionListener(this);
		jbtk.addActionListener(this);
		jbtl.addActionListener(this);
		jbtm.addActionListener(this);
		jbtn.addActionListener(this);
		jbtp.addActionListener(this);
		jbt1.setBackground(Color.BLACK);
		jbt1.setForeground(Color.WHITE);
		jbt2.setBackground(Color.BLACK);
		jbt2.setForeground(Color.WHITE);
		jbt3.setBackground(Color.BLACK);
		jbt3.setForeground(Color.WHITE);
		jbt4.setBackground(Color.BLACK);
		jbt4.setForeground(Color.WHITE);
		jbt5.setBackground(Color.BLACK);
		jbt5.setForeground(Color.WHITE);
		jbt6.setBackground(Color.BLACK);
		jbt6.setForeground(Color.WHITE);
		jbt7.setBackground(Color.BLACK);
		jbt7.setForeground(Color.WHITE);
		jbt8.setBackground(Color.BLACK);
		jbt8.setForeground(Color.WHITE);
		jbt9.setBackground(Color.BLACK);
		jbt9.setForeground(Color.WHITE);
		jbt0.setBackground(Color.BLACK);
		jbt0.setForeground(Color.WHITE);
		jbtm.setBackground(Color.BLACK);
		jbtm.setForeground(Color.WHITE);
		jbtk.setBackground(Color.BLACK);
		jbtk.setForeground(Color.WHITE);
		jbtl.setBackground(Color.BLACK);
		jbtl.setForeground(Color.WHITE);
		jbtn.setBackground(Color.BLACK);
		jbtn.setForeground(Color.WHITE);
		jbtp.setBackground(Color.BLACK);
		jbtp.setForeground(Color.WHITE);
		JPanel p2=new JPanel();
		Border lineBorder=new LineBorder(Color.BLACK,2);
		jlb.setBorder(lineBorder);
		p2.add(jlb);	
		add(p2,BorderLayout.NORTH);
		add(p1,BorderLayout.CENTER);
	}
	
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()==jbt1)//show the word 
		{			
			jlb.setText(output("1"));
		}
		else if(e.getSource()==jbt2)
		{
			jlb.setText(output("2"));
		}
		else if(e.getSource()==jbt3)
		{
			jlb.setText(output("3"));
		}
		else if(e.getSource()==jbt4)
		{
			jlb.setText(output("4"));
		}
		else if(e.getSource()==jbt5)
		{
			jlb.setText(output("5"));
		}
		else if(e.getSource()==jbt6)
		{
			jlb.setText(output("6"));
		}
		else if(e.getSource()==jbt7)
		{
			jlb.setText(output("7"));
		}
		else if(e.getSource()==jbt8)
		{
			jlb.setText(output("8"));
		}
		else if(e.getSource()==jbt9)
		{
			jlb.setText(output("9"));
		}
		else if(e.getSource()==jbt0)
		{
			jlb.setText(output("0"));
		}
		else if(e.getSource()==jbtk)
		{
			jlb.setText(output("+"));
		}
		else if(e.getSource()==jbtl)
		{
			jlb.setText(output("-"));
		}
		else if(e.getSource()==jbtm)
		{
			jlb.setText(output("*"));
		}
		else if(e.getSource()==jbtn)
		{
			jlb.setText(output("/"));
		}
		else 
			jlb.setText(output("��"));
	}
	
	public String output(String word)//method to handle the string
	{		
		answer+=word;		
		return answer; 
	}

}