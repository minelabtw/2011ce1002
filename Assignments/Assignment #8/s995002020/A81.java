package a8.s995002020;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class A81 extends JFrame {
	private JButton jbtnum1;   // JButton Member
	private JButton jbtnum2;
	private JButton jbtnum3;
	private JButton jbtnum4;
	private JButton jbtnum5;
	private JButton jbtnum6;
	private JButton jbtnum7;
	private JButton jbtnum8;
	private JButton jbtnum9;
	private JButton jbtnum0;
	
	private JButton jbtadd;
	private JButton jbtsub;
	private JButton jbtdiv;
	private JButton jbtmul;
	private JTextField jtfResult;
	public String display = "";
	
	public A81(){
		JPanel p1 = new JPanel();               //Panel 1
		p1.setLayout(new GridLayout(4,3));
		
		p1.add(jbtnum1= new JButton("1"));	
        p1.add(jbtnum2 = new JButton("2")); 
        p1.add(jbtnum3 = new JButton("3")); 
        p1.add(jbtnum4 = new JButton("4")); 
        p1.add(jbtnum5 = new JButton("5")); 
        p1.add(jbtnum6 = new JButton("6")); 
        p1.add(jbtnum7 = new JButton("7")); 
        p1.add(jbtnum8 = new JButton("8")); 
        p1.add(jbtnum9 = new JButton("9"));
        p1.add(jbtnum0 = new JButton("0")); 
		
		
		 JPanel p2 = new JPanel();             //Panel 2
	        p2.setLayout(new FlowLayout()); 
	        p2.add(jtfResult = new JTextField(20)); 
	        jtfResult.setHorizontalAlignment(JTextField.RIGHT); 
	        jtfResult.setEditable(false); 
	 
	                JPanel p3 = new JPanel();           //Panel 3
	                p3.setLayout(new GridLayout(5, 1)); 
	                p3.add(jbtadd = new JButton("+")); 
	                p3.add(jbtsub = new JButton("-")); 
	                p3.add(jbtmul = new JButton("*")); 
	                p3.add(jbtdiv = new JButton("/")); 
	                
	                JPanel p = new JPanel();               //Border Setting
	    	        p.setLayout(new GridLayout()); 
	    	        p.add(p2, BorderLayout.NORTH); 
	    	        p.add(p1, BorderLayout.SOUTH); 
	    	        p.add(p3, BorderLayout.EAST); 
	    	        add(p); 
	    	        
	    	        	jbtnum1.addActionListener(new ListenToOne()); //execute every situation
	    		        jbtnum2.addActionListener(new ListenToTwo()); 
	    		        jbtnum3.addActionListener(new ListenToThree()); 
	    		        jbtnum4.addActionListener(new ListenToFour()); 
	    		        jbtnum5.addActionListener(new ListenToFive()); 
	    		        jbtnum6.addActionListener(new ListenToSix()); 
	    		        jbtnum7.addActionListener(new ListenToSeven()); 
	    		        jbtnum8.addActionListener(new ListenToEight()); 
	    		        jbtnum9.addActionListener(new ListenToNine()); 
	    		        jbtnum0.addActionListener(new ListenToZero()); 
	    		 
	    		        jbtadd.addActionListener(new ListenToAdd()); 
	    		        jbtsub.addActionListener(new ListenToSubtract()); 
	    		        jbtmul.addActionListener(new ListenToMultiply()); 
	    		        jbtdiv.addActionListener(new ListenToDivide()); 
	                 
	}
	
	
	  class ListenToOne implements ActionListener { 
	        public void actionPerformed(ActionEvent e) { 
	            
	        	{display = jtfResult.getText(); 
	            jtfResult.setText(display+ "1");}  
	    
	   }
	    }
	    class ListenToTwo implements ActionListener { 
	        public void actionPerformed(ActionEvent e) { 
	            display = jtfResult.getText(); 
	            jtfResult.setText(display + "2"); 
	        } 
	    } 
	    class ListenToThree implements ActionListener { 
	        public void actionPerformed(ActionEvent e) { 
	            display = jtfResult.getText(); 
	            jtfResult.setText(display + "3"); 
	        } 
	    } 
	    class ListenToFour implements ActionListener { 
	        public void actionPerformed(ActionEvent e) { 
	            display = jtfResult.getText(); 
	            jtfResult.setText(display + "4"); 
	        } 
	    } 
	    class ListenToFive implements ActionListener { 
	        public void actionPerformed(ActionEvent e) { 
	            display = jtfResult.getText(); 
	            jtfResult.setText(display + "5"); 
	        } 
	    } 
	    class ListenToSix implements ActionListener { 
	        public void actionPerformed(ActionEvent e) { 
	            display = jtfResult.getText(); 
	            jtfResult.setText(display + "6"); 
	        } 
	    } 
	    class ListenToSeven implements ActionListener { 
	        public void actionPerformed(ActionEvent e) { 
	            display = jtfResult.getText(); 
	            jtfResult.setText(display + "7"); 
	        } 
	    } 
	    class ListenToEight implements ActionListener { 
	        public void actionPerformed(ActionEvent e) { 
	            display = jtfResult.getText(); 
	            jtfResult.setText(display + "8"); 
	        } 
	    } 
	    class ListenToNine implements ActionListener { 
	        public void actionPerformed(ActionEvent e) { 
	            display = jtfResult.getText(); 
	            jtfResult.setText(display + "9"); 
	        } 
	    } 
	    class ListenToZero implements ActionListener { 
	        public void actionPerformed(ActionEvent e) { 
	            display = jtfResult.getText(); 
	            jtfResult.setText(display + "0"); 
	        } 
	    } 
	 
	    class ListenToAdd implements ActionListener { 
	        public void actionPerformed(ActionEvent e) { 
	        	 display = jtfResult.getText(); 
		            jtfResult.setText(display + "+"); 
	 
	        } 
	    } 
	    class ListenToSubtract implements ActionListener { 
	        public void actionPerformed(ActionEvent e) { 
	            display = jtfResult.getText(); 
	            jtfResult.setText(display + "-"); 
	        } 
	    } 
	    class ListenToMultiply implements ActionListener { 
	        public void actionPerformed(ActionEvent e) { 
	            display = jtfResult.getText(); 
	            jtfResult.setText(display + "*"); 
	        } 
	    } 
	    class ListenToDivide implements ActionListener { 
	        public void actionPerformed(ActionEvent e) { 
	            display = jtfResult.getText(); 
	            jtfResult.setText(display + "/"); 
	        } 
	    
	    }
	
	
	    public static void main(String[] args) { //main function
	        // TODO Auto-generated method stub 
	        A81 calc = new A81(); 
	        calc.pack(); 
	        calc.setLocationRelativeTo(null); 
	                calc.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
	        calc.setVisible(true); 
	    }
	
	

}
