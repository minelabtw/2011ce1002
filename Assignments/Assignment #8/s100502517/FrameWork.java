package a8.s100502517;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrameWork extends JFrame implements ActionListener {
	
	String word = "";
	String add = "";
	
	JButton b1 = new JButton(" 1 "); //create button
	JButton b2 = new JButton(" 2 "); //create button
	JButton b3 = new JButton(" 3 "); //create button
	JButton b4 = new JButton(" 4 "); //create button
	JButton b5 = new JButton(" 5 "); //create button
	JButton b6 = new JButton(" 6 "); //create button
	JButton b7 = new JButton(" 7 "); //create button
	JButton b8 = new JButton(" 8 "); //create button
	JButton b9 = new JButton(" 9 "); //create button
	JButton b10 = new JButton(" 0 " ); //create button
	JButton b11 = new JButton(" + "); //create button
	JButton b12 = new JButton(" - "); //create button
	JButton b13 = new JButton(" * "); //create button
	JButton b14 = new JButton(" / "); //create button
	JButton b15 = new JButton(" clear "); //create button
	
	JPanel p2 = new JPanel();
	JLabel printout = new JLabel(" ");		
	
	Font largeFont = new Font("TimesRoman", Font.BOLD,30);//字體型態、大小	
	Font largestFont = new Font("TimesRoman", Font.BOLD,50);
	
	public FrameWork(){
		
		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(3,5));//排列
		
		b1.setFont(largeFont);
		b2.setFont(largeFont);
		b3.setFont(largeFont);
		b4.setFont(largeFont);
		b5.setFont(largeFont);
		b6.setFont(largeFont);
		b7.setFont(largeFont);
		b8.setFont(largeFont);
		b9.setFont(largeFont);
		b10.setFont(largeFont);
		b11.setFont(largeFont);
		b12.setFont(largeFont);
		b13.setFont(largeFont);
		b14.setFont(largeFont);
		b15.setFont(largeFont);
		
		p1.add(b1);
		p1.add(b2);
		p1.add(b3);
		p1.add(b4);
		p1.add(b5);
		p1.add(b6);
		p1.add(b7);
		p1.add(b8);
		p1.add(b9);
		p1.add(b10);
		p1.add(b11);
		p1.add(b12);
		p1.add(b13);
		p1.add(b14);
		p1.add(b15);
		
		printout.setFont(largestFont);
		p2.add(printout);
		
		setLayout(new BorderLayout(2,1));
		add(p1, BorderLayout.SOUTH);
		add(p2,BorderLayout.CENTER);		
		
		b1.addActionListener(this);
		b2.addActionListener(this);
		b3.addActionListener(this);
		b4.addActionListener(this);
		b5.addActionListener(this);
		b6.addActionListener(this);
		b7.addActionListener(this);
		b8.addActionListener(this);
		b9.addActionListener(this);
		b10.addActionListener(this);
		b11.addActionListener(this);
		b12.addActionListener(this);
		b13.addActionListener(this);
		b14.addActionListener(this);
		b15.addActionListener(this);		
	}

	//如果按下按鈕會發生的事件
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == b1){
			add = " 1 ";
			printout.setText(addfunction(add));
		}
		if(e.getSource() == b2){
			add = " 2 ";
			printout.setText(addfunction(add));
		}
		if(e.getSource() == b3){
			add = " 3 ";
			printout.setText(addfunction(add));
		}
		if(e.getSource() == b4){
			add = " 4 ";
			printout.setText(addfunction(add));
		}
		if(e.getSource() == b5){
			add = " 5 ";
			printout.setText(addfunction(add));
		}
		if(e.getSource() == b6){
			add = " 6 ";
			printout.setText(addfunction(add));
		}
		if(e.getSource() == b7){
			add = " 7 ";
			printout.setText(addfunction(add));
		}
		if(e.getSource() == b8){
			add = " 8 ";
			printout.setText(addfunction(add));
		}
		if(e.getSource() == b9){
			add = " 9 ";
			printout.setText(addfunction(add));
		}
		if(e.getSource() == b10){
			add = " 0 ";
			printout.setText(addfunction(add));
		}
		if(e.getSource() == b11){
			add = " + ";
			printout.setText(addfunction(add));
		}
		if(e.getSource() == b12){
			add = " - ";
			printout.setText(addfunction(add));
		}
		if(e.getSource() == b13){
			add = " * ";
			printout.setText(addfunction(add));
		}
		if(e.getSource() == b14){
			add = " / ";
			printout.setText(addfunction(add));
		}
		
		//清除所有東西
		if(e.getSource() == b15){
			this.word = " ";			
			printout.setText(word);
		}
		
	}
	
	//增加東西
	public String addfunction(String addthing){
		word = word + addthing;
		return word;
	}
}


