package a8.s995002204;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;


public class FrameWork extends JFrame implements ActionListener{
	
	//declare component
	private JButton button0 = new JButton("0");
	private JButton button1 = new JButton("1");
	private JButton button2 = new JButton("2");
	private JButton button3 = new JButton("3");
	private JButton button4 = new JButton("4");
	private JButton button5 = new JButton("5");
	private JButton button6 = new JButton("6");
	private JButton button7 = new JButton("7");
	private JButton button8 = new JButton("8");
	private JButton button9 = new JButton("9");
	private JButton buttonPlu = new JButton("+");
	private JButton buttonMin = new JButton("-");
	private JButton buttonMul = new JButton("*");
	private JButton buttonDiv = new JButton("/");
	private JButton buttonEqu = new JButton("=");
	private JButton buttonCan = new JButton("C");
	
	//Window Panel
	private JLabel text = new JLabel();
	private JPanel buttonControl1 = new JPanel();
	private JPanel buttonControl2 = new JPanel();
	private JPanel buttonControl3 = new JPanel();
	private JPanel all = new JPanel();
	
	FrameWork()
	{
		buttonControl2.setLayout(new GridLayout(4,3,5,5));
		buttonControl2.add(button7);
		buttonControl2.add(button8);
		buttonControl2.add(button9);
		buttonControl2.add(button4);
		buttonControl2.add(button5);
		buttonControl2.add(button6);
		buttonControl2.add(button1);
		buttonControl2.add(button2);
		buttonControl2.add(button3);
		buttonControl2.add(button0);
		buttonControl2.add(buttonCan);
		buttonControl2.add(buttonEqu);
		buttonControl2.setSize(300, 100);
		
		buttonControl3.setLayout(new GridLayout(4,2,5,5));
		buttonControl3.add(buttonDiv);
		buttonControl3.add(buttonMul);
		buttonControl3.add(buttonMin);
		buttonControl3.add(buttonPlu);
		buttonControl3.setSize(100, 100);
		
		text.setSize(400, 15);
		text.setFont(new Font( "�s�ө���", Font.PLAIN, 40));
		
		buttonControl1.setLayout(new GridLayout(1,2,5,5));
		buttonControl1.add(buttonControl2,BorderLayout.WEST);
		buttonControl1.add(buttonControl3,BorderLayout.EAST);
		
		all.setLayout(new GridLayout(2,1));
		all.add(text,BorderLayout.NORTH);
		all.add(buttonControl1,BorderLayout.SOUTH);
		
		add(all);
		
		//add action listener to button
		button0.addActionListener(this);
		button1.addActionListener(this);
		button2.addActionListener(this);
		button3.addActionListener(this);
		button4.addActionListener(this);
		button5.addActionListener(this);
		button6.addActionListener(this);
		button7.addActionListener(this);
		button8.addActionListener(this);
		button9.addActionListener(this);
		buttonPlu.addActionListener(this);
		buttonMin.addActionListener(this);
		buttonMul.addActionListener(this);
		buttonDiv.addActionListener(this);
		buttonEqu.addActionListener(this);
		buttonCan.addActionListener(this);
		
	}
	
	public void actionPerformed(ActionEvent e)
	{	
		//examine which button is clicked
		if(e.getSource()==buttonCan)
		{
			text.setText("");
		}
		if(e.getSource()==button0)
		{
			text.setText(text.getText()+"0");
			
		}
		if(e.getSource()==button1)
		{
			text.setText(text.getText()+"1");
		}
		if(e.getSource()==button2)
		{
			text.setText(text.getText()+"2");
		}
		if(e.getSource()==button3)
		{
			text.setText(text.getText()+"3");
		}
		if(e.getSource()==button4)
		{
			text.setText(text.getText()+"4");
		}
		if(e.getSource()==button5)
		{
			text.setText(text.getText()+"5");
		}
		if(e.getSource()==button6)
		{
			text.setText(text.getText()+"6");
		}
		if(e.getSource()==button7)
		{
			text.setText(text.getText()+"7");
		}
		if(e.getSource()==button8)
		{
			text.setText(text.getText()+"8");
		}
		if(e.getSource()==button9)
		{
			text.setText(text.getText()+"9");
		}
		if(e.getSource()==buttonPlu)
		{
			text.setText(text.getText()+"+");
		}
		if(e.getSource()==buttonMin)
		{
			text.setText(text.getText()+"-");
		}
		if(e.getSource()==buttonMul)
		{
			text.setText(text.getText()+"*");
		}
		if(e.getSource()==buttonDiv)
		{
			text.setText(text.getText()+"/");
		}
		
		
		if(e.getSource()==buttonEqu)	//user click equal button
		{
			text.setText(text.getText()+"=");
			
			int count = 0;		//count + - * /
			int store[] = new int[10];	//store + - * /
			int k = 0;		//control store
			double a = 0,b = 0;		//use to store number
			double result = 0;		//put the result
			
			String temp;			//use to save character
			temp = text.getText();
			char temp1;
			store[0] = -1;
			
			char array[] = new char[temp.length()];
			
			for(int i=0;i<temp.length();i++)
			{
				temp1 = temp.charAt(i);
				array[i] = temp1;
				if(temp1=='+'||temp1=='-'||temp1=='*'||temp1=='/'||temp1=='=')
				{
					store[++k] = i;
					count++;
				}
			}
			
			for(int i=0;i<count-1;i++)
			{
				//for 2+3 condition
				if(result==0)
				{
					a = Double.parseDouble(temp.substring(store[i]+1,store[i+1]));
					b = Double.parseDouble(temp.substring(store[i+1]+1,store[i+2]));
					
					if(array[store[i+1]]=='*')
					{
						result = result+(a*b);
					}
					if(array[store[i+1]]=='/')
					{
						result = result+(a/b);
					}
					if(array[store[i+1]]=='+')
					{
						result = result+(a+b);
					}
					if(array[store[i+1]]=='-')
					{
						result = result+(a-b);
					}
				}
				
				//for 2+3-1 condition
				else
				{
					a = Double.parseDouble(temp.substring(store[i+1]+1,store[i+2]));
					
					if(array[store[i+1]]=='*')
					{
						result = result*a;
					}
					if(array[store[i+1]]=='/')
					{
						result = result/a;
					}
					if(array[store[i+1]]=='+')
					{
						result = result+a;
					}
					if(array[store[i+1]]=='-')
					{
						result = result-a;
					}
				}
			}
			text.setText(String.valueOf(result));
		}
	}
}
