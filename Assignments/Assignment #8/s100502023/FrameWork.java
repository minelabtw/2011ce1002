package a8.s100502023;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class FrameWork extends JFrame implements  ActionListener
{
	//panel
	private JPanel p1 = new JPanel();
	private JPanel p2 = new JPanel();
	private JPanel p3 = new JPanel();
	private JPanel p4 = new JPanel();
	
	//p1
	private JButton button_7 =new JButton(" 7 ");
	private JButton button_8 =new JButton(" 8 ");
	private JButton button_9 =new JButton(" 9 ");
	private JButton button_divide =new JButton(" / ");
	private JButton button_mod =new JButton(" Mod ");
	private JButton button_4 =new JButton(" 4 ");
	private JButton button_5 =new JButton(" 5 ");
	private JButton button_6 =new JButton(" 6 ");
	private JButton button_multiply =new JButton(" * ");
	private JButton button_reciprocal =new JButton(" 1/x ");
	private JButton button_1 =new JButton(" 1 ");
	private JButton button_2 =new JButton(" 2 ");
	private JButton button_3 =new JButton(" 3 ");
	private JButton button_add =new JButton(" + ");
	private JButton button_minus =new JButton(" - ");
	
	//p2
	private JButton button_zero =new JButton(" 0 ");
	private JButton button_dot =new JButton(" . ");
	private JButton button_equal =new JButton(" = ");
	private JButton button_clear =new JButton(" clear ");
	
	//p3 
	private JLabel label_show_store = new JLabel("",SwingConstants.RIGHT);//靠右對齊
	private JLabel label_show_operator = new JLabel("",SwingConstants.RIGHT);
	private JLabel label_show_input = new JLabel("",SwingConstants.RIGHT);
	
	//store input
	private String input = new String();//正在按壓的數字按鈕
	private String store = new String();//已儲存的數字按鈕
	private String operator = new String();//已儲存的運算子按鈕
	private boolean press_equal = false;//是否有按壓 = 的按鈕
	//font
	private Font font1= new Font("SansSerif",Font.BOLD,32);//input
	private Font font2= new Font("SansSerif",Font.ITALIC,32);//initial
	private Font font3= new Font("SansSerif",Font.BOLD,28);//store
	private Font font4= new Font("SansSerif",Font.BOLD,14);//operator
	
	public FrameWork()
	{
		p1.setLayout(new GridLayout(3,5,5,5));
		p2.setLayout(new GridLayout(1,4,5,5));
		p3.setLayout(new GridLayout(3,1,5,5));
		
		p4.setLayout(new BorderLayout(5,5));
		
		label_show_input.setFont(font2);
		label_show_input.setText("Please press the button ");
		label_show_store.setFont(font3);
		
		p3.add(label_show_store);
		p3.add(label_show_operator);
		p3.add(label_show_input);
		
		p4.add(p3,BorderLayout.NORTH);
		p4.add(p1,BorderLayout.CENTER);
		p4.add(p2,BorderLayout.SOUTH);
		
		//frame
		setLayout(new BorderLayout(5,5));
		add(p4,BorderLayout.CENTER);
		
		add_buttons_to_panel();  //add method
		add_listeners_to_buttons();  //add method
		
		input="";//initialize
		store="";
		operator="";
	}
	public void add_buttons_to_panel()
	{
		p1.add(button_7);
		p1.add(button_8);
		p1.add(button_9);
		p1.add(button_divide);
		p1.add(button_mod);
		p1.add(button_4);
		p1.add(button_5);
		p1.add(button_6);
		p1.add(button_multiply);
		p1.add(button_reciprocal);
		p1.add(button_1);
		p1.add(button_2);
		p1.add(button_3);
		p1.add(button_add);
		p1.add(button_minus);
		
		p2.add(button_zero);
		p2.add(button_dot);
		p2.add(button_equal);
		p2.add(button_clear);
	}
	public void add_listeners_to_buttons()
	{
		button_7.addActionListener(this);
		button_8.addActionListener(this);
		button_9.addActionListener(this);
		button_divide.addActionListener(this);
		button_mod.addActionListener(this);
		button_4.addActionListener(this);
		button_5.addActionListener(this);
		button_6.addActionListener(this);
		button_multiply.addActionListener(this);
		button_reciprocal.addActionListener(this);
		button_1.addActionListener(this);
		button_2.addActionListener(this);
		button_3.addActionListener(this);
		button_add.addActionListener(this);
		button_minus.addActionListener(this);
		button_zero.addActionListener(this);
		button_dot.addActionListener(this);
		button_equal.addActionListener(this);
		button_clear.addActionListener(this);
		
	}
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == button_7)  //當按壓的按鈕為7時
		{
			if(press_equal==true)  //已按過 = 按鈕，若再按壓數字按鈕，將已儲存的數字歸0
			{
				store="";
				press_equal=false;
			}
			input+="7";
			showInput();
			showStore();
		}
		else if (e.getSource() == button_8)  //當按壓的按鈕為8時
		{
			if(press_equal==true)
			{
				store="";
				press_equal=false;
			}
			input+="8";
			showInput();
			showStore();
		}
		else if (e.getSource() == button_9)  //當按壓的按鈕為9時
		{
			if(press_equal==true)
			{
				store="";
				press_equal=false;
			}
			input+="9";
			showInput();
			showStore();
		}
		else if (e.getSource() == button_divide)  //當按壓的按鈕為 / 時
		{
			if(press_equal==true)  //已按過 = 按鈕，若再按壓 運算子 按鈕，繼續計算(store不歸0)
			{
				press_equal=false;
			}
			access_number();
			operator="/";

			showInput();
			showStore();
			
			showOperator();
			input="";
			
		}
		else if (e.getSource() == button_mod)  //當按壓的按鈕為 Mod 時
		{
			if(press_equal==true)
			{
				press_equal=false;
			}
			access_number();
			operator="Mod";
			
			//access
			showInput();
			showStore();
			
			showOperator();
			input="";
		}
		else if (e.getSource() == button_4)  //當按壓的按鈕為4時
		{
			if(press_equal==true)
			{
				store="";
				press_equal=false;
			}
			input+="4";
			showInput();
			showStore();
		}
		else if (e.getSource() == button_5)  //當按壓的按鈕為5時
		{
			if(press_equal==true)
			{
				store="";
				press_equal=false;
			}
			input+="5";
			showInput();
			showStore();
		}
		else if (e.getSource() == button_6)  //當按壓的按鈕為6時
		{
			if(press_equal==true)
			{
				store="";
				press_equal=false;
			}
			input+="6";
			showInput();
			showStore();
		}
		else if (e.getSource() == button_multiply)  //當按壓的按鈕為 * 時
		{
			if(press_equal==true)
			{
				press_equal=false;
			}
			access_number();
			operator="*";
			
			showInput();
			showStore();
			
			showOperator();
			input="";
		}
		else if (e.getSource() == button_reciprocal) //當按壓的按鈕為 1/x 時
		{
			if(press_equal==true)
			{
				press_equal=false;
			}
			
			operator="^(-1)";
			
			if(store=="")
			{
				store=input;
			}
			access_number();
			
			showInput();
			showStore();
			
		}
		else if (e.getSource() == button_1 )  //當按壓的按鈕為1時
		{
			if(press_equal==true)
			{
				store="";
				press_equal=false;
			}
			input+="1";
			showInput();
			showStore();
		}
		else if (e.getSource() == button_2)  //當按壓的按鈕為2時
		{
			if(press_equal==true)
			{
				store="";
				press_equal=false;
			}
			input+="2";
			showInput();
			showStore();
		}
		else if (e.getSource() == button_3)  //當按壓的按鈕為3時
		{
			if(press_equal==true)
			{
				store="";
				press_equal=false;
			}
			input+="3";
			showInput();
			showStore();
		}
		else if (e.getSource() == button_add)  //當按壓的按鈕為 + 時
		{
			if(press_equal==true)
			{
				press_equal=false;
			}
			access_number();
			//access
			operator="+";
			
			showInput();
			showStore();
			
			showOperator();
			input="";
		}
		else if (e.getSource() == button_minus)  //當按壓的按鈕為 - 時
		{
			if(press_equal==true)
			{
				press_equal=false;
			}
			access_number();
			//access
			operator="-";
	
			showInput();
			showStore();
			
			showOperator();
			input="";
		}
		else if (e.getSource() == button_zero)  //當按壓的按鈕為 0 時
		{
			if(press_equal==true)
			{
				store="";
				press_equal=false;
			}
			input+="0";
			showInput();
		}
		else if (e.getSource() == button_dot)  //當按壓的按鈕為 . 時
		{
			input+=".";
			showInput();
		}
		else if (e.getSource() == button_equal)  //當按壓的按鈕為 = 時
		{
			press_equal=true;
			
			access_number();
			
			showStore();
			input="";

			showInput();
			showOperator();
			
		}
		else if (e.getSource() == button_clear)  //當按壓的按鈕為 clear 時
		{
			input="";
			label_show_input.setText("Please press the button ");
			store="";
			operator="";
			showStore();
			showOperator();
		}
		
	}
	
	public void showInput()  //show method
	{
		label_show_input.setFont(font1);
		label_show_input.setText(input);
		
	}
	public void showStore()  //show method
	{
		label_show_store.setFont(font3);
		label_show_store.setText(store);
	}
	public void showOperator()  //show method
	{
		label_show_operator.setFont(font4);
		label_show_operator.setText(operator);
	}
	public void access_number()  //calculate
	{
		if (store=="")
		{
			store=input;
		}
		else if(store!="" && operator=="^(-1)")  // 1/x 的計算
		{
			double store_number = Double.parseDouble(store);
			
			store_number=Math.pow(store_number, -1);
			store=String.valueOf(store_number);
			operator="";
			input="";
		}
		else if (store!="" && operator!="" && input!="")
		{
			double store_number = Double.parseDouble(store);
			double input_number = Double.parseDouble(input);
			
			if(operator=="/")  // / 的計算
			{
				store_number/=input_number;
				store=String.valueOf(store_number);
				operator="";
				input="";
			}
			else if(operator=="Mod")  // Mod 的計算
			{
				store_number%=input_number;
				store=String.valueOf(store_number);
				operator="";
				input="";
			}
			else if(operator=="*")   // * 的計算
			{
				store_number*=input_number;
				store=String.valueOf(store_number);
				operator="";
				input="";
			}
			else if(operator=="-")  // - 的計算
			{
				store_number-=input_number;
				store=String.valueOf(store_number);
				operator="";
				input="";
			}
			else if(operator=="+")  // + 的計算
			{
				store_number+=input_number;
				store=String.valueOf(store_number);
				operator="";
				input="";
			}
			else //something wrong
			{
				label_show_input.setText("something error,please press the clear button!!");
			}
		}
		
		
		
	}
	
}
