package a8.s992008002;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.Color;
import java.awt.Font;

  
public class FrameWork extends JFrame implements ActionListener{
 
 
        Font font1 = new Font("SansSerif", Font.BOLD, 50);     
        JTextField tf = new JTextField("0");
        JButton[] controlButton = new JButton[27];
 
 
        
 
        boolean isOperatorFlag = true;   //開始一個新的運算旗標
        String  firstNumber = "0";		 //紀錄上次運算數字
        String  firstTotal  = "0";		 //紀錄上次運算合計
        String  firstOperator = "+";	 //紀錄上次運算子
 
 
public FrameWork(){
	super("Calculator"); //set title
 
			//*set Size    (720,480)
  			setSize(720,480);
 
  			setBackground(new Color(215,215,215));//background color
  			setForeground(Color.red);//foreground color
 
  			//panel佈置
  			JPanel pa0 = new JPanel();
  			pa0.setLayout(new BorderLayout());  		
  			JPanel pa2 = new JPanel();
  			pa2.setLayout(new BorderLayout());
  			JPanel pa21 = new JPanel();
  			pa21.setLayout(new GridLayout(1,3,0,3));//Backspace,CE,C 那行
  			String[] strc = {"Backspace","CE","C"};
  			for (int i=4 ;i<7;i++){
  				controlButton[i] = new JButton(strc[(i-4)]);
  				controlButton[i].setForeground(Color.red);
 				  pa21.add(controlButton[i]);
 				  controlButton[i].addActionListener(this);  //button 事件處理
  			}
 
 
  			//鍵盤部分
  			JPanel pa22 = new JPanel();
  			pa22.setLayout(new GridLayout(4,5,3,3));
  			String[] str = {"7","8","9","/","sqrt","4","5","6","*","%","1","2","3","-","1/x","0","+/-",".","+","="};
  			for (int i=7 ;i<27;i++){
  				controlButton[i] = new JButton(str[(i-7)]);
  				controlButton[i].setForeground(Color.blue);
 
  				//****多加controlButton[i].setFont(font1)，目的為了放大按鍵裡字體的大小
  				controlButton[i].setFont(font1)	;
  				String strNumber = new String("-*/=+");
  				if (strNumber.indexOf(str[(i-7)])>=0)
  					controlButton[i].setForeground(Color.red);
  					pa22.add(controlButton[i]);
  				controlButton[i].addActionListener(this);  //button 事件處理
  			}
 
 
  			pa2.add(pa21,BorderLayout.NORTH);
  			pa2.add(pa22,BorderLayout.CENTER);
  			pa0.add(pa2,BorderLayout.CENTER);
  			
                                tf.setLocation(5, 5);
                                tf.setSize(900,40);
                                tf.setFont(font1);
                                tf.setHorizontalAlignment(JTextField.CENTER);
                                tf.setForeground(Color.black);
                                tf.setFocusable(false);
                                add(tf,BorderLayout.NORTH);
                                add(pa0,BorderLayout.CENTER);
                                setVisible(true);
			}

	public void actionPerformed(ActionEvent e){
		String clickedButton= e.getActionCommand();
		String str = ("1234567890.");
		String str2 = ("+-*/=");
		//數字與小數點
		if (str.indexOf(clickedButton)>=0){
			if ( (tf.getText().equals("系統功能建構中") | tf.getText().equals("0")) && (tf.getText()).indexOf(".")<0 && clickedButton.equals(".")==false) {
				tf.setText(clickedButton);
			}
			else{
				if ( (tf.getText()).indexOf(".")>=0 && clickedButton.equals(".") )
					tf.setText(tf.getText());
				else{
					if (isOperatorFlag)
						tf.setText(clickedButton);
					else
						tf.setText(tf.getText()+clickedButton);
				}	
			}
			isOperatorFlag = false ;
		}
//處理+-*/=
		else if (str2.indexOf(clickedButton)>=0)
			count(clickedButton,tf.getText());

//處理Backspace
		else if (clickedButton.equals("Backspace")){
			tf.setText((tf.getText()).substring(0,((tf.getText()).length() -1)));
				if ((tf.getText()).equals(""))
					tf.setText("0");
		}
//處理CE	
		else if (clickedButton.equals("CE"))
			tf.setText("0");
//處理sqrt
		else if (clickedButton.equals("sqrt"))
			tf.setText(Double.toString(Math.sqrt((new Double(tf.getText())))));
//處理1/x
		else if (clickedButton.equals("1/x")){
			Double n1x = new Double(tf.getText());
			if (n1x!=0)
				tf.setText(Double.toString(1/n1x));
		}
//處理%
		else if (clickedButton.equals("%"))
			tf.setText(Double.toString((new Double(tf.getText()))/100));
//處理+/-
		else if (clickedButton.equals("+/-"))
			tf.setText(Double.toString((new Double(tf.getText())* (-1))));
//處理C
		else if (clickedButton.equals("C")){
			tf.setText("0");
			firstOperator = "+";
			firstNumber = "0";
			firstTotal = "0";
			isOperatorFlag = true ;
 
		}
	}
//處理 +-*/=

	public void count(String currentButton,String currentNumber){
		String operationNumber = firstTotal;
		if (isOperatorFlag && currentButton.equals("=")) { //已經執行完運算,且本次重複按=,則以上次運算數字取代上次合計
			operationNumber = firstNumber;
		}
		else if (isOperatorFlag && currentButton.equals("=")==false && (firstOperator != currentButton)) { //已經執行完運算,且換其他非=的運算子
			operationNumber = currentNumber;
			if (firstOperator.equals("+") | firstOperator.equals("-"))
				currentNumber = "0";
			else
				currentNumber = "1";
 
			System.out.println(currentButton);
			System.out.println(firstOperator);
			System.out.println(firstOperator == currentButton) ;
		}
 
			System.out.println("operationNumber:"+operationNumber);
			System.out.println("currentNumber:"+currentNumber);
			System.out.println("----------------------------------------");
			if (firstOperator.equals("+"))//處理+
				operationNumber = Double.toString(new Double(operationNumber)+ new Double(currentNumber));
			else if (firstOperator.equals("-")){ //處理-
				if (isOperatorFlag && currentButton.equals("=")) 
					operationNumber = Double.toString(new Double(currentNumber) - Math.abs(new Double(operationNumber)));
				else
					operationNumber = Double.toString(new Double(operationNumber)- Math.abs(new Double(currentNumber)));
			}
			else if (firstOperator.equals("*"))//處理*
				operationNumber = Double.toString(new Double(operationNumber)* new Double(currentNumber));
			else if (firstOperator.equals("/")){//處理/
				if (isOperatorFlag && currentButton.equals("=")) 
					operationNumber = Double.toString(new Double(currentNumber) / new Double(operationNumber));
				else
					operationNumber = Double.toString(new Double(operationNumber)/ new Double(currentNumber));
			}
 
 
			if ((isOperatorFlag == false) | (currentButton.equals("=")))
				tf.setText(operationNumber);
 
			if (currentButton.equals("=")==false) 
				firstOperator = currentButton; //本次運算子存入上次運算子變數中
			if (isOperatorFlag == false | currentButton.equals("=")==false) 			 //上次運算是由數字輸入起
				firstNumber = currentNumber;	 //本次運潠數字存入上次運算數字變數中
			isOperatorFlag = true ; //可以開始下一個新運算
			firstTotal = operationNumber;//將本次運算結果存入上次合計
		}
 
	}
