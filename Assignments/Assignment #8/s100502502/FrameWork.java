//FrameWork.java
package a8.s100502502;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class FrameWork extends JFrame implements ActionListener{
	private String string = "";//initialize string
	JButton[] button = new JButton[16];//initialize button array
	JTextField Text = new JTextField("0.");//initialize text field
	Font font = new Font("SansSerif", Font.PLAIN, 45);//set font
	FrameWork(){//constructor
		JPanel p1 = new JPanel(new BorderLayout());//create a panel p1
		Text.setFont(font);//set text field font
		Text.setOpaque(true);
		Text.setBackground(Color.WHITE);
		Text.setEditable(false);
		p1.add(Text, BorderLayout.NORTH);
		JPanel p2 = new JPanel(new GridLayout(4, 4));//create a panel p2
		for(int i = 0; i < 10; i++){//set button text
			button[i] = new JButton(""+ i);
		}
		button[10] = new JButton("+");//set button text
		button[11] = new JButton("-");
		button[12] = new JButton("*");
		button[13] = new JButton("/");
		button[14] = new JButton("��");
		button[15] = new JButton(".");
		for(int i = 0; i < 16; i++){//add button and active button event 
			button[i].setFont(font);
			p2.add(button[i]);
			button[i].addActionListener(this);
		}
		p1.add(p2, BorderLayout.CENTER);
		add(p1);
	}
	public void actionPerformed(ActionEvent e){//after press the button
		for(int i = 0; i < 16; i ++){
			if(e.getSource() == button[i]){
				settext(button[i].getText());
				Text.setText(string);//reset the text
			}
		}
	}
	public void settext(String input){//set text
		string += input;
	}
}
