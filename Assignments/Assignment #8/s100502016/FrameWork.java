package a8.s100502016;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener {

	private String StringInText = new String("");
	private double former = 0;
	private double later = 0;
	private double result = 0;
	private char operator = ' ';

	JButton Btn0 = new JButton("0");
	JButton Btn1 = new JButton("1");
	JButton Btn2 = new JButton("2");
	JButton Btn3 = new JButton("3");
	JButton Btn4 = new JButton("4");
	JButton Btn5 = new JButton("5");
	JButton Btn6 = new JButton("6");
	JButton Btn7 = new JButton("7");
	JButton Btn8 = new JButton("8");
	JButton Btn9 = new JButton("9");
	JButton Btnclear = new JButton("C");
	JButton Btnpoint = new JButton(".");
	JButton BtnPlus = new JButton("+");
	JButton BtnMinus = new JButton("-");
	JButton BtnMultiply = new JButton("*");
	JButton BtnDevide = new JButton("/");
	JButton BtnEqual = new JButton("=");

	JPanel numPnl = new JPanel();
	JPanel funcPnl = new JPanel();
	JLabel DisplayLbl = new JLabel("");
	Dimension size = new Dimension(100, 100);

	FrameWork() {
		numPnl.setLayout(new GridLayout(4, 4, 5, 5));
		numPnl.add(Btn7);
		numPnl.add(Btn8);
		numPnl.add(Btn9);
		numPnl.add(Btn4);
		numPnl.add(Btn5);
		numPnl.add(Btn6);
		numPnl.add(Btn1);
		numPnl.add(Btn2);
		numPnl.add(Btn3);
		numPnl.add(Btnpoint);
		numPnl.add(Btn0);
		numPnl.add(Btnclear);
		Btn0.addActionListener(this);
		Btn1.addActionListener(this);
		Btn2.addActionListener(this);
		Btn3.addActionListener(this);
		Btn4.addActionListener(this);
		Btn5.addActionListener(this);
		Btn6.addActionListener(this);
		Btn7.addActionListener(this);
		Btn8.addActionListener(this);
		Btn9.addActionListener(this);
		Btnpoint.addActionListener(this);
		Btnclear.addActionListener(this);

		funcPnl.setLayout(new GridLayout(5, 1, 5, 5));
		funcPnl.add(BtnPlus);
		funcPnl.add(BtnMinus);
		funcPnl.add(BtnMultiply);
		funcPnl.add(BtnDevide);
		funcPnl.add(BtnEqual);
		BtnPlus.addActionListener(this);
		BtnMinus.addActionListener(this);
		BtnMultiply.addActionListener(this);
		BtnDevide.addActionListener(this);
		BtnEqual.addActionListener(this);

		DisplayLbl.setBackground(new Color(255, 255, 255));
		setLayout(new BorderLayout(20, 20));
		this.add(DisplayLbl, BorderLayout.NORTH);
		this.add(numPnl, BorderLayout.CENTER);
		this.add(funcPnl, BorderLayout.EAST);

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == Btn0) {
			StringInText += "0";
			DisplayLbl.setText(StringInText);
		} else if (e.getSource() == Btn1) {
			StringInText = StringInText + "1";
			DisplayLbl.setText(StringInText);
		} else if (e.getSource() == Btn2) {
			StringInText += "2";
			DisplayLbl.setText(StringInText);
		} else if (e.getSource() == Btn3) {
			StringInText += "3";
			DisplayLbl.setText(StringInText);
		} else if (e.getSource() == Btn4) {
			StringInText += "4";
			DisplayLbl.setText(StringInText);
		} else if (e.getSource() == Btn5) {
			StringInText += "5";
			DisplayLbl.setText(StringInText);
		} else if (e.getSource() == Btn6) {
			StringInText += "6";
			DisplayLbl.setText(StringInText);
		} else if (e.getSource() == Btn7) {
			StringInText += "7";
			DisplayLbl.setText(StringInText);
		} else if (e.getSource() == Btn8) {
			StringInText += "8";
			DisplayLbl.setText(StringInText);
		} else if (e.getSource() == Btn9) {
			StringInText += "9";
			DisplayLbl.setText(StringInText);
		} else if (e.getSource() == Btnpoint) {
			StringInText += ".";
			DisplayLbl.setText(StringInText);
		} else if (e.getSource() == BtnPlus) {
			if (StringInText != "") {
				former = Double.parseDouble(StringInText);
				operator = '+';
				StringInText = "";
			} else
				;
		} else if (e.getSource() == BtnMinus) {
			if (StringInText != "") {
				former = Double.parseDouble(StringInText);
				operator = '-';
				StringInText = "";
			} else
				;
		} else if (e.getSource() == BtnMultiply) {
			if (StringInText != "") {
				former = Double.parseDouble(StringInText);
				operator = '*';
				StringInText = "";
			} else
				;
		} else if (e.getSource() == BtnDevide) {
			if (StringInText != "") {
				former = Double.parseDouble(StringInText);
				operator = '/';
				StringInText = "";
			} else
				;
		} else if (e.getSource() == BtnEqual) {
			if (operator != ' ' && StringInText != "") {
				later = Double.parseDouble(StringInText);
				switch (operator) {
				case '+':
					result = former + later;
					break;
				case '-':
					result = former - later;
					break;
				case '*':
					result = former * later;
					break;
				case '/':
					result = former / later;
					break;
				}
				DisplayLbl.setText(String.valueOf(result));
				StringInText = "";
				operator = ' ';
			} else {
				;
			}
		} else if (e.getSource() == Btnclear) {
			former = 0;
			later = 0;
			StringInText = "";
			DisplayLbl.setText(StringInText);
		}

	}
}
