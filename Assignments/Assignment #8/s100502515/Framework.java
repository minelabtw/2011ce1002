package a8.s100502515;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Framework implements ActionListener {

	JButton[] numberButtons;
	JButton plusButtons;
	JButton minusButtons;
	JButton timesButtons;
	JButton divideButtons;
	JButton equalButtons;
	JButton clearButtons;
	JButton clearAllButtons;
	JButton decimalButtons;
	JTextField textField;

	private double result = 0.0;
	private double store = 0.0;
	private int counter = -1;
	private double temp = 0.0;
	private boolean check = false;
	private char choice = 0;

	public Framework() {//buttons set.
		numberButtons = new JButton[10];
		plusButtons = new JButton("+");
		minusButtons = new JButton("-");
		timesButtons = new JButton("x");
		divideButtons = new JButton("/");
		equalButtons = new JButton("=");
		clearAllButtons = new JButton("AC");
		clearButtons = new JButton("C");
		decimalButtons = new JButton(".");

		for (int i = 0; i <= 9; i++)
			numberButtons[i] = new JButton(new Integer(i).toString());

	}

	public void makeCalcu() {//Calculator set
		JFrame frame = new JFrame();
		JPanel numberPanel = new JPanel();
		JPanel funcPanel = new JPanel();
		JPanel zeroPanel = new JPanel();
		textField = new JTextField();

		frame.getContentPane().add(BorderLayout.WEST, numberPanel);
		frame.getContentPane().add(BorderLayout.EAST, funcPanel);
		frame.getContentPane().add(BorderLayout.CENTER, zeroPanel);
		frame.getContentPane().add(BorderLayout.NORTH, textField);

		textField.selectAll();
		textField.requestFocus();

		for (int i = 0; i <= 9; i++)
			numberButtons[i].addActionListener(this);

		plusButtons.addActionListener(this);
		minusButtons.addActionListener(this);
		timesButtons.addActionListener(this);
		divideButtons.addActionListener(this);
		equalButtons.addActionListener(this);
		clearAllButtons.addActionListener(this);
		decimalButtons.addActionListener(this);
		clearButtons.addActionListener(this);

		numberPanel.setLayout(new GridLayout(3, 3));
		funcPanel.setLayout(new GridLayout(8, 1));
		zeroPanel.setLayout(new GridLayout(1, 1));

		frame.setSize(300, 400);
		for (int i = 1; i <= 9; i++)
			numberPanel.add(numberButtons[i]);
		zeroPanel.add(numberButtons[0]);
		funcPanel.add(clearAllButtons);
		funcPanel.add(clearButtons);
		funcPanel.add(plusButtons);
		funcPanel.add(minusButtons);
		funcPanel.add(timesButtons);
		funcPanel.add(divideButtons);
		funcPanel.add(equalButtons);
		funcPanel.add(decimalButtons);
		

		frame.setVisible(true);

	}

	@Override
	public void actionPerformed(ActionEvent e) {//action after pressed

		if (e.getSource().equals(numberButtons[0])) {
			if (check == true) {
				counter--;
				store = temp;
				textField.setText(Double.toString(temp));
			} else {
				temp *= 10;
				store = temp;
				textField.setText(Double.toString(temp));
			}
		}
		for (int i = 1; i <= 9; i++) {
			if (e.getSource().equals(numberButtons[i])) {
				if (check == true) {
					temp += i * Math.pow(10, counter);
					counter--;
					store = temp;
					textField.setText(Double.toString(temp));
				} else {
					temp = temp * 10.0 + i;
					store = temp;
					textField.setText(Double.toString(temp));
				}
			}
		}
		if (e.getSource().equals(plusButtons)) {
			result = store;
			choice = '+';
			temp = 0.0;
			counter = 0;
			check = false;
			textField.setText("");
			textField.setText("+");
		}
		if (e.getSource().equals(minusButtons)) {
			result = store;
			choice = '-';
			counter = 0;
			temp = 0.0;
			check = false;
			textField.setText("");
			textField.setText("-");
		}
		if (e.getSource().equals(timesButtons)) {
			result = store;
			choice = 'x';
			counter = 0;
			temp = 0.0;
			check = false;
			textField.setText("");
			textField.setText("x");
		}
		if (e.getSource().equals(divideButtons)) {
			result = store;
			choice = '/';
			counter = 0;
			temp = 0.0;
			check = false;
			textField.setText("");
			textField.setText("/");
		}
		if (e.getSource().equals(decimalButtons)) {
			check = true;
		}
		if (e.getSource() == equalButtons) {
			if (choice == '+')
				result += temp;
				store = result;
			if (choice == '-')
				result -= temp;
				store = result;
			if (choice == 'x')
				result *= temp;
				store = result;
			if (choice == '/')
				result /= temp;
				store = result;

			textField.setText(Double.toString(result));
		}
		if (e.getSource().equals(clearAllButtons)) {
			result = 0.0;
			temp = 0.0;
			store = 0.0;
			check = false;
			textField.setText("");
		}
		if (e.getSource().equals(clearButtons)) {
			temp = 0.0;
			check = false;
			textField.setText("");
		}
		
	}
}
