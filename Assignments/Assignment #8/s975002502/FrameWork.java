package a8.s975002502;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

public class FrameWork extends JFrame implements ActionListener {
	protected int numBtn = 20;
	protected JButton button[] = new JButton[20];// Create JButton array button
	protected JTextField list;
	protected JTextField display;
	protected Double num = 0.0;
	protected Double result = 0.0;
	protected boolean isop = false;
	protected int op = 0;
	
	public FrameWork() {
		
		
		// Create JPanel numbtnPanel for the buttons and set GridLayout
		JPanel btnPanel = new JPanel();
		btnPanel.setLayout(new GridLayout(5, 4, 5, 5));
				
		// Create Fonts
		Font font1 = new Font("SansSerif", Font.BOLD, 25);
		Font font2 = new Font("SansSerif", Font.BOLD, 40);
		
		// Create the number buttons
		for (int i=0; i<10; i++) {
			button[i] = new JButton("" +i);
			button[i].addActionListener(this);
		}
		
		// Create the operation buttons
		button[10] = new JButton("��");
		button[10].addActionListener(this);
		button[11] = new JButton("C");
		button[11].addActionListener(this);
		button[12] = new JButton("CE");
		button[12].addActionListener(this);
		button[13] = new JButton("+");
		button[13].addActionListener(this);
		button[14] = new JButton("-");
		button[14].addActionListener(this);
		button[15] = new JButton("*");
		button[15].addActionListener(this);
		button[16] = new JButton("/");
		button[16].addActionListener(this);
		button[17] = new JButton("=");
		button[17].addActionListener(this);
		button[18] = new JButton("��");
		button[18].addActionListener(this);
		button[19] = new JButton(".");
		button[19].addActionListener(this);
		
		// add buttons to the panel
		btnPanel.add(button[10]);	// "��"
		btnPanel.add(button[11]);	// "C"
		btnPanel.add(button[12]);	// "CE"
		btnPanel.add(button[13]);	// "+"
		
		btnPanel.add(button[7]);	// "1"
		btnPanel.add(button[8]);	// "2"
		btnPanel.add(button[9]);	// "3"
		btnPanel.add(button[14]);	// "-"
		
		btnPanel.add(button[4]);	// "4"
		btnPanel.add(button[5]);	// "5"
		btnPanel.add(button[6]);	// "6"
		btnPanel.add(button[15]);	// "*"
		
		btnPanel.add(button[1]);	// "7"
		btnPanel.add(button[2]);	// "8"
		btnPanel.add(button[3]);	// "9"
		btnPanel.add(button[16]);	// "/"
		
		btnPanel.add(button[0]);	// "0"
		btnPanel.add(button[19]);	// "."
		btnPanel.add(button[18]);	// "��"
		btnPanel.add(button[17]);	// "="
		
		// Set the font of buttons
		for (JButton btn : button) {
			btn.setFont(font1);
		}
		
		// Create JPanel panel_1 to hold a JLabel display and p1
		//JPanel panel_1 = new JPanel(new BorderLayout());
		
		list = new JTextField("");
		display = new JTextField("0");
		
		list.setFont(font1);
		list.setEditable(false);
		list.setBackground(Color.WHITE);
		list.setPreferredSize(new Dimension(700,35));
		
		display.setFont(font2);
		display.setEditable(false);
		display.setBackground(Color.WHITE);
		display.setPreferredSize(new Dimension(700,75));

		add(list, BorderLayout.NORTH);
		add(display, BorderLayout.CENTER);
		add(btnPanel, BorderLayout.SOUTH);

	}
	
	//press button event
	public void actionPerformed(ActionEvent e){
		JButton btn=(JButton) e.getSource();
		
		for(int i=0;i<=9;i++) {
			if(btn==button[i]) {
				if(display.getText().length() == 1) {
					int value = Integer.parseInt(display.getText());
					
					if(value != 0) {
					display.setText(display.getText()+btn.getText());
					}
					else {
					display.setText(btn.getText());
					}
				}
				else {
					display.setText(display.getText()+btn.getText());
				}
				break;
			}
		}
		// �� operation
		if(btn==button[10]) {
			if(display.getText().length() != 1) {
				String newText = display.getText().substring(0, display.getText().length() - 1);
				display.setText(newText);
			}
			else {
				display.setText("0");
			}
		}
		// C operation
		else if(btn==button[11]) {
			isop=false;
		    result = 0.0;
		    num = 0.0;
		    op = 0;
		    list.setText("");
		    display.setText("0");
		}
		// CE operation
		else if(btn==button[12]) {
			display.setText("0");
		}
		// �� operation
		else if(btn==button[18]) {
			int value = (int)display.getText().charAt(0);
			
			if(value != 45) {
				display.setText("-" + display.getText());
			}
			else {
				String newText = display.getText().substring(1, display.getText().length());
				display.setText(newText);
			}
			
		}
		// . operation
		else if(btn==button[19]) {
			boolean isdouble = false;
			
			for (int i=0; i<display.getText().length(); i++) {
				int value = (int)display.getText().charAt(i);
				if (value == 46) {
					isdouble = true;
				}
			}
			
			if (isdouble==false) {
				display.setText(display.getText() + ".");
			}
		}
		// + operation
		else if(btn==button[13]) {
			op=1;
			int value = (int)display.getText().charAt(display.getText().length()-1);
			if (value != 46) {
				save_num();
				output_list("+");
			}
			else {
				display.setText(display.getText().substring(0, display.getText().length()-1));
				save_num();
				output_list("+");
			}
		}
		// - operation
		else if(btn==button[14]) {
			int value = (int)display.getText().charAt(display.getText().length()-1);
			if (value != 46) {
				save_num();
				output_list("-");
			}
			else {
				display.setText(display.getText().substring(0, display.getText().length()-1));
				save_num();
				output_list("-");
			}
			op=2;
		}
		// * operation
		else if (btn==button[15]) {
			int value = (int)display.getText().charAt(display.getText().length()-1);
			if (value != 46) {
				save_num();
				output_list("*");
			}
			else {
				display.setText(display.getText().substring(0, display.getText().length()-1));
				save_num();
				output_list("*");
			}
			op=3;
		}
		// / operation
		else if (btn==button[16]) {
			int value = (int)display.getText().charAt(display.getText().length()-1);
			if (value != 46) {
				save_num();
				output_list("/");
			}
			else {
				display.setText(display.getText().substring(0, display.getText().length()-1));
				save_num();
				output_list("/");
			}
			op=4;
		}
		// = operation
		else if (btn == button[17]) {
			result = Double.parseDouble(display.getText());
			
			switch(op)
			{
				case 1:
					num += result;
					break;
				case 2:
					num -= result;
					break;
				case 3:
					num *= result;
					break;
				case 4: 
					try {num /= result;}
					catch(ArithmeticException ae) {
						num = 0.0;
						result = 0.0;
						System.out.println("Wrong operation!");
					}
					break;
				default:
					break;
			}
			
			isop = false;
			result = 0.0;
			list.setText("");
			display.setText(num.toString());
		}
	}
	
	// display the operation of user
	private void output_list(String op) {
		if(list.getText().length() != 0) {
			System.out.println(display.getText());
			list.setText(list.getText() + display.getText() + op);
		}
		else {
			list.setText(display.getText() + op);
		}
		display.setText("0");
	}
	
	// Save the previous number
	private void save_num() {
		Double text = Double.parseDouble(display.getText());
		num = text;
	}
}
