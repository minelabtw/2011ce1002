package a8.s100502031;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.*;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener{
	
	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();
	JLabel L = new JLabel();
	JButton[] button;
	int times=0;
	int[] PM=new int[50];
	int SaveTimes=0;
	int SaveOperater=0;
	double[] SaveNumber=new double[50];
	String save="";
	
	public FrameWork(){
		//number of button
		button=new JButton[16];
		p1.setLayout(new GridLayout(4,4));
		p2.setLayout(new GridLayout(2,1));
		Font myFont = new Font("SansSerif ", Font.BOLD, 30);
		//set the button		
		for (int i = 0;i<10;i++){
					button[i] = new JButton("" + i);
					button[i].setFont(myFont);
				}
				L.setFont(myFont);
		button[10] = new JButton("/");
		button[11] = new JButton("*");
		button[12] = new JButton("+");
		button[13] = new JButton("-");
		button[14] = new JButton(".");
		button[15] = new JButton("=");
		button[10].setFont(myFont);
		button[11].setFont(myFont);
		button[12].setFont(myFont);
		button[13].setFont(myFont);
		button[14].setFont(myFont);
		button[15].setFont(myFont);
		for (int i = 0;i < button.length;i++){
			button[i].addActionListener(this);
			p1.add(button[i]);
		}
		p2.add(L, BorderLayout.NORTH);
		p2.add(p1, BorderLayout.SOUTH);
		add(p2);
	}
	
	public void actionPerformed(ActionEvent e){
		//the operater of number from 0 to 9
		for ( int i = 0 ; i < 10;i++){
			if(e.getSource() == button[i]){
				L.setText(L.getText() + i);
				SaveTimes = SaveTimes + i;
				p2.removeAll();
				p2.add(L, BorderLayout.NORTH);
				p2.add(p1, BorderLayout.SOUTH);
				p2.validate();
			}
		}
		//the operater of "/"
		if(e.getSource() == button[10]){
			L.setText(L.getText() + "/");
			SaveNumber[times] = Double.parseDouble(save);
			save = "";
			if(SaveOperater == 1){
				SaveNumber[times - 1] = SaveNumber[times-1] * SaveNumber[times];
			}
			else if (SaveOperater == 2){
				SaveNumber[times - 1] = SaveNumber[times-1] /SaveNumber[times];
			}
			else{
				times++;
			}
			SaveOperater=2;
			p2.removeAll();
			p2.add(L, BorderLayout.NORTH);
			p2.add(p1, BorderLayout.SOUTH);
			p2.validate();
		}
		//the operater of "*"
		if(e.getSource() == button[11]){
			L.setText(L.getText() + "*");
			SaveNumber[times] = Double.parseDouble(save);
			if(SaveOperater == 1){
				SaveNumber[times - 1] = SaveNumber[times-1] * SaveNumber[times];
			}
			else if (SaveOperater == 2){
				SaveNumber[times - 1] = SaveNumber[times-1] / SaveNumber[times];
			}
			else{
				times++;
			}
			SaveOperater = 1;
			save = "";
			p2.removeAll();
			p2.add(L, BorderLayout.NORTH);
			p2.add(p1, BorderLayout.SOUTH);
			p2.validate();
		}
		//the operater of "+"
		if(e.getSource() == button[12]){
			L.setText(L.getText() + "+");
			SaveNumber[times] = Double.parseDouble(save);
			if(SaveOperater == 1){
				SaveNumber[times - 1] = SaveNumber[times-1] * SaveNumber[times];
			}
			else if (SaveOperater == 2){
				SaveNumber[times - 1] = SaveNumber[times-1] / SaveNumber[times];
			}
			else{
				times++;
			}
			SaveOperater = 3;
			PM[SaveTimes] = 3;
			SaveTimes++;
			save = "";
			p2.removeAll();
			p2.add(L, BorderLayout.NORTH);
			p2.add(p1, BorderLayout.SOUTH);
			p2.validate();
		}
		//the operater of "-"
		if(e.getSource() == button[13]){
			L.setText(L.getText() + "-");
			SaveNumber[times] = Double.parseDouble(save);
			if(SaveOperater == 1){
				SaveNumber[times - 1] = SaveNumber[times-1] * SaveNumber[times];
			}
			else if (SaveOperater == 2){
				SaveNumber[times - 1] = SaveNumber[times-1] / SaveNumber[times];
			}
			else{
				times++;
			}
			SaveOperater = 4;
			PM[SaveTimes] = 4;
			SaveTimes++;
			save = "";
			p2.removeAll();
			p2.add(L, BorderLayout.NORTH);
			p2.add(p1, BorderLayout.SOUTH);
			p2.validate();
		}
		//the operater of "."
		if(e.getSource() == button[14]){
			L.setText(L.getText() + ".");
			save = save + ".";
			p2.removeAll();
			p2.add(L, BorderLayout.NORTH);
			p2.add(p1, BorderLayout.SOUTH);
			p2.validate();
		}
		//the operater of "="
		if(e.getSource() == button[15]){
			L.setText(L.getText() + "=");
			SaveNumber[times] = Double.parseDouble(save);
			save = "";
			if(SaveOperater == 1){
				SaveNumber[times - 1] = SaveNumber[times-1] * SaveNumber[times];
			}
			else if (SaveOperater == 2){
				SaveNumber[times - 1] = SaveNumber[times-1] / SaveNumber[times];
			}
			double sum = SaveNumber[0];
			for(int i = 1;i<=SaveTimes;i++){
				if (PM[i-1]==3){
					sum = sum + SaveNumber[i];
				}
				else if (PM[i-1]==4){
					sum = sum - SaveNumber[i];
					
				}
			}
			L.setText("" + sum);
			p2.removeAll();
			p2.add(L, BorderLayout.NORTH);
			p2.add(p1, BorderLayout.SOUTH);
			p2.validate();
		}
		
		
		
	}
	
	
	
		
}

