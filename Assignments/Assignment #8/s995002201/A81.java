package a8.s995002201;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class A81 
{
	public static void main( String[] args )
	{
		FrameWork frameWork = new FrameWork();
		
		frameWork.pack(); 
		frameWork.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameWork.setSize(500,500);
		frameWork.setVisible(true);
	}
}
