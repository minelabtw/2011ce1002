package a8.s995002201;

import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class FrameWork extends JFrame
{
	private JButton zero;
	private JButton one;
	private JButton two;
	private JButton three;
	private JButton four;
	private JButton five;
	private JButton six;
	private JButton seven;
	private JButton eight;
	private JButton nine;//0~9
	private JButton plus;
	private JButton minus;
	private JButton mult;
	private JButton div;
	private JButton equal;
	private JButton radical;//+-*/=根號
	JLabel label1 = new JLabel();
	Container container;
	private String word = "";
	public FrameWork()
	{
		super("計算機");
		int m = 5,n = 5;
		this.setLayout(new GridLayout(m,n,0,0));//按鈕大小
		
		/*從0開始分別對各個按鈕作設定*/
		zero = new JButton("0");
		add(zero);
		zero.addActionListener(new ActionListener() //ActionListener 裡面用JLbabel 顯示出使用者按的是哪個按鈕 用+=累加方式才不會被蓋掉
		{
            public void actionPerformed(ActionEvent e)
            {
                word += "0";
                label1.setText("<html>"+word+"<html>");
            }
        });      
		one = new JButton("1");
		add(one);
		one.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e)
            {
            	word += "1";
                label1.setText("<html>"+word+"<html>");
            }
        }); 
		two = new JButton("2");
		add(two);
		two.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e)
            {
            	word += "2";
                label1.setText("<html>"+word+"<html>");
            }
        }); 
		three = new JButton("3");
		add(three);
		three.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e)
            {
            	word += "3";
                label1.setText("<html>"+word+"<html>");
            }
        }); 
		four = new JButton("4");
		add(four);
		four.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e)
            {
            	word += "4";
                label1.setText("<html>"+word+"<html>");
            }
        }); 
		five = new JButton("5");
		add(five);
		five.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e)
            {
            	word += "5";
                label1.setText("<html>"+word+"<html>");
            }
        }); 
		six = new JButton("6");
		add(six);
		six.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e)
            {
            	word += "6";
                label1.setText("<html>"+word+"<html>");
            }
        }); 
		seven = new JButton("7");
		add(seven);
		seven.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e)
            {
            	word += "7";
                label1.setText("<html>"+word+"<html>");
            }
        }); 
		eight = new JButton("8");
		add(eight);
		eight.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e)
            {
            	word += "8";
                label1.setText("<html>"+word+"<html>");
            }
        }); 
		nine = new JButton("9");
		add(nine);
		nine.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e)
            {
            	word += "9";
                label1.setText("<html>"+word+"<html>");
            }
        }); 
		plus = new JButton("+");
		add(plus);
		plus.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e)
            {
            	word += "+";
                label1.setText("<html>"+word+"<html>");
            }
        }); 
		minus = new JButton("-");
		add(minus);
		minus.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e)
            {
            	word += "-";
                label1.setText("<html>"+word+"<html>");
            }
        }); 
		mult = new JButton("*");
		add(mult);
		mult.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e)
            {
            	word += "*";
                label1.setText("<html>"+word+"<html>");
            }
        }); 
		div = new JButton("/");
		add(div);
		div.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e)
            {
            	word += "/";
                label1.setText("<html>"+word+"<html>");
            }
        }); 
		radical = new JButton("√");
		add(radical);
		radical.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e)
            {
            	word += "√";
                label1.setText("<html>"+word+"<html>");
            }
        }); 
		equal = new JButton("=");
		add(equal);
		equal.addActionListener(new ActionListener() 
		{
            public void actionPerformed(ActionEvent e)
            {
            	word += "=";
                label1.setText("<html>"+word+"<html>");
            }
        });
		Border border = LineBorder.createGrayLineBorder();//設定邊界
	    label1.setForeground(Color.BLUE);//設定顏色
	    label1.setHorizontalTextPosition(JLabel.LEFT);//設定位置
	    label1.setVerticalTextPosition(JLabel.BOTTOM);
	    label1.setBorder(border);
	    add(label1);//添加Label
	}
}
