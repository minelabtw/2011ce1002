package a8.s100502012;

import javax.swing.*;
import javax.swing.border.*; 
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrameWork extends JFrame implements ActionListener{
	JPanel p1=new JPanel();
	JPanel p2=new JPanel();
	JLabel number;
	String print="";
	JButton[] b =new JButton[16];
	Font largeFont=new Font("Mesquite Std",Font.BOLD,35);

	public FrameWork(){
		
		//p1 panel,which display the output
		p1.setLayout(new GridLayout(1,1));
		p1.setBackground(Color.WHITE);
		number=new JLabel("");// new a jlabel which is a empty blank as the default label  
		number.setFont(largeFont);
		p1.add(number);
		
		
		//p2 panel,which is the set of buttons
	    p2.setLayout(new GridLayout(4,4,1,1));
	    for(int a=0 ; a<=15 ; a++){
	    	String s="";
	    	
	    	if (a==9)
	    		s+=0;
	    	else if (a==10)
	    	    s+=".";
	    	else if (a==11)
	    		s+="=";
	    	else if (a==12)
	    		s+="+";
	    	else if (a==13)
	    		s+="-";
	    	else if (a==14)
	    		s+="��";
	    	else if (a==15)
	    		s+="��";
	    	else
	    	    s+=a+1;
	    	
	    	b[a]=new JButton(s);// store 16 buttons by array[16]
	    	b[a].setFont(largeFont);
	    	p2.add(b[a]);
	    	
	    	b[a].addActionListener(this);
	    }
	    
	    
	    //post the two panels to the frame by gridlayout
	    setLayout(new GridLayout(2,1));
	    add(p1);
	    add(p2);  
	}
	
	public void actionPerformed(ActionEvent e){
		for(int a=0 ; a<16 ; a++){
		    if (e.getSource()==b[a]){
			    if (a==9)
				    print+="0";
			    else if (a==10)
		    	    print+=".";
			    else if (a==11)
		    		print+="=";
			    else if (a==12)
		    		print+="+";
			    else if (a==13)
		    		print+="-";
			    else if (a==14)
		    		print+="��";
			    else if (a==15)
		    		print+="��";
			    else
			        print+=a+1;
			        number.setText(print);// reset the value of number to cover the default value
		    }   
		}
	}
}
