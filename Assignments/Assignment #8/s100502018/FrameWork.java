package a8.s100502018;

import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener {
	String result = ""; //store the input
	JLabel display = new JLabel(result); //show the result
	JButton button0 = new JButton("0"); //Buttonsea
	JButton button1 = new JButton("1");
	JButton button2 = new JButton("2");
	JButton button3 = new JButton("3");
	JButton button4 = new JButton("4");
	JButton button5 = new JButton("5");
	JButton button6 = new JButton("6");
	JButton button7 = new JButton("7");
	JButton button8 = new JButton("8");
	JButton button9 = new JButton("9");
	JButton buttondot = new JButton(".");
	JButton buttonplu = new JButton("+");
	JButton buttonmin = new JButton("-");
	JButton buttonmul = new JButton("*");
	JButton buttondiv = new JButton("/");
	JButton buttonequ = new JButton("=");
	JButton buttonrot = new JButton("��");
	JButton buttonper = new JButton("%");
	JButton buttonbac = new JButton("��");
	JButton buttoncle = new JButton("C");
	Font buttonfont = new Font("SansSerif", Font.BOLD, 12);
	Font displayfont = new Font("SansSerif", Font.BOLD, 30);
	Border partborder = new LineBorder(Color.BLACK, 2);

	public FrameWork() {
		JPanel buttons = new JPanel(new GridLayout(4, 5, 5, 5));
		buttons.add(button7);
		buttons.add(button8);
		buttons.add(button9);
		buttons.add(buttondiv);
		buttons.add(buttoncle);
		buttons.add(button4);
		buttons.add(button5);
		buttons.add(button6);
		buttons.add(buttonmul);
		buttons.add(buttonbac);
		buttons.add(button1);
		buttons.add(button2);
		buttons.add(button3);
		buttons.add(buttonmin);
		buttons.add(buttonper);
		buttons.add(button0);
		buttons.add(buttondot);
		buttons.add(buttonequ);
		buttons.add(buttonplu);
		buttons.add(buttonrot);
		button0.addActionListener(this);
		button1.addActionListener(this);
		button2.addActionListener(this);
		button3.addActionListener(this);
		button4.addActionListener(this);
		button5.addActionListener(this);
		button6.addActionListener(this);
		button7.addActionListener(this);
		button8.addActionListener(this);
		button9.addActionListener(this);
		buttondot.addActionListener(this);
		buttonplu.addActionListener(this);
		buttonmin.addActionListener(this);
		buttonmul.addActionListener(this);
		buttondiv.addActionListener(this);
		buttonequ.addActionListener(this);
		buttonrot.addActionListener(this);
		buttonper.addActionListener(this);
		buttonbac.addActionListener(this);
		buttoncle.addActionListener(this);
		buttons.setBorder(partborder);
		JPanel total = new JPanel(new GridLayout(2, 1, 5, 5));
		display.setFont(displayfont);
		total.add(display);
		total.add(buttons);
		this.add(total);
	}

	public void actionPerformed(ActionEvent ex) { //after pressing the button
		if (ex.getSource() == button0) {
			result += "0";
		} else if (ex.getSource() == button1) {
			result += "1";
		} else if (ex.getSource() == button2) {
			result += "2";
		} else if (ex.getSource() == button3) {
			result += "3";
		} else if (ex.getSource() == button4) {
			result += "4";
		} else if (ex.getSource() == button5) {
			result += "5";
		} else if (ex.getSource() == button6) {
			result += "6";
		} else if (ex.getSource() == button7) {
			result += "7";
		} else if (ex.getSource() == button8) {
			result += "8";
		} else if (ex.getSource() == button9) {
			result += "9";
		} else if (ex.getSource() == buttondot) {
			result += ".";
		} else if (ex.getSource() == buttonplu) {
			result += "+";
		} else if (ex.getSource() == buttonmin) {
			result += "-";
		} else if (ex.getSource() == buttonmul) {
			result += "*";
		} else if (ex.getSource() == buttondiv) {
			result += "/";
		} else if (ex.getSource() == buttonequ) {
			result = result;
		} else if (ex.getSource() == buttonrot) {
			result += "��";
		} else if (ex.getSource() == buttonper) {
			result += "%";
		} else if (ex.getSource() == buttonbac) {
			if (result.length() == 0) {
			} else
				result = result.substring(0, result.length() - 1);
		} else if (ex.getSource() == buttoncle) {
			result = "";
		}
		display.setText(result);
	}
}
