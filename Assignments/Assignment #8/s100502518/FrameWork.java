package a8.s100502518;

import java.awt.Button;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
public class FrameWork extends JFrame implements ActionListener{
	JLabel screen;
	JButton []button=new JButton[14];
	JPanel panel;
	private String word="";
	Font font1=new Font("Serif",0,40);
	
	public FrameWork()
	{
		for(int a=0;a<=9;a++)//把數字放到按鈕上
		{
			String aString=""+a;
			button[a]=new JButton(aString);
		}
		
		button[10]=new JButton("+");//再放+-*/
		button[11]=new JButton("-");
		button[12]=new JButton("*");
		button[13]=new JButton("/");
		
		panel=new JPanel(new GridLayout(2,7));
		
		for(int a=0;a<=13;a++)
		{
			panel.add(button[a]);
		}
		
		setLayout(new GridLayout(2,1));//合成一個計算機樣式
		add(screen=new JLabel(""));
		add(panel);
		
		for(int a=0;a<=13;a++)//設定觸發按鍵
		{
			button[a].addActionListener(this);
		}
	}
	
	public void actionPerformed(ActionEvent e)//設定按鍵內容
	{
		for(int a=0;a<=9;a++)
		{
			if(e.getSource()==button[a])
			{
				String aString=""+a;
				setword(aString);
				screen.setText(word);
				screen.setFont(font1);
				setLayout(new GridLayout(2,1));
				add(screen);
				add(panel);
			}
		}
		
		if(e.getSource()==button[10])
		{
			String aString="+";
			setword(aString);
			screen.setText(word);
			screen.setFont(font1);
			setLayout(new GridLayout(2,1));
			add(screen);
			add(panel);
		}
		
		if(e.getSource()==button[11])
		{
			String aString="-";
			setword(aString);
			screen.setText(word);
			screen.setFont(font1);
			setLayout(new GridLayout(2,1));
			add(screen);
			add(panel);
		}
		
		if(e.getSource()==button[12])
		{
			String aString="*";
			setword(aString);
			screen.setText(word);
			screen.setFont(font1);
			setLayout(new GridLayout(2,1));
			add(screen);
			add(panel);
		}
		
		if(e.getSource()==button[13])
		{
			String aString="/";
			setword(aString);
			screen.setText(word);
			screen.setFont(font1);
			setLayout(new GridLayout(2,1));
			add(screen);
			add(panel);
		}
	}
	
	public void setword(String a)
	{	
		word=word+a;
	}
}
