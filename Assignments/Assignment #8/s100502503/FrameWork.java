package a8.s100502503;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.*;

import javax.swing.*;


public class FrameWork	extends JFrame implements ActionListener
{
	//declare the buttons, text field, and some font and string type
	public JButton[] buttonArray = new JButton[18];
	public JTextField showText = new JTextField("");
	public String text = "";
	public Font font1 = new Font("Serif", Font.BOLD, 30);
	
	public FrameWork()
	{
		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(3, 6));
		showText.setFont(font1);
		
		//the number button
		for(int i = 0; i < 10; i++)
		{
			buttonArray[i] = new JButton("" + i);
		}
		
		//the button which are not numbers
		buttonArray[10] = new JButton("+");
		buttonArray[11] = new JButton("-");
		buttonArray[12] = new JButton("*");
		buttonArray[13] = new JButton("/");
		buttonArray[14] = new JButton("=");
		buttonArray[15] = new JButton("��");
		buttonArray[16] = new JButton(".");
		buttonArray[17] = new JButton("%");
		
		//set the panel and buttons
		for(int i = 0; i < 18; i++)
		{
			p1.add(buttonArray[i]);
		}
		
		JPanel p2 = new JPanel(new BorderLayout());
		
		p2.add(showText, BorderLayout.NORTH);
		p2.add(p1, BorderLayout.SOUTH);
		
		add(p2);
		
		for(int i = 0; i < 18; i++)
		{
			
			buttonArray[i].addActionListener(this);
		}
		
		
	}
	//while user click the button, output the text on the button to the screen
	public void actionPerformed(ActionEvent e)
	{
		for(int i = 0; i < 18; i++)
		{
			if(e.getSource() == buttonArray[i])
			{
				text += buttonArray[i].getText();
				showText.setText(text);
			}
		}
	}
}
