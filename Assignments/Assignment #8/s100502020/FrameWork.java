package a8.s100502020;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener{
	private JButton[] button = new JButton[16];	
	private String input = "";
	private JPanel P1 = new JPanel();
	private JPanel P2 = new JPanel();
	
	private JLabel label = new JLabel();
	private JLabel cleanup = new JLabel();
	
	
	public FrameWork()
	{
		setLayout(new GridLayout(2,1));//set frame layout
		this.add(P1);
		this.add(P2);
		
		button[0]= new JButton("0");//set the image on the button
		button[1]= new JButton("1");
		button[2]= new JButton("2");
		button[3]= new JButton("3");
		button[4]= new JButton("4");
		button[5]= new JButton("5");
		button[6]= new JButton("6");
		button[7]= new JButton("7");
		button[8]= new JButton("8");
		button[9]= new JButton("9");
		button[10]= new JButton("+");
		button[11]= new JButton("-");
		button[12]= new JButton("*");
		button[13]= new JButton("/");
		button[14]= new JButton(".");
		button[15]= new JButton("AC");
		
		Font F = new Font("TimesRoman",Font.BOLD, 60);//set the word form
		label.setFont(F);
		
		
		P2.setLayout(new GridLayout(4,4,1,1));//4x4
		P2.setBackground(Color.WHITE);
		P2.add(button[7]);
		P2.add(button[8]);
		P2.add(button[9]);
		P2.add(button[10]);
		P2.add(button[4]);
		P2.add(button[5]);
		P2.add(button[6]);
		P2.add(button[11]);
		P2.add(button[1]);
		P2.add(button[2]);
		P2.add(button[3]);
		P2.add(button[12]);
		P2.add(button[15]);
		P2.add(button[0]);
		P2.add(button[14]);
		P2.add(button[13]);
		
		
		
		P1.setLayout(new FlowLayout(FlowLayout.LEFT));//set the output from left to right
		P1.setBackground(Color.WHITE);//background white
		P1.add(label);//add a null label
		
		for(int a = 0 ; a<=15 ; a++)//set the react of press the button
		{
			button[a].addActionListener(this);
		}
				
	}
	
	public void actionPerformed(ActionEvent e)//set the react of press the button
	{
		for(int a = 0 ; a<=15 ; a++)
		{
			if(e.getSource() == button[a]){
			
				input = input + button[a].getText();
				label.setText(input);
			}			
		}	
		
		
	}
}
