//Please implement an easy calculator GUI and meet the following requirements:
//
//class FrameWork:
//Use JButton to create the keyboard, and JPanel to create the screen.
//Implement interface ActionListener and method ActionPerformed for processing the button click event.
//Show the current input STRING on the screen, and write a method to handle the string.
//class A81: main method to initial the framework
package a8.s100502506;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class FrameWork extends JFrame implements ActionListener,KeyListener		//GUI
{
	private JButton[] numButtons=new JButton[10];								//10 number button
	private JButton[] operButtons=new JButton[10];								//10 operator button
	private JButton[] sysButtons =new JButton[2];								//AC DEL
	private JTextField disField =new JTextField();								//display
	private JPanel[] disPanels =new JPanel[11];
	private Font font1=new Font("Serif",Font.BOLD,25);
	private Font font2=new Font("Serif",Font.BOLD,35);
	private String temp=new String();
	public FrameWork()
	{
		operButtons[0]=new JButton("(");
		operButtons[1]=new JButton(")");
		operButtons[2]=new JButton("^");
		operButtons[3]=new JButton("1/x");
		operButtons[4]=new JButton("*");
		operButtons[5]=new JButton("/");
		operButtons[6]=new JButton("+");
		operButtons[7]=new JButton("-");
		operButtons[8]=new JButton("=");
		operButtons[9]=new JButton(".");
		numButtons[0]=new JButton("7");
		numButtons[1]=new JButton("8");
		numButtons[2]=new JButton("9");
		numButtons[3]=new JButton("4");
		numButtons[4]=new JButton("5");
		numButtons[5]=new JButton("6");
		numButtons[6]=new JButton("1");
		numButtons[7]=new JButton("2");
		numButtons[8]=new JButton("3");
		numButtons[9]=new JButton("0");
		sysButtons[0]=new JButton("AC");
		sysButtons[1]=new JButton("DEL");
		for(int i=0;i<11;i++)
			disPanels[i]=new JPanel(new BorderLayout());
		
		disField.setFont(font2);
		for(int i=0;i<10;i++)
		{
			operButtons[i].setFont(font1);
			numButtons[i].setFont(font1);
		}
		sysButtons[0].setFont(font1);
		sysButtons[1].setFont(font1);
		
		disPanels[0].add(disField);//textfield
		disPanels[1].setLayout(new GridLayout(2,4,2,2));
		for(int i=0;i<8;i++)//operators(8)
			disPanels[1].add(operButtons[i]);
		
		
		
		disPanels[2].setLayout(new GridLayout(2,4,2,2));
		
		disPanels[2].add(numButtons[0]);//7
		disPanels[2].add(numButtons[1]);//8
		disPanels[2].add(numButtons[2]);//9
		disPanels[2].add(sysButtons[0]);//AC
		disPanels[2].add(numButtons[3]);//4
		disPanels[2].add(numButtons[4]);//5
		disPanels[2].add(numButtons[5]);//6
		disPanels[2].add(sysButtons[1]);//DEL
		
		
		
		disPanels[3].setLayout(new GridLayout(1,2,2,2));
		disPanels[3].add(numButtons[6]);//1
		disPanels[3].add(numButtons[7]);//2
		disPanels[4].add(numButtons[9]);//0
		
		disPanels[7].add(disPanels[3]);
		disPanels[7].add(disPanels[4]);
		disPanels[7].setLayout(new GridLayout(2,1,2,2));
		
		
		disPanels[5].add(numButtons[8]);//3
		disPanels[5].add(operButtons[9]);//.
		disPanels[5].setLayout(new GridLayout(2,1,2,2));
		disPanels[6].add(operButtons[8]);
		
		disPanels[8].add(disPanels[5]);
		disPanels[8].add(disPanels[6]);
		disPanels[8].setLayout(new GridLayout(1,2,2,2));
		
		disPanels[9].add(disPanels[7]);
		disPanels[9].add(disPanels[8]);
		disPanels[9].setLayout(new GridLayout(1,2,2,2));
		
		disPanels[10].add(disPanels[0]);
		disPanels[10].add(disPanels[1]);
		disPanels[10].add(disPanels[2]);
		disPanels[10].add(disPanels[9]);
		disPanels[10].setLayout(new GridLayout(4,1,2,2));
		add(disPanels[10]);
		
		
		for(int i=0;i<10;i++)					//add action listener to button
		{
			operButtons[i].addActionListener(this);
			numButtons[i].addActionListener(this);
			
		}
		sysButtons[0].addActionListener(this);
		sysButtons[1].addActionListener(this);
		disField.addKeyListener(this);			//add key listener to Text field
		
		disField.setEditable(false);			//can't input from the outside
	}
	//button
	public void actionPerformed(ActionEvent ev) //action
	{
		if(ev.getSource()==operButtons[0])//(
		{
			reviseString("(");
		}
		else if(ev.getSource()==operButtons[1])//)
		{
			reviseString(")");
		}
		else if(ev.getSource()==operButtons[2])//^
		{
			reviseString("^");
		}
		else if(ev.getSource()==operButtons[3])//1/x
		{
			if(!hasoper(temp))
				temp=Reciprocal(temp);
		}
		else if(ev.getSource()==operButtons[4])//*
		{
			reviseString("*");
		}
		else if(ev.getSource()==operButtons[5])// /
		{
			reviseString("/");
		}
		else if(ev.getSource()==operButtons[6])// +
		{
			reviseString("+");
		}
		else if(ev.getSource()==operButtons[7])// -
		{
			reviseString("-");
		}
		else if(ev.getSource()==operButtons[8])// =
		{
			reviseString("=");
		}
		//numbers
		else if(ev.getSource()==operButtons[9])// .
		{
			reviseString(".");
		}
		else if(ev.getSource()==numButtons[0])// 7
		{
			reviseString("7");
		}
		else if(ev.getSource()==numButtons[1])// 8
		{
			reviseString("8");
		}
		else if(ev.getSource()==numButtons[2])// 9
		{
			reviseString("9");
		}
		else if(ev.getSource()==numButtons[3])//4
		{
			reviseString("4");
		}
		else if(ev.getSource()==numButtons[4])//5
		{
			reviseString("5");
		}
		else if(ev.getSource()==numButtons[5])//6
		{
			reviseString("6");
		}
		else if(ev.getSource()==numButtons[6])//1
		{
			reviseString("1");
		}
		else if(ev.getSource()==numButtons[7])//2
		{
			reviseString("2");
		}
		else if(ev.getSource()==numButtons[8])//3
		{
			reviseString("3");
		}
		else if(ev.getSource()==numButtons[9])//0
		{
			reviseString("0");
		}
		//AC
		else if(ev.getSource()==sysButtons[0])
		{
			temp="";
		}
		//DEL
		else if(ev.getSource()==sysButtons[1])
		{
			if(!temp.isEmpty())
			{
				temp=temp.substring(0,temp.length()-1);
			}
		}
		disField.setText(temp);
	}
	
	//keyboard
	public void keyReleased(KeyEvent kev) 
	{
	}
	public void keyTyped(KeyEvent kev) 
	{
	}
	public void keyPressed(KeyEvent kev) 
	{
		//operator
		if(kev.getKeyCode()==106)//*
		{
			reviseString("*");
		}
		else if(kev.getKeyCode()==111)// /
		{
			reviseString("/");
		}
		else if(kev.getKeyCode()==107)//+
		{
			reviseString("+");
		}
		else if(kev.getKeyCode()==109)//-
		{
			reviseString("-");
		}
		else if(kev.getKeyCode()==10)//=
		{
			reviseString("=");
		}
		//numbers
		else if(kev.getKeyCode()==110)//.
		{
			reviseString(".");
		}
		else if(kev.getKeyCode()==103)//7
		{
			reviseString("7");
		}
		else if(kev.getKeyCode()==104)//8
		{
			reviseString("8");
		}
		else if(kev.getKeyCode()==105)//9
		{
			reviseString("9");
		}
		else if(kev.getKeyCode()==100)//4
		{
			reviseString("4");
		}
		else if(kev.getKeyCode()==101)//5
		{
			reviseString("5");
		}
		else if(kev.getKeyCode()==102)//6
		{
			reviseString("6");
		}
		else if(kev.getKeyCode()==97)//1
		{
			reviseString("1");
		}
		else if(kev.getKeyCode()==98)//2
		{
			reviseString("2");
		}
		else if(kev.getKeyCode()==99)//3
		{
			reviseString("3");
		}
		else if(kev.getKeyCode()==96)//0
		{
			reviseString("0");
		}
		//AC
		else if(kev.getKeyCode()==32)
		{
			temp="";
		}
		//DEL
		else if(kev.getKeyCode()==8)
		{
			if(!temp.isEmpty())
			{
				temp=temp.substring(0,temp.length()-1);
			}
		}
		disField.setText(temp);
	}
	public void reviseString(String input)//handle string
	{
		temp=temp+input;
	}
	public String Reciprocal(String input)									//�˼�
	{
		String tempString=new String();
		tempString=String.valueOf(1/Double.parseDouble(input));
		return tempString;
	}
	public boolean hasoper(String input)									//check if has operator(string)
	{
		boolean check=false;
		for(int i=0;i<input.length();i++)
		{
			if(input.charAt(i)>'9'||input.charAt(i)<'.'||input.charAt(i)=='/')
			{
				if(!Character.isLetter(input.charAt(i)))
				{
					check=true;
				}
			}
		}
		return check;
	}
	public boolean hasoper(char input)									//check if has operator(char)
	{
		boolean check=false;
		
		if(input>'9'||input<'.'||input=='/')
		{
			if(!Character.isLetter(input))
			{
				check=true;
			}
			
		}
		return check;
	}
}
