//Please implement an easy calculator GUI and meet the following requirements:
//class FrameWork:
//Use JButton to create the keyboard, and JPanel to create the screen.
//Implement interface ActionListener and method ActionPerformed for processing the button click event.
//Show the current input STRING on the screen, and write a method to handle the string.

//class A81: main method to initial the framework
package a8.s100502506;

import javax.swing.JFrame;

public class A81 
{
	public static void main(String argc[])//main
	{
		FrameWork testFrameWork =new FrameWork();//initial the framework
		testFrameWork.setVisible(true);//set visible
		testFrameWork.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		testFrameWork.setSize(600, 800);//set size
		testFrameWork.setLocation(500,0);//set location
		testFrameWork.setResizable(false);//can't resize
	}
	
	
}
