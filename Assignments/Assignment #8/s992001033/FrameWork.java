package a8.s992001033;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
public class FrameWork extends JFrame 
{	
	static String input = " ";
	static JLabel display = new JLabel(input);
	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();
	JPanel p3 = new JPanel();
	JButton b1 =new JButton();
	JButton b2 =new JButton();
	JButton b3 =new JButton();
	JButton b4 =new JButton();
	JButton b5 =new JButton();
	JButton b6 =new JButton();
	JButton b7 =new JButton();
	JButton b8 =new JButton();
	JButton b9 =new JButton();
	JButton b10 =new JButton();
	JButton b11 =new JButton();
	JButton b12 =new JButton();
	JButton b13 =new JButton();
	JButton b14 =new JButton();
	JButton b15 =new JButton();
	Font largeFont = new Font("",Font.LAYOUT_NO_LIMIT_CONTEXT, 50);
	
	public FrameWork()
	{
		
		//p1放按鈕,p3放label,p2則放p1和p2
		p1.setLayout(new GridLayout(3,5));
		p2.setLayout(new BorderLayout());
		b1.setText("0");
		b2.setText("1");
		b3.setText("2");
		b4.setText("3");
		b5.setText("4");
		b6.setText("5");
		b7.setText("6");
		b8.setText("7");
		b9.setText("8");
		b10.setText("9");
		b11.setText("+");
		b12.setText("-");
		b13.setText("*");
		b14.setText("/");
		b15.setText("=");
		p1.add(b1);
		p1.add(b2);
		p1.add(b3);
		p1.add(b4);
		p1.add(b5);
		p1.add(b6);
		p1.add(b7);
		p1.add(b8);
		p1.add(b9);
		p1.add(b10);
		p1.add(b11);
		p1.add(b12);
		p1.add(b13);
		p1.add(b14);
		p1.add(b15);
		display.setFont(largeFont);
		p3.add(display);
		p2.add(p3,BorderLayout.NORTH);
		p2.add(p1,BorderLayout.CENTER);
		add(p2);
		
		//listener和按鈕連結
		b1ListenerClass listener1 = new b1ListenerClass();
		b1.addActionListener(listener1);
		b2ListenerClass listener2 = new b2ListenerClass();
		b2.addActionListener(listener2);
		b3ListenerClass listener3 = new b3ListenerClass();
		b3.addActionListener(listener3);
		b4ListenerClass listener4 = new b4ListenerClass();
		b4.addActionListener(listener4);
		b5ListenerClass listener5 = new b5ListenerClass();
		b5.addActionListener(listener5);
		b6ListenerClass listener6 = new b6ListenerClass();
		b6.addActionListener(listener6);
		b7ListenerClass listener7 = new b7ListenerClass();
		b7.addActionListener(listener7);
		b8ListenerClass listener8 = new b8ListenerClass();
		b8.addActionListener(listener8);
		b9ListenerClass listener9 = new b9ListenerClass();
		b9.addActionListener(listener9);
		b10ListenerClass listener10 = new b10ListenerClass();
		b10.addActionListener(listener10);
		b11ListenerClass listener11 = new b11ListenerClass();
		b11.addActionListener(listener11);
		b12ListenerClass listener12 = new b12ListenerClass();
		b12.addActionListener(listener12);
		b13ListenerClass listener13 = new b13ListenerClass();
		b13.addActionListener(listener13);
		b14ListenerClass listener14 = new b14ListenerClass();
		b14.addActionListener(listener14);
		b15ListenerClass listener15 = new b15ListenerClass();
		b15.addActionListener(listener15);
		
	}
	public static void changeInput(String change)//控制imput的method
	{
		input += change;
		display.setText(input);
	}
}
//actionlistener
class b1ListenerClass implements ActionListener 
{
	public void actionPerformed(ActionEvent e)
	{
		FrameWork.changeInput("0");
	}
}
class b2ListenerClass implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		FrameWork.changeInput("1");
	}
}
class b3ListenerClass implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		FrameWork.changeInput("2");
	}
}
class b4ListenerClass implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		FrameWork.changeInput("3");
	}
}
class b5ListenerClass implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		FrameWork.changeInput("4");
	}
}
class b6ListenerClass implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		FrameWork.changeInput("5");
	}
}
class b7ListenerClass implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		FrameWork.changeInput("6");
	}
}
class b8ListenerClass implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		FrameWork.changeInput("7");
	}
}
class b9ListenerClass implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		FrameWork.changeInput("8");
	}
}
class b10ListenerClass implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		FrameWork.changeInput("9");
	}
}
class b11ListenerClass implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		FrameWork.changeInput("+");
	}
}
class b12ListenerClass implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		FrameWork.changeInput("-");
	}
}
class b13ListenerClass implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		FrameWork.changeInput("*");
	}
}
class b14ListenerClass implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		FrameWork.changeInput("/");
	}
}
class b15ListenerClass implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		FrameWork.changeInput("=");
	}
}

