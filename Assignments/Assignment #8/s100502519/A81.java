package a8.s100502519;
import javax.swing.JFrame;

public class A81 {
	public static void main(String [] args){
		
		FrameWork f = new FrameWork();			//建一個frame
		f.setTitle("Asignment #7");			//標頭
		f.setSize(400, 600);			//長寬
		f.setLocationRelativeTo(null);			//相對位置 中央
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);			//叉叉
		f.setVisible(true);			//設成visible
		
	}
}
