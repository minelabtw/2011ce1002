package a8.s100502519;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener{
	
	String write = new String();			//存輸入
	Color color1 = new Color(21,21,94);			//一些顏色
	Color color2 = new Color(2,83,9);
	Color color3 = new Color(234,101,21);
	Color color4 = new Color(233,238,13);
	Font font = new Font("Monospaced",Font.BOLD,26);			//字體
	JLabel l_screen = new JLabel();			//顯示畫面
	JPanel p_kb = new JPanel();			//鍵盤
	JButton b_0 = new JButton("0");			//一些按鈕
	JButton b_1 = new JButton("1");
	JButton b_2 = new JButton("2");
	JButton b_3 = new JButton("3");
	JButton b_4 = new JButton("4");
	JButton b_5 = new JButton("5");
	JButton b_6 = new JButton("6");
	JButton b_7 = new JButton("7");
	JButton b_8 = new JButton("8");
	JButton b_9 = new JButton("9");
	JButton b_plus = new JButton("+");
	JButton b_minus = new JButton("-");
	JButton b_multiply = new JButton("*");
	JButton b_divide = new JButton("/");
	JButton b_sqrt = new JButton("√");
	JButton b_refresh = new JButton("Refresh");
	
	public FrameWork(){
		
		setLayout(new GridLayout(3,1,0,0));			//主frame用gridLayout
		
		//label 顯示畫面區  的一些美化
		l_screen.setForeground(color3);
		l_screen.setFont(font);
		
		//按鈕的一些設定 美化
		b_0.setForeground(color1);
		b_1.setForeground(color1);
		b_2.setForeground(color1);
		b_3.setForeground(color1);
		b_4.setForeground(color1);
		b_5.setForeground(color1);
		b_6.setForeground(color1);
		b_7.setForeground(color1);
		b_8.setForeground(color1);
		b_9.setForeground(color1);
		b_plus.setForeground(color2);
		b_minus.setForeground(color2);
		b_multiply.setForeground(color2);
		b_divide.setForeground(color2);
		b_sqrt.setForeground(color2);
		b_refresh.setForeground(color4);
		
		b_0.setFont(font);
		b_1.setFont(font);
		b_2.setFont(font);
		b_3.setFont(font);
		b_4.setFont(font);
		b_5.setFont(font);
		b_6.setFont(font);
		b_7.setFont(font);
		b_8.setFont(font);
		b_9.setFont(font);
		b_plus.setFont(font);
		b_minus.setFont(font);
		b_multiply.setFont(font);
		b_divide.setFont(font);
		b_sqrt.setFont(font);
		b_refresh.setFont(font);
		
		//panel 鍵盤區 的一些設定  以GridLayout方式加紐 
		p_kb.setLayout(new GridLayout(3,5,0,0));
		p_kb.add(b_0);
		p_kb.add(b_1);
		p_kb.add(b_2);
		p_kb.add(b_3);
		p_kb.add(b_4);
		p_kb.add(b_5);
		p_kb.add(b_6);
		p_kb.add(b_7);
		p_kb.add(b_8);
		p_kb.add(b_9);
		p_kb.add(b_plus);
		p_kb.add(b_minus);
		p_kb.add(b_multiply);
		p_kb.add(b_divide);
		p_kb.add(b_sqrt);
		
		//把 顯示區   鍵盤區   refresh紐   加到主frame
		add(l_screen);
		add(p_kb);
		add(b_refresh);
		
		//把按鈕們加listener
		b_0.addActionListener(this);
		b_1.addActionListener(this);
		b_2.addActionListener(this);
		b_3.addActionListener(this);
		b_4.addActionListener(this);
		b_5.addActionListener(this);
		b_6.addActionListener(this);
		b_7.addActionListener(this);
		b_8.addActionListener(this);
		b_9.addActionListener(this);
		b_plus.addActionListener(this);
		b_minus.addActionListener(this);
		b_multiply.addActionListener(this);
		b_divide.addActionListener(this);
		b_sqrt.addActionListener(this);
		b_refresh.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e) {
		
		//設定每個鈕按下去要做的事   即把相對應的數字或運算符號 加到 string
		if(e.getSource() == b_0){
			write = write + "0";
		}
		if(e.getSource() == b_1){
			write = write + "1";
		}
		if(e.getSource() == b_2){
			write = write + "2";
		}
		if(e.getSource() == b_3){
			write = write + "3";
		}
		if(e.getSource() == b_4){
			write = write + "4";
		}
		if(e.getSource() == b_5){
			write = write + "5";
		}
		if(e.getSource() == b_6){
			write = write + "6";
		}
		if(e.getSource() == b_7){
			write = write + "7";
		}
		if(e.getSource() == b_8){
			write = write + "8";
		}
		if(e.getSource() == b_9){
			write = write + "9";
		}
		if(e.getSource() == b_plus){
			write = write + "+";
		}
		if(e.getSource() == b_minus){
			write = write + "-";
		}
		if(e.getSource() == b_multiply){
			write = write + "*";
		}
		if(e.getSource() == b_divide){
			write = write + "/";
		}
		if(e.getSource() == b_sqrt){
			write = write + "√";
		}
		if(e.getSource() == b_refresh){
			write = "";			//讓string空掉
		}
		
		l_screen.setText(write);			//把string加到顯示區
	}
}
