package a8.s100502521;

import javax.swing.JFrame;
public class A81 
{
	public static void main(String[] args)
	{ 
		FrameWork demo = new FrameWork();//創造視窗
		demo.setTitle("計算機");
        demo.setSize(550,350);//設定大小
        demo.setLocationRelativeTo(null);
        demo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//設定視窗標題列的關閉按鈕結束程式執行
        demo.setVisible(true);//顯示視窗
        demo.setResizable(false);//設定不可更改大小
    }
}
