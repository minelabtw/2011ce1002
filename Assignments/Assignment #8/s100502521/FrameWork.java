package a8.s100502521;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
public class FrameWork extends JFrame implements ActionListener
{
	private JPanel p1=new JPanel();
	private JButton[] button=new JButton[20];
	private String display="";
	private double temp=0;//計算使用 儲存地一個數字
	private String Cal=new String("");//存存運算按鍵狀態
	private JLabel tempLabel=new JLabel(String.valueOf(temp));
	private JLabel displayLabel=new JLabel(display);
	public FrameWork()
	{
		setLayout(new BorderLayout());//以下視窗設定
		Font myfont=new Font("標楷體",Font.PLAIN,48);
		displayLabel.setFont(myfont);
		add(tempLabel,BorderLayout.NORTH);
		add(displayLabel,BorderLayout.CENTER);
		p1.setLayout(new GridLayout(5,4,10,10));
		for(int i=0;i<10;i++)
		{
			button[i]=new JButton(String.valueOf(i));
		}
		button[10]=new JButton("+");
		button[11]=new JButton("-");
		button[12]=new JButton("*");
		button[13]=new JButton("/");
		button[14]=new JButton("=");
		button[15]=new JButton("<-");
		button[16]=new JButton(".");
		button[17]=new JButton("Clean");
		button[18]=new JButton("√");
		button[19]=new JButton("Exit");
		
		button[18].addActionListener(this);
		p1.add(button[18]);
		button[13].addActionListener(this);
		p1.add(button[13]);
		button[12].addActionListener(this);
		p1.add(button[12]);
		button[19].addActionListener(this);
		p1.add(button[19]);
		for(int i=7;i<=9;i++)
		{
			button[i].addActionListener(this);
			p1.add(button[i]);
		}
		button[11].addActionListener(this);
		p1.add(button[11]);
		for(int i=4;i<=6;i++)
		{
			button[i].addActionListener(this);
			p1.add(button[i]);
		}
		button[10].addActionListener(this);
		p1.add(button[10]);
		for(int i=1;i<=3;i++)
		{
			button[i].addActionListener(this);
			p1.add(button[i]);
		}
		button[15].addActionListener(this);
		p1.add(button[15]);
		button[0].addActionListener(this);
		p1.add(button[0]);
		button[16].addActionListener(this);
		p1.add(button[16]);
		button[14].addActionListener(this);
		p1.add(button[14]);
		button[17].addActionListener(this);
		p1.add(button[17]);
		
		add(p1,BorderLayout.SOUTH);
	}
	public void actionPerformed(ActionEvent e) 
	{
		for(int i=0;i<20;i++)
		{
			if(e.getSource()==button[i])
			{
				modifeDisplay(button[i].getText());//將按鍵上的字串傳入
				displayLabel.setText(display);
			}
		}
	}
	public void modifeDisplay(String a)
	{
		if(Cal!="=")//如果按鍵剛剛是按=算出答案 則下次按鍵時把答案清空
		{
			calculator(a,0.0);
		}
		else
		{
			Double temp2=Double.parseDouble(display);
			display="";
			calculator(a,temp2);
			if(Cal=="=")
			{
				Cal="";
			}
		}
	}
	public void calculator(String a,Double b)
	{
		if(a=="<-")//減一個字
		{
			String temp="";
			for(int i=0;i<display.length()-1;i++)
			{
				temp=temp+display.charAt(i);
			}
			display=temp;
		}
		else if(a=="Clean")//清空
		{
			temp=0;
			display="0";
		}
		else if(a=="Exit")//離開
		{
			System.exit(0);
		}
		else if(a=="+")//+-*/ 相同
		{
			if(Cal=="=")//剛剛按等於 在按+還是把數字加上去
			{
				temp=b;
				tempLabel.setText(String.valueOf(temp));
				Cal=a;
				return;
			}
			if(temp==0)//如果地一個數字沒有東西 直接等於
			{
				temp=Double.parseDouble(display);
				tempLabel.setText(String.valueOf(temp));
				display="";
			}
			else
			{
				if(Cal=="+")//加上去
				{
					temp+=Double.parseDouble(display);
				}
				else if(Cal=="-")//加上去
				{
					temp-=Double.parseDouble(display);
				} 
				else if(Cal=="*")//加上去
				{
					temp*=Double.parseDouble(display);
				} 
				else if(Cal=="/")//加上去
				{
					temp/=Double.parseDouble(display);
				} 
				tempLabel.setText(String.valueOf(temp));
				display="0";
			}
			Cal=a;//設定按鍵狀態
		}
		else if(a=="-")
		{
			if(Cal=="=")
			{
				temp=b;
				tempLabel.setText(String.valueOf(temp));
				Cal=a;
				return;
			}
			if(temp==0)//同+
			{
				temp=Double.parseDouble(display);
				tempLabel.setText(String.valueOf(temp));
				display="";
			}
			else
			{
				if(Cal=="+")//加上去
				{
					temp+=Double.parseDouble(display);
				}
				else if(Cal=="-")//加上去
				{
					temp-=Double.parseDouble(display);
				} 
				else if(Cal=="*")//加上去
				{
					temp*=Double.parseDouble(display);
				} 
				else if(Cal=="/")//加上去
				{
					temp/=Double.parseDouble(display);
				} 
				tempLabel.setText(String.valueOf(temp));
				display="0";
			}
			Cal=a;//設定按鍵狀態
		}
		else if(a=="*")
		{
			if(Cal=="=")
			{
				temp=b;
				tempLabel.setText(String.valueOf(temp));
				Cal=a;
				return;
			}
			if(temp==0)//同+
			{
				temp=Double.parseDouble(display);
				tempLabel.setText(String.valueOf(temp));
				display="";
			}
			else
			{
				if(Cal=="+")//加上去
				{
					temp+=Double.parseDouble(display);
				}
				else if(Cal=="-")//加上去
				{
					temp-=Double.parseDouble(display);
				} 
				else if(Cal=="*")//加上去
				{
					temp*=Double.parseDouble(display);
				} 
				else if(Cal=="/")//加上去
				{
					temp/=Double.parseDouble(display);
				} 
				tempLabel.setText(String.valueOf(temp));
				display="0";
			}
			Cal=a;//設定按鍵狀態
		}
		else if(a=="/")
		{
			if(Cal=="=")
			{
				temp=b;
				tempLabel.setText(String.valueOf(temp));
				Cal=a;
				return;
			}
			if(temp==0)//同+
			{
				temp=Double.parseDouble(display);
				tempLabel.setText(String.valueOf(temp));
				display="";
			}
			else
			{
				if(Cal=="+")//加上去
				{
					temp+=Double.parseDouble(display);
				}
				else if(Cal=="-")//加上去
				{
					temp-=Double.parseDouble(display);
				} 
				else if(Cal=="*")//加上去
				{
					temp*=Double.parseDouble(display);
				} 
				else if(Cal=="/")//加上去
				{
					temp/=Double.parseDouble(display);
				} 
				tempLabel.setText(String.valueOf(temp));
				display="0";
			}
			Cal=a;//設定按鍵狀態
		}
		else if(a=="√")//直接開根號
		{
			Double sqrt=Double.parseDouble(display);
			sqrt=Math.sqrt(sqrt);
			display=String.valueOf(sqrt);
		}
		else if(a=="=")
		{
			if(Cal=="+")//判斷剛剛是按甚麼運算
			{
				Double number=Double.parseDouble(display);
				number=temp+number;
				display=String.valueOf(number);
			}
			else if(Cal=="-")
			{
				Double number=Double.parseDouble(display);
				number=temp-number;
				display=String.valueOf(number);
			}
			else if(Cal=="*")
			{
				Double number=Double.parseDouble(display);
				number=temp+number;
				display=String.valueOf(number);
			}
			else if(Cal=="/")
			{
				Double number=Double.parseDouble(display);
				number=temp/number;
				display=String.valueOf(number);
			}
			else
			{
				display=String.valueOf(temp);
			}
			Cal="=";
			temp=0;//暫存歸零
			tempLabel.setText(String.valueOf(temp));
		}
		else
		{
			display=display+a;
		}

	}
}
