package a8.s100502013;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener{
	private String myinput = ""; //store the input
	private JPanel mypanel = new JPanel(new GridLayout(2,1));
	private JLabel outputpanel = new JLabel();
	private JPanel keyboard = new JPanel(new GridLayout(4,4));
	private Font keyfont = new Font("TimesRoman",Font.BOLD,22);
	private Font outputfont = new Font("TimesRoman",Font.PLAIN,22);
	private JButton[] keybutton = new JButton[16]; //keyboard button array
	private String[] keychar = {"1","2","3","+","4","5","6","-","7","8","9","*","(","0",")","/"}; //keyboard text array
	
	FrameWork(){
		mypanel.setBackground(Color.WHITE);
		for(int i=0;i<16;i++){
			keybutton[i] = new JButton(keychar[i]); //set the button text
			addandevent(keyboard,keybutton[i]); //use the function
		}
		mypanel.add(outputpanel);
		mypanel.add(keyboard);
		add(mypanel);
	}
	
	public void addandevent(JPanel x,JButton y){ //set button onto the keyboard panel and add event
		x.add(y).setFont(keyfont);
		y.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e){ //according to the button being pressed to launch appropriate event
		if(e.getSource()==keybutton[0])
			myinput += keychar[0];
		if(e.getSource()==keybutton[1])
			myinput += keychar[1];
		if(e.getSource()==keybutton[2])
			myinput += keychar[2];
		if(e.getSource()==keybutton[3])
			myinput += keychar[3];
		if(e.getSource()==keybutton[4])
			myinput += keychar[4];
		if(e.getSource()==keybutton[5])
			myinput += keychar[5];
		if(e.getSource()==keybutton[6])
			myinput += keychar[6];
		if(e.getSource()==keybutton[7])
			myinput += keychar[7];
		if(e.getSource()==keybutton[8])
			myinput += keychar[8];
		if(e.getSource()==keybutton[9])
			myinput += keychar[9];
		if(e.getSource()==keybutton[10])
			myinput += keychar[10];
		if(e.getSource()==keybutton[11])
			myinput += keychar[11];
		if(e.getSource()==keybutton[12])
			myinput += keychar[12];
		if(e.getSource()==keybutton[13])
			myinput += keychar[13];
		if(e.getSource()==keybutton[14])
			myinput += keychar[14];
		if(e.getSource()==keybutton[15])
			myinput += keychar[15];
		outputpanel.setFont(outputfont);
		outputpanel.setText(myinput); //output
	}
}
