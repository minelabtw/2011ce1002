package a8.s100502513;

import javax.swing.JFrame;

public class A81 {
	public static void main(String[] args) {
		FrameWork f = new FrameWork();
		f.setTitle("A8 - Easy Calculator");
		f.setSize(500, 400);
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
