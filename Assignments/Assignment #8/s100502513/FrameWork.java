package a8.s100502513;

import javax.swing.*;
import javax.swing.border.LineBorder;

import java.awt.*;
import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener {
	private JButton[] buttons = new JButton[16];
	private String output = "";  //空字串
	private JLabel result;
	public FrameWork() {
		
		JPanel p1 = new JPanel();  //下方按鈕部分
		p1.setLayout(new GridLayout(4, 4));
		this.setNumber();  //設置數字0~9
		p1.add(buttons[7]);  
		p1.add(buttons[8]);
		p1.add(buttons[9]);
		buttons[10] = new JButton("/");
		p1.add(buttons[10]);
		p1.add(buttons[4]);
		p1.add(buttons[5]);
		p1.add(buttons[6]);
		buttons[11] = new JButton("*");
		p1.add(buttons[11]);
		p1.add(buttons[1]);
		p1.add(buttons[2]);
		p1.add(buttons[3]);
		buttons[12] = new JButton("-");
		p1.add(buttons[12]);
		p1.add(buttons[0]);
		buttons[13] = new JButton(".");
		p1.add(buttons[13]);
		buttons[14] = new JButton("+");
		p1.add(buttons[14]);
		buttons[15] = new JButton("MC");
		p1.add(buttons[15]);

		JPanel p2 = new JPanel();  //上面顯示結果
		p2.setBackground(Color.WHITE);
		p2.setBorder(new LineBorder(Color.BLACK, 5));
		result = new JLabel(output);	
		result.setFont(new Font("Cambria", Font.BOLD + Font.ITALIC, 70));
		p2.add(result);
		
		add(p2,BorderLayout.CENTER);
		add(p1,BorderLayout.SOUTH);
		
		for (int i = 0; i < 16; i++) {  //讓按鈕作用
			buttons[i].addActionListener(this);
		}
		
		this.setfontandborder();  //設定字型和邊界
	}

	public void setNumber() {  //產生數字鍵數字
		for (int i = 0; i < 10; i++) {
			buttons[i] = new JButton("" + i);
		}
	}

	public void setfontandborder() {  //設定字型和邊界
		for (int i = 0; i < 16; i++) {
			buttons[i].setFont(new Font("Cambria", Font.BOLD + Font.ITALIC, 50));	
			buttons[i].setBorder(new LineBorder(Color.WHITE, 2));
			buttons[i].setBackground(Color.BLACK);
			buttons[i].setForeground(Color.WHITE);
		}
	}
	
	public void actionPerformed(ActionEvent e) {  //按鈕功能
		for (int i = 0; i < 10; i++) {
			if (e.getSource() == buttons[i])
				getresult(""+i);
		}
		if(e.getSource() == buttons[10])
			getresult("/");
		else if(e.getSource() == buttons[11])
			getresult("*");
		else if(e.getSource() == buttons[12])
			getresult("-");
		else if(e.getSource() == buttons[13])
			getresult(".");
		else if(e.getSource() == buttons[14])
			getresult("+");
		else if(e.getSource() == buttons[15])
			delet();
	}

	public void getresult(String s){  //改變原字串
		output += s;  //增加原字串長度
		result.setText(output);  
	}
	
	public void delet(){  //清除原字串
		output = "";  
		result.setText(output);  
	}
}
