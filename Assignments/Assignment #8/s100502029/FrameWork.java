package a8.s100502029;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

public class FrameWork extends JFrame implements ActionListener {
	// create data member to store keyboard and screen
	private JButton[] jbtKeyboard= new JButton[16];
	private StringBuilder text = new StringBuilder();
	private JLabel jlblScreen = new JLabel();
	
	public FrameWork() {
		// initialize the word that on the every button
		for (int i = 0; i < 10; i++)
			jbtKeyboard[i] = new JButton("" + i);
		jbtKeyboard[10] = new JButton(".");
		jbtKeyboard[11] = new JButton("=");
		jbtKeyboard[12] = new JButton("+");
		jbtKeyboard[13] = new JButton("-");
		jbtKeyboard[14] = new JButton("*");
		jbtKeyboard[15] = new JButton("/");
		
		// create font and lineborder of keyboard and screen to decorate them
		Font fontKeyboard = new Font("TimesRoman", Font.BOLD, 16);
		Font fontScreen = new Font("TimesRoman", Font.BOLD, 16);
		Border borderKeyboard = new LineBorder(Color.CYAN, 1);
		Border borderScreen = new LineBorder(Color.ORANGE, 2);
		
		// create a panel to store the above half keyboard
		JPanel keyboard1 = new JPanel(new GridLayout(2, 4, 0, 1));
		for (int i = 0; i < 8; i++) {
			// set font, lineborder, and background of each button
			jbtKeyboard[i].setFont(fontKeyboard);
			jbtKeyboard[i].setBorder(borderKeyboard);
			jbtKeyboard[i].setBackground(Color.PINK);
			keyboard1.add(jbtKeyboard[i]); // add buttons on the panel
			
		}
		
		// create a panel to store the bottom half keyboard
		JPanel keyboard2 = new JPanel(new GridLayout(2, 4, 0, 1));
		for (int i = 8; i < 16; i++) {
			jbtKeyboard[i].setFont(fontKeyboard);
			jbtKeyboard[i].setBorder(borderKeyboard);
			jbtKeyboard[i].setBackground(Color.PINK);
			keyboard2.add(jbtKeyboard[i]);
		}
		
		// create a panel to store the screen
		JPanel screen = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		jlblScreen.setFont(fontScreen);
		screen.setBorder(borderScreen);
		screen.setBackground(Color.WHITE);
		screen.add(jlblScreen); // add a label that will show the result on the panel
		
		// combine three panels on the frame;
		setLayout(new GridLayout(3, 1));
		add(screen);
		add(keyboard1);
		add(keyboard2);
		
		// add action perform after clicked button
		for (int i = 0; i < 16; i++)
			jbtKeyboard[i].addActionListener(this);
	}
	
	// after clicked keyboard's button, show the word that on the button on screen
	public void actionPerformed(ActionEvent e) {
		for (int i = 0; i < 16; i++) { // check which button user clicked
			if (e.getSource() == jbtKeyboard[i]) {
				text.append(jbtKeyboard[i].getText()); // appends the word that on the button to the text
				jlblScreen.setText(text.toString()); // set the text on the screen
			}
		}
	}
}
