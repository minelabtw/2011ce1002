package a8.s100502002;
import javax.swing.*;
import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import java.awt.event.*;
public class FrameWork extends JFrame
{
	//宣告所有按鈕
	JButton button1 = new JButton(""+1);
	JButton button2 = new JButton(""+2);
	JButton button3 = new JButton(""+3);
	JButton button4 = new JButton(""+4);
	JButton button5 = new JButton(""+5);
	JButton button6 = new JButton(""+6);
	JButton button7 = new JButton(""+7);
	JButton button8 = new JButton(""+8);
	JButton button9 = new JButton(""+9);
	JButton button0 = new JButton(""+0);
	JButton button_equl = new JButton(" = ");
	JButton button_point = new JButton(" . ");
	JButton button_add = new JButton(" + ");
	JButton button_min = new JButton(" - ");
	JButton button_mul = new JButton(" x ");
	JButton button_div = new JButton(" ÷ ");
	JButton button_c = new JButton(" C ");
	JButton button_double = new JButton("00");
	JButton button_per = new JButton("%");
	JButton button_ce = new JButton(" CE ");
	JLabel text=new JLabel();
	//用來顯示的字串，一開始是空的
	String string = new String("");
	public FrameWork()
	{
		JPanel show = new JPanel();
		JPanel number = new JPanel();//NEW兩個PANEL來放東西
		
		show.setLayout(new BorderLayout(1,1));
		text.setBackground(Color.WHITE);
		Border lineBorder = new LineBorder(Color.BLACK,2);
		text.setBorder(lineBorder);//設框線
		
		number.add(button1);
		number.add(button2);
		number.add(button3);
		number.add(button_add);
		
		number.add(button4);
		number.add(button5);
		number.add(button6);
		number.add(button_min);
		
		number.add(button7);
		number.add(button8);
		number.add(button9);
		number.add(button_mul);
		
		number.add(button0);
		number.add(button_double);
		number.add(button_point);
		number.add(button_div);
		
		number.add(button_c);
		number.add(button_ce);
		number.add(button_equl);
		number.add(button_per);
		//將所有按鈕放上NUMBER的PANEL
		
		show.add(text);
		number.setLayout(new GridLayout(5,3));
		setLayout(new GridLayout(5,3));
		add(show,BorderLayout.NORTH);
		add(number,BorderLayout.CENTER);
		
		
		numberListener l1 = new numberListener();
		button1.addActionListener(l1);
		button2.addActionListener(l1);
		button3.addActionListener(l1);
		button4.addActionListener(l1);
		button5.addActionListener(l1);
		button6.addActionListener(l1);
		button7.addActionListener(l1);
		button8.addActionListener(l1);
		button9.addActionListener(l1);
		button0.addActionListener(l1);
		button_add.addActionListener(l1);
		button_min.addActionListener(l1);
		button_mul.addActionListener(l1);
		button_div.addActionListener(l1);
		button_point.addActionListener(l1);
		button_equl.addActionListener(l1);
		button_c.addActionListener(l1);
        button_double.addActionListener(l1);
        button_per.addActionListener(l1);
        button_ce.addActionListener(l1);
        //每一個按鈕的動作
	}
	public class numberListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			//詳細說明每一個按鈕的功能
			
			if(e.getSource() == button1)
			{
				text.setText(string = string +"1");//顯示按鈕上的文字
			}
			if(e.getSource() == button2)
			{
				text.setText(string = string +"2");//顯示按鈕上的文字
			}
			if(e.getSource() == button3)
			{
				text.setText(string = string +"3");//顯示按鈕上的文字
			}
			if(e.getSource() == button4)
			{
				text.setText(string = string +"4");//顯示按鈕上的文字
			}
			if(e.getSource() == button5)
			{
				text.setText(string = string +"5");//顯示按鈕上的文字
			}
			if(e.getSource() == button6)
			{
				text.setText(string = string +"6");//顯示按鈕上的文字
			}
			if(e.getSource() == button7)
			{
				text.setText(string = string +"7");//顯示按鈕上的文字
			}
			if(e.getSource() == button8)
			{
				text.setText(string = string +"8");//顯示按鈕上的文字
			}
			if(e.getSource() == button9)
			{
				text.setText(string = string +"9");//顯示按鈕上的文字
			}
			if(e.getSource() == button0)
			{
				text.setText(string = string +"0");//顯示按鈕上的文字
			}
			if(e.getSource() == button_add)
			{
				text.setText(string = string +"+");//顯示按鈕上的文字
			}
			if(e.getSource() == button_min)
			{
				text.setText(string = string +"-");//顯示按鈕上的文字
			}
			if(e.getSource() == button_mul)
			{
				text.setText(string = string +"x");//顯示按鈕上的文字
			}
			if(e.getSource() == button_div)
			{
				text.setText(string = string +"÷");//顯示按鈕上的文字
			}
			if(e.getSource() == button_point)
			{
				text.setText(string = string +".");//顯示按鈕上的文字
			}
			if(e.getSource() == button_equl)
			{
				text.setText(string = string +"=");//顯示按鈕上的文字
			}
			if(e.getSource() == button_c)
			{
				text.setText(string = " ");//清除的功能，把字串設為空
			}
			if(e.getSource() == button_double)
			{
				text.setText(string = string + "00");//顯示按鈕上的文字
			}
			if(e.getSource() == button_per)
			{
				text.setText(string = string + "%");//顯示按鈕上的文字
			}
			if(e.getSource() == button_ce)
			{
				text.setText(string =" ");//清除的功能，把字串設為空
			}
		}
	}
}