package a8.s100502004;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class FrameWork extends JFrame implements ActionListener{
	JLabel screen = new JLabel("");//create the buttons and label
	JButton B1 = new JButton("1");
	JButton B2 = new JButton("2");
	JButton B3 = new JButton("3");
	JButton B4 = new JButton("4");
	JButton B5 = new JButton("5");
	JButton B6 = new JButton("6");
	JButton B7 = new JButton("7");
	JButton B8 = new JButton("8");
	JButton B9 = new JButton("9");
	JButton B0 = new JButton("0");
	JButton Bplus = new JButton("+");
	JButton Bminus = new JButton("-");
	JButton Bmul = new JButton("*");
	JButton Bexc = new JButton("/");
	JButton Brequ = new JButton("=");
	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();
	JPanel p3 = new JPanel();
	String fin = "";
	public FrameWork(){
		screen.setBorder(BorderFactory.createEtchedBorder());
		B1.addActionListener(this);//add action listener for each button
		B2.addActionListener(this);
		B3.addActionListener(this);
		B4.addActionListener(this);
		B5.addActionListener(this);
		B6.addActionListener(this);
		B7.addActionListener(this);
		B8.addActionListener(this);
		B9.addActionListener(this);
		B0.addActionListener(this);
		Bplus.addActionListener(this);
		Bminus.addActionListener(this);
		Bmul.addActionListener(this);
		Bexc.addActionListener(this);
		Brequ.addActionListener(this);
		B1.setFont(new Font("標楷體", Font.BOLD, 50));//set the size of each word
		B2.setFont(new Font("標楷體", Font.BOLD, 50));
		B3.setFont(new Font("標楷體", Font.BOLD, 50));
		B4.setFont(new Font("標楷體", Font.BOLD, 50));
		B5.setFont(new Font("標楷體", Font.BOLD, 50));
		B6.setFont(new Font("標楷體", Font.BOLD, 50));
		B7.setFont(new Font("標楷體", Font.BOLD, 50));
		B8.setFont(new Font("標楷體", Font.BOLD, 50));
		B9.setFont(new Font("標楷體", Font.BOLD, 50));
		B0.setFont(new Font("標楷體", Font.BOLD, 50));
		Bplus.setFont(new Font("標楷體", Font.BOLD, 50));
		Bminus.setFont(new Font("標楷體", Font.BOLD, 50));
		Bmul.setFont(new Font("標楷體", Font.BOLD, 50));
		Bexc.setFont(new Font("標楷體", Font.BOLD, 50));
		Brequ.setFont(new Font("標楷體", Font.BOLD, 50));
		
		p1.setLayout(new GridLayout(3,5));//combine the buttons and label
		p1.add(B1);
		p1.add(B2);
		p1.add(B3);		
		p1.add(B4);
		p1.add(B5);
		p1.add(B6);
		p1.add(B7);
		p1.add(B8);
		p1.add(B9);
		p1.add(B0);
		p1.add(Bplus);
		p1.add(Bminus);
		p1.add(Bmul);
		p1.add(Bexc);		
		p1.add(Brequ);
		p2.setLayout(new GridLayout(1,1));
		p2.add(screen);
		p3.setLayout(new GridLayout(2,1));
		p3.add(p2);
		p3.add(p1);
		add(p3);//add them together
	}
	
	public void output(String s){//the method that use to set the word you want to output	
		fin+=s;
		screen.setFont(new Font("標楷體", Font.BOLD, 50));
		screen.setText(fin);
	}
	
	
	public void actionPerformed(ActionEvent e) {//set if you press the button what will happen
		if(e.getSource().equals(B0)){
			output("0");
		}
		if(e.getSource().equals(B1)){
			output("1");
		}
		if(e.getSource().equals(B2)){
			output("2");
		}
		if(e.getSource().equals(B3)){
			output("3");
		}
		if(e.getSource().equals(B4)){
			output("4");
		}
		if(e.getSource().equals(B5)){
			output("5");
		}
		if(e.getSource().equals(B6)){
			output("6");
		}
		if(e.getSource().equals(B7)){
			output("7");
		}
		if(e.getSource().equals(B8)){
			output("8");
		}
		if(e.getSource().equals(B9)){
			output("9");
		}
		if(e.getSource().equals(Bplus)){
			output("+");
		}
		if(e.getSource().equals(Bminus)){
			output("-");
		}
		if(e.getSource().equals(Bmul)){
			output("*");
		}
		if(e.getSource().equals(Bexc)){
			output("/");
		}
		if(e.getSource().equals(Brequ)){
			output("=");
		}		
	}
}
	
	
	
	
	
	

