package a8.s100502017;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
public class FrameWork extends JFrame implements ActionListener{
	JButton[] button=new JButton[15];//the button array
	JLabel screen=new JLabel();
	JPanel pan=new JPanel();
	String str=new String();
	Font bigfont=new Font("Mesquite Std",Font.BOLD,60);
	public FrameWork(){
		pan.setLayout(new GridLayout(3,4));
		for(int i=0;i<=9;i++){//set button 0~9
			button[i]=new JButton(""+i);
			pan.add(button[i]);
		}
		button[10]=new JButton("+");//set button +,-,*,/,AC
		button[11]=new JButton("-");
		button[12]=new JButton("*");
		button[13]=new JButton("/");
		button[14]=new JButton("AC");
		for(int i=10;i<=14;i++){
			pan.add(button[i]);
		}
		screen.setFont(bigfont);//use Font class let screen be more clear
		add(pan,BorderLayout.SOUTH);
		add(screen,BorderLayout.CENTER);
		for(int j=0;j<15;j++){
			button[j].addActionListener(this);
		}
	}
	public void actionPerformed(ActionEvent e){//when button be tagged
		for(int k=0;k<15;k++){//button let screen output the element
			if(e.getSource()==button[k]){
				stringHandle(button[k].getText());
				screen.setText(str);
			}
		}
	}
	public void stringHandle(String s){
		if(s=="AC")//the "AC" button clear screen 
			str="";
		else
			str+=s;
	}
}
