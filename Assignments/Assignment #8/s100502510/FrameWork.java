package a8.s100502510;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener {
	static String display = "";

	JButton numbers[] = new JButton[9];
	JButton plus = new JButton("+");
	JButton minus = new JButton("-");
	JButton multiply = new JButton("*");
	JButton division = new JButton("/");
	JButton dot = new JButton(".");
	JButton zero = new JButton("0");
	JButton equal = new JButton("=");
	JButton bracketleft = new JButton("(");
	JButton bracketright = new JButton(")");
	JButton exclamationpoint = new JButton("!");
	JButton dash = new JButton("-");
	JButton and = new JButton("&");

	JPanel number = new JPanel();
	JPanel others = new JPanel();
	JPanel up = new JPanel();
	JPanel down = new JPanel();
	JLabel screen;

	Font buttonfont = new Font("serif", Font.BOLD, 50);
	Font screenfont = new Font("serif", Font.BOLD, 60);

	public FrameWork() {
		screen = new JLabel(display);
		number.setLayout(new GridLayout(3, 3));
		down.setLayout(new BorderLayout());
		others.setLayout(new GridLayout(3, 4));
		up.setLayout(new GridLayout(1, 1));

		plus.setFont(buttonfont);
		minus.setFont(buttonfont);
		multiply.setFont(buttonfont);
		division.setFont(buttonfont);
		dot.setFont(buttonfont);
		zero.setFont(buttonfont);
		equal.setFont(buttonfont);
		bracketleft.setFont(buttonfont);
		bracketright.setFont(buttonfont);
		exclamationpoint.setFont(buttonfont);
		dash.setFont(buttonfont);
		and.setFont(buttonfont);
		screen.setFont(screenfont);

		for (int i = 0; i < 9; i++) {
			String display = "";
			display = display + (i + 1);
			numbers[i] = new JButton(display);
			numbers[i].setFont(buttonfont);
			number.add(numbers[i]);
		}

		others.add(zero);
		others.add(plus);
		others.add(minus);
		others.add(multiply);
		others.add(division);
		others.add(dot);
		others.add(equal);
		others.add(bracketleft);
		others.add(bracketright);
		others.add(exclamationpoint);
		others.add(dash);
		others.add(and);

		down.add(number, BorderLayout.CENTER);
		down.add(others, BorderLayout.EAST);
		up.add(screen);
		add(up, BorderLayout.CENTER);
		add(down, BorderLayout.SOUTH);

		for (int i = 0; i < 9; i++) {
			numbers[i].addActionListener(this);
		}
		plus.addActionListener(this);
		minus.addActionListener(this);
		multiply.addActionListener(this);
		division.addActionListener(this);
		dot.addActionListener(this);
		zero.addActionListener(this);
		equal.addActionListener(this);
		bracketleft.addActionListener(this);
		bracketright.addActionListener(this);
		exclamationpoint.addActionListener(this);
		dash.addActionListener(this);
		and.addActionListener(this);

	}

	public void actionPerformed(ActionEvent e) {

		for (int i = 0; i < 9; i++) {
			if (e.getSource() == numbers[i]) {
				this.add2(i+1);
				screen.setText(display);
			}
		}

		if (e.getSource() == plus) {
			this.add("+");
			screen.setText(display);
		}
		if (e.getSource() == minus) {
			this.add("-");
			screen.setText(display);
		}
		if (e.getSource() == multiply) {
			this.add("*");
			screen.setText(display);
		}
		if (e.getSource() == division) {
			this.add("/");
			screen.setText(display);
		}
		if (e.getSource() == dot) {
			this.add(".");
			screen.setText(display);
		}
		if (e.getSource() == zero) {
			this.add("0");
			screen.setText(display);
		}
		if (e.getSource() == equal) {
			this.add("=");
			screen.setText(display);
		}
		if (e.getSource() == bracketleft) {
			this.add("(");
			screen.setText(display);
		}
		if (e.getSource() == bracketright) {
			this.add(")");
			screen.setText(display);
		}
		if (e.getSource() == exclamationpoint) {
			this.add("!");
			screen.setText(display);
		}
		if (e.getSource() == dash) {
			this.add("-");
			screen.setText(display);
		}
		if (e.getSource() == and) {
			this.add("&");
			screen.setText(display);
		}

	}
	public void add(String adds){
		display=display+adds;
	}
	public void add2(int adds){
		display=display+adds;
	}
}
