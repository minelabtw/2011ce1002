package a8.s100502511;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.*;

public class FrameWork extends JFrame implements ActionListener {

	JButton one = new JButton("1"); // 19個Button
	JButton two = new JButton("2");
	JButton three = new JButton("3");
	JButton four = new JButton("4");
	JButton five = new JButton("5");
	JButton six = new JButton("6");
	JButton seven = new JButton("7");
	JButton eight = new JButton("8");
	JButton nine = new JButton("9");
	JButton zero = new JButton("0");
	JButton plus = new JButton("+");
	JButton minus = new JButton("-");
	JButton multiply = new JButton("*");
	JButton divide = new JButton("/");
	JButton equal = new JButton("=");
	JButton dot = new JButton(".");
	JButton radical = new JButton("√");
	JButton percent = new JButton("%");
	JButton clean = new JButton("C");

	String displaymessage = ""; // 顯示的訊息
	JLabel Message = new JLabel(displaymessage);

	Border lineBorder = new LineBorder(Color.BLACK, 2); // 設定框線
	Font setfont = new Font("TimesRoman", Font.BOLD, 15); // 設定字型
	Font setfont1 = new Font("TimesRoman", Font.BOLD, 20);

	public FrameWork() {
		JPanel p1 = new JPanel(new GridLayout(4, 5, 10, 10)); // 建立JPanel p1
		one.setFont(setfont); // 設定Button字型
		two.setFont(setfont);
		three.setFont(setfont);
		four.setFont(setfont);
		five.setFont(setfont);
		six.setFont(setfont);
		seven.setFont(setfont);
		eight.setFont(setfont);
		nine.setFont(setfont);
		zero.setFont(setfont);
		plus.setFont(setfont);
		minus.setFont(setfont);
		multiply.setFont(setfont);
		divide.setFont(setfont);
		equal.setFont(setfont);
		dot.setFont(setfont);
		radical.setFont(setfont);
		percent.setFont(setfont);
		clean.setFont(setfont);
		Message.setFont(setfont1);
		p1.add(one);
		p1.add(two);
		p1.add(three);
		p1.add(plus);
		p1.add(clean);
		p1.add(four);
		p1.add(five);
		p1.add(six);
		p1.add(minus);
		p1.add(percent);
		p1.add(seven);
		p1.add(eight);
		p1.add(nine);
		p1.add(multiply);
		p1.add(radical);
		p1.add(zero);
		p1.add(dot);
		p1.add(equal);
		p1.add(divide);

		JPanel p2 = new JPanel(new GridLayout(1, 1, 10, 10)); // 建立JPanel p2
		p2.add(Message);
		p2.setBorder(lineBorder);

		JPanel p3 = new JPanel(new GridLayout(2, 1, 10, 10)); // 建立JPanel p3
		p3.add(p2);
		p3.add(p1);
		add(p3);

		one.addActionListener(this);
		two.addActionListener(this);
		three.addActionListener(this);
		four.addActionListener(this);
		five.addActionListener(this);
		six.addActionListener(this);
		seven.addActionListener(this);
		eight.addActionListener(this);
		nine.addActionListener(this);
		zero.addActionListener(this);
		plus.addActionListener(this);
		minus.addActionListener(this);
		multiply.addActionListener(this);
		divide.addActionListener(this);
		equal.addActionListener(this);
		dot.addActionListener(this);
		radical.addActionListener(this);
		clean.addActionListener(this);
		percent.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) { // 當按下Button，就加入對應的數字符號
		if (e.getSource() == one) {
			displaymessage = displaymessage + "1";
			Message.setText(displaymessage);
		} else if (e.getSource() == two) {
			displaymessage = displaymessage + "2";
			Message.setText(displaymessage);
		} else if (e.getSource() == three) {
			displaymessage = displaymessage + "3";
			Message.setText(displaymessage);
		} else if (e.getSource() == four) {
			displaymessage = displaymessage + "4";
			Message.setText(displaymessage);
		} else if (e.getSource() == five) {
			displaymessage = displaymessage + "5";
			Message.setText(displaymessage);
		} else if (e.getSource() == six) {
			displaymessage = displaymessage + "6";
			Message.setText(displaymessage);
		} else if (e.getSource() == seven) {
			displaymessage = displaymessage + "7";
			Message.setText(displaymessage);
		} else if (e.getSource() == eight) {
			displaymessage = displaymessage + "8";
			Message.setText(displaymessage);
		} else if (e.getSource() == nine) {
			displaymessage = displaymessage + "9";
			Message.setText(displaymessage);
		} else if (e.getSource() == zero) {
			displaymessage = displaymessage + "0";
			Message.setText(displaymessage);
		} else if (e.getSource() == plus) {
			displaymessage = displaymessage + "+";
			Message.setText(displaymessage);
		} else if (e.getSource() == minus) {
			displaymessage = displaymessage + "-";
			Message.setText(displaymessage);
		} else if (e.getSource() == multiply) {
			displaymessage = displaymessage + "*";
			Message.setText(displaymessage);
		} else if (e.getSource() == divide) {
			displaymessage = displaymessage + "/";
			Message.setText(displaymessage);
		} else if (e.getSource() == equal) {
			displaymessage = displaymessage + "=";
			Message.setText(displaymessage);
		} else if (e.getSource() == dot) {
			displaymessage = displaymessage + ".";
			Message.setText(displaymessage);
		} else if (e.getSource() == radical) {
			displaymessage = displaymessage + "√";
			Message.setText(displaymessage);
		} else if (e.getSource() == percent) {
			displaymessage = displaymessage + "%";
			Message.setText(displaymessage);
		} else if (e.getSource() == clean) {
			displaymessage = " ";
			Message.setText(displaymessage);
		}
	}
}
