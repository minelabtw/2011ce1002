package a8.s100502511;

import javax.swing.JFrame;

public class A81 {
	public static void main(String[] args) {
		FrameWork homework = new FrameWork();
		homework.setTitle("Asignment 8-1");
		homework.setSize(400, 400);
		homework.setLocationRelativeTo(null);
		homework.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		homework.setVisible(true);
	}
}
