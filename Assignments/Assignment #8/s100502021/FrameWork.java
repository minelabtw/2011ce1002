package a8.s100502021;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.*;
public class FrameWork extends JFrame implements ActionListener{
	JButton[] butt=new JButton[15];	//set array
	Font largeFont=new Font("",Font.BOLD,20); 
	JPanel p2=new JPanel(new GridLayout(1,1));
	String str=""; //the string
	JLabel jlbl=new JLabel();

	public FrameWork(){
		JPanel p1=new JPanel();
		p1.setLayout(new GridLayout(3,5));
		butt[0]=new JButton("0");		
		butt[1]=new JButton("1");		
		butt[2]=new JButton("2");		
		butt[3]=new JButton("3");		
		butt[4]=new JButton("4");		
		butt[5]=new JButton("5");		
		butt[6]=new JButton("6");		
		butt[7]=new JButton("7");		
		butt[8]=new JButton("8");		
		butt[9]=new JButton("9");	
		butt[10]=new JButton("+");		
		butt[11]=new JButton("-");		
		butt[12]=new JButton("*");	
		butt[13]=new JButton("/");		
		butt[14]=new JButton("=");		
		
		for(int t=0;t<15;t++){
			p1.add(butt[t]);
		}		
		for(int t=0;t<15;t++){
			butt[t].addActionListener(this);
		}		
		jlbl.setForeground(Color.ORANGE);
		
		jlbl.setFont(largeFont);
		p2.add(jlbl);
		this.add(p1,BorderLayout.SOUTH);		
		this.add(p2,BorderLayout.NORTH);		
	}
	
	public void actionPerformed(ActionEvent e){		
		for(int t=0;t<15;t++){
			if(e.getSource() == butt[t]){			
				str=str+butt[t].getText(); //add word
				jlbl.setText(str);
			}
		}
	}	
}