package a8.s100502008;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.LineBorder;

public class FrameWork extends JFrame implements ActionListener{
	JButton[] b=new JButton[16];
	String showstr="";
	JLabel show=new JLabel(showstr);
	public FrameWork()//constructor
	{
		JPanel p1=new JPanel(new GridLayout(4,4,5,5));//panel to create button
		b[0]=new JButton("0");//set button
		b[1]=new JButton("1");
		b[2]=new JButton("2");
		b[3]=new JButton("3");
		b[4]=new JButton("4");
		b[5]=new JButton("5");
		b[6]=new JButton("6");
		b[7]=new JButton("7");
		b[8]=new JButton("8");
		b[9]=new JButton("9");
		b[10]=new JButton("+");
		b[11]=new JButton("-");
		b[12]=new JButton("*");
		b[13]=new JButton("/");
		b[14]=new JButton("��");
		b[15]=new JButton("=");
		Font font1= new Font("SensSerif",Font.BOLD,50);
		Font font2= new Font("SensSerif",Font.BOLD,30);
		for(int i=0;i<16;i++)
		{
			b[i].setFont(font2);
			p1.add(b[i]);
		}
		JPanel p2=new JPanel(new GridLayout(2,1));//panel to create label
		
		show.setBackground(Color.WHITE);
		show.setFont(font1);
		show.setBorder(new LineBorder(Color.BLACK,2));
		p2.add(show);
		p2.add(p1);
		add(p2);//add to frame
		for(int i=0;i<16;i++)
		{
			b[i].addActionListener(this);
		}
	}
	public void actionPerformed(ActionEvent e)//event
	{
		if(e.getSource()==b[0])
		{
			show.setText(showstr+="0");
		}
		else if(e.getSource()==b[1])
		{
			show.setText(showstr+="1");
		}
		else if(e.getSource()==b[2])
		{
			show.setText(showstr+="2");
		}
		else if(e.getSource()==b[3])
		{
			show.setText(showstr+="3");
		}
		else if(e.getSource()==b[4])
		{
			show.setText(showstr+="4");
		}
		else if(e.getSource()==b[5])
		{
			show.setText(showstr+="5");
		}
		else if(e.getSource()==b[6])
		{
			show.setText(showstr+="6");
		}
		else if(e.getSource()==b[7])
		{
			show.setText(showstr+="7");
		}
		else if(e.getSource()==b[8])
		{
			show.setText(showstr+="8");
		}
		else if(e.getSource()==b[9])
		{
			show.setText(showstr+="9");
		}
		else if(e.getSource()==b[10])
		{
			show.setText(showstr+="+");
		}
		else if(e.getSource()==b[11])
		{
			show.setText(showstr+="-");
		}
		else if(e.getSource()==b[12])
		{
			show.setText(showstr+="*");
		}
		else if(e.getSource()==b[13])
		{
			show.setText(showstr+="/");
		}
		else if(e.getSource()==b[14])
		{
			show.setText(showstr+="��");
		}
		else if(e.getSource()==b[15])
		{
			show.setText(showstr+="=");
		}
	}
}
