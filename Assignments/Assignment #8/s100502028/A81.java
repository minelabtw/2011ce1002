package a8.s100502028;

import javax.swing.JFrame;

public class A81 {
	// Main method
	public static void main(String[] args) {
		FrameWork f = new FrameWork(); // Create a frame
		f.setTitle("Asignment #8"); // Set the frame name
		f.setSize(500, 350); // Set the frame size
		f.setLocationRelativeTo(null); // Center a frame
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true); // Display the frame
	} // End main method
}
