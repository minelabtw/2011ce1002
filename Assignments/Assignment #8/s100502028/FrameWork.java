package a8.s100502028;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener {
	
	private JLabel showResult = new JLabel("0");
	private JButton[] button = new JButton[20];
	private String output = "";
	
	public FrameWork() {
		
		// Create some borders
		Border lineBorder1 = new LineBorder(Color.BLUE, 2);
		Border lineBorder2 = new LineBorder(Color.RED, 2);
		
		// Create some fonts
		Font font1 = new Font("TimeNewRoman", Font.BOLD, 16);
		Font font2 = new Font("BritannicBold", Font.BOLD, 30);
		
		// Create panel p1 for several buttons
		JPanel p1 = new JPanel(new GridLayout(4, 5, 0, 0));
		p1.add(button[0] = new JButton("7"));
		p1.add(button[1] = new JButton("8"));
		p1.add(button[2] = new JButton("9"));
		p1.add(button[3] = new JButton("��"));
		p1.add(button[4] = new JButton("C"));
		p1.add(button[5] = new JButton("4"));
		p1.add(button[6] = new JButton("5"));
		p1.add(button[7] = new JButton("6"));
		p1.add(button[8] = new JButton("��"));
		p1.add(button[9] = new JButton("��"));
		p1.add(button[10] = new JButton("1"));
		p1.add(button[11] = new JButton("2"));
		p1.add(button[12] = new JButton("3"));
		p1.add(button[13] = new JButton("-"));
		p1.add(button[14] = new JButton("%"));
		p1.add(button[15] = new JButton("."));
		p1.add(button[16] = new JButton("0"));
		p1.add(button[17] = new JButton("="));
		p1.add(button[18] = new JButton("+"));
		p1.add(button[19] = new JButton("��"));
		
		// For loop to set the border and the font of buttons
		for(int i=0;i<20;i++) {
			button[i].setBorder(lineBorder1);
			button[i].setFont(font1);
		}
		
		// Create panel p2 for showing the result
		JPanel p2 = new JPanel(new FlowLayout(FlowLayout.RIGHT, 50, 10));
		p2.setBorder(lineBorder2);
		p2.setBackground(Color.WHITE);
		showResult.setFont(font2);
		p2.add(showResult);
		
		// Create panel p3 for setting BorderLayout
		JPanel p3 = new JPanel(new BorderLayout(5,10));
		p3.add(p1, BorderLayout.CENTER);
		p3.add(p2, BorderLayout.NORTH);
		
		add(p3); // Add panel to the frame
		
		for(int i=0;i<20;i++) {
			button[i].addActionListener(this); // Active button event here
		}
	}
	
	// Method to let the button works
	public void actionPerformed(ActionEvent e) {
		for(int i=0;i<20;i++) {
			if(e.getSource() == button[i]) { // If statement while the button is pressed
				 showOnScreen(button[i].getText());
			}
		}
	}
	
	// Method to handle the string on the screen
	public void showOnScreen(String s) {
		output += s;
		showResult.setText(output);
	}
} // End class FrameWork
