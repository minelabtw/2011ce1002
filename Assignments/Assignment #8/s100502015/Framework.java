package a8.s100502015;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Framework extends JFrame implements ActionListener {//frame
	private JButton button0 = new JButton("0");
	private JButton button1 = new JButton("1");
	private JButton button2 = new JButton("2");
	private JButton button3 = new JButton("3");
	private JButton button4 = new JButton("4");
	private JButton button5 = new JButton("5");
	private JButton button6 = new JButton("6");
	private JButton button7 = new JButton("7");
	private JButton button8 = new JButton("8");
	private JButton button9 = new JButton("9");
	private JButton buttoncancel = new JButton("c");
	private JButton buttonequal = new JButton("=");
	private JButton buttonplus = new JButton("+");
	private JButton buttonminus = new JButton("-");
	private JButton buttontimes = new JButton("*");
	private JButton buttondivide = new JButton("/");
	private String temp = "";//show number in textfield
	private double tempnumber = 0; //calculate 	
	private int zero = 0;
	private int checksigh = 0;
	private JTextField display = new JTextField(temp);
	private JPanel p1 = new JPanel();
	private JPanel p2 = new JPanel();
	private JPanel p3 = new JPanel();

	public Framework() {

		p1.setLayout(new GridLayout(4, 4));
		p1.add(button0);
		p1.add(button1);
		p1.add(button2);
		p1.add(buttonplus);
		p1.add(button3);
		p1.add(button4);
		p1.add(button5);
		p1.add(buttonminus);
		p1.add(button6);
		p1.add(button7);
		p1.add(button8);
		p1.add(buttontimes);
		p1.add(buttonequal);
		p1.add(button9);
		p1.add(buttoncancel);
		p1.add(buttondivide);
		p2.setLayout(new GridLayout(1, 1));
		p2.add(display);
		p3.setLayout(new GridLayout(2, 1));
		p3.add(p2);
		p3.add(p1);
		button0.addActionListener(this);
		button1.addActionListener(this);
		button2.addActionListener(this);
		button3.addActionListener(this);
		button4.addActionListener(this);
		button5.addActionListener(this);
		button6.addActionListener(this);
		button7.addActionListener(this);
		button8.addActionListener(this);
		button9.addActionListener(this);
		buttonplus.addActionListener(this);
		buttonminus.addActionListener(this);
		buttontimes.addActionListener(this);
		buttondivide.addActionListener(this);
		buttoncancel.addActionListener(this);
		buttonequal.addActionListener(this);
		add(p3);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == button0) {//it won't show if you push 0 first
			if (zero == 0) {
				temp += " ";
			} else {
				temp += 0;
				display.setText(temp);
			}

		} else if (e.getSource() == button1) {
			temp += 1;
			display.setText(temp);
			zero=1;
		} else if (e.getSource() == button2) {
			temp += 2;
			display.setText(temp);
			zero=1;
		} else if (e.getSource() == button3) {
			temp += 3;
			display.setText(temp);
			zero=1;
		} else if (e.getSource() == button4) {
			temp += 4;
			display.setText(temp);
			zero=1;
		} else if (e.getSource() == button5) {
			temp += 5;
			display.setText(temp);
			zero=1;
		} else if (e.getSource() == button6) {
			temp += 6;
			display.setText(temp);
			zero=1;
		} else if (e.getSource() == button7) {
			temp += 7;
			display.setText(temp);
			zero=1;
		} else if (e.getSource() == button8) {
			temp += 8;
			display.setText(temp);
			zero=1;
		} else if (e.getSource() == button9) {
			temp += 9;
			display.setText(temp);
			zero=1;
		} else if (e.getSource() == buttoncancel) {//cancel all number in textfield
			temp = "";
			tempnumber = 0;
			display.setText(temp);

		} else if (e.getSource() == buttonequal) {//equal
			if (temp != "") {//prevent click twice
				if (checksigh == 1) {
					tempnumber += Double.parseDouble(temp);
				} else if (checksigh == 2) {
					tempnumber -= Double.parseDouble(temp);
				} else if (checksigh == 3) {
					tempnumber *= Double.parseDouble(temp);
				} else if (checksigh == 4) {
					tempnumber /= Double.parseDouble(temp);
				} else {
					tempnumber = Double.parseDouble(temp);
				}
				checksigh = 0;
				temp = "";
				display.setText(String.valueOf(tempnumber));
			} else {

			}
		}

		else if (e.getSource() == buttonplus) {//plus

			if (temp != "") {//prevent click twice
				if (checksigh == 1) {
					tempnumber += Double.parseDouble(temp);
				} else if (checksigh == 2) {
					tempnumber -= Double.parseDouble(temp);
				} else if (checksigh == 3) {
					tempnumber *= Double.parseDouble(temp);
				} else if (checksigh == 4) {
					tempnumber /= Double.parseDouble(temp);
				} else {
					tempnumber = Double.parseDouble(temp);
				}
				checksigh = 1;
				temp = "";
				display.setText(String.valueOf(tempnumber));
			} else {

			

			}
		} else if (e.getSource() == buttonminus) {//minus
			if (temp != "") {//prevent click twice
				if (checksigh == 1) {
					tempnumber += Double.parseDouble(temp);
				} else if (checksigh == 2) {
					tempnumber -= Double.parseDouble(temp);
				} else if (checksigh == 3) {
					tempnumber *= Double.parseDouble(temp);
				} else if (checksigh == 4) {
					tempnumber /= Double.parseDouble(temp);
				} else {
					tempnumber = Double.parseDouble(temp);
				}
				checksigh = 2;
				temp = "";
				display.setText(String.valueOf(tempnumber));
			} else {

			}
		} else if (e.getSource() == buttontimes) {//times
			if (temp != "") {//prevent click twice
				if (checksigh == 1) {
					tempnumber += Double.parseDouble(temp);
				} else if (checksigh == 2) {
					tempnumber -= Double.parseDouble(temp);
				} else if (checksigh == 3) {
					tempnumber *= Double.parseDouble(temp);
				} else if (checksigh == 4) {
					tempnumber /= Double.parseDouble(temp);
				} else {
					tempnumber = Double.parseDouble(temp);
				}
				checksigh = 3;
				temp = "";
				display.setText(String.valueOf(tempnumber));
			} else {

			}
		} else if (e.getSource() == buttondivide) {//divide
			if (temp != "") {//prevent click twice
				if (checksigh == 1) {
					tempnumber += Double.parseDouble(temp);
				} else if (checksigh == 2) {
					tempnumber -= Double.parseDouble(temp);
				} else if (checksigh == 3) {
					tempnumber *= Double.parseDouble(temp);
				} else if (checksigh == 4) {
					tempnumber /= Double.parseDouble(temp);
				} else {
					tempnumber = Double.parseDouble(temp);
				}
				checksigh = 4;
				temp = "";
				display.setText(String.valueOf(tempnumber));
			} else {

			}
		}
	}

}
