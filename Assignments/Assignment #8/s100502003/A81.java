package a8.s100502003;

import javax.swing.JFrame;
public class A81 {
	public static void main(String[] args) {
		FrameWork f = new FrameWork(); // declare an object of FrameWork
		// set the characters about the result that will going to be showed out
		f.setTitle("Calculator");
		f.setSize(300,350);
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
