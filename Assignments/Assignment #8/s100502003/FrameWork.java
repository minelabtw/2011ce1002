package a8.s100502003;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FrameWork extends JFrame{
	public String show = " "; // declare a string to be showed on the screen
	private JButton[] buttons = new JButton[16]; // an array to store those JButtons
	private String[] values = new String[16]; // an array to store the values on each button
	JLabel screen = new JLabel(show);
	public FrameWork() {
		int round=7, sign=10; // variables that help to print out the buttons in beautiful ways
		setValues(); // set those values going to be showed on each button
		ListenerClass listener = new ListenerClass(); // create listener
		JPanel p1 = new JPanel(new GridLayout(4,4));
		JPanel p2 = new JPanel(new BorderLayout(10,10));
		
		for(int i=0; i<16; i++) {
			buttons[i] = new JButton(values[i]); // store the buttons in the array
			buttons[i].addActionListener(listener); // register listener for each button
		}
		for(int j=1; j<=3; j++) {
			for(int k=round; k<(round+3); k++) { 
				p1.add(buttons[k]); // one layer for 3 numbers
				p1.add(buttons[sign]); // signals been shown in the right side
			}
			round-=3;
			sign++;
		}
		// the last line
		p1.add(buttons[14]);
		p1.add(buttons[0]);
		p1.add(buttons[15]);
		p1.add(buttons[13]);
		// add the JLabel screen to p2
		p2.add(screen);
		
		// show the result
		add(p1, BorderLayout.CENTER);
		add(p2, BorderLayout.NORTH);
	}
	
	public void setValues() {
		for(int a=0; a<10; a++) {
			values[a] = Integer.toString(a); // change the integer into string and store in an array
		}
		values[10] = "/";
		values[11] = "*";
		values[12] = "-";
		values[13] = "+";
		values[14] = ".";
		values[15] = "=";
	}
	
	class ListenerClass implements ActionListener { // listener class
		public void actionPerformed(ActionEvent e) { // process event
			for(int b=0; b<16; b++) {
				if(e.getSource() == buttons[b]) {
					screen.setText(handleString(b)); // call the method handleString and change the string been shown on the top
				}
			}
		}
	}
	
	public String handleString(int choice) {
		show = show + values[choice]; // add new values into the former string
		return show;
	}
}
