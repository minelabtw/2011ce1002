package a8.s995002026;

import java.awt.*;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;

import javax.swing.*;

public class FrameWork  extends JFrame implements ActionListener {
	private JPanel p1 =new JPanel(new GridLayout(5,3));		//keyboard
	
	private JPanel p2=new JPanel();					//screen
	
	private JPanel p3=new JPanel(new GridLayout(2,1));
	
	private JLabel label=new JLabel();
	
	private JButton[] button=								//一堆按鈕
		{new JButton("C"),new JButton("/"),new JButton("*"),new JButton("-")
			,new JButton("+"),new JButton("0"),new JButton("7"),new JButton("8"),
			new JButton("9"),new JButton("4"),new JButton("5"),new JButton("6"),new JButton("1")
			,new JButton("2"),new JButton("3")};
	
	private String data1="",data2="" ;      						//儲存要印出的東西
	
	FrameWork(){
			p2.setBackground(Color.white);							//背景顏色
			keyboard();											
			screen();
			p3.add(p2,BorderLayout.NORTH);							//視窗
			p3.add(p1,BorderLayout.CENTER);							//按鈕
			
			add(p3);
			for(int i=0;i<15;i++){
				button[i].addActionListener(this);
			}
	}
	
	public void keyboard(){			//鍵盤
		
		for(int i=0;i<15;i++){
			p1.add(button[i]);
		}
		
	}
	
	public void screen(){						//字框
		data1=data1+data2;					//更新資料
		label.setFont( new Font("TimesRoman",Font.BOLD, 20));
		
		label.setText(data1);						//輸入資料
		
		
		p2.add(label);
	}
	
	public void actionPerformed(ActionEvent e){					//設定按鈕作用
		if(e.getSource()==button[0]){
			data1=" ";
			data2=" ";
		}
		else if(e.getSource()==button[1])
			data2="/";
		else if(e.getSource()==button[2])
			data2="*";
		else if(e.getSource()==button[3])
			data2="-";
		else if(e.getSource()==button[4])
			data2="+";
		else if(e.getSource()==button[5])
			data2="0";
		else if(e.getSource()==button[6])
			data2="7";
		else if(e.getSource()==button[7])
			data2="8";
		else if(e.getSource()==button[8])
			data2="9";
		else if(e.getSource()==button[9])
			data2="4";
		else if(e.getSource()==button[10])
			data2="5";
		else if(e.getSource()==button[11])
			data2="6";
		else if(e.getSource()==button[12])
			data2="1";
		else if(e.getSource()==button[13])
			data2="2";
		else if(e.getSource()==button[14])
			data2="3";
		screen();
	}
}
