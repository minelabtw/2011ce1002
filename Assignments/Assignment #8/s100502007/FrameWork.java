package a8.s100502007;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener{
	JButton B1 = new JButton("1");//create buttons
	JButton B2 = new JButton("2");
	JButton B3 = new JButton("3");
	JButton B4 = new JButton("4");
	JButton B5 = new JButton("5");
	JButton B6 = new JButton("6");
	JButton B7 = new JButton("7");
	JButton B8 = new JButton("8");
	JButton B9 = new JButton("9");
	JButton B0 = new JButton("0");
	JButton Bplus = new JButton("+");
	JButton Bminus = new JButton("-");
	JButton Bmulti = new JButton("*");
	JButton Bdiv = new JButton("/");
	JButton Bradical = new JButton("��");
	JButton Bequal = new JButton("=");
	
	String tend="",string="";
	JTextField t1 = new JTextField();
	
	public FrameWork(){	
		JPanel p1=new JPanel();//create panel p1 for the buttons and set GridLayout
		p1.setLayout(new GridLayout(4,4));
		p1.add(B1);//add buttons to the panel
		p1.add(B2);
		p1.add(B3);
		p1.add(B4);
		p1.add(B5);
		p1.add(B6);
		p1.add(B7);
		p1.add(B8);
		p1.add(B9);
		p1.add(B0);
		p1.add(Bplus);
		p1.add(Bminus);
		p1.add(Bmulti);
		p1.add(Bdiv);
		p1.add(Bradical);
		p1.add(Bequal);

		
		JPanel p2 = new JPanel(new BorderLayout());//create panel p2 to hold a text field and p1
		
		p2.add(t1,BorderLayout.NORTH);
		p2.add(p1,BorderLayout.CENTER);
		add(p2,BorderLayout.CENTER);
		
		B1.addActionListener(this);//register listener
		B2.addActionListener(this);
		B3.addActionListener(this);
		B4.addActionListener(this);
		B5.addActionListener(this);
		B6.addActionListener(this);
		B7.addActionListener(this);
		B8.addActionListener(this);
		B9.addActionListener(this);
		B0.addActionListener(this);
		Bplus.addActionListener(this);
		Bminus.addActionListener(this);
		Bmulti.addActionListener(this);
		Bdiv.addActionListener(this);
		Bradical.addActionListener(this);
		Bequal.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == B1){
			string="1";
			calculate(string);
		}
		else if(e.getSource() == B2){
			string="2";
			calculate(string);
		}
		else if(e.getSource() == B3){
			string="3";	
			calculate(string);
		}
		else if(e.getSource() == B4){
			string="4";
			calculate(string);
		}
		else if(e.getSource() == B5){
			string="5";
			calculate(string);
		}
		else if(e.getSource() == B6){
			string="6";
			calculate(string);
		}
		else if(e.getSource() == B7){
			string="7";
			calculate(string);
		}
		else if(e.getSource() == B8){
			string="8";
			calculate(string);
		}
		else if(e.getSource() == B9){
			string="9";
			calculate(string);
		}
		else if(e.getSource() == B0){
			string="0";
			calculate(string);
		}
		else if(e.getSource() == Bplus){
			string="+";
			calculate(string);
		}
		else if(e.getSource() == Bminus){
			string="-";
			calculate(string);
		}
		else if(e.getSource() == Bmulti){
			string="*";
			calculate(string);
		}
		else if(e.getSource() == Bdiv){
			string="/";
			calculate(string);
		}
		else if(e.getSource() == Bradical){
			string="��";
			calculate(string);
		}
		else if(e.getSource() ==Bequal){
			string="=";
			calculate(string);
		}
		t1.setText(tend);
	}
	public void calculate(String string){
		tend+=string;
	}
}
