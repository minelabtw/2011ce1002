package a8.s100502514;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;

public class MyScreen extends JLabel {
	
	/** Auto-generated UID by Eclipse */
	private static final long serialVersionUID = 1L;
	
	public MyScreen(String word){
		setForeground(new Color(255, 64, 0)); //Orange red
		setFont(new Font("Impact", Font.PLAIN, 25));
		setText(word);
		this.setHorizontalAlignment(RIGHT);
		this.setVerticalAlignment(CENTER);
	}
	public MyScreen(){
		this("");
	}
	
	/** Methods to response the button events for this screen */
	public boolean append(String str){
		String txt = getText();
		if(txt.length()>=16)return false; /** The Maximum Character Restrict is 16. */
		setText(txt+str);
		return true;
	}
	public boolean backspace(){
		String txt = getText();
		if(txt.length()==0)return false;
		setText(txt.substring(0, txt.length()-1));
		return true;
	}
	public void clear(){
		setText("");
	}
	
	/** getText() no need to override */
	
}
