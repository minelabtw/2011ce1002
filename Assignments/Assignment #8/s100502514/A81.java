/**
 * CE1002-100502514 FlashTeens Chiang
 * [The Following Rules are from ce1002 website]
 * Assignment 8-1
 * 
 * Please implement an easy calculator GUI and meet the following requirements:

	* class FrameWork:
		Use JButton to create the keyboard, and JPanel to create the screen.
		Implement interface ActionListener and method ActionPerformed for processing the button click event.
		Show the current input STRING on the screen, and write a method to handle the string.
	
	* class A81: main method to initial the framework

 * For more informations, please see on ce1002 website.
	
 */
package a8.s100502514;

public class A81 {
	public static void main(String[] args){
		FrameWork myWindow = new FrameWork();
		myWindow.setTitle("Assignment 8-1 by FlashTeens (Apr. 12, 2012)");
		myWindow.setVisible(true);
	}
}
