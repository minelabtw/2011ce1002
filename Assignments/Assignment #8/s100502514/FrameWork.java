package a8.s100502514;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener, KeyListener {

	/** Auto-generated UID by Eclipse */
	private static final long serialVersionUID = 1L;
	
	MyScreen display;
	JPanel num_buttons;
	JPanel buttons;
	JButton clear_button;
	
	static String[] nums = {
		"7", "8", "9", "/",
		"4", "5", "6", "*",
		"1", "2", "3", "-",
		"0", ".", "��Back", "+",
		};
	
	public FrameWork(){
		
		/** Set Window Size and Closing Operation */
		setSize(250, 350);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		setLayout(new GridLayout(2,1));
		
		display = new MyScreen();
		JPanel display_panel = new JPanel(new BorderLayout());
		display_panel.setBackground(new Color(51, 0, 153)); // Dark Blue Background
		display_panel.add(display);
		add(display_panel);
		
		num_buttons = new JPanel(new GridLayout(4, 4));
		buttons = new JPanel(new BorderLayout());
		buttons.add(num_buttons);
		
		clear_button = new JButton("Clear");
		clear_button.addActionListener(this);
		buttons.add(clear_button, BorderLayout.SOUTH);
		
		add(buttons);
		
		for(int i=0;i<16;i++){
			String label_txt = nums[i];
			MyKeyButton btn = new MyKeyButton(label_txt, label_txt.toLowerCase().charAt(0));
			if(label_txt.length()>1)btn.setKeychar('\b'); /* To Identify the Back button */
			num_buttons.add(btn);
			btn.addActionListener(this);
		}
		
		/** Add Keyboard Events */
		setFocusable(true);
		addKeyListener(this);
	}

	/** When clicked or key-pressed on a button */
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof MyKeyButton){
			String txt = (""+((MyKeyButton)(e.getSource())).getKeychar()).toUpperCase();
			if(txt.charAt(0)<' '){
				if(! display.backspace()){
					JOptionPane.showMessageDialog(null, "The Display Number is EMPTY!!",
							"Assignment 8-1", JOptionPane.WARNING_MESSAGE);
				}
			}
			else{
				if(! display.append(txt)){
					JOptionPane.showMessageDialog(null, "More than 16 characters is not allowed!!",
							"Assignment 8-1", JOptionPane.WARNING_MESSAGE);
				}
			}
			requestFocusInWindow();
		}else if(e.getSource() == clear_button){
			/** clear the number screen */
			if(JOptionPane.showConfirmDialog(null, "Would you really like to clear the screen?",
					"Assignment 8-1", JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
				display.clear();
				JOptionPane.showMessageDialog(null, "Cleared Successfully!!",
						"Assignment 8-1", JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}
	
	/** Empty implements but all needed */
	public void keyPressed(KeyEvent e) {}
	public void keyReleased(KeyEvent e) {}

	/** Typing a matched key can also trigger an ActionEvent if use_keychar is on. */
	public void keyTyped(KeyEvent e) {
		for(Component btn : num_buttons.getComponents()){
			if(e.getKeyChar()==((MyKeyButton)btn).getKeychar()){
				((MyKeyButton)btn).doClick();
			}
		}
	}
	
}
