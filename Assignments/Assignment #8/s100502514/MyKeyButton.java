package a8.s100502514;

import javax.swing.JButton;

public class MyKeyButton extends JButton {

	/** Auto-generated UID by Eclipse */
	private static final long serialVersionUID = 1L;
	
	private char keychar; /* The Fast-key to Trigger ActionEvent */
	private boolean use_keychar; /* Determine whether the Fast-key will be used or not */
	
	private MyKeyButton(String label, char keychar, boolean use_keychar){
		/** Never use this constructor syntax in public. */
		setText(label);
		this.keychar = keychar;
		this.use_keychar = use_keychar;
	}
	
	public MyKeyButton(String label, char keychar){
		this(label, keychar, true);
	}
	public MyKeyButton(char keychar){
		this(String.valueOf(keychar), keychar, true);
	}
	public MyKeyButton(String label, boolean use_keychar){
		this(label, label.charAt(0), use_keychar);
	}
	public MyKeyButton(String label){
		this(label, true);
	}
	public MyKeyButton(){ /* Default Constructor */
		this("");
	}
	
	public void setKeychar(char keychar){//Enable fast-key and set a new key char
		this.keychar = keychar;
		this.use_keychar = true;
	}
	public char getKeychar(){//Get the key char
		if(use_keychar)return keychar;
		else return '\0';
	}
	public void disableKeychar(){//Disable fast-key
		this.use_keychar = false;
	}
	
}
