package a8.s100502011;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener{
	private JPanel upperp = new JPanel();
	private JPanel lowwerp = new JPanel();
	private JPanel total = new JPanel();
	private JLabel input = new JLabel("");
	private String number ="";
	private Font font =new Font("Sarif",Font.BOLD,40);
	private JButton[] button = new JButton[20];
	public FrameWork(){
		
		//set button
		for(int i=1;i<=9;i++){
			button[i] =new JButton(""+i);
		}
		button[0]=new JButton("0");
		button[10] = new JButton("(");
		button[11] = new JButton(")");
		button[12] = new JButton("+");
		button[13] = new JButton("-");
		button[14] = new JButton("x");
		button[15] = new JButton("��");
		button[16] = new JButton("�H");
		button[17] = new JButton("��");
		button[18] = new JButton("=");
		button[19] = new JButton(".");
		
		//set two panel
		upperp.setLayout(new GridLayout(1,1));
		input.setFont(font);
		input.setForeground(Color.BLACK);
		upperp.add(input);
		lowwerp.setLayout(new GridLayout(5,4));
		for(int i=0;i<20;i++){ // let button active and add to panel
			button[i].setFont(font);
			lowwerp.add(button[i]);
			button[i].addActionListener(this);
		}
		
		//add to full panel and frame
		total.setLayout(new GridLayout(2,1,10,10));
		total.add(upperp,BorderLayout.NORTH);
		total.add(lowwerp,BorderLayout.CENTER);
		add(total);
	}
	public void actionPerformed(ActionEvent e){ // button action
		for(int a=0;a<20;a++){
			if(e.getSource()==button[a]){
				number+=button[a].getText();
				input.setText(number); // show input
			}
		}
	}

}