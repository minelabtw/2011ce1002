package a8.s985003038;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.*;

public class FrameWork extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	private JLabel screen;
	private JButton number[];
	private JButton add, minus,	multiple, divide, mod, openBlanket, closeBlanket, dot, equal, backspace;
	private boolean newEquation, errorFlag;

	public FrameWork(){													// constructor of the framework
		setTitle("Functional Calculator fx-992MS");
		setSize(450, 350);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new GridLayout(5,1));
		
		JPanel screenPanel = new JPanel();								// create the screen in the JPanel
		screenPanel.setLayout(new GridLayout(1,1));
		screen = new JLabel("Functional Calculator fx-992MS");			// using JLabel to create the screen
		screen.setOpaque(true);
		screen.setForeground(Color.blue);
		screen.setFont(new Font("Arial", Font.BOLD, 25));
		screen.setHorizontalAlignment(JLabel.RIGHT);
		screen.setBorder(BorderFactory.createCompoundBorder(new LineBorder(Color.black, 1), (Border)BorderFactory.createEmptyBorder(10,20,10,20)));
		screenPanel.add(screen);										// add the screen to the panel 
		add(screenPanel);												// and also add the panel to the frame
		
		number = new JButton[10];										// create digit buttons array
		for(int i = 0; i < number.length; i++){
			number[i] = new JButton(String.valueOf(i));					// for each elements in the array, create an JButton object
			number[i].setFont(new Font("Arial", Font.BOLD, 25));
			number[i].addActionListener(this);							// and add an event handler to the button
		}
		 
		(add = new JButton("+")).setFont(new Font("Arial", Font.BOLD, 25));
		(minus = new JButton("-")).setFont(new Font("Arial", Font.BOLD, 25));
		(multiple = new JButton("*")).setFont(new Font("Arial", Font.BOLD, 25));
		(divide = new JButton("/")).setFont(new Font("Arial", Font.BOLD, 25));
		(mod = new JButton("%")).setFont(new Font("Arial", Font.BOLD, 25));
		(openBlanket = new JButton("(")).setFont(new Font("Arial", Font.BOLD, 25));
		(closeBlanket = new JButton(")")).setFont(new Font("Arial", Font.BOLD, 25));
		(dot = new JButton(".")).setFont(new Font("Arial", Font.BOLD, 25));
		(equal = new JButton("=")).setFont(new Font("Arial", Font.BOLD, 25));
		(backspace = new JButton("Back")).setFont(new Font("Arial", Font.BOLD, 15));
		
		add.addActionListener(this);									// similarly, create the operator buttons, and add it to the event handler
		minus.addActionListener(this);
		multiple.addActionListener(this);
		divide.addActionListener(this);
		mod.addActionListener(this);
		openBlanket.addActionListener(this);
		closeBlanket.addActionListener(this);
		dot.addActionListener(this);
		equal.addActionListener(this);
		backspace.addActionListener(this);
		
		for(int i = 0; i < 4; i++){										// add each component to the frame
			JPanel linePanel = new JPanel();
			if(i != 3){
				linePanel.setLayout(new GridLayout(1,5));
				for(int j = 1; j <= 3; j++)
					linePanel.add(number[(2-i)*3+j]);
				if(i == 0){
					linePanel.add(add);
					linePanel.add(openBlanket);
				} else if(i == 1){
					linePanel.add(minus);
					linePanel.add(closeBlanket);
				} else if(i == 2){
					linePanel.add(multiple);
					linePanel.add(mod);
				}
			} else {
				linePanel.setLayout(new GridLayout(1,3));
				linePanel.add(number[0]);
				linePanel.add(dot);
				linePanel.add(equal);
				linePanel.add(divide);
				linePanel.add(backspace);
			}
			add(linePanel);
		}
		
		newEquation = true;
		errorFlag = true;
	}
	
	public void actionPerformed(ActionEvent e){							// event handler
		JButton eventButton = (JButton)e.getSource();					// get the event button
		if(errorFlag){													// if error flag is on, it means that there is an error occurs in the previous formula
			screen.setText("0");										// so the screen is displaying with special color and font size
			screen.setForeground(Color.black);							// we reset the screen style and the error flag
			errorFlag = false;
		}
		
		if(eventButton == equal){										// if equal button is clicked
			try {														// call the calculating method in the calculator class, and try to catch the exceptions
				screen.setText(Calculator.scanForToken(screen.getText()));
			} catch (CalculatorException exception) {					// if there is any exceptions, set the error flag and show the error message
				System.err.println("Error(" + exception.getExceptionID() + "): " + exception.getExceptionDescription());
				screen.setForeground(Color.red);
				screen.setText("Error(" + exception.getExceptionID() + "): " + exception.getExceptionDescription());
				errorFlag = true;
			}
			newEquation = true;											// whatever there is an error or not, set the new equation flag, since the equal button is clicked
		} else if(eventButton == backspace){							// if the backspace button is clicked, delete the last character in the screen
			if(newEquation || screen.getText().length() <= 1){			// if there is no more character in the screen
				screen.setText("0");									// just show zero
				newEquation = true;										// and also reset the new equation flag
			} else
				screen.setText(screen.getText().substring(0, screen.getText().length()-1));
		} else {														// if the other buttons are clicked, decide what we want to do
			if(isDigitButton(eventButton) && (newEquation || (screen.getText().length() == 1 && screen.getText() != "-" && screen.getText() != "(" && screen.getText() != ")" && Integer.valueOf(screen.getText()) == 0)))
				screen.setText(eventButton.getText());					// if the new equation flag is on, or the screen is just showing a zero, and you input something, the screen will be refreshed first before showing your input
			else if(eventButton == dot && newEquation)					// if the new equation flag is on, and you click the dot button, we will not only show the dot, but also the zero before the dot
				screen.setText("0.");
			else if((eventButton == openBlanket || eventButton == closeBlanket || eventButton == minus) && (screen.getText() == "0" || newEquation))
				screen.setText(eventButton.getText());					// the formula can start with special operators such as open blanket and minus operator
			else														// otherwise, just append the button name after the formula
				screen.setText(screen.getText() + eventButton.getText());
			newEquation = false;										// if a button is clicked, set the new equation to false since the formula has content in it
		}
	}
	
	private boolean isDigitButton(JButton button){
		if(Character.isDigit(button.getText().charAt(0)))				// check if the input button is a digit button
			return true;												// and return the result
		else
			return false;
	}
}
