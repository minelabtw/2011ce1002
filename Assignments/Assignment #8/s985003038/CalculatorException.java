package a8.s985003038;

public class CalculatorException extends Exception {
	private static final long serialVersionUID = 1L;
	private int exceptionID;

	public CalculatorException(int caseID){			// constructor of exception 
		exceptionID = caseID;						// set the exception case ID
	}
	
	public int getExceptionID(){
		return exceptionID;							// get the exception case ID
	}
	
	public String getExceptionDescription(){		// get the exception case description according to the case ID
		if(exceptionID == 0)
			return "Unknown symbols";
		else if(exceptionID == 1)
			return "Multiple dots";
		else if(exceptionID == 2)
			return "Blankets not match";
		else if(exceptionID == 3)
			return "Operator missing";
		else if(exceptionID == 4)
			return "Continuous operators";
		else if(exceptionID == 5)
			return "Start with +, *, / or %";
		else if(exceptionID == 6)
			return "End with operator";
		else if(exceptionID == 7)
			return "Divided by zero";
		else
			return "Undefined exception";
	}
}
