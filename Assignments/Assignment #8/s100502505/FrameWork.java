package a8.s100502505;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener {
	JPanel up = new JPanel();//兩個Panel
	JPanel down = new JPanel();
	JLabel words = new JLabel();//screen
	JButton number0 = new JButton("0");//按鈕
	JButton number1 = new JButton("1");
	JButton number2 = new JButton("2");
	JButton number3 = new JButton("3");
	JButton number4 = new JButton("4");
	JButton number5 = new JButton("5");
	JButton number6 = new JButton("6");
	JButton number7 = new JButton("7");
	JButton number8 = new JButton("8");
	JButton number9 = new JButton("9");
	JButton point = new JButton(".");
	JButton plus = new JButton("+");
	JButton minus = new JButton("-");
	JButton x = new JButton("*");
	JButton divide = new JButton("/");
	JButton clean = new JButton("C");
	Font size = new Font("serif",Font.BOLD,40);//設定字體
	Font wordsize = new Font("serif",Font.BOLD,100);
	
	public FrameWork()
	{
		add(up, BorderLayout.NORTH);
		add(down, BorderLayout.SOUTH);
		up.add(words);
		down.setLayout(new GridLayout(4,4));//4行4列
		down.add(number1);
		down.add(number2);
		down.add(number3);
		down.add(plus);
		down.add(number4);
		down.add(number5);
		down.add(number6);
		down.add(minus);
		down.add(number7);
		down.add(number8);
		down.add(number9);
		down.add(x);
		down.add(number0);
		down.add(point);
		down.add(clean);
		down.add(divide);
		words.setFont(wordsize);//設定字體
		number0.setFont(size);
		number1.setFont(size);
		number2.setFont(size);
		number3.setFont(size);
		number4.setFont(size);
		number5.setFont(size);
		number6.setFont(size);
		number7.setFont(size);
		number8.setFont(size);
		number9.setFont(size);
		point.setFont(size);
		plus.setFont(size);
		minus.setFont(size);
		x.setFont(size);
		divide.setFont(size);
		clean.setFont(size);
		x.setFont(size);
		number0.addActionListener(this);//按下去有動作
		number1.addActionListener(this);
		number2.addActionListener(this);
		number3.addActionListener(this);
		number4.addActionListener(this);
		number5.addActionListener(this);
		number6.addActionListener(this);
		number7.addActionListener(this);
		number8.addActionListener(this);
		number9.addActionListener(this);
		point.addActionListener(this);
		plus.addActionListener(this);
		minus.addActionListener(this);
		x.addActionListener(this);
		divide.addActionListener(this);
		clean.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent a) {
		if(a.getSource() == number0)//顯示號碼
		{
			words.setText(word() + "0");
		}
		if(a.getSource() == number1)
		{
			words.setText(word() + "1");
		}
		if(a.getSource() == number2)
		{
			words.setText(word() + "2");
		}
		if(a.getSource() == number3)
		{
			words.setText(word() + "3");
		}
		if(a.getSource() == number4)
		{
			words.setText(word() + "4");
		}
		if(a.getSource() == number5)
		{
			words.setText(word() + "5");
		}
		if(a.getSource() == number6)
		{
			words.setText(word() + "6");
		}
		if(a.getSource() == number7)
		{
			words.setText(word() + "7");
		}
		if(a.getSource() == number8)
		{
			words.setText(word() + "8");
		}
		if(a.getSource() == number9)
		{
			words.setText(word() + "9");
		}
		if(a.getSource() == point)
		{
			words.setText(word() + ".");
		}
		if(a.getSource() == plus)
		{
			words.setText(word() + "+");
		}
		if(a.getSource() == minus)
		{
			words.setText(word() + "-");
		}
		if(a.getSource() == x)
		{
			words.setText(word() + "*");
		}
		if(a.getSource() == divide)
		{
			words.setText(word() + "/");
		}
		if(a.getSource() == clean)
		{
			words.setText("");//清除之前所輸入的
		}
	}
	
	public String word()//之前輸入的
	{
		return words.getText();
	}
}
