package a8.s100502512;

import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.LineBorder;

public class FrameWork extends JFrame implements ActionListener {
	JButton zero = new JButton("" + 0);//設置計算機上的按鈕
	JButton one = new JButton("" + 1);
	JButton two = new JButton("" + 2);
	JButton three = new JButton("" + 3);
	JButton four = new JButton("" + 4);
	JButton five = new JButton("" + 5);
	JButton six = new JButton("" + 6);
	JButton seven = new JButton("" + 7);
	JButton eight = new JButton("" + 8);
	JButton nine = new JButton("" + 9);
	JButton start = new JButton("Start");
	JButton exit = new JButton("Exit");
	JButton add = new JButton("+");
	JButton minus = new JButton("-");
	JButton multiply = new JButton("*");
	JButton divide = new JButton("/");
	JTextField A = new JTextField("");//顯示計算過程的JTextField

	public FrameWork() {
		JPanel p1 = new JPanel();//排版,利用三個panel
		p1.setLayout(new GridLayout(4, 3));
		p1.add(zero);
		zero.setFont(new Font("serif", Font.BOLD, 25));//設定字型
		p1.add(one);
		one.setFont(new Font("serif", Font.BOLD, 25));
		p1.add(two);
		two.setFont(new Font("serif", Font.BOLD, 25));
		p1.add(three);
		three.setFont(new Font("serif", Font.BOLD, 25));
		p1.add(four);
		four.setFont(new Font("serif", Font.BOLD, 25));
		p1.add(five);
		five.setFont(new Font("serif", Font.BOLD, 25));
		p1.add(six);
		six.setFont(new Font("serif", Font.BOLD, 25));
		p1.add(seven);
		seven.setFont(new Font("serif", Font.BOLD, 25));
		p1.add(eight);
		eight.setFont(new Font("serif", Font.BOLD, 25));
		p1.add(nine);
		nine.setFont(new Font("serif", Font.BOLD, 25));
		p1.add(start);
		start.setFont(new Font("serif", Font.BOLD, 25));
		p1.add(exit);
		exit.setFont(new Font("serif", Font.BOLD, 25));
		add(p1);
		JPanel p2 = new JPanel();
		p2.setLayout(new BorderLayout());
		p2.add(A, BorderLayout.NORTH);
		p2.add(p1, BorderLayout.CENTER);
		add(p2);
		JPanel p3 = new JPanel();
		p3.setLayout(new BorderLayout(4, 2));
		p1.add(add);
		add.setFont(new Font("serif", Font.BOLD, 25));
		p1.add(minus);
		minus.setFont(new Font("serif", Font.BOLD, 25));
		p1.add(multiply);
		multiply.setFont(new Font("serif", Font.BOLD, 25));
		p1.add(divide);
		divide.setFont(new Font("serif", Font.BOLD, 25));
		p3.add(p2, BorderLayout.CENTER);
		add(p3);
		start.addActionListener(this);//讓按按鈕時會有回應
		exit.addActionListener(this);
		zero.addActionListener(this);
		one.addActionListener(this);
		two.addActionListener(this);
		three.addActionListener(this);
		four.addActionListener(this);
		five.addActionListener(this);
		six.addActionListener(this);
		seven.addActionListener(this);
		eight.addActionListener(this);
		nine.addActionListener(this);
		add.addActionListener(this);
		minus.addActionListener(this);
		multiply.addActionListener(this);
		divide.addActionListener(this);
	}

	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == start) {
			System.out.println("Start button clicked");//按下時會顯示的訊息
			A.setText("start  :");//在計算機上顯現的訊息
		}
		if (arg0.getSource() == exit) {
			System.out.println("Exit button clicked");
			A.setText("exit");
			System.exit(0);//若按下exit則跳出計算機
		}

		if (arg0.getSource() == zero) {
			System.out.println("0 button clicked");
			String str = A.getText();
			A.setText(str + "0");
		}
		if (arg0.getSource() == one) {
			System.out.println("1 button clicked");
			String str = A.getText();
			A.setText(str + "1");
		}
		if (arg0.getSource() == two) {
			System.out.println("2 button clicked");
			String str = A.getText();
			A.setText(str + "2");
		}
		if (arg0.getSource() == three) {
			System.out.println("3 button clicked");
			String str = A.getText();
			A.setText(str + "3");
		}
		if (arg0.getSource() == four) {
			System.out.println("4 button clicked");
			String str = A.getText();
			A.setText(str + "4");
		}
		if (arg0.getSource() == five) {
			System.out.println("5 button clicked");
			String str = A.getText();
			A.setText(str + "5");
		}
		if (arg0.getSource() == six) {
			System.out.println("6 button clicked");
			String str = A.getText();
			A.setText(str + "6");
		}
		if (arg0.getSource() == seven) {
			System.out.println("7 button clicked");
			String str = A.getText();
			A.setText(str + "7");
		}
		if (arg0.getSource() == eight) {
			System.out.println("8 button clicked");
			String str = A.getText();
			A.setText(str + "8");
		}
		if (arg0.getSource() == nine) {
			System.out.println("9 button clicked");
			String str = A.getText();
			A.setText(str + "9");
		}
		if (arg0.getSource() == add) {
			System.out.println("+ button clicked");
			String str = A.getText();
			A.setText(str + "+");
		}
		if (arg0.getSource() == minus) {
			System.out.println("- button clicked");
			String str = A.getText();
			A.setText(str + "-");
		}
		if (arg0.getSource() == multiply) {
			System.out.println("* button clicked");
			String str = A.getText();
			A.setText(str + "*");
		}
		if (arg0.getSource() == divide) {
			System.out.println("/ button clicked");
			String str = A.getText();
			A.setText(str + "/");
		}
	}

}
