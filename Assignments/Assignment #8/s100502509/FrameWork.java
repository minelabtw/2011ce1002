package a8.s100502509;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.LineBorder;
public class FrameWork extends JFrame implements ActionListener{
	LineBorder bor = new LineBorder(Color.BLACK,2);
	JButton b1 = new JButton("1");
	//ActionEvent B1;
	JButton b2 = new JButton("2");
	//ActionEvent B2;
	JButton b3 = new JButton("3");
	JButton b4 = new JButton("4");
	JButton b5 = new JButton("5");
	JButton b6 = new JButton("6");
	JButton b7 = new JButton("7");
	JButton b8 = new JButton("8");
	JButton b9 = new JButton("9");
	JButton b0 = new JButton("0");
	JButton addNum  = new JButton("+");
	JButton decreaseNum  = new JButton("-");
	JButton multiNum = new JButton("*");
	JButton divideNum = new JButton("/");
	JButton equal = new JButton("=");
	JButton dot = new JButton(".");
	JPanel screen = new JPanel();
	
	FrameWork(){
		b1.addActionListener(this);
		b2.addActionListener(this);
		b3.addActionListener(this);
		b4.addActionListener(this);
		b5.addActionListener(this);
		b6.addActionListener(this);
		b7.addActionListener(this);
		b8.addActionListener(this);
		b9.addActionListener(this);
		b0.addActionListener(this);
		dot.addActionListener(this);
		equal.addActionListener(this);
		addNum.addActionListener(this);
		decreaseNum.addActionListener(this);
		multiNum.addActionListener(this);
		divideNum.addActionListener(this);
		JPanel p1 = new JPanel();
		screen.setBorder(bor);
		p1.setLayout(new GridLayout(4,4,5,5));
		p1.add(b7);
		p1.add(b8);
		p1.add(b9);
		p1.add(divideNum);
		p1.add(b4);
		p1.add(b5);
		p1.add(b6);
		p1.add(multiNum);
		p1.add(b1);
		p1.add(b2);
		p1.add(b3);
		p1.add(decreaseNum);
		p1.add(b0);
		p1.add(dot);
		p1.add(addNum);
		p1.add(equal);
		add(screen,BorderLayout.NORTH);
		add(p1,BorderLayout.SOUTH);
		
		
		
	}
	
	
	public void actionPerformed(ActionEvent e) {
		String content="";
		JLabel content1;
		if(e.getSource()==b1){
			content = content + "1";
			content1 = new JLabel("1");
			screen.add(content1);
		}
		
	}
	
	

}
