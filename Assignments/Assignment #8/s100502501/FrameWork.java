package a8.s100502501;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class FrameWork extends JFrame implements ActionListener{
	private String input="";
	JButton[] button = new JButton[16]; //create button array
	JLabel jlb = new JLabel(); //create label
	
	public FrameWork(){
		button[0]= new JButton("7"); //set text of button
		button[1]= new JButton("8");
		button[2]= new JButton("9");
		button[3]= new JButton("��");
		button[4]= new JButton("4");
		button[5]= new JButton("5");
		button[6]= new JButton("6");
		button[7]= new JButton("��");
		button[8]= new JButton("1");
		button[9]= new JButton("2");
		button[10]= new JButton("3");
		button[11]= new JButton("-");
		button[12]= new JButton("0");
		button[13]= new JButton(".");
		button[14]= new JButton("=");
		button[15]= new JButton("+");
		jlb.setFont(new Font("SansSerif",Font.BOLD,28)); //set font of label
		jlb.setSize(400,250); //set size of label
		JPanel p0=new JPanel(new GridLayout(2,4)); //create panel
		JPanel p1=new JPanel(new GridLayout(2,4));
		JPanel p2=new JPanel(new GridLayout(3,1));
		for(int i=0;i<8;i++){
			button[i].setFont(new Font("SansSerif",Font.BOLD,22)); //set font of button
			p0.add(button[i]); //add button to panel
			button[i].addActionListener(this); //active button event here
		}
		for(int i=8;i<16;i++){
			button[i].setFont(new Font("SansSerif",Font.BOLD,22)); //set font of button
			p1.add(button[i]); //add button to panel
			button[i].addActionListener(this); //active button event here
		}
		p2.add(jlb); //add label to panel
		p2.add(p0); 
		p2.add(p1);
		add(p2); //add panel to frame
	}
	public void str(String string0){ //handle the input string
		input+=string0;
	}
	
	public void actionPerformed(ActionEvent e){
		for(int i=0;i<16;i++){
			if(e.getSource()==button[i]){
				this.str(button[i].getText());
				jlb.setText(input); //set text of label
			}
		}
	}
}
