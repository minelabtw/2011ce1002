package a8.s100502030;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.*;

public class FrameWork extends JFrame implements ActionListener {
	private Font font = new Font("TimesRoman", Font.BOLD, 40);
	private Border lineBorder = new LineBorder(Color.CYAN, 5);
	private JLabel show = new JLabel(" ");
	private int size = 20;
	private JButton[] button = new JButton[size];
	private String text[] = new String[size];
	private int number = 0;

	public FrameWork() {

		button[0] = new JButton("7");
		button[1] = new JButton("8");
		button[2] = new JButton("9");
		button[3] = new JButton("+");
		button[4] = new JButton("CE");
		button[5] = new JButton("4");
		button[6] = new JButton("5");
		button[7] = new JButton("6");
		button[8] = new JButton("-");
		button[9] = new JButton("→");
		button[10] = new JButton("1");
		button[11] = new JButton("2");
		button[12] = new JButton("3");
		button[13] = new JButton("*");
		button[14] = new JButton("√");
		button[15] = new JButton("0");
		button[16] = new JButton(".");
		button[17] = new JButton("=");
		button[18] = new JButton("/");
		button[19] = new JButton("%");
		// creat button

		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();
		JPanel p3 = new JPanel();

		show.setFont(font);
		p1.setBorder(lineBorder);
		p1.setBackground(Color.WHITE);
		p1.setLayout(new FlowLayout(FlowLayout.RIGHT, 10, 20));//set p1
		p2.setLayout(new GridLayout(2, 5));// set p2
		p3.setLayout(new GridLayout(2, 5));// set p3

		for (int i = 0; i < 10; i++) {
			button[i].setFont(font);
			if ((i + 1) % 5 == 4 || (i + 1) % 5 == 0)
				button[i].setForeground(Color.RED);// 設置右兩排顯示紅色
			else
				button[i].setForeground(Color.BLUE);// 設置右兩排顯示藍色
			p2.add(button[i]);
			button[i].addActionListener(this);// buttin action
		}
		for (int i = 10; i < size; i++) {
			button[i].setFont(font);
			if ((i + 1) % 5 == 4 || (i + 1) % 5 == 0)
				button[i].setForeground(Color.RED);
			else
				button[i].setForeground(Color.BLUE);
			p3.add(button[i]);
			button[i].addActionListener(this);
		}

		p1.add(show);

		setLayout(new GridLayout(3, 1, 2, 2));
		add(p1);
		add(p2);
		add(p3);
	}

	public void actionPerformed(ActionEvent e) {

		for (int i = 0; i < size; i++) {
			if (e.getSource() == button[i]) {
				show.setText(string_onScreen(button[i].getText()));// 改變顯示
			}
		}

	}

	public String string_onScreen(String sign) {
		String show_onScreen = "";
		if (sign == "CE") {
			number = 0;
			text[0] = "";// 消去全部
		} else if (sign == "→") {
			number--;
			if (number < 0)
				number = 0;// 消去1個
		} else {

			if (number < size) {
				text[number] = sign;
				number++;
			}
		}
		for (int i = 0; i < number; i++) {
			show_onScreen = show_onScreen + text[i];
		}
		return show_onScreen;

	}// get showMessage
}
