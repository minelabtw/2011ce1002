package a8.s100502022;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener{
	//initialize store variable
	private String a = "";
	
	private JPanel cal_interaface = new JPanel();
	private JPanel enter_button = new JPanel();
	private JPanel show_interface = new JPanel();
	
	private JButton button_1 = new JButton();
	private JButton button_2 = new JButton();
	private JButton button_3 = new JButton();
	private JButton button_4 = new JButton();
	private JButton button_5 = new JButton();
	private JButton button_6 = new JButton();
	private JButton button_7 = new JButton();
	private JButton button_8 = new JButton();
	private JButton button_9 = new JButton();
	private JButton button_10 = new JButton();
	private JButton button_11 = new JButton();
	private JButton button_12 = new JButton();

	private JButton button_13 = new JButton();
	private JButton button_14 = new JButton();
	private JButton button_15 = new JButton();
	private JButton button_16 = new JButton();
	private JButton button_17 = new JButton();
	private JLabel l = new JLabel("");
	
	public FrameWork(){
		
		
		//try darkGray interface
		show_interface.setBackground(Color.darkGray);
		//set button name
		button_1.setText("0");
		button_2.setText("1");
		button_3.setText("2");
		button_4.setText("3");
		button_5.setText("4");
		button_6.setText("5");
		button_7.setText("6");
		button_8.setText("7");
		button_9.setText("8");
		button_10.setText("9");
		button_11.setText(".");
		button_12.setText("+");
		button_13.setText("-");
		button_14.setText("*");
		button_15.setText("/");
		button_16.setText("C");
		button_17.setText("Enter(Exit)");
		//set boxes
		cal_interaface.setLayout(new GridLayout(4,4,1,1));
		
		cal_interaface.add(button_16);
		cal_interaface.add(button_15);
		cal_interaface.add(button_14);
		cal_interaface.add(button_13);
		cal_interaface.add(button_8);
		cal_interaface.add(button_9);
		cal_interaface.add(button_10);
		cal_interaface.add(button_12);
		cal_interaface.add(button_5);
		cal_interaface.add(button_6);
		cal_interaface.add(button_7);
		cal_interaface.add(button_11);
		cal_interaface.add(button_2);
		cal_interaface.add(button_3);
		cal_interaface.add(button_4);
		cal_interaface.add(button_1);
		//set enter button to clixk
		enter_button.add(button_17);
		enter_button.setLayout(new GridLayout(1,1));
		//set border of buttons
		Border lineBorder = new LineBorder(Color.gray, 5);
		button_1.setBorder(lineBorder);
		button_2.setBorder(lineBorder);
		button_3.setBorder(lineBorder);
		button_4.setBorder(lineBorder);
		button_5.setBorder(lineBorder);
		button_6.setBorder(lineBorder);
		button_7.setBorder(lineBorder);
		button_8.setBorder(lineBorder);
		button_9.setBorder(lineBorder);
		button_10.setBorder(lineBorder);
		button_11.setBorder(lineBorder);
		button_12.setBorder(lineBorder);
		button_13.setBorder(lineBorder);
		button_14.setBorder(lineBorder);
		button_15.setBorder(lineBorder);
		button_16.setBorder(lineBorder);
		button_17.setBorder(lineBorder);
		
		//devide three section of frame
		setLayout(new GridLayout(3,1));
		
		show_interface.add(l);
		show_interface.setLayout(new FlowLayout(FlowLayout.RIGHT));
		//change size and font of String
		Font font = new Font("TimesRoman",Font.BOLD, 50);
		l.setForeground(Color.WHITE);
		l.setFont(font);
	    
		
		add(show_interface);
		add(cal_interaface);
		add(enter_button);
		//let it has function
		button_1.addActionListener(this);
		button_2.addActionListener(this);
		button_3.addActionListener(this);
		button_4.addActionListener(this);
		button_5.addActionListener(this);
		button_6.addActionListener(this);
		button_7.addActionListener(this);
		button_8.addActionListener(this);
		button_9.addActionListener(this);
		button_10.addActionListener(this);
		button_11.addActionListener(this);
		button_12.addActionListener(this);
		button_13.addActionListener(this);
		button_14.addActionListener(this);
		button_15.addActionListener(this);
		button_16.addActionListener(this);
		button_17.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e){
		//if inputs are inside the box
		if(l.getWidth()<=show_interface.getWidth()){
			//if we click
			if (e.getSource()==button_1) {
				a=a+button_1.getText();
				l.setText(a);
			}
			else if (e.getSource()==button_11) {
				a=a+button_11.getText();
				l.setText(a);
			}
			else if (e.getSource()==button_10) {
				a=a+button_10.getText();
				l.setText(a);
			}
			else if (e.getSource()==button_9) {
				a=a+button_9.getText();
				l.setText(a);
			}
			else if (e.getSource()==button_8) {
				a=a+button_8.getText();
				l.setText(a);
			}
			else if (e.getSource()==button_7) {
				a=a+button_7.getText();
				l.setText(a);
			}
			else if (e.getSource()==button_6) {
				a=a+button_6.getText();
				l.setText(a);
			}
			else if (e.getSource()==button_5) {
				a=a+button_5.getText();
				l.setText(a);
			}
			else if (e.getSource()==button_4) {
				a=a+button_4.getText();
				l.setText(a);
			}
			else if (e.getSource()==button_3) {
				a=a+button_3.getText();
				l.setText(a);
			}
			else if (e.getSource()==button_2) {
				a=a+button_2.getText();
				l.setText(a);
			}
			else if (e.getSource()==button_12){
				a=a+button_12.getText();
				l.setText(a);
			}
			else if (e.getSource()==button_13){
				a=a+button_13.getText();
				l.setText(a);
			}
			else if (e.getSource()==button_14){
				a=a+button_14.getText();
				l.setText(a);
			}
			else if (e.getSource()==button_15){
				a=a+button_15.getText();
				l.setText(a);
			}
			else if (e.getSource()==button_16){
				a="";
				l.setText(a);
			}
			else if(e.getSource()==button_17){
				a="Thanks for ur using!! ";
				l.setText(a);
			}
		}
		else if(l.getWidth()>show_interface.getWidth()){
			if (e.getSource()==button_16){
				a="";
				l.setText(a);
			}
		}
		
		
		
	}
	

}
