package a8.s995002203;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

public class FrameWork extends JFrame{
	private JPanel p1 =new JPanel(new GridLayout(4, 3, 5, 5));//contain 數字鍵
	private JPanel p2 =new JPanel(new GridLayout(4, 3, 5, 5));//contain 運算元鍵
	private JPanel p3 =new JPanel(new BorderLayout());//contain p1&p2
	private JTextField screen = new JTextField("0");//顯示區
	private JButton add = new JButton ("+");
	private JButton subtract = new JButton ("-");
    private JButton multiply = new JButton ("*"); 
	private JButton divide = new JButton ("/");
    private JButton equate = new JButton (" = ");
    private JButton reset = new JButton(" C ");
    private JButton[] number = new JButton[10];
	private boolean start=true;//標記現在是一串數字的開始 還是在輸入數字中
	private double total=0;//運算的結果
	Object preOP;
	public FrameWork(){
		Font largeFont=new Font("TimeRoman", Font.BOLD, 20);
		ActionListener numberListener = new NumberListener();//宣告兩個自訂的ActionListener
		ActionListener optionListener = new OptionListener();
		p1.setBorder(new TitledBorder("number button"));//用titleborder包住數字鍵
		for(int i=0; i<10;i++){
			number[i]=new JButton(""+i);//加入數字按鈕
			p1.add(number[i]);
			number[i].setFont(largeFont);
			number[i].addActionListener(numberListener);//跟自訂的actionListener連接
		}
		
		

		p2.setBorder(new TitledBorder("operator button"));//用titleborder包住運算元鍵
		equate.setFont(largeFont);
		equate.addActionListener(optionListener);//跟自訂的actionListener連接
		reset.setFont(largeFont);
		reset.addActionListener(optionListener);
		add.setFont(largeFont);
		add.addActionListener(optionListener);
		subtract.setFont(largeFont);
		subtract.addActionListener(optionListener);
		multiply.setFont(largeFont);
		multiply.addActionListener(optionListener);
		divide.setFont(largeFont);
		divide.addActionListener(optionListener);
		p2.add(equate);//加入怨算元按鈕
		p2.add(reset);
		p2.add(add);
		p2.add(subtract);
		p2.add(multiply);
		p2.add(divide);
		
		
		
		//screen.setColumns(20);
		screen.setSize(200, 200);
		screen.setFont(largeFont);
		screen.setHorizontalAlignment (JTextField.RIGHT);//顯示區靠右
		p3.add(screen, BorderLayout.NORTH);
		p3.add(p1,BorderLayout.CENTER);
		p3.add(p2,BorderLayout.EAST);
		add(p3);
	}

	class OptionListener implements ActionListener {//給運算元按鈕的ActionListener
        public void actionPerformed(ActionEvent e) {
        	if(e.getSource() ==reset){//reset按鈕有最大優先權 所以先處理
    			total=0;
    			screen.setText("0");
    		}
        	
        	else if(start){//其餘的按鈕則檢查start是否為true true:現在應該輸入數字
        		if(preOP==equate){//只有之前按下等於按鈕跟之後才有可能不是接數字
        			screen.setText(""+total);//所以繼續印出total 不清空 以免使用者還要繼續運算
        		}
        		else{//其餘的則為錯誤輸入
        			screen.setText("0");//清空total
        			total=0;
        			
        		}
        	}
        	else if(!start)//start不為true表示之前的輸入是數字 而數字之後輸入運算元是正常情況
        	{
        		 if(e.getSource() == equate ){//如果目前的按鈕是等號
        			if(preOP==add)//若之前的按鈕是加號
        				total+=converter(screen.getText());//則將screen上的數字加到total中
        			else if(preOP==subtract)//同上
        				total-=converter(screen.getText());
        			else if(preOP==multiply)
        				total*=converter(screen.getText());
        			else if(preOP==divide)
        				total/=converter(screen.getText());
        			else
        				total=converter(screen.getText());
     
        		}
        		else if(e.getSource() == add ){//目前的按鈕是加號
        			if(total==0)//如果第一次輸入數字後按了加號鍵
        				total=converter(screen.getText());//則將screen上的數字轉從字串轉成數字 存在total中
        			else//如果之前已輸入過鍋的運算
        				total+=converter(screen.getText());//則姜screen上的數字加到total中
        		}
        		
        		else if(e.getSource() == subtract ){
        			if(total==0)
        				total=converter(screen.getText());
        			else
        				total-=converter(screen.getText());
        		}
        		else if(e.getSource() == multiply ){
        			if(total==0)
        				total=converter(screen.getText());
        			else
        				total*=converter(screen.getText());
        		}
        		else if(e.getSource() == divide){
        			if(total==0)
        				total=converter(screen.getText());
        			else
        				total/=converter(screen.getText());
        		}		
        		screen.setText(""+total);//顯示total
        	}
        	preOP=e.getSource();//把現在的運算元暫存起來
        	start=true;//start轉成數字 下一次要輸入數字
        }
        public int converter(String s){//字串轉數字
        	return Integer.parseInt(s);
        }
	}
	
	class NumberListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
        	String digit = e.getActionCommand();//取得數字按鈕的字串
        	if(start){//數字的第一位
        		if(preOP==equate)
        			screen.setText("");
        		screen.setText(digit);
        		start=false;
        	}
        	else//第一位之後
        	{
        		screen.setText(screen.getText()+digit);
        	}
        }
	}
}
