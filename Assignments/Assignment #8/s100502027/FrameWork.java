package a8.s100502027;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class FrameWork extends JFrame implements ActionListener{
	private JLabel screencontain = new JLabel("") ;
	private JButton[] JB = new JButton[20];
	public FrameWork(){
		Font Big = new Font("TimesRoman",Font.BOLD,40);  //  let the String of button bigger 
		Font screenshow = new Font("TimesRoman",Font.BOLD,30);  // to the screen
		Border linesblack = new LineBorder(Color.BLACK,2);
		Border linesgray = new LineBorder(Color.GRAY,2);
		screencontain.setBorder(linesgray);
		screencontain.setFont(screenshow);
		for(int t=0;t<20;t++){      //Declare the buttons  , draw color ,and (set font and border)
			JB[t]=new JButton("");
			JB[t].setFont(Big);
			JB[t].setBackground(new Color(215,255,254));
			JB[t].setBorder(linesblack);
			JB[t].addActionListener(this);
		}
		setLayout(new GridLayout(3,1));   
		
		Panel PJB1 = new Panel();          //pane1 to  take the button of String is letter
		PJB1.setLayout(new GridLayout(1,10));
		for(int t=0;t<10;t++){
			PJB1.add(JB[t]);
		}
		
		Panel PJB2 = new Panel();         //pane2 to take the button of String is number
		PJB2.setLayout(new GridLayout(1,10));
		for(int t=10;t<20;t++){
			PJB2.add(JB[t]);
		}

		JB[0].setText("��");  // to set the String of button  
		JB[1].setText("%");
		JB[2].setText("�U");
		JB[3].setText("�k");
		JB[4].setText(".");
		JB[5].setText("+");
		JB[6].setText("-");
		JB[7].setText("*");
		JB[8].setText("/");
		JB[9].setText("=");
		JB[10].setText("0");
		JB[11].setText("1");
		JB[12].setText("2");
		JB[13].setText("3");
		JB[14].setText("4");
		JB[15].setText("5");
		JB[16].setText("6");
		JB[17].setText("7");
		JB[18].setText("8");
		JB[19].setText("9");
		add(screencontain);
		add(PJB1);
		add(PJB2);
	}
	
	public void  actionPerformed(ActionEvent e){  //When the Button is clicked , get the string of the Button and then turn to the method
		handleString(e.getActionCommand());
	}
	
	public void handleString(String letter){   //get the string of screen now , and then  set the String of (screen + get )
		String temp = screencontain.getText();
		screencontain.setText(temp+letter);
	}
}
