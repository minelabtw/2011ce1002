package a8.s100502516;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;

public class FrameWork extends JFrame {
	protected JButton[] jbtNum = new JButton[9];
	protected JButton jbtZero = new JButton();
	protected JButton jbtDot = new JButton();
	protected JButton jbtPlus = new JButton();
	protected JButton jbtMinus = new JButton();
	protected JButton jbtMulti = new JButton();
	protected JButton jbtDivi = new JButton();
	protected JButton jbtEqual = new JButton();
	protected JButton jbtC = new JButton();
	protected JButton jbtDelete = new JButton();
	protected JLabel jlelDisplay = new JLabel("0");
	protected Font fontB20 = new Font("serif", Font.BOLD, 20);
	protected Font fontB30 = new Font("serif", Font.BOLD, 30);	
	
	public FrameWork()
	{
		InitButton();
		
		JPanel p1 = new JPanel();		
		SetNumPanel(p1);
		
		JPanel p2 = new JPanel();
		SetOperatorPanel(p2);
		
		setLayout(new GridBagLayout());		
		
		jlelDisplay.setFont(fontB30);	
		jlelDisplay.setHorizontalAlignment(SwingConstants.RIGHT);
		jlelDisplay.setOpaque(true);
		jlelDisplay.setBackground(Color.WHITE);
		
		
		GridBagConstraints c0 = new GridBagConstraints();	//add property
		c0.gridx = 0;
		c0.gridy = 0;
		c0.gridwidth = 5;
		c0.gridheight = 1;
		c0.weightx = 1;
		c0.weighty = 1;
		c0.fill = GridBagConstraints.BOTH;
		c0.anchor = GridBagConstraints.CENTER;
		add(jlelDisplay, c0);
		
		GridBagConstraints c1 = new GridBagConstraints();
		c1.gridx = 0;
		c1.gridy = 1;
		c1.gridwidth = 3;
		c1.gridheight = 4;
		c1.weightx = 2;
		c1.weighty = 1;
		c1.fill = GridBagConstraints.BOTH;
		c1.anchor = GridBagConstraints.CENTER;
		add(p1, c1);
		
		GridBagConstraints c2 = new GridBagConstraints();
		c2.gridx = 3;
		c2.gridy = 1;
		c2.gridwidth = 2;
		c2.gridheight = 4;
		c2.weightx = 1;
		c2.weighty = 1;
		c2.fill = GridBagConstraints.BOTH;
		c2.anchor = GridBagConstraints.CENTER;
		add(p2, c2);
	}
	public void InitButton()
	{
		for(int i = 0; i < 9; i++)
		{
			String keep = String.valueOf(i+1);			
			char input = keep.charAt(0);
			
			jbtNum[i] = new JButton(String.valueOf(i+1));
			jbtNum[i].setFont(fontB20);
			jbtNum[i].addActionListener(new ButtonActionListener(input));
		}
		
		jbtZero.setText("0");
		jbtZero.setFont(fontB20);
		jbtZero.addActionListener(new ButtonActionListener('0'));
		jbtDot.setText(".");
		jbtDot.setFont(fontB20);
		jbtDot.addActionListener(new ButtonActionListener('.'));
		jbtPlus.setText("+");
		jbtPlus.setFont(fontB20);
		jbtPlus.addActionListener(new ButtonActionListener('+'));
		jbtMinus.setText("-");
		jbtMinus.setFont(fontB20);
		jbtMinus.addActionListener(new ButtonActionListener('-'));
		jbtMulti.setText("*");
		jbtMulti.setFont(fontB20);
		jbtMulti.addActionListener(new ButtonActionListener('*'));
		jbtDivi.setText("/");
		jbtDivi.setFont(fontB20);
		jbtDivi.addActionListener(new ButtonActionListener('/'));
		jbtEqual.setText("=");
		jbtEqual.setFont(fontB20);
		jbtEqual.addActionListener(new ButtonActionListener('='));
		jbtDelete.setText("��");
		jbtDelete.setFont(fontB20);
		jbtDelete.addActionListener(new ButtonActionListener('��'));
		jbtC.setText("C");
		jbtC.setFont(fontB20);
		jbtC.addActionListener(new ButtonActionListener('C'));
	}
	public void SetNumPanel(JPanel p)	//I use panel because assignment require previously, originally I didn't want to use it.
	{
		p.setLayout(new GridBagLayout());
		
		for(int i = 0; i < 9; i++)
		{
			GridBagConstraints c0 = new GridBagConstraints();
			c0.gridx = i % 3;
			c0.gridy = i / 3;
			c0.gridwidth = 1;
			c0.gridheight = 1;
			c0.weightx = 1;
			c0.weighty = 1;
			c0.fill = GridBagConstraints.BOTH;
			c0.anchor = GridBagConstraints.CENTER;
			p.add(jbtNum[i], c0);
		}
		
		GridBagConstraints c1 = new GridBagConstraints();
		c1.gridx = 0;
		c1.gridy = 3;
		c1.gridwidth = 2;
		c1.gridheight = 1;
		c1.weightx = 1;
		c1.weighty = 1;
		c1.fill = GridBagConstraints.BOTH;
		c1.anchor = GridBagConstraints.CENTER;
		p.add(jbtZero, c1);
		
		GridBagConstraints c2 = new GridBagConstraints();
		c2.gridx = 2;
		c2.gridy = 3;
		c2.gridwidth = 1;
		c2.gridheight = 1;
		c2.weightx = 1;
		c2.weighty = 1;
		c2.fill = GridBagConstraints.BOTH;
		c2.anchor = GridBagConstraints.CENTER;
		p.add(jbtDot, c2);
	}
	public void SetOperatorPanel(JPanel p)
	{
		p.setLayout(new GridBagLayout());
		
		GridBagConstraints c0 = new GridBagConstraints();
		c0.gridx = 0;
		c0.gridy = 0;
		c0.gridwidth = 1;
		c0.gridheight = 1;
		c0.weightx = 1;
		c0.weighty = 1;
		c0.fill = GridBagConstraints.BOTH;
		c0.anchor = GridBagConstraints.CENTER;
		p.add(jbtDelete, c0);
		
		GridBagConstraints c1 = new GridBagConstraints();
		c1.gridx = 1;
		c1.gridy = 0;
		c1.gridwidth = 1;
		c1.gridheight = 1;
		c1.weightx = 1;
		c1.weighty = 1;
		c1.fill = GridBagConstraints.BOTH;
		c1.anchor = GridBagConstraints.CENTER;
		p.add(jbtC, c1);
		
		GridBagConstraints c2 = new GridBagConstraints();
		c2.gridx = 0;
		c2.gridy = 1;
		c2.gridwidth = 1;
		c2.gridheight = 1;
		c2.weightx = 1;
		c2.weighty = 1;
		c2.fill = GridBagConstraints.BOTH;
		c2.anchor = GridBagConstraints.CENTER;
		p.add(jbtPlus, c2);
		
		GridBagConstraints c3 = new GridBagConstraints();
		c3.gridx = 1;
		c3.gridy = 1;
		c3.gridwidth = 1;
		c3.gridheight = 1;
		c3.weightx = 1;
		c3.weighty = 1;
		c3.fill = GridBagConstraints.BOTH;
		c3.anchor = GridBagConstraints.CENTER;
		p.add(jbtMinus, c3);
		
		GridBagConstraints c4 = new GridBagConstraints();
		c4.gridx = 0;
		c4.gridy = 2;
		c4.gridwidth = 1;
		c4.gridheight = 1;
		c4.weightx = 1;
		c4.weighty = 1;
		c4.fill = GridBagConstraints.BOTH;
		c4.anchor = GridBagConstraints.CENTER;
		p.add(jbtMulti, c4);
		
		GridBagConstraints c5 = new GridBagConstraints();
		c5.gridx = 0;
		c5.gridy = 3;
		c5.gridwidth = 1;
		c5.gridheight = 1;
		c5.weightx = 1;
		c5.weighty = 1;
		c5.fill = GridBagConstraints.BOTH;
		c5.anchor = GridBagConstraints.CENTER;
		p.add(jbtDivi, c5);
		
		GridBagConstraints c6 = new GridBagConstraints();
		c6.gridx = 1;
		c6.gridy = 2;
		c6.gridwidth = 1;
		c6.gridheight = 2;
		c6.weightx = 1;
		c6.weighty = 1;
		c6.fill = GridBagConstraints.BOTH;
		c6.anchor = GridBagConstraints.CENTER;
		p.add(jbtEqual, c6);
	}

	class ButtonActionListener implements ActionListener 	//inner class to set action listener
	{
		private char input = 0;		
		private Calculator calculator = new Calculator();
		
		public ButtonActionListener(char input)
		{	
			this.input = input;		
		}
		public void actionPerformed(ActionEvent e)
		{			
			switch(input)
			{
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
				case '0':					
				case '.':
					calculator.SetOperaChangeFlag(false);
					calculator.SetInput(input);
					break;	
					
				case '+':
				case '-':
				case '*':	
				case '/':
					if(calculator.GetOperaChangeFlag())
					{
						calculator.ChangeOperator(input);
						break;
					}
					else if(calculator.GetOperatorFlag())
						calculator.Caculate();
					else if(calculator.getOutput().length() == 1 && calculator.getOutput().charAt(0) == '0' && (input == '+' || input == '-'))
					{
						calculator.SetInput(input);		
						break;
					}
					else
					{
						calculator.SetOperaChangeFlag(true);
						calculator.SetOperatorFlag(true);
					}
					calculator.SetInput(input);					
					break;
					
				case '=':
					if(calculator.GetOperaChangeFlag() || !calculator.GetOperatorFlag())
					{
						calculator.Reset();
						calculator.SetOperaChangeFlag(false);
						calculator.SetOperatorFlag(false);
					}					
					else
					{
						calculator.Caculate();
						calculator.SetEqulFlag(true);
						calculator.SetOperaChangeFlag(false);
						calculator.SetOperatorFlag(false);
					}
					break;
					
				case '��':
					if(calculator.getOutput().charAt(calculator.getOutput().length()-1) == '+' || 
					calculator.getOutput().charAt(calculator.getOutput().length()-1) == '-' ||
					calculator.getOutput().charAt(calculator.getOutput().length()-1) == '*' ||
					calculator.getOutput().charAt(calculator.getOutput().length()-1) == '/')
					{
						calculator.SetOperaChangeFlag(false);
						calculator.SetOperatorFlag(false);
					}
					calculator.DeleteOne();
					break;
					
				case 'C':
					calculator.Reset();
					calculator.SetOperaChangeFlag(false);
					calculator.SetOperatorFlag(false);
					break;			
			}
			
			jlelDisplay.setText(calculator.getOutput());
		}
	}
}
