package a8.s100502516;

import javax.swing.JFrame;

public class A81 {
	public static void main(String[] args)
	{
		FrameWork frame = new FrameWork();
		frame.setSize(300, 400);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}
