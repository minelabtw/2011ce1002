package a8.s100502516;

public class Calculator {	//the class to calculate
	private static String output = new String();	
	private static boolean equalFlag;	//enter equal button
	private static boolean operatorFlag;	//enter operator button
	private static boolean operaChangeFlag;	//last enter is operator button
		
	public Calculator()
	{		
		if(output.length() <= 0)
		{
			output += '0';
			equalFlag = false;	
			operatorFlag = false;
			operaChangeFlag = false;
		}		
	}
	public void SetInput(char input)
	{
		if(equalFlag)
		{			
			Reset();
			equalFlag = false;
		}
		
		if(output.length() == 1 && output.charAt(0) == '0' && input != '.' && input != '*' && input != '/')
		{
			output = "";
			output += input;
		}
		else
			output += input;
	}
	public void Caculate()	//be responsible for calculate
	{
		String keep1 = "";
		String keep2 = "";		
		double num1 = 0;
		double num2 = 0;
		char operator = 0;
		double result = 0;
		boolean change = false;
		
		for(int i = 0; i < output.length(); i++)
		{
			if(!change)
			{
				if(i == 0 || (output.charAt(i) != '+' && output.charAt(i) != '-' && output.charAt(i) != '*' && output.charAt(i) != '/'))
					keep1 += output.charAt(i);
				else
				{
					 change = true;
					 operator = output.charAt(i);
				}
			}
			else
				keep2 += output.charAt(i);
		}
		
		output = "";
		
		num1 = Double.parseDouble(keep1);
		num2 = Double.parseDouble(keep2);
		
		switch(operator)
		{
			case '+':
				result = num1 + num2;
				break;
				
			case '-':
				result = num1 - num2;
				break;
				
			case '*':
				result = num1 * num2;
				break;
				
			case '/':
				result = num1 / num2;
				break;				
		}		
		
		output = String.valueOf(result);	
		
		if(output.charAt(output.length()-1) == '0' && output.charAt(output.length()-2) == '.')
		{
			int resultKeep = (int) result;
			output = String.valueOf(resultKeep);
		}
	}
	public void DeleteOne()
	{			
		if(output.length() > 1)
		{
			char[] keep = new char[output.length()-1];
			
			for(int i = 0; i < output.length()-1; i++)
				keep[i] = output.charAt(i);
			
			String str = new  String(keep);
			output = str;
		}		
		else if(output.length() <= 1)
			output = "0";		
	}
	public void Reset()
	{
		output = "0";		
	}
	public String getOutput()
	{		
		return output;
	}
	public void ChangeOperator(char opera)
	{
		char[] keep = new char[output.length()-1];
		
		for(int i = 0; i < output.length()-1; i++)
			keep[i] = output.charAt(i);
		
		String str = new  String(keep);
		output = str + opera;
	}
	public void SetEqulFlag(boolean flag)
	{		
		equalFlag = flag;
	}
	public void SetOperatorFlag(boolean flag)
	{
		operatorFlag = flag;
	}
	public void SetOperaChangeFlag(boolean flag)
	{
		operaChangeFlag = flag;
	}
	public boolean GetOperatorFlag()
	{
		return operatorFlag;
	}
	public boolean GetOperaChangeFlag()
	{
		return operaChangeFlag;
	}
}
