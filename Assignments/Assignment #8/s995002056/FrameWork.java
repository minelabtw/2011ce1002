 package a8.s995002056;
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener{
	
	//宣告新的物件
	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();
	JLabel sc = new JLabel();
	JButton l1 = new JButton("1");
	JButton l2 = new JButton("2");
	JButton l3 = new JButton("3");
	JButton l4 = new JButton("4");
	JButton l5 = new JButton("5");
	JButton l6 = new JButton("6");
	JButton l7 = new JButton("7");
	JButton l8 = new JButton("8");
	JButton l9 = new JButton("9");
	JButton l0 = new JButton("0");
	JButton lz = new JButton("C");
	JButton la = new JButton("+");
	JButton ls = new JButton("-");
	JButton lm = new JButton("*");
	JButton lc = new JButton("/");
	JButton ld = new JButton(".");

	//螢幕上的字串
	String scs = " ";
	    
	public FrameWork(){
		//字形
		Font lb = new Font("TimesRoman",Font.BOLD,50);
		p1.setLayout(new GridLayout(1,1));
		//螢幕的數值
		p1.setBackground(Color.white);
		sc.setFont(lb);
		sc.setText(scs);	//讓螢幕初始為一個空白,才會跑出來
		p1.add(sc);
		add(p1,BorderLayout.NORTH);
		
		//將按鈕設定ActionListener
		l1.addActionListener(this);
		l2.addActionListener(this);
		l3.addActionListener(this);
		l4.addActionListener(this);
		l5.addActionListener(this);
		l6.addActionListener(this);
		l7.addActionListener(this);
		l8.addActionListener(this);
		l9.addActionListener(this);
		l0.addActionListener(this);
		la.addActionListener(this);
		ls.addActionListener(this);
		lm.addActionListener(this);
		lc.addActionListener(this);
		lz.addActionListener(this);
		ld.addActionListener(this);
		
		//把按鈕加到Panel
		p2.setLayout(new GridLayout(4,4));
		p2.add(l1);
		p2.add(l2);
		p2.add(l3);
		p2.add(la);
		p2.add(l4);
		p2.add(l5);
		p2.add(l6);
		p2.add(ls);
		p2.add(l7);
		p2.add(l8);
		p2.add(l9);
		p2.add(lm);
		p2.add(lz);
		p2.add(l0);
		p2.add(ld);
		p2.add(lc);
		add(p2,BorderLayout.CENTER);
		
	}
	
	//按下按鈕的event
	public void actionPerformed(ActionEvent e){
		
		if(e.getSource() == l1){
			scs += "1";			//將字串+上新的字元
			sc.setText(scs);	//把字串丟進Label(screen)
		}
		if(e.getSource() == l2){
			scs += "2";
			sc.setText(scs);		
		}
		if(e.getSource() == l3){
			scs += "3";
			sc.setText(scs);
		}
		if(e.getSource() == l4){
			scs += "4";
			sc.setText(scs);
		}
		if(e.getSource() == l5){
			scs += "5";
			sc.setText(scs);
		}
		if(e.getSource() == l6){
			scs += "6";
			sc.setText(scs);
		}
		if(e.getSource() == l7){
			scs += "7";
			sc.setText(scs);
		}
		if(e.getSource() == l8){
			scs += "8";
			sc.setText(scs);
		}
		if(e.getSource() == l9){
			scs += "9";
			sc.setText(scs);
		}
		if(e.getSource() == l0){
			scs += "0";
			sc.setText(scs);
		}
		if(e.getSource() == lz){
			scs = " ";			//把字串清除
			sc.setText(scs);
		}
		if(e.getSource() == la){
			scs += "+";
			sc.setText(scs);
		}
		if(e.getSource() == ls){
			scs += "-";
			sc.setText(scs);
		}
		if(e.getSource() == lm){
			scs += "*";
			sc.setText(scs);
		}
		if(e.getSource() == lc){
			scs += "/";
			sc.setText(scs);
		}
		if(e.getSource() == ld){
			scs += ".";
			sc.setText(scs);
		}
	}
}
