package a7.s100502023;

import java.math.*;

public class User 
{
	protected int [] card = new int [5];  //組牌的點數
	protected int [] kindOfCard = new int [5];  //組牌的花色
	protected String type = new String();  //result : type
	protected int priority;  //判斷誰贏 1~6 
	
	//card : A=0; 2=1; J=2; Q=3; K=4 ; Joker=5;
	//kindOfCard : 梅花=0; 方塊=1; 紅心=2; 黑桃=3; 
	
	protected int [] [] statistics_number_Value_Kind = new int [6] [4];
	//紀錄選重的牌的張數
	
	/*
		陣列:statistics_number_Value_Kind (6*4)
		
			梅花		方塊		紅心		黑桃
			0		1		2		3 
	A	0			
	2	1
	J	2
	Q	3
	K	4
Joker	5   o       x       x       x
	*/
	
	public User()
	{
	  //constructor	
	}
	
	public void accessTheCard ()
	{
		random();  //random card
		check_repeat();  //check repeat or not
		
		initialize_statistics_number_Value_Kind();  //紀錄歸0
		checkandSetTypeOfCards();  //檢查type
		setPriority();  //紀錄優勝順序
	}
	
	
	public void random ()
	{
		for (int i=0;i<5;i++)
		{
			card[i]=(int)((Math.random()*60)%6);
			
			if(card[i]==5)  //joker 設只存在梅花 joker
			{
				kindOfCard[i]=0;
			}
			else
			{
				kindOfCard[i]=(int)((Math.random()*40)%4);
			}
			
		}	
	}
	
	public void repeat_random(int j)  //check repeat
	{
		card[j]=(int)((Math.random()*60)%6);
		
		if(card[j]==5)  //joker
		{
			kindOfCard[j]=0;
		}
		else
		{
			kindOfCard[j]=(int)((Math.random()*40)%4);
		}
	}
	
	public void check_repeat() 
	{
		boolean finished = false;
		
		while(!finished)
		{
			boolean is_repeat = false;
			for (int i=0;i<5;i++)
			{
				for (int j=4;j>i;j--)
				{
					if (card[i]==5 && card[j]==5)  //joker
					{
						is_repeat=true;
						repeat_random(j);
					}
					else if (card[i]==card[j] && kindOfCard[i]==kindOfCard[j])
					{
						//repeat
						is_repeat=true;
						repeat_random(j);
					}
				}
			}
			if (is_repeat==false)
			{
				finished=true;
			}
		}
	}
	
	public void calculate_numberOfCard_and_KindOfCard(int card,int kindOfCard)
	{  //記錄張數
		++statistics_number_Value_Kind[card][kindOfCard];
	}
	

	public void initialize_statistics_number_Value_Kind()
	{  //紀錄歸0
		for(int i=0;i<6;i++)
		{
			for(int j=0;j<4;j++)
			{
				statistics_number_Value_Kind[i][j]=0;
			}
		}
	}
	
	public void checkandSetTypeOfCards()
	{
		int value_two_same=0;  //二張牌值相同;預設:0對
		boolean value_three_same=false;  //三張牌值相同
		boolean value_four_same=false;  //四張牌值相同
		int numberOfSameValue=0;
		
		initialize_statistics_number_Value_Kind();
		
		for(int i=0;i<5;i++)
		{
			calculate_numberOfCard_and_KindOfCard(card[i],kindOfCard[i]);
		}
		
		/*		
				梅花		方塊		愛心		黑桃
				0		1		2		3 
		A	0			
		2	1
		J	2
		Q	3
		K	4
	Joker	5   o       x       x       x
		*/
	

		//Four of a kind ; Full House ; Three of kind ; Two pairs ; Nothing
			
		for (int i=0;i<5;i++)
		{
			for(int j=0;j<4;j++)
			{
				numberOfSameValue+=statistics_number_Value_Kind[i][j];
			}
				
			if(numberOfSameValue==2)
			{
				value_two_same+=1;
			}
			else if(numberOfSameValue==3)
			{
				value_three_same=true;
			}
			else if(numberOfSameValue==4)
			{
				value_four_same=true;
			}
				
			numberOfSameValue=0;//歸0
		}
			
		if (statistics_number_Value_Kind[5][0]!=0)
		{
			type=" Joker ";	
		}
		else if (value_four_same)
		{
			type=" Four of a kind ";
		}
		else if (value_two_same==1 && value_three_same)
		{
			type=" Full House ";
		}
		else if (value_three_same && value_two_same==0)
		{
			type=" Three of kind ";
		}
		else if (value_two_same==2)
		{
			type=" Two pairs ";
		}
		else
		{
			type=" Nothing ";
		}
	}
	
	public String getTypeOfCards()  //回傳type值
	{
		return type;
	}
	
	
	public void setPriority()
	{  //優勝順序
		if (type==" Joker ")
		{
			priority=1;
		}
		else if (type==" Four of a kind ")
		{
			priority=2;
		}
		else if (type==" Full House ")
		{
			priority=3;
		}
		else if (type==" Three of kind ")
		{
			priority=4;
		}
		else if (type==" Two pairs ")
		{
			priority=5;
		}
		else
		{
			priority=6;
		}
	}
	
	public int getPriority()
	{  //return priority
		return priority;
	}	
}
