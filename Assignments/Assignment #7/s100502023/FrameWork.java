package a7.s100502023;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

public class FrameWork extends JFrame implements ActionListener
{
	private JPanel p1 = new JPanel (new GridLayout(2,7,5,5));   //card,result
	private JPanel p2 = new JPanel (new GridLayout(1,2,5,5));   //random botton,final result
	private JPanel p3 = new JPanel (new GridLayout(2,1,5,5));	//�^�Op1,p3
	
	private JLabel label_1 = new JLabel("He");
	
	private JLabel [] set_1_of_card = new JLabel [5];  //�Ĥ@�յP

	private JLabel result_He = new JLabel();
	
	private JLabel label_2 = new JLabel("You");  //�ĤG�յP
	
	private JLabel [] set_2_of_card = new JLabel [5];

	private JLabel result_You = new JLabel();
	
	private JButton button = new JButton("random");  //random botton
	private JLabel result_final = new JLabel();
	
	private ImageIcon [] [] card_Value_Kind = new ImageIcon [6][4];  //card_Value_Kind[6][4]
	private ImageIcon [] init_Image = new ImageIcon [5];             //init_Image[5]
	
	User he = new User();  //user class
	User you = new User();
	/*
		Image�}�C:card_Value_Kind (6*4)
		
			����		���		����		�®�
			0		1		2		3 
	A	0			
	2	1
	J	2
	Q	3
	K	4
Joker	5   o       x       x       x
	*/
	//--------------------------------------------------------------------
	private Font font1= new Font("SansSerif",Font.BOLD,24);
	private Font font2= new Font("SansSerif",Font.BOLD,80);
	private Border lineBorder = new LineBorder(Color.BLACK,2);
	private int card_Image_index=0;  //0~20
	private int init_Image_index=1;  //init_1~init_5
	
	public FrameWork()
	{
		// Image set
		for (int i=0;i<6;i++)
		{
			for (int j=0;j<4;j++)
			{
				if(i==5)
				{
					card_Value_Kind[i][0]=new ImageIcon("image/card/20.png");
					break;
				}
				else
				{
					card_Value_Kind[i][j]=new ImageIcon("image/card/" + card_Image_index + ".png");
					++card_Image_index;
				}
				
			}
		}
	
		for (int i=0;i<5;i++)
		{
			init_Image[i]=new ImageIcon("image/card/init_"+init_Image_index+".png");
			++init_Image_index;
		}
		
		//-------------------------------------------------------------------
		//label set
		
		label_1.setFont(font1);  //He
		label_2.setFont(font1);  //You
		
		for(int i=0;i<5;i++)  //card init_image
		{
			set_1_of_card[i] = new JLabel();
			set_1_of_card[i].setIcon(init_Image[i]);
			set_1_of_card[i].setBorder(lineBorder);
			
			set_2_of_card[i] = new JLabel();
			set_2_of_card[i].setIcon(init_Image[i]);
			set_2_of_card[i].setBorder(lineBorder);
		}

		result_He.setFont(font1);
		result_You.setFont(font1);
		result_final.setFont(font2);
		
		//add
		p1.add(label_1);
		for(int i=0;i<5;i++)
		{
			p1.add(set_1_of_card[i]);
		}
		p1.add(result_He);
		
		p1.add(label_2);
		for(int i=0;i<5;i++)
		{
			p1.add(set_2_of_card[i]);
		}
		p1.add(result_You);
				
		p2.add(button);
		p2.add(result_final);
		
		p3.add(p1);
		p3.add(p2);
		
		setLayout(new BorderLayout(2,4));
		add(p3,BorderLayout.CENTER);
		
		button.addActionListener(this); //active button event here
	}
	
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == button)
		{
			//what do you want when you push "random" button? Obviously, random card!
			he.accessTheCard();  //user class : accessTheCard() method
			you.accessTheCard();
			setCard(he,you);	//decide card image
			setResult(he,you);  //type of card
			setResult_final(he,you);  //win or lose
		}
	}
	
	public ImageIcon decideCard(int random_card,int random_typeOfCard)
	{
		int card=random_card;
		int kindOfCard=random_typeOfCard;
		ImageIcon decideIcon = null;
		
		//card : A=0; 2=1; J=2; Q=3; K=4 ; Joker=5;
		//kindOfCard : ����=0; �j��=1; ����=2; �®�=3; 
		/*
		Image�}�C:card_Value_Kind (6*4)
		
			����		���		�R��		�®�
			0		1		2		3 
	A	0			
	2	1
	J	2
	Q	3
	K	4
Joker	5   o       x       x       x
	*/
		//ImageIcon [] [] card_Value_Kind
		
		switch(card)
		{                 //simplify_decide function
			case 0:
				decideIcon=simplify_decide(0,kindOfCard);
				break;
			case 1:
				decideIcon=simplify_decide(1,kindOfCard);
				break;
			case 2:
				decideIcon=simplify_decide(2,kindOfCard);
				break;
			case 3:
				decideIcon=simplify_decide(3,kindOfCard);
				break;
			case 4:
				decideIcon=simplify_decide(4,kindOfCard);
				break;
			case 5://joker
				decideIcon=card_Value_Kind[5][0];
				break;
		}
		
		return decideIcon;
	}

	public ImageIcon simplify_decide(int card,int kindOfCard)
	{
		ImageIcon decideIcon_simplify = null;
	
		switch(kindOfCard)
		{
			//no joker
			case 0:
				decideIcon_simplify=card_Value_Kind[card][0];
				break;
			case 1:
				decideIcon_simplify=card_Value_Kind[card][1];
				break;
			case 2:
				decideIcon_simplify=card_Value_Kind[card][2];
				break;
			case 3:
				decideIcon_simplify=card_Value_Kind[card][3];
				break;
		}	
		return decideIcon_simplify;
	}
	
	public void setCard(User he,User you)
	{
		//user he
		//set_1_of_card_1
		for (int i=0;i<5;i++)
		{
			set_1_of_card[i].setIcon(decideCard(he.card[i],he.kindOfCard[i]));
		}
		
		//user you
		//set_1_of_card_1
		for (int i=0;i<5;i++)
		{
			set_2_of_card[i].setIcon(decideCard(you.card[i],you.kindOfCard[i]));
		}
		
	}
	
	public void setResult(User he ,User you)
	{
		result_He.setText("<"+he.getTypeOfCards()+">");
		result_You.setText("<"+you.getTypeOfCards()+">");
	}
	
	public void setResult_final(User he,User you)
	{
		if (he.getPriority()<you.getPriority())
		{
			result_final.setText("You lose");
		}
		else if(he.getPriority()>you.getPriority())
		{
			result_final.setText("You win");
		}
		else
		{
			result_final.setText("Peace");
		}
	}
	
	
}
