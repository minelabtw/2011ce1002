package a7.s100502023;

public class Password 
{
	protected String password = new String();
	protected int length;

	public Password(String input) throws InvalidPasswordException
	{
		if (!check_LengthOfPassword(input))//The length is not enough
		{
			InvalidPasswordException ex_length = new InvalidPasswordException("The length of the password "
					+ input + " is not enough!!",input);
			throw ex_length;
		}
		else if (!check_LetterofPassord(input))  //no letter
		{
			InvalidPasswordException ex_letter = new InvalidPasswordException("The password " 
					+ input + " has no letter!!",input);
			throw ex_letter;
		}
		else if (!check_NumberofPassord(input))  //no number
		{
			InvalidPasswordException ex_number = new InvalidPasswordException("The password " 
					+ input + " has no number!!",input);
			throw ex_number;
		}
		else if (check_LengthOfPassword(input) && check_LetterofPassord(input) && check_NumberofPassord(input))
		{  //valid
			password=input;
		}
	}
	
	public boolean check_LengthOfPassword(String input)  //check_Length
	{
		boolean is_valid;
		
		if (input.length()>=7)
		{
			is_valid=true;
		}
		else
		{
			is_valid=false;
		}
		return is_valid;
	}
	
	public boolean check_LetterofPassord(String input)  //check_Letter
	{
		boolean is_valid=false;
		
		for (int i=0;i<input.length();i++)
		{
			if (!Character.isLetter(input.charAt(i)))
			{
				//not valid
			}
			else
			{
				is_valid=true;
			}
		}
		
		return is_valid;
	}
	
	public boolean check_NumberofPassord(String input)  //check_Number
	{
		boolean is_valid=false;
		
		for (int i=0;i<input.length();i++)
		{
			if (!Character.isDigit(input.charAt(i)))
			{
				//not valid
			}
			else
			{
				is_valid=true;
			}
		}
		
		return is_valid;
	}
	
	public String getValidPassword()  //return password
	{
		return password;
	}
}
