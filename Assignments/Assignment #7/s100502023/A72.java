package a7.s100502023;

import java.util.*;

public class A72 
{
	public static void main(String[] args) 
	{	
		Scanner input = new Scanner(System.in);
		
		boolean is_success=false;
		
		do
		{
			System.out.println("Please input a password to varify: ");
			String password = input.nextLine();
			try //Exception
			{
				Password input_password = new Password(password);
				is_success=true;
				System.out.println("The password " + input_password.getValidPassword() + " is valid!!");
			} 
			catch (InvalidPasswordException ex) 
			{
				System.out.println(ex);
			}
			
		}while(!is_success);
		
	}

}
