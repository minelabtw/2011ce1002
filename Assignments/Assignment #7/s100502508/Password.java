package a7.s100502508;

public class Password 
{
	private String password;
	
	public Password(String receivepassword)
	{
		password=receivepassword;
		int counter=judge();
		
		try
		{
			if(counter==password.length()||counter==0||password.length()<7)
			{
				throw new InvalidPasswordException(judge(),password.length(),password);//error happen and throw to  InvalidPasswordException class
			}
			System.out.println("The password "+password+" is valid!!");
		}
		
		catch(Exception ex)//handle error
		{
			System.out.println("The password "+password+" is not correct!!");
		}
	}//end constructor
	
	public int judge()
	{
		int count=0;
		for(int a=0;a<password.length();a++)
		{
			if(Character.isLetter(password.charAt(a)))
			{
				count++;
			}
		}
		return count;
	}//end judge method
}
