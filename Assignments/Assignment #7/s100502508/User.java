package a7.s100502508;

import javax.swing.ImageIcon;

public class User 
{
	private ImageIcon[] Ace;//store Ace
	private ImageIcon[] Two;//store Two
	private ImageIcon[] Jack;//store Jack
	private ImageIcon[] Queen;//store Queen
	private ImageIcon[] King;//store King
	private ImageIcon Joker;
	private ImageIcon[] randomIcons;
	private ImageIcon[] arrayIcons;
	private String uptype;
	private String downtype;
	
	public User()//initialize arrays
	{
		Ace=new ImageIcon[4];
		Two=new ImageIcon[4];
		Jack=new ImageIcon[4];
		Queen=new ImageIcon[4];
		King=new ImageIcon[4];
		randomIcons=new ImageIcon[10];
		arrayIcons=new ImageIcon[21];
		store();//call the method of store
	}
	
	public void store()//store every image in arrays
	{
		for(int a=0;a<4;a++)
		{
			Ace[a]=new ImageIcon("image/card/"+a+".png");
			Two[a]=new ImageIcon("image/card/"+(a+4)+".png");
			Jack[a]=new ImageIcon("image/card/"+(a+8)+".png");
			Queen[a]=new ImageIcon("image/card/"+(a+12)+".png");
			King[a]=new ImageIcon("image/card/"+(a+16)+".png");
		}
		Joker=new ImageIcon("image/card/20.png");
		
		for(int a=0;a<21;a++)
		{
			if(a<4)
			{
				arrayIcons[a]=Ace[a];
			}
			else if(a>=4&&a<8)
			{
				arrayIcons[a]=Two[a-4];
			}
			else if(a>=8&&a<12)
			{
				arrayIcons[a]=Jack[a-8];
			}
			else if(a>=12&&a<16)
			{
				arrayIcons[a]=Queen[a-12];
			}
			else if(a>=16&&a<20)
			{
				arrayIcons[a]=King[a-16];
			}
			else 
			{
				arrayIcons[a]=Joker;
			}
		}
		
	}
	
	public void random()//do random and store ten cards in a array
	{
		for(int a=0;a<10;a++)
		{
			int number=(int)(Math.random()*21);
			if(a==0)
			{
				randomIcons[a]=arrayIcons[number];
			}
			else
			{
				for(int b=0;b<a;b++)
				{
					if(randomIcons[b]==arrayIcons[number])
					{
						a--;
						break;
					}
					else 
					{
						randomIcons[a]=arrayIcons[number];
					}
				}
			}
		}
	}
	
	public int Upjudge(ImageIcon[] card)//judge computer cards type
	{
		int count1=0;
		int count2=0;
		int count3=0;
		int count4=0;
		int count5=0;
		int count6=0;
		for(int a=0;a<5;a++)
		{
			for(int b=0;b<4;b++)
			{
				if(card[a]==Ace[b])
					count1++;
			}
			
			for(int b=0;b<4;b++)
			{
				if(card[a]==Two[b])
					count2++;
			}
			
			for(int b=0;b<4;b++)
			{
				if(card[a]==Jack[b])
					count3++;
			}
			
			for(int b=0;b<4;b++)
			{
				if(card[a]==Queen[b])
					count4++;
			}
			
			for(int b=0;b<4;b++)
			{
				if(card[a]==King[b])
					count5++;
			}
			
			for(int b=0;b<1;b++)
			{
				if(card[a]==Joker)
					count6++;
			}
		}
		if(count6==1)
		{
			uptype="Joker";
			return 6;
		}
		else if(count1==4||count2==4||count3==4||count4==4||count5==4)
		{
			uptype="Four of a kind";
			return 5;
		}
		else if(count1==3||count2==3||count3==3||count4==3||count5==3)
		{
			if(count1==2||count2==2||count3==2||count4==2||count5==2)
			{
				uptype="Full House";
				return 4;
			}
			else 
			{
				uptype="Three of kind";
				return 3;
			}
		}
		else if((count1==2&&(count2==2||count3==2||count4==2||count5==2))||(count2==2&&(count1==2||count3==2||count4==2||count5==2))||(count3==2&&(count2==2||count1==2||count4==2||count5==2))||(count4==2&&(count2==2||count3==2||count1==2||count5==2))||(count5==2)&&(count2==2||count3==2||count4==2||count1==2))
		{
			uptype="Two pairs";
			return 2;
		}
		else if(count1==2||count2==2||count3==2||count4==2||count5==2)
		{
			uptype="A pair";
			return 1;
		}
		else 
		{
			uptype="Nothing";
			return 0;
		}
	}
	public int Downjudge(ImageIcon[] card)//judge my cards type
	{
		int count1=0;
		int count2=0;
		int count3=0;
		int count4=0;
		int count5=0;
		int count6=0;
		for(int a=5;a<10;a++)
		{
			for(int b=0;b<4;b++)
			{
				if(card[a]==Ace[b])
					count1++;
			}
			
			for(int b=0;b<4;b++)
			{
				if(card[a]==Two[b])
					count2++;
			}
			
			for(int b=0;b<4;b++)
			{
				if(card[a]==Jack[b])
					count3++;
			}
			
			for(int b=0;b<4;b++)
			{
				if(card[a]==Queen[b])
					count4++;
			}
			
			for(int b=0;b<4;b++)
			{
				if(card[a]==King[b])
					count5++;
			}
			
			for(int b=0;b<1;b++)
			{
				if(card[a]==Joker)
					count6++;
			}
		}
		if(count6==1)
		{
			downtype="Joker";
			return 6;
		}
		else if(count1==4||count2==4||count3==4||count4==4||count5==4)
		{
			downtype="Four of a kind";
			return 5;
		}
		else if(count1==3||count2==3||count3==3||count4==3||count5==3)
		{
			if(count1==2||count2==2||count3==2||count4==2||count5==2)
			{
				downtype="Full House";
				return 4;
			}
			else 
			{
				downtype="Three of kind";
				return 3;
			}
		}
		else if((count1==2&&(count2==2||count3==2||count4==2||count5==2))||(count2==2&&(count1==2||count3==2||count4==2||count5==2))||(count3==2&&(count2==2||count1==2||count4==2||count5==2))||(count4==2&&(count2==2||count3==2||count1==2||count5==2))||(count5==2)&&(count2==2||count3==2||count4==2||count1==2))
		{
			downtype="Two pairs";
			return 2;
		}
		else if(count1==2||count2==2||count3==2||count4==2||count5==2)
		{
			downtype="A pair";
			return 1;
		}
		else 
		{
			downtype="Nothing";
			return 0;
		}
	}
	
	public String result(int upjudge,int downjudge)//judge who win
	{
		if(upjudge>downjudge)
			return "You lose";
		else if(upjudge<downjudge)
			return "You win";
		else 
			return "Duce";
	}
	
	public String getuptype()//get computer cards type
	{
		return uptype;
	}
	
	public String getdowntype()//get my cards type
	{
		return downtype;
	}
	
	public ImageIcon[] getrandomIcon()//get the random array
	{
		random();
		return randomIcons;
	}
	
}
