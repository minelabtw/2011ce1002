package a7.s100502508;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.*;

public class FrameWork extends JFrame implements ActionListener
{
	private ImageIcon Please = new ImageIcon("image/card/init_1.png");
	private ImageIcon Press = new ImageIcon("image/card/init_2.png");
	private ImageIcon Random = new ImageIcon("image/card/init_3.png");
	private ImageIcon Button = new ImageIcon("image/card/init_4.png");
	private ImageIcon Littlepon = new ImageIcon("image/card/init_5.png");
	private ImageIcon[] cardIcons;
	
	JButton button = new JButton("random"); //create button 
	
	JPanel p1=new JPanel(new GridLayout(1,6,5,10));//create panels and set GridLayout
	JPanel p2=new JPanel(new GridLayout(1,6,5,10));
	JPanel p3=new JPanel(new GridLayout(3,1,5,10));
	JPanel p4=new JPanel(new GridLayout(3,1,5,10));
	JPanel p5=new JPanel(new GridLayout(1,2,5,10));
	
	LineBorder lineBorder=new LineBorder(Color.BLACK,5);//create a line border
	JLabel word1=new JLabel();//create labels
	JLabel word2=new JLabel();
	JLabel word3=new JLabel();
	
	JLabel jLabel1=new JLabel(Please);//create labels and set image in
	JLabel jLabel2=new JLabel(Press);
	JLabel jLabel3=new JLabel(Random);
	JLabel jLabel4=new JLabel(Button);
	JLabel jLabel5=new JLabel(Littlepon);
	
	JLabel jLabel6=new JLabel(Please);//create labels and set image in
	JLabel jLabel7=new JLabel(Press);
	JLabel jLabel8=new JLabel(Random);
	JLabel jLabel9=new JLabel(Button);
	JLabel jLabel10=new JLabel(Littlepon);
	
	Font font=new Font("SansSerif",Font.BOLD,17);//create fonts
	Font font2=new Font("SansSerif",Font.BOLD,100);
	Font font3=new Font("SansSerif",Font.BOLD,50);
	
	User user=new User();//create a User object
	
	public FrameWork()
	{
		cardIcons=new  ImageIcon[10];
		
		word1=new JLabel("He");
		word1.setFont(font);
		p1.add(word1);//add labels to panel
		p1.add(jLabel1);
		p1.add(jLabel2);
		p1.add(jLabel3);
		p1.add(jLabel4);
		p1.add(jLabel5);
		p4.add(p1);
		word1=new JLabel("");
		p3.add(word1);
		
		word2=new JLabel("You");
		word2.setFont(font);
		p2.add(word2);//add labels to panel
		p2.add(jLabel6);
		p2.add(jLabel7);
		p2.add(jLabel8);
		p2.add(jLabel9);
		p2.add(jLabel10);
		p4.add(p2);
		word2=new JLabel("");
		p3.add(word2);
		
		jLabel1.setBorder(lineBorder);//set border
		jLabel2.setBorder(lineBorder);
		jLabel3.setBorder(lineBorder);
		jLabel4.setBorder(lineBorder);
		jLabel5.setBorder(lineBorder);
		jLabel6.setBorder(lineBorder);
		jLabel7.setBorder(lineBorder);
		jLabel8.setBorder(lineBorder);
		jLabel9.setBorder(lineBorder);
		jLabel10.setBorder(lineBorder);
		
		
		p4.add(button);//add button into panel
		word3=new JLabel("V.S.");
		word3.setFont(font2);
		p3.add(word3);//add labels to panel
		
		add(p4,BorderLayout.WEST);
		add(p3,BorderLayout.CENTER);
		
		button.addActionListener(this);//active button event here
		
	}
	
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == button)
		{
			cardIcons=user.getrandomIcon();//call getrandomIcon method in user class
			
			jLabel1.setIcon(cardIcons[0]);//set image
			jLabel2.setIcon(cardIcons[1]);
			jLabel3.setIcon(cardIcons[2]);
			jLabel4.setIcon(cardIcons[3]);
			jLabel5.setIcon(cardIcons[4]);
			jLabel6.setIcon(cardIcons[5]);
			jLabel7.setIcon(cardIcons[6]);
			jLabel8.setIcon(cardIcons[7]);
			jLabel9.setIcon(cardIcons[8]);
			jLabel10.setIcon(cardIcons[9]);
			
			word3.setText(user.result(user.Upjudge(cardIcons),user.Downjudge(cardIcons)));//call result method in user class
			word3.setFont(font2);//set font
			word1.setText("<"+user.getuptype()+">");//call getuptype method in user class
			word1.setFont(font3);
			word2.setText("<"+user.getdowntype()+">");//call getdowntype method in user class
			word2.setFont(font3);
		}
	}		
}