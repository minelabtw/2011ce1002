package a7.s100502508;

public class InvalidPasswordException extends Exception
{
	public InvalidPasswordException(int count,int length,String receivepassword) 
	{
		if(length<7)//length of password is shorter than seven
		{
			System.out.println("The length of the password "+receivepassword+" is not enough!!");
		}
		
		else 
		{
			if(count==0)//password has no letter
			{
				System.out.println("The password "+receivepassword+" has no letter!!");
			}
			else//password has no number 
			{
				System.out.println("The password "+receivepassword+" has no number!!");
			}
		}
	}//end InvalidPasswordException
}
