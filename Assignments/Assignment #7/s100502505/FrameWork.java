package a7.s100502505;

import java.applet.Applet;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.border.*;

public class FrameWork extends JFrame implements ActionListener{
	JButton button = new JButton("random"); //create button 
	JPanel up = new JPanel();//上面的panel
	JPanel down = new JPanel();//下面的panel
	JLabel He = new JLabel("He");
	JLabel You = new JLabel("You");
	JLabel He_type = new JLabel("");//牌的類別
	JLabel You_type = new JLabel("");//牌的類別
	JLabel result = new JLabel("");//結果
	JLabel pic1 = new JLabel();//圖片
	JLabel pic2 = new JLabel();
	JLabel pic3 = new JLabel();
	JLabel pic4 = new JLabel();
	JLabel pic5 = new JLabel();
	JLabel pic6 = new JLabel();
	JLabel pic7 = new JLabel();
	JLabel pic8 = new JLabel();
	JLabel pic9 = new JLabel();
	JLabel pic10 = new JLabel();
	Font user_font = new Font("serif", Font.BOLD, 20);//字體大小 字型
	Font result_font = new Font("serif", Font.BOLD, 100);
	Font button_font = new Font("serif", Font.BOLD, 120);
	ImageIcon card1 = new ImageIcon("image/card/init_1.png");//圖片
	ImageIcon card2 = new ImageIcon("image/card/init_2.png");
	ImageIcon card3 = new ImageIcon("image/card/init_3.png");
	ImageIcon card4 = new ImageIcon("image/card/init_4.png");
	ImageIcon card5 = new ImageIcon("image/card/init_5.png");
	
	public FrameWork (){
		add(up, BorderLayout.CENTER);//上面的panel
		add(down, BorderLayout.SOUTH);//下面的panel
		up.setLayout(new GridLayout(2,6,2,5));//排版
		up.add(He);
		pic1 = new JLabel(card1);//圖片
		pic1.setBorder(new LineBorder(Color.black,4));//設定邊框
		up.add(pic1);
		pic2 = new JLabel(card2);
		pic2.setBorder(new LineBorder(Color.black,4));
		up.add(pic2);
		pic3 = new JLabel(card3);
		pic3.setBorder(new LineBorder(Color.black,4));
		up.add(pic3);
		pic4 = new JLabel(card4);
		pic4.setBorder(new LineBorder(Color.black,4));
		up.add(pic4);
		pic5 = new JLabel(card5);
		pic5.setBorder(new LineBorder(Color.black,4));
		up.add(pic5);
		up.add(He_type);
		up.add(You);
		pic6 = new JLabel(card1);
		pic6.setBorder(new LineBorder(Color.black,4));
		up.add(pic6);
		pic7 = new JLabel(card2);
		pic7.setBorder(new LineBorder(Color.black,4));
		up.add(pic7);
		pic8 = new JLabel(card3);
		pic8.setBorder(new LineBorder(Color.black,4));
		up.add(pic8);
		pic9 = new JLabel(card4);
		pic9.setBorder(new LineBorder(Color.black,4));
		up.add(pic9);
		pic10 = new JLabel(card5);
		pic10.setBorder(new LineBorder(Color.black,4));
		up.add(pic10);
		up.add(You_type);
		He.setFont(user_font);//設定字體
		You.setFont(user_font);
		He_type.setFont(user_font);
		You_type.setFont(user_font);
		button.setFont(button_font);
		result.setFont(result_font);
		down.setLayout(new BorderLayout());//button和result的排版
		down.add(button,BorderLayout.WEST);
		down.add(result,BorderLayout.CENTER);

		button.addActionListener(this); //active button event here			
	}
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == button)
		{
			User He = new User();//He
			User You = new User();//You
			int card1 = User.random();//亂數
			int card2,card3,card4,card5,card6,card7,card8,card9,card10;
			while(true)//牌不能重複!
			{
				card2 = User.random();
				if(card1 != card2)
				{
					break;
				}
			}
			while(true)
			{
				card3 = User.random();
				if(card3 != card1 && card3 != card2)
				{
					break;
				}
			}
			while(true)
			{
				card4 = User.random();
				if(card4 != card1 && card4 != card2 && card4 != card3)
				{
					break;
				}
			}
			while(true)
			{
				card5 = User.random();
				if(card5 != card1 && card5 != card2 && card5 != card3 && card5 != card4)
				{
					break;
				}
			}
			while(true)
			{
				card6 = User.random();
				if(card6 != card1 && card6 != card2 && card6 != card3 && card6 != card4 && card6 != card5)
				{
					break;
				}
			}
			while(true)
			{
				card7 = User.random();
				if(card7 != card1 && card7 != card2 && card7 != card3 && card7 != card4 && card7 != card5 && card7 != card6)
				{
					break;
				}
			}
			while(true)
			{
				card8 = User.random();
				if(card8 != card1 && card8 != card2 && card8 != card3 && card8 != card4 && card8 != card5 && card8 != card6 && card8 != card7)
				{
					break;
				}
			}
			while(true)
			{
				card9 = User.random();
				if(card9 != card1 && card9 != card2 && card9 != card3 && card9 != card4 && card9 != card5 && card9 != card6 && card9 != card7 && card9 != card8)
				{
					break;
				}
			}
			while(true)
			{
				card10 = User.random();
				if(card10 != card1 && card10 != card2 && card10 != card3 && card10 != card4 && card10 != card5 && card10 != card6 && card10 != card7 && card10 != card8 && card10 != card9)
				{
					break;
				}
			}
			
			ImageIcon carda = new ImageIcon("image/card/" + card1 + ".png");//撲克牌
			ImageIcon cardb = new ImageIcon("image/card/" + card2 + ".png");
			ImageIcon cardc = new ImageIcon("image/card/" + card3 + ".png");
			ImageIcon cardd = new ImageIcon("image/card/" + card4 + ".png");
			ImageIcon carde = new ImageIcon("image/card/" + card5 + ".png");
			ImageIcon cardf = new ImageIcon("image/card/" + card6 + ".png");
			ImageIcon cardg = new ImageIcon("image/card/" + card7 + ".png");
			ImageIcon cardh = new ImageIcon("image/card/" + card8 + ".png");
			ImageIcon cardi = new ImageIcon("image/card/" + card9 + ".png");
			ImageIcon cardj = new ImageIcon("image/card/" + card10 + ".png");
			pic1.setIcon(carda);
			pic2.setIcon(cardb);
			pic3.setIcon(cardc);
			pic4.setIcon(cardd);
			pic5.setIcon(carde);
			pic6.setIcon(cardf);
			pic7.setIcon(cardg);
			pic8.setIcon(cardh);
			pic9.setIcon(cardi);
			pic10.setIcon(cardj);
			
			if(He.Type(card1,card2,card3,card4,card5) == 0)//看牌的類別
			{
				He_type.setText("<Nothing>");
			}else if(He.Type(card1,card2,card3,card4,card5) == 1)
			{
				He_type.setText("<Two pairs>");
			}else if(He.Type(card1,card2,card3,card4,card5) == 2)
			{
				He_type.setText("<Three of a kind>");
			}else if(He.Type(card1,card2,card3,card4,card5) == 3)
			{
				He_type.setText("<Full house>");
			}else if(He.Type(card1,card2,card3,card4,card5) == 4)
			{
				He_type.setText("<Four of a kind>");
			}else if(He.Type(card1,card2,card3,card4,card5) == 5)
			{
				He_type.setText("<Joker>");
			}
			
			if(You.Type(card6,card7,card8,card9,card10) == 0)//看牌的類別
			{
				You_type.setText("<Nothing>");
			}else if(You.Type(card6,card7,card8,card9,card10) == 1)
			{
				You_type.setText("<Two pairs>");
			}else if(You.Type(card6,card7,card8,card9,card10) == 2)
			{
				You_type.setText("<Three of a kind>");
			}else if(You.Type(card6,card7,card8,card9,card10) == 3)
			{
				You_type.setText("<Full house>");
			}else if(You.Type(card6,card7,card8,card9,card10) == 4)
			{
				You_type.setText("<Four of a kind>");
			}else if(You.Type(card6,card7,card8,card9,card10) == 5)
			{
				You_type.setText("<Joker>");
			}
			
			if(He.Type(card1,card2,card3,card4,card5) > You.Type(card6,card7,card8,card9,card10))
			{//比較誰輸誰贏
				result.setText("He wins!!");
			}else if(He.Type(card1,card2,card3,card4,card5) == You.Type(card6,card7,card8,card9,card10))
			{
				result.setText("Draw!!");
			}else{
				result.setText("You win!!");
			}
		}
	}
}//完成!!

