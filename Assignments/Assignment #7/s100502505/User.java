package a7.s100502505;

import java.util.Random;

public class User {
	static Random random = new Random();
	static int type;//牌的類別
	static int card[] = new int[5];//五張牌
	
	public static int pokers(int cards)
	{//不比花色,只看大小
		int poker=0;
		if (cards == 0 || cards == 1 || cards == 2 || cards == 3) {
			poker = 1;
		} else if (cards == 4 || cards == 5 || cards == 6 || cards == 7) {
			poker = 2;
		} else if (cards == 8 || cards == 9 || cards == 10 || cards == 11) {
			poker = 11;
		} else if (cards == 12 || cards == 13 || cards == 14 || cards == 15) {
			poker = 12;
		} else if (cards == 16 || cards == 17 || cards == 18 || cards == 19) {
			poker = 13;
		} else if (cards == 20) {
			poker = 100;
		}
		return poker; 
	}
	
	public static int Type(int card1,int card2,int card3,int card4,int card5)//看牌的類別
	{
		card[0] = pokers(card1);
		card[1] = pokers(card2);
		card[2] = pokers(card3);
		card[3] = pokers(card4);
		card[4] = pokers(card5);
		
		int time = 0;
		
		for(int i=0;i<4;i++)
		{
			for(int j=i+1;j<5;j++)
			{
				if(card[i] == card[j])//若有重複就+1
				{
					time++;
				}
			}
		}
		for(int i=0;i<5;i++)
		{
			if(card[i] == 100)//鬼牌最大
			{
				type = 5;
				break;
			}
			else if(time == 2 && card[i] != 100)//tow pairs
			{
				type = 1;
			}else if(time == 3 && card[i] != 100)//三條
			{
				type = 2;
			}else if(time == 4 && card[i] != 100)//葫蘆
			{
				type = 3;
			}else if(time == 6 && card[i] != 100)//鐵支
			{
				type = 4;
			}else{//什麼都沒有
				type = 0;
			}
		}
		return type;
	}
	
	public static int random()
	{
		int number = random.nextInt(21);//0~20的亂數
		return number;
	}
}
