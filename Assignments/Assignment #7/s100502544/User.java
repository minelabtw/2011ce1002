package a7.s100502544;

import javax.swing.ImageIcon;

public class User {
	public int computer[]=new int [5];//宣告computer陣列
	public int user[] =new int [5];//宣告user陣列
	public ImageIcon[] aa=new ImageIcon[21];//aa為一個陣列  存牌圖片
	public int comcounter=0;
	public int usercounter=0;
	public User(){
		ImageIcon[] bb=new ImageIcon[6];//宣告一個陣列  用來存圖片
		for(int i=1;i<=5;i++){
			bb[i]=new ImageIcon("image/card/init_"+i+".png");
		}
		
		for(int i=0;i<=20;i++){
			aa[i]=new ImageIcon("image/card/"+i+".png");
		}
		
		int number[]={1,1,1,1,2,2,2,2,11,11,11,11,12,12,12,12,13,13,13,13,20};
		//將圖片轉成代號
		
		
		int[] as=new int[10];// as 陣列存 "不重複" 的牌
		boolean seeTF=true;
		int num=0;
		while(num<10){
			as[num]=(int)(Math.random()*21);//隨機變數 決定牌
			seeTF=true;
	
			for(int j=0;j<num;j++){
				
				if(as[num]==as[j]){
					seeTF=false;
				}
			}
			if(seeTF==true){
				num++;
			}
		}
		for(int i =0;i<5;i++){//拿 as陣列  儲存到computer的牌
			computer[i]=as[i];
		}
		for(int i=5;i<10;i++){//拿 as陣列 儲存到user的牌
			user[i-5]=as[i];//*user是由0到4!!
		}
		
		
		for(int i=0;i<5;i++){
			if(number[computer[i]]==20){
				comcounter=20;//(判斷)如果computer陣列裡面有Joker=20   跳出迴圈
				break;
			}
			for(int j=0;j<5;j++){
				if(number[computer[i]]==number[computer[j]]){
					comcounter=comcounter+1;//判斷computer陣列裡的元素有沒有相同的元素
				}
			}
		}
//		for(int i=0;i<5;i++){//如果computer陣列裡面有Joker=20
//			if(number[computer[i]]==20){
//				comcounter=20;
//			}
//		}
		
		
		for(int i=0;i<5;i++){//判斷user陣列裡的元素有沒有相同的元素
			if(number[user[i]]==20){//(判斷)如果user陣列裡面有Joker=20 跳出迴圈
				usercounter=20;
				break;
			}
			for(int j=0;j<5;j++){
				if(number[user[i]]==number[user[j]]){
					usercounter=usercounter+1;
				}
			}
		}
		
		
//		for(int i=0;i<5;i++){//如果user陣列裡面有Joker=20
//			if(number[computer[i]]==20){
//				usercounter=20;
//			}
//		}
		
		
		
//		for(int i=0;i<5;i++){
//			if(as[i]==20){
//				System.out.println("<Joker>");
//			}
//		}
		
	}
	
	public String Comcounter(){//顯示牌型是什麼
		if(comcounter==20 ){//判斷牌型
			return "<Joker>";
		}
		else if(comcounter==17 ){
			return "<Four of a kind>";
		}
		else if(comcounter==13 ){
			return "<Full house>";
		}
		else if(comcounter==11 ){
			return "<Three of kind>";
		}
		else if(comcounter==9 ){
			return "<Two pairs>";
		}
		else{
			return "<Nothing>";
		}
	}
	
	public String Usercounter(){//顯示牌型是什麼
		if(usercounter==20){//判斷牌型
			return "<Joker>";
		}
		else if(usercounter==17){
			return "<Four of a kind>";
		}
		else if(usercounter==13){
			return "<Full house>";
		}
		else if(usercounter==11){
			return "<Three of kind>";
		}
		else if(usercounter==9){
			return "<Two pairs>";
		}
		else{
			return "<Nothing>";
		}
	}
	public String whowin(){//顯示誰贏字串
		//判斷誰贏 
			if(comcounter>usercounter){
				return "He Win";//顯示誰贏字串
			}
			else if(comcounter<usercounter){
				return "You Win";
			}
			else{
				return "平手";
			}
	}
	
	
}
