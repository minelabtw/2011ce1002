package a7.s100502544;
import java.util.Scanner;
import java.io.*;
public class A72 {
	
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);
		boolean a=true;//結束條件
		while(a){
			System.out.println("Please input a password to varify:");
			String message=new String();
			message=input.next();//輸入字串
			Password password=new Password(message);//傳給Password constructor 輸入的字串
			try{
				if(password.check()){//錯誤意外 不夠的數量
					throw new InvalidPasswordException("The length of the password "+message+" is not enough!!");
				}
				else if(password.check1()){//錯誤意外 沒有數字
				
					throw new InvalidPasswordException("The password "+message+" has no number!!");
				}
				else if(password.check2()){//錯誤意外 沒有字母
					throw new InvalidPasswordException("The password "+message+" has no letter!!");
				}
				//如果都沒有錯誤執行  正確訊息發布
				System.out.println("The password "+message+" is valid!!");//正確訊息
				a=false;// 正確 結束迴圈
			}
			catch(InvalidPasswordException Ex){
				System.out.println("The password "+message+" in not correct!!");
			}
		}
		
	}
}
