package a7.s100502544;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.*;



public class FrameWork extends JFrame implements ActionListener{
	public JButton jb11=new JButton();
	public JButton jb12=new JButton();
	public JButton jb13=new JButton();
	public JButton jb14=new JButton();
	public JButton jb15=new JButton();
	public JButton jb21=new JButton();
	public JButton jb22=new JButton();
	public JButton jb23=new JButton();
	public JButton jb24=new JButton();
	public JButton jb25=new JButton();

	public JLabel name3=new JLabel("   ");
	public JLabel name4=new JLabel("   ");
	
	public JLabel name5=new JLabel("   ");
	private JButton jbp4;
	public FrameWork (){
		JPanel p1=new JPanel(new GridLayout(2,5,5,5));//設一個GridLayout panel 5*2
		
		Border lineborder=new LineBorder(Color.BLACK,3);//黑框*2		
		
		jb11.setBorder(lineborder);
		
		jb12.setBorder(lineborder);
		
		jb13.setBorder(lineborder);
		
		jb14.setBorder(lineborder);
		
		jb15.setBorder(lineborder);
		
		
		jb21.setBorder(lineborder);
		
		jb22.setBorder(lineborder);
		
		jb23.setBorder(lineborder);
		
		jb24.setBorder(lineborder);
		
		jb25.setBorder(lineborder);
		p1.add(jb11);
		p1.add(jb12);
		p1.add(jb13);
		p1.add(jb14);
		p1.add(jb15);
		p1.add(jb21);
		p1.add(jb22);
		p1.add(jb23);
		p1.add(jb24);
		p1.add(jb25);
		p1.setBorder(new TitledBorder("Poker Game"));
		JPanel p2=new JPanel(new GridLayout(2,1,5,5));
		JLabel name1=new JLabel("He");
		JLabel name2=new JLabel("You");
		name1.setBorder(lineborder);
		name2.setBorder(lineborder);
		p2.add(name1);
		p2.add(name2);
		p2.setBorder(new TitledBorder("人"));
		
		JPanel p3=new JPanel(new GridLayout(2,1,5,5));
		
		name3.setBorder(lineborder);
		name4.setBorder(lineborder);
		p3.add(name3);
		p3.add(name4);
		p3.setBorder(new TitledBorder("Result"));
		
		JPanel p4=new JPanel(new GridLayout(1,2,5,5));
		jbp4=new JButton("random");
		jbp4.addActionListener(this);
		jbp4.setBorder(lineborder);
		
		name5.setBorder(lineborder);
		p4.add(jbp4);
		p4.add(name5);
		
		
		JPanel p5=new JPanel(new BorderLayout());
		p5.add(p1,BorderLayout.CENTER);
		p5.add(p2,BorderLayout.WEST);
		p5.add(p3,BorderLayout.EAST);
		p5.add(p4,BorderLayout.SOUTH);
		add(p5);
	}//FrameWork end
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == jbp4){
			//what do you want when you push "random" button? Obviously, random card!
			User user=new User(); //new constructor
			//取牌
			jb11.setIcon(user.aa[user.computer[0]]);
			jb12.setIcon(user.aa[user.computer[1]]);
			jb13.setIcon(user.aa[user.computer[2]]);
			jb14.setIcon(user.aa[user.computer[3]]);
			jb15.setIcon(user.aa[user.computer[4]]);
			
			jb21.setIcon(user.aa[user.user[0]]);
			jb22.setIcon(user.aa[user.user[1]]);
			jb23.setIcon(user.aa[user.user[2]]);
			jb24.setIcon(user.aa[user.user[3]]);
			jb25.setIcon(user.aa[user.user[4]]);
			
			name3.setText(user.Comcounter());
			name4.setText(user.Usercounter());
			name5.setText(user.whowin());
		}
		
	}	
	
}
