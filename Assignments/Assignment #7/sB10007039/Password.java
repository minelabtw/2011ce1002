package a7.sB10007039;

import java.util.regex.Pattern;

public class Password {
	
	private String Password;
	
	public Password(String NewPassword) throws InvalidPasswordException {
		
		try {
			setPassword(NewPassword);
		} catch (InvalidPasswordException e) {
			throw e;
		}
	}
	public String setPassword(String NewPassword) throws InvalidPasswordException {
		
		if (NewPassword.length() < 7) {//check length
			throw new InvalidPasswordException(1,NewPassword);
		}
       
        Pattern thePatternEng = Pattern.compile("[a-zA-z]*");//check all letter
        if(thePatternEng.matcher(NewPassword).matches()) {
        	throw new InvalidPasswordException(2,NewPassword);
        }
        
        
        Pattern thePatternNum = Pattern.compile("[0-9]*");//check all number
        if(thePatternNum.matcher(NewPassword).matches()) {
        	throw new InvalidPasswordException(3,NewPassword);
        }
        
        Pattern thePatternAll = Pattern.compile("[0-9a-zA-z]*");//check letter and number
        if(thePatternAll.matcher(NewPassword).matches()) {
        	Password = NewPassword;
        	return "The password "+ getPassword() +" is valid!!";
        }
		return null;
	}
	
	public String getPassword() {
		return Password;
	}
	
}
