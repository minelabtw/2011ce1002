package a7.sB10007039;

import java.util.Random;

public class User {
	private int Self_Win = 0;
	private int Rival_win = 0;
	private boolean Last_Self_Win = false;
	private boolean Last_Rival_win = false;
	private String Last_Self_Type = "Nothing";
	private String Last_Rival_Tpye = "Nothing";
	private int[] Self_Card = new int[5];
	private int[] Rival_Card = new int[5];
	private boolean[] Global_Card = new boolean[21];
	private Random Rand = new Random();
	private boolean Clear = true;
	
	public User() {
		ClearValue();
	}
	
	public void ClearValue() {//init value
		Last_Self_Win = false;
		Last_Rival_win = false;
		Last_Self_Type = "Nothing";
		Last_Rival_Tpye = "Nothing";
		for (int i = 0; i < Self_Card.length; i++) {
			Self_Card[i] = 0;
		}
		for (int i = 0; i < Rival_Card.length; i++) {
			Rival_Card[i] = 0;
		}
		for (int i = 0; i < Global_Card.length; i++) {
			Global_Card[i] = false; 
		}
		Clear = true;
	}
	
	public void RandomCard() {//tack new cards
		int Temp = 0;
		
		if (!Clear) {
			ClearValue();
		}
		
		for (int i = 0; i < Self_Card.length; i++) {
			Temp = Rand.nextInt(Global_Card.length);
			if (Global_Card[Temp]) {
				i--;
			} else {
				Global_Card[Temp] = true;
				Self_Card[i] = Temp;
			}
		}
		for (int i = 0; i < Rival_Card.length; i++) {
			Temp = Rand.nextInt(Global_Card.length);
			if (Global_Card[Temp]) {
				i--;
			} else {
				Global_Card[Temp] = true;
				Rival_Card[i] = Temp;
			}
		}
		Clear = false;
		WhoWin();
	}
	
	public String getLastfWin() {//get string who win the game
		if (Last_Self_Win) {
			return "Self Win!!!";
		}else if (Last_Rival_win) {
			return "Rival Win!!!";
		}else {
			return "NO One Win!!!";
		}
	}
	
	public String getLastfSelfType() {//get last game self type of cards
		return Last_Self_Type;
	}
	
	public String getLastfRivalType() {//get last game rival type of cards
		return Last_Rival_Tpye;
	}
	
	public int getSelfWin() {//get self win count
		return Self_Win;
	}
	
	public int getRivalWin() {//get rival win count
		return Rival_win;
	}
	
	public void WhoWin() {//judgment who win this game
		int[] Self_Card_Check = new int[6];
		int[] Rival_Card_Check = new int[6];
		for (int i = 0; i < Self_Card_Check.length; i++) {
			Self_Card_Check[i] = 0;
		}
		for (int i = 0; i < Rival_Card_Check.length; i++) {
			Rival_Card_Check[i] = 0;
		}
		
		
		for (int i = 0; i < Self_Card.length; i++) {
			Self_Card_Check[(Self_Card[i]/4)]++;
			//System.out.println(Self_Card[i] + "/ 4 = " + (Self_Card[i]/4));
			
		}
		for (int i = 0; i < Rival_Card.length; i++) {
			Rival_Card_Check[(Rival_Card[i]/4)]++;
			//System.out.println(Rival_Card[i] + "/ 4 = " + (Rival_Card[i]/4));
		}
		
		
		int Self_Win_Check = 0;
		int Rival_Win_Check = 0;
		
		/*check type of cards Start*/
		
		if (two_pair(Self_Card_Check)) {
			Self_Win_Check = 1;
			Last_Self_Type = "Two Pairs";
		}
		if (two_pair(Rival_Card_Check)) {
			Rival_Win_Check = 1;
			Last_Rival_Tpye = "Two Pairs";
		}
		
		if (three_of_kind(Self_Card_Check)) {
			Self_Win_Check = 2;
			Last_Self_Type = "Three of kind";
			
		}
		if (three_of_kind(Rival_Card_Check)) {
			Rival_Win_Check = 2;
			Last_Rival_Tpye = "Three of kind";
		}
		
		if (full_house(Self_Card_Check)) {
			Self_Win_Check = 3;
			Last_Self_Type = "Full House";
		}
		
		if (full_house(Rival_Card_Check)) {
			Rival_Win_Check = 3;
			Last_Rival_Tpye = "Full House";
		}
		
		if (four_of_kind(Self_Card_Check)) {
			Self_Win_Check = 4;
			Last_Self_Type = "Four Of Kind";
		}
		if (four_of_kind(Rival_Card_Check)) {
			Rival_Win_Check = 4;
			Last_Rival_Tpye = "Four Of Kind";
		}
		
		if (Self_Card_Check[5] > 0) {
			Self_Win_Check = 5;
			Last_Self_Type = "Joker";
		} else if (Rival_Card_Check[5] > 0) {
			Rival_Win_Check = 5;
			Last_Rival_Tpye = "Joker";
		}
		
		/*check type of cards End*/
		
		
		if (Self_Win_Check > Rival_Win_Check) {//check who wind Start
			Self_Win++;
			Last_Self_Win = true;
			Last_Self_Type = Last_Self_Type + " Win!!";
		}else if (Self_Win_Check < Rival_Win_Check) {
			Rival_win++;
			Last_Rival_win = true;
			Last_Rival_Tpye = Last_Rival_Tpye + " Win!!";
		}
		//System.out.println("ok");
	}
	
	private boolean two_pair(int[] Card) {//check have two pair
		int temp = 0;
		for (int i = 0; i < Card.length; i++) {
			if(Card[i]>1)temp++;
		}
		if (temp > 1) {
			return true;
		} else {
			return false;
		}
	}
	private boolean three_of_kind(int[] Card) {//check have three of kind
		int temp = 0;
		for (int i = 0; i < Card.length; i++) {
			if(Card[i]>2)temp++;
		}
		if (temp == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	private boolean full_house(int[] Card) {//check have full house
		int temp = 0;
		int temp2 = 0;
		for (int i = 0; i < Card.length; i++) {
			if(Card[i]>2)temp = i;
		}
		if (temp == 1) {
			for (int i = 0; i < Card.length; i++) {
				if (i != temp) {
					if(Card[i]>1)temp2++;
				}
			}
			if (temp2 > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	
	private boolean four_of_kind(int[] Card) {//check have four of kind
		int temp = 0;
		for (int i = 0; i < Card.length; i++) {
			if(Card[i]>3)temp++;
		}
		if (temp == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	public int getCardNum(boolean Self,int Num) {//get card image number form stores
		if (Self) {
			return Self_Card[Num];
		} else {
			return Rival_Card[Num];
		}
	}
	
}
