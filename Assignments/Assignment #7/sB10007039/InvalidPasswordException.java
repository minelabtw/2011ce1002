package a7.sB10007039;

@SuppressWarnings("serial")
public class InvalidPasswordException extends Exception  {//custom exception
	
	private int ErrorCode = 0;
	private String InputPassword;
	
	public InvalidPasswordException(int ErrorCodein,String Input) {//set error code
		super();
		ErrorCode = ErrorCodein;
		InputPassword = Input;
	}
	
	public String getMessage() {//override to custom message
		String temp = null;
		switch (ErrorCode) {
		case 1:
			temp = "The length of the password " + InputPassword + " is not enough!!";
			break;
		case 2:
			temp = "The length of the password " + InputPassword + " has no number!!";
			break;
		case 3:
			temp = "The length of the password " + InputPassword + " no letter!!";
			break;
		default:
			break;
		}
		return temp + "\nThe password " + InputPassword + " in not correct!!";
		
	}
	

}
/*
The length of the password 1234 is not enough!!
The password 1234 in not correct!!*/