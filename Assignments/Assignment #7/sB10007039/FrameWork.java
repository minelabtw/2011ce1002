package a7.sB10007039;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.*;
import javax.swing.*;

@SuppressWarnings("serial")
public class FrameWork extends JFrame implements ActionListener{
	private JPanel TopPanel = new JPanel();
	private JPanel CenterPanel = new JPanel();
	private JPanel UnderPanel = new JPanel();
	private JPanel CenterPanel_1 = new JPanel();
	private JPanel CenterPanel_2 = new JPanel();
	private JPanel CenterPanel_3 = new JPanel();
	private JButton BRandom = new JButton("Random");
	private ImageIcon[] CardImages = new ImageIcon[21];
	private ImageIcon[] InitImages = new ImageIcon[5];
	private JLabel[] InitLables = new JLabel[10];
	private JLabel[] TextMessage = new JLabel[2];
	private JLabel[] WinMessage = new JLabel[5];
	private JLabel[] TypeMessage = new JLabel[2];
	private User AUser = new User();
	
	public FrameWork() {//init gui
		super();
		
		BRandom.addActionListener(this);
		getContentPane().add(TopPanel,BorderLayout.NORTH);
		getContentPane().add(CenterPanel,BorderLayout.CENTER);
		getContentPane().add(UnderPanel,BorderLayout.SOUTH);
		CenterPanel.add(CenterPanel_1,BorderLayout.NORTH);
		CenterPanel.add(CenterPanel_2,BorderLayout.CENTER);
		CenterPanel.add(CenterPanel_3,BorderLayout.SOUTH);
		
		
		TextMessage[0] = new JLabel("Rival");
		TextMessage[1] = new JLabel("Self");
		TextMessage[0].setFont(new Font("新細明體", Font.BOLD, 30));
		TextMessage[1].setFont(new Font("新細明體", Font.BOLD, 30));
		TopPanel.add(TextMessage[0]);
		UnderPanel.add(TextMessage[1]);
		WinMessage[0] = new JLabel("Wind:");
		WinMessage[1] = new JLabel();
		WinMessage[2] = new JLabel();
		WinMessage[3] = new JLabel("Wind:");
		WinMessage[4] = new JLabel();
		WinMessage[2].setFont(new Font("新細明體", Font.BOLD, 30));
		TypeMessage[0] = new JLabel();
		TypeMessage[1] = new JLabel();
		TypeMessage[0].setFont(new Font("新細明體", Font.BOLD, 30));
		TypeMessage[1].setFont(new Font("新細明體", Font.BOLD, 30));
		CenterPanel_1.add(TypeMessage[0]);
		CenterPanel_3.add(TypeMessage[1]);
		CenterPanel_1.setPreferredSize(new Dimension(400, 100));
		CenterPanel_2.setPreferredSize(new Dimension(300, 100));
		CenterPanel_3.setPreferredSize(new Dimension(400, 100));
		CenterPanel_2.add(BRandom);
		for (int i = 0; i < InitImages.length; i++) {
			try {
				InitImages[i] = new ImageIcon("image/card/init_" + (i+1) + ".png");
				InitLables[i] = new JLabel(InitImages[i]);
				InitLables[i+5] = new JLabel(InitImages[i]);
				TopPanel.add(InitLables[i]);
				UnderPanel.add(InitLables[i+5]);
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		
		TopPanel.add(WinMessage[0]);
		TopPanel.add(WinMessage[1]);
		UnderPanel.add(WinMessage[3]);
		UnderPanel.add(WinMessage[4]);
		
		for (int i = 0; i < CardImages.length; i++) {
			try {
				CardImages[i] = new ImageIcon("image/card/" + i + ".png");
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		//start game, change images and win count
		if (e.getSource() == BRandom) {
			AUser.RandomCard();
			for (int i = 0; i < 5; i++) {
				int temp = 0;
				temp = AUser.getCardNum(true, i);
				InitLables[i+5].setIcon(CardImages[temp]);
				temp = AUser.getCardNum(false, i);
				InitLables[i].setIcon(CardImages[temp]);
			}
			//System.out.println(AUser.getLastfWin());
			WinMessage[4].setText(String.valueOf(AUser.getSelfWin()));
			WinMessage[1].setText(String.valueOf(AUser.getRivalWin()));
			WinMessage[2].setText(AUser.getLastfWin());
			TypeMessage[0].setText(AUser.getLastfRivalType());
			TypeMessage[1].setText(AUser.getLastfSelfType());
		}
	}


}
