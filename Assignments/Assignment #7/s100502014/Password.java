package a7.s100502014;
import javax.swing.*;
public class Password {
	private String pass = "";
	private boolean flag = true;
	public Password() {
	}
	public void passTest(String testPass) throws InvalidPasswordException {
		setPass(testPass);
		boolean lenTest = false;//length
		boolean hasLet = false;//letter
		boolean hasNum = false;//number
		
		//test length
		if (pass.length() >= 7)
			lenTest = true;
		
		//test letter and number
		for (int i = 0; i < pass.length(); i++) {
			if (pass.charAt(i)-'a' >= 0 && pass.charAt(i)-'a' < 26)
				hasLet = true;
			if (pass.charAt(i)-'0' >= 0 && pass.charAt(i)-'0' < 10)
				hasNum = true;
		}
		
		//valid
		if (hasLet && hasNum && lenTest) {
			JOptionPane.showMessageDialog(null,
										  "The password " + pass + " is valid!!",
										  "Valid!!!",
										  JOptionPane.INFORMATION_MESSAGE);
			flag = false;
		}
		//length false
		else if (!lenTest) {
			throw new InvalidPasswordException("The length of the password " + pass + " is not enough!!" + 
											   "\nThe password " + pass + " is not correct!!");
		}
		//no letter
		else if (!hasLet) {
			throw new InvalidPasswordException("The password " + pass +" has no letter!!" +
											   "\nThe password " + pass + " is not correct!!");
		}
		//no number
		else if (!hasNum) {
			throw new InvalidPasswordException("The password " + pass + " has no number!!" +
											   "\nThe password " + pass + " is not correct!!");
		}
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String newPass) {
		pass = newPass;
	}
	
	public boolean getFlag() {
		return flag;
	}
}
