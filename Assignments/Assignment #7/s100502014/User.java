package a7.s100502014;

public class User {
	private boolean[] cards = new boolean[21];
	private int[] yourCards = new int[5];
	private int type = 1;
	public void randomCards() {
		//clean the cards
		for(int i=0;i<21;i++) {
			if(cards[i])
				cards[i] = false;
		}
		
		//random the cards
		for(int i=0;i<5;i++) {
			int rand = (int)(Math.random()*21);
			while(cards[rand]) {
				rand = (int)(Math.random()*21);
			}
			cards[rand] = true;
			yourCards[i] = rand;
		}
		
		//type of the cards
		int[] tempCards = new int[5];
		for(int i=0;i<5;i++) {
			tempCards[i] = yourCards[i]/4;
		}
		//tempType[How many cards are the same]
		int[] tempType = new int[5];
		for(int i=0;i<4;i++) {
			//joker
			if(tempCards[i]==5) {
				tempType[4]++;
				break;
			}
			//others
			else if(tempCards[i]!=-1) {
				int combo = 0;
				for(int j=i+1;j<5;j++) {
					if(tempCards[j]!=-1 && tempCards[i]==tempCards[j]) {
						combo++;
						tempCards[j]=-1;
					}
				}
				tempType[combo]++;
			}
		}
		if(tempCards[4]!=0)
			tempType[0]++;
		
		//test
		if(tempType[4]==1)//Joker
			type = 6;
		else if(tempType[3]==1)//Four of a kind
			type = 5;
		else if(tempType[2]==1 && tempType[1]==1)//Full House
			type = 4;
		else if(tempType[2]==1)//Three of kind
			type = 3;
		else if(tempType[1]==2)//Two pairs
			type = 2;
		else//Nothing
			type = 1;
	}
	public int getCards(int i) {
		return yourCards[i];
	}
	public int getType() {
		return type;
	}
	//You should generated cards randomly(the cards can��t be repeated in any player��s hand).
}
