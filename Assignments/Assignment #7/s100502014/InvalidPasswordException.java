package a7.s100502014;

import javax.swing.JOptionPane;

public class InvalidPasswordException extends Exception {
	public InvalidPasswordException() {
	}

	public InvalidPasswordException(String a) {
		JOptionPane.showMessageDialog(null,
									  a,
									  "Not correct!!!",
									  JOptionPane.INFORMATION_MESSAGE);
	}
}
