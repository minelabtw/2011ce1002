package a7.s100502014;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
public class FrameWork extends JFrame implements ActionListener {
	private JLabel[] card2 = new JLabel[5];
	private JLabel[] card3 = new JLabel[5];
	JPanel jpn = new JPanel();
	JLabel result = new JLabel();
	JTextArea his = new JTextArea();
	JTextArea mine = new JTextArea();
	JButton button = new JButton("random"); //create button 
	
	public FrameWork() {
		//create
		result.setFont(new Font("SansSerif", Font.BOLD, 60));
		jpn.setLayout(new GridLayout(2,7,2,5));
		JLabel he = new JLabel("     He");
		he.setFont(new Font("SansSerif", Font.BOLD, 30));
		he.setBorder(new LineBorder(Color.BLUE, 3));
		jpn.add(he);
		for(int i=1;i<=5;i++) {
			card2[i-1] = new JLabel(new ImageIcon("image/card/init_"+i+".png"));
			jpn.add(card2[i-1]);
		}
		his.setFont(new Font("SansSerif", Font.BOLD, 30));
		his.setBorder(new LineBorder(Color.YELLOW, 3));
		his.setEditable(false);
		his.setLineWrap(true);
		jpn.add(his);
		JLabel I = new JLabel("       I");
		I.setFont(new Font("SansSerif", Font.BOLD, 30));
		I.setBorder(new LineBorder(Color.RED, 3));
		jpn.add(I);
		for(int i=1;i<=5;i++) {
			card3[i-1] = new JLabel(new ImageIcon("image/card/init_"+i+".png"));
			jpn.add(card3[i-1]);
		}
		mine.setFont(new Font("SansSerif", Font.BOLD, 30));
		mine.setBorder(new LineBorder(Color.GREEN, 3));
		mine.setEditable(false);
		mine.setLineWrap(true);
		jpn.add(mine);
		
		//set border layout
		setLayout(new BorderLayout(5,5));
		add(result, BorderLayout.NORTH);
		add(jpn, BorderLayout.CENTER);
		add(button, BorderLayout.SOUTH);
		
		button.addActionListener(this); //active button event here
	}
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == button){
			//random cards
			User he = new User();
			User I = new User();
			he.randomCards();
			I.randomCards();
			
			//put image on the label
			for(int i=0;i<5;i++) {
				card2[i].setIcon(new ImageIcon("image/card/"+he.getCards(i)+".png"));
				card3[i].setIcon(new ImageIcon("image/card/"+I.getCards(i)+".png"));
			}
			
			//change type name
			his.setText(typeName(he.getType()));
			mine.setText(typeName(I.getType()));
			
			//change result name
			if(I.getType()>he.getType())
				result.setText("You Win!!!");
			else if(I.getType()<he.getType())
				result.setText("You Lose!!!");
			else
				result.setText("Draw!!!");
		}
	}
	public String typeName(int type) {
		if(type==6)
			return "Joker";
		else if(type==5)
			return "Four of a kind";
		else if(type==4)
			return "Full House";
		else if(type==3)
			return "Three of kind";
		else if(type==2)
			return "Two pairs";
		else
			return "Nothing";
	}
}
