package a7.s100502012;

import javax.swing.*;
import javax.swing.border.*; 
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrameWork extends JFrame implements ActionListener {
	
	private ImageIcon[] defaultCard=new ImageIcon[5];
	JLabel[] label1=new JLabel[5];
	JLabel[] label2=new JLabel[5];
	JLabel String1;
	JLabel String2;
	JLabel String3;
	private ImageIcon[] c=new ImageIcon[5];
	private ImageIcon[] d=new ImageIcon[5];
    int[] cpuNumber=new int[5];
    int[] playerNumber=new int[5];
    String[] type_result=new String[3];
	JButton start=new JButton("RANDOM");
	Font largeFont=new Font("Mesquite Std",Font.BOLD,35);
	Color color=new Color(15,100,0);
	
	public FrameWork(){
		
		// default icon on p1 & p2
		store();
		for(int a=0 ; a<5 ; a++){
			label1[a]=new JLabel("");
			label1[a].setIcon(defaultCard[a]);
			label2[a]=new JLabel("");
			label2[a].setIcon(defaultCard[a]);
		}
		
		Border lineBorder=new LineBorder(Color.BLACK,10);
		//p11 panel
		JPanel p11=new JPanel();
		JLabel j11=new JLabel("    CPU");
		p11.setLayout(new GridLayout(1,6,1,1));
		p11.setBorder(lineBorder);
		p11.setBackground(color);
		j11.setForeground(Color.WHITE);
		p11.add(j11);
		for(int a=0 ; a<5 ; a++){
			p11.add(label1[a]);
		}
		
		//p12 panel
		JPanel p12=new JPanel();
		JLabel j12=new JLabel("    PLAYER");
		p12.setLayout(new GridLayout(1,6,1,1));
		p12.setBorder(lineBorder);
		p12.setBackground(color);
		j12.setForeground(Color.WHITE);
		p12.add(j12);
		for(int a=0 ; a<5 ; a++){
			p12.add(label2[a]);
		}
		
		//p2 panel
		JPanel p2=new JPanel();
		p2.setLayout(new GridLayout(3,1,5,5));
		p2.add(p11);
		p2.add(p12);
		start.setBackground(Color.PINK);
		start.setFont(largeFont);
		p2.add(start);
		
		//default string on p3
		String1=new JLabel("    <CPU cards type>");
		String1.setFont(largeFont);
		String1.setForeground(Color.WHITE);
		String2=new JLabel("    <Player cards type>");
		String2.setFont(largeFont);
		String2.setForeground(Color.WHITE);
		String3=new JLabel("     RESULT");
		String3.setFont(largeFont);
		String3.setForeground(Color.WHITE);;
		
		//p3 panel
		JPanel p3=new JPanel();
		p3.setLayout(new GridLayout(3,1,5,5));
		p3.setBackground(Color.BLUE);
        p3.add(String1);
	    p3.add(String2);
	    p3.add(String3);
		
	    //component p2 & p3
		setLayout(new GridLayout(1,2,5,5));
		add(p2);
		add(p3);
		
		start.addActionListener(this);
	}
	
	// random button linking to execution 
	public void actionPerformed(ActionEvent e){
		
		if (e.getSource()==start){// if push the random button ,it will start to sent new value into the panels,covering the default icons and strings
			User pass=new User();
			type_result=pass.TypeRank_result();
			String1.setText(type_result[0]);//send new string of the result of cpuCardType
			String2.setText(type_result[1]);//send new string of the result of playerCardType
			String3.setText(type_result[2]);//send new string of the result of the game
			
		    c=pass.storeCpuCard();
		    d=pass.storePlayerCard();
			for(int a=0 ; a<5 ; a++){
			    label1[a].setIcon(c[a]);//send new icons to the cardset of cpu
			}
			for(int a=0 ; a<5 ; a++){
			    label2[a].setIcon(d[a]);//send new icons to the cardset of player
			}                        
		}
	}
    
	public ImageIcon[] store(){
		for(int a=0 ; a<5 ; a++){
			defaultCard[a]=new ImageIcon("image/card/init_"+(a+1)+".png");//save the default icons from file
		}
		return defaultCard;
	}
}
