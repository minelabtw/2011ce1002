package a7.s100502012;

import javax.swing.*;

public class User {
    private ImageIcon[] cpuCard=new ImageIcon[5];
    private ImageIcon[] playerCard=new ImageIcon[5];
    
    
	int cpuRank;
    String cputype;
    int playerRank;
	String playertype;
	String result;
	
	int[] cpuNumber=new int[5];
	int[] playerNumber=new int[5];
	String[] type_result=new String[3];
    
	FrameWork pass=new FrameWork();
	
		
	public User(){
		
		for(int a=0 ; a<5 ; a++){
			cpuNumber[a]=(int)(Math.random()*21);//generating 21 random number from 0 to 20
		    for (int c=0;c<a;c++){  //avoid the same number 
			    if (cpuNumber[c]==cpuNumber[a]){
			        a--;
			    }
			}
		}
		for(int a=0 ; a<5 ; a++){
			playerNumber[a]=(int)(Math.random()*21);//generating 21 random number from 0 to 20
			for (int c=0;c<a;c++){  // avoid the same number 
			    if (playerNumber[c]==playerNumber[a]){
			        a--;
			    }
			}
		}
	}
	
	
	public  ImageIcon[] storeCpuCard(){//cast five random cards to cpu  
	    for(int a=0 ; a<5 ; a++){	
	    	int b=cpuNumber[a];
	    	cpuCard[a]=new ImageIcon("image/card/"+b+".png"); 
	    }
	    return cpuCard;
	}

	
	public  ImageIcon[] storePlayerCard(){// cast five random cards to player
		for(int a=0 ; a<5 ; a++){	
		    int b=playerNumber[a];
		    playerCard[a]=new ImageIcon("image/card/"+b+".png"); 
		}
		return playerCard;
	}
	
	
	
	
	
    public String[] TypeRank_result(){//judge the type and rank of cardset , then store in a array preparing to be send back to the cardset type and game result
    	int[] cardkind=new int[5];
    	int yuncard=0,zancard=0,nicard=0;
    	for(int a=0 ; a<5 ; a++){
    		if (cpuNumber[a]<4){
    			cardkind[0]++;
    		}
    		else if(cpuNumber[a]<8){
    			cardkind[1]++;
    		}
    		else if(cpuNumber[a]<12){
    			cardkind[2]++;
    		}
    		else if(cpuNumber[a]<16){
    			cardkind[3]++;
    		}
    		else if(cpuNumber[a]<20){
    			cardkind[4]++;
    		}
    		else{
    		}
    	}
    	
    	
    	for(int a=0 ; a<5 ; a++){
    		if (cardkind[a]>3){
        		yuncard++;
        	}
    		else if(cardkind[a]>2){
    			zancard++;
    		}
    		else if(cardkind[a]>1){
    			nicard++;
    		}
    		else{
    		}
    	}
    	
    	
    		
        if (cpuNumber[0]==20||cpuNumber[1]==20||cpuNumber[2]==20||cpuNumber[3]==20||cpuNumber[4]==20){
        	cpuRank=6;
        	cputype=new String("Joker");
        }
       	else if(yuncard>0){
       		cpuRank=5;
       		cputype=new String("Four of a kind");
       	}
       	else if(zancard>0){
       		if (nicard>0){    
       		    cpuRank=4;
       		    cputype=new String("Full House");
       		}
       		else{
       			cpuRank=3;
       		    cputype=new String("Three of a kind");
       		}
       	}
       	else if(nicard>0){
       		if (nicard>1){
       		    cpuRank=2;
       		    cputype=new String("Two Pair");
       		}
       		else{
       			cpuRank=1;
       			cputype=new String("One Pair");
       		}
       	}
       	else{
       		cpuRank=0;
       		cputype=new String("Nothing");
       	}
        
      

    
    	cardkind=new int[5];
    	yuncard=0;
    	zancard=0;
    	nicard=0;
    	for(int a=0 ; a<5 ; a++){
    		if (playerNumber[a]<4){
    			cardkind[0]++;
    		}
    		else if(playerNumber[a]<8){
    			cardkind[1]++;
    		}
    		else if(playerNumber[a]<12){
    			cardkind[2]++;
    		}
    		else if(playerNumber[a]<16){
    			cardkind[3]++;
    		}
    		else if(playerNumber[a]<20){
    			cardkind[4]++;
    		}
    		else{
    		}
    	}
    	
    	
    	for(int a=0 ; a<5 ; a++){
    		if (cardkind[a]>3){
        		yuncard++;
        	}
    		else if(cardkind[a]>2){
    			zancard++;
    		}
    		else if(cardkind[a]>1){
    			nicard++;
    		}
    		else{
    		}
    	}
    	
    	
    		
        if (playerNumber[0]==20||playerNumber[1]==20||playerNumber[2]==20||playerNumber[3]==20||playerNumber[4]==20){
        	playerRank=6;
        	playertype=new String("Joker");
        }
       	else if(yuncard>0){
       		playerRank=5;
       		playertype=new String("Four of a kind");
       	}
       	else if(zancard>0){
       		if (nicard>0){    
       		    playerRank=4;
       		    playertype=new String("Full House");
       		}
       		else{
       			playerRank=3;
       		    playertype=new String("Three of a kind");
       		}
       	}
       	else if(nicard>0){
       		if (nicard>1){
       		    playerRank=2;
       		    playertype=new String("Two Pair");
       		}
       		else{
       			playerRank=1;
       			playertype=new String("One Pair");
       		}
       	}
       	else{
       		playerRank=0;
       		playertype=new String("Nothing");
       	}
        
        
        
        if (cpuRank>playerRank){
        	result=new String("YOU LOSE~");
        }
        
        else if(playerRank>cpuRank){
        	result=new String("YOU WIN!!!");
        }
        else if(playerRank==cpuRank){
        	result=new String("EVEN");
        }
        else{
        }
        
        type_result[0]=cputype;
        type_result[1]=playertype;
        type_result[2]=result;
        
        return type_result;
	}
}
