package a7.s100502012;

public class Password {
	private String word;
	private int alpha=0;
	private int number=0;
	String errorA;
	String errorB;
	String errorC;
	
	// eat password
    public Password(String a){
    	word=a;
    	judgeLength(word);
    }
    
    //count alpha & number
    public void judgeLength(String word){
    	for(int a=0 ; a<word.length() ; a++){
    	    if (Character.isLetter(word.charAt(a)) == true){
    	    	alpha++;
    	    }
    	    else if (Character.isDigit(word.charAt(a)) == true){
    	    	number++;
    	    }
    	}
    } 
    
    //differ the error
    public void testResult()throws InvalidPasswordException{
    	errorA="The password "+word+" has no letter!!\nThe password "+word+" is not correct!!";
    	errorB="The password "+word+" has no number!!\nThe password "+word+" is not correct!!";
    	errorC="The length of the password "+word+" is not enough!!\nThe password "+word+" is not correct!!";
    	
    	if (alpha<1){
    		throw new InvalidPasswordException(errorA);
    	}
    	else if(number<1){
    		throw new InvalidPasswordException(errorB);
    	}
    	else if(word.length()<7){
    		throw new InvalidPasswordException(errorC);
    	}
    }
    
}
