package a7.s100502002;
import java.util.Random;
import javax.swing.ImageIcon;
public class User 
{
	protected ImageIcon cards[] = new ImageIcon [21];
	protected ImageIcon[] Player;
	protected int[] amount = new int[6];//方便計算六種種類的排個數
	protected String Level;
	boolean FullHouse = false;
	boolean TwoPair = false;
	public void setcards()
	{
		for(int i=0;i<21;i++)
			{
				cards[i] =new ImageIcon("image/card/"+i+".png");
			}
	}//用迴圈把圖片放進陣列
	public User()
	{
		setcards();//把排傳進陣列
		Player = new ImageIcon[5];//玩家手上的五張牌
	}
	public void counter(int now)
	{
		for(int i = 0; i < 5; i++, now--)
		{
			Random R = new Random();//宣告隨機變數
			int ran1 = R.nextInt(now)+1;
			for(int j = 0, counter = 0; j < 21; j++)
			{
				if(cards[j] != null)
				{
					counter = counter + 1;
					if(counter == ran1)
					{
						Player[i] = cards[j];
						if(j == 1 || j == 2 || j == 3 || j == 0)//他們都是A
							amount[0]++;
						else if(j == 4 || j == 5 || j == 6 || j == 7)//他們都是2
							amount[1]++;
						else if(j == 8 || j == 9 || j == 10 || j == 11)//他們都是J
							amount[2]++;
						else if(j == 12 || j == 13 || j == 14 || j == 15)//他們都是Q
							amount[3]++;
						else if(j == 16 || j == 17 || j == 18 || j == 19)//他們都是K
							amount[4]++;
						else if(j == 20)//鬼牌
							amount[5]++;
						else{}
						cards[j] = null;
						break;
					}
					else{}
				}
				else{}
			}
		}
		
	}
	public int Rank()
	{
		for(int i = 0; i < 4; i++)//判斷是否有兩對一樣
		{
			for(int j = 1; j <= 4; j++)
			{
				if(i==j)
					break;
				else{}
				
				if(amount[i] == 2 && amount[j] == 2)
				{
					TwoPair = true;
					i = 4;
					break;
				}
				else{}
			}
		}
		for(int i = 0; i < 4; i++)//用迴圈判斷葫蘆
		{
			for(int j = 1; j <= 4; j++)
			{
				if(amount[i] == 2 && amount[j] == 3)
				{
					FullHouse = true;
					i = 4;
					break;
				}
				else if(amount[i] == 3 && amount[j] == 2)
				{
					FullHouse = true;
					i = 4;
					break;
				}
				else{}
			}
		}
		//開始設定等級
		if(amount[5] == 1)
		{
			Level = "<Joker>";
			return 6;
		}//小胖牌最大
		else if(amount[0] == 4 || amount[1] == 4 || amount[2] == 4 || amount[3] == 4 || amount[4] == 4)
		{
			Level = "<Four of a kind>";
			return 5;
		}//鐵之第二
		else if(FullHouse)
		{
			Level = "<Full House>";
			return 4;
		}//葫蘆三
		else if(amount[0] == 3 || amount[1] == 3 || amount[2] == 3 || amount[3] == 3 || amount[4] == 3)
		{
			Level = "<Three of kind>";
			return 3;
		}//three of kind四
		else if(TwoPair)
		{
			Level = "<Two pairs>";
			return 2;
		}//two pairs 五
		else
		{
			Level = "<Nothing>";
			return 1;
		}//nothing當然最小		
	}
}
