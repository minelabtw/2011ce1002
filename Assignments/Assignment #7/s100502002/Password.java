package a7.s100502002;
public class Password 
{
	private String password;
	Password()
	{
		
	}
	public void setpassword(String word)
	{
		password = word;//設定密碼
	}
	public String getpassword()
	{
		return password;//取出密碼
	}
	public void Throw(String password)throws InvalidPasswordException
	{
		setpassword(password);
		if(getpassword().length() < 7)
		{
			throw new InvalidPasswordException("1" + getpassword());//拋出訊息
		}
		if(Nonumber())
		{
			throw new InvalidPasswordException("2" + getpassword());//拋出訊息
		}
		if(Noletter())
		{
			throw new InvalidPasswordException("3" + getpassword());//拋出訊息
		}
	}
	public boolean Nonumber()
	{
		char array[] = getpassword().toCharArray();
		boolean result = true;//假設理面本來沒有數字
		for(int i=0;i<getpassword().length();i++)
		{
			if(Character.isDigit(array[i]))//如果其中有數字就設成false
			result = false;
		}
		return result;
	}
	public boolean Noletter()
	{
		boolean result = true;//假設理面本來沒有字母
		char array [] = getpassword().toCharArray();
		for(int i = 0; i < getpassword().length(); i++)
		{
			if(Character.isLetter(array[i]))
			{
				result = false;//只要其中一個是字母，就設成false
			}
		}
		return result;
	}
}

