package a7.s100502021;
import java.util.*;
public class Password extends Exception {
	protected String str;
	public Password(String string){
		str=string;
	}	
	protected boolean flag1=false,flag2=false;
	protected int x=0;	
	public void check(){
		for(int i=0;i<str.length();i++){
			x++;
			if((int)str.charAt(i)>47&&(int)str.charAt(i)<58){ //判斷字元的ASCII
				flag1=true;
			}
			if((int)str.charAt(i)>64&&(int)str.charAt(i)<91){
				flag2=true;
			}
			if((int)str.charAt(i)>96&&(int)str.charAt(i)<123){
				flag2=true;
			}
		}		
	}	 
	public void finish() throws InvalidPasswordException{ //丟出三種情況
		if(x<7)
			throw new InvalidPasswordException("The length of the password "+str+" is not enough!!"+"\n"+"The password "+str+" in not correct!!");
		else if(flag1==false)
			throw new InvalidPasswordException("The password "+str+" has no number!!"+"\n"+"The password "+str+" in not correct!!");
		else if(flag2==false)
			throw new InvalidPasswordException("The password "+str+" has no letter!!"+"\n"+"The password "+str+" in not correct!!");
			
		else		
			System.out.println("The password "+str+" is valid!!");
	}	
}