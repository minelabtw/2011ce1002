package a7.s100502503;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener
{
	User user = new User();
	
	JButton button = new JButton("random"); //create button
	
	//declare the panels 
	JPanel pBase = new JPanel(new BorderLayout());
	JPanel pNPC = new JPanel(new BorderLayout());
	JPanel pNPCCard = new JPanel(new GridLayout(1, 5, 1, 1));
	JPanel pUser = new JPanel(new BorderLayout());
	JPanel pUserCard = new JPanel(new GridLayout(1, 5, 1, 1));
	JPanel pButton = new JPanel(new BorderLayout());
	JPanel pResult = new JPanel(new GridLayout(3, 1, 10, 10));
	
	//The Label to store the image of card
	protected JLabel[] cardNPC = new JLabel[5];
	protected JLabel[] cardUser = new JLabel[5];

	protected JLabel resultNPC = new JLabel("?");
	protected JLabel resultUser = new JLabel("?");
	protected JLabel resultAll = new JLabel("?");
	
	public FrameWork ()
	{	
		cardNPC[0] = new JLabel(new ImageIcon("image/card/init_1.png"));
		cardNPC[1] = new JLabel(new ImageIcon("image/card/init_2.png"));
		cardNPC[2] = new JLabel(new ImageIcon("image/card/init_3.png"));
		cardNPC[3] = new JLabel(new ImageIcon("image/card/init_4.png"));
		cardNPC[4] = new JLabel(new ImageIcon("image/card/init_5.png"));
		
		cardUser[0] = new JLabel(new ImageIcon("image/card/init_1.png"));
		cardUser[1] = new JLabel(new ImageIcon("image/card/init_2.png"));
		cardUser[2] = new JLabel(new ImageIcon("image/card/init_3.png"));
		cardUser[3] = new JLabel(new ImageIcon("image/card/init_4.png"));
		cardUser[4] = new JLabel(new ImageIcon("image/card/init_5.png"));

		Font font1 = new Font("Serif", Font.BOLD, 30);
		Font font2 = new Font("SansSerif", Font.ITALIC, 20);
		Font font3 = new Font("Serif", Font.BOLD, 40);

		
		//The NPC's field
		pNPC.add(new JLabel("NPC"), BorderLayout.WEST);
		
		//The pictures show out at first
		pNPCCard.add(cardNPC[0]);
		pNPCCard.add(cardNPC[1]);
		pNPCCard.add(cardNPC[2]);
		pNPCCard.add(cardNPC[3]);
		pNPCCard.add(cardNPC[4]);
		
		pNPC.add(pNPCCard, BorderLayout.CENTER);
		pBase.add(pNPC, BorderLayout.NORTH);
		
		//The User's field
		pUser.add(new JLabel("User"), BorderLayout.WEST);	
		//The pictures show out at first
		pUserCard.add(cardUser[0]);
		pUserCard.add(cardUser[1]);
		pUserCard.add(cardUser[2]);
		pUserCard.add(cardUser[3]);
		pUserCard.add(cardUser[4]);
		
		pUser.add(pUserCard, BorderLayout.CENTER);
		pBase.add(pUser, BorderLayout.CENTER);
	
		//The random button.
		pButton.add(button, BorderLayout.WEST);
		pBase.add(pButton, BorderLayout.SOUTH);
		
		//The result field
		pResult.add(resultNPC, BorderLayout.NORTH);
		pResult.add(resultUser, BorderLayout.CENTER);
		pResult.add(resultAll, BorderLayout.SOUTH);
		
		resultNPC.setFont(font2);
		resultUser.setFont(font2);
		resultAll.setFont(font3);

		
		add(pBase, BorderLayout.WEST);
		add(pResult, BorderLayout.EAST);
		
		button.setFont(font1);
		button.addActionListener(this); //active button event here

	}
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == button)
		{
			User user = new User();
		
			//change the icon
			for(int i = 0; i < 5; i++)
			{
				cardNPC[i].setIcon(user.pic[user.randomNumber[i]]);
			
				cardUser[i].setIcon(user.pic[user.randomNumber[i+5]]);
			}
			
			int pointNPC = user.cardKind(user.randomNumber[0], user.randomNumber[1], user.randomNumber[2], user.randomNumber[3], user.randomNumber[4]);
			int pointUser = user.cardKind(user.randomNumber[5], user.randomNumber[6], user.randomNumber[7], user.randomNumber[8], user.randomNumber[9]);
			
			//The label of NPC
			if(pointNPC == 6)
				resultNPC.setText("Joker");
			else if(pointNPC == 5)
				resultNPC.setText("Four of a kind");
			else if(pointNPC == 4)
				resultNPC.setText("Full House");
			else if(pointNPC == 3)
				resultNPC.setText("Three of a kind");
			else if(pointNPC == 2)
				resultNPC.setText("Two pairs");
			else
				resultNPC.setText("NOTHING QQ");
			
			//The label of user
			if(pointUser == 6)
				resultUser.setText("Joker");
			else if(pointNPC == 5)
				resultUser.setText("Four of a kind");
			else if(pointNPC == 4)
				resultUser.setText("Full House");
			else if(pointNPC == 3)
				resultUser.setText("Three of a kind");
			else if(pointNPC == 2)
				resultUser.setText("Two pairs");
			else
				resultUser.setText("NOTHING QQ");
			
			//The final result
			if(pointNPC > pointUser)
				resultAll.setText("You Lose!");
			else if(pointNPC == pointUser)
				resultAll.setText("DRAW!!!");
			else	
				resultAll.setText("YOU WIN");
		}
	}
}

