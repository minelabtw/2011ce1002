package a7.s100502503;

import javax.swing.ImageIcon;

public class User 
{
	public ImageIcon[] pic = new ImageIcon[21]; 
	
	//Shuffle
	protected char[] cardType = new char[6];
	protected int[] randomNumber = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
	
	public User()
	{
		pic[0] = new ImageIcon("image/card/0.png");
		pic[1] = new ImageIcon("image/card/1.png");
		pic[2] = new ImageIcon("image/card/2.png");
		pic[3] = new ImageIcon("image/card/3.png");
		pic[4] = new ImageIcon("image/card/4.png");
		pic[5] = new ImageIcon("image/card/5.png");
		pic[6] = new ImageIcon("image/card/6.png");
		pic[7] = new ImageIcon("image/card/7.png");
		pic[8] = new ImageIcon("image/card/8.png");
		pic[9] = new ImageIcon("image/card/9.png");
		pic[10] = new ImageIcon("image/card/10.png");
		pic[11] = new ImageIcon("image/card/11.png");
		pic[12] = new ImageIcon("image/card/12.png");
		pic[13] = new ImageIcon("image/card/13.png");
		pic[14] = new ImageIcon("image/card/14.png");
		pic[15] = new ImageIcon("image/card/15.png");
		pic[16] = new ImageIcon("image/card/16.png");
		pic[17] = new ImageIcon("image/card/17.png");
		pic[18] = new ImageIcon("image/card/18.png");
		pic[19] = new ImageIcon("image/card/19.png");
		pic[20] = new ImageIcon("image/card/20.png");
		
		randomNumber();
		
	}
	//the method to shuffle
	public void randomNumber()
	{
		for(int i = 0; i < 21; i++)
		{
			int j = (int)(Math.random() * 100) % 21;
			
			int k = randomNumber[i];
			randomNumber[i] = randomNumber[j];
			randomNumber[j] = k;
		}
	}
	//  0=A     1=2    2=J    3=Q   4=K
	public int cardKind(int A, int B, int C, int D, int E)
	{
		int card_A = 0;
		int card_2 = 0;
		int card_J = 0;
		int card_Q = 0;
		int card_K = 0;
		int[] testArray = {A, B, C, D, E};
		
		if(A == 20 || B == 20 || C == 20 || D == 20 || E == 20)
			return 6;
		
		//check how many cards of each type
		for(int i = 0; i < 5; i++)
		{
			if(testArray[i]/4 == 0)
				card_A++;
			else if(testArray[i]/4 == 1)
				card_2++;
			else if(testArray[i]/4 == 2)
				card_J++;
			else if(testArray[i]/4 == 3)
				card_Q++;
			else if(testArray[i]/4 == 4)
				card_K++;
		}

		//four of a kind
		if(card_A == 4 || card_2 == 4 || card_J == 4 || card_Q == 4 || card_K == 4)
			return 5;
		//full house & three of a kind
		if(card_A == 3 || card_2 == 3 || card_J == 3 || card_Q == 3 || card_K == 3)
		{
			if(card_A == 2 || card_2 == 2 || card_J == 2 || card_Q == 2 || card_K == 2)
				return 4;
			else
				return 3;
		}
		if(card_A == 2 || card_2 == 2 || card_J == 2 || card_Q == 2 || card_K == 2)
		{
			int check = 0;
			
			if(card_A == 2)
				check++;
			if(card_2 == 2)
				check++;
			if(card_J == 2)
				check++;
			if(card_Q == 2)
				check++;
			if(card_K == 2)
				check++;
			
			if(check == 2)
				return 2;
		}
		return 1;
		
	}
}
