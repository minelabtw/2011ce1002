package a7.s100502503;

import java.util.Scanner;

import javax.swing.JOptionPane;


public class Password
{

	public static void main(String[] args)
	{		
		int numCheck = 0;
		int letterCheck = 0;
		Scanner input = new Scanner(System.in);
		
		System.out.println("Please input a password to varify: ");
		String passWord = input.nextLine();

		//The flag to determine whether the string has number and letter
		boolean num = true;
		boolean let = true;
		
		for(int i = 0; i < passWord.length(); i++)
		{			
			if(checkNumber(passWord.charAt(i)))
				numCheck++;

			if(checkLetter(passWord.charAt(i)))
				letterCheck++;
		}
		
		//if there is no number or letter in the string, change to false
		if(numCheck == 0)
			num = false;
		if(letterCheck == 0)
			let = false;
		
		try
		{
			if(!num || !let || passWord.length() < 7)
				throw new InvalidPasswordException("The password is invalid");
		}
		catch(InvalidPasswordException ex)
		{
			if(passWord.length() < 7)
				System.out.println("The length of the password " + passWord + " is not enough!!");
			
			//if the length is enough but has no letter or number
			else if(passWord.length() >= 7 && let == true && num == false)
				System.out.println("The password " + passWord + " has no number!!");
			
			else if(passWord.length() >= 7 && let == false && num == true)
				System.out.println("The password " + passWord + " has no letter!!");
			else
				System.out.println("The password " + passWord + " has no number!!");

			System.out.println("The password " + passWord +"in not correct!!");
		}
	}
	
	
	
	
	//check is there a letter exist in the password
	public static boolean checkLetter(char input) 
	{
		if((65 <= input && input <= 90) || (97 <= input && input <= 122))
			return true;
		else
			return false;
	}
	//check is there a number exist in the password
	public static boolean checkNumber(char input)
	{
		if(48 <= input && input <= 57)
			return true;
		else 
			return false;
	}
	
}
