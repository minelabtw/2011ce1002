package a7.s100502514;

import java.util.ArrayList;

public class A71_AllOfCards extends ArrayList<Integer>{
	
	public static A71_AllOfCards defaultGroup = new A71_AllOfCards();
	
	public A71_AllOfCards(){
		resetAll();
	}
	
	/** Draw a card from the card list */
	public Integer drawCard() throws Exception{
		try{
			return remove((int)(Math.random()*size()));
		}catch(IndexOutOfBoundsException err){
			//Rethrows exception if arraylist is empty.
			throw new Exception("The card list is empty!!");
		}/*catch(Exception err){
			err.printStackTrace();
			System.err.println("---------------");
			System.err.println(err.getMessage());
			System.err.println("===============");
			return null;
		}*/
	}
	
	/** Reset the card list */
	public void resetAll(){
		clear();
		for(int card_id=0;card_id<=20;card_id++){
			//Add integer data from 0 to 20 (20 is for Joker)
			add(card_id);
		}
	}
}
