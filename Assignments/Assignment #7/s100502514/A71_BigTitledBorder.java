package a7.s100502514;

import java.awt.Font;

import javax.swing.border.TitledBorder;

public class A71_BigTitledBorder extends TitledBorder {
	public A71_BigTitledBorder(String title){
		super(title);
		setTitleFont(new Font("Comic Sans MS",Font.BOLD,24));
	}
}
