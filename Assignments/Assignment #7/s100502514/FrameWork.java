/**
 * class FrameWork from CE1002 Assignment 7-1
 * For basic requirement for Assignment, please see on A71.java header comments.
 */
package a7.s100502514;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener {
	
	/** Window Instance Declarations */
	JPanel TopPanel = new JPanel(new FlowLayout());
	JPanel CenterPanel = new JPanel(new GridLayout(2, 1, 5, 10));
	User CenterComPanel = new User("Computer's Cards");
	User CenterYouPanel = new User("Your Cards");
	A71_WinnerJudge HiddenRightPanel = new A71_WinnerJudge(
			CenterComPanel.getStatusLabel(), CenterYouPanel.getStatusLabel());
	JPanel BottomPanel = new JPanel(new FlowLayout());
	JButton randomButton = new A71_RandomButton("Random Select");
	
	public FrameWork(){
		/** Outer Layout of a Window Frame */
		setLayout(new BorderLayout());
		
		/** Add Main Title */
		A71_BigLabel title = new A71_BigLabel("Card Game Test");
		title.setForeground(new Color(0, 153, 102)); //Sap Green
		TopPanel.add(title);
		add(TopPanel, BorderLayout.NORTH);
		
		/** Add Computer-Player Panel */
		CenterPanel.add(CenterComPanel);
		CenterPanel.add(CenterYouPanel);
		add(CenterPanel, BorderLayout.CENTER);
		
		/** Add Button in the bottom */
		BottomPanel.setAlignmentX(CENTER_ALIGNMENT);
		BottomPanel.add(randomButton);
		add(BottomPanel, BorderLayout.SOUTH);
		
		/** Add Button Listener */
		randomButton.addActionListener(this);
	}
	
	public void showCard(){
		/** Reset both players' cards randomly with no repeat. */
		CenterComPanel.resetCards();
		CenterYouPanel.resetCards();
		/** Reset both players' cards randomly with no repeat. */
		HiddenRightPanel.Judge(CenterComPanel, CenterYouPanel);
		/** Reset the card list. */
		A71_AllOfCards.defaultGroup.resetAll();
	}
	
	/** Event Handler Implement */
	public void actionPerformed(ActionEvent evt) {
		if(evt.getSource()==randomButton){
			showCard();
			setVisible(true);
		}
	}
}
