/**
 * class InvalidLengthException extends InvalidPasswordException
 * A Subclass that indicates the code length is not enough. (when length<7)
 */
package a7.s100502514;

public class InvalidLengthException extends InvalidPasswordException {
	public InvalidLengthException(String content){
		super("Code '"+content+"' Length is NOT enough!!");
	}
	public InvalidLengthException(){
		super("Code Length is NOT enough!!");
	}
}
