/**
 * CE1002-100502514 FlashTeens Chiang
 * This is a class for bigger text label.
 * Here using this class as the title.
 */
package a7.s100502514;

import java.awt.*;

import javax.swing.*;

//A New Label Class for Bigger Text Style
public class A71_BigLabel extends JLabel{
	public A71_BigLabel(String txt){
		super(txt);
		/** Only this line is modified here from Assignment 1-1 Extra Class MyBigLabel.java */
		setFont(new Font("Comic Sans MS",Font.BOLD,24));
		setHorizontalAlignment(JLabel.CENTER);
		setVerticalAlignment(JLabel.CENTER);
	}
	public A71_BigLabel(){
		//Allows default constructor with no strings
		this("");
	}
}
