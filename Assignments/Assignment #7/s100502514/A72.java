/**
 * CE1002-100502514 FlashTeens Chiang
 * [The Following Rules are from ce1002 website]
 * Assignment 7-2
 * 
 * Write a Password class that stores a password String.
 * Password must contain at least seven alpha-numeric characters,
	with at least one letter and one number.
 * Also write an InvalidPasswordException class that extends Exception.

 * The Password constructor should throw an InvalidPasswordException,
	which should report an appropriate message based on a given String
	(e.g., if the String contains no numbers, the exception should report this fact).
 * If there is more than one error, the exception need only report one.

 * For more informations, please see on CE1002 site.
 */

package a7.s100502514;

import javax.swing.*;

public class A72 {
	public static void main(String[] args){
		try{
			while(true){
				/** Infinite run until user closes the dialog. */
				String input = JOptionPane.showInputDialog(null, "Please Input the Checking Code:",
					"Assignment 7-2", JOptionPane.DEFAULT_OPTION);
				Password checker = new Password(input);
				String message = checker.getErrorMessage();
				if(message.isEmpty()){
					/** According to definition in class Password, empty string is for correct code. */
					JOptionPane.showMessageDialog(null,	"Code '"+input+"' is valid.",
							"Assignment 7-2", JOptionPane.INFORMATION_MESSAGE);
				}else{
					/** Show error messages */
					JOptionPane.showMessageDialog(null,	checker.getErrorMessage(),
							"Assignment 7-2", JOptionPane.ERROR_MESSAGE);
				}
			}
		}catch(Exception err){
			//When closing the dialog, do nothing but exit the program.
		}
	}
}
