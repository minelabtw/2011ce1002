package a7.s100502514;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class A71_RandomButton extends JButton implements MouseListener {
	
	/** Button states */
	private boolean isOver;
	private boolean isDown;
	
	/** The text colors of different states of the button */
	final Color COLOR_UP = new Color(255,224,153); //Default Color
	final Color COLOR_OVER = new Color(255,255,0); //Lemon Yellow
	final Color COLOR_DOWN = new Color(255,64,0); //Orange Red
	
	public A71_RandomButton(String text){
		//Set the message, states, font and background color respectively.
		setText(text);
		isOver=false;
		isDown=false;
		setFont(new Font("Comic Sans MS",Font.BOLD,24));
		setForeground(COLOR_UP);
		setBackground(Color.BLACK);
		//Enable the effect for roll-over and press-down states.
		addMouseListener(this);
	}
	public void mouseClicked(MouseEvent e) {
		/** Also Handled in either mousePressed() or mouseReleased(). */
		//setForeground(/*omitted*/);
	}
	public void mouseEntered(MouseEvent e) {
		isOver=true;
		setForeground(isDown?COLOR_DOWN:COLOR_OVER);
	}
	public void mouseExited(MouseEvent e) {
		isOver=false;
		setForeground(isDown?COLOR_OVER:COLOR_UP);
	}
	public void mousePressed(MouseEvent e) {
		isDown=true;
		setForeground(COLOR_DOWN);
	}
	public void mouseReleased(MouseEvent e) {
		isDown=false;
		setForeground(isOver?COLOR_OVER:COLOR_UP);
	}
}
