/**
 * class NoDigitException extends InvalidPasswordException
 * A Subclass that indicates there is no any numeric characters in the code.
 */
package a7.s100502514;

public class NoDigitException extends InvalidPasswordException {
	public NoDigitException(String content){
		super("Code '"+content+"' has NO DIGITS!!");
	}
	public NoDigitException(){
		super("The Code has NO DIGITS!!");
	}
}
