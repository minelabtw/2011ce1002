package a7.s100502514;

import java.io.*;

import javax.swing.*;

/** This class is not public for other packages. */
final class Images {
	
	private static final ImageIcon[] images = new ImageIcon[21];
	private static final ImageIcon[] init_images = new ImageIcon[6];
	
	public static final ImageIcon getCard(int num) throws FileNotFoundException, IndexOutOfBoundsException{
		return getPicture(num, images, "image/card/");
	}
	public static final ImageIcon getInitCard(int num) throws FileNotFoundException, IndexOutOfBoundsException{
		return getPicture(num, init_images, "image/card/init_");
	}
	
	@SuppressWarnings("finally") /** Auto-generated command to supress warning for "finally" clause. */
	private static final ImageIcon getPicture(int num, ImageIcon[] source, String prefix)
			throws FileNotFoundException, IndexOutOfBoundsException{
		try{
			if(source[num]==null){
				source[num] = new ImageIcon(prefix+num+".png");
				if(source[num].getImage()==null){
					source[num] = null;
					throw new FileNotFoundException("File \""+prefix+num+".png\" is NOT FOUND!!");
				}
			}
		}catch(FileNotFoundException err){
			throw err;
		}catch(Exception err){
			throw new IndexOutOfBoundsException("Invalid number index "+num+" with prefix \""+prefix+"\"!!");
		}finally{
			return source[num];
			//No matter whether any exception is thrown or not, the value will be returned.
		}
	}
}
