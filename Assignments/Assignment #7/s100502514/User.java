/**
 * class User from CE1002 Assignment 7-1
 * For basic requirement for Assignment, please see on A71.java header comments.
 * This class also extends JPanel as superclass. (extra)
 */
package a7.s100502514;

import java.awt.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;

public class User extends JPanel {
	
	/** Private members */
	private String title;
	private ImageIcon[] images = new ImageIcon[5];
	private int[] card_data = new int[5];
	private CardState card_state = CardState.UNDECLARED;
	private A71_WinnerLabel statusLabel;
	
	/** Creates the panel with initializing images. */
	public User(String _title){
		title = _title;
		setLayout(new GridLayout(1, 6, 5, 10));
		setBorder(new A71_BigTitledBorder(title));
		for(int i=0;i<images.length;i++){
			updateInitCard(i);
			JLabel myImageLabel = new JLabel(images[i]);
			add(myImageLabel);
		}
		statusLabel = new A71_WinnerLabel();
		add(statusLabel);
	}
	
	/** Reset the cards and refresh the card state. */
	public void resetCards(){
		/** Remove all image labels in order to re-add. */
		removeAll();
		/** Draw 5 random cards from A71_AllOfCards. */
		for(int i=0;i<images.length;i++){
			drawOneCard(i);
			JLabel myImageLabel = new JLabel(images[i]);
			add(myImageLabel);
		}
		add(statusLabel);
		/** Refresh the card state. */
		changeCardState();
		/** Retitle the border with current card status. */
		setBorder(getCardStateTitle());
	}
	
	public A71_WinnerLabel getStatusLabel(){
		return statusLabel;
	}
	
	public static enum CardState{UNDECLARED, 
		NOTHING, ONE_PAIR, TWO_PAIRS, THREE_OF_KIND, FULL_HOUSE, FOUR_OF_KIND, JOKER
	};
	//The state "ONE_PAIR" is extra.
	
	/** Comparing who wins, returns:
	 * a positive value for this user wins;
	 * a negative value for another wins;
	 * zero for draw.
	 */
	public int compareTo(User another){
		return getCardState().compareTo(another.getCardState());
	}
	
	/** Returns the read-only card state. */
	public CardState getCardState(){
		return card_state;
	}
	public A71_BigTitledBorder getCardStateTitle(){
		A71_BigTitledBorder br;
		switch(getCardState()){
		case NOTHING:
			br = new A71_BigTitledBorder(title+" (Nothing)");
			br.setTitleColor(new Color(102,51,51)); //Rust Brown
			break;
		case ONE_PAIR:
			br = new A71_BigTitledBorder(title+" (One Pair)");
			br.setTitleColor(new Color(0,102,51)); //Dark Green
			break;
		case TWO_PAIRS:
			br = new A71_BigTitledBorder(title+" (Two Pairs)");
			br.setTitleColor(new Color(102,170,51)); //Lime Green
			break;
		case THREE_OF_KIND:
			br = new A71_BigTitledBorder(title+" (Three of a Kind)");
			br.setTitleColor(new Color(153,204,0)); //Olive Green
			break;
		case FULL_HOUSE:
			br = new A71_BigTitledBorder(title+" (Full House)");
			br.setTitleColor(new Color(224,170,0)); //Gold
			break;
		case FOUR_OF_KIND:
			br = new A71_BigTitledBorder(title+" (Four of a Kind)");
			br.setTitleColor(new Color(255,127,51)); //Bright Orange
			break;
		case JOKER:
			br = new A71_BigTitledBorder(title+" (Contains JOKER)");
			br.setTitleColor(new Color(255,51,0)); //Bright Red
			break;
		default:
			br = new A71_BigTitledBorder(title);
			br.setTitleColor(Color.BLACK); //Black
			break;
		}
		return br;
	}
	
	/** Declare the following private method to change the state in the object,
	    so that the computer doesn't have to calculate again when
	    calling getCardState() twice or more. */
	private void changeCardState(){
		int[] cardArray = new int[5]; /** Storing numbers generated in getCardNumber() */
		int[] cardCount = new int[6]; /** Counting numbers of A, 2, J, Q, K respectively,
		                                  but the last one is always zero. */
		
		/** Scanning the array and get card numbers. */
		for(int i=0;i<card_data.length;i++){
			cardArray[i] = getCardNumber(card_data[i]); //Recording a card number
			switch(cardArray[i]){
			case JOKER_NUMBER:
				card_state = CardState.JOKER;
				return;
			case -1:
				card_state = CardState.UNDECLARED;
				return;
			default:
				if(cardArray[i]<cardCount.length){
					cardCount[cardArray[i]]++; //Counting cards of same numbers
				}
			}
		}
		
		int first_largest_counted_cardnums = 5;
		int second_largest_counted_cardnums = 5;
		for(int i=0;i<5;i++){
			//The actual length of array cardCount is 6, but cardCount[5] is always zero.
			//System.out.print(cardCount[i]+" ");
			if(cardCount[second_largest_counted_cardnums] < cardCount[i]){
				second_largest_counted_cardnums = i;
			}
			if(cardCount[first_largest_counted_cardnums] < cardCount[i]){
				second_largest_counted_cardnums = first_largest_counted_cardnums;
				first_largest_counted_cardnums = i;
			}
		}
		//System.out.println();
		/** Keeps running here if there are ONLY one group of same numbers. */
		switch(cardCount[first_largest_counted_cardnums]){
		case 4:
			card_state = CardState.FOUR_OF_KIND;
			break;
		case 3:
			if(cardCount[second_largest_counted_cardnums]==2){
				card_state = CardState.FULL_HOUSE;
			}else{
				card_state = CardState.THREE_OF_KIND;
			}
			break;
		case 2:
			if(cardCount[second_largest_counted_cardnums]==2){
				card_state = CardState.TWO_PAIRS;
			}else{
				card_state = CardState.ONE_PAIR;
			}
			break;
		default:
			card_state = CardState.NOTHING;
		}
	}
	
	/** toString() returns each card numbers in String message */
	private static final String[] sign_arr = {"A", "2", "J", "Q", "K", "Joker"};
	public String toString(){
		String str="";
		for(int i=0;i<card_data.length;i++){
			str+="["+sign_arr[getCardNumber(card_data[i])]+"]";
		}
		return str;
	}
	
	private static int getCardNumber(int pic_id){
		return pic_id/4;
		/** The result 0 is for Aces; 1 for Twos; 2 for Jacks; 3 for Queens; 4 for Kings;
		               5 for Joker. */
	}
	private static final int JOKER_NUMBER = 5;
	
	
	private void drawOneCard(int index){
		try{
			card_data[index] = A71_AllOfCards.defaultGroup.drawCard();
			images[index] = Images.getCard(card_data[index]);
		}catch(Exception err){
			//System.err.println(err.getMessage());
		}
	}
	private void updateInitCard(int index){
		try{
			card_data[index] = -1;
			images[index] = Images.getInitCard(index+1);
		}catch(Exception err){
			System.err.println(err.getMessage());
		}
	}
	
}
