package a7.s100502514;

public class A71_WinnerJudge{
	public A71_WinnerLabel ComLabel;
	public A71_WinnerLabel YouLabel;
	
	/** This class can take 2 A71_WinnerLabels as arguments,
		namely these 2 labels can also be in different parent JPanels. */
	public A71_WinnerJudge(A71_WinnerLabel ComLabel, A71_WinnerLabel YouLabel){
		this.ComLabel = ComLabel;
		this.YouLabel = YouLabel;
	}
	
	public void Judge(User Com, User You){
		int who_wins = You.compareTo(Com);
		//System.out.println("You("+You.getCardState()+You+") : Com("+Com.getCardState()+Com+")");
		YouLabel.setState(who_wins);
		ComLabel.setState(-who_wins); //Reverse the result for the opposite player
	}
}
