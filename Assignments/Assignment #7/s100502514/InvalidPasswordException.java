/**
 * class InvalidPasswordException from CE1002 Assignment 7-2
 * For basic requirement for Assignment, please see on A72.java header comments.
 * This class is thrown when an invalid input occurred.
 * There are 5 subclasses for extra points:
	InvalidLengthException, NoLetterException, NoDigitException,
	EmptyPasswordException, InvalidCharacterException
 */
package a7.s100502514;

public class InvalidPasswordException extends Exception {
	public InvalidPasswordException(String message){
		super("Invalid Code:\n"+message);
	}
	
	public InvalidPasswordException(){
		/** Default Constructor */
		super("Invalid Code!!");
	}
}
