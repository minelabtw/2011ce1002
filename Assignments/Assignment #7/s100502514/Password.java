/**
 * class Password from CE1002 Assignment 7-2
 * For basic requirement for Assignment, please see on A72.java header comments.
 * This class is for password checking:
	1. The length must not be less than 7.
	2. Contains both alphabetic and numberic character(s).
 */
package a7.s100502514;

public class Password {
	private String code;
	private String error_message;
	public Password(String input){
		code = input;
		try{
			if(code.isEmpty()){
				throw new EmptyPasswordException(code);
			}else if(code.length()<7){
				throw new InvalidLengthException(code);
			}else if(containsInvalidChar(code)){
				throw new InvalidCharacterException(code);
			}else if(!containsLetter(code)){
				throw new NoLetterException(code);
			}else if(!containsDigit(code)){
				throw new NoDigitException(code);
			}
			error_message = ""; /** Empty error string for no errors. */
		}catch(InvalidPasswordException err){
			error_message = err.getMessage();
		}
	}
	public String getErrorMessage(){
		return new String(error_message);
	}
	
	/** Check if a string corrosponds to the rule. (the following 3 methods) */
	private static boolean containsLetter(String str){
		for(int i=0;i<str.length();i++){
			char c = str.charAt(i);
			if(c>='A'&&c<='Z' || c>='a'&&c<='z'){
				return true;
			}
		}
		return false;
	}
	private static boolean containsDigit(String str){
		for(int i=0;i<str.length();i++){
			char c = str.charAt(i);
			if(c>='0'&&c<='9'){
				return true;
			}
		}
		return false;
	}
	private static boolean containsInvalidChar(String str){
		for(int i=0;i<str.length();i++){
			char c = str.charAt(i);
			if((c<'0'||c>'9')&&(c<'A'||c>'Z')&&(c<'a'||c>'z')){
				return true;
			}
		}
		return false;
	}
}
