/**
 * CE1002-100502514 FlashTeens Chiang
 * [The Following Rules are from ce1002 website]
 * Assignment 7-1
 * 
 * Please write a program ��Poker Game��, and need the following requirements:

 * class User:
	* Some data members to store the cards
	* Some data members to store the type of cards. There are six types:
		Joker, Four of a kind, Full House, Three of kind, Two pairs, Nothing.
	* Their priority is listed from highest to lowest.
	* You should generated cards randomly.

 * class FrameWork:
	* Design GUI here.
	* You have to use the classes such as:
		GridLayout, Font, Boder, JLabel, JButton, ImageIcon, BorderLayout
	* You have to show the layout such as(at least):
		��random button��, ��the 2 sets of cards��, ��the type of cards��, ��game result��.
	* Please unzip the image.zip into the project directory
		and the path of the cards are ��image/card/xxxx.png��
	* The sample layout is on CE1002 Website.

 * Use the test program given by TA.
 * For more informations, please see on CE1002 site.
 */

/** The following code is the test program given by TA. (from CE1002 website) */

package a7.s100502514;

import javax.swing.*;

public class A71 {

	public static void main(String [] args){
		FrameWork f = new FrameWork();
		f.setTitle("Asignment #7");
		f.setSize(800, 550); /** Width adjusted from 1500 to 800 */
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}