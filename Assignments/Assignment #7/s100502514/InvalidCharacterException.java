/**
 * class NoDigitException extends InvalidPasswordException
 * A Subclass that indicates there is no any numeric characters in the code.
 */
package a7.s100502514;

public class InvalidCharacterException extends InvalidPasswordException {
	public InvalidCharacterException(String content){
		super("Code '"+content+"' contains INVALID CHARACTER(S)!!");
	}
	public InvalidCharacterException(){
		super("The Code contains INVALID CHARACTER(S)!!");
	}
}
