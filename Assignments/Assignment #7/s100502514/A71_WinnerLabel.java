package a7.s100502514;

import java.awt.*;

public class A71_WinnerLabel extends A71_BigLabel {
	public A71_WinnerLabel(){
		setState(null);
	}
	
	/** Refresh the winner state that is to be shown. */
	public void setState(Integer result){
		/** Here I use "Integer" instead of "int" as the parameter so that
		 	we can take null as the parameter too!! */
		if(result==null){
			setText("???");
			setForeground(new Color(153,153,102)); // Dark Kakhi (Yellowish Gray)
		}else if(result>0){
			setText("Winner! ^^");
			setForeground(new Color(102,170,0)); // Olive Green
		}else if(result<0){
			setText("Loser! ><");
			setForeground(new Color(224,0,51)); // Scarlet (Dark Red)
		}else{
			setText("Draw! = =");
			setForeground(new Color(0,170,255)); // Light Blue
		}
	}
}
