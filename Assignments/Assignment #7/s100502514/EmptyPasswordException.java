/**
 * class EmptyPasswordException extends InvalidPasswordException
 * A Subclass that indicates the code is empty.
 */
package a7.s100502514;

public class EmptyPasswordException extends InvalidPasswordException {
	public EmptyPasswordException(String content){
		this();
	}
	public EmptyPasswordException(){
		super("The Code is EMPTY!!");
	}
}
