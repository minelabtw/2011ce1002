/**
 * class NoLetterException extends InvalidPasswordException
 * A Subclass that indicates there is no any alphabetic characters in the code.
 */
package a7.s100502514;

public class NoLetterException extends InvalidPasswordException {
	public NoLetterException(String content){
		super("Code '"+content+"' has NO LETTERS!!");
	}
	public NoLetterException(){
		super("The Code has NO LETTERS!!");
	}
}
