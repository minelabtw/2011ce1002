package a7.s100502520;

import java.util.Random;

import javax.swing.ImageIcon;

public class User {
	public ImageIcon[] P = new ImageIcon[21];
	public ImageIcon Puser = new ImageIcon("image/card/init_5.png");
	public ImageIcon please = new ImageIcon("image/card/init_1.png");
	public ImageIcon press = new ImageIcon("image/card/init_2.png");
	public ImageIcon randombutton = new ImageIcon("image/card/init_3.png");
	public ImageIcon button = new ImageIcon("image/card/init_4.png");
	public int[] Pkind={1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,0};
	public int[] bosskind = new int[6];
	public int[] userkind = new int[6];
	public String bossresult;
	public String userresult;
	int[] rand = new int[21];
	
	public User(){
		//讀入撲克牌圖片
		P[0] = new ImageIcon("image/card/0.png");
		P[1] = new ImageIcon("image/card/1.png");
		P[2] = new ImageIcon("image/card/2.png");
		P[3] = new ImageIcon("image/card/3.png");
		P[4] = new ImageIcon("image/card/4.png");
		P[5] = new ImageIcon("image/card/5.png");
		P[6] = new ImageIcon("image/card/6.png");
		P[7] = new ImageIcon("image/card/7.png");
		P[8] = new ImageIcon("image/card/8.png");
		P[9] = new ImageIcon("image/card/9.png");
		P[10] = new ImageIcon("image/card/10.png");
		P[11] = new ImageIcon("image/card/11.png");
		P[12] = new ImageIcon("image/card/12.png");
		P[13] = new ImageIcon("image/card/3.png");
		P[14] = new ImageIcon("image/card/14.png");
		P[15] = new ImageIcon("image/card/15.png");
		P[16] = new ImageIcon("image/card/16.png");
		P[17] = new ImageIcon("image/card/17.png");
		P[18] = new ImageIcon("image/card/18.png");
		P[19] = new ImageIcon("image/card/19.png");
		P[20] = new ImageIcon("image/card/20.png");
	}
		
	public void run(){
		Random random = new Random();
		int x = 0;
		int swap;
		for(int j = 0; j<21; j++){
			rand[j] = j;
		}
		
		//利用陣列元素亂排取一未重複的數列
		for(int i = 0; i<100; i++){
			x = random.nextInt(21);
			swap = 0;
			swap = rand[0];
			rand[0] = rand[x];
			rand[x] = swap;
		}

		for(int m = 0; m<6; m++){
			bosskind[m] = 0;
			userkind[m] = 0;
		}
		
		//儲存牌的類型
		for(int k = 0; k<5; k++){
			bosskind[Pkind[rand[k]]]++;
		}
		
		for(int l = 5; l<10; l++){
			userkind[Pkind[rand[l]]]++;
		}
		
		bossresult = kind(bosskind);
		userresult = kind(userkind);
		WinOrLose();
	}
	
	//判斷牌組類型
	public String kind(int[] material ){
		if(material[0] == 1){
			return "<Joker>";
		}
		else{
			for(int i = 1; i<6; i++){
				if(material[i] == 4){
					return "<Four of a kind>";
				}
				
				else if(material[i] == 3){
					for(int j = 1; j<6; j++){
						if(material[j] == 2){
							return "<Full House>";
						}
					}
					return "<Three of kind>";
				}
				else if(material[i] == 2){
					for(int k = 1; k<6; k++){
						if(material[k] == 2 && k != i){
							return "<Two pairs>";
						}
					}
				}
			}
			return "<Nothing>";
		}
	}
	
	//判斷輸贏
	public String WinOrLose(){
		int BossScore = 0;
		int UserScore = 0;
		if(bossresult == "<Joker>"){
			BossScore = 6;
		}
		else if(bossresult == "<Four of a kind>"){
			BossScore = 5;
		}
		else if(bossresult == "<Full House>"){
			BossScore = 4;
		}
		else if(bossresult == "<Three of kind>"){
			BossScore = 3;
		}
		else if(bossresult == "<Two pairs>"){
			BossScore = 2;
		}
		else if(bossresult == "<Nothing"){
			BossScore = 1;
		}
		
		if(userresult == "<Joker>"){
			UserScore = 6;
		}
		else if(userresult == "<Four of a kind>"){
			UserScore = 5;
		}
		else if(userresult == "<Full House>"){
			UserScore = 4;
		}
		else if(userresult == "<Three of kind>"){
			UserScore = 3;
		}
		else if(userresult == "<Two pairs>"){
			UserScore = 2;
		}
		else if(userresult == "<Nothing"){
			UserScore = 1;
		}
	
		if(UserScore > BossScore){
			return "You Win!!";
		}
		else if (UserScore < BossScore){
			return "You Lose!!";
		}
		else{
			return "Draw!!";
		}		
	}
}
