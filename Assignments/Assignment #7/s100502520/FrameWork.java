package a7.s100502520;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class FrameWork extends JFrame implements ActionListener {
	public JFrame frame = new JFrame();
	public User useUser = new User();
	private JButton button = new JButton(">> random!! <<");
	private Font font1 = new Font("Boss", Font.BOLD, 30);
	private Font font2 = new Font("result", Font.BOLD, 50);
	private Font font3 = new Font("result", Font.BOLD, 20);
	private Border line = new LineBorder(Color.BLACK, 2);
	private JPanel p1 = new JPanel();
	private JPanel p2 = new JPanel();
	private JPanel p3 = new JPanel();
	private JPanel p4 = new JPanel();
	private JLabel end = new JLabel("Result");

	public FrameWork() {
		stableLabel();
		button.addActionListener(this);
	}

	public void setTitle(String title) {
		frame.setTitle(title);
	}

	public void setSize(int x, int y) {
		frame.setSize(x, y);
	}

	public void setLocationRelativeTo(Component jud) {
		frame.setLocationRelativeTo(jud);
	}

	public void setDefaultCloseOperation(int operation) {
		frame.setDefaultCloseOperation(operation);
	}

	public void setVisible(boolean jud) {
		frame.setVisible(jud);
	}

	//創立並顯示原始畫面
	public void stableLabel() {
		p1.setLayout(new GridLayout(4, 1));
		JLabel bossname = new JLabel("Boss");
		bossname.setFont(font3);
		JLabel username = new JLabel("Defier");
		username.setFont(font3);
		JLabel bossface = new JLabel("（☉ｏ☉）");
		bossface.setFont(font3);
		JLabel userpicture = new JLabel(useUser.Puser);
		p1.add(bossname);
		p1.add(bossface);
		p1.add(username);
		p1.add(userpicture);
		frame.add(p1, BorderLayout.WEST);

		p2.setLayout(new GridLayout(1, 4));
		p2.add(new JLabel(useUser.please));
		p2.add(new JLabel(useUser.press));
		p2.add(new JLabel(useUser.randombutton));
		p2.add(new JLabel(useUser.button));
		frame.add(p2, BorderLayout.CENTER);

		button.setFont(font2);
		p4.add(button, BorderLayout.CENTER);
		frame.add(p4, BorderLayout.SOUTH);
	}

	//當按下random鍵所執行的指令
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == button) {
			useUser.run();
			cards();
			cate();
			result();
		}
	}

	//顯示雙方的牌組
	public void cards() { 
		p2.removeAll();
		p2.setLayout(new GridLayout(2, 5, 2, 4));
		for (int i = 0; i < 10; i++) {
			JLabel label = new JLabel(useUser.P[useUser.rand[i]]);
			label.setBorder(line);
			p2.add(label);
		}
		frame.setVisible(true);

	}

	//顯示雙方牌組類型
	public void cate() {
		p3.removeAll();
		p3.setLayout(new GridLayout(2, 1));
		JLabel bossresult = new JLabel(useUser.bossresult);
		JLabel userresult = new JLabel(useUser.userresult);
		bossresult.setFont(font1);
		userresult.setFont(font1);
		p3.add(bossresult);
		p3.add(userresult);
		frame.add(p3, BorderLayout.EAST);
		frame.setVisible(true);
	}

	//顯示輸贏
	public void result() {
		p4.removeAll();
		end =new JLabel(useUser.WinOrLose());
		end.setFont(font2);
		button.setFont(font2);
		p4.setLayout(new GridBagLayout());
		GridBagConstraints c0 = new GridBagConstraints();
        c0.gridx = 0;
        c0.gridy = 0;
        c0.gridwidth = 4;
        c0.gridheight = 1;
        c0.weightx = 1;
        c0.weighty = 1;
        c0.fill = GridBagConstraints.BOTH;
        c0.anchor = GridBagConstraints.WEST;
        p4.add(button, c0);
        
        GridBagConstraints c1 = new GridBagConstraints();
        c1.gridx = 4;
        c1.gridy = 0;
        c1.gridwidth = 1;
        c1.gridheight = 1;
        c1.weightx = 1;
        c1.weighty = 1;
        c1.fill = GridBagConstraints.NONE;
        c1.anchor = GridBagConstraints.CENTER;
        p4.add(end, c1);
        frame.setVisible(true);
        
		
	}
}
