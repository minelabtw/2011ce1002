package a7.s100502520;

import java.util.Scanner;

public class Password extends InvalidPasswordException{
	private String password;
	public Password(){
		Scanner input = new Scanner(System.in);
		boolean run = true;
		while(run){
			//例外處理
			try{
				System.out.println("Please input a password to varify: ");
				password = input.next();
				TooShort();  //檢查長度
				NoNumberOrLetter();  //檢查字母和數字
				System.out.println("The password "+ password +" is valid!!");
				run = false;
			}
		
			//發生例外時顯是錯誤訊息
			catch(InvalidPasswordException e){
				System.out.println(e.getMessage());
				System.out.println("The password "+ password +" is not correct!!");
			}
		}
	}
	
	//判斷是否符合長度
	public void TooShort() throws InvalidPasswordException{
		if(password.length()<7){
			throw new InvalidPasswordException("The length of the password "+ password +" is not enough!!");
		}
	}
	
	//判斷是否含字母和數字
	public void NoNumberOrLetter() throws InvalidPasswordException{
		char[] word = password.toCharArray();
		int numb = 0;
		for(int i = 0; i<password.length(); i++){
			if(Character.isDigit(word[i])){
				numb++;
			}
		}
		//如果沒數字
		if(numb == 0){
			throw new InvalidPasswordException("The password "+ password +" has no number!!");
		}
		//如果沒字母
		else if(numb == password.length()){
			throw new InvalidPasswordException("The password "+ password +" has no letter!!");
		}
	}
}
