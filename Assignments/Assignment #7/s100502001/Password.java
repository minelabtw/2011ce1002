package a7.s100502001;

public class Password{
	private String pw;
	private char[] elements;
 	public  Password(String PW)throws InvalidPasswordException{
		pw=PW;
		elements=new char[pw.length()];
		for(int i=0;i<pw.length();i++) //separate the character of password
			elements[i]=pw.charAt(i);
		if(!check_length()) //the length is less than 7
			throw new InvalidPasswordException("The length of the password "+getPW()+" is not enough!!",getPW());
		else if(Is_letter_Or_num()){ //only can input letter or number
			if(All_Letter()) //all letter is wrong
				throw new InvalidPasswordException("The length of the password "+getPW()+" is no number!!",getPW());
			else if(All_Number()) //all number is wrong
				throw new InvalidPasswordException("The length of the password "+getPW()+" is no letter!!",getPW());
		}
		else //the password include not letter or number
			throw new InvalidPasswordException("You have to enter number and letter",getPW());
		
	}
 	public String getPW(){ //return the password
 		return pw;
 	}
	public boolean check_length(){ //密碼不得低於7碼
		if(pw.length()<7)
			return false;
		else
			return  true;
		
	}
	public boolean Is_letter_Or_num(){ //allow just letter and number
		for(int i=0;i<pw.length();i++){
			boolean isLetter=Character.isLetter(elements[i]);
			boolean isNumber=Character.isDigit(elements[i]);
			if(!isLetter && !isNumber) 
				return false;
		}
		return true;
	}
	public boolean All_Letter(){ //在Is_letter_Or_num()為true的前提下!!
		for(int i=0;i<pw.length();i++){
			if(Character.isDigit(elements[i]))
				return false; //false~則有數字
		}
		return true; //均為字母
	}
	public boolean All_Number(){
		for(int i=0;i<pw.length();i++){
			if(Character.isLetter(elements[i]))
				return false; //false~則有字母
		}
		return true; //均為數字
	}
	
}
