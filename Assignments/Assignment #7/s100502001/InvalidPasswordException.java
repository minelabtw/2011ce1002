package a7.s100502001;

public class InvalidPasswordException extends Exception{
	private String  statement;
	private String wrong_pw; 
	public InvalidPasswordException(String str,String pw){ //get the wrong message and password
		statement=str;
		wrong_pw=pw;
	}
	public String getStatement(){
		return statement;
	}
	public String getWrongPw(){
		return wrong_pw;
	}
}
