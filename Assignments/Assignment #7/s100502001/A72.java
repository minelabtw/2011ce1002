package a7.s100502001;
import java.util.Scanner;
public class A72{
	public static void main(String[] args){
		boolean judge=true;
		while(judge){
			Scanner input=new Scanner(System.in);
			System.out.println("1.Please input a password to varify:\n2.exit");
			int choice=input.nextInt(); // input or exit
			switch(choice){
				case 1:
					String pw=input.next(); 
					try{
						Password temp_pw=new Password(pw); //有throw就不會print下方的statements
						System.out.println("The password "+temp_pw.getPW()+" is valid!!");
					}
					catch(InvalidPasswordException error){
						System.out.println(error.getStatement());
						System.out.println("The password "+error.getWrongPw()+" is not correct!!");
						input.nextLine();// discard the current line
					}
					break;
				case 2:
					judge=false;
					System.out.println("See you next time.");
					break;
				default:
					System.out.println("There is not the choice.");
					break;
			}
		}
	}
}



