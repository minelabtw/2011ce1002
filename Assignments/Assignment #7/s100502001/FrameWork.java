package a7.s100502001;
import java.util.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
public class FrameWork extends JFrame implements ActionListener{
	private JButton button = new JButton("Random"); //create button
	private JLabel[] u1; //image of popo
	private JLabel[] u2; //image of player
	private JLabel con1; //description of popo
	private JLabel con2; //description of player
	private JLabel compare; //description of compare
	private User popo;
	private User player;
	public FrameWork (){ //initialize
		popo=new User("Popo");
	    player=new User("Player");
		Font font1=new Font("ScanSerif",Font.BOLD,12);
		Font font2=new Font("ScanSerif",Font.BOLD,30);
		ArrayList<ImageIcon> init=new ArrayList<ImageIcon>(); //初始圖片
		for(int i=1;i<=5;i++)
			init.add(new ImageIcon("image/card/init_"+i+".png"));
		
		Panel Card=new Panel(); //card
		Border lineborder=new LineBorder(Color.black,3);
		Card.setLayout(new BorderLayout());
		Card.setLayout(new GridLayout(2,6));
		u1=new JLabel[5];
		for(int popo_button=0;popo_button<5;popo_button++){ //初始化圖片
			u1[popo_button]=new JLabel(init.get(popo_button));
			u1[popo_button].setBorder(lineborder);
			Card.add(u1[popo_button]); 
		}
		con1=new JLabel("");
		Card.add(con1);
		u2=new JLabel[5];
		for(int player_button=0;player_button<5;player_button++){ //初始化圖片
			u2[player_button]=new JLabel(init.get(player_button));
			u2[player_button].setBorder(new LineBorder(Color.black,3));
			Card.add(u2[player_button]);
		}
		con2=new JLabel("");
		Card.add(con2);
		add(Card);
		
		Panel User=new Panel();//user name
		User.setLayout(new BorderLayout());
		User.setLayout(new GridLayout(2,1));
		JLabel user1=new JLabel(popo.getName());
		JLabel user2=new JLabel(player.getName());
		user1.setFont(font1);
		User.add(user1);
		user2.setFont(font1);
		User.add(user2);
		add(User,BorderLayout.WEST);
		
		Panel ran=new Panel(); //random button
		ran.setLayout(new BorderLayout());
		ran.setLayout(new GridLayout(1,2));
		ran.add(button);
		compare=new JLabel("");
		compare.setFont(font2);
		ran.add(compare);
		add(ran,BorderLayout.SOUTH);

		button.addActionListener(this); //active button event here
	}
	public void actionPerformed(ActionEvent e){ //按下button之後要改變圖片和文字
		if(e.getSource() == button){
			do{ //出牌
				    popo=new User("Popo");
				    player=new User("Player");
					popo.setNumber();
					popo.sort();
					player.setNumber();
					player.sort();
			}
			while(popo.IsSameNumber()||player.IsSameNumber());
				
			for(int popo_button=0;popo_button<5;popo_button++)//顯示popo的牌
				u1[popo_button].setIcon(popo.getcard()[popo_button]);
			for(int player_button=0;player_button<5;player_button++)//顯示player的牌
				u2[player_button].setIcon(player.getcard()[player_button]);
			con1.setText(getresult(popo));
			con2.setText(getresult(player));
			compare.setText(popo.GameResult(player));
		}
	}
	public String getresult(User name){ //回傳牌形
		if(name.isPoker())
			return "< Joker >";
		else if(name.isFourofkind())
			return "< Four of kind >";
		else if(name.isFullHouse())
			return "< Full House >";
		else if(name.isThreeofkind())
			return "< Three of kind >";
		else if(name.isTwopairs())
			return "< Two pairs >";
		else 
			return "< Nothing >";
	}
	
}
