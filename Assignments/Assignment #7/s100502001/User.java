package a7.s100502001;

import java.awt.*;
import javax.swing.*;

import java.util.*;

public class User {
	// Poker, Four of a kind, Full House, Three of kind, Two pairs, Nothing 21張牌
	private ArrayList<ImageIcon> card;// ?? <...>裡面要放class的名稱
	private int num; // 使用者一次可出幾次牌
	private int[] user; // 使用者的牌編碼
	private int A;// 各種牌的數量
	private int N2;
	private int J;
	private int Q;
	private int K;
	private int Poker;
	private String name;

	public User(String username) { // initialize the card's choice by random
		card = new ArrayList<ImageIcon>();
		for (int i = 0; i <= 20; i++)
			// 存圖片進來
			card.add(new ImageIcon("image/card/" + i + ".png"));
		A = 0;
		N2 = 0;
		J = 0;
		Q = 0;
		K = 0;
		Poker = 0;
		name = username;
		num = 5; // 使用者一次有五張牌
		user = new int[num];
	}

	public String getName() {
		return name;
	}

	public void setNumber() { // 產生0~20的號碼(代表牌)
		for (int i = 0; i < num; i++) {
			user[i] = (int) (Math.random() * 21); // integer 0~20
		}
	}

	public boolean IsSameNumber() { // 牌號是否有重複
		for (int i = 0; i < num - 1; i++) {
			for (int j = i + 1; j < num; j++) {
				if (user[i] == user[j])
					return true;
			}
		}
		return false;
	}

	public String card(int num) { // 以號碼將牌種歸類
		if (num <= 3)
			return "A";
		else if (num <= 7)
			return "2";
		else if (num <= 11)
			return "J";
		else if (num <= 15)
			return "Q";
		else if (num <= 19)
			return "K";
		else
			return "Poker";
	}

	public void sort() { // 歸類
		for (int i = 0; i < num; i++) {
			if (card(user[i]) == "A")
				A++;
			else if (card(user[i]) == "2")
				N2++;
			else if (card(user[i]) == "J")
				J++;
			else if (card(user[i]) == "Q")
				Q++;
			else if (card(user[i]) == "K")
				K++;
			else
				Poker++;
		}
	}

	public boolean isPoker() { // 鬼牌(最大)
		if (Poker == 1)
			return true;
		else
			return false;
	}

	public boolean isFourofkind() { // 4同(二)
		if (A == 4 || N2 == 4 || J == 4 || Q == 4 || K == 4)
			return true;
		else
			return false;
	}

	public boolean isFullHouse() { // 3同2同(三)
		if (A == 3 || N2 == 3 || J == 3 || Q == 3 || K == 3) {
			if (A == 2 || N2 == 2 || J == 2 || Q == 2 || K == 2)
				return true;
			else
				return false;
		} else
			return false;
	}

	public boolean isThreeofkind() { // 3同2異(四)
		if (A == 3 || N2 == 3 || J == 3 || Q == 3 || K == 3) {
			if (A < 2 && N2 < 2 && J < 2 && Q < 2 && K < 2)
				return true;
			else
				return false;
		} else
			return false;
	}

	public boolean isTwopairs() { // 2同2同1異(五)
		if (A == 2 && (N2 == 2 || J == 2 || Q == 2 || K == 2))
			return true;
		else if (N2 == 2 && (A == 2 || J == 2 || Q == 2 || K == 2))
			return true;
		else if (J == 2 && (A == 2 || N2 == 2 || Q == 2 || K == 2))
			return true;
		else if (Q == 2 && (A == 2 || N2 == 2 || J == 2 || K == 2))
			return true;
		else if (K == 2 && (A == 2 || N2 == 2 || J == 2 || Q == 2))
			return true;
		else
			return false;
	}

	public ImageIcon[] getcard() { // 回傳圖片
		ImageIcon[] Card = new ImageIcon[num];
		for (int i = 0; i < num; i++)
			Card[i] = card.get(user[i]);
		return Card;
	}

	public String GameResult(User TheOther) {
		if (isPoker()&&!TheOther.isPoker())
			return name + "Win";
		else if (TheOther.isPoker()&&!isPoker())
			return TheOther.getName() + "Win";
		else if (isFourofkind()&&!TheOther.isFourofkind())
			return name + "Win";
		else if (TheOther.isFourofkind()&&!isFourofkind())
			return TheOther.getName() + "Win";
		else if (isFullHouse()&&!TheOther.isFullHouse())
			return name + "Win";
		else if (TheOther.isFullHouse()&&!isThreeofkind())
			return TheOther.getName() + "Win";
		else if (isThreeofkind()&&!TheOther.isThreeofkind())
			return name + "Win";
		else if (TheOther.isThreeofkind()&&isTwopairs())
			return TheOther.getName() + "Win";
		else if (isTwopairs()&&!TheOther.isTwopairs())
			return name + "Win";
		else if (TheOther.isTwopairs()&&!isTwopairs())
			return TheOther.getName() + "Win";
		else
			return "Peace";
	}
}
