package a7.s995002204;

import java.util.Random;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener{
	
	//declare Object
	private JPanel frame_1 = new JPanel();
	private JPanel frame_2 = new JPanel();
	private JPanel frame_3 = new JPanel();
	private JLabel user_name_1;
	private JLabel user_name_2;
	private JButton button = new JButton("random");
	private JLabel photo1 = new JLabel();
	private JLabel photo2 = new JLabel();
	private JLabel photo3 = new JLabel();
	private JLabel photo4 = new JLabel();
	private JLabel photo5 = new JLabel();
	private JLabel photo6 = new JLabel();
	private JLabel photo7 = new JLabel();
	private JLabel photo8 = new JLabel();
	private JLabel photo9 = new JLabel();
	private JLabel photo10 = new JLabel();
	
	private JLabel final_answer1;
	private JLabel final_answer2;
	private JLabel result;
	
	public FrameWork()
	{
		super("Asignment #7");
		frame_1.setLayout(new GridLayout(2,6));
		frame_2.setLayout(new GridLayout(3,1));
		frame_3.setLayout(new BorderLayout(1,1));
		
		button.addActionListener(this);
		
		//frame1
		user_name_1 = new JLabel("He");		//computer opponent name
		user_name_2 = new JLabel("You");	//user name
		user_name_1.setSize(72,96);
		user_name_1.setFont(new Font( "新細明體", Font.PLAIN, 40));
		user_name_2.setSize(72,96);
		user_name_2.setFont(new Font( "新細明體", Font.PLAIN, 40 ));
		//set image to label
		ImageIcon image1 = new ImageIcon("src/image/card/init_1.png");
		photo1.setIcon(image1);
		photo1.setBorder(BorderFactory.createLineBorder (Color.black,1));
		ImageIcon image2 = new ImageIcon("src/image/card/init_2.png");
		photo2.setIcon(image2);
		photo2.setBorder(BorderFactory.createLineBorder (Color.black,1));
		ImageIcon image3 = new ImageIcon("src/image/card/init_3.png");
		photo3.setIcon(image3);
		photo3.setBorder(BorderFactory.createLineBorder (Color.black,1));
		ImageIcon image4 = new ImageIcon("src/image/card/init_4.png");
		photo4.setIcon(image4);
		photo4.setBorder(BorderFactory.createLineBorder (Color.black,1));
		ImageIcon image5 = new ImageIcon("src/image/card/init_5.png");
		photo5.setIcon(image5);
		photo5.setBorder(BorderFactory.createLineBorder (Color.black,1));
		ImageIcon image6 = new ImageIcon("src/image/card/init_1.png");
		photo6.setIcon(image6);
		photo6.setBorder(BorderFactory.createLineBorder (Color.black,1));
		ImageIcon image7 = new ImageIcon("src/image/card/init_2.png");
		photo7.setIcon(image7);
		photo7.setBorder(BorderFactory.createLineBorder (Color.black,1));
		ImageIcon image8 = new ImageIcon("src/image/card/init_3.png");
		photo8.setIcon(image8);
		photo8.setBorder(BorderFactory.createLineBorder (Color.black,1));
		ImageIcon image9 = new ImageIcon("src/image/card/init_4.png");
		photo9.setIcon(image9);
		photo9.setBorder(BorderFactory.createLineBorder (Color.black,1));
		ImageIcon image10 = new ImageIcon("src/image/card/init_5.png");
		photo10.setIcon(image10);
		photo10.setBorder(BorderFactory.createLineBorder (Color.black,1));
		
		//frame1
		frame_1.add(user_name_1);
		frame_1.add(photo1);
		frame_1.add(photo2);
		frame_1.add(photo3);
		frame_1.add(photo4);
		frame_1.add(photo5);
		frame_1.add(user_name_2);
		frame_1.add(photo6);
		frame_1.add(photo7);
		frame_1.add(photo8);
		frame_1.add(photo9);
		frame_1.add(photo10);

		
		//frame2
		final_answer1 = new JLabel("Nothing");
		final_answer1.setFont(new Font( "新細明體", Font.PLAIN, 60));
		final_answer2 = new JLabel("Nothing");
		final_answer2.setFont(new Font( "新細明體", Font.PLAIN, 60));
		result = new JLabel("No result");
		result.setFont(new Font( "新細明體", Font.PLAIN, 60));
		frame_2.add(final_answer1);
		frame_2.add(final_answer2);
		frame_2.add(result);
		
		//frame3
		frame_3.add(frame_1,BorderLayout.WEST);
		frame_3.add(frame_2,BorderLayout.EAST);
		frame_3.add(button,BorderLayout.SOUTH);
		add(frame_3,BorderLayout.CENTER);

	}
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == button){
			Random number = new Random();
			ImageIcon image[] = new ImageIcon[22];
			image[1] = new ImageIcon("src/image/card/0.png");
			image[2] = new ImageIcon("src/image/card/1.png");
			image[3] = new ImageIcon("src/image/card/2.png");
			image[4] = new ImageIcon("src/image/card/3.png");
			image[5] = new ImageIcon("src/image/card/4.png");
			image[6] = new ImageIcon("src/image/card/5.png");
			image[7] = new ImageIcon("src/image/card/6.png");
			image[8] = new ImageIcon("src/image/card/7.png");
			image[9] = new ImageIcon("src/image/card/8.png");
			image[10] = new ImageIcon("src/image/card/9.png");
			image[11] = new ImageIcon("src/image/card/10.png");
			image[12] = new ImageIcon("src/image/card/11.png");
			image[13] = new ImageIcon("src/image/card/12.png");
			image[14] = new ImageIcon("src/image/card/13.png");
			image[15] = new ImageIcon("src/image/card/14.png");
			image[16] = new ImageIcon("src/image/card/15.png");
			image[17] = new ImageIcon("src/image/card/16.png");
			image[18] = new ImageIcon("src/image/card/17.png");
			image[19] = new ImageIcon("src/image/card/18.png");
			image[20] = new ImageIcon("src/image/card/19.png");
			image[21] = new ImageIcon("src/image/card/20.png");
			
			int array[] = new int[10];
			int var1=0,var2=0,var3=0,var4=0,var5=0,var6=0;
			int va1=0,va2=0,va3=0,va4=0,va5=0,va6=0;
			int priority1=0,priority2=0;
			int count1 = 0;
			int count2 = 0;
			
			for(int i=0;i<10;i++)
			{
				int choose = 1+number.nextInt(21);
				array[i] = choose;
				for(int j=0;j<i;j++)
				{
					if(array[i]==array[j])
					{
						i--;
						break;
					}
				}
			}
			
			photo1.setIcon(image[array[0]]);
			photo2.setIcon(image[array[1]]);
			photo3.setIcon(image[array[2]]);
			photo4.setIcon(image[array[3]]);
			photo5.setIcon(image[array[4]]);
			photo6.setIcon(image[array[5]]);
			photo7.setIcon(image[array[6]]);
			photo8.setIcon(image[array[7]]);
			photo9.setIcon(image[array[8]]);
			photo10.setIcon(image[array[9]]);
			
			frame_1.add(user_name_1);
			frame_1.add(photo1);
			frame_1.add(photo2);
			frame_1.add(photo3);
			frame_1.add(photo4);
			frame_1.add(photo5);
			frame_1.add(user_name_2);
			frame_1.add(photo6);
			frame_1.add(photo7);
			frame_1.add(photo8);
			frame_1.add(photo9);
			frame_1.add(photo10);
			
			for(int i=0;i<5;i++)
			{
				if(array[i]==1||array[i]==2||array[i]==3||array[i]==4)
					var1++;
				else if(array[i]==5||array[i]==6||array[i]==7||array[i]==8)
					var2++;
				else if(array[i]==9||array[i]==10||array[i]==11||array[i]==12)
					var3++;
				else if(array[i]==13||array[i]==14||array[i]==15||array[i]==16)
					var4++;
				else if(array[i]==17||array[i]==18||array[i]==19||array[i]==20)
					var5++;
				else
					var6++;
			}
			for(int i=5;i<10;i++)
			{
				if(array[i]==1||array[i]==2||array[i]==3||array[i]==4)
					va1++;
				else if(array[i]==5||array[i]==6||array[i]==7||array[i]==8)
					va2++;
				else if(array[i]==9||array[i]==10||array[i]==11||array[i]==12)
					va3++;
				else if(array[i]==13||array[i]==14||array[i]==15||array[i]==16)
					va4++;
				else if(array[i]==17||array[i]==18||array[i]==19||array[i]==20)
					va5++;
				else if(array[i]==21)
					va6++;
			}
			
			if((var1==4||var2==4||var3==4||var4==4||var5==4)&&(var6==0))
			{
				final_answer1.setText("<Four of a kind>");
			}
			if((va1==4||va2==4||va3==4||va4==4||va5==4)&&(va6==0))
			{
				final_answer2.setText("<Four of a kind>");
			}
			if((var1==3||var2==3||var3==3||var4==3||var5==3)&&(var6==0))
			{
				if((var1==2||var2==2||var3==2||var4==2||var5==2)&&(var6==0))
				{
					final_answer1.setText("<Full House>");
				}	
				else
				{
					final_answer1.setText("<Three of kind>");
				}
			}
			if((va1==3||va2==3||va3==3||va4==3||va5==3)&&(va6==0))
			{
				if((va1==2||va2==2||va3==2||va4==2||va5==2)&&(va6==0))
				{
					final_answer2.setText("<Full House>");
				}
				else
				{
					final_answer2.setText("<Three of kind>");
				}
			}
			if((var1==2||var2==2||var3==2||var4==2||var5==2)&&(var6==0))
			{
				count1=0;
				if(var1==2)
					count1++;
				if(var2==2)
					count1++;
				if(var3==2)
					count1++;
				if(var4==2)
					count1++;
				if(var5==2)
					count1++;
				if(count1==2)
				{
					final_answer1.setText("<Two pairs>");
				}
				else
				{
					final_answer1.setText("<Nothing>");
				}
			}
			if((va1==2||va2==2||va3==2||va4==2||va5==2)&&(va6==0))
			{
				if(va1==2)
				{
					count2++;
				}
				if(va2==2)
				{
					count2++;
				}
				if(va3==2)
				{
					count2++;
				}
				if(va4==2)
				{
					count2++;
				}
				if(va5==2)
				{
					count2++;
				}
				if(count2==2)
				{
					final_answer2.setText("<Two pairs>");
				}
				else
				{
					final_answer2.setText("<Nothing>");
				}
			}
			
			if(var6==1)
			{
				final_answer1.setText("<Joker>");
			}
			
			if(va6==1)
			{
				final_answer2.setText("<Joker>");
			}
			
			if(final_answer1.getText()=="<Joker>")
				priority1 = 6;
			if(final_answer2.getText()=="<Joker>")
				priority2 = 6;
			if(final_answer1.getText()=="<Four of a kind>")
				priority1 = 5;
			if(final_answer2.getText()=="<Four of a kind>")
				priority2 = 5;
			if(final_answer1.getText()=="<Full House>")
				priority1 = 4;
			if(final_answer2.getText()=="<Full House>")
				priority2 = 4;
			if(final_answer1.getText()=="<Three of kind>")
				priority1 = 3;
			if(final_answer2.getText()=="<Three of kind>")
				priority2 = 3;
			if(final_answer1.getText()=="<Two pairs>")
				priority1 = 2;
			if(final_answer2.getText()=="<Two pairs>")
				priority2 = 2;
			if(final_answer1.getText()=="<Nothing>")
				priority1 = 1;
			if(final_answer2.getText()=="<Nothing>")
				priority2 = 1;
			
			
			if(priority1>priority2)
				result.setText("<He wins>");
			else if(priority1<priority2)
				result.setText("<You wins>");
			else
				result.setText("<Draw>");
				
			if(priority1==6)
			{
				result.setText("<He wins>");
			}
			if(priority2==6)
			{
				result.setText("<You wins>");
			}
			
			frame_2.add(final_answer1);
			frame_2.add(final_answer2);
			frame_2.add(result);
		}
	}
	
}


