package a7.s100502004;
import javax.swing.*;

import java.awt.*;

public class User extends JFrame{	
	int random1;
	int random2;
	int random3;
	int random4;
	int random5;
	
	public User(){
			
	}
	
	public void Random(){
		random1 = (int) (Math.random()*21);
		while(true){
			random2 = (int) (Math.random()*21);
			if(random2!=random1){				
				break;
			}
		}
		while(true){
			random3 = (int) (Math.random()*21);
			if(random3!=random1&&random3!=random2){				
				break;
			}
		}
		while(true){
			random4 = (int) (Math.random()*21);
			if(random4!=random1&&random4!=random2&&random4!=random3){				
				break;
			}
		}
		while(true){
			random5 = (int) (Math.random()*21);
			if(random5!=random1&&random5!=random2&&random5!=random3&&random5!=random4){				
				break;
			}
		}		
	}
	
	public  int check(int card1 , int card2 , int card3 , int card4 , int card5){
		int cal1=0;
		int cal2=0;
		int cal3=0;
		int cal4=0;
		int cal5=0;
		int [] b = new int [5];
		int checkPairNumber=0;
		boolean checkJoker=false;
		boolean checkFour=false;
		boolean checkFull=false;
		boolean checkThree=false;
		boolean checkTwoPairs=false;
		boolean checkOnePairs=false;
		b[0]=card1;
		b[1]=card2;
		b[2]=card3;
		b[3]=card4;
		b[4]=card5;
		
		for(int i =0;i<5;i++){
			if(b[i]==0||b[i]==1||b[i]==2||b[i]==3){
				cal1++;
			}
			if(b[i]==4||b[i]==5||b[i]==6||b[i]==7){
				cal2++;
			}
			if(b[i]==8||b[i]==9||b[i]==10||b[i]==11){
				cal3++;
			}
			if(b[i]==12||b[i]==13||b[i]==14||b[i]==15){
				cal4++;
			}
			if(b[i]==16||b[i]==17||b[i]==18||b[i]==19){
				cal5++;
			}
			if(b[i]==20){
				checkJoker=true;
			}
		}
		

		
		//Use the five check to choose what kind of the card it is		
		//Create a array to determine what kind of type they are
		int [] a = new int [5];
		a[0]=cal1;//store how many time 
		a[1]=cal2;
		a[2]=cal3;
		a[3]=cal4;
		a[4]=cal5;
		for(int i=0;i<5;i++){//Use to check the four of a kind
			if(a[i]==4){
				checkFour=true;
			}
		}
		
		for(int i=0;i<5;i++){//Use to check the three of the kind
			if(a[i]==3){
				checkThree=true;
			}
		}
		
		for(int i=0;i<5;i++){
			if(a[i]==2){
				checkPairNumber++;
			}
		}
		
		if(checkPairNumber==1){ //this "if" is use to check how many pair it has
			checkOnePairs=true;
		}
		else if(checkPairNumber==2){
			checkTwoPairs=true;
		}	
		else{
			checkOnePairs=false;
			checkTwoPairs=false;
		}
		
		if(checkThree&&checkOnePairs){//Use to check the full house
			checkFull=true;
		}
		
		if(checkJoker){
			return 7;
		}
		else if(checkFour){
			return 6;
		}
		else if(checkFull){
			return 5;
		}
		else if(checkThree&&checkFull==false){
			return 4;
		}
		else if(checkTwoPairs){
			return 3;
		}
		else if(checkOnePairs&&checkTwoPairs==false){
			return 2;
		}
		else{
			return 1;
		}
		
	}
	
	
	
	
	
	
	
}
