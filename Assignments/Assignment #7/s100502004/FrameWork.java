package a7.s100502004;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrameWork extends JFrame  implements ActionListener{
	private ImageIcon cardImage[] = new ImageIcon[21];
	private ImageIcon Joker = new ImageIcon("image/card/20.png");
	private ImageIcon Please = new ImageIcon("image/card/init_1.png");
	private ImageIcon Press = new ImageIcon("image/card/init_2.png");
	private ImageIcon Random = new ImageIcon("image/card/init_3.png");
	private ImageIcon Button = new ImageIcon("image/card/init_4.png");
	private ImageIcon P = new ImageIcon("image/card/init_5.png");
	JLabel card1 = new JLabel(Please);
	JLabel card2 = new JLabel(Press);
	JLabel card3 = new JLabel(Random);
	JLabel card4 = new JLabel(Button);
	JLabel card5 = new JLabel("                                                   ");
	JLabel card6 = new JLabel(Please);
	JLabel card7 = new JLabel(Press);
	JLabel card8 = new JLabel(Random);
	JLabel card9 = new JLabel(Button);
	JLabel card10 = new JLabel("                                                   ");
	JLabel cardP1 = new JLabel(P);
	JLabel cardP2 = new JLabel(P);
	JLabel p2s = new JLabel("                                                   ");
	JLabel He = new JLabel("He");
	JLabel You = new JLabel("You");
	JLabel JokerText = new JLabel("<Joker>");
	JLabel FourText = new JLabel("<Four of a kind>");
	JLabel FullText = new JLabel("<Full House>");
	JLabel ThreeText = new JLabel("<Three of a kind>");
	JLabel TwoText = new JLabel("<Two Pairs>");
	JLabel OneText = new JLabel("<One Pair>");
	JLabel NothingText = new JLabel("<Nothing>");	
	JButton button = new JButton("random");
	
//	@Override
//	public void actionPerformed(ActionEvent e) {
//		// TODO Auto-generated method stub
//		
//	}
	public FrameWork(){
		Font font1 = new Font("Serif",Font.BOLD,72);
				
		for(int i=0;i<cardImage.length;i++){
			cardImage[i]=new ImageIcon("image/card/"+i+".png");
		}
		card1.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		card2.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		card3.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		card4.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		cardP1.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		card6.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		card7.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		card8.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		card9.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		cardP2.setBorder(BorderFactory.createLineBorder(Color.BLACK));	
		
		
		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(1,6,1,30));
		He.setFont(new Font("標楷體", Font.BOLD, 50));		
		p1.add(He);
		p1.add(card1);
		p1.add(card2);
		p1.add(card3);	
		p1.add(card4);	
		p1.add(cardP1);
		
		JPanel p2 = new JPanel();
		p2.setLayout(new GridLayout(1,6,1,30));
		You.setFont(new Font("標楷體", Font.BOLD, 50));
		p2.add(You);
		p2.add(card6);
		p2.add(card7);
		p2.add(card8);
		p2.add(card9);			
		p2.add(cardP2);
				
		
		JPanel p3 = new JPanel();
		button = new JButton("Random");
		button.setFont(font1);
		button.addActionListener(this);
		p3.setLayout(new GridLayout(1,1));
		p3.add(button);
		p3.add(p2s);
		
		JPanel p4 = new JPanel();
		p4.setLayout(new GridLayout(3,1,20,20));
		p4.add(card5);
		p4.add(card10);
		p4.add(p2s);
		
		JPanel p5 = new JPanel();
		p5.setLayout(new GridLayout(3,1));
		p5.add(p1);
		p5.add(p2);
		p5.add(p3);
		
		add(p4,BorderLayout.EAST);
		  add(p5,BorderLayout.CENTER);
		
		
	}
	
	
	public void actionPerformed(ActionEvent e){

		if(e.getSource().equals(button)){
			
			User comu = new User();
			comu.Random();
			card6.setIcon(cardImage[comu.random1]);
			card7.setIcon(cardImage[comu.random2]);
			card8.setIcon(cardImage[comu.random3]);
			card9.setIcon(cardImage[comu.random4]);
			cardP2.setIcon(cardImage[comu.random5]);
			switch(comu.check(comu.random1, comu.random2,comu.random3, comu.random4,comu.random5)){
				case 1:
					card10.setFont(new Font("標楷體", Font.BOLD, 36));
					card10.setText("<Nothing>");
					break;
				case 2:
					card10.setFont(new Font("標楷體", Font.BOLD, 36));
					card10.setText("<One Pair>");
					break;
				case 3:
					card10.setFont(new Font("標楷體", Font.BOLD, 36));
					card10.setText("<Two Pairs>");
					break;
				case 4:
					card10.setFont(new Font("標楷體", Font.BOLD, 36));
					card10.setText("<Three of a kind>");
					break;
				case 5:
					card10.setFont(new Font("標楷體", Font.BOLD, 36));
					card10.setText("<Full House>");
					break;
				case 6:
					card10.setFont(new Font("標楷體", Font.BOLD, 36));
					card10.setText("<Four of a kind>");
					break;
				case 7:
					card10.setFont(new Font("標楷體", Font.BOLD, 36));
					card10.setText("<Joker>");
					break;
			}			
			
			
			User u = new User();
			u.Random();
			card1.setIcon(cardImage[u.random1]);
			card2.setIcon(cardImage[u.random2]);
			card3.setIcon(cardImage[u.random3]);
			card4.setIcon(cardImage[u.random4]);
			cardP1.setIcon(cardImage[u.random5]);
			switch(u.check(u.random1, u.random2,u.random3, u.random4,u.random5)){
				case 1:
					card5.setFont(new Font("標楷體", Font.BOLD, 36));
					card5.setText("<Nothing>");
					break;
				case 2:
					card5.setFont(new Font("標楷體", Font.BOLD, 36));
					card5.setText("<One Pair>");
					break;
				case 3:
					card5.setFont(new Font("標楷體", Font.BOLD, 36));
					card5.setText("<Two Pairs>");
					break;
				case 4:
					card5.setFont(new Font("標楷體", Font.BOLD, 36));
					card5.setText("<Three of a kind>");
					break;
				case 5:
					card5.setFont(new Font("標楷體", Font.BOLD, 36));
					card5.setText("<Full House>");
					break;
				case 6:
					card5.setFont(new Font("標楷體", Font.BOLD, 36));
					card5.setText("<Four of a kind>");
					break;
				case 7:
					card5.setFont(new Font("標楷體", Font.BOLD, 36));
					card5.setText("<Joker>");
					break;
			}						
				
			
			if(u.check(u.random1, u.random2,u.random3, u.random4,u.random5)>comu.check(comu.random1, comu.random2,comu.random3, comu.random4,comu.random5)){
				p2s.setFont(new Font("標楷體", Font.BOLD, 72));
				p2s.setText("You lose.    ");
			}
			else if(u.check(u.random1, u.random2,u.random3, u.random4,u.random5)<comu.check(comu.random1, comu.random2,comu.random3, comu.random4,comu.random5)){
				p2s.setFont(new Font("標楷體", Font.BOLD, 72));
				p2s.setText("You win.     ");
			}
			else{
				p2s.setFont(new Font("標楷體", Font.BOLD,72));
				p2s.setText("Draw.        ");
			}
			
			
			
			
		}		
	}		
}
