package a7.s100502004;
import java.awt.font.NumericShaper;

public class Password {
	String password = "";
	char[] a = new char[7];
	int number = 0;
	int letter = 0;
	int total = 0;

	public Password(String input) throws InvalidPasswordException{
		password = input;
		if(password.length()>7){
			throw new InvalidPasswordException(4,password);
		}
		
		for(int i = 0; i <password.length() ; i++){
			a[i] =password.charAt(i);
		}
		
		for(int i=0 ; i < password.length() ; i++){
			if(Character.isLetter(a[i])){
				letter++;
			}
			else {
				number++;
			}				
		}
		
		total = number+letter;
		
		if(total<7){
			throw new InvalidPasswordException(1,password);
		}
				
		else{			
			if(number==0&&letter!=0){
				throw new InvalidPasswordException(2,password);
			}
			if(number!=0&&letter==0){
				throw new InvalidPasswordException(3,password);
			}		
		}
		
		
	}
	
	
	
}
