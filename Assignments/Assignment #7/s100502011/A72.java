package a7.s100502011;

import java.util.Scanner;
public class A72 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		boolean flag = true; // stop or continue
		while(flag){ // infinite input
			
			System.out.print("Please enter a password :");
			String password = input.next();
			try{ //processing file
				Password word = new Password(password);
				word.JudgeException();
				System.out.println("The password "+password+" is valid!!");
			}
			catch(InvalidPasswordException ex){ // exception
				System.out.println(ex.getException());
				System.out.println("The password "+password+" is not correct!!\n");
			}
		}
	}
}
