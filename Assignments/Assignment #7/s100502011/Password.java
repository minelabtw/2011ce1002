package a7.s100502011;

public class Password {
	private String yourpass; // your password
	private int NumOfalphabat = 0; // count how many letter
	private int NumOfNum = 0; // count how many number
	public Password(String password){ // constructor
		yourpass = password;
		JudgeNumOrAlpha(yourpass); // count char
		
	}
	public void JudgeNumOrAlpha(String word){ // count number and letter
		char[] wordOfChar = word.toCharArray(); // store to char array
		for(int i = 0;i<word.length();i++){
			if(wordOfChar[i]<='9' && wordOfChar[i]>='0')
				NumOfNum++;
			else
				NumOfalphabat++;
		}
	}
	
	public void JudgeException() throws InvalidPasswordException{ // exception
		String Exception1 = "The password "+yourpass+" has no number!"; // no number
		String Exception2 = "The password "+yourpass+" has no letter!"; // no letter
		String Exception3 = "The length of the password "+yourpass+" is not enough!!"; // too short
		String Exception4 = "The length of the password "+yourpass+" is too long!!"; // too long
		if(yourpass.length()<7) // too short
			throw new InvalidPasswordException(Exception3);
		else if(NumOfNum<1) // no number
			throw new InvalidPasswordException(Exception1);
		else if(NumOfalphabat<1) // no letter
			throw new InvalidPasswordException(Exception2);
		else if(yourpass.length()>7)// too long
			throw new InvalidPasswordException(Exception4);
		else{
			
		}
	}

}
