package a7.s100502011;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener{
	private ImageIcon C22 = new ImageIcon("image/card/init_1.png"); //initial label
	private ImageIcon C23 = new ImageIcon("image/card/init_2.png"); //initial label
	private ImageIcon C24 = new ImageIcon("image/card/init_3.png"); //initial label
	private ImageIcon C25 = new ImageIcon("image/card/init_4.png"); //initial label
	private ImageIcon C26 = new ImageIcon("image/card/init_5.png"); //initial label
	private JButton random = new JButton("random"); // random button
	private JLabel[] Usercard = new JLabel[6]; // user card
	private JLabel[] Comcard = new JLabel[6]; // computer's card
	private ImageIcon[] usercard = new ImageIcon[5]; //user image
	private ImageIcon[] comcard = new ImageIcon[5]; // computer's image
	
	JPanel result = new JPanel(); // random result
	JPanel Com = new JPanel();  // computer's panel
	JPanel You = new JPanel(); // your panel
	JPanel playerAndCard = new JPanel(); // add computer and user's panel
	JLabel combine1 = new JLabel(); // computer's combination
	JLabel combine2 = new JLabel(); // your combination
	JLabel answer = new JLabel(); // win or lose
	Border line = new LineBorder(Color.BLACK,2); // set border
	Font font = new Font("Sarif",Font.BOLD,20); // set style
	
	public FrameWork(){ // constructor
		
		//initial user label
        Usercard[0] =(new JLabel (C22));
        Usercard[1] =(new JLabel (C23));
        Usercard[2] =(new JLabel (C24));
        Usercard[3] =(new JLabel (C25));
        Usercard[4] =(new JLabel (C26));
        Usercard[5] =(new JLabel());
        
        //initial computer's label
        Comcard[0] =(new JLabel (C22));
        Comcard[1] =(new JLabel (C23));
        Comcard[2] =(new JLabel (C24));
        Comcard[3] =(new JLabel (C25));
        Comcard[4] =(new JLabel (C26));
        Comcard[5] =(new JLabel());
        
        // player and set their style
		JLabel use1 = new JLabel("Com");
		JLabel use2 = new JLabel("You");
		use1.setFont(font);
		use2.setFont(font);
		
		//add label to panel
		Com.setLayout(new GridLayout(1,6));
		Com.add(use1);
		for(int i=0;i<5;i++){
			Com.add(Comcard[i]);
		}
		
        //add label to panel
		You.setLayout(new GridLayout(1,6));
		You.add(use2);
		for(int i=0;i<5;i++){
			You.add(Usercard[i]);
		}
		
		//player and card panel
		playerAndCard.setLayout(new GridLayout(3,1));
		playerAndCard.add(Com);
		playerAndCard.add(You);
		
		//random button and result panel
		result.setLayout(new GridLayout(3,1));
		random.setFont(font);
		playerAndCard.add(random);
		result.add(combine1);
		result.add(combine2);
		result.add(answer);
		random.addActionListener(this);
			
		//add panel to frame
		add(playerAndCard,BorderLayout.CENTER);
		add(result,BorderLayout.EAST);
	}
	
	public void actionPerformed(ActionEvent e){ // button's action
		if(e.getSource()==random){ // action
			User user = new User(); // object
			usercard = user.userc; // store label
			comcard = user.Comc; //store label
			
			//store image to label and set their properties
			for(int i=0;i<5;i++){
				Usercard[i].setIcon(usercard[i]);
				Comcard[i].setIcon(comcard[i]);
		        Usercard[i].setBorder(line);
		        Comcard[i].setBorder(line);
			}
			
			// add to panel
			for(int j=0;j<5;j++){
				Com.add(Comcard[j]);
			}
			
			//add to panel
			for(int i=0;i<5;i++){
				You.add(Usercard[i]);
			}
			
			//show your card's permutation and combination
			int rank1 = user.Judge(user.comCardNum);
			combine1.setText(user.level(rank1));
			combine1.setFont(font);
			result.add(combine1);
			
			//show your card's permutation and combination
			int rank2 = user.Judge(user.userCardNum);
			combine2.setText(user.level(rank2));
			combine2.setFont(font);
			result.add(combine2);
			
			// to detremine the winner
			if(rank2>rank1){ // win
				answer.setText("You Win");
				answer.setFont(font);
				result.add(answer);
			}
			else if(rank2 < rank1){ // lose
				answer.setText("You Lose");
				answer.setFont(font);
				result.add(answer);
			}
			else{ //draw
				answer.setText("Draw");
				answer.setFont(font);
				result.add(answer);
			}
		}
	}
}