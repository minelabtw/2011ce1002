package a7.s100502011;

import java.util.*;
import javax.swing.ImageIcon;

public class User {
	public ImageIcon[] userc = new ImageIcon[5]; // image array
	public ImageIcon[] Comc = new ImageIcon[5]; // image array
	public int[] userCardNum = new int[6]; // to count you have what value of card
	public int[] comCardNum = new int[6]; // to count you have what value of card
	public ImageIcon[] pic = new ImageIcon[21];
	
	public User(){ // constructor
		
		for(int k=0;k<21;k++){ // store image to array
			pic[k] = new ImageIcon("image/card/"+k+".png");
		}
		
		for(int i =0;i<5;i++){  //Store your card
			int now = getRandomNum();
			while(pic[now]==null){ // until you get a new card
				now = getRandomNum();
			}
			if(now ==0||now==1||now==2||now==3)
				userCardNum[4]++; //A
			else if(now ==4||now ==5||now==6||now==7)
				userCardNum[0]++; //2
			else if(now ==8||now ==9||now==10||now==11)
				userCardNum[1]++; //J
			else if(now ==12||now ==13||now==14||now==15)
				userCardNum[2]++; //Q
			else if(now ==16||now ==17||now==18||now==19)
				userCardNum[3]++; //K
			else
				userCardNum[5]++; //JOKER
			userc[i] = pic[now]; // store to your hand card
			pic[now] = null; // set it null
		}
		
		for(int j=0;j<5;j++){ //Store computer's card
			int now = getRandomNum(); // random number
			while(pic[now]==null){ // until you get a new card
				now = getRandomNum();
			}
			
			if(now ==0||now==1||now==2||now==3)
				comCardNum[4]++; //A
			else if(now ==4||now ==5||now==6||now==7)
				comCardNum[0]++; //2
			else if(now ==8||now ==9||now==10||now==11)
				comCardNum[1]++; //J
			else if(now ==12||now ==13||now==14||now==15)
				comCardNum[2]++; //Q
			else if(now ==16||now ==17||now==18||now==19)
				comCardNum[3]++; //K
			else
				comCardNum[5]++; //JOKER
			Comc[j] = pic[now]; // store to your hand card
			pic[now] = null; // set it null
		}
	}
	
	public 	int getRandomNum(){ // get random card
		int total = 21;
		Random RanCard = new Random();
		int RNum = RanCard.nextInt(total); //0~20
		return RNum;
	}
	
	public int Judge(int[] array){ // judge what combination of hand card
		if(array[5]==1) // Joker
			return 6;
		
		 //Four of a kind
		else if(array[0]==4 ||array[1]==4||array[2]==4||array[3]==4||array[4]==4)
			return 5;

		// Full house
		else if((array[0]==3&&array[1]==2)||(array[0]==3&&array[2]==2)||(array[0]==3&&array[3]==2)||(array[0]==3&&array[4]==2)
				||(array[1]==3&&array[0]==2)||(array[1]==3&&array[2]==2)||(array[1]==3&&array[3]==2)||(array[1]==3&&array[4]==2)
				||(array[2]==3&&array[0]==2)||(array[2]==3&&array[1]==2)||(array[2]==3&&array[3]==2)||(array[2]==3&&array[4]==2)
				||(array[3]==3&&array[0]==2)||(array[3]==3&&array[1]==2)||(array[3]==3&&array[2]==2)||(array[3]==3&&array[4]==2)
				||(array[4]==3&&array[0]==2)||(array[4]==3&&array[1]==2)||(array[4]==3&&array[2]==2)||(array[4]==3&&array[3]==2))
			return 4;
		//three of kind
		else if(array[0]==3 || array[1]==3 || array[2]==3 || array[3]==3 || array[4]==3)
			return 3;
		
		//two pairs
		else if((array[0]==2&&array[1]==2)||(array[0]==2&&array[2]==2)||(array[0]==2&&array[3]==2)||(array[0]==2&&array[4]==2)
				||(array[1]==2&&array[0]==2)||(array[1]==2&&array[2]==2)||(array[1]==2&&array[3]==2)||(array[1]==2&&array[4]==2)
				||(array[2]==2&&array[0]==2)||(array[2]==2&&array[1]==2)||(array[2]==2&&array[3]==2)||(array[2]==2&&array[4]==2)
				||(array[3]==2&&array[0]==2)||(array[3]==2&&array[1]==2)||(array[3]==2&&array[2]==2)||(array[3]==2&&array[4]==2)
				||(array[4]==2&&array[0]==2)||(array[4]==2&&array[1]==2)||(array[4]==2&&array[2]==2)||(array[4]==2&&array[3]==2))
			return 2;
		
		//nothing
		else 
			return 1;
	}
	
	public String level(int rank){ // change combination to rank to judge big or samll
		if(rank==1)
			return "<Nothing>";
		else if(rank ==2)
			return "<Two pairs>";
		else if(rank ==3)
			return "<Three of kind>";
		else if(rank ==4)
			return "<Full house>";
		else if(rank ==5)
			return "<Four of a kind>";
		else
			return "<Joker>";
	}
}
