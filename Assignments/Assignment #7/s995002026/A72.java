package a7.s995002026;

import java.util.Scanner;

public class A72 {
	public static void main(String[] argv){
		
		Scanner input=new Scanner(System.in);
		
		while(true){
		
			System.out.println("Please input a password to varify: ");
		
			Password password=new Password(input.next());
		
			try{
				password.decide();					//如果有錯則丟出exception
			}
		
			catch(InvalidPasswordException ipe){
				System.out.println(ipe);			//印出哪裡錯誤
			}
		
			System.out.println(password.TorF());	//印出有效或無效
			
			if(password.getTF()==0)					//結束
				break;
		}
	}
}
