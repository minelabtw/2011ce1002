package a7.s995002026;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.*;

import javax.swing.*;

public class FrameWork  extends JFrame implements ActionListener{
	private ImageIcon[] icon=new ImageIcon[21];		//21張牌
	
	private int[] cards=new int[10];				//得到牌的值
	
	private JButton button=new JButton("Random");	//隨機按鈕
	
	private JPanel p1=new JPanel();
	
	private JPanel p2=new JPanel();
	
	FrameWork(){
		String png;			//紀錄要取得哪一張牌
		
		button.addActionListener(this); //active button event here
		
		for(int i=0;i<=20;i++){						//儲存牌
			png=Integer.toString(i)+".png";
			icon[i]=new ImageIcon("image/card/"+png);
		}
	}
	
	public void actionPerformed(ActionEvent e){
		
		if(e.getSource() == button){
			//what do you want when you push "random" button? Obviously, random card!
			
			p1.setLayout(new GridLayout(2,6));
			
			p2.setLayout(new GridLayout(4,1));
			
			User user=new User();
			
			cards=user.getCards();
			
			for(int i=1;i<=11;i++){
				if(i==1)
					p1.add(new JLabel("HE"));
				
				if(i==7)
					p1.add(new JLabel("you"));
				
				p1.add(new JButton(icon[cards[i-1]]));
				
			}
			
			
		}
	}
	
	public void window(){
	
		
		
		add(p1,BorderLayout.CENTER);
		
		add(p2,BorderLayout.EAST);
		
		add(button,BorderLayout.SOUTH);
	}

		
	
}
