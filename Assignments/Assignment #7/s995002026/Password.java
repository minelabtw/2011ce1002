package a7.s995002026;

public class Password{
	
	private String password;
	
	private int eng=0;			//判斷是否有英文
	
	private int num=0;			//判斷是否有數字
	
	private int tf=0;           //判斷是否password成立
	
	Password(String input){
		password = input;
	}
	
	public int getTF(){
		return tf;
	}
	
	public String TorF(){
		if(tf==0)																//成立
			return "The password "+password+" is valid!!";
		else
			return "The password "+password+" is not correct!!";				//失敗
	}
	
	
	public String Engfail(){								//不含英文字母
		tf=1;
		
		return "The password "+password+" has no letter!!";
	}
	
	public String Numfail(){								//不含數字
		tf=1;
		
		return "The password "+password+" has no number!!";
	}
	
	
	
	public String length(){									//長度不足
		tf=1;
		
		return "The length of the password "+password+" is not enough!!";
	}
	
	public void decide() throws InvalidPasswordException{
				
		for(int i=0;i<password.length();i++){							//先判斷是否存在數字跟英文字母
			if(password.charAt(i)>=48 && password.charAt(i)<=57 )
				num=1;
			
			else if( ( password.charAt(i)>=65 && password.charAt(i) >=90 ) || ( password.charAt(i)>=97 && password.charAt(i)<=122))
				eng=1;
		}
		
		if(password.length()<7)								//長度不夠
			throw new InvalidPasswordException(length());
		
		else if( eng==0 )									//沒有英文字母
			throw new InvalidPasswordException(Engfail());
		
		else if( num==0 )									//沒有數字
			throw new InvalidPasswordException(Numfail());
		
		
	}
}
