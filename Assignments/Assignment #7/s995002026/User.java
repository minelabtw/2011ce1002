package a7.s995002026;

import java.util.Random;

import javax.swing.ImageIcon;

public class User {
	private int[] cards=new int[10];
	
	private String type;
	
	private int[] repeat=new int[21];			//紀錄有沒有重複出現過
	
	User(){
		
		for(int i=0;i<21;i++){					//初始化
			repeat[i]=0;
		}
			
		for(int i=0;i<10;i++){
			cards[i]=random();
		}
	}
	
	public int[] getCards(){
		return cards;
	}
	
	public int random(){
		Random r=new Random();
		
		int a;								//記錄亂數
		
		while(true){
			a=r.nextInt(21);
			if(repeat[a]==0){
				repeat[a]=1;
				break;
			}
		}
		
		return a;
	}
	
	/*public String Type(){
		
	}*/
}
