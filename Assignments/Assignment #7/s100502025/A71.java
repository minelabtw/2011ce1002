package a7.s100502025;

import javax.swing.JFrame;

public class A71 {
	public static void main(String [] args) {
		FrameWork f = new FrameWork();  //顯示一個視窗	
		f.setTitle("Asignment #7");  //視窗名稱
		f.setSize(1500, 550);  //視窗大小
		f.setLocationRelativeTo(null);  //讓視窗位置在螢幕中間	
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  //可以正常關閉	
		f.setVisible(true);  //可看見
	}
}
