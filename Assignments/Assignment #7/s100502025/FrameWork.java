package a7.s100502025;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.*;

public class FrameWork extends JFrame implements ActionListener {
	
	private ImageIcon[] pic = new ImageIcon[21];  //宣告陣列來儲存紙牌圖片
	private ImageIcon[] picinit = new ImageIcon[5];  //宣告陣列儲存開始的圖片
	private int[] cards = new int[10];  //這個陣列來儲存排組
	private JLabel[] jlbcard = new JLabel[10];  //這個陣列儲存十個位置裡面的內容
	private JLabel[] jlbType = new JLabel[3];  //這個陣列是畫面最右邊個三個結果
	private int rank1 , rank2 ;  //排組的大小，rank1為上面的排組，rank2為下面的排組
	private User user = new User();
	
	JButton button = new JButton("random"); //create button 
	
	public ImageIcon SetPicture(boolean TrueOrFalse , int pictures) {  //設置每個Label中的東西，TrueOrFalse是代表著有沒有按下random，pictures則代表第幾張牌，總共有十張，前五張牌是第一副牌(上面的)，後五張是第二副牌(下面的)
		if(TrueOrFalse == false) {  //當還沒按下random
			if(pictures <= 4) {  //第一副牌
				return picinit[ pictures ];  //回傳牌組			
			}
			else {  //第二副牌
				return picinit[ pictures - 5 ];  //回傳牌組
			}
		}
		else {  //當按下random
			return pic[ cards[ pictures ]];  //回傳兩組牌組				
		}				
	}

	public FrameWork ( ) {  //畫面顯示
		
		for(int i = 0 ; i < 21 ; i++) {  //先將牌的圖片存到陣列當中
			pic[i] = new ImageIcon("image/card/" + i + ".png");
		}
		for(int i = 0 ; i <= 4 ; i++) {  //先將開始的圖片存到陣列當中
			picinit[i] = new ImageIcon("image/card/init_" + (i + 1) + ".png");
		}
				
		Border lineBorder = new LineBorder(Color.BLACK , 2);  //設置邊線，黑色，粗為2
		Font font1 = new Font("Serif",Font.PLAIN,40);  //設置第一種字型
		Font font2 = new Font("Serif",Font.BOLD,55);  //設置第二種字型
		JPanel p1 = new JPanel(new GridLayout(1,6));  //先宣告一個panel名稱為p1，存放名稱還有存放第一副牌
		JLabel jlb1 = new JLabel("      He");  //名稱
		jlb1.setFont(font1);  //名稱用第一種字型
		p1.add(jlb1);  //再加入p1的panel上
		for(int i = 0 ; i <= 4 ; i++){
			jlbcard[i] = new JLabel(SetPicture(false , i));  //分別將五個位置放上圖片
			jlbcard[i].setBorder(lineBorder);  //將五個放圖片的位置加上邊線
			p1.add(jlbcard[i]);  //再加到p1的panel上
		}	
		JPanel p2 = new JPanel(new GridLayout(1,6));  //先宣告一個panel名稱為p2，存放名稱還有存放第二副牌  
		JLabel jlb2 = new JLabel("      You");  //名稱
		jlb2.setFont(font1);  //名稱用第一種字型
		p2.add(jlb2);  //再加入p2的panel上
		for(int i = 5 ; i <= 9 ; i++){
			jlbcard[ i ] = new JLabel(SetPicture(false , i));  //分別將五個位置放上圖片
			jlbcard[ i ].setBorder(lineBorder);  //將五個放圖片的位置加上邊線
			p2.add(jlbcard[ i ]);  //再加到p2的panel上
		}	
		JPanel p3 = new JPanel(new GridLayout(3,1));  //先宣告一個panel名稱為p3，存放第一副牌、第二副牌還有存放random這個按鈕  
		p3.add(p1);  //將第一副牌加到p3的panel上
		p3.add(p2);  //將第二副牌加到p3的panel上
		p3.add(button);  //將random按鈕加到p3的panel上
		JPanel p4 = new JPanel(new GridLayout(3,1));  //這是右邊顯示結果的panel，名稱宣告為p4
		jlbType[0] = new JLabel(user.Type(user.Typee(cards , 1)));  //這是第一副牌的結果
		jlbType[0].setFont(font1);  //將第一副牌的結果字型設為第一種字型
		p4.add(jlbType[0]);  //把第一副牌的結果加到p4的panel上
		jlbType[1] = new JLabel(user.Type(user.Typee(cards , 2)));  //這是第二副牌的結果
		jlbType[1].setFont(font1);  //將第二副牌的結果字型設為第一種字型
		p4.add(jlbType[1]);  //把第二副牌的結果加到p4的panel上
		jlbType[2] = new JLabel("Start");  //最後的結果，決定輸或贏，一開始還沒有結果，所以先顯示為start
		jlbType[2].setFont(font2);  //將最後的結果字型設為第二種字型
		p4.add(jlbType[2]);  //將最後的結果加到p4的panel上
		
		setLayout(new BorderLayout());  //宣告成BorderLayout的排列方式
		add(p3 , BorderLayout.CENTER);  //將p3加到視窗的中間位置
		add(p4 , BorderLayout.EAST);  //將p4加到視窗的東邊位置
		
		button.addActionListener(this); //active button event here	

	}	


	public void actionPerformed(ActionEvent e){		
		if(e.getSource() == button){			//what do you want when you push "random" button? Obviously, random card!		
			cards = user.getRandom();			
			for(int i = 0 ; i < 10 ; i++ ){
				jlbcard[i].setIcon(SetPicture(true , i));  //每按一次random按鈕，就將兩副牌的十張圖片改變
			}		
			jlbType[0].setText(user.Type(user.Typee(cards , 1 )));  //隨著十個圖片改變，第一個結果也改變
			rank1 = user.Typee(cards , 1);  //第一個牌的大小
			jlbType[1].setText(user.Type(user.Typee(cards , 2 )));  //隨著十個圖片改變，第二個結果也改變
			rank2 = user.Typee(cards , 2);  //第二個牌的大小
			jlbType[2].setText(user.WinOrNot(rank1 , rank2 ));  //用兩副牌的大小比較輸贏
		}	
	}
}
