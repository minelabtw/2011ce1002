package a7.s100502025;

import java.util.Random;

import javax.swing.JLabel;

public class User {
	
	private int[] cards = new int[10];  //宣告一個陣列，可以儲存10個不一樣的亂數
			
	public User(){
		
	}
	
	public int randomnumber() {
		Random random = new Random();  //new出亂數的object
		return random.nextInt(21);  //亂數會介於0~21之間
	}
	
	public int[] getRandom() {  //得到十個亂數
		for(int i = 0 ; i < 10 ; i++ ) {  
			if(i == 0) {
				cards[i] = this.randomnumber();  //先找到第一個亂數
			}
			else {
				cards[i] = this.randomnumber();  //找第(i+1)個亂數
				boolean keep = true;
				int j = 0 ;
				while(keep == true) {  //判斷之前的亂數有沒有和第(i+1)個亂數重覆到
					if(j == i) {  //判斷到自己的話，跳出while
						keep = false;
					}	
					else if(cards[i] == cards[j]) {  //若有和之前的重複
						cards[i] = this.randomnumber();  //重新再產生一個亂數
						j = 0;  //從頭再判斷一次
					}
					else {
						j++;  //若這個沒有重複的話，再檢查下一個
					}
				}
			}
		}
		return cards;  //回傳10個亂數
	}

	public int Typee(int[] number , int num) {  //判斷是哪組牌組，number[]是牌，num是代表著第num副牌
		if(num == 1) {  //第一副牌
			int[] type1 = new int[6];  //宣告一個陣列，type[0]代表著A有幾張，type[1]代表著2有幾張，以此類推，type[5]代表著joker有幾張
			for(int i = 0 ; i < 6 ; i++ ) {  //先將六個牌的張數都設為0
				type1[i] = 0;
			}
			for(int i = 0 ; i < 5 ; i++ ) {  //一張一張檢查
				if(number[i] >=0 && number[i] <= 3) {  //這張是A的話就type[0]加1
					type1[0]++;
				}
				else if(number[i] >=4 && number[i] <= 7) {  //這張是2的話就type[1]加1
					type1[1]++;
				}
				else if(number[i] >=8 && number[i] <=11) {  //這張是J的話就type[2]加1
					type1[2]++;
				}
				else if(number[i] >=12 && number[i] <= 15) {  //這張是Q的話就type[3]加1
					type1[3]++;
				}
				else if(number[i] >=16 && number[i] <= 19) {  //這張是K的話就type[4]加1
					type1[4]++;
				}
				else if(number[i] == 20 ) {  //這張是Joker的話就type[5]加1
					type1[5]++;
				}		
			}
			if(type1[5] == 1) {  //先判斷是否有鬼牌，有鬼牌的話回傳1
				return 1;
			}
			else {  //在判斷其他牌型
				if(type1[0] == 4 || type1[1] == 4 || type1[2] == 4 || type1[3] == 4 || type1[4] == 4 ) {  //再先判斷有四張一樣的話，回傳2
					return 2;
				}
				else {  //再判斷其他牌型
					if(type1[0] == 3 || type1[1] == 3 || type1[2] == 3 || type1[3] == 3 || type1[4] == 3 ) {  //再先判斷有三張一樣的話  
						if(type1[0] == 2 || type1[1] == 2 || type1[2] == 2 || type1[3] == 2 || type1[4] == 2 ) {  //若又有兩張一樣，回傳3
							return 3;
						}
						return 4;  //只有三個一樣，剩下兩個不一樣，則回傳4
					}
					else {  //再判斷其他牌型
						if(type1[0] == 2 || type1[1] == 2 || type1[2] == 2 || type1[3] == 2 || type1[4] == 2 ) {  //只有兩張一樣的，回傳5
							return 5;
						}
						else {  //沒有任何牌型，回傳6
							return 6;
						}
					}
				}
			}
		}
		else {  //第二副牌
			int[] type2 = new int[6];  //宣告一個陣列，type[0]代表著A有幾張，type[1]代表著2有幾張，以此類推，type[5]代表著joker有幾張
			for(int i = 0 ; i < 6 ; i++ ) {  //先將六個牌的張數都設為0
				type2[i] = 0;
			}
			for(int i = 5 ; i <= 9 ; i++ ) {  //一張一張檢查
				if(number[i] >=0 && number[i] <= 3) {  //這張是A的話就type[0]加1
					type2[0]++;
				}
				else if(number[i] >= 4 && number[i] <= 7) {  //這張是2的話就type[1]加1
					type2[1]++;
				}
				else if(number[i] >=8 && number[i] <=11) {  //這張是J的話就type[2]加1
					type2[2]++;
				}
				else if(number[i] >=12 && number[i] <= 15) {  //這張是Q的話就type[3]加1
					type2[3]++;
				}
				else if(number[i] >=16 && number[i] <= 19) {  //這張是K的話就type[4]加1
					type2[4]++;
				}
				else if(number[i] == 20 ) {  //這張是Joker的話就type[5]加1
					type2[5]++;
				}		
			}
			if(type2[5] == 1) {  //先判斷是否有鬼牌，有鬼牌的話回傳1
				return 1;
			}
			else {  //在判斷其他牌型
				if(type2[0] == 4 || type2[1] == 4 || type2[2] == 4 || type2[3] == 4 || type2[4] == 4 ) {  //再先判斷有四張一樣的話，回傳2
					return 2;
				}
				else {  //在判斷其他牌型
					if(type2[0] == 3 || type2[1] == 3 || type2[2] == 3 || type2[3] == 3 || type2[4] == 3 ) {  //再先判斷有三張一樣的話  
						if(type2[0] == 2 || type2[1] == 2 || type2[2] == 2 || type2[3] == 2 || type2[4] == 2 ) {  //若又有兩張一樣，回傳3
							return 3;
						}
						return 4;  //只有三個一樣，剩下兩個不一樣，則回傳4
					}
					else {  //在判斷其他牌型
						if(type2[0] == 2 || type2[1] == 2 || type2[2] == 2 || type2[3] == 2 || type2[4] == 2 ) {  //只有兩張一樣的，回傳5
							return 5;
						}
						else {  //沒有任何牌型，回傳6
							return 6;
						}
					}
				}
			}
		}
	}
	
	public String Type(int num) {  //這副牌的大小是第幾
		if(num == 1) {  //若大小為1
			return "Joker";  //這副牌的牌型為Joker
		}
		else if (num == 2) {  //若大小為2
			return "Four of kind";  //這副牌的牌型為Four of kind
		}
		else if (num == 3) {  //若大小為3
			return "Full House";  //這副牌的牌型為Full House
		}
		else if (num == 4) {  //若大小為4
			return "Three of kind";  //這副牌的牌型為Three of kind
		}
		else if (num == 5) {  //若大小為5
			return "Two pairs";  //這副牌的牌型為Two pairs
		}
		else {  //若大小為6
			return "Nothing";  //這副牌的牌型為Nothing
		}
	}
	
	public String WinOrNot(int number1 , int number2 ) {  //判斷哪一個贏
		if(number1 > number2) {  
			return "You Win";
		}
		else if(number1 == number2 ) {
			return "Draw";
		}
		else {
			return "You Lose";
		}
	}
}
