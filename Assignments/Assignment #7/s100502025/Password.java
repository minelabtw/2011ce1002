package a7.s100502025;

public class Password {
	
	String password;
	
	public Password(String pass) throws InvalidPasswordException {
		try {
			if(pass.length() > 6) {  //若長度有超過7個，則繼續檢查其他條件
				boolean keep1 = false;  //這是判斷是否有數字，有的話keep1為true
				for(int i = 0 ; i < pass.length() ; i++) {  //一個字一個字判斷
					if(pass.charAt(i) >= 48 && pass.charAt(i) <= 57) {  //用ASCII來判斷，48~57為數字0~9
						keep1 = true;  //若有數字，keep1為true
						break;  //然後跳出迴圈
					}
				}
				try {
					if(keep1 == false) {  //若沒有數字
						throw new InvalidPasswordException(pass);  //回傳錯誤的訊息
					}
					else {
						boolean keep2 = false;  //這是判斷是否有字母，有的話keep2為true
						for(int i = 0 ; i < pass.length() ; i++) {  //一個字一個字判斷
							if(pass.charAt(i) >= 97 && pass.charAt(i) <= 122) {  //用ASCII來判斷，97~122為字母小寫a~z
								keep2 = true;  //若有字母，keep1為true
								break;  //然後跳出迴圈
							}
						}
						try {
							if(keep2 == false) {  //若沒有字母
								throw new InvalidPasswordException(pass);  //回傳錯誤的訊息
							}
							else {  //當所有條件都符合
								password = pass;  //將pass存到這class的private中
								System.out.println("The password " + password + " is valid!!");
							}
						}
						catch(InvalidPasswordException ex) {  //接收到有發生錯誤的訊息
							System.out.println("The password " + ex.getPassword() + " has no letter!!\n" + ex.getMessage());
						}
					}
				}
				catch(InvalidPasswordException ex) {  //接收到有發生錯誤的訊息
					System.out.println("The password " + ex.getPassword() + " has no number!!\n" + ex.getMessage());
				}
			}	
			else {  //若長度不到7
				throw new InvalidPasswordException(pass);  //回傳錯誤的訊息
			}
		}
		catch (InvalidPasswordException ex) {  //接收到有發生錯誤的訊息
			System.out.println("The length of the password " + ex.getPassword() + " is not enough!!\n" + ex.getMessage());
		}
	}
}
