package a7.s100502511;

import java.util.Scanner;

public class Password {
	public static String yourinput;

	public Password() throws InvalidPasswordException {
		Scanner input = new Scanner(System.in);
		for (;;) {
			for (;;) {
				System.out.println("Please input a password to varify: ");
				yourinput = input.next();

				try {
					InvalidPasswordException.exception1(); // 檢查字元長度夠不夠
				} catch (InvalidPasswordException ex) {
					InvalidPasswordException.False1();
					break;
				}

				try {
					InvalidPasswordException.exception2(); // 檢查有沒有數字(字元長度夠的情況下)
				} catch (InvalidPasswordException ex) {
					InvalidPasswordException.False2();
					break;
				}
				
				try {
					InvalidPasswordException.exception3(); // 檢查有沒有字母(字元長度夠的情況下)
				} catch (InvalidPasswordException ex) {
					InvalidPasswordException.False3();
					break;
				}
				System.out.println("The password " + yourinput + " is valid!!"); // 當沒有錯誤，則輸出此訊息
				System.out.println("");
				break;
			}
		}
	}
}
