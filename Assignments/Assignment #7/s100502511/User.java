package a7.s100502511;

import java.util.Random;

public class User {

	static int card[] = new int[5];
	static int count = 0;
	static int judge = 0;
	static Random Randcard = new Random(); // 亂數功能

	public static int random() {
		int randcard = Randcard.nextInt(21);
		return randcard;
	}

	public static int judgetypes(int c1, int c2, int c3, int c4, int c5) {
		card[0] = c1;
		card[1] = c2;
		card[2] = c3;
		card[3] = c4;
		card[4] = c5;
		for (int a = 0; a < 5; a++) {
			if (card[a] == 0 || card[a] == 1 || card[a] == 2 || card[a] == 3) {
				card[a] = 1;
			} else if (card[a] == 4 || card[a] == 5 || card[a] == 6
					|| card[a] == 7) {
				card[a] = 2;
			} else if (card[a] == 8 || card[a] == 9 || card[a] == 10
					|| card[a] == 11) {
				card[a] = 11;
			} else if (card[a] == 12 || card[a] == 13 || card[a] == 14
					|| card[a] == 15) {
				card[a] = 12;
			} else if (card[a] == 16 || card[a] == 17 || card[a] == 18
					|| card[a] == 19) {
				card[a] = 13;
			} else if (card[a] == 20)
				card[a] = 999;
		}

		count = 0; // count是拿來判斷哪種牌型

		for (int i = 0; i < 5; i++) {
			if (card[i] == 999) {
				count = 999;
				return count;
			} else {
				for (int j = 0; j < 5; j++) { // 方法為不算自己那張牌，在比對其他的牌，一樣的話count+1
					if (i == j)
						continue;
					if (card[i] == card[j]) {
						count++;
					}
				}
			}
		}
		return count;
	}

	public static String returntypes(int judgetypes) {

		judge = judgetypes;

		if (judge == 999) { // judge為999時:Joker
			return "<Joker>";
		} else if (judge == 12) { // judge為12時:Four of a kind
			return "<Four of a kind>";
		} else if (judge == 8) { // judge為8時:Full House
			return "<Full House>";
		} else if (judge == 6) { // judge為6時:Three of a kind
			return "<Three of a kind>";
		} else if (judge == 4) { // judge為4時:Two pairs
			return "<Two pairs>";
		} else {
			return "<Nothing>";
		}
	}

}
