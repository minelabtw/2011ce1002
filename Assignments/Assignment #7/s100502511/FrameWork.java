package a7.s100502511;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.*;

public class FrameWork extends JFrame implements ActionListener {
	ImageIcon image1 = new ImageIcon("image/card/init_1.png"); // 初始圖案1
	ImageIcon image2 = new ImageIcon("image/card/init_2.png");// 初始圖案2
	ImageIcon image3 = new ImageIcon("image/card/init_3.png");// 初始圖案3
	ImageIcon image4 = new ImageIcon("image/card/init_4.png");// 初始圖案4
	ImageIcon image5 = new ImageIcon("image/card/init_5.png");// 初始圖案5
	ImageIcon image6 = new ImageIcon();
	ImageIcon image7 = new ImageIcon();
	ImageIcon image8 = new ImageIcon();
	ImageIcon image9 = new ImageIcon();
	ImageIcon image10 = new ImageIcon();
	JLabel card1 = new JLabel(image1);// You手牌1
	JLabel card2 = new JLabel(image2);// You手牌2
	JLabel card3 = new JLabel(image3);// You手牌3
	JLabel card4 = new JLabel(image4);// You手牌4
	JLabel card5 = new JLabel(image5);// You手牌5
	JLabel card6 = new JLabel(image1);// Com手牌1
	JLabel card7 = new JLabel(image2);// Com手牌2
	JLabel card8 = new JLabel(image3);// Com手牌3
	JLabel card9 = new JLabel(image4);// Com手牌4
	JLabel card10 = new JLabel(image5);// Com手牌5

	String message1 = "Your type of the cards";
	String message2 = "Computer's type of the cards";
	String message3 = "Result";

	JLabel Message1 = new JLabel(message1); // 右方訊息1
	JLabel Message2 = new JLabel(message2);// 右方訊息2
	JLabel Message3 = new JLabel(message3);// 右方訊息3

	JButton button = new JButton("Random"); // create button

	public FrameWork() {
		button.addActionListener(this); // active button event here

		Font setfont = new Font("TimesRoman", Font.BOLD, 25); // 設定字型
		Font setfont1 = new Font("TimesRoman", Font.BOLD, 40);

		JPanel p1 = new JPanel(new GridLayout(1, 6, 10, 10)); // 建立JPanel p1
		JLabel You = new JLabel("You");

		You.setFont(setfont);
		p1.add(You);

		card1.setBorder(new LineBorder(Color.BLACK, 2));
		p1.add(card1);

		card2.setBorder(new LineBorder(Color.BLACK, 2));
		p1.add(card2);

		card3.setBorder(new LineBorder(Color.BLACK, 2));
		p1.add(card3);

		card4.setBorder(new LineBorder(Color.BLACK, 2));
		p1.add(card4);

		card5.setBorder(new LineBorder(Color.BLACK, 2));
		p1.add(card5);

		JPanel p2 = new JPanel(new GridLayout(1, 6, 10, 10));// 建立JPanel p2
		JLabel Com = new JLabel("Com");
		Com.setFont(setfont);
		p2.add(Com);

		card6.setBorder(new LineBorder(Color.BLACK, 2));
		p2.add(card6);

		card7.setBorder(new LineBorder(Color.BLACK, 2));
		p2.add(card7);

		card8.setBorder(new LineBorder(Color.BLACK, 2));
		p2.add(card8);

		card9.setBorder(new LineBorder(Color.BLACK, 2));
		p2.add(card9);

		card10.setBorder(new LineBorder(Color.BLACK, 2));
		p2.add(card10);

		JPanel p3 = new JPanel(new GridLayout(1, 6, 10, 10));// 建立JPanel p3
		p3.add(button);
		button.setFont(setfont1);

		JPanel p4 = new JPanel(new BorderLayout(1, 7));// 建立JPanel p4
		p4.add(p1, BorderLayout.CENTER);// 把p1加到p4
		p4.add(Message1, BorderLayout.EAST);
		JPanel p5 = new JPanel(new BorderLayout(1, 7));// 建立JPanel p5
		p5.add(p2, BorderLayout.CENTER);// 把p2加到p5
		p5.add(Message2, BorderLayout.EAST);
		JPanel p6 = new JPanel(new BorderLayout(1, 7));// 建立JPanel p6
		p6.add(p3, BorderLayout.CENTER);// 把p3加到p6
		p6.add(Message3, BorderLayout.EAST);
		Message3.setFont(setfont1);
		setLayout(new GridLayout(3, 7, 10, 10)); // 把p4 p5 p6合起來
		add(p4);
		add(p5);
		add(p6);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == button) { // 當按下Random時，執行下列功能

			int randcard[] = new int[10];

			for (int i = 0; i < 10; i++) { // 判斷重覆
				randcard[i] = User.random();
				if (i >= 1) {
					for (int j = 0; j < i; j++) {
						if (randcard[i] == randcard[j]) { // 如果重覆，就把i-1，在重新Random
							i--;
							continue;
						}
					}
				}
			}

			int count1 = User.judgetypes(randcard[0], randcard[1], randcard[2], // count1和count2為判斷哪種牌型
					randcard[3], randcard[4]);
			int count2 = User.judgetypes(randcard[5], randcard[6], randcard[7],
					randcard[8], randcard[9]);

			String youcard = User.returntypes(count1); // youcard為哪種牌型
			String computercard = User.returntypes(count2); // computercard為哪種牌型
			message1 = youcard;
			message2 = computercard;
			String message31 = "You Win";
			String message32 = "You Lose";
			String message33 = "Draw";
			Message1.setText(message1);
			Message2.setText(message2);
			if (count1 > count2) {
				Message3.setText(message31);
			} else if (count1 < count2) {
				Message3.setText(message32);
			} else if (count1 == count2)
				Message3.setText(message33);

			image1 = new ImageIcon("image/card/" + randcard[0] + ".png"); // image1~image10為放入亂數牌
			image2 = new ImageIcon("image/card/" + randcard[1] + ".png");
			image3 = new ImageIcon("image/card/" + randcard[2] + ".png");
			image4 = new ImageIcon("image/card/" + randcard[3] + ".png");
			image5 = new ImageIcon("image/card/" + randcard[4] + ".png");
			image6 = new ImageIcon("image/card/" + randcard[5] + ".png");
			image7 = new ImageIcon("image/card/" + randcard[6] + ".png");
			image8 = new ImageIcon("image/card/" + randcard[7] + ".png");
			image9 = new ImageIcon("image/card/" + randcard[8] + ".png");
			image10 = new ImageIcon("image/card/" + randcard[9] + ".png");

			card1.setIcon(image1); // card1~card10為設定圖案
			card2.setIcon(image2);
			card3.setIcon(image3);
			card4.setIcon(image4);
			card5.setIcon(image5);
			card6.setIcon(image6);
			card7.setIcon(image7);
			card8.setIcon(image8);
			card9.setIcon(image9);
			card10.setIcon(image10);
		}
	}
}
