package a7.s100502511;

public class InvalidPasswordException extends Exception {
	static InvalidPasswordException ex = new InvalidPasswordException(); // 例外

	public static void False1() { // 字元長度不夠時，輸出此訊息
		System.out.println("The length of the password " + Password.yourinput
				+ " is not enough!!");
		System.out.println("The password " + Password.yourinput
				+ " is not correct!!");
		System.out.println("");
	}

	public static void False2() { // 密碼沒有數字時，輸出此訊息
		System.out.println("The password " + Password.yourinput
				+ " has no number!!");
		System.out.println("The password " + Password.yourinput
				+ " is not correct!!");
		System.out.println("");
	}

	public static void False3() { // 密碼沒有字母時，輸出此訊息
		System.out.println("The password " + Password.yourinput
				+ " has no letter!!");
		System.out.println("The password " + Password.yourinput
				+ " is not correct!!");
		System.out.println("");
	}

	public static void exception1() throws InvalidPasswordException {
		if (Password.yourinput.length() < 7) { // 字元長度不夠
			throw ex;
		}
	}

	public static void exception2() throws InvalidPasswordException {
		int numbercount = 0;
		int[] password = new int[Password.yourinput.length()];
		for (int i = 0; i < Password.yourinput.length(); i++) {
			password[i] = Password.yourinput.charAt(i);
			if (password[i] >= 48 && password[i] <= 57) {
				numbercount++;
			}
		}
		if (numbercount == 0) { // 沒有數字
			throw ex;
		}
	}

	public static void exception3() throws InvalidPasswordException {
		int lettercount = 0;
		int[] password = new int[Password.yourinput.length()];
		for (int i = 0; i < Password.yourinput.length(); i++) {
			password[i] = Password.yourinput.charAt(i);
			if (password[i] >= 97 && password[i] <= 122) {
				lettercount++;
			}
		}
		if (lettercount == 0) { // 沒有字母
			throw ex;
		}
	}
}
