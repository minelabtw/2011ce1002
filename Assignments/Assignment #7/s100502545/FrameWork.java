package a7.s100502545;

import java.applet.Applet;
import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.*;


public class FrameWork extends JFrame implements ActionListener
{
	
	JLabel Com = new JLabel("Computer");
	JLabel p1 = new JLabel();//宣告放圖片的Label
	JLabel p2 = new JLabel();
	JLabel p3 = new JLabel();
	JLabel p4 = new JLabel();
	JLabel p5 = new JLabel();
	JLabel result1 = new JLabel("");//show 牌型  result 的 Label
	
	JLabel User = new JLabel("User");
	JLabel p6 = new JLabel();//宣告放圖片的Label
	JLabel p7 = new JLabel();
	JLabel p8 = new JLabel();
	JLabel p9 = new JLabel();
	JLabel p10 = new JLabel();
	JLabel result2 = new JLabel("");//show 牌型  result
		
	JButton button = new JButton("random");//create button 
	JLabel result3 = new JLabel("");//放勝負 
	
	private ImageIcon init1 = new ImageIcon("image/card/init_1.png");//Panel起始圖片
	private ImageIcon init2 = new ImageIcon("image/card/init_2.png");
	private ImageIcon init3 = new ImageIcon("image/card/init_3.png");
	private ImageIcon init4 = new ImageIcon("image/card/init_4.png");
	private ImageIcon init5 = new ImageIcon("image/card/init_5.png");

	

	
	public FrameWork ()
	{
		
		JPanel Pa1 = new JPanel();  //宣告兩個Panel,並設置行列間距
		Pa1.setLayout(new GridLayout(2,7,4,4));
		
		button.addActionListener(this); //active button event here
		User.setFont(new Font("Serif", Font.BOLD, 24));
		Com.setFont(new Font("Serif", Font.BOLD, 24));
		Pa1.add(Com);//在第一個Panel,放入初始圖片,設置邊框
		p1 = new JLabel(init1);
		p1.setBorder(new LineBorder(Color.black, 3));
		Pa1.add(p1);
		p2 = new JLabel(init2);
		p2.setBorder(new LineBorder(Color.black, 3));
		Pa1.add(p2);
		p3 = new JLabel(init3);
		p3.setBorder(new LineBorder(Color.black, 3));
		Pa1.add(p3);
		p4 = new JLabel(init4);
		p4.setBorder(new LineBorder(Color.black, 3));
		Pa1.add(p4);
		p5 = new JLabel(init5);
		p5.setBorder(new LineBorder(Color.black, 3));
		Pa1.add(p5);
		Pa1.add(result1);
		
		Pa1.add(User);
		p6 = new JLabel(init1);
		p6.setBorder(new LineBorder(Color.black, 3));
		Pa1.add(p6);
		p7 = new JLabel(init2);
		p7.setBorder(new LineBorder(Color.black, 3));
		Pa1.add(p7);
		p8 = new JLabel(init3);
		p8.setBorder(new LineBorder(Color.black, 3));
		Pa1.add(p8);
		p9 = new JLabel(init4);
		p9.setBorder(new LineBorder(Color.black, 3));
		Pa1.add(p9);
		p10 = new JLabel(init5);
		p10.setBorder(new LineBorder(Color.black, 3));
		Pa1.add(p10);
		Pa1.add(result2);
		
		JPanel Pa2 = new JPanel();//在第二個Panel,放入按鈕,
		Pa2.setLayout(new BorderLayout(4,4));
		Pa2.add((button), BorderLayout.CENTER);
		Pa2.add((result3), BorderLayout.EAST);
		Pa2.add(Pa1, BorderLayout.NORTH);
		result1.setFont(new Font("Serif", Font.BOLD, 24));
		result2.setFont(new Font("Serif", Font.BOLD, 24));
		result3.setFont(new Font("Serif", Font.BOLD, 48));
		add(Pa1,BorderLayout.CENTER);
		add(Pa2,BorderLayout.SOUTH);
		
	
		
		
		
	}
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == button)
		{
			User computer = new User();//宣告user的物件
			User user = new User();
			user.setRandom();
			int r1 = user.Number1[0];//取得亂數的值
			int r2 = user.Number1[1];
			int r3 = user.Number1[2];
			int r4 = user.Number1[3];
			int r5 = user.Number1[4];
			int r6 = user.Number1[5];
			int r7 = user.Number1[6];
			int r8 = user.Number1[7];
			int r9 = user.Number1[8];
			int r10 = user.Number1[9];
			
			
			ImageIcon pic1 = new ImageIcon("image/card/" + r1 + ".png");
			ImageIcon pic2 = new ImageIcon("image/card/" + r2 + ".png");
			ImageIcon pic3 = new ImageIcon("image/card/" + r3 + ".png");
			ImageIcon pic4 = new ImageIcon("image/card/" + r4 + ".png");
			ImageIcon pic5 = new ImageIcon("image/card/" + r5 + ".png");
			ImageIcon pic6 = new ImageIcon("image/card/" + r6 + ".png");
			ImageIcon pic7 = new ImageIcon("image/card/" + r7 + ".png");
			ImageIcon pic8 = new ImageIcon("image/card/" + r8 + ".png");
			ImageIcon pic9 = new ImageIcon("image/card/" + r9 + ".png");
			ImageIcon pic10 = new ImageIcon("image/card/" + r10 + ".png");
			
			p1.setIcon(pic1);
			p2.setIcon(pic2);
			p3.setIcon(pic3);
			p4.setIcon(pic4);
			p5.setIcon(pic5);
			p6.setIcon(pic6);
			p7.setIcon(pic7);
			p8.setIcon(pic8);
			p9.setIcon(pic9);
			p10.setIcon(pic10);
			
			user.setcards(r1, r2, r3, r4 ,r5);
			computer.setcards(r6, r7, r8, r9, r10);
			
			
			//User vs Com 的結果
			if(user.gettype()> computer.gettype())
			{
				result3.setText("You Win");
			}
			else if(user.gettype()==computer.gettype())
			{
				result3.setText("Draw");
			}
			else
			{
				result3.setText("Computer Win!");
			}
			//User 的排組型態
			if(user.gettype()==1)
			{
				result1.setText("Poker");
			}
			else if (user.gettype()==2)
			{
				result1.setText("Four of a kind");
			}
			else if (user.gettype()==3)
			{
				result1.setText("Full House");
			}
			else if (user.gettype()==4)
			{
				result1.setText("Three of kind");
			}
			else if (user.gettype()==5)
			{
				result1.setText("Two pairs");
			}
			else
			{
				result1.setText("Nothing");
			}
			//Computer 的排組型態
			if(computer.gettype()==1)
			{
				result2.setText("Poker");
			}
			else if (computer.gettype()==2)
			{
				result2.setText("Four of a kind");
			}
			else if (computer.gettype()==3)
			{
				result2.setText("Full House");
			}
			else if (computer.gettype()==4)
			{
				result2.setText("Three of kind");
			}
			else if (computer.gettype()==5)
			{
				result2.setText("Two pairs");
			}
			else
			{
				result2.setText("Nothing");
			}
		}
	}
			
}