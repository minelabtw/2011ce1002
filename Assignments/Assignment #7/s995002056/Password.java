package a7.s995002056;

public class Password {
	String pw;
	boolean t = false;													//判斷是否成功
	boolean en = false;													//判斷有無英文
	boolean ma = false;													//判斷有無數字
	
	public Password(String pw) {
		this.pw = pw;
	}

	public boolean gett(){
		return t;
	}
	
	public String tot(){
		if(t)
			return "The password "+ pw +" is valid!!";
		else
			return "The password abc "+ pw +" in not correct!!";
	}
	
	public void setpw () throws InvalidPasswordException{
		for ( int counter = 0; counter < pw.length(); counter++ ) {		//逐字判斷
			if(pw.charAt(counter) >= '0' && pw.charAt(counter) <= '9')
				ma = true;
				else if(pw.charAt(counter) >= 'A' && pw.charAt(counter) <= 'Z'
						||pw.charAt(counter) >= 'a' && pw.charAt(counter) <= 'z' )
					en = true;
		}
		
		if(pw.length() < 7 )											//throw
			throw new InvalidPasswordException("The length of the password "+ pw +" is not enough!!");
		else if(ma == false)
			throw new InvalidPasswordException("The password "+ pw +" has no number!!");
		else if(en == false)
			throw new InvalidPasswordException("The password "+ pw +" has no letter!!");
		else if(ma == true && en == true)								//為正確密碼
			t = true;
	}
}
