package e8.s995002056;
import java.awt.*;

import javax.swing.*;

public class E8 extends JFrame{
	public E8(){
		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(8 , 8));
		
		for(int i =0 ; i < 4 ; i++){							//輸入四次
			
//--------------------白黑白黑白黑白黑---------------------------//
			for(int j=0 ; j<4 ; j++){
				Button white = new Button();					//new一個button
				white.setBackground(Color.white);				//set color
				p1.add(white);									//將button輸入到p1
				Button black = new Button();
				black.setBackground(Color.black);
				p1.add(black);
			}
//--------------------黑白黑白黑白黑白---------------------------
			for(int j=0 ; j<4 ; j++){
				Button black = new Button();
				black.setBackground(Color.black);
				p1.add(black);
				Button white = new Button();
				white.setBackground(Color.white);
				p1.add(white);
			}
		}
		
		add(p1,BorderLayout.CENTER);							//將p1丟進panel裡
		
	}
//-------------------主程式-------------------------------------
	public static void main(String[] argv) {
		//把frame引進主程式裡
		E8 frame = new E8();
		frame.setTitle("board");
		frame.setSize(400,400);
		frame.setLocationRelativeTo(null); // Center the frame
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setVisible(true);
	}
	
}