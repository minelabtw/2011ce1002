package a7.s995002056;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener{
	JButton button = new JButton("random"); //create button 
	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();
	JPanel p3 = new JPanel();
	
	public FrameWork (){			
		button.addActionListener(this); //active button event here
		add(button,BorderLayout.SOUTH);	
		
		//設定10張牌
		p1.setLayout(new GridLayout(2 , 5));
		for(int i=0 ; i<10 ; i++){
			ImageIcon ba = new ImageIcon("image/card/init_5.png");//10張牌圖
			p1.add(new JLabel(ba));
		}
		add(p1,BorderLayout.CENTER);//設定位置
		
		//設定角色
		p2.setLayout(new GridLayout(2, 1));
		p2.add(new JLabel("computer"));
		p2.add(new JLabel("you"));
		add(p2,BorderLayout.WEST);
		
		//設定牌種及輸贏
		p3.setLayout(new GridLayout(3, 1));
		JLabel ch1 = new JLabel("<>");
		JLabel ch2 = new JLabel("<>");
		JLabel sc  = new JLabel("=");
		p3.add(ch1);
		p3.add(ch2);
		p3.add(sc);
		add(p3,BorderLayout.EAST);
		}
	
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == button){
			//what do you want when you push "random" button? Obviously, random card!
			
			//發10張牌到陣列裡面
			int [] ca = new int [10] ;
			for(int i=0 ; i<10 ; i++){
				ca[i] = (int) Math.random()*21;
			//檢查重複
				for(int j=0 ; j<i ; j++){
					if(ca[j]==ca[i])
						i--;
				}
			}
			
			//將10張牌的圖案載入
			p1.setLayout(new GridLayout(2 , 5));
			for(int i=0 ; i<10 ; i++){
				ImageIcon card = new ImageIcon("image/card/"+ca[i]+".png");
			p1.add(new JLabel(card));
			add(p1,BorderLayout.CENTER);
			}
			
/*-------------------------判斷牌形-------------------------------*/
			
			
			p2.setLayout(new GridLayout(2, 1));
			for(int i=0 ; i<5 ; i++){
				if(ca[i] == 20)//鬼牌
					p2.add(new JLabel("joker"));
				//else if()
			}
			for(int i=4 ; i<10 ; i++){
				if(ca[i] == 20)//鬼牌
					p2.add(new JLabel("joker"));
			}
		}
	}
			
}
