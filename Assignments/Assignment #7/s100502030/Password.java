package a7.s100502030;

public class Password {
	private String password;
	private String errorMessage;
	private boolean haveLetter = false;
	private boolean haveNumber = false;

	public Password(String password) {
		this.password = password;
	}

	public boolean ihLetter(String password) {
		for (int i = 0; i < password.length(); i++) {
			if (Character.isLetter(password.charAt(i)))
				haveLetter = true;
		}
		return haveLetter;
	}// �P�_�O�_���r��

	public boolean ihNumber(String password) {
		for (int i = 0; i < password.length(); i++) {
			if (Character.isDigit(password.charAt(i)))
				haveNumber = true;
		}
		return haveNumber;
	}// �P�_�O�_���Ʀr

	public String getPassword() {
		return password;
	}

	public void resultOfpassword() throws InvalidPasswordException {
		if (password.length() < 7) {
			errorMessage = "The length of the password " + password
					+ " is not enough!!";
			throw new InvalidPasswordException(password);
		} else if (!ihLetter(password)) {
			errorMessage = "The password " + password + " has no letter!!";
			throw new InvalidPasswordException(password);
		} else if (!ihNumber(password)) {
			errorMessage = "The password " + password + " has no number!!";
			throw new InvalidPasswordException(password);
		}
		// �P�_���~���p
	}

	public String getErrorMessage() {
		return errorMessage;
	}// return error message

}
