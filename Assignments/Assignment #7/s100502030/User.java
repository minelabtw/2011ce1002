package a7.s100502030;

import java.lang.Math;

public class User {

	private final int arraysize = 21;

	private String[] cards = new String[arraysize];
	private String[] results = new String[6];
	private String[] card = new String[5];

	public User() {

		for (int i = 0; i < arraysize; i++) {
			cards[i] = "image/card/" + i + ".png";//存入卡片
		}
		results[0] = "Nothing";
		results[1] = "Two pairs";
		results[2] = "Three of kind";
		results[3] = "Full House";
		results[4] = "Four of a kind";
		results[5] = "Joker";//卡片結果

		boolean refind = true;
		card = new String[5];
		for (int i = 0; i < 5; i++) {
			while (refind) {
				card[i] = cards[(int) (Math.random() * arraysize)];
				refind = false;
				for (int j = 0; j < i; j++) {
					if (card[i] == card[j]) {
						refind = true;
					}
				}
			}
			refind = true;
		}// 隨機出現卡片

	}

	public String[] getCards() {
		return card;
	}

	public int getResult() {
		int[] caseArray = new int[5];
		boolean joker = false;
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < arraysize; j++) {
				if (j < 4 && card[i] == cards[j]) {
					caseArray[0]++;
				} else if (j < 8 && card[i] == cards[j]) {
					caseArray[1]++;
				} else if (j < 12 && card[i] == cards[j]) {
					caseArray[2]++;
				} else if (j < 16 && card[i] == cards[j]) {
					caseArray[3]++;
				} else if (j < 20 && card[i] == cards[j]) {
					caseArray[4]++;
				} else if (card[i] == cards[20])
					joker = true;
			}
		}// 牌情統計
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < i; j++) {
				if (caseArray[i] > caseArray[j]) {
					int hold = caseArray[i];
					caseArray[i] = caseArray[j];
					caseArray[j] = hold;
				}
			}
		}// 排序

		if (joker)
			return 5;
		else if (caseArray[0] == 4)
			return 4;
		else if (caseArray[0] == 3) {
			if (caseArray[1] == 2)
				return 3;
			else
				return 2;
		} else if (caseArray[0] == 2 && caseArray[1] == 2)
			return 1;
		else
			return 0;
	}

	public String getResult_string() {

		return results[this.getResult()];

	}
}
