package a7.s100502030;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrameWork extends JFrame implements ActionListener {
	private String[] init_cards = new String[5];
	private ImageIcon[] initCards = new ImageIcon[5];
	private JButton button = new JButton("random"); // create button
	private Font Font1 = new Font("TimesRoman", Font.CENTER_BASELINE, 20);
	private Font Font2 = new Font("TimesRoman", Font.ITALIC, 20);
	private Border lineBorder = new LineBorder(Color.BLUE, 2);
	private JLabel cardResult1 = new JLabel("?????");
	private JLabel cardResult2 = new JLabel("?????");
	private JLabel result = new JLabel("   result   ");
	private JLabel[] he_cards = new JLabel[5];
	private JLabel[] you_cards = new JLabel[5];

	public FrameWork() {
		for (int i = 0; i < 5; i++) {
			init_cards[i] = "image/card/init_" + (i + 1) + ".png";
			initCards[i] = new ImageIcon(init_cards[i]);
		}// 存入初始卡片

		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(1, 6, 5, 5));
		JLabel he = new JLabel("He");
		he.setFont(Font1);
		p1.add(he);
		for (int i = 0; i < 5; i++) {
			he_cards[i] = new JLabel("");
			p1.add(he_cards[i]);
			he_cards[i].setIcon(initCards[i]);
			he_cards[i].setBorder(lineBorder);
		}// 製作電腦面板

		JPanel p2 = new JPanel();
		p2.setLayout(new GridLayout(1, 6, 5, 5));
		JLabel you = new JLabel("You");
		you.setFont(Font1);
		p2.add(you);
		for (int i = 0; i < 5; i++) {
			you_cards[i] = new JLabel("");
			p2.add(you_cards[i]);
			you_cards[i].setIcon(initCards[i]);
			you_cards[i].setBorder(lineBorder);
		}// 製作使用者面板

		JPanel p3 = new JPanel();
		p3.setLayout(new GridLayout(3, 1, 5, 5));

		p3.add(p1);
		p3.add(p2);
		p3.add(button);//按鈕

		JPanel p4 = new JPanel();
		p4.setLayout(new GridLayout(3, 1, 5, 5));

		result.setFont(new Font("TimesRoman", Font.BOLD, 40));
		result.setForeground(Color.RED);
		cardResult1.setFont(Font2);
		cardResult2.setFont(Font2);
		cardResult1.setForeground(Color.ORANGE);
		cardResult2.setForeground(Color.ORANGE);
		// 設置字
		
		p4.add(cardResult1);
		p4.add(cardResult2);
		p4.add(result);

		add(p3, BorderLayout.CENTER);
		add(p4, BorderLayout.EAST);
		button.addActionListener(this); // active button event here
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == button) { //假如按下按鈕
			User computer = new User();
			User user = new User();

			String[] card = computer.getCards();
			String[] card2 = user.getCards();

			for (int i = 0; i < 5; i++) {
				he_cards[i].setIcon(new ImageIcon(card[i]));
				you_cards[i].setIcon(new ImageIcon(card2[i]));
			}
			cardResult1.setText(computer.getResult_string());
			cardResult2.setText(user.getResult_string());
			if (user.getResult() > computer.getResult())
				result.setText(" You Win");
			else if (user.getResult() < computer.getResult())
				result.setText("You Lose");
			else
				result.setText("    Draw   ");
			// 顯示勝負
		}
	}

}
