package a7.s100502027;

import java.util.Scanner;

public class A72 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		while(true){  // to let user repeat enter 
			
			System.out.println("Please input a password to varify: ");
			String enterword = input.next();
			try{
				Password test = new Password(enterword);
				test.confirmPassword();   // to confirm the password  is valid or not 
				System.out.println("The password "+test.getPassword()+" is valid!!");
				System.out.println("");
			}
			catch (InvalidPasswordException ex){   // when the password is invalid ,to run this 
				System.out.println(ex.getResult());
				System.out.println("The password "+enterword+" in not correct!!");
				System.out.println("");
			}
		}
	}
}