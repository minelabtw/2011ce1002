package a7.s100502027;

public class User {
	private int[] cardsnumber = new int[5];
	private String[] cardtype = {"Joker", "Four of a kind", "Full House", "Three of kind", "Two pairs","One pair", "Nothing"};
	int cardstrong=0;
	public User(){           // when it is declared  make the random for five numbers
		for(int t=0;t<5;t++){
			cardsnumber[t] = (int) (Math.random()*21);
			for (int i=0;i<t;i++){  // to avoid the same number 
				if(cardsnumber[t]==cardsnumber[i]){
					t--;
				}
			}
		}
	}
	public int getnumber(int t){    //return the card number to frame to change the label image
		return cardsnumber[t];
	}
	public String getcardresult(){
		int[] cardkind = new int[5];
		int Joker=0,fourcard=0,threecard=0,twocard=0;
		for(int t=0;t<5;t++){               // At  1 step  ,let the number have its value to make sure the count for the same value
			if(cardsnumber[t]<4){
				cardkind[0]++;
			}
			else if(cardsnumber[t]<8){
				cardkind[1]++;
			}
			else if(cardsnumber[t]<12){
				cardkind[2]++;
			}
			else if(cardsnumber[t]<16){
				cardkind[3]++;
			}
			else if(cardsnumber[t]<20){
				cardkind[4]++;
			}
			else{
				Joker++;
			}
		}
		for(int t=0;t<5;t++){            //At 2 step , according to the count of the same value , to make sure the times of a count
			if(cardkind[t]>3){
				fourcard++;
			}
			else if(cardkind[t]>2){
				threecard++;
			}
			else if(cardkind[t]>1){
				twocard++;
			}
			else{
			}
		}
		if(Joker==1){     // At 3 step , according the times to Category the result  and set the code of result strong 
			cardstrong=0;
			return cardtype[0];
		}
		else if(fourcard>0){
			cardstrong=1;
			return cardtype[1];
		}
		else if(threecard>0){
			if(twocard>0){
				cardstrong=2;
				return cardtype[2];
			}
			else{
				cardstrong=3;
				return cardtype[3];
			}
		}
		else if(twocard>0){
			if(twocard>1){
				cardstrong=4;
				return cardtype[4];
			}
			else{
				cardstrong=5;
				return cardtype[5];
			}
		}
		else{
			cardstrong=6;
			return cardtype[6];
		}
	}
	public int getcardstrong(){   // to return the code of strong to let outside use
		return cardstrong;
	}
	public String gameresult(User a ){      // to use this.strong code  than outside.strong code  to output the result
		if(a.getcardstrong()>this.getcardstrong()){
			return "YOU WIN!!!";
		}
		else if(a.getcardstrong()==this.getcardstrong()){
			return "---DRAW---";
		}
		else{
			return "YOU LOSE";
		}
	}
}