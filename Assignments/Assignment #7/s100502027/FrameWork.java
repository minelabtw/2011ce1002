package a7.s100502027;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import java.awt.*;

public class FrameWork extends JFrame implements ActionListener{
	private ImageIcon[]  cardIcon = new ImageIcon[21];     //store the card pic
	private ImageIcon[]  startIcon = new ImageIcon[5];     //store the init pic
	private JLabel[] playcard = new JLabel[5];                    //data member to store the data
	private JLabel[] comcard = new JLabel[5];
	private JLabel comresult = new JLabel("<COM cards type>");
	private JLabel playresult = new JLabel("<PLAY cards type>");
	private JLabel gameresult = new JLabel("<RESULT>");
	JButton button = new JButton("random"); //create button 
	public FrameWork (){	
		Border linesred = new LineBorder(Color.RED,3);       //set the border to use
		Border linesblue = new LineBorder(Color.BLUE,3);
		Font notice = new Font("TimesRoman",Font.BOLD,30);   //set the font to the gameresult use
		Font STARTRANDOM = new Font("TimesRoman",Font.BOLD,70);  //  the button used
		button.setBackground(Color.GRAY);
		for(int t=0;t<21;t++){
			cardIcon[t] = new ImageIcon("image/card/"+t+".png");   //let them store the pic of cards
		}
		for(int t=0;t<5;t++){
			startIcon[t]= new ImageIcon("image/card/init_"+(t+1)+".png");	
		}
		setLayout(new BorderLayout(5,5));   //set the frame layout way
		
		JPanel P1 = new JPanel();            //set pp1 & pp2 on the p1 
		P1.setLayout(new GridLayout(3,1,5,5));
		P1.setBackground(Color.WHITE);
		JPanel PP1 = new JPanel();           //pp1 is the computer's data
		PP1.setLayout(new GridLayout(1,7,5,5));
		JPanel PP2 = new JPanel();           //pp2 is the player's data
		PP2.setLayout(new GridLayout(1,7,5,5));
		JLabel setcom = new JLabel("Computer");
		setcom.setForeground(Color.RED);     // let the fore of computer is red
		PP1.add(setcom);
		for(int t=0;t<5;t++){   // add the labels for the [five cards]
			comcard[t]=new JLabel("");
			comcard[t].setBorder(linesred);
			PP1.add(comcard[t]);
			comcard[t].setIcon(startIcon[t]);
		}
		JLabel setplay = new JLabel("Player");
		setplay.setForeground(Color.BLUE);  //let the fore of play is blue
		PP2.add(setplay);
		for(int t=0;t<5;t++){
			playcard[t]=new JLabel("");
			playcard[t].setBorder(linesblue);
			PP2.add(playcard[t]);
			playcard[t].setIcon(startIcon[t]);
		}
		P1.add(PP1);
		P1.add(PP2);
		button.setFont(STARTRANDOM);   //let the font of button more big 
		P1.add(button);
		
		JPanel P2 = new JPanel();  //P2 store the result labels
		P2.setLayout(new GridLayout(3,1,5,5));
		P2.add(comresult);
		P2.add(playresult);
		gameresult.setFont(notice);  //let the gameresult more big
		P2.add(gameresult);
		
		add(P1,BorderLayout.CENTER);
		add(P2,BorderLayout.EAST);	
		button.addActionListener(this);
		
	}
	public void actionPerformed(ActionEvent e){  //to let button have its work
		if(e.getSource() == button){
			User com =new User();   // declared the objects for the computer and player
			User play = new User();
			for(int t=0;t<5;t++){
				comcard[t].setIcon(cardIcon[com.getnumber(t)]);   // to change the icon of label
				playcard[t].setIcon(cardIcon[play.getnumber(t)]);
			}
			comresult.setText(com.getcardresult());  // to change the string of label
			playresult.setText(play.getcardresult());
			gameresult.setText(play.gameresult(com));
		}
	}
}