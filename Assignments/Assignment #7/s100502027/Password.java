package a7.s100502027;

public class Password {
	private String inputpassword ;  //  to store the password
	private int theNumberCount=0,theLetterCount=0;   // to store the number of letter and number
	
	public Password(String words){
		inputpassword = words;
		CategoryWordType(words);
	}
	
	public String getPassword(){
		return inputpassword;
	}
	
	public void CategoryWordType(String words){     // to distinguish everyone in the string is letter or number
		char[] password = words.toCharArray() ;
		for(int t=0;t<words.length();t++){
			if(password[t]<='9' && password[t]>='0'){
				theNumberCount++;
			}
			else{
				theLetterCount++;
			}
		}
	}
	public void confirmPassword () throws InvalidPasswordException{  //accroding to the length and count of letter or number   to confirm thepassword is valid or invalid
		String errorResult1 = "The length of the password "+inputpassword+" is not enough!!";
		String errorResult2 = "The password "+inputpassword+" has no letter!!";
		String errorResult3 = "The password "+inputpassword+" has no number!!";
		if(inputpassword.length()<7){
			throw new InvalidPasswordException(errorResult1);
		}
		else if(theLetterCount<1){
			throw new InvalidPasswordException(errorResult2);
		}
		else if(theNumberCount<1){
			throw new InvalidPasswordException(errorResult3);
		}
		else{
		}
	}
}
