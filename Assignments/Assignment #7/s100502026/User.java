package a7.s100502026;

public class User {
	
	private int[][] cards=new int[4][14];
	private int[] player=new int[5];
	private int[] NPC=new int[5];
	
	User(){
		for(int i=0;i<player.length;i++)
		{
			player[i]=1;
			NPC[i]=1;
		}
		player[4]=0;
		player[4]=0;
	}
	
	//Get array,player
	public int[] getPlayer()
	{
		return player;
	}
	
	//Get array,NPC
	public int[] getNPC()
	{
		return NPC;
	}
	
	//Get a element of player[]
	public int getPlayer(int i)
	{
		return player[i];
	}
	
	//Get a element of NPC[]
	public int getNPC(int i)
	{
		return NPC[i];
	}
	
	//Get sort of card
	public int getSort()
	{
		return (int)(Math.random()*4)+1;
	}
	
	//Get number of card
	public int getNumber()
	{
		return (int)(Math.random()*14);
	}
	
	//Get a set of cards for one round
	public void dealCards()
	{
		boolean deal=true;
		int counter=0;
		int sort=0;
		int number=0;
		
		//Get whole cards
		for(int i=0;i<5;i++)
		{
			for(int j=0;j<14;j++)
			{
				cards[i][j]=1;
			}
		}
		cards[1][13]=0;//this is another Joker
		cards[2][13]=0;//this is spare card
		cards[3][13]=0;//this is always null
		
		//For this assignment,take some cards out
		for(int i=0;i<5;i++)
		{
			for(int j=2;j<11;j++)
			{
				cards[i][j]=0;
			}
		}	

		if(counter<5)
		{
			//Deal a card to player
			while(deal)
			{
				sort=getSort();
				number=getNumber();
				if( cards[sort][number]==1 )
				{
					deal=false;
					cards[sort][number]=cards[sort][number]-1;
				}
			}
			deal=true;
			
			player[counter]=sort*1000+number;
			
			//Deal a card to NPC
			while(deal)
			{
				sort=getSort();
				number=getNumber();
				if( cards[sort][number]==1 )
				{
				deal=false;
				cards[sort][number]=cards[sort][number]-1;
				}
			}
			deal=true;
			
			NPC[counter]=sort*1000+number;
			counter=counter+1;
		}
	}

	public int getType(int array[])
	{
		//Joker
		if( ( array[0]==0 ) || ( array[1]==0 ) || ( array[2]==0 ) || ( array[3]==0 ) || ( array[5]==0 ) )
		{
			return 6;
		}
		//Four of a kind
		else if( (array[0]==array[1] && array[1]==array[2] && array[2]==array[3]) || ( array[1]==array[2] && array[2]==array[3] && array[3]==array[4] ) )
		{
			return 5;
		}
		//Full house
		else if( ( array[0]==array[1] && array[1]==array[2] && array[3]==array[4] ) || ( array[2]==array[3] && array[3]==array[4] && array[0]==array[1] ) )
		{
			return 4;
		}
		//Three of a kind
		else if( ( array[0]==array[1] && array[1]==array[2] ) || ( array[1]==array[2] && array[2]==array[3] ) || ( array[2]==array[3] && array[3]==array[4] ) )
		{
			return 3;
		}
		//Two pairs
		else if( ( array[0]==array[1] && array[2] == array[3] ) || ( array[0]==array[1] && array[3]==array[4] ) || ( array[1]==array[2] && array[3]==array[4]) )
		{
			return 2;
		}
		//Others
		else
		{
			return 1;
		}		
	}
	
	//Bubble sort modified
	public void bubbleSorty(int array[])
	{
		boolean bubble=true;
		int counter=array.length-1;
		
		while(bubble)
		{			
			for(int i=0;i<counter;i++)
			{
				if(array[i]<array[i+1])
					swap(i,i+1,array);
			}
			bubble=bubble(array);
		}
	}
	
	//Bubble for bubble sort modified
	public boolean bubble(int[] array)
	{
		for(int i=0;i< (array.length-1);i++)
		{
			if(array[i]<array[i+1])
			{
				return true;
			}
		}
		return false;
	}
	
	//Swap for bubble sort modified 
	public int[] swap(int a,int b,int[] array)
	{
		int temp=array[a]; 
		array[a]=array[b];
		array[b]=temp;
		
		return array;
	}
}
