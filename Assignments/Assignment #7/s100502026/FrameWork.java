package a7.s100502026;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.*;
import javax.swing.colorchooser.*;

public class FrameWork extends JFrame implements ActionListener{
	

	
	//This is frame's game 
	private	User game=new User();
	
	/*
	 * These are some objects of frame's panels 
	 * 1.About "random"
	 * 2.About image
	 * 3.About type
	 * 4.About result
	 */	
	private JPanel panel=new JPanel(new BorderLayout());
	private JPanel playerPanel=new JPanel(new BorderLayout());//1
	private JPanel cardPanel=new JPanel(new BorderLayout());//2
	private JPanel PlayerxCardPanel=new JPanel(new GridLayout(1,5,5,5));//2
	private JPanel NPCxCardPanel=new JPanel(new GridLayout(1,5,5,5));//2
	private JPanel typePanel=new JPanel(new BorderLayout());//3
	private JPanel resultPanel=new JPanel();//4
	
	/*
	 * Some variables
	 * 1.About "random"
	 * 2.About image
	 * 3.About type
	 * 4.About result
	 * 5.About font and bolder
	 */
	
	//1
	private JButton button = new JButton("random");
	//2
	private ImageIcon card1Icon=new ImageIcon("image/card/init_1.png");
	private ImageIcon card2Icon=new ImageIcon("image/card/init_2.png");
	private ImageIcon card3Icon=new ImageIcon("image/card/init_3.png");
	private ImageIcon card4Icon=new ImageIcon("image/card/init_4.png");
	private ImageIcon card5Icon=new ImageIcon("image/card/init_5.png");
	private ImageIcon card6Icon=new ImageIcon("image/card/init_1.png");
	private ImageIcon card7Icon=new ImageIcon("image/card/init_2.png");
	private ImageIcon card8Icon=new ImageIcon("image/card/init_3.png");
	private ImageIcon card9Icon=new ImageIcon("image/card/init_4.png");
	private ImageIcon card10Icon=new ImageIcon("image/card/init_5.png");
	private JLabel jlblPlayerxCard1=new JLabel(card1Icon);
	private JLabel jlblPlayerxCard2=new JLabel(card2Icon);
	private JLabel jlblPlayerxCard3=new JLabel(card3Icon);
	private JLabel jlblPlayerxCard4=new JLabel(card4Icon);
	private JLabel jlblPlayerxCard5=new JLabel(card5Icon);
	private JLabel jlblNPCxCard1=new JLabel(card6Icon);
	private JLabel jlblNPCxCard2=new JLabel(card7Icon);
	private JLabel jlblNPCxCard3=new JLabel(card8Icon);
	private JLabel jlblNPCxCard4=new JLabel(card9Icon);
	private JLabel jlblNPCxCard5=new JLabel(card10Icon);
	//3
	private JLabel jlblPlayerxType=new JLabel("Joker");
	private JLabel jlblNPCxType=new JLabel("Joker");
	//4
	private JLabel jlblResult=new JLabel("Draw");
	//5
	Font font=new Font("TimesRoman",Font.BOLD,20);
	Border lineBorder=new LineBorder(Color.CYAN,2);

	//active button event here
	public FrameWork ()
	{
		button.addActionListener(this);		

		//Control label
		jlblResult.setBackground(Color.PINK);
		jlblResult.setBorder(lineBorder);
		jlblResult.setFont(font);
		
		//Main panel
		panel.add(button,BorderLayout.NORTH);
		
		//Panel for appealing player's name
		playerPanel.add( new JLabel("Player's cards") , BorderLayout.NORTH);
		playerPanel.add( new JLabel("NPC's card") , BorderLayout.SOUTH);
		
		
		/*
		 * Panel for appealing cards
		 * 1.Panel for player
		 * 2.Panel for NPC
		 * 3.Complete panel of card
		 */
		PlayerxCardPanel.add(jlblPlayerxCard1);//1
		PlayerxCardPanel.add(jlblPlayerxCard2);
		PlayerxCardPanel.add(jlblPlayerxCard3);
		PlayerxCardPanel.add(jlblPlayerxCard4);
		PlayerxCardPanel.add(jlblPlayerxCard5);
		
		NPCxCardPanel.add(jlblNPCxCard1);//2
		NPCxCardPanel.add(jlblNPCxCard2);
		NPCxCardPanel.add(jlblNPCxCard3);
		NPCxCardPanel.add(jlblNPCxCard4);
		NPCxCardPanel.add(jlblNPCxCard5);
		
		cardPanel.add(PlayerxCardPanel,BorderLayout.NORTH);//3
		cardPanel.add(NPCxCardPanel,BorderLayout.SOUTH);
		
		//Panel for appealing type of cards
		typePanel.add(jlblPlayerxType,BorderLayout.NORTH);
		typePanel.add(jlblNPCxType,BorderLayout.SOUTH);
		
		//Panel for result
		resultPanel.add(jlblResult);
		
		//Add panels and frame
		add(panel,BorderLayout.NORTH);
		add(playerPanel,BorderLayout.WEST);
		add(cardPanel,BorderLayout.CENTER);
		add(typePanel,BorderLayout.EAST);
		add(resultPanel,BorderLayout.SOUTH);
	}
	
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == button)
		{
			//Declare
			int typeOfPlayer;
			int typeOfNPC;
			
			/*
			 * 1.Deal cards
			 * 2.Get type of your cards
			 * 3.Get type of NPC's cards
			 * 4.Compare two types
			 */
			
			//1
			game.dealCards();
			
			switch(game.getPlayer(0))
			{
				case 1000:
					card1Icon= new ImageIcon("image/card/20.png");
					break;
				case 1001:
					card1Icon= new ImageIcon("image/card/0.png");
					break;
				case 1002:
					card1Icon= new ImageIcon("image/card/1.png");
					break;
				case 1011:
					card1Icon= new ImageIcon("image/card/2.png");
					break;
				case 1012:
					card1Icon= new ImageIcon("image/card/3.png");
					break;
				case 1013:
					card1Icon= new ImageIcon("image/card/4.png");
					break;
					
				case 2001:
					card1Icon= new ImageIcon("image/card/5.png");
					break;
				case 2002:
					card1Icon= new ImageIcon("image/card/6.png");
					break;
				case 2011:
					card1Icon= new ImageIcon("image/card/7.png");
					break;
				case 2012:
					card1Icon= new ImageIcon("image/card/8.png");
					break;
				case 2013:
					card1Icon= new ImageIcon("image/card/9.png");
					break;
					
				case 3001:
					card1Icon= new ImageIcon("image/card/10.png");
					break;
				case 3002:
					card1Icon= new ImageIcon("image/card/11.png");
					break;
				case 3011:
					card1Icon= new ImageIcon("image/card/12.png");
					break;					
				case 3012:
					card1Icon= new ImageIcon("image/card/13.png");
					break;
				case 3013:
					card1Icon= new ImageIcon("image/card/14.png");
					break;
					
				case 4001:
					card1Icon= new ImageIcon("image/card/15.png");
					break;
				case 4002:
					card1Icon= new ImageIcon("image/card/16.png");
					break;
				case 4011:
					card1Icon= new ImageIcon("image/card/17.png");
					break;
				case 4012:
					card1Icon= new ImageIcon("image/card/18.png");
					break;
				case 4013:
					card1Icon= new ImageIcon("image/card/19.png");
					break;
			}
			
			switch(game.getPlayer(1))
			{
				case 1000:
					card2Icon= new ImageIcon("image/card/20.png");
					break;
				case 1001:
					card2Icon= new ImageIcon("image/card/0.png");
					break;
				case 1002:
					card2Icon= new ImageIcon("image/card/1.png");
					break;
				case 1011:
					card2Icon= new ImageIcon("image/card/2.png");
					break;
				case 1012:
					card2Icon= new ImageIcon("image/card/3.png");
					break;
				case 1013:
					card2Icon= new ImageIcon("image/card/4.png");
					break;
					
				case 2001:
					card2Icon= new ImageIcon("image/card/5.png");
					break;
				case 2002:
					card2Icon= new ImageIcon("image/card/6.png");
					break;
				case 2011:
					card2Icon= new ImageIcon("image/card/7.png");
					break;
				case 2012:
					card2Icon= new ImageIcon("image/card/8.png");
					break;
				case 2013:
					card2Icon= new ImageIcon("image/card/9.png");
					break;
					
				case 3001:
					card2Icon= new ImageIcon("image/card/10.png");
					break;
				case 3002:
					card2Icon= new ImageIcon("image/card/11.png");
					break;
				case 3011:
					card2Icon= new ImageIcon("image/card/12.png");
					break;					
				case 3012:
					card2Icon= new ImageIcon("image/card/13.png");
					break;
				case 3013:
					card2Icon= new ImageIcon("image/card/14.png");
					break;
					
				case 4001:
					card2Icon= new ImageIcon("image/card/15.png");
					break;
				case 4002:
					card2Icon= new ImageIcon("image/card/16.png");
					break;
				case 4011:
					card2Icon= new ImageIcon("image/card/17.png");
					break;
				case 4012:
					card2Icon= new ImageIcon("image/card/18.png");
					break;
				case 4013:
					card2Icon= new ImageIcon("image/card/19.png");
					break;
			}
			
			switch(game.getPlayer(2))
			{
				case 1000:
					card3Icon= new ImageIcon("image/card/20.png");
					break;
				case 1001:
					card3Icon= new ImageIcon("image/card/0.png");
					break;
				case 1002:
					card3Icon= new ImageIcon("image/card/1.png");
					break;
				case 1011:
					card3Icon= new ImageIcon("image/card/2.png");
					break;
				case 1012:
					card3Icon= new ImageIcon("image/card/3.png");
					break;
				case 1013:
					card3Icon= new ImageIcon("image/card/4.png");
					break;
					
				case 2001:
					card3Icon= new ImageIcon("image/card/5.png");
					break;
				case 2002:
					card3Icon= new ImageIcon("image/card/6.png");
					break;
				case 2011:
					card3Icon= new ImageIcon("image/card/7.png");
					break;
				case 2012:
					card3Icon= new ImageIcon("image/card/8.png");
					break;
				case 2013:
					card3Icon= new ImageIcon("image/card/9.png");
					break;
					
				case 3001:
					card3Icon= new ImageIcon("image/card/10.png");
					break;
				case 3002:
					card3Icon= new ImageIcon("image/card/11.png");
					break;
				case 3011:
					card3Icon= new ImageIcon("image/card/12.png");
					break;					
				case 3012:
					card3Icon= new ImageIcon("image/card/13.png");
					break;
				case 3013:
					card3Icon= new ImageIcon("image/card/14.png");
					break;
					
				case 4001:
					card3Icon= new ImageIcon("image/card/15.png");
					break;
				case 4002:
					card3Icon= new ImageIcon("image/card/16.png");
					break;
				case 4011:
					card3Icon= new ImageIcon("image/card/17.png");
					break;
				case 4012:
					card3Icon= new ImageIcon("image/card/18.png");
					break;
				case 4013:
					card3Icon= new ImageIcon("image/card/19.png");
					break;
			}
			
			switch(game.getPlayer(3))
			{
				case 1000:
					card4Icon= new ImageIcon("image/card/20.png");
					break;
				case 1001:
					card4Icon= new ImageIcon("image/card/0.png");
					break;
				case 1002:
					card4Icon= new ImageIcon("image/card/1.png");
					break;
				case 1011:
					card4Icon= new ImageIcon("image/card/2.png");
					break;
				case 1012:
					card4Icon= new ImageIcon("image/card/3.png");
					break;
				case 1013:
					card4Icon= new ImageIcon("image/card/4.png");
					break;
					
				case 2001:
					card4Icon= new ImageIcon("image/card/5.png");
					break;
				case 2002:
					card4Icon= new ImageIcon("image/card/6.png");
					break;
				case 2011:
					card4Icon= new ImageIcon("image/card/7.png");
					break;
				case 2012:
					card4Icon= new ImageIcon("image/card/8.png");
					break;
				case 2013:
					card4Icon= new ImageIcon("image/card/9.png");
					break;
					
				case 3001:
					card4Icon= new ImageIcon("image/card/10.png");
					break;
				case 3002:
					card4Icon= new ImageIcon("image/card/11.png");
					break;
				case 3011:
					card4Icon= new ImageIcon("image/card/12.png");
					break;					
				case 3012:
					card4Icon= new ImageIcon("image/card/13.png");
					break;
				case 3013:
					card4Icon= new ImageIcon("image/card/14.png");
					break;
					
				case 4001:
					card4Icon= new ImageIcon("image/card/15.png");
					break;
				case 4002:
					card4Icon= new ImageIcon("image/card/16.png");
					break;
				case 4011:
					card4Icon= new ImageIcon("image/card/17.png");
					break;
				case 4012:
					card4Icon= new ImageIcon("image/card/18.png");
					break;
				case 4013:
					card4Icon= new ImageIcon("image/card/19.png");
					break;
			}
			
			switch(game.getPlayer(4))
			{
				case 1000:
					card5Icon= new ImageIcon("image/card/20.png");
					break;
				case 1001:
					card5Icon= new ImageIcon("image/card/0.png");
					break;
				case 1002:
					card5Icon= new ImageIcon("image/card/1.png");
					break;
				case 1011:
					card5Icon= new ImageIcon("image/card/2.png");
					break;
				case 1012:
					card5Icon= new ImageIcon("image/card/3.png");
					break;
				case 1013:
					card5Icon= new ImageIcon("image/card/4.png");
					break;
					
				case 2001:
					card5Icon= new ImageIcon("image/card/5.png");
					break;
				case 2002:
					card5Icon= new ImageIcon("image/card/6.png");
					break;
				case 2011:
					card5Icon= new ImageIcon("image/card/7.png");
					break;
				case 2012:
					card5Icon= new ImageIcon("image/card/8.png");
					break;
				case 2013:
					card5Icon= new ImageIcon("image/card/9.png");
					break;
					
				case 3001:
					card5Icon= new ImageIcon("image/card/10.png");
					break;
				case 3002:
					card5Icon= new ImageIcon("image/card/11.png");
					break;
				case 3011:
					card5Icon= new ImageIcon("image/card/12.png");
					break;					
				case 3012:
					card5Icon= new ImageIcon("image/card/13.png");
					break;
				case 3013:
					card5Icon= new ImageIcon("image/card/14.png");
					break;
					
				case 4001:
					card5Icon= new ImageIcon("image/card/15.png");
					break;
				case 4002:
					card5Icon= new ImageIcon("image/card/16.png");
					break;
				case 4011:
					card5Icon= new ImageIcon("image/card/17.png");
					break;
				case 4012:
					card5Icon= new ImageIcon("image/card/18.png");
					break;
				case 4013:
					card5Icon= new ImageIcon("image/card/19.png");
					break;
			}		
			
			switch(game.getNPC(0))
			{
				case 1000:
					card6Icon= new ImageIcon("image/card/20.png");
					break;
				case 1001:
					card6Icon= new ImageIcon("image/card/0.png");
					break;
				case 1002:
					card6Icon= new ImageIcon("image/card/1.png");
					break;
				case 1011:
					card6Icon= new ImageIcon("image/card/2.png");
					break;
				case 1012:
					card6Icon= new ImageIcon("image/card/3.png");
					break;
				case 1013:
					card6Icon= new ImageIcon("image/card/4.png");
					break;
					
				case 2001:
					card6Icon= new ImageIcon("image/card/5.png");
					break;
				case 2002:
					card6Icon= new ImageIcon("image/card/6.png");
					break;
				case 2011:
					card6Icon= new ImageIcon("image/card/7.png");
					break;
				case 2012:
					card6Icon= new ImageIcon("image/card/8.png");
					break;
				case 2013:
					card6Icon= new ImageIcon("image/card/9.png");
					break;
					
				case 3001:
					card6Icon= new ImageIcon("image/card/10.png");
					break;
				case 3002:
					card6Icon= new ImageIcon("image/card/11.png");
					break;
				case 3011:
					card6Icon= new ImageIcon("image/card/12.png");
					break;					
				case 3012:
					card6Icon= new ImageIcon("image/card/13.png");
					break;
				case 3013:
					card6Icon= new ImageIcon("image/card/14.png");
					break;
					
				case 4001:
					card6Icon= new ImageIcon("image/card/15.png");
					break;
				case 4002:
					card6Icon= new ImageIcon("image/card/16.png");
					break;
				case 4011:
					card6Icon= new ImageIcon("image/card/17.png");
					break;
				case 4012:
					card6Icon= new ImageIcon("image/card/18.png");
					break;
				case 4013:
					card6Icon= new ImageIcon("image/card/19.png");
					break;
			}
			
			switch(game.getNPC(1))
			{
				case 1000:
					card7Icon= new ImageIcon("image/card/20.png");
					break;
				case 1001:
					card7Icon= new ImageIcon("image/card/0.png");
					break;
				case 1002:
					card7Icon= new ImageIcon("image/card/1.png");
					break;
				case 1011:
					card7Icon= new ImageIcon("image/card/2.png");
					break;
				case 1012:
					card7Icon= new ImageIcon("image/card/3.png");
					break;
				case 1013:
					card7Icon= new ImageIcon("image/card/4.png");
					break;
					
				case 2001:
					card7Icon= new ImageIcon("image/card/5.png");
					break;
				case 2002:
					card7Icon= new ImageIcon("image/card/6.png");
					break;
				case 2011:
					card7Icon= new ImageIcon("image/card/7.png");
					break;
				case 2012:
					card7Icon= new ImageIcon("image/card/8.png");
					break;
				case 2013:
					card7Icon= new ImageIcon("image/card/9.png");
					break;
					
				case 3001:
					card7Icon= new ImageIcon("image/card/10.png");
					break;
				case 3002:
					card7Icon= new ImageIcon("image/card/11.png");
					break;
				case 3011:
					card7Icon= new ImageIcon("image/card/12.png");
					break;					
				case 3012:
					card7Icon= new ImageIcon("image/card/13.png");
					break;
				case 3013:
					card7Icon= new ImageIcon("image/card/14.png");
					break;
					
				case 4001:
					card7Icon= new ImageIcon("image/card/15.png");
					break;
				case 4002:
					card7Icon= new ImageIcon("image/card/16.png");
					break;
				case 4011:
					card7Icon= new ImageIcon("image/card/17.png");
					break;
				case 4012:
					card7Icon= new ImageIcon("image/card/18.png");
					break;
				case 4013:
					card7Icon= new ImageIcon("image/card/19.png");
					break;
			}
			
			switch(game.getNPC(2))
			{
				case 1000:
					card8Icon= new ImageIcon("image/card/20.png");
					break;
				case 1001:
					card8Icon= new ImageIcon("image/card/0.png");
					break;
				case 1002:
					card8Icon= new ImageIcon("image/card/1.png");
					break;
				case 1011:
					card8Icon= new ImageIcon("image/card/2.png");
					break;
				case 1012:
					card8Icon= new ImageIcon("image/card/3.png");
					break;
				case 1013:
					card8Icon= new ImageIcon("image/card/4.png");
					break;
					
				case 2001:
					card8Icon= new ImageIcon("image/card/5.png");
					break;
				case 2002:
					card8Icon= new ImageIcon("image/card/6.png");
					break;
				case 2011:
					card8Icon= new ImageIcon("image/card/7.png");
					break;
				case 2012:
					card8Icon= new ImageIcon("image/card/8.png");
					break;
				case 2013:
					card8Icon= new ImageIcon("image/card/9.png");
					break;
					
				case 3001:
					card8Icon= new ImageIcon("image/card/10.png");
					break;
				case 3002:
					card8Icon= new ImageIcon("image/card/11.png");
					break;
				case 3011:
					card8Icon= new ImageIcon("image/card/12.png");
					break;					
				case 3012:
					card8Icon= new ImageIcon("image/card/13.png");
					break;
				case 3013:
					card8Icon= new ImageIcon("image/card/14.png");
					break;
					
				case 4001:
					card8Icon= new ImageIcon("image/card/15.png");
					break;
				case 4002:
					card8Icon= new ImageIcon("image/card/16.png");
					break;
				case 4011:
					card8Icon= new ImageIcon("image/card/17.png");
					break;
				case 4012:
					card8Icon= new ImageIcon("image/card/18.png");
					break;
				case 4013:
					card8Icon= new ImageIcon("image/card/19.png");
					break;
			}
			
			switch(game.getNPC(3))
			{
				case 1000:
					card9Icon= new ImageIcon("image/card/20.png");
					break;
				case 1001:
					card9Icon= new ImageIcon("image/card/0.png");
					break;
				case 1002:
					card9Icon= new ImageIcon("image/card/1.png");
					break;
				case 1011:
					card9Icon= new ImageIcon("image/card/2.png");
					break;
				case 1012:
					card9Icon= new ImageIcon("image/card/3.png");
					break;
				case 1013:
					card9Icon= new ImageIcon("image/card/4.png");
					break;
					
				case 2001:
					card9Icon= new ImageIcon("image/card/5.png");
					break;
				case 2002:
					card9Icon= new ImageIcon("image/card/6.png");
					break;
				case 2011:
					card9Icon= new ImageIcon("image/card/7.png");
					break;
				case 2012:
					card9Icon= new ImageIcon("image/card/8.png");
					break;
				case 2013:
					card9Icon= new ImageIcon("image/card/9.png");
					break;
					
				case 3001:
					card9Icon= new ImageIcon("image/card/10.png");
					break;
				case 3002:
					card9Icon= new ImageIcon("image/card/11.png");
					break;
				case 3011:
					card9Icon= new ImageIcon("image/card/12.png");
					break;					
				case 3012:
					card9Icon= new ImageIcon("image/card/13.png");
					break;
				case 3013:
					card9Icon= new ImageIcon("image/card/14.png");
					break;
					
				case 4001:
					card9Icon= new ImageIcon("image/card/15.png");
					break;
				case 4002:
					card9Icon= new ImageIcon("image/card/16.png");
					break;
				case 4011:
					card9Icon= new ImageIcon("image/card/17.png");
					break;
				case 4012:
					card9Icon= new ImageIcon("image/card/18.png");
					break;
				case 4013:
					card9Icon= new ImageIcon("image/card/19.png");
					break;
			}
			
			switch(game.getNPC(4))
			{
				case 1000:
					card10Icon= new ImageIcon("image/card/20.png");
					break;
				case 1001:
					card10Icon= new ImageIcon("image/card/0.png");
					break;
				case 1002:
					card10Icon= new ImageIcon("image/card/1.png");
					break;
				case 1011:
					card10Icon= new ImageIcon("image/card/2.png");
					break;
				case 1012:
					card10Icon= new ImageIcon("image/card/3.png");
					break;
				case 1013:
					card10Icon= new ImageIcon("image/card/4.png");
					break;
					
				case 2001:
					card10Icon= new ImageIcon("image/card/5.png");
					break;
				case 2002:
					card10Icon= new ImageIcon("image/card/6.png");
					break;
				case 2011:
					card10Icon= new ImageIcon("image/card/7.png");
					break;
				case 2012:
					card10Icon= new ImageIcon("image/card/8.png");
					break;
				case 2013:
					card10Icon= new ImageIcon("image/card/9.png");
					break;
					
				case 3001:
					card10Icon= new ImageIcon("image/card/10.png");
					break;
				case 3002:
					card10Icon= new ImageIcon("image/card/11.png");
					break;
				case 3011:
					card10Icon= new ImageIcon("image/card/12.png");
					break;					
				case 3012:
					card10Icon= new ImageIcon("image/card/13.png");
					break;
				case 3013:
					card10Icon= new ImageIcon("image/card/14.png");
					break;
					
				case 4001:
					card10Icon= new ImageIcon("image/card/15.png");
					break;
				case 4002:
					card10Icon= new ImageIcon("image/card/16.png");
					break;
				case 4011:
					card10Icon= new ImageIcon("image/card/17.png");
					break;
				case 4012:
					card10Icon= new ImageIcon("image/card/18.png");
					break;
				case 4013:
					card10Icon= new ImageIcon("image/card/19.png");
					break;
			}		

			
			//2
			typeOfPlayer=game.getType( game.getPlayer() );
			
			//3
			typeOfNPC=game.getType( game.getNPC() );
			
			//4
			if( typeOfPlayer-typeOfNPC>0 )
			{
				jlblResult.setText("Player wins.");
			}
			else if( typeOfPlayer-typeOfNPC<0 )
			{
				jlblResult.setText("NPC wins.");
			}
			else
			{
				jlblResult.setText("Draw");
			}
		}
	}
}
