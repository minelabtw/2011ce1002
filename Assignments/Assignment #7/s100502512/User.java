package a7.s100502512;

import java.util.Random;

public class User {
	static Random rand = new Random();
	static int cards[] = new int[5];// 代表一個人有五張牌

	public int random(int Rand) {// 使用亂數
		int rand2 = rand.nextInt(21);
		Rand = rand2;
		return Rand;
	}

	public int Setcards(int card) {// 每一種路徑代表哪種牌
		int c = 0;
		if (card == 0 || card == 1 || card == 2 || card == 3) {
			c = 1;
		} else if (card == 4 || card == 5 || card == 6 || card == 7) {
			c = 2;
		} else if (card == 8 || card == 9 || card == 10 || card == 11) {
			c = 11;
		} else if (card == 12 || card == 13 || card == 14 || card == 15) {
			c = 12;
		} else if (card == 16 || card == 17 || card == 18 || card == 19) {
			c = 13;
		} else if (card == 20) {
			c = -1;
		}
		return c;
	}

	public int type(int r1, int r2, int r3, int r4, int r5) {// 代表何種牌種
		int times = 0;// 儲存重複的次數
		cards[0] = Setcards(r1);
		cards[1] = Setcards(r2);
		cards[2] = Setcards(r3);
		cards[3] = Setcards(r4);
		cards[4] = Setcards(r5);
		for (int i = 0; i <= 4; i++) {
			for (int j = i + 1; j <= 4; j++) {
				if (cards[i] == cards[j]) {// 若牌種一樣則加一
					times++;
				}
			}
		}
		for (int i = 0; i <= 4; i++) {
			if (cards[i] == -1) {// Joker
				return 5;
			}
		}
		if (times == 2) {// two pairs
			return 1;
		}

		else if (times == 3) {// three of a kind
			return 2;
		}

		else if (times == 4) {// full house
			return 3;
		}

		else if (times == 6) {// four of a kind
			return 4;
		}

		else {
			return 0;// Nothing
		}

	}
}
