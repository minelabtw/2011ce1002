package a7.s100502512;

import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.LineBorder;

public class FrameWork extends JFrame implements ActionListener {
	JButton button = new JButton("Random");
	private ImageIcon PleaseIcon = new ImageIcon("image/card/init_1.PNG");//起始圖片
	private ImageIcon PressIcon = new ImageIcon("image/card/init_2.PNG");
	private ImageIcon RandomIcon = new ImageIcon("image/card/init_3.PNG");
	private ImageIcon ButtonIcon = new ImageIcon("image/card/init_4.PNG");
	private ImageIcon StartIcon = new ImageIcon("image/card/init_5.PNG");
	private ImageIcon Please2Icon = new ImageIcon("image/card/init_1.PNG");
	private ImageIcon Press2Icon = new ImageIcon("image/card/init_2.PNG");
	private ImageIcon Random2Icon = new ImageIcon("image/card/init_3.PNG");
	private ImageIcon Button2Icon = new ImageIcon("image/card/init_4.PNG");
	private ImageIcon Start2Icon = new ImageIcon("image/card/init_5.PNG");
	JLabel label1 = new JLabel("");// 顯示牌型
	JLabel label2 = new JLabel("");// 顯示牌型
	JLabel label3 = new JLabel("");// 顯示輸贏
	JLabel im1, im2, im3, im4, im5, im6, im7, im8, im9, im10;
    int rand;
	public FrameWork() {
		JPanel p1 = new JPanel();//第一個panel撲克牌的部分
		p1.setLayout(new GridLayout(2, 7, 5, 5));
		button.setFont(new Font("serif", Font.BOLD, 120));
		button.addActionListener(this);
		JLabel computer = new JLabel("Computer");
		computer.setFont(new Font("Serif", Font.BOLD, 24));
		p1.add(computer);
		im1 = new JLabel(PleaseIcon);
		im1.setBorder(new LineBorder(Color.black, 15));
		p1.add(im1);
		im2 = new JLabel(PressIcon);
		im2.setBorder(new LineBorder(Color.black, 15));
		p1.add(im2);
		im3 = new JLabel(RandomIcon);
		im3.setBorder(new LineBorder(Color.black, 15));
		p1.add(im3);
		im4 = new JLabel(ButtonIcon);
		im4.setBorder(new LineBorder(Color.black, 15));
		p1.add(im4);
		im5 = new JLabel(StartIcon);
		im5.setBorder(new LineBorder(Color.black, 15));
		p1.add(im5);
		JLabel player = new JLabel("Player");
		player.setFont(new Font("Serif", Font.BOLD, 24));
		p1.add(player);
		im6 = new JLabel(Please2Icon);
		im6.setBorder(new LineBorder(Color.black, 15));
		p1.add(im6);
		im7 = new JLabel(Press2Icon);
		im7.setBorder(new LineBorder(Color.black, 15));
		p1.add(im7);
		im8 = new JLabel(Random2Icon);
		im8.setBorder(new LineBorder(Color.black, 15));
		p1.add(im8);
		im9 = new JLabel(Button2Icon);
		im9.setBorder(new LineBorder(Color.black, 15));
		p1.add(im9);
		im10 = new JLabel(Start2Icon);
		im10.setBorder(new LineBorder(Color.black, 15));
		p1.add(im10);
		JPanel p2 = new JPanel();//第二個部分,按鈕部分加在第一部分上
		p2.setLayout(new BorderLayout(5, 5));
		p2.add((button), BorderLayout.CENTER);
		p2.add(p1, BorderLayout.NORTH);
		add(p2);
		JPanel p3 = new JPanel();//第三部分,顯示訊息部分
		p3.setLayout(new BorderLayout(2, 2));
		p3.add(label1, BorderLayout.NORTH);
		label1.setFont(new Font("Serif", Font.BOLD, 24));
		p3.add(label2, BorderLayout.CENTER);
		label2.setFont(new Font("Serif", Font.BOLD, 24));
		p3.add(label3, BorderLayout.SOUTH);
		label3.setFont(new Font("Serif", Font.BOLD, 50));
		setLayout(new BorderLayout());
		add(p3, BorderLayout.CENTER);
		add(p2, BorderLayout.WEST);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == button) {
			User computer = new User();
			User player = new User();
			int rand1 = computer.random(rand);
			int rand2;
			int rand3;
			int rand4;
			int rand5;
			int rand6;
			int rand7;
			int rand8;
			int rand9;
			int rand10;
			while(true) {//判斷牌是否重複
				rand2 = computer.random(rand);
				if (rand2 != rand1) {
					break;
				} else {
					rand2 = computer.random(rand);
				}
			}
			while(true) {
				rand3 = computer.random(rand);
				if (rand3 != rand1 && rand3 != rand2) {
					break;
				} else {
					rand3 = computer.random(rand);
				}
			}
			while(true) {
				rand4 = computer.random(rand);
				if (rand4 != rand1 && rand4 != rand2 && rand4 != rand3) {
					break;
				} else {
					rand4 = computer.random(rand);
				}
			}
			while(true) {
				rand5 = computer.random(rand);
				if (rand5 != rand1 && rand5 != rand2 && rand5 != rand3
						&& rand5 != rand4) {
					break;
				} else {
					rand5 = computer.random(rand);
				}
			}
			while(true) {
				rand6 = computer.random(rand);
				if (rand6 != rand1 && rand6 != rand2 && rand6 != rand3
						&& rand6 != rand4 && rand6 != rand5) {
					break;
				} else {
					rand6 = computer.random(rand);
				}
			}
			while(true) {
				rand7 = computer.random(rand);
				if (rand7 != rand1 && rand7 != rand2 && rand7 != rand3
						&& rand7 != rand4 && rand7 != rand5 && rand7 != rand6) {
					break;
				} else {
					rand7 = computer.random(rand);
				}
			}
			while(true) {
				rand8 = computer.random(rand);
				if (rand8 != rand1 && rand8 != rand2 && rand8 != rand3
						&& rand8 != rand4 && rand8 != rand5 && rand8 != rand6
						&& rand8 != rand7) {
					break;
				} else {
					rand8 = computer.random(rand);
				}
			}
			while(true) {
				rand9 = computer.random(rand);
				if (rand9 != rand1 && rand9 != rand2 && rand9 != rand3
						&& rand9 != rand4 && rand9 != rand5 && rand9 != rand6
						&& rand9 != rand7 && rand9 != rand8) {
					break;
				} else {
					rand9 = computer.random(rand);
				}
			}
			while(true) {
				rand10 = computer.random(rand);
				if (rand10 != rand1 && rand10 != rand2 && rand10 != rand3
						&& rand10 != rand4 && rand10 != rand5
						&& rand10 != rand6 && rand10 != rand7
						&& rand10 != rand8 && rand10 != rand9) {
					break;
				} else {
					rand10 = computer.random(rand);
				}
			}
			if (computer.type(rand1, rand2, rand3, rand4, rand5) == 5) {//訊息顯示何種牌種
				label1.setText("Joker");
			} else if (computer.type(rand1, rand2, rand3, rand4, rand5) == 1) {
				label1.setText("two pairs");
			} else if (computer.type(rand1, rand2, rand3, rand4, rand5) == 2) {
				label1.setText("three of a kind");
			} else if (computer.type(rand1, rand2, rand3, rand4, rand5) == 3) {
				label1.setText("full house");
			} else if (computer.type(rand1, rand2, rand3, rand4, rand5) == 4) {
				label1.setText("four of a kind");
			} else if (computer.type(rand1, rand2, rand3, rand4, rand5) == 0) {
				label1.setText("Nothing");
			}

			if (player.type(rand6, rand7, rand8, rand9, rand10) == 5) {
				label2.setText("Joker");
			} else if (player.type(rand6, rand7, rand8, rand9, rand10) == 1) {
				label2.setText("two pairs");
			} else if (player.type(rand6, rand7, rand8, rand9, rand10) == 2) {
				label2.setText("three of a kind");
			} else if (player.type(rand6, rand7, rand8, rand9, rand10) == 3) {
				label2.setText("full house");
			} else if (player.type(rand6, rand7, rand8, rand9, rand10) == 4) {
				label2.setText("four of a kind");
			} else if (player.type(rand6, rand7, rand8, rand9, rand10) == 0) {
				label2.setText("Nothing");
			}
			if (player.type(rand6, rand7, rand8, rand9, rand10) > computer //比較大小
					.type(rand1, rand2, rand3, rand4, rand5)) {
				label3.setText("You Win");
			} else if (player.type(rand6, rand7, rand8, rand9, rand10) == computer
					.type(rand1, rand2, rand3, rand4, rand5)) {
				label3.setText("Draw");
			} else if (player.type(rand6, rand7, rand8, rand9, rand10) < computer
					.type(rand1, rand2, rand3, rand4, rand5)) {
				label3.setText("You Lose");
			}
			PleaseIcon = new ImageIcon("image/card/" + rand1 + ".png");//帶入新圖片
			PressIcon = new ImageIcon("image/card/" + rand2 + ".png");
			RandomIcon = new ImageIcon("image/card/" + rand3 + ".png");
			ButtonIcon = new ImageIcon("image/card/" + rand4 + ".png");
			StartIcon = new ImageIcon("image/card/" + rand5 + ".png");
			Please2Icon = new ImageIcon("image/card/" + rand6 + ".png");
			Press2Icon = new ImageIcon("image/card/" + rand7 + ".png");
			Random2Icon = new ImageIcon("image/card/" + rand8 + ".png");
			Button2Icon = new ImageIcon("image/card/" + rand9 + ".png");
			Start2Icon = new ImageIcon("image/card/" + rand10 + ".png");
			im1.setIcon(PleaseIcon);
			im2.setIcon(PressIcon);
			im3.setIcon(RandomIcon);
			im4.setIcon(ButtonIcon);
			im5.setIcon(StartIcon);
			im6.setIcon(Please2Icon);
			im7.setIcon(Press2Icon);
			im8.setIcon(Random2Icon);
			im9.setIcon(Button2Icon);
			im10.setIcon(Start2Icon);

		}
	}
}
