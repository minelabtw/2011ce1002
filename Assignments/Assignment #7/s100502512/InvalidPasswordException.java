package a7.s100502512;

public class InvalidPasswordException extends Exception {
	static InvalidPasswordException exception1;
	static InvalidPasswordException exception2;
	static InvalidPasswordException exception3;
	static InvalidPasswordException exception4;

	public static void situation1() throws InvalidPasswordException {// 字元不足
		if (A72.A.length() < 7) {
			exception1 = new InvalidPasswordException();
			throw exception1;
		}
	}

	public static void situation2() throws InvalidPasswordException {// 判斷有無數字

		for (int i = 0; i < A72.A.length(); i++) {
			if (A72.A.charAt(i) >= 97 && A72.A.charAt(i) <= 122) {

				exception2 = new InvalidPasswordException();
				throw exception2;
			}

		}

	}

	public static void situation3() throws InvalidPasswordException {// 判斷有無數字
		int Test2 = 0;
		for (int i = 0; i < A72.A.length(); i++) {
			if (A72.A.charAt(i) >= 65 && A72.A.charAt(i) <= 90) {

				exception3 = new InvalidPasswordException();
				throw exception3;
			}
		}

	}

	public static void situation4() throws InvalidPasswordException {// 判斷有無字母
		int numbers = 0;
		for (int i = 0; i < A72.A.length(); i++) {

			for (int k = 97; k <= 122; k++) {
				if (A72.A.charAt(i) == k) {
					numbers++;

				}
			}

		}
		if (numbers == 0) {
			exception4 = new InvalidPasswordException();
			throw exception4;
		}
	}
}
