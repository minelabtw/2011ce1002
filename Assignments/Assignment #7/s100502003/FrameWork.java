package a7.s100502003;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener {
	private ImageIcon[] Initcards = new ImageIcon[5]; // ImageIcon array that save initial cards
	private User player; // declare the object of User
	JButton button = new JButton("random");
	JPanel p2 = new JPanel();
	JPanel p3 = new JPanel();
	JPanel initial1 = new JPanel();
	JLabel Type1, Type2, Result;
	JLabel[] InitCards = new JLabel[10];

	public FrameWork() {
		for(int n=0; n<5; n++) {
			Initcards[n] = new ImageIcon(ClassLoader.getSystemResource("image/card/init_"+(n+1)+".png")); // let the system be able to load those cards
		}
		Font players = new Font("TimesNewRoman",  Font.BOLD, 16);
		Font types = new Font("TimesNewRoman", Font.ITALIC, 14);
		Font result = new Font("TimesNewRoman", Font.BOLD, 20);
		Border lineBorder = new LineBorder(Color.BLACK,2);
		
		// panel that will be put in the left side
		p2.setLayout(new GridLayout(2,1));
		JLabel computer = new JLabel("He");
		computer.setFont(players);
		JLabel user = new JLabel("You");
		user.setFont(players);
		user.setForeground(Color.GREEN);
		p2.add(computer);
		p2.add(user);
		
		// panel that will be put in the right side
		p3.setLayout(new GridLayout(3,1));
		Type1 = new JLabel("< HisType >");
		Type1.setFont(types);
		Type2 = new JLabel("< YourType >");
		Type2.setFont(types);
		Result = new JLabel("Result");
		Result.setFont(result);
		Result.setForeground(Color.BLUE);
		p3.add(Type1);
		p3.add(Type2);
		p3.add(Result);
		
		// panel that concludes those cards and be put in the middle
		initial1.setLayout(new GridLayout(2,5,5,5));
		for(int x=0; x<5; x++){
			InitCards[x] = new JLabel(Initcards[x]);
			InitCards[x].setBorder(lineBorder);
			initial1.add(InitCards[x]);
		}
		for(int y=5; y<10; y++){
			InitCards[y] = new JLabel(Initcards[9-y]);
			InitCards[y].setBorder(lineBorder);
			initial1.add(InitCards[y]);
		}
		
		// let the panels be together and show out
		setLayout(new BorderLayout(5,5));
		add(p2,BorderLayout.WEST);
		add(initial1,BorderLayout.CENTER);
		add(button,BorderLayout.SOUTH);
		add(p3,BorderLayout.EAST);
		button.addActionListener(this);	
	}
	
	public void actionPerformed(ActionEvent e) { // situation after enter the button
		if(e.getSource() == button) {
			player = new User();
			for(int z=0; z<10; z++) {
				InitCards[z].setIcon(player.picture()[player.randomSol()[z]]); // reset those cards again
			}
			Result.setText(player.checkWin()); // reset the information that show the result
			Type1.setText(player.getType1()); // reset type1
			Type2.setText(player.getType2()); // reset type2
		}
	}
}
