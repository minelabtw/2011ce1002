package a7.s100502003;

import javax.swing.*;

public class User  {
	private ImageIcon[] cards = new ImageIcon[21]; // declare an ImageIcon array to store those poker cards
	private int[] random = new int[10]; // array that used to store the random values
	private int[] round = new int[5]; // array that help to define the value for each card
	private  String type1, type2; // the type of you and computer's cards 
	
	public User() {
		for(int m=0; m<=20; m++) {
			cards[m] = new ImageIcon(ClassLoader.getSystemResource("image/card/"+m+".png")); // let the system be able to read those cards
		}
		random[0] = (int)(Math.random()*21); // random the first value in the array named random
		for(int i=1; i<10; i++) { // random other values in the array and make sure that we haven't get the newer value yet
			random[i] = (int)(Math.random()*21);  
			for(int j=i-1; j>=0; j--) {
				while(random[j] == random[i])
					random[i] = (int)(Math.random()*21);
			}		
		}
		for(int k=0; k<5; k++) { // about computer's cards
			round[k] = random[k];
		}
		type1 = GetType();
		
		for(int k=0; k<5; k++) { // about user's cards
			round[k] = random[k+5];
		}
		type2 = GetType();
	}
	
	public String GetType() {
		char[] same = new char[5]; // array that used to store the values for each card have you gotten
		int count=0;
		
		for(int a=0; a<5; a++) {
			if(round[a] < 4)
				same[a] = 'A';
			else if(round[a] < 8)
				same[a] = '2';
			else if(round[a] < 12)
				same[a] = 'J';
			else if(round[a] < 16)
				same[a] = 'Q';
			else if(round[a] < 20)
				same[a] = 'K';
			else {
				same[a] = 'W';
				return "Joker";
			}
		}
		
		for(int b=0; b<5; b++) { // totally have how many same cards
			for(int c=4; c>b; c--) {
				if(same[b] == same[c]) 
					count++;
			}	
		}
		
		if (count == 6)
			return "Four of a kind";
		else if(count == 4)
				return "Full House";
		else if(count == 3)
				return "Three of kind";
		else if(count == 2)
				return "Two pairs";
		else 
				return "Nothing";
	}
	
	public String checkWin() { // check who wins
		if(type1.equals(type2))
			return "Peace";
		else if(type2.equals("Joker"))
			return "You win!!";
		else if(type1.equals("Joker"))
			return "You lose!!";
		else if(type2.equals("Four of a kind"))
			return "You win!!";
		else if(type1.equals("Four of a kind"))
			return "You lose!!";
		else if(type2.equals("Full House"))
			return "You win!!";
		else if(type1.equals("Full House"))
			return "You lose!!";
		else if(type2.equals("Three of kind"))
			return "You win!!";
		else if(type1.equals("Three of kind"))
			return "You lose!!";
		else if(type2.equals("Two pairs"))
			return "You win!!";
		else
			return "You lose!!";
	}
	
	public ImageIcon[]  picture() { // get array cards
		return cards;
	}
	
	public int[] randomSol() { // get array random
		return random;
	}
	
	public String getType1() { // get type1
		return type1;
	}
	
	public String getType2() { // get type2
		return type2;
	}
	
}
