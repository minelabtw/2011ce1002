package a7.s100502003;

public class Password {
	private String password;
	public Password() { // constructor
		
	}
	public Password(String in) { // set
		setPassword(in);
	}
	public void setPassword(String input) { // initialize
		password = input;
	}
	public String getPassword() { // get
		return password;
	}
	
	public void Exceptions() throws InvalidPasswordException{ // throw the exception and pass it to InvalidPasswordException
		if(getPassword().length() < 7) // length is not enough
			throw new InvalidPasswordException("The length of the password "+getPassword()+" is not enough!!");
		else if(checknum() == false) // no numbers
			throw new InvalidPasswordException("The password " + getPassword()+ " has no number!!");
		else if(checkletter() == false) // no letters
			throw new InvalidPasswordException("The password " + getPassword()+ " has no letters!!");
	}
	
	public boolean checknum() { // check if there is any numbers
		boolean check1 = false;
		for(int a=0; a<getPassword().length(); a++) {
			if((char)getPassword().charAt(a) <= 57 && (char)getPassword().charAt(a) >= 48) // ascii code of 0~9
				check1 = true; // true means there have number
		}
		return check1;
	}
	
	public boolean checkletter() {
		boolean check2 = false;
		for(int b=0; b<getPassword().length(); b++) {
			if((char)getPassword().charAt(b)>=65 && (char)getPassword().charAt(b) <=90) // ascii code of a~z
				check2 = true; // there really have letter
		}
		for(int c=0; c<getPassword().length(); c++) {
			if((char)getPassword().charAt(c)>=97 && (char)getPassword().charAt(c) <=122) // ascii code of A~Z
				check2 = true; // there really have letter
		}
		return check2;
	}
}
