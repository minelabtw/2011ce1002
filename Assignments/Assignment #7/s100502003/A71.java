package a7.s100502003;

import javax.swing.JFrame;

public class A71 {
	public static void main(String[] args) { // show the result with GUI
		FrameWork f = new FrameWork();
		f.setTitle("Assignment #7");
		f.setSize(800,600);
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
