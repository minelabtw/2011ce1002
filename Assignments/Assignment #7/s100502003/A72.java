package a7.s100502003;

import java.util.*;

public class A72 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Password passwords = new Password();
		boolean stop = false;
		while(stop != true) {
			System.out.println("Please input a password to varify: ");
			String Input = input.next();
			try{ // see that if there has any exception
				passwords.setPassword(Input); // set
				passwords.Exceptions(); // method that throw exceptions
				System.out.println("The password " + passwords.getPassword() + " is valid!!");
				stop = true;
			}
			catch(InvalidPasswordException ex) { // catch the string
				System.out.println(ex.PrintException()); // print the string
				System.out.println("The password " + passwords.getPassword() + " is not correct!!!\n");
			}
		}
	}
}
