package e8.s100502022;

import javax.swing.*;
import java.util.*;
import java.awt.*;

public class E8 {
	public static void main(String[] args) {
		//new a object p1 of panel
		JPanel p1 = new JPanel();

		p1.setLayout(new GridLayout(8, 8));//set 8*8
		JFrame frame = new JFrame("E8");
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if ((i + j) % 2 == 0) {
					JButton black = new JButton("");
					black.setBackground(Color.BLACK);//let the panel 's color isBlack
					p1.add(black);//add black buttom to panel
				} else {
					JButton write = new JButton("");
					write.setBackground(Color.WHITE);//let the panel 's color isWhite
					p1.add(write);// add white buttom to panel
				}
			}
		}

		//set the size
		frame.setSize(800, 800);
		frame.add(p1);//add p1 into frame
		frame.setVisible(true);
	}

}
