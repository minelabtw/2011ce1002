package a7.s100502022;
import java.util.*;
public class A72 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		while (true) {
			System.out.println("Please input a password to varify:");
			String password_input = input.next();//input password
			Password password = new Password(password_input);

			try {
				password.resultOfpassword();
				System.out.println("The password " + password.getPassword()
						+ " is valid!!\n");//show message
			} catch (InvalidPasswordException example) {
				System.err.println(password.getErrorMessage());
				System.err.println("The password " + example.getPassword()
						+ " in not correct!!\n");//show error message
			}
		}
	}
}
