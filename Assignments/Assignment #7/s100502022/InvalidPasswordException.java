package a7.s100502022;

public class InvalidPasswordException extends Exception {

	private String password;

	public InvalidPasswordException(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}
}
