package a7.s100502022;

public class Password {
	private String password;
	private String Message;
	
	public Password(String password) {
		this.password = password;
	}

	public static boolean letterFound(String s){
		String [] array = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "b", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
		int i;
		boolean a=false;
		for (i=0; i<array.length; i++){
			if (s.equals(array[i]) || s.equals(array[i].toUpperCase())){
				a=true;
				break;
			}
		}
		return a;
	}

	public static boolean numericFound(String s){
		String [] array = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
		boolean a=false;
		int i;
		for (i=0; i<array.length; i++){
			if (s.equals(array[i])){
				a=true;
			break;
			}
		}
		return a;
	}
	public String getPassword() {
		return password;
	}

	public void resultOfpassword() throws InvalidPasswordException {
		if (password.length() < 7) {
			Message = "The length of the password " + password
					+ " is not enough!!";
			
		} else if (letterFound(password)==false) {
			Message = "The password " + password + " has no letter!!";
			
		} else if (numericFound(password)==false) {
			Message = "The password " + password + " has no number!!";
			
		}
	}

	public String getErrorMessage() {
		return Message;
	}

}
