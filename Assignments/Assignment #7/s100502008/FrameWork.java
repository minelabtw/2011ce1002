package a7.s100502008;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.border.*;

public class FrameWork extends JFrame implements ActionListener {
	int[] num = new int[10];// declare valuables
	String result1 = "None", result2 = "None", allresult = "None";
	JButton button = new JButton("random");
	User user = new User();
	Border lineBorder = new LineBorder(Color.black, 2);
	Font font1 = new Font("SansSerif", Font.BOLD, 16);
	Font font2 = new Font("SansSerif", Font.BOLD, 50);
	JLabel name1 = new JLabel("Com");
	JLabel name2 = new JLabel("You");
	JLabel s1 = new JLabel(result1);
	JLabel s2 = new JLabel(result2);
	JLabel result = new JLabel(allresult);
	JLabel[] l = new JLabel[10];

	public FrameWork() {// constructor set Forum
		name1.setFont(font1);
		name2.setFont(font1);
		s1.setFont(font1);
		s2.setFont(font1);
		result.setFont(font2);
		button.addActionListener(this);
		JPanel p1 = new JPanel(new GridLayout(2, 6, 5, 5));// card
		p1.add(name1);
		for (int i = 0; i < 5; i++) {
			l[i] = new JLabel(user.init[i]);
			l[i].setBorder(lineBorder);
			p1.add(l[i]);
		}
		p1.add(s1);
		p1.add(name2);
		for (int i = 0; i < 5; i++) {
			l[i + 5] = new JLabel(user.init[i]);
			l[i + 5].setBorder(lineBorder);
			p1.add(l[i + 5]);
		}
		p1.add(s2);
		JPanel p2 = new JPanel(new BorderLayout(5, 5));// button
		p2.add(button, BorderLayout.CENTER);
		p2.add(result, BorderLayout.EAST);
		JPanel p3 = new JPanel(new BorderLayout(5, 5));// all
		p3.add(p1, BorderLayout.CENTER);
		p3.add(p2, BorderLayout.SOUTH);
		add(p3);

	}

	public void actionPerformed(ActionEvent e) {// event
		if (e.getSource() == button) {
			random();
			for (int i = 0; i < 10; i++) {// set card
				l[i].setIcon(user.card[num[i]]);
				l[i].setBorder(lineBorder);
			}
			if (Check(0) == 1) {// check card result
				s1.setText("Joker");
			} else if (Check(0) == 2) {
				s1.setText("Four of a kind");
			} else if (Check(0) == 3) {
				s1.setText("Full House");
			} else if (Check(0) == 4) {
				s1.setText("Three of  a kind");
			} else if (Check(0) == 5) {
				s1.setText("Two pairs");
			} else {
				s1.setText("Nothing");
			}
			if (Check(5) == 1) {// check card result
				s2.setText("Joker");
			} else if (Check(5) == 2) {
				s2.setText("Four of a kind");
			} else if (Check(5) == 3) {
				s2.setText("Full House");
			} else if (Check(5) == 4) {
				s2.setText("Three of a kind");
			} else if (Check(5) == 5) {
				s2.setText("Two pairs");
			} else {
				s2.setText("Nothing");
			}
			if (Check(0) < Check(5)) {// check result
				result.setText("You Lose");
			} else if (Check(0) == Check(5)) {
				result.setText("Draw");
			} else {
				result.setText("You Win");
			}
		}
	}

	public void random() {// random 10 numbers
		boolean check = true;
		int temp;
		for (int i = 0; i < 10; i++) {
			check = true;
			temp = (int) (Math.random() * 21);// get random number
			for (int j = 0; j < i; j++) {// get same number
				if (temp == num[j]) {
					check = false;
				}
			}
			if (check) {
				num[i] = temp;
			} else {// do again
				--i;
			}
		}
	}

	public int Check(int n) {// check card result
		int acount = 0, bcount = 0, jcount = 0, qcount = 0, kcount = 0, jokercount = 0;
		for (int i = n; i < n + 5; i++) {
			if (num[i] == 0 || num[i] == 1 || num[i] == 2 || num[i] == 3) {// a
				acount++;
			} else if (num[i] == 4 || num[i] == 5 || num[i] == 6 || num[i] == 7) {// 2
				bcount++;
			} else if (num[i] == 8 || num[i] == 9 || num[i] == 10// j
					|| num[i] == 11) {
				jcount++;
			} else if (num[i] == 20) {// joker
				jokercount++;
			} else if (num[i] == 13 || num[i] == 14 || num[i] == 15// q
					|| num[i] == 12) {
				qcount++;
			} else if (num[i] == 17 || num[i] == 18 || num[i] == 19// k
					|| num[i] == 16) {
				kcount++;
			}
		}
		if (jokercount == 1) {// joker
			return 1;
		} else if (acount == 4 || bcount == 4 || jcount == 4 || qcount == 4// Four
																			// of
																			// a
																			// kind
				|| kcount == 4) {
			return 2;
		} else if (acount == 3 || bcount == 3 || jcount == 3 || qcount == 3
				|| kcount == 3) {
			if (acount == 2 || bcount == 2 || jcount == 2 || qcount == 2// Full
																		// House
					|| kcount == 2) {
				return 3;
			} else {// Three of a kind
				return 4;
			}
		} else if (acount == 2 || bcount == 2 || jcount == 2 || qcount == 2// Two
																			// Pairs
				|| kcount == 2) {
			int i=6;
			if (acount == 2) {
				if (bcount == 2 || jcount == 2 || qcount == 2 || kcount == 2) {
					i=5;
				}		
			} else if (bcount == 2) {
				if (acount == 2 || jcount == 2 || qcount == 2 || kcount == 2) {
					i=5;
				}
			} else if (jcount == 2) {
				if (acount == 2 || bcount == 2 || qcount == 2 || kcount == 2) {
					i=5;
				}
			} else if (qcount == 2) {
				if (acount == 2 || jcount == 2 || bcount == 2 || kcount == 2) {
					i=5;
				}
			} else if (kcount == 2) {
				if (acount == 2 || jcount == 2 || qcount == 2 || bcount == 2) {
					i=5;
				}
			}
			return i;
		} else {// Nothing
			return 6;
		}
	}
}
