package a7.s100502008;

public class Password {
	private String passward;
	public Password(String inpassward) throws InvalidPasswordException//constructor
	{
		passward=inpassward;
		boolean result=true;
		int count=0;
		int situatnum=0;;
		for(int i=0;i<passward.length();i++)//check letter and number
		{
			if(Character.isLetter(passward.charAt(i)))
					{
						count++;
					}
		}
		if(passward.length()<7)//check length
		{
			result=false;
			situatnum=1;
		}
		else if(count==passward.length())//no number
		{
			result=false;
			situatnum=2;
		}
		else if(count==0)//no letter
		{
			result=false;
			situatnum=3;
		}
		if(!result)//throw to invalid
		{
			throw new InvalidPasswordException(passward,situatnum);
		}
	}
	public String getPassward()
	{
		return passward;
	}
}
