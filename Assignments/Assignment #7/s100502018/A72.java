package a7.s100502018;

import java.util.Scanner;

public class A72 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Please input a password to varify: ");
		String inputpw = input.nextLine(); //get the input
		try {
			Password myPassword = new Password(inputpw);
			System.out.println("The password " + myPassword.getPassword()
					+ " is valid!!!");
		} catch (InvalidPasswordException ex) {
			System.out.println(ex.Result());
		}
	}
}
