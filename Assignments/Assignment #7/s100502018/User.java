package a7.s100502018;

import javax.swing.*;
import java.awt.*;

public class User {
	public ImageIcon[] card = new ImageIcon[21];
	public ImageIcon[] init = new ImageIcon[5];
	public int comrank = 0;
	public int plarank = 0;

	public User() { //constructor to get the image
		for (int i = 0; i < 21; i++) {
			card[i] = new ImageIcon("image/card/" + i + ".png");
		}
		for (int i = 0; i < 5; i++) {
			init[i] = new ImageIcon("image/card/init_" + (i+1) + ".png");
		}
	}

	public String Rank(int[] array) { //a method to check the rank
		int acetimes = 0;
		int twotimes = 0;
		int eletimes = 0;
		int twetimes = 0;
		int thitimes = 0;
		int poktimes = 0;
		for (int i = 0; i < 5; i++) { //check the card
			if (array[i] >= 0 && array[i] <= 3) {
				acetimes++;
			} else if (array[i] >= 4 && array[i] <= 7) {
				twotimes++;
			} else if (array[i] >= 8 && array[i] <= 11) {
				eletimes++;
			} else if (array[i] >= 12 && array[i] <= 15) {
				twetimes++;
			} else if (array[i] >= 16 && array[i] <= 19) {
				thitimes++;
			} else {
				poktimes++;
			}
		}
		if (poktimes > 0) { //Poker
			return "Poker";
		} else if (acetimes == 4 || twotimes == 4 || eletimes == 4 //Four of a kind
				|| twetimes == 4 || thitimes == 4) {
			return "Four of a kind";
		} else if ((acetimes == 3 && twotimes == 2) //Full house
				|| (acetimes == 3 && eletimes == 2)
				|| (acetimes == 3 && twetimes == 2)
				|| (acetimes == 3 && thitimes == 2)
				|| (twotimes == 3 && acetimes == 2)
				|| (twotimes == 3 && eletimes == 2)
				|| (twotimes == 3 && twetimes == 2)
				|| (twotimes == 3 && thitimes == 2)
				|| (eletimes == 3 && acetimes == 2)
				|| (eletimes == 3 && twotimes == 2)
				|| (eletimes == 3 && twetimes == 2)
				|| (eletimes == 3 && thitimes == 2)
				|| (twetimes == 3 && acetimes == 2)
				|| (twetimes == 3 && twotimes == 2)
				|| (twetimes == 3 && eletimes == 2)
				|| (twetimes == 3 && thitimes == 2)
				|| (thitimes == 3 && acetimes == 2)
				|| (thitimes == 3 && twotimes == 2)
				|| (thitimes == 3 && eletimes == 2)
				|| (thitimes == 3 && twetimes == 2))
			return "Full house";
		else if (acetimes == 3 || twotimes == 3 || eletimes == 3 //Three of a kind
				|| twetimes == 3 || thitimes == 3)
			return "Three of a kind";
		else if ((acetimes == 2 && twotimes == 2) //Two pairs
				|| (acetimes == 2 && eletimes == 2)
				|| (acetimes == 2 && twetimes == 2)
				|| (acetimes == 2 && thitimes == 2)
				|| (twotimes == 2 && acetimes == 2)
				|| (twotimes == 2 && eletimes == 2)
				|| (twotimes == 2 && twetimes == 2)
				|| (twotimes == 2 && thitimes == 2)
				|| (eletimes == 2 && acetimes == 2)
				|| (eletimes == 2 && twotimes == 2)
				|| (eletimes == 2 && twetimes == 2)
				|| (eletimes == 2 && thitimes == 2)
				|| (twetimes == 2 && acetimes == 2)
				|| (twetimes == 2 && twotimes == 2)
				|| (twetimes == 2 && eletimes == 2)
				|| (twetimes == 2 && thitimes == 2)
				|| (thitimes == 2 && acetimes == 2)
				|| (thitimes == 2 && twotimes == 2)
				|| (thitimes == 2 && eletimes == 2)
				|| (thitimes == 2 && twetimes == 2))
			return "Two pairs";
		else //Nothing
			return "Nothing";
	}

	public String Judge(String x, String y) { //tell which is bigger
		int comrank = 0;
		int plarank = 0;
		if (x == "Poker") {
			comrank = 6;
		} else if (x == "Four of a kind") {
			comrank = 5;
		} else if (x == "Full house") {
			comrank = 4;
		} else if (x == "Three of a kind") {
			comrank = 3;
		} else if (x == "Two pairs") {
			comrank = 2;
		} else {
			comrank = 1;
		}
		if (y == "Poker") {
			plarank = 6;
		} else if (y == "Four of a kind") {
			plarank = 5;
		} else if (y == "Full house") {
			plarank = 4;
		} else if (y == "Three of a kind") {
			plarank = 3;
		} else if (y == "Two pairs") {
			plarank = 2;
		} else {
			plarank = 1;
		}
		if (comrank > plarank) {
			return "You lose";
		} else if (comrank == plarank) {
			return "Draw";
		} else {
			return "You win";
		}
	}
}
