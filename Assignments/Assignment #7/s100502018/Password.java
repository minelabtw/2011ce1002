package a7.s100502018;

public class Password {
	private String password;

	public Password(String x) throws InvalidPasswordException { //check the password
		this.password = x;
		boolean result = true;
		int condition = 0;
		int LetterQuantity = 0;
		for (int i = 0; i < password.length(); i++) {
			if (Character.isLetter(password.charAt(i))) {
				LetterQuantity++;
			}
		}
		if (password.length() < 7) { //get the number of condition
			condition = 1;
		} else if (LetterQuantity == password.length()) {
			condition = 2;
		} else if (LetterQuantity == 0) {
			condition = 3;
		}
		if (condition != 0) {
			result = false;
		}
		if (!result) {
			throw new InvalidPasswordException(password, condition);
		}
	}

	public String getPassword() {
		return password;
	}
}
