package a7.s100502018;

import javax.swing.JFrame;

public class A71 {
	public static void main(String[] args) {
		FrameWork myFrameWork = new FrameWork(); //show the GUI
		myFrameWork.setTitle("Assignment #7");
		myFrameWork.setSize(1000, 550);
		myFrameWork.setLocationRelativeTo(null);
		myFrameWork.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		myFrameWork.setVisible(true);
	}
}