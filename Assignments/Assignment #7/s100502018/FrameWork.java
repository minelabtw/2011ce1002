package a7.s100502018;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.border.*;

public class FrameWork extends JFrame implements ActionListener {
	JLabel name1 = new JLabel("Computer");
	JLabel hand1 = new JLabel("");
	JLabel name2 = new JLabel("Player");
	JLabel hand2 = new JLabel("");
	JButton randombutton = new JButton("Random");
	JLabel result = new JLabel("");
	Font part1font = new Font("TimesRoman", Font.BOLD, 15);
	Font part2font = new Font("TimesRoman", Font.BOLD, 45);
	Border cardborder = new LineBorder(Color.BLACK, 1);
	Border partborder = new LineBorder(Color.BLACK, 2);
	User myUser = new User();
	JLabel[] show = new JLabel[10];
	int[] cardnum = new int[10];
	int[] comnum = new int[5];
	int[] planum = new int[5];

	public FrameWork() { //the GUI structure
		name1.setFont(part1font);
		hand1.setFont(part1font);
		name2.setFont(part1font);
		hand2.setFont(part1font);
		result.setFont(part2font);
		JPanel part1 = new JPanel(new GridLayout(2, 7, 5, 5));
		part1.add(name1);
		for (int i = 0; i < 5; i++) {
			show[i] = new JLabel(myUser.init[i]);
			show[i].setBorder(cardborder);
			part1.add(show[i]);
		}
		part1.add(hand1);
		part1.add(name2);
		for (int i = 0; i < 5; i++) {
			show[i] = new JLabel(myUser.init[i]);
			show[i].setBorder(cardborder);
			part1.add(show[i]);
		}
		part1.add(hand2);
		part1.setBorder(partborder);

		JPanel part2 = new JPanel(new GridLayout(1, 2, 5, 5));
		part2.add(randombutton);
		randombutton.addActionListener(this);
		part2.add(result);

		JPanel total = new JPanel(new GridLayout(2, 1, 5, 5));
		total.add(part1);
		total.add(part2);
		add(total);
	}

	public void actionPerformed(ActionEvent e) { //after button pushed
		if (e.getSource() == randombutton) {
			Random();
			for (int i = 0; i < 5; i++) {
				show[i].setIcon(myUser.card[comnum[i]]);
				show[i].setBorder(cardborder);
			}
			for (int i = 0; i < 5; i++) {
				show[i].setIcon(myUser.card[planum[i]]);
				show[i].setBorder(cardborder);
			}
			hand1.setText(myUser.Rank(comnum));
			hand2.setText(myUser.Rank(planum));
			result.setText(myUser.Judge(myUser.Rank(comnum),
					myUser.Rank(planum)));
		}
	}

	public void Random() { //to get the random number of card (no repeat)
		boolean repeat = false;
		int temp;
		for (int i = 0; i < 10; i++) {
			repeat = false;
			temp = (int) (Math.random() * 20);
			for (int j = 0; j < i; j++) {
				if (temp == cardnum[j]) {
					repeat = true;
				}
			}
			if (!repeat) {
				cardnum[i] = temp;
			} else {
				--i;
			}
		}
		for (int i = 0; i < 5; i++) {
			comnum[i] = cardnum[i];
		}
		for (int i = 0; i < 5; i++) {
			planum[i] = cardnum[i + 5];
		}
	}
}
