package a7.s100502018;

public class InvalidPasswordException extends Exception {
	private String password;
	private int condition;

	public InvalidPasswordException(String x, int y) { //constructor to initial the value
		this.password = x;
		this.condition = y;
	}

	public String Result() { //show the condition 
		if (condition == 1) {
			return "The length of the password " + getPassword()
					+ " is not enough!!!\n" + Incorrect();
		} else if (condition == 2) {
			return "The password " + getPassword() + " has no number!!!\n"
					+ Incorrect();
		} else {
			return "The password " + getPassword() + " has no letter!!!\n"
					+ Incorrect();
		}
	}

	public String getPassword() { //get the password
		return this.password;
	}

	public String Incorrect() { //get the incorrect message
		return "The password " + getPassword() + " is not correct!!!";
	}
}
