package a7.s100502016;

public class InvalidPasswordException extends Exception {

	public InvalidPasswordException(String errorMessage, String password) {
		super(errorMessage + "\n" + "The password " + password
				+ " is not correct!!");
	}

}
