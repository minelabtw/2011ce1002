package a7.s100502016;

public class Password {
	private String password;

	public Password(String newPassword) throws InvalidPasswordException {
		setPassword(newPassword);

	}

	public void setPassword(String newPassword) throws InvalidPasswordException {
		password = newPassword;
		if (newPassword.length() < 7)
			throw new InvalidPasswordException("The length of the password "
					+ password + " is not enough!!", password);
		else if (checkPassword(password) == 1)
			throw new InvalidPasswordException("The password " + password
					+ " has no Number!!", password);
		else if (checkPassword(password) == 2)
			throw new InvalidPasswordException("The password " + password
					+ " has no letter!!", password);
		else {
			System.out.println("The password " + password + " is valid!!");
		}
	}

	public int checkPassword(String newPassword) {
		int NoLetter = 0;
		int NoNumber = 0;
		for (int i = 0; i < newPassword.length(); i++) {
			if (Character.isLetter(newPassword.charAt(i)))
				NoNumber++;
			else
				NoLetter++;
		}
		if (NoNumber == newPassword.length())
			return 1;
		else if (NoLetter == newPassword.length())
			return 2;
		else
			return 3;
	}
}
