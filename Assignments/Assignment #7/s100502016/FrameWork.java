package a7.s100502016;

import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.TitledBorder;

public class FrameWork extends JFrame implements ActionListener {

	private ImageIcon Ace1Icon = new ImageIcon("image/card/0.png");
	private ImageIcon Ace2Icon = new ImageIcon("image/card/1.png");
	private ImageIcon Ace3Icon = new ImageIcon("image/card/2.png");
	private ImageIcon Ace4Icon = new ImageIcon("image/card/3.png");
	private ImageIcon Two1Icon = new ImageIcon("image/card/4.png");
	private ImageIcon Two2Icon = new ImageIcon("image/card/5.png");
	private ImageIcon Two3Icon = new ImageIcon("image/card/6.png");
	private ImageIcon Two4Icon = new ImageIcon("image/card/7.png");
	private ImageIcon J1Icon = new ImageIcon("image/card/8.png");
	private ImageIcon J2Icon = new ImageIcon("image/card/9.png");
	private ImageIcon J3Icon = new ImageIcon("image/card/10.png");
	private ImageIcon J4Icon = new ImageIcon("image/card/11.png");
	private ImageIcon Q1Icon = new ImageIcon("image/card/12.png");
	private ImageIcon Q2Icon = new ImageIcon("image/card/13.png");
	private ImageIcon Q3Icon = new ImageIcon("image/card/14.png");
	private ImageIcon Q4Icon = new ImageIcon("image/card/15.png");
	private ImageIcon K1Icon = new ImageIcon("image/card/16.png");
	private ImageIcon K2Icon = new ImageIcon("image/card/17.png");
	private ImageIcon K3Icon = new ImageIcon("image/card/18.png");
	private ImageIcon K4Icon = new ImageIcon("image/card/19.png");
	private ImageIcon JokerIcon = new ImageIcon("image/card/20.png");
	private ImageIcon init_1Icon = new ImageIcon("image/card/init_1.png");
	private ImageIcon init_2Icon = new ImageIcon("image/card/init_2.png");
	private ImageIcon init_3Icon = new ImageIcon("image/card/init_3.png");
	private ImageIcon init_4Icon = new ImageIcon("image/card/init_4.png");
	private ImageIcon init_5Icon = new ImageIcon("image/card/init_5.png");

	JButton button = new JButton("random"); // create button

	JPanel p1 = new JPanel(new GridLayout(2, 6, 10, 10));
	JPanel p2 = new JPanel(new GridLayout(2, 2, 10, 10));

	Font font32 = new Font("TimesRoman", Font.BOLD, 32);
	Font font20 = new Font("TimesRoman", Font.BOLD, 20);

	JLabel Ccard1 = new JLabel(init_1Icon);
	JLabel Ccard2 = new JLabel(init_2Icon);
	JLabel Ccard3 = new JLabel(init_3Icon);
	JLabel Ccard4 = new JLabel(init_4Icon);
	JLabel Ccard5 = new JLabel(init_5Icon);
	JLabel Pcard1 = new JLabel();
	JLabel Pcard2 = new JLabel();
	JLabel Pcard3 = new JLabel();
	JLabel Pcard4 = new JLabel();
	JLabel Pcard5 = new JLabel();
	JLabel Message = new JLabel("Game Result");
	JLabel CType = new JLabel("Computer's Type");
	JLabel PType = new JLabel("Player's Type");

	public FrameWork() {

		p1.setBorder(new TitledBorder("Card"));
		p1.add(new JLabel("Computer"));
		p1.add(Ccard1);
		p1.add(Ccard2);
		p1.add(Ccard3);
		p1.add(Ccard4);
		p1.add(Ccard5);
		p1.add(CType);
		p1.add(new JLabel("Player"));
		p1.add(Pcard1);
		p1.add(Pcard2);
		p1.add(Pcard3);
		p1.add(Pcard4);
		p1.add(Pcard5);
		p1.add(PType);
		Message.setFont(font32);
		button.setFont(font20);
		p2.add(Message);
		p2.add(button);

		button.addActionListener(this); // active button event here
		setLayout(new GridLayout(2, 2));
		add(p1);
		add(p2);

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == button) {
			User user1 = new User();
			User user2 = new User();
			user1.setCardInHand();
			user1.setCardType();
			user2.setCardInHand();
			user2.setCardType();

			if (user2.getCardTypeLevel() > user1.getCardTypeLevel())
				Message.setText("You WIN!");
			else if (user2.getCardTypeLevel() < user1.getCardTypeLevel())
				Message.setText("You LOST!");
			else
				Message.setText("DRAW");
			CType.setText(user1.getCardType());
			PType.setText(user2.getCardType());

			for (int i = 0; i < 5; i++)
				System.out.print(user1.getCardInHand(i) + " ");
			System.out.println(user1.getCardType());

			DisplayCard(user1.getCardInHand(0), Ccard1);
			DisplayCard(user1.getCardInHand(1), Ccard2);
			DisplayCard(user1.getCardInHand(2), Ccard3);
			DisplayCard(user1.getCardInHand(3), Ccard4);
			DisplayCard(user1.getCardInHand(4), Ccard5);
			DisplayCard(user2.getCardInHand(0), Pcard1);
			DisplayCard(user2.getCardInHand(1), Pcard2);
			DisplayCard(user2.getCardInHand(2), Pcard3);
			DisplayCard(user2.getCardInHand(3), Pcard4);
			DisplayCard(user2.getCardInHand(4), Pcard5);
		}
	}

	private void DisplayCard(int card, JLabel label) {
		switch (card) {
		case 0:
			label.setIcon(Ace1Icon);
			break;
		case 1:
			label.setIcon(Ace2Icon);
			break;
		case 2:
			label.setIcon(Ace3Icon);
			break;
		case 3:
			label.setIcon(Ace4Icon);
			break;
		case 4:
			label.setIcon(Two1Icon);
			break;
		case 5:
			label.setIcon(Two2Icon);
			break;
		case 6:
			label.setIcon(Two3Icon);
			break;
		case 7:
			label.setIcon(Two4Icon);
			break;
		case 8:
			label.setIcon(J1Icon);
			break;
		case 9:
			label.setIcon(J2Icon);
			break;
		case 10:
			label.setIcon(J3Icon);
			break;
		case 11:
			label.setIcon(J4Icon);
			break;
		case 12:
			label.setIcon(Q1Icon);
			break;
		case 13:
			label.setIcon(Q2Icon);
			break;
		case 14:
			label.setIcon(Q3Icon);
			break;
		case 15:
			label.setIcon(Q4Icon);
			break;
		case 16:
			label.setIcon(K1Icon);
			break;
		case 17:
			label.setIcon(K2Icon);
			break;
		case 18:
			label.setIcon(K3Icon);
			break;
		case 19:
			label.setIcon(K4Icon);
			break;
		case 20:
			label.setIcon(JokerIcon);
			break;
		}
	}

}