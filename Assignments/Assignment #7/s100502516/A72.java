package a7.s100502516;

import java.util.*;

public class A72 {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		boolean end = false;	//loop end
		
		while(!end)
		{
			try 
			{
				System.out.print("Please input a password to varify: ");
				Password password = new Password(input.next());
				System.out.print("The password " + password.getPassword() + " is valid!!\n\n");	//if valid
			} 
			catch (InvalidPasswordException e) 
			{				
				e.ShowExceptionMessage();
				System.out.print("The password " + e.getPassword() + " is not correct!!\n\n");	//if invalid
			}
			
			System.out.print("Enter 1 to continue, enter 2 to exit: ");
			
			int selection = input.nextInt();
			
			if(selection == 2)
				end = true;			
		}
	}
}
