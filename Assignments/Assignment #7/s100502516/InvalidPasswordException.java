package a7.s100502516;

public class InvalidPasswordException extends Exception {	
	private String ExceptionType;
	private String password;
	private String caseOne = "lengthException";	//symbolize exception type
	private String caseTwo = "noLetterException";
	private String caseThree = "noNumberException";
	
	public InvalidPasswordException(String password, String ExceptionType)
	{
		this.ExceptionType = ExceptionType;
		this.password = password;
	}	
	
	public void ShowExceptionMessage()	//show correspond message 
	{
		if(ExceptionType.equals(caseOne))
			System.out.println("The length of the password " + password + " is not enough!!");
		else if(ExceptionType.equals(caseTwo))
			System.out.println("The password " + password + " has no letter!!");
		else if(ExceptionType.equals(caseThree))
			System.out.println("The password " + password + " has no number!!");
	}
	
	public String getPassword()
	{
		return password;
	}
}
