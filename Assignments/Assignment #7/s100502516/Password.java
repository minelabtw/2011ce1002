package a7.s100502516;

public class Password {
	private String password;
	private String ExceptionType;
	
	public Password(String password) throws InvalidPasswordException
	{		
		this.password = password;		
		
		if(CheckException())
			throw new InvalidPasswordException(password, ExceptionType);
	}
	
	public boolean CheckException()
	{
		boolean noNumberException = true;
		boolean noLetterException = true;		
		
		for(int i = 0; i < password.length(); i++)	//check is it no letter or number
		{			
			if(Character.isLetter(password.charAt(i)))
				noLetterException = false;
			else
				noNumberException = false;
		}
		
		if(password.length() < 7)	//store the type
		{
			ExceptionType = "lengthException";	
			return true;
		}
		else if(noLetterException)
		{
			ExceptionType = "noLetterException";
			return true;
		}
		else if(noNumberException)
		{
			ExceptionType = "noNumberException";
			return true;
		}
		else 
			return false;
	}
	
	public String getPassword()
	{
		return password;
	}
}
