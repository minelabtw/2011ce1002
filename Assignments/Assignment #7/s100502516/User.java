package a7.s100502516;

import java.util.*;

public class User {
	private interface TYPES	//set the card type interface
	{
		int NOTHING = 0;
		int TWO_PAIRS = 1;
		int THREE_OF_KIND = 2;
		int FULL_HOUSE = 3;
		int FOUR_OF_KIND = 4;
		int JOKER = 5;
	}
	private interface NUMS	//set the card numbers interface
	{
		int A = 0;
		int TWO = 1;
		int J = 2;
		int Q = 3;
		int K = 4;
		int PON = 5;
	}	
	private int[] card = new int[5];	//store current cards
	private static Random ran = new Random();
	private static ArrayList<Integer> ar = new ArrayList<Integer>();
	private static int randomSize = 21;
	private String type;	//store type
	
	public User()
	{		
		if(randomSize == 21)	//Licensing
		{
			for(int i = 0; i < randomSize; i++)
				ar.add(i);
		}
		
		for(int i = 0; i < 5; i++)	//Licensing
		{
			card[i] = ar.remove(ran.nextInt(randomSize)).intValue();
			randomSize--;
			
			if(randomSize == 11)
			{
				ar.clear();
				randomSize = 21;
			}
		}
		
		switch(checkType())	//store type
		{
			case TYPES.JOKER:
				type = "Joker";
				break;
				
			case TYPES.FOUR_OF_KIND:
				type = "Four of kind";
				break;
				
			case TYPES.FULL_HOUSE:
				type = "Full House";
				break;
				
			case TYPES.THREE_OF_KIND:
				type = "Three of kind";
				break;
				
			case TYPES.TWO_PAIRS:
				type = "Two Pairs";
				break;
				
			case TYPES.NOTHING:
				type = "Nothing";
				break;
		}
	}	
	public int checkType()	//check priority 
	{		
		int[] nums = new int[6];	//store card quantity
		boolean twoF = false;	//same card boolean
		boolean twoS = false;
		boolean three = false;
		boolean four = false;				
		
		for(int i = 0; i < 5; i++)
		{
			switch(card[i])
			{
				case 0:
				case 1:
				case 2:
				case 3:
					nums[NUMS.A]++;
					break;
					
				case 4:
				case 5:
				case 6:
				case 7:
					nums[NUMS.TWO]++;
					break;
					
				case 8:
				case 9:
				case 10:
				case 11:
					nums[NUMS.J]++;
					break;
					
				case 12:
				case 13:
				case 14:
				case 15:
					nums[NUMS.Q]++;
					break;
					
				case 16:
				case 17:
				case 18:
				case 19:
					nums[NUMS.K]++;
					break;
					
				case 20:
					nums[NUMS.PON]++;
					break;
			}
		}
		
		if(nums[NUMS.PON] == 1)
			return TYPES.JOKER;
		else
		{
			for(int i = 0; i < 5; i++)
			{
				if(nums[i] == 4)
					four = true;
				else if(nums[i] == 3)
					three = true;
				else if(nums[i] == 2 && twoF)
					twoS = true;
				else if(nums[i] == 2)
					twoF = true;
			}
			
			if(four)
				return TYPES.FOUR_OF_KIND;
			else if(three && twoF)
				return TYPES.FULL_HOUSE;
			else if(three)
				return TYPES.THREE_OF_KIND;
			else if(twoS)
				return TYPES.TWO_PAIRS;
			else
				return TYPES.NOTHING;
		}
	}
	public String getType()
	{
		return type;
	}
	public int getCard(int order)
	{
		return card[order];
	}
}
