package a7.s100502516;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener {	
	private ImageIcon[] card = new ImageIcon[21];	//store card image	
	private Font fontB18 = new Font("Serif", Font.BOLD, 18);
	private Font fontB24 = new Font("Serif", Font.BOLD, 24);
	private Border lineBorder = new LineBorder(Color.BLACK, 2);
	private JButton jbtRandom = new JButton("Random");
	private JLabel jlblType1 = new JLabel("<Type>");
	private JLabel jlblType2 = new JLabel("<Type>");
	private JLabel jlblResult = new JLabel("Result");
	private JLabel[] jlbCard = new JLabel[10];	//current card label
	
	public FrameWork()
	{
		for(int i = 0; i < 21; i++)	//store image
			card[i] = new ImageIcon("image/card/" + i + ".png");
		
		JPanel p1 = new JPanel(new GridLayout(1, 6, 2, 2));				
		
		JLabel jlblEnemy = new JLabel("Enemy");
		jlblEnemy.setFont(fontB18);				
		
		p1.add(jlblEnemy);
		
		for(int i = 0; i < 5; i++)	//initialize image
		{
			jlbCard[i] = new JLabel(new ImageIcon("image/card/init_" + (i + 1) + ".png"));
			jlbCard[i].setBorder(lineBorder);			
			p1.add(jlbCard[i]);
		}
		
		JPanel p2 = new JPanel(new GridLayout(1, 6, 2, 2));
		
		JLabel jlblYou = new JLabel("You");	
		jlblYou.setFont(fontB18);
		
		p2.add(jlblYou);		
		
		for(int i = 5; i < 10; i++)	//initialize image
		{
			jlbCard[i] = new JLabel(new ImageIcon("image/card/init_" + (i - 4) + ".png"));
			jlbCard[i].setBorder(lineBorder);			
			p2.add(jlbCard[i]);
		}		
		
		jbtRandom.setFont(fontB24);	
		jbtRandom.addActionListener(this);	//add button action
		
		JPanel p3 = new JPanel(new GridLayout(3, 1, 4, 4));
		
		p3.add(p1);		
		p3.add(p2);
		p3.add(jbtRandom);
		
		JPanel p4 = new JPanel(new GridLayout(3, 1));
				
		jlblType1.setFont(fontB18);		
		jlblType2.setFont(fontB18);		
		jlblResult.setFont(fontB24);			
		
		p4.add(jlblType1);
		p4.add(jlblType2);
		p4.add(jlblResult);
		
		setLayout(new BorderLayout(20, 20));		
		
		add(p3, BorderLayout.WEST);
		add(p4, BorderLayout.CENTER);		
	}	
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == jbtRandom)
		{
			User enemy = new User();
			User you = new User();			
			
			jlblType1.setText("<" + enemy.getType() + ">");			
			jlblType2.setText("<" + you.getType() + ">");			
						
			for(int i = 0; i < 10; i++)	//update images
			{
				if(i < 5)
					jlbCard[i].setIcon(card[enemy.getCard(i)]);
				else
					jlbCard[i].setIcon(card[you.getCard(i - 5)]);
			}
			
			if(you.checkType() > enemy.checkType())	//update result
				jlblResult.setText("You Win");
			else if(you.checkType() == enemy.checkType())
				jlblResult.setText("Draw");
			else
				jlblResult.setText("You Lose");				
		}
	}	
}
