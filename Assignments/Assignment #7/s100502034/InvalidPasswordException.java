package a7.s100502034;

public class InvalidPasswordException {
	public InvalidPasswordException(String a) {
		char[] aP = new char[a.length()];//用aP把使用者輸入的String切割裝好
		int intCheck =0;//數字檢查
		int letterCheck = 0;//字母檢查
		for (int i = 0 ; i<a.length();i++){//切割步驟
			aP[i] = a.charAt(i);
			int ascii = a.charAt(i);//用萬國碼判斷是否數字
			if (Character.isLetter(aP[i])==true){//用isLetter判斷是否字母
				letterCheck++;
			}
			else if(ascii>=48 && ascii<=57){//還要查表呢
				intCheck++;
			}
		}
		if (a.length() < 7){//判斷長度是否足夠
			System.out.println("The length of the passwordis not enough!!");
		}
		else if (intCheck == 0){//判斷有沒有數字
			System.out.println("The password "+ a +" has no number!!");
		}
		else if (letterCheck ==0){//判斷有沒有字母
			System.out.println("The password "+ a +" has no letter!!");
		}
	}

}
