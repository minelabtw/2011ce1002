package a7.s100502034;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.*;
import javax.swing.*;

import a7.s100502034.User;
public class FrameWork extends JFrame implements ActionListener{//這邊是排版
	JButton button = new JButton("random");
	JPanel p2 = new JPanel();//我一共用了三個JPanel來排版
	JPanel p3 = new JPanel();//p3是random和結果(win or lose)
	JPanel p4 = new JPanel();//p4是總版
	public FrameWork (){
		p2.setLayout(new GridLayout(2,7));
		p3.setLayout(new GridLayout(1,2));
		p4.setLayout(new GridLayout(2,1));
		p2.add(new JLabel("He"),BorderLayout.WEST);
		for ( int i =0;i<5;i++){
			p2.add(new JLabel(new ImageIcon("image/card/init_2.png")));//一大堆起始圖片
		}
		p2.add(new JLabel(new ImageIcon("image/card/init_1.png")));
		p2.add(new JLabel("You"),BorderLayout.WEST);
		for ( int i =0;i<5;i++){
			p2.add(new JLabel(new ImageIcon("image/card/init_2.png")));//一大堆起始圖片
		}
		p2.add(new JLabel(new ImageIcon("image/card/init_1.png")));
		p4.add(p2);
		p3.add(button);
		p3.add(new JLabel(new ImageIcon("image/card/init_5.png")));
		p4.add(p3);//用總版把p2 p3合起來
		add(p4);//然後把p4加到Frame
		button.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == button){
			p4.removeAll();//首先洗版 再add
			p3.removeAll();
			p2.removeAll();
			p2.add(new JLabel("He"),BorderLayout.WEST);//再一個一個把東西add進來
			User u = new User();//先宣告一個User
			ImageIcon[] cards = new ImageIcon[5];
			cards = u.getC();//getC是發好的卡牌圖片
			for (int i = 0;i<5;i++){//一張一張的卡牌掉進來
				p2.add(new JLabel(cards[i]));
			}
			kind(User.getResult());//kind 是一個辨別何種結果的method
			p2.add(new JLabel("You"),BorderLayout.WEST);//然後到p2 (也就是使用者you
			for (int i = 5;i<10;i++){
				p2.add(new JLabel(cards[i]));
			}
			kind(User.getResult2());
			p4.add(p2);//重新用p4把p2拉進來
			p3.add(button);
			if (User.getResult()>User.getResult2()){//最後判斷結果
				p3.add(new JLabel("YOU LOSE"));
			}
			else if (User.getResult() == User.getResult2()){
				p3.add(new JLabel("TIE"));
			}
			else{
				p3.add(new JLabel("YOU WIN"));
			}
			p4.add(p3);
			p4.validate();
		}
	}
	private void kind(int a){
		switch(a){//根據丟進來的參數判斷是何種結果
		case 1:
			p2.add(new JLabel("Nothing"));
			break;
		case 2:
			p2.add(new JLabel("Two pairs"));
			break;
		case 3:
			p2.add(new JLabel("Three of kind"));
			break;
		case 4:
			p2.add(new JLabel("Full House"));
			break;
		case 5:
			p2.add(new JLabel("Four of a kind"));
			break;
		case 6:
			p2.add(new JLabel("Joker"));
			break;
		}
	}
}