package a7.s100502034;
import a7.s100502034.InvalidPasswordException;
public class Password {
	public static String password = new String("abc1234");
	private static int stopInt = 0; 
	public Password(String givenP){
		char[] charP = new char[givenP.length()];//用char來切割使用者輸入的String
		char[] correctP = new char[password.length()];//也把正確的password切割
		for (int i = 0;i<password.length();i++){//切割正確的password
			correctP[i] = password.charAt(i);
		}
		for (int i = 0; i<givenP.length();i++){//切割使用者輸入的password
			charP[i] = givenP.charAt(i);
		}
		InvalidPasswordException pp = new InvalidPasswordException(givenP);
		int check = 0;//檢查使用者輸入的密碼是否正確的參數
		if (charP.length == password.length()){//首先要判斷長度是否一致
			for (int i = 0 ;i < password.length();i++){//再一個一個字元比較
				if (charP[i] == correctP[i]){
					check++;
				}
			}
			if (check == password.length()){//如果check長度等於正確密碼的長度 (也就是每個字元的正確
				System.out.println("The password abc1234 is valid!!\n");//就是密碼正確
				stop();//報行stop動作
			}
			else{
				System.out.println("The password "+ givenP+ " in not correct!!\n");//不對的話就顯示錯誤
			}
		}
		else{
			System.out.println("The password "+ givenP+ " in not correct!!\n");//長度不一樣的話 直接顯示錯誤
		}
	}
	public void stop(){
		stopInt = 1;//將stopInt參數改成1  讓A72 的main可以知道密碼已經正確
	}
	public int stopCheck(){//給A72存錯stopInt的method
		return stopInt;
	}
}
