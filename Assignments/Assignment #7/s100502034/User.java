package a7.s100502034;
import javax.swing.*;

import java.awt.*;
public class User extends JFrame{
	public static ImageIcon[] image = new ImageIcon[10];
	static int[] resulT = new int[2];
	public User(){
		ImageIcon[] card = new ImageIcon[10];
		int judge = 0;//judge是一個判斷不重複數字的參數
		int[] a = new int[10];//a是十個不同的數字
		while (judge == 0){
			judge = 1;
			for (int i = 0;i<10;i++){
				a[i] = (int) (Math.random()*21);
				String b = "image/card/"+a[i]+".png";//b是路徑
				card[i] = new ImageIcon(b);
			}
			for (int i = 0;i<9;i++){//判斷是否有重複
				for (int j = i+1 ;j<10;j++){
					if(a[i] == a[j]){
						judge = 0;//有的話 重新while ((話說 這個方法我是馬上想到的...不過很暴力= =" 再不過~>我想了很久都想不到其他方法了.......
					}
				}
			}		
		}
		int[] group1 = new int[6];//我的方法是判斷同一個數字的牌有多少張 來決定到底是哪種牌
		for (int i = 0;i<6;i++){//起始值
			group1[i] = 0;
		}
		for(int i = 0;i<5;i++){
			if(a[i]/4-1<0){//a牌的數量
				group1[0]++;
			}
			else if(a[i]/4-1<1 && a[i]/4-1>=0){//2牌的數量
				group1[1]++;
			}
			else if (a[i]/4-1<2 && a[i]/4-1>=1){//j牌的數量
				group1[2]++;		
			}
			else if (a[i]/4-1<3 && a[i]/4-1>=2){//q牌的數量
				System.out.println(a[i]/4-1);
				group1[3]++;
			}
			else if (a[i]/4-1<4 && a[i]/4-1>=3){//k數的數量
				group1[4]++;
			}
			else if (a[i] == 20){//判斷是否有joker
				group1[5]++;
			}
		}
		int[] group2 = new int[6];//跟group1一樣
		for (int i = 0;i<6;i++){
			group2[i] = 0;
		}
		for(int i = 5;i<10;i++){
			if(a[i]/4-1<0){
				group2[0]++;
			}
			else if(a[i]/4-1<1 && a[i]/4-1>=0){
				group2[1]++;
			}
			else if (a[i]/4-1<2 && a[i]/4-1>=1){
				group2[2]++;		
			}
			else if (a[i]/4-1<3 && a[i]/4-1>=2){
				System.out.println(a[i]/4-1);
				group2[3]++;
			}
			else if (a[i]/4-1<4 && a[i]/4-1>=3){
				group2[4]++;
			}
			else if (a[i] == 20){
				group2[5]++;
			}
		}
		image = card;//為了要掉給FrameWork 所以用image裝
		resulT[0] = sort(group1);//計算結果
		resulT[1] = sort(group2);
	}
	public static ImageIcon[] getC(){
		return image;
	}
	private static int sort(int[] a){
		int result = 1;
		for (int i = 0;i<5;i++){//四張一樣
			if (a[i]==4){
				result = 5;
			}
			if (a[i]==3){//三張一樣
				for(int j = 0;j<5;j++){//再看有沒有多一對
					if(a[j] == 2){
						result = 4;//俘虜
					}
					else{
						result = 3;//三條
					}
				}
			}
			if (a[i]==2){
				for (int j = 0;j<5;j++){//兩對
					if(a[j]==2&& j!=i){
						result = 2;
					}
				}			
			}
		}
		if (a[5]>=1){//joker放在最後判斷
			result = 6;
		}
		return result;
	}
	public static int getResult(){//存回result
		return resulT[0];
	}
	public static int getResult2(){
		return resulT[1];
	}
}
