package a7.s100502028;

public class User {
	private int[] cardNumber = new int[5];
	private String joker = new String("< Joker >");
	private String fourOfAKind = new String("< Four of a kind >");
	private String fullHouse = new String("< Full house >");
	private String threeOfAKind = new String("< Three of a kind >");
	private String twoPairs = new String("< Two pairs >");
	private String onePair = new String("< One pair >");
	private String nothing = new String("< Nothing >");
	private int typePriority = 0;
	
	// Constructor to generate random number
	public User() {
		for(int i=0;i<5;i++) {
			cardNumber[i] = (int)(Math.random() * 21);
			for(int j=0;j<i;j++) { // If the number is repeated, run again
				if(cardNumber[j] == cardNumber[i])
					i--;
			}
		}
	} // End the Constructor
	
	// Return the number
	public int getNumber(int num) {
		return cardNumber[num];
	}
	
	// Return the priority of the cards' type
	public int getPriority() {
		return typePriority;
	}
	
	// Method to determine the cards' type 
	public String getCardType() {
		int[] cardKind = new int[5];
		int jokerNumber = 0, twoSameKind = 0, threeSameKind = 0, fourSameKind = 0;
		
		// For loop to get the kind of the card
		for(int i=0;i<5;i++) {
			if(cardNumber[i] < 4) // A
				cardKind[0]++;
			else if(cardNumber[i] < 8) // 2
				cardKind[1]++;
			else if(cardNumber[i] < 12) // J
				cardKind[2]++;
			else if(cardNumber[i] < 16) // Q
				cardKind[3]++;
			else if(cardNumber[i] < 20) // K
				cardKind[4]++;
			else  // Joker
				jokerNumber = 1; 
		}
		
		// For loop to calculate the same kind card
		for(int i=0;i<5;i++) {
			if(cardKind[i] == 2)
				twoSameKind++;
			else if(cardKind[i] == 3)
				threeSameKind++;
			else if(cardKind[i] == 4)
				fourSameKind++;
		}
		
		// Several if statement to determine the type of the cards and its priority
		if(jokerNumber == 1) { // Joker 
			typePriority = 7;
			return joker;
		}
		else if(fourSameKind == 1) { // Four of a kind
			typePriority = 6;
			return fourOfAKind;
		}
		else if(threeSameKind == 1) {
			if(twoSameKind == 1) { // Full house
				typePriority = 5;
				return fullHouse;
			}
			else {
				typePriority = 4; // Three of a kind
				return threeOfAKind;
			}
		}
		else if(twoSameKind == 2) { // Two pairs
			typePriority = 3;
			return twoPairs;
		}
		else if(twoSameKind == 1) { // One pair
			typePriority = 2;
			return onePair;
		}
		else { // Nothing
			typePriority = 1;
			return nothing;
		}
	} // End method getCardType()
	
	// Method to determine and show the game result
	public String showGameResult(User computer, User user){
		if(computer.getPriority() > user.getPriority())
			return "===You  Lose===";
		else if(computer.getPriority() < user.getPriority())
			return "===You  Win!!===";
		else
			return "=====Draw=====";
	}
} // End class User
