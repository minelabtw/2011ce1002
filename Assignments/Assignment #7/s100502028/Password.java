package a7.s100502028;

public class Password {
	private String password;
	private int letter = 0;
	private int digit = 0;
	
	// Construct the password
	public Password(String p) {
		password = p;
		verifyPassword(password);
	}
	
	// Return password
	public String getPassword() {
		return password;
	}
	
	// Method to calculate the number of letter or digit in password
	public void verifyPassword(String verify) {
		for(int i=0;i<verify.length();i++) {
			if(Character.isLetter(verify.charAt(i)) == true) {
				letter++;
			}
			else if(Character.isDigit(verify.charAt(i)) == true) {
				digit++;
			}
		}
	}
	
	// Method to determine the exception kind of the password
	public void passwordResult() throws InvalidPasswordException {
		String wrongResult1 = new String("The length of the password " + password + " is not enough!!");
		String wrongResult2 = new String("The password " + password + " has no number!!");
		String wrongResult3 = new String("The password " + password + " has no letter!!");
		if(password.length() < 7) { // The length of the password isn't enough
			throw new InvalidPasswordException(wrongResult1);
		}
		else if(digit < 1) { // The password doens't have any digit
			throw new InvalidPasswordException(wrongResult2);
		}
		else if(letter < 1) { // The password doens't have any letter
			throw new InvalidPasswordException(wrongResult3);
		}
		else {
			
		}
	}
}
