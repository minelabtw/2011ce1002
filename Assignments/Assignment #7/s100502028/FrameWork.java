package a7.s100502028;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.*;

public class FrameWork extends JFrame implements ActionListener {
	private ImageIcon[] initialPicture = new ImageIcon[5];
	private ImageIcon[] cardPicture = new ImageIcon[21];
	private JLabel[] computerInitial = new JLabel[5];
	private JLabel[] userInitial = new JLabel[5];
	private JLabel computer = new JLabel("Computer");
	private JLabel user = new JLabel("User");
	private JLabel computerType = new JLabel("< The type of computer's card >");
	private JLabel userType = new JLabel("< The type of user's card >");
	private JLabel gameResult = new JLabel("< Game result >");
	JButton button = new JButton("random"); // Create the random button
	
	// Construct the frame to display
	public FrameWork () {
		// Create some borders
		Border lineBorder1 = new LineBorder(Color.BLUE, 3);
		Border lineBorder2 = new LineBorder(Color.GREEN, 3);
		
		// Create some fonts
		Font font1 = new Font("Algerian", Font.BOLD, 20);
		Font font2 = new Font("TimeNewRoman",Font.ITALIC, 16);
		Font font3 = new Font("BritannicBold", Font.BOLD, 50);
		
		// Store the image of the card in the array
		for(int i=0;i<=20;i++) {
			cardPicture[i] = new ImageIcon("image/card/" + i + ".png");
		}
		
		// Create panel p1 for computer's name and its cards
		JPanel p1 = new JPanel(new GridLayout(1, 5, 5, 5));
		
		// Set the background color, font, font color and then add to the panel 
		p1.setBackground(Color.PINK);
		computer.setFont(font2);
		computer.setForeground(Color.BLUE);
		p1.add(computer);
		
		// For loop to store initial images in the array and then add to the panel
		for(int i=1;i<=5;i++) {
			computerInitial[i-1] = new JLabel("");
			initialPicture[i-1] = new ImageIcon("image/card/init_" + i + ".png");
			p1.add(computerInitial[i-1]);
			computerInitial[i-1].setIcon(initialPicture[i-1]);
			computerInitial[i-1].setBorder(lineBorder1);
		} // End for loop
		
		// Create panel p2 for user's name and its cards
		JPanel p2 = new JPanel(new GridLayout(1, 5, 5, 5));
		
		// Set the background color, font, font color and then add to the panel
		p2.setBackground(Color.PINK);
		user.setFont(font2);
		user.setForeground(Color.GREEN);
		p2.add(user);
		
		// For loop to store initial images in the array and then add to the panel
		for(int i=1;i<=5;i++) {
			userInitial[i-1] = new JLabel("");
			initialPicture[i-1] = new ImageIcon("image/card/init_" + i + ".png");
			p2.add(userInitial[i-1]);
			userInitial[i-1].setIcon(initialPicture[i-1]);
			userInitial[i-1].setBorder(lineBorder2);
		} // End for loop
		
		// Create panel p3 for p1, p2 , the random button
		JPanel p3 = new JPanel(new GridLayout(3, 1, 5, 5));
		p3.add(p1);
		p3.add(p2);
		
		// Set the background color, font, font colorand then add to the panel
		button.setBackground(Color.GRAY);
		button.setFont(font3);
		button.setForeground(Color.CYAN);
		p3.add(button);
		
		// Create panel p4 for the type of the computer's and user's card, game result
		JPanel p4 = new JPanel(new GridLayout(3, 1, 5, 5));
		
		// Set the font, font color and then add to panel
		computerType.setFont(font2);
		computerType.setForeground(Color.BLUE);
		p4.add(computerType);
		
		// Set the font, font color and then add to panel
		userType.setFont(font2);
		userType.setForeground(Color.BLUE);
		p4.add(userType);
		
		// Set the background color, font, font color and then add to the panel
		gameResult.setFont(font1);
		gameResult.setForeground(Color.RED);
		p4.setBackground(Color.YELLOW);
		p4.add(gameResult);
		
		// Add panel to the frame
		add(p3, BorderLayout.CENTER);
		add(p4, BorderLayout.EAST);
		p3.setBackground(Color.PINK);
		
		button.addActionListener(this); // Active button event here
	} // End the constructor
	
	// Method to let the button works
	public void actionPerformed(ActionEvent e) {
		// If statement while the button is pressed
		if(e.getSource() == button) {
			User computerCard = new User(); // Create an object of User type for computer's card
			User userCard = new User(); // Create an object of User type for user's card
			
			// For loop to get the generated random cards and store in the initial array
			for(int i=0;i<5;i++) {
				computerInitial[i].setIcon(cardPicture[computerCard.getNumber(i)]);
				userInitial[i].setIcon(cardPicture[userCard.getNumber(i)]);
			}
			
			// Get the type of the computer's and the user's card, game result
			computerType.setText(computerCard.getCardType());
			userType.setText(userCard.getCardType());
			gameResult.setText(computerCard.showGameResult(computerCard, userCard));
		} // End if statement
	} // End method actionPerformed
} // End class FrameWork
