package a7.s100502028;

public class InvalidPasswordException extends Exception {
	private String wrongPassword;
	
	// Construct an exception
	public InvalidPasswordException(String error) {
		wrongPassword = error;
	}
	
	// Return the exception
	public String getResult() {
		return wrongPassword;
	}
}
