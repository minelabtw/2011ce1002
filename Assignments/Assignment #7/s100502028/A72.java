package a7.s100502028;

import java.util.*;

public class A72 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		while (true) {
			System.out.println("Please input a password to varify:"); // Prompt the user for password to verify
			String userinput = input.next();
			
			Password test = new Password(userinput); // Create an object of Password and take user's input as argument
			
			// Try the code
			try {
				test.passwordResult(); // Call the method passwordResult() to determine whether the exception occurs or not
				System.out.println("The password " + test.getPassword() + " is valid!!\n");
			} 
			
			// Catch the exception
			catch (InvalidPasswordException ex) { 
				System.out.println(ex.getResult()); // Call the method getResult() to show the exception
				System.out.println("The password " + test.getPassword() + " is not correct!!\n");
			}
		}
	}
}
