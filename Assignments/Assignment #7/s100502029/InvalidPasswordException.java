package a7.s100502029;

public class InvalidPasswordException extends Exception {
	private String password;
	
	public InvalidPasswordException(String errorMessage, String password) {
		super(errorMessage);
		this.password = password; // set password
	}
	
	// return the password
	public String getPassword() {
		return password;
	}
}
