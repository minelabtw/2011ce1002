package a7.s100502029;
import javax.swing.*;

public class User {
	// declare two imageicon array to store all cards
	private ImageIcon[] cards = new ImageIcon[21];
	private ImageIcon[] initialCards = new ImageIcon[5];
	
	// declare seven type of cards
	private String joker = "Joker";
	private String four_of_a_kind = "Fourofakind";
	private String full_house = "FullHouse";
	private String three_of_a_kind = "Threeofakind";
	private String two_pairs = "Twopairs";
	private String one_pair = "A pair";
	private String nothing = "Nothing";
	
	// constructor to connect array to the cards
	public User() {
		for (int i = 0; i < 21; i++) {
			cards[i] = new ImageIcon("image/card/" + i + ".png");
		}
		for (int i = 0; i < 5; i++) {
			initialCards[i] = new ImageIcon("image/card/init_" + (i + 1) + ".png");
		}
	}
	
	// get initial five cards
	public ImageIcon[] getInitialCards() {
		return initialCards;
	}
	
	// generated five cards randomly(the cards can’t be repeated) and return the card set
	public ImageIcon[] setOfFiveCards() {
		ImageIcon[] cardsSet = new ImageIcon[5];
		for (int i = 0; i < 5; i++) {
			cardsSet[i] = cards[(int)(Math.random() * 21)];
			while (checkRepeated(cardsSet, i))
				cardsSet[i] = cards[(int)(Math.random() * 21)];
		}
		return cardsSet;
	}
	
	// check certain poker whether repeated in the set or not
	public boolean checkRepeated(ImageIcon[] cardsSet, int term) {
		for (int i = term - 1; i >= 0; i--) {
			if (cardsSet[term] == cardsSet[i])
				return true;
		}
		return false;
	}
	
	// determine the type of the card set
	public String determineType(ImageIcon[] cardsSet) {
		// number[0] is represented A, number[1] is 2, number[2] is J, number[3] is Q, number[4] is K, number[5] is joker.
		int[] number = new int[6];
		
		// 統計各個數字的牌出現了幾次
		for (int i = 0; i < cardsSet.length; i++) {
			for (int j = 0; j < cards.length; j++) {
				if (cardsSet[i] == cards[j]) {
					if (j < 4)
						number[0]++;
					else if (j < 8)
						number[1]++;
					else if (j < 12)
						number[2]++;
					else if (j < 16)
						number[3]++;
					else if (j < 20)
						number[4]++;
					else
						number[5]++;
					break;
				}
			}
		}
		
		// if number[5] is not equal to zero, that indicate joker is appeared in the card set
		if (number[5] != 0)
			return joker;
		
		// 若某種牌出現4次,則牌型為four_of_a_kind
		for (int i = 0; i < 5; i++) {
			if (number[i] == 4)
				return four_of_a_kind;
		}
		
		// 統計沒出現的牌的數字有幾種
		int numberOfNotAppearCardTypes = 0;
		for (int i = 0; i < 5; i++) {
			if (number[i] == 0)
				numberOfNotAppearCardTypes++;
		}
		
		// if numberOfNotAppearCardTypes is equal to zero, that represent every number of cards appear once, so the type is nothing
		if (numberOfNotAppearCardTypes == 0)
			return nothing;
		else if (numberOfNotAppearCardTypes == 1)
			return one_pair;
		else if (numberOfNotAppearCardTypes == 3)
			return full_house;
		else {
			for (int i = 0; i < 5; i++) {
				if (number[i] == 3)
					return three_of_a_kind;
			}
			return two_pairs;
		}			
	}
	
	// determine player win, lose, or draw
	public String determineWinOrLose(String com, String player) {
		int computer, you;
		
		// Their priority is listed from large to small.
		if (com.equals(joker))
			computer = 7;
		else if (com.equals(four_of_a_kind))
			computer = 6;
		else if (com.equals(full_house))
			computer = 5;
		else if (com.equals(three_of_a_kind))
			computer = 4;
		else if (com.equals(two_pairs))
			computer = 3;
		else if (com.equals(one_pair))
			computer = 2;
		else
			computer = 1;
		
		if (player.equals(joker))
			you = 7;
		else if (player.equals(four_of_a_kind))
			you = 6;
		else if (player.equals(full_house))
			you = 5;
		else if (player.equals(three_of_a_kind))
			you = 4;
		else if (player.equals(two_pairs))
			you = 3;
		else if (player.equals(one_pair))
			you = 2;
		else
			you = 1;
		
		if (you > computer)
			return "You Win!!!";
		else if (you < computer)
			return "You Lose!!!";
		else
			return "Draw";
	}
}
