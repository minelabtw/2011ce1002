package a7.s100502029;

public class Password {
	private boolean correctFlag = false; // true represent password valid
	
	// constructor to set password
	public Password(String newPassword) throws InvalidPasswordException {
		setPassword(newPassword);
	}
	
	// check password whether correct or not. If incorrect, throws exception.
	public void setPassword(String newPassword) throws InvalidPasswordException {
		if (!checkLength(newPassword))
			throw new InvalidPasswordException("The length of the password " + newPassword + " is not enough!!", newPassword);
		else if (!checkPasswordHasNumber(newPassword))
			throw new InvalidPasswordException("The password " + newPassword + " has no number!!", newPassword);
		else if (!checkPasswordHasLetter(newPassword))
			throw new InvalidPasswordException("The password " + newPassword + " has no letter!!", newPassword);
		else {
			
		}
	}
	
	// check password whether long enough
	public boolean checkLength(String password) {
		if (password.length() >= 7) {
			correctFlag = true;
			return true;
		}
		else
			return false;
	}
	
	// check password has Digit
	public boolean checkPasswordHasNumber(String password) {
		int number = 0;
		for (int i = 0; i < password.length(); i++) {
			if (Character.isDigit(password.charAt(i)))
				number++;
		}
		if (number != 0)
			return true;
		else
			return false;
	}
	
	// check password has letter
	public boolean checkPasswordHasLetter(String password) {
		int letter = 0;
		for (int i = 0; i < password.length(); i++) {
			if (Character.isLetter(password.charAt(i)))
				letter++;
		}
		if (letter != 0)
			return true;
		else
			return false;
	}
	
	// return the correct flag
	public boolean getCorrectFlag() {
		return correctFlag;
	}
}
