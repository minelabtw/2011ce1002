package a7.s100502029;
import java.util.Scanner;

public class A72 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String newPassword; // this variable is to store new password
		boolean exitFlag = false; // true to exit
		do {
			System.out.println("Please input a password to varify:");
			newPassword = input.next(); // read new password from user
			try {
				Password password = new Password(newPassword); // check password if valid. If not, throws exception
				exitFlag = password.getCorrectFlag(); // if password is valid, exitFlag will be true
			}
			catch (InvalidPasswordException ex) {
				System.out.println(ex.getMessage()); // invoked getMessage method to get error message
				System.out.println("The password " + ex.getPassword() + " is not correct!!\n"); // show incorrect message
			}
		} while (!exitFlag);
		System.out.println("The password " + newPassword + " is valid!!"); // show valid message
	}
}
