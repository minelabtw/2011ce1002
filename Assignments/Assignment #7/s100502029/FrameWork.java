package a7.s100502029;
import java.awt.event.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

public class FrameWork extends JFrame implements ActionListener {
	private JLabel[] comCards = new JLabel[5]; // to store computer's carts
	private JLabel[] playerCards = new JLabel[5]; // to store player's cards
	
	// create all kind of label
	private JLabel com = new JLabel("Com");
	private JLabel you = new JLabel("You");
	private JLabel comType = new JLabel("<ComputerType>");
	private JLabel playerType = new JLabel("<PlayerType>");
	private JLabel result = new JLabel("<Win or Lose>");
	
	// create two player: com and player
	private User computer = new User();
	private User player = new User();
	
	// create random button
	JButton button = new JButton("Random");
	
	public FrameWork() {
		// set initial cards to computer cards and player cards
		for (int i = 0; i < 5; i++) {
			comCards[i] = new JLabel(computer.getInitialCards()[i]);
		}
		for (int i = 0; i < 5; i++) {
			playerCards[i] = new JLabel(player.getInitialCards()[i]);
		}
		
		// create random, result, and name font object
		Font randomFont = new Font("SansSerif", Font.BOLD, 18);
		Font resultFont = new Font("TimesRoman", Font.BOLD, 70);
		Font nameFont = new Font("Times New Roman", Font.BOLD, 24);
		
		// create random, computer, player, and else border object
		Border randomBorder = new LineBorder(Color.GRAY, 2);
		Border comBorder = new LineBorder(Color.GREEN, 3);
		Border playerBorder = new LineBorder(Color.ORANGE, 2);
		Border elseBorder = new LineBorder(Color.PINK, 2);
		
		// decorate the random button
		button.setFont(randomFont);
		button.setBorder(randomBorder);
		button.setForeground(Color.YELLOW);
		button.setBackground(Color.DARK_GRAY);
		
		// decorate the computer name and type
		com.setFont(nameFont);
		com.setBorder(comBorder);
		comType.setForeground(new Color(19, 19, 172));
		comType.setBorder(elseBorder);
		
		// decorate the player name and type
		you.setFont(nameFont);
		you.setBorder(playerBorder);
		playerType.setForeground(new Color(19, 19, 172));
		playerType.setBorder(elseBorder);
		
		// decorate the game result
		result.setFont(resultFont);
		result.setBorder(elseBorder);
		result.setForeground(Color.CYAN);
		
		// set frame's layout
		setLayout(new BorderLayout(1, 1));
		
		// create panel 1 to store computer name and cards
		JPanel p1 = new JPanel();
		p1.setLayout(new GridLayout(1, 6, 1, 1));
		p1.add(com);
		for (int i = 0; i < 5; i++) {
			p1.add(comCards[i]);
			comCards[i].setBorder(comBorder);
		}
		
		// create panel 2 to store player name and cards
		JPanel p2 = new JPanel();
		p2.setLayout(new GridLayout(1, 6, 1, 1));
		p2.add(you);
		for (int i = 0; i < 5; i++) {
			p2.add(playerCards[i]);
			playerCards[i].setBorder(playerBorder);
		}
		
		// combine panel 1, panel 2, and game result
		JPanel p3 = new JPanel();
		p3.setLayout(new GridLayout(3, 1, 1, 1));
		p3.add(p1);
		p3.add(p2);
		p3.add(result);
		
		// create panel 4 to store computer's type, player's type, and random button
		JPanel p4 = new JPanel();
		p4.setLayout(new GridLayout(3, 1, 1, 1));
		p4.add(comType);
		p4.add(playerType);
		p4.add(button);
		
		// combine panel 3 and panel 4
		add(p3, BorderLayout.CENTER);
		add(p4, BorderLayout.EAST);
		
		//active button event here
		button.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == button) {
			// random computer's cards and store in label
			ImageIcon[] comCardsSet = new ImageIcon[5];
			comCardsSet = computer.setOfFiveCards();
			for (int i = 0; i < 5; i++)
				comCards[i].setIcon(comCardsSet[i]);
			
			// invoke method to determine the type of computer's cards
			String comType = computer.determineType(comCardsSet);
			
			// store the type of computer's cards in label
			this.comType.setText(comType);
			
			// random player's cards and store in label
			ImageIcon[] playerCardsSet = new ImageIcon[5];
			playerCardsSet = player.setOfFiveCards();
			for (int i = 0; i < 5; i++)
				playerCards[i].setIcon(playerCardsSet[i]);
			
			// invoke method to determine the type of player's cards
			String playerType = player.determineType(playerCardsSet);
			
			// store the type of player's cards in label
			this.playerType.setText(playerType);
			
			// store the game result in label
			result.setText(player.determineWinOrLose(comType, playerType));
		}
	}
}
