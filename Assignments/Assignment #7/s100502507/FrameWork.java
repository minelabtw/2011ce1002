package a7.s100502507;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.LineBorder;

public class FrameWork extends JFrame implements ActionListener{
	JButton button = new JButton("Random");
	FrameWork() {
		user = new User();//New user
		p1 = new JPanel();//Basic panel(left)
		pf1 = new JPanel();//A front panel of p1 contains He, his card
		p2 = new JPanel();//Basic panel(right)
		pf2 = new JPanel();//A front panel of p2 contains You, your card
		infromationTextType = new Font("TimesRoman", Font.BOLD, 25);//Font used by He and You
		cardTextType = new Font("TimesRoman", Font.BOLD, 25);//Font used by type of cards
		resultTextType = new Font("TimesRoman", Font.BOLD, 40);//Font used by the result
		buttonText = new Font("TimesRoman", Font.BOLD, 40);//Font used by button
		cardType01 = new JLabel("");//Font used by his type of cards
		cardType02 = new JLabel("");//Font used by your type of cards
		information = new JLabel("He");//A label with the word He
		
		pf1.setLayout(new GridLayout(1, 6, 5, 5));//6 place to store He and his cards
		p1.setLayout(new GridLayout(3, 1, 5, 10));//3 places to store pf1, pf2 and a button
		pf2.setLayout(new GridLayout(1, 6, 5, 5));//6 place to store You and your cards
		p2.setLayout(new GridLayout(3, 1, 5, 10));//3 places to store his and your type of cards
		
		information.setFont(infromationTextType);//Set the font of He
		
		cardBorder = new LineBorder(Color.BLACK, 3);//Border used by cards
		card = new ImageIcon("image/card/init_1.png");//Stores the image which needs to be used now
		cardField01 = new JLabel(card);//First card of He
		cardField11 = new JLabel(card);//First card of You
		card = new ImageIcon("image/card/init_2.png");
		cardField02 = new JLabel(card);//2nd. card of He
		cardField12 = new JLabel(card);//2nd. card of You
		card = new ImageIcon("image/card/init_3.png");
		cardField03 = new JLabel(card);//3rd. card of He
		cardField13 = new JLabel(card);//3rd. card of You
		card = new ImageIcon("image/card/init_4.png");
		cardField04 = new JLabel(card);//4th. card of He
		cardField14 = new JLabel(card);//4th. card of You
		card = new ImageIcon("image/card/init_5.png");
		cardField05 = new JLabel(card);//5th. card of He
		cardField15 = new JLabel(card);//5th. card of You
		
		cardField01.setBorder(cardBorder);//Set the border to cards
		cardField02.setBorder(cardBorder);
		cardField03.setBorder(cardBorder);
		cardField04.setBorder(cardBorder);
		cardField05.setBorder(cardBorder);
		cardField11.setBorder(cardBorder);
		cardField12.setBorder(cardBorder);
		cardField13.setBorder(cardBorder);
		cardField14.setBorder(cardBorder);
		cardField15.setBorder(cardBorder);
		
		pf1.add(information);//Added to pf1
		pf1.add(cardField01);//Add cards to pf1
		pf1.add(cardField02);
		pf1.add(cardField03);
		pf1.add(cardField04);
		pf1.add(cardField05);
		
		cardType01.setFont(cardTextType);//Set font to his type of cards
		
		information = new JLabel("You");//Change this label to the word You
		information.setFont(infromationTextType);//Set the font of You
		
		pf2.add(information);//Add it to pf2
		pf2.add(cardField11);//Then add your cards to pf2
		pf2.add(cardField12);
		pf2.add(cardField13);
		pf2.add(cardField14);
		pf2.add(cardField15);
		
		cardType02.setFont(cardTextType);//Set font to his type of cards
		
		button.addActionListener(this);//Therefore we can use the button to do something
		button.setFont(buttonText);//Set the font to button
		result = new JLabel("");//The final result(Win or Loose)
		result.setFont(resultTextType);//Set the font to result
		
		p1.add(pf1);//Add all the items which should be present at left
		p1.add(pf2);
		p1.add(button);
		p2.add(cardType01);//Add all the items which should be present at right
		p2.add(cardType02);
		p2.add(result);
		
		setLayout(new BorderLayout(10, 5));//Set current frame to BorderLayout
		add(p1, BorderLayout.WEST);//Set p1 to center
		add(p2, BorderLayout.CENTER);//Set p2 to east
	}
	
	public void actionPerformed(ActionEvent e){
		if(e.getSource()==button){
			user.random();
			int[] cards = user.getCards();//Get what card produced
			String[] types = user.getType();//Get type and result
			
			card = new ImageIcon("image/card/" + String.valueOf(cards[0]) + ".png");//Reload picture and revise them
			cardField01.setIcon(card);
			card = new ImageIcon("image/card/" + String.valueOf(cards[1]) + ".png");
			cardField02.setIcon(card);
			card = new ImageIcon("image/card/" + String.valueOf(cards[2]) + ".png");
			cardField03.setIcon(card);
			card = new ImageIcon("image/card/" + String.valueOf(cards[3]) + ".png");
			cardField04.setIcon(card);
			card = new ImageIcon("image/card/" + String.valueOf(cards[4]) + ".png");
			cardField05.setIcon(card);
			card = new ImageIcon("image/card/" + String.valueOf(cards[5]) + ".png");
			cardField11.setIcon(card);
			card = new ImageIcon("image/card/" + String.valueOf(cards[6]) + ".png");
			cardField12.setIcon(card);
			card = new ImageIcon("image/card/" + String.valueOf(cards[7]) + ".png");
			cardField13.setIcon(card);
			card = new ImageIcon("image/card/" + String.valueOf(cards[8]) + ".png");
			cardField14.setIcon(card);
			card = new ImageIcon("image/card/" + String.valueOf(cards[9]) + ".png");
			cardField15.setIcon(card);
			
			cardType01.setText(types[0]);//Reset text
			cardType02.setText(types[1]);
			result.setText(types[2]);
		}
	}
	private JPanel p1, pf1, p2, pf2;
	private JLabel cardField01, cardField02, cardField03, cardField04, cardField05, cardField11, cardField12, cardField13, cardField14, cardField15, information, cardType01, cardType02, result;
	private ImageIcon card;
	private User user;
	private Font infromationTextType, cardTextType, resultTextType, buttonText;
	private LineBorder cardBorder;
}
