package a7.s100502507;
import java.util.*;
public class Password {
	Password(String input) {//Initialize variable and call the check function
		passWord = input;
		information = "";
		check();
	}
	
	public void check() {//Check if the password fills the bill or not
		try {
			checkString = new boolean[3];//New the boolean to check if there're numbers or letters of this password
			checkString[0] = checkString[1] = checkString[2] = false;//Set to false
			for(int i=0; i<passWord.length(); i++) {
				if(passWord.charAt(i) - '0'<10 && passWord.charAt(i) - '0'>=0) {//Number detected
					checkString[0] = true;
				}
				else if(!Character.isLetter(passWord.charAt(i)))  {//Word detected
					checkString[1] = true;
				}
				else {//Symbol detected
					checkString[2] = true;
				}
			}
			
			if(passWord.length()<7) {//Too short
				throw new InvalidPasswordException("The length of the password " + passWord + " is not enough!!");
			}
			else if(!checkString[0]) {//No number
				throw new InvalidPasswordException("The password " + passWord + " has no number!!");
			}
			else if(!checkString[1]) {//No letter
				throw new InvalidPasswordException("The password " + passWord + " has no letter!!");
			}
			else if(checkString[2]) {
				throw new InvalidPasswordException("The password " + passWord + " has symbols!!");
			}
			else {//The password fills the bill!
				information = "The password " + passWord + " is valid";
			}
		}
		catch (InvalidPasswordException lessWords) {//Set information in order to output the result after checked
			information = lessWords.toString() + "The password " + passWord + " is not correct!!";
		}
	}
	
	public String getInformation() {//Return the information
		return information;
	}
	
	public String getPassWord() {//Return the password
		return passWord;
	}
	private boolean[] checkString;
	private String information;
	private String passWord;
}