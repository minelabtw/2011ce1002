package a7.s100502507;
import java.lang.Math;
public class User {
	User() {//Initialize variables
		cards = new int[10];//Top 5 cards represent the cards which computer gets, the other represent the cards which user receives
		types = new String[6];//All types of cards such as Joker, Two pairs, and so on
		victor = new int[2];//Elements 0 represents computer, 1 represents user. If one element is bigger then the other, then the bigger one is the victor
		types[5] = "<Joker>";//Initializes types
		types[4] = "<Four of a kind>";
		types[3] = "<Full House>";
		types[2] = "<Three of kind>";
		types[1] = "<Two pairs>";
		types[0] = "<Nothing>";
		victor[0] = victor[1] = 0;
		for(int i=0; i<10; i++) {
			cards[i] = 0;
		}
	}
	
	public void random() {//Give cards randomly
		for(int i=0; i<10; i++) {
			cards[i] = (int)(Math.random()*21);
			for(int j=0; j<i; j++) {//Check repeat
				if(i>0 && cards[i]==cards[j]) {
					i -= 1;
					break;
				}
			}
		}
	}
	
	public int[] getCards() {//Return the cards
		return cards;
	}
	
	public String[] getType() {//Return the types
		String[] resultTypes = new String[3];//A variable to hold the current result
		int[][] cardTypes = new int[2][6];//A variable used to check how many cards of the same number is appear(i=0=computer, i=1=user)
		for(int i=0; i<2; i++) {//First initialize the variable used to check
			for(int j=0; j<6; j++) {
				cardTypes[i][j] = 0;
			}
		}
		for(int i=0; i<2; i++) {//i=0 represents computer, i=1 represents user
			for(int j=i*5; j<(i+1)*5; j++) {//If i=0, check the top 5 elements only. Else, check the last 5 elements only
				switch(cards[j]) {//Some group of types0. If condition fills the bill, then the group+1
				case 0:case 1:case 2:case 3://A
					cardTypes[i][0] += 1;
					break;
				case 4:case 5:case 6:case 7://2
					cardTypes[i][1] += 1;
					break;
				case 8:case 9:case 10:case 11://J
					cardTypes[i][2] += 1;
					break;
				case 12:case 13:case 14:case 15://Q
					cardTypes[i][3] += 1;
					break;
				case 16:case 17:case 18:case 19://K
					cardTypes[i][4] += 1;
					break;
				case 20:
					cardTypes[i][5] += 1;//Joker
					break;
				}
			}
		}
		boolean[][] check = new boolean[2][4];//A boolean to check which type already has appeared
		for(int j=0; j<2; j++) {//At first, set all boolean to false
			for(int k=0; k<4; k++) {
				check[j][k] = false;
			}
		}
		int counter;//A variable to check 2 pairs
		for(int i=0; i<2; i++) {
			counter = 0;//Initialize counter
			for(int j=0; j<6; j++) {
				if(cardTypes[i][5]==1) {//If Joker appears
					check[i][3] = true;
				}
				if(cardTypes[i][j]==4) {//If four of a kind appears
					check[i][2] = true;
				}
				if(cardTypes[i][j]==3) {//3 cards with same number
					check[i][1] = true;
				}
				if(cardTypes[i][j]==2) {//2 cards with same number
					counter += 1;//1 pairs detected
					if(counter==2) {//Oh man~ there're 2 pairs
						check[i][0] = true;
					}
				}
			}
			if(check[i][3]) {//Joker
				resultTypes[i] = types[5];
				victor[i] = 5;
			}
			else if(check[i][2]) {//Four of a kind
				resultTypes[i] = types[4];
				victor[i] = 4;
			}
			else if(check[i][1] && check[i][0]) {//Full house
				resultTypes[i] = types[3];
				victor[i] = 3;
			}
			else if(check[i][1]) {//Three of a kind
				resultTypes[i] = types[2];
				victor[i] = 2;
			}
			else if(check[i][0]) {//2 pairs
				resultTypes[i] = types[1];
				victor[i] = 1;
			}
			else {//Nothing
				victor[i] = 0;
				resultTypes[i] = types[0];
			}
		}
		if(victor[1]>victor[0]) {
			resultTypes[2] = "You Won!";
		}
		else if(victor[1]==victor[0]) {
			resultTypes[2] = "Duce!";
		}
		else {
			resultTypes[2] = "You Lost!";
		}
		return resultTypes;
	}
	
	private int[] victor;
	private int[] cards;
	private String[] types;
}
