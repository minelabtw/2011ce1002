package a7.s100502507;

import java.awt.*;
import java.awt.event.*;
import javax.swing.border.*;
import javax.swing.*;

public class A72 extends JFrame implements ActionListener {
	A72() {//Initialize GUI
		JPanel pUp = new JPanel(new GridLayout(1, 1));//Base panel(upper)
		JPanel pDown = new JPanel();//Another base panel
		JPanel input = new JPanel();//A panel which will add a field of text to itself
		
		passWordField = new JTextField(30);//A field of text which let user type the password
		Font titleFont = new Font("TimesRoman", Font.BOLD + Font.ITALIC, 20);//Font used by the title
		TitledBorder titleBorder = new TitledBorder("Enter a password with at least seven alphanumeric characters");//A border with a sentence
		
		titleBorder.setTitleFont(titleFont);//Set font to it
		input.setBorder(titleBorder);//Set border with the information needing to care while typing
		
		button = new JButton("Confirm");//A button let user decide if he or she is ready or not
		button.addActionListener(this);//Therefore we can use the button to do something
		button.setFont(titleFont);//Set the font to button
		
		input.add(passWordField);//Add the field of text to panel
		input.add(button);//Then add button to it
		pUp.add(input);//Finally set this panel to the base panel
		
		TitledBorder condition = new TitledBorder("Condition");//A border with "Condition" to tell user the information inside this is the condition of password user inputed
		result = new JLabel("Waitting for input");//Initialize result
		Font resultFont = new Font("TimesRoman", Font.BOLD, 20);//A font used by the text of result
		
		condition.setTitleFont(titleFont);//Set the font of "Condition"
		result.setFont(resultFont);//Set the font to result
		pDown.setBorder(condition);//Set the border with "Condition" to the base panel
		pDown.setFont(resultFont);//Set the font of the text pDown will display
		
		pDown.add(result);//Add the result label to the base panel
		
		setLayout(new GridLayout(2, 1, 5, 5));//Set the layout to current frame
		add(pUp);//Add these 2 base panel to current frame
		add(pDown);
	}
	
	public static void main(String[] args) {//Function main begins execution
		A72 f = new A72();//Create frame
		f.setTitle("A72");
		f.setSize(900, 200);
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
	
	public void actionPerformed(ActionEvent e){//Once button is pressed, check the password user inputed
		if(e.getSource()==button){
			passWordClass = new Password(passWordField.getText());
			result.setText(passWordClass.getInformation());//Show if the password is appropriate or not
		}
	}
	
	private Password passWordClass;
	private JTextField passWordField;
	private JButton button;
	private JLabel result;
}
