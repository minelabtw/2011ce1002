package a7.s100502507;

public class InvalidPasswordException extends Exception {
	InvalidPasswordException(String input) {
		super(input);
		result = input;
	}
	
	public String toString() {
		return result;
	}
	
	private String result;
}
