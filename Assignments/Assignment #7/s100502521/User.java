package a7.s100502521;

public class User 
{
	private Card Lipon;//一副牌
	private String[] Type=new String[6];//牌型
	
	public User()
	{
		Lipon=new Card();
		Type[0]="Joker";
		Type[1]="Four of a kind";
		Type[2]="Full House";
		Type[3]="Three of kind";
		Type[4]="Two pairs";
		Type[5]="Nothing";
	}
	public String getTypeName(int input)//回傳牌型名稱
	{
		return Type[input];
	}
	public int getCardNumber(int input)//將數字編號轉換成牌
	{
		if(input>=0 && input<=3)
		{
			return 1;
		}
		else if(input>=4 && input<=7)
		{
			return 2;
		}
		else if(input>=8 && input<=11)
		{
			return 11;
		}
		else if(input>=12 && input<=15)
		{
			return 12;
		}
		else if(input>=16 && input<=19)
		{
			return 13;
		}
		else
		{
			return 53;
		}
		
	}
	public int[] checkType(int[] fiveCard)//判斷牌型
	{//最多兩種牌重覆
		int[] Tcard=new int[5];
		int[] Repeat=new int[2];
		int[] RepeatNumber=new int[2];
		int[] reSize=new int[2];//牌型，牌型大小
		int[] cardNumber=new int[5];//0>A 1>2 2>J 3>Q 4>k
		for(int i=0;i<5;i++)
		{
			cardNumber[i]=0;
		}
		for(int i=0;i<5;i++)
		{
			Tcard[i]=getCardNumber(fiveCard[i]);
			if(Tcard[i]==53)
			{
				reSize[0]=0;
				reSize[1]=0;
				return reSize;//Joker
			}
			if(Tcard[i]==54)
			{
				reSize[0]=0;
				reSize[1]=1;
				return reSize;//Joker
			}
			switch(Tcard[i])
			{
			case 1:
				cardNumber[0]++;
				break;
			case 2:
				cardNumber[1]++;
				break;
			case 11:
				cardNumber[2]++;
				break;
			case 12:
				cardNumber[3]++;
				break;
			case 13:
				cardNumber[4]++;
				break;
			default:
				break;
			}
		}
		int j=0;
		Repeat[0]=0;//最多兩種牌重覆 陣列0 跟1
		Repeat[1]=0;//存他是什麼牌重覆
		RepeatNumber[0]=0;//存牠重覆的數量
		RepeatNumber[1]=0;
		for(int i=0;i<5;i++)
		{
			if(cardNumber[i]>1)
			{
				RepeatNumber[j]=cardNumber[i];
				switch(i)
				{
				case 0:
					Repeat[j]=1;
					break;
				case 1:
					Repeat[j]=2;
					break;
				case 2:
					Repeat[j]=11;
					break;
				case 3:
					Repeat[j]=12;
					break;
				case 4:
					Repeat[j]=13;
					break;
				default:
					break;
				}
				j++;
			}
		}
		for(int i=0;i<2;i++)
		{
			if(RepeatNumber[i]==4)//如果有4張 那就是鐵支啦
			{
				reSize[0]=1;
				reSize[1]=Repeat[i];
				return reSize;
			}
			else if(RepeatNumber[i]==3)//3張
			{
				if(i==1)
				{
					if(RepeatNumber[0]==2)//3+2葫蘆
					{
						reSize[0]=2;
						reSize[1]=Repeat[i];
						return reSize;
					}
					else
					{
						reSize[0]=3;
						reSize[1]=Repeat[i];
						return reSize;
					}
				}
				else
				{
					if(RepeatNumber[1]==2)//3+2
					{
						reSize[0]=2;
						reSize[1]=Repeat[i];
						return reSize;
					}
					else
					{
						reSize[0]=3;
						reSize[1]=Repeat[i];
						return reSize;
					}
				}
			}
			else if(RepeatNumber[i]==2)//2
			{
				if(i==1)
				{
					if(RepeatNumber[0]==2)//2+2
					{
						reSize[0]=4;
						if(Repeat[0]>Repeat[1])
						{
							reSize[1]=Repeat[0];
						}
						else if(Repeat[0]<Repeat[1])
						{
							reSize[1]=Repeat[1];
						}
						else
						{
							reSize[1]=0;
						}
						return reSize;
					}
				}
				else
				{
					if(RepeatNumber[1]==2)//2+2
					{
						reSize[0]=4;
						if(Repeat[0]>Repeat[1])
						{
							reSize[1]=Repeat[0];
						}
						else if(Repeat[0]<Repeat[1])
						{
							reSize[1]=Repeat[1];
						}
						else
						{
							reSize[1]=0;
						}
						return reSize;
					}
				}
			}
			else
			{
				reSize[0]=5;
				reSize[1]=Tcard[4];
			}
		}
		reSize[0]=5;
		reSize[1]=0;
		return reSize;
	}
	public int VsCard(int[] C,int[] U)//0 平手 1電腦贏2玩家贏
	{
		if(C[0]<U[0])//數字小越大
		{
			return 1;
		}
		else if(C[0]>U[0])
		{
			return 2;
		}
		else 
		{
			if(C[1]<U[1])
			{
				return 1;
			}
			else if(C[1]>U[1])
			{
				return 2;
			}
			else
			{
				return 0;
			}
		}
	}
	public String getCardPath(int input)//取得圖片路徑
	{
		return Lipon.getCardPath(input);
	}
	public int[] getRamdomTenCard()//隨機10張牌(不重覆) 前5張電腦 後5張玩家
	{
		int[] ram=new int[10];
		int temp;
		boolean check=false;
		int i=0;
		while(i<10)
		{
			temp=(int) (Math.random()*21);
			check=false;
			for(int j=0;j<i;j++)
			{
				if(ram[j]==temp)
				{
					check=true;
				}
			}
			if(!check)
			{
				ram[i]=temp;
				i++;
			}
		}
		return ram;
	}
}
