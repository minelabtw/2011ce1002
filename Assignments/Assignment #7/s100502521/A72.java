package a7.s100502521;

public class A72
{
	public static void main(String[] argc)
	{
		Password data=new Password();
		boolean loop=true;
		while(loop)
		{
			while(true)
			{
				if(data.setPassword())//如果設定正確的話
				{
					break;
				}
				else//錯誤代表按下取消
				{
					loop=false;
					break;
				}
			}
			if(loop)
			{
				loop=false;
			}
			else//取消  離開
			{
				break;
			}
			try//錯誤處理
			{
				if(!data.checkPasswordLength())
				{
					loop=true;
					throw new InvalidPasswordException(0);
				}
				if(!data.checkPasswordEnglish())
				{
					loop=true;
					throw new InvalidPasswordException(1);
				}
				if(!data.checkPasswordNumber())
				{
					loop=true;
					throw new InvalidPasswordException(2);
				}
			}
			catch(InvalidPasswordException e)
			{
				System.out.println(data.getPassword()+"是錯誤的!!");
			}			
		}
		if(data.getPassword()!=null)
		{
			System.out.println(data.getPassword()+"是正確的!!");
		}
	}
}
