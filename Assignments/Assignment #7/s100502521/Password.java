package a7.s100502521;
import javax.swing.JOptionPane;
public class Password 
{
	private String password;
	public Password()
	{
		password=new String();
	}
	public boolean setPassword()//設定密碼
	{
		password=JOptionPane.showInputDialog("請輸入密碼:");
		if(password==null)//按取消的話
		{
			return false;
		}
		return true;
	}
	public String getPassword()
	{
		return password;
	}
	public boolean checkPasswordLength()//檢查長度
	{
		if(password.length()<7)
		{
			return false;
		}
		return true;
	}
	public boolean checkPasswordEnglish()//檢查有沒有英文字
	{
		for(int i=0;i<7;i++)
		{
			if( (int)password.charAt(i)>=65 && (int)password.charAt(i)<=90 )
			{
				return true;
			}
			if( (int)password.charAt(i)>=97 && (int)password.charAt(i)<=122 )
			{
				return true;
			}
		}
		return false;
	}
	public boolean checkPasswordNumber()//檢查有沒有數字
	{
		for(int i=0;i<7;i++)
		{
			if( (int)password.charAt(i)>=48 && (int)password.charAt(i)<=57 )
			{
				return true;
			}
		}
		return false;
	}
}
