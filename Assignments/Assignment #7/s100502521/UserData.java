package a7.s100502521;

import javax.swing.JOptionPane;

public class UserData//存玩家得分
{
	private int Computer_Score;
	private int User_Score;
	private String User_name;
	public UserData()
	{
		Computer_Score=0;
		User_Score=0;
		User_name=JOptionPane.showInputDialog("請輸入您的大名:");//讓使用者輸入姓名
	}
	public int getComputerScore()
	{
		return Computer_Score;
	}
	public int getUserScore()
	{
		return User_Score;
	}
	public String getUserName()
	{
		return User_name;
	}
	public void addComputerScore(int input)
	{
		Computer_Score+=input;
	}
	public void addUserScore(int input)
	{
		User_Score+=input;
	}
}
