package a7.s100502521;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.*;
public class FrameWork extends JFrame implements ActionListener//類別繼承類別，類別實做介面
{
	private static final long serialVersionUID = 1L;
	private JPanel North = new JPanel();
	private JPanel South = new JPanel();
	private JPanel West = new JPanel();
	private JPanel East = new JPanel();
	private JPanel East1 = new JPanel();
	private JPanel East2 = new JPanel();
	private JPanel Center = new JPanel();
	private JButton button;
	private JLabel[] North_label=new JLabel[6];//label標題
	private JLabel[] South_label=new JLabel[6];//label標題
	private JLabel[] C_label=new JLabel[2];//label標題
	private JLabel[] U_label=new JLabel[2];//label標題
	private JLabel[] Center_label=new JLabel[3];
	private ImageIcon[] image=new ImageIcon[10];
	private User user=new User();
	private UserData data=new UserData();
	public FrameWork()
	{
		North.setLayout(new GridLayout(1,6,0,0));
		for(int i=0;i<10;i++)
		{
			image[i]=new ImageIcon("image/card/init_5.png");//設定初始圖片
		}
		EtchedBorder border=new EtchedBorder();
		for(int i=0;i<5;i++)
		{
			North_label[i]=new JLabel(image[i]);
			North_label[i].setBorder(border);
			North.add(North_label[i]);
		}
		North_label[5]=new JLabel("Computer");
		North.add(North_label[5]);
		
		South.setLayout(new GridLayout(1,6,0,0));
		for(int i=0;i<5;i++)
		{
			South_label[i]=new JLabel(image[i+5]);
			South_label[i].setBorder(border);
			South.add(South_label[i]);
		}
		South_label[5]=new JLabel("玩家:"+data.getUserName());
		South.add(South_label[5]);
		East1.setLayout(new GridLayout(2,1,0,0));
		East2.setLayout(new GridLayout(2,1,0,0));
		East.setLayout(new BorderLayout());
		C_label[0]=new JLabel("目前得分");
		C_label[1]=new JLabel(String.valueOf(data.getComputerScore()));
		East1.add(C_label[0],BorderLayout.NORTH);
		East1.add(C_label[1],BorderLayout.NORTH);
		U_label[0]=new JLabel("目前得分");
		U_label[1]=new JLabel(String.valueOf(data.getUserScore()));
		East2.add(U_label[0],BorderLayout.SOUTH);
		East2.add(U_label[1],BorderLayout.SOUTH);
		East.add(East1,BorderLayout.NORTH);
		East.add(East2,BorderLayout.SOUTH);
		button=new JButton("Random");
		button.setBounds(400,150, 120, 50);
		Font myfont=new Font("標楷體",Font.PLAIN,24);
		Center.setLayout(null);
		Center_label[0]=new JLabel("123");
		Center_label[0].setFont(myfont);
		Center_label[1]=new JLabel("123");
		Center_label[1].setFont(myfont);
		Center_label[2]=new JLabel("按Random開始遊戲");
		Center_label[2].setFont(myfont);
		Center_label[0].setBounds(300,0, 200, 50);
		Center.add(Center_label[0]);
		Center_label[1].setBounds(300,300, 200, 50);
		Center.add(Center_label[1]);
		Center_label[2].setBounds(220,150, 360, 50);
		Center.add(Center_label[2]);
		East.add(button,BorderLayout.CENTER);
		button.addActionListener(this);
		
		setLayout(new BorderLayout());
		add(North,BorderLayout.NORTH);
		add(South,BorderLayout.SOUTH);
		//add(West,BorderLayout.WEST);
		add(East,BorderLayout.EAST);
		add(Center,BorderLayout.CENTER);
	}
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource() == button)
		{
			int[] ten=new int[10];
			ten=user.getRamdomTenCard();
			for(int i=0;i<10;i++)
			{
				image[i]=new ImageIcon("image/card/"+ten[i]+".png");
			}
			for(int i=0;i<5;i++)
			{
				North_label[i].setIcon(image[i]);
			}
			for(int i=0;i<5;i++)
			{
				South_label[i].setIcon(image[i+5]);
			}
			int[] Computer_card=new int[5];
			int[] User_card=new int[5];
			for(int i=0;i<5;i++)
			{
				Computer_card[i]=ten[i];
			}
			for(int i=0;i<5;i++)
			{
				User_card[i]=ten[i+5];
			}
			int[] Computer_Type=new int[2];
			int[] User_Type=new int[2];
			Computer_Type=user.checkType(Computer_card);
			User_Type=user.checkType(User_card);
			Center_label[0].setText(user.getTypeName(Computer_Type[0]));
			Center_label[1].setText(user.getTypeName(User_Type[0]));
			switch(user.VsCard(Computer_Type, User_Type))
			{
			case 0://平手
				Center_label[2].setText("平手!");
				break;
			case 1://電腦贏
				data.addComputerScore(1);
				C_label[1].setText(String.valueOf(data.getComputerScore()));
				Center_label[2].setText("你輸了!太大意了!");
				break;
			case 2://玩家贏
				data.addUserScore(1);
				U_label[1].setText(String.valueOf(data.getUserScore()));
				Center_label[2].setText("你贏了!");
				break;
			default:
				break;
			}
		}
	}

}
