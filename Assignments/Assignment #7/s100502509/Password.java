package a7.s100502509;

public class Password {
	
	private String password;

	public Password(String receiveNumber) throws InvalidPasswordException
	{

		setPassword(receiveNumber);
	}
	
	public void setPassword(String newPassword) throws InvalidPasswordException {
		password = newPassword;
		if (newPassword.length() < 7)//the length is not enough
			throw new InvalidPasswordException("The length of the password "+ password + " is not enough!!");
		else if (checkPassword(password) == 1)//empty
			throw new InvalidPasswordException("The password " + password+ " has no Number!!");
		else if (checkPassword(password) == 2)//empty
			throw new InvalidPasswordException("The password " + password+ " has no letter!!");
		else {
			System.out.println("The password " + password + " is valid!!");
		}
	}

	public int checkPassword(String Password1) {
		int EmptyLetter = 0;
		int EmptyNumber = 0;
		for (int i = 0; i < Password1.length(); i++) {//Check Whether is the letter
			if (Character.isLetter(Password1.charAt(i)))
				EmptyNumber++;
			else
				EmptyLetter++;
		}
		if (EmptyNumber == Password1.length())
			return 1;
		else if (EmptyLetter == Password1.length())
			return 2;
		else
			return 3;

	}

}
