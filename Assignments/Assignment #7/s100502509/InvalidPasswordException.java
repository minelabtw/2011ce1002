package a7.s100502509;

public class InvalidPasswordException extends Exception{
	private String password;

	public InvalidPasswordException(String Check) {
		super("The password "+Check+"in not correct!!");
		password = Check;
	}

	public String getString() {
		return password;
	}

}
