package a7.s972002502;

public class InvalidPasswordException extends Exception{
	/** Construct an exception with no message */
	public InvalidPasswordException() {
		super("Illegal password type");
	}
	
	/** Construct an exception with the password */
	public InvalidPasswordException(String password) {
		super(password);
	}

}
