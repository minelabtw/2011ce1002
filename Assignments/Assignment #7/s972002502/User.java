package a7.s972002502;
import java.awt.*;
import javax.swing.*;

public class User extends JPanel{
	protected JPanel Cards;
	protected ImageIcon Img1 = new ImageIcon("image/card/init_1.png");
	protected ImageIcon Img2 = new ImageIcon("image/card/init_2.png");
	protected ImageIcon Img3 = new ImageIcon("image/card/init_3.png");
	protected ImageIcon Img4 = new ImageIcon("image/card/init_4.png");
	protected ImageIcon Img5 = new ImageIcon("image/card/init_5.png");
	protected JLabel Card1 = new JLabel(Img1);
	protected JLabel Card2 = new JLabel(Img2);
	protected JLabel Card3 = new JLabel(Img3);
	protected JLabel Card4 = new JLabel(Img4);
	protected JLabel Card5 = new JLabel(Img5);
	
	public User(){
		// set GridLayout on panel Cards to hold 5 cards
		Cards = new JPanel();
		Cards.setLayout(new GridLayout(1, 5, 5, 5));
		Cards.add(Card1);
		Cards.add(Card2);
		Cards.add(Card3);
		Cards.add(Card4);
		Cards.add(Card5);
	}
	
	/** get the Cards */
	public JPanel getCards(){
		return Cards;
	}
	
	/** set card 1 */
	public void setCard1(String img1){
		this.Img1 = new ImageIcon(img1);
		Card1.setIcon(Img1);
	}
	
	/** set card 2 */
	public void setCard2(String img2){
		this.Img2 = new ImageIcon(img2);
		Card2.setIcon(Img2);
	}
	
	/** set card 3 */
	public void setCard3(String img3){
		this.Img3 = new ImageIcon(img3);
		Card3.setIcon(Img3);
	}
	
	/** set card 4 */
	public void setCard4(String img4){
		this.Img4 = new ImageIcon(img4);
		Card4.setIcon(Img4);
	}
	
	/** set card 5 */
	public void setCard5(String img5){
		this.Img5 = new ImageIcon(img5);
		Card5.setIcon(Img5);
	}

}
