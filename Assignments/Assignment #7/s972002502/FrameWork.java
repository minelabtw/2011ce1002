package a7.s972002502;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

public class FrameWork extends JFrame implements ActionListener{
	
	protected JPanel playerHe;								// create panel 'playerHe' for the information of play He.
	protected JPanel playerYou;								// create panel 'playerYou' for the information of play You.
	protected JPanel showCards;
	protected JPanel showResult;
	protected JPanel panel;

	protected JButton button = new JButton("random"); 		// create button 'random'.
	protected JLabel He = new JLabel(" He");				// create label 'He' 
	protected JLabel You = new JLabel("You");				// create label 'You' 
	protected JLabel resultHe = new JLabel("Result");		// create label 'resultHe' to show the result of His Card.
	protected JLabel resultYou = new JLabel("Result");		// create label 'resultYou' to show the result of Your Card.
	protected JLabel resultGame = new JLabel("Result");		// create label 'resultGame' to show the result of the game.
	
	protected User His = new User();						// create User 'His' for the random cards He got.
	protected User Your = new User();						// create User 'Your' for the random cards You got.
	
	public FrameWork (){
		// active button event here.
		button.addActionListener((ActionListener)this); 
		
		// create Font font1 & font2 to set the font
		Font font1 = new Font("SansSerif", Font.BOLD, 30);
		Font font2 = new Font("Serif", Font.BOLD+Font.ITALIC, 50);
		He.setFont(font1);
		You.setFont(font1);
		resultHe.setFont(font1);
		resultHe.setForeground(Color.BLUE);
		resultYou.setFont(font1);
		resultYou.setForeground(Color.GREEN);
		resultGame.setFont(font1);
		resultGame.setForeground(Color.ORANGE);
		button.setFont(font2);
		
		// set BorderLayout on panel playerHe to hold label He
		// and Panel Cards of User His 
		playerHe = new JPanel(new BorderLayout());
		playerHe.add(His.getCards(), BorderLayout.EAST);
		playerHe.add(He, BorderLayout.WEST);

		
		// set BorderLayout on panel playerYou to hold label You
		// and Panel Cards of User Your
		playerYou = new JPanel(new BorderLayout(5, 10));
		playerYou.add(Your.getCards(), BorderLayout.EAST);
		playerYou.add(You, BorderLayout.WEST);
		
		// set GridLayout on panel showCards to hold panel playerHe,
		// panel playerYou and button
		showCards = new JPanel(new GridLayout(3, 1));
		showCards.add(playerHe);
		showCards.add(playerYou);
		showCards.add(button);
		
		// set GridLayout on panel showResult to hold label resultHe,
		// label resultYou and label resultGame
		showResult = new JPanel(new GridLayout(3, 1));
		showResult.add(resultHe);
		showResult.add(resultYou);
		showResult.add(resultGame);
		
		// set BorderLayout on panel panel to hold panel showCards,
		// panel showResult
		panel = new JPanel(new BorderLayout(5, 70));
		panel.add(showCards,BorderLayout.WEST);
		panel.add(showResult,BorderLayout.EAST);

		
		// Set BorderLayout of frame and add contents into the frame
		setLayout(new BorderLayout(5, 70));
		add(panel,BorderLayout.CENTER);
	
	}
	
	/**  event handler */
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == button){
			
			Set<String> set = new HashSet<String>();		// new a String Set for store random numbers
			
			// provide random numbers to the set
			while (set.size() < 10) {
								
				int random = (int)(Math.random()*21);
				set.add(Integer.toString(random));
			}
			
	        String[] randomNum = new String[10];			// new a string array for store random numbers
	        Iterator<String> iterator = set.iterator();		// new a string Iterator to get the value of Set
	        int i = 0;
			while(iterator.hasNext()) {
	            randomNum[i] = iterator.next();
	            i++;
	        }
       
			// set His Cards
	        His.setCard1("image/card/"+ randomNum[0] +".png");
	        His.setCard2("image/card/"+ randomNum[1] +".png");
	        His.setCard3("image/card/"+ randomNum[2] +".png");
	        His.setCard4("image/card/"+ randomNum[3] +".png");
	        His.setCard5("image/card/"+ randomNum[4] +".png");
	        
	        // set Your Cards
	        Your.setCard1("image/card/"+ randomNum[5] +".png");
	        Your.setCard2("image/card/"+ randomNum[6] +".png");
	        Your.setCard3("image/card/"+ randomNum[7] +".png");
	        Your.setCard4("image/card/"+ randomNum[8] +".png");
	        Your.setCard5("image/card/"+ randomNum[9] +".png");
	        
	        // set the Result labels
	        setResultLabel(randomNum);
		}
	}
	
	/** set the Result labels */
	public void setResultLabel (String[] randomNum){
		int[] counter = new int[6];							// integer array 'counter' store the number of each card's type
		int rankHe = 0;										// store the rank of He 
		int rankYou = 0;									// store the rank of You 
		
		// count the number of each Hiscard's type
		for(int i=0; i<5; i++){
			if (Integer.valueOf(randomNum[i])/4==0){
				counter[0]++;
			}
			else if (Integer.valueOf(randomNum[i])/4==1){
				counter[1]++;
			}
			else if (Integer.valueOf(randomNum[i])/4==2){
				counter[2]++;
			}
			else if (Integer.valueOf(randomNum[i])/4==3){
				counter[3]++;
			}
			else if (Integer.valueOf(randomNum[i])/4==4){
				counter[4]++;
			}
			else{
				counter[5]++;
			}
		}

		// judge the rank of He
		rankHe = judgeResult(counter, 1);
		
		// initial integer array 'counter'
		counter = new int[6];	
		
		// count the number of each Yourcard's type
		for(int i=5; i<10; i++){
			if (Integer.valueOf(randomNum[i])/4==0){
				counter[0]++;
			}
			else if (Integer.valueOf(randomNum[i])/4==1){
				counter[1]++;
			}
			else if (Integer.valueOf(randomNum[i])/4==2){
				counter[2]++;
			}
			else if (Integer.valueOf(randomNum[i])/4==3){
				counter[3]++;
			}
			else if (Integer.valueOf(randomNum[i])/4==4){
				counter[4]++;
			}
			else{
				counter[5]++;
			}
		}
	
		// judge the rank of You
		rankYou = judgeResult(counter, 2);
		
		// judge the winner
		if(rankHe > rankYou){
			resultGame.setText("He WIN!!");
		}
		if(rankHe < rankYou){
			resultGame.setText("You WIN!!");
		}
		if(rankHe == rankYou){
			resultGame.setText("DROWN!!");
		}
	}
	
	/** judge the rank */
	public int judgeResult(int[] counter, int heORyou){
		int maxCountr = maxNum(counter);					// integer 'maxcountr' to store the largest number of counter's value 
		int countOne = countOne(counter);					// integer 'countOne' to store the number of counter which has value of one
		int rank = 0;										// store the rank number (the larger the priority higher)

		// judge the rank of He
		if(heORyou == 1){
			System.out.println("IN 111");
			if (counter[5] == 1){
				System.out.println("J");
				resultHe.setText("Joker");
				rank = 5;
			}
			else if (maxCountr == 4){
				System.out.println("Four");
				resultHe.setText("Four of a kind");
				rank = 4;
			}
			else if (maxCountr == 3 && countOne==0){
				System.out.println("Full");
				resultHe.setText("Full House");
				rank = 3;
			}
			else if(maxCountr == 3){
				System.out.println("Three");
				resultHe.setText("Three of a kind");
				rank = 2;
			}
			else if(maxCountr == 2 && countOne==1){
				System.out.println("Two");
				resultHe.setText("Two Pars");
				rank = 1;
			}
			else{
				System.out.println("No");
				resultHe.setText("Nothing");
			}
		}
		
		// judge the rank of You
		if(heORyou == 2){
			System.out.println("IN 222");
			if (counter[5] == 1){
				resultYou.setText("Joker");
				rank = 5;
			}
			else if (maxCountr == 4){
				resultYou.setText("Four of a kind");
				rank = 4;
			}
			else if (maxCountr == 3 && countOne==0){
				resultYou.setText("Full House");
				rank = 3;
			}
			else if(maxCountr == 3){
				resultYou.setText("Three of a kind");
				rank = 2;
			}
			else if(maxCountr == 2 && countOne==1){
				resultYou.setText("Two Pars");
				rank = 1;
			}
			else{
				resultYou.setText("Nothing");
			}
		}
		return rank;
	}
	
	/** find the max number of counter value */
	public int maxNum(int[] counter){
		int max_value = 0;
		int max_ptr = 0;
		
		for (int i = 0 ; i < counter.length ; i++) {
		  if (counter[i] > max_value) {
		    max_value = counter[i];
		    max_ptr = i;
		  }
		}
		counter[max_ptr] = 0;
		return max_value;
	}
	
	/** find the number of counter which has value of one */
	public int countOne(int[] counter){
		int one = 0;
		for (int i = 0 ; i < counter.length ; i++) {
			  if (counter[i] == 1) {
				  one++;
			  }
			}
		return one;
	}
}