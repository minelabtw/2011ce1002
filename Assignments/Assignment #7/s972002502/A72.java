package a7.s972002502;
import java.util.*;

public class A72 {
	public static void main (String[] args) {
		String password;							// String password to store the password user inputed
		Password psw = new Password();				// new a password
		Scanner input = new Scanner(System.in);		// new a scanner
		
		// let user input a password and judge whether its format is right or not
		while(true){
			System.out.println("Please input a password to varify: ");
			password = input.next();
			
			try {
				psw.setPassword(password);
			}
			catch (InvalidPasswordException ex){
				System.out.println(ex.getMessage());
				System.out.println("The password " + password + " in not correct!!");
				System.out.println("");
			}
		}
	}
}
