package a7.s972002502;

public class Password {
	protected String password;
	
	/** Construct a Password for initialization */
	public Password(){
		password = "";
	}

	/** Get the stored password */
	public String getPassword(){
		return password;
	}
	
	/** Check format and set the new password */
	public void setPassword(String password)throws InvalidPasswordException{
		// throw InvalidPasswordException if the length of password is less than 7 
		if(password.length()<7){
			throw new InvalidPasswordException("The length of the password " + password + " is not enough!!");
		}
		
		// throw InvalidPasswordException if there exists non-alphanumeric character in the password
		else if(flagElse(password)){
			throw new InvalidPasswordException("The password " + password + " contains non-alphanumeric characters!!");
		}
		
		// throw InvalidPasswordException if the password doesn't have any letter
		else if(!flagLetter(password)){
			throw new InvalidPasswordException("The password " + password + " has no letter!!");
		}
		
		// throw InvalidPasswordException if the password doesn't have any number
		else if(!flagNum(password)){
			throw new InvalidPasswordException("The password " + password + " has no number!!");
		}
		
		// the format of the password is right
		else{
			this.password = password;
			System.out.println("The password abc1234 is valid!!");
		}
	}
	
	/** Check whether the password has non-alphanumeric or not */
	public boolean flagNum(String password) {
		boolean flagNum = false;
		for(int i=0; i<password.length();i++){
			int ascii = (int)password.charAt(i);
			if(ascii>=48&&ascii<=57){
				flagNum = true;
			}
		}
		return flagNum;
	}
	
	/** Check whether the password has letter or not */
	public boolean flagLetter(String password) {
		boolean flagLetter = false;
		for(int i=0; i<password.length();i++){
			int ascii = (int)password.charAt(i);
			if((ascii>=65&&ascii<=90) || (ascii>=97&&ascii<=122)){
				flagLetter = true;
			}
		}
		return flagLetter;
	}

	
	/** Check whether the password has letter or not */
	public boolean flagElse(String password) {
		boolean flagElse = false;
		for(int i=0; i<password.length();i++){
			int ascii = (int)password.charAt(i);
			if(!(ascii>=48&&ascii<=57) && !(ascii>=65&&ascii<=90) && !(ascii>=97&&ascii<=122)){
				flagElse = true;
			}
		}
		return flagElse;
	}
}
