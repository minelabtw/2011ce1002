package a7.s100502518;

public class User {
	private char []card=new char[11];
	private String types;
	
	public void setcard(int[] c)//先判斷什麼牌 
	{
		for(int a=1;a<=10;a++)
		{
			if(0<=c[a]&&c[a]<=3)
			{
				card[a]='A';
			}
			
			else if(3<c[a]&&c[a]<8)
			{
				card[a]='2';
			}
			
			else if (7<c[a]&&c[a]<12)
			{
				card[a]='J';
			}
			
			else if (11<c[a]&&c[a]<16)
			{
				card[a]='Q';
			}
			
			else if (15<c[a]&&c[a]<20)
			{
				card[a]='K';
			}
			
			else
			{
				card[a]='G';
			}
		}
	}
	
	public void settypes(char a,char b,char c,char d,char e)//判斷牌型
	{
		int count=0;
		char []store=new char[5];
		store[0]=a;
		store[1]=b;
		store[2]=c;
		store[3]=d;
		store[4]=e;
		
		for(int f=1;f<=4;f++)
		{
			for(int g=(f-1);g>=0;g--)
			{
				if(store[f]==store[g])
				{
					count++;
				}
			}
		}
		if(a=='G'||b=='G'||c=='G'||d=='G'||e=='G')
		{
			types="Joker";
		}
		else if(count==6)//用pair數來計算是哪種牌行
		{
			types="Four of a kind";
		}
		
		else if (count==4)
		{
			types="Full House";
		}
		
		else if (count==3)
		{
			types="Three of kind";
		}
		
		else if (count==2)
		{
			types="Two pairs";
		}
		
		else
		{
			types="Nothing";
		}
	}
	
	public char getcard(int a)
	{
		return card[a];
	}
	
	public String gettypes() 
	{
		return types;
	}
}