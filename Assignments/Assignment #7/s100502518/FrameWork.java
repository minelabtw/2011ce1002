package a7.s100502518;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
public class FrameWork extends JFrame implements ActionListener{
	private int []n=new int[6];//宣告需要的東西
	private ImageIcon[] init_Icon=new ImageIcon[6];
	
	JButton button = new JButton("random");
	JPanel p1,p2;
	JLabel []card=new JLabel [11];
	JLabel c1,c2,c3,w1,w2;
	Border linebBorder= new LineBorder(Color.black, 2);
	Font font1=new Font("sansSerif",Font.ITALIC,25);
	Font font2=new Font("Serif",Font.ITALIC,50);
	
	public FrameWork ()
	{
		for(int a=1;a<=5;a++)//用迴圈設定數值並建構p1
		{	
			n[a]=a;
		}
		
		for(int a=1;a<=5;a++)
		{
			init_Icon[a]=new ImageIcon("image/card/init_"+a+".png");
		}
		
		p1=new JPanel(new GridLayout(2,7));
		w1=new JLabel("He");
		w1.setFont(font1);
		p1.add(w1);
		for(int a=1;a<=5;a++)
		{
			card[a]=new JLabel(init_Icon[a]);
			card[a].setBorder(linebBorder);
			p1.add(card[a]);
		}
		p1.add(c1=new JLabel());
		w2=new JLabel("You");
		w2.setFont(font1);
		p1.add(w2);
		for(int a=1;a<=5;a++)
		{
			card[a+5]=new JLabel(init_Icon[a]);
			card[a+5].setBorder(linebBorder);
			p1.add(card[a+5]);
		}
		p1.add(c2=new JLabel());

		p2=new JPanel(new GridLayout(1,2));//建構p2
		p2.add(button);
		p2.add(c3=new JLabel());
		
		setLayout(new BorderLayout());//組合
		add(p1,BorderLayout.NORTH);
		add(p2,BorderLayout.CENTER);
		button.addActionListener(this);
	}
	
	public void actionPerformed(ActionEvent e)
	{
		int []storecard=new int [11];
		if(e.getSource() == button)
		{
			for(int a=1;a<=10;a++)//亂數決定圖片並避免重複
			{
				int temp=(int)(Math.random()*21);
				int b=a;
				while(b>1)
				{
					if(temp==storecard[b-1])
					{
						temp=(int)(Math.random()*21);
						b=a;
					}
					else
					{
						b--;
					}
						
				}
				storecard[a]=temp;
				card[a].setIcon(new ImageIcon("image/card/"+temp+".png"));
				
			User user=new User();//把牌丟進另一個class並判斷牌型
			user.setcard(storecard);
			user.settypes(user.getcard(1),user.getcard(2),user.getcard(3),user.getcard(4),user.getcard(5));
			c1.setText(user.gettypes());
			c1.setFont(font1);
			user.settypes(user.getcard(6),user.getcard(7),user.getcard(8),user.getcard(9),user.getcard(10));
			c2.setText(user.gettypes());
			c2.setFont(font1);
			c3.setFont(font2);
			if(c1.getText()==c2.getText())//比較輸贏
			{
				c3.setText("Draw");
			}
			
			else if(c1.getText()=="Joker")
			{
				c3.setText("You Lose");
			}
			
			else if(c2.getText()=="Joker")
			{
				c3.setText("You Win");
			}
			
			else if(c1.getText()=="Four of a kind")
			{
				c3.setText("You Lose");
			}
			
			else if(c2.getText()=="Four of a kind")
			{
				c3.setText("You Win");
			}
			
			else if(c1.getText()=="Full House")
			{
				c3.setText("You Lose");
			}
			
			else if(c2.getText()=="Full House")
			{
				c3.setText("You Win");
			}
			
			else if(c1.getText()=="Three of kind")
			{
				c3.setText("You Lose");
			}
			
			else if(c2.getText()=="Three of kind")
			{
				c3.setText("You Win");
			}
			
			else if(c1.getText()=="Two pairs")
			{
				c3.setText("You Lose");
			}
			
			else
			{
				c3.setText("You Win");
			}
		}

		setLayout(new BorderLayout());
		add(p1,BorderLayout.NORTH);
		add(p2,BorderLayout.CENTER);
		}
	}
	
}