package a7.s100502518;

public class Password {
	private String password;
	private int []store=new int[7];
	
	public void setpassword(String a)
	{
		password=a;
	}
	
	public boolean getresult()
	{
		try//都正確的話就true
		{
			checklength();
			checkletternumber();
			System.out.println("The password "+ password +" is valid!!");
			return true;
		}
		
		catch(InvalidPasswordException ex)//沒的話就false並顯示錯誤訊息
		{
			System.out.println(ex.getMessage());
			System.out.println("The password "+ password +" is not correct!!");
			return false;
		}
	}
	
	public void checklength() throws InvalidPasswordException
	{
		if(password.length()<7)
		{
			throw new InvalidPasswordException("The length of the password "+ password +" is not enough!!");
		}
	}
	
	public void checkletternumber() throws InvalidPasswordException
	{
		int check=0;
		for(int a=0;a<=6;a++)
		{
			store[a]=(int)password.charAt(a);
			if(47<store[a]&&store[a]<58)//如果都數字就在這區間，有的畫+1
			{
				check++;
			}
		}
		
		if(check==0)//沒數字
		{
			throw new InvalidPasswordException("The password "+" has no number!!");
		}
		
		else if(check==7)//全數字
		{
			throw new InvalidPasswordException("The password "+" has no letter!!");
		}
	}

}
