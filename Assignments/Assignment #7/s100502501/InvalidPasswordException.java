package a7.s100502501;
public class InvalidPasswordException extends Exception {
	public InvalidPasswordException(String pw){//constructor
		boolean alphaflag=true;
		boolean numberflag=true;	
		if(pw.length()<7) //if length<7
			System.out.print("The length of the password "+pw+" is not enough!!"+
							"\nThe password "+pw+" in not correct!!\n");
		else{
			for(int i=0; i<pw.length(); i++) {
				if((pw.charAt(i)>='a'&&pw.charAt(i)<='z')||(pw.charAt(i)>='A'&&pw.charAt(i)<='Z')) //find a letter
					alphaflag=false;
				if((pw.charAt(i)>='0'&&pw.charAt(i)<='9')) //find a number
					numberflag=false;
			 }
			if(alphaflag==true){ //situation of no letter
				System.out.print("The password "+pw+" has no letter!!"+
						"\nThe password "+pw+" in not correct!!\n");
			}
			if(numberflag==true) //situation of no number
				System.out.print("The password "+pw+" has no number!!"+
						"\nThe password "+pw+" in not correct!!\n");
		}
	}
}
