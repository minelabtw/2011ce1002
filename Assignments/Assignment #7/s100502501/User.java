package a7.s100502501;
import java.util.*;
import javax.swing.*;
public class User {
	protected int[] pt1=new int[6]; 
	protected int[] pt2=new int[6]; 
	protected ImageIcon[] cards=new ImageIcon[21]; //create imageicon
	protected int[] jdg=new int[21]; 
	protected ImageIcon[] p1=new ImageIcon[10];
	protected ImageIcon[] p2=new ImageIcon[5];
	protected ImageIcon[] p3=new ImageIcon[5];
	protected String type="";
	boolean flag1=false; //boolean flag
	boolean flag2=false;
	boolean flag3=false;
	public User(){
		for(int i=0;i<21;i++){
			cards[i]=new ImageIcon("image/card/"+i+".png"); //store image into array
			jdg[i]=i;
		}
		Random rr=new Random();
		int i=0;
		while(i<10){ //random method
			int g=rr.nextInt(21-i);
			p1[i]=cards[g];
			if(i>=0&&i<5){
				if(jdg[g]==0||jdg[g]==1||jdg[g]==2||jdg[g]==3)
					pt1[0]++;
				else if(jdg[g]==4||jdg[g]==5||jdg[g]==6||jdg[g]==7)
					pt1[1]++;
				else if(jdg[g]==8||jdg[g]==9||jdg[g]==10||jdg[g]==11)
					pt1[2]++;
				else if(jdg[g]==12||jdg[g]==13||jdg[g]==14||jdg[g]==15)
					pt1[3]++;
				else if(jdg[g]==16||jdg[g]==17||jdg[g]==18||jdg[g]==19)
					pt1[4]++;
				else if(jdg[g]==20)
					pt1[5]++;
			}
			if(i>=5&&i<10){
				if(jdg[g]==0||jdg[g]==1||jdg[g]==2||jdg[g]==3)
					pt2[0]++;
				else if(jdg[g]==4||jdg[g]==5||jdg[g]==6||jdg[g]==7)
					pt2[1]++;
				else if(jdg[g]==8||jdg[g]==9||jdg[g]==10||jdg[g]==11)
					pt2[2]++;
				else if(jdg[g]==12||jdg[g]==13||jdg[g]==14||jdg[g]==15)
					pt2[3]++;
				else if(jdg[g]==16||jdg[g]==17||jdg[g]==18||jdg[g]==19)
					pt2[4]++;
				else if(jdg[g]==20)
					pt2[5]++;
			}
			i++;
			for(int j=g;j<cards.length-1;j++){
				cards[j]=cards[j+1];
				jdg[j]=jdg[j+1];
			}
		}
		for(int o=0;o<5;o++){ //array of His card
			p2[o]=p1[o];	
		}
		for(int o=5;o<10;o++){ //array of Your card
			p3[o-5]=p1[o];
		}
	}
	public int Priority(int[] tp){ //judge Priority
		for(int i=0;i<4;i++){
			for(int j=1;j<5;j++){
				if(tp[i]==2&&tp[j]==3){
					flag1=true;
					i=4;
					break;
				}
				else if(tp[j]==2&&tp[i]==3){
					flag1=true;
					i=4;
					break;
				}
			}
		}
		for(int i=0;i<4;i++){
			for(int j=1;j<5;j++){
				if(tp[i]==3&&tp[j]!=2){
					flag2=true;
					i=4;
					break;
				}
				else if(tp[i]!=2&&tp[j]==3){
					flag2=true;
					i=4;
					break;
				}
				else 
					flag2=false;
			}
		}
		for(int i=0;i<4;i++){
			for(int j=1;j<5;j++){
				if(tp[i]==2&&tp[j]==2&&tp[i]!=1&&tp[j]!=1&&tp[i]!=3&&tp[j]!=3){
					flag3=true;
					i=4;
					break;
				}
				else
					flag3=false;
			}
		}
		if(tp[5]==1){
			return 1;
		}
		else if(tp[0]==4||tp[1]==4||tp[2]==4||tp[3]==4||tp[4]==4){
			return 2;
		}
		else if(flag1==true){			
			return 3;
		}
		else if(flag2==true){			
			return 4;
		}
		else if(flag3==true){
			return 5;
		}
		else if(flag1==false||flag2==false||flag3==false)
			return 6;
		else
			return 6;
	}
	public String Type1(int[] pt1){ //judge Type of card
		if(Priority(pt1)==1)
			type="<Joker>";
		else if((Priority(pt1)==2))
			type="<Four of a kind>";
		else if((Priority(pt1)==3))
			type="<Full House>";
		else if((Priority(pt1)==4))
			type="<Three of kind>";
		else if((Priority(pt1)==5))
			type="<Two pairs>";
		else if((Priority(pt1)==6))
			type="<Nothing>";
		return type;
	}
	
}