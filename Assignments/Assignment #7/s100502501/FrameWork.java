package a7.s100502501;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
public class FrameWork extends JFrame implements ActionListener{
	JButton button = new JButton("random"); //create button
	ImageIcon ini1=new ImageIcon("image/card/init_1.png"); //store image
	ImageIcon ini2=new ImageIcon("image/card/init_2.png");
	ImageIcon ini3=new ImageIcon("image/card/init_3.png");
	ImageIcon ini4=new ImageIcon("image/card/init_4.png");
	ImageIcon ini5=new ImageIcon("image/card/init_5.png");
	JLabel a1=new JLabel("He");//create label
	JLabel a2=new JLabel(ini1);
	JLabel a3=new JLabel(ini2);
	JLabel a4=new JLabel();
	JLabel a5=new JLabel();
	JLabel a6=new JLabel();
	JLabel b1=new JLabel("You");
	JLabel b2=new JLabel();
	JLabel b3=new JLabel();
	JLabel b4=new JLabel(ini3);
	JLabel b5=new JLabel(ini4);
	JLabel b6=new JLabel(ini5);
	JLabel c1=new JLabel();
	JLabel c2=new JLabel();
	JLabel c3=new JLabel();
	public FrameWork(){
		JPanel p1=new JPanel(new GridLayout(1,6)); //create panel and set GridLayout
		JPanel p4=new JPanel(new GridLayout(1,6));
		JPanel p2=new JPanel(new GridLayout(3,1));
		JPanel p3=new JPanel(new GridLayout(3,1));
		
		Border linebd=new LineBorder(Color.BLACK); //create a border
		a2.setBorder(linebd); //set label's border
		a3.setBorder(linebd);
		a4.setBorder(linebd);
		a5.setBorder(linebd);
		a6.setBorder(linebd);
		b2.setBorder(linebd);
		b3.setBorder(linebd);
		b4.setBorder(linebd);
		b5.setBorder(linebd);
		b6.setBorder(linebd);
		a1.setFont(new Font("SansSerif",Font.BOLD,20)); //set font
		b1.setFont(new Font("SansSerif",Font.BOLD,20));
		c3.setFont(new Font("Serif",Font.BOLD,36));
		p1.add(a1); //add label to panel
		p1.add(a2);
		p1.add(a3);
		p1.add(a4);
		p1.add(a5);
		p1.add(a6);
		p4.add(b1);
		p4.add(b2);
		p4.add(b3);
		p4.add(b4);
		p4.add(b5);
		p4.add(b6);
		p2.add(c1);
		p2.add(c2);
		p2.add(c3);
		p3.add(p1);
		p3.add(p4);
		p3.add(button); //add button to panel
		add(p3,BorderLayout.WEST); //add panel to frame and set BorderLayout
		add(p2,BorderLayout.EAST);
		button.addActionListener(this); //active button event here
	}
	
	public void actionPerformed(ActionEvent e){
		if(e.getSource()==button){//what do you want when you push "random" button? Obviously, random card!
			User user=new User();
			a2.setIcon(user.p2[0]); //set image to label
			a3.setIcon(user.p2[1]);
			a4.setIcon(user.p2[2]);
			a5.setIcon(user.p2[3]);
			a6.setIcon(user.p2[4]);
			b2.setIcon(user.p3[0]);
			b3.setIcon(user.p3[1]);
			b4.setIcon(user.p3[2]);
			b5.setIcon(user.p3[3]);
			b6.setIcon(user.p3[4]);
			c1.setText(user.Type1(user.pt1)); //set text of label
			c2.setText(user.Type1(user.pt2));
			String result="";
			if(user.Priority(user.pt1)==user.Priority(user.pt2))
				result="Draw";
			else if(user.Priority(user.pt1)>user.Priority(user.pt2))
				result="You Win";
			else if(user.Priority(user.pt1)<user.Priority(user.pt2))
				result="You Lose";
			c3.setText(result);
		}
	}
}
