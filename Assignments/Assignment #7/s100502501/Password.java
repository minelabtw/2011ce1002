package a7.s100502501;
public class Password{
	public String pw="";
	public Password(String pw) throws InvalidPasswordException{	
			boolean alphaflag=true;
			boolean numberflag=true;
			for(int i=0; i<pw.length(); i++) {
				if((pw.charAt(i)>='a'&&pw.charAt(i)<='z')||(pw.charAt(i)>='A'&&pw.charAt(i)<='Z')){//find a letter
					alphaflag=false;
				}
				if((pw.charAt(i)>='0'&&pw.charAt(i)<='9')){ //find a number
					numberflag=false;						
				}	
			}
			if(pw.length()<7) //if length<7, throw into InvalidPasswordException
				throw new InvalidPasswordException(pw);
			if(alphaflag==true) //if there's no letter, throw into InvalidPasswordException
				throw new InvalidPasswordException(pw);
			if(numberflag==true) //if there's no number, throw into InvalidPasswordException
				throw new InvalidPasswordException(pw);				
		}
}
