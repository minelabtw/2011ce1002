package a7.s985003038;

public class InvalidPasswordException extends Exception {						// InvalidPasswordException class that extends Exception
	private static final long serialVersionUID = 1L;

	public InvalidPasswordException(String message, String newPassword){		// constructor that print the warning message
		System.out.println(message);
		System.out.println("The password " + newPassword + " is not correct!!");
	}
}
