package a7.s985003038;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;

public class FrameWork extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	private JLabel computerCards[] = new JLabel[5];			// here use the JLabel
	private JLabel playerCards[] = new JLabel[5];
	private JButton start = new JButton("Start");			// and also JButton
	private JButton add = new JButton("+");
	private JButton minus = new JButton("-");
	JLabel computerLabel = new JLabel("Computer");
	JLabel playerLabel = new JLabel("Player");
	JLabel bet = new JLabel("0 / 300");
	JLabel result = new JLabel();

	private User computer = new User();						// creating two user, one computer and one player
	private User player = new User();
	private int money = 300;								// the initialized money
	private int betMoney = 0;
	
	public FrameWork(){
		setTitle("Assignment #7");
		setSize(800, 600);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout(2,1));					// here is the BorderLayout
		
		JPanel main = new JPanel();
		main.setLayout(new GridLayout(2,6));				// and also GridLayout
		computerLabel.setForeground(Color.blue);			// then, the font style
		computerLabel.setFont(new Font("Arial", Font.BOLD, 15));
		computerLabel.setHorizontalAlignment(JLabel.CENTER);
		main.add(computerLabel);
		
		for(int i = 0; i < 5; i++){							// the ImageIcon and the Border
			computerCards[i] = new JLabel(new ImageIcon("image/card/back.png"));
			computerCards[i].setBorder(new LineBorder(Color.black, 1));
			main.add(computerCards[i]);						// creating the computer's cards
		}
		
		playerLabel.setForeground(Color.red);
		playerLabel.setFont(new Font("Arial", Font.BOLD, 15));
		playerLabel.setHorizontalAlignment(JLabel.CENTER);
		main.add(playerLabel);
		
		for(int i = 0; i < 5; i++){
			playerCards[i] = new JLabel(new ImageIcon("image/card/back.png"));
			playerCards[i].setBorder(new LineBorder(Color.black, 1));
			main.add(playerCards[i]);						// creating the player's cards
		}
		
		JPanel bottom = new JPanel();
		bottom.setLayout(new GridLayout(1,5));
		result.setForeground(Color.red);
		result.setFont(new Font("Arial", Font.BOLD, 15));
		result.setHorizontalAlignment(JLabel.CENTER);
		bottom.add(result);
		minus.addActionListener(this);						// if click the minus button, minus the money going to be bet
		bottom.add(minus);
		bet.setHorizontalAlignment(JLabel.CENTER);
		bottom.add(bet);
		add.addActionListener(this);						// if click the add button, add the money going to be bet
		bottom.add(add);
		start.addActionListener(this);						// if click the start button, start the game
		bottom.add(start);
		
		add(main, BorderLayout.CENTER);
		add(bottom, BorderLayout.SOUTH);
	}
	
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == start){							// start the game
			if(betMoney > 0){								// if bet money is 0, the game will not start
				User.shuffle();								// shuffle the cards
				computer.getYourCard(0);					// get computer's cards
				player.getYourCard(5);						// and also get player's cards
				for(int i = 0; i < 5; i++){					// show the cards images
					computerCards[i].setIcon(new ImageIcon("image/card/" + (computer.getCard(i)+1) + ".png"));
					playerCards[i].setIcon(new ImageIcon("image/card/" + (player.getCard(i)+1) + ".png"));
				}
															// show the results
				computerLabel.setText("<html>Computer<br>" + computer.getStateName() + "</html>");
				playerLabel.setText("<html>Player<br>" + player.getStateName() + "</html>");
				if(computer.getState() > player.getState()){
					result.setText(" Result: You Lose!");
					money -= betMoney;						// if you lose, you also lose your money
				} else if(computer.getState() < player.getState()){
					result.setText(" Result: You Win!");
					money += betMoney;						// if you win, you will be paid
				} else
					result.setText(" Result: Draw!");
				
				betMoney = money > betMoney ? betMoney : money;
				bet.setText(betMoney + " / " + money);		// update the JLabel to show the state of your money
				
				if(money == 0){								// if you has no money, end the game
					JOptionPane.showMessageDialog(null, "You have lost all your money! See you!", "Assignment #7", JOptionPane.WARNING_MESSAGE);
					System.exit(0);
				}
			} else {										// if bet money is 0, show tips message
				JOptionPane.showMessageDialog(null, "You cannot bet with $0", "Asignment #7", JOptionPane.WARNING_MESSAGE);
			}
		} else if(e.getSource() == add){					// add bet money
			betMoney += money > betMoney + 100 ? 100 : money - betMoney;
			bet.setText(betMoney + " / " + money);
		} else if(e.getSource() == minus){					// minus bet money
			betMoney -= betMoney >= 100 ? 100 : betMoney;
			bet.setText(betMoney + " / " + money);
		}
	}
}
