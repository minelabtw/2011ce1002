package a7.s985003038;

public class Password {
	private String password;
	
	public Password(String newPassword) throws InvalidPasswordException {			// constructor that call the set password method with exception handler
		setPassword(newPassword);
	}
	
	public void setPassword(String newPassword) throws InvalidPasswordException {	// set password method with exception handler
		boolean digitFlag = false, alphaFlag = false;								// using flags to consider if there is at least one letter or one digit in the input password  
		for(int i = 0; i < newPassword.length(); i++)								// check the whole string if there is at least one digit and one letter
			if(Character.isDigit(newPassword.charAt(i))) digitFlag = true;			// if a digit is found, rise the digit flag 
			else if(Character.isLetter(newPassword.charAt(i))) alphaFlag = true;	// if a letter is found, rise the alphabet flag
		
		if(newPassword.length() < 7)												// if the length of the input password is too short, throw an exception
			throw new InvalidPasswordException("The length of the password " + newPassword + " is not enough!!", newPassword);
		else if(!digitFlag)															// if there is no digits in the password, throw an exception
			throw new InvalidPasswordException("The password " + newPassword + " has no number!!", newPassword);
		else if(!alphaFlag)															// if there is no letters in the password, throw an exception
			throw new InvalidPasswordException("The password " + newPassword + " has no letter!!", newPassword);	
		else {																		// otherwise, set the password, and show successful message
			System.out.println("The password " + newPassword + " is valid!!");
			password = newPassword;
		}
	}
	
	public String getPassword(){													// get password method
		return password;
	}
}
