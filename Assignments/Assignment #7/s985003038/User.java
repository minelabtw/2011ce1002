package a7.s985003038;

import javax.swing.JOptionPane;

public class User {												// User class
	private final static int totalCards = 54;					// static variable to control the number of cards
	private static int[] cards = new int[totalCards];			// static array to content the cards inside
	private int[] yourCards = new int[5];						// array to keep your five cards
	private int state;											// your state, which are Joker, Four of a Kind, Full House, Three of Kind, Two Pairs, One Pair, and only One Card
	private String[] stateName = {"None", "One Card", "One Pair", "Two Pairs", "Three of Kind", "Full House", "Four of a Kind", "Joker"};
	
	static {
		for(int i = 0; i < totalCards; i++)						// create the cards
			cards[i] = i;
	}
	
	public static void shuffle(){								// a static shuffle method, which will shuffle the cards by swapping the elements in the card array
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < totalCards; j++){				// to ensure each card has been swapped
				int swapIndex = (int)(Math.random()*totalCards);// decided which index of card are going to be swap
				int temp = cards[swapIndex];					// do the swapping operation
				cards[swapIndex] = cards[j];
				cards[j] = temp;
			}
		}
	}
	
	public void getYourCard(int startIndex){					// method to get 5 continues cards start from the specified index from the card array
		state = 1;												// reset your state, assume you get 5 different cards
		for(int i = 0; i < 5; i++)								// get your 5 cards
			yourCards[i] = cards[i+startIndex];
		calculateState();										// calculate your state according your new cards
	}
	
	private void calculateState(){								// method to calculate your state
		int cards[] = new int[(int)Math.floor(totalCards/4)];	// create an array to record the number of each kind of cards
		for(int i = 0; i < 5; i++)
			if(yourCards[i] >= totalCards-totalCards%4){		// if you have a joker in your hand
				state = 7;										// set your state to 7, and finish the calculation
				return;
			} else												// otherwise, counting each kind of cards
				cards[yourCards[i]%(int)Math.floor(totalCards/4)]++;
		for(int i = 0; i < Math.floor(totalCards/4); i++)		// decide what is the state according to the counting result
			if(cards[i] == 2)									// if there is a pair
				state += 1;
			else if(cards[i] == 3)								// if there are three cards of a kind
				state += 3;
			else if(cards[i] == 4)								// if there are four cards of a kind
				state = 6;
	}
	
	public int getCard(int index){								// method to get a card at the specified index
		try {
			if(index >= 0 && index < 5)
				return yourCards[index];
			else												// if the reading index is out of range, throw an exception
				throw new Exception("Warning: index out of range");
		} catch(Exception ex){
			JOptionPane.showMessageDialog(null, "Warning: index out of range", "Assignment #7", JOptionPane.ERROR_MESSAGE);
			return -1;
		}
	}
	
	public int getState(){										// method to get the state
		return state;
	}
	
	public String getStateName(){								// method to get the name of the state
		return stateName[state];
	}
}
