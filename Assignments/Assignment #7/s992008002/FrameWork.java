/*
 * Design GUI here.
You have to use the classes such as: GridLayout, Font, Boder, JLabel, JButton, ImageIcon, BorderLayout
You have to show the layout such as(at least): ��random button��, ��the 2 sets of cards��, ��the type of cards��, ��game result��.
Please unzip the image.zip into the project directory and the path of the cards are ��image/card/xxxx.png��
Here is the sample layout:
 */


package a7.s992008002;

public class FrameWork extends JFrame implements ActionListener{
	
	JButton button = new JButton("random"); //create button 
	
	public FrameWork (){
			...
		button.addActionListener(this); //active button event here
			...
	}
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == button){
			//what do you want when you push "random" button? Obviously, random card!
		}
	}
			...
}
