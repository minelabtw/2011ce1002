package a7.s992008002;

public class InvalidPasswordException extends Exception {


	
	private String password;
	
	public InvalidPasswordException(String notenough){
		super("The length of the password "+ notenough +"is not enough!!");
		this.password = notenough;
	}
	
	public InvalidPasswordException(String hasNonumber){
		super("The length of the password "+ hasNonumber +"has no number!!");
		this.password = hasNonumber;
	}
	
	public InvalidPasswordException(String hasNoLetter){
		super("The length of the password "+ hasNoLetter +"has no letter!!");
		this.password = hasNoLetter;
	}
	
	
	

}


