package a7.s100502517;

public class Password {
	private boolean run = false;
	private int[] list = new int[7];
	private boolean number = true;//預設沒有數字
	private boolean letter = true;//預設沒有文字
	
	public Password(String password) throws InvalidPasswordException{
		int k = this.judgement(password);
		if(run){
			throw new InvalidPasswordException(k, password);//如果錯誤就丟進去
		}
	}
	
	//判斷
	public int judgement(String password){
		int length = password.length();
		//長度的判斷
		if(length != 7){
			run = true;
			return 1;
		}
		else{
			for(int i = 0; i < 7; i++){
				list[i] = password.charAt(i);
			}			
			
			//利用ASCII來判斷是否為 數字 或 文字
			for(int i = 0; i < 7; i++){
				if((list[i]-60) < 0){
					number = false;
				}
				else{
					letter = false;
				}
			}
			
			if(letter){
				run = true;
				return 2;
			}			
			if(number){
				run = true;
				return 3;
			}
			else{
				return 0;
			}
			
		}
	}
	
}
