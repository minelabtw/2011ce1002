package a7.s100502517;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class FrameWork extends JFrame implements ActionListener{
	JButton button = new JButton("random"); //create button 

	JLabel cPlease = new JLabel(new ImageIcon("image/card/init_1.png"));//初始化圖片
	JLabel cPress = new JLabel(new ImageIcon("image/card/init_2.png"));
	JLabel cRandom = new JLabel(new ImageIcon("image/card/init_3.png"));
	JLabel cButton = new JLabel(new ImageIcon("image/card/init_4.png"));
	JLabel cLitpon = new JLabel(new ImageIcon("image/card/init_5.png"));
	
	JLabel pPlease = new JLabel(new ImageIcon("image/card/init_1.png"));
	JLabel pPress = new JLabel(new ImageIcon("image/card/init_2.png"));
	JLabel pRandom = new JLabel(new ImageIcon("image/card/init_3.png"));
	JLabel pButton = new JLabel(new ImageIcon("image/card/init_4.png"));
	JLabel pLitpon = new JLabel(new ImageIcon("image/card/init_5.png"));
	
	JLabel cardtypeone = new JLabel("          <Welcome To>");//初始化
	JLabel cardtypetwo = new JLabel("          <Poker Game>");
	JLabel cardresault = new JLabel("Win or Lose");	
	
	private ImageIcon[] pic = new ImageIcon[21];
	
	public FrameWork (){
		
		for(int read = 0; read < 21; read++){
			pic[read] = new ImageIcon("image/card/"+read+".png");//讀圖存入陣列
		}
		
		Font largeFont = new Font("TimesRoman", Font.BOLD,25);//字體型態、大小
		Font largestFont = new Font("TimesRoman", Font.BOLD,50);
		Border lineBorder = new LineBorder(Color.BLACK,2);//邊框
		
	
		//創立一個JPanel p2去存取  6個JLabel 的物件
		JPanel p2 = new JPanel();
		p2.setLayout(new GridLayout(1,6));		
		JLabel computer = new JLabel("Computer");		
		
		cPlease.setBorder(lineBorder);
		cPress.setBorder(lineBorder);
		cRandom.setBorder(lineBorder);
		cButton.setBorder(lineBorder);
		cLitpon.setBorder(lineBorder);
		
		computer.setFont(largeFont);//設定字體
		
		p2.add(computer);		
		p2.add(cPlease);
		p2.add(cPress);
		p2.add(cRandom);
		p2.add(cButton);
		p2.add(cLitpon);		
		
		
		//創建一個JPanel p3去存取  6個JLabel 的物件
		JPanel p3 = new JPanel();
		p3.setLayout(new GridLayout(1,6));		
		JLabel player = new JLabel("Player");
		
		pPlease.setBorder(lineBorder);//設定邊框
		pPress.setBorder(lineBorder);
		pButton.setBorder(lineBorder);
		pRandom.setBorder(lineBorder);
		pLitpon.setBorder(lineBorder);
		
		player.setFont(largeFont);//設定字體
		
		p3.add(player);
		p3.add(pPlease);		
		p3.add(pPress);
		p3.add(pRandom);
		p3.add(pButton);
		p3.add(pLitpon);		
		
		
		//創建一個JPanel p5 去存取 左邊  2 個JPanel(玩家和電腦的) 和一個JButton(Random) 
		JPanel p5 = new JPanel();
		p5.setLayout(new GridLayout(3,1,5,0));		
		p5.add(p2,BorderLayout.NORTH);
		p5.add(p3,BorderLayout.CENTER);
		p5.add(button,BorderLayout.SOUTH);
		
		
		//創建一個JPanel p6 去存取 右邊  3個JLabel(用來顯示結果)的物件
		JPanel p6 = new JPanel();
		p6.setLayout(new GridLayout(3,1,5,40));	//50 是 上 下 的 距 離	
		cardtypeone.setFont(largeFont);
		cardtypetwo.setFont(largeFont);
		cardresault.setFont(largestFont);
		p6.add(cardtypeone);
		p6.add(cardtypetwo);
		p6.add(cardresault);
		
		setLayout(new FlowLayout(FlowLayout.LEFT,2,1));//
		add(p5);
		add(p6);		
			
		button.addActionListener(this); //active button event here
			
	}
	public void actionPerformed(ActionEvent e){
		//如果 按下 random 這個按鈕的話
		if(e.getSource() == button){
			int[] allcard = new int[10];//10張 牌
			int[] computercard = new int[5]; //電腦的5張牌
			int[] playercard = new int[5];// 玩家的5張牌
			
			User card = new User();
			User computer = new User();
			User player = new User();
			allcard = card.Wash();//洗牌
			
			//隨機洗出10張牌~~5張給電腦~~5張給玩家
			for(int writein = 0; writein < 5; writein++){
				computercard[writein] = allcard[writein];
			}
			for(int writein = 0; writein < 5; writein++){
				playercard[writein] = allcard[writein+5];
			}			
			
			//setIcon跟改原本的圖片
			
			cPlease.setIcon(pic[computercard[0]] );
			cPress.setIcon(pic[computercard[1]] );
			cRandom.setIcon(pic[computercard[2]] );
			cButton.setIcon(pic[computercard[3]] );
			cLitpon.setIcon(pic[computercard[4]] );			
			
			pPlease.setIcon(pic[playercard[0]] );
			pPress.setIcon(pic[playercard[1]] );
			pRandom.setIcon(pic[playercard[2]] );
			pButton.setIcon(pic[playercard[3]] );
			pLitpon.setIcon(pic[playercard[4]] );
			
			//判斷牌型
			int c = computer.cardtype(computercard);
			int p = player.cardtype(playercard);
			cardtypeone.setText(computer.showtype(c));
			cardtypetwo.setText(player.showtype(p));
			
			//比較牌型大小
			if(c < p){
				cardresault.setText("You Lose");
			}
			else if(c == p){
				cardresault.setText("Draw");
			}
			else{
				cardresault.setText("You Win");
			}			
		}
	}
	
			
}