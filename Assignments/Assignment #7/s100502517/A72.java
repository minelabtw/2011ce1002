package a7.s100502517;
import java.util.Scanner;
public class A72 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		boolean stop = true;
		
		while(stop){	
			try{
				System.out.println("Please input a password to varify: ");
				String password = input.next();
				Password pw = new Password(password);
				System.out.println("The password "+password+" is valid!!");		
			}
			//if it is error
			catch(InvalidPasswordException ex){				
				System.out.println(ex.getresault());
			}
			
			//continue or not
			System.out.println("Continue?(y/n)");
			String cont = input.next();
			if(cont.equals("y")){
				stop = true;
			}
			else if(cont.equals("n")){
				System.out.println("Bye Bye~!!!");
				stop = false;
			}
			
			//input not y or n
			else{
				System.out.println("Wrong input~!!!");
				stop = false;
			}
		}
	}
}
