package a7.s100502517;

import java.util.Random;

public class User {
	private int[] cardnum = new int[10];//用來存入random出來的數字，但不重複
	private int[] num = new int[6];//0 is A, 1 is 2, 2 is J, 3 is Q, 4 is K, 5 is Joker
	private int[] type = new int[6];//type[0]=Joker  type[1]=four of a kind  type[2]=Full house  type[3]=Three of a kind  type[4]=two pairs  type[5]=Nothing
	
	//發出 10 張 牌 ， 5張是玩家的 ， 5張是電腦的
	public int[] Wash(){
		for(int clean = 0; clean < 6; clean++){			
			type[clean] = clean;
		}
		for(int cleanup = 0; cleanup < 10; cleanup++){
			cardnum[cleanup] = -1;
		}	
		
		Random rand = new Random();
		
		int card = 0;
		while(card!=10){
			int k = rand.nextInt(21);
			boolean i = true;
			for(int checkup =0; checkup < card; checkup++){
				if(cardnum[checkup] == k){
					i = false;
					break;
				}				
			}
			if(i){
				cardnum[card] = k;
				card++;
			}			
		}
		return cardnum;
	}		
	
	//檢查5張牌的牌型是 Joker 、 four of a kind、 full house、 three of a kind、 two pairs、 Nothing
	public int cardtype(int[] number){		
		for(int cn= 0; cn < 5; cn++){
			num[cn]=0;
		}
		for(int c= 0; c < 5; c++){		
			switch(number[c]){
				case 0:					
				case 1:
				case 2:
				case 3:
					num[0]++;
					break;
				case 4:
				case 5:
				case 6:
				case 7:
					num[1]++;					
					break;
				case 8:
				case 9:
				case 10:
				case 11:
					num[2]++;					
					break;
				case 12:
				case 13:
				case 14:
				case 15:
					num[3]++;					
					break;
				case 16:
				case 17:
				case 18:
				case 19:
					num[4]++;					
					break;
				case 20:
					num[5]++;
					break;					
			}	
		}
		
		if(num[5]==1){//Joker
			return 0;
		}
		else{
			if(num[0]==4||num[1]==4||num[2]==4||num[3]==4||num[4]==4){//four of a kind 
				return 1;
			}
			else{
				if(num[0]==3||num[1]==3||num[2]==3||num[3]==3||num[4]==3){
					if(num[0]==2||num[1]==2||num[2]==2||num[3]==2||num[4]==2){//Full house
						return 2;
					}
					else{
						return 3;//three of a kind
					}
				}
				else{
					if(num[0]==2 && num[1]==2||num[0]==2 && num[2]==2||num[0]==2 && num[3]==2||num[0]==2 && num[4]==2
							||num[1]==2 && num[2]==2||num[1]==2 && num[3]==2||num[1]==2 && num[4]==2
								||num[2]==2 && num[3]==2 ||num[2]==2 && num[4]==2
									||num[3]==2 && num[4]==2){//two pairs
						return 4;
					}
					else{
						return 5;//Nothing
					}
				}
			}
		}		
	}
	
	//show the card type
	public String showtype(int which){
		switch(which){
			case 0:
				return "< Joker >";
			case 1:
				return "< Four of a kind >";
			case 2:
				return "< Full house >";
			case 3:
				return "< Three of a kind >";
			case 4:
				return "< Two pairs >";
			case 5:
				return "< Nothing >";					
		}
		return "12345";
	}
	
}