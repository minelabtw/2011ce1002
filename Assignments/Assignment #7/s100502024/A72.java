package a7.s100502024;
import java.util.Scanner;
public class A72 
{
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		String password;
		while(true)
		{
			System.out.println("Please input a password to varify: "); // 提示輸入
			password = input.next();
			try
			{
				Password test = new Password(password);  // 建立一個新的 object,並呼叫他
				System.out.println("The password "+password+" is valid!!");  // 若沒throw exception則顯示 password 正確
			}
			catch(InvalidPasswordException ex) 
			{
				System.out.println("The password "+password+" in not correct!!");  //若 catch 到 exception 則顯示password錯誤
			}
		}
	}
}
