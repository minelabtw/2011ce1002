package a7.s100502024;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class FrameWork extends JFrame implements ActionListener
{
	private ImageIcon card[] = new ImageIcon[21];  // 建立 array存圖片
	private ImageIcon init_card[]= new ImageIcon[5];   // 建立array存圖片
	private ImageIcon Icon = null;
	
	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();
	JPanel p3 = new JPanel();
	JPanel p4 = new JPanel();
	JPanel p5 = new JPanel();
	JLabel end1 = new JLabel();
	JLabel end2 = new JLabel();
	JLabel WL = new JLabel();
	private JLabel[] jlb1 = new JLabel[5];
	private JLabel[] jlb2 = new JLabel[5];
	
	Border lineborder = new LineBorder(Color.BLACK,5); // 邊框線
	Font font1 = new Font("SansSerif",Font.BOLD,50);     // 字
	Font font2 = new Font("SansSerif",Font.BOLD,20);
	JButton button = new JButton("random");   // 建立新按鈕
	public FrameWork()
	{
		storeIcon(); // 執行儲存圖片的method
		button.addActionListener(this);

		p1.setLayout(new GridLayout(2,6,50,10));  // 設定 p1
		
		JLabel line1 = new JLabel("     He");
		line1.setFont(font1);  // 設定字形
		p1.add(line1);  // 增加字到panel上
		
		for(int i=0;i<5;i++) //  依續增加初始圖片,框線
		{
			jlb1[i] = new JLabel();
			jlb1[i].setIcon(init_card[i]);
			jlb1[i].setBorder(lineborder);
			p1.add(jlb1[i]);
		}
		
		
		JLabel line2 = new JLabel("     You");
		line2.setFont(font1);
		p1.add(line2);
		
		for(int i=0;i<5;i++)  //  依序增加初始圖片,框線
		{
			jlb2[i] = new JLabel();
			jlb2[i].setIcon(init_card[i]);
			jlb2[i].setBorder(lineborder);
			p1.add(jlb2[i]);
		}
		
		p2.setLayout(new GridLayout(2,1));  // 設定 p2
		
		end1 = new JLabel("         <Two pairs>");
		end1.setFont(font2);
		p2.add(end1);
		
		end2 = new JLabel("     <Three of a kind>");
		end2.setFont(font2);
		p2.add(end2);
		
		p3.setLayout(new BorderLayout());  // 設定 p3
		p3.add(p1,BorderLayout.WEST);
		p3.add(p2,BorderLayout.CENTER);
		
		p4.setLayout(new GridLayout(1,2));
		p4.add(button); // 新增按鈕
		WL = new JLabel("                     You Win");
		WL.setFont(font1);  // 設定字形大小
		p4.add(WL);
		
		p5.setLayout(new GridLayout(2,1));
		p5.add(p3);
		p5.add(p4);
		add(p5); // 把 p5 panel 加到 frame上
	}
	public void storeIcon()  // 儲存圖片的 method
	{
		for(int i=0;i<21;i++)
		{
			card[i] = new ImageIcon("image/card/"+i+".png");  //  把圖片存入依序 array
		}
		for(int i=0;i<5;i++)
		{
			init_card[i] = new ImageIcon("image/card/init_"+(i+1)+".png"); 
		}
	}
	public void action(ImageIcon aIcon,ImageIcon bIcon,int i) // 顯示圖片的 method
	{
		jlb1[i].setIcon(aIcon);  // 顯示產生的圖片
		jlb1[i].setBorder(lineborder);    // 框線
	
		
		jlb2[i].setIcon(bIcon);
		jlb2[i].setBorder(lineborder);
		
	}
	public void action2(String a,String b,String c)  // 顯示牌的型別和輸贏
	{
		end1.setText("        "+a);  // 顯示牌組1的牌型
		end1.setFont(font2);
		p2.add(end1);
		
		end2.setText("        "+b);   // 顯示牌組2的牌型
		end2.setFont(font2);
		p2.add(end2);
		
		WL.setText("                       "+c);  // 顯示輸贏
		WL.setFont(font1);
		p4.add(WL);
		
	}
	public String determineWL(int He,int You)  // 判別輸贏
	{
		String WL = null;
		if (He == You)
		{
			WL = "Draw";
		}
		else if(He < You)
		{
			WL = "Lose";
		}
		else
		{
			WL = "Win";
		}
		System.out.println(He+" "+You);
		return WL;
		
	}
	public String displaytype(int TYPE)  // 將判斷牌型由數字轉換成字串
	{
		String display;
		if(TYPE == 1)
		{
			display = "Joker";
		}
		else if(TYPE == 2)
		{
			display = "Four of a kind";
		}
		else if(TYPE == 3)
		{
			display = "Full House";
		}
		else if(TYPE == 4)
		{
			display = "Three of kind";
		}
		else if(TYPE == 5)
		{
			display = "Two pairs";
		}
		else 
		{
			display = "Nothing";
		}
		return display;
	}
	public void actionPerformed(ActionEvent e)   // Button 觸發事件
	{		
		
		if(e.getSource() == button)  // 按下Button後要做的事
		{
			User He = new User();
			User You = new User();
			
			for(int i=0;i<5;i++)
			{
				He.generate();  // 產生牌組1的亂數
				You.generate();  // 產生牌組2的亂數
				action(setIcon(He.getcard()),setIcon(You.getcard()),i);  // 執行action來產生新的牌組圖片
			}
			action2(displaytype(He.determinetype()),displaytype(You.determinetype()),determineWL(He.determinetype(),You.determinetype()));
		}    // 執行action2來顯示牌組1,2的牌型及顯示輸贏
	}
	public ImageIcon setIcon(int c)  // 將產生的亂數轉換成相對應的圖形
	{
		//System.out.println(c);
		Icon = card[c];
		return Icon;
	}
}
