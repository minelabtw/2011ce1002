package a7.s100502024;
import java.util.Scanner;
public class Password 
{
	public Password (String password) throws InvalidPasswordException
	{
		if(lengthcheck(password) == false)
		{
			throw new InvalidPasswordException(1,password); // 若長度錯誤則throw exception執行 InvalidPasswordException
		}
		else if(numbercheck(password) == false)
		{
			throw new InvalidPasswordException(2,password); // 若無數字則throw exception執行 InvalidPasswordException
		}
		else if(lettercheck(password) == false)
		{
			throw new InvalidPasswordException(3,password); // 若無字元則throw exception執行 InvalidPasswordException
		}
		else
		{
			
		}
	}
	public boolean lengthcheck (String Password)   // 檢查password長度
	{
		int length = Password.length();
		if(length<7)
		{
			return  false;
		}
		else
		{
			return true;
		}
	}
	public boolean lettercheck(String Password)  // 檢查password有無letter
	{
		int checknumber;
		boolean check = true;
		int numbercounter=0;
		for(int i=0;i<Password.length();i++)
		{
			checknumber = Password.charAt(i);
			if(checknumber >= 48 && checknumber <= 57) 
			{
				numbercounter++;
			}
		}
		if(numbercounter == Password.length())  //若number個數等於password長度,則return false 表無 letter
		{
			check = false;
		}
		return check;
	}
	public boolean numbercheck(String Password)  // 檢查password有無number
	{
		int checkletter;
		boolean check = true;
		int lettercounter=0;
		for(int i=0;i<Password.length();i++)
		{
			checkletter = Password.charAt(i);
			if(checkletter >= 65 && checkletter <=90)
			{
				lettercounter++;
			}
			else if(checkletter >= 97 && checkletter <=122)
			{
				lettercounter++;
			}
		}
		if (lettercounter == Password.length()) //若letter個數等於password長度,則return false 表無 number
		{
			check = false;
		}
		return check;
	}
}
