package a7.s100502024;

public class InvalidPasswordException extends Exception
{
	public InvalidPasswordException (int event,String a)
	{
		ShowMessage(event,a);
	}
	public void ShowMessage(int eventnumber,String password)
	{
		String Message = null;
		if(eventnumber == 1)
		{
			System.out.println("The length of the password "+password+" is not enough!!");  // 顯示錯誤訊息
		}
		else if(eventnumber == 2)
		{
			System.out.println("The password "+password+" has no number!!"); // 顯示錯誤訊息
		}
		else
		{
			System.out.println("The password "+password+" has no letter!!"); // 顯示錯誤訊息
		}
		
	}
}
