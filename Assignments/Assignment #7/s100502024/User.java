package a7.s100502024;

public class User 
{
	private int card[] = new int[5]; // array儲存一副牌(5張)
	protected int Card;
	protected int kind[] = new int[6];  // 儲存A,2,J,Q,K,JOKER各有幾張(產生一副牌時)
	//private int type;
	private int Number[] = new int[21];  // 儲存產生的牌
	private int Rand;
	private int i=0;
	//private boolean keep;
	public User()
	{
		
	}
	public int getcard()
	{
		return Card;
	}
	/*public int gettype()
	{
		return type;
	}*/
	public void generate()
	{
		//keep = false;
			Rand = (int)(Math.random()*66)%21;  // 產生亂數
			if(i == 0)
			{
				card[i] = Rand;  // 把牌存起來以檢查是否有重複
				Card = Rand;
				Number[card[i]]++;  // 把產生的牌存進相對應的陣列裡
			}
			else
			{
				for(int j=0;j<i;j++)
				{
					while(card[j] == Rand)  // 檢查是否與前面的牌有重複
					{
						Rand = (int)(Math.random()*66)%21;  // 若重複,再產生一個亂數
					}
				}
				card[i] = Rand;
				Card = Rand;
				Number[card[i]]++;
			}
			//System.out.println(i);
			i++;
	}
	int type=8;
	public int determinetype() // 判斷牌的型態
	{
		
		kind[0] = Number[0]+Number[1]+Number[2]+Number[3];  // 儲存牌A有幾張
		kind[1] = Number[4]+Number[5]+Number[6]+Number[7];  // 儲存牌2有幾張
		kind[2] = Number[8]+Number[9]+Number[10]+Number[11];  // 儲存牌J有幾張
		kind[3] = Number[12]+Number[13]+Number[14]+Number[15];  // 儲存牌Q有幾張
		kind[5] = Number[20];
		/*for(int i=0;i<6;i++)
		{
			System.out.println(kind[i]);
		}*/
		if(kind[5] == 1) // 判斷牌的型態
		{
			type = 1; // 1代表JOKER
		}
		else if(kind[0] == 4 || kind[1] == 4 || kind[2] == 4 || kind[3] == 4 || kind[4] == 4)
		{
			type = 2;  // four of a kind
		}
		else if (kind[0] == 3 || kind[1] == 3 || kind[2] == 3 || kind[3] == 3 || kind[4] == 3)
		{
			if (kind[0] == 2 || kind[1] == 2 || kind[2] == 2 || kind[3] == 2 || kind[4] == 2)
			{
				type = 3; // 
			}
			else
			{
				type = 4; // three of kind
			}
		}
		else if (kind[0] == 2) // two pairs
		{
			if(kind[1] == 2 || kind[2] == 2 || kind[3] == 2 || kind[4] == 2)
			{
				type = 5; 
			}
		}
		else if (kind[1] == 2)
		{
			if(kind[0] == 2 || kind[2] == 2 || kind[3] == 2 || kind[4] == 2)
			{
				type = 5;
			}
		}
		else if (kind[2] == 2)
		{
			if(kind[0] == 2 || kind[1] == 2 || kind[3] == 2 || kind[4] == 2)
			{
				type = 5;
			}
		}
		else if (kind[3] == 2)
		{
			if(kind[0] == 2 || kind[1] == 2 || kind[2] == 2 || kind[4] == 2)
			{
				type = 5;
			}
		}
		else if (kind[4] == 2)
		{
			if(kind[0] == 2 || kind[1] == 2 || kind[2] == 2 || kind[3] == 2)
			{
				type = 5;
			}
		}
		else if (kind[0] == 1 && kind[1] == 1 && kind[2] == 1 && kind[3] == 1 && kind[4] == 1)
		{
			type = 6; // nothing
		}
		else  // a pair
		{
			type = 6;  // nothing
		}
		return type;
	}
	
}
