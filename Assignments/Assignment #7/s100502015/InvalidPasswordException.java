package a7.s100502015;

public class InvalidPasswordException extends Exception {

	public InvalidPasswordException(String password,String message) {
		super(message+"\nthe password " + password + "is not correct!!");

	}
}
