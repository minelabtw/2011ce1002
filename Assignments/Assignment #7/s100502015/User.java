package a7.s100502015;

import java.util.Random;

import javax.swing.ImageIcon;

public class User {
	private ImageIcon A1 = new ImageIcon("image/card/0.png");
	private ImageIcon A2 = new ImageIcon("image/card/1.png");
	private ImageIcon A3 = new ImageIcon("image/card/2.png");
	private ImageIcon A4 = new ImageIcon("image/card/3.png");
	private ImageIcon t1 = new ImageIcon("image/card/4.png");
	private ImageIcon t2 = new ImageIcon("image/card/5.png");
	private ImageIcon t3 = new ImageIcon("image/card/6.png");
	private ImageIcon t4 = new ImageIcon("image/card/7.png");
	private ImageIcon j1 = new ImageIcon("image/card/8.png");
	private ImageIcon j2 = new ImageIcon("image/card/9.png");
	private ImageIcon j3 = new ImageIcon("image/card/10.png");
	private ImageIcon j4 = new ImageIcon("image/card/11.png");	
	private ImageIcon q1 = new ImageIcon("image/card/13.png");
	private ImageIcon q2 = new ImageIcon("image/card/14.png");
	private ImageIcon q3 = new ImageIcon("image/card/15.png");
	private ImageIcon q4 = new ImageIcon("image/card/a.png");
	private ImageIcon k1 = new ImageIcon("image/card/16.png");
	private ImageIcon k2 = new ImageIcon("image/card/17.png");
	private ImageIcon k3 = new ImageIcon("image/card/18.png");
	private ImageIcon k4 = new ImageIcon("image/card/19.png");
	private ImageIcon joker1 = new ImageIcon("image/card/12.png");
	private ImageIcon cardarray[] = {A1,A2,A3,A4,t1,t2,t3,t4,j1,j2,j3,j4,q1,q2,q3,q4,k1,k2,k3,k4,joker1};
	public ImageIcon comcard[] = new  ImageIcon[5];
	public ImageIcon usercard[] =new  ImageIcon[5];
	public int[] husercard = new int[5];
	public int[] hcomcard = new int[5];
	public int[] allcard = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
			15, 16, 17, 18, 19, 20 };
	public int []userpattern = new int[6];
	public int []compattern = new int[6];	

	public void random() {
		Random rand = new Random();
		int count = 1;
		int count2 =1;
		while (count <= 5) {
			for (int j = 0; j < 5; j++) {
				int number = rand.nextInt(20) % 21;
				while (allcard[number] == 0) {
					number = rand.nextInt(20) % 21;
				}
				husercard[count - 1] = allcard[number];
				allcard[number] = 0;
				usercard[count-1] = cardarray[number];
				count++;

			}			
		}
		while (count2 <= 5) {
			for (int j = 0; j < 5; j++) {
				int number = rand.nextInt(20) % 21;
				while (allcard[number] == 0) {
					number = rand.nextInt(20) % 21;
				}
				hcomcard[count2 - 1] = allcard[number];
				allcard[number] = 0;
				comcard[count2-1] = cardarray[number];
				count2++;

			}			
		}
		
	}

	public void checkcard(int handcard[],int inputarray[]) {
		for (int i = 0; i < 5; i++) {
			if (handcard[i] == 0 || handcard[i] == 1 || handcard[i] == 2
					|| handcard[i] == 3) {
				inputarray[0]++;
			} else if (handcard[i] == 4 || handcard[i] == 5 || handcard[i] == 6
					|| handcard[i] == 7) {
				inputarray[1]++;
			} else if (handcard[i] == 8 || handcard[i] == 9
					|| handcard[i] == 10 || handcard[i] == 11) {
				inputarray[2]++;
			} else if (handcard[i] == 12 || handcard[i] == 13
					|| handcard[i] == 14 || handcard[i] == 15) {
				inputarray[3]++;
			} else if (handcard[i] == 16 || handcard[i] == 17
					|| handcard[i] == 18 || handcard[i] == 19) {
				inputarray[4]++;
			} else {
				inputarray[5]++;
			}
		}
	}

	public int judge(int inputarray[] ){
		if(inputarray[5]==1)
		{
			return 1;
		}
		else if(inputarray[0]==4||inputarray[1]==4||inputarray[2]==4||inputarray[3]==4||inputarray[4]==4)
		{
			return 2;//4 inputarray[4]ind
		}
		
		else if((inputarray[0]==3&&inputarray[1]==2)||(inputarray[0]==3&&inputarray[2]==2)||(inputarray[0]==3&&inputarray[3]==2)||(inputarray[0]==3&&inputarray[4]==2)||
				(inputarray[1]==3&&inputarray[0]==2)||(inputarray[1]==3&&inputarray[2]==2)||(inputarray[1]==3&&inputarray[3]==2)||(inputarray[1]==3&&inputarray[4]==2)||
				(inputarray[2]==3&&inputarray[0]==2)||(inputarray[2]==3&&inputarray[1]==2)||(inputarray[2]==3&&inputarray[3]==2)||(inputarray[2]==3&&inputarray[4]==2)||
				(inputarray[3]==3&&inputarray[0]==2)||(inputarray[3]==3&&inputarray[1]==2)||(inputarray[3]==3&&inputarray[2]==2)||(inputarray[3]==3&&inputarray[4]==2)||
				(inputarray[4]==3&&inputarray[0]==2)||(inputarray[4]==3&&inputarray[1]==2)||(inputarray[4]==3&&inputarray[2]==2)||(inputarray[4]==3&&inputarray[3]==2))
		{
			return 3;//full house 
		}
		else if(inputarray[0]==3||inputarray[1]==3||inputarray[2]==3||inputarray[3]==3||inputarray[4]==3)
		{
			return 4;//3 kind 
		}
		else if((inputarray[0]==2&&inputarray[1]==2)||(inputarray[0]==2&&inputarray[2]==2)||(inputarray[0]==2&&inputarray[3]==2)||(inputarray[0]==2&&inputarray[4]==2)||
				(inputarray[1]==2&&inputarray[2]==2)||(inputarray[1]==2&&inputarray[3]==2)||(inputarray[1]==2&&inputarray[4]==2)||
				(inputarray[2]==2&&inputarray[3]==2)||(inputarray[2]==2&&inputarray[4]==2)||(inputarray[3]==2&&inputarray[4]==2))
		{
			return 5;//inputarray[1] pair
		}
		else 
		{
			return 6;
		}		
	}
	public String rank(int judge)
	{
		if(judge==1)
		{
			return "joker";
		}
		else if(judge==2)
		{
			return "4 kind";
		}
		else if(judge==3)
		{
			return "full house";
		}
		else if(judge==4)
		{
			return "3 kind";
		}
		else if(judge==5)
		{
			return "two pair";
		}
		else
		{
			return "nothing";
		}
		
	}
}
