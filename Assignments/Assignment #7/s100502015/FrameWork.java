package a7.s100502015;

import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrameWork extends JFrame {

	private ImageIcon init_1Icon = new ImageIcon("image/card/init_1.png");
	private ImageIcon init_2Icon = new ImageIcon("image/card/init_2.png");
	private ImageIcon init_3Icon = new ImageIcon("image/card/init_3.png");
	private ImageIcon init_4Icon = new ImageIcon("image/card/init_4.png");
	private ImageIcon init_5Icon = new ImageIcon("image/card/init_5.png");
	User user = new User();
	JButton button = new JButton("random"); // create button
	JPanel p1 = new JPanel(new FlowLayout(FlowLayout.CENTER, 100, 10));
	JPanel p2 = new JPanel(new FlowLayout(FlowLayout.CENTER, 100, 2));
	JPanel p3 = new JPanel(new FlowLayout(FlowLayout.CENTER, 100, 2));

	public FrameWork() {

		user.random();
		user.checkcard(user.husercard, user.userpattern);
		user.checkcard(user.hcomcard, user.compattern);
		for (int i = 0; i < 5; i++) {
			p1.add(new JLabel(user.comcard[i]));
		}
		p1.add(new JLabel(user.rank(user.judge(user.compattern))));
		p1.setBorder(new TitledBorder("he"));
		for (int i = 0; i < 5; i++) {
			p2.add(new JLabel(user.usercard[i]));
		}
		p2.add(new JLabel(user.rank(user.judge(user.userpattern))));
		p2.setBorder(new TitledBorder("you"));
		p3.add(button);
		if (user.judge(user.userpattern) > user.judge(user.compattern)) {
			p3.add(new JLabel("you win"));
		} else if (user.judge(user.userpattern) < user.judge(user.compattern)) {
			p3.add(new JLabel("you lose"));
		} else {
			p3.add(new JLabel("draw"));
		}

		setLayout(new GridLayout(3, 7, 5, 5));
		add(p1);
		add(p2);
		add(p3);
	}

}
