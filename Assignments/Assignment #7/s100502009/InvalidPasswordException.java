package a7.s100502009;

public class InvalidPasswordException extends Exception{
	private String password;	
	private int condition;
	
	public InvalidPasswordException(String pw,int condition)//accept which kind of the situation of the wrong password is
	{
		super("The password "+ pw+" is not correct!!\n");
		setPassword(pw);
		this.condition=condition;
	}
	
	public void setPassword(String pw)//store the password
	{
		password=pw;
	}
	
	public String getPassword()//show the description of the wrong password
	{
		String answer="";
		if(condition==1)
		{
			answer="The length of the password "+password+" is not enough!!";
		}
		else if(condition==2)
		{
			answer="The password "+password+" has no number!!";
		}
		else if(condition==3)
		{
			answer="The password "+password+" has no letter!!";
		}
		return answer;
	}
}