package a7.s100502009;
import javax.swing.*;
import java.lang.Math;


public class User {
	private ImageIcon[] imageIcons={//store the pictures
		new ImageIcon("image/card/0.png"),
		new ImageIcon("image/card/1.png"),
		new ImageIcon("image/card/2.png"),
		new ImageIcon("image/card/3.png"),
		new ImageIcon("image/card/4.png"),
		new ImageIcon("image/card/5.png"),
		new ImageIcon("image/card/6.png"),
		new ImageIcon("image/card/7.png"),
		new ImageIcon("image/card/8.png"),
		new ImageIcon("image/card/9.png"),
		new ImageIcon("image/card/10.png"),
		new ImageIcon("image/card/11.png"),
		new ImageIcon("image/card/12.png"),
		new ImageIcon("image/card/13.png"),
		new ImageIcon("image/card/14.png"),
		new ImageIcon("image/card/15.png"),
		new ImageIcon("image/card/16.png"),
		new ImageIcon("image/card/17.png"),
		new ImageIcon("image/card/18.png"),
		new ImageIcon("image/card/19.png"),
		new ImageIcon("image/card/20.png"),
		new ImageIcon("image/card/init_1.png"),
		new ImageIcon("image/card/init_2.png"),
		new ImageIcon("image/card/init_3.png"),
		new ImageIcon("image/card/init_4.png"),
		new ImageIcon("image/card/init_5.png")
	};
	//store the types
	private String[] type={"<Joker>"," <Four of a kind>","< Full House>"," <Three of kind>"," <Two pairs>","< Nothing>"};
	
	User()
	{		
	}	

	public ImageIcon[] getImage()
	{
		return imageIcons;
	}	
	
	
	public int[] index()//product random numbers
	{
		int [] number = new int[5];  
		for (int i = 0; i < number.length; i++)
		{
			number[i] = (int)(Math.random()*21);
			for (int j = 0 ; j < i; j++) 
			{
				if (number[i]==number[j]) 
				{
					i--; 
					break; 
				}    
			}
		}
		return number;
	}
	
	public int getRank(int[] array)//get the type
	{		
		int acount=0,bcount=0,rank=0;
		for(int i=0;i<6;i++)
		{			
			if(array[i]==4)
				rank=1;
			else if(array[i]==3)
				acount=3;
			else if(array[i]==2)
				bcount+=2;
		}
		if(array[0]==1)
			rank=0;
		else if(acount+bcount==5)
			rank=2;
		else if(acount==3)
			rank=3;
		else if(bcount==4)
			rank=4;
		else if(acount+bcount<3)
			rank=5;
		return rank;
	}
	
	public String game(int n1,int n2)//decide the winner
	{
		String answer="";
		if(n1==n2)
			answer="DRAW";
		else if(n1==0||n1<n2)
			answer="YOU LOSE";
		else if(n2==0||n1>n2)
			answer="YOU WIN";
		return answer;
	}
	
	public String[] getType()
	{
		return type;
	}
}