package a7.s100502009;

public class Password {
	private String password;
	private int condition=0;
	
	public Password(String pw) throws InvalidPasswordException//to check the password is correct or not
	{				
		if(pw.length()<7)
		{			
			condition=1;
			throw new InvalidPasswordException(pw,condition);
		}
		else if(number(pw)==0)
		{
			condition=2;
			throw new InvalidPasswordException(pw,condition);
		}
		else if(alpha(pw)==0)
		{
			condition=3;
			throw new InvalidPasswordException(pw,condition);
		}
		else
		{
			setPassword(pw);
		}
	}
	
	public void setPassword(String pw)//store the password
	{
		password=pw;
	}
	
	public String getPassword()//show the password
	{
		return password;
	}
	
	public int alpha(String pw)//to count the number of the alpha
	{
		int alpha=0;
		for(int i=0;i<pw.length();i++)
		{
			int word=pw.charAt(i)-'0';
			if(word>9)
			{
				alpha++;
			}
		}
		return alpha;
	}
	
	public int number(String pw)//to count how many numbers there are
	{
		int number=0;
		for(int i=0;i<pw.length();i++)
		{
			int word=pw.charAt(i)-'0';
			if(word<=9)
			{
				number++;
			}
		}
		return number;
	}
}
