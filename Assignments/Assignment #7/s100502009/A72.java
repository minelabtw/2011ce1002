package a7.s100502009;
import java.util.Scanner;

public class A72 {
	public static void main(String[] args)
	{
		String password;
		Scanner input=new Scanner(System.in);
		for(int i=1;i>0;i++)
		{
			System.out.println("Please input a password to varify: ");
			password=input.next();//let user input a password
			try
			{
				Password pw=new Password(password);//create an object
				System.out.println("The password "+ pw.getPassword()+" is valid!!");
				i=-1;//end the for-loop
			}
			catch(InvalidPasswordException ex)
			{
				System.out.println(ex.getPassword());
				System.out.println(ex.getMessage());
			}
		}
	}
}
