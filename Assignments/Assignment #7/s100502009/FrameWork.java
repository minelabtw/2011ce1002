package a7.s100502009;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.*;

public class FrameWork extends JFrame implements ActionListener
{
	JButton button = new JButton("random"); //create button	
	User user=new User();
	JPanel p1=new JPanel(new GridLayout(2,7,5,5));
	Border lineBorder=new LineBorder(Color.BLACK,2);
	Font largeFont= new Font("TimesRoman",Font.BOLD,40);
	JPanel p2=new JPanel(new BorderLayout());
	JLabel[] pic=new JLabel[5];
	JLabel[] pic1=new JLabel[5];
	JLabel rank1;
	JLabel rank2;
	JLabel message;
	public FrameWork ()
	{		
		p1.add(new JLabel("	  Computer"));	
		for(int i=21;i<=25;i++)
		{
			pic[i-21]=new JLabel(user.getImage()[i]);
			pic[i-21].setBorder(lineBorder);
			p1.add(pic[i-21]);		
		}	
		rank1=new JLabel("Type1");
		p1.add(rank1);
		p1.add(new JLabel("   Player"));
		for(int i=21;i<=25;i++)
		{
			pic1[i-21]=new JLabel(user.getImage()[i]);
			pic1[i-21].setBorder(lineBorder);
			p1.add(pic1[i-21]);		
		}
		rank2=new JLabel("Type2");
		p1.add(rank2);				
		button.addActionListener(this); //active button event here
		message=new JLabel("GAME START!");			
		button.setBorder(lineBorder);		
		message.setFont(largeFont);
		button.setFont(largeFont);
		p2.add(button,BorderLayout.CENTER);
		p2.add(message,BorderLayout.EAST);
		setLayout(new BorderLayout(5,10));
		add(p1,BorderLayout.CENTER);
		add(p2,BorderLayout.SOUTH);		
	}
	
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == button)//show when the button is pressed
		{			
			int[] array=new int[6];
			int[] index=user.index();
			for(int i=0;i<5;i++)//store the times of a type showing
			{
				int num=index[i];
				pic[i].setIcon(user.getImage()[num]);//renew the pictures
				if(num==20)
				{
					array[0]++;
				}
				else if(num>=0&&num<=3)
				{
					array[1]++;
				}
				else if(num>=4&&num<=7)
				{
					array[2]++;
				}
				else if(num>=8&&num<=11)
				{
					array[3]++;
				}
				else if(num>=12&&num<=15)
				{
					array[4]++;
				}
				else if(num>=16&&num<=19)
				{
					array[5]++;
				}
			}
			int set1=user.getRank(array);
			rank1.setText(user.getType()[set1]);//renew the type
			int[] array1=new int[6];
			int[] index1=user.index();
			for(int i=0;i<5;i++)
			{
				int num=index1[i];
				pic1[i].setIcon(user.getImage()[num]);	
				if(num==20)
				{
					array1[0]++;
				}
				else if(num>=0&&num<=3)
				{
					array1[1]++;
				}
				else if(num>=4&&num<=7)
				{
					array1[2]++;
				}
				else if(num>=8&&num<=11)
				{
					array1[3]++;
				}
				else if(num>=12&&num<=15)
				{
					array1[4]++;
				}
				else if(num>=16&&num<=19)
				{
					array1[5]++;
				}
			}
			int set2=user.getRank(array1);
			rank2.setText(user.getType()[set2]);
			message.setText(user.game(set1, set2));//renew the message
		}
	}	
}