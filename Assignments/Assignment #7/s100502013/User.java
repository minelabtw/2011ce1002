package a7.s100502013;
import java.util.ArrayList;

public class User {
	private int card1;
	private int card2;
	private int card3;
	private int card4;
	private int card5;
	private int cardkind1;
	private int cardkind2;
	private int cardkind3;
	private int cardkind4;
	private int cardkind5;
	private int cardtype;
	private int count1=0;
	private int count2=0;
	private int count11=0;
	private int count12=0;
	private int count13=0;
	private int countg=0;
	
	User(){ //default constructor
		
	}
	
	public void setcard(int a,int b,int c,int d,int e){ //set cards imformation and save some details
		card1 = a;
		card2 = b;
		card3 = c;
		card4 = d;
		card5 = e;
		cardkind1 = checktype(a);
		cardkind2 = checktype(b);
		cardkind3 = checktype(c);
		cardkind4 = checktype(d);
		cardkind5 = checktype(e);
		count1=0;
		count2=0;
		count11=0;
		count12=0;
		count13=0;
		count(cardkind1);
		count(cardkind2);
		count(cardkind3);
		count(cardkind4);
		count(cardkind5);
	}
	
	public int getcard1(){
		return card1;
	}
	
	public int getcard2(){
		return card2;
	}
	
	public int getcard3(){
		return card3;
	}
	
	public int getcard4(){ 
		return card4;
	}
	
	public int getcard5(){
		return card5;
	}
	
	public void checking(){ //check the cards' type
		if(cardkind1==20 || cardkind2==20 || cardkind3==20 || cardkind4==20 || cardkind5==20)
			cardtype = 5;
		else if(count1==4 || count2==4 || count11==4 || count12==4 || count13==4)
			cardtype = 4;
		else if((count1==0 && count2==0 && count11==0) || (count1==0 && count2==0 && count12==0)
				|| (count1==0 && count2==0 && count13==0) || (count1==0 && count11==0 && count13==0)
				|| (count1==0 && count12==0 && count13==0) || (count2==0 && count12==0 && count13==0)
				|| (count11==0 && count12==0 && count13==0) || (count1==0 && count11==0 && count12==0)
				|| (count2==0 && count11==0 && count12==0) || (count2==0 && count11==0 && count13==0))
			cardtype = 3;
		else if(count1==3 || count2==3 || count11==3 || count12==3 || count13==3)
			cardtype = 2;
		else if((count1==0 && count2==0) || (count1==0 && count11==0) || (count1==0 && count12==0)
				|| (count1==0 && count13==0) || (count2==0 && count11==0) || (count2==0 && count12==0)
				|| (count2==0 && count13==0) || (count11==0 && count12==0) || (count11==0 && count13==0)
				|| (count12==0 && count13==0))
			cardtype = 1;
		else
			cardtype = 0;
	}
	
	public String getcardkind(){ //get the tpye name
		if(cardtype==5)
			return "Joker";
		else if(cardtype==4)
			return "Four of a kind";
		else if(cardtype==3)
			return "Full House";
		else if(cardtype==2)
			return "Three of a kind";
		else if(cardtype==1)
			return "Two pairs";
		else
			return "Nothing";
	}
	
	public int getcardtype(){
		return cardtype;
	}
	
	public int checktype(int x){ //check the card's point
		if(x==0||x==1||x==2||x==3)
			return 1;
		else if(x==4||x==5||x==6||x==7)
			return 2;
		else if(x==8||x==9||x==10||x==11)
			return 11;
		else if(x==12||x==13||x==14||x==15)
			return 12;
		else if(x==16||x==17||x==18||x==19)
			return 13;
		else
			return 20;
	}
	
	public void count(int a){ //count same-point cards
		if(a==1)
			count1++;
		else if(a==2)
			count2++;
		else if(a==11)
			count11++;
		else if(a==12)
			count12++;
		else if(a==13)
			count13++;
		else
			countg++;
			
	}
}
