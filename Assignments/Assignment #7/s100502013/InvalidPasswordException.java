package a7.s100502013;

public class InvalidPasswordException {
	InvalidPasswordException(){ //default constructor
		
	}
	
	InvalidPasswordException(String x){
		if(checklength(x)==false){
			System.out.println("The length of the password " + x + " is not enough!!");
			System.out.println("The password " + x + " is not correct!!");
		}
		else if(checknumber(x)==false){
			System.out.println("The password " + x + " has no number!!");
			System.out.println("The password " + x + " is not correct!!");
		}
		else if(checkletter(x)==false){
			System.out.println("The password " + x + " has no letter!!");
			System.out.println("The password " + x + " is not correct!!");
		}
		else{
			System.out.println("The password " + x + " is valid!!");
		}
	}
	
	public boolean checklength(String a){ //check the length
		if(a.length()>=7)
			return true;
		else
			return false;
	}
	
	public boolean checknumber(String a){ //check whether there is at least one number
		int numberset = 0;
		for(int i=0;i<a.length();i++){
			if(a.charAt(i)<=57 && a.charAt(i)>=48)
				numberset++;
		}
		if(numberset==0)
			return false;
		else
			return true;
	}
	
	public boolean checkletter(String a){ //check whether there is at least one letter
		int letterset = 0;
		for(int i=0;i<a.length();i++){
			if((a.charAt(i)<=90 && a.charAt(i)>=65) || (a.charAt(i)<=122 && a.charAt(i)>=97))
				letterset++;
		}
		if(letterset==0)
			return false;
		else
			return true;
	}
}
