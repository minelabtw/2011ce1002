package a7.s100502013;
import java.lang.Math;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.*;
import java.util.Random;

public class FrameWork extends JFrame implements ActionListener{ //set the frame
	private User enemy = new User();
	private User player = new User();
	private JPanel mypanel01 = new JPanel();
	private JPanel mypanel02 = new JPanel();
	private JLabel type01 = new JLabel();
	private JLabel type02 = new JLabel();
	private Border labelline = new LineBorder(Color.BLACK, 2);
	private Font rfont = new Font("TimesRoman",Font.BOLD, 40);
	private JLabel la11 = new JLabel(new ImageIcon("image/card/init_1.png"));
	private JLabel la12 = new JLabel(new ImageIcon("image/card/init_2.png"));
	private JLabel la13 = new JLabel(new ImageIcon("image/card/init_3.png"));
	private JLabel la14 = new JLabel(new ImageIcon("image/card/init_4.png"));
	private JLabel la15 = new JLabel(new ImageIcon("image/card/init_5.png"));
	private JLabel la21 = new JLabel(new ImageIcon("image/card/init_1.png"));
	private JLabel la22 = new JLabel(new ImageIcon("image/card/init_2.png"));
	private JLabel la23 = new JLabel(new ImageIcon("image/card/init_3.png"));
	private JLabel la24 = new JLabel(new ImageIcon("image/card/init_4.png"));
	private JLabel la25 = new JLabel(new ImageIcon("image/card/init_5.png"));
	private JButton rbotton = new JButton("random");
	private JLabel result = new JLabel();
	
	FrameWork(){
		mypanel01.setLayout(new GridLayout(2,7)); //cards
		mypanel02.setLayout(new GridLayout(1,2)); //random botton and result
		rbotton.setFont(rfont);
		mypanel01.add(new JLabel("He")).setFont(rfont);
		((JComponent) mypanel01.add(la11)).setBorder(labelline);
		((JComponent) mypanel01.add(la12)).setBorder(labelline);
		((JComponent) mypanel01.add(la13)).setBorder(labelline);
		((JComponent) mypanel01.add(la14)).setBorder(labelline);
		((JComponent) mypanel01.add(la15)).setBorder(labelline);
		mypanel01.add(type01);
		mypanel01.add(new JLabel("You")).setFont(rfont);
		((JComponent) mypanel01.add(la21)).setBorder(labelline);
		((JComponent) mypanel01.add(la22)).setBorder(labelline);
		((JComponent) mypanel01.add(la23)).setBorder(labelline);
		((JComponent) mypanel01.add(la24)).setBorder(labelline);
		((JComponent) mypanel01.add(la25)).setBorder(labelline);
		mypanel01.add(type02);
		mypanel02.add(rbotton);
		((JLabel) mypanel02.add(result)).setHorizontalAlignment(JTextField.CENTER);
		add(mypanel01,BorderLayout.NORTH);
		add(mypanel02,BorderLayout.SOUTH);
		rbotton.addActionListener(this); //random botton action
	}

	public void actionPerformed(ActionEvent e) { //random action and result
		if(e.getSource() == rbotton){
			Random card = new Random();
			int[] ran = new int[10];
			for(int j=0;j<10;j++){
				ran[j] = Math.abs(card.nextInt()%21);
			}
			enemy.setcard(ran[0],ran[1],ran[2],ran[3],ran[4]);
			player.setcard(ran[5],ran[6],ran[7],ran[8],ran[9]);
		}
		setimage(la11,enemy.getcard1());
		setimage(la12,enemy.getcard2());
		setimage(la13,enemy.getcard3());
		setimage(la14,enemy.getcard4());
		setimage(la15,enemy.getcard5());
		setimage(la21,player.getcard1());
		setimage(la22,player.getcard2());
		setimage(la23,player.getcard3());
		setimage(la24,player.getcard4());
		setimage(la25,player.getcard5());
		player.checking();
		enemy.checking();
		type01.setText(enemy.getcardkind());
		type02.setText(player.getcardkind());
		result.setFont(rfont);
		result.setText(lasto(player.getcardtype(),enemy.getcardtype()));
	}
	
	public String lasto(int a,int b){ //check win/lose
		if(a==b)
			return "Draw";
		else if(a>b)
			return "You win";
		else
			return "You lose";
	}
	
	public void setimage(JLabel j,int i){ //set appropriate images
		ImageIcon ic = new ImageIcon();
		switch(i){
			case 0:
				ic = new ImageIcon("image/card/0.png");
				j.setIcon(ic);
				break;
			case 1:
				ic = new ImageIcon("image/card/1.png");
				j.setIcon(ic);
				break;
			case 2:
				ic = new ImageIcon("image/card/2.png");
				j.setIcon(ic);
				break;
			case 3:
				ic = new ImageIcon("image/card/3.png");
				j.setIcon(ic);
				break;
			case 4:
				ic = new ImageIcon("image/card/4.png");
				j.setIcon(ic);
				break;
			case 5:
				ic = new ImageIcon("image/card/5.png");
				j.setIcon(ic);
				break;
			case 6:
				ic = new ImageIcon("image/card/6.png");
				j.setIcon(ic);
				break;
			case 7:
				ic = new ImageIcon("image/card/7.png");
				j.setIcon(ic);
				break;
			case 8:
				ic = new ImageIcon("image/card/8.png");
				j.setIcon(ic);
				break;
			case 9:
				ic = new ImageIcon("image/card/9.png");
				j.setIcon(ic);
				break;
			case 10:
				ic = new ImageIcon("image/card/10.png");
				j.setIcon(ic);
				break;
			case 11:
				ic = new ImageIcon("image/card/11.png");
				j.setIcon(ic);
				break;
			case 12:
				ic = new ImageIcon("image/card/12.png");
				j.setIcon(ic);
				break;
			case 13:
				ic = new ImageIcon("image/card/13.png");
				j.setIcon(ic);
				break;
			case 14:
				ic = new ImageIcon("image/card/14.png");
				j.setIcon(ic);
				break;
			case 15:
				ic = new ImageIcon("image/card/15.png");
				j.setIcon(ic);
				break;
			case 16:
				ic = new ImageIcon("image/card/16.png");
				j.setIcon(ic);
				break;
			case 17:
				ic = new ImageIcon("image/card/17.png");
				j.setIcon(ic);
				break;
			case 18:
				ic = new ImageIcon("image/card/18.png");
				j.setIcon(ic);
				break;
			case 19:
				ic = new ImageIcon("image/card/19.png");
				j.setIcon(ic);
				break;
			case 20:
				ic = new ImageIcon("image/card/20.png");
				j.setIcon(ic);
				break;
		}
	}
}
