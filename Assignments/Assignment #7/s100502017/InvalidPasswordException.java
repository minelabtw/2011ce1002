package a7.s100502017;

public class InvalidPasswordException extends Exception{
	private String pw;
	public InvalidPasswordException(String p,int i){//according to i ,determined message
		if(i==3)
			System.out.println("The length of the password "+p+" is not enough!!");
		else if(i==2)
			System.out.println("The password "+p+" has no letter!!");
		else if(i==1)
			System.out.println("The password "+p+" has no number!!");
		pw=p;
	}
	public String getPassword(){//return password
		return pw;
	}
}
