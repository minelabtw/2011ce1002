package a7.s100502017;
import java.lang.Math;
public class User{
	private int[] card;
	private int Joker=5,FourOfKind=4,FullHouse=3,ThreeOfkind=2,TwoPairs=1,Nothing=0;
	public User(){//constructor give the card[] initial value
		final int number=21;
		card=new int[number];
		for(int i=0,j=0;i<number;i++){
			if(i%4==0)
				j++;
			card[i]=j;
		}
	}
	public int DetermineType(int[] FiveCard){		
		int count=0,count_two=0,count_three=0;
		//------------Joker---------------
		for(int k=0;k<5;k++){
			if(FiveCard[k]==6)
				return Joker;
		}
		//-----------Other----------------
		for(int n=0;n<5;n++){
			for(int m=0;m<5;m++){
				if(FiveCard[n]==FiveCard[m])
					count++;
			}
			if(count==4)
				return FourOfKind;
			else if(count==3)
				count_three++;
			else if(count==2)
				count_two++;
			count=0;
		}
		
		if(count_three==3&&count_two==2)//use count to determined type
			return FullHouse;
		else if(count_three==3)
			return ThreeOfkind;
		else if(count_two==4)
			return TwoPairs;
		else
			return Nothing;
	}
	public int[] RandomNum(){//get a Random array[5]
		int[] RandomNumber=new int[5];
		int rand;
		for(int t=0;t<5;t++){
			rand=(int)(Math.random()*21);
			for(int h=0;h<t;h++){//can't repeat
				if(RandomNumber[h]==rand){
					rand=(int)(Math.random()*21);
					h=0;
				}
			}
			RandomNumber[t]=rand;
		}
		return RandomNumber;
	}
	public int[] RandtoCard(int[] Rand){//get card number by Random array
		int[] FiveCard=new int[5];
		for(int k=0;k<5;k++){
			FiveCard[k]=card[Rand[k]];
		}
		return FiveCard;
	}
}
