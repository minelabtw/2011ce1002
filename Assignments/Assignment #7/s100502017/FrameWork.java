package a7.s100502017;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.*;
public class FrameWork extends JFrame implements ActionListener{
	JButton Random = new JButton("random");
	JLabel[] cardspace=new JLabel[12];
	JLabel[] WinLose=new JLabel[3];
	JPanel p1=new JPanel(new GridLayout(2,6,15,100));
	JPanel p2=new JPanel(new GridLayout(3,1,5,5));
	JPanel p3=new JPanel();
	Font bigfont=new Font("Mesquite Std",Font.BOLD,50);
	public FrameWork(){
		Border line=new LineBorder(Color.BLACK,2);
		cardspace[0]=new JLabel(" He");
		cardspace[0].setFont(bigfont);
		for(int i=1;i<=5;i++){//set initial icon to label
			String name=String.valueOf(i);
			cardspace[i]=new JLabel(new ImageIcon("image/card/init_"+name+".png"));
			cardspace[i].setBorder(line);
			cardspace[i+6]=new JLabel(new ImageIcon("image/card/init_"+name+".png"));
			cardspace[i+6].setBorder(line);
		}
		cardspace[6]=new JLabel(" You");
		cardspace[6].setFont(bigfont);
		for(int j=0;j<12;j++){//make panel 1
			p1.add(cardspace[j]);
		}
		for(int k=0;k<3;k++){
			WinLose[k]=new JLabel("welcome");
			WinLose[k].setFont(bigfont);
			p2.add(WinLose[k]);
		}
		Random.setFont(bigfont);
		p3.add(p1,BorderLayout.CENTER);//use BorderLayout to set location
		p3.add(Random,BorderLayout.SOUTH);
		this.add(p3,BorderLayout.CENTER);
		this.add(p2,BorderLayout.EAST);
		Random.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e){//when button be tagged
		User game=new User();
		ImageIcon[] Card=new ImageIcon[21];
		for(int i=0;i<=20;i++){
			String num=String.valueOf(i);
			Card[i]=new ImageIcon("image/card/"+num+".png");
		}
		int[] you= new int[5];
		int[] he= new int[5];
		you=game.RandomNum();//get random array
		he=game.RandomNum();
		for(int i=1;i<=5;i++){
			cardspace[i].setIcon(Card[he[i-1]]);//change icon
			cardspace[i+6].setIcon(Card[you[i-1]]);
		}
		int You=game.DetermineType(game.RandtoCard(you));
		int He=game.DetermineType(game.RandtoCard(he));
		WinLose[0].setText(InttoType(He));//print type
		WinLose[1].setText(InttoType(You));
		if(He>You)//print result
			WinLose[2].setText("You Lose!");
		else if(He<You)
			WinLose[2].setText("You Win!");
		else
			WinLose[2].setText("Draw");
	}
	public String InttoType(int t){//get type name
		if(t==5)
			return "<Joker>";
		else if(t==4)
			return "<Four of a kind>";
		else if(t==3)
			return"<Full House>";
		else if(t==2)
			return"<Three of kind>";
		else if(t==1)
			return"<Two pairs>";
		else
			return"<Nothing>";
					
	}
}
