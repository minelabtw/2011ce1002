package a7.s100502017;

public class Password{
	private String pw;
	public Password(String p)throws InvalidPasswordException{
		boolean number=false,letter=false;
		if(p.length()<7)//Password must contain at least seven characters
			throw new InvalidPasswordException(p,3);
		for(int i=0;i<p.length();i++){
			if(Character.isLetter(p.charAt(i)))
				letter=true;
			if(!Character.isLetter(p.charAt(i)))
				number=true;
		}
		if(!letter)//Password must at least one letter 
			throw new InvalidPasswordException(p,2);
		else if(!number)//Password must at least one number
			throw new InvalidPasswordException(p,1);
		else{//Correct
			setPassword(p);
			System.out.println("The password "+pw+" is valid!!");
		}
	}
	public void setPassword(String p){
		pw=p;
	}
	public String getPassword(){//return password
		return pw;
	}
}
