package a7.s100502010;

import a7.s100502010.InvalidPasswordException;

public class Password 
{
	public Password(String password) throws InvalidPasswordException
	{
		int check=0;
		for(int counter2=0;counter2<password.length();counter2++)
		{
			if(Character.isLetter(password.charAt(counter2)))//check how many words
			{
					check++;
	 		}
		}
		if(password.length()<7)//password too short
		{
			throw new InvalidPasswordException(password);
		}
		else if(check==password.length())//password without number
		{
			throw new InvalidPasswordException(password);
		}
		else if(check==0)//password without letter
		{
			throw new InvalidPasswordException(password);
		}
		else
		{
			System.out.println("The password "+password+" is valid!!");
		}
	}
}
