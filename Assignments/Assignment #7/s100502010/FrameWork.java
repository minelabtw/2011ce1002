package a7.s100502010;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
public class FrameWork extends JFrame implements ActionListener	
{
	private ImageIcon pre1 =new ImageIcon("image/card/init_1.png");
	private ImageIcon pre2 =new ImageIcon("image/card/init_2.png");
	private ImageIcon pre3 =new ImageIcon("image/card/init_3.png");
	private ImageIcon pre4 =new ImageIcon("image/card/init_4.png");
	private ImageIcon pre5 =new ImageIcon("image/card/init_5.png");
	
	private JButton jbtrandom=new JButton("Random");
	private JLabel jlabelresult=new JLabel("start");
	
	private JLabel[] User = new JLabel[6];
	private JLabel[] Com = new JLabel[6];
	private ImageIcon[] usernew = new ImageIcon[5];
	private ImageIcon[] comnew = new ImageIcon[5];
	
	JPanel p1=new JPanel();
	JPanel p2=new JPanel();
	JPanel p3=new JPanel();
	JPanel p4=new JPanel();
	JLabel AI=new JLabel("AI");
	JLabel MAN=new JLabel("MAN");
	JLabel comre=new JLabel("result");
	JLabel manre=new JLabel("result");
	
	public FrameWork()
	{
		User[0] =(new JLabel (pre1));
	    User[1] =(new JLabel (pre2));
	    User[2] =(new JLabel (pre3));
	    User[3] =(new JLabel (pre4));
	    User[4] =(new JLabel (pre5));
	    User[5] =(new JLabel());
	    Com[0] =(new JLabel (pre1));
	    Com[1] =(new JLabel (pre2));
	    Com[2] =(new JLabel (pre3));
	    Com[3] =(new JLabel (pre4));
	    Com[4] =(new JLabel (pre5));
	    Com[5] =(new JLabel());
    
	    Font font = new Font("Sarif",Font.BOLD,18);

		AI.setFont(font);//設定字型
		MAN.setFont(font);
		
		p1.setLayout(new GridLayout(2,5,10,10));
	
		p1.add(Com[0]);//牌的位置
		p1.add(Com[1]);
		p1.add(Com[2]);
		p1.add(Com[3]);
		p1.add(Com[4]);
		
		p1.add(User[0]);//牌的位置
		p1.add(User[1]);
		p1.add(User[2]);
		p1.add(User[3]);
		p1.add(User[4]);

		p2.setLayout(new GridLayout(2,1,0,0));//各自的牌形
		p2.add(comre);
		p2.add(manre);

		
		p3.setLayout(new BorderLayout());//ramdom和輸贏的位置
		p3.add(jbtrandom,BorderLayout.WEST);
		p3.add(jlabelresult,BorderLayout.EAST);
		
		p4.setLayout(new GridLayout(2,1,0,0));
		p4.add(AI);
		p4.add(MAN);
		
		
		jbtrandom.addActionListener(this);
		
		setLayout(new BorderLayout()); 
		add(p1,BorderLayout.CENTER);
		add(p2,BorderLayout.EAST);
		add(p3,BorderLayout.SOUTH);
		add(p4,BorderLayout.WEST);
		
	}
	public void actionPerformed(ActionEvent e) 
	{
		int rankcom=0;
		int rankman=0;
		
		if(e.getSource()==jbtrandom)
		{
			User use=new User();
			usernew=use.usercard;
			comnew=use.comcard;
			
			for(int counter=0;counter<5;counter++)
			{
				User[counter].setIcon(usernew[counter]);
				Com[counter].setIcon(comnew[counter]);
			}
			int therankofuser=use.tojudge(use.usercardcon);
			int therankofcom=use.tojudge(use.comcardcon);
			
			manre.setText(use.finalanswer(therankofuser));//更改各自的牌形
			comre.setText(use.finalanswer(therankofcom));
			
			if(therankofuser>therankofcom)
			{
				jlabelresult.setText("You Win");
			}
			else if(therankofuser<therankofcom)
			{
				jlabelresult.setText("You lose");
			}
			else if(therankofuser==therankofcom)
			{
				jlabelresult.setText("Draw");
			}
		}
	}
}