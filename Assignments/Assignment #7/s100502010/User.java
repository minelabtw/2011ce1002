package a7.s100502010;

import java.util.*;
import javax.swing.ImageIcon;

public class User 
{
	private ImageIcon im1 =new ImageIcon("image/card/0.png");
	private ImageIcon im2 =new ImageIcon("image/card/1.png");
	private ImageIcon im3 =new ImageIcon("image/card/2.png");
	private ImageIcon im4 =new ImageIcon("image/card/3.png");
	private ImageIcon im5 =new ImageIcon("image/card/4.png");
	private ImageIcon im6 =new ImageIcon("image/card/5.png");
	private ImageIcon im7 =new ImageIcon("image/card/6.png");
	private ImageIcon im8 =new ImageIcon("image/card/7.png");
	private ImageIcon im9 =new ImageIcon("image/card/8.png");
	private ImageIcon im10 =new ImageIcon("image/card/9.png");
	private ImageIcon im11 =new ImageIcon("image/card/10.png");
	private ImageIcon im12 =new ImageIcon("image/card/11.png");
	private ImageIcon im13 =new ImageIcon("image/card/12.png");
	private ImageIcon im14 =new ImageIcon("image/card/13.png");
	private ImageIcon im15 =new ImageIcon("image/card/14.png");
	private ImageIcon im16 =new ImageIcon("image/card/15.png");
	private ImageIcon im17 =new ImageIcon("image/card/16.png");
	private ImageIcon im18 =new ImageIcon("image/card/17.png");
	private ImageIcon im19 =new ImageIcon("image/card/18.png");
	private ImageIcon im20 =new ImageIcon("image/card/19.png");
	private ImageIcon im21 =new ImageIcon("image/card/20.png");
	private ImageIcon[] picture={im1,im2,im3,im4,im5,im6,im7,im8,im9,im10,im11,im12,im13,im14,im15,im16,im17,im18,im19,im20,im21};
	public int[] usercardcon=new int[6];
	public int[] comcardcon=new int [6];
	public ImageIcon[] usercard= new ImageIcon[5];
	public ImageIcon[] comcard= new ImageIcon[5];
	
	User()
	{
		for(int counter=0;counter<5;counter++)
		{
			int random=random();
			for(int counter1=0;counter1<5;counter1++)
			{
				
				if(picture[random]==null)//如果用過的牌就會是null
				{
					random=random();
					counter1=1;
				}
				else
				{
					counter1=10;
				}
			}
			if(random ==0||random==1||random==2||random==3)
			{
				usercardcon[0]++; //A
			}
			else if(random ==4||random ==5||random==6||random==7)
			{
				usercardcon[1]++; //2
			}
			else if(random ==8||random ==9||random==10||random==11)
			{
				usercardcon[4]++; //J
			}
			else if(random ==12||random ==13||random==14||random==15)
			{
				usercardcon[3]++; //Q
			}
			else if(random ==16||random ==17||random==18||random==19)
			{
				usercardcon[2]++; //K
			}	
			else
			{
				usercardcon[5]++; //JOKER
			}
			usercard[counter]=picture[random];
			picture[random]=null;
		}
		
		for(int counter=0;counter<5;counter++)
		{
			int random=random();
			for(int counter1=0;counter1<5;counter1++)
			{
				if(picture[random]==null)//如果用過的牌就會是null
				{
					random=random();
					counter1=1;
				}
				else
				{
					counter1=10;
				}
			}
			if(random ==0||random==1||random==2||random==3)
			{
				comcardcon[0]++; //A
			}
			else if(random ==4||random ==5||random==6||random==7)
			{
				comcardcon[1]++; //2
			}
			else if(random ==8||random ==9||random==10||random==11)
			{
				comcardcon[4]++; //J
			}
			else if(random ==12||random ==13||random==14||random==15)
			{
				comcardcon[3]++; //Q
			}
			else if(random ==16||random ==17||random==18||random==19)
			{
				comcardcon[2]++; //K
			}	
			else
			{
				comcardcon[5]++; //JOKER
			}
			comcard[counter]=picture[random];
			picture[random]=null;
		}
	}
	public int tojudge(int[] cardcon)
	{
		if(cardcon[5]==1)
		{//joker
			return 6;
		}
		else if(cardcon[0]==4 ||cardcon[1]==4||cardcon[2]==4||cardcon[3]==4||cardcon[4]==4)
		{//four of a kind
			return 5;
		}
		else if(cardcon[0]==1 && cardcon[1]==1 && cardcon[2]==1 &&cardcon[3]==1 &&cardcon[4]==1)
		{//two pairs
			return 1;
		}
		else if((cardcon[0]==3&&cardcon[1]==2)||(cardcon[0]==3&&cardcon[2]==2)||(cardcon[0]==3&&cardcon[3]==2)||(cardcon[0]==3&&cardcon[4]==2)||(cardcon[1]==3&&cardcon[0]==2)||(cardcon[1]==3&&cardcon[2]==2)||(cardcon[1]==3&&cardcon[3]==2)||(cardcon[1]==3&&cardcon[4]==2)||(cardcon[2]==3&&cardcon[0]==2)||(cardcon[2]==3&&cardcon[1]==2)||(cardcon[2]==3&&cardcon[3]==2)||(cardcon[2]==3&&cardcon[4]==2)||(cardcon[3]==3&&cardcon[0]==2)||(cardcon[3]==3&&cardcon[1]==2)||(cardcon[3]==3&&cardcon[2]==2)||(cardcon[3]==3&&cardcon[4]==2)||(cardcon[4]==3&&cardcon[0]==2)||(cardcon[4]==3&&cardcon[1]==2)||(cardcon[4]==3&&cardcon[2]==2)||(cardcon[4]==3&&cardcon[3]==2))
		{//葫蘆
			return 4;
		}
		else if(cardcon[0]==3 && cardcon[1]==3 && cardcon[2]==3 && cardcon[3]==3 && cardcon[4]==3)
		{//三條
			return 3;
		}
		else
		{
			return 2;
		}
	}
	public String finalanswer(int condi)//判斷牌形
	{
		if(condi==2)
		{
			return "<Nothing>";
		}
		else if(condi ==1)
		{
			return "<Two pairs>";
		}
		else if(condi ==3)
		{
			return "<Three of kind>";
		}
		else if(condi ==4)
		{
			return "<Full house>";
		}
		else if(condi ==5)
		{
			return "<Four of a kind>";
		}
		else
		{
			return "<Joker>";
		}
	}
	public int random()
	{
		int top=21;
		Random randomuser=new Random();
		int answer=randomuser.nextInt(top);
		return answer;	
	}
	
	
}
