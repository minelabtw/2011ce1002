package a7.s982003034;

public class Password {
	private String password;
	
	public Password (String pass) throws InvalidPasswordException {
		
		password = pass;
		
		boolean letter = false;
		boolean digit = false;
		
		for (int i = 0; i < password.length(); i++) {
			if (Character.isLetter(password.charAt(i))) letter = true;
			if (Character.isDigit(password.charAt(i))) digit = true;
		}
		
		if (password.length() < 7) {
			throw new InvalidPasswordException ("The length of the password " + password + " is not enough!");
		}
		
		else if (!letter) {
			throw new InvalidPasswordException ("The password " + password + " has no letter!");
		}
		
		else if (!digit) {
			throw new InvalidPasswordException ("The password " + password + " has no number!");
		}
		
		else System.out.println ("password " + password + "is valid!");
		
				
	}
}
