package a7.s982003034;

import java.util.Scanner;

public class A72 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Password pass;		
		
		String input;
		Scanner sc = new Scanner (System.in);
		while (true) {
			System.out.println ("\nPlease insert a password to verify:");
			input = sc.next();
			try {
				pass = new Password (input);
			} catch (InvalidPasswordException e) {
				System.out.println ("password " + input + " is invalid!");
			}
		}
	}

}
