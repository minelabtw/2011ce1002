package a7.s982003034;

import java.util.ArrayList;

public class User {
	
	int deck_size;
	int hand_size;
	
	int [] player1_card;
	int player1_score;
	String player1_type;
		
	int [] player2_card;
	int player2_score;
	String player2_type;
	
	
	public User (int dsize, int hsize) {
		
		deck_size = dsize;
		hand_size = hsize;
		
		player1_card = new int [hand_size];
		player2_card = new int [hand_size];
		distribute(player1_card, player2_card, hand_size, deck_size);
		
		player1_score = determine_score (player1_card);
		player2_score = determine_score (player2_card);
		
		player1_type = determine_type (player1_score);
		player2_type = determine_type (player2_score);
		
	}
	
	public int[] get_player1_card () {
		int [] temp = new int [hand_size];
		for (int i = 0; i < hand_size; i++) temp [i] = player1_card[i];
		return temp; 
	}
	
	public int get_player1_score () {
		int temp = player1_score;
		return temp; 
	}
	
	public String get_player1_type () {
		String temp = player1_type;
		return temp;
	}
	
	public int[] get_player2_card () {
		int [] temp = new int [hand_size];
		for (int i = 0; i < hand_size; i++) temp [i] = player2_card[i];
		return temp; 
	}
	
	public int get_player2_score () {
		int temp = player2_score;
		return temp; 
	}
	
	public String get_player2_type () {
		String temp = player2_type;
		return temp;
	}
	
	private void distribute (int[] arr1, int[] arr2, int size, int range) {
		
		// this array list simply hold the filename of the cards
		ArrayList<Integer> temp_deck = new ArrayList <Integer> (range);
		for (int i = 0; i < range; i++) {
			temp_deck.add(i);
		}
		
		// every time a card has been selected, it is deleted from the array list
		// so we do not need to determine whether the card has been used or not
		int [] temp_hand = new int [size*2];
		for (int i = 0; i < temp_hand.length; i++) {
			int pos = (int)(Math.random()*temp_deck.size());
			temp_hand[i] = temp_deck.get(pos);
			temp_deck.remove(pos);
			
		}
		
		// give the first half of the card to the first player
		// give the other half of the card to the second player
		for (int i = 0; i < size; i++) {
			arr1 [i]= temp_hand[i];
			arr2 [i]= temp_hand[i+size];
		}
		
		
	}
	
	private int determine_score (int[] hand) {

		/* counter that holds the amount of all types of card
		 * 0 = joker
		 * 1 = ace
		 * 2 = 2
		 * ...
		 * 9 = 9
		 * 10 = 10
		 * 11 = jack
		 * 12 = queen
		 * 13 = king
		 */
		int [] cardcounter = new int [14];
		
		for (int i = 0; i < 14; i ++) cardcounter [i] = 0;
		
		// check every type of card in the hand
		for (int i = 0; i < hand.length; i++) {
			switch (hand[i]) {
			
			// card is ace
			case 0:case 1: case 2: case 3:
				cardcounter[1]++;
				break;
			// card is 2
			case 4:	case 5:	case 6:	case 7:
				cardcounter[2]++;
				break;
			// card is jack
			case 8:	case 9:	case 10: case 11:
				cardcounter[11]++;
				break;
			// card is queen
			case 12: case 13: case 14: case 15:
				cardcounter[12]++;
				break;
			// card is king
			case 16: case 17: case 18: case 19:
				cardcounter[13]++;
				break;
			// card is joker
			case 20: cardcounter[0]++;
				break;
				
			} // switch
		} // for
		
		/* give score
		 * score[0] = pair
		 * score[1] = triple
		 * score[2] = four
		 */
		int [] score = new int[3];
		for (int i = 0; i < score.length; i++) score[i] = 0;
		// scan again for score
		for (int i = 0; i < cardcounter.length; i++) {
			switch (cardcounter[i]) {
			case 2: score[0]++;
			case 3: score[1]++;
			case 4: score[2]++;
			}
		}
		
		if (cardcounter[0] > 0) return 5; // joker
		else if (score [2] == 1) return 4; // four of a kind
		else if (score [1] == 1 && score[0] == 1) return 3; // full house
		else if (score [1] == 1) return 2; // three of a kind
		else if (score [1] == 2) return 1; // two pairs
		else return 0; // normal
		
	}
	
	private String determine_type (int score) {
		// simply convert score to string
		switch (score) {
		case 0: return "normal";
		case 1: return "two pairs";
		case 2: return "three of a kind";
		case 3: return "full house";
		case 4: return "four of a kind";
		case 5: return "joker";
		default: return "";
		}
		
		
	}
	
}