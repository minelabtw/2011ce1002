package a7.s982003034;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class FrameWork extends JFrame implements ActionListener {
	
	final String card_path = "image/card/";
	final String picture_format = ".png";
	
	User user;
	
	// player1's stuff
	JLabel player1_name; // the player's name
	JLabel player1_type; // what kind of hand the player holds
	ImageIcon [] player1_card_icon; // array of the pointer to card image
	JLabel[] player1_card_label; // the label to put the image
	
	// player2's stuff
	JLabel player2_name;
	JLabel player2_type;
	ImageIcon [] player2_card_icon;
	JLabel[] player2_card_label;
	
	// misc stuff
	JButton random;
	JLabel win;
	
	// the player's stuff above are further contained in the panels below
	JPanel player_name; // holds the players' name
	JPanel player_card; // holds the players' card
	JPanel player_score; // holds the players' type
	JPanel button_panel; // holds the misc stuff
	
	
	
	public FrameWork () {
		
		// construct players stuff
		player1_name = new JLabel ("Comp");
		player1_type = new JLabel ("");
		player1_card_icon = new ImageIcon[5];
		player1_card_label = new JLabel[5];
		
		player2_name = new JLabel ("You");
		player2_type = new JLabel ("");
		player2_card_icon = new ImageIcon[5];
		player2_card_label = new JLabel[5];
		
		// get image
		for (int i = 0; i < 5; i ++) {
			player1_card_icon[i] = getimage (card_path + "init_" + (i+1) + picture_format);
			player1_card_label[i] = new JLabel (player1_card_icon[i]);
			player2_card_icon[i] = getimage (card_path + "init_" + (i+1) + picture_format);
			player2_card_label[i] = new JLabel (player2_card_icon[i]);
		}
		
		random = new JButton ("Random");
		random.addActionListener (this);
		win = new JLabel ("<< Press this!");
		
		// initialize panels
		player_name = new JPanel();
		player_card = new JPanel();
		player_score = new JPanel();
		button_panel = new JPanel();
		
		// set layouts
		this.setLayout(new BorderLayout());
		player_name.setLayout(new GridLayout (2,1,2,0));
		player_card.setLayout(new GridLayout (2,5,2,0));
		player_score.setLayout(new GridLayout (2,1,2,0));
		button_panel.setLayout(new GridLayout (1,2,2,0));
		
		// add player stuff to the panels
		player_name.add(player1_name);
		player_name.add(player2_name);
		for (int i = 0; i < 5; i ++) player_card.add(player1_card_label[i]);
		for (int i = 0; i < 5; i ++) player_card.add(player2_card_label[i]);
		player_score.add(player1_type);
		player_score.add(player2_type);
		button_panel.add(random);
		button_panel.add(win);
		
		// add panels to the main frame
		this.add (player_name, BorderLayout.WEST);
		this.add (player_card, BorderLayout.CENTER);
		this.add (player_score, BorderLayout.EAST);
		this.add (button_panel, BorderLayout.SOUTH);
		
		// note that user has not yet been initialized
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// because there is only one source of event (random button)
		// there is no need to determine the source of event
		
		// user is initialized here
		user = new User (20, 5);

		// set the displayed gui to follow user data
		player1_type.setText (user.get_player1_type());
		player2_type.setText (user.get_player2_type());
		
		int [] temp_card1 = user.get_player1_card();
		int [] temp_card2 = user.get_player2_card();
		
		// find the new card icon
		for (int i = 0; i < 5; i ++) {
			player1_card_icon[i] = getimage (card_path + temp_card1[i] + picture_format);
			player1_card_label[i].setIcon(player1_card_icon[i]);
			player2_card_icon[i] = getimage (card_path + temp_card2[i] + picture_format);
			player2_card_label[i].setIcon(player2_card_icon[i]);
		}
		
		// determine win and lose
		if (user.get_player2_score() > user.get_player1_score()) win.setText("YOU WIN!!!");
		else if (user.get_player2_score() < user.get_player1_score()) win.setText("YOU LOSE");
		else win.setText("DRAW");
		
	}
	
	// function to obtain image
	private ImageIcon getimage (String path) {
		
		URL imgurl = FrameWork.class.getResource(path);
		if (imgurl != null) return new ImageIcon (imgurl, "");
		else {
			System.err.println ("Cannot find image!");
			return null;
		}
		
	}
	
	
	
}
