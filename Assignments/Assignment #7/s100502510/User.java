package a7.s100502510;

import java.util.Random;

public class User {
	static Random rand = new Random();
	static int cards[] = new int[5];// 代表一個人的五張牌
	static int Result;// 用來顯示最後的牌型
	static int times = 0;// 用來儲存重複的次數

	public static int random(int Rand) {// 使用random
		Rand = rand.nextInt(21);
		return Rand;
	}

	public static int setcards(int card) {// 每一種路徑的牌代表何種牌
		int card2 = 0;
		if (card == 0 || card == 1 || card == 2 || card == 3) {
			card2 = 1;
		} else if (card == 4 || card == 5 || card == 6 || card == 7) {
			card2 = 2;
		} else if (card == 8 || card == 9 || card == 10 || card == 11) {
			card2 = 11;
		} else if (card == 12 || card == 13 || card == 14 || card == 15) {
			card2 = 12;
		} else if (card == 16 || card == 17 || card == 18 || card == 19) {
			card2 = 13;
		} else if (card == 20) {
			card2 = 14;
		}
		return card2;
	}

	public static int type(int rand1, int rand2, int rand3, int rand4, int rand5) {
		cards[0] = setcards(rand1);
		cards[1] = setcards(rand2);
		cards[2] = setcards(rand3);
		cards[3] = setcards(rand4);
		cards[4] = setcards(rand5);
		int times = 0;
		for (int i = 0; i <= 4; i++) {
			if (i == 4) {
				break;
			}
			for (int j = i + 1; j <= 4; j++) {
				if (cards[i] == cards[j]) {
					times++;
				}
			}
		}
		for (int i = 0; i <= 4; i++) {

			if (cards[i] == 14) {// Joker
				Result = 5;
				break;
			} else if (times == 2 && cards[i] != 14) {// Two pairs
				Result = 1;
			} else if (times == 3 && cards[i] != 14) {// Three of a kind
				Result = 2;
			} else if (times == 4 && cards[i] != 14) {// Full house
				Result = 3;
			} else if (times == 6 && cards[i] != 14) {// Four of a kind
				Result = 4;
			} else {
				Result = 0;// Nothing
			}
		}
		return Result;
	}

}
