package a7.s100502510;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class FrameWork extends JFrame implements ActionListener {
	Font font = new Font("serif", Font.BOLD, 20);// user跟computer的Font
	Font font2 = new Font("serif", Font.BOLD, 150);// random button的Font
	Font font3 = new Font("serif", Font.BOLD, 15);// 顯示出牌型的Font
	Font font4 = new Font("serif", Font.BOLD, 60);// 顯示輸贏的Font
	JButton button = new JButton("random");
	JPanel p1 = new JPanel();// 分成兩個Panel,此為第一個
	JPanel p2 = new JPanel();// 第二個
	JLabel label1 = new JLabel("Computer");
	JLabel label2 = new JLabel("User");
	JLabel label3 = new JLabel("");// 用來顯示牌型
	JLabel label4 = new JLabel("");// 用來顯示牌型
	JLabel label5 = new JLabel("");// 用來顯示輸贏
	JLabel picture1 = new JLabel();// 這些picture是用來顯示撲克牌的
	JLabel picture2 = new JLabel();
	JLabel picture3 = new JLabel();
	JLabel picture4 = new JLabel();
	JLabel picture5 = new JLabel();
	JLabel picture6 = new JLabel();
	JLabel picture7 = new JLabel();
	JLabel picture8 = new JLabel();
	JLabel picture9 = new JLabel();
	JLabel picture10 = new JLabel();
	ImageIcon carda = new ImageIcon("image/card/init_1.png");// 一開始的圖片
	ImageIcon cardb = new ImageIcon("image/card/init_2.png");
	ImageIcon cardc = new ImageIcon("image/card/init_3.png");
	ImageIcon cardd = new ImageIcon("image/card/init_4.png");
	ImageIcon carde = new ImageIcon("image/card/init_5.png");
	int rand = 1;

	public FrameWork() {
		button.addActionListener(this);
		label1.setFont(font);
		label2.setFont(font);
		label3.setFont(font3);
		label4.setFont(font3);
		label5.setFont(font4);
		button.setFont(font2);
		p1.setLayout(new GridLayout(2, 6, 5, 10));// 第一個Panel
		p1.add(label1);
		picture1 = new JLabel(carda);
		picture1.setBorder(new LineBorder(Color.BLACK, 2));
		p1.add(picture1);
		picture2 = new JLabel(cardb);
		picture2.setBorder(new LineBorder(Color.BLACK, 2));
		p1.add(picture2);
		picture3 = new JLabel(cardc);
		picture3.setBorder(new LineBorder(Color.BLACK, 2));
		p1.add(picture3);
		picture4 = new JLabel(cardd);
		picture4.setBorder(new LineBorder(Color.BLACK, 2));
		p1.add(picture4);
		picture5 = new JLabel(carde);
		picture5.setBorder(new LineBorder(Color.BLACK, 2));
		p1.add(picture5);
		p1.add(label3);
		p1.add(label2);
		picture6 = new JLabel(carda);
		picture6.setBorder(new LineBorder(Color.BLACK, 2));
		p1.add(picture6);
		picture7 = new JLabel(cardb);
		picture7.setBorder(new LineBorder(Color.BLACK, 2));
		p1.add(picture7);
		picture8 = new JLabel(cardc);
		picture8.setBorder(new LineBorder(Color.BLACK, 2));
		p1.add(picture8);
		picture9 = new JLabel(cardd);
		picture9.setBorder(new LineBorder(Color.BLACK, 2));
		p1.add(picture9);
		picture10 = new JLabel(carde);
		picture10.setBorder(new LineBorder(Color.BLACK, 2));
		p1.add(picture10);
		p1.add(label4);
		p2.setLayout(new BorderLayout());// 第二個Panel,將button跟顯示輸贏的label合在一起
		p2.add(button, BorderLayout.WEST);
		p2.add(label5, BorderLayout.CENTER);
		add(p1, BorderLayout.CENTER);
		add(p2, BorderLayout.SOUTH);
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == button) {
			User computer = new User();// 將computer帶入User
			User user = new User();// 將user帶入User
			int rand1 = User.random(rand);
			int rand2;
			while (1 == 1) {// 判斷是否重複
				rand2 = User.random(rand);
				if (rand2 != rand1) {
					break;
				}
			}
			int rand3;
			while (1 == 1) {
				rand3 = User.random(rand);
				if (rand3 != rand1 && rand3 != rand2) {
					break;
				}
			}
			int rand4;
			while (1 == 1) {
				rand4 = User.random(rand);
				if (rand4 != rand1 && rand4 != rand2 && rand4 != rand3) {
					break;
				}
			}
			int rand5;
			while (1 == 1) {
				rand5 = User.random(rand);
				if (rand5 != rand1 && rand5 != rand2 && rand5 != rand3
						&& rand5 != rand4) {
					break;
				}
			}
			int rand6;
			while (1 == 1) {
				rand6 = User.random(rand);
				if (rand6 != rand1 && rand6 != rand2 && rand6 != rand3
						&& rand6 != rand4 && rand6 != rand5) {
					break;
				}
			}
			int rand7;
			while (1 == 1) {
				rand7 = User.random(rand);
				if (rand7 != rand1 && rand7 != rand2 && rand7 != rand3
						&& rand7 != rand4 && rand7 != rand5 && rand7 != rand6) {
					break;
				}
			}
			int rand8;
			while (1 == 1) {
				rand8 = User.random(rand);
				if (rand8 != rand1 && rand8 != rand2 && rand8 != rand3
						&& rand8 != rand4 && rand8 != rand5 && rand8 != rand6
						&& rand8 != rand7) {
					break;
				}
			}
			int rand9;
			while (1 == 1) {
				rand9 = User.random(rand);
				if (rand9 != rand1 && rand9 != rand2 && rand9 != rand3
						&& rand9 != rand4 && rand9 != rand5 && rand9 != rand6
						&& rand9 != rand7 && rand9 != rand8) {
					break;
				}
			}
			int rand10;
			while (1 == 1) {
				rand10 = User.random(rand);
				if (rand10 != rand1 && rand10 != rand2 && rand10 != rand3
						&& rand10 != rand4 && rand10 != rand5
						&& rand10 != rand6 && rand10 != rand7
						&& rand10 != rand8 && rand10 != rand9) {
					break;
				}
			}
			if (computer.type(rand1, rand2, rand3, rand4, rand5) == 5) {// 每種數字代表一種牌型
				label3.setText("<Joker>");
			} else if (computer.type(rand1, rand2, rand3, rand4, rand5) == 4) {
				label3.setText("<Four of a kind>");
			} else if (computer.type(rand1, rand2, rand3, rand4, rand5) == 3) {
				label3.setText("<Full house>");
			} else if (computer.type(rand1, rand2, rand3, rand4, rand5) == 2) {
				label3.setText("<Three of a kind>");
			} else if (computer.type(rand1, rand2, rand3, rand4, rand5) == 1) {
				label3.setText("<Two pairs>");
			} else if (computer.type(rand1, rand2, rand3, rand4, rand5) == 0) {
				label3.setText("<Nothing>");
			}
			if (user.type(rand6, rand7, rand8, rand9, rand10) == 5) {
				label4.setText("<Joker>");
			} else if (user.type(rand6, rand7, rand8, rand9, rand10) == 4) {
				label4.setText("<Four of a kind>");
			} else if (user.type(rand6, rand7, rand8, rand9, rand10) == 3) {
				label4.setText("<Full house>");
			} else if (user.type(rand6, rand7, rand8, rand9, rand10) == 2) {
				label4.setText("<Three of a kind>");
			} else if (user.type(rand6, rand7, rand8, rand9, rand10) == 1) {
				label4.setText("<Two pairs>");
			} else if (user.type(rand6, rand7, rand8, rand9, rand10) == 0) {
				label4.setText("<Nothing>");
			}
			if (user.type(rand6, rand7, rand8, rand9, rand10) > computer.type(// 比大小
					rand1, rand2, rand3, rand4, rand5)) {
				label5.setText("User wins!");
			} else if (user.type(rand6, rand7, rand8, rand9, rand10) == computer
					.type(rand1, rand2, rand3, rand4, rand5)) {
				label5.setText("Draw!");
			} else if (user.type(rand6, rand7, rand8, rand9, rand10) < computer
					.type(rand1, rand2, rand3, rand4, rand5)) {
				label5.setText("Computer wins!");
			}
			ImageIcon carda = new ImageIcon("image/card/" + rand1 + ".png");// 帶入新的圖形
			ImageIcon cardb = new ImageIcon("image/card/" + rand2 + ".png");
			ImageIcon cardc = new ImageIcon("image/card/" + rand3 + ".png");
			ImageIcon cardd = new ImageIcon("image/card/" + rand4 + ".png");
			ImageIcon carde = new ImageIcon("image/card/" + rand5 + ".png");
			ImageIcon cardf = new ImageIcon("image/card/" + rand6 + ".png");
			ImageIcon cardg = new ImageIcon("image/card/" + rand7 + ".png");
			ImageIcon cardh = new ImageIcon("image/card/" + rand8 + ".png");
			ImageIcon cardi = new ImageIcon("image/card/" + rand9 + ".png");
			ImageIcon cardj = new ImageIcon("image/card/" + rand10 + ".png");
			picture1.setIcon(carda);
			picture2.setIcon(cardb);
			picture3.setIcon(cardc);
			picture4.setIcon(cardd);
			picture5.setIcon(carde);
			picture6.setIcon(cardf);
			picture7.setIcon(cardg);
			picture8.setIcon(cardh);
			picture9.setIcon(cardi);
			picture10.setIcon(cardj);
		}
	}
}
