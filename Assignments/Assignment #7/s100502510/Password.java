package a7.s100502510;

public class Password {
	String password;

	public Password(String password2) throws InvalidPasswordException {
		password = password2;
		for (;;) {// 無限迴圈,當有一個條件符合就跳出
			try {
				InvalidPasswordException.exceptionone();

			} catch (InvalidPasswordException exception1) {// 字元不足時
				System.out.println("The length of the password " + password + " is not enough!!");
				System.out.println("The password " + password + " in not correct!!");
				break;
			}
			try {
				InvalidPasswordException.exceptiontwo();

			} catch (InvalidPasswordException exception2) {// 密碼沒數字時
				System.out.println("The password " + password + " has no number!!");
				System.out.println("The password " + password + " in not correct!!");
				break;
			}
			try {
				InvalidPasswordException.exceptionthree();
				System.out.println("The password " + password + " is valid!!");
				break;
			} catch (InvalidPasswordException exception3) {// 密碼沒字母時
				System.out.println("The password " + password + " has no letter!!");
				System.out.println("The password " + password + " in not correct!!");
				break;
			}
		}
	}
}
