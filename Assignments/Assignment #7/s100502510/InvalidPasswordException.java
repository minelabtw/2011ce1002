package a7.s100502510;

public class InvalidPasswordException extends Exception {

	static InvalidPasswordException exception1;// 儲存各種例外
	static InvalidPasswordException exception2;
	static InvalidPasswordException exception3;

	public static void exceptionone() throws InvalidPasswordException {// 字元不足時
		if (A72.inputed.length() < 7) {
			exception1 = new InvalidPasswordException();
			throw exception1;
		}
	}

	public static void exceptiontwo() throws InvalidPasswordException {// 沒有數字時
		int times = 0;// 儲存數字數
		for (int i = 0; i < A72.inputed.length(); i++) {
			for (int j = 48; j <= 57; j++) {
				if (A72.inputed.charAt(i) == j) {
					times++;
				}
			}
		}
		if (times == 0) {
			exception2 = new InvalidPasswordException();
			throw exception2;
		}
	}

	public static void exceptionthree() throws InvalidPasswordException {// 沒有字母時
		int times = 0;// 儲存字母數
		for (int i = 0; i < A72.inputed.length(); i++) {

			for (int k = 97; k <= 122; k++) {
				if (A72.inputed.charAt(i) == k) {
					times++;
				}
			}
		}
		if (times == 0) {
			exception3 = new InvalidPasswordException();
			throw exception3;
		}
	}
}
