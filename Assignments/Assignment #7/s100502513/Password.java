package a7.s100502513;

public class Password {
	private String password;  

	public Password() throws InvalidPasswordException {  //default
		this("");
	}

	public Password(String instring) throws InvalidPasswordException {
		password = instring;
		if (checklength() == false) {  //如果長度不夠
			throw new InvalidPasswordException("The length of the password " + password + " is not enough!!");
		} 
		else if (checksletter() == false && checkbletter() == false) {  //如果沒英文字母
			throw new InvalidPasswordException("The password " + password + " has no letter!!");
		}
		else if (checknumber() == false) {  //如果沒數字
			throw new InvalidPasswordException("The password " + password + " has no number!!");
		} 
		else if (checknumber() == true && checknumber() == true && checknumber() == true){  //都符合條件
			
		}
	}

	public boolean checklength() {  //判斷長度是否大於7
		if (password.length() < 7)
			return false;
		else {
			return true;
		}
	}

	public boolean checksletter() {  //判斷是否有小寫英文字母
		for(int i=0;i<password.length();i++){
			if (password.charAt(i)*1  >=97 && password.charAt(i)*1  <= 122) {  //小寫英文ASCII範圍98~122
				return true;  //如果是在範圍內TRUE
			} 
			else
				continue;
		}
		return false;
	}

	public boolean checkbletter() {  //判斷是否有大寫英文字母
		for(int i=0;i<password.length();i++){
			if (password.charAt(i)*1 >=65 && password.charAt(i)*1  <= 90) {  //大寫英文ACSII範圍65~90
				return true;  //如果是在範圍內TRUE
			} 
			else
				continue;
		}
		return false;
	}

	public boolean checknumber() {  //判斷是否有數字
		for(int i=0;i<password.length();i++){
			if (password.charAt(i)*1 >=48 && password.charAt(i)*1 <= 57) {  //數字ASCII範圍48~57
				return true;  //如果是在範圍內TRUE
			} 
			else
				continue;
		}
		return false;
	}
}