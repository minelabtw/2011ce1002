package a7.s100502513;

import java.util.Scanner;

public class A72 {
	public static void main(String args[]) {
		for (;;) {
			System.out.println("Please input a password to varify: ");
			Scanner input = new Scanner(System.in);
			String inString = input.next();
			try {
				Password word = new Password(inString);  //例外處理
				System.out.println("The password " + inString + " is valid!!");  //成功，顯示一段話
				System.out.println();
			} 
			catch (InvalidPasswordException e) {
				System.out.println(e.getMessage());  //回傳一段話如果有例外
				System.out.println("The password " + inString + " is not correct!!");
				System.out.println();
			}
		}
	}
}
