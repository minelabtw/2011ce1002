package a7.s100502513;

public class InvalidPasswordException extends Exception {
	
	public InvalidPasswordException() {
	}
	
	public InvalidPasswordException(String passw) {
		super(passw);
	}
}
