package a7.s100502513;

import java.util.*;

public class User {
	private int[] cards = new int[5];  //使用者的牌
	private int[] cards2 = new int[5];  //電腦的牌
	private int Joker = 5;  //牌的類型以及大小
	private int Fourofakind = 4;
	private int FullHouse = 3;
	private int Threeofakind = 2;
	private int Twopairs = 1;
	private int Nothing = 0;

	private Random random = new Random();  //亂數
	
	User() {
		cards=this.random(random);  //產生5張亂數
		cards2=this.random(random);
	}
	
	public int[] random(Random ran){  //產生不重複的亂數
		 int[] number = new int[21];
		 int[] card = new int[5];
		 int temp,temp2;
		 int count=4;
		 for(int i=0;i<number.length;i++) {
			 number[i]=i;
		 }
		 while(count!=0) {
			 temp = number[count];
			 temp2=ran.nextInt(22-count);
			 number[count]=number[temp2];
			 number[temp2]=temp;
			 count--;
		 }
		 for(int j=0;j<5;j++)
			 card[j]=number[j];
		 return card;
	}
	
	public char[] point(int[] card) {  //判斷牌的花色
		char[] p = new char[5];
		for (int i = 0; i < 5; i++) {
			if (card[i] == 0 || card[i] == 1 || card[i] == 2 || card[i] == 3)
				p[i] = 'A';
			else if (card[i] == 4 || card[i] == 5 || card[i] == 6 || card[i] == 7)
				p[i] = '2';
			else if (card[i] == 8 || card[i] == 9 || card[i] == 10 || card[i] == 11)
				p[i] = 'J';
			else if (card[i] == 12 || card[i] == 13 || card[i] == 14 || card[i] == 15)
				p[i] = 'Q';
			else if (card[i] == 16 || card[i] == 17 || card[i] == 18 || card[i] == 19)
				p[i] = 'K';
			else if (card[i] == 20)
				p[i] = 'X';
		}
		return p;
	}

	public int check(char[] card) {  //判斷牌型
		int same = 0;
		for (int x = 0; x < 5; x++) {
			if (card[x] == 'X')
				return Joker;
			else
				continue;
		}
		for (int i = 0; i < 5; i++) {
			for (int j = i + 1; j < 5; j++) {
				if (card[i] == card[j])
					same++;
				else
					continue;
			}
		}
		if (same == 6)
			return Fourofakind;
		else if (same == 4)
			return FullHouse;
		else if (same == 3)
			return Threeofakind;
		else if (same == 2)
			return Twopairs;
		else
			return Nothing;
	}

	public int[] array() {  //回傳牌的陣列
		return cards;
	}

	public int[] array2() {
		return cards2;
	}

	public String toString(int type) {  //回傳牌型文字
		if (type == 5)
			return "Joker";
		else if (type == 4)
			return "Four of a kind";
		else if (type == 3)
			return "Full House";
		else if (type == 2)
			return "Three of a kind";
		else if (type == 1)
			return "Two pairs";
		else
			return "Nothing";
	}

	public String WinOrLose(int c1, int c2) {  //回傳輸贏文字
		if (c1 > c2)
			return "User Lose";
		else if (c1 < c2)
			return "User Win";
		else
			return "Draw";
	}

	public int number(int a) {  //回傳某點數以產生相對應圖片
		return cards[a];
	}

	public int number2(int a) {
		return cards2[a];
	}
}
