package a7.s100502513;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.LineBorder;

public class FrameWork extends JFrame implements ActionListener {
	private JButton button = new JButton("Random");  //產生BUTTON
	private ImageIcon poker1 = new ImageIcon("image/card/init_1.png");  //初始圖片
	private ImageIcon poker2 = new ImageIcon("image/card/init_2.png");
	private ImageIcon poker3 = new ImageIcon("image/card/init_3.png");
	private ImageIcon poker4 = new ImageIcon("image/card/init_4.png");
	private ImageIcon poker5 = new ImageIcon("image/card/init_5.png");
	private ImageIcon poker6 = new ImageIcon("image/card/init_1.png");
	private ImageIcon poker7 = new ImageIcon("image/card/init_2.png");
	private ImageIcon poker8 = new ImageIcon("image/card/init_3.png");
	private ImageIcon poker9 = new ImageIcon("image/card/init_4.png");
	private ImageIcon poker10 = new ImageIcon("image/card/init_5.png");
	private JLabel im1, im2, im3, im4, im5, im6, im7, im8, im9, im10;  //產生標籤(撲克牌圖片)
	private JLabel type1, type2;  //產生標籤(牌的類型)
	private JLabel WoL;  //產生標籤(輸贏)
	private String st1, st2;  //類型的文字
	private String stWoL;  //輸贏的文字

	public FrameWork() {
		button.setFont(new Font("Serif", Font.BOLD, 150));
		button.addActionListener(this);  //讓按鈕產生作用
		
		JPanel p1 = new JPanel();  //撲克牌部分
		p1.setLayout(new GridLayout(2, 6, 5, 10));
		JLabel com = new JLabel("Computer");
		com.setFont(new Font("Serif", Font.BOLD, 25));
		p1.add(com);
		im1 = new JLabel(poker1);  //把圖片貼上標籤
		im1.setBorder(new LineBorder(Color.DARK_GRAY, 5));
		p1.add(im1);
		im2 = new JLabel(poker2);
		im2.setBorder(new LineBorder(Color.DARK_GRAY, 5));
		p1.add(im2);
		im3 = new JLabel(poker3);
		im3.setBorder(new LineBorder(Color.DARK_GRAY, 5));
		p1.add(im3);
		im4 = new JLabel(poker4);
		im4.setBorder(new LineBorder(Color.DARK_GRAY, 5));
		p1.add(im4);
		im5 = new JLabel(poker5);
		im5.setBorder(new LineBorder(Color.DARK_GRAY, 5));
		p1.add(im5);
		JLabel user = new JLabel("User");
		user.setFont(new Font("Serif", Font.BOLD, 25));
		p1.add(user);
		im6 = new JLabel(poker6);
		im6.setBorder(new LineBorder(Color.DARK_GRAY, 5));
		p1.add(im6);
		im7 = new JLabel(poker7);
		im7.setBorder(new LineBorder(Color.DARK_GRAY, 5));
		p1.add(im7);
		im8 = new JLabel(poker8);
		im8.setBorder(new LineBorder(Color.DARK_GRAY, 5));
		p1.add(im8);
		im9 = new JLabel(poker9);
		im9.setBorder(new LineBorder(Color.DARK_GRAY, 5));
		p1.add(im9);
		im10 = new JLabel(poker10);
		im10.setBorder(new LineBorder(Color.DARK_GRAY, 5));
		p1.add(im10);

		JPanel p2 = new JPanel();  //撲克牌加按鈕部分
		p2.setLayout(new BorderLayout());
		p2.add(p1, BorderLayout.CENTER);
		p2.add(button, BorderLayout.SOUTH);

		JPanel p3 = new JPanel();  //右邊文字部分
		p3.setLayout(new GridLayout(3, 1));
		type1 = new JLabel(st1);  //把文字貼上標籤
		type1.setFont(new Font("Serif", Font.BOLD, 40));
		type2 = new JLabel(st2);
		type2.setFont(new Font("Serif", Font.BOLD, 40));
		WoL = new JLabel(stWoL);
		WoL.setFont(new Font("Serif", Font.BOLD, 100));
		p3.add(type1);
		p3.add(type2);
		p3.add(WoL);

		add(p2, BorderLayout.WEST);
		add(p3, BorderLayout.CENTER);

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == button) {
			User card = new User();  //產生新物件並產生亂數
			poker1 = new ImageIcon("image/card/" + card.number(0) + ".png");  //修改圖片
			poker2 = new ImageIcon("image/card/" + card.number(1) + ".png");
			poker3 = new ImageIcon("image/card/" + card.number(2) + ".png");
			poker4 = new ImageIcon("image/card/" + card.number(3) + ".png");
			poker5 = new ImageIcon("image/card/" + card.number(4) + ".png");
			poker6 = new ImageIcon("image/card/" + card.number2(0) + ".png");
			poker7 = new ImageIcon("image/card/" + card.number2(1) + ".png");
			poker8 = new ImageIcon("image/card/" + card.number2(2) + ".png");
			poker9 = new ImageIcon("image/card/" + card.number2(3) + ".png");
			poker10 = new ImageIcon("image/card/" + card.number2(4) + ".png");
			im1.setIcon(poker1);
			im2.setIcon(poker2);
			im3.setIcon(poker3);
			im4.setIcon(poker4);
			im5.setIcon(poker5);
			im6.setIcon(poker6);
			im7.setIcon(poker7);
			im8.setIcon(poker8);
			im9.setIcon(poker9);
			im10.setIcon(poker10);
			if (card.check(card.point(card.array())) == 5 || card.check(card.point(card.array2())) == 5) {  //判斷牌的類型並修改文字
				st1 = card.toString(card.check(card.point(card.array())));
				st2 = card.toString(card.check(card.point(card.array2())));
			} 
			else if (card.check(card.point(card.array())) == 4 || card.check(card.point(card.array2())) == 4) {
				st1 = card.toString(card.check(card.point(card.array())));
				st2 = card.toString(card.check(card.point(card.array2())));
			} 
			else if (card.check(card.point(card.array())) == 3 || card.check(card.point(card.array())) == 3) {
				st1 = card.toString(card.check(card.point(card.array())));
				st2 = card.toString(card.check(card.point(card.array2())));
			} 
			else if (card.check(card.point(card.array())) == 2 || card.check(card.point(card.array2())) == 2) {
				st1 = card.toString(card.check(card.point(card.array())));
				st2 = card.toString(card.check(card.point(card.array2())));
			} 
			else if (card.check(card.point(card.array())) == 1 || card.check(card.point(card.array2())) == 1) {
				st1 = card.toString(card.check(card.point(card.array())));
				st2 = card.toString(card.check(card.point(card.array2())));
			} 
			else if (card.check(card.point(card.array())) == 0 || card.check(card.point(card.array2())) == 0) {
				st1 = card.toString(card.check(card.point(card.array())));
				st2 = card.toString(card.check(card.point(card.array2())));
			}
			type1.setText(st1);
			type2.setText(st2);
			stWoL = card.WinOrLose(card.check(card.point(card.array())),card.check(card.point(card.array2())));  //判斷輸贏
			WoL.setText(stWoL);
		}
	}

}
