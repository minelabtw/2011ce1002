package a7.s100502519;

public class Password {

	private String pws = new String();

	public Password(String input_pw) {
		pws = input_pw;			//set
		
		try{
			tooshort();			//call method 判斷長度
			incorrect();			//call method 判斷是否只有字母或數字
			System.out.println("The password" + pws + "is valid!!\n");			//沒有以上兩者就是正確了
		}
		catch(InvalidPasswordException ex){
			System.out.println(ex.getMessage());			//get throw回來的訊息
			System.out.println("The password abc is not correct!!\n");
		}
	}
	
	public void tooshort() throws InvalidPasswordException{
		if(pws.length()<7){			/*長度小於7 錯誤*/
			throw new InvalidPasswordException("The length of the password " + pws + " is not enough!!");
		}
	}
	
	public void incorrect() throws InvalidPasswordException{
		boolean letter = false;
		boolean digit = false;
		
		for(int i=0; i<pws.length();i++){
			if(Character.isLetter(pws.charAt(i))){
				 letter = true;			//有字母
				 break;
			}
		}
		
		for(int i=0; i<pws.length();i++){
			if(Character.isDigit(pws.charAt(i))){
				 digit = true;			//有數字
				 break;
			}
		}
		
		if(letter == true && digit == false){			/*只有字母 沒有數字*/
			throw new InvalidPasswordException("The password " + pws + " has no number!!");
		}
		
		if(letter == false && digit == true){			/*只有數字沒有字母*/
			throw new InvalidPasswordException("The password " + pws + " has no letter!!");
		}
	}
}
