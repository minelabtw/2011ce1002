package a7.s100502519;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.border.*;

public class FrameWork extends JFrame implements ActionListener{
	
	User pic_library = new User();			//User object
	
	Font font1 = new Font("Serif",Font.BOLD,12);			//字型、大小
	Border lineBorder = new LineBorder(Color.PINK,2);			//邊線
	
	JButton button_random = new JButton("random");			//button
	JLabel label_he = new JLabel("He");			//label
	JLabel label_you = new JLabel("You");
	JLabel label_joker1 = new JLabel("Joker");
	JLabel label_four1 = new JLabel("Four of a kind");
	JLabel label_fullHouse1 = new JLabel("Full House");
	JLabel label_three1 = new JLabel("Three of kind");
	JLabel label_two1 = new JLabel("Two pairs");
	JLabel label_nothing1 = new JLabel("Nothing");
	JLabel label_joker2 = new JLabel("Joker");
	JLabel label_four2 = new JLabel("Four of a kind");
	JLabel label_fullHouse2 = new JLabel("Full House");
	JLabel label_three2 = new JLabel("Three of kind");
	JLabel label_two2 = new JLabel("Two pairs");
	JLabel label_nothing2 = new JLabel("Nothing");
	JLabel label_win = new JLabel("You Win");
	JLabel label_lose = new JLabel("You Lose");
	JLabel label_draw = new JLabel("Draw");
	JPanel up = new JPanel(new BorderLayout());			//panel
	JPanel down = new JPanel(new BorderLayout());
	JPanel cards_ini = new JPanel();
	JPanel Cards = new JPanel();
	JPanel HeYou = new JPanel();
	JPanel kind = new JPanel();
	JPanel result = new JPanel();
	JPanel panel_random = new JPanel();
	
	
	public FrameWork (){
		
		button_random.addActionListener(this);

		cards_ini.setLayout(new GridLayout(1,5));			//歡迎畫面
		cards_ini.add(new JLabel(pic_library.imageInit1));
		cards_ini.add(new JLabel(pic_library.imageInit2));
		cards_ini.add(new JLabel(pic_library.imageInit3));
		cards_ini.add(new JLabel(pic_library.imageInit4));
		cards_ini.add(new JLabel(pic_library.imageInit5));
		
		cards_ini.setBorder(lineBorder);			//設定邊界
		cards_ini.setBorder(new TitledBorder("welcome !"));			//邊界標頭
		button_random.setFont(font1);			//字型 大小
		button_random.setBackground(Color.YELLOW);			//底色
		button_random.setForeground(Color.BLACK);			//字色
		
		panel_random.add(button_random);			//鑲入
		up.add(cards_ini,BorderLayout.CENTER);
		down.add(panel_random,BorderLayout.WEST);
		add(up,BorderLayout.NORTH);
		add(down,BorderLayout.SOUTH);

	}


	
	
	public void actionPerformed(ActionEvent e){
		if(e.getSource() == button_random){			/*若按了random button*/
			cards_ini.removeAll();			//remove掉一些東西
			Cards.removeAll();
			kind.removeAll();
			result.removeAll();
			up.removeAll();
			down.removeAll();
			
			setVisible(true);			//把一些東西set visible
			up.setVisible(true);
			down.setVisible(true);
			Cards.setVisible(true);
			kind.setVisible(true);
			result.setVisible(true);
			
			int i=0;
			int temp;
			int [] series = new int [10];
			boolean find = true;
			
			while(i<10){			/*製造10個不重複且0~20的亂數*/
				find = true;
				
				temp = (int)(Math.random()*21);
				
				for(int j=0;j<=i;j++){
					if(series[j] == temp){
						find = false;
						break;	
					}
				}
				
				if(find == true){
					series[i] = temp;
					i++;
				}
			}

			Cards.setBorder(lineBorder);			//邊界
			Cards.setBorder(new TitledBorder("Cards Block"));			//邊界標頭
			Cards.setLayout(new GridLayout(2,5));			//設定layout型式  卡片區
			Cards.add(new JLabel(pic_library.image[series[0]]));			//鑲入
			Cards.add(new JLabel(pic_library.image[series[1]]));
			Cards.add(new JLabel(pic_library.image[series[2]]));
			Cards.add(new JLabel(pic_library.image[series[3]]));
			Cards.add(new JLabel(pic_library.image[series[4]]));
			Cards.add(new JLabel(pic_library.image[series[5]]));
			Cards.add(new JLabel(pic_library.image[series[6]]));
			Cards.add(new JLabel(pic_library.image[series[7]]));
			Cards.add(new JLabel(pic_library.image[series[8]]));
			Cards.add(new JLabel(pic_library.image[series[9]]));

			HeYou.setLayout(new GridLayout(2,1));			//設定layout型式  所有者區
			HeYou.add(label_he);			//鑲入
			HeYou.add(label_you);
			
			int He_one = 0;
			int He_two = 0;
			int He_J = 0;
			int He_Q = 0;
			int He_K = 0;
			int He_Joker = 0;
			int You_one = 0;
			int You_two = 0;
			int You_J = 0;
			int You_Q = 0;
			int You_K = 0;
			int You_Joker = 0;
			int He_point = 0;
			int You_point = 0;
			
			for(int j=0 ; j<=4 ; j++){			/*電腦排色統計*/
				switch(series[j]){
					case 0: case 1: case 2: case 3:
						He_one++;
						break;
					case 4: case 5: case 6: case 7:
						He_two++;
						break;
					case 8: case 9: case 10: case 11:
						He_J++;
						break;
					case 12: case 13: case 14: case 15:
						He_Q++;
						break;
					case 16: case 17: case 18: case 19:
						He_K++;
						break;
					case 20:
						He_Joker++;
						break;
				}
				
				switch(series[j+5]){			/*玩家排色統計*/
					case 0: case 1: case 2: case 3:
						You_one++;
						break;
					case 4: case 5: case 6: case 7:
						You_two++;
						break;
					case 8: case 9: case 10: case 11:
						You_J++;
						break;
					case 12: case 13: case 14: case 15:
						You_Q++;
						break;
					case 16: case 17: case 18: case 19:
						You_K++;
						break;
					case 20:
						You_Joker++;
						break;
				}
			}
			
			kind.setBorder(lineBorder);			//邊界
			kind.setBorder(new TitledBorder("kind"));			//邊界標頭
			kind.setLayout(new GridLayout(2,1));			//設定layout型式  牌型區

			if(He_Joker == 1){			/*判斷電腦的牌型及分數 分數6~1 分別表示 "鬼牌" "鐵支" "葫蘆" "三支" "兩支" "無"*/
				kind.add(label_joker1);
				He_point = 6;
			}
			else if(He_one == 4 || He_two == 4 || He_J == 4 || He_Q == 4 || He_K == 4){
				kind.add(label_four1);
				He_point = 5;
			}
			else if(He_one == 3 || He_two == 3 || He_J == 3 || He_Q == 3 || He_K == 3){
				if(He_one == 2 || He_two == 2 || He_J == 2 || He_Q == 2 || He_K == 2){
					kind.add(label_fullHouse1);
					He_point = 4;
				}
				else{
					kind.add(label_three1);
					He_point = 3;
				}
			}
			else if(He_one == 2 || He_two == 2 || He_J == 2 || He_Q == 2 || He_K == 2){
				kind.add(label_two1);
				He_point = 2;
			}

			else if(He_one == 1 && He_two == 1 && He_J == 1 && He_Q == 1 && He_K == 1){
				kind.add(label_nothing1);
				He_point = 1;
			}
			
			if(You_Joker == 1){			/*判斷玩家的牌型及分數 分數6~1 分別表示 "鬼牌" "鐵支" "葫蘆" "三支" "兩支" "無"*/
				kind.add(label_joker2);
				You_point = 6;
			}

			else if(You_one == 4 || You_two == 4 || You_J == 4 || You_Q == 4 || You_K == 4){
				kind.add(label_four2);
				You_point = 5;
			}
			else if(You_one == 3 || You_two == 3 || You_J == 3 || You_Q == 3 || You_K == 3){
				if(You_one == 2 ||You_two == 2 || You_J == 2 || You_Q == 2 || You_K == 2){
					kind.add(label_fullHouse2);
					You_point = 4;
				}
				else{
					kind.add(label_three2);
					You_point = 3;
				}
			}
			else if(You_one == 2 || You_two == 2 || You_J == 2 || You_Q == 2 || You_K == 2){
				kind.add(label_two2);
				You_point = 2;
			}
			else if(You_one == 1 && You_two == 1 && You_J == 1 && You_Q == 1 && You_K == 1){
				kind.add(label_nothing2);
				You_point = 1;
			}
			
			if(He_point > You_point){			/*比較得分 來判斷勝負*/
				label_lose.setForeground(Color.BLUE);
				result.add(label_lose);
			}
			else if(He_point < You_point){
				label_win.setForeground(Color.RED);
				result.add(label_win);
			}
			else{
				label_draw.setForeground(Color.DARK_GRAY);
				result.add(label_draw);
			}

			panel_random.add(button_random);			//鑲入
			down.add(panel_random,BorderLayout.WEST);
			up.add(Cards,BorderLayout.CENTER);
			up.add(HeYou,BorderLayout.WEST);
			up.add(kind,BorderLayout.EAST);
			down.add(result,BorderLayout.EAST);
			
			setVisible(true);			//把一些東西set visible
			up.setVisible(true);
			down.setVisible(true);
			Cards.setVisible(true);
			kind.setVisible(true);
			result.setVisible(true);
		}
	}
}
