package a7.s100502519;

import javax.swing.JFrame;

public class A71 {

	public static void main(String [] args){
		FrameWork f = new FrameWork();			//建一個自訂的frame
		f.setTitle("Asignment #7");			//標頭
		f.setSize(600, 300);			//長寬
		f.setLocationRelativeTo(null);			//位置 中央
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);			//叉叉
		f.setVisible(true);			//visible
	}
	
}
