package a7.s100502506;

import javax.swing.JFrame;

public class A71 
{
	public static void main(String [] args)
	{		
		FrameWork f = new FrameWork();						//new Framework
		f.setTitle("Asignment #7");							//set title
		f.setSize(1500, 550);								//set size
		f.setLocation(0,0);									//set location
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		f.setVisible(true);									//set visible
	}
}