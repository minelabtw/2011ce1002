package a7.s100502506;

import javax.swing.JOptionPane;

public class InvalidPasswordException extends Exception
{
	public InvalidPasswordException(int inputnum,String input) 
	{
		String message1 = new String();
		if(inputnum==1)												// too short
		{
			message1=String.format("The length of the password %s is not enough!!\n", input);
		}
		else if(inputnum==2)											//no letter
		{
			message1=String.format("The password %s has no letter!!\n", input);
		}
		else if(inputnum==3)											//no number
		{
			message1=String.format("The password %s has no number!!\n", input);
		}
		else															//has symbol
		{
			message1=String.format("The password %s  has symbols!!\n", input);
		}
		JOptionPane.showMessageDialog(null, message1);
	}
}
