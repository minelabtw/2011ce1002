package a7.s100502506;

import javax.swing.JOptionPane;

public class Password 
{
	private String password=new String();
	public Password(String input)
	{	
		
		int typeError=0;
		boolean flag1 =false;										//check if has letter
		boolean flag2 =false;										//check if has symbol
		int counter1 =0;											//check number of character
		int counter2 =0;											//check number of letter
		try 
		{
			for(int i=0;i<input.length();i++)
			{
				counter1++;
				if(Character.isLetter(input.charAt(i)))
				{
					flag1=true;										//letter > true
					counter2++;										//letter ++
				}
				if(  !Character.isLetter(input.charAt(i) )   &&  (input.charAt(i)>='9')||(input.charAt(i)<='0'))
				{
					flag2=true;
				}
			}
			
			
			if(counter1<7)											// too short
			{
				typeError=1;
				
				throw new InvalidPasswordException(1,input);
			}
			else if(flag1!=true)													//no letter
			{
				typeError=2;
				
				throw new InvalidPasswordException(2,input);
			}
			else if(counter2==input.length())									//no number
			{
				typeError=3;
				
				throw new InvalidPasswordException(3,input);
			}
			else if(flag2==true)
			{
				typeError=4;
				throw new InvalidPasswordException(4,input);
			}
			else 
			{
																		//if not throw
				String message2=new String();
				message2=String.format("The password %s is valid!!\n", password, counter2);
				JOptionPane.showMessageDialog(null,message2 );
				password=input;
			}
			
		} 
		catch (InvalidPasswordException ev) 							//after InvalidPasswordException
		{
			String message1;
			message1=String.format("The password %s in not correct!!",input);
			JOptionPane.showMessageDialog(null, message1);
		}
															
	}
}
