//class User:
//Some data members to store the cards
//Some data members to store the type of cards. 
//There are six types: Joker, Four of a kind, Full House, Three of kind, Two pairs, Nothing.
//Their priority is listed from highest to lowest.
//You should generated cards randomly.
package a7.s100502506;

public class User 											//create two sets of cards 
{
	private String[] cards=new String[21];
	private String[] havecards1=new String[5];
	private String[] havecards2=new String[5];
	private String[] randomcard=new String[10];
	private int[] randomnum=new int[10];
	private int type1;
	private int type2;
	public User()
	{
		//total cards
		for(int i=0;i<4;i++)
			cards[i]=String.format("%d%d",1,i+1);
		for(int i=4;i<8;i++)
			cards[i]=String.format("%d%d",2,i-3);
		for(int i=8;i<12;i++)
			cards[i]=String.format("J%d",i-7);
		for(int i=12;i<16;i++)
			cards[i]=String.format("Q%d",i-12);
		for(int i=16;i<19;i++)
			cards[i]=String.format("K%d",i-15);
		cards[20]="joker";
		
		//random cards
		for(int i=0;i<10;i++)
		{
			do 
			{
				randomnum[i]=(int)((Math.random()*100)%21);
			}while (checksame(cards[randomnum[i]]));
			
			randomcard[i]=cards[randomnum[i]];
		}
		//send random cards to player1 cards
		for(int i=0;i<5;i++)
		{
			havecards1[i]=randomcard[i];
		}
		setType1(Type(havecards1));//set play1 cards type
		
		//send random cards to player2 cards
		for(int i=0;i<5;i++)
		{
			havecards2[i]=randomcard[i+5];
		}
		setType2(Type(havecards2));//set play2 cards type
	}
	public int[] getrandom()				//get all random numbers
	{
		return randomnum;
	}
	public int getType1()					//get player1 type
	{
		return type1;
	}
	public int getType2()					//get player2 type
	{
		return type2;
	}
	public void setType1(int input)			//set player1 type
	{
		type1=input;
	}
	public void setType2(int input)			//set player2 type
	{
		type2=input;
	}
	public String[] getHandcard1()			//get player1 cards
	{
		return havecards1;
	}
	public String[] getHandcard2()			//get player2 cards
	{
		return havecards2;
	}
	
	public boolean checksame(String input)	//check if the same cards
	{
		boolean flag =false;
		
		for(int i=0;i<10;i++)
		{
			if(input==randomcard[i])
			{
				flag=true;
			}
		}
		return flag;
	}
	
	public int Type(String[] input)				//Type set system
	{
		int this_type=0;
		int[] counter=new int[5];
		for(int i=0;i<5;i++)
		{
			
			if(input[i].charAt(0)=='1')
			{
				counter[0]++;
			}
			else if(input[i].charAt(0)=='2')
			{
				counter[1]++;
			}
			else if(input[i].charAt(0)=='J')
			{
				counter[2]++;
			}
			else if(input[i].charAt(0)=='Q')
			{
				counter[3]++;
			}
			else if(input[i].charAt(0)=='K')
			{
				counter[4]++;
			}
			else												//joker
			{
				this_type = 5;
				break;
			}
		}
		if(this_type!=5)
		{
			if(checkfourofkind(counter))								
				this_type=4;//Four of a kind
			else if(checkfullhouse(counter))
				this_type=3;//Full House
			else if(checkthreeofkind(counter))
				this_type=2;//Three of kind
			else if(checktwopairs(counter))
				this_type=1;//Two pairs
			else 
				this_type=0;//Nothing.
		}
		return this_type;
	}
	
	
	public boolean checkfourofkind(int[] input)				//check if four of kind
	{
		boolean flag=false;
		for(int i=0;i<5;i++)
		{
			if(input[i]==4)
			{
				flag=true;
			}
		}
		return flag;
	}
	public boolean checkfullhouse(int[] input)				//check if full house
	{
		boolean flag=false;
		for(int i=0;i<5;i++)
		{
			if(input[i]==3)
			{
				for(int j=0;j<5;j++)
				{
					if(input[j]==2)
						flag=true;
				}
			}
		}
		return flag;
	}
	public boolean checkthreeofkind(int[] input)				//check three of kind
	{
		boolean flag=false;
		for(int i=0;i<5;i++)
		{
			if(input[i]==3)
			{
				
				for(int j=0;j<5;j++)
				{
					if(input[j]==1)
					{
						for(int h=0;h<5;h++)
						{
							if(input[h]==1)
							{
								flag=true;
							}
						}
					}
				}
				
			}
		}
		return flag;
	}
	public boolean checktwopairs(int[] input)					//check if two pairs
	{
		boolean flag=false;
		for(int i=0;i<5;i++)
		{
			if(input[i]==2)
			{
				for(int j=i+1;j<5;j++)
				{
					if(input[j]==2)
					{
						flag=true;
					}
				}
			}
		}
		return flag;
	}
	

}


//fk