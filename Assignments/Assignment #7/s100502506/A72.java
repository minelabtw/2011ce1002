//Write a Password class that stores a password String. Password must contain at least seven alpha-numeric characters,
//with at least one letter and one number. Also write an InvalidPasswordException class that extends Exception.
//The Password constructor should throw an InvalidPasswordException,
//which should report an appropriate message based on a given String 
//(e.g., if the String contains no numbers, the exception should report this fact). 
//If there is more than one error, the exception need only report one.
package a7.s100502506;


import java.util.Scanner;

import javax.swing.JOptionPane;

public class A72 
{

	public static void main(String argc[])
	{
		Scanner inputScanner =new Scanner(System.in);
	
		while(true)																	//if continue
		{
			String message=JOptionPane.showInputDialog("Please input a password to varify: ");
			Password myPassword =new Password(message);						//input password
		
		}
	}
}
