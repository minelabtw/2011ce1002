//class FrameWork:
//Design GUI here.
//You have to use the classes such as: GridLayout, Font, Boder, JLabel, JButton, ImageIcon, BorderLayout
//You have to show the layout such as(at least): ��random button��, ��the 2 sets of cards��, ��the type of cards��, ��game result��.
//Please unzip the image.zip into the project directory and the path of the cards are ��image/card/xxxx.png��
//Here is the sample layout:
package a7.s100502506;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

public class FrameWork extends JFrame implements ActionListener
{
	private JButton button = new JButton("Random"); //create button 
	private JButton[] buttons;
	private JLabel[] labels;
	private GridLayout mygridlayout=new GridLayout(1,7,5,5);
	private Font font1 =new Font("SansSerif",Font.BOLD,50);
	private Font font2 =new Font("Serif",Font.BOLD,50);
	private Border myborder =new LineBorder(Color.black,2);
	private ImageIcon[] Icon;
	private JPanel panel1 =new JPanel(new BorderLayout());
	private JPanel panel2 =new JPanel(new BorderLayout());
	private JPanel panel3 =new JPanel(new BorderLayout());
	private JPanel panel4 =new JPanel(new BorderLayout());
	private JPanel panel5 =new JPanel(new BorderLayout());
	private JFrame frame =new JFrame();
	private int user1_type;
	private int user2_type;
	private String[] user1_handcard=new String[5];
	private String[] user2_handcard=new String[5];
	
	public FrameWork ()
	{
		
		super("Poker Game");
		buttons=new JButton[10];
		Icon=new ImageIcon[26];
		labels=new JLabel[5];
		for(int i=0;i<5;i++)
			labels[i]=new JLabel();
		for(int i=0;i<26;i++)
			Icon[i]=new ImageIcon();
		
		//set ICON
		//A
		Icon[0]=new ImageIcon("images/card/0.png");
		Icon[1]=new ImageIcon("images/card/1.png");
		Icon[2]=new ImageIcon("images/card/2.png");
		Icon[3]=new ImageIcon("images/card/3.png");
		//2
		Icon[4]=new ImageIcon("images/card/4.png");
		Icon[5]=new ImageIcon("images/card/5.png");
		Icon[6]=new ImageIcon("images/card/6.png");
		Icon[7]=new ImageIcon("images/card/7.png");
		//J
		Icon[8]=new ImageIcon("images/card/8.png");
		Icon[9]=new ImageIcon("images/card/9.png");
		Icon[10]=new ImageIcon("images/card/10.png");
		Icon[11]=new ImageIcon("images/card/11.png");
		//Q
		Icon[12]=new ImageIcon("images/card/a.png");
		Icon[13]=new ImageIcon("images/card/13.png");
		Icon[14]=new ImageIcon("images/card/14.png");
		Icon[15]=new ImageIcon("images/card/15.png");
		//K
		Icon[16]=new ImageIcon("images/card/16.png");
		Icon[17]=new ImageIcon("images/card/17.png");
		Icon[18]=new ImageIcon("images/card/18.png");
		Icon[19]=new ImageIcon("images/card/19.png");
		//joker
		Icon[20]=new ImageIcon("images/card/12.png");
		
		Icon[21]=new ImageIcon("images/card/init_1.png");	
		Icon[22]=new ImageIcon("images/card/init_2.png");	
		Icon[23]=new ImageIcon("images/card/init_3.png");	
		Icon[24]=new ImageIcon("images/card/init_4.png");	
		Icon[25]=new ImageIcon("images/card/init_5.png");	
		
		//set icon to buttons
		for(int i=0;i<10;i++)
		{	
			if(i<5)
				buttons[i]=new JButton(Icon[21+i]);
			if(i>4)
				buttons[i]=new JButton(Icon[16+i]);
		}
		
		Color colorbg =new Color(238,238,238);
		Border lineBorder = new LineBorder(Color.BLACK,3);
		//set button background and border
		for(int i=0;i<10;i++)
		{
			buttons[i].setBackground(colorbg);
			buttons[i].setBorder(lineBorder);
		}
		//set labels
		labels[0].setText("He");
		labels[0].setFont(font1);
		labels[1].setText("You");
		labels[1].setFont(font1);
		//panel1//
		panel1.setLayout(mygridlayout);
		panel1.add(labels[0]);
		for(int i=0;i<5;i++)
			panel1.add(buttons[i]);
		//panek2//
		panel2.setLayout(mygridlayout);
		panel2.add(labels[1]);
		for(int i=0;i<5;i++)
			panel2.add(buttons[i+5]);
		//panel3//
		panel3.setLayout(new GridLayout(1,2,5,5));
		button.setFont(font2);
		panel3.add(button);
		//panel5//
		labels[4].setFont(font2);
		labels[4].setText("Enter Radom �@");
		panel5.setLayout(new GridLayout(3,1,5,5));
		labels[2].setFont(font2);
		labels[3].setFont(font2);
		panel5.add(labels[2]);
		panel5.add(labels[3]);
		panel5.add(labels[4]);
		//panel4 has panel1 panel2 panel3//
		panel4.setLayout(new GridLayout(3,1,5,5));
		panel4.add(panel1);
		panel4.add(panel2);
		panel4.add(panel3);
		
		//add p4 and p5 to Jframe
		add(panel4,BorderLayout.CENTER);
		add(panel5,BorderLayout.EAST);
		
		button.addActionListener(this); //active button event here
			
	}
	
	public void actionPerformed(ActionEvent e)
	{
		//when push button
		if(e.getSource() == button)
		{
			User Users =new User();	
			user1_type=Users.getType1();
			user2_type=Users.getType2();
			user1_handcard=Users.getHandcard1();
			user2_handcard=Users.getHandcard2();
			int[] random =new int[10];
			random=Users.getrandom();
			//when push button set the button again
			for(int i=0;i<10;i++)
				buttons[i].setIcon(Icon[random[i]]);
			
			//when push button set the label again
			setTypeText(user1_type, labels[2]);
			setTypeText(user2_type, labels[3]);
			
			//set the label to show the result
			if(user1_type>user2_type)
				labels[4].setText("You Lost");
			else if(user1_type<user2_type)
				labels[4].setText("You Win          ");
			else 
				labels[4].setText("Draw             ");
			
		}
	}
	public void setTypeText(int type,JLabel input)
	{
		if(type==5)
		{
			input.setText("Joker             ");
		}
		else if(type==4)
		{
			input.setText("Four of kind ");
		}
		else if(type==3)
		{ 
			input.setText("Full House        ");
		}
		else if(type==2)
		{
			input.setText("Three of kind");
		}
		else if(type==1)
		{
			input.setText("Two Pairs          ");
		}
		else 
		{
			input.setText("Nothing            ");
		} 
	}
}
			
