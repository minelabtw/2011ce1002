package a7.s100502502;
import java.util.Random;
import javax.swing.*;
public class User {
	//create ImageIcon object and set image
	private ImageIcon O1 = new ImageIcon("image/card/0.png");
	private ImageIcon O2 = new ImageIcon("image/card/1.png");
	private ImageIcon O3 = new ImageIcon("image/card/2.png");
	private ImageIcon O4 = new ImageIcon("image/card/3.png");
	private ImageIcon T1 = new ImageIcon("image/card/4.png");
	private ImageIcon T2 = new ImageIcon("image/card/5.png");
	private ImageIcon T3 = new ImageIcon("image/card/6.png");
	private ImageIcon T4 = new ImageIcon("image/card/7.png");
	private ImageIcon J1 = new ImageIcon("image/card/8.png");
	private ImageIcon J2 = new ImageIcon("image/card/9.png");
	private ImageIcon J3 = new ImageIcon("image/card/10.png");
	private ImageIcon J4 = new ImageIcon("image/card/11.png");
	private ImageIcon Q1 = new ImageIcon("image/card/12.png");
	private ImageIcon Q2 = new ImageIcon("image/card/13.png");
	private ImageIcon Q3 = new ImageIcon("image/card/14.png");
	private ImageIcon Q4 = new ImageIcon("image/card/15.png");
	private ImageIcon K1 = new ImageIcon("image/card/16.png");
	private ImageIcon K2 = new ImageIcon("image/card/17.png");
	private ImageIcon K3 = new ImageIcon("image/card/18.png");
	private ImageIcon K4 = new ImageIcon("image/card/19.png");
	private ImageIcon G1 = new ImageIcon("image/card/20.png");
	protected ImageIcon[] Card = {O1, O2, O3, O4, T1, T2, T3, T4, J1, J2, J3, J4, Q1, Q2, Q3, Q4, K1, K2, K3, K4, G1};
	protected ImageIcon[] Player;//to save player's cards
	protected int[] kind = new int[6];//save the number of the six kinds of card(A, 2, J, K, Q, G)
	protected String Level = "";//save player is level
	boolean FHouse = false;
	boolean TPairs = false;
	public User(){//constructor
		Player = new ImageIcon[5];//initialize array
	}
	public void Random(int now){//choose card randomly
		for(int i = 0; i < 5; i++, now--){
			Random R = new Random();
			int RNum = R.nextInt(now-1) + 1;//random from 1 to now
			for(int j = 0, counter = 0; j < 21; j++){
				if(Card[j] != null){
					counter = counter + 1;
					if(counter == RNum){//if find the card
						Player[i] = Card[j];//save the card to player array
						//determine what kind of the card and calculate the number
						if(j == 0 || j == 1 || j == 2 || j == 3)
							kind[0]++;
						else if(j == 4 || j == 5 || j == 6 || j == 7)
							kind[1]++;
						else if(j == 8 || j == 9 || j == 10 || j == 11)
							kind[2]++;
						else if(j == 12 || j == 13 || j == 14 || j == 15)
							kind[3]++;
						else if(j == 16 || j == 17 || j == 18 || j == 19)
							kind[4]++;
						else if(j == 20)
							kind[5]++;
						else{}
						Card[j] = null;//set card to null
						break;
					}
					else{}
				}
				else{}
			}
		}
	}
	public int Rank(){//determine player's rank
		//check whether the player's card is Full House
		for(int i = 0; i < 4; i++){
			for(int j = 1; j <= 4; j++){
				if(kind[i] == 2 && kind[j] == 3){
					FHouse = true;
					i = 4;//if FHouse = true, end the for loop
					break;
				}
				else if(kind[i] == 3 && kind[j] == 2){
					FHouse = true;//if FHouse = true, end the for loop
					i = 4;
					break;
				}
				else{}
			}
		}
		//check whether the player's card is Two Pairs
		for(int i = 0; i < 4; i++){
			for(int j = 1; j <= 4; j++){
				if(kind[i] == 2 && kind[j] == 2){
					TPairs = true;
					i = 4;//if TPairs = true, end the for loop
					break;
				}
				else{}
			}
		}
		if(kind[5] == 1){//if player has Joker card
			Level = "<Joker>";
			return 6;
		}
		//if player is Four of a kind
		else if(kind[0] == 4 || kind[1] == 4 || kind[2] == 4 || kind[3] == 4 || kind[4] == 4){
			Level = "<Four of a kind>";
			return 5;
		}
		else if(FHouse == true){//if player is Full House
			Level = "<Full House>";
			return 4;
		}
		//if player is Three of kind
		else if(kind[0] == 3 || kind[1] == 3 || kind[2] == 3 || kind[3] == 3 || kind[4] == 3){
			Level = "<Three of kind>";
			return 3;
		}
		else if(TPairs == true){//if player is Two pairs
			Level = "<Two pairs>";
			return 2;
		}
		else//if player is nothing
			Level = "<Nothing>";
			return 1;
	}
}
