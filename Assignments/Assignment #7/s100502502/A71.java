//A71.java
package a7.s100502502;
import javax.swing.JFrame;
public class A71 {
	public static void main(String[] args){
		FrameWork f = new FrameWork();//create a framework object
		f.setTitle("Asignment #7");//set frame title
		f.setSize(1500, 550);//set frame size
		f.setLocationRelativeTo(null);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
}
