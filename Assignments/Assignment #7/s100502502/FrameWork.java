//FrameWork.java
package a7.s100502502;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
public class FrameWork extends JFrame implements ActionListener{
	JButton button = new JButton("random"); //create button
	Border lineBorder = new LineBorder(Color.BLACK, 2);//create border
	Font font = new Font("SansSerif", Font.BOLD, 24);//create font
	//create Labels and panels
	JLabel H1 = new JLabel("He");
	JLabel H2 = new JLabel(new ImageIcon());
	JLabel H3 = new JLabel(new ImageIcon());
	JLabel H4 = new JLabel(new ImageIcon());
	JLabel H5 = new JLabel(new ImageIcon());
	JLabel H6 = new JLabel(new ImageIcon());
	JLabel H7 = new JLabel(new ImageIcon());
	JLabel Y1 = new JLabel("You");
	JLabel Y2 = new JLabel(new ImageIcon("image/card/init_5.png"));
	JLabel Y3 = new JLabel(new ImageIcon("image/card/init_1.png"));
	JLabel Y4 = new JLabel(new ImageIcon("image/card/init_2.png"));
	JLabel Y5 = new JLabel(new ImageIcon("image/card/init_3.png"));
	JLabel Y6 = new JLabel(new ImageIcon("image/card/init_4.png"));
	JLabel Y7 = new JLabel(new ImageIcon());
	JLabel C = new JLabel("");
	JPanel p1 = new JPanel(new GridLayout(2, 7, 5, 10));
	JPanel p2 = new JPanel(new GridLayout(1, 2, 5, 10));
	public FrameWork(){//constructor
		//set label's font and border
		H1.setFont(font);
		H2.setBorder(lineBorder);
		H3.setBorder(lineBorder);
		H4.setBorder(lineBorder);
		H5.setBorder(lineBorder);
		H6.setBorder(lineBorder);
		Y1.setFont(font);
		Y2.setBorder(lineBorder);
		Y3.setBorder(lineBorder);
		Y4.setBorder(lineBorder);
		Y5.setBorder(lineBorder);
		Y6.setBorder(lineBorder);
		C.setFont(font);
		p1.add(H1);
		p1.add(H2);
		p1.add(H3);
		p1.add(H4);
		p1.add(H5);
		p1.add(H6);
		p1.add(H7);
		p1.add(Y1);
		p1.add(Y2);
		p1.add(Y3);
		p1.add(Y4);
		p1.add(Y5);
		p1.add(Y6);
		p1.add(Y7);
		p2.add(button);
		button.addActionListener(this); //active button event here
		p2.add(C);
		add(p1, BorderLayout.NORTH);
		add(p2, BorderLayout.CENTER);
	}
	public void actionPerformed(ActionEvent e){//after press the button
		if(e.getSource() == button){
			//what you want?
			User He = new User();//create two players
			User You = new User();
			He.Random(21);//random total 21 card 
			You.Card = He.Card;
			You.Random(16);//random 16 card
			//set Icon 
			H2.setIcon(He.Player[0]);
			H3.setIcon(He.Player[1]);
			H4.setIcon(He.Player[2]);
			H5.setIcon(He.Player[3]);
			H6.setIcon(He.Player[4]);
			Y2.setIcon(You.Player[0]);
			Y3.setIcon(You.Player[1]);
			Y4.setIcon(You.Player[2]);
			Y5.setIcon(You.Player[3]);
			Y6.setIcon(You.Player[4]);
			if(He.Rank() > You.Rank()){//set Text
				H7.setText(He.Level);
				Y7.setText(You.Level);
				C.setText("You Loss");
			}
			else if(He.Rank() == You.Rank()){//set Text
				H7.setText(He.Level);
				Y7.setText(You.Level);
				C.setText("Draw");
			}
			else if(He.Rank() < You.Rank()){//set Text
				H7.setText(He.Level);
				Y7.setText(You.Level);
				C.setText("You Win");
			}
			else{}
		}
		else{}
	}
}
