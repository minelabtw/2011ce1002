package a7.s992001033;

public class Password 
{
	private String password = ""; 

	public Password(String input) throws InvalidPasswordException
	{
		if(input.length()<7)//字元數小於7
			throw new InvalidPasswordException("The length of the password "+input+" is not enough!!");
		else if(noNumber(input))//沒有數字
			throw new InvalidPasswordException("The password "+input+" has no number!!");
		else if(noLetter(input))//沒有字母
			throw new InvalidPasswordException("The password "+input+" has no letter!!");
		else
		{
			password = input;
			System.out.println(input);
			System.out.println("The password "+input+" in valid!!");
			A72.valid = true;
		}
	}
	public String getPassword()
	{
		return password;
	}
	public boolean noNumber(String input)//檢查沒有數字的method
	{
		for(int i=0;i<input.length();i++)
		{
			if((input.charAt(i)-'0')<=9 && (input.charAt(i)-'0')>=0)
				return false;
		}
			return true;
	}
	public boolean noLetter(String input)//檢查沒有字母的method
	{
		for(int i=0;i<input.length();i++)
		{
			if(((input.charAt(i)-'A')<=25 && (input.charAt(i)-'A')>=0)||((input.charAt(i)-'a')<=25 && (input.charAt(i)-'a')>=0))
				return false;
		}
			return true;
	}
}
