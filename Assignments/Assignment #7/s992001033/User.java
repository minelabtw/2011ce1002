package a7.s992001033;

public class User
{
	private int[] hePoker = new int[5];//對面的牌
	private int[] youPoker = new int[5];//己方的牌
	private int hePokerType = -1;//牌型
	private int youPokerType = -1;
	public User()
	{
		for (int i=0;i<5;i++)
		{
			hePoker[i] = -1;
			youPoker[i] = -1;
		}
	}
	public void setHePoker (int index,int input)
	{
		hePoker[index] = input;
	}
	public void setYouPoker (int index,int input)
	{
		youPoker[index] = input;
	}
	public void setHePokertype (int input)
	{
		hePokerType = input;
	}
	public void setYouPokertype (int input)
	{
		youPokerType = input;
	}
}
