package a4.s100502034;

import a4.s100502034.Triangle2D;
import java.util.Scanner;
import java.io.*;
public class A42 {
	public static void main(String args[]){
		int choose = 0;//用來選擇模式的變數
		Scanner input = new Scanner(System.in);
		double[] t1member = new double[6];//第一個三角形的點坐標
		double[] t2member = new double[6];//第二個三角形的點坐標
		double[] p = new double[2];//單獨的個點坐標
		System.out.println("Please enter three points of the new triangle: ");
		for (int i = 0; i < t1member.length ; i++){//輸入第一個新三角形坐標的值
			t1member[i] = input.nextDouble();
		}
		Triangle2D t1 = new Triangle2D(t1member[0],t1member[1],t1member[2],t1member[3],t1member[4],t1member[5]);//宣告第一個新三角形
		System.out.println("\n1.get the area of the new triangle");//求三角形面積
		System.out.println("2.get the perimeter of the new triangle");//求三角形周長
		System.out.println("3.check the new triangle is in the original triangle or not");//求第二個新三角形是否在系統內建三角形裡
		System.out.println("4.enter a point and check the point is in the original triangle or not");//求任意一點是否在系統內建三角形裡
		System.out.println("5.exit\n");
		while (choose != 5){//funtion們可獨立重複使用
			choose = input.nextInt();//選擇哪個模式
			switch (choose){
			case 1:
				System.out.println("The area of the new triangle is " + t1.getArea()+"\n");//利用getArea得到面積
				break;
			case 2:
				System.out.println("The perimeter of the new triangle is "+ t1.getPerimeter() + "\n");//利用getPerimeter得到周長
				break;
			case 3:
				System.out.println("Please input a new triangle for the new object:");
				for ( int i = 0;i < t2member.length;i++){//輸入第二個新三角形的坐標點
					t2member[i] = input.nextDouble();
				}
				Triangle2D t2 = new Triangle2D(t2member[0],t2member[1],t2member[2],t2member[3],t2member[4],t2member[5]);//宣告第二個新三角形
				if (t1.contains(t2) == true){//利用contians(Triangle2D)計算
					System.out.println("The new triangle of new object is in the original triangle of the old object!!!\n");
					break;
				}
				else{
					System.out.println("The new triangle of new object is out the original triangle of the old object!!!\n");
					break;
				}
			case 4:
				System.out.println("Please input a new print for the new object:");
				for ( int i = 0 ; i<p.length;i++){//輸入一個獨立的點
					p[i] = input.nextDouble();
				}
				MyPoint point = new MyPoint(p[0], p[1]);//用MyPoint宣告一個獨立的點
				if (t1.contains(point) == true){//用contains(Mypoint)計算
					System.out.println("The new point is ABSOLUTE in the original triangle (0,0), (17,6), (10, 15)!!!\n");
				}
				else{
					System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!\n");
				}
				break;
			default://其他選項即離開 (即沒有超出範圍的輸入)
				choose = 5;
				break;
			}
		}
	}
}

