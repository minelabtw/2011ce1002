package a4.s100502008;

import java.lang.Math;

public class MyPoint {
	private double x, y;

	public MyPoint()// constructor
	{
		x = 0;
		y = 0;
	}

	public MyPoint(double inx, double iny) {// constructor
		setX(inx);
		setY(iny);
	}

	public double getX() {//return x
		return x;
	}

	public double getY() {//return y
		return y;
	}

	public void setX(double inx) {//set x
		x = inx;
	}

	public void setY(double iny) {//set y
		y = iny;
	}

	public double distance(MyPoint input) {//compute distance
		return Math.sqrt((x - input.x) * (x - input.x) + (y - input.y)
				* (y - input.y));
	}
}
