package a4.s100502008;

import java.lang.Math;

public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0, 0.0);
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint p1_new, p2_new, p3_new;

	public MyPoint getP1_new() {// return p1new
		return p1_new;
	}

	public MyPoint getP2_new() {// return p2new
		return p2_new;
	}

	public MyPoint getP3_new() {// return p3new
		return p3_new;
	}

	public Triangle2D(double x1, double y1, double x2, double y2, double x3,
			double y3)// constructor
	{
		p1_new = new MyPoint(x1, y1);
		p2_new = new MyPoint(x2, y2);
		p3_new = new MyPoint(x3, y3);
	}

	public double getArea() {// compute area
		return Math
				.sqrt((p1_new.distance(p2_new) + p2_new.distance(p3_new) + p3_new
						.distance(p1_new))
						/ 2
						* ((p1_new.distance(p2_new) + p2_new.distance(p3_new) + p3_new
								.distance(p1_new)) / 2 - p1_new
								.distance(p2_new))
						* ((p1_new.distance(p2_new) + p2_new.distance(p3_new) + p3_new
								.distance(p1_new)) / 2 - p2_new
								.distance(p3_new))
						* ((p1_new.distance(p2_new) + p2_new.distance(p3_new) + p3_new
								.distance(p1_new)) / 2 - p3_new
								.distance(p1_new)));
	}

	public double getPerimeter()// compute Perimeter
	{
		return p1_new.distance(p2_new) + p2_new.distance(p3_new)
				+ p3_new.distance(p1_new);
	}

	public boolean contains(MyPoint p)// compute point in triangle or
										// not
	{
		double side1, side2, side3, outside1, outside2, outside3, s, areaPAB, areaPBC, areaPCA, areaABC;
		side1 = p.distance(p1);
		side2 = p.distance(p2);
		side3 = p.distance(p3);
		outside1 = p1.distance(p2);
		outside2 = p2.distance(p3);
		outside3 = p3.distance(p1);
		s = (side1 + side2 + outside1) / 2;
		areaPAB = Math.sqrt(s * (s - side1) * (s - side2) * (s - outside1));
		s = (side2 + side3 + outside2) / 2;
		areaPBC = Math.sqrt(s * (s - side3) * (s - side2) * (s - outside2));
		s = (side1 + side3 + outside3) / 2;
		areaPCA = Math.sqrt(s * (s - side3) * (s - side1) * (s - outside3));
		s = (outside1 + outside2 + outside3) / 2;
		areaABC = Math.sqrt(s * (s - outside2) * (s - outside1)
				* (s - outside3));
		if (Math.abs(areaABC - areaPCA - areaPBC - areaPAB) <= 0.5) {
			return true;// in
		} else {
			return false;// out
		}
	}

	public boolean contains(Triangle2D t)// compute new triangle in original
											// triangle or not
	{
		boolean check1 = false, check2 = false, check3 = false;
		if (contains(t.getP1_new())) {
			check1 = true;
		}
		if (contains(t.getP2_new())) {
			check2 = true;
		}
		if (contains(t.getP3_new())) {
			check3 = true;
		}
		if (check1 && check2 && check3) {
			return true;
		} else {
			return false;
		}
	}
}
