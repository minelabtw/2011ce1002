package a4.s100502008;

import a4.s100502008.Triangle2D;
import a4.s100502008.MyPoint;
import java.util.Scanner;

public class A42 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double x1, y1, x2, y2, x3, y3;//first 3 point
		System.out.print("Please enter three points of the new triangle: ");
		x1 = input.nextDouble();
		y1 = input.nextDouble();
		x2 = input.nextDouble();
		y2 = input.nextDouble();
		x3 = input.nextDouble();
		y3 = input.nextDouble();
		Triangle2D myTriangle1 = new Triangle2D(x1, y1, x2, y2, x3, y3);
		Triangle2D t2;
		MyPoint newp;
		for (int i = 1; i < 2; i++) {//loop
			System.out.println("\n1.get the area of the new triangle.");
			System.out.println("2.get the perimeter of the new triangle.");
			System.out
					.println("3.check the new triangle is in the original triangle or not");
			System.out
					.println("4.enter a point and check the point is in the original triangle or not");
			System.out.println("5.Exit");
			int chos = input.nextInt();
			switch (chos) {//choose
			case 1://area
				System.out.println("The area of the new triangle is "
						+ myTriangle1.getArea());
				i=0;
				break;
			case 2://perimeter
				System.out.println("The perimeter of the new triangle is "
						+ myTriangle1.getPerimeter());
				i=0;
				break;
			case 3://check the new triangle is in the original one or not
				double newx1,newy1,newx2,newy2,newx3,newy3;
				newx1=input.nextDouble();
				newy1=input.nextDouble();
				newx2=input.nextDouble();
				newy2=input.nextDouble();
				newx3=input.nextDouble();
				newy3=input.nextDouble();
				t2= new Triangle2D(newx1,newy1,newx2,newy2,newx3,newy3);
				if (myTriangle1.contains(t2)) {
					System.out
							.println("The new triangle is in the original triangle!!!");
				} else {
					System.out
							.println("The new triangle is NOT in the original triangle!!!");
				}
				i=0;
				break;
			case 4://check new point is in the original one or not
				double x = input.nextDouble();
				double y = input.nextDouble();
				newp=new MyPoint(x,y);
				if (myTriangle1.contains(newp)) {
					System.out
							.println("The new point is in the original triangle!!!");
				} else {
					System.out
							.println("The new point is NOT in the original triangle!!!");
				}
				i=0;
				break;
			case 5://exit
				i=2;
				break;
			default://error
				i=0;
				System.out.println("Error!!!");
				break;
			}
		}
	}
}
