	// ========================================================================================= //
	//		Author: Pluto Chan, Ka Hou															 //
	//		E-mail: jesic01@jesic-tech.com														 //
	//		Date: 2012/3/15																		 //
	//		Description: Assignment IV for course CE1002 - Introduction to Computer Science II	 //
	//					 A simple class for points, which can read and write the coordinates	 //
	//					 and calculate the distance between two points							 //
	// ========================================================================================= //

package a4.s985003038;

public class MyPoint {
	private int X, Y;											// variable for X and Y coordinates
	
	public MyPoint(){											// constructor for points with no assigning coordinates 
		X = 0;													// use default coordinates
		Y = 0;
	}
	
	public MyPoint(int newX, int newY){							// constructor for points with assigning coordinates
		X = newX;
		Y = newY;
	}
	
	// ========================================================================================= //
	//		Name: getX																			 //
	//		Input: none																			 //
	//		Output: X coordinates as integer													 //
	//		Description: read the value of X													 //
	// ========================================================================================= //
	public int getX(){
		return X;
	}
	
	// ========================================================================================= //
	//		Name: getY																			 //
	//		Input: none																			 //
	//		Output: Y coordinates as integer													 //
	//		Description: read the value of Y													 //
	// ========================================================================================= //
	public int getY(){
		return Y;
	}
	
	// ========================================================================================= //
	//		Name: setX																			 //
	//		Input: new X coordinates as integer													 //
	//		Output: none																		 //
	//		Description: write to the variable X												 //
	// ========================================================================================= //
	public void setX(int newX){
		X = newX;
	}
	
	// ========================================================================================= //
	//		Name: setY																			 //
	//		Input: new Y coordinates as integer													 //
	//		Output: none																		 //
	//		Description: write to the variable Y												 //
	// ========================================================================================= //
	public void setY(int newY){
		Y = newY;
	}
	
	// ========================================================================================= //
	//		Name: distance																		 //
	//		Input: another point as MyPoint														 //
	//		Output: the distance between the original point and the input point					 //
	//		Description: calculate the distance between two points								 //
	// ========================================================================================= //
	public double distance(MyPoint p){
		return Math.sqrt(Math.pow(X-p.getX(), 2) + Math.pow(Y-p.getY(), 2));
	}
}