	// ========================================================================================= //
	//		Author: Pluto Chan, Ka Hou															 //
	//		E-mail: jesic01@jesic-tech.com														 //
	//		Date: 2012/3/15																		 //
	//		Last-Mod: 2012/3/18																	 //
	//		Description: Assignment IV for course CE1002 - Introduction to Computer Science II	 //
	//					 A simple class for polygon, which can read and update the points,		 //
	//					 calculate the perimeter and area of the polygon, and also decide		 //
	//					 whether some points is within the polygon								 //
	// ========================================================================================= //

package a4.s985003038;

import javax.swing.JOptionPane;

public class Polygon {
	private static String programName = "Pluto's Programming";			// title of the message dialog and the input dialog
	private static int indexCounter;									// static counting variable for the index of polygon
	private int edge;													// the number of edges of the polygon
	private MyPoint[] point;											// the point set of the polygon
	private String name;												// the name of the polygon
	
	public Polygon(int numberOfEdge){									// constructor for only assigning the number of edges
		edge = numberOfEdge;
		point = new MyPoint[edge];										// create the point set with size corresponding to the number of edges
		name = "Polygen" + indexCounter++;								// use default polygon name, and increase the index counter
	}
	
	public Polygon(int numberOfEdge, String newName){					// constructor for assigning the number of edges and the name of the polygon
		edge = numberOfEdge;
		point = new MyPoint[edge];										// create the point set with size corresponding to the number of edges
		name = newName;													// use the name assigned
		indexCounter++;													// increase the index counter
	}
	
	// ========================================================================================= //
	//		Name: getName																		 //
	//		Input: none																			 //
	//		Output: name as String																 //
	//		Description: read the value of name													 //
	// ========================================================================================= //
	public String getName(){
		return name;
	}

	// ========================================================================================= //
	//		Name: setName																		 //
	//		Input: new name as String															 //
	//		Output: none																		 //
	//		Description: update the value of name												 //
	// ========================================================================================= //
	public void setName(String newName){
		name = newName;
	}
	
	// ========================================================================================= //
	//		Name: setPoint																		 //
	//		Input: none																			 //
	//		Output: none																		 //
	//		Description: update coordinates of all the points									 //
	// ========================================================================================= //
	public void setPoint(){
		for(int i = 0; i < edge; i++){
			String[] coord = JOptionPane.showInputDialog(null, "Please input the coordinate of point " + (i + 1) + " of " + name + " <Example: 5,10>", programName, JOptionPane.QUESTION_MESSAGE).split(",");
			point[i] = new MyPoint(Integer.parseInt(coord[0].trim()), Integer.parseInt(coord[1].trim()));
		}
	}
	
	// ========================================================================================= //
	//		Name: setPoint																		 //
	//		Input: a new point as MyPoint, the index of the point which is going to be updated	 //
	//		Output: none																		 //
	//		Description: update coordinates of one specified points								 //
	// ========================================================================================= //
	public void setPoint(MyPoint newPoint, int index){
		if(index < 0 || index > (edge - 1))								// if the index is out of range, show error message
			JOptionPane.showMessageDialog(null, "Error: point" + (index + 1) + " does not exist.", programName, JOptionPane.ERROR_MESSAGE);
		else {
			point[index] = newPoint;
		}
	}
	
	// ========================================================================================= //
	//		Name: getPoint																		 //
	//		Input: none																			 //
	//		Output: point set array as MyPoint													 //
	//		Description: get the point set of the polygon										 //
	// ========================================================================================= //
	public MyPoint[] getPoint(){
		return point;
	}
	
	// ========================================================================================= //
	//		Name: getArea																		 //
	//		Input: none																			 //
	//		Output: area of the polygon in double												 //
	//		Description: calculate the area of the polygon										 //
	// ========================================================================================= //
	public double getArea(){
		if(edge > 2){													// if the polygon has edges less than 2, the area of the polygon is 0
			double result = 0;											// otherwise, separate the polygon into some triangles
			for(int i = 0; i < edge - 2; i++)							// and calculate the area of each triangles, the summation of area of these triangles is equal to the area of the original polygon
				result += triangleArea(point[i], point[i+1], point[edge-1]);
			return result;
		} else return 0;
	}
	
	// ========================================================================================= //
	//		Name: getPerimeter																	 //
	//		Input: none																			 //
	//		Output: perimeter of the polygon in double											 //
	//		Description: calculate the perimeter of the polygon									 //
	// ========================================================================================= //
	public double getPerimeter(){
		double result = 0;
		for(int i = 0; i < edge; i++)									// calculate the length of each edges, the summation of length of the edges is the perimeter of the polygon
			result += point[i].distance(point[(i + 1) % edge]);
		return result;
	}
	
	// ========================================================================================= //
	//		Name: contain																		 //
	//		Input: point set as MyPoint															 //
	//		Output: result as boolean															 //
	//		Description: consider all the points in the point set is within the polygon			 //
	// ========================================================================================= //
	public boolean contain(MyPoint[] points){
		if(getArea() == 0) return false;								// if all the points of the polygon line up as a line or even gather as a point, it cannot contain anything
		if(edge > 2){													// if the polygon with edges less than 3, it also cannot contain anything 
			for(int i = 0; i < points.length; i++){						// check each of the points in the point set is within the polygon
				double totalArea = 0;
				for(int j = 0; j < edge; j++)							// to consider that, we calculate the area of triangles formed by that point and every two of the points of the polygon
					totalArea += triangleArea(point[j], point[(j+1)%edge], points[i]);
				if(Math.abs(totalArea - getArea()) > 0.05) return false;// if the total area of the triangles is equal to the area of the polygon, with some floating point error, it will be considered within the polygon
			}
			return true;
		}
		return false;
	}
	
	// ========================================================================================= //
	//		Name: triangleArea																	 //
	//		Input: three points as MyPoint														 //
	//		Output: area of the triangle formed by the input points as double					 //
	//		Description: calculate the area formed by the input points							 //
	// ========================================================================================= //
	private double triangleArea(MyPoint p1, MyPoint p2, MyPoint p3){
		double a = p1.distance(p2);
		double b = p1.distance(p3);
		double c = p2.distance(p3);
		double p = (a+b+c)/2;
		return Math.sqrt(p*(p-a)*(p-b)*(p-c));							// using the Heron formula to calculate the area
	}
}