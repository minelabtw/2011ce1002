	// ========================================================================================= //
	//		Author: Pluto Chan, Ka Hou															 //
	//		E-mail: jesic01@jesic-tech.com														 //
	//		Date: 2012/3/15																		 //
	//		Last-Mod: 2012/3/16																	 //
	//		Description: Assignment IV for course CE1002 - Introduction to Computer Science II	 //
	//					 A class for Triangle2D, which contain two triangle as class Polygon	 //
	//					 and some methods using the methods of Polygon							 //
	// ========================================================================================= //

package a4.s985003038;

public class Triangle2D {
	private Polygon originalTriangle, newTriangle;
	
	public Triangle2D(){												// constructor to define the original triangle and the new triangle
		originalTriangle = new Polygon(3, "Original Triangle");
		originalTriangle.setPoint(new MyPoint(0, 0), 0);
		originalTriangle.setPoint(new MyPoint(10, 15), 1);
		originalTriangle.setPoint(new MyPoint(17, 6), 2);
		
		newTriangle = new Polygon(3, "New Triangle");
		newTriangle.setPoint();
	}
	
	// ========================================================================================= //
	//		Name: getOriginalTriangle															 //
	//		Input: none																			 //
	//		Output: the original triangle as Polygon											 //
	//		Description: get the original triangle												 //
	// ========================================================================================= //
	public Polygon getOriginalTriangle(){
		return originalTriangle;
	}
	
	// ========================================================================================= //
	//		Name: getNewTriangle																 //
	//		Input: none																			 //
	//		Output: the new triangle as Polygon													 //
	//		Description: get the new triangle													 //
	// ========================================================================================= //
	public Polygon getNewTriangle(){
		return newTriangle;
	}
	
	// ========================================================================================= //
	//		Name: getNewTrianglePerimeter														 //
	//		Input: none																			 //
	//		Output: the perimeter of the new triangle as double									 //
	//		Description: get the perimeter of the new triangle									 //
	// ========================================================================================= //
	public double getNewTrianglePerimeter(){
		return newTriangle.getPerimeter();
	}
	
	// ========================================================================================= //
	//		Name: getNewTriangleArea															 //
	//		Input: none																			 //
	//		Output: the area of the new triangle as double										 //
	//		Description: get the area of the new triangle										 //
	// ========================================================================================= //
	public double getNewTriangleArea(){
		return newTriangle.getArea();
	}
	
	// ========================================================================================= //
	//		Name: contains																		 //
	//		Input: two dimensional coordinates of a point										 //
	//		Output: result as boolean															 //
	//		Description: check if the input point is within the original triangle				 //
	// ========================================================================================= //
	public boolean contains(int x, int y){
		MyPoint[] p = new MyPoint[1];
		p[0] = new MyPoint(x, y);										// create a MyPoint object and send it to the contain method of class Polygon, which can only use a MyPoint array as parameter
		return originalTriangle.contain(p);
	}
	
	// ========================================================================================= //
	//		Name: contains																		 //
	//		Input: a Triangle2D	object															 //
	//		Output: result as boolean															 //
	//		Description: check if the new triangle in the input object is within the original	 //
	//					 triangle of the original object										 //
	// ========================================================================================= //
	public boolean contains(Triangle2D triangle){						// send the point set of the new triangle of the input object to the contain method of class Polygon, which can only use a MyPoint array as parameter
		return originalTriangle.contain(triangle.getNewTriangle().getPoint());
	}
}
