	// ========================================================================================= //
	//		Author: Pluto Chan, Ka Hou															 //
	//		E-mail: jesic01@jesic-tech.com														 //
	//		Date: 2012/3/18																		 //
	//		Description: Assignment IV for course CE1002 - Introduction to Computer Science II	 //
	//					 Class for StackOfPrime, which inherit the class of StackOfIntegers,	 //
	//					 which can only push prime integers into the stack						 //
	// ========================================================================================= //

package a4.s985003038;

import java.util.Scanner;

public class StackOfPrime extends StackOfIntegers {
	public StackOfPrime(int num) {									// constructor through StackOfIntegers
		super(num);
	}

	// ========================================================================================= //
	//		Name: Push																			 //
	//		Input: how many elements you want to push into the stack							 //
	//		Output: none																		 //
	//		Description: override method, push a number of prime integers into the stack		 //
	// ========================================================================================= //
	public void Push(int num){
		Scanner input = new Scanner(System.in);
		if(isFull())												// if the stack is full, you cannot push even one element into the stack anymore
			System.out.println("Error: the stack is full.");		// so, just show an error message
		else {
			if(num > maximumOfElements - numberOfElements)			// if the stack has not full yet, but there are not enough space to push all the elements into the stack
				System.out.println("Warning: the stack is already full. You can only push " + (maximumOfElements - numberOfElements) + " numbers into the stack.");
																	// warn the user that he can only push a number of elements into the stack
			int canInputNumber = Math.min(num, maximumOfElements - numberOfElements);
			System.out.print("Please input " + canInputNumber + " number" + (canInputNumber == 1 ? "" : "s") + ": ");
			
			int notPrimeCounter;									// a counter to count how many input integers are not prime
			do {
				String notPrime = "";								// reinitialize the counter
				notPrimeCounter = 0;
				
				for(int i = 0; i < canInputNumber; i++){
					int inputNumber = input.nextInt();
					if(isPrime(inputNumber))						// if the input number is prime, push it into the stack
						elements[numberOfElements + i - notPrimeCounter] = inputNumber;
					else {											// otherwise, increase the counter, and let the user input again in the next turn
						notPrimeCounter++;
						notPrime = notPrime + (notPrime.isEmpty() ? inputNumber : ", " + inputNumber);
					}
				}

				numberOfElements += canInputNumber - notPrimeCounter;
																	// update the pointer to the top of stack in each turn 
				if(notPrimeCounter > 0){
					canInputNumber = notPrimeCounter;				// if there are more than one not prime elements, show the notice and let the user input again
					System.out.print("Warning: " + notPrime + (notPrimeCounter == 1 ? " is" : " are") + " not " + (notPrimeCounter == 1 ? "a " : "") + "prime number" + (notPrimeCounter == 1 ? "" : "s") + ", please input " + notPrimeCounter + " number" + (notPrimeCounter == 1 ? "" : "s") + " again: ");
				}
			} while(notPrimeCounter > 0);
			
			showAll();
		}
	}
	
	// ========================================================================================= //
	//		Name: Push																			 //
	//		Input: an element array going to push into the stack								 //
	//		Output: none																		 //
	//		Description: push all the prime integers in the input array into the stack			 //
	// ========================================================================================= //
	public void Push(int[] num){
		if(isFull())												// if the stack is full, you cannot push even one element into the stack anymore
			System.out.println("Error: the stack is full.");		// so, just show an error message
		else {
			if(num.length > maximumOfElements - numberOfElements)	// if the stack has not full yet, but there are not enough space to push all the elements into the stack
				System.out.println("Warning: the stack is already full. You can only push " + (maximumOfElements - numberOfElements) + " numbers into the stack.");
																	// warn the user that only a number of elements have been pushed into the stack
			int canInputNumber = Math.min(num.length, maximumOfElements - numberOfElements);
			
			int notPrimeCounter;									// a counter to count how many input integers are not prime
			String notPrime = "";									// reinitialize the counter
			notPrimeCounter = 0;
				
			for(int i = 0; i < canInputNumber; i++){
				int inputNumber = num[i];
				if(isPrime(inputNumber))							// if the input number is prime, push it into the stack
					elements[numberOfElements + i - notPrimeCounter] = inputNumber;
				else {												// otherwise, increase the counter
					notPrimeCounter++;
					notPrime = notPrime + (notPrime.isEmpty() ? inputNumber : ", " + inputNumber);
				}
			}

			numberOfElements += canInputNumber - notPrimeCounter;
																	// update the pointer to the top of stack in each turn 
			if(notPrimeCounter > 0)
				System.out.print("Warning: " + notPrime + (notPrimeCounter == 1 ? " is" : " are") + " not " + (notPrimeCounter == 1 ? "a " : "") + "prime number" + (notPrimeCounter == 1 ? "" : "s") + ", only " + (canInputNumber - notPrimeCounter) + " number" + (canInputNumber - notPrimeCounter == 1 ? " has" : "s have") + " pushed into the stack: ");
			
			showAll();
		}
	}
	
	// ========================================================================================= //
	//		Name: isPrime																		 //
	//		Input: the integer number which is going to be checked								 //
	//		Output: the result as boolean														 //
	//		Description: check if the input number is a prime or not							 //
	// ========================================================================================= //
	private boolean isPrime(int num){
		int prime[] = new int[num + 1];								// create a temporary array to keep all the integers less than or equal to the input number
		for(int i = 2; i <= num; i++)
			prime[i] = i;
		
		for(int i = 2; i <= Math.sqrt(num); i++)					// kick out all the integers which are multiples of the other integers, until the square root of the input integer
			if(prime[i] != 0){										// since there is no way to form the input integers by multiplying two numbers greater than the square root of that integer
				int step = i * i;									// we use 0 to replace the value of the elements in the temporary array if we know that is not a prime
				while(step <= num){									// after doing this, the elements, which are not 0, in the temporary array are prime numbers
					prime[step] = 0;
					step += i;
				}
			}
		
		if(prime[num] != 0)											// to check whether the input number is a prime number or not
			return true;											// we can just check the corresponding element in the temporary array 
		else														// if the value of the element is not 0, we know that the input number is a prime
			return false;											// otherwise, it is not a prime
	}
}
