	// ========================================================================================= //
	//		Author: Pluto Chan, Ka Hou															 //
	//		E-mail: jesic01@jesic-tech.com														 //
	//		Date: 2012/3/15																		 //
	//		Last-Mod: 2012/3/18																	 //
	//		Description: Assignment IV for course CE1002 - Introduction to Computer Science II	 //
	//					 A testing program for class of Triangle2D with simple GUI				 //
	// ========================================================================================= //

package a4.s985003038;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class A42 extends JPanel {
	private static final long serialVersionUID = 1L;
	private static String programName = "Pluto's Programming";			// title of the message dialog and the input dialog
	private static int state = 0;										// a finite state to control the drawing method
	private static int multiple = 8;									// ratio of the length, that is, the ratio when drawing the triangle, you can adjust it by yourself
	private static Triangle2D originalTriangle2D;						// global variables as parameters of painting method
	private static Triangle2D newTriangle2D;
	private static MyPoint point;
	
	public static void main(String[] argv){		
		JFrame frame = new JFrame();									// creating a GUI frame
		frame.add(new A42());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);			// close the frame when the program is terminated
		frame.setTitle(programName);									// set the title of the frame
        frame.setSize(500, 500);										// set the size of the frame
        frame.setVisible(true);											// set the visibility so you can see the frame
        
        originalTriangle2D = new Triangle2D();							// create the first Triangle2D
        state = 1;														// and paint the original triangle and new triangle in the first Triangle2D
        frame.repaint();
        if(originalTriangle2D.getNewTriangleArea() == 0)				// if the new triangle is detected as a line or even a point, show the warning message
        	JOptionPane.showMessageDialog(null, "Warning: the new triangle you defined is too small", programName, JOptionPane.WARNING_MESSAGE);
		
		printMenu();													// show the menu
		while(true){
			String operation = JOptionPane.showInputDialog(null, "Please input your comment <5 to show menu>", programName, JOptionPane.QUESTION_MESSAGE);
																		// get the comment from the user
			if(operation.trim().equals("1")){							// 1 -- show the area
				state = 1;												// repaint the original triangle and new triangle in the original Triangle2D
		        frame.repaint();
				JOptionPane.showMessageDialog(null, "The area of the new triangle is " + originalTriangle2D.getNewTriangleArea(), programName, JOptionPane.INFORMATION_MESSAGE);
			} else if(operation.trim().equals("2")){					// 2 -- show the perimeter
				state = 1;												// repaint the original triangle and new triangle in the original Triangle2D
		        frame.repaint();
				JOptionPane.showMessageDialog(null, "The perimeter of the new triangle is " + originalTriangle2D.getNewTrianglePerimeter(), programName, JOptionPane.INFORMATION_MESSAGE);
			} else if(operation.trim().equals("3")){					// 3 -- create a new Triangle2D object, decide whether the new triangle of the new object is in the old triangle of the old object
				newTriangle2D = new Triangle2D();						// create the new Triangle2D
				if(newTriangle2D.getNewTriangleArea() == 0)				// if the new triangle is detected as a line or even a point, show the warning message
		        	JOptionPane.showMessageDialog(null, "Warning: the new triangle you defined is too small", programName, JOptionPane.WARNING_MESSAGE);
				state = 2;												// paint the original triangle and new triangle in the new Triangle2D
				frame.repaint();
				JOptionPane.showMessageDialog(null, "The new triangle of new object is " + (originalTriangle2D.contains(newTriangle2D) ? "" : "NOT ") + "in the original triangle of the old object", programName, JOptionPane.INFORMATION_MESSAGE);
			} else if(operation.trim().equals("4")){					// 4 -- check whether a point is in the original triangle
				String[] coord = JOptionPane.showInputDialog(null, "Please input your point <Example: 5,10>", programName, JOptionPane.QUESTION_MESSAGE).split(",");
				point = new MyPoint(Integer.parseInt(coord[0].trim()), Integer.parseInt(coord[1].trim()));
				state = 3;												// paint only the point and the original triangle
				frame.repaint();
				JOptionPane.showMessageDialog(null, "The new point is " + (originalTriangle2D.contains(point.getX(), point.getY()) ? "" : "NOT ") + "in the original triangle (0,0), (17,6), (10,15)", programName, JOptionPane.INFORMATION_MESSAGE);
			} else if(operation.trim().equals("5"))						// 5 -- show the menu again
				printMenu();
			else if(operation.trim().equals("6"))						// 6 -- exit the program
				break;
			else														// otherwise, wrong input
				JOptionPane.showMessageDialog(null, "Error: no such operations", programName, JOptionPane.ERROR_MESSAGE);
		}
		
		JOptionPane.showMessageDialog(null, "Bye.", programName, JOptionPane.INFORMATION_MESSAGE);
		System.exit(0);
	}
	
	// ========================================================================================= //
	//		Name: printMenu																		 //
	//		Input: none																			 //
	//		Output: none																		 //
	//		Description: show the menu															 //
	// ========================================================================================= //
	private static void printMenu(){
		JOptionPane.showMessageDialog(null, "=================== Operations that you can take ===================\r\n" +
				"1. Get the area of the new triangle\r\n" +
				"2. Get the perimeter of the new triangle\r\n" +
				"3. Check the new triangle is in the original triangle or not\r\n" +
				"4. Enter a point and check the point is in the original triangle or not\r\n" +
				"5. Show the menu\r\n" +
				"6. Exit\r\n" +
				"====================================================================="
				, programName, JOptionPane.INFORMATION_MESSAGE);
	}
	
	// ========================================================================================= //
	//		Name: paint																			 //
	//		Input: none																			 //
	//		Output: none																		 //
	//		Description: a redefined method in JPanel, used to draw a graph on the frame.		 //
	//					 never call this method directly! this method will automatically execute //
	//					 when the frame is created and whenever the repaint() method are called	 //
	// ========================================================================================= //
	public void paint(Graphics graph){
		if(state == 0){													// if state is 0, it is just starting the program
			graph.setFont(new Font("Arial", Font.BOLD, 45));			// set the font-family as Arial, font-weight as Bold, and font-size as 45pt
			graph.setColor(Color.blue);									// set the font-color as Blue
			graph.drawString(programName, 15, 230);						// and draw out the title of the program
		} else if(state == 1 || state == 2 || state == 3){
			Triangle2D drawTriangle;									// decide to draw the original object or the new objects, state 2 for drawing the new objects
			if(state == 1 || state == 3) drawTriangle = originalTriangle2D;
			else drawTriangle = newTriangle2D;							// to draw the triangle, we need all the coordinates of the triangles, here, we get the points from the triangles
			MyPoint[] originalPoint = drawTriangle.getOriginalTriangle().getPoint();
			MyPoint[] newPoint = drawTriangle.getNewTriangle().getPoint();
			graph.setFont(new Font("Arial", Font.PLAIN, 12));			// to write the actual coordinate beside the points, we use the font-size of 12pt
			
			graph.setColor(Color.blue);									// using blue color to draw the original triangle
			for(int i = 0; i < originalPoint.length; i++){				// draw for each lines, calculate the relative coordinates for each two points, draw a line to connect them, and write the actual coordinates beside the points
				int x1Location = 230 + originalPoint[i].getX() * multiple;
				int y1Location = 230 + originalPoint[i].getY() * -multiple;
				int x2Location = 230 + originalPoint[(i+1)%3].getX() * multiple;
				int y2Location = 230 + originalPoint[(i+1)%3].getY() * -multiple;
				graph.drawLine(x1Location, y1Location, x2Location, y2Location);
				graph.drawLine(x1Location - 5, y1Location - 5, x1Location + 5, y1Location + 5);
				graph.drawLine(x1Location + 5, y1Location - 5, x1Location - 5, y1Location + 5);
				graph.drawString("(" + originalPoint[i].getX() + ", " + originalPoint[i].getY() + ")", x1Location + 10, y1Location + 10);
			}
			
			if(state != 3){
				graph.setColor(Color.red);								// using red color to draw the new triangle
				for(int i = 0; i < newPoint.length; i++){				// same here for each lines, calculate the relative coordinates for each two points, draw a line to connect them, and write the actual coordinates beside the points
					int x1Location = 230 + newPoint[i].getX() * multiple;
					int y1Location = 230 + newPoint[i].getY() * -multiple;
					int x2Location = 230 + newPoint[(i+1)%3].getX() * multiple;
					int y2Location = 230 + newPoint[(i+1)%3].getY() * -multiple;
					graph.drawLine(x1Location, y1Location, x2Location, y2Location);
					graph.drawLine(x1Location - 5, y1Location - 5, x1Location + 5, y1Location + 5);
					graph.drawLine(x1Location + 5, y1Location - 5, x1Location - 5, y1Location + 5);
					graph.drawString("(" + newPoint[i].getX() + ", " + newPoint[i].getY() + ")", x1Location + 10, y1Location + 10);
				}
			}
			
			graph.setFont(new Font("Arial", Font.PLAIN, 25));			// to write the notice, we use the size of 25pt
			graph.setColor(Color.blue);									// using blue color to write the blue notice
			graph.drawString("- Blue color for the original triangle", 20, 30);
			if(state != 3){
				graph.setColor(Color.red);								// using red color to write the red notice
				graph.drawString("- Red color for the new triangle", 20, 60);
			}
			
			if(state == 3){
				graph.setColor(Color.magenta);							// if we need to draw a point, we use magenta color
				graph.drawString("- Magenta color for the point", 20, 60);
				int xLocation = 230 + point.getX() * multiple;			// calculating the relative coordinate on the displaying graph
				int yLocation = 230 + point.getY() * -multiple;			// then, draw a cross symbol on the location of the point
				graph.drawLine(xLocation - 5, yLocation - 5, xLocation + 5, yLocation + 5);
				graph.drawLine(xLocation + 5, yLocation - 5, xLocation - 5, yLocation + 5);
				graph.setFont(new Font("Arial", Font.PLAIN, 12));		// and write the actual coordinate of the point beside the cross symbol
				graph.drawString("(" + point.getX() + ", " + point.getY() + ")", xLocation + 10, yLocation + 10);
			}
		}
	}
}