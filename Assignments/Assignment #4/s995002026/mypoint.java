package a4.s995002026;

public class mypoint {
	private double x,y;												//X，Y座標
	
	mypoint(){																				//沒參數的constructor
		
	};															
	
	mypoint(double for_x,double for_y){								//有參數的constructor
		x = for_x;
		y= for_y;
	}
	
	public double getX(){															 // return x value
			return x;
	}

	public double getY(){															// return y value
			return y;
	}

	public void setX(double input) 											// set x value
	{
			x	=	input;
	}

	public void setY(double input) 											// set y value
	{
			y	=	input;
	}

	public double distance(mypoint  input){							 // the distance between this point and the input point
			double dis;
			
			dis=Math.sqrt( (input.getX()-getX())* (input.getX()-getX())+ (input.getY()-getY())* (input.getY()-getY()));
			
			return dis; 
	}
}
