package a4.s995002026;

public class triangle2d {
	
	private mypoint p1 = new mypoint(0.0, 0.0); 				// points of original triangle triangle
	
	private mypoint p2 = new mypoint(17.0, 6.0);
	
	private mypoint p3 = new mypoint(10.0, 15.0);
	
	private mypoint np1, np2, np3;				 // points of new triangle

	public mypoint getP1_new(){ 										// get the p1 of new triangle
			
			return np1;
	}

	public mypoint getP2_new(){ 										// get the p2 of new triangle
		
		return np2;
	}

	public mypoint getP3_new(){ 										// get the p3 of new triangle
		
		return np3;
	}

	// constructor with arguments to initial the points of the new triangle
	public triangle2d(double x1, double y1, double x2, double y2, double x3, double y3){
			np1=new mypoint(x1,y1);
			
			np2=new mypoint(x2,y2);
			
			np3=new mypoint(x3,y3);
			
	}

	public double getArea(){ 							// calculate the area of the new triangle
			double s;
			s=	(	np1.distance(np2)+np1.distance(np3)+np2.distance(np3)	)	/2 ;
			
			return Math.sqrt(	s*(s-np1.distance(np2))*(s-np1.distance(np3))*(s-np2.distance(np3))	);
	}

	public double getPerimeter(){ 					// calculate the perimeter of the new triangle
			
		return np1.distance(np2)+np1.distance(np3)+np2.distance(np3);
	}

	// check the input point is in the original triangle of the object or not
	public boolean contains(mypoint p){
		// if input point p is in the triangle, area of PAB + area of PBC + area of PCA = area of ABC 
		double side1, side2, side3, s, areaPAB, areaPBC, areaPCA, areaABC;
			
		side1=p.distance(p1);								//P到NP1的長度
		
		side2=p.distance(p2);								//P到NP2的長度
		
		side3=p.distance(p3);								//P到NP3的長度
		// calculate area of PAB
		double s1;
		
		s1 = (side1+side2 + p1.distance(p2))/2 ;
		
		areaPAB = Math.sqrt(s1*(s1-side1)*(s1-side2)*(s1-p1.distance(p2)));

		// calculate area of PBC
		double s2;
		
		s2=(side1+side3 + p1.distance(p3))/2 ;

		areaPCA =Math.sqrt(s2*(s2-side1)*(s2-side3)*(s2-p1.distance(p3)));
		
		// calculate area of PCA
		double s3;
		
		s3=(side2+side3 + p2.distance(p3))/2 ;
		
		areaPBC =Math.sqrt(s3*(s3-side2)*(s3-side3)*(s3-p2.distance(p3)));


		// calculate area of ABC
		
		s=(	p1.distance(p2)+p1.distance(p3)+p2.distance(p3)	)	/2 ;
		
		areaABC=Math.sqrt (s* (s-p1.distance(p2) )*(s-p1.distance(p2))*(s-p2.distance(p3)));


		// if areaPAB + areaPBC + areaPCA = areaABC, then p is in the ABC
                // However, there is some error(誤差) in this calculation
                // So if the absolute value of (areaABC - areaPAB + areaPBC + areaPCA) is less than 0.5
                // we consider the point is inside the original triangle
                // you can use Math.abs(double) to calculate the absolute value
		if( Math.abs(areaPAB+areaPBC+areaPCA-areaABC)<0.5)
			return true;
		else
			return false;
			

	}

	// check the input triangle(Triangle2D object) is in the original triangle of this object or not
	public boolean contains(triangle2d input){
	       // if the input triangle is the original triangle of this object
               // then all points of the input triangle is in the original triangle of this object
		if(contains(input.np1))
			if(contains(input.np2))
				if(contains(input.np3))
					if(input.getArea()<=getArea())
						return true;
		
		return false;
	}
}
