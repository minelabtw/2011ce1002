package a4.s100502015;

public class MyPoint 
{
	private double x;
	private double y;
	public MyPoint()
	{
		setx(0);
		sety(0);
	}
	public MyPoint(double conx,double cony)
	{//constructer
		setx(conx);
		sety(cony);		
	}
	public void setx(double inputx)
	{
		x = inputx;
	}
	public void sety(double inputy)
	{
		y = inputy;
	}
	public double getx()
	{
		return x;
	}
	public double gety()
	{
		return y;
	}
	public double distance(MyPoint input)//calculate distance 
	{
		return Math.sqrt((x-input.x)*(x-input.x)+(y-input.y)*(y-input.y));
	}

}
