package a4.s100502015;
import java.util.Scanner; 
import a4.s100502015.Triangle2D;
import a4.s100502015.MyPoint;
public class A42 
{
	public static void main(String[]args)
	{
		double x1,x2,x3,y1,y2,y3;
		int select2=0;		
		boolean flag = true;
		Scanner input = new Scanner(System.in);		
		System.out.print("Please enter three points of the new triangle:");
		x1 = input.nextDouble();//input point 
		x2 = input.nextDouble();
		x3 = input.nextDouble();
		y1 = input.nextDouble();
		y2 = input.nextDouble();
		y3 = input.nextDouble();		
		Triangle2D triangle = new Triangle2D(x1,y1,x2,y2,x3,y3);//new object triangle
		Triangle2D triangle2;
		MyPoint newpoint;
		while(flag==true)
		{
			System.out.print("1.get the area of the new triangle\n " +
							 "2.get the perimeter of the new triangle\n"+
					  		 "3.check the new triangle is in the original triangle or not\n"+
							 "4.enter a point and check the point is in the original triangle or not\n"+
							 "5.Exit");
			//option
			select2 = input.nextInt();
			switch(select2)
			{
			case 1:
				System.out.print("answer is "+triangle.getArea());
				break;
			case 2:
				System.out.print("answer is "+triangle.getPerimeter());
				break;
			case 3://check new triangle in old triangle
				double xx1,yy1,xx2,yy2,xx3,yy3;
				xx1=input.nextDouble();
				yy1=input.nextDouble();
				xx2=input.nextDouble();
				yy2=input.nextDouble();
				xx3=input.nextDouble();
				yy3=input.nextDouble();
				//input new (x,y)
				triangle2 = new Triangle2D(xx1,yy1,xx2,yy2,xx3,yy3);
				if(triangle.contains(triangle2))
				{
					System.out.print("new triangle is in old triangle\n");					
				}
				else
				{
					System.out.print("new triangle is not in old triangle\n");
				}
				break;
			case 4://check point in old triangle
				double x,y;
				x = input.nextDouble();			
				y = input.nextDouble();	
				newpoint = new MyPoint(x,y);
				if(triangle.contains(newpoint)==true)
				{
					System.out.println("The new point is in the original triangle\n");					
				}
				else
				{
					System.out.println("The new point is not in the original triangle\n");
				}
				break;
			case 5://exit
				flag = false;
				break;
			default://error				
				System.out.println("Error!!!");
				break;				
			
			}
		}
	}
}
