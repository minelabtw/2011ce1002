package a4.s100502015;
import java.lang.Math;

public class Triangle2D 
{	
	private MyPoint p1 = new MyPoint(0.0, 0.0); // points of original triangle triangle
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint p1_new, p2_new, p3_new; // points of new triangle

	public MyPoint getP1_new()
	{ 
		return p1_new;
	}

	public MyPoint getP2_new()
	{ 
		return p2_new;
	}

	public MyPoint getP3_new()
	{ 
		return p3_new;
	}

	// constructor with arguments to initial the points of the new triangle
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3)
	{
		p1_new = new MyPoint(x1,y1);
		p2_new = new MyPoint(x2,y2);
		p3_new = new MyPoint(x3,y3);
	}

	public double getArea()
	{ // calculate the area of the new triangle
		double s = (p1_new.distance(p2_new)+p2_new.distance(p3_new)+p3_new.distance(p1_new))/2; 
		return Math.sqrt(s*(s-p1_new.distance(p2_new))*(s-p2_new.distance(p3_new))*(s-p3_new.distance(p1_new)));
	}
	public double getPerimeter()
	{ // calculate the perimeter of the new triangle
		return p1_new.distance(p2_new)+p2_new.distance(p3_new)+p3_new.distance(p1_new);
	}

	// check the input point is in the original triangle of the object or not
	public boolean contains(MyPoint p)
	{		
		double side1, side2, side3,a,b,c,s1,s2,s3,sall, areaPAB, areaPBC, areaPCA, areaABC;
		side1 = p.distance(p1);
		side2 = p.distance(p2);
		side3 = p.distance(p3);
		a = p1.distance(p2);
		b = p2.distance(p3);
		c = p3.distance(p1);
		s1 = (side1+side2+a)/2;
		s2 = (side2+side3+b)/2;		
		s3 = (side3+side1+c)/2;
		sall =(a+b+c)/2;
		areaPAB = Math.sqrt(s1*(s1-side1)*(s1-a)*(s1-side2));
		areaPBC = Math.sqrt(s2*(s2-side2)*(s2-b)*(s2-side3));
		areaPCA = Math.sqrt(s3*(s3-side3)*(s3-c)*(s3-side1));
		areaABC = Math.sqrt(sall*(sall-a)*(sall-b)*(sall-c)); 
		if(Math.abs(areaABC-areaPAB - areaPBC - areaPCA) <=0.5 )
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	//if new triangle is in old one returen true	
	public boolean contains(Triangle2D input)
	{
		boolean check1 = false,check2 = false,check3 = false;
		if(contains(input.getP1_new()))
		{
			check1 = true;
		}
		if(contains(input.getP2_new()))
		{
			check2 = true;
		}
		if(contains(input.getP3_new()))
		{
			check3 = true;
		}
		if(check1 && check2 && check3)
		{
			return true;
		}
		else
		{
			return false;
		}	
	}
}



