/**
 * CE1002-100502514 FlashTeens Chiang
 * Class Triangle2D required in Assignment 4-2
 * Methods:
	+Triangle2D(MyPoint p1, MyPoint p2, MyPoint p3):
		Constructor that creates a triangle.
	+getArea():
		returns the area of the new triangle.
	+getPerimeter():
		returns the perimeter of the new triangle.
	+contains(double x, double y):
	+contains(MyPoint p):
		returns true if the specified point p(x,y) is inside the original triangle.
	+contains(Triangle2D t):
		returns true if the specified triangle is inside the original triangle.
	
	[Outer class only below:]
	+getOriginalTriangle():
		to access the original triangle and return as class Triangle2D

 * [NOTICE] This class just calls class InnerTriangle2D's function because
			there are 2 triangles in this class.

 */
package a4.s100502514;

public class Triangle2D {
	/** Private InnerTriangles */
	private InnerTriangle2D originalTriangle, newTriangle;
	
	/** Constructor with 3 points */
	public Triangle2D(MyPoint p1, MyPoint p2, MyPoint p3){
		originalTriangle = new InnerTriangle2D(
				new MyPoint(0,0), new MyPoint(10,15), new MyPoint(17,6)
				);
		newTriangle = new InnerTriangle2D(p1, p2, p3);
	}
	/** Cloning Constructor */
	public Triangle2D(Triangle2D t){
		this(t.getMyPoint(0), t.getMyPoint(1), t.getMyPoint(2));
	}
	/** Constructor with an inner triangle */
	public Triangle2D(InnerTriangle2D t){
		this(t.getMyPoint(0), t.getMyPoint(1), t.getMyPoint(2));
	}
	
	/** Get the point by index */
	public MyPoint getMyPoint(int index){
		return newTriangle.getMyPoint(index);
	}
	
	/** Get the triangle area by cross product */
	public double getArea(){
		return newTriangle.getArea();
	}
	
	/** Get the perimeter of the triangle */
	public double getPerimeter(){
		return newTriangle.getPerimeter();
	}
	
	/** Returns true if the specified point p(x,y) is inside the original triangle.
	 	Otherwise returns false.
	 	There are 3 syntaxes for method contains():
	 	Two are for point; one is for triangle, as parameter(s).
	 	(My own solution: See header comments in A42.java)
	 */
	/** The following two for point: */
	public boolean contains(double x, double y){
		return contains(new MyPoint(x, y));
	}
	public boolean contains(MyPoint p){
		return newTriangle.contains(p);
	}
	/** The following one for triangle: */
	public boolean contains(Triangle2D t){
		return newTriangle.contains(t.newTriangle);
	}
	public boolean contains(InnerTriangle2D t){
		return newTriangle.contains(t);
	}
	
	/** Get the original Triangle */
	public Triangle2D getOriginalTriangle(){
		return new Triangle2D(originalTriangle);
	}
}
