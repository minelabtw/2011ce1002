/**
 * CE1002-100502514 FlashTeens Chiang
 * Class MyPoint required in Assignment 4-2
 * Methods that is used in this Assignment:
	1. (constructor)
	2. double getX(), double getY()
	3. double crossProductZ(MyPoint another)
	4. double getDistance(MyPoint another)
	5. MyPoint minus(MyPoint another)
	(Other methods or syntax may be used afterwards.)
 */
package a4.s100502514;

public class MyPoint {
	/** Private variable members */
	private double xpos, ypos;
	
	/** Constructor with parameters x and y */
	public MyPoint(double x, double y){
		xpos = x;
		ypos = y;
	}
	
	/** Default Constructor sets the initial value (0,0) */
	public MyPoint(){
		this(0,0);
	}
	
	/** Reset the position */
	public void setPosition(double x, double y){
		xpos = x;
		ypos = y;
	}
	
	/** Move the point with relative (x,y) position */
	public void move(double x, double y){
		setPosition(xpos+x, ypos+y);
	}
	
	/** Move the point toward(+)/backward(-) another point (by ratio and distance) */
	public void moveTowardByRatio(MyPoint another, double ratio){
		/** If 2 points are at the same position, then omit. */
		if(getDistance(another)==0)return;
		
		MyPoint deltaVector = another.minus(this);
		move(deltaVector.getX()*ratio, deltaVector.getY()*ratio);
	}
	public void moveTowardByDistance(MyPoint another, double distance){
		/** Instead of "double distance" for the movement,
			"double dist" gets the distance between 2 points */
		double dist = getDistance(another);
		/** If 2 points are at the same position, then omit. */
		if(dist==0)return;
		
		//Call the previous method
		moveTowardByRatio(another, distance/dist);
	}
	
	/** Get the X position */
	public double getX(){
		return xpos;
	}
	
	/** Get the Y position */
	public double getY(){
		return ypos;
	}
	
	/** Get the addition of 2 vectors */
	public MyPoint plus(MyPoint another){
		return new MyPoint(xpos+another.getX(), ypos+another.getY());
	}
	
	/** Get the subtraction of 2 vectors */
	public MyPoint minus(MyPoint another){
		return new MyPoint(xpos-another.getX(), ypos-another.getY());
	}
	
	/** Get the dot product of 2 vectors */
	public double dotProduct(MyPoint another){
		return xpos*another.getX()+ypos*another.getY();
	}
	
	/** Get the cross product of 2 vectors */
	/** This method just returns a double for the "Z position"
	 	because both 2 points crossing-product at the x-y 2D plain gets (0,0,z) */
	public double crossProductZ(MyPoint another){
		/**
			| x1 y1 0 |
			| x2 y2 0 | gets (0, 0, x1*y2-x2*y1)
			| i  j  k |
		 */
		return xpos*another.getY()-another.getX()*ypos;
	}
	
	/** Get the distance from another point (3 syntaxes) */
	public double getDistance(double x, double y){
		double deltaX = xpos-x, deltaY = ypos-y;
		return Math.sqrt(deltaX*deltaX+deltaY*deltaY);
	}
	public double getDistance(MyPoint another){
		return getDistance(another.getX(), another.getY());
	}
	public double getDistance(){
		/** Default: the distance to the origin(0,0) */
		return getDistance(0,0);
	}
	
	/** Get the String of this point for display */
	public String toString(){
		return "("+Math.rint(getX()*1000)/1000+", "+Math.rint(getY()*1000)/1000+")";
	}
}
