/** Modified from class A22_EquationPane in Assignment 2-2 */
/** Change the circle into Triangle */
package a4.s100502514;

import java.awt.*;

import javax.swing.*;

/** Declare another class for equation pane */
public class A42_EquationPane extends JPanel{
	
	private Triangle2D T0, T1, T2;
	private MyPoint P;
	
	/** Constructor with default initialization */
	public A42_EquationPane(){
		initialize(null, null, null);
	}
	
	/** Constructor with specified initialization */
	public A42_EquationPane(Triangle2D t1, Triangle2D t2, MyPoint p){
		initialize(t1, t2, p);
	}
	
	/** Initialization method */
	public void initialize(Triangle2D t1, Triangle2D t2, MyPoint p){
		T0=(new Triangle2D(p,p,p)).getOriginalTriangle();
		T1=t1;
		T2=t2;
		P=p;
		repaint();
	}
	
	/** The method to paint this component (Overriden the default method) */
	protected void paintComponent(Graphics g){
		//Call the original painting method
		super.paintComponent(g);
		
		setBackground(new Color(255,240,224));
		
		//Draw 3 triangles
		for(int tn=0;tn<3;tn++){
			Triangle2D T;
			switch(tn){
			case 0:
				//Initial Triangle
				T = T0;
				g.setColor(new Color(0, 51, 102));
				break;
			case 1:
				//New Triangle 1
				T = T1;
				g.setColor(new Color(255, 51, 102));
				break;
			default:
				//New Triangle 2
				T = T2;
				g.setColor(new Color(0, 153, 255));
			}
			
			//Skip a phase in this loop if the triangle is null.
			//Namely, the triangle won't be drawn.
			if(T==null)continue;
			
			int[] Xs=new int[3], Ys=new int[3];
			for(int pn=0;pn<3;pn++){
				Xs[pn] = 150 + (int)(15*T.getMyPoint(pn).getX());
				Ys[pn] = getHeight()-40 - (int)(15*T.getMyPoint(pn).getY());
				if(tn>=1){
					//Draw Points A, B, C on New Triangle
					g.fillOval(Xs[pn]-3, Ys[pn]-3, 6, 6);
					g.setFont(new Font("Comic Sans MS",0,16));
					g.drawString(""+(char)('A'+(tn-1)*3+pn), Xs[pn], Ys[pn]);
				}
			}
			//Draw the triangle
			g.drawPolygon(Xs, Ys, 3);
		}
		
		if(P!=null){
			//Adjust the position on the display
			int px = 150 + (int)(15*P.getX());
			int py = getHeight()-40 - (int)(15*P.getY());
			//Draw Point P if not null
			g.setColor(new Color(153, 0, 255));
			g.fillOval(px-3, py-3, 6, 6);
			g.setFont(new Font("Comic Sans MS",0,16));
			g.drawString("P", px, py);
		}
		
	}
	
}
