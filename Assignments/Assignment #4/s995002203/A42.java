package a4.s995002203;
import java.util.Scanner;

public class A42 {
    public static void main(String[] args) throws Exception{
    	int flag=1, choice;
    	double []input=new double [6];
    	Scanner scanner = new Scanner(System.in);
    	System.out.println("Please enter three points of the new triangle:");
        for(int i=0;i<6;i++)//user input
            input[i]=scanner.nextDouble();
        Triangle2D t1=new Triangle2D(input[0],input[1],input[2],input[3],input[4],input[5]);
        //Triangle2D t1=new Triangle2D(8,6,12,7,11,14);
        while(flag==1){
            System.out.println("1.get the area of the new triangle\n" +
            				   "2.get the perimeter of the new triangle\n" +
            		           "3.check the new triangle is in the original triangle or not\n" +
            		           "4.enter a point and check the point is in the original triangle or not\n" +
            		           "5.Exit");
           choice=scanner.nextInt();
            switch(choice)
            {
            	case 1:
            		System.out.println("The area of the new triangle is "+t1.getArea()); 
            		break;
            	case 2:
            		System.out.println("The perimeter of the new triangle is "+t1.getPerimeter());
            		break;
            	case 3:
            		System.out.println("Please input a new triangle for the new object: ");
                    for(int i=0;i<6;i++)//user input
                		input[i]=scanner.nextDouble();
                    Triangle2D t2=new Triangle2D(input[0],input[1],input[2],input[3],input[4],input[5]);
                    if(t1.contains(t2))
                    	System.out.println("The new triangle of new object is in the original triangle of the old object!!!");
                    else
                    	System.out.println("The new triangle of new object is NOT in the original triangle of the old object!!!");
            		break;
            	case 4:
            		System.out.println("enter 2 numbers: ");
            		double num1=scanner.nextDouble();
            		double num2=scanner.nextDouble();
            		MyPoint p1=new MyPoint(num1, num2);
            		if(t1.contains(p1))
                    	System.out.println("The new triangle of new object is in the original triangle of the old object!!!");
                    else
                    	System.out.println("The new triangle of new object is NOT in the original triangle of the old object!!!");
            		break;
            	case 5:
            		flag=0;
            		break;
            	default:
            		break;
            }
    	}
    }
}
