package a4.s995002203;

public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0, 0.0); // points of original triangle triangle
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint p1_new=new MyPoint();
	private MyPoint p2_new=new MyPoint();
	private MyPoint p3_new=new MyPoint();

	public MyPoint getP1_new(){ // get the p1 of new triangle
		return p1_new;
	}

	public MyPoint getP2_new(){ // get the p2 of new triangle
		return p2_new;
	}

	public MyPoint getP3_new(){ // get the p3 of new triangle
		return p3_new;
	}

	// constructor with arguments to initial the points of the new triangle
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3){
		p1_new.setX(x1);//呼叫MyPoint的setX函式
		p1_new.setY(y1);
		p2_new.setX(x2);
		p2_new.setY(y2);
		p3_new.setX(x3);
		p3_new.setY(y3);
	}

	public double getArea(){ // calculate the area of the new triangle
		double result;
		double s,s1,s2,s3;
		s1=p1_new.distance(p2_new);
		s2=p2_new.distance(p3_new);
		s3=p3_new.distance(p1_new);
		s=(s1+s2+s3)/2;
		result=Math.sqrt(s*(s-s1)*(s-s2)*(s-s3));
		//result=((p2_new.getX()-p1_new.getX())*(p3_new.getY()-p1_new.getY()))-((p2_new.getY()-p1_new.getY())*(p3_new.getX()-p1_new.getX()))/2;//行列式
		//if(result<0)//取絕對值
			//result=-result;
		return result;	
	}

	public double getPerimeter(){ // calculate the perimeter of the new triangle
		double result;
		result=p1_new.distance(p2_new)+p2_new.distance(p3_new)+p3_new.distance(p1_new);//用MyPoint的距離公式
		return result;
	}

	// check the input point is in the original triangle of the object or not
	public boolean contains(MyPoint p){
		// if input point p is in the triangle, area of PAB + area of PBC + area of PCA = area of ABC 
		double areaPAB, areaPBC, areaPCA, areaABC,s,s1,s2,s3,ab,ac,bc,ap,bp,cp;
		ab=p1.distance(p2);
		bc=p2.distance(p3);
		ac=p3.distance(p1);
		ap=p1.distance(p);
		bp=p2.distance(p);
		cp=p3.distance(p);
		s1=(ab+ap+bp)/2;
		s2=(bc+bp+cp)/2;
		s3=(ac+ap+cp)/2;
		s=(ab+bc+ac)/2;
		areaPAB=Math.sqrt(s1*(s1-ab)*(s1-ap)*(s1-bp));
		areaPBC=Math.sqrt(s2*(s2-bc)*(s2-bp)*(s2-cp));
		areaPCA=Math.sqrt(s3*(s3-ac)*(s3-ap)*(s3-cp));
		areaABC=Math.sqrt(s*(s-ab)*(s-bc)*(s-ac));
		if((areaPAB+areaPBC+areaPCA)!=areaABC)
			return false;
		else 
			return true;
	}

	// check the input triangle(Triangle2D object) is in the original triangle of this object or not
	public boolean contains(Triangle2D input){
		if(this.contains(input.p1_new))//若三點皆在原三角形內
			if(this.contains(input.p2_new))
				if(this.contains(input.p3_new))
					return true;//回傳true
		return false;
					
	       // if the input triangle is in the original triangle of this object
               // then all points of the input triangle is in the original triangle of this object
	}
}
