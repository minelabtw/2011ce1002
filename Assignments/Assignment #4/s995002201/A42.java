package a4.s995002201;
/*8 6 12 7 11 14
 *14.500000000000014 
 *19.73817718280067*/
import java.util.Scanner;

public class A42 
{
	public static void main ( String[] args )
	{
		Triangle2D triangle = new Triangle2D();
		Scanner input = new Scanner(System.in);
		double x1,y1,x2,y2,x3,y3,p1,p2,choice;
		System.out.println("Please enter three points of the new triangle:");
		x1 = input.nextDouble();
		y1 = input.nextDouble();
		x2 = input.nextDouble();
		y2 = input.nextDouble();
		x3 = input.nextDouble();
		y3 = input.nextDouble();
		
		System.out.println("1.get the area of the new triangle");
		System.out.println("2.get the perimeter of the new triangle");
		System.out.println("3.check the new triangle is in the original triangle or not");
		System.out.println("4.enter a point and check the point is in the original triangle or not");
		System.out.println("5.Exit");
		choice = input.nextDouble();
		do
		{
			if(choice==1)
			{
				System.out.print("The area of the new triangle is "+triangle.getArea(x1, y1, x2, y2, x3, y3));
			}
			if(choice==2)
			{
				System.out.print("The perimeter of the new triangle is "+triangle.getPerimeter(x1, y1, x2, y2, x3, y3));
			}
			if(choice==3)
			{
				if(triangle.contains(triangle)==true)
					System.out.print("The new triangle is in the original triangle!!!");
				else
					System.out.print("The new triangle is NOT in the original triangle!!!");
			}
			if(choice==4)
			{
				p1 = input.nextDouble();
				p2 = input.nextDouble();
				if(triangle.contains(p1,p2)!=true)
					System.out.print("The new point is in the original triangle!!!");
				else
					System.out.print("The new point is NOT in the original triangle!!!");
			}
			choice = input.nextDouble();
		}while(choice!=5);
	}
}
