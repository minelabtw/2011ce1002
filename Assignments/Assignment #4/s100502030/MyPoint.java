package a4.s100502030;

public class MyPoint {
	private double x;
	private double y;

	public MyPoint() {
		x = 0;
		y = 0;
	}

	public MyPoint(double x_coordinate, double y_coordinate) {
		setX(x_coordinate);
		setY(y_coordinate);
	}

	public void setX(double x_coordinate) // set x value
	{
		x = x_coordinate;
	}

	public void setY(double y_coordinate) // set y value
	{
		y = y_coordinate;
	}

	public double getX() { // return x value
		return x;
	}

	public double getY() { // return y value
		return y;
	}

	public double distance(MyPoint temp) {
		return Math.sqrt((temp.getX() - x) * (temp.getX() - x)
				+ (temp.getY() - y) * (temp.getY() - y));
	}
}
