package a4.s100502030;

import java.util.*;

public class A42 {
	public static void main(String args[]) {
		Scanner input = new Scanner(System.in);
		System.out.println("Please enter three points of the new triangle: ");
		double Ax = input.nextDouble();
		double Ay = input.nextDouble();
		double Bx = input.nextDouble();
		double By = input.nextDouble();
		double Cx = input.nextDouble();
		double Cy = input.nextDouble();// 輸入三角形之三點
		Triangle2D triangle = new Triangle2D(Ax, Ay, Bx, By, Cx, Cy);
		System.out.println("Function: ");
		System.out.println("1.get the area of the new triangle");
		System.out.println("2.get the perimeter of the new triangle");
		System.out
				.println("3.check the new triangle is in the original triangle or not");
		System.out
				.println("4.enter a point and check the point is in the original triangle or not");
		System.out.println("5.Exit");// 顯示功能
		while (true) {
			int choice = input.nextInt();
			if (choice == 1)
				System.out.println("The area of the new triangle is "
						+ triangle.getArea());// 顯示面積
			if (choice == 2)
				System.out.println("The perimeter of the new triangle is "
						+ triangle.getPerimeter());// 顯示周長
			if (choice == 3) {
				System.out
						.println("Please input a new triangle for the new object:");
				double nAx = input.nextDouble();
				double nAy = input.nextDouble();
				double nBx = input.nextDouble();
				double nBy = input.nextDouble();
				double nCx = input.nextDouble();
				double nCy = input.nextDouble();// 輸入另一個三角形之三點
				Triangle2D nTriangle = new Triangle2D(nAx, nAy, nBx, nBy, nCx,
						nCy);
				// 判斷是否在Triangle2D之內建三角形裡
				if (triangle.contains(nTriangle))
					System.out
							.println("The new triangle of new object is in the original triangle of the old object!");
				else
					System.out
							.println("The new triangle of new object is Not in the original triangle of the old object!");
			}
			if (choice == 4) {
				System.out.println("Please input a point:");
				double Px = input.nextDouble();
				double Py = input.nextDouble();
				MyPoint point = new MyPoint(Px, Py);
				// 判斷是否在Triangle2D之內建三角形裡
				if (triangle.contains(point))
					System.out
							.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!");
				else
					System.out
							.println("The new point is Not in the original triangle (0,0), (17,6), (10, 15)!");
			}
			if (choice == 5) {
				System.out.println("Thank for your use !");
				System.exit(0);// 離開
			}

		}

	}
}
