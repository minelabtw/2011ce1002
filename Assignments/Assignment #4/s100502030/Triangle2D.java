package a4.s100502030;

public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0, 0.0); // points of original triangle
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint p1_new, p2_new, p3_new; // points of new triangle

	public MyPoint getP1_new() { // get the p1 of new triangle
		return p1_new;
	}

	public MyPoint getP2_new() { // get the p2 of new triangle
		return p2_new;
	}

	public MyPoint getP3_new() { // get the p3 of new triangle
		return p3_new;
	}

	// constructor with arguments to initial the points of the new triangle
	public Triangle2D(double x1, double y1, double x2, double y2, double x3,
			double y3) {
		p1_new = new MyPoint(x1, y1);
		p2_new = new MyPoint(x2, y2);
		p3_new = new MyPoint(x3, y3);

	}

	public double getArea() { // calculate the area of the new triangle
		double a = p1_new.distance(p2_new);
		double b = p2_new.distance(p3_new);
		double c = p3_new.distance(p1_new);
		double s = (a + b + c) / 2;
		return Math.sqrt(s * (s - a) * (s - b) * (s - c));
	}

	public double getPerimeter() { // calculate the perimeter of the new triangle
		return p1_new.distance(p2_new) + p2_new.distance(p3_new)
				+ p3_new.distance(p1_new);
	}

	// check the input point is in the original triangle of the object or not
	public boolean contains(MyPoint p) {
		LinearEquation conditions = new LinearEquation((p2.getX() - p1.getX()),
				(p3.getX() - p1.getX()), (p2.getY() - p1.getY()),
				(p3.getY() - p1.getY()), p.getX(), p.getY());
		// 利用 向量 AP=i向量 AB+j向量 AC
		// i+j=1 --> 在BC上
		// i+j<=0,i>=0,j>=0 --> P在三角形ABC內
		if (conditions.getX() >= 0 && conditions.getY() >= 0
				&& conditions.getX() + conditions.getY() <= 1)
			return true;
		else
			return false;
	}

	// check the input triangle(Triangle2D object) is in the original triangle
	// of this object or not
	public boolean contains(Triangle2D t) {
		if (t.contains(t.getP1_new()) && t.contains(t.getP2_new())
				&& t.contains(t.getP3_new()))
			return true;
		else
			return false;
		// if the input triangle is the original triangle of this object
		// then all points of the input triangle is in the original triangle of
		// this object
	}
}
