package a4.s100502033;

public class MyPoint 
{
	private double x ;
	private double y ;
	public MyPoint(double x_input, double y_input) //constructor to initial private data member
	{
		setX(x_input);
		setY(y_input); //
	}
	public double getX()
	{
		return x;
	}
	public double getY()
	{
		return y;
	}
	public void setX(double input)
	{
		x = input;
	}
	public void setY(double input)
	{
		y = input;
	}
	public double distance(MyPoint p)//計算兩點間的距離
	{
		
		return Math.sqrt(Math.pow((p.getX() - x ) , 2) +  Math.pow((p.getY() - y) , 2));//兩點間距離公式
	}
}
