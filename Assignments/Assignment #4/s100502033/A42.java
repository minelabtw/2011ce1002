package a4.s100502033;
import javax.swing.*;

import java.io.*;
public class A42
{
	public static void main(String[] args)
	{
		String number = JOptionPane.showInputDialog("請輸入X1的值");
		double x1 = Double.parseDouble(number);
		String number1 = JOptionPane.showInputDialog("請輸入Y1的值");
		double y1 = Double.parseDouble(number1);
		String number2 = JOptionPane.showInputDialog("請輸入X2的值");
		double x2 = Double.parseDouble(number2);
		String number3 = JOptionPane.showInputDialog("請輸入Y2的值");
		double y2 = Double.parseDouble(number3);
		String number4 = JOptionPane.showInputDialog("請輸入X3的值");
		double x3 = Double.parseDouble(number4);
		String number5 = JOptionPane.showInputDialog("請輸入Y3的值");
		double y3 = Double.parseDouble(number5);
		Triangle2D newtriangle = new Triangle2D(x1 , y1 , x2 , y2 , x3 , y3);
		System.out.println("Please enter three points of the new triangle:");
		newtriangle.getP1_new();//呼叫CLASS
		newtriangle.getP2_new();//呼叫CLASS
		newtriangle.getP3_new();//呼叫CLASS
		for (int i = 0 ; i < 2 ; i--)
		{
			String number8 = JOptionPane.showInputDialog("請選擇一種功能:\n1.get the area of the new triangle\n2.get the perimeter of the new triangle\n3.check the new triangle is in the original triangle or not\n4.enter a point and check the point is in the original triangle or not\n5.Exit\n");
			int number9 = Integer.parseInt(number8);
			switch(number9)
			{
				case 1:
					System.out.println("The area of the new triangle is " + newtriangle.getArea() + "\n");
					break;
				case 2:
					System.out.println("The perimeter of the new triangle is " + newtriangle.getPerimeter() + "\n");
					break;
				case 3:
					String number10 = JOptionPane.showInputDialog("請輸入新的X1的值");
					double X1 = Double.parseDouble(number10);
					String number11 = JOptionPane.showInputDialog("請輸入新的Y1的值");
					double Y1 = Double.parseDouble(number11);
					String number12 = JOptionPane.showInputDialog("請輸入新的X2的值");
					double X2 = Double.parseDouble(number12);
					String number13 = JOptionPane.showInputDialog("請輸入新的Y2的值");
					double Y2 = Double.parseDouble(number13);
					String number14 = JOptionPane.showInputDialog("請輸入新的X3的值");
					double X3 = Double.parseDouble(number14);
					String number15 = JOptionPane.showInputDialog("請輸入新的Y3的值");
					double Y3 = Double.parseDouble(number15);
					System.out.println("Please input a new triangle for the new object:\n" + X1 + " " + Y1 + " " + X2 + " " + Y2 + " " + X3 + " " + Y3);
					Triangle2D newtriangle1 = new Triangle2D(X1 , Y1 , X2 , Y2 , X3 , Y3);
					newtriangle.contains(newtriangle1);//CALL CALSS
					if (newtriangle.contains(newtriangle1) == true)//判段新三角形的三個點是否在原三角形內
					{
						System.out.println("The new triangle of new object is in the original triangle of the old object!!!");
					}
					else
					{
						System.out.println("The new triangle of new object is NOT in the original triangle of the old object!!!");
					}
					System.out.println("");
					break;
				case 4:
					String number6 = JOptionPane.showInputDialog("請輸入新的點X的值");
					double x = Double.parseDouble(number6);//將STRING轉成DOUBLE
					String number7 = JOptionPane.showInputDialog("請輸入新的點Y的值");
					double y = Double.parseDouble(number7);//將STRING轉成DOUBLE
					System.out.println(x + " " + y);
					MyPoint p = new MyPoint(x , y);
					newtriangle.contains(p);//CALL CLASS
					if (newtriangle.contains(p) == true)//判段P點是否在原三角形內
					{
						System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!\n");
					}
					else
					{
						System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!\n");
					}
					break;
				case 5:
					System.exit(0);	//離開		
			}
		}	
	}
}
