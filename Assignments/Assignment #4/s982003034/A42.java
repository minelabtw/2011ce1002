package a4.s982003034;

import java.util.Scanner;

import javax.swing.JOptionPane;

public class A42 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		double[] tripts = new double [6]; // triangle points
		getdouble (6, tripts, "coordinates data");
		
		boolean loop = true;
		Triangle2D t1 = new Triangle2D (tripts [0], tripts [1], tripts [2], tripts [3], tripts [4], tripts [5]);
		
		
		// our main while loop
		while (loop) {
			// reset selection to zero
			int sel = 0;
			// reset input to null
			// this is to avoid if user inputs nothing and the program will do the previous command
			String input;
			
			// get user selection
			input = JOptionPane.showInputDialog(
					"1.Get the area of the new triangle\n"+
					"2.Get the perimeter of the new triangle\n"+
					"3.Check if new triangle is in the original triangle or not\n"+
					"4.Enter a point and check the point is in the original triangle or not\n"+
					"5.Exit"
					);
			
			// this is to manage exception occurred in input
			// basically, I just hate how the program throws NullPointerException
			// if I select "cancel" on the JOptionPane.showInputDialog above
			// so, if cancel is pressed (input == null), just exit
			if (input == null) sel = 5;
			else { // if there is something in the string
				// try to get an integer
				// handle error if it is not an integer (do nothing)
				try {
					sel = Integer.parseInt(input);
				} catch ( NumberFormatException e ) {
					sel = 0;
				}
			}
			
			switch (sel) {
			
			case 0: break; // do nothing, simply to overcome erroneous input
			
			case 1: // get new triangle area
				JOptionPane.showMessageDialog(null, "Area = " + t1.getArea() );
				break;
				
			case 2: // get new triangle parameter
				JOptionPane.showMessageDialog(null, "Perimeter = " + t1.getPerimeter() );
				break;
				
			case 3: // check if another triangle is inside original triangle
				getdouble (6, tripts, "new triangle coordinates data"); // here we simply recycle tripts
				Triangle2D t2= new Triangle2D (tripts [0], tripts [1], tripts [2], tripts [3], tripts [4], tripts [5]);
				if(t1.contains(t2)) JOptionPane.showMessageDialog(null, "The new triangle is contained in the original triangle");
				else JOptionPane.showMessageDialog(null, "The new triangle is not contained in the original triangle");
				break;
				
			case 4:
				getdouble (2, tripts, "new points coordinates data"); // another recycle of tripts
				if (t1.contains( (new MyPoint (tripts[0], tripts[2]) ) ) ) JOptionPane.showMessageDialog(null, "The point is contained in the original triangle");
				else JOptionPane.showMessageDialog(null, "The point is not contained in the original triangle");
				break;
				
			case 5:
				loop = false;
				JOptionPane.showMessageDialog(null, "Bye bye!");
				break;
				
				default:
					sel = 0;
					break;
				
			}
		}
	}

	// ask user for an amount of double
	// if the given input contain less than what "amount" required
	// the function will ask you for input again
	// however if you give just enough input, the function ends
	// if you give too much input, the function will ignore the excess input
	// the function though, does not check whether your container is
	// enough to contain all inputs or not
	// amount = number of inputs
	// container = the container of inputs
	// stuff = a simple message to tell the user what the input is
	public static void getdouble (int amount, double[] container, String stuff) {
		
		String input;
		for (int i = 0; i < amount;) {
			
			input = JOptionPane.showInputDialog("Please insert " + (amount-i) + " more of " + stuff);
			if (input == null) { // if user input nothing or cancel then exit
				JOptionPane.showMessageDialog(null, "Bye bye");
				System.exit(0);
			}
			Scanner sc = new Scanner (input);
			
			while (sc.hasNextDouble()) {
				container [i] = sc.nextDouble();
				i++;
				if (i == amount) break;
			}
			
			
		}
		
	}
}
