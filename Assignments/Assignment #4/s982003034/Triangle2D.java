package a4.s982003034;

public class Triangle2D {
	
	// use array to define the triangles, rather than individual points
	private MyPoint [] t2d_ori = { // original triangle
		new MyPoint (0.0, 0.0),
		new MyPoint (17.0, 6.0),
		new MyPoint (10.0, 15.0)
	};
	
	private MyPoint [] t2d_new = new MyPoint[3]; // new triangle
	
	// constructor with arguments to initial the points of the new triangle
	public Triangle2D(double x0, double y0, double x1, double y1, double x2, double y2){
		t2d_new[0] = new MyPoint (x0, y0);
		t2d_new[1] = new MyPoint (x1, y1);
		t2d_new[2] = new MyPoint (x2, y2);
	}
	
	public MyPoint[] get_new() { // get the points of new triangle
		return t2d_new;
	}
	
	public double calcPerimeter (MyPoint a, MyPoint b, MyPoint c) { // calculate the perimeter of three points
		// distance from a to b + distance from b to c + distance from c to a
		return a.distance(b) + b.distance(c) + c.distance(a);
	}
	
	public double calcArea (MyPoint a, MyPoint b, MyPoint c) { // calculate the area of three points
		double half = calcPerimeter(a, b, c) / 2; // half of the perimeter
		return Math.sqrt(
					(
						// half * (half - AB) * (half - BC) * (half - CA)
						(half)*
						(half - a.distance(b))*
						(half - b.distance(c))*
						(half - c.distance(a))
					)
				);
		
	}
	
	public double getPerimeter() { // calculate the perimeter of the new triangle
		// the perimeter of the triangle is
		// distance from p0 to p1 + distance from p1 to p2 + distance from p2 to p0
		return calcPerimeter (t2d_new[0], t2d_new[1], t2d_new[2]);
	}
	
	public double getArea() { // calculate the area of the new triangle
		return calcArea (t2d_new[0], t2d_new[1], t2d_new[2]);
	}
	
	// check the input point is in the original triangle of the object or not
	public boolean contains (MyPoint p) {
		// if input point p is in the triangle, area of PAB + area of PBC + area of PCA = area of ABC
		double areaPAB, areaPBC, areaPCA, area;
	
		// calculate area of PAB
		areaPAB = calcArea (p, t2d_ori[0], t2d_ori[1]);
		// calculate area of PBC
		areaPBC = calcArea (p, t2d_ori[1], t2d_ori[2]);
		// calculate area of PCA
		areaPCA = calcArea (p, t2d_ori[2], t2d_ori[0]);
		// calculate original triangle area
		area = calcArea (t2d_ori[0], t2d_ori[1], t2d_ori[2]);

		
		// if areaPAB + areaPBC + areaPCA = areaABC, then p is in the ABC
		// However, there is some error(誤差) in this calculation
		// So if the absolute value of (areaABC - (areaPAB + areaPBC + areaPCA)) is less than 0.5
		// we consider the point is inside the original triangle
		// you can use Math.abs(double) to calculate the absolute value

		if (Math.abs(areaPAB + areaPBC + areaPCA - area) <= 0.5) return true;
		else return false;

	}

	// check the input triangle(Triangle2D object) is in the original triangle of this object or not
	public boolean contains (Triangle2D input) {
		
		MyPoint[] temp = input.get_new();
		// check if the points of input triangle are all contained inside original rectangle
		if ( contains(temp[0]) && contains(temp[1]) && contains(temp[2]) ) return true;
		else return false;
		
	}
		
}
