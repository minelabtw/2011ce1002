package a4.s100502028;

public class MyPoint {
	private double x, y; // Declare private data member
	
	public MyPoint() { // A no-argument constructor that creates point (0,0)
		x = 0;
		y = 0;
	}

	public MyPoint(double xcoordinate, double ycoordinate) { // Constructor to initial private data member
		setX(xcoordinate);
		setY(ycoordinate);
	}

	public double getX() { // Return x value
		return x;
	}

	public double getY() { // Return y value
		return y;
	}
	
	public void setX(double input){ // Set x value
		x = input;
	}
	
	public void setY(double input){ // Set y value
		y = input;
	}
	
	public double distance(MyPoint p) { // Calculate the distance between this point and the input point
		return Math.sqrt((p.getX() - x) * (p.getX() - x) + (p.getY() - y) * (p.getY() - y));
	}
} // End class MyPoint