package a4.s100502028;

public class Triangle2D {
	// points of original triangle
	private MyPoint originalP1 = new MyPoint(0.0, 0.0);
	private MyPoint originalP2 = new MyPoint(10.0, 15.0);
	private MyPoint originalP3 = new MyPoint(17.0, 6.0);
	
	private MyPoint p1_new, p2_new, p3_new; // points of new triangle
	
	public MyPoint getP1_new(){ // get the p1 of new triangle
		return p1_new;
	}
	
	public MyPoint getP2_new(){ // get the p2 of new triangle
		return p2_new;
	}
	
	public MyPoint getP3_new(){ // get the p3 of new triangle
		return p3_new;
	}
	
	// constructor with arguments to initial the points of the new triangle
	public Triangle2D(double p1x , double p1y , double p2x , double p2y , double p3x , double p3y){
		p1_new = new MyPoint(p1x,p1y);
		p2_new = new MyPoint(p2x,p2y);
		p3_new = new MyPoint(p3x,p3y);
	}
	
	public double getArea(){ // calculate the area of the new triangle
		double s = getPerimeter() / 2;
		double area = Math.sqrt(s * (s - p1_new.distance(p2_new)) * (s - p1_new.distance(p3_new)) * (s - p2_new.distance(p3_new)));
		return area;
	}
	
	public double getPerimeter(){ // calculate the perimeter of the new triangle
		double perimeter = p1_new.distance(p2_new) + p1_new.distance(p3_new) + p2_new.distance(p3_new);
		return perimeter;
	}
	
	// check the input point is in the original triangle of the object or not
	public boolean contains(Triangle2D t){ 
		boolean result1 = contains(t.getP1_new());
		boolean result2 = contains(t.getP2_new());
		boolean result3 = contains(t.getP3_new());
		
		// if the input triangle is in the original triangle of this object             
		// then all points of the input triangle is in the original triangle of this object
		if(result1 == true && result2 == true && result3 == true)
			return true;
		else
			return false;
	}
	
	// check the input triangle(Triangle2D object) is in the original triangle of this object or not
	public boolean contains(MyPoint p){
		double areaABC, areaPAB ,  areaPBC, areaPCA;
		// calculate area of ABC
		double side1 = originalP1.distance(originalP2);
		double side2 = originalP1.distance(originalP3);
		double side3 = originalP2.distance(originalP3);
		double s = (side1 + side2 + side3) / 2;
		areaABC = Math.sqrt(s * (s - side1) * (s - side2) * (s - side3));
		
		// calculate area of PAB
		double side1PAB = originalP1.distance(originalP2);
		double side2PAB = originalP1.distance(p);
		double side3PAB = p.distance(originalP2);
		double sPAB = (side1PAB + side2PAB + side3PAB) / 2;
		areaPAB = Math.sqrt(sPAB * (sPAB - side1) * (sPAB - originalP1.distance(p)) * (sPAB - originalP2.distance(p)));
		
		// calculate area of PBC
		double side1PBC = originalP2.distance(originalP3);
		double side2PBC = originalP2.distance(p);
		double side3PBC = p.distance(originalP3);
		double sPBC = (side1PBC + side2PBC + side3PBC) / 2;
		areaPBC = Math.sqrt(sPBC * (sPBC - p.distance(originalP2)) * (sPBC - p.distance(originalP3)) * (sPBC - side3));
		
		// calculate area of PCA
		double side1PCA = originalP1.distance(originalP3);
		double side2PCA = originalP1.distance(p);
		double side3PCA = p.distance(originalP3);
		double sPCA = (side1PCA + side2PCA + side3PCA) / 2;
		areaPCA = Math.sqrt(sPCA * (sPCA - originalP1.distance(p)) * (sPCA - side2) * (sPCA - originalP3.distance(p)));
		
		// if areaPAB + areaPBC + areaPCA = areaABC, then p is in the ABC                
		// However, there is some error in this calculation                
		// So if the absolute value of (areaABC - (areaPAB + areaPBC + areaPCA)) is less than 0.5                
		// we consider the point is inside the original triangle and return true
		if(Math.abs(areaABC - (areaPAB + areaPBC + areaPCA)) < 0.5)
			return true;
		else
			return false;
		}
} // End class Triangle2D
