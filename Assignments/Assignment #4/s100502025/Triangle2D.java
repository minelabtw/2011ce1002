package a4.s100502025;

import java.lang.Math;

public class Triangle2D {	
	private MyPoint p1 = new MyPoint(0.0, 0.0);  //宣告一個MyPoint的object p1 ，再傳入兩個點進入constructor
	private MyPoint p2 = new MyPoint(17.0, 6.0);  //宣告一個MyPoint的object p2 ，再傳入兩個點進入constructor
	private MyPoint p3 = new MyPoint(10.0, 15.0);  //宣告一個MyPoint的object p3 ，再傳入兩個點進入constructor
	private MyPoint p1_new, p2_new, p3_new;  //宣告三個MyPoint的object p1_new, p2_new, p3_new
	
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3){  //
		p1_new = new MyPoint(x1,y1);
		p2_new = new MyPoint(x2,y2);
		p3_new = new MyPoint(x3,y3);
	}
	
	public MyPoint getP1_new(){  //得到p1的座標
		return p1_new;
	}
	
	public MyPoint getP2_new(){  //得到p2的座標
		return p2_new;
	}
	
	public MyPoint getP3_new(){  //得到p3的座標
		return p3_new;
	}
	
	public double getArea(){  //算新輸入的三角形面積
		double a = p1_new.distance(p2_new);  //第一個點到第二個點的距離
		double b = p2_new.distance(p3_new);  //第二個點到第三個點的距離
		double c = p3_new.distance(p1_new);  //第三個點到第一個點的距離
		
		double s = ( a + b + c ) / 2;

		double area = Math.pow(s*(s-a)*(s-b)*(s-c), 0.5);  //海龍公式算面積

		return area;
	}
	
	public double getPerimeter(){  //算新輸入的三角形面積
		double distance1 = p1_new.distance(p2_new);  //第一個點到第二個點的距離
		double distance2 = p2_new.distance(p3_new);  //第二個點到第三個點的距離
		double distance3 = p3_new.distance(p1_new);  //第三個點到第一個點的距離
		return ( distance1 + distance2 + distance3 );  //回傳周長
		
	}
	public boolean contains(MyPoint p){
		
		double side12, side23, side31, side_P1, side_P2, side_P3, s_of_123, s_of_P12 , s_of_P23, s_of_P31, areaP12, areaP23, areaP31, area123;
		side12 = p1.distance(p2);  //第一個點到第二個點的距離
		side23 = p2.distance(p3);  //第二個點到第三個點的距離
		side31 = p3.distance(p1);  //第三個點到第一個點的距離
		side_P1= p.distance(p1);  //輸入的P點到第一個點的距離
		side_P2= p.distance(p2);  //輸入的P點到第二個點的距離
		side_P3= p.distance(p3);  //輸入的P點到第三個點的距離
		
		s_of_123 = ( side12 + side23 + side31 ) / 2;
		s_of_P12 = ( side_P1 + side12 + side_P2 ) / 2;
		s_of_P23 = ( side_P2 + side23 + side_P3 ) / 2;
		s_of_P31 = ( side_P3 + side31 + side_P1 ) / 2;
		
		areaP12 = Math.pow(s_of_P12*(s_of_P12-side_P1)*(s_of_P12-side12)*(s_of_P12-side_P2), 0.5);  //海龍公式
		areaP23 = Math.pow(s_of_P23*(s_of_P23-side_P2)*(s_of_P23-side23)*(s_of_P23-side_P3), 0.5);
		areaP31 = Math.pow(s_of_P31*(s_of_P31-side_P3)*(s_of_P31-side31)*(s_of_P31-side_P1), 0.5);
		area123 = Math.pow(s_of_123*(s_of_123-side12)*(s_of_123-side23)*(s_of_123-side31), 0.5);
		
		if(area123 - (areaP12 + areaP23 + areaP31) <= 0.5 ){  //若p點在三角形內，則areaP12 + areaP23 + areaP31 = area123，但會有0.5的誤差
			return true;  //若p點在三角形內，回傳true
		}
		else{
			return false; //若p點不在三角形內，回傳false
		}		
	}
	
	public boolean contain(Triangle2D input){
		boolean P1_in_or_not_in_triangle = contains(input.p1_new);  //新三角形的p1點是否在舊三角形內
		boolean P2_in_or_not_in_triangle = contains(input.p2_new);  //新三角形的p2點是否在舊三角形內
		boolean P3_in_or_not_in_triangle = contains(input.p3_new);  //新三角形的p3點是否在舊三角形內
		if(P1_in_or_not_in_triangle == true && P2_in_or_not_in_triangle == true && P3_in_or_not_in_triangle == true){
			return true;  //若三點都在三角形內，回傳true
		}
		else{
			return false;  //其中一點沒有的話，回傳false
		}
	}
}
