package a4.s100502025;

import java.util.Scanner;

public class A42 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
	
		System.out.print("Please enter three points of the new triangle:");  //輸入三個座標點
		double X1 = input.nextDouble();
		double Y1 = input.nextDouble();
		
		double X2 = input.nextDouble();
		double Y2 = input.nextDouble();
		
		double X3 = input.nextDouble();
		double Y3 = input.nextDouble();
		
		Triangle2D Triangle1 = new Triangle2D(X1,Y1,X2,Y2,X3,Y3);  //將三個座標點傳入constructor
		
		
		boolean flag = true;  //判斷是否執行迴圈
		
		while(flag){
			System.out.println("1.get the area of the new triangle");
			System.out.println("2.get the perimeter of the new triangle");
			System.out.println("3.check the new triangle is in the original triangle or not");
			System.out.println("4.enter a point and check the point is in the original triangle or not");
			System.out.println("5.Exit");
			
			int choice = input.nextInt();
			
			switch(choice){
				case 1:  //計算新三角形面積
					double area = Triangle1.getArea();
					System.out.println("The area of the new triangle is " + area );
					break;
				case 2:  //計算新三角形周長
					double perimeter = Triangle1.getPerimeter();
					System.out.println("The perimeter of the new triangle is " + perimeter);
					break;
				case 3:  //判斷輸入的三角形有沒有在舊三角形裡面
					System.out.print("Please input a new triangle for the new object:");
					double x1 = input.nextDouble();
					double y1 = input.nextDouble();
					double x2 = input.nextDouble();
					double y2 = input.nextDouble();
					double x3 = input.nextDouble();
					double y3 = input.nextDouble();
					
					Triangle2D Triangle2 = new Triangle2D(x1,y1,x2,y2,x3,y3);
					if(Triangle1.contain(Triangle2) == true){  //在三角形裡面
						System.out.println("The new triangle of new object is in the original triangle of the old object!!!");
					}
					else{  //沒在三角形裡面
						System.out.println("The new triangle of new object is NOT in the original triangle of the old object!!!");
					}
					
					break;
				case 4:  //判斷點是否在舊三角形裡面
					System.out.print("Please input a new point:");
					double x = input.nextDouble();
					double y = input.nextDouble();
					MyPoint pp = new MyPoint(x , y);
	
					if(Triangle1.contains(pp) == false){  //沒有在三角形裡面
						System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!");
					}
					if(Triangle1.contains(pp) == true){  //有在三角形裡面
						System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!");
					}		
					break;
				case 5:  //離開
					flag = false;  
					break;
				default:  //輸入1~5以外的數
					System.out.println("You input an error number!!Try again!!");
					break;					
			}
		}
	}
}
