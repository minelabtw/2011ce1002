package a4.s100502025;

import java.lang.Math;

public class MyPoint {
	private double x;  //宣告private varible x
	private double y;  //宣告private varible x
	
	public MyPoint(){   //contructor
		
	}
	public MyPoint(double x_input, double y_input){   //contructor
		setX(x_input);
		setY(y_input);
	}
	public double getX(){ 
		return x;
	}
	public double getY(){ 
		return y;
	}
	public void setX(double input){  //將x值存入private中
		x = input;
	}
	public void setY(double input){  //將y值存入private中
		y = input;
	}
	public double distance(MyPoint p){ 
		double dis = Math.pow(Math.pow( x - p.getX(), 2 ) + Math.pow( y - p.getY(), 2 ), 0.5 );  //計算兩點間的距離
		return dis;  //回傳
	}
}


