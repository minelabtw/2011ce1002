package a4.s100502516;

import java.util.Scanner;

public class A4 {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);	
		
		boolean stop = false;//end of while
		
		System.out.print("Please enter three points of the new triangle: ");
		
		MyPoint p1 = new MyPoint(input.nextDouble(), input.nextDouble());//triangle1 points
		MyPoint p2 = new MyPoint(input.nextDouble(), input.nextDouble());
		MyPoint p3 = new MyPoint(input.nextDouble(), input.nextDouble());
		
		Triangle2D triangle1 = new Triangle2D(p1, p2, p3);		
		
			System.out.print("\n1.get the area of the new triangle" +
					"\n2.get the perimeter of the new triangle" +
					"\n3.check the new triangle is in the original triangle or not" +
					"\n4.enter a point and check the point is in the original triangle or not" +
					"\n5.Exit");	
		while(!stop)
		{			
			System.out.print("\n\nChose: ");
			int choose = input.nextInt();
			
			while(choose < 1 || choose > 5)
			{
				System.out.print("Invalid enter, try again: ");
				choose = input.nextInt();
			}
			
			switch(choose)
			{
				case 1:
					System.out.println("The area of the new triangle is " + triangle1.getArea() + "\n");
					break;
				case 2:
					System.out.println("The perimeter of the new triangle is " + triangle1.getPerimeter() + "\n");
					break;
				case 3:
					System.out.print("Please input a new triangle for the new object: ");
					MyPoint t1 = new MyPoint(input.nextDouble(), input.nextDouble());//triangle2 points
					MyPoint t2 = new MyPoint(input.nextDouble(), input.nextDouble());
					MyPoint t3 = new MyPoint(input.nextDouble(), input.nextDouble());
					Triangle2D triangle2 = new Triangle2D(t1, t2, t3);
					if(triangle1.contains(triangle2))
						System.out.println("The new triangle of new object is in the original triangle of the old object!!!");
					else
						System.out.println("The new triangle of new object is NOT in the original triangle of the old object!!!");
					break;
				case 4:
					System.out.print("Enter a point: ");
					MyPoint p = new MyPoint(input.nextDouble(), input.nextDouble());
					if(triangle1.contains(p))
						System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!");
					else
						System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!");
					break;
				case 5:
					stop = true;
					break;
			}
		}
	}
}
