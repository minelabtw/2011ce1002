package a4.s100502516;

public class MyPoint {
	private double x;
	private double y;
	
	public MyPoint()//no argument constructor
	{
		
	}
	
	public MyPoint(double X, double Y)
	{
		x = X;
		y = Y;
	}
	
	public double getx()
	{
		return x;
	}
	
	public double gety()
	{
		return y;
	}
	
	public void setx(double X)
	{
		x = X;
	}
	
	public void sety(double Y)
	{
		y = Y;
	}
	
	public double distance(MyPoint input)//calculate the distance between two point
	{
		return Math.sqrt(Math.pow(x - input.getx(), 2) + Math.pow(y - input.gety(), 2));
	}
}
