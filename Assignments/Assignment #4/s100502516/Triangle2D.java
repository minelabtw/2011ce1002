package a4.s100502516;

public class Triangle2D {
	private MyPoint d1 = new MyPoint(0,0);//original triangle points
	private MyPoint d2 = new MyPoint(10,15);
	private MyPoint d3 = new MyPoint(17,6);
	private MyPoint p1, p2, p3;	//new triangle points
		
	public Triangle2D(MyPoint P1, MyPoint P2, MyPoint P3)
	{
		p1 = P1;
		p2 = P2;
		p3 = P3;
	}
	
	public MyPoint getP1()
	{
		return p1;
	}
	
	public MyPoint getP2()
	{
		return p2;
	}
	
	public MyPoint getP3()
	{
		return p3;
	}
	
	public double getArea()//get new triangle area
	{
		double s = (p1.distance(p2) + p2.distance(p3) + p3.distance(p1)) / 2;
		
		return Math.sqrt(s * (s - p1.distance(p2)) * (s - p2.distance(p3)) * (s - p3.distance(p1)));
	}
	
	public double getAreaOld()//get original triangle area
	{
		double s = (d1.distance(d2) + d2.distance(d3) + d3.distance(d1)) / 2;
		
		return Math.sqrt(s * (s - d1.distance(d2)) * (s - d2.distance(d3)) * (s - d3.distance(d1)));
	}
	
	public double getPerimeter()//get new triangle perimeter
	{
		return p1.distance(p2) + p2.distance(p3) + p3.distance(p1);
	}	
	
	public boolean contains(MyPoint p)//check the point is in the original triangle or not
	{
		double PAB_s = (d1.distance(d2) + d2.distance(p) + p.distance(d1)) / 2;
		double PBC_s = (d2.distance(d3) + d3.distance(p) + p.distance(d2)) / 2;
		double PCA_s = (d1.distance(d3) + d3.distance(p) + p.distance(d1)) / 2;
		
		double PAB = Math.sqrt(PAB_s * (PAB_s - d1.distance(d2)) * (PAB_s - d2.distance(p)) * (PAB_s - p.distance(d1)));
		double PBC = Math.sqrt(PBC_s * (PBC_s - p.distance(d2)) * (PBC_s - d2.distance(d3)) * (PBC_s - d3.distance(p)));
		double PCA = Math.sqrt(PCA_s * (PCA_s - d1.distance(p)) * (PCA_s - p.distance(d3)) * (PCA_s - d3.distance(d1)));
		
		if(PAB + PBC + PCA - getAreaOld() <= 0.5 && PAB + PBC + PCA - getAreaOld() >= -0.5)
			return true;
		else
			return false;
	}
	
	public boolean contains(Triangle2D input)//check input new triangle is in the original triangle or not
	{
		if(contains(input.getP1()) && contains(input.getP2()) && contains(input.getP1()))
			return true;
		else
			return false;
	}
}
