package a4.s100502004;

public class MyPoint {
	private double x;
	private double y;
	
	public MyPoint(){//the constructor do not catch the input
			
	}
	public MyPoint(double x,double y){//the constructor catch the input
		this.x=x;
		this.y=y;		
	}
	
	public void setX(double x){//set the point
		this.x=x;
	}
	public void setY(double y){
		this.y=y;
	}
	
	public double getX(){//get the point
		return x;
	}
	public double getY(){
		return y;
	}
	
	public 	double distance(MyPoint p){//to calculate the distance between two point
		double howlong=Math.sqrt((p.getX()-x)*(p.getX()-x)+(p.getY()-y)*(p.getY()-y));
		return howlong;
	}
	
	
	
}
