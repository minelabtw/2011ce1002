package a4.s100502004;
import java.util.Scanner;


public class A42 {
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);
		double [] uinput=new double[6];//create three array to store the numbers you put
		double [] newuinput=new double[6];
		double [] point=new double[2];
		System.out.println("Please enter three points of the new triangle:");
		for(int i=0;i<6;i++){//input your numbers
			uinput[i]=input.nextDouble();
			
		}		
		Triangle2D t=new Triangle2D(uinput[0],uinput[1],uinput[2],uinput[3],uinput[4],uinput[5]);//create a new object	
		
		while(true){//choose the method you want
			System.out.println("1.get the area of the new triangle"+"\n"+"2.get the perimeter of the new triangle"+"\n"+"3.check the new triangle is in the original triangle or not"+"\n" +"4.enter a point and check the point is in the original triangle or not"+"\n"+"5.Exit");
			int choose=input.nextInt();
			
			switch(choose){
				case 1:
					System.out.println("The area of the new triangle is "+t.getArea());
					System.out.println();
					break;
			
				case 2:
					System.out.println("The perimeter of the new triangle is "+t.getPerimeter());
					System.out.println();
					break;
				
				case 3:
					System.out.println("Please input a new triangle for the new object: ");					
					for(int i=0;i<6;i++){
						double number=input.nextDouble();
						newuinput[i]=number;
					}					
					Triangle2D newt=new Triangle2D(newuinput[0],newuinput[1],newuinput[2],newuinput[3],newuinput[4],newuinput[5]);
					newt.contains(newt);
					System.out.println();
					break;
				
				case 4:
					System.out.println("Input your point: ");
					Triangle2D newp=new Triangle2D();
					MyPoint newPoint=new MyPoint();
					for(int i=0;i<2;i++){
						double number=input.nextDouble();
						point[i]=number;
					}
					newPoint.setX(point[0]);
					newPoint.setY(point[1]);
					if(newp.contains(newPoint)){
						System.out.println("The new point is IN the original triangle (0,0), (17,6), (10, 15)!!! ");
					}
					else{
						System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!");
					}
					System.out.println();
					break;
					
				case 5: 
					System.out.println("Goodbye!");//break the for loop
					return;
					
					
					
					
			}
			
			
			
		}
		
		
	}
	
	
	
	
	
	
	
}
