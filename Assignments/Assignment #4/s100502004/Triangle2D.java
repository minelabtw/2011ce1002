package a4.s100502004;

public class Triangle2D {
	MyPoint p1=new MyPoint(0,0);//set the original triangle 
	MyPoint p2=new MyPoint(17,6);
	MyPoint p3=new MyPoint(10,15);	
	private MyPoint p1_new, p2_new, p3_new;
	boolean check=false;
	boolean check1=false;
	boolean check2=false;
	boolean check3=false;
	
	public Triangle2D(){
		
	}
	
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3){
		p1_new=new MyPoint(x1,y1);//set the new triangle
		p2_new=new MyPoint(x2,y2);
		p3_new=new MyPoint(x3,y3);
		p1_new.setX(x1);//set the point
		p1_new.setY(y1);
		p2_new.setX(x2);
		p2_new.setY(y2);
		p3_new.setX(x3);
		p3_new.setY(y3);
		
	}
	
	public MyPoint getP1_new(){ 
		return p1_new;
	}
	
	public MyPoint getP2_new(){ 
		return p2_new;
	}
	
	public MyPoint getP3_new(){ 
		return p3_new;
	}
	
	public double getArea(){//the method to get the truangle's area
		double s=(p1_new.distance(p2_new)+p1_new.distance(p3_new)+p2_new.distance(p3_new))/2;
		double area=Math.sqrt(s*(s-p1_new.distance(p2_new))*(s-p1_new.distance(p3_new))*(s-p2_new.distance(p3_new)));		
		return area;		
	}
	
	public double getPerimeter(){//the method to get the truangle's perimeter
		double Perimeter=p1_new.distance(p2_new)+p1_new.distance(p3_new)+p2_new.distance(p3_new);
		return Perimeter;
	}
	
	public boolean contains(MyPoint p){//to check the point if it is in the triangle	
		double s=(p1.distance(p2)+p1.distance(p3)+p2.distance(p3))/2;
		double area=Math.sqrt(s*(s-p1.distance(p2))*(s-p1.distance(p3))*(s-p2.distance(p3)));		
		double sPAB=(p.distance(p1)+p.distance(p2)+p1.distance(p2))/2;
		double aPAB=Math.sqrt(sPAB*(sPAB-p.distance(p1))*(sPAB-p.distance(p2))*(sPAB-p1.distance(p2)));
		double sPAC=(p.distance(p1)+p.distance(p3)+p1.distance(p3))/2;
		double aPAC=Math.sqrt(sPAC*(sPAC-p.distance(p1))*(sPAC-p.distance(p3))*(sPAC-p1.distance(p3)));
		double sPBC=(p.distance(p2)+p.distance(p3)+p2.distance(p3))/2;
		double aPBC=Math.sqrt(sPBC*(sPBC-p.distance(p2))*(sPBC-p.distance(p3))*(sPBC-p2.distance(p3)));
		if(Math.abs(area-(aPAB+aPAC+aPBC))<=0.5){			
			return true;
		}
		else{			
			return false;
		}
		
	}		
		
	public boolean contains(Triangle2D t){//to check the triangle if it is in the original triangle
		check1=t.contains(p1_new);
		check2=t.contains(p2_new);		
		check3=t.contains(p3_new);
		
		
		
		if(check1&&check2&&check3){
			System.out.println("The new triangle of new object is in the original triangle of the old object. ");	
			return true;
		}
		
		else{
			System.out.println("The new triangle of new object is not in the original triangle of the old object ");
			return false;
		}
		
		
	}
	
	
}