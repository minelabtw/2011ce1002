package a4.s100502501;
import java.lang.Math;
public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0, 0.0); // points of original triangle 
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint p1_new, p2_new, p3_new; // points of new triangle

	public MyPoint getP1_new(){ // get the p1 of new triangle
		return  p1_new;
	}
	public MyPoint getP2_new(){ // get the p2 of new triangle
		return p2_new;
	}
	public MyPoint getP3_new(){ // get the p3 of new triangle
		return p3_new;
	}
	// constructor with arguments to initialize the points of the new triangle
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3){
		p1_new=new MyPoint(x1,y1);
		p2_new=new MyPoint(x2,y2);
		p3_new=new MyPoint(x3,y3);
	}
	public double getArea(){ // calculate the area of the new triangle
		double s=(p1_new.distance(p2_new)+p2_new.distance(p3_new)+p3_new.distance(p1_new))/2;
		double area=Math.sqrt(s*(s-p1_new.distance(p2_new))*(s-p2_new.distance(p3_new))*(s-p3_new.distance(p1_new)));
		return area;
	}
	public double getPerimeter(){ // calculate the perimeter of the new triangle
		double peri=p1_new.distance(p2_new)+p2_new.distance(p3_new)+p3_new.distance(p1_new);
		return peri;
	}
	public boolean contains(MyPoint p){// check the input point is in the original triangle of the object or not
		// if input point p is in the triangle, area of PAB + area of PBC + area of PCA = area of ABC 
		double side1,side2,side3,s,ptoA,ptoB,ptoC,areaPAB,areaPBC,areaPCA,areaABC;
		side1=p1.distance(p2);
		side2=p2.distance(p3);
		side3=p3.distance(p1);
		s=(side1+side2+side3)/2;
		ptoA=p.distance(p1);
		ptoB=p.distance(p2);
		ptoC=p.distance(p3);
		double sPAB=(ptoA+ptoB+side1)/2;// calculate area of PAB
		areaPAB=Math.sqrt(sPAB*(sPAB-ptoA)*(sPAB-ptoB)*(sPAB-side1));
		double sPBC=(ptoC+ptoB+side2)/2;// calculate area of PBC
		areaPBC=Math.sqrt(sPBC*(sPBC-ptoC)*(sPBC-ptoB)*(sPBC-side2));
		double sPCA=(ptoC+ptoA+side3)/2;// calculate area of PCA
		areaPCA=Math.sqrt(sPCA*(sPCA-ptoC)*(sPCA-ptoA)*(sPCA-side3));
		areaABC=Math.sqrt(s*(s-side1)*(s-side2)*(s-side3));// calculate area of ABC

		if(Math.abs(areaPAB+areaPBC+areaPCA-areaABC)>0.5)
			return false;
		return true;
	}
	// check the input triangle(Triangle2D object) is in the original triangle of this object or not
	public boolean contains(Triangle2D input){
	    if(!(contains(input.getP1_new())&&contains(input.getP2_new())&&contains(input.getP3_new())))// if the input triangle is the original triangle of this object
	    	return false;
	    return true;
               // then all points of the input triangle is in the original triangle of this object
	}
}
