package a4.s100502501;
import java.util.Scanner;
public class A42 {
	public static void main(String[] args){
		double x4,x5,x6,y4,y5,y6,x7,y7;
		Scanner input=new Scanner(System.in); //create a new Scanner
		System.out.println("Please enter three points of the new triangle:");
		double x1=input.nextDouble();
		double y1=input.nextDouble();
		double x2=input.nextDouble();
		double y2=input.nextDouble();
		double x3=input.nextDouble();
		double y3=input.nextDouble();
		Triangle2D point1=new Triangle2D(x1,y1,x2,y2,x3,y3); //create a new Triangle2D object point1
		while(true){
			System.out.println("\n1.get the area of the new triangle"+"\n2.get the perimeter of the new triangle"+"\n3.check the new triangle is in the original triangle or not"+"\n4.enter a point and check the point is in the original triangle or not"+"\n5.Exit");
			int choice=input.nextInt();
			if(choice==5) //exit
				break;
			else
				switch(choice){
				case 1: //show the area of the new triangle
					System.out.print("The area of the new triangle is "+point1.getArea()+"\n");
					break;
				case 2: //show the perimeter of the new triangle
					System.out.print("The perimeter of the new triangle is "+point1.getPerimeter()+"\n");
					break;
				case 3: // check input triangle is in the original triangle of the object or not
					System.out.println("Please input a new triangle for the new object:");
					x4=input.nextDouble();
					y4=input.nextDouble();
					x5=input.nextDouble();
					y5=input.nextDouble();
					x6=input.nextDouble();
					y6=input.nextDouble();
					Triangle2D point2=new Triangle2D(x4,y4,x5,y5,x6,y6); //create a new Triangle2D object point2
					if(point1.contains(point2))
						System.out.println("The new triangle of new object is in the original triangle of the old object!!!");
					else
						System.out.println("The new triangle of new object is NOT in the original triangle of the old object!!!");
					break;
				case 4: // check input point is in the original triangle of the object or not
					System.out.println("Please enter a point : ");
					x7=input.nextDouble();
					y7=input.nextDouble();
					MyPoint mypoint3=new MyPoint(x7,y7); //create a new MyPoint object mypoint3
					if(point1.contains(mypoint3))
						System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!");
					else
						System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!");
					break;
				}
		}
		System.out.print("Bye!!!");
	}
}
