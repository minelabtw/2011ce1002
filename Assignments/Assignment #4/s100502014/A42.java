package a4.s100502014;
import javax.swing.*;
public class A42 {
	//test whether it is a triangle or not
	public static boolean isTriangle(double[][] tri) {
		//if (y1-y2)*(x2-x3) = (y2-y3)*(x1-x2) , it is not a triangle
		if(((tri[0][0]-tri[1][0])*(tri[1][1]-tri[2][1])) != ((tri[1][0]-tri[2][0])*(tri[0][1]-tri[1][1])))
			return true;
		else
			return false;
	}
	public static void main(String[] args) {
		int flag1 = 1; //flag for the whole loop
		while(flag1!=0) {
			flag1 = 0; //initial the flag
			int flag2 = 1; 	//flag for option
			
			//enter triangle
			String[][] triString = new String[3][2];
			JOptionPane.showMessageDialog(null,
					   					  "Please enter three points of the new triangle:",
					   					  "Triangle!!",
					   					  JOptionPane.INFORMATION_MESSAGE);
			for(int i=0;i<3;i++) {
				for(int j=0;j<2;j++) {
					String output = new String();
					if(j==0)
						output = "X" + (i+1) + " :"; 
					else
						output = "Y" + (i+1) + " :"; 
					triString[i][j] = JOptionPane.showInputDialog(null,
								   	  output,
								   	  "Triangle!!",
								   	  JOptionPane.QUESTION_MESSAGE);
				}
			}
			double[][] triangle = new double[3][2];
			for(int i=0;i<3;i++) {
				for(int j=0;j<2;j++) {
					triangle[i][j] = Double.parseDouble(triString[i][j]);
				}
			}
			//test whether it is a triangle
			if(isTriangle(triangle) == false) {
				JOptionPane.showMessageDialog(null,
											  "This is NOT a triangle.\n" +
											  "Please undo!!!",
											  "Error!!",
											  JOptionPane.ERROR_MESSAGE);
				flag1 = 1;
				continue;
			}
			
			//show the points of triangle
			String output1 = "Your triangle is :\n("+
						  	 triangle[0][0]+","+triangle[0][1]+") ("+
						  	 triangle[1][0]+","+triangle[1][1]+") ("+
						  	 triangle[2][0]+","+triangle[2][1]+")";
			JOptionPane.showMessageDialog(null,
 					  					  output1,
 					  					  "Triangle!!",
 					  					  JOptionPane.INFORMATION_MESSAGE);
			Triangle2D t = new Triangle2D(triangle);
			
			//option
			while(flag2!=0) {
				String opString = JOptionPane.showInputDialog(null,
							      "Please choose your option:\n" +
							      "1.Get the area of the new triangle\n"+
							      "2.Get the perimeter of the new triangle\n"+
							      "3.Check the new triangle is in the original triangle or not\n"+
							      "4.Enter a point and check the point is in the original triangle or not\n"+
							      "5.Enter a new triangle\n"+
							      "6.Exit",
							      "Option!!",
							      JOptionPane.QUESTION_MESSAGE);
				int option = Integer.parseInt(opString);
				switch(option) {
					//area
					case 1:
						String output2 = "The area of the new triangle is " + t.getArea();
						JOptionPane.showMessageDialog(null,
													  output2,
													  "Area!!",
													  JOptionPane.INFORMATION_MESSAGE);
						break;
						
					//perimeter
					case 2:
						String output3 = "The perimeter of the new triangle is " + t.getPerimeter();
						JOptionPane.showMessageDialog(null,
			   					  					  output3,
			   					  					  "Perimeter!!",
			   					  					  JOptionPane.INFORMATION_MESSAGE);
						break;
						
					//new triangle whether in original triangle or not
					case 3:
						double[][] tri = new double[3][2];
						while(true) {
							String[][] nTriString = new String[3][2];
							JOptionPane.showMessageDialog(null,
									   					  "Please enter three points of the new triangle:",
									   					  "Triangle!!",
									   					  JOptionPane.INFORMATION_MESSAGE);
							for(int i=0;i<3;i++) {
								for(int j=0;j<2;j++) {
									String output = new String();
									if(j==0)
										output = "X" + (i+1) + " :"; 
									else
										output = "Y" + (i+1) + " :"; 
									nTriString[i][j] = JOptionPane.showInputDialog(null,
												   	  output,
												   	  "Triangle!!",
												   	  JOptionPane.QUESTION_MESSAGE);
								}
							}
							for(int i=0;i<3;i++) {
								for(int j=0;j<2;j++) {
									tri[i][j] = Double.parseDouble(nTriString[i][j]);
								}
							}
							if(isTriangle(tri) == false) {
								JOptionPane.showMessageDialog(null,
															  "This is NOT a triangle.\n" +
															  "Please undo!!!",
															  "Error!!",
															  JOptionPane.ERROR_MESSAGE);
							}
							else
								break;
						}
						Triangle2D t2 = new Triangle2D(tri);
						
						//test whether or not
						if(t.contains(t2) == true)
							JOptionPane.showMessageDialog(null,
				   					  					  "The new triangle of new object is in the original triangle of the old object!!!",
				   					  					  "Inside!!",
				   					  					  JOptionPane.INFORMATION_MESSAGE);
						else
							JOptionPane.showMessageDialog(null,
				   					  					  "The new triangle of new object is NOT in the original triangle of the old object!!!",
				   					  					  "Outside!!",
				   					  					  JOptionPane.INFORMATION_MESSAGE);
						break;
						
					//new point whether in original triangle or not
					case 4:
						String xString = JOptionPane.showInputDialog(null,
									     "Please input a new point:\n" +
									     "X = ",
									     "Point!!",
									     JOptionPane.QUESTION_MESSAGE);
						String yString = JOptionPane.showInputDialog(null,
									     "Y = ",
									     "Point!!",
									     JOptionPane.QUESTION_MESSAGE);
						double x = Double.parseDouble(xString);
						double y = Double.parseDouble(yString);
						MyPoint point = new MyPoint(x,y);
						
						//test whether or not
						if(t.contains(point) == true)
							JOptionPane.showMessageDialog(null,
														  "The new point is in the original triangle (0,0), (17,6), (10, 15) !!!",
														  "Inside!!",
														  JOptionPane.INFORMATION_MESSAGE);
						else
							JOptionPane.showMessageDialog(null,
														  "The new point is NOT in the original triangle (0,0), (17,6), (10, 15) !!!",
														  "Outside!!",
														  JOptionPane.INFORMATION_MESSAGE);
						break;
						
					//clear all and enter new triangle
					case 5:
						flag1 = 1;
						flag2 = 0;
						break;
						
					//exit
					case 6:
						flag2 = 0;
						break;
					default:
						JOptionPane.showMessageDialog(null,
								  "Error!!!\nPlease enter again!!",
								  "Error!!!",
								  JOptionPane.ERROR_MESSAGE);
				}
			}
		}
	}
}
