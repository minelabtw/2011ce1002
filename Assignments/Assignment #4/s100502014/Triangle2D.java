package a4.s100502014;
public class Triangle2D {
	//points of original triangle triangle
	private MyPoint p1 = new MyPoint(0.0, 0.0);
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	
	// points of new triangle
	private MyPoint p1_new, p2_new, p3_new;
	
	public MyPoint getP1_new() { // get the p1 of new triangle
		return p1_new;
	}
	
	public MyPoint getP2_new() { // get the p2 of new triangle
		return p2_new;
	}
	
	public MyPoint getP3_new() { // get the p3 of new triangle
		return p3_new;
	}
	
	// constructor with arguments to initial the points of the new triangle
	public Triangle2D(double[][] p) {
		p1_new = new MyPoint(p[0][0],p[0][1]);
		p2_new = new MyPoint(p[1][0],p[1][1]);
		p3_new = new MyPoint(p[2][0],p[2][1]);
	}
	
	//calculate area by double
	public double area(double x1, double y1, double x2, double y2, double x3, double y3) {
		return Math.abs((1/2.0)*(x1*y2+x2*y3+x3*y1-x2*y1-x3*y2-x1*y3));
	}
	
	//calculate the area of the new triangle by MyPoint
	public double getArea() { 
		return area(p1_new.getX(),p1_new.getY(),
					p2_new.getX(),p2_new.getY(),
					p3_new.getX(),p3_new.getY());
	}
	
	// calculate the perimeter of the new triangle
	public double getPerimeter(){ 
		return p1_new.distance(p2_new)+p2_new.distance(p3_new)+p3_new.distance(p1_new);
	}

	// check the input point is in the original triangle of the object or not
	public boolean contains(MyPoint p){
		if(area(p.getX(),p.getY(),p1.getX(),p1.getY(),p2.getX(),p2.getY())+
		   area(p.getX(),p.getY(),p2.getX(),p2.getY(),p3.getX(),p3.getY())+
		   area(p.getX(),p.getY(),p3.getX(),p3.getY(),p1.getX(),p1.getY())==
		   area(p1.getX(),p1.getY(),p2.getX(),p2.getY(),p3.getX(),p3.getY()))
			return true;
		else
			return false;
	}

	// check the input triangle(Triangle2D object) is in the original triangle of this object or not
	public boolean contains(Triangle2D input){
		if(contains(input.getP1_new()) == true &&
		   contains(input.getP2_new()) == true &&
		   contains(input.getP3_new()) == true)
			return true;
		else
			return false;
	}
}
