package a4.s100502014;

public class MyPoint {
	private double x;
	private double y;
	
	// constructor to initial private data member
	public MyPoint(double x_input, double y_input){ 
		setX(x_input);
		setY(y_input);
	}
	
	// return x value
	public double getX(){
		return x;
	}
	
	// return y value
	public double getY(){
		return y;
	}
	
	// set x value
	public void setX(double input) {
		x = input;
	}
	
	// set y value
	public void setY(double input) {
		y = input;
	}
	
	// the distance between this point and the input point
	public double distance(MyPoint input) {
		return Math.sqrt(Math.pow(input.getX()-x, 2)+Math.pow(input.getY()-y, 2));
	}
}
