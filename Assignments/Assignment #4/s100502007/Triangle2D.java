package a4.s100502007;

public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0, 0.0); //original points 
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint newp1,newp2,newp3; //new points
	public MyPoint getnewp1(){ // get p1 of new triangle
		return newp1;
	}
	public MyPoint getnewp2(){ // get p2 of new triangle
		return newp2;
	}
	public MyPoint getnewp3(){ // get p3 of new triangle
		return newp3;
	}
	
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3){//initialize the point of new triangle
		newp1=new MyPoint(x1,y1);
		newp2=new MyPoint(x2,y2);
		newp3=new MyPoint(x3,y3);
	}
	
	double s1,s2,s3;
	public double getPerimeter(){//get perimeter of new triangle
		s1=newp2.distance(newp3);
		s2=newp3.distance(newp1);
		s3=newp1.distance(newp2);
		return s1+s2+s3;
	}
	public double getArea(){//get area of new triangle
		s1=newp2.distance(newp3);
		s2=newp3.distance(newp1);
		s3=newp1.distance(newp2);
		double s=(s1+s2+s3)/2;
		return Math.sqrt(s*(s-s1)*(s-s2)*(s-s3));
	}
	public double getoriginalArea(){//get area of original triangle
		double os1=p2.distance(p3);
		double os2=p3.distance(p1);
		double os3=p1.distance(p2);
		double s=(os1+os2+os3)/2;
		return Math.sqrt(s*(s-os1)*(s-os2)*(s-os3));
	}
	
	public boolean contains(MyPoint p){//check the point is in the triangle or not
		double firstsidetotal=p2.distance(p)+p3.distance(p)+p2.distance(p3);
		double secondsidetotal=p3.distance(p)+p1.distance(p)+p3.distance(p1);
		double thirdsidetotal=p1.distance(p)+p2.distance(p)+p1.distance(p2);
		double firstarea=Math.sqrt((firstsidetotal/2)*((firstsidetotal/2)-p2.distance(p))*((firstsidetotal/2)-p3.distance(p))*((firstsidetotal/2)-p2.distance(p3)));
		double secondarea=Math.sqrt((secondsidetotal/2)*((secondsidetotal/2)-p3.distance(p))*((secondsidetotal/2)-p1.distance(p))*((secondsidetotal/2)-p3.distance(p1)));
		double thirdarea=Math.sqrt((thirdsidetotal/2)*((thirdsidetotal/2)-p1.distance(p))*((thirdsidetotal/2)-p2.distance(p))*((thirdsidetotal/2)-p1.distance(p2)));
		if(Math.abs(getoriginalArea()-(firstarea+secondarea+thirdarea))<=0.5){
			return true;
		}
		else{
			return false;
		}
	}
	public boolean contains(Triangle2D t){//check the triangle is in the triangle or not
		if((contains(t.getnewp1())==true)&&(contains(t.getnewp2())==true)&&(contains(t.getnewp3())==true)){
			return true;
		}
		else{
			return false;
		}
	}
	
}
