package a4.s100502007;
import java.util.Scanner;
public class A42 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		System.out.print("Please enter three points of the new triangle : ");
		
		double a=input.nextDouble();//enter three points
		double b=input.nextDouble();
		double c=input.nextDouble();
		double d=input.nextDouble();
		double e=input.nextDouble();
		double f=input.nextDouble();
		Triangle2D t=new Triangle2D(a,b,c,d,e,f);
		for( ; ; ){
			System.out.println("1.get the area of the new triangle");
			System.out.println("2.get the perimeter of the new triangle");
			System.out.println("3.check the new triangle is in the original triangle or not");
			System.out.println("4.enter a point and check the point is in the original triangle or not");
			System.out.println("5.Exit");
			System.out.print("Please enter your choice : ");
			int choose=input.nextInt();			
			switch(choose){
				case 1:
					System.out.println("The area of the new triangle is "+t.getArea());//area
					break;
				case 2:
					System.out.println("The perimeter of the new triangle is "+t.getPerimeter());//perimeter
					break;
				case 3:
					System.out.println("Enter three points to check inside or ouside the triangle : ");//check point
					double xp1=input.nextDouble();
					double yp1=input.nextDouble();
					double xp2=input.nextDouble();
					double yp2=input.nextDouble();
					double xp3=input.nextDouble();
					double yp3=input.nextDouble();
					Triangle2D t1=new Triangle2D(xp1,yp1,xp2,yp2,xp3,yp3);
					if(t.contains(t1)==true){
						System.out.println("The new triangle is in the original triangle!!!");
					}
					else{
						System.out.println("The new triangle is NOT in the original triangle!!!");
					}
					break;
				case 4:
					System.out.print("Enter a point to check inside or ouside the triangle : ");//check triangle
					double x=input.nextDouble();
					double y=input.nextDouble();
					MyPoint mypoint=new MyPoint(x,y);
					if(t.contains(mypoint)==true){
						System.out.println("The new point is in the original triangle!!!");
					}
					else{
						System.out.println("The new point is NOT in the original triangle (0,0) (17,6) (10,15)!!!");
					}
					break;
				case 5:
					break;
			}
			if(choose==5){//exit
				break;
			}
		}
	}
}
