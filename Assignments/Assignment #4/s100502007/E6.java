package e6.s100502007;
import java.util.Scanner;

public class E6 {
	public static void main(String[] args)throws Exception{
		java.io.File file = new java.io.File("input.txt");//read information from input.txt
		Scanner input = new Scanner(file);
		String word = input.next();
		String newword=" ";//output information
		char[] wordarray=new char[word.length()];//array
		
		for(int i=0;i<word.length();i++){
			wordarray[i]=word.charAt(i);//put number in array
		}
		for(int i=1;i<word.length();i++){//sort method
			for(int j=0;j<word.length()-i;j++){
				if(wordarray[j]<=wordarray[j+1]){
					swap(j,j+1,wordarray);
				}
			}
		}
		for(int i=0;i<word.length();i++){//let output information in a line
			newword=newword+wordarray[i];
		}
		java.io.PrintWriter output = new java.io.PrintWriter("output.txt");//output data to txt
		output.print(newword);
		input.close();
		output.close();
	}
	public static void swap(int before,int after,char[] array){//the method to sort
		char temp=array[before];
		array[before]=array[after];
		array[after]=temp;
	}
}
