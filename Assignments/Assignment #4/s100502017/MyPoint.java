package a4.s100502017;

import java.lang.Math;

public class MyPoint {
	private double x;
	private double y;
	public MyPoint(){//default constructor set(x,y)=(0,0)
		setX(0.0);
		setY(0.0);
	}
	public MyPoint(double x_input, double y_input){//constructor 
		setX(x_input);
		setY(y_input);
	}
	public double getX(){//return x value
		return x;
	}
	public double getY(){//return y value
		return y;
	}
	public void setX(double input){//set x value
		x=input;
	}
	public void setY(double input){//set y value
		y=input;
	}
	public double distance(MyPoint input){//use Math class to calculate distance
		double dist=Math.sqrt((x-input.x)*(x-input.x)+(y-input.y)*(y-input.y));
		return dist;
	}
}
