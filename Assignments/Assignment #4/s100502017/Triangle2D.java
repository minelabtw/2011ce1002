package a4.s100502017;

import java.lang.Math;

public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0, 0.0);// points of original triangle triangle
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint p1_new, p2_new, p3_new;// points of new triangle
	public MyPoint getP1_new(){// get the p1 of new triangle
		return p1_new;
	}
	public MyPoint getP2_new(){// get the p2 of new triangle
		return p2_new;
	}
	public MyPoint getP3_new(){// get the p3 of new triangle
		return p3_new;
	}
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3){//constructor
		p1_new=new MyPoint(x1,y1);
		p2_new=new MyPoint(x2,y2);
		p3_new=new MyPoint(x3,y3);
	}
	public double getArea(){//use Heron's formula to calculate the area of the new triangle
		double a=p1_new.distance(p2_new);
		double b=p2_new.distance(p3_new);
		double c=p3_new.distance(p1_new);
		double s=(a+b+c)/2.0;
		return Math.sqrt(s*(s-a)*(s-b)*(s-c));
	}
	public double getPerimeter(){//calculate the perimeter
		double a=p1_new.distance(p2_new);
		double b=p2_new.distance(p3_new);
		double c=p3_new.distance(p1_new);
		return a+b+c;
	}
	public boolean contains(MyPoint p){//check the point is in the original triangle or not
		double side1,side2,side3,s,areaPAB,areaPBC,areaPCA,areaABC;
		
		side1=p.distance(p1);// calculate area of PAB
		side2=p1.distance(p2);
		side3=p2.distance(p);
		s=(side1+side2+side3)/2;
		areaPAB=Math.sqrt(s*(s-side1)*(s-side2)*(s-side3));
		
		side1=p.distance(p2);//calculate area of PBC
		side2=p2.distance(p3);
		side3=p3.distance(p);
		s=(side1+side2+side3)/2;
		areaPBC=Math.sqrt(s*(s-side1)*(s-side2)*(s-side3));
		
		side1=p.distance(p3);//calculate area of PCA
		side2=p3.distance(p1);
		side3=p1.distance(p);
		s=(side1+side2+side3)/2;
		areaPCA=Math.sqrt(s*(s-side1)*(s-side2)*(s-side3));
		
		side1=p1.distance(p2);//calculate area of ABC
		side2=p2.distance(p3);
		side3=p3.distance(p1);
		s=(side1+side2+side3)/2;
		areaABC=Math.sqrt(s*(s-side1)*(s-side2)*(s-side3));
		
		if(Math.abs(areaPAB+areaPBC+areaPCA-areaABC)<=0.5)//because of the error we use absolute value <=0.5
			return true;
		else 
			return false;
	}
	public boolean contains(Triangle2D input){//check the new triangle is in the original triangle or not
		if(contains(input.p1_new)&&contains(input.p2_new)&&contains(input.p3_new))
			return true;
		else
			return false;
	}
}
