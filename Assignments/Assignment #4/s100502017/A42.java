package a4.s100502017;

import java.util.Scanner;

public class A42 {
	public static void main (String[] args){
		Scanner input=new Scanner(System.in);
		System.out.println("Please enter three points of the new triangle: ");
		double x1=input.nextDouble();
		double y1=input.nextDouble();
		double x2=input.nextDouble();
		double y2=input.nextDouble();
		double x3=input.nextDouble();
		double y3=input.nextDouble();
		Triangle2D userinput=new Triangle2D(x1,y1,x2,y2,x3,y3);// declare a object
		System.out.println("1.get the area of the new triangle\n2.get the perimeter of the new triangle\n3.check the new triangle is in the original triangle or not\n4.enter a point and check the point is in the original triangle or not\n5.Exit");
		boolean key=true;
		while(key){//use key to determine whether the loop continue or not
			System.out.println("choise functionality: ");
			int choise=input.nextInt();
			switch(choise){//use switch statement to determine the choice
				case 1:// output the area of the new triangle
					System.out.println("The area of the new triangle is "+userinput.getArea());
					break;
				case 2://output the perimeter of the new triangle
					System.out.println("The perimeter of the new triangle is "+userinput.getPerimeter());
					break;
				case 3://check the new triangle is in the original triangle or not
					System.out.println("Please input a new triangle for the new object:");
					x1=input.nextDouble();
					y1=input.nextDouble();
					x2=input.nextDouble();
					y2=input.nextDouble();
					x3=input.nextDouble();
					y3=input.nextDouble();
					Triangle2D newuserinput=new Triangle2D(x1,y1,x2,y2,x3,y3);
					if(userinput.contains(newuserinput))
						System.out.println("The new triangle of new object is in the original triangle of the old object!!!");
					else
						System.out.println("The new triangle of new object is NOT in the original triangle of the old object!!!");
					break;
				case 4://check the point is in the original triangle or not
					System.out.println("Please input a point (x,y)");
					double x=input.nextDouble();
					double y=input.nextDouble();
					MyPoint point=new MyPoint(x,y);
					if(userinput.contains(point))
						System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!");
					else 
						System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!");
					break;
				case 5://exit
					key=false;
					break;
				default:
					System.out.println("The number is wrong!!");
					break;
			}
		}
		System.out.println("Thank for using!!");
	}
}
