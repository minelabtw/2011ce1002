package a4.s100502508;

import java.util.Scanner;
public class A42 
{
	public static void main(String[] args)//the main execute
	{
		Scanner input=new Scanner(System.in);//new a Scanner object
		System.out.println("Please enter three points of the new triangle:");
		double Xx=input.nextDouble();
		double Xy=input.nextDouble();
		double Yx=input.nextDouble();
		double Yy=input.nextDouble();
		double Zx=input.nextDouble();
		double Zy=input.nextDouble();
		Triangle2D triangle1=new Triangle2D(Xx,Xy,Yx,Yy,Zx,Zy);//Declare an object and pass three points
		
		System.out.println("\n1.get the area of the new triangle");
		System.out.println("2.get the perimeter of the new triangle");
		System.out.println("3.check the new triangle is in the original triangle or not");
		System.out.println("4.enter a point and check the point is in the original triangle or not");
		System.out.println("5.Exit\n");
		
		boolean flag=true;
		while(flag)
		{
			int choose=input.nextInt();
			switch(choose)//switch case
			{
				case 1://get area
					System.out.println("The area of the new triangle is "+triangle1.getArea()+"\n");
					flag=true;
					break;
				case 2://get perimeter
					System.out.println("The perimeter of the new triangle is "+triangle1.getPerimeter()+"\n");
					flag=true;
					break;
				case 3://check new triangle if in the old triangle or not
					System.out.println("Please enter new three points of the new triangle:");
					double Ax=input.nextDouble();
					double Ay=input.nextDouble();
					double Bx=input.nextDouble();
					double By=input.nextDouble();
					double Cx=input.nextDouble();
					double Cy=input.nextDouble();
					Triangle2D triangle2=new Triangle2D(Ax,Ay,Bx,By,Cx,Cy);//Declare another object and pass three points
					if(triangle1.contains(triangle2)==true)
					{
						System.out.println("The new triangle of new object is in the original triangle of the old object!!!\n");
					}
					else
					{
						System.out.println("The new triangle of new object is NOT in the original triangle of the old object!!!\n");
					}
					break;
				case 4://check a point if in the old triangle or not
					double inputx=input.nextDouble();
					double inputy=input.nextDouble();
					MyPoint test3=new MyPoint(inputx,inputy);
					if(triangle1.contains(test3)==true)
					{
						System.out.println("The new triangle is in the original triangle (0,0), (17,6), (10, 15)!!!\n");
					}
					else
					{
						System.out.println("The new triangle is Not in the original triangle (0,0), (17,6), (10, 15)!!!\n");
					}
					flag=true;
					break;
				case 5://exit
					flag=false;
					break;
			}//end switch
		}//end while
	}//end main
}
