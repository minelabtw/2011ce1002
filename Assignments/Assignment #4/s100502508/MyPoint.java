package a4.s100502508;

import java.lang.Math;
public class MyPoint 
{
	private double x;
	private double y;

	public MyPoint()
	{

	}

	public MyPoint(double x_input, double y_input)// constructor to initial private data member
	{ 
		setX(x_input);
		setY(y_input);
	}

	public double getX()// return x value
	{ 
		return x;
	}

	public double getY()// return y value
	{ 
		return y;
	}

	public void setX(double inputx)// set x value
	{
		x=inputx;
	}

	public void setY(double inputy)// set y value
	{
		y=inputy;
	}

	public double distance(MyPoint input)// the distance between this point and the input point
	{ 
		return Math.sqrt((x-input.getX())*(x-input.getX())+(y-input.getY())*(y-input.getY()));
	}
}
