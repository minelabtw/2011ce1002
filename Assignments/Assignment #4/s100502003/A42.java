package a4.s100502003;

import java.util.Scanner;

public class A42 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.print("Please enter three points of the new triangle: ");
		double[] new_points = new double[6]; 
		for(int i=0; i<6; i++) { // store those points into an array
			new_points[i] = input.nextDouble();
		}
		
		MyPoint mypoint = new MyPoint(); // declare an object to help calling the method in MyPoint
		MyPoint p1 = new MyPoint(new_points[0], new_points[1]); // the object p1 is for storing the value of first point
		MyPoint p2 = new MyPoint(new_points[2], new_points[3]); // the object p2 is for storing the value of second point
		MyPoint p3 = new MyPoint(new_points[4], new_points[5]); // the object p3 is for storing the value of third point
		// objects that count the lengths for each side of triangle
		double side1 = mypoint.distance(p1, p2);
		double side2 = mypoint.distance(p2, p3);
		double side3 = mypoint.distance(p3, p1);
		
		Triangle2D T2D = new Triangle2D(new_points[0], new_points[1], new_points[2], new_points[3], new_points[4], new_points[5]); // initialize the value
		System.out.println("\n1.get the area of the new triangle");
		System.out.println("2.get the perimeter of the new triangle");
		System.out.println("3.check the new traingle is in the original triangle or not");
		System.out.println("4.enter a point and check the point is in the original triangle or not");
		System.out.println("5.Exit\n");
		
		int choice = 0;
		while(choice != 5) { // 5 is for leaving. If the user entered 5, break the loop
			System.out.println("Please enter your selection: ");
			choice = input.nextInt();
			switch(choice) {
				case 1:
					System.out.println("The area of the new triangle is " + T2D.getArea(side1, side2, side3) + "\n");
					break;
				case 2:
					System.out.println("The perimeter of the new triangle is " + T2D.getPerimeter(side1, side2, side3) + "\n");
					break;
				case 3:
					System.out.println("Please input a new triangle for the new object: ");
					double[] newtriangle = new double[6]; // store the new points into another array
					for(int j=0; j<6; j++) {
						newtriangle[j] = input.nextDouble();
					}
					Triangle2D T2D_new = new Triangle2D(newtriangle[0], newtriangle[1], newtriangle[2], newtriangle[3], newtriangle[4], newtriangle[5]); // initialize
					boolean inside = T2D_new.contains(T2D_new); // pass the object to the contains method and call it by the object
					if(inside == true) 
						System.out.println("The new triangle of new object is in the original triangle of the old object!!!\n");
					else
						System.out.println("The new triangle of new object is NOT in the original triangle of the old object!!!\n");
					break;
				case 4:
					System.out.print("Please input a point: ");
					double[] newpoint = new double[2];
					newpoint[0] = input.nextDouble();
					newpoint[1] = input.nextDouble();
					boolean point_in = T2D.contains(newpoint[0], newpoint[1]); // call the method and check whether the point is in the triangle
					if(point_in == true)
						System.out.println("The new point is in the original triangle (0,0), (17,6), (10,15)!!!\n");
					else
						System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10,15)!!!\n");
					break;
				case 5:
					break;
				default:
					System.out.println("Wrong input!!\n");
					break;
			}
		}
	}
}
