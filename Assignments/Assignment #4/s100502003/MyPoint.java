package a4.s100502003;

public class MyPoint {
	private double x, y; 
	MyPoint() { // constructor that let the object to call
		x = 0;
		y = 0;
	}
	MyPoint(double X, double Y) { // constructor that stores the point entered by the user
		set_x(X);
		set_y(Y);
	}
	public double get_x() { // get the value of x 
		return x;
	}
	public double get_y() { // get the value of y 
		return y;
	}
	public void set_x(double input_x) { // let the user be able to change the value of the private variable x
		x = input_x;
	}
	public void set_y(double input_y) { // let the user be able to change the value of the private variable y 
		y = input_y;
	}
	public double distance(MyPoint input1, MyPoint input2) { // read two objects(each of them is with a point in it) and count the distance between those two point 
		return Math.sqrt(Math.pow((input1.get_x()-input2.get_x()),2)+Math.pow((input1.get_y()-input2.get_y()),2));
	}
}
