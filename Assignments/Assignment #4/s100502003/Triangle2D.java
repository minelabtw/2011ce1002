package a4.s100502003;

public class Triangle2D {
	// set the original points with objects been declared
	private MyPoint my_point1 = new MyPoint(0.0, 0.0); 
	private MyPoint my_point2 = new MyPoint(10.0, 15.0);
	private MyPoint my_point3 = new MyPoint(17.0, 6.0);
	private MyPoint p1_new, p2_new, p3_new; // three objects that store the new points
	public MyPoint my_point = new MyPoint(); // object that help to call the class
	
	public MyPoint get_p1_new() { // get the object by the method
		return p1_new;
	}
	
	public MyPoint get_p2_new() {
		return p2_new;
	}
	
	public MyPoint get_p3_new() {
		return p3_new;
	}
	
	Triangle2D(double p1_x, double p1_y, double p2_x, double p2_y, double p3_x, double p3_y) { // initialize each objects
		p1_new = new MyPoint(p1_x, p1_y);
		p2_new = new MyPoint(p2_x, p2_y);
		p3_new = new MyPoint(p3_x, p3_y);	
	}
	
	public double getArea(double Side1, double Side2, double Side3) { // count the area of triangle with the formula
		double string_sum = (Side1 + Side2 + Side3)/2;
		return Math.sqrt(string_sum*(string_sum-Side1)*(string_sum-Side2)*(string_sum-Side3));
	}
	
	public double getPerimeter(double Side1, double Side2, double Side3) { // count the perimeter
		return Side1+Side2+Side3;
	}
	
	public boolean contains(double x, double y) {
		MyPoint point_now = new MyPoint(x,y); // an object of MyPoint that help to call the method in MyPoint
		// call the distance method and count the lengths for each side of triangles
		double Side_pa = my_point.distance(point_now, my_point1);
		double Side_pb = my_point.distance(point_now, my_point2);
		double Side_pc = my_point.distance(point_now, my_point3);
		double Side_ab = my_point.distance(my_point1, my_point2);
		double Side_bc = my_point.distance(my_point2, my_point3);
		double Side_ca = my_point.distance(my_point3, my_point1);
		double area_pab = getArea(Side_pa, Side_pb, Side_ab); // the area of triangle PAB
		double area_pbc = getArea(Side_pb, Side_pc, Side_bc); // the area of triangle PBC
		double area_pca = getArea(Side_pc, Side_pa, Side_ca); // the area of triangle PCA
		double area_abc = getArea(Side_ab, Side_bc, Side_ca); // the area of triangle ABC
		if(Math.abs(area_abc - (area_pab + area_pbc + area_pca))<=0.5) { // consider the deviation value of 0.5
			return true;
		}
		else
			return false;
	}
	
	public boolean contains(Triangle2D input) {
		// call another contain method to check that whether the points are in the area(for three times)
		boolean point_1 = contains(get_p1_new().get_x(), get_p1_new().get_y());
		boolean point_2 = contains(get_p2_new().get_x(), get_p2_new().get_y());
		boolean point_3 = contains(get_p3_new().get_x(), get_p3_new().get_y());
		if(point_1 && point_2 && point_3 == true){ // correspond the condition
			return true;
		}
		else
			return false;
	}
}
