package a4.s995002204;

import java.util.Scanner;

public class A42 {
	public static void main(String args[])
	{
		int decision=0;		//declare variable with number zero
		double x1=0,y1=0,x2=0,y2=0,x3=0,y3=0;
		double p1=0,p2=0;
		Scanner input = new Scanner(System.in);
		System.out.println("Please enter three points of the new triangle:");
		x1 = input.nextDouble();	//let user put number into the variable
		y1 = input.nextDouble();
		x2 = input.nextDouble();
		y2 = input.nextDouble();
		x3 = input.nextDouble();
		y3 = input.nextDouble();
		
		Triangle2D Triangle2D_1 = new Triangle2D(x1,y1,x2,y2,x3,y3);	//declare a class Triangle2D
		
		do	//a loop let user can choose what he needs
		{
			System.out.println("1.get the area of the new triangle");
			System.out.println("2.get the perimeter of the new triangle");
			System.out.println("3.check the new triangle is in the original triangle or not");
			System.out.println("4.enter a point and check the point is in the original triangle or not");
			System.out.println("5.Exit");
			decision = input.nextInt();
			
			switch(decision)	//choose
			{
			case 1:
				System.out.println("The area of the new triangle is "+Triangle2D_1.getArea());
				break;
			case 2:
				System.out.println("The perimeter of the new triangle is "+Triangle2D_1.getPerimeter());
				break;
			case 3:
				System.out.println("Please input a new triangle for the new object:");
				x1 = input.nextDouble();
				y1 = input.nextDouble();
				x2 = input.nextDouble();
				y2 = input.nextDouble();
				x3 = input.nextDouble();
				y3 = input.nextDouble();
				Triangle2D Triangle2D_2 = new Triangle2D(x1,y1,x2,y2,x3,y3);
				
				if(Triangle2D_2.contains(Triangle2D_2))
					System.out.println("The new triangle of new object is in the original triangle of the old object!!!");
				else
					System.out.println("The new triangle of new object is not in the original triangle of the old object!!!");
				break;
			case 4:
				MyPoint MyPoint_1 = new MyPoint();
				MyPoint_1.setX(input.nextDouble());
				MyPoint_1.setY(input.nextDouble());
				if(Triangle2D_1.contains(MyPoint_1))
				{
					System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!");
				}
				else
				{
					System.out.println("The new point is not in the original triangle (0,0), (17,6), (10, 15)!!!");
				}
			default:
				break;
			}
			
			
		}while(decision!=5);	//loop ends
		
	}
}
