package a4.s995002204;

import java.math.*;

public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0, 0.0); // points of original triangle triangle
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint p1_new = new MyPoint();
	private MyPoint p2_new = new MyPoint();
	private MyPoint p3_new = new MyPoint(); // points of new triangle

	public MyPoint getP1_new(){ // get the p1 of new triangle
		return p1_new;
	}

	public MyPoint getP2_new(){ // get the p2 of new triangle
		return p2_new;
	}

	public MyPoint getP3_new(){ // get the p3 of new triangle
		return p3_new;
	}

	// constructor with arguments to initial the points of the new triangle
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3){
		p1_new.setX(x1);
		p1_new.setY(y2);
		p2_new.setX(x2);
		p2_new.setY(y2);
		p3_new.setX(x3);
		p3_new.setY(y3);
	}

	public double getArea(){ // calculate the area of the new triangle
		double a,b,c,s;
		a = Math.sqrt(Math.pow(Math.abs(p1_new.getX()-p2_new.getX()), 2)+Math.pow(Math.abs(p1_new.getY()-p2_new.getY()), 2));
		b = Math.sqrt(Math.pow(Math.abs(p2_new.getX()-p3_new.getX()), 2)+Math.pow(Math.abs(p2_new.getY()-p3_new.getY()), 2));
		c = Math.sqrt(Math.pow(Math.abs(p3_new.getX()-p1_new.getX()), 2)+Math.pow(Math.abs(p3_new.getY()-p1_new.getY()), 2));
		s = (a+b+c)/2;
		return Math.sqrt(s*(s-a)*(s-b)*(s-c));
	}

	public double getPerimeter(){ // calculate the perimeter of the new triangle
		double a,b,c,s;
		a = Math.sqrt(Math.pow(Math.abs(p1_new.getX()-p2_new.getX()), 2)+Math.pow(Math.abs(p1_new.getY()-p2_new.getY()), 2));
		b = Math.sqrt(Math.pow(Math.abs(p2_new.getX()-p3_new.getX()), 2)+Math.pow(Math.abs(p2_new.getY()-p3_new.getY()), 2));
		c = Math.sqrt(Math.pow(Math.abs(p3_new.getX()-p1_new.getX()), 2)+Math.pow(Math.abs(p3_new.getY()-p1_new.getY()), 2));
		s = a+b+c;
		return s;
	}

	// check the input point is in the original triangle of the object or not
	public boolean contains(MyPoint p){
		// if input point p is in the triangle, area of PAB + area of PBC + area of PCA = area of ABC 
		double side1, side2, side3,areaPAB, areaPBC, areaPCA, areaABC;
		double side4,side5,side6;
		
		side1 = Math.sqrt(Math.pow(Math.abs(p.getX()-p1.getX()), 2)+Math.pow(Math.abs(p.getY()-p1.getY()), 2));
		side2 = Math.sqrt(Math.pow(Math.abs(p.getX()-p2.getX()), 2)+Math.pow(Math.abs(p.getY()-p2.getY()), 2));
		side3 = Math.sqrt(Math.pow(Math.abs(p.getX()-p3.getX()), 2)+Math.pow(Math.abs(p.getY()-p3.getY()), 2));
		side4 = Math.sqrt(Math.pow(Math.abs(p1.getX()-p2.getX()), 2)+Math.pow(Math.abs(p1.getY()-p2.getY()), 2));
		side5 = Math.sqrt(Math.pow(Math.abs(p1.getX()-p3.getX()), 2)+Math.pow(Math.abs(p1.getY()-p3.getY()), 2));
		side6 = Math.sqrt(Math.pow(Math.abs(p2.getX()-p3.getX()), 2)+Math.pow(Math.abs(p2.getY()-p3.getY()), 2));
		// calculate area of PAB
		areaPAB = Math.sqrt((side1+side2+side4)/2*((side1+side2+side4)/2-side1)*((side1+side2+side4)/2-side2)*((side1+side2+side4)/2-side4));
		System.out.println(areaPAB);
		// calculate area of PBC
		areaPBC = Math.sqrt((side2+side3+side6)/2*((side2+side3+side6)/2-side2)*((side2+side3+side6)/2-side3)*((side2+side3+side6)/2-side6));
		System.out.println(areaPBC);
		// calculate area of PCA
		areaPCA = Math.sqrt((side1+side3+side5)/2*((side1+side3+side5)/2-side1)*((side1+side3+side5)/2-side3)*((side1+side3+side5)/2-side5));
		System.out.println(areaPCA);
		// calculate area of ABC
		areaABC = Math.sqrt((side4+side5+side6)/2*((side4+side5+side6)/2-side4)*((side4+side5+side6)/2-side5)*((side4+side5+side6)/2-side6));
		System.out.println(areaABC);
		// if areaPAB + areaPBC + areaPCA = areaABC, then p is in the ABC
		if(areaABC-(areaPAB+areaPBC+areaPCA)<0.5)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	// check the input triangle(Triangle2D object) is in the original triangle of this object or not
	public boolean contains(Triangle2D abc){
		if(abc.contains(abc.p1_new)&&abc.contains(abc.p2_new)&&abc.contains(abc.p3_new))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
