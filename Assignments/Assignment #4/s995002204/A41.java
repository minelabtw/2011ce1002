package a4.s995002204;

import java.util.Scanner;

public class A41 {
	public static void main(String args[])
	{
		StackOfIntegers StackOfIntegers1 = new StackOfIntegers();
		Scanner input = new Scanner(System.in);
		int decision = 0;
		int number = 0;
		
		do
		{
			System.out.println("1.push");
			System.out.println("2.pop");
			System.out.println("3.Show all");
			System.out.println("4.exit");
			decision = input.nextInt();
			
			switch(decision)
			{
			
			case 1:
				number = input.nextInt();
				StackOfIntegers1.Push(number);
				break;
			case 2:
				number = input.nextInt();
				StackOfIntegers1.Pop(number);
				break;
			case 3:
				StackOfIntegers1.showAll();
				break;
			default:
				break;
			}
			
		}while(decision!=4);
	}
}
