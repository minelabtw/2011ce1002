package a4.s995002204;

import java.util.Scanner;
import java.math.*;

public class StackOfIntegers {
	
	private static int top;
	private static int Max = 100;
	private static int item[] = new int[Max];
	
	StackOfIntegers()
	{
		top = -1;
	}
	
	public static void showAll()
	{
		if (isEmpty())
			System.out.println("the stack is empty!!");
		else
		{
			System.out.print("the stack here is :");
			for(int i=0;i<=top;i++)
			{
				System.out.print(item[i]+" ");
			}
			System.out.println();
		}
	}
	
	public static void Pop(int num)
	{
		if(isEmpty())
			System.out.println("the stack is empty!!");
		else
		{
			for(int i=0;i<num;i++)
			{
				top--;
			}
		}
	}
	
	public static void Push(int num)
	{
		if(isFull())
			System.out.println("the stack is full!!");
		else
		{
			int k=2;
			do
			{
				
				boolean[] prime = new boolean[k+1];
				for(int i=2;i<=k;i++)
				{
					prime[i] = true;
				}
				for(int i=2;i<=Math.sqrt(k);i++){
					for(int j=2;j<=(k/i);j++){
						if(i*j<=k)
							prime[i*j] = false;
					}
				}
				for(int i=2;i<=k;i++) 
				{
					if(prime[i]);
					{
						top++;
						item[top] = i;
					}	
				}
				k++;
			}while(top==k-2);
		}
	}
	
	public static boolean isEmpty()
	{
		if (top<0)
			return true;
		else
			return false;
	}
	
	public static boolean isFull()
	{
		if (top>=Max-1)
			return true;
		else
			return false;
	}
}
