package a4.s992008002;

public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0, 0.0); // points of original triangle triangle
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint p1_new, p2_new, p3_new; // points of new triangle

	public MyPoint getP1_new(){ // get the p1 of new triangle
		return p1_new;
	}

	public MyPoint getP2_new(){ // get the p2 of new triangle
		return p2_new;
	}

	public MyPoint getP3_new(){ // get the p3 of new triangle
		return p3_new;
	}

	// constructor with arguments to initial the points of the new triangle
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3){
		p1_new = new MyPoint(x1,y1);
		p2_new = new MyPoint(x2,y2);
		p3_new = new MyPoint(x3,y3);
	}

	public double getArea(){ // calculate the area of the new triangle
		double s = 0.5 * getPerimeter();
		return Math.sqrt(s*(s-p1_new.distance(p2_new))*(s-p2_new.distance(p3_new))*(s-p3_new.distance(p1_new)));
	}

	public double getPerimeter(){ // calculate the perimeter of the new triangle
		return p1_new.distance(p2_new)+p2_new.distance(p3_new)+p3_new.distance(p1_new);
	}

	// check the input point is in the original triangle of the object or not
	public boolean contains(MyPoint p){
		
		
		// if input point p is in the triangle, area of PAB + area of PBC + area of PCA = area of ABC 
		double side1, side2, side3, s, areaPAB, areaPBC, areaPCA, areaABC;

		// calculate area of ABC
		p1_new = p1;
		p2_new = p2;
		p3_new = p3;
		areaABC = getArea();
		
		
		// calculate area of PAB
		p3_new = p;
		areaPAB = getArea();
		
		
		// calculate area of PBC
		p1_new = p;
		p3_new = p3;
		areaPBC = getArea();
		

		// calculate area of PCA
		p2_new = p;
		p1_new = p1;
		areaPCA = getArea();
		


		if(Math.abs(areaABC - areaPAB - areaPBC - areaPCA)<0.5)
		return true;
		else return false;
		// if areaPAB + areaPBC + areaPCA = areaABC, then p is in the ABC
		     
		

	}

	// check the input triangle(Triangle2D object) is in the original triangle of this object or not
	public boolean contains(Triangle2D input){
		if (contains(input.getP1_new()) && contains(input.getP2_new()) && contains(input.getP3_new()))return true;
		//計算三個點是否皆在原三角形內
		
		else return false;
	       // if the input triangle is the original triangle of this object
               // then all points of the input triangle is in the original triangle of this object
	}
}
