package a4.s992008002;

import java.util.Scanner;



public class A42 {

public static void main(String args[]){
		
		
	    Scanner input = new Scanner(System.in);
		//輸入新三角形
	    System.out.print("Please enter three points of the new triangle:");	
		
	    double A = input.nextDouble();
		double B = input.nextDouble();
		double C = input.nextDouble();
		double D = input.nextDouble();
		double E = input.nextDouble();
		double F = input.nextDouble();
		
		
		
		//建立新三角形類別
			
		Triangle2D now = new Triangle2D(A,B,C,D,E,F);
		Triangle2D old = new Triangle2D(0,0,10,15,17,6);
		//(0,0), (10, 15) and (17, 6).
		
		
		
		int userChoice;
		boolean exitFlag = false;//設定跳出旗標
		while(exitFlag == false){
			System.out.println("1.get the area of the new triangle");
			System.out.println("2.get the perimeter of the new triangle");
			System.out.println("3.check the new triangle is in the original triangle or not");
			System.out.println("4.enter a point and check the point is in the original triangle or not");
			System.out.println("5.Exit");

			userChoice = input.nextInt();//鍵入選擇

			switch(userChoice){
				case 1: //計算面積
					
						System.out.printf("The area of the new triangle is %f\n\n",now.getArea());
					break;
				case 2: 	//計算周長			
						System.out.println("The perimeter of the new triangle is " + now.getPerimeter() + "\n\n");
					break;
				case 3:
					
					System.out.println("Please input a new triangle for the new object:");
				    double a = input.nextDouble();
					double b = input.nextDouble();
					double c = input.nextDouble();
					double d = input.nextDouble();
					double e = input.nextDouble();
					double f = input.nextDouble();
					Triangle2D Try = new Triangle2D(a,b,c,d,e,f);
					//產生預測試的三角形並測試三個點是否皆在新三角形內
							
					if(now.contains(Try))System.out.println("The new triangle of new object is in the original triangle of the old object!!!\n\n");
					else System.out.println("The new triangle of new object is NOT in the original triangle of the old object!!!\n\n");

					break;
					
					
				case 4://測試點是否在原三角形內
					
					double X = input.nextInt();
					double Y = input.nextInt();
					
					MyPoint test = new MyPoint(X,Y);
					
					if(old.contains(test))System.out.println("The new point is in the original triangle(0,0), (17,6), (10, 15)!!!\n\n");
					else System.out.println("The new point is NOT in the original triangle(0,0), (17,6), (10, 15)!!!\n\n");
					
					
					break;
				
				case 5:
					exitFlag = true;		
				default:
					
					break;
			}
		}
		
  
	 
	    
	}
	
	
}


