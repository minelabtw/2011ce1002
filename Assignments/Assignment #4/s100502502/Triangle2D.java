//Triangle2D.java
package a4.s100502502;
public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0, 0.0); // points of original triangle triangle
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint p1_new, p2_new, p3_new; // points of new triangle
	
	public MyPoint getP1_new(){ // get the p1 of new triangle
		return p1_new;
	}
	public MyPoint getP2_new(){ // get the p2 of new triangle
		return p2_new;
	}
	public MyPoint getP3_new(){ // get the p3 of new triangle
		return p3_new;
	}
	// constructor with arguments to initial the points of the new triangle
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3){
		p1_new = new MyPoint(x1, y1);
		p2_new = new MyPoint(x2, y2);
		p3_new = new MyPoint(x3, y3);
	}
	public double getArea(){ // calculate the area of the new triangle
		double a = p1_new.distance(p2_new);
		double b = p2_new.distance(p3_new);
		double c = p3_new.distance(p1_new);
		double S = 0.5*(a+ b+ c);
		double A = Math.sqrt(S*(S-a)*(S-b)*(S-c));
		return A;
	}
	public double getPerimeter(){ // calculate the perimeter of the new triangle
		double a = p1_new.distance(p2_new);
		double b = p2_new.distance(p3_new);
		double c = p3_new.distance(p1_new);
		double sum = a+ b+ c;
		return sum;
	}
	public boolean contains(MyPoint p){
		// if input point p is in the triangle, area of PAB + area of PBC + area of PCA = area of ABC
		double D1, D2, D3, S, side1, side2, side3, s1, s2, s3, areaPAB, areaPBC, areaPCA, areaABC;
		D1 = p1.distance(p2);
		D2 = p2.distance(p3);
		D3 = p3.distance(p1);
		side1 = p1.distance(p);
		side2 = p2.distance(p);
		side3 = p3.distance(p);
		s1 = 0.5*(D1 + side1 + side2);
		areaPAB = Math.sqrt(s1*(s1-D1)*(s1-side1)*(s1-side2));
		s2 = 0.5*(D2 + side2 + side3);
		areaPBC = Math.sqrt(s2*(s2-D2)*(s2-side2)*(s2-side3));
		s3 = 0.5*(D3 + side3 + side1);
		areaPCA = Math.sqrt(s3*(s3-D3)*(s3-side3)*(s3-side1));
		S = 0.5*(D1 + D2 + D3);
		areaABC = Math.sqrt(S*(S-D1)*(S-D2)*(S-D3));
		//when the error less than 0.5, return true
		if((areaABC - areaPAB - areaPBC - areaPCA) >= 0 || (Math.abs(areaABC - areaPAB - areaPBC - areaPCA))<=0.5)
			return true;
		else
			return false;
	}
	// check the input triangle(Triangle2D object) is in the original triangle of this object or not
	public boolean contains(Triangle2D input){
		int counter = 0;
		if(contains(input.getP1_new()) == true){
			counter = counter + 1;
		}
		if(contains(input.getP2_new()) == true){
			counter = counter + 1;
		}
		if(contains(input.getP3_new()) == true){
			counter = counter + 1;
		}
		//if three points of the input triangle in the original triangle, return true 
		if(counter == 3)
			return true;
		else
			return false;
	}
}
