//A42.java
package a4.s100502502;
import java.util.Scanner;
public class A42 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		boolean ON = true;//control the while loop
		System.out.println("Please enter three points of the new triangle:");
		double[] array = new double[6];
		for(int i = 0; i < 6; i++){
			array[i] = input.nextDouble();//save user's input
		}
		Triangle2D T1 = new Triangle2D(array[0], array[1], array[2], array[3], array[4], array[5]);
		System.out.println("\n1.get the area of the new triangle");
		System.out.println("2.get the perimeter of the new triangle");
		System.out.println("3.check the new triangle is in the original triangle or not");
		System.out.println("4.enter a point and check the point is in the original triangle or not");
		System.out.println("5.Exit\n");
		while(ON){
			System.out.print("Choose number : ");
			int choice = input.nextInt();
			switch(choice){
				case 1://get the area of the new triangle
					System.out.println("The area of the new triangle is " + T1.getArea() + "\n");
					break;
				case 2://get the perimeter of the new triangle
					System.out.println("The perimeter of the new triangle is " + T1.getPerimeter() + "\n");
					break;
				case 3://check the new triangle is in the original triangle or not
					System.out.println("Please input a new triangle for the new object:");
					double[] array2 = new double[6];
					for(int i = 0; i < 6; i++){
						array2[i] = input.nextDouble();
					}
					Triangle2D T2 = new Triangle2D(array2[0], array2[1], array2[2], array2[3], array2[4],array2[5]);
					if(T1.contains(T2) == true)
						System.out.println("The new triangle of new object is in the original triangle of the old object!!!\n");
					else
						System.out.println("The new triangle of new object is NOT in the original triangle of the old object!!!\n");
					break;
				case 4://enter a point and check the point is in the original triangle or not
					System.out.println("Enter x and y :");
					double X = input.nextDouble();//save user's input
					double Y = input.nextDouble();
					MyPoint P = new MyPoint(X, Y);
					if(T1.contains(P) == true){
						System.out.println("The new point is in the original triangle (0,0), (17,6), (10,15)!!!");
					}
					else
						System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10,15)!!!");
					break;
				case 5://Exit
					System.out.println("ByrBye~");
					ON = false;
					break;
				default:
					System.out.println("Error!!");
					break;
			}
		}
	}
}
