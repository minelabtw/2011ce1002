package a4.s100502027;

public class MyPoint {
	private double x;
	private double y;

	public MyPoint(){

	}

	public MyPoint(double xin, double yin){ // constructor to initial private data member
		setX(xin);
		setY(yin);
	}

	public double getX(){ // return x value
		return x;
	}

	public double getY(){ // return y value
		return y;
	}

	public void setX(double in) // set x value
	{
		x = in ;
	}

	public void setY(double in) // set y value
	{
		y = in ;
	}

	public double distance(MyPoint input){ // the distance between this point and the input point
		double dista=Math.sqrt(Math.pow(x-input.getX(),2)+Math.pow(y-input.getY(),2));
		return dista;
	}
}