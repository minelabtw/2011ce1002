package a4.s100502027;
import java.util.*;
public class A42 {
	public static void main(String [] args){
		Scanner input = new Scanner(System.in);
		System.out.println("Please enter three points of the new triangle:");
		double a = input.nextDouble();
		double b = input.nextDouble();
		double c = input.nextDouble();
		double d = input.nextDouble();
		double e = input.nextDouble();
		double f = input.nextDouble();
		Triangle2D user1 = new Triangle2D(a,b,c,d,e,f); //declare the class, and the class declare the class of class
		int turnon=1;
		System.out.println("1.get the area of the new triangle");//output the list 
		System.out.println("2.get the perimeter of the new triangle");
		System.out.println("3.check the new triangle is in the original triangle or not");
		System.out.println("4.enter a point and check the point is in the original triangle or not");
		System.out.println("5.Exit");
		while(turnon>0){ //[while] for infinite loop
			System.out.println("");
			System.out.println("Please choose the work : ");
			int choose1 = input.nextInt();
			switch(choose1){
				case 1:
					System.out.println("The area of the new triangle is " + user1.getArea() );
					break ;
					
				case 2:
					System.out.println("The perimeter of the new triangle is " + user1.getPerimeter());
					break ;
					
				case 3:
					System.out.println("Please input a new triangle for the new object:");
					double A = input.nextDouble();
					double B = input.nextDouble();
					double C = input.nextDouble();
					double D = input.nextDouble();
					double E = input.nextDouble();
					double F = input.nextDouble();
					Triangle2D user2 = new Triangle2D(A,B,C,D,E,F); //declare the class2 to use
					if(user1.contains(user2)){
						System.out.println("The new triangle of new object is in the original triangle of the old object!!!");
					}
					else{
						System.out.println("The new triangle of new object is NOT in the original triangle of the old object!!!");
					}
					break;
					
				case 4:
					System.out.println("Please input a new point :");
					double x = input.nextDouble();
					double y = input.nextDouble();
					MyPoint userpoint = new MyPoint(x,y); //declare the small class:point to use
					if(user1.contains(userpoint)){
						System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!");
					}
					else{
						System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!");
					}
					break;
					
				case 5:
					turnon=0;  // to exit 
					break;
					
				default:
					System.out.println("Please enter correct choose ");
					break;
			}
		}
	}
}
