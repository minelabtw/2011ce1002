package a4.s100502027;

public class Triangle2D {
	private MyPoint op1 = new MyPoint(0.0, 0.0); // points of original triangle triangle
	private MyPoint op2 = new MyPoint(17.0, 6.0);
	private MyPoint op3 = new MyPoint(10.0, 15.0);
	private MyPoint np1, np2, np3; // points of new triangle

	public MyPoint getNP1(){ // get the p1 of new triangle
		return np1;
	}

	public MyPoint getNP2(){ // get the p2 of new triangle
		return np2;
	}

	public MyPoint getNP3(){ // get the p3 of new triangle
		return np3;
	}

	// constructor with arguments to initial the points of the new triangle
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3){
		np1 = new MyPoint (x1,y1);
		np2 = new MyPoint (x2,y2);
		np3 = new MyPoint (x3,y3);
	}

	public double getArea(){ // calculate the area of the new triangle
		double lengthA=np1.distance(np2);
		double lengthB=np2.distance(np3);
		double lengthC=np3.distance(np1);
		double lengthS=(lengthA+lengthB+lengthC)/2;
		double Area=Math.sqrt(lengthS*(lengthS-lengthA)*(lengthS-lengthB)*(lengthS-lengthC));
		return Area;
	}

	public double getPerimeter(){ // calculate the perimeter of the new triangle
		double lengthA=np1.distance(np2);
		double lengthB=np2.distance(np3);
		double lengthC=np3.distance(np1);
		double perimeter = lengthA+lengthB+lengthC;
		return perimeter ;
	}

	// check the input point is in the original triangle of the object or not
	public boolean contains(MyPoint p){
		// if input point p is in the triangle, area of PAB + area of PBC + area of PCA = area of ABC 
		double si1, si2, si3, s, areaPAB, areaPBC, areaPCA, areaABC;

		// calculate area of PAB
		si1=p.distance(op1);
		si2=p.distance(op2);
		si3=op1.distance(op2);
		s=(si1+si2+si3)/2;
		areaPAB=Math.sqrt(s*(s-si1)*(s-si2)*(s-si3));
		// calculate area of PBC
		si1=p.distance(op2);
		si2=p.distance(op3);
		si3=op2.distance(op3);
		s=(si1+si2+si3)/2;
		areaPBC=Math.sqrt(s*(s-si1)*(s-si2)*(s-si3));
		// calculate area of PCA
		si1=p.distance(op3);
		si2=p.distance(op1);
		si3=op3.distance(op1);
		s=(si1+si2+si3)/2;
		areaPCA=Math.sqrt(s*(s-si1)*(s-si2)*(s-si3));
		// calculate area of ABC
		si1=op1.distance(op2);
		si2=op2.distance(op3);
		si3=op3.distance(op1);
		s=(si1+si2+si3)/2;
		areaABC=Math.sqrt(s*(s-si1)*(s-si2)*(s-si3));
		// if areaPAB + areaPBC + areaPCA = areaABC, then p is in the ABC
                // However, there is some error(�~�t) in this calculation
                // So if the absolute value of (areaABC - (areaPAB + areaPBC + areaPCA)) is less than 0.5
                // we consider the point is inside the original triangle
                // you can use Math.abs(double) to calculate the absolute value
		if(Math.abs(areaABC -( areaPAB + areaPBC + areaPCA))<0.5){
			return true;
			}
		else{
			return false;
			}

	}

	// check the input triangle(Triangle2D object) is in the original triangle of this object or not
	public boolean contains(Triangle2D input){
	       // if the input triangle is the original triangle of this object
               // then all points of the input triangle is in the original triangle of this object
		if(input.contains(input.getNP1()) && input.contains(input.getNP2()) && input.contains(input.getNP3())){
			return true ;
		}
		else{
			return false ;
		}
	}
}
