package a4.s100502011;

import java.util.Scanner;

public class A42 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		boolean flag = true; // to judge the loop to run or to stop
		System.out.print("please enter three points :");
		double x1 = input.nextDouble(); //p1x
		double y1 = input.nextDouble(); //p1y
		double x2 = input.nextDouble(); //p2x
		double y2 = input.nextDouble(); //p2y
		double x3 = input.nextDouble(); //p3x
		double y3 = input.nextDouble(); //p3y
		Triangle2D t = new Triangle2D(x1, y1, x2, y2, x3, y3); //return to constructer
		while(flag){ // to run infinitely
			System.out.println("1.get the area of triangle \n"
					+ "2.get the perimeter of the triangle \n"
					+ "3.check the new triangle is in the original triangle or not\n"
					+ "4.enter a point and check the point is in the original triangle or not\n"
					+ "5.exit");
			System.out.print("please enter what information you want to know :");
			int choose = input.nextInt(); // the function you want to choose
			switch (choose) {
			case 1: // get new area
				System.out.println("\n1\n");
				System.out.println("The area of the new triangle is " + t.getArea()+"\n");
				break;
			case 2: // get new perimeter
				System.out.println("\n2\n");
				System.out.println("The perimeter of the new triangle is "+ t.getPerimeter()+"\n");
				break;
			case 3: //judge it is contain or not
				System.out.println("\n3\n");
				System.out.print("Please input a new triangle for the new object:");
				double nx1 = input.nextDouble();
				double ny1 = input.nextDouble();
				double nx2 = input.nextDouble();
				double ny2 = input.nextDouble();
				double nx3 = input.nextDouble();
				double ny3 = input.nextDouble();
				Triangle2D nt = new Triangle2D(nx1, ny1, nx2, ny2, nx3, ny3);
				if (t.contains(nt)) { // contain in original triangle
					System.out.println("The new triangle of new object is in the original triangle of the old object!!!\n");
				} 
				else { //out of original triangle
					System.out.println("The new triangle of new object is out of the original triangle of the old object!!!\n");
				}
				break;
			case 4: // judge the point is in or out
				System.out.println("\n4\n");
				System.out.println("Please input a new point for the new object:");
				double newx1 = input.nextDouble();
				double newy1 = input.nextDouble();
				MyPoint newPoint = new MyPoint(newx1, newy1); // new point
				if (t.contains(newPoint)) { // contain in original triangle
					System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)\n");
				}  // out of original triangle
				else {
					System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!\n");
				}
				break;
			case 5: // exit
				System.out.println("Thanks for your using!!");
				flag = false;
				break;
			default:
				break;
			} // end switch
		} // end while
	} // end main
}
