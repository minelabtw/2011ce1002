package a4.s100502011;

public class MyPoint {

	private double x; // value of x
	private double y; // value of y

	public MyPoint() { // initial value

	}

	public MyPoint(double tx, double ty) { // to set value
		set_X(tx);
		set_Y(ty);
	}

	public double get_X() { // return x of point
		return x;
	}

	public double get_Y() { // return y of point
		return y;
	}

	public void set_X(double inpu_x) { // set x value of point
		x = inpu_x;
	}

	public void set_Y(double inpu_y) { // set y value of point
		y = inpu_y;
	}

	public double distance(MyPoint input) { // the distance of two point
		return Math.sqrt(Math.pow(x - input.x, 2) + Math.pow(y - input.y, 2));
	}

}
