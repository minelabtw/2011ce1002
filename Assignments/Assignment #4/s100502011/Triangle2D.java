package a4.s100502011;

import java.lang.Math;

public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0, 0.0); //point A
	private MyPoint p2 = new MyPoint(17.0, 6.0); //point B
	private MyPoint p3 = new MyPoint(10.0, 15.0); //point C
	private MyPoint p1_new, p2_new, p3_new; // new point

	public MyPoint getP1_new() { // get new point
		return p1_new;
	}

	public MyPoint getP2_new() {  // get new point
		return p2_new;
	}

	public MyPoint getP3_new() {  // get new point
		return p3_new;
	}

	public Triangle2D(double x1, double y1, double x2, double y2, double x3,double y3) { // to set value of point
		p1_new = new MyPoint(x1, y1);
		p2_new = new MyPoint(x2, y2);
		p3_new = new MyPoint(x3, y3);
	}

	public double getArea() { // calculate area of triangle
		return Math.sqrt((getPerimeter() * 0.5)
				* ((getPerimeter() * 0.5) - p1_new.distance(p2_new))
				* ((getPerimeter() * 0.5) - p2_new.distance(p3_new))
				* ((getPerimeter() * 0.5) - p3_new.distance(p1_new)));
	}

	public double getPerimeter() { // calculate perimeter of triangle
		return p1_new.distance(p2_new) + p2_new.distance(p3_new)
				+ p3_new.distance(p1_new);
	}

	public boolean contains(MyPoint p) { // to judge it is in original triangle or not
		double side1, side2, side3, s1, s2, s3, areaPAB, areaPBC, areaPCA, areaABC;
		side1 = p.distance(p1); //PA
		side2 = p.distance(p2); //PB
		side3 = p.distance(p3); //PC
		s1 = p1.distance(p2) + side2 + side1; // PAB
		s2 = p1.distance(p3) + side1 + side3; // PCA
		s3 = p2.distance(p3) + side2 + side3; // PBC
		areaABC = Math.sqrt(((p1.distance(p2) + p2.distance(p3)+ p3.distance(p1)) * 0.5)
				* (((p1.distance(p2) + p2.distance(p3)+ p3.distance(p1)) * 0.5) - p1.distance(p2))
				* (((p1.distance(p2) + p2.distance(p3)+ p3.distance(p1)) * 0.5) - p2.distance(p3))
				* (((p1.distance(p2) + p2.distance(p3)+ p3.distance(p1))* 0.5) - p3.distance(p1)));
		areaPAB = Math.sqrt((s1 * 0.5) * ((s1 * 0.5) - side1)
				* ((s1 * 0.5) - side2) * ((s1 * 0.5) - p1.distance(p2)));
		areaPBC = Math.sqrt((s3 * 0.5) * ((s3 * 0.5) - side2)
				* ((s3 * 0.5) - side3) * ((s3 * 0.5) - p2.distance(p3)));
		areaPCA = Math.sqrt((s2 * 0.5) * ((s2 * 0.5) - side1)
				* ((s2 * 0.5) - side3) * ((s2 * 0.5) - p1.distance(p3)));

		if (Math.abs(areaABC - (areaPAB + areaPBC + areaPCA)) < 0.5) {
			return true;
		} 
		else {
			return false;
		}
	}

	public boolean contains(Triangle2D t) { // to judge it is in original triangle or not
		if (contains(t.getP1_new()) && contains(t.getP2_new()) 
				&& contains(t.getP3_new())) {
			return true;
		} 
		else {
			return false;
		}
	}

}
