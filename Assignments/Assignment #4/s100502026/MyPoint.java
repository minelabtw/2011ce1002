package a4.s100502026;

import java.lang.Math;

public class MyPoint 
{
	//Pts of original triangle
	private static double[] PtsOfOriginal1=new double[2];
	private static double[] PtsOfOriginal2=new double[2];
	private static double[] PtsOfOriginal3=new double[2];
	
	//Pts of new triangle
	private static double[] PtsOfnew1=new double[2];
	private static double[] PtsOfnew2=new double[2];
	private static double[] PtsOfnew3=new double[2];
	
	//Constructor with initialing pts
	MyPoint( double[] p1 , double[] p2 , double[] p3 )
	{
		PtsOfOriginal1[0]=0;
		PtsOfOriginal1[1]=0;
		PtsOfOriginal2[0]=10;
		PtsOfOriginal2[1]=15;
		PtsOfOriginal3[0]=17;
		PtsOfOriginal3[1]=6;
		
		PtsOfnew1=p1;
		PtsOfnew2=p2;
		PtsOfnew3=p3;
	}
	
	//Method for calculate area of triangle
	public static double getArea(double[] point1,double[] point2,double[] point3)
	{
		double s=0;
		s=( getLength(point1,point2) + getLength(point2,point3) + getLength(point1,point3) )/2;
		return Math.pow( s*( s-getLength(point1,point2) )*( s-getLength(point3,point2) )*( s-getLength(point1,point3) ) , 0.5 );
	}

	//Method for calculate perimeter of two pts
	public static double getPerimeter(double[] point1,double[] point2,double[] point3)
	{
		return getLength(point1,point2) + getLength(point2,point3) + getLength(point1,point3);
	}
	
	//Method for judge a point is in a triangle or not
	public static boolean contains(double[] point1,double[] point2,double[] point3,double[] point)
	{
		if( getArea(point1,point2,point) + getArea(point1,point3,point) + getArea(point3,point2,point) != getArea(point1,point2,point3) )
			return false;
		else
			return true;
	}
	
	//Method for judge a triangle is in a triangle or not
	public static boolean contains(double[] point1,double[] point2,double[] point3,double[] point4,double[] point5,double[] point6)
	{
		if( contains(point1,point2,point3,point4) && contains(point1,point2,point3,point5) && contains(point1,point2,point3,point6) )
			return true;
		else
			return false;
	}

	//Method for calculate length of two pts
	public static double getLength(double[] point1,double[] point2)
	{
		return Math.pow( Math.pow( (point1[0]-point2[0]) , 2 ) + Math.pow( (point1[1] - point2[1]), 2 ) , 0.5);	
	}

	//Method for get PtsOfOriginal1
	public static double[] getPtsOfOriginal1()
	{
		return PtsOfOriginal1;
	}
	
	//Method for get PtsOfOriginal2
	public static double[] getPtsOfOriginal2()
	{
		return PtsOfOriginal2;
	}
	
	//Method for get PtsOfOriginal3
	public static double[] getPtsOfOriginal3()
	{
		return PtsOfOriginal3;
	}
}
