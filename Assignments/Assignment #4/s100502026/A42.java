package a4.s100502026;

import java.util.Scanner;

public class A42 {
	
	public static Scanner input=new Scanner(System.in);
	
	public static void main(String[] args)
	{
		//Declare
		double[] point1=new double[2];
		double[] point2=new double[2];
		double[] point3=new double[2];
		double[] point4=new double[2];
		boolean exit=true;
		int choice;
		
		//Enter pts
		System.out.println("Please enter three points of the new triangle:");
		point1[0]=input.nextDouble();
		point1[1]=input.nextDouble();
		point2[0]=input.nextDouble();
		point2[1]=input.nextDouble();
		point3[0]=input.nextDouble();
		point3[1]=input.nextDouble();
		
		MyPoint points = new MyPoint(point1,point2,point3);
		
		//Choose functions
		System.out.println("1.get the area of the new triangle");
		System.out.println("2.get the perimeter of the new triangle");
		System.out.println("3.check the new triangle is in the original triangle or not");
		System.out.println("4.enter a point and check the point is in the original triangle or not");
		System.out.println("5.Exit");
		
		//Do the functions
		while(exit)
		{
			choice=input.nextInt();
			
			switch(choice)
			{
				case 1:
					System.out.println( "The area of the new triangle is " + points.getArea(point1, point2, point3) );
					System.out.println();
					break;
					
				case 2:
					System.out.println("The perimeter of the new triangle is " + points.getPerimeter(point1, point2, point3) );
					System.out.println();
					break;
					
				case 3:
					if(points.contains( points.getPtsOfOriginal1() , points.getPtsOfOriginal2() , points.getPtsOfOriginal3() , point1 , point2 ,point3))
					{
						System.out.println("The new triangle is in the original triangle!!!");
					}
					else
					{
						System.out.println("The new triangle is out the original triangle!!!");
					}
					System.out.println();
					break;
					
				case 4:
					point4[0]=input.nextDouble();
					point4[1]=input.nextDouble();
					if(points.contains( points.getPtsOfOriginal1() , points.getPtsOfOriginal2() , points.getPtsOfOriginal3() , point4 ))
					{
						System.out.println("The new point is in the original triangle!!!");
					}
					else
					{
						System.out.println("The new point is NOT in the original triangle!!!");
					}
					System.out.println();
					break;
					
				case 5:
					exit=false;
					break;
			}			
		}		
	}

}
