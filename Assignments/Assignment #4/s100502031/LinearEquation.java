package a4.s100502031;

public class LinearEquation {
	//data field 
		private double a;
		private double b;
		private double c;
		private double d;
		private double e;
		private double f;
		
		
		//constructor
		public  LinearEquation(double Anumber,double Bnumber,double Cnumber,double Dnumber,double Enumber,double Fnumber){ 
			//asign the data field
			a=Anumber;
			b=Bnumber;
			c=Cnumber;
			d=Dnumber;
			e=Enumber;
			f=Fnumber;
			
			
			
		}
		
		//the method to get the data field
		public double getA(){  
			return a;
		}
		public double getB(){  
			return b;
		}
		public double getC(){ 
			return c;
		}
		public double getD(){  
			return d;
		}
		public double getE(){  
			return e;
		}
		public double getF(){  
			return f;
		}
		
		//method to identify wether the answer is exist or not
		public boolean isSolvable(){
			/*
			//if it exist,then return true
			if(a*d-b*c!=0){
				return true;
			}
			//if not,return error
			else{
				return false;
			}
			*/
			return a*d-b*c!=0;
		}
		//caculate x
		public double getX(){
			double x;
			x=(e*d-b*f)/(a*d-b*c);
			return x;
		}
		//caculate y
		public double getY(){
			double y;
			y=(a*f-e*c)/(a*d-b*c);
			return y;
		}

	}


