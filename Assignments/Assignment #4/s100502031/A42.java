package a4.s100502031;

import java.util.Scanner;

public class A42 {
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);
		System.out.println("Input three point: ");
		double number1=input.nextDouble();
		double number2=input.nextDouble();
		double number3=input.nextDouble();
		double number4=input.nextDouble();
		double number5=input.nextDouble();
		double number6=input.nextDouble();
		
		int choice;
		
		boolean exitFlag=false;
		while(exitFlag==false){
			System.out.println("Chose the function !!");
			System.out.print("1.Area;2.Perimeter;3.check the new triangle is in the original triangle or not;4.enter a point and check the point is in the original triangle or not");
			choice=input.nextInt();
			Triangle2D TD=new Triangle2D(number1,number2,number3,number4,number5,number6);
			switch(choice){
				//1.get the area of the new triangle
				case 1:
					System.out.println("The area is "+TD.getArea());
					break;
				//2.get the perimeter of the new triangle
				case 2:
					System.out.println("The perimeter is " +TD.getPerimeter());
					break;
				//3.check the new triangle is in the original triangle or not
				case 3:
					double []a=new double[6];
					
					for(int i=0;i<6;i++){
						a[i]=input.nextDouble();
					}
					Triangle2D TDF=new Triangle2D(a[0],a[1],a[2],a[3],a[4],a[5]);
					Triangle2D original = new Triangle2D(TDF.getP1(),TDF.getP2(),TDF.getP3());
					if(original.contains(TDF)){
						System.out.println("Inside");
					}
					else{
						System.out.println("OutSide");
					}
					break;
				//4.enter a point and check the point is in the original triangle or not
				case 4:
					double []b=new double[2];
					
					for(int i=0;i<2;i++){
						b[i]=input.nextDouble();
					}
					
					Triangle2D original2 = new Triangle2D(TD.getP1(),TD.getP2(),TD.getP3());
					if(original2.contains(new MyPoint(b[0],b[1]))){
						System.out.println("Inside");
					}
					else{
						System.out.println("OutSide");
					}
					
					
					break;
				//5.Exit
				case 5:
					System.out.println("The end!! Thank you!!");
					exitFlag=true;
					break;
				default:
					break;
			}
		
	}

}
}







