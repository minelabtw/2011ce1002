package a4.s100502031;


public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0, 0.0); // points of original triangle triangle
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	
	private MyPoint p1_new, p2_new, p3_new; // points of new triangle
	
	public MyPoint getP1(){
		return p1;
	}
	public MyPoint getP2(){
		return p2;
	}
	public MyPoint getP3(){
		return p3;
	}

	public MyPoint getP1_new(){ // get the p1 of new triangle
		return p1_new;
	}

	public MyPoint getP2_new(){ // get the p2 of new triangle
		return p2_new;
	}

	public MyPoint getP3_new(){ // get the p3 of new triangle
		return p3_new;
	}

	// constructor with arguments to initial the points of the new triangle
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3){
		p1_new=new MyPoint(x1,y1);
		p2_new=new MyPoint(x2,y2);
		p3_new=new MyPoint(x3,y3);
	}
	public Triangle2D(MyPoint a,MyPoint b,MyPoint c){
		p1_new=a;
		p2_new=b;
		p3_new=c;
	}

	public double getArea(){ // calculate the area of the new triangle
		double L1=p1_new.distance(p2_new);
		double L2=p2_new.distance(p3_new);
		double L3=p3_new.distance(p1_new);
		double S=(L1+L2+L3)/2;
		double Area=Math.sqrt(S*(S-L1)*(S-L2)*(S-L3));
		System.err.println(L1+" "+L2+" "+L3);
		return Area;
		
	}

	public double getPerimeter(){ // calculate the perimeter of the new triangle
		double L1=p1_new.distance(p2_new);
		double L2=p2_new.distance(p3_new);
		double L3=p3_new.distance(p1_new);
		double Perimeter=L1+L2+L3;
		return Perimeter;
	}

	// check the input point is in the original triangle of the object or not
	public boolean contains(MyPoint p){
		MyPoint AP=p1_new.Dis(p);
		MyPoint AB=p1_new.Dis(p2_new);
		MyPoint AC=p1_new.Dis(p3_new);
		
		LinearEquation Equation =new LinearEquation(AB.getX(),AC.getX(),AB.getY(),AC.getY(),AP.getX(),AP.getY());
		
		double m=Equation.getX();
		double n=Equation.getY();
		if(m>=0&&n>=0&&m+n<=1){
			return true;
		}
		else{
			return false;
		}
		
	}

	// check the input triangle(Triangle2D object) is in the original triangle of this object or not
	public boolean contains(Triangle2D input){
	       // if the input triangle is the original triangle of this object
               // then all points of the input triangle is in the original triangle of this object
		return contains(p1_new) && contains(p2_new) && contains(p3_new);
	}
}

