package a4.s100502031;


public class MyPoint {
	private double x;
	private double y;

	public MyPoint(){
		x=0;
		y=0;
	}

	// constructor to initial private data member
	public MyPoint(double x_input, double y_input){ 
		x=x_input;
		y=y_input;
	}

	// return x value
	public double getX(){ 
		return x;
	}
	
	// return y value
	public double getY(){ 
		return y;
	}
	
	// set x value
	public void setX(double input) 
	{
		x=input;
		
	}
	
	// set y value
	public void setY(double input) 
	{
		y=input;
	}
	
	public MyPoint getMove(double dx,double dy){
		MyPoint pn=new MyPoint(x+dx,y+dy);
		return pn;
	}

	public double distance(MyPoint input){ // the distance between this point and the input point
		
		double delX = x-input.getX();
		double delY = y-input.getY();
		
		double Distance=Math.sqrt(delX*delX+delY*delY);
		return Distance;
	}
	public MyPoint Dis(MyPoint input){
		return new MyPoint(input.getX()-x,input.getY()-y);
	}
}



