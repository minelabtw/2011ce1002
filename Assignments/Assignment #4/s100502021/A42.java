package a4.s100502021;
import java.util.Scanner;
public class A42 {
	public static void main(String [] argv){
		Scanner input = new Scanner(System.in); 
		System.out.println("Please enter three points of the new triangle : ");
		Double a=input.nextDouble(); //enter point
		Double b=input.nextDouble();
		Double c=input.nextDouble();
		Double d=input.nextDouble();
		Double e=input.nextDouble();
		Double f=input.nextDouble();
		Triangle2D tri=new Triangle2D(a,b,c,d,e,f);			
		for(int i=1;i>0;i++){	
			System.out.println("1.get the area of the new triangle");
			System.out.println("2.get the perimeter of the new triangle");
			System.out.println("3.check the new triangle is in the original triangle or not");
			System.out.println("4.enter a point and check the point is in the original triangle or not");
			System.out.println("5.Exit");
			int cho=input.nextInt();
			switch(cho){
			case 1:
				System.out.println("The area of the new triangle is "+tri.getArea());
				break;
			case 2:
				System.out.println("The perimeter of the new triangle is "+tri.getPerimeter());
				break;
			case 3:
				System.out.println("Please input a new triangle for the new object : ");
				Double a2=input.nextDouble(); //enter test triangle
				Double b2=input.nextDouble();
				Double c2=input.nextDouble();
				Double d2=input.nextDouble();
				Double e2=input.nextDouble();
				Double f2=input.nextDouble();
				Triangle2D tri2=new Triangle2D(a2,b2,c2,d2,e2,f2);				
				if(tri2.contains2()==true){
					System.out.println("The new triangle of new object is in the original triangle of the old object!!!");
				}
				else
					System.out.println("The new triangle of new object is not in the original triangle of the old object!!!");
				break;
			case 4:
				System.out.println("enter p point : ");
				Double px=input.nextDouble();
				Double py=input.nextDouble();
				MyPoint p=new MyPoint (px,py);
				if(tri.contains(p)==true){
					System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!");
				}
				else
					System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!");
				break;
			case 5:
				i=-10;
				System.out.println("byebye~");
				break;
			}
		}			
	}
}
