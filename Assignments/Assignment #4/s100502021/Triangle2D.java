package a4.s100502021;

public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0, 0.0); // points of original triangle triangle
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint newP1, newP2, newP3; // points of new triangle
	
	public MyPoint getnewP1(){ // get the p1 of new triangle
		return newP1;
	}

	public MyPoint getnewP2(){ // get the p2 of new triangle
		return newP2;
	}

	public MyPoint getnewP3(){ // get the p3 of new triangle
		return newP3;
	}

	// constructor with arguments to initial the points of the new triangle
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3){
		newP1=new MyPoint(x1,y1);
		newP2=new MyPoint(x2,y2);
		newP3=new MyPoint(x3,y3);
	}

	public double getArea(){ // calculate the area of the new triangle
		double s=(newP1.distance(newP2)+newP1.distance(newP3)+newP2.distance(newP3))/2;
		return Math.sqrt(s*(s-newP1.distance(newP2))*(s-newP1.distance(newP3))*(s-newP2.distance(newP3)));
	}

	public double getPerimeter(){ // calculate the perimeter of the new triangle
		return (newP1.distance(newP2)+newP1.distance(newP3)+newP2.distance(newP3));
	}

	// check the input point is in the original triangle of the object or not
	public boolean contains(MyPoint p){
		// if input point p is in the triangle, area of PAB + area of PBC + area of PCA = area of ABC 
		double side1, side2, side3, s, areaPAB, areaPBC, areaPCA, areaABC;
		side1=p1.distance(p2);
		side2=p1.distance(p3);
		side3=p2.distance(p3);
		s=(side1+side2+side3)/2;
		
		double s1=(p.distance(p1)+p.distance(p2)+p1.distance(p2))/2;
		areaPAB=Math.sqrt(s1*(s1-p.distance(p1))*(s1-p.distance(p2))*(s1-p1.distance(p2)));//area of PAB

		double s2=(p.distance(p1)+p.distance(p3)+p1.distance(p3))/2;
		areaPBC=Math.sqrt(s2*(s2-p.distance(p1))*(s2-p.distance(p3))*(s2-p1.distance(p3)));//area of PBC

		double s3=(p.distance(p2)+p.distance(p3)+p2.distance(p3))/2;
		areaPCA=Math.sqrt(s3*(s3-p.distance(p2))*(s3-p.distance(p3))*(s3-p2.distance(p3)));//area of PCA

		areaABC=Math.sqrt(s*(s-side1)*(s-side2)*(s-side3));//area of ABC

		if(Math.abs(areaABC-(areaPAB+areaPBC+areaPCA))<0.5){
			return true;
		}
		else
			return false;
	}
	// check the input triangle(Triangle2D object) is in the original triangle of this object or not
	public boolean contains2(){

		if(contains(newP1)==true&&contains(newP2)==true&&contains(newP2)==true){  //confirm all points of the input triangle is in the original triangle of this object
			return true;
		}
		else
			return false; 
               
	}

}
