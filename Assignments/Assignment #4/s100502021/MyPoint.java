package a4.s100502021;
import java.lang.Math;
public class MyPoint {
	private double x;	
	private double y;

	public MyPoint(){
		x=0.0;
		y=0.0;
	}
	public MyPoint(double inputx, double inputy){ // constructor to initial private data member
		setX(inputx);
		setY(inputy);
	}
	
	public void setX(double input) // set x value
	{
		x=input;
	}

	public void setY(double input) // set y value
	{
		y=input;
	}
	

	public double getX(){ // return x value
		return x; 
	}

	public double getY(){ // return y value
		return y;
	}	

	public double distance(MyPoint input){ // the distance between this point and the input point
		return Math.sqrt(Math.pow(x-input.getX(),2)+Math.pow(y-input.getY(),2));
	}
}
