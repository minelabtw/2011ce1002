package a4.s100502009;

public class MyPoint {
	private double x;
	private double y;

	public MyPoint()
	{
	}

	public MyPoint(double x_input, double y_input)// constructor to initial private data member
	{
		setX(x_input);
		setY(y_input);
	}

	public double getX()// return x value
	{ 
		return x;
	}

	public double getY()// return y value
	{ 
		return y;
	}

	public void setX(double input) // set x value
	{
		x=input;
	}

	public void setY(double input) // set y value
	{
		y=input;
	}

	public double distance(MyPoint input) // the distance between this point and the input point
	{
		double a=input.getX();
		double b=input.getY();
		return Math.sqrt((x-a)*(x-a)+(y-b)*(y-b));
	}
}