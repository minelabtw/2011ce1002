package a4.s100502009;

public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0, 0.0); // points of original triangle triangle
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint p1_new, p2_new, p3_new; // points of new triangle

	public MyPoint getP1_new() // get the p1 of new triangle
	{
		return p1_new;
	}

	public MyPoint getP2_new() // get the p2 of new triangle
	{
		return p2_new;
	}

	public MyPoint getP3_new() // get the p3 of new triangle
	{
		return p3_new;
	}

	// constructor with arguments to initial the points of the new triangle
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3)
	{
		p1_new=new MyPoint(x1,y1);
		p2_new=new MyPoint(x2,y2);
		p3_new=new MyPoint(x3,y3);
	}
	double s1,s2,s3;
	public double getPerimeter()//get the perimeter of the new triangle
	{
		s1=p1_new.distance(p2_new);
		s2=p2_new.distance(p3_new);
		s3=p3_new.distance(p1_new);
		return s1+s2+s3;
	}
	public double getArea()//get the area of the new triangle
	{
		double r=getPerimeter()/2;
		return Math.sqrt(r*(r-p1_new.distance(p2_new))*(r-p2_new.distance(p3_new))*(r-p3_new.distance(p1_new)));
	}
	public boolean contains(MyPoint p)//tell the input point is in the original triangle or not
	{
		double a,b,c,d,A1,A2,A3,A4;
		a=p1.distance(p);
		b=p1.distance(p2);
		c=p2.distance(p);
		d=(a+b+c)/2;
		A1=Math.sqrt(d*(d-a)*(d-b)*(d-c));
		a=p1.distance(p);
		b=p1.distance(p3);
		c=p3.distance(p);
		d=(a+b+c)/2;
		A2=Math.sqrt(d*(d-a)*(d-b)*(d-c));
		a=p2.distance(p);
		b=p3.distance(p2);
		c=p3.distance(p);
		d=(a+b+c)/2;		
		A3=Math.sqrt(d*(d-a)*(d-b)*(d-c));
		a=p2.distance(p1);
		b=p3.distance(p2);
		c=p3.distance(p1);
		d=(a+b+c)/2;		
		A4=Math.sqrt(d*(d-a)*(d-b)*(d-c));
		if(Math.abs(A4-(A1+A2+A3))<0.5)
			return true;
		else
			return false;
	}
	public boolean contains(Triangle2D t)//tell the new triangle of the new object is in the original triangle or not
	{
		boolean check1=contains(t.getP1_new());
		boolean check2=contains(t.getP2_new());
		boolean check3=contains(t.getP3_new());
		boolean check=false;
		if(check1==true&&check2==true&&check3==true)
			check=true;
		return check;
	}
}
