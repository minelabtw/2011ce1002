package a4.s100502009;
import java.util.Scanner;

public class A42 {
	public static void main(String[] args)
	{
		Scanner input= new Scanner(System.in);
		System.out.println("\nPlease enter three points of the new triangle:");
		double a=input.nextDouble();
		double b=input.nextDouble();
		double c=input.nextDouble();
		double d=input.nextDouble();
		double e=input.nextDouble();
		double f=input.nextDouble();
		Triangle2D t1=new Triangle2D(a,b,c,d,e,f);//declare an object to input the new triangle
		for(int i=1;i>0;i++)
		{
			System.out.println("Please choose a service: ");
			System.out.println("1.get the area of the new triangle"
				+"\n2.get the perimeter of the new triangle"
				+"\n3.check the new triangle is in the original triangle or not"
				+"\n4.enter a point and check the point is in the original triangle or not"
				+"\n5.Exit");
			int choose=input.nextInt();
			switch(choose)
			{
				case 1:
					System.out.print("The area of the new triangle is: "+t1.getArea());
					break;
				case 2:
					System.out.print("The perimeter of the new triangle is: "+t1.getPerimeter());
					break;
				case 3:
					System.out.println("Please input a new triangle for the new object:");
					double a1=input.nextDouble();
					double b1=input.nextDouble();
					double c1=input.nextDouble();
					double d1=input.nextDouble();
					double e1=input.nextDouble();
					double f1=input.nextDouble();
					Triangle2D t2=new Triangle2D(a1,b1,c1,d1,e1,f1);//declare a new object to input the new triangle of the new object
					if(t1.contains(t2)==true)
						System.out.println("The new triangle of new object is in the original triangle of the old object!!!");
					else
						System.out.println("The new triangle of new object is NOT in the original triangle of the old object!!!");
					break;
				case 4://to check the input point is in the original triangle or not
					System.out.print("Ente a point: ");
					double x=input.nextDouble();
					double y=input.nextDouble();
					MyPoint mp=new MyPoint(x,y);
					if(t1.contains(mp)==true)
						System.out.print("The new point is in the original triangle!!!");
					else
						System.out.print("The new point is NOT in the original triangle (0,0) (17,6) (10,15)!!!");
					break;
				case 5://exit
					i=-1;
					System.out.print("Bye~");
					break;
				default:
					System.out.println("Error: invalid status");
					System.exit(0);
					break;				
			}
		}
	}
}
