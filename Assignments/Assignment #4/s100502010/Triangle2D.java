package a42.s100502010;


public class Triangle2D 
{
	private MyPoint p1 = new MyPoint(0.0, 0.0); //原三角形座標
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint p1_new, p2_new, p3_new; //新三角形座標物件宣告
	
	public MyPoint getP1_new()
	{ 
		return p1_new;
	}

	public MyPoint getP2_new()
	{ 
		return p2_new;
	}

	public MyPoint getP3_new()
	{ 
		return p3_new;
	}

	
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3)
	{
		p1_new=new MyPoint(x1,y1);
		p2_new=new MyPoint(x2,y2);
		p3_new=new MyPoint(x3,y3);
	}
	public double getArea()
	{ // 海龍公式算面積
		return Math.sqrt((p1_new.distance(p2_new)+p2_new.distance(p3_new)+p3_new.distance(p1_new))/2*
				((p1_new.distance(p2_new)+p2_new.distance(p3_new)+p3_new.distance(p1_new))/2-p1_new.distance(p2_new))*
				((p1_new.distance(p2_new)+p2_new.distance(p3_new)+p3_new.distance(p1_new))/2-p2_new.distance(p3_new))*
				((p1_new.distance(p2_new)+p2_new.distance(p3_new)+p3_new.distance(p1_new))/2-p1_new.distance(p3_new)));				
	}
	public double getPerimeter()
	{ //算周長
		return p1_new.distance(p2_new)+p2_new.distance(p3_new)+p3_new.distance(p1_new);
	}

	
	public boolean contains(MyPoint point)// 算一個點是否在元三角形中
	{
		double side1, side2, side3, s, areaPAB, areaPBC, areaPCA, areaABC,ab,bc,ac;
		side1 = point.distance(p1);
		side2 = point.distance(p2);
		side3 = point.distance(p3);
		ab=p1.distance(p2);
		bc=p1.distance(p3);
		ac=p2.distance(p3);
		
		areaPAB=Math.sqrt((side1+side2+ab)/2*(((side1+side2+ab)/2)-side1)*(((side1+side2+ab)/2)-side2)*(((side1+side2+ab)/2)-ab));
		areaPBC=Math.sqrt((side1+side3+bc)/2*(((side1+side3+bc)/2)-side1)*(((side1+side3+bc)/2)-side3)*(((side1+side3+bc)/2)-bc));
		areaPCA=Math.sqrt((side3+side2+ac)/2*(((side3+side2+ac)/2)-side2)*(((side3+side2+ac)/2)-side3)*(((side3+side2+ac)/2)-ac));

		areaABC=Math.sqrt((ab+bc+ac)/2*(((ab+bc+ac)/2)-ab)*(((ab+bc+ac)/2)-bc)*(((ab+bc+ac)/2)-ac));
		if(Math.abs(areaABC-areaPAB-areaPBC-areaPCA)<0.5)
		{
			return true;	
		}
		else
		{
			return false;
		}
	}
	public boolean contains(Triangle2D input)//若三個點都在原三角形中澤鑫三角形在元三角形中
	{
		int counter1=0;
		if(contains(input.getP1_new())==true)
		{
			counter1++;
		}
		if(contains(input.getP1_new())==true)
		{
			counter1++;
		}
		if(contains(input.getP1_new())==true)
		{
			counter1++;
		}
		if(counter1==3)
		{
			return true;
		}
		else
		{
			return false;
		}
	      
	}
}
