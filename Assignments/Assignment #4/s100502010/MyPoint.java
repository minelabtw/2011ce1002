package a42.s100502010;

public class MyPoint 
{
	private double x;
	private double y;

	public MyPoint()
	{
		x=0;//原始值
		y=0;
	}
	public MyPoint(double x_input, double y_input)
	{ //將傳入constructor的值傳至變數
		setX(x_input);
		setY(y_input);
	}

	public double getX()
	{ 
		return x;
	}

	public double getY()
	{ 
		return y;
	}

	public void setX(double input) 
	{
		x=input;

	}
	public void setY(double input) 
	{
		y=input;
	}

	public double distance(MyPoint input)
	{ //計算兩點間距離
			return Math.sqrt((x-input.x)*(x-input.x)+(y-input.y)*(y-input.y));
	}
}
