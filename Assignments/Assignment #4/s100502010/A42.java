package a42.s100502010;
import java.util.Scanner;
import a42.s100502010.Triangle2D;
import a42.s100502010.MyPoint;

public class A42 
{
	public static void main(String[] args)
	{
		System.out.println("Please enter three points of the new triangle:");
		Scanner input=new Scanner(System.in);//讀取新三角形的座標
		int x1=input.nextInt();
		int y1=input.nextInt();
		int x2=input.nextInt();
		int y2=input.nextInt();
		int x3=input.nextInt();
		int y3=input.nextInt();

		System.out.println("1.get the area of the new triangle");//選單
		System.out.println("2.get the perimeter of the new triangle");
		System.out.println("3.check the new triangle is in the original triangle or not");
		System.out.println("4.enter a point and check the point is in the original triangle or not");
		System.out.println("5.exit");
		
		Triangle2D triangle=new Triangle2D(x1,y1,x2,y2,x3,y3); //宣告物件
		
		for(int counter=0;counter<100;counter++)
		{
		
			int choose=input.nextInt();
			
			switch(choose)
			{
				case 1://功能一
				{
					System.out.println("The area of the new triangle is:"+triangle.getArea());
					counter=0;
					break;
				}
				case 2://功能二
				{
					System.out.println("The perimeter of the new triangle is:"+triangle.getPerimeter());
					counter=0;
					break;
				}
				case 3://功能三
				{
					System.out.println("Please input a new triangle for the new object:");
					double newx1=input.nextDouble();
					double newy1=input.nextDouble();
					double newx2=input.nextDouble();
					double newy2=input.nextDouble();
					double newx3=input.nextDouble();
					double newy3=input.nextDouble();
					
					Triangle2D judge=new Triangle2D(newx1,newy1,newx2,newy2,newx3,newy3);
					if(triangle.contains(judge)==true)
					{
						System.out.println("the new triangle is inside of the original one!!!");
					}
					else
					{
						System.out.println("the new triangle is out side of the original one!! ");
					}
					counter=0;
					break;
				}
					
				case 4://功能四
				{
					System.out.println("Please input a new point:");
					double px=input.nextDouble();
					double py=input.nextDouble();
					MyPoint onepoint=new MyPoint(px,py);
					if (triangle.contains(onepoint)) 
					{
						System.out.println("The new point is in the original triangle!!!");
					} 
					else 
					{
						System.out.println("The new point is NOT in the original triangle!!!");
					}
					
					counter=0;
					break;
				}
					
				case 5://跳出
				{
					System.out.println("Good Bye");
					counter=200;
					break;
				}
			}
		}
	}
}
