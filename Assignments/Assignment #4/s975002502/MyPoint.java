package a4.s975002502;
import java.lang.Math;

public class MyPoint {
    private double x;
    private double y;

    public MyPoint() {}

    // constructor to initial private data member
    public MyPoint(double x_input, double y_input) {
        x = x_input;
        y = y_input;
    }
	
    // return x value
    public double getX() {
    	return x;
	}
    
    // return y value
    public double getY() {
    	return y;
    }
    
    // set x value
    public void setX(double input) {
    	this.x = input;
	}
    
    // set y value 
    public void setY(double input) {
    	this.y = input;
    }

    // the distance between this point and the input point
    public double distance(MyPoint secondPoint) {
        return distance(this, secondPoint);
    }

    // the distance between two points
    public static double distance(MyPoint p1, MyPoint p2) {
        return Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
    }

}
