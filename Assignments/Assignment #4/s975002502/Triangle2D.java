package a4.s975002502;
import java.lang.Math;

public class Triangle2D {
	// points of original triangle triangle
	private MyPoint A_Ori = new MyPoint(0.0, 0.0); 
	private MyPoint B_Ori = new MyPoint(17.0, 6.0);
	private MyPoint C_Ori = new MyPoint(10.0, 15.0);
	
	// points of new triangle triangle
	private MyPoint A_new = new MyPoint();
	private MyPoint B_new = new MyPoint();
	private MyPoint C_new = new MyPoint();
	
	// constructor with arguments to initial the points of triangle(read Coordinates)
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3){
		// Set the Coordinates of MyPoint A_new
		A_new.setX(x1);
		A_new.setY(y1);
		
		// Set the Coordinates of MyPoint B_new
		B_new.setX(x2);
		B_new.setY(y2);
		
		// Set the Coordinates of MyPoint C_new
		C_new.setX(x3);
		C_new.setY(y3);
	}
	
	// constructor with arguments to initial the points of triangle(read Points)
	public Triangle2D(MyPoint A, MyPoint B, MyPoint C){
		// Set the Points of the triangle
		A_new = A;
		B_new = B;
		C_new = C;
	}
	
	// get the p1 of new triangle
	public MyPoint getP1_new(){ 
		return A_new;
	}

	// get the p2 of new triangle
	public MyPoint getP2_new(){ 
		return A_new;
	}
	
	// get the p3 of new triangle
	public MyPoint getP3_new(){ 
		return A_new;
	}
	
	// calculate the area of the new triangle
	public double getArea(){ 
		double s = this.getPerimeter()*(0.5);		// The s on the Heron Formula
		double AB, AC, BC;				// The three side lengths of the triangle
		
		// get the three side lengths of the triangle
		AB = A_new.distance(B_new);
		AC = A_new.distance(C_new);
		BC = B_new.distance(C_new);
		
		//return the Area of the triangle by using the Heron Formula
		return 	Math.sqrt(s*(s-AB)*(s-AC)*(s-BC));
	}

	// calculate the perimeter of the new triangle
	public double getPerimeter(){ 
		double AB, AC, BC;				// The three side lengths of the triangle
		
		// get the three side lengths of the triangle
		AB = A_new.distance(B_new);
		AC = A_new.distance(C_new);
		BC = B_new.distance(C_new);
		
		//return the Perimeter of the triangle
		return AB+AC+BC;
	}
	
	// check the input triangle(Triangle2D object) is in the original triangle of this object or not
	public boolean contains(Triangle2D input){
	    // if the input triangle is the original triangle of this object
        // then all points of the input triangle is in the original triangle of this object
		if((this.contains(input.A_new)&&this.contains(input.B_new)&&this.contains(input.C_new))==true){
			return true;
		}
		else{
			return false;
		}
	}
	
	// check the input point is in the original triangle of the object or not
	public boolean contains(MyPoint P){
		// if input point P is in the triangle, area of PAB + area of PBC + area of PCA = area of ABC
		
		double areaPAB, areaPBC, areaPCA, areaABC, error;
		
		// construct the triangles that will be used
	    Triangle2D PAB = new Triangle2D(P, A_Ori, B_Ori);
        Triangle2D PCA = new Triangle2D(P, C_Ori, A_Ori);
        Triangle2D PBC = new Triangle2D(P, B_Ori, C_Ori);
        Triangle2D ABC = new Triangle2D( A_Ori, B_Ori, C_Ori);

		// calculate area of PAB
       	areaPAB = PAB.getArea();

		// calculate area of PCA
       	areaPCA = PCA.getArea();
       	
		// calculate area of PBC
       	areaPBC = PBC.getArea();

		// calculate area of ABC
       	areaABC = ABC.getArea();

		// if areaPAB + areaPBC + areaPCA = areaABC, then p is in the ABC
        // However, there is some error(�~�t) in this calculation
        // So if the absolute value of (areaABC - (areaPAB + areaPBC + areaPCA)) is less than 0.5
        // we consider the point is inside the original triangle
        // you can use Math.abs(double) to calculate the absolute value
       	error = Math.abs(areaABC - (areaPAB + areaPBC + areaPCA));
		
		if(error < 0.5){
			return true;
		}
		else{
			return false;
		}
	}
}

