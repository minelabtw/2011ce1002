package a4.s975002502;
import java.util.Scanner;

public class A42 {
	public static void main(String[] args) {
		double x1, y1, x2, y2, x3, y3;			// Coordinates of three points
		
        Scanner input = new Scanner(System.in); // new a Scanner object
        
		System.out.println("Please enter three points of the new triangle: ");
		x1 = input.nextDouble();
		y1 = input.nextDouble();
		x2 = input.nextDouble();
		y2 = input.nextDouble();
		x3 = input.nextDouble();
		y3 = input.nextDouble();
        
		System.out.println("");
        System.out.println("1.get the area of the new triangle");
        System.out.println("2.get the perimeter of the new triangle");
        System.out.println("3.check the new triangle is in the original triangle or not");
        System.out.println("4.enter a point and check the point is in the original triangle or not");
        System.out.println("5.Exit");
        System.out.println("");
        
        int func; // the chosen function
        
        // construct the new triangle
        Triangle2D tri1 = new Triangle2D(x1, y1, x2, y2, x3, y3);
        
        // executing the functions
        do{
        	func = input.nextInt();
        	        	
        	switch(func){
        		// get the area of the new triangle
        		case 1:
        			System.out.println("The area of the new triangle is "+tri1.getArea());
        			break;
        		// get the perimeter of the new triangle
        		case 2:
        			System.out.println("The perimeter of the new triangle is "+tri1.getPerimeter());
        			break;
        		// check the new triangle is in the original triangle or not
        		case 3:
        			System.out.println("Please input a new triangle for the new object: ");
        			x1 = input.nextDouble();
        			y1 = input.nextDouble();
        			x2 = input.nextDouble();
        			y2 = input.nextDouble();
        			x3 = input.nextDouble();
        			y3 = input.nextDouble();
        			
        			Triangle2D tri2 = new Triangle2D(x1, y1, x2, y2, x3, y3);
        			
        			if(tri1.contains(tri2)==true){
        				System.out.println("The new triangle of new object is in the original triangle of the old object!!!");
        			}
        			else{
        				System.out.println("The new triangle of new object is NOT in the original triangle of the old object!!!");
        			}
        			break;
        		//enter a point and check the point is in the original triangle or not
        		case 4:
        	        MyPoint P = new MyPoint();
        	        P.setX(input.nextDouble());
        			P.setY(input.nextDouble());
	                
        			if(tri1.contains(P)==true){
        				System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!");
        			}
        			else{
        				System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!");
        			}
        			break;
        		default:
        			break;	
        	}
        	
        	System.out.println("");
        }while(func != 5);	//Exit
        System.out.println("Bye-bye!!");
        
    }
}

