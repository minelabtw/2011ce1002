package a4.s100502013;

public class Triangle2D{
	private MyPoint p1 = new MyPoint(0.0 , 0.0);
	private MyPoint p2 = new MyPoint(10.0 , 15.0);
	private MyPoint p3 = new MyPoint(17.0 , 6.0); //initial triangle points
	private MyPoint p1_new,p2_new,p3_new;
	
	public Triangle2D(double x1,double y1,double x2,double y2,double x3,double y3){ //constructor: initialize new points
		p1_new = new MyPoint(x1,y1);
		p2_new = new MyPoint(x2,y2);
		p3_new = new MyPoint(x3,y3);
	}
	
	public MyPoint getP1_new(){ //get the first point
		return p1_new;
	}
	
	public MyPoint getP2_new(){ //get the second point
		return p2_new;
	}

	public MyPoint getP3_new(){ //get the third point
		return p3_new;
	}
	
	public double getArea(){ //calculate the area of the new triangle
		double side1,side2,side3,s;
		side1 = p1_new.distance(p2_new);
		side2 = p2_new.distance(p3_new);
		side3 = p3_new.distance(p1_new);
		s = (side1 + side2 + side3) / 2;
		return Math.sqrt(s*(s-side1)*(s-side2)*(s-side3));
	}
	
	public double getPerimeter(){ //calculate the perimeter of the new triangle
		double side1,side2,side3;
		side1 = p1_new.distance(p2_new);
		side2 = p2_new.distance(p3_new);
		side3 = p3_new.distance(p1_new);
		return side1 + side2 + side3;
	}
	
	public boolean contains(MyPoint p){ //check whether the new point is inside the original triangle or not
		double side1,side2,side3,s,areaPAB,areaPBC,areaPCA,areaABC;
		side1 = p.distance(p1);
		side2 = p.distance(p2);
		side3 = p1.distance(p2);
		s = (side1 + side2 + side3) / 2;
		areaPAB = Math.sqrt(s*(s-side1)*(s-side2)*(s-side3)); //area PAB
		side1 = p.distance(p2);
		side2 = p.distance(p3);
		side3 = p2.distance(p3);
		s = (side1 + side2 + side3) / 2;
		areaPBC = Math.sqrt(s*(s-side1)*(s-side2)*(s-side3)); //area PBC
		side1 = p.distance(p3);
		side2 = p.distance(p1);
		side3 = p3.distance(p1);
		s = (side1 + side2 + side3) / 2;
		areaPCA = Math.sqrt(s*(s-side1)*(s-side2)*(s-side3)); //area PCA
		side1 = p1.distance(p2);
		side2 = p2.distance(p3);
		side3 = p3.distance(p1);
		s = (side1 + side2 + side3) / 2;
		areaABC = Math.sqrt(s*(s-side1)*(s-side2)*(s-side3)); //area ABC
		if(Math.abs(areaABC - areaPAB - areaPBC - areaPCA) <= 0.5)
			return true;
		else 
			return false;
	}
	
	public boolean contains(Triangle2D t){ //check whether the new triangle is inside the original triangle or not
		if(t.contains(t.getP1_new()) || t.contains(t.getP2_new()) || t.contains(t.getP3_new()))
			return true;
		else
			return false;
	}
}
