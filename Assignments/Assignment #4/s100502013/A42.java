package a4.s100502013;
import java.util.Scanner;
import java.lang.Math;

public class A42 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		double myp1,myp2,myp3,myp4,myp5,myp6;
		int choose=1; //use it to choose functions
		System.out.print("Please enter three points of the new triangle: ");
		myp1 = input.nextDouble();
		myp2 = input.nextDouble();
		myp3 = input.nextDouble();
		myp4 = input.nextDouble();
		myp5 = input.nextDouble();
		myp6 = input.nextDouble();
		Triangle2D mytri = new Triangle2D(myp1,myp2,myp3,myp4,myp5,myp6); //create object
		System.out.println("1. get the area of the new triangle");
		System.out.println("2. get the perimeter of the new triangle");
		System.out.println("3. check the new triangle is in the original triangle or not");
		System.out.println("4. enter a point and check the point is in the original triangle or not");
		System.out.println("5. exit"); //function list
		while(choose!=5){ //when choose = 5 , exit.
			System.out.print("\nPlease enter a function you want to use: ");
			choose = input.nextInt(); //enter the number represent to a certain function
			switch(choose){
				case 1: //new area
					System.out.println("The area of the new triangle is " + mytri.getArea());
					break;
					
				case 2: //new perimeter
					System.out.println("The perimeter of the new triangle is " + mytri.getPerimeter());
					break;
					
				case 3: //check triangle inside
					System.out.print("Please enter a new triangle for the new object: ");
					myp1 = input.nextDouble();
					myp2 = input.nextDouble();
					myp3 = input.nextDouble();
					myp4 = input.nextDouble();
					myp5 = input.nextDouble();
					myp6 = input.nextDouble();
					Triangle2D newtri = new Triangle2D(myp1,myp2,myp3,myp4,myp5,myp6);
					if(mytri.contains(newtri))
						System.out.println("The new triangle of new object is in the original triangle of the old object!");
					else
						System.out.println("The new triangle of new object is NOT in the original triangle of the old object.");
					break;
					
				case 4: //check point inside
					System.out.print("Please enter a new point: ");
					myp1 = input.nextDouble();
					myp2 = input.nextDouble();
					MyPoint newpoint = new MyPoint(myp1,myp2);
					if(mytri.contains(newpoint))
						System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!");
					else
						System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15).");
					break;
					
				case 5: //exit
					System.out.println("Goodbye!");
					break;
					
				default: //invalid number
					System.out.println("This is invalid.");
					break;
			}
		}
	}
}
