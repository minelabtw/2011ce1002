package a4.s100502013;

public class MyPoint {
	private double x;
	private double y;
	
	public MyPoint(){ //default constructor: nothing
	}
	
	public MyPoint(double a, double b){ //constructor: use this to initialized a point
		setX(a);
		setY(b);
	}
	
	public double getX(){ //get x-axis point
		return x;
	}
	
	public double getY(){ //get y-axis point
		return y;
	}
	
	public void setX(double p){ //save the x-axis point
		x = p;
	}
	
	public void setY(double p){ //save the y-axis point
		y = p;
	}
	
	public double distance(MyPoint p){ //input another point and calculate their distance
		double dissq = (getX()-p.getX())*(getX()-p.getX()) + (getY()-p.getY())*(getY()-p.getY());
		return Math.sqrt(dissq);
	}
}
