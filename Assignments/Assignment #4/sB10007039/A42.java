package a4.sB10007039;

import java.util.Scanner;

public class A42 {
	public static void main(String[] args) {
		double[] Point = new double[6];
		Scanner Inputer = new Scanner(System.in);
		boolean WhileMenu = true;
		
		System.out.println("Please enter three points of the new triangle:");
		for (int i = 0; i < Point.length; i++) {
			Point[i] = Inputer.nextDouble();
		}
		Triangle2D Work = new Triangle2D(Point[0], Point[1], Point[2], Point[3], Point[4], Point[5]);
		
		System.out.println("1.get the area of the new triangle");
		System.out.println("2.get the perimeter of the new triangle");
		System.out.println("3.check the new triangle is in the original triangle or not");
		System.out.println("4.enter a point and check the point is in the original triangle or not");
		System.out.println("5.Exit");
		
		while (WhileMenu) {
			switch (Inputer.nextInt()) {
			case 1:
				System.out.println("The area of the new triangle is "+Work.getArea());
				break;
				
			case 2:
				System.out.println("The perimeter of the new triangle is "+Work.getPerimeter());
				break;
				
			case 3:
				System.out.println("Please input a new triangle for the new object:");
				for (int i = 0; i < Point.length; i++) {
					Point[i] = Inputer.nextDouble();
				}
				if (Work.contains(new Triangle2D(Point[0], Point[1], Point[2], Point[3], Point[4], Point[5]))) {
					System.out.println("The new triangle of new object is in the original triangle of the old object!!!");
				} else {
					System.out.println("The new triangle of new object is NOT in the original triangle of the old object!!!");
				}
				break;
			case 4:
				System.out.println("Please input a new point for check the point is in the original triangle or not");
				if (Work.contains(new MyPoint(Inputer.nextDouble(),Inputer.nextDouble()))) {
					System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!");
				} else {
					System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!");
				}
				break;
			case 5:
				WhileMenu = false;
				break;
			default:
				System.out.println("Not a good choose!");
				break;
			}
			
		}
		System.exit(0);
		
	}
	
	
}
