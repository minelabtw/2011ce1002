package a4.sB10007039;

public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0, 0.0); // points of original triangle triangle
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint p1_new, p2_new, p3_new; // points of new triangle

	public MyPoint getP1_new(){ // get the p1 of new triangle
		return p1_new;
	}

	public MyPoint getP2_new(){ // get the p2 of new triangle
		return p2_new;
	}

	public MyPoint getP3_new(){ // get the p3 of new triangle
		return p3_new;
	}

	// constructor with arguments to initial the points of the new triangle
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3){
		p1_new = new MyPoint(x1,y1);
		p2_new = new MyPoint(x2,y2);
		p3_new = new MyPoint(x3,y3);
	}

	public double getArea(){ // calculate the area of the new triangle
		double a,b,c,p;
		a = p1_new.distance(p2_new);
		b = p2_new.distance(p3_new);
		c = p3_new.distance(p1_new);
		p = (a+b+c)/2;
		return Math.pow(p*(p-a)*(p-b)*(p-c), 0.5);
	}
	public double getArea(double x1,double y1,double x2,double y2,double x3,double y3){ // calculate the area of the new triangle
		double a,b,c,p;
		MyPoint np1,np2,np3;
		np1 = new MyPoint(x1,y1);
		np2 = new MyPoint(x2,y2);
		np3 = new MyPoint(x3,y3);
		a = np1.distance(np2);
		b = np2.distance(np3);
		c = np3.distance(np1);
		p = (a+b+c)/2;
		return Math.pow(p*(p-a)*(p-b)*(p-c), 0.5);
	}

	public double getPerimeter(){ // calculate the perimeter of the new triangle
		return p1_new.distance(p2_new)+p2_new.distance(p3_new)+p3_new.distance(p1_new);
	}

	// check the input point is in the original triangle of the object or not
	public boolean contains(MyPoint p){
		// if input point p is in the triangle, area of PAB + area of PBC + area of PCA = area of ABC 
		double areaPAB, areaPBC, areaPCA, areaABC;
		// calculate area of PAB
		areaPAB = getArea(p1.getX(), p1.getY(), p2.getX(), p2.getY(), p.getX(), p.getY());
		
		// calculate area of PBC
		areaPBC = getArea(p1.getX(), p1.getY(), p3.getX(), p3.getY(), p.getX(), p.getY());
		
		// calculate area of PCA
		areaPCA = getArea(p3.getX(), p3.getY(), p2.getX(), p2.getY(), p.getX(), p.getY());
		
		// calculate area of ABC
		areaABC = getArea(p1.getX(), p1.getY(), p2.getX(), p2.getY(), p3.getX(), p3.getY());
		
		// if areaPAB + areaPBC + areaPCA = areaABC, then p is in the ABC
                // However, there is some error(�~�t) in this calculation
                // So if the absolute value of (areaABC - (areaPAB + areaPBC + areaPCA)) is less than 0.5
                // we consider the point is inside the original triangle
                // you can use Math.abs(double) to calculate the absolute value
		if (((areaABC - (areaPAB + areaPBC + areaPCA)) < 0.5) && ((areaABC - (areaPAB + areaPBC + areaPCA)) > -0.5)) {
			return true;
		} else {
			return false;
		}
	}

	// check the input triangle(Triangle2D object) is in the original triangle of this object or not
	public boolean contains(Triangle2D input){
	       // if the input triangle is in the original triangle of this object
           // then all points of the input triangle is in the original triangle of this object
			
			boolean[] AreaTest = new boolean[3];
			
			AreaTest[0] = this.contains(input.p1_new);
			AreaTest[1] = this.contains(input.p2_new);
			AreaTest[2] = this.contains(input.p3_new);
			
			if (AreaTest[0] && AreaTest[1] && AreaTest[2]) {
				return true;
			} else {
				return false;
			}
			
	}
}
