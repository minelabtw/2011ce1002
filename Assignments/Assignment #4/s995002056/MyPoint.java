package a4.s995002056;

public class MyPoint {
	private double x;
	private double y;

	public MyPoint(){

	}

	public MyPoint(double x_input, double y_input){ // constructor to initial private data member
		this.x = x_input;
		this.y = y_input;
	}

	public double getX(){ // return x value
		return x;
	}

	public double getY(){ // return y value
		return y;
	}

	public void setX(double input) // set x value
	{
		this.x = input;
	}

	public void setY(double input) // set y value
	{
		this.y = input;
	}

	public double distance(MyPoint input){ // the distance between this point and the input point
		double d;
		d = Math.sqrt((x-input.getX())*(x-input.getX())+(y-input.getY())*(y-input.getY()));
		return d;
	}
}
