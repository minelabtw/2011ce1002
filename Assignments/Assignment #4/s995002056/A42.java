package a4.s995002056;
import java.util.Scanner;

public class A42 {
	public static void main(String[] argv){
		Scanner input = new Scanner(System.in);
		
		System.out.println("Please enter three points of the new triangle:");
		double a = input.nextDouble();			
		double b = input.nextDouble();
		double c = input.nextDouble();
		double d = input.nextDouble();
		double e = input.nextDouble();
		double f = input.nextDouble();
										
		Triangle2D t2d = new Triangle2D(a,b,c,d,e,f);
		
		boolean exit = true;
		
		while(exit){
			System.out.println("1.get the area of the new triangle");
			System.out.println("2.get the perimeter of the new triangle");
			System.out.println("3.check the new triangle is in the original triangle or not");
			System.out.println("4.enter a point and check the point is in the original triangle or not");
			System.out.println("5.Exit");
			int ch = input.nextInt();
			
			switch(ch){
				case(1):
						System.out.println("The area of the new triangle is " + t2d.getArea());
						break;
				
				case(2):
						System.out.println("The perimeter of the new triangle is " + t2d.getPerimeter());
						break;
				
				case(3):
						System.out.println("Please input a new triangle for the new object:");
						double g = input.nextDouble();			
						double h = input.nextDouble();
						double i = input.nextDouble();
						double j = input.nextDouble();
						double k = input.nextDouble();
						double l = input.nextDouble();
						Triangle2D t2D = new Triangle2D(g,h,i,j,k,l);
						if(t2D.contains(t2D) == true)
							System.out.println("The new triangle of new object is in the original triangle of the old object!!!");
							else
								System.out.println("The new triangle of new object is NOT in the original triangle of the old object!!!");
						break;
						
				case(4):
						System.out.println("Please input a point:");
						double p1 = input.nextDouble();
						double p2 = input.nextDouble();
						MyPoint p = new MyPoint(p1,p2);
						if(t2d.contains(p) == true)
							System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!");
							else
								System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!");
						break;
						
				case(5):
						exit = false;
						System.out.println("Thank you!");
						break;
								
			}
		}
		
	}

}
