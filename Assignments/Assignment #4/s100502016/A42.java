package a4.s100502016;

import java.util.Scanner;

public class A42 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		double[] tempInput = new double[6];

		System.out.println("Please enter three points of the new triangle:");
		for (int i = 0; i < 6; i++) {
			tempInput[i] = input.nextDouble();
		}

		Triangle2D myTriangle = new Triangle2D(tempInput[0], tempInput[1],
				tempInput[2], tempInput[3], tempInput[4], tempInput[5]);

		System.out
				.println("1.Get the area of the new triangle\n"
						+ "2.Get the perimeter of the new triangle\n"
						+ "3.Check the new triangle is in the original triangle or not\n"
						+ "4.Enter a point and check the point is in the original triangle or not\n"
						+ "5.Exit");

		boolean Exit = false;

		while (!Exit) {
			int caseSelect = input.nextInt();
			switch (caseSelect) {
			case 1:
				System.out.println("The area of the new triangle is "
						+ myTriangle.getArea());

				break;
			case 2:
				System.out.println("The perimeter of the new triangle is "
						+ myTriangle.getPerimeter());

				break;
			case 3:
				System.out
						.println("Please input a new triangle for the new object:");
				for (int i = 0; i < 6; i++)
					tempInput[i] = input.nextDouble();
				Triangle2D TrangleForCompare = new Triangle2D(tempInput[0],
						tempInput[1], tempInput[2], tempInput[3], tempInput[4],
						tempInput[5]);
				if (myTriangle.contains(TrangleForCompare))
					System.out
							.println("The new triangle of new object is in the original triangle.");
				else
					System.out
							.println("The new triangle of new object is NOT in the original triangle!!!");

				break;
			case 4:
				System.out
						.println("Please input a new point for the new object:");
				for (int i = 0; i < 2; i++)
					tempInput[i] = input.nextDouble();
				MyPoint PointForCompare = new MyPoint(tempInput[0],
						tempInput[1]);
				if (myTriangle.contains(PointForCompare))
					System.out
							.println("The new point is in the original triangle (0,0), (17,6), (10, 15).");
				else

					System.out
							.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!");

				break;
			case 5:
				System.out.println("User Exit.");
				Exit = true;
				break;
			}
		}

	}
}
