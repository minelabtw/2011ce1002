package a4.s100502507;

import javax.swing.JOptionPane;

public class A42 {
	static double x1;
	static double y1;
	static double x2;
	static double y2;
	static double x3;
	static double y3;
	static String input;
	public static void main(String[] args) {//Function main begins execution
		revisePoints();
		Triangle2D triangle = new Triangle2D(x1, y1, x2, y2, x3, y3);//Declare an old object and pass 3 points into it
		Triangle2D triangle02;//Declare an new object and pass 3 points into it later
		boolean exit = false;//Exit condition
		
		int choose;//The user's choice
		
		while(!exit) {
			input = JOptionPane.showInputDialog(null, "1.get the area of the new triangle\n"
					+ "2.get the perimeter of the new triangle\n"
					+ "3.check the new triangle is in the original triangle or not\n"
					+ "4.enter a point and check the point is in the original triangle or not\n"
					+ "5.exit", "Test program", JOptionPane.INFORMATION_MESSAGE);
			choose = Integer.parseInt(input);
			switch(choose) {
				case 1://Show area of new triangle of the old object
					JOptionPane.showMessageDialog(null, "The area of the new triangle is " + triangle.getArea(), "Test program", JOptionPane.INFORMATION_MESSAGE);
					break;
				case 2://Show perimeter of new triangle of the old object
					JOptionPane.showMessageDialog(null, "The perimeter of the new triangle is " + triangle.getPerimeter(), "Test program", JOptionPane.INFORMATION_MESSAGE);
					break;
				case 3://Check new triangle of new object is in the original one of old object or not
					revisePoints();
					triangle02 = new Triangle2D(x1, y1, x2, y2, x3, y3);//New the new object and pass 3 points into it
					if(triangle.contains(triangle02)) {
						JOptionPane.showMessageDialog(null, "The new triangle of new object is in the original triangle of the old object!!!", "Test program", JOptionPane.INFORMATION_MESSAGE);
					}
					else {
						JOptionPane.showMessageDialog(null, "The new triangle of new object is NOT in the original triangle of the old object (0,0), (17,6), (10, 15)!!!", "Test program", JOptionPane.INFORMATION_MESSAGE);
					}
					break;
				case 4://Let user input one point, and check this point is in the original triangle of old object or not
					input = JOptionPane.showInputDialog(null, "Input X please", "Test program", JOptionPane.INFORMATION_MESSAGE);
					x1 = Double.parseDouble(input);
					input = JOptionPane.showInputDialog(null, "Input Y please", "Test program", JOptionPane.INFORMATION_MESSAGE);
					y1 = Double.parseDouble(input);
					MyPoint inputPoint = new MyPoint(x1, y1);
					if(triangle.contains(inputPoint)) {
						JOptionPane.showMessageDialog(null, "The new point is in the original triangle!!!", "Test program", JOptionPane.INFORMATION_MESSAGE);
					}
					else {
						JOptionPane.showMessageDialog(null, "The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!", "Test program", JOptionPane.INFORMATION_MESSAGE);
					}
					break;
				case 5://Exit choice
					exit = true;
					break;
				case ' ':
				case '\n':
				case '\t':
					exit = true;
					break;
				default://Avoid error
					JOptionPane.showMessageDialog(null, "Incorrect input!", "Test program", JOptionPane.ERROR_MESSAGE);
					break;
			}//End switch
		}//End while loop
	}//End function main
	static void revisePoints() {
		input = JOptionPane.showInputDialog(null, "Input X of point1", "Test program", JOptionPane.INFORMATION_MESSAGE);//Start inputting points
		x1 = Double.parseDouble(input);
		input = JOptionPane.showInputDialog(null, "Input Y of point1", "Test program", JOptionPane.INFORMATION_MESSAGE);
		y1 = Double.parseDouble(input);
		input = JOptionPane.showInputDialog(null, "Input X of point2", "Test program", JOptionPane.INFORMATION_MESSAGE);
		x2 = Double.parseDouble(input);
		input = JOptionPane.showInputDialog(null, "Input Y of point2", "Test program", JOptionPane.INFORMATION_MESSAGE);
		y2 = Double.parseDouble(input);
		input = JOptionPane.showInputDialog(null, "Input X of point3", "Test program", JOptionPane.INFORMATION_MESSAGE);
		x3 = Double.parseDouble(input);
		input = JOptionPane.showInputDialog(null, "Input Y of point3", "Test program", JOptionPane.INFORMATION_MESSAGE);
		y3 = Double.parseDouble(input);
	}
}
