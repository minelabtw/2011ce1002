package a4.s100502507;

import java.lang.Math;

public class MyPoint {
	private double x;
	private double y;

	public MyPoint() {
		
	}

	public MyPoint(double x_input, double y_input) {//Constructor to initial private data member
		x = x_input;
		y = y_input;
	}

	public double getX() {//Return x value
		return x;
	}

	public double getY() {//Return y value
		return y;
	}

	public void setX(double input) {//Set x value
		x = input;
	}

	public void setY(double input) {//Set y value
		y = input ;
	}

	public double distance(MyPoint input){//The distance between this point and the input point
		return Math.sqrt(Math.pow(input.getX()-x, 2)+Math.pow(input.getY()-y, 2));
	}
}