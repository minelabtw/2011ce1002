package a4.s100502507;

public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0, 0.0); //Points of original triangle triangle
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint p1_new, p2_new, p3_new; //Points of new triangle

	public MyPoint getP1_new(){ //Get the p1 of new triangle
		return p1_new;
	}

	public MyPoint getP2_new(){ //Get the p2 of new triangle
		return p2_new;
	}

	public MyPoint getP3_new(){ //Get the p3 of new triangle
		return p3_new;
	}

	//Constructor with arguments to initial the points of the new triangle
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3){
		p1_new = new MyPoint(x1, y1);
		p2_new = new MyPoint(x2, y2);
		p3_new = new MyPoint(x3, y3);
	}

	public double getArea(){ //Calculate the area of the new triangle
		double a = p1_new.distance(p2_new);
		double b = p2_new.distance(p3_new);
		double c = p1_new.distance(p3_new);
		double s = getPerimeter()/2.0;
		return Math.sqrt(s*(s - a)*(s - b)*(s - c));
	}

	public double getPerimeter(){ //Calculate the perimeter of the new triangle
		double a = p1_new.distance(p2_new);
		double b = p2_new.distance(p3_new);
		double c = p1_new.distance(p3_new);
		return a+b+c;
	}
	
	private double area(MyPoint point01, MyPoint point02, MyPoint point03) { //A private function to calculate the area of a triangle
		double a = point01.distance(point02);
		double b = point02.distance(point03);
		double c = point01.distance(point03);
		double s = (a + b + c)/2.0;
		return Math.sqrt(s*(s - a)*(s - b)*(s - c));
	}
	
	//Check the input point is in the original triangle of the object or not
	public boolean contains(MyPoint p){
		//If input point p is in the triangle, area of PAB + area of PBC + area of PCA = area of ABC 
		double areaPAB, areaPBC, areaPCA, areaABC;
		//Calculate area of PAB
		areaPAB = area(p, p1, p2);
		//Calculate area of PBC
		areaPBC = area(p, p2, p3);
		//Calculate area of PCA
		areaPCA = area(p, p1, p3);
		//Calculate area of ABC
		areaABC = area(p1, p2, p3);
		//If areaPAB + areaPBC + areaPCA = areaABC, then p is in the ABC
        //However, there is some error(�~�t) in this calculation
        //So if the absolute value of (areaABC - areaPAB + areaPBC + areaPCA) is less than 0.5
        //We consider the point is inside the original triangle
		if(Math.abs(areaABC - (areaPAB + areaPBC + areaPCA))<=0.5) {
			return true;
		}
		else {
			return false;
		}
	}

	//Check the input triangle(Triangle2D object) is in the original triangle of this object or not
	public boolean contains(Triangle2D input){
	       //If the input triangle is the original triangle of this object
               //Then all points of the input triangle is in the original triangle of this object
		if(contains(input.getP1_new()) && contains(input.getP2_new()) && contains(input.getP3_new())) {
			return true;
		}
		else {
			return false;
		}
	}
}
