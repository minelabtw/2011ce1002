package a4.s100502018;

import java.util.Scanner;

public class A42 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out
				.print("Please input three points' coordinate for a new triangle: ");
		double x1 = input.nextDouble(); // get the the triangle inputed by user
		double y1 = input.nextDouble();
		double x2 = input.nextDouble();
		double y2 = input.nextDouble();
		double x3 = input.nextDouble();
		double y3 = input.nextDouble();
		Triangle2D myTriangle1 = new Triangle2D(x1, y1, x2, y2, x3, y3); // initial
																			// a
																			// object
		Triangle2D myTriangle2;
		MyPoint mypoint;
		int choice;
		do { // repeat until user choose 5
			System.out
					.print("\n1. Get the area of the new triangle\n2. Get the perimeter of the new triangle\n3. Enter a point and check the point is in the original triangle or not\n4. Check the new triangle is in the original triangle or not\n5. Exit\nPlease choose a service: ");
			choice = input.nextInt();
			switch (choice) {
			case 1: // get area
				System.out.println("The area of the new triangle is "
						+ myTriangle1.getArea());
				break;
			case 2: // get perimeter
				System.out.println("The perimeter of the new triangle is "
						+ myTriangle1.getPerimeter());
				break;
			case 3: // check the position of new triangle
				System.out
						.print("Please input three points' coordinate for a new triangle: ");
				double myx1 = input.nextDouble();
				double myy1 = input.nextDouble();
				double myx2 = input.nextDouble();
				double myy2 = input.nextDouble();
				double myx3 = input.nextDouble();
				double myy3 = input.nextDouble();
				myTriangle2 = new Triangle2D(myx1, myy1, myx2, myy2, myx3, myy3);
				if (myTriangle1.contains(myTriangle2)) {
					System.out
							.println("The new triangle of new object is in the original triangle of the old object!!!");
				} else {
					System.out
							.println("The new triangle of new object is NOT in the original triangle of the old object!!!");
				}
				break;
			case 4: // check the position of new point
				System.out.print("Please input a new point: ");
				double myx = input.nextDouble();
				double myy = input.nextDouble();
				mypoint = new MyPoint(myx, myy);
				if (myTriangle1.contains(mypoint)) {
					System.out
							.println("The new point is in the original triangle!!!");
				} else {
					System.out
							.println("The new point is NOT in the original triangle!!!");
				}
				break;
			case 5: // leave the program
				System.out.println("Thanks for your using");
				break;
			default:
				System.out.println("Something is wrong! Please try again");
				break;
			}
		} while (choice != 5);
	}
}
