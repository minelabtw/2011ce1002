package a4.s100502018;

public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0, 0.0); // points of the original
												// triangle
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint p1_new, p2_new, p3_new; // points of new triangle

	public MyPoint getP1_new() { // get the p1 of new triangle
		return p1_new;
	}

	public MyPoint getP2_new() { // get the p2 of new triangle
		return p2_new;
	}

	public MyPoint getP3_new() { // get the p3 of new triangle
		return p3_new;
	}

	public Triangle2D(double x1, double y1, double x2, double y2, double x3,
			double y3) { // constructor with arguments to initial the points of
							// the new triangle
		p1_new = new MyPoint(x1, y1);
		p2_new = new MyPoint(x2, y2);
		p3_new = new MyPoint(x3, y3);
	}

	public double getArea() { // calculate the area of the new triangle
		double s = (p1_new.distance(p2_new) + p2_new.distance(p3_new) + p3_new
				.distance(p1_new)) / 2;
		return Math
				.sqrt(s * (s - p1_new.distance(p2_new))
						* (s - p2_new.distance(p3_new))
						* (s - p3_new.distance(p1_new)));
	}

	public double getPerimeter() { // calculate the perimeter of the new
									// triangle
		return p1_new.distance(p2_new) + p2_new.distance(p3_new)
				+ p3_new.distance(p1_new);
	}

	public boolean contains(MyPoint p) { // check the input point is in the
											// original triangle of the object
											// or not
		double inside1, inside2, inside3, outside1, outside2, outside3, s, areaPAB, areaPBC, areaPCA, areaABC;
		inside1 = p.distance(p1);
		inside2 = p.distance(p2);
		inside3 = p.distance(p3);
		outside1 = p1.distance(p2);
		outside2 = p2.distance(p3);
		outside3 = p3.distance(p1);
		s = (inside1 + inside2 + outside1) / 2;
		areaPAB = Math.sqrt(s * (s - inside1) * (s - inside2) * (s - outside1));
		s = (inside2 + inside3 + outside2) / 2;
		areaPBC = Math.sqrt(s * (s - inside2) * (s - inside3) * (s - outside2));
		s = (inside3 + inside1 + outside3) / 2;
		areaPCA = Math.sqrt(s * (s - inside3) * (s - inside1) * (s - outside3));
		s = (outside1 + outside2 + outside3) / 2;
		areaABC = Math.sqrt(s * (s - outside1) * (s - outside2)
				* (s - outside3));
		if (Math.abs(areaABC - areaPAB - areaPBC - areaPCA) <= 0.5) {
			return true;
		} else {
			return false;
		}
	}

	public boolean contains(Triangle2D input) { // check the input
												// triangle(Triangle2D object)
												// is in the original triangle
												// of this object or not
		int checktimes = 0;
		if (contains(input.getP1_new())) {
			checktimes++;
		}
		if (contains(input.getP2_new())) {
			checktimes++;
		}
		if (contains(input.getP3_new())) {
			checktimes++;
		}
		if (checktimes == 3) {
			return true;
		} else {
			return false;
		}
	}
}
