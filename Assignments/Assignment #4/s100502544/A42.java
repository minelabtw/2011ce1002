package a4.s100502544;
import java.util.Scanner;
public class A42 {
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);
		System.out.println("Please enter three points of the new triangle:");
		double x1=input.nextDouble();
		double y1=input.nextDouble();
		double x2=input.nextDouble();
		double y2=input.nextDouble();
		double x3=input.nextDouble();
		double y3=input.nextDouble();
		Triangle2D triangle2D=new Triangle2D(x1,y1,x2,y2,x3,y3);//把六個值都new給Triangle2D
		for(;;){//令無窮迴圈
			System.out.println("1.get the area of the new triangle");
			System.out.println("2.get the perimeter of the new triangle");
			System.out.println("3.check the new triangle is in the original triangle or not");
			System.out.println("4.enter a point and check the point is in the original triangle or not");
			System.out.println("5.Exit");
			System.out.print("Enter:");
			int choose=input.nextInt();
			if(choose==5){
				System.out.println("BYE");
				break;//迴圈停止條件
			}
			switch(choose){
			case 1:
				System.out.println("1");
				System.out.println("The area of the new triangle is "+triangle2D.getArea());
				break;                                                //呼叫object triangle2D的getarea面積
			case 2:
				System.out.println("2");
				System.out.println("The perimeter of the new triangle is "+triangle2D.getPerimeter());
				break;                                                   //呼叫object triangle2D的getPerimeter長度
			case 3:
				System.out.println("Please input a new triangle for the new object: ");
				double x11=input.nextDouble();
				double y11=input.nextDouble();
				double x22=input.nextDouble();
				double y22=input.nextDouble();
				double x33=input.nextDouble();
				double y33=input.nextDouble();
				Triangle2D triangle2DD=new Triangle2D(x11,y11,x22,y22,x33,y33);//給新的六個值 assign Triangle2D 
				if(triangle2D.contains(triangle2DD)==true)//判斷由object triangle2DD回傳的條件
					System.out.println("The new triangle of new object is in the original triangle of the old object!!!");
				else
					System.out.println("The new triangle of new object is NOT in the original triangle of the old object!!!");
				break;
			case 4:
				System.out.println("4");
				System.out.println("Enter a point:");
				double newx=input.nextDouble();
				double newy=input.nextDouble();
				MyPoint mypoint=new MyPoint(newx,newy);//assign MyPoint新值
				if(triangle2D.contains(mypoint)==true)//判斷由object triangle2DD回傳的條件 
					System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!");
				else
					System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!");
				break;
				default:
					System.out.println("Error!! Try again,thanks!");
					break;
			}
				
		}

	}
}
