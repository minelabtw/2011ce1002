package a4.s100502544;

public class MyPoint {
	private double x;
	private double y;
	//private型態的x y

	public MyPoint(){
	}

	public MyPoint(double x_input, double y_input){ // constructor to initial private data member
		setX(x_input);
		setY(y_input);//因為宣告和x和y是private  所以要用一個public的constructor做初始化
	}

	public double getX(){ // return x value
		return x;//取值

	}

	public double getY(){ // return y value
		return y;//取值
	}

	public void setX(double input) // set x value
	{
		x=input;//初始化x的值

	}

	public void setY(double input) // set y value
	{
		y=input;//初始化y值

	}

	public double distance(MyPoint input){ // the distance between this point and the input point
		return Math.sqrt(Math.pow((input.getX()-x),2)+Math.pow((input.getY()-y),2));
	}//由input的值和原先值   算長度
	

}
