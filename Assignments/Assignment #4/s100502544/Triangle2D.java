package a4.s100502544;

public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0, 0.0); // points of original triangle triangle
	private MyPoint p2 = new MyPoint(17.0, 6.0);//固定原本的三角形
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint p1_new, p2_new, p3_new; // points of new triangle//新的三角型三點
	
	// constructor with arguments to initial the points of the new triangle
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3){
		p1_new=new MyPoint(x1,y1);//第一點assign給object p1_new 
		p2_new=new MyPoint(x2,y2);//第二點assign給object p2_new
		p3_new=new MyPoint(x3,y3);//第三點assign給object p3_new
	}

	public MyPoint getP1_new(){ // get the p1 of new triangle
		return p1_new;
	}

	public MyPoint getP2_new(){ // get the p2 of new triangle
		return p2_new;
	}

	public MyPoint getP3_new(){ // get the p3 of new triangle
		return p3_new;
	}

	public double getArea(){ // calculate the area of the new triangle
		double s=0.5*getPerimeter();
		double Areatot=Math.sqrt(s*(s-p1_new.distance(p2_new))*(s-p1_new.distance(p3_new))*(s-p2_new.distance(p3_new)));
		return Areatot;//新三角型面積   根號s*(s-a)*(s-b)*(s-c)
	}

	public double getPerimeter(){ // calculate the perimeter of the new triangle//三邊邊長
		double Perimetertot=p1_new.distance(p2_new)+p1_new.distance(p3_new)+p2_new.distance(p3_new);
		return Perimetertot;//a+b+c
	}
	public boolean contains(Triangle2D input){
	       // if the input triangle is in the original triangle of this object
            // then all points of the input triangle is in the original triangle of this object
		double sPAB1=0.5*(p1.distance(p2)+p1.distance(input.p1_new)+p2.distance(input.p1_new));
		double areaPAB1=Math.sqrt(sPAB1*(sPAB1-p1.distance(p2))*(sPAB1-p1.distance(input.p1_new))*(sPAB1-p2.distance(input.p1_new)));
		double sPBC1=0.5*(input.p1_new.distance(p2)+input.p1_new.distance(p3)+p2.distance(p3));
		double areaPBC1=Math.sqrt(sPBC1*(sPBC1-input.p1_new.distance(p2))*(sPBC1-input.p1_new.distance(p3))*(sPBC1-p2.distance(p3)));
		double sPCA1=0.5*(p1.distance(input.p1_new)+input.p1_new.distance(p3)+p1.distance(p3));
		double areaPCA1=Math.sqrt(sPCA1*(sPBC1-p1.distance(input.p1_new))*(sPCA1-input.p1_new.distance(p3))*(sPCA1-p1.distance(p3)));
		//先以第一個點(取代原本三角型的其中一點)  算第一點跟另外其他兩點(原本三角型的另外兩點)的面積
		
		double sPAB2=0.5*(p1.distance(p2)+p1.distance(input.p2_new)+p2.distance(input.p2_new));
		double areaPAB2=Math.sqrt(sPAB2*(sPAB2-p1.distance(p2))*(sPAB2-p1.distance(input.p2_new))*(sPAB2-p2.distance(input.p2_new)));
		double sPBC2=0.5*(input.p2_new.distance(p2)+input.p2_new.distance(p3)+p2.distance(p3));
		double areaPBC2=Math.sqrt(sPBC2*(sPBC2-input.p2_new.distance(p2))*(sPBC2-input.p2_new.distance(p3))*(sPBC2-p2.distance(p3)));
		double sPCA2=0.5*(p1.distance(input.p2_new)+input.p2_new.distance(p3)+p1.distance(p3));
		double areaPCA2=Math.sqrt(sPCA2*(sPBC2-p1.distance(input.p2_new))*(sPCA2-input.p2_new.distance(p3))*(sPCA2-p1.distance(p3)));
		//以第二個點(取代原本三角型的其中一點)  算第二點跟另外其他兩點(原本三角型的另外兩點)的面積
		
		double sPAB3=0.5*(p1.distance(p2)+p1.distance(input.p3_new)+p2.distance(input.p3_new));
		double areaPAB3=Math.sqrt(sPAB3*(sPAB3-p1.distance(p2))*(sPAB3-p1.distance(input.p3_new))*(sPAB3-p2.distance(input.p3_new)));
		double sPBC3=0.5*(input.p3_new.distance(p2)+input.p3_new.distance(p3)+p2.distance(p3));
		double areaPBC3=Math.sqrt(sPBC3*(sPBC3-input.p3_new.distance(p2))*(sPBC3-input.p3_new.distance(p3))*(sPBC3-p2.distance(p3)));
		double sPCA3=0.5*(p1.distance(input.p3_new)+input.p3_new.distance(p3)+p1.distance(p3));
		double areaPCA3=Math.sqrt(sPCA3*(sPBC3-p1.distance(input.p3_new))*(sPCA3-input.p3_new.distance(p3))*(sPCA3-p1.distance(p3)));
		//以第三個點(取代原本三角型的其中一點)  算第三點跟另外其他兩點(原本三角型的另外兩點)的面積
		
		double sABC=0.5*(p1.distance(p2)+p1.distance(p3)+p2.distance(p3));
		double areaABC=Math.sqrt(sABC*(sABC-p1.distance(p2))*(sABC-p1.distance(p3))*(sABC-p2.distance(p3)));
		//原本三角型的面積
		if(Math.abs(areaABC-areaPAB1-areaPBC1-areaPCA1)<0.5&&Math.abs(areaABC-areaPAB2-areaPBC2-areaPCA2)<0.5&&Math.abs(areaABC-areaPAB3-areaPBC3-areaPCA3)<0.5)
			return true;
		else 
			return false;
	}
	public boolean contains(MyPoint p){
		// if input point p is in the triangle, area of PAB + area of PBC + area of PCA = area of ABC 
		// calculate area of PAB
		double sPAB=0.5*(p1.distance(p2)+p1.distance(p)+p2.distance(p));
		double areaPAB=Math.sqrt(sPAB*(sPAB-p1.distance(p2))*(sPAB-p1.distance(p))*(sPAB-p2.distance(p)));
		// calculate area of PBC
		double sPBC=0.5*(p.distance(p2)+p.distance(p3)+p2.distance(p3));
		double areaPBC=Math.sqrt(sPBC*(sPBC-p.distance(p2))*(sPBC-p.distance(p3))*(sPBC-p2.distance(p3)));
		// calculate area of PCA
		double sPCA=0.5*(p1.distance(p)+p.distance(p3)+p1.distance(p3));
		double areaPCA=Math.sqrt(sPCA*(sPBC-p1.distance(p))*(sPCA-p.distance(p3))*(sPCA-p1.distance(p3)));
		// calculate area of ABC
		double sABC=0.5*(p1.distance(p2)+p1.distance(p3)+p2.distance(p3));
		double areaABC=Math.sqrt(sABC*(sABC-p1.distance(p2))*(sABC-p1.distance(p3))*(sABC-p2.distance(p3)));
		// if areaPAB + areaPBC + areaPCA = areaABC, then p is in the ABC
                // However, there is some error(誤差) in this calculation
                // So if the absolute value of (areaABC - (areaPAB + areaPBC + areaPCA)) is less than 0.5
                // we consider the point is inside the original triangle
                // you can use Math.abs(double) to calculate the absolute value
		if(Math.abs(areaABC-areaPAB-areaPBC-areaPCA)<0.5)
			return true;//誤差0.5以內   true
		else
			return false;
	}

}
