package a4.s100502506;


public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0,0.0); // points of original triangle triangle
	private MyPoint p2 = new MyPoint(17.0,6.0);
	private MyPoint p3 = new MyPoint(10.0,15.0);
	private MyPoint p1_new=new MyPoint(); // points of new triangle
	private MyPoint p2_new=new MyPoint();
	private MyPoint p3_new=new MyPoint();
	public MyPoint getP1_new()// get the p1 of new triangle
	{ 
		return p1_new;
	}

	public MyPoint getP2_new()// get the p2 of new triangle
	{ 
		return p2_new;
	}

	public MyPoint getP3_new()// get the p3 of new triangle
	{ 
		return p3_new;
	}

	// constructor with arguments to initial the points of the new triangle
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3)
	{
		
		p1_new.setX(x1);
		p1_new.setY(y1);
		p2_new.setX(x2);
		p2_new.setY(y2);
		p3_new.setX(x3);
		p3_new.setY(y3);
	
	}

	public double getArea()// calculate the area of the new triangle
	{ 
		double Area=0;
		double a=getSidelength(p1_new,p2_new);
		double b=getSidelength(p2_new,p3_new);
		double c=getSidelength(p3_new,p1_new);
		double s=((a+b+c)/2);

		Area=Math.sqrt((s*(s-a)*(s-b)*(s-c)));
		return Area;
	}
	public double get_other_Area(MyPoint p1,MyPoint p2,MyPoint p3)//returns the area of the other triangle.
	{
		double Area;
		
		double a=getSidelength(p1,p2);
		double b=getSidelength(p2,p3);
		double c=getSidelength(p1,p3);
		double s=((a+b+c)/2);
		Area=Math.sqrt((s*(s-a)*(s-b)*(s-c)));
		return Area;
	}
	public double getPerimeter()// calculate the perimeter of the new triangle
	{ 
		double Perimeter;
		Perimeter=getSidelength(p1_new,p2_new)
				 +getSidelength(p2_new,p3_new)
				 +getSidelength(p3_new,p1_new);
		return Perimeter;
	}
	public double getSidelength(MyPoint p1,MyPoint p2)// calculate the distance of the two points
	{
		double sideLength=Math.sqrt(Math.pow((p1.getX()-p2.getX()),2)+Math.pow((p1.getY()-p2.getY()),2));
		return sideLength;
	}
	// check the input point is in the original triangle of the object or not
	public boolean contains(MyPoint p)
	{
		// if input point p is in the triangle, area of PAB + area of PBC + area of PCA = area of ABC 
		double areaPAB, areaPBC, areaPCA, areaABC;
		
		// calculate area of PAB
		areaPAB=get_other_Area(p,p1,p2);

		// calculate area of PBC
		areaPBC=get_other_Area(p, p2,p3);

		// calculate area of PCA
		areaPCA=get_other_Area(p, p1, p3);

		// calculate area of ABC
		areaABC=get_other_Area(p1,p2,p3);
		
		// if areaPAB + areaPBC + areaPCA = areaABC, then p is in the ABC
        // However, there is some error(�~�t) in this calculation
        // So if the absolute value of (areaABC - areaPAB + areaPBC + areaPCA) is less than 0.5
        // we consider the point is inside the original triangle
        // you can use Math.abs(double) to calculate the absolute value
		if(Math.abs(areaABC - (areaPAB + areaPBC + areaPCA))<=0.5)
		{
			return true;
		}
		else 
		{
			return false;
		}

	}

	// check the input triangle(Triangle2D object) is in the original triangle of this object or not
	
	public boolean contains(Triangle2D input)// if the input triangle is the original triangle of this object
	{										 // then all points of the input triangle is in the original triangle of this object
		boolean checkflag=true;
		checkflag=contains(input.getP1_new());//if one point outside return false
		if(checkflag==false)
		{
			return checkflag;
		}
		checkflag=contains(input.getP2_new());//if one point outside return false
		if(checkflag==false)
		{
			return checkflag;
		}
		checkflag=contains(input.getP3_new());//if one point outside return false
		if(checkflag==false)
		{
			return checkflag;
		}
		//if no point outside return true
		return checkflag;
		
	}
}