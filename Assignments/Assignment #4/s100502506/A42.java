package a4.s100502506;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;

public class A42 extends JFrame
{
	private JPanel panel1;						//use to install GUI on it
	private JPanel panel2;
	private JPanel panel3;
	
	private JLabel label1;						//use to show message
	private JLabel label2;
	private JLabel label3;
	private JLabel label4;
	private JLabel label5;
	private JLabel label6;
	private JLabel label7;
	private JLabel label8;
	private JLabel label9;
	private JLabel label10;
	private JLabel label11;
	
	private JTextField textField1;				//use to enter words
	private JTextField textField2;
	private JTextField textField3;
	private JTextField textField4;
	private JTextField textField5;
	private JTextField textField6;
	
	private GridLayout gridLayout;				//use the GridLayout
	private JButton checkButton1;				//use the Button
	public A42()
	{
		super("Triangle2D Test");				//set the title
		String message1;
		message1=String.format("Please enter three points of the new triangle:");
		JOptionPane.showMessageDialog(null, message1);//show MessageDialog
		
		
		panel1=new JPanel();
		panel2=new JPanel();
		panel3=new JPanel();
		textField1=new JTextField(5);
		textField2=new JTextField(5);
		textField3=new JTextField(5);
		textField4=new JTextField(5);
		textField5=new JTextField(5);
		textField6=new JTextField(5);
		checkButton1=new JButton("OK");
		gridLayout=new GridLayout(3,6);			//3*3
		setLayout(gridLayout);					//set the GridLayout
		
		
		label1=new JLabel();
		label1.setText("P1");
		label2=new JLabel();
		label2.setText("P2");
		label3=new JLabel();
		label3.setText("P3");
		label4=new JLabel();
		label4.setText("X");
		label5=new JLabel();
		label5.setText("Y");
		
		label6=new JLabel();
		label6.setText("X");
		label7=new JLabel();
		label7.setText("Y");
		label8=new JLabel();
		label8.setText("X");
		label9=new JLabel();
		label9.setText("Y");
		label10=new JLabel();
		label10.setText("                 ");
		label11=new JLabel();
		label11.setText("                 ");
		
		//add GUI to panel1
		panel1.add(label1);
		panel1.add(label4);
		panel1.add(textField1);
		panel1.add(label5);
		panel1.add(textField2);
		panel1.add(label10);
		//add GUI to panel2
		panel2.add(label2);
		panel2.add(label6);
		panel2.add(textField3);
		panel2.add(label7);
		panel2.add(textField4);
		panel2.add(checkButton1);
		
		//add GUI to panel3
		panel3.add(label3);
		panel3.add(label8);
		panel3.add(textField5);
		panel3.add(label9);
		panel3.add(textField6);
		panel3.add(label11);
		checkButton1.addActionListener(new buttonListener());
		
		add(panel1);//add panel1 to JFRAME
		add(panel2);//add panel2 to JFRAME
		add(panel3);//add panel3 to JFRAME
		
	}

	public static void main(String argc[])
	{
		
		A42 test =new A42();									//create A42
		test.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		test.setLocation(400,400);								//create test at (400,400)
		test.setResizable(false); 								//can't be resizable
		test.setSize(500,150);									//set frame size
		test.setVisible(true);									//display frame
	}
	private class buttonListener implements ActionListener
	{
		//handle button event
		public void actionPerformed(ActionEvent e)
		{
			String question1,question2,question3,question4,message1,message2,message3,message4,message5;
			String message6,message7,message8,message9,message10,message11;
			int[] inputarray=new int[6];			// create an array with 10 integers
			//take the 10 String of the TextFields to Integer
			inputarray[0]=Integer.parseInt(textField1.getText());
			inputarray[1]=Integer.parseInt(textField2.getText());
			inputarray[2]=Integer.parseInt(textField3.getText());
			inputarray[3]=Integer.parseInt(textField4.getText());
			inputarray[4]=Integer.parseInt(textField5.getText());
			inputarray[5]=Integer.parseInt(textField6.getText());
			//
			
			double temp1[]=new double[6];
			double temp2[]=new double[2];
			
			MyPoint myPit1=new MyPoint(inputarray[0],inputarray[1]);
			MyPoint myPit2=new MyPoint(inputarray[2],inputarray[3]);
			MyPoint myPit3=new MyPoint(inputarray[4],inputarray[5]);
			Triangle2D myTriangle1=new Triangle2D(myPit1.getX(),myPit1.getY(),myPit2.getX(),myPit2.getY(),myPit3.getX(),myPit3.getY());
			//The messages
			message1=String.format("The area of the new triangle is %f\n",myTriangle1.getArea());
			message2=String.format("The perimeter of the new triangle is %f\n",myTriangle1.getPerimeter());
			message3=String.format("Please input a new triangle for the new object:");
			message4=String.format("Enter the point of the x-coordinate");
			message5=String.format("Enter the point of the y-coordinate");
			message7=String.format("The new triangle of new object is in the original triangle of the old object!!!");
			message8=String.format("The new triangle of new object is NOT in the original triangle of the old object!!!");
			message9=String.format("The new point is  in the original triangle (0,0), (17,6), (10, 15)!!!");
			message10=String.format("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!");
			message11=String.format("Please enter 1~5");
			do 
			{
				message6=String.format("1.get the area of the new triangle\n"
							          +"2.get the perimeter of the new triangle\n" 
							          +"3.check the new triangle is in the original triangle or not\n" 
							          +"4.enter a point and check the point is in the original triangle or not\n"
							          +"5.Exit");
						
				question1=JOptionPane.showInputDialog(message6);// show the InputDialog
				int times=1;
				switch(Integer.parseInt(question1))
				{
					case 1:
						JOptionPane.showMessageDialog(null, message1);//show MessageDialog
						
						break;
					case 2:
						JOptionPane.showMessageDialog(null, message2);//show MessageDialog
						break;
					case 3:
						JOptionPane.showMessageDialog(null, message3);//show MessageDialog
						for(int i=0;i<=4;i+=2)
						{
							
							question3=JOptionPane.showInputDialog("P"+times+"   "+message4);//show InputDialog
							temp1[i]=Integer.parseInt(question3);
							question4=JOptionPane.showInputDialog("P"+times+"   "+message5);//show InputDialog
							temp1[i+1]=Integer.parseInt(question4);
							times+=1;
						}
						MyPoint myPit4=new MyPoint(temp1[0],temp1[1]);
						MyPoint myPit5=new MyPoint(temp1[2],temp1[3]);
						MyPoint myPit6=new MyPoint(temp1[4],temp1[5]);
						Triangle2D myTriangle2=new Triangle2D(myPit4.getX(),myPit4.getY(),myPit5.getX(),myPit5.getY(),myPit6.getX(),myPit6.getY());
						if(myTriangle1.contains(myTriangle2)==true)
							JOptionPane.showMessageDialog(null, message7);//show MessageDialog
						else 
							JOptionPane.showMessageDialog(null, message8);//show MessageDialog
						break;
					case 4:
					
						question4=JOptionPane.showInputDialog(message4);//show InputDialog
						temp2[0]=Integer.parseInt(question4);
						question4=JOptionPane.showInputDialog(message5);//show InputDialog
						temp2[1]=Integer.parseInt(question4);
						
						MyPoint myPit7=new MyPoint(temp2[0],temp2[1]);
						if(myTriangle1.contains(myPit7)==true)
							JOptionPane.showMessageDialog(null, message9);//show MessageDialog
							
						else
							JOptionPane.showMessageDialog(null, message10);//show MessageDialog
							
						break;
					case 5:
						return;
					default:
						JOptionPane.showMessageDialog(null, message11);
						
						break;
				}
				message6=String.format("Do again? y=1/n=2\n");
				question2=JOptionPane.showInputDialog(message6);//show InputDialog
				

			}while(Integer.parseInt(question2)==1);
			
			
		}
	}
	
}
