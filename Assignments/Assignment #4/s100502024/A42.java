package a4.s100502024;
import java.util.Scanner;
public class A42 
{
	public static void main (String[] args)
	{
		boolean flag = true; 
		System.out.println("Please enter three points of the new triangle: ");
		Scanner input = new Scanner (System.in);
		double p1x = input.nextDouble();
		double p1y = input.nextDouble();
		double p2x = input.nextDouble();
		double p2y = input.nextDouble();
		double p3x = input.nextDouble();
		double p3y = input.nextDouble();
		Triangle2D triangle1 = new Triangle2D(p1x,p1y,p2x,p2y,p3x,p3y); // 建立一個新的Object並傳值進去
		while (flag == true) // 判斷是否跳出迴圈
		{
			System.out.println("1.Get the area of the new triangle"); // 選項
			System.out.println("2.Get the perimeter of the new triangle");
			System.out.println("3.Check the new triangle is in the original triangle or not");
			System.out.println("4.Enter a point and check the point is in the original triangle or not");
			System.out.println("5.Exit");
			int choose = input.nextInt(); 
			switch (choose)
			{
				case 1:
					System.out.println("The area of the new triangle is "+triangle1.getArea()); // 顯示新三角形的面積
					break;
				case 2:
					System.out.println("The perimeter of the new triangle is "+triangle1.getPerimeter()); // 顯示新三角形的周長
					break;
				case 3:
					System.out.println("Please input a new triangle for the new object: "); // 輸入新三角形的三個點
					p1x = input.nextDouble();
					p1y = input.nextDouble();
					p2x = input.nextDouble();
					p2y = input.nextDouble();
					p3x = input.nextDouble();
					p3y = input.nextDouble();
					Triangle2D triangle2 = new Triangle2D(p1x,p1y,p2x,p2y,p3x,p3y); // 再建立一個新的Object並傳點進去
					if (triangle1.contains(triangle2) == true)
					{
						System.out.println("The new triangle of new object is in the original triangle of the old object!!!"); // 顯示新三角形是否在舊三角形裡
					}
					else
					{
						System.out.println("The new triangle of new object is NOT in the original triangle of the old object!!!");
					}
					break;
				case 4:
					System.out.println("Enter a point :");
					double new_px = input.nextDouble(); 
					double new_py = input.nextDouble();
					MyPoint new_point = new MyPoint(new_px,new_py); // 建立一個新的物件new_point
					if (triangle1.contains(new_point) == true) // 判斷點是否在舊三角形裡
					{
						System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!"); // 顯示資訊
					}
					else
					{
						System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!");
					}
					break;
				case 5:
					flag = false; // 跳出迴圈
					break;
			}
		}
	}
}
