package a4.s100502024;
import java.lang.Math;
public class MyPoint 
{
	private double x; // private date
	private double y;
	 
	public MyPoint() // 一個class裡可以有兩個constructor
	{
		
	}
	public MyPoint (double x_input,double y_input) // constructor(有吃參數)
	{
		setX(x_input);
		setY(y_input);
	}
	public double getX() // 回傳X值
	{
		return x;
	}
	public double getY() // 回傳Y值
	{
		return y;
	}
	public void setX(double input) // 設定X座標,並把X座標存入private date裡
	{
		x = input;
	}
	public void setY(double input) // 設定Y座標,並把Y座標存入private date裡
	{
		y = input;
	}
	public double distance(MyPoint input) // 計算此Object點與傳入的新Object點之間的距離
	{
		double Distance = Math.pow(Math.pow(x-input.getX(),2) + Math.pow(y-input.getY(),2),0.5);
		return Distance;
	}
}
