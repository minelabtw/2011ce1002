package a4.s100502517;

import java.lang.Math;

public class MyPoint {
	private double x;
	private double y;

	public MyPoint(){
	}

	public MyPoint(double x_input, double y_input){ // constructor to initial private data member
		x = x_input;
		y = y_input;
	}

	public double getX(){ // return x value
		return x;
	}

	public double getY(){ // return y value
		return y;
	}

	public void setX(double input) // set x value
	{
		x = input;
	}

	public void setY(double input) // set y value
	{
		y = input;
	}

	public double distance(MyPoint input){ // the distance between this point and the input point
		double a = Math.pow( x-input.getX() , 2 );
		double b = Math.pow( y-input.getY() , 2);
		return Math.pow( a+b , 0.5);
	}
}