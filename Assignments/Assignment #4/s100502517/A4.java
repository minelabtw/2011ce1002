package a4.s100502517;

import java.util.Scanner;

public class A4 {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		
		boolean run = true;		
		
		System.out.println("Please enter three points of the new triangle: ");
		MyPoint p1 = new MyPoint(input.nextDouble(), input.nextDouble());
		MyPoint p2 = new MyPoint(input.nextDouble(), input.nextDouble());
		MyPoint p3 = new MyPoint(input.nextDouble(), input.nextDouble());//input the first triangle
		
		Triangle2D triangle = new Triangle2D(p1, p2, p3);
		
		while(run){
		
			System.out.println("\nYou have following Options");
			System.out.println("1.get the area of the new triangle");
			System.out.println("2.get the perimeter of the new triangle");
			System.out.println("3.check the new triangle is in the original triangle or not");
			System.out.println("4.enter a point and check the point is in the original triangle or not");
			System.out.println("5.Exit\n\nEnter: ");
			int select = input.nextInt();
			
			if(select>=1 || select <= 5){
			
				switch(select){
					case 1:
						System.out.println("The area of the new triangle is "+triangle.getArea());//get the area of the triangle
						break;
					
					case 2:
						System.out.println("The perimeter of the new triangle is "+triangle.getPerimeter());//get the perimeter of the triangle
						break;
					
					case 3:					
						System.out.println("Please input a new triangle for the new object: ");// input another new triangle to check if the new triangle is in the original triangle or not
						MyPoint p1_new = new MyPoint(input.nextDouble(), input.nextDouble());
						MyPoint p2_new = new MyPoint(input.nextDouble(), input.nextDouble());
						MyPoint p3_new = new MyPoint(input.nextDouble(), input.nextDouble());
					
						Triangle2D triangle_new = new Triangle2D(p1_new, p2_new, p3_new);
						
						if(triangle.contains(triangle_new) == true){
							System.out.println("The new triangle of new object is in the original triangle of the old object!!!");
						}
						else{
							System.out.println("The new triangle of new object is not in the original triangle of the old object!!!");
						}
						break;
					
					case 4:
						MyPoint p_new = new MyPoint(input.nextDouble(), input.nextDouble());//check the point you input is in the original triangle or not
						
						if(triangle.contains(p_new) == true){
							System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!");
						}
						else{
							System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!");
						}
						break;
					case 5:
						run = false;//end the while
						break;
				}
			}
			else{
				System.out.println("Error~!!!");//if you input the number >5 or < 1 you will get Error
			}
		}
		System.out.println("Having Fun ~ SEE YOU ~ !!");
		
	}

}
