package a4.s100502517;

import java.lang.Math;

public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0, 0.0); // points of original triangle triangle
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint p1_new, p2_new, p3_new; // points of new triangle

	public MyPoint getP1_new(){ // get the p1 of new triangle
		return p1_new;
	}

	public MyPoint getP2_new(){ // get the p2 of new triangle
		return p2_new;
	}

	public MyPoint getP3_new(){ // get the p3 of new triangle
		return p3_new;
	}

	// constructor with arguments to initial the points of the new triangle
	public Triangle2D(MyPoint p1new, MyPoint p2new, MyPoint p3new){
		p1_new = p1new;
		p2_new = p2new;
		p3_new = p3new;
	}
	
	public double getArea(){ // calculate the area of the new triangle
		double x_s = (p1_new.distance(p2_new)+p2_new.distance(p3_new)+p3_new.distance(p1_new));
		return Math.pow(0.5, x_s*(x_s-p1_new.distance(p2_new))*(x_s-p2_new.distance(p3_new)*(x_s-p3_new.distance(p1_new))));
	}

	public double getPerimeter(){ // calculate the perimeter of the new triangle
		return p1_new.distance(p2_new)+p2_new.distance(p3_new)+p3_new.distance(p1_new);
	}

	// check the input point is in the original triangle of the object or not
	
	public boolean contains(MyPoint p){
		// if input point p is in the triangle, area of PAB + area of PBC + area of PCA = area of ABC 
		double s_PAB = (p1.distance(p2)+p2.distance(p)+p.distance(p1))/2;
		double s_PBC = (p2.distance(p3)+p3.distance(p)+p.distance(p2))/2;
		double s_PCA = (p1.distance(p3)+p3.distance(p)+p.distance(p1))/2;
		
		double x_s = (p1.distance(p2)+p2.distance(p3)+p3.distance(p1))/2;
		double area_ABC = Math.pow( x_s*(x_s-p1.distance(p2))*(x_s-p2.distance(p3))*(x_s-p3.distance(p1)) , 0.5 );
		
		
		double area_PAB = Math.pow( s_PAB*(s_PAB-p1.distance(p2))*(s_PAB-p2.distance(p))*(s_PAB-p.distance(p1)) , 0.5 );
		double area_PBC = Math.pow( s_PBC*(s_PBC-p2.distance(p3))*(s_PBC-p3.distance(p))*(s_PBC-p.distance(p2)) , 0.5 );
		double area_PCA = Math.pow( s_PCA*(s_PCA-p1.distance(p3))*(s_PCA-p3.distance(p))*(s_PCA-p.distance(p1)) , 0.5 );
		
		if(Math.abs(area_ABC-area_PAB-area_PBC-area_PCA) < 0.5){//check if the point p is in the triangle
			return true;
		}
		else{
			return false;
		}
	}

	// check the input triangle(Triangle2D object) is in the original triangle of this object or not
	public boolean contains(Triangle2D input){
		if(input.contains(input.getP1_new()) == true && input.contains(input.getP2_new()) == true && input.contains(input.getP1_new())){
			return true;
			}
		else{
			return false;
		}
		
	       // if the input triangle is the original triangle of this object
               // then all points of the input triangle is in the original triangle of this object
	}
}