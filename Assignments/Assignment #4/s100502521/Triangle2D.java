package a4.s100502521;
import java.lang.Math;
public class Triangle2D 
{
	private MyPoint p1 = new MyPoint(0.0, 0.0); // points of original triangle triangle
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint p1_new, p2_new, p3_new; // points of new triangle

	public MyPoint getP1_new()
	{ // get the p1 of new triangle
		return p1_new;
	}

	public MyPoint getP2_new()
	{ // get the p2 of new triangle
		return p2_new;
	}

	public MyPoint getP3_new()
	{ // get the p3 of new triangle
		return p3_new;
	}

	// constructor with arguments to initial the points of the new triangle
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3)
	{
		p1_new=new MyPoint(x1,y1);
		p2_new=new MyPoint(x2,y2);
		p3_new=new MyPoint(x3,y3);
	}

	public double getArea()
	{ // calculate the area of the new triangle
		double s=getPerimeter()/2;
		return Math.sqrt(s*(s-getTwoPointDistance(p1_new,p2_new))*(s-getTwoPointDistance(p2_new,p3_new))*(s-getTwoPointDistance(p3_new,p1_new)));
	}

	public double getPerimeter()
	{ // calculate the perimeter of the new triangle
		return (getTwoPointDistance(p1_new,p2_new)+getTwoPointDistance(p2_new,p3_new)+getTwoPointDistance(p3_new,p1_new));
	}

	// check the input point is in the original triangle of the object or not
	public boolean contains(MyPoint p)
	{
		// if input point p is in the triangle, area of PAB + area of PBC + area of PCA = area of ABC 
		double areaPAB, areaPBC, areaPCA, areaABC;

		// calculate area of PAB
		areaPAB=getThreePointArea(p,p1,p2);

		// calculate area of PBC
		areaPBC=getThreePointArea(p,p2,p3);

		// calculate area of PCA
		areaPCA=getThreePointArea(p,p3,p1);

		// calculate area of ABC
		areaABC=getThreePointArea(p1,p2,p3);
		
		// if areaPAB + areaPBC + areaPCA = areaABC, then p is in the ABC
                // However, there is some error(�~�t) in this calculation
                // So if the absolute value of (areaABC - (areaPAB + areaPBC + areaPCA)) is less than 0.5
                // we consider the point is inside the original triangle
                // you can use Math.abs(double) to calculate the absolute value
		if(Math.abs(areaABC-(areaPAB+areaPBC+areaPCA))<0.5)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	// check the input triangle(Triangle2D object) is in the original triangle of this object or not
	public boolean contains(Triangle2D input)
	{
		if(contains(input.getP1_new())&&contains(input.getP2_new())&&contains(input.getP3_new()))
		{
			return true;
		}
		else
		{
			return false;
		}
	       // if the input triangle is in the original triangle of this object
               // then all points of the input triangle is in the original triangle of this object
	}
	public double getTwoPointDistance(MyPoint a,MyPoint b)
	{
		return Math.sqrt((a.getX()-b.getX())*(a.getX()-b.getX())+(a.getY()-b.getY())*(a.getY()-b.getY()));
	}
	public double getThreePointArea(MyPoint a,MyPoint b,MyPoint c)
	{
		double d1=getTwoPointDistance(a,b),d2=getTwoPointDistance(b,c),d3=getTwoPointDistance(c,a);
		double s=(d1+d2+d3)/2;
		return Math.sqrt(s*(s-d1)*(s-d2)*(s-d3));
	}
}

