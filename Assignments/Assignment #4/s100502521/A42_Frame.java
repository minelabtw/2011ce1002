package a4.s100502521;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.image.BufferedImage;
public class A42_Frame extends JFrame implements ActionListener//類別繼承類別，類別實做介面
{
	private static final long serialVersionUID = 1L;//eclipse加的
	private JButton[] button;//按鈕
	private JTextField[] textField;//textbox
	private static JLabel[] label;//label標題
	private JLabel labelImage;//label標題    用來存圖片 當畫布
	private Triangle2D t1=new Triangle2D(0,0,0,0,0,0);
	private Triangle2D t2=new Triangle2D(0,0,0,0,0,0);
	public A42_Frame() //初始化各個物件
	{
		super("三角形!!");
		setLayout(new FlowLayout(FlowLayout.LEFT));
		textField=new JTextField[14];
		for(int i=0;i<14;i++)
		{
			textField[i] = new JTextField(3);
		}		
		button =new JButton[8];
		button[0] = new JButton("計算");
		button[1] = new JButton("清空");
		button[2] = new JButton("計算");
		button[3] = new JButton("清空");
		button[4] = new JButton("計算");
		button[5] = new JButton("清空");
		button[6] = new JButton("計算");
		button[7] = new JButton("清空");
		for(int i=0;i<8;i++)
		{
			button[i].addActionListener(this);// 加入按鈕
		}	
		label = new JLabel[11];
		label[0]= new JLabel("請輸入三角形的3個點:");
		label[1]= new JLabel("1.get the area of the new triangle                                                                                                                           ");
		label[2]= new JLabel("                                                                                                                                                             ");
		label[3]= new JLabel("2.get the perimeter of the new triangle                                                                                                                      ");
		label[4]= new JLabel("                                                                                                                                                             ");
		label[5]= new JLabel("3.check the new triangle is in the original triangle or not.Please input a new triangle for the new object:                        	           			     ");
		label[6]= new JLabel("                                                                                                                                                                ");
		label[7]= new JLabel("4.enter a point and check the point is in the original triangle or not                                                                    			       ");
		label[8]= new JLabel("                                                                                                                                                                 ");
		label[9]= new JLabel("                                                                                        							                  				");
		label[10]= new JLabel("                                                                                                                                                                  				 					");	
		add(label[0]);
		for(int i=0;i<6;i++)
		{
			add(textField[i]);
		}
		add(label[1]);
		add(button[0]);
		add(button[1]);
		add(label[2]);
		add(label[3]);
		add(button[2]);
		add(button[3]);
		add(label[4]);
		add(label[5]);
		for(int i=6;i<12;i++)
		{
			add(textField[i]);
		}
		add(label[9]);
		add(button[4]);
		add(button[5]);
		add(label[6]);
		add(label[7]);
		add(textField[12]);
		add(textField[13]);
		add(label[10]);
		add(button[6]);
		add(button[7]);
		add(label[8]);
		labelImage=new JLabel();
		add(labelImage);
	}
	 
	@Override
	public void actionPerformed(ActionEvent e)//毒入3個值 判斷跟畫圖
	{
		double[] inputDouble=new double[6];
		for(int i=0;i<6;i++)
		{
			inputDouble[i]=Double.parseDouble(textField[i].getText());
		}
		t1=new Triangle2D(inputDouble[0],inputDouble[1],inputDouble[2],inputDouble[3],inputDouble[4],inputDouble[5]);
		if (e.getSource() == button[0])
		{
			label[2].setText("The area of the new triangle is  "+t1.getArea()+"                                                                                                                              ");
		}	
		else if (e.getSource() == button[1])//清空
		{
			label[2].setText("                                                                                                                                                                               ");
		}
		else if (e.getSource() == button[2])//清空
		{
			label[4].setText("The perimeter of the new triangle is "+t1.getPerimeter()+"                                                                                                     				   ");
		}
		else if (e.getSource() == button[3])//清空
		{
			label[4].setText("                                                                                                                                                                                 ");
		}
		else if (e.getSource() == button[4])//清空
		{
			for(int i=0;i<6;i++)
			{
				inputDouble[i]=Double.parseDouble(textField[i+6].getText());
			}
			t2=new Triangle2D(inputDouble[0],inputDouble[1],inputDouble[2],inputDouble[3],inputDouble[4],inputDouble[5]);
			if( t1.contains(t2) )
			{
				label[6].setText("The new triangle of new object is in the original triangle of the old object!!!                                                          				        ");
			}
			else
			{
				label[6].setText("The new triangle of new object is NOT in the original triangle of the old object!!!                                                          				        ");
			}
			remove(labelImage);
			this.repaint();
			BufferedImage image=new BufferedImage(400,200,BufferedImage.TYPE_INT_ARGB);//設定圖片大小
			Graphics graphics=image.getGraphics();
			graphics.setColor(Color.white);
			graphics.fillRect(0,0,400,200);//全畫白
			graphics.setColor(Color.black);
			graphics.drawLine(200, 0, 200, 200);//畫線
			graphics.drawLine(0, 100, 400, 100);//畫線
			graphics.drawString("X", 390, 115);//畫字
			graphics.drawString("Y", 190, 10);//畫字
			int xPoint[] = { 200+0, 200+17, 200+10 };
			int yPoint[] = { 100-0, 100-6, 100-15 };
			graphics.setColor(Color.red);
			graphics.fillPolygon(xPoint, yPoint, 3);
			graphics.setColor(Color.black);
			int x2Point[] = { (int) (200+t2.getP1_new().getX()), (int) (200+t2.getP2_new().getX()), (int) (200+t2.getP3_new().getX()) };
			int y2Point[] = { (int) (100-t2.getP1_new().getY()), (int) (100-t2.getP2_new().getY()), (int) (100-t2.getP3_new().getY()) };
			graphics.setColor(Color.blue);
			graphics.fillPolygon(x2Point, y2Point, 3);
			//graphics.fillOval(196+(int)x, 96-(int)y, 8, 8);//畫紅點
			graphics.dispose();//釋放資源
			labelImage=new JLabel(new ImageIcon(image));//設定labelImage
			add(labelImage);
			this.repaint();//重畫視窗
			button[4].setEnabled(false);//讓使用者可以按確定
			button[6].setEnabled(false);//讓使用者可以按確定
		}
		else if (e.getSource() == button[5])//清空
		{
			label[6].setText("                                                                                                                                                                                 ");
			for(int i=6;i<12;i++)
			{
				textField[i].setText("");
			}
			remove(labelImage);
			button[4].setEnabled(true);//讓使用者可以按確定
			button[6].setEnabled(true);//讓使用者可以按確定
			this.repaint();//重畫視窗*/
		}
		else if (e.getSource() == button[6])//清空
		{
			for(int i=0;i<2;i++)
			{
				inputDouble[i]=Double.parseDouble(textField[i+12].getText());
			}
			MyPoint temp=new MyPoint(inputDouble[0],inputDouble[1]);
			if ( t1.contains(temp) )
			{
				label[8].setText("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!                                                          				        ");
			}
			else
			{
				label[8].setText("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!                                                          				        ");
			}
			remove(labelImage);
			this.repaint();
			BufferedImage image=new BufferedImage(400,200,BufferedImage.TYPE_INT_ARGB);//設定圖片大小
			Graphics graphics=image.getGraphics();
			graphics.setColor(Color.white);
			graphics.fillRect(0,0,400,200);//全畫白
			graphics.setColor(Color.black);
			graphics.drawLine(200, 0, 200, 200);//畫線
			graphics.drawLine(0, 100, 400, 100);//畫線
			graphics.drawString("X", 390, 115);//畫字
			graphics.drawString("Y", 190, 10);//畫字
			int xPoint[] = { 200+0, 200+17, 200+10 };
			int yPoint[] = { 100-0, 100-6, 100-15 };
			graphics.setColor(Color.red);
			graphics.fillPolygon(xPoint, yPoint, 3);
			graphics.setColor(Color.blue);
			graphics.fillOval(196+(int)temp.getX(), 96-(int)temp.getY(), 8, 8);//畫紅點
			graphics.dispose();//釋放資源
			labelImage=new JLabel(new ImageIcon(image));//設定labelImage
			add(labelImage);
			this.repaint();//重畫視窗
			button[4].setEnabled(false);//讓使用者可以按確定
			button[6].setEnabled(false);//讓使用者可以按確定
		}
		else if (e.getSource() == button[7])//清空
		{
			label[8].setText("                                                                                                                                                                                 ");
			for(int i=12;i<14;i++)
			{
				textField[i].setText("");
			}
			remove(labelImage);
			button[4].setEnabled(true);//讓使用者可以按確定
			button[6].setEnabled(true);//讓使用者可以按確定
			this.repaint();//重畫視窗*/
		}			
	}
}
