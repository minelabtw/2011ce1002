package a4.s100502521;
import java.util.Scanner;
import javax.swing.JFrame;

public class A42
{
	public static void main(String[] args)
	{
		Scanner input=new Scanner(System.in);
		System.out.println("輸入<1>跳出GUI介面<2>在小黑窗輸入<0>離開");
		int choose=input.nextInt();
		switch(choose)
		{
		case 1:
			JFrame demo = new A42_Frame();//創造視窗
			demo.setSize(600,650);//設定大小
			demo.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//設定視窗標題列的關閉按鈕結束程式執行
			demo.setAlwaysOnTop(true);//讓它永遠在最上層XDD
			demo.setVisible(true);//顯示視窗
			demo.setResizable(false);//設定不可更改大小
			break;
		case 2:
			littleBlack();
			break;
		case 0:
			break;
		default:
			System.out.println("輸入方法不對，程式結束!!88~");
			break;
		}
	}
	public static void littleBlack()
	{
		double[] inputDouble=new double[6];
		int choose=0;
		Triangle2D t1,t2;
		Scanner input=new Scanner(System.in);
		System.out.println("Please enter three points of the new triangle:");
		for(int i=0;i<6;i++)
		{
			inputDouble[i]=input.nextInt();
		}
		t1=new Triangle2D(inputDouble[0],inputDouble[1],inputDouble[2],inputDouble[3],inputDouble[4],inputDouble[5]);
		while(choose!=5)//使用者選擇
		{
			System.out.print("\n1.get the area of the new triangle\n2.get the perimeter of the new triangle\n3.check the new triangle is in the original triangle or not\n4.enter a point and check the point is in the original triangle or not\n5.Exit\n");
			choose=input.nextInt();
			switch(choose)
			{
				case 1://面積
					System.out.printf("The area of the new triangle is %.2f", t1.getArea());
					break;
				case 2://周長
					System.out.printf("The area of the new triangle is %.2f", t1.getPerimeter());
					break;
				case 3://檢查3絞刑是否在三角形內
					System.out.println("Please input a new triangle for the new object:");
					for(int i=0;i<6;i++)
					{
						inputDouble[i]=input.nextInt();
					}
					t2=new Triangle2D(inputDouble[0],inputDouble[1],inputDouble[2],inputDouble[3],inputDouble[4],inputDouble[5]);
					if( t1.contains(t2) )
					{
						System.out.println("The new triangle of new object is in the original triangle of the old object!!!");
					}
					else
					{
						System.out.println("The new triangle of new object is Not in the original triangle of the old object!!!");
					}
					break;
				case 4://檢查點是否在三角形內
					System.out.println("Please input a point:");
					inputDouble[0]=input.nextDouble();
					inputDouble[1]=input.nextDouble();
					MyPoint temp=new MyPoint(inputDouble[0],inputDouble[1]);
					if ( t1.contains(temp) )
					{
						System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!");
					}
					else
					{
						System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!");
					}
					break;
				case 5:
					System.out.println("程式結束! 掰掰~~~");
					break;
				default:
					System.out.println("輸入有誤，請重新輸入");
					break;
			}
		}
	}
}
