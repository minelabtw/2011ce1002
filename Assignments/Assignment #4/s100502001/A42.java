package a4.s100502001;
import java.util.Scanner;
public class A42 {
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);
		System.out.println("Please Enter three point of the new triangle: ");
		double[][] point_1=new double[3][2];
		for(int i=0;i<point_1.length;i++){ //input tri_1's new triangle 
			for(int j=0;j<point_1[0].length;j++)
				point_1[i][j]=input.nextDouble();	
		}
		System.out.println("1.get the area of the new triangle");
		System.out.println("2.get the perimeter of the new triangle");
		System.out.println("3.check the new triangle is in the original triangle or not");
		System.out.println("4.enter a point and check the point is in the original triangle or not");
		System.out.println("5.Exit");
		boolean judge=true;
		Triangle2D tri_1=new Triangle2D(point_1[0][0],point_1[0][1],point_1[1][0],point_1[1][1],point_1[2][0],point_1[2][1]);
		while(judge){
			int choice=input.nextInt();
			switch(choice){
				case 1:
					System.out.println("The area of the new triangle is "+tri_1.getArea());
					break;
				case 2:
					System.out.println("The perimeter of the new triangle is "+tri_1.getPerimeter());
					break;
				case 3:
					System.out.println("Please Enter three point of the new triangle: ");
					double[][] point_2=new double[3][2];//input tri_2's new triangle
					for(int i=0;i<point_2.length;i++){
						for(int j=0;j<point_2[0].length;j++)
							point_2[i][j]=input.nextDouble();
					}
					Triangle2D tri_2=new Triangle2D(point_2[0][0],point_2[0][1],point_2[1][0],point_2[1][1],point_2[2][0],point_2[2][1]);
					if(tri_1.contains(tri_2)) //judge whether old triangle of tri_1 is equal to new triangle of tri_2
						System.out.println("The new triangle of new object is in the original triangle of the old object!!!^.<!!");
					else
						System.out.println("The new triangle of new object is not in the original triangle of the old object!!!= =!!");
					break;
				case 4:
					double x=input.nextDouble();
					double y=input.nextDouble();
					if(tri_1.contains(x,y))//judge whether the point is within the original triangle 
						System.out.println("The new point is in the original triangle (0,0) (10,15) (17,6)");
					else
						System.out.println("The new point is NOT in the original triangle! (0,0) (10,15) (17,6) !");
					break;
				case 5:
					System.out.println("See you next time~~");
					judge=false;
					break;
				default:
					System.out.println("There is not the choice.");
					break;
			}
		}
	}

}
