package a4.s100502001;

public class MyPoint { //儲存點坐標用的
	private double x;
	private double y;
	public MyPoint(){ //purely declare object
		
	}
	public MyPoint(double X, double Y){ //initialize the constructor
		x=X;
		y=Y;
	}
	public void setX(double XX){
		x=XX;
	}
	public void setY(double YY){
		y=YY;
	}
	public double getX(){
		return x;
	}
	public double getY(){
		return y;
	}
	public double distance(MyPoint input){ //calculate the distance between this point and the input point 
		return Math.sqrt((input.getX()-x)*(input.getX()-x)+(input.getY()-y)*(input.getY()-y));
	}
}
