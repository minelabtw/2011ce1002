package a4.s100502001;

public class Triangle2D { //儲存兩個三角形
	//The points of the original triangle
	private MyPoint p1=new MyPoint(0.0,0.0);
	private MyPoint p2=new MyPoint(10.0,15.0);
	private MyPoint p3=new MyPoint(17.0,6.0);
	private MyPoint get_p1_new,get_p2_new,get_p3_new;
	public MyPoint getP1_new(){ //return object whose type is "MyPoint"
		return get_p1_new;
	}
	public MyPoint getP2_new(){ //return object whose type is "MyPoint"
		return get_p2_new;
	}
	public MyPoint getP3_new(){ //return object whose type is "MyPoint"
		return get_p3_new;
	}
	public Triangle2D(){
		
	}
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3){ //initialize the new triangle's point and calculate the lengths
		get_p1_new=new MyPoint(x1,y1);
		get_p2_new=new MyPoint(x2,y2);
		get_p3_new=new MyPoint(x3,y3);
	}
	
	public double getArea(){ //get the new triangle's area
		double s=getPerimeter()/2;
		return Math.sqrt(s*(s-getP1_new().distance(getP2_new()))*(s-getP1_new().distance(getP3_new()))*(s-getP2_new().distance(getP3_new())));
	}
	public double getPerimeter(){ //get the new triangle's perimeter
		return getP1_new().distance(getP2_new())+getP1_new().distance(getP3_new())+getP2_new().distance(getP3_new());
	}
	public boolean contains(double XX, double YY){ //judge whether the point is inside the original triangle
		MyPoint user_input=new MyPoint(XX,YY);//將P點放進MyPoint這個class的user_input的object之中
		double[] intenal_area=new double[3];
		double[] intenal_perimeter=new double[3];
		
		//calculate triangle PBC
		intenal_perimeter[0]=(p2.distance(user_input)+p3.distance(user_input)+p2.distance(p3))/2;
		intenal_area[0]=Math.sqrt(intenal_perimeter[0]*(intenal_perimeter[0]-p2.distance(user_input))*(intenal_perimeter[0]-p3.distance(user_input))*(intenal_perimeter[0]-p2.distance(p3)));
		
		//calculate triangle PCA
		intenal_perimeter[1]=(p3.distance(user_input)+p1.distance(user_input)+p3.distance(p1))/2;
		intenal_area[1]=Math.sqrt(intenal_perimeter[1]*(intenal_perimeter[1]-p3.distance(user_input))*(intenal_perimeter[1]-p1.distance(user_input))*(intenal_perimeter[1]-p3.distance(p1)));
		
		//calculate triangle PAB
		intenal_perimeter[2]=(p1.distance(user_input)+p2.distance(user_input)+p1.distance(p2))/2;
		intenal_area[2]=Math.sqrt(intenal_perimeter[2]*(intenal_perimeter[2]-p1.distance(user_input))*(intenal_perimeter[2]-p2.distance(user_input))*(intenal_perimeter[2]-p1.distance(p2)));
		
		//calculate the total triangle area
		double original_length=(p1.distance(p2)+p1.distance(p3)+p2.distance(p3))/2;
		double original_area=Math.sqrt(original_length*(original_length-p1.distance(p2))*(original_length-p1.distance(p3))*(original_length-p2.distance(p3)));
		double point_area=0;
		for(int i=0;i<intenal_area.length;i++)
			point_area+=intenal_area[i];
		if(Math.abs(original_area-point_area)<=0.5)//if 誤差 < 0.5 則回傳true
			return true;
		return false;
	}
	public boolean contains(Triangle2D tri_input){ //observe whether the new triangle is within the original one
		// if the three point is within the original triangle, then the special triangle is within the original ones
		if(tri_input.contains(tri_input.getP1_new().getX(),tri_input.getP1_new().getY())&&tri_input.contains(tri_input.getP2_new().getX(),tri_input.getP2_new().getY())&&tri_input.contains(tri_input.getP3_new().getX(),tri_input.getP3_new().getY()))
			return true;
		return false;
	}
}
