package a4.s100502520;

import java.util.Scanner;

import org.omg.Messaging.SYNC_WITH_TRANSPORT;

public class A42 {
	public static void main(String[] args){
		double[] store1 = new double[6];
		double[] store2 = new double[6];
		double a,b,c,d,e,f;
		double new_p_x;
		double new_p_y;
		Scanner input = new Scanner(System.in);
		System.out.println("Please enter three points of the new triangle: ");
		for(int i = 0; i<6; i++){
			store1[i] = input.nextDouble();
		}
		//用使用者輸入的值創造一個三角型物件
		Triangle2D new1 = new Triangle2D(store1[0],store1[1],store1[2],store1[3],store1[4],store1[5]);
		int j = 1;
		int choose;
		while(j==1){
			System.out.println("1.get the area of the new triangle");
			System.out.println("2.get the perimeter of the new triangle");
			System.out.println("3.check the new triangle is in the original triangle or not");
			System.out.println("4.enter a point and check the point is in the original triangle or not");
			System.out.println("5.Exit");
			choose = 0;
			choose = input.nextInt();
			switch (choose) {
				//顯示面積
				case 1:
					System.out.println("The area of the new triangle is " + new1.getArea());
					break;
				//顯示周長
				case 2:
					System.out.println("The perimeter of the new triangle is " + new1.getPerimeter());
					break;
				//比較三角形是否在預設三角形裡
				case 3:
					System.out.println("Please input a new triangle for the new object: ");
					for(int k=0; k<6; k++){
						store2[k] = input.nextDouble();
					}
					Triangle2D new2 = new Triangle2D(store2[0],store2[1],store2[2],store2[3],store2[4],store2[5]);
					if(new1.contains_tri(new2)){
						System.out.println("The new triangle of new object is in the original triangle of the old object!!!");
					}
					else {
						System.out.println("The new triangle of new object is NOT in the original triangle of the old object!!!");
					}
					break;
				//比較點是否在預設三角型內
				case 4:
					System.out.println("Please input a new point :");
					new_p_x = input.nextDouble();
					new_p_y = input.nextDouble();
					MyPoint newPoint = new MyPoint(new_p_x, new_p_y);
					if(new1.contains_p(newPoint)){
						System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!");
					}
					else {
						System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!");
					}
					break;
				//離開
				case 5:
					System.out.println("bye bye");
					j = 0;
					break;
				default:
					break;
			}
		}
	}
}
