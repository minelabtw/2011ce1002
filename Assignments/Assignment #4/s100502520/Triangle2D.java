package a4.s100502520;

public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0, 0.0);
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint p1_new, p2_new, p3_new;
	
	//初始化  p1_new, p2_new, p3_new
	public Triangle2D(double x1, double y1, double x2, double y2, double x3,
			double y3) {
		p1_new = new MyPoint(x1,y1);
		p2_new = new MyPoint(x2,y2);
		p3_new = new MyPoint(x3,y3);
	}

	//初始化  p1_new
	public MyPoint getP1_new() {
		return p1_new;
	}

	//初始化  p2_new
	public MyPoint getP2_new() {
		return p2_new;
	}

	//初始化  p3_new
	public MyPoint getP3_new() {
		return p3_new;
	}
	
	//回傳 三角形的值
	public double getArea(){
		return setArea(p1_new, p2_new, p3_new);
	}
	
	//計算三角形的值
	public double setArea(MyPoint a, MyPoint b, MyPoint c){
		double p =(a.distance(b)+b.distance(c)+c.distance(a))/2.0;
		return Math.pow((p*(p-a.distance(b))*(p-b.distance(c))*(p-c.distance(a))),0.5);
	}

	//計算並回傳三角型的周長
	public double getPerimeter() {
		return (p1_new.distance(p2_new)+p2_new.distance(p3_new)+p3_new.distance(p1_new));
	}

	//判斷p是否在預設三角型內
	public boolean contains_p(MyPoint p) {
		double areaABC = setArea(p1, p2, p3);
		double areaPAB = setArea(p, p2, p3);
		double areaPBC = setArea(p1, p, p3);
		double areaPCA = setArea(p1, p2, p);
		if(Math.abs(areaABC-areaPAB-areaPBC-areaPCA)<=0.5){
			return true;
		}
		else {
			return false;
		}
	}

	//判斷new2三角型是否在預設三角型內
	public boolean contains_tri(Triangle2D new2) {
		if(new2.contains_p(new2.getP1_new()) && new2.contains_p(new2.getP2_new()) && new2.contains_p(new2.getP3_new())){
			return true;
		}
		else {
			return false;
		}
		
	}
}
