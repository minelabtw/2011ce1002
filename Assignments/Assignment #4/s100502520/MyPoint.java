package a4.s100502520;

public class MyPoint {
	private double x;
	private double y;

	public MyPoint() {

	}

	//初始化x和y
	public MyPoint(double Xinput, double Yinput) {
		x = Xinput;
		y = Yinput;
	}

	//回傳x
	public double getX() {
		return x;
	}

	//回傳y
	public double getY() {
		return y;
	}

	//初始化x
	public void setX(double inputx) {
		x = inputx;
	}

	//初始化y
	public void setY(double inputy) {
		y = inputy;
	}

	//計算此 MyPoint 與傳入的  MyPoint 之間的距離
	public double distance(MyPoint compare) {
		return Math.pow((Math.pow((x - compare.getX()),2) + Math.pow((y - compare.getY()),2)),0.5);
	}
}
