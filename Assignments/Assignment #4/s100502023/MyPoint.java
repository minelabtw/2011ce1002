package a4.s100502023;
import java.math.*;

public class MyPoint 
{
	private double x;
	private double y;

	public MyPoint()  //未輸入任何參數時，將x,y設為0
	{
		x=0;
		y=0;
	}

	public MyPoint(double x_input, double y_input)
	{ // constructor to initial private data member
		
		x=x_input;
		y=y_input;
	}

	public double getX()
	{ // return x value
		return x;
	}

	public double getY()
	{ // return y value
		return y;
	}

	public void setX(double input) // set x value
	{
		x=input;
	}

	public void setY(double input) // set y value
	{
		y=input;
	}

	public double distance(MyPoint input)
	{ // the distance between this point and the input point
		double distance;
		
		distance=Math.pow(Math.pow(x-input.getX(),2)+Math.pow(y-input.getY(),2), 0.5);
		
		return distance;
	}

}
