package a4.s100502023;
import java.util.*;

public class A42 
{

	public static void main(String[] args)
	{
		System.out.print("Please enter three points of the new triangle:");
		
		Scanner input= new Scanner(System.in);
		
		double p1_new_x=input.nextDouble();
		double p1_new_y=input.nextDouble();
		double p2_new_x=input.nextDouble();
		double p2_new_y=input.nextDouble();
		double p3_new_x=input.nextDouble();
		double p3_new_y=input.nextDouble();
		
		//origin triangle object
		Triangle2D triangle= new Triangle2D(p1_new_x,p1_new_y,p2_new_x,p2_new_y,p3_new_x,p3_new_y);
		
		boolean done=true;
		
		while(done)
		{
			System.out.println("\n\n1.get the area of the new triangle");
			System.out.println("2.get the perimeter of the new triangle");
			System.out.println("3.check the new triangle is in the original triangle or not");
			System.out.println("4.enter a point and check the point is in the original triangle or not");
			System.out.println("5.Exit\n");
			System.out.print("input:");
			
			int choice=input.nextInt();  //choice儲存使用者輸入的代碼
			
			switch (choice)
			{
				case 1:  //get the area of the new triangle
					double areaOfNewTriangle;
					areaOfNewTriangle=triangle.getArea();
					System.out.println("The area of the new triangle is " + areaOfNewTriangle);
					break;
					
				case 2:  //get the perimeter of the new triangle
					double perimeterOfNewTriangle;
					perimeterOfNewTriangle=triangle.getPerimeter();
					System.out.println("The perimeter of the new triangle is " + perimeterOfNewTriangle);
					break;
					
				case 3:  //check the new triangle is in the original triangle or not
					System.out.print("Please enter three points of the new triangle:");	
					double p1_new2_x=input.nextDouble();
					double p1_new2_y=input.nextDouble();
					double p2_new2_x=input.nextDouble();
					double p2_new2_y=input.nextDouble();
					double p3_new2_x=input.nextDouble();
					double p3_new2_y=input.nextDouble();
					
					//new triangle2 object
					Triangle2D triangle2= new Triangle2D(p1_new2_x,p1_new2_y,p2_new2_x,p2_new2_y,p3_new2_x,p3_new2_y);
					
					if (triangle.contains(triangle2)==true)
					{
						System.out.println("The new triangle of new object is in the original triangle of the old object!!!");	
					}
					else  //false
					{
						System.out.println("The new triangle of new object is Not in the original triangle of the old object");
					}
					
					break;
					
				case 4:  //enter a point and check the point is in the original triangle or not
					System.out.print("Enter the point:");
					double point_x=input.nextDouble();
					double point_y=input.nextDouble();
					
					MyPoint point = new MyPoint(point_x,point_y);
					
					if (triangle.contains(point)==true)
					{
						System.out.print("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!");
					}
					else
					{
						System.out.print("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!");
					}
					
					break;
					
				case 5:  //end
					done=false;
					break;
			}
			
		}
		

	}

}
