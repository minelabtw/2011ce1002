package a4.s100502023;
import java.math.*;

public class Triangle2D 
{
	
	private MyPoint p1 = new MyPoint(0.0, 0.0); // points of original triangle triangle
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint p1_new= new MyPoint(), p2_new= new MyPoint(), p3_new= new MyPoint(); // points of new triangle

	public MyPoint getP1_new()
	{ // get the p1 of new triangle
		return p1_new;
	}

	public MyPoint getP2_new()
	{ // get the p2 of new triangle
		return p2_new;
	}

	public MyPoint getP3_new()
	{	// get the p3 of new triangle
		return p3_new;
	}

	// constructor with arguments to initial the points of the new triangle
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3)
	{		
		p1_new.setX(x1);
		p1_new.setY(y1);
		p2_new.setX(x2);
		p2_new.setY(y2);
		p3_new.setX(x3);
		p3_new.setY(y3);
	}

	public double getArea()
	{	// calculate the area of the new triangle
		double side1, side2, side3, s;
		double area;
		
		side1=p1_new.distance(p2_new);
		side2=p2_new.distance(p3_new);
		side3=p3_new.distance(p1_new);
		
		//���s����
		s=(side1+side2+side3)/2;
		
		area=Math.pow(s*(s-side1)*(s-side2)*(s-side3),0.5);
		
		return area;
		
	}

	public double getPerimeter()
	{	// calculate the perimeter of the new triangle
		double side1, side2, side3;
		double perimeter;
		
		side1=p1_new.distance(p2_new);
		side2=p2_new.distance(p3_new);
		side3=p3_new.distance(p1_new);
		
		perimeter=side1+side2+side3;
		
		return perimeter;
	}

	// check the input point is in the original triangle of the object or not
	public boolean contains(MyPoint p)
	{
		// if input point p is in the triangle, area of PAB + area of PBC + area of PCA = area of ABC 
		double side1, side2, side3, s, areaPAB, areaPBC, areaPCA, areaABC;

		// calculate area of PAB
		side1=p1.distance(p);
		side2=p2.distance(p);
		side3=p1.distance(p2);
		s=(side1+side2+side3)/2;
		
		areaPAB=Math.pow(s*(s-side1)*(s-side2)*(s-side3),0.5);

		// calculate area of PBC
		side1=p2.distance(p);
		side2=p3.distance(p);
		side3=p2.distance(p3);
		s=(side1+side2+side3)/2;
		
		areaPBC=Math.pow(s*(s-side1)*(s-side2)*(s-side3),0.5);

		// calculate area of PCA
		side1=p1.distance(p);
		side2=p3.distance(p);
		side3=p1.distance(p3);
		s=(side1+side2+side3)/2;
		
		areaPCA=Math.pow(s*(s-side1)*(s-side2)*(s-side3),0.5);

		// calculate area of ABC
		side1=p1.distance(p2);
		side2=p2.distance(p3);
		side3=p3.distance(p1);
		
		s=(side1+side2+side3)/2;
		
		areaABC=Math.pow(s*(s-side1)*(s-side2)*(s-side3),0.5);

		// if areaPAB + areaPBC + areaPCA = areaABC, then p is in the ABC
        // However, there is some error(�~�t) in this calculation
        // So if the absolute value of (areaABC - areaPAB - areaPBC - areaPCA) is less than 0.5
        // we consider the point is inside the original triangle
        // you can use Math.abs(double) to calculate the absolute value
		
		if (Math.abs(areaABC - areaPAB - areaPBC - areaPCA)<0.5)
		{
			return true;
		}
		else
		{
			return false;
		}

	}

	// check the input triangle(Triangle2D object) is in the original triangle of this object or not
	public boolean contains(Triangle2D input)
	{
		// if the input triangle is the original triangle of this object
        // then all points of the input triangle is in the original triangle of this object
		
		if (input.contains(input.getP1_new())==true && input.contains(input.getP2_new())==true && input.contains(input.getP3_new())==true)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}
