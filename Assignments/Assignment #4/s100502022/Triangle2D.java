package a4.s100502022;

public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0, 0.0); // points of original triangle triangle
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint p1_new, p2_new, p3_new; // points of new triangle

	private double s;
	public void sets(){
		s=getPerimeter()/2;
	}
	public MyPoint getP1_new(){ // get the p1 of new triangle
		return p1_new;
	}

	public MyPoint getP2_new(){ // get the p2 of new triangle
		return p2_new;
	}

	public MyPoint getP3_new(){ // get the p3 of new triangle
		return p3_new;
	}

	// constructor with arguments to initial the points of the new triangle
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3){
		p1_new =new MyPoint(x1,y1);
		p2_new =new MyPoint(x2,y2);
		p3_new =new MyPoint(x3,y3);
	}

	public double getArea(){ // calculate the area of the new triangle
		sets();//use the method
		return Math.sqrt(s*(s-p1_new.distance(p2_new))*(s-p2_new.distance(p3_new))*(s-p3_new.distance(p1_new)));
	}

	public double getPerimeter(){ // calculate the perimeter of the new triangle
		return p1_new.distance(p2_new)+p2_new.distance(p3_new)+p3_new.distance(p1_new);
	}
	
	// check the input point is in the original triangle of the object or not
	public boolean contains(MyPoint p){
		// if input point p is in the triangle, area of PAB + area of PBC + area of PCA = area of ABC 
		double side1, side2, side3, s, areaPAB, areaPBC, areaPCA, areaABC,sPAB,sPBC,sPCA;
		side1=p1.distance(p2);
		side2=p2.distance(p3);
		side3=p3.distance(p1);
		s=(side1+side2+side3)/2;
		sPAB=(side1+p1.distance(p)+p2.distance(p))/2;
		sPBC=(side2+p2.distance(p)+p3.distance(p))/2;
		sPCA=(side3+p3.distance(p)+p1.distance(p))/2;
		// calculate area of PAB
		areaPAB=Math.sqrt(sPAB*(sPAB-p1.distance(p))*(sPAB-p2.distance(p))*(sPAB-side1));

		// calculate area of PBC
		areaPBC=Math.sqrt(sPBC*(sPBC-p2.distance(p))*(sPBC-p3.distance(p))*(sPBC-side2));

		// calculate area of PCA
		areaPCA=Math.sqrt(sPCA*(sPCA-p3.distance(p))*(sPCA-p1.distance(p))*(sPCA-side3));

		// calculate area of ABC
		areaABC=Math.sqrt(s*(s-side1)*(s-side2)*(s-side3));

		// if areaPAB + areaPBC + areaPCA = areaABC, then p is in the ABC
                // However, there is some error(�~�t) in this calculation
                // So if the absolute value of (areaABC - (areaPAB + areaPBC + areaPCA)) is less than 0.5
                // we consider the point is inside the original triangle
                // you can use Math.abs(double) to calculate the absolute value
		double result=Math.abs(areaABC - (areaPAB + areaPBC + areaPCA));
		if(result<0.5){
			return true;
		}
		else 
			return false; 

	}

	// check the input triangle(Triangle2D object) is in the original triangle of this object or not
	public boolean contains(Triangle2D input){
	       // if the input triangle is in the original triangle of this object
               // then all points of the input triangle is in the original triangle of this object
		if(contains(input.p1)!=true||contains(input.p2)!=true||contains(input.p3)!=true){
			return false;
		}
		else 
			return true;
	}
}
