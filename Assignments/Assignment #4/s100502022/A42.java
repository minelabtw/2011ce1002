package a4.s100502022;
import java.util.Scanner;
public class A42 {
	public static void main(String[] args){
		Scanner input= new Scanner(System.in);
		//declare
		double x1,y1,x2,y2,x3,y3;
		int decision=0;
		//input the value
		System.out.println("Please enter three points of the new triangle:");
		x1=input.nextDouble();
		y1=input.nextDouble();
		x2=input.nextDouble();
		y2=input.nextDouble();
		x3=input.nextDouble();
		y3=input.nextDouble();
		//print what use need
		System.out.println("1.get the area of the new triangle");
		System.out.println("2.get the perimeter of the new triangle");
		System.out.println("3.check the new triangle is in the original triangle or not:");
		System.out.println("4.enter a point and check the point is in the original triangle or not");
		System.out.println("5.Exit");
		System.out.println("What u want??(plz input a number (1-5)");
		//declare class to use
		Triangle2D Tri=new Triangle2D(x1,y1,x2,y2,x3,y3);
		//loop start
		while(decision!=5){
			decision=input.nextInt();
			//decision choose
			switch(decision){
				case 1:
					System.out.print("The area of the new triangle is "+Tri.getArea());
					break;
				case 2:
					System.out.print("The perimeter of the new triangle is "+Tri.getPerimeter());
					break;
				case 3:
					System.out.print("Please input a new triangle for the new object: ");
					//declare new point of new Triangle
					double a1,b1,a2,b2,a3,b3;
					a1=input.nextDouble();
					b1=input.nextDouble();
					a2=input.nextDouble();
					b2=input.nextDouble();
					a3=input.nextDouble();
					b3=input.nextDouble();
					//let the value of Triangle's point constructor
					Triangle2D theotherTri=new Triangle2D(a1,b1,a2,b2,a3,b3);
					//if not print out the sentence
					if(Tri.contains(theotherTri)==false){
						System.out.print("The new triangle of new object is not in the original triangle of the old object!!!");
					}
					else
						System.out.print("The new triangle of new object is in the original triangle of the old object!!!");
					break;
				case 4:
					System.out.print("Please enter a new point:");
					//declare
					double a,b;
					a=input.nextDouble();
					b=input.nextDouble();
					
					MyPoint p=new MyPoint(a,b);
					//if not print out the sentence
					if(Tri.contains(p)==false){
						System.out.print("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!");
					}
					else 
						System.out.print("Yeah,the new point is in the original triangle (0,0), (17,6), (10, 15)!!!");
					break;
				case 5:
					System.out.print("Thank u");
					break;
			}
			
			
		}
	}
	
	

}
