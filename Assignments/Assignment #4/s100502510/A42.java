package a4.s100502510;

import java.util.Scanner;

public class A42 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		double x1, x2, x3, y1, y2, y3, x11, x12, x13, y11, y12, y13, x21, y21;

		while (1 == 1) {
			System.out
					.println("Please enter three points of the new triangle:");
			x1 = input.nextDouble();
			y1 = input.nextDouble();
			x2 = input.nextDouble();
			y2 = input.nextDouble();
			x3 = input.nextDouble();
			y3 = input.nextDouble();

			Triangle2D inputed = new Triangle2D(x1, y1, x2, y2, x3, y3);// 將輸入的數帶入三角形的class

			System.out.println("1.get the area of the new triangle");
			System.out.println("2.get the perimeter of the new triangle");
			System.out
					.println("3.check the new triangle is in the original triangle or not");
			System.out
					.println("4.enter a point and check the point is in the original triangle or not");
			System.out.println("5.Exit");
			int choice = input.nextInt();// 做為選擇方法的變數
			switch (choice) {
			case 1:
				System.out.println("The area of the new triangle is "
						+ inputed.getArea());// 使用Triangle2D的method
				break;
			case 2:
				System.out.println("The perimeter of the triangle is "
						+ inputed.getPerimeter());
				break;
			case 3:
				System.out
						.println("Please input a new triangle for the new object:");
				x11 = input.nextDouble();
				y11 = input.nextDouble();
				x12 = input.nextDouble();
				y12 = input.nextDouble();
				x13 = input.nextDouble();
				y13 = input.nextDouble();
				Triangle2D inputedtwo = new Triangle2D(x11, y11, x12, y12, x13,
						y13);// 重新帶入新的三角形
				if (inputed.contains(inputedtwo) == true) {
					System.out
							.println("The new triangle of new object is in the original triangle of the old object!!!");
				} else if (inputed.contains(inputedtwo) == false) {
					System.out
							.println("The new triangle of new object isn't in the original triangle of the old object!!!");
				}
				break;
			case 4:
				System.out.print("Please input the point:");
				x21 = input.nextDouble();
				y21 = input.nextDouble();
				MyPoint three = new MyPoint(x21, y21);
				if (inputed.contains(three) == true) {
					System.out
							.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!");
				} else if (inputed.contains(three) == false) {
					System.out
							.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!");
				}
				break;
			case 5:
				System.exit(0);// 結束程式
			}

		}
	}
}
