package a4.s100502510;

public class MyPoint {
	private double x;
	private double y;

	public MyPoint() {
		double x;
		double y;

	}

	public MyPoint(double x_input, double y_input) { // constructor to initial
		x = x_input;
		y = y_input;// private data member

	}

	public double getX() { // return x value
		return x;

	}

	public double getY() { // return y value
		return y;
	}

	public void setX(double input) // set x value
	{
		x = input;
	}

	public void setY(double input) // set y value
	{
		y = input;
	}

	public double distance(MyPoint input) { // the distance between this point
		return Math.pow(
				input.getX() * input.getX() + input.getY() * input.getY(), 0.5); // and
																					// the
																					// input
																					// point

	}
}
