package a4.s100502510;

public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0, 0.0); // points of original triangle
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint p1_new, p2_new, p3_new; // points of new triangle

	public MyPoint getP1_new() { // get the p1 of new triangle
		return p1_new;
	}

	public MyPoint getP2_new() { // get the p2 of new triangle
		return p2_new;
	}

	public MyPoint getP3_new() { // get the p3 of new triangle
		return p3_new;
	}

	// constructor with arguments to initial the points of the new triangle
	public Triangle2D(double x1, double y1, double x2, double y2, double x3,
			double y3) {
		p1_new = new MyPoint(x1, y1);
		p2_new = new MyPoint(x2, y2);
		p3_new = new MyPoint(x3, y3);

	}

	public double getArea() { // calculate the area of the new triangle
		double one = Math.pow(
				(p1_new.getX() - p2_new.getX())
						* (p1_new.getX() - p2_new.getX())
						+ (p1_new.getY() - p2_new.getY())
						* (p1_new.getY() - p2_new.getY()), 0.5);
		double two = Math.pow(
				(p1_new.getX() - p3_new.getX())
						* (p1_new.getX() - p3_new.getX())
						+ (p1_new.getY() - p3_new.getY())
						* (p1_new.getY() - p3_new.getY()), 0.5);
		double three = Math.pow(
				(p2_new.getX() - p3_new.getX())
						* (p2_new.getX() - p3_new.getX())
						+ (p2_new.getY() - p3_new.getY())
						* (p2_new.getY() - p3_new.getY()), 0.5);
		double S = (one + two + three) / 2;
		return Math.pow(S * (S - one) * (S - two) * (S - three), 0.5);
	}

	public double getPerimeter() { // calculate the perimeter of the new
		double one = Math.pow(
				(p1_new.getX() - p2_new.getX())
						* (p1_new.getX() - p2_new.getX())
						+ (p1_new.getY() - p2_new.getY())
						* (p1_new.getY() - p2_new.getY()), 0.5);
		double two = Math.pow(
				(p1_new.getX() - p3_new.getX())
						* (p1_new.getX() - p3_new.getX())
						+ (p1_new.getY() - p3_new.getY())
						* (p1_new.getY() - p3_new.getY()), 0.5);
		double three = Math.pow(
				(p2_new.getX() - p3_new.getX())
						* (p2_new.getX() - p3_new.getX())
						+ (p2_new.getY() - p3_new.getY())
						* (p2_new.getY() - p3_new.getY()), 0.5);
		return one + two + three;// triangle

	}

	// check the input point is in the original triangle of the object or not
	public boolean contains(MyPoint p) {
		// if input point p is in the triangle, area of PAB + area of PBC + area
		// of PCA = area of ABC
		double side1, side2, side3, s, areaPAB, areaPBC, areaPCA, areaABC;
		side1 = Math.pow(
				(p1.getX() - p.getX()) * (p1.getX() - p.getX())
						+ (p1.getY() - p.getY()) * (p1.getY() - p.getY()), 0.5);
		side2 = Math.pow(
				(p2.getX() - p.getX()) * (p2.getX() - p.getX())
						+ (p2.getY() - p.getY()) * (p2.getY() - p.getY()), 0.5);
		side3 = Math.pow(
				(p3.getX() - p.getX()) * (p3.getX() - p.getX())
						+ (p3.getY() - p.getY()) * (p3.getY() - p.getY()), 0.5);
		double S1 = (side1 + side2 + Math.pow(325.0, 0.5)) / 2;
		areaPCA = Math.pow(
				S1 * (S1 - side1) * (S1 - side2) * (S1 - Math.pow(325.0, 0.5)),
				0.5);// calculate area of PAC
		double S2 = (side3 + side2 + Math.pow(130.0, 0.5)) / 2;
		areaPBC = Math.pow(
				S2 * (S2 - side3) * (S2 - side2) * (S2 - Math.pow(130.0, 0.5)),
				0.5);// calculate area of PBC
		double S3 = (side3 + side1 + Math.pow(325.0, 0.5)) / 2;
		areaPAB = Math.pow(
				S3 * (S3 - side3) * (S3 - side1) * (S3 - Math.pow(325.0, 0.5)),
				0.5);// calculate area of PAB
		s = (Math.pow(325.0, 0.5) + Math.pow(325.0, 0.5) + Math.pow(130.0, 0.5)) / 2;
		areaABC = Math.pow(
				s * (s - Math.pow(325.0, 0.5)) * (s - Math.pow(325.0, 0.5))
						* (s - Math.pow(130.0, 0.5)), 0.5);// calculate area of
															// ABC

		if (areaABC - (areaPAB + areaPBC + areaPCA) < 0.5) {
			return true;// if areaPAB + areaPBC + areaPCA = areaABC, then p is
						// in the ABC
		}
		// However, there is some error(�~�t) in this calculation
		// So if the absolute value of (areaABC - areaPAB + areaPBC + areaPCA)
		// is less than 0.5
		// we consider the point is inside the original triangle
		// you can use Math.abs(double) to calculate the absolute value
		else {
			return false;
		}

	}

	// check the input triangle(Triangle2D object) is in the original triangle
	// of this object or not
	public boolean contains(Triangle2D input) {
		if (contains(input.getP1_new()) && contains(input.getP2_new())
				&& contains(input.getP3_new()) == true) {
			return true;

		} else {
			return false;
		}
		// if the input triangle is the original triangle of this object
		// then all points of the input triangle is in the original triangle of
		// this object
	}
}
