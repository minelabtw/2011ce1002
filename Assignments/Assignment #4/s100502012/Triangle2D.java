package a4.s100502012;

public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0, 0.0); // points of original triangle triangle
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint p1new,p2new,p3new; // points of new triangle

	public MyPoint getP1_new(){ // get the p1 of new triangle
		return p1new;
	}

	public MyPoint getP2_new(){ // get the p2 of new triangle
        return p2new; 
	}

	public MyPoint getP3_new(){ // get the p3 of new triangle
        return p3new;
	}

	// constructor with arguments to initial the points of the new triangle
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3){
        p1new=new MyPoint(x1,y1);
        p2new=new MyPoint(x2,y2);
        p3new=new MyPoint(x3,y3);
	}

	public double getArea(){ // calculate the area of the new triangle
		double ax1=p1new.getX();
		double ay1=p1new.getY();
		double ax2=p2new.getX();
		double ay2=p2new.getY();
		double ax3=p3new.getX();
		double ay3=p3new.getY();
        double area=0.5*Math.abs((ax2-ax1)*(ay3-ay1)-(ax3-ax1)*(ay2-ay1));//0.5|x2-x1 y2-y1|
        return area;                                                      //   |x3-x1 y3-y1|  (�G����C��)
	}

	public double getPerimeter(){ // calculate the perimeter of the new triangle
        double l1=p1new.distance(p2new);
        double l2=p2new.distance(p3new);
        double l3=p3new.distance(p1new);
        double totalLength=l1+l2+l3;
        return totalLength;
	}

	// check the input point is in the original triangle of the object or not
	public boolean contains(MyPoint pass2){// using the characteristic which is 
		                                   // if the point on the line(function) then the value of the function will be zero
		                                   // and on the left or right side will be positive or negative.
		                                   // by using this , we can find a relationship between the the specific point and the three
		                                   // functions which compose the default triangle 
		double px=pass2.getX();
		double py=pass2.getY();
		double value1=function1(px,py);
		double value2=function2(px,py);
		double value3=function3(px,py);
		
		if (value1>=0 && value2<=0 && value3>=0){
		   return true;	
		}
		else {
		   return false;	
		}
	}
	
	// check the input triangle(Triangle2D object) is in the original triangle of this object or not
	public boolean contains(){//judging whether the three points that compose a new triangle are in the default triangle or not
		double tx1=p1new.getX();
		double ty1=p1new.getY();
		double tx2=p2new.getX();
		double ty2=p2new.getY();
		double tx3=p3new.getX();
		double ty3=p3new.getY();
		double value11=function1(tx1,ty1);
		double value12=function2(tx1,ty1);
		double value13=function3(tx1,ty1);
		
		double value21=function1(tx2,ty2);
		double value22=function2(tx2,ty2);
		double value23=function3(tx2,ty2);
		
		double value31=function1(tx3,ty3);
		double value32=function2(tx3,ty3);
		double value33=function3(tx3,ty3);
		
		if (value11>=0&value12<=0&value13>=0  &&  value21>=0&value22<=0&value23>=0  &&  value31>=0&value32<=0&value33>=0){
			return true;
		}
		else{
			return false;
		}
	       // if the input triangle is the original triangle of this object
               // then all points of the input triangle is in the original triangle of this object
	}
	
	public double function1(double x, double y){//y=1.5x
		double f1;
		f1=(1.5*(x-10))-y+15;
		return f1;
	}
	
	public double function2(double x, double y){//y=6x/17
		double f2;
		f2=(6*(x-17)/17)-y+6;
		return f2;
	}
	
	public double function3(double x, double y){//y-15=-9(x-10)/7
		double f3;
		f3=(-9*(x-10)/7)-y+15;
		return f3;
	}
}


