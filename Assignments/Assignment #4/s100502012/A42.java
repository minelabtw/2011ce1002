package a4.s100502012;

import java.util.Scanner;

public class A42 {
public static void main(String[] args){
    Scanner input=new Scanner(System.in);
    int flag=0;// fuse
    
    
    double p1x=0.0,p1y=0.0,p2x=0.0,p2y=0.0,p3x=0.0,p3y=0.0,pspx=0.0,pspy=0.0;
    System.out.println("Please enter three random points : ");
    p1x=input.nextDouble();
    p1y=input.nextDouble();
    p2x=input.nextDouble();
    p2y=input.nextDouble();
    p3x=input.nextDouble();
    p3y=input.nextDouble();  
    Triangle2D pass1=new Triangle2D(p1x,p1y,p2x,p2y,p3x,p3y);// send the default value
    
    while(flag==0){	
    	System.out.println("\n\n\n1.get the area of the new triangle\n2.get the perimeter of the new triangle\n3.check the new triangle is in the original triangle or not\n4.enter a point and check the point is in the original triangle or not\n5.Exit");
    	int order=input.nextInt();
    	switch(order){
    	    case 1:
    	        System.out.println("\nThe area of the new triangle is "+pass1.getArea()+" . ");
    	        break;
    	        
    	    case 2:
    	    	System.out.println("\nThe perimeter of the new triangle is "+pass1.getPerimeter()+" .");
    	    	break;
    	    	
    	    case 3:
    	    	double n1x=0.0,n1y=0.0,n2x=0.0,n2y=0.0,n3x=0.0,n3y=0.0;
    	        System.out.println("Please enter three random points : ");
    	        n1x=input.nextDouble();
    	        n1y=input.nextDouble();
    	        n2x=input.nextDouble();
    	        n2y=input.nextDouble();
    	        n3x=input.nextDouble();
    	        n3y=input.nextDouble();  
    	        pass1=new Triangle2D(n1x,n1y,n2x,n2y,n3x,n3y);
    	    	boolean bool1=pass1.contains();
  	            if (bool1==true){
    	    	   System.out.println("\nThe new triangle is in the default triangle .");  
  	            }
  	            else{
  	               System.out.println("\nThe new triangle is out of the default triangle .");	
  	            }
    	    	break;
    	    	
    	    case 4:
    	    	System.out.println("Please enter a random points : ");
    	        pspx=input.nextDouble();
  	            pspy=input.nextDouble();
  	            MyPoint pass2=new MyPoint(pspx,pspy);
  	            boolean bool2=pass1.contains(pass2);
  	            if (bool2==true){
    	    	   System.out.println("\nThe point is in the default triangle .");  
  	            }
  	            else{
  	               System.out.println("\nThe point is out of the default triangle .");	
  	            }
    	    	break;
    	    	
    	    case 5:
    	    	flag=1;
                break;
    	}	    
    }
}
}
