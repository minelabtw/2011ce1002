package a4.s100502513;

import java.lang.Math;

public class MyPoint {
	private double x;  
	private double y;

	public MyPoint() {  //單純宣告CLASS
		
	}

	public MyPoint(double inx, double iny) {   //宣告CLASS可以吃兩參數
		x = inx;
		y = iny;
	}

	public double getx() {  //回傳X值
		return x;
	}

	public double gety() {  //回傳Y值
		return y;
	}

	public void setx(double input) {  //設定X值
		x = input;
	}

	public void sety(double input) {  //設定Y值
		y = input;
	}

	public double distance(MyPoint input) {  //算距離
		double d = Math.pow( Math.pow((input.getx() - x), 2) + Math.pow((input.gety() - y), 2) , 0.5);  //{(X-X')^2+(Y-Y')^2}^0.5
		return d;
	}
}
