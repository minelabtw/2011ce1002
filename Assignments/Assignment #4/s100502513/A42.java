package a4.s100502513;

import java.util.Scanner;

public class A42 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Please enter three points of the new triangle:");
		double x1 = input.nextDouble();
		double y1 = input.nextDouble();
		double x2 = input.nextDouble();
		double y2 = input.nextDouble();
		double x3 = input.nextDouble();		
		double y3 = input.nextDouble();
		Triangle2D triangle = new Triangle2D(x1, y1, x2, y2, x3, y3);  //宣告新的物件AND傳入六個參數作為點座標
		for (;;) {
			System.out.println("\n1.get the area of the new triangle\n"  //列清單供使用者選擇
							 + "2.get the perimeter of the new triangle\n"
							 + "3.check the new triangle is in the original triangle or not\n"
							 + "4.enter a point and check the point is in the original triangle or not\n"
							 + "5.Exit");
			int choose = input.nextInt();
			switch(choose){
				case 1:  //求新三角形面積
					System.out.print("The area of the new triangle is " + triangle.getArea());
					break;
				case 2:  //求新三角形周長
					System.out.print("The perimeter of the new triangle is " + triangle.getPerimeter());
					break;
				case 3:  //求新宣告的三角形是否在舊三角形內
					System.out.println("Please input a new triangle for the new object:");
					double nx1 = input.nextDouble();
					double ny1 = input.nextDouble();
					double nx2 = input.nextDouble();
					double ny2 = input.nextDouble();
					double nx3 = input.nextDouble();										
					double ny3 = input.nextDouble();
					Triangle2D triangle2 = new Triangle2D(nx1, ny1, nx2, ny2, nx3, ny3);  //宣告新的物件
					if(triangle.contains(triangle2))
						System.out.print("The new triangle of new object is in the original triangle of the old object!!!\n");
					else
						System.out.print("The new triangle of new object is Not in the original triangle of the old object!!!\n");
					break;
				case 4:  //求點是否在三角形內
					double npx = input.nextDouble();
					double npy = input.nextDouble();
					MyPoint newPoint = new MyPoint(npx,npy);  //宣告新的點物件
					if(triangle.contains(newPoint))
						System.out.print("The new point is in the original triangle (0,0),(17,6),(10,15)!!!\n");
					else 
						System.out.print("The new point is Not in the original triangle (0,0),(17,6),(10,15)!!!\n");
					break;
				case 5:  //離開程式
					System.out.print("Bye Bye\n");
					System.exit(0);
				default:  //輸入無效數字
					System.out.print("Please choose again!!\n");
					break;
			}
		}
	}
}
