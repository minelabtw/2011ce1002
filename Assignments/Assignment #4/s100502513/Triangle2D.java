package a4.s100502513;

import java.lang.Math;

public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0, 0.0);  //セ㏕﹚ぇ3翴
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint newp1, newp2, newp3;  //块穝翴
	
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3) {  //肚把计倒翴
		newp1 = new MyPoint(x1, y1);
		newp2 = new MyPoint(x2, y2);
		newp3 = new MyPoint(x3, y3);
	}
	
	public MyPoint getnewP1() {  //肚P1翴
		return newp1;
	}
	  
	public MyPoint getnewP2() {  //肚P2翴
		return newp2;
	}
	 
	public MyPoint getnewP3() {  //肚P3翴
		return newp3;
	}
	 
	

	public double getArea() {  //―穝翴ぇ縩
		double s = (newp1.distance(newp2) + newp1.distance(newp3) + newp2.distance(newp3))/2 ;  //纒そΑぇS
		double a = Math.pow(s * (s - newp1.distance(newp2)) * (s - newp1.distance(newp3))* (s - newp2.distance(newp3)), 0.5);  //纒そΑ
		return a;
	}
	
	public double getPerimeter() {  //―穝翴ぇ㏄
		double s = (newp1.distance(newp2) + newp1.distance(newp3) + newp2.distance(newp3));
		return s;
	}

	public boolean contains(MyPoint p) {  //―翴琌セ㏕﹚àず   
		double sPAB = (p.distance(p1) + p.distance(p2) + p1.distance(p2)) / 2;
		double sPAC = (p.distance(p1) + p.distance(p3) + p1.distance(p3)) / 2;
		double sPBC = (p.distance(p2) + p.distance(p3) + p2.distance(p3)) / 2;
		double sABC = (p1.distance(p2) + p1.distance(p3) + p2.distance(p3)) / 2;
		double areaPAB = Math.pow(sPAB * (sPAB - p.distance(p1)) * (sPAB - p.distance(p2))* (sPAB - p1.distance(p2)), 0.5);
		double areaPAC = Math.pow(sPAC * (sPAC - p.distance(p1)) * (sPAC - p.distance(p3))* (sPAC - p1.distance(p3)), 0.5);
		double areaPBC = Math.pow(sPBC * (sPBC - p.distance(p2)) * (sPBC - p.distance(p3))* (sPBC - p2.distance(p3)), 0.5);;
		double areaABC = Math.pow(sABC * (sABC - p1.distance(p2)) * (sABC - p1.distance(p3))* (sABC - p2.distance(p3)), 0.5);
		if((areaPAB+areaPAC+areaPBC)-areaABC < 0.5 && (areaPAB+areaPAC+areaPBC)-areaABC > -0.5)  //狦縩ぃ单┪玥à
			return true;
		else 
			return false;
	}

	public boolean contains(Triangle2D input) {  //肚穝CLASS把计―琌穝把计ààず
		if(contains(input.getnewP1()) && contains(input.getnewP2()) && contains(input.getnewP3()))
			return true;
		else
			return false;
	}
}
