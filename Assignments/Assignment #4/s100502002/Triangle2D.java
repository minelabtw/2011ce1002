package a4.s100502002;

public class Triangle2D 
{
	private MyPoint p1 = new MyPoint(0.0, 0.0); 
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);//設定三個原本的內定三角形
	private MyPoint new_p1,new_p2,new_p3;
	public MyPoint getP1_new()
	{
		return new_p1;
	}
	public MyPoint getP2_new()
	{
		return new_p2;

	}
	public MyPoint getP3_new()
	{
		return new_p3;
	}
	Triangle2D(double x1,double y1,double x2,double y2,double x3,double y3)
	{
		new_p1 = new MyPoint(x1,y1);
		new_p2 = new MyPoint(x2,y2);
		new_p3 = new MyPoint(x3,y3);//初始化三個新的點
	}
	public double getArea()//用海龍公式算面積
	{
		double a=new_p1.distance(new_p2);
		double b=new_p1.distance(new_p3);
		double c=new_p2.distance(new_p3);
		double s=(a+b+c)/2;
		double area = Math.sqrt(s*(s-a)*(s-b)*(s-c));
		return area;
		
		
	}
	public double getPerimeter()//計算周長
	{
		double a=new_p1.distance(new_p2);
		double b=new_p1.distance(new_p3);
		double c=new_p2.distance(new_p3);
		double s=a+b+c;
		return s;
		
	}
	public boolean contains(double x, double y)
	{
		boolean result=false;
		MyPoint point = new MyPoint(x,y);
		double pa = p1.distance(point);
		double pb = p2.distance(point);
		double pc = p3.distance(point);
		double ab=p1.distance(p2);
		double ca=p1.distance(p3);
		double bc=p2.distance(p3);//算各個邊長 pa pb pc ab ac bc
		double s1=(pa+pb+ab)/2;
		double s2=(pa+pc+ca)/2;
		double s3=(pb+pc+bc)/2;//算三個s
		double area_pAB = Math.sqrt(s1*(s1-pa)*(s1-pb)*(s1-ab));
		double area_pCA = Math.sqrt(s2*(s2-pa)*(s2-pb)*(s2-ca));
		double area_pBC = Math.sqrt(s3*(s3-pb)*(s3-pc)*(s3-bc));//用三次海龍公式算三個面積
		double three_area=area_pAB+area_pCA+area_pBC;
		double area=getArea();
		if(Math.abs(area)-Math.abs(three_area)<=0.5)
			result=true;//接受誤差小於0.5
		else
			result=false;
		return result;
	}
	public boolean contains(Triangle2D t)
	{
		boolean result=false;
		if(t.contains(t.getP1_new().getpx(),t.getP1_new().getpy())&&t.contains(t.getP2_new().getpx(),t.getP2_new().getpy())&&t.contains(t.getP3_new().getpx(),t.getP3_new().getpy()))
			result=true;//用上一個method算是否三個點都在原本的三角形裡
		else
			result=false;
		return result;
	}
	
}
