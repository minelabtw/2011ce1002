package a4.s100502002;

public class MyPoint {
	
	private double px;
	private double py;
	MyPoint()//用來宣告用的constructor
	{
		
	}
	MyPoint(double x,double y)//初始化用的
	{
		setpx(x);
		setpy(y);
		px=getpx();
		py=getpy();
	}
	public void setpx(double x)//設定private變數用的method
	{
		px=x;
	}
	public void setpy(double y)//設定private變數用的method
	{
		py=y;
	}
	public double getpx()//取出變數
	{
		return px;
	}
	public double getpy()//取出變數
	{
		return py;
	}
	public double distance(MyPoint input)
	{
		double result = Math.sqrt((px-input.getpx())*(px-input.getpx())+(py-input.getpy())*(py-input.getpy()));
		return result;
	}//計算點跟點之簽的距離
	
	
}
