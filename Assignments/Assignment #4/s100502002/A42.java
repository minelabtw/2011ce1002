package a4.s100502002;
import java.util.Scanner;
public class A42 {
	public static void main(String[] args){
		Scanner input=new Scanner(System.in);
		System.out.print("Please enter three points of the new triangle:");
		double triangle[][]= new double[3][2];//寫城陣列好像比較方便
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<2;j++)
			{
				triangle[i][j]=input.nextDouble();
			}
		}//讀入三個點
		Triangle2D tri1=new Triangle2D(triangle[0][0],triangle[0][1],triangle[1][0],triangle[1][1],triangle[2][0],triangle[2][1]);
		System.out.println("1.get the area of the new triangle");
		System.out.println("2.get the perimeter of the new triangle");
		System.out.println("3.check the new triangle is in the original triangle or not");
		System.out.println("4.enter a point and check the point is in the original triangle or not");
		System.out.println("5.exit");//輸出一些訊息
		
		for(;;)//無窮迴圈
		{
			int choise = input.nextInt();//選擇功能
			switch(choise)
			{
			case 1://顯示面積
				System.out.println("The area of the new triangle is "+tri1.getArea());
				break;
			case 2://顯示周長
				System.out.println("The perimeter of the new triangle is "+tri1.getPerimeter());
				break;
			case 3://比較三角形們
				System.out.println("Please input a new triangle for the new object:");
				double triangle2[][]= new double[3][2];
				for(int i=0;i<3;i++)
				{
					for(int j=0;j<2;j++)
					{
						triangle2[i][j]=input.nextDouble();
					}
				}
				Triangle2D tri2=new Triangle2D(triangle2[0][0],triangle2[0][1],triangle2[1][0],triangle2[1][1],triangle2[2][0],triangle2[2][1]);
				if(tri1.contains(tri2))
					System.out.println("The new triangle of new object is in the original triangle of the old object!!!");
				else
					System.out.println("The new triangle of new object is Not in the original triangle of the old object!!!");
				break;
			case 4://查看點是否再三角形裡
				double x = input.nextDouble();
				double y = input.nextDouble();
				if(tri1.contains(x, y))
					System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!");
				else
					System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!");
				break;
			case 5://離開
				System.out.println("See you next time~~");
				break;
			}
			if(choise==5)
				break;
		}
		
	}
}
