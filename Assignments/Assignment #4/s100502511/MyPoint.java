package a4.s100502511;

public class MyPoint {
	private double x;
	private double y;

	public MyPoint(double x_input, double y_input) { // 設定x,y的值
		x = x_input;
		y = y_input;
	}

	public double getX() { // 回傳x
		return x;
	}

	public double getY() { // 回傳y
		return y;
	}

	public void setX(double input) // x為input的值
	{
		x = input;
	}

	public void setY(double input) // y 為input的值
	{
		y = input;
	}

	public double distance(MyPoint input) { // 計算兩點間距離
		double Distance = Math.pow(
				Math.pow(x - input.getX(), 2) + Math.pow(y - input.getY(), 2),
				0.5);
		return Distance;
	}
}
