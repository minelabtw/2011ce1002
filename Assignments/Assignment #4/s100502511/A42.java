package a4.s100502511;

import java.util.Scanner;

public class A42 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.print("Please enter three points of the new triangle: "); // 輸入新三角形的三個點座標
		double p1x = input.nextDouble();
		double p1y = input.nextDouble();
		double p2x = input.nextDouble();
		double p2y = input.nextDouble();
		double p3x = input.nextDouble();
		double p3y = input.nextDouble();
		Triangle2D triangle = new Triangle2D(p1x, p1y, p2x, p2y, p3x, p3y);
		for (;;) {
			System.out.println("1.get the area of the new triangle");
			System.out.println("2.get the perimeter of the new triangle");
			System.out
					.println("3.check the new triangle is in the original triangle or not");
			System.out
					.println("4.enter a point and check the point is in the original triangle or not");
			System.out.println("5.Exit");
			int choosefunction = input.nextInt(); // 輸入要選擇的功能
			switch (choosefunction) {
			case 1: // 算面積

				System.out.println("1");
				System.out.println("The area of the new triangle is "
						+ triangle.getArea() + "\n");
				break;

			case 2: // 算周長
				System.out.println("2");
				System.out.println("The perimeter of the new triangle is "
						+ triangle.getPerimeter() + "\n");
				break;
			case 3: // 判斷下面輸入的三角形是否在原本的三角形內
				System.out.println("3");
				System.out
						.println("Please input a new triangle for the new object:"); // 輸入另一個三角形的三個點座標
				double newp1x = input.nextDouble();
				double newp1y = input.nextDouble();
				double newp2x = input.nextDouble();
				double newp2y = input.nextDouble();
				double newp3x = input.nextDouble();
				double newp3y = input.nextDouble();
				Triangle2D newtriangle = new Triangle2D(newp1x, newp1y, newp2x,
						newp2y, newp3x, newp3y);

				if (triangle.contains(newtriangle) == true) { // 如果為true則在三角形內
					System.out
							.println("The new triangle of new object is in the original triangle of the old object!!"
									+ "\n");
				} else {
					System.out
							.println("The new triangle of new object is NOT in the original triangle of the old object!!"
									+ "\n");
				}
				break;
			case 4: // 判斷輸入的點是否在原本的三角形內
				System.out.println("4");
				double newpx = input.nextDouble();
				double newpy = input.nextDouble();

				MyPoint newmypoint = new MyPoint(newpx, newpy);
				if (triangle.contains(newmypoint) == true) { // 如果為true則在三角形內
					System.out
							.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!"
									+ "\n");
				} else {
					System.out
							.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!"
									+ "\n");
				}
				break;
			case 5: // 離開程式
				System.out.printf("Exit!!");
				System.exit(0);
				break;
			}
		}
	}
}
