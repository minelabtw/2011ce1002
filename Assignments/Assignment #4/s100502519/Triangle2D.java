package a4.s100502519;

public class Triangle2D {

	private MyPoint p1_new, p2_new, p3_new;			/*points of new triangle*/

	public MyPoint getP1_new(){			/*get the p1 of new triangle*/
		return p1_new;
	}

	public MyPoint getP2_new(){			/*get the p2 of new triangle*/
			return p2_new;
	}

	public MyPoint getP3_new(){			/* get the p3 of new triangle*/
		return p3_new;
	}

	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3){			/*initial the points of the new triangle*/
		p1_new = new MyPoint(x1,y1);
		p2_new = new MyPoint(x2,y2);
		p3_new = new MyPoint(x3,y3);
	}

	public double getArea(){			/*calculate the area of the new triangle*/
		double lengthAB = p1_new.distance(p2_new);			//AB長
		double lengthBC = p2_new.distance(p3_new);			//BC長
		double lengthCA = p3_new.distance(p1_new);			//CA長
		double halfABC = (lengthAB+lengthBC+lengthCA)/2;			//ABC周長一半
		double areaABC = Math.sqrt(halfABC*(halfABC-lengthAB)*(halfABC-lengthBC)*(halfABC-lengthCA));			//ABC面積
		return areaABC;
	}

	public double getPerimeter(){			/*calculate the perimeter of the new triangle*/
		double lengthAB = p1_new.distance(p2_new);			//AB長
		double lengthBC = p2_new.distance(p3_new);			//BC長
		double lengthCA = p3_new.distance(p1_new);			//CA長
		return (lengthAB+lengthBC+lengthCA);			//ABC周長
	}

	public boolean contains(MyPoint p){
		double lengthAB = p1_new.distance(p2_new);			//AB長
		double lengthBC = p2_new.distance(p3_new);			//BC長
		double lengthCA = p3_new.distance(p1_new);			//CA長
		double halfABC = (lengthAB+lengthBC+lengthCA)/2;			//ABC周長一半
		double areaABC = Math.pow((halfABC*(halfABC-lengthAB)*(halfABC-lengthBC)*(halfABC-lengthCA)),0.5);
		double lengthAp = p1_new.distance(p);			//Ap長
		double lengthBp = p2_new.distance(p);			//Bp長
		double lengthCp = p3_new.distance(p);			//Cp長
		double halfABp = (lengthAB+lengthBp+lengthAp)/2;			//ABp周長一半
		double halfBCp = (lengthBC+lengthBp+lengthCp)/2;			//BCp周長一半
		double halfCAp = (lengthCA+lengthCp+lengthAp)/2;			//CAp周長一半
		double areaABp = Math.sqrt(halfABp*(halfABp-lengthAB)*(halfABp-lengthAp)*(halfABp-lengthBp));
		double areaBCp = Math.sqrt(halfBCp*(halfBCp-lengthBC)*(halfBCp-lengthBp)*(halfBCp-lengthCp));
		double areaCAp = Math.sqrt(halfCAp*(halfCAp-lengthCA)*(halfCAp-lengthCp)*(halfCAp-lengthAp));
		
		if(Math.abs(areaABC-areaABp-areaBCp-areaCAp)<=0.5){
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean contains(Triangle2D PointNew2) {
		boolean Newp1 = PointNew2.contains(PointNew2.getP1_new());
		boolean Newp2 = PointNew2.contains(PointNew2.getP2_new());
		boolean Newp3 = PointNew2.contains(PointNew2.getP3_new());
		
		if(Newp1 == true && Newp2 == true && Newp3 == true){			/*三點都在內才真正在裡面*/
			return true;
		}
		else {
			return false;
		}
	}	
}
