package a4.s100502519;
import java.util.Scanner;

public class A42 {
	public static void main (String[]args){
		Scanner input = new Scanner (System.in);
	
		Triangle2D PointOriginal = new Triangle2D(0,0,10,15,17,6);			//舊三角
		
		System.out.println("Please enter three points of the new triangle:");
		double nX1 = input.nextDouble();
		double nY1 = input.nextDouble();
		double nX2 = input.nextDouble();
		double nY2 = input.nextDouble();
		double nX3 = input.nextDouble();
		double nY3 = input.nextDouble();
		
		Triangle2D PointNew = new Triangle2D(nX1,nY1,nX2,nY2,nX3,nY3);			//新三角
		
		boolean finish = false;			//判斷要結束否
		
		while(finish == false){
			System.out.print("\n\n1.get the area of the new triangle\n" +
								"2.get the perimeter of the new triangle\n" +
								"3.check the new triangle is in the original triangle or not\n" +
								"4.enter a point and check the point is in the original triangle or not\n" +
								"5.Exit");
			
			int choice = input.nextInt();
			
			switch(choice){	
				case 1:
					System.out.println("\nThe area of the new triangle is " + PointNew.getArea());
					break;			//用getArea得新三角的面積
				
				case 2:
					System.out.println("\nThe perimeter of the new triangle is " + PointNew.getPerimeter());
					break;			//用getPerimeter得新三角的周長
				
				case 3:
					System.out.println("\nPlease input a new triangle for the new object: ");
					double NewX1 = input.nextDouble();
					double NewY1 = input.nextDouble();
					double NewX2 = input.nextDouble();
					double NewY2 = input.nextDouble();
					double NewX3 = input.nextDouble();
					double NewY3 = input.nextDouble();
					
					Triangle2D PointNew2 = new Triangle2D(NewX1,NewY1,NewX2,NewY2,NewX3,NewY3);			//新輸入的三角
					
					boolean InorNot1 = PointOriginal.contains(PointNew2);			//判斷是否在舊三角裡面
					
					if(InorNot1 == true){
						System.out.println("The new triangle of new object is in the original triangle of the old object!!!");
					}
					else {
						System.out.println("The new triangle of new object is NOT in the original triangle of the old object!!!");
					}
					
					break;
					
				case 4:
					System.out.println("\nPlease enter a new point: ");
					double nx = input.nextDouble();
					double ny = input.nextDouble();
					
					MyPoint extraP = new MyPoint(nx,ny);			//新的點

					boolean InorNot2 = PointOriginal.contains(extraP);			//判斷新點是否在舊三角內
					
					if(InorNot2 == true){
						System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!");
					}
					else {
						System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!");
					}
					
					break;
					
				case 5:
					finish = true;			//使跳出迴圈
					System.out.println("\nBye Bye~");
					break;
				
				default:
					System.out.println("\nYou should enter 1~5 !!!");
					break;
			}
		}
	}
}
