package a4.s100502545;

import java.util.Scanner;

public class A42 {

	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		System.out.println("Enter three points of the new triangle:");
		//宣告
		double x1,y1,x2,y2,x3,y3;
		x1=input.nextDouble();
		y1=input.nextDouble();
		x2=input.nextDouble();
		y2=input.nextDouble();
		x3=input.nextDouble();
		y3=input.nextDouble();
		
		Triangle2D t1 = new Triangle2D(x1, y1, x2, y2, x3, y3);
		System.out.println("1.get the area of the new triangle"+"\n"+"2.get the perimeter of the new triangle"+"\n"+"3.check the new triangle is in the original triangle or not"+"\n" +"4.enter a point and check the point is in the original triangle or not"+"\n"+"5.Exit");
		
		boolean flag = true;
		System.out.print("Please choose:");
		int choose ;
		choose = input.nextInt();
		
		while(flag==true)
		{
			switch(choose)
			{
			case 1:
				System.out.println("The area of the new triangle is :"+t1.getArea());
				flag=false;
				break;
				
			case 2:
				System.out.println("The perimeter of the new triangle is :"+t1.getPerimeter());
				break;
				
			case 3://New 三角型是否在三型型內
				System.out.println("Please input a new triangle for the new object:");
				double a1,a2,a3,b1,b2,b3;
				a1=input.nextDouble();
				b1=input.nextDouble();
				a2=input.nextDouble();
				b2=input.nextDouble();
				a3=input.nextDouble();
				b3=input.nextDouble();
				Triangle2D t2 = new Triangle2D(a1, b1, a2, b2, a3, b3);
				if (t1.contains(t2)) 
				{
					System.out.println("The new triangle of new object is in the original triangle of the old object");
				} 
				else
					System.out.println("The new triangle of new object is not in the original triangle of the old object");
				flag=false;
				break;
				
			case 4://是否在三角型內
				double inA = input.nextDouble();
				double inB = input.nextDouble();
				MyPoint p = new MyPoint(inA,inB);
				if (t1.contains(p) == true)
				{
					System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)");
				}
				else 				
					System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)");
				flag=false;
				break;
				
				
				
			case 5://離開
				System.out.println("Exit");
				flag=false;
			}
		}
	}
}
