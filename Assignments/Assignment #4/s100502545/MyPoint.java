package a4.s100502545;

public class MyPoint 
{	
	private double x;
	private double y;
	
	public MyPoint()
	{//the constructor do not catch the input
			
	}
	public MyPoint(double x_input,double y_input)
	{//the constructor catch the input
		x=x_input;
		y=y_input;		
	}
	
	public void setX(double inputX)
	{// set x value
		 x=inputX; 
	}
	
	public void setY(double inputY)
	{// set y value
		y=inputY;
	}
	
	public double getX()
	{// return x value
		return x;
	}
	public double getY()
	{// return y value
		return y;
	}
	
	public 	double distance(MyPoint p)
	{//to calculate the distance between two point
		return Math.pow(Math.pow((p.getX() - x), 2)+ Math.pow((p.getY() - y), 2), 0.5);
	}
	
}
