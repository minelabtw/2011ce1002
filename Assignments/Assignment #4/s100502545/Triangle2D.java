package a4.s100502545;

import java.util.Scanner;

public class Triangle2D {
	
		private MyPoint p1 = new MyPoint(0.0, 0.0); 
		private MyPoint p2 = new MyPoint(17.0, 6.0);
		private MyPoint p3 = new MyPoint(10.0, 15.0);
		private MyPoint p1_new, p2_new, p3_new;

		public MyPoint getP1_new()
		{  // get the p1 of new triangle
			return p1_new;
		}

		public MyPoint getP2_new()
		{ // get the p2 of new triangle
			return p2_new;
		}

		public MyPoint getP3_new()
		{ // get the p3 of new triangle
			return p3_new;
		}

		// constructor with arguments to initial the points of the new triangle
		public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3)
		{
			p1_new =new MyPoint(x1, y1);
			p2_new =new MyPoint(x2, y2);
			p3_new =new MyPoint(x3, y3);
		}

		public double getArea()
		{ // calculate the area of the new triangle
			double S=(p1_new.distance(p2_new)+p1_new.distance(p3_new)+p2_new.distance(p3_new))/2;
			
			double A=(Math.sqrt(S*(S-p1_new.distance(p2_new))*(S-p1_new.distance(p3_new))*(S-p2_new.distance(p3_new))));
			return A; 

		}

		public double getPerimeter()
		{
			double P = (p1_new.distance(p2_new) + p1_new.distance(p3_new) + p2_new.distance(p3_new));
			return P;
		}

		
		public boolean contains(MyPoint p)
		{// check the input point is in the original triangle of the object or not
			double side1, side2, side3, s, s1, s2, s3 ,areaPAB, areaPBC, areaPCA, areaABC;
			side1 = p.distance(p1);
			side2 = p.distance(p2);
			side3 = p.distance(p3);
			s1=(side1+side2+p1.distance(p2))/2;
			s2=(side2+side3+p2.distance(p3))/2;
			s3=(side3+side1+p3.distance(p1))/2;
			s=(p1.distance(p2)+p2.distance(p3)+p1.distance(p3))/2;
			
			areaPAB = Math.pow(s1*(s1-side1)*(s1-side2)*(s1-(p1.distance(p2))), 0.5);
			areaPBC = Math.pow(s2*(s2-side2)*(s2-side3)*(s2-(p2.distance(p3))), 0.5);
			areaPCA = Math.pow(s3*(s3-side3)*(s3-side1)*(s3-(p3.distance(p1))), 0.5);
			areaABC = Math.pow(s*(s-(p1.distance(p2)))*(s-(p2.distance(p3)))*(s-(p1.distance(p3))), 0.5);
			double resultArea = (areaPAB + areaPBC + areaPCA)-areaABC;
			
			if (resultArea<0.5 || resultArea>-0.5)
				return true;
			
			else
				return false;
		}
		public boolean contains(Triangle2D input) 
		{// check the input triangle(Triangle2D object) is in the original triangle of this object or not
			
		if (contains(input.getP1_new()) && contains(input.getP2_new())&& contains(input.getP3_new())) 
		{
			return true;
		}
		else
			return false;
		}


}