package a4.s992001033;
import java.util.Scanner;

public class A42
{
	public static void main(String[] args)
	{
		System.out.print("Please enter three points of the new triangle : ");
		Scanner input = new Scanner(System.in);//偵測輸入
		Triangle2D newTriangle = new Triangle2D(input.nextDouble(),input.nextDouble(),input.nextDouble(),input.nextDouble(),input.nextDouble(),input.nextDouble());//建立object
		int choose = 0;
		while(choose!=5)//5的話就離開
		{
			System.out.println("1.get the area of the new triangle");//列表
			System.out.println("2.get the perimeter of the new triangle");
			System.out.println("3.check the new triangle is in the original triangle or not");
			System.out.println("4.enter a point and check the point is in the original triangle or not");
			System.out.println("5.Exit");
			choose = input.nextInt();
			switch(choose)//選擇選項
			{
				case 1:
						System.out.println("The area of the new triangle is "+newTriangle.getArea());
						break;
				case 2:
						System.out.println("The perimeter of the new triangle is "+newTriangle.getPerimeter());
						break;
				case 3:
						System.out.print("Please input a new triangle for the new object : ");
						Triangle2D theOtherTriangle = new Triangle2D(input.nextDouble(),input.nextDouble(),input.nextDouble(),input.nextDouble(),input.nextDouble(),input.nextDouble());
						if(newTriangle.contains(theOtherTriangle))//在原本的三角形內
							System.out.println("The new triangle of new object is in the original triangle of the old object!!!");
						else
							System.out.println("The new triangle of new object is NOT in the original triangle of the old object!!!");
						break;
				case 4:
						MyPoint newPoint = new MyPoint(input.nextDouble(),input.nextDouble());
						if(newTriangle.contains(newPoint))//在原本的三角形內
							System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!");
						else
							System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)!!!");
						break;
			}
		}
		
	}
}
