package a4.s995002020;

public class MyPoint{//MyPoint method
	double[] x=new double[3];
	double[] y=new double[3];
	public MyPoint(double[] x,double[] y){
		this.x=x;
		this.y=y;
	}
	public boolean contains(double a, double b){
		double[] xx={0,0,0},yy={0,0,0};//the point (a,b) in triangle or not
		double result=0;
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				xx[j]=x[j];
				yy[j]=y[j];
			}
			xx[i]=a;
			yy[i]=b;
			MyPoint n=new MyPoint(xx,yy);
			result+=n.getArea();
		}
		return result==getArea();
	}
	public double getArea(){//calculate the area of the triangle
		double a=Math.abs((x[0]*y[1]+x[1]*y[2]+x[2]*y[0]-y[0]*x[1]-y[1]*x[2]-y[2]*x[0])/2.0);
		return a;
	}
	public double getPerimeter(){
		double a=Math.sqrt(Math.pow((x[0]-x[1]),2)+Math.pow((y[0]-y[1]),2));
		double b=Math.sqrt(Math.pow((x[0]-x[2]),2)+Math.pow((y[0]-y[2]),2));
		double c=Math.sqrt(Math.pow((x[1]-x[2]),2)+Math.pow((y[1]-y[2]),2));
		return a+b+c;
	}
	public boolean contains(Triangle2D t){//the new triangle in original triangle or not
		MyPoint a=t.or;
		boolean result=true;
		for(int i=0;i<3;i++){
			result=result & a.contains(t.xx[i],t.yy[i]);
		}
		return result;
	}
}