package a4.s995002020;
import java.io.*;
import java.util.Scanner;
public class A42 {
	public static void main(String args[]){//main function
		try{
			System.out.println("Please enter three points of the new triangle:");
			DataInputStream in=new DataInputStream(System.in);
			String[] str=(in.readLine()).split(" ");
			double[] x=new double[3];
			double[] y=new double[3];
			x[0]=Double.parseDouble(str[0]);//read double data
			y[0]=Double.parseDouble(str[1]);
			x[1]=Double.parseDouble(str[2]);
			y[1]=Double.parseDouble(str[3]);
			x[2]=Double.parseDouble(str[4]);
			y[2]=Double.parseDouble(str[5]);
			Triangle2D d=new Triangle2D(x,y);
			MyPoint a=d.ne;
			System.out.println("1.get the area of the new triangle \n2.get the perimeter of the new triangle \n3.check the new triangle is in the original triangle or not \n4.enter a point and check the point is in the original triangle or not \n5.Exit");
			boolean ex=false; //control switch
			while(!ex){
				Scanner s=new Scanner(System.in);
				int choose=s.nextInt();
				switch(choose){
					case 1:
						System.out.println("The area of the new triangle is "+a.getArea()+"\n");
						break;
					case 2:
						System.out.println("The perimeter of the new triangle is "+a.getPerimeter()+"\n");
						break;
					case 3:
						if(!a.contains(d))
							System.out.println("The new triangle is in the original triangle!!!\n");
						break;
					case 4:
						DataInputStream inp=new DataInputStream(System.in);
						String st[]=(inp.readLine()).split(" ");
						double n1=Double.parseDouble(st[0]),n2=Double.parseDouble(st[1]);
						if(!a.contains(n1,n2))
							System.out.println("The new point is NOT in the original triangle!!!\n");
						else 
							System.out.println("The new point is in the original triangle!!!\n");
						break;
					case 5:
						System.out.println("Thank you.Have a nice day.");
						ex=true;
						break;
					default:
						System.out.println("��J1~5");// if entered information is not 1-5 ,default method 
						break;
				}	
			}
		}catch(Exception e){
			System.out.println("���~");//default 
		}
	}

}
