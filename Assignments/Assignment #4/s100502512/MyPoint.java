package a4.s100502512;

public class MyPoint {

	private double x;
	private double y;

	public MyPoint() {// 宣告的constructure
		double x;
		double y;
	}

	public MyPoint(double x_input, double y_input) {// 初始話點坐標
		x = x_input;
		y = y_input;
	}

	public double getX() {// 取得x值
		return x;
	}

	public double getY() {// 取得y值
		return y;
	}

	public void setX(double input) {// 將輸入的x值回傳
		x = input;
	}

	public void setY(double input) {// 將輸入的y值回傳
		y = input;
	}

	public double distance(MyPoint input) {// 計算距離
		return Math.pow(// Math.pow用來計算次方(0.5次方=開更號)
				Math.pow((input.getX() - x), 2)
						+ Math.pow((input.getY() - y), 2), 0.5);
	}

}
