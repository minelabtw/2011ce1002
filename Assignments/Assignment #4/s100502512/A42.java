package a4.s100502512;

import java.util.Scanner;

public class A42 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Please enter three points of the new triangle:");
		double x1, y1, x2, y2, x3, y3;// 輸入三個點座標
		x1 = input.nextDouble();
		y1 = input.nextDouble();
		x2 = input.nextDouble();
		y2 = input.nextDouble();
		x3 = input.nextDouble();
		y3 = input.nextDouble();
		Triangle2D Test = new Triangle2D(x1, y1, x2, y2, x3, y3);// 使用Triangle2D這個class
		System.out
				.print("1.get the area of the new triangle"
						+ "\n"
						+ "2.get the perimeter of the new triangle"
						+ "\n"
						+ "3.check the new triangle is in the original triangle or not"
						+ "\n"
						+ "4.enter a point and check the point is in the original triangle or not"
						+ "\n" + "5.Exit" + "\n");// 顯示選項
		for (;;) {// 無限輸入
			System.out.println("Please input your chose:");
			int i = input.nextInt();
			switch (i) {
			case 1:
				System.out.println("The area of the new triangle is "
						+ Test.getArea());// 面積
				break;
			case 2:
				System.out.println("The perimeter of the new triangle is "
						+ Test.getPerimeter());// 周長
				break;
			case 3:// 看新三角形是否在舊三角形裡面
				System.out
						.println("Please input a new triangle for the new object:");
				double a1,
				b1,
				a2,
				b2,
				a3,
				b3;
				a1 = input.nextDouble();
				b1 = input.nextDouble();
				a2 = input.nextDouble();
				b2 = input.nextDouble();
				a3 = input.nextDouble();
				b3 = input.nextDouble();
				Triangle2D test2 = new Triangle2D(a1, b1, a2, b2, a3, b3);
				if (Test.contains(test2)) {
					System.out
							.println("The new triangle of new object is in the original triangle of the old object");
				} else
					System.out
							.println("The new triangle of new object is not in the original triangle of the old object");
				break;
			case 4:// 看點是否在舊三角形裡頭
				double a = input.nextDouble();
				double b = input.nextDouble();
				MyPoint p = new MyPoint(a, b);
				if (Test.contains(p) == true) {
					System.out
							.println("The new point is in the original triangle (0,0), (17,6), (10, 15)");
				} else {
					System.out
							.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)");
				}
				break;
			case 5:// 跳出
				System.exit(0);
				break;
			}

		}
	}

}
