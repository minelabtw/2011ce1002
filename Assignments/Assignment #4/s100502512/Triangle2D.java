package a4.s100502512;

import java.util.Scanner;

public class Triangle2D {
	private MyPoint p1 = new MyPoint(0.0, 0.0);// 原本的三個點
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint p1_new, p2_new, p3_new;// 新的三個點

	public MyPoint getP1_new() {
		return p1_new;// 取得p1_new坐標
	}

	public MyPoint getP2_new() {// 取得p2_new坐標
		return p2_new;
	}

	public MyPoint getP3_new() {// 取得p3_new坐標
		return p3_new;
	}

	public Triangle2D(double x1, double y1, double x2, double y2, double x3,
			double y3) {// 用MyPoint來取得點坐標
		p1_new = new MyPoint(x1, y1);
		p2_new = new MyPoint(x2, y2);
		p3_new = new MyPoint(x3, y3);
	}

	public double getArea() {// 面積
		double S = (p1_new.distance(p2_new) + p1_new.distance(p3_new) + p2_new
				.distance(p3_new)) / 2;
		double A = Math.pow(
				S * (S - p1_new.distance(p2_new))
						* (S - p1_new.distance(p3_new))
						* (S - p2_new.distance(p3_new)), 0.5);// 海龍公式
		return A;
	}

	public double getPerimeter() {// 周長
		double S = (p1_new.distance(p2_new) + p1_new.distance(p3_new) + p2_new
				.distance(p3_new));
		return S;
	}

	public boolean contains(Triangle2D input) {// 使用contains(MyPoint
												// p)來各自判斷三個點是否在三角形內,若皆為true則return
												// true
		if (contains(input.getP1_new()) && contains(input.getP2_new())
				&& contains(input.getP3_new())) {
			return true;
		} else
			return false;
	}

	public boolean contains(MyPoint p) {// 計算點是否在三角形內
		double side1, side2, side3, s, s1, s2, s3, areaPAB, areaPBC, areaPCA, areaABC, result = 0;
		side1 = p.distance(p1);
		side2 = p.distance(p2);
		side3 = p.distance(p3);
		s1 = (side1 + side2 + p1.distance(p2)) / 2;
		s2 = (side1 + side3 + p1.distance(p3)) / 2;
		s3 = (side2 + side3 + p2.distance(p3)) / 2;
		s = (p1.distance(p2) + p1.distance(p3) + p2.distance(p3)) / 2;
		areaPAB = Math.pow((s1 * (s1 - side1) * (s1 - side2)// 計算面積皆為海龍公式
		* (s1 - p1.distance(p2))), 0.5);
		areaPCA = Math.pow(
				(s2 * (s2 - side1) * (s2 - side3) * (s2 - p1.distance(p3))),
				0.5);
		areaPBC = Math.pow(
				(s3 * (s3 - side2) * (s3 - side3) * (s3 - p2.distance(p3))),
				0.5);
		areaABC = Math.pow(
				(s * (s - p1.distance(p2)) * (s - p1.distance(p3)) * (s - p3
						.distance(p2))), 0.5);
		result = Math.abs((areaPAB + areaPBC + areaPCA) - areaABC);// Math.abs用來計算準確的誤差值
		if (result < 0.5 && result > -0.5) {
			return true;
		} else
			return false;
	}

}
