package a4.s100502509;

public class MyPoint 
{
	private double x;	
	private double y;	
	public MyPoint()
	{	
		
	}
	
	public MyPoint(double x_input, double y_input)
	{ 
		x = x_input;
		y = y_input;
	}
	
	public double getX()
	{ 
		return x;	
	}
	
	public double getY()
	{ 
		return y;	
	}
	
	public void setX(double input) // set x value	
	{	
		x = input;
	}
	
	public void setY(double input) // set y value	
	{	
		y = input;
	}
	
	public double distance(MyPoint input)// the distance between this point and the input point
	{ 	
		return Math.pow(Math.pow((input.getX() - x), 2)+ Math.pow((input.getY() - y), 2), 0.5);
	}
	
	
	

}
