package a4.s100502509;
import java.util.*;
public class A42 
{
	public static void main(String [] args)
	{
		Scanner input = new Scanner(System.in);
		System.out.println("Please enter three points of the new triangle:");
		double x1 = input.nextDouble();//declare six points
		double y1 = input.nextDouble();
		double x2 = input.nextDouble();
		double y2 = input.nextDouble();
		double x3 = input.nextDouble();
		double y3 = input.nextDouble();
		Triangle2D triangle2D = new Triangle2D(x1, y1, x2, y2, x3, y3);// use constructor in the class Triangle2D
		System.out.println("1.get the area of the new triangle");
		System.out.println("2.get the perimeter of the new triangle");
		System.out.println("3.check the new triangle is in the original triangle or not");
		System.out.println("4.enter a point and check the point is in the original triangle or not");
		System.out.println("5.Exit");
		System.out.println("Which function do you want to use: ");
		
		for(;;)
		{
			int choose = input.nextInt();
			
			switch(choose)
			{
				
				case 1://use function to get the area of the new triangle
					System.out.println("The area of the new triangle is "+ triangle2D.getArea());
					break;
				
				case 2://use the function to get the perimeter of the new triangle
					System.out.println("The perimeter of the new triangle is "+ triangle2D.getPerimeter());
					break;
				
				case 3://function to check the new triangle is in the original triangle or not
					System.out.println("Please input a new triangle for the new object:");
					double x4 = input.nextDouble();
					double y4 = input.nextDouble();
					double x5 = input.nextDouble();
					double y5 = input.nextDouble();
					double x6 = input.nextDouble();
					double y6 = input.nextDouble();
					Triangle2D anotherTriangle2D = new Triangle2D(x4, y4, x5, y5, x6, y6);//declare object anotherTriangle2D and use constructor in class Triangle2D
					if ( triangle2D.contains(anotherTriangle2D) ) 
					{
						System.out.println("The new triangle of new object is in the original triangle of the old object");
					} 
					else
						System.out.println("The new triangle of new object is not in the original triangle of the old object");
					break;
				
				case 4://function to enter a point and check the point is in the original triangle or not
					System.out.println("Please enter the two points: ");
					double p1 = input.nextDouble();
					double p2 = input.nextDouble();
					MyPoint p = new MyPoint(p1,p2); //declare object p and pass p1,p2
					if( triangle2D.contains(p)==true )
					{
						System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)");
					}
					
					else
					{
						System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)");
					}
					break;
				
				case 5://exist
					System.out.println("Thank you for using!!Bye~Bye ");
					System.exit(0);
					break;
					
				default:
					System.out.println("It is error,please input again: ");
					break;
			}
		}
		
	}
}
