package a4.s100502029;
import java.util.Scanner;

public class A42 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		double x1, y1, x2, y2, x3, y3; // declare six variable to store three points
		
		// display information, let user input and store user's input
		System.out.println("Please enter three points of the new triangle:");
		System.out.print("First point(x, y): ");
		x1 = input.nextDouble();
		y1 = input.nextDouble();
		System.out.print("Second point(x, y): ");
		x2 = input.nextDouble();
		y2 = input.nextDouble();
		System.out.print("Third point(x, y): ");
		x3 = input.nextDouble();
		y3 = input.nextDouble();
		
		// declare an object of Triangle2D (use three points declare a triangle)
		Triangle2D triangle1 = new Triangle2D(x1, y1, x2, y2, x3, y3);
		
		// show functionality list
		System.out.println();
		System.out.println("1.Get the area of the new triangle");
		System.out.println("2.Get the perimeter of the new triangle");
		System.out.println("3.Check the new triangle is in the original triangle or not");
		System.out.println("4.Enter a point and check the point is in the original triangle or not");
		System.out.println("5.Exit");
		
		int choice = 0;
		while (choice != 5) { // infinite loop if user do not choose function 5
			System.out.println();
			System.out.print("Use function: ");
			choice = input.nextInt();
			switch (choice) { // determine which function that user choose
				case 1: // display the area of triangle
					System.out.println("The area of the new triangle is " + triangle1.getArea());
					break;
				case 2: // display the perimeter of the triangle
					System.out.println("The perimeter of the new triangle is " + triangle1.getPerimeter());
					break;
				case 3: // display the new triangle whether in the original triangle or not
					double x_1, y_1, x_2, y_2, x_3, y_3;
					System.out.println("Please input a new triangle for the new object:");
					System.out.print("First point(x, y): ");
					x_1 = input.nextDouble();
					y_1 = input.nextDouble();
					System.out.print("Second point(x, y): ");
					x_2 = input.nextDouble();
					y_2 = input.nextDouble();
					System.out.print("Third point(x, y): ");
					x_3 = input.nextDouble();
					y_3 = input.nextDouble();
					Triangle2D triangle2 = new Triangle2D(x_1, y_1, x_2, y_2, x_3, y_3);
					System.out.println("The new triangle of new object is " + (triangle1.contains(triangle2) ? "" : "NOT ") + "in the original triangle of the old object");
					break;
				case 4: // display the new point whether in the triangle or not
					double x, y;
					System.out.print("Please input a new point(x, y): ");
					x = input.nextDouble();
					y = input.nextDouble();
					MyPoint point = new MyPoint(x, y);
					System.out.println("The new point is " + (triangle1.contains(point) ? "" : "NOT ") + "in the original triangle (0,0), (17,6), (10, 15)");
					break;
				case 5: // exit the program
					System.exit(0);
				default: // display error message
					System.out.println("Input error!!");
					break;
			}
		}
	}
}
