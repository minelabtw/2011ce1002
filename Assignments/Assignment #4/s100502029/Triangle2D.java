package a4.s100502029;

public class Triangle2D {
	// points of original triangle 
	private MyPoint p1 = new MyPoint(0.0, 0.0);
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	
	// points of new triangle
	private MyPoint p1_new, p2_new, p3_new;
	
	// get the p1 of new triangle
	public MyPoint getP1_new() {
		return p1_new;
	}
	
	// get the p2 of new triangle
	public MyPoint getP2_new() {
		return p2_new;
	}
	
	// get the p3 of new triangle
	public MyPoint getP3_new() {
		return p3_new;
	}
	
	// constructor with arguments to initial the points of the new triangle
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3) {
		p1_new = new MyPoint(x1, y1);
		p2_new = new MyPoint(x2, y2);
		p3_new = new MyPoint(x3, y3);
	}
	
	// calculate the area of the new triangle
	public double getArea() {
		return Math.sqrt((getPerimeter() / 2) * (getPerimeter() / 2 - getP1_new().distance(getP2_new())) * (getPerimeter() / 2 - getP2_new().distance(getP3_new())) * (getPerimeter() / 2 - getP3_new().distance(getP1_new())));
	}
	
	// calculate the perimeter of the new triangle
	public double getPerimeter() {
		return getP1_new().distance(getP2_new()) + getP2_new().distance(getP3_new()) + getP3_new().distance(getP1_new());
	}
	
	// check the input point is in the original triangle of the object or not
	public boolean contains(MyPoint p) {
		double a = p2.getX() - p1.getX();
		double b = p2.getY() - p1.getY();
		double c = p3.getX() - p1.getX();
		double d = p3.getY() - p1.getY();
		double e = p.getX() - p1.getX();
		double f = p.getY() - p1.getY();
		double alpha = (e * d - b * f) / (a * d - b * c);
		double gamma = (a * f - e * c) / (a * d - b * c);
		if ( (alpha >= 0) && (gamma >= 0) && (alpha + gamma <= 1))
			return true;
		else
			return false;
	}
	
	// check the input triangle(Triangle2D object) is in the original triangle of this object or not
	public boolean contains(Triangle2D input) {
		if (input.contains(input.getP1_new()) && input.contains(input.getP2_new()) && input.contains(input.getP3_new()))
			return true;
		else
			return false;
	}
}
