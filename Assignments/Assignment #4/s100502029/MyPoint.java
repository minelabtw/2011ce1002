package a4.s100502029;

public class MyPoint {
	// declare private variable x and y
	private double x;
	private double y;
	
	// constructor to initial private data x=0, y=0
	public MyPoint() {
		x = 0.0;
		y = 0.0;
	}
	
	// constructor to initial private data member
	public MyPoint(double x_input, double y_input) {
		setX(x_input);
		setY(y_input);
	}
	
	// return x value
	public double getX() {
		return x;
	}
	
	// return y value
	public double getY() {
		return y;
	}
	
	// set x value
	public void setX(double input) {
		x = input;
	}
	
	// set y value
	public void setY(double input) {
		y = input;
	}
	
	// the distance between this point and the input point
	public double distance(MyPoint input) {
		return Math.sqrt(Math.pow((getX() - input.getX()), 2.0) + Math.pow((getY() - input.getY()), 2.0));
	}
}
