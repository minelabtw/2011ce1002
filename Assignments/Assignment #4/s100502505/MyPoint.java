package a4.s100502505;

public class MyPoint {
	private double x;
	private double y;
	
	public MyPoint(){}
	
	public MyPoint(double x_input,double y_input)//點座標
	{
		x = x_input;
		y = y_input;
	}
	
	public double getX()//X座標
	{
		return x;
	}
	
	public double getY()//Y座標
	{
		return y;
	}
	
	public void setX(double input)
	{
		x = input;
	}
	
	public void setY(double input)
	{
		y = input;
	}

	public double distance(MyPoint input)//算兩點的距離
	{
		double result = Math.sqrt(Math.pow(x - input.getX(),2) + Math.pow(y - input.getY(),2));
		return result;
	}
}//完成!
