package a4.s100502505;

import java.util.Scanner;

public class A42 {
	public static void main(String args[]) {
		System.out.print("Please enter three points of the new triangle:");//輸入三個點座標
		Scanner input = new Scanner(System.in);
		double p11 = input.nextDouble();//存入
		double p12 = input.nextDouble();
		double p21 = input.nextDouble();
		double p22 = input.nextDouble();
		double p31 = input.nextDouble();
		double p32 = input.nextDouble();
		Triangle2D tri = new Triangle2D(p11, p12, p21, p22, p31, p32);//宣告一個三角形
		Triangle2D tri2;
		MyPoint point;
		while (true) {//一直循環
			System.out.println("Please choose:");//五個選擇
			System.out.println("1.get the area of the new triangle");
			System.out.println("2.get the perimeter of the new triangle");
			System.out
					.println("3.check the new triangle is in the original triangle or not");
			System.out
					.println("4.enter a point and check the point is in the original triangle or not");
			System.out.println("5.Exit");
			int terms = input.nextInt();
			switch (terms) {
			case 1://新三角形的面積
				System.out.println("The area of the new triangle is ");
				System.out.println(tri.getArea());
				break;
			case 2://新三角形的周長
				System.out.println("The perimeter of the new triangle is ");
				System.out.println(tri.getPerimeter());
				break;
			case 3://看新三角形有沒有在舊三角形裡面
				System.out.println("Please input a new triangle for the new object:");
				p11 = input.nextDouble();//存入點座標
				p12 = input.nextDouble();
				p21 = input.nextDouble();
				p22 = input.nextDouble();
				p31 = input.nextDouble();
				p32 = input.nextDouble();
				tri2 = new Triangle2D(p11, p12, p21, p22, p31, p32);
				if (tri.contains(tri2)) {//如果有
					System.out.println("The new triangle of new object is in the original triangle of the old object!!!");
				} else {//如果沒有
					System.out.println("The new triangle of new object is not in the original triangle of the old object!!!");
				}
				break;
			case 4://看點有沒有在三角形裡面
				double point1 = input.nextDouble();//存入點座標
				double point2 = input.nextDouble();
				point = new MyPoint(point1, point2);
				if (tri.contains(point)) {//如果有
					System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!");
				} else {//如果沒有
					System.out.println("The new point is not in the original triangle (0,0), (17,6), (10, 15)!!!");
				}
				break;
			case 5://離開
				System.out.println("Exit");
				System.exit(0);
			}
		}
	}
}//完成!
