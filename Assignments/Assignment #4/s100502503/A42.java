package a4.s100502503;

import java.util.Scanner;

public class A42 {
	public static void main(String[] args)
	{		
		Scanner input = new Scanner(System.in);
		
		//declare a new Triangle2D
		System.out.println("Please enter three points of the new triangle: ");
		double x1 = input.nextDouble();	double y1 = input.nextDouble();
		double x2 = input.nextDouble();	double y2 = input.nextDouble();
		double x3 = input.nextDouble();	double y3 = input.nextDouble();

		Triangle2D triangleNew1 = new Triangle2D(x1, y1, x2, y2, x3, y3);
		
		//declare a flag determine whether stop or not
		boolean flag = true;
		
		while(flag)
		{
			System.out.println("\n1.get the area of the new triangle");
			System.out.println("2.get the perimeter of the new triangle");
			System.out.println("3.check the new triangle is in the original triangle or not");
			System.out.println("4.enter a point and check the point is in the original triangle or not");
			System.out.println("5.Exit");
			
			int choice = input.nextInt();
			switch(choice)
			{
				case 1:
					System.out.println("The area of the new triangle is " + triangleNew1.getArea());
					break;
					
				case 2:
					System.out.println("The perimeter of the new triangle is " + triangleNew1.getPerimeter());
					break;
				
				//check if the new triangle is in the old one
				case 3:
					System.out.println("Please input a new triangle for the new object: ");
					double x1N = input.nextDouble();	double y1N = input.nextDouble();
					double x2N = input.nextDouble();	double y2N = input.nextDouble();
					double x3N = input.nextDouble();	double y3N = input.nextDouble();
					
					Triangle2D triangleNew2 = new Triangle2D(x1N, y1N, x2N, y2N, x3N, y3N);
					
					if(triangleNew1.contains(triangleNew2) == true)
						System.out.println("The new triangle of new object is in the original triangle of the old object!!!");
					else
						System.out.println("The new triangle of new object is not in the original triangle of the old object...QQ");
					break;

				//check if the new point is in the old triangle
				case 4:
					System.out.println("Please input a new point: ");
					double x1NP = input.nextDouble();	double y1NP = input.nextDouble();
					
					MyPoint newPoint = new MyPoint(x1NP, y1NP);					
					
					if(triangleNew1.contains(newPoint) == true)
						System.out.println("The new point is in the original triangle (0,0), (17,6), (10, 15)!!!");
					else
						System.out.println("The new point is NOT in the original triangle (0,0), (17,6), (10, 15)...QQ");
					break;
				
				case 5:
					flag = false;
					break;
			}	
		}
		System.out.println("\nBYEBYE...");
	}

}
