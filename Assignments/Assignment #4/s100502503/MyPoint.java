package a4.s100502503;

public class MyPoint
{	
	private double x;
	private double y;
	
	public MyPoint()//constructor to initial
	{
	}
	
	public MyPoint(double input_x, double input_y)
	{
		setX(input_x);
		setY(input_y);
		getX();
		getY();
	}
	
	public double getX()// return x and y
	{
		return x;
	}
	
	public double getY()
	{
		return y;
	}
	
	public void setX(double input)//set x and y
	{
		x = input;
	}
	
	public void setY(double input)
	{
		y = input;
	}
	
	public double distance(MyPoint object)//calculate the distance between two point
	{
		double ans;
		ans = Math.sqrt(((object.getX()-x)*(object.getX()-x)) + ((object.getY()-y)*(object.getY()-y)));
		return ans;
	}
}
