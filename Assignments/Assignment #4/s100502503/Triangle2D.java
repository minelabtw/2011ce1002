package a4.s100502503;

public class Triangle2D 
{
	private MyPoint p1 = new MyPoint(0.0, 0.0);
	private MyPoint p2 = new MyPoint(17.0, 6.0);
	private MyPoint p3 = new MyPoint(10.0, 15.0);
	private MyPoint p1New = new MyPoint(0, 0);
	private MyPoint p2New = new MyPoint(0, 0);
	private MyPoint p3New = new MyPoint(0, 0);
	
	public MyPoint get_p1New()	//To get the new point that user entered
	{
		return p1New;
	}

	public MyPoint get_p2New()
	{
		return p2New;
	}
	
	public MyPoint get_p3New()
	{
		return p3New;
	}
	
	public Triangle2D(double x1, double y1, double x2, double y2, double x3, double y3)//The constructor that give the new point
	{
		p1New.setX(x1);
		p1New.setY(y1);
		p2New.setX(x2);
		p2New.setY(y2);
		p3New.setX(x3);
		p3New.setY(y3);
		
	}
	public double getArea()	//FUNCTION 1: To get the area of the new triangle
	{
		double aNew = p1New.distance(p2New);	//The side length of new triangle
		double bNew = p2New.distance(p3New);
		double cNew = p3New.distance(p1New);
		double s = 0.5 * (aNew + bNew + cNew);
		double ans = Math.sqrt(s * (s - aNew) * (s - bNew) * (s - cNew));
		
		return	ans;
	}
	
	public double getPerimeter()	//FUNCTION 2: To get the perimeter of the new triangle
	{
		double aNew = p1New.distance(p2New);	//The side length of new triangle
		double bNew = p2New.distance(p3New);
		double cNew = p3New.distance(p1New);
		double ans = aNew + bNew + cNew;
		return ans;
	}

	public boolean contains(MyPoint point)
	{
		double a = p1.distance(p2);	//The side length of original triangle
		double b = p2.distance(p3);
		double c = p3.distance(p1);
		
		double aPoint = point.distance(p1);	//The length between the new point and points of original triangle 
		double bPoint = point.distance(p2);
		double cPoint = point.distance(p3);
		boolean flag = false;
		
		double s_pP1P2 = 0.5 * (a + aPoint + bPoint);//The area of pP1P2
		double area_pP1P2 = Math.sqrt(s_pP1P2 * (s_pP1P2 - a) * (s_pP1P2 - aPoint) * (s_pP1P2 - bPoint));
		
		double s_pP2P3 = 0.5 * (b + bPoint + cPoint);//The area of pP2P3
		double area_pP2P3 = Math.sqrt(s_pP2P3 * (s_pP2P3 - b) * (s_pP2P3 - bPoint) * (s_pP2P3 - cPoint));
		
		double s_pP1P3 = 0.5 * (c + aPoint + cPoint);//The area of pP1P3
		double area_pP1P3 = Math.sqrt(s_pP1P3 * (s_pP1P3 - c) * (s_pP1P3 - aPoint) * (s_pP1P3 - cPoint)); 
		
		double s_original = 0.5 * (a + b + c);
		double area_original = Math.sqrt(s_original * (s_original - a) * (s_original - b) * (s_original - c));
		
		if(Math.abs(area_original - (area_pP1P2 + area_pP2P3 + area_pP1P3)) < 0.5)
			flag = true;
		
		return flag;
	}
	public boolean contains(Triangle2D tri)	//If three point are all in the original triangle, the new triangle is in the original one
	{
		boolean flag = false;
		
		if(tri.contains(get_p1New()) == tri.contains(get_p2New()) == tri.contains(get_p3New()) == true)
			flag = true;

		return flag;
	}
}
